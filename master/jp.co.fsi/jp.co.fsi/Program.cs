﻿using jp.co.fsi.common;
using jp.co.fsi.common.util;
using jp.co.fsi.Login;
using jp.co.fsi.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jp.co.fsi
{
#pragma warning disable 3270

	static class Program
	{


		#region ログ
		readonly static log4net.ILog log =
			log4net.LogManager.GetLogger(
				System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		#endregion

		/// <summary>
		/// アプリケーションのメイン エントリ ポイントです。
		/// </summary>
		[STAThread]
		static void Main()
		{
            //管理者として自分自身を起動する
            System.Diagnostics.ProcessStartInfo psi =
                new System.Diagnostics.ProcessStartInfo();
            //動詞に「runas」をつける
            psi.Verb = "runas";

			try
			{
				ConfigLoader cLdr = new ConfigLoader();

				string msgs = SSHConnection.SSHConnect();

				log.Info("画面起動開始");

				// Config.xmlの[UserInfo][doLogin]のフラグで起動画面を切り替える。
				if ("1" == cLdr.LoadCommonConfig("UserInfo", "dologin"))
				{
					log.Info("ログイン画面起動");
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);
					Application.Run(new FrmLogin());
				}
				else
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);
					Application.Run(new FrmMenu());
				}
			}
			catch (Exception ex)
			{
				log.Error("プログラム起動時のエラー：" + Environment.NewLine + ex.Message);
				MessageBox.Show("起動時のエラー:" + ex.Message);
			}
			finally
			{
				SSHConnection.SSHDisConnect();
			}
		}
	}
}
