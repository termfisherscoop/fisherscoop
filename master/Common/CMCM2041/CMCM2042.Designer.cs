﻿namespace jp.co.fsi.cm.cmcm2041
{
    partial class CMCM2042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonCd = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.lblJigyoKubun = new System.Windows.Forms.Label();
            this.txtJigyoKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtBumonKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonKanaNm = new System.Windows.Forms.Label();
            this.lblJigyoKbn = new System.Windows.Forms.Label();
            this.txtBumonNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(401, 23);
            this.lblTitle.Text = "部門の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 97);
            this.pnlDebug.Size = new System.Drawing.Size(418, 100);
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AutoSizeFromLength = true;
            this.txtBumonCd.DisplayLength = null;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBumonCd.Location = new System.Drawing.Point(132, 13);
            this.txtBumonCd.MaxLength = 6;
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.Size = new System.Drawing.Size(48, 20);
            this.txtBumonCd.TabIndex = 2;
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
            // 
            // lblBumonCd
            // 
            this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBumonCd.Location = new System.Drawing.Point(17, 11);
            this.lblBumonCd.Name = "lblBumonCd";
            this.lblBumonCd.Size = new System.Drawing.Size(166, 25);
            this.lblBumonCd.TabIndex = 1;
            this.lblBumonCd.Text = "部門コード";
            this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.lblJigyoKubun);
            this.pnlMain.Controls.Add(this.txtJigyoKbn);
            this.pnlMain.Controls.Add(this.txtBumonKanaNm);
            this.pnlMain.Controls.Add(this.lblBumonKanaNm);
            this.pnlMain.Controls.Add(this.lblJigyoKbn);
            this.pnlMain.Controls.Add(this.txtBumonNm);
            this.pnlMain.Controls.Add(this.lblBumonNm);
            this.pnlMain.Location = new System.Drawing.Point(12, 39);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(400, 93);
            this.pnlMain.TabIndex = 3;
            // 
            // lblJigyoKubun
            // 
            this.lblJigyoKubun.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJigyoKubun.Location = new System.Drawing.Point(167, 53);
            this.lblJigyoKubun.Name = "lblJigyoKubun";
            this.lblJigyoKubun.Size = new System.Drawing.Size(155, 25);
            this.lblJigyoKubun.TabIndex = 6;
            this.lblJigyoKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJigyoKbn
            // 
            this.txtJigyoKbn.AutoSizeFromLength = true;
            this.txtJigyoKbn.DisplayLength = null;
            this.txtJigyoKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJigyoKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtJigyoKbn.Location = new System.Drawing.Point(117, 55);
            this.txtJigyoKbn.MaxLength = 3;
            this.txtJigyoKbn.Name = "txtJigyoKbn";
            this.txtJigyoKbn.Size = new System.Drawing.Size(48, 20);
            this.txtJigyoKbn.TabIndex = 5;
            this.txtJigyoKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJigyoKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtJigyoKbn_KeyDown);
            this.txtJigyoKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtJigyoKbn_Validating);
            // 
            // txtBumonKanaNm
            // 
            this.txtBumonKanaNm.AutoSizeFromLength = false;
            this.txtBumonKanaNm.DisplayLength = null;
            this.txtBumonKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtBumonKanaNm.Location = new System.Drawing.Point(118, 30);
            this.txtBumonKanaNm.MaxLength = 30;
            this.txtBumonKanaNm.Name = "txtBumonKanaNm";
            this.txtBumonKanaNm.Size = new System.Drawing.Size(269, 20);
            this.txtBumonKanaNm.TabIndex = 3;
            this.txtBumonKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonKanaNm_Validating);
            // 
            // lblBumonKanaNm
            // 
            this.lblBumonKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblBumonKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBumonKanaNm.Location = new System.Drawing.Point(3, 28);
            this.lblBumonKanaNm.Name = "lblBumonKanaNm";
            this.lblBumonKanaNm.Size = new System.Drawing.Size(387, 25);
            this.lblBumonKanaNm.TabIndex = 2;
            this.lblBumonKanaNm.Text = "部門カナ名";
            this.lblBumonKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJigyoKbn
            // 
            this.lblJigyoKbn.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJigyoKbn.Location = new System.Drawing.Point(3, 53);
            this.lblJigyoKbn.Name = "lblJigyoKbn";
            this.lblJigyoKbn.Size = new System.Drawing.Size(165, 25);
            this.lblJigyoKbn.TabIndex = 4;
            this.lblJigyoKbn.Text = "事業区分";
            this.lblJigyoKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonNm
            // 
            this.txtBumonNm.AutoSizeFromLength = false;
            this.txtBumonNm.DisplayLength = null;
            this.txtBumonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtBumonNm.Location = new System.Drawing.Point(118, 5);
            this.txtBumonNm.MaxLength = 40;
            this.txtBumonNm.Name = "txtBumonNm";
            this.txtBumonNm.Size = new System.Drawing.Size(269, 20);
            this.txtBumonNm.TabIndex = 1;
            this.txtBumonNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonNm_Validating);
            // 
            // lblBumonNm
            // 
            this.lblBumonNm.BackColor = System.Drawing.Color.Silver;
            this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBumonNm.Location = new System.Drawing.Point(3, 3);
            this.lblBumonNm.Name = "lblBumonNm";
            this.lblBumonNm.Size = new System.Drawing.Size(387, 25);
            this.lblBumonNm.TabIndex = 0;
            this.lblBumonNm.Text = "部　門　名";
            this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CMCM2042
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 200);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtBumonCd);
            this.Controls.Add(this.lblBumonCd);
            this.Name = "CMCM2042";
            this.ShowFButton = true;
            this.Text = "部門の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblBumonCd, 0);
            this.Controls.SetChildIndex(this.txtBumonCd, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblBumonCd;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonNm;
        private System.Windows.Forms.Label lblBumonNm;
        private System.Windows.Forms.Label lblBumonKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonKanaNm;
        private System.Windows.Forms.Label lblJigyoKbn;
        private System.Windows.Forms.Label lblJigyoKubun;
        private common.controls.FsiTextBox txtJigyoKbn;
    }
}