﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm2031
{
    /// <summary>
    /// 船主マスタ一覧(KOBC9023)
    /// </summary>
    public partial class KOBC9023 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KOBC9023()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // ボタンの位置調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;

            this.txtShishoCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 船名CDに、
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShishoCdFr":
                case "txtShishoCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }
        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtShishoCdFr":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("cmcm2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.cmcm2031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShishoCdFr.Text = outData[0];
                                this.lblShishoCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShishoCdTo":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("cmcm2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.cmcm2031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShishoCdTo.Text = outData[0];
                                this.lblShishoCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // プレビュー処理
            DoPrint(true);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 船主コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtShishoCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtShishoCdTo.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 船主コードの入力チェック(先頭）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiCdFr()
        {
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.txtShishoCdFr.Text))
            {
                this.lblShishoCdFr.Text = "先　頭";
                return true;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShishoCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 4バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShishoCdFr.Text, this.txtShishoCdFr.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 名称を表示(存在しないコードを入力されたら空白表示)
            string shishoCd = this.txtShishoCdFr.Text;
            string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", shishoCd, this.txtShishoCdFr.Text);
            this.lblShishoCdFr.Text = name;

            return true;
        }

        /// <summary>
        /// 船主コードの入力チェック(最後）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtShishoCdTo.Text))
            {
                this.lblShishoCdTo.Text = "最　後";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShishoCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 4バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShishoCdTo.Text, this.txtShishoCdTo.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string shishoCd = this.txtShishoCdTo.Text;
            string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", shishoCd, this.txtShishoCdTo.Text);
            this.lblShishoCdTo.Text = name;

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 船主コードのチェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtShishoCdFr.Focus();
                this.txtShishoCdFr.SelectAll();
                return false;
            }

            // 船主コードのチェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtShishoCdTo.Focus();
                this.txtShishoCdTo.SelectAll();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            bool dataFlag;
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                Report rpt = new Report();
                rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "cmcm2031.mdb"), "R_cmcm2031", this.UnqId, isPreview);
            }

            // ワークテーブルに作成したデータを削除
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                this.Dba.Delete("PR_HN_TBL", "GUID = @GUID", dpc);
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備

            // 船主コード設定
            string SHISHO_CD_FR;
            string SHISHO_CD_TO;

            string FUNANUSHI_NM_FR;
            string FUNANUSHI_NM_TO;
            if (Util.ToDecimal(txtShishoCdFr.Text) > 0)
            {
                SHISHO_CD_FR = txtShishoCdFr.Text;
            }
            else
            {
                SHISHO_CD_FR = "0";
            }
            if (Util.ToDecimal(txtShishoCdTo.Text) > 0)
            {
                SHISHO_CD_TO = txtShishoCdTo.Text;
            }
            else
            {
                SHISHO_CD_TO = "9999";
            }
            FUNANUSHI_NM_FR = lblShishoCdFr.Text;
            FUNANUSHI_NM_TO = lblShishoCdTo.Text;
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            string FunanushiJusho;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            //VI_HN_TORIHIKISAKI_JOHOからデータ取得
            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM ");
            Sql.Append(" TB_CM_SHISHO ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append("ORDER BY ");
            Sql.Append(" SHISHO_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD_FR", SqlDbType.Decimal, 6, SHISHO_CD_FR);
            dpc.SetParam("@SHISHO_CD_TO", SqlDbType.Decimal, 6, SHISHO_CD_TO);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(" ,ITEM15");
                    Sql.Append(" ,ITEM16");
                    Sql.Append(" ,ITEM17");
                    Sql.Append(" ,ITEM18");
                    Sql.Append(" ,ITEM19");
                    Sql.Append(" ,ITEM20");
                    Sql.Append(" ,ITEM21");
                    Sql.Append(" ,ITEM22");
                    Sql.Append(" ,ITEM23");
                    Sql.Append(" ,ITEM24");
                    Sql.Append(" ,ITEM25");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(" ,@ITEM15");
                    Sql.Append(" ,@ITEM16");
                    Sql.Append(" ,@ITEM17");
                    Sql.Append(" ,@ITEM18");
                    Sql.Append(" ,@ITEM19");
                    Sql.Append(" ,@ITEM20");
                    Sql.Append(" ,@ITEM21");
                    Sql.Append(" ,@ITEM22");
                    Sql.Append(" ,@ITEM23");
                    Sql.Append(" ,@ITEM24");
                    Sql.Append(" ,@ITEM25");
                    Sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, SHISHO_CD_FR); // 船主コード（先頭）
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, SHISHO_CD_TO); // 船主コード（最後）
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, FUNANUSHI_NM_FR); // 船主名（先頭）
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, FUNANUSHI_NM_TO); // 船主名（最後）
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]); // 船主コード
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_NM"]); // 正式船主名
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["RYAKUSHO"]); // 略称船主名
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YUBIN_BANGO1"]); // 郵便番号1
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YUBIN_BANGO2"]); // 郵便番号2
                    FunanushiJusho = Util.ToString(dtMainLoop.Rows[i]["JUSHO1"]) + "" + Util.ToString(dtMainLoop.Rows[i]["JUSHO2"]);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, FunanushiJusho); // 住所
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FUNE_NM_CD"]); // 船主CD
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FUNE_NM"]); // 船名
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOHO_CD"]); // 漁法CD
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOHO_NM"]); // 漁法名
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KUMIAI_TESURYO_RITSU"], 2)); // 組合手数料率
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"]); // 普通口座番号
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FUTSU_KOZA_KUBUN"]); // 普通口座区分
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TSUMITATE_KUBUN"]); // 積立区分
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["TSUMITATE_RITSU"], 2)); // 積立率
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TSUMITATE_KOZA_BANGO"]); // 積立口座番号
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TSUMITATE_KOZA_KUBUN"]); // 積立口座区分
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["DENWA_BANGO"]); // 電話番号
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FAX_BANGO"]); // FAX番号
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHISHO_KUBUN"]); // 支所区分
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SEIJUN_KUBUN"]); // 正準区分

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion

                    i++;
                }
            }
            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}

