﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm2021
{
    /// <summary>
    /// 担当者の登録(CMCM2021)
    /// </summary>
    public partial class CMCM2021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region 変数
        private bool gridEnterFlag;          // 
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtShishoCd.Text = "1";
#else
            this.txtShishoCd.Text = this.UInfo.ShishoCd;
#endif
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = this.lblTitle.Text;
                // サイズを縮める
                this.Size = new Size(581,438);
                this.dgvList.Size = new Size(540, 296);
                // フォームの配置を上へ移動する
                this.lblMizuageShisho.Location = new System.Drawing.Point(312, 10);
                this.txtShishoCd.Location = new System.Drawing.Point(355, 12);
                this.lblShishoNm.Location = new System.Drawing.Point(390, 12);
                this.lblKanaNm.Location = new System.Drawing.Point(12, 10);
                this.txtKanaNm.Location = new System.Drawing.Point(89, 12);
                this.dgvList.Location = new System.Drawing.Point(12, 39);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.lblTitle.Visible = false;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;

                this.txtShishoCd.Text = (this.Par2 == "") ? this.UInfo.ShishoCd : this.Par2;
            }

            //this.ShowFButton = false;

            this.txtShishoCd.Enabled = (this.txtShishoCd.Text == "1") ? true : false;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtShishoCd.Text, this.txtShishoCd.Text);

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            //try
            //{
            //    this.dgvList.CurrentCell = null;
            //}
            //catch (Exception) { }

            // カナ名にフォーカス
            this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaNm.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 支所フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                //case "txtMizuageShishoCd":
                //    this.btnF1.Enabled = true;
                //    break;

                //default:
                //    this.btnF1.Enabled = false;
                //    break;
                case "txtKanaNm":
                    this.btnF1.Enabled = false;
                    break;
                default:
                    this.btnF1.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            int flg = 0;
            switch (this.ActiveCtlNm)
            {
                case "txtShishoCd": // 支所
                    // アセンブリのロード
                    Assembly asm = Assembly.LoadFrom("CMCM2031.exe");
                    // フォーム作成
                    Type t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                String[] result = (String[])frm.OutData;
                                this.txtShishoCd.Text = result[0];
                                this.lblShishoNm.Text = result[1];
                            }
                        }
                        flg = 1;
                    }
                    break;
                default:
                    // カナ名にフォーカスを戻す
                    this.txtKanaNm.Focus();
                    this.txtKanaNm.SelectAll();
                    flg = 1;
                    break;
            }
            // グリッド中からのF1対応
            if (gridEnterFlag && flg == 0)
            {
                // カナ名にフォーカスを戻す
                this.txtKanaNm.Focus();
                this.txtKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ担当者登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 担当者登録画面の起動
                EditTantosha(string.Empty);
            }
        }
#endregion

#region イベント
        /// <summary>
        /// 支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtShishoCd.Text, this.lblShishoNm.Text, this.txtShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtShishoCd.SelectAll();
                this.txtShishoCd.Focus();
            }
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaNm_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない

            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditTantosha(Util.ToString(this.dgvList.SelectedRows[0].Cells["担当者コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditTantosha(Util.ToString(this.dgvList.SelectedRows[0].Cells["担当者コード"].Value));
            }
        }

        /// <summary>
        /// グリッドEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Enter(object sender, EventArgs e)
        {
            gridEnterFlag = true;
            this.btnF1.Enabled = true;
            /*
            if (this.MenuFrm != null)
                ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = this.btnF1.Enabled;
                */
        }

        /// <summary>
        /// グリッドLeave時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Leave(object sender, EventArgs e)
        {
            gridEnterFlag = false;
            //try
            //{
            //    this.dgvList.CurrentCell = null;
            //}
            //catch (Exception) { }
        }

        private void dgvList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            gridEnterFlag = true;
            this.btnF1.Enabled = true;
            /*
            if (this.MenuFrm != null)
                ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = this.btnF1.Enabled;
                */
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtShishoCd.Text) || Equals(this.txtShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtShishoCd.Text = "0";
                this.lblShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtShishoCd.Text, this.txtShishoCd.Text);

            if (ValChk.IsEmpty(this.lblShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 担当者マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            if (isInitial)
            {
                // 初期処理の場合、検索結果がヒットしないようにあり得ない検索条件を設定する
                //where.Append(" AND TANTOSHA_CD = -1");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaNm.Text))
                {
                    where.Append(" AND TANTOSHA_KANA_NM LIKE @TANTOSHA_KANA_NM");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@TANTOSHA_KANA_NM", SqlDbType.VarChar, 30, "%" + this.txtKanaNm.Text + "%");
                }
            }
            string cols = "TANTOSHA_CD AS 担当者コード";
            cols += ", TANTOSHA_NM AS 担当者名";
            cols += ", TANTOSHA_KANA_NM AS 担当者カナ名";
            string from = "TB_CM_TANTOSHA";

            DataTable dtTantosha =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "TANTOSHA_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtTantosha.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtTantosha.Rows.Add(dtTantosha.NewRow());
            }

            this.dgvList.DataSource = dtTantosha;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 200;
            this.dgvList.Columns[2].Width = 209;

            //try
            //{
            //    this.dgvList.CurrentCell = null;
            //}
            //catch (Exception) { }
        }

        /// <summary>
        /// 担当者を追加編集する
        /// </summary>
        /// <param name="code">担当者コード(空：新規登録、以外：編集)</param>
        private void EditTantosha(string code)
        {
            CMCM2022 frmCMCM2022;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmCMCM2022 = new CMCM2022("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmCMCM2022 = new CMCM2022("2");
                frmCMCM2022.InData = code;
            }
            frmCMCM2022.Par2 = this.txtShishoCd.Text;

            DialogResult result = frmCMCM2022.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["担当者コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["担当者コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["担当者名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["担当者カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
