﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.com.calendar
{
    /// <summary>
    /// 休日メンテナンス画面(HolidayMnt)
    /// </summary>
    public partial class HolidayMnt : BasePgForm
    {
        #region プロパティ
        private int _targetYear;
        /// <summary>
        /// 対象年
        /// </summary>
        public int TargetYear
        {
            get
            {
                return this._targetYear;
            }
            set
            {
                this._targetYear = value;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HolidayMnt()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // Escape、F1、F3～8を表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = true;
            this.btnF5.Visible = true;
            this.btnF6.Visible = true;
            this.btnF7.Visible = true;
            this.btnF8.Visible = true;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 今年のデータを取得
            DispData();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveControl.Name)
            {
                case "txtYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            System.Reflection.Assembly asm = null;
            Type t = null;

            switch (this.ActiveControl.Name)
            {
                case "txtYear":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.txtGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.txtGengo.Text,
                                        this.txtYear.Text,
                                        "12",
                                        "31",
                                        this.Dba);
                                this.txtGengo.Text = arrJpDate[0];
                                this.txtYear.Text = arrJpDate[2];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // メッセージ表示
            if (Msg.ConfOKCancel("選択されている行を削除します。よろしいですか？") == DialogResult.Cancel)
            {
                return;
            }

            int curIdx = this.dgvList.SelectedRows[0].Index;
            bool lastRowFlg = (curIdx == this.dgvList.Rows.Count - 1);

            // 行削除処理
            this.dgvList.Rows.RemoveAt(curIdx);

            if (lastRowFlg)
            {
                // 最終行が削除された場合
                // カレントセルは選択されていた行の前の行の、先頭の列
                this.dgvList.CurrentCell = this.dgvList[0, curIdx - 1];

                // 新しい最終行を選択状態にする
                this.dgvList.Rows[curIdx - 1].Selected = true;
                this.dgvList.Focus();
            }
            else
            {
                // 最終行以外が削除された場合
                // 削除された次の行がカレント行
                this.dgvList.CurrentCell = this.dgvList[0, curIdx];
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 行追加処理(最下行に追加)
            DataTable dtSource = (DataTable)this.dgvList.DataSource;
            dtSource.Rows.Add(dtSource.NewRow());
            this.dgvList.DataSource = dtSource;

            // 追加された行がカレント行
            this.dgvList.CurrentCell = this.dgvList[0, this.dgvList.Rows.Count - 1];
            this.dgvList.Rows[this.dgvList.Rows.Count - 1].Selected = true;
            this.dgvList.Focus();
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // メッセージ表示
            if (Msg.ConfOKCancel("前年の情報を元に、今年のデータを作成します。"
                + Environment.NewLine + "現在登録されているデータは画面からクリアされます。"
                + Environment.NewLine + "よろしいですか？") == DialogResult.Cancel)
            {
                return;
            }

            // 前年の情報をコピー(仮に現状データがあってもクリア)
            CopyData();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 入力チェック(の必要無かったりして)
            if (!ValidateAll())
            {
                return;
            }

            // メッセージ表示
            if (Msg.ConfOKCancel("登録します。よろしいですか？") == DialogResult.Cancel)
            {
                return;
            }

            // 登録処理
            RegistData();

            // 再表示
            DispData();

            Msg.Info("登録が完了しました。");
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        public override void PressF7()
        {
            // 処理対象を前年に移動
            this._targetYear--;

            // データ表示
            DispData();
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            // 処理対象を翌年に移動
            this._targetYear++;

            // データ表示
            DispData();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 前年ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrevYear_Click(object sender, EventArgs e)
        {
            // 処理対象を前年に移動
            this._targetYear--;

            // データ表示
            DispData();
        }

        /// <summary>
        /// 翌年ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNextYear_Click(object sender, EventArgs e)
        {
            // 処理対象を翌年に移動
            this._targetYear++;

            // データ表示
            DispData();
        }

        /// <summary>
        /// 移動ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMoveMonth_Click(object sender, EventArgs e)
        {
            // 処理対象を指定した年に移動
            DateTime datTmp = Util.ConvAdDate(
                this.txtGengo.Text, this.txtYear.Text, "12", "31", this.Dba);
            this._targetYear = datTmp.Year;

            // データ表示
            DispData();
        }

        /// <summary>
        /// 明細のCellEnterイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 0:
                    // 日付なのでDisable
                    this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
                    break;

                case 1:
                    // 名称なのでOn
                    this.dgvList.ImeMode = System.Windows.Forms.ImeMode.On;
                    break;

                default:
                    this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Inherit;
                    break;
            }
        }

        /// <summary>
        /// 明細のDataErrorイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null)
            {
                Msg.Notice("入力値が不正です。");
                e.Cancel = true;
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 画面にデータを表示します。
        /// </summary>
        private void DispData()
        {
            // 西暦を元に和暦を表示
            string[] arrJpDate = Util.ConvJpDate(new DateTime(this._targetYear, 12, 31), this.Dba);
            this.txtGengo.Text = arrJpDate[0];
            this.txtYear.Text = arrJpDate[2];

            // 西暦部分を表示
            lblYearAddInfo.Text = "(" + Util.ToString(_targetYear) + "年)";

            // 対象年のデータを取得
            DataTable dtSource = GetData(this._targetYear);

            // 1行空行として追加したげる
            dtSource.Rows.Add(dtSource.NewRow());

            // データソースとしてバインド
            this.dgvList.DataSource = dtSource;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 100;
            this.dgvList.Columns[1].Width = 199;
            this.dgvList.Columns[2].Width = 100;
            this.dgvList.Columns[3].Width = 100;

            // 日付の列のフォーマット設定
            this.dgvList.Columns[0].DefaultCellStyle.Format = "yyyy/MM/dd";
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // カレントセルは最終行の先頭の列
            this.dgvList.CurrentCell = this.dgvList[0, this.dgvList.Rows.Count - 1];

            // 最終行を選択状態にする
            this.dgvList.Rows[this.dgvList.Rows.Count - 1].Selected = true;
            this.dgvList.Focus();
        }

        /// <summary>
        /// 休日データを取得します。
        /// (振替休日も込みで取得します)
        /// </summary>
        /// <param name="year">取得対象の年</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetData(int year)
        {
            return GetData(year, true);
        }

        /// <summary>
        /// 休日データを取得します。
        /// </summary>
        /// <param name="year">取得対象の年</param>
        /// <param name="inclFurikae">振替休日を含むか(true:含む)</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetData(int year, bool inclFurikae)
        {
            DateTime datFr = new DateTime(year, 1, 1);
            DateTime datTo = new DateTime(year, 12, 31);

            StringBuilder cols = new StringBuilder();
            cols.Append("DATE AS 日付");
            cols.Append(", HOLIDAY_NM AS 祝日名称");
            cols.Append(", CONVERT(bit, HM_KUBUN) AS ﾊｯﾋﾟｰﾏﾝﾃﾞｰ");
            cols.Append(", CONVERT(bit, FURIKAE_KUBUN) AS 振替休日");

            StringBuilder where = new StringBuilder();
            where.Append("DATE BETWEEN @DATE_FR AND @DATE_TO");
            if (!inclFurikae)
            {
                where.Append(" AND FURIKAE_KUBUN = 0");
            }

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, datFr);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, datTo);

            return this.Dba.GetDataTableByConditionWithParams(
                cols.ToString(), "TB_CM_HOLIDAY", where.ToString(), dpc);
        }

        /// <summary>
        /// 前年のデータをコピーして今年のデータを作成する
        /// </summary>
        /// <remarks>
        /// 現在の対象年の前年のデータをコピーして対象年のデータを作成する
        /// </remarks>
        private void CopyData()
        {
            // 前年のデータを取得する(振替休日を含まない)
            DataTable dtLastYear = GetData(this._targetYear - 1, false);
            DataTable dtTmp = dtLastYear.Clone();
            DataTable dtCurYear = dtLastYear.Clone();

            DateTime tmpDate;

            DataRow drLastYear;
            DataRow drTmp;
            int addWeeks;
            int addDays;
            DayOfWeek dow;
            DayOfWeek firstDayDow;
            bool furikaeSearchFlg = false;

            for (int i = 0; i < dtLastYear.Rows.Count; i++)
            {
                drLastYear = dtLastYear.Rows[i];

                if ((bool)drLastYear["ﾊｯﾋﾟｰﾏﾝﾃﾞｰ"])
                {
                    // ハッピーマンデーの場合、「第○×曜日」という規則を取得
                    addWeeks = (Util.ToDate(drLastYear["日付"]).Day - 1) / 7;
                    dow = Util.ToDate(drLastYear["日付"]).DayOfWeek;

                    // 翌年のその月の1日を基準に元と同じ「第○×曜日」の日付を算出
                    tmpDate = Util.ToDate(drLastYear["日付"]).AddYears(1);
                    tmpDate = new DateTime(tmpDate.Year, tmpDate.Month, 1);
                    firstDayDow = tmpDate.DayOfWeek;
                    addDays = dow - firstDayDow;
                    if (addDays < 0) addDays += 7;
                    tmpDate = tmpDate.AddDays(addWeeks * 7).AddDays(addDays);

                    drTmp = dtTmp.NewRow();
                    drTmp["日付"] = tmpDate;
                    drTmp["祝日名称"] = drLastYear["祝日名称"];
                    drTmp["ﾊｯﾋﾟｰﾏﾝﾃﾞｰ"] = drLastYear["ﾊｯﾋﾟｰﾏﾝﾃﾞｰ"];
                    drTmp["振替休日"] = drLastYear["振替休日"];
                    dtTmp.Rows.Add(drTmp);
                }
                else
                {
                    tmpDate = Util.ToDate(drLastYear["日付"]).AddYears(1);
                    drTmp = dtTmp.NewRow();
                    drTmp["日付"] = tmpDate;
                    drTmp["祝日名称"] = drLastYear["祝日名称"];
                    drTmp["ﾊｯﾋﾟｰﾏﾝﾃﾞｰ"] = drLastYear["ﾊｯﾋﾟｰﾏﾝﾃﾞｰ"];
                    drTmp["振替休日"] = drLastYear["振替休日"];
                    dtTmp.Rows.Add(drTmp);

                    if (tmpDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        tmpDate = Util.ToDate(drLastYear["日付"]);
                        furikaeSearchFlg = false;

                        // コピー対象の祝日が日曜日の場合、国民の祝日のレコードを作成する(月曜が祝日の場合火曜、水曜…)
                        while (!furikaeSearchFlg)
                        {
                            tmpDate = tmpDate.AddDays(1);
                            if (dtLastYear.Select("日付 = '" + tmpDate.ToString("yyyy/MM/dd") + "'").Length == 0)
                            {
                                furikaeSearchFlg = true;
                            }
                        }

                        drTmp = dtTmp.NewRow();
                        drTmp["日付"] = tmpDate.AddYears(1);
                        drTmp["祝日名称"] = "国民の祝日";
                        drTmp["ﾊｯﾋﾟｰﾏﾝﾃﾞｰ"] = false;
                        drTmp["振替休日"] = true;
                        dtTmp.Rows.Add(drTmp);
                    }
                }
            }

            // ここまでで登録した情報を日付順に整列する
            DataRow[] arrDrTmp = dtTmp.Select(null, "日付 ASC");
            for (int i = 0; i < arrDrTmp.Length; i++)
            {
                dtCurYear.ImportRow(arrDrTmp[i]);
            }

            // 新規行を追加
            dtCurYear.Rows.Add(dtCurYear.NewRow());

            // 画面に表示
            this.dgvList.DataSource = dtCurYear;

            // カレントセルは最終行の先頭の列
            this.dgvList.CurrentCell = this.dgvList[0, this.dgvList.Rows.Count - 1];

            // 最終行を選択状態にする
            this.dgvList.Rows[this.dgvList.Rows.Count - 1].Selected = true;
            this.dgvList.Focus();
        }

        /// <summary>
        /// 全項目の入力値チェックをします
        /// </summary>
        /// <returns>true:正しい、false:不正</returns>
        private bool ValidateAll()
        {
            DateTime datFr = new DateTime(this._targetYear, 1, 1);
            DateTime datTo = new DateTime(this._targetYear, 12, 31);

            DataTable dtSource = (DataTable)this.dgvList.DataSource;
            DataRow drSource;

            for (int i = 0; i < dtSource.Rows.Count; i++)
            {
                drSource = dtSource.Rows[i];

                // 日付と名称が両方空の場合はスルー対象
                if (ValChk.IsEmpty(drSource["日付"]) && ValChk.IsEmpty(drSource["祝日名称"]))
                {
                    continue;
                }

                // 日付が空の場合そこでエラー
                if (ValChk.IsEmpty(drSource["日付"]))
                {
                    this.dgvList.CurrentCell = this.dgvList[0, i];
                    this.dgvList.Rows[i].Selected = true;
                    this.dgvList.Focus();

                    Msg.Notice("日付が未入力です。");

                    return false;
                }

                // 日付が今年の日付でない場合そこでエラー
                if (Util.ToDate(drSource["日付"]).CompareTo(datFr) < 0
                    || Util.ToDate(drSource["日付"]).CompareTo(datTo) > 0)
                {
                    this.dgvList.CurrentCell = this.dgvList[0, i];
                    this.dgvList.Rows[i].Selected = true;
                    this.dgvList.Focus();

                    Msg.Notice("処理対象年の日付でありません。");

                    return false;
                }

                // 祝日名称が空の場合そこでエラー
                if (ValChk.IsEmpty(drSource["祝日名称"]))
                {
                    this.dgvList.CurrentCell = this.dgvList[1, i];
                    this.dgvList.Rows[i].Selected = true;
                    this.dgvList.Focus();

                    Msg.Notice("祝日名称が未入力です。");

                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 入力されたデータを登録する
        /// </summary>
        private void RegistData()
        {
            try
            {
                StringBuilder where = new StringBuilder();
                DbParamCollection dpc;

                this.Dba.BeginTransaction();

                DateTime datFr = new DateTime(this._targetYear, 1, 1);
                DateTime datTo = new DateTime(this._targetYear, 12, 31);

                // 処理対象の年のデータを全削除
                where.Append("DATE BETWEEN @DATE_FR AND @DATE_TO");
                dpc = new DbParamCollection();
                dpc.SetParam("@DATE_FR", SqlDbType.DateTime, datFr);
                dpc.SetParam("@DATE_TO", SqlDbType.DateTime, datTo);
                this.Dba.Delete("TB_CM_HOLIDAY", where.ToString(), dpc);

                DataTable dtSource = (DataTable)this.dgvList.DataSource;
                DataRow drSource;

                // 画面で入力された情報を登録
                for (int i = 0; i < dtSource.Rows.Count; i++)
                {
                    drSource = dtSource.Rows[i];

                    // 日付と名称が両方空の場合はスルー対象
                    if (ValChk.IsEmpty(drSource["日付"]) && ValChk.IsEmpty(drSource["祝日名称"]))
                    {
                        continue;
                    }

                    dpc = new DbParamCollection();
                    dpc.SetParam("@DATE", SqlDbType.DateTime, drSource["日付"]);
                    dpc.SetParam("@HOLIDAY_NM", SqlDbType.VarChar, 40, drSource["祝日名称"]);
                    dpc.SetParam("@HM_KUBUN", SqlDbType.Decimal, 1, (ValChk.IsEmpty(drSource["ﾊｯﾋﾟｰﾏﾝﾃﾞｰ"]) || !(bool)drSource["ﾊｯﾋﾟｰﾏﾝﾃﾞｰ"]) ? 0 : 1);
                    dpc.SetParam("@FURIKAE_KUBUN", SqlDbType.Decimal, 1, (ValChk.IsEmpty(drSource["振替休日"]) || !(bool)drSource["振替休日"]) ? 0 : 1);
                    dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
                    dpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
                    this.Dba.Insert("TB_CM_HOLIDAY", dpc);
                }

                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }
        }
        #endregion
    }
}
