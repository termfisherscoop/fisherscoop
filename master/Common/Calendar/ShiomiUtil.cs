﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.util;

namespace jp.co.fsi.com.calendar
{
    /// <summary>
    /// 潮見表の状態を保持するクラスです。
    /// </summary>
    public class ShiomiUtil
    {
        #region private変数
        /// <summary>
        /// DBアクセスオブジェクト
        /// </summary>
        private DbAccess _dba;
        #endregion

        #region 定数
        /// <summary>
        /// 潮位データのURL(共通部分)
        /// </summary>
        private const string DATA_URL = "http://www.data.jma.go.jp/gmd/kaiyou/data/db/tide/suisan/txt/";

        /// <summary>
        /// 潮位データをダウンロードする先のディレクトリ
        /// </summary>
        private const string DL_DIR = "tide";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ShiomiUtil(DbAccess dba)
        {
            // 画面クラスから受け取ったDBアクセスオブジェクトを保持
            this._dba = dba;

            if (!Directory.Exists(Path.Combine(Util.GetPath(), DL_DIR)))
            {
                // tideディレクトリが無ければまず作成する
                Directory.CreateDirectory(Path.Combine(Util.GetPath(), DL_DIR));
            }
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 潮見表の情報を取得します。
        /// </summary>
        /// <param name="loc">観測地点のコード</param>
        /// <param name="year">対象の年</param>
        /// <returns>1年分の潮見データ(今年の予測値＋過去3年分の実績)</returns>
        public DataTable GetShiomiData(string loc, int year)
        {
            // 返却用の変数
            DataTable dtResult;

            // 今年の年を保持
            int curYear = DateTime.Now.Year;

            // 取得範囲のfrom,toを保持
            DateTime datFr = new DateTime(year, 1, 1);
            DateTime datTo = new DateTime(year, 12, 31);

            // 引数の年の1月1日のレコードを取得
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@LOC", SqlDbType.VarChar, 2, loc);
            dpc.SetParam("@DATE", SqlDbType.DateTime, datFr);
            DataTable dtTmp = this._dba.GetDataTableByConditionWithParams(
                "UPDATE_DATE", "TB_CM_CHOI", "LOC = @LOC AND DATE = @DATE", dpc);

            if (dtTmp.Rows.Count > 0 && Util.ToDate(dtTmp.Rows[0]["UPDATE_DATE"]).Year == curYear)
            {
                // データがあり、かつ今年ダウンロードされていれば、
                // 該当年の全レコードを取得
                dpc = new DbParamCollection();
                dpc.SetParam("@LOC", SqlDbType.VarChar, 2, loc);
                dpc.SetParam("@DATE_FR", SqlDbType.DateTime, datFr);
                dpc.SetParam("@DATE_TO", SqlDbType.DateTime, datTo);
                dtResult = this._dba.GetDataTableByConditionWithParams(
                    "*", "TB_CM_CHOI", "LOC = @LOC AND DATE BETWEEN @DATE_FR AND @DATE_TO", dpc);
            }
            else
            {
                // 潮位データをtideフォルダにダウンロード
                WebClient wc = new WebClient();

                // 対象年のデータをダウンロード
                try
                {
                    wc.DownloadFile(DATA_URL + "/" + year.ToString() + "/" + loc + ".txt",
                        Path.Combine(DL_DIR, loc + "_" + year.ToString() + ".txt"));
                }
                catch (Exception)
                {
                    // 指定したURLにデータが存在しなければスルー
                }

                wc.Dispose();

                // 返却用のDataTableの定義を作成
                dtResult = CreateDataTableDef();

                // ダウンロードしたテキストファイルから情報をDataTableに詰め替える
                string path = string.Empty;
                StreamReader sr = null;
                path = Path.Combine(DL_DIR, loc + "_" + year.ToString() + ".txt");

                if (File.Exists(path))
                {
                    // ファイルを開いて1行ずつ読み出す
                    sr = new StreamReader(path);
                    while (sr.Peek() >= 0)
                    {
                        dtResult.Rows.Add(SetData(dtResult, sr.ReadLine()));
                    }

                    sr.Close();
                }

                if (sr != null) sr.Dispose();

                try
                {
                    // dtResultの情報をDBに更新
                    // トランザクション開始
                    this._dba.BeginTransaction();

                    // まずは引数の範囲のデータの全レコードを削除
                    dpc = new DbParamCollection();
                    dpc.SetParam("@LOC", SqlDbType.VarChar, 2, loc);
                    dpc.SetParam("@DATE_FR", SqlDbType.DateTime, datFr);
                    dpc.SetParam("@DATE_TO", SqlDbType.DateTime, datTo);

                    this._dba.Delete("TB_CM_CHOI", "LOC = @LOC AND DATE BETWEEN @DATE_FR AND @DATE_TO", dpc);

                    // 再度引数の範囲のデータをINSERT
                    DataRow dr;
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        dr = dtResult.Rows[i];

                        dpc = new DbParamCollection();
                        dpc.SetParam("@LOC", SqlDbType.VarChar, 2, dr["LOC"]);
                        dpc.SetParam("@DATE", SqlDbType.DateTime, dr["DATE"]);
                        dpc.SetParam("@CHOI_00H", SqlDbType.Decimal, 3, dr["CHOI_00H"]);
                        dpc.SetParam("@CHOI_01H", SqlDbType.Decimal, 3, dr["CHOI_01H"]);
                        dpc.SetParam("@CHOI_02H", SqlDbType.Decimal, 3, dr["CHOI_02H"]);
                        dpc.SetParam("@CHOI_03H", SqlDbType.Decimal, 3, dr["CHOI_03H"]);
                        dpc.SetParam("@CHOI_04H", SqlDbType.Decimal, 3, dr["CHOI_04H"]);
                        dpc.SetParam("@CHOI_05H", SqlDbType.Decimal, 3, dr["CHOI_05H"]);
                        dpc.SetParam("@CHOI_06H", SqlDbType.Decimal, 3, dr["CHOI_06H"]);
                        dpc.SetParam("@CHOI_07H", SqlDbType.Decimal, 3, dr["CHOI_07H"]);
                        dpc.SetParam("@CHOI_08H", SqlDbType.Decimal, 3, dr["CHOI_08H"]);
                        dpc.SetParam("@CHOI_09H", SqlDbType.Decimal, 3, dr["CHOI_09H"]);
                        dpc.SetParam("@CHOI_10H", SqlDbType.Decimal, 3, dr["CHOI_10H"]);
                        dpc.SetParam("@CHOI_11H", SqlDbType.Decimal, 3, dr["CHOI_11H"]);
                        dpc.SetParam("@CHOI_12H", SqlDbType.Decimal, 3, dr["CHOI_12H"]);
                        dpc.SetParam("@CHOI_13H", SqlDbType.Decimal, 3, dr["CHOI_13H"]);
                        dpc.SetParam("@CHOI_14H", SqlDbType.Decimal, 3, dr["CHOI_14H"]);
                        dpc.SetParam("@CHOI_15H", SqlDbType.Decimal, 3, dr["CHOI_15H"]);
                        dpc.SetParam("@CHOI_16H", SqlDbType.Decimal, 3, dr["CHOI_16H"]);
                        dpc.SetParam("@CHOI_17H", SqlDbType.Decimal, 3, dr["CHOI_17H"]);
                        dpc.SetParam("@CHOI_18H", SqlDbType.Decimal, 3, dr["CHOI_18H"]);
                        dpc.SetParam("@CHOI_19H", SqlDbType.Decimal, 3, dr["CHOI_19H"]);
                        dpc.SetParam("@CHOI_20H", SqlDbType.Decimal, 3, dr["CHOI_20H"]);
                        dpc.SetParam("@CHOI_21H", SqlDbType.Decimal, 3, dr["CHOI_21H"]);
                        dpc.SetParam("@CHOI_22H", SqlDbType.Decimal, 3, dr["CHOI_22H"]);
                        dpc.SetParam("@CHOI_23H", SqlDbType.Decimal, 3, dr["CHOI_23H"]);
                        dpc.SetParam("@MAN_TIME1", SqlDbType.VarChar, 5, dr["MAN_TIME1"]);
                        dpc.SetParam("@MAN_CHOI1", SqlDbType.Decimal, 3, dr["MAN_CHOI1"]);
                        dpc.SetParam("@MAN_TIME2", SqlDbType.VarChar, 5, dr["MAN_TIME2"]);
                        dpc.SetParam("@MAN_CHOI2", SqlDbType.Decimal, 3, dr["MAN_CHOI2"]);
                        dpc.SetParam("@MAN_TIME3", SqlDbType.VarChar, 5, dr["MAN_TIME3"]);
                        dpc.SetParam("@MAN_CHOI3", SqlDbType.Decimal, 3, dr["MAN_CHOI3"]);
                        dpc.SetParam("@MAN_TIME4", SqlDbType.VarChar, 5, dr["MAN_TIME4"]);
                        dpc.SetParam("@MAN_CHOI4", SqlDbType.Decimal, 3, dr["MAN_CHOI4"]);
                        dpc.SetParam("@KAN_TIME1", SqlDbType.VarChar, 5, dr["KAN_TIME1"]);
                        dpc.SetParam("@KAN_CHOI1", SqlDbType.Decimal, 3, dr["KAN_CHOI1"]);
                        dpc.SetParam("@KAN_TIME2", SqlDbType.VarChar, 5, dr["KAN_TIME2"]);
                        dpc.SetParam("@KAN_CHOI2", SqlDbType.Decimal, 3, dr["KAN_CHOI2"]);
                        dpc.SetParam("@KAN_TIME3", SqlDbType.VarChar, 5, dr["KAN_TIME3"]);
                        dpc.SetParam("@KAN_CHOI3", SqlDbType.Decimal, 3, dr["KAN_CHOI3"]);
                        dpc.SetParam("@KAN_TIME4", SqlDbType.VarChar, 5, dr["KAN_TIME4"]);
                        dpc.SetParam("@KAN_CHOI4", SqlDbType.Decimal, 3, dr["KAN_CHOI4"]);
                        dpc.SetParam("@DEL_FLG", SqlDbType.Decimal, 1, 0);
                        dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
                        dpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

                        // 登録
                        this._dba.Insert("TB_CM_CHOI", dpc);
                    }

                    // 更新をコミット
                    this._dba.Commit();
                }
                finally
                {
                    this._dba.Rollback();
                }
            }

            return dtResult;
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 返却用のDataTableの定義を作成する
        /// ※定義のみでデータは作成しない
        /// </summary>
        /// <returns></returns>
        private DataTable CreateDataTableDef()
        {
            DataTable dtRet = new DataTable();
            dtRet.Columns.Add("CHOI_00H", typeof(int));     // 毎時潮位(0時)
            dtRet.Columns.Add("CHOI_01H", typeof(int));     // 毎時潮位(1時)
            dtRet.Columns.Add("CHOI_02H", typeof(int));     // 毎時潮位(2時)
            dtRet.Columns.Add("CHOI_03H", typeof(int));     // 毎時潮位(3時)
            dtRet.Columns.Add("CHOI_04H", typeof(int));     // 毎時潮位(4時)
            dtRet.Columns.Add("CHOI_05H", typeof(int));     // 毎時潮位(5時)
            dtRet.Columns.Add("CHOI_06H", typeof(int));     // 毎時潮位(6時)
            dtRet.Columns.Add("CHOI_07H", typeof(int));     // 毎時潮位(7時)
            dtRet.Columns.Add("CHOI_08H", typeof(int));     // 毎時潮位(8時)
            dtRet.Columns.Add("CHOI_09H", typeof(int));     // 毎時潮位(9時)
            dtRet.Columns.Add("CHOI_10H", typeof(int));     // 毎時潮位(10時)
            dtRet.Columns.Add("CHOI_11H", typeof(int));     // 毎時潮位(11時)
            dtRet.Columns.Add("CHOI_12H", typeof(int));     // 毎時潮位(12時)
            dtRet.Columns.Add("CHOI_13H", typeof(int));     // 毎時潮位(13時)
            dtRet.Columns.Add("CHOI_14H", typeof(int));     // 毎時潮位(14時)
            dtRet.Columns.Add("CHOI_15H", typeof(int));     // 毎時潮位(15時)
            dtRet.Columns.Add("CHOI_16H", typeof(int));     // 毎時潮位(16時)
            dtRet.Columns.Add("CHOI_17H", typeof(int));     // 毎時潮位(17時)
            dtRet.Columns.Add("CHOI_18H", typeof(int));     // 毎時潮位(18時)
            dtRet.Columns.Add("CHOI_19H", typeof(int));     // 毎時潮位(19時)
            dtRet.Columns.Add("CHOI_20H", typeof(int));     // 毎時潮位(20時)
            dtRet.Columns.Add("CHOI_21H", typeof(int));     // 毎時潮位(21時)
            dtRet.Columns.Add("CHOI_22H", typeof(int));     // 毎時潮位(22時)
            dtRet.Columns.Add("CHOI_23H", typeof(int));     // 毎時潮位(23時)
            dtRet.Columns.Add("DATE", typeof(DateTime));    // 年月日
            dtRet.Columns.Add("LOC", typeof(string));       // 地点記号
            dtRet.Columns.Add("MAN_TIME1", typeof(string)); // 満潮時の時刻1
            dtRet.Columns.Add("MAN_CHOI1", typeof(int));    // 満潮時の潮位1
            dtRet.Columns.Add("MAN_TIME2", typeof(string)); // 満潮時の時刻2
            dtRet.Columns.Add("MAN_CHOI2", typeof(int));    // 満潮時の潮位2
            dtRet.Columns.Add("MAN_TIME3", typeof(string)); // 満潮時の時刻3
            dtRet.Columns.Add("MAN_CHOI3", typeof(int));    // 満潮時の潮位3
            dtRet.Columns.Add("MAN_TIME4", typeof(string)); // 満潮時の時刻4
            dtRet.Columns.Add("MAN_CHOI4", typeof(int));    // 満潮時の潮位4
            dtRet.Columns.Add("KAN_TIME1", typeof(string)); // 干潮時の時刻1
            dtRet.Columns.Add("KAN_CHOI1", typeof(int));    // 干潮時の潮位1
            dtRet.Columns.Add("KAN_TIME2", typeof(string)); // 干潮時の時刻2
            dtRet.Columns.Add("KAN_CHOI2", typeof(int));    // 干潮時の潮位2
            dtRet.Columns.Add("KAN_TIME3", typeof(string)); // 干潮時の時刻3
            dtRet.Columns.Add("KAN_CHOI3", typeof(int));    // 干潮時の潮位3
            dtRet.Columns.Add("KAN_TIME4", typeof(string)); // 干潮時の時刻4
            dtRet.Columns.Add("KAN_CHOI4", typeof(int));    // 干潮時の潮位4

            return dtRet;
        }

        /// <summary>
        /// テキストファイルの行データをDataRowに詰め替えます
        /// </summary>
        /// <param name="dtTarget">セットするDataTable</param>
        /// <param name="lineData">テキストファイルから取得した1行分のデータ(固定長)</param>
        /// <returns>潮見データ</returns>
        private DataRow SetData(DataTable dtTarget, string lineData)
        {
            // 切り出す際のオフセットと桁数
            int tmpOffset;
            int tmpLength;

            // 切り出したバッファ
            string tmpBuf;

            // 年月日用のバッファ
            string[] tmpYMD;

            // 格納するDataRowの作成
            DataRow dr = dtTarget.NewRow();

            // 毎時潮位データを3桁ずつ取得する
            for (int i = 0; i < 24; i++)
            {
                // それぞれの潮位を切り出す
                tmpOffset = 3 * i;
                tmpLength = 3;

                tmpBuf = lineData.Substring(tmpOffset, tmpLength);

                dr["CHOI_" + i.ToString("00") + "H"] = Util.ToInt(tmpBuf.Trim());
            }

            // 年月日を取得
            tmpYMD = new string[3];
            tmpYMD[0] = lineData.Substring(72, 2);
            tmpYMD[1] = lineData.Substring(74, 2);
            tmpYMD[2] = lineData.Substring(76, 2);
            tmpBuf = tmpYMD[0].Trim() + "/" + tmpYMD[1].Trim() + "/" + tmpYMD[2].Trim();
            dr["DATE"] = Util.ToDate(tmpBuf);

            // 地点記号を取得
            dr["LOC"] = lineData.Substring(78, 2);

            // 満潮時刻及び潮位を取得
            for (int i = 1; i <= 4; i++)
            {
                // 時刻を取得
                tmpOffset = 80 + (7 * (i - 1));
                tmpLength = 4;

                tmpBuf = lineData.Substring(tmpOffset, tmpLength);

                if (tmpBuf.Equals("9999"))
                {
                    dr["MAN_TIME" + i.ToString()] = DBNull.Value;
                }
                else
                {
                    dr["MAN_TIME" + i.ToString()] = (tmpBuf.Substring(0, 2) + ":" + tmpBuf.Substring(2, 2)).Trim().Replace(" ", "0");
                }

                // 潮位を取得
                tmpOffset = 84 + (7 * (i - 1));
                tmpLength = 3;

                tmpBuf = lineData.Substring(tmpOffset, tmpLength);

                if (tmpBuf.Equals("999"))
                {
                    dr["MAN_CHOI" + i.ToString()] = DBNull.Value;
                }
                else
                {
                    dr["MAN_CHOI" + i.ToString()] = Util.ToInt(tmpBuf.Trim());
                }
            }

            // 干潮時刻及び潮位を取得
            for (int i = 1; i <= 4; i++)
            {
                // 時刻を取得
                tmpOffset = 108 + (7 * (i - 1));
                tmpLength = 4;

                tmpBuf = lineData.Substring(tmpOffset, tmpLength);

                if (tmpBuf.Equals("9999"))
                {
                    dr["KAN_TIME" + i.ToString()] = DBNull.Value;
                }
                else
                {
                    dr["KAN_TIME" + i.ToString()] = (tmpBuf.Substring(0, 2) + ":" + tmpBuf.Substring(2, 2)).Trim().Replace(" ", "0");
                }

                // 潮位を取得
                tmpOffset = 112 + (7 * (i - 1));
                tmpLength = 3;

                tmpBuf = lineData.Substring(tmpOffset, tmpLength);

                if (tmpBuf.Equals("999"))
                {
                    dr["KAN_CHOI" + i.ToString()] = DBNull.Value;
                }
                else
                {
                    dr["KAN_CHOI" + i.ToString()] = Util.ToInt(tmpBuf.Trim());
                }
            }

            // 結果を返却
            return dr;
        }
        #endregion
    }
}
