﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.com.calendar
{
    /// <summary>
    /// カレンダー(Calendar)
    /// </summary>
    public partial class Calendar : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 画像が配置されているフォルダ
        /// </summary>
        private string IMG_DIR = "images\\moon";
        #endregion

        #region private変数
        /// <summary>
        /// 引数に指定された日付
        /// </summary>
        private DateTime _parDate;

        /// <summary>
        /// 表示対象の年月
        /// </summary>
        private DateTime _targetYm;

        /// <summary>
        /// 表示対象の年
        /// </summary>
        private int _targetYear;

        /// <summary>
        /// セルのインデックスと日付をマッピングするためのテーブル
        /// </summary>
        private DataTable _dtDateMapping;

        /// <summary>
        /// 潮見データ
        /// </summary>
        private DataTable _dtShiomi;

        /// <summary>
        /// 当月の平日の色
        /// </summary>
        private Color _wdColor = Color.FromArgb(0, 0, 0);

        /// <summary>
        /// 当月の土曜の色
        /// </summary>
        private Color _satColor = Color.FromArgb(0, 0, 255);

        /// <summary>
        /// 当月の日祝の色
        /// </summary>
        private Color _holColor = Color.FromArgb(255, 0, 0);

        /// <summary>
        /// 当月以外の平日の色
        /// </summary>
        private Color _notCurMonWdColor = Color.FromArgb(128, 128, 128);

        /// <summary>
        /// 当月以外の土曜の色
        /// </summary>
        private Color _notCurMonSatColor = Color.FromArgb(102, 204, 255);

        /// <summary>
        /// 当月以外の日祝の色
        /// </summary>
        private Color _notCurMonHolColor = Color.FromArgb(255, 153, 153);
        #endregion

        #region private変数(暫定版)
        private DataTable _mstHoliday;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Calendar()
        {
            InitializeComponent();
            BindGotFocusEvent();

            // マッピング用のテーブルの定義を設定
            this._dtDateMapping = new DataTable();
            this._dtDateMapping.Columns.Add("ROW_IDX", typeof(int));            // 行インデックス
            this._dtDateMapping.Columns.Add("COL_IDX", typeof(int));            // 列インデックス
            this._dtDateMapping.Columns.Add("DATE", typeof(DateTime));          // 日付
            this._dtDateMapping.Columns.Add("IS_IN_MONTH", typeof(int));        // 当月内フラグ
            this._dtDateMapping.Columns.Add("IS_HOLIDAY", typeof(int));         // 祝日フラグ
            this._dtDateMapping.Columns.Add("HOLIDAY_NM", typeof(string));      // 祝日名
            this._dtDateMapping.Columns.Add("ROKUYO", typeof(string));          // 六曜
            this._dtDateMapping.Columns.Add("ETO", typeof(string));             // 干支
            this._dtDateMapping.Columns.Add("KYUREKI", typeof(string));         // 旧暦
            this._dtDateMapping.Columns.Add("MAGE", typeof(decimal));           // 月齢
            this._dtDateMapping.Columns.Add("MANCHO_INFO", typeof(string));     // 満潮時刻の情報
            this._dtDateMapping.Columns.Add("KANCHO_INFO", typeof(string));     // 干潮時刻の情報

            this._targetYear = 0;
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 休日設定を取得
            this._mstHoliday = this.Dba.GetDataTableByCondition(
                "DATE, HOLIDAY_NM", "TB_CM_HOLIDAY", null, "DATE ASC");

            // ファンクションキーの表示位置を変更
            this.btnF1.Location = this.btnF3.Location;
            this.btnF10.Location = this.btnF7.Location;
            this.btnF12.Location = this.btnF8.Location;
            this.btnF7.Location = this.btnF4.Location;
            this.btnF8.Location = this.btnF5.Location;
            this.btnF9.Location = this.btnF6.Location;

            // InDataの日付(ルール：DateTimeオブジェクトを引き渡す)を元に
            // 対象の月を初期設定する
            this._parDate = Util.ToDate(this.InData);
            this._targetYm = new DateTime(this._parDate.Year, this._parDate.Month, 1);

            // カレンダーの表示
            DispCalendar();

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列の幅設定
            for (int i = 0; i < 7; i++)
            {
                this.dgvList.Columns[i].Width = 78;
                this.dgvList.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter;
                this.dgvList.Columns[i].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            }

            // 日曜の見出しを赤、土曜の見出しを青
            this.dgvList.Columns[0].HeaderCell.Style.ForeColor = this._holColor;
            this.dgvList.Columns[6].HeaderCell.Style.ForeColor = this._satColor;
            this.dgvList.Columns[0].DefaultCellStyle.ForeColor = this._holColor;
            this.dgvList.Columns[6].DefaultCellStyle.ForeColor = this._satColor;
            
            // InDataの日付を初期選択
            DataRow[] aryMapping = this._dtDateMapping.Select("DATE = '" + this._parDate.ToString("yyyy/MM/dd") + "'");
            if (aryMapping.Length > 0)
            {
                this.dgvList.CurrentCell = this.dgvList[Util.ToInt(aryMapping[0]["COL_IDX"]), Util.ToInt(aryMapping[0]["ROW_IDX"])];
            }

            // Gridに初期フォーカス
            this.dgvList.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveControl.Name)
            {
                case "txtYear":
                case "txtMonth":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1ボタンクリック時処理
        /// </summary>
        public override void PressF1()
        {
            System.Reflection.Assembly asm = null;
            Type t = null;

            switch (this.ActiveControl.Name)
            {
                case "txtYear":
                case "txtMonth":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.txtGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.txtGengo.Text,
                                        this.txtYear.Text,
                                        this.txtMonth.Text,
                                        "1",
                                        this.Dba);
                                this.txtGengo.Text = arrJpDate[0];
                                this.txtYear.Text = arrJpDate[2];
                                this.txtMonth.Text = arrJpDate[3];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F7ボタンクリック時処理
        /// </summary>
        public override void PressF7()
        {
            // 対象年月を前月に移動
            this._targetYm = this._targetYm.AddMonths(-1);

            // カレンダーの表示
            DispCalendar();
        }

        /// <summary>
        /// F8ボタンクリック時処理
        /// </summary>
        public override void PressF8()
        {
            // 対象年月を翌月に移動
            this._targetYm = this._targetYm.AddMonths(1);

            // カレンダーの表示
            DispCalendar();
        }

        /// <summary>
        /// F9ボタンクリック時処理
        /// </summary>
        public override void PressF9()
        {
            // 対象年月を翌月に移動
            this._targetYm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            // カレンダーの表示
            DispCalendar();
        }

        /// <summary>
        /// F10ボタンクリック時処理
        /// </summary>
        public override void PressF10()
        {
            // 現在のセルの日付を取得
            DataRow[] aryMapping = this._dtDateMapping.Select(
                "COL_IDX = " + this.dgvList.CurrentCell.ColumnIndex.ToString()
                + " AND ROW_IDX = " + this.dgvList.CurrentCell.RowIndex.ToString());
            if (aryMapping.Length > 0)
            {
                DateTime targetDate = Util.ToDate(aryMapping[0]["DATE"]);

                // メモ画面を起動
                ScheduleMemo frmSM = new ScheduleMemo();
                frmSM.TargetDate = targetDate;
                frmSM.ShowDialog(this);

                // 閉じるときはオブジェクトを破棄
                frmSM.Dispose();

                // メモを表示
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@DATE", SqlDbType.DateTime, targetDate);
                dpc.SetParam("@TARGET_USER", SqlDbType.VarChar, 30, this.UInfo.UserId);

                DataTable dtMemo = this.Dba.GetDataTableByConditionWithParams(
                    "MEMO_CONTENT", "TB_CM_MEMO",
                    "[DATE] = @DATE AND (OPEN_KUBUN = 1 OR (OPEN_KUBUN = 9 AND TARGET_USER = @TARGET_USER)) AND DEL_FLG = 0",
                    "OPEN_KUBUN, SORT", dpc);

                this.txtMemo.Text = string.Empty;
                for (int i = 0; i < dtMemo.Rows.Count; i++)
                {
                    if (!ValChk.IsEmpty(this.txtMemo.Text)) this.txtMemo.Text += Environment.NewLine;
                    this.txtMemo.Text += Util.ToString(dtMemo.Rows[i]["MEMO_CONTENT"]);
                }
            }
        }

        /// <summary>
        /// F12ボタンクリック時処理
        /// </summary>
        public override void PressF12()
        {
            // 休日設定を起動
            HolidayMnt frmHM = new HolidayMnt();
            frmHM.TargetYear = this._targetYm.Year;
            frmHM.ShowDialog(this);

            // 閉じるときはオブジェクトを破棄
            frmHM.Dispose();

            // 休日設定を取得し直す
            this._mstHoliday = this.Dba.GetDataTableByCondition(
                "DATE, HOLIDAY_NM", "TB_CM_HOLIDAY", null, "DATE ASC");
        }
        #endregion

        #region イベント
        /// <summary>
        /// 前月ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrevMonth_Click(object sender, EventArgs e)
        {
            // 対象年月を前月に移動
            this._targetYm = this._targetYm.AddMonths(-1);

            // カレンダーの表示
            DispCalendar();
        }

        /// <summary>
        /// 翌月ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNextMonth_Click(object sender, EventArgs e)
        {
            // 対象年月を翌月に移動
            this._targetYm = this._targetYm.AddMonths(1);

            // カレンダーの表示
            DispCalendar();
        }

        /// <summary>
        /// 移動ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMoveMonth_Click(object sender, EventArgs e)
        {
            // 入力された年月から対象年月を設定
            this._targetYm = Util.ConvAdDate(this.txtGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, "1", this.Dba);

            // カレンダーの表示
            DispCalendar();
        }

        /// <summary>
        /// CellFormattingイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            // 当月以外は薄く表示する
            DataRow[] aryMapping = this._dtDateMapping.Select("ROW_IDX = " + Util.ToString(e.RowIndex) + " AND COL_IDX = " + Util.ToString(e.ColumnIndex));
            if (aryMapping.Length > 0)
            {
                if (Util.ToInt(aryMapping[0]["IS_IN_MONTH"]) == 0)
                {
                    if (Util.ToInt(aryMapping[0]["IS_HOLIDAY"]) == 1)
                    {
                        // 休日の色を設定
                        e.CellStyle.ForeColor = this._notCurMonHolColor;
                    }
                    else
                    {
                        switch (Util.ToDate(aryMapping[0]["DATE"]).DayOfWeek)
                        {
                            case DayOfWeek.Saturday:
                                // 土曜の色を設定
                                e.CellStyle.ForeColor = this._notCurMonSatColor;
                                break;

                            case DayOfWeek.Sunday:
                                // 日曜の色を設定
                                e.CellStyle.ForeColor = this._notCurMonHolColor;
                                break;

                            default:
                                // 平日の色を設定
                                e.CellStyle.ForeColor = this._notCurMonWdColor;
                                break;
                        }
                    }
                }
                else
                {
                    if (Util.ToInt(aryMapping[0]["IS_HOLIDAY"]) == 1)
                    {
                        // 休日の色を設定
                        e.CellStyle.ForeColor = this._holColor;
                    }
                }
            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            ReturnVal();
        }

        /// <summary>
        /// CellToolTipTextNeededイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            //e.ToolTipText = e.ColumnIndex.ToString() + ", " + e.RowIndex.ToString();
            // とりあえず休日マスタの情報を表示
            // セルの日付を取得
            DataRow[] aryMapping = this._dtDateMapping.Select(
                "COL_IDX = " + e.ColumnIndex.ToString() + " AND ROW_IDX = " + e.RowIndex.ToString());
            if (aryMapping.Length > 0)
            {
                e.ToolTipText = "";
                if (!ValChk.IsEmpty(aryMapping[0]["HOLIDAY_NM"])) e.ToolTipText += Util.ToString(aryMapping[0]["HOLIDAY_NM"]) + Environment.NewLine;
                e.ToolTipText += Util.ToString(aryMapping[0]["ROKUYO"]) + " " + Util.ToString(aryMapping[0]["ETO"]) + Environment.NewLine;
                e.ToolTipText += Util.ToString(aryMapping[0]["KYUREKI"]);
            }
        }

        /// <summary>
        /// CurrentCellChangedイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                DataRow[] aryMapping = this._dtDateMapping.Select(
                    "COL_IDX = " + this.dgvList.CurrentCell.ColumnIndex.ToString()
                    + " AND ROW_IDX = " + this.dgvList.CurrentCell.RowIndex.ToString());
                if (aryMapping.Length > 0)
                {
                    // 日付の表示(祝日の場合はその内容も)
                    DateTime targetDate = Util.ToDate(aryMapping[0]["DATE"]);
                    string[] aryJpDate = Util.ConvJpDate(targetDate, this.Dba);
                    this.lblDate.Text = aryJpDate[0] + aryJpDate[2] + "年" + aryJpDate[3] + "月" + aryJpDate[4] + "日";
                    if (Util.ToInt(aryMapping[0]["IS_HOLIDAY"]) == 1)
                    {
                        this.lblDate.Text += "(祝・" + Util.ToString(aryMapping[0]["HOLIDAY_NM"]) + ")";
                        this.lblDate.ForeColor = this._holColor;
                    }
                    else
                    {
                        this.lblDate.ForeColor = this._wdColor;
                        this.lblDate.Text += "(";
                        switch (targetDate.DayOfWeek)
                        {
                            case DayOfWeek.Sunday:
                                this.lblDate.Text += "日";
                                this.lblDate.ForeColor = this._holColor;
                                break;

                            case DayOfWeek.Monday:
                                this.lblDate.Text += "月";
                                break;

                            case DayOfWeek.Tuesday:
                                this.lblDate.Text += "火";
                                break;

                            case DayOfWeek.Wednesday:
                                this.lblDate.Text += "水";
                                break;

                            case DayOfWeek.Thursday:
                                this.lblDate.Text += "木";
                                break;

                            case DayOfWeek.Friday:
                                this.lblDate.Text += "金";
                                break;

                            case DayOfWeek.Saturday:
                                this.lblDate.Text += "土";
                                this.lblDate.ForeColor = this._satColor;
                                break;

                            default:
                                break;
                        }
                        this.lblDate.Text += ")";
                    }

                    // 旧暦等の情報を表示
                    this.lblInfo.Text = Util.ToString(aryMapping[0]["ROKUYO"]) + " " + Util.ToString(aryMapping[0]["ETO"]) + Environment.NewLine
                        + Util.ToString(aryMapping[0]["KYUREKI"]);

                    // 潮の干満の情報を表示
                    if (!ValChk.IsEmpty(aryMapping[0]["MANCHO_INFO"])) this.lblInfo.Text += Environment.NewLine + Util.ToString(aryMapping[0]["MANCHO_INFO"]);
                    if (!ValChk.IsEmpty(aryMapping[0]["KANCHO_INFO"])) this.lblInfo.Text += Environment.NewLine + Util.ToString(aryMapping[0]["KANCHO_INFO"]);

                    // 月画像を表示
                    if (Math.Floor(Util.ToDecimal(aryMapping[0]["MAGE"])) == 0)
                    {
                        this.pbxMoon.Image = Image.FromFile(Path.Combine(IMG_DIR, "31.gif"));
                    }
                    else
                    {
                        this.pbxMoon.Image = Image.FromFile(Path.Combine(IMG_DIR, Util.ToString(Math.Floor(Util.ToDecimal(aryMapping[0]["MAGE"]))) + ".gif"));
                    }

                    // メモを表示
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@DATE", SqlDbType.DateTime, targetDate);
                    dpc.SetParam("@TARGET_USER", SqlDbType.VarChar, 30, this.UInfo.UserId);

                    DataTable dtMemo = this.Dba.GetDataTableByConditionWithParams(
                        "MEMO_CONTENT", "TB_CM_MEMO",
                        "[DATE] = @DATE AND (OPEN_KUBUN = 1 OR (OPEN_KUBUN = 9 AND TARGET_USER = @TARGET_USER)) AND DEL_FLG = 0",
                        "OPEN_KUBUN, SORT", dpc);

                    this.txtMemo.Text = string.Empty;
                    for (int i = 0; i < dtMemo.Rows.Count; i++)
                    {
                        if (!ValChk.IsEmpty(this.txtMemo.Text)) this.txtMemo.Text += Environment.NewLine;
                        this.txtMemo.Text += Util.ToString(dtMemo.Rows[i]["MEMO_CONTENT"]);
                    }
                }
                else
                {
                    // 通常ありえない
                    this.lblDate.Text = string.Empty;
                    this.lblInfo.Text = string.Empty;
                    this.pbxMoon.Image = null;
                    this.txtMemo.Text = string.Empty;
                }
            }
            catch (Exception)
            {
                // 例外発生時はスルー
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// カレンダーを表示する
        /// </summary>
        private void DispCalendar()
        {
            // 画面の年月を表示
            string[] aryJpDate = Util.ConvJpDate(this._targetYm, this.Dba);
            this.txtGengo.Text = aryJpDate[0];
            this.txtYear.Text = aryJpDate[2];
            this.txtMonth.Text = aryJpDate[3];

            // 対象年の潮見データを取得(但し、直前に表示していた年月と違う年を表示する場合のみ)
            if (this._targetYm.Year != this._targetYear)
            {
                this._targetYear = this._targetYm.Year;

                ShiomiUtil su = new ShiomiUtil(this.Dba);
                this._dtShiomi = su.GetShiomiData(this.Config.LoadCommonConfig("Calendar", "Location"), this._targetYear);
            }

            // 西暦及び年の干支を表示
            string[] eto = new string[12] { "申", "酉", "戌", "亥", "子", "丑",
                "寅", "卯", "辰", "巳", "午", "未" };
            this.lblYearAddInfo.Text = "(" + this._targetYm.Year.ToString() + "年・"
                + eto[this._targetYm.Year % 12] + "年)";

            // データを取得して表示用に加工
            DataTable dtDispData = GetDispList();

            this.dgvList.DataSource = dtDispData;
        }

        /// <summary>
        /// 表示するデータのリストを取得する
        /// </summary>
        /// <returns>表示するデータ</returns>
        private DataTable GetDispList()
        {
            // 対象日付の開始および終了を保持
            DateTime datStart;
            DateTime datEnd;

            DateTime datEndOfMonth; // 月の最終日(テンポラリ変数)
            DateTime datTmpDate;    // それぞれのセルの日付(テンポラリ変数)
            int rowIdx = 0;         // 表示するGridの行インデックス
            int colIdx = 0;         // 表示するGridの列インデックス

            DataRow drDateMapping;  // マッピング用のテーブルの新規行

            DataRow[] aryHoliday;

            KyurekiCalc clsKrk = new KyurekiCalc(); // 旧暦算出クラス
            KyurekiBean objKrk;

            DbParamCollection dpc = new DbParamCollection();
            string where = "DATE = @DATE AND (OPEN_KUBUN = 1 OR (OPEN_KUBUN = 9 AND TARGET_USER = @TARGET_USER)) AND DEL_FLG = 0";
            bool memoExists;

            // 対象年月をもとに表示用のデータを作成する
            // 同時に、セルのインデックスと日付の紐づけの情報を内部に保持する
            DataTable dtResult = new DataTable();

            // 返却用のDataTableの定義を作成する
            dtResult.Columns.Add("SUN", typeof(string));
            dtResult.Columns.Add("MON", typeof(string));
            dtResult.Columns.Add("TUE", typeof(string));
            dtResult.Columns.Add("WED", typeof(string));
            dtResult.Columns.Add("THU", typeof(string));
            dtResult.Columns.Add("FRI", typeof(string));
            dtResult.Columns.Add("SAT", typeof(string));

            // 返却用のDataTableの行
            DataRow drResult = null;

            // 対象月の1日の曜日を求める
            if (this._targetYm.DayOfWeek == DayOfWeek.Sunday)
            {
                // 日曜であれば1日がスタート
                datStart = this._targetYm;
            }
            else
            {
                // 日曜でなければ遡って日曜までの前月内の日付を求めて表示対象にする
                datStart = this._targetYm.AddDays(DayOfWeek.Sunday - this._targetYm.DayOfWeek);
            }

            // 対象月の末日の曜日を求める
            datEndOfMonth = new DateTime(this._targetYm.Year, this._targetYm.Month, DateTime.DaysInMonth(this._targetYm.Year, this._targetYm.Month));
            if (datEndOfMonth.DayOfWeek == DayOfWeek.Saturday)
            {
                // 土曜であればその日付が表示対象の最終日付
                datEnd = datEndOfMonth;
            }
            else
            {
                // 土曜でなければ土曜までの翌月内の日付を求めて表示対象にする
                datEnd = datEndOfMonth.AddDays(DayOfWeek.Saturday - datEndOfMonth.DayOfWeek);
            }

            // 各種DataTableに日付情報を格納
            this._dtDateMapping.Clear();
            datTmpDate = datStart;
            while (datTmpDate.CompareTo(datEnd) <= 0)
            {
                // セルと日付をマッピングするDataTableに行を格納
                drDateMapping = this._dtDateMapping.NewRow();
                drDateMapping["ROW_IDX"] = rowIdx;
                drDateMapping["COL_IDX"] = colIdx;
                drDateMapping["DATE"] = datTmpDate;
                if (datTmpDate.CompareTo(this._targetYm) < 0
                    || datTmpDate.CompareTo(datEndOfMonth) > 0)
                {
                    // 前月または翌月
                    drDateMapping["IS_IN_MONTH"] = 0;
                }
                else
                {
                    // 当月
                    drDateMapping["IS_IN_MONTH"] = 1;
                }
                // TODO:休日判断 ※要エンティティ設計
                aryHoliday = this._mstHoliday.Select("DATE = '" + datTmpDate.ToString("yyyy/MM/dd") + "'");
                if (aryHoliday.Length > 0)
                {
                    drDateMapping["IS_HOLIDAY"] = 1;
                    drDateMapping["HOLIDAY_NM"] = aryHoliday[0]["HOLIDAY_NM"];
                }
                else
                {
                    drDateMapping["IS_HOLIDAY"] = 0;
                }

                // 六曜、干支、旧暦を求める
                objKrk = clsKrk.GetKyureki(datTmpDate);
                drDateMapping["ROKUYO"] = objKrk.Rokuyo;
                drDateMapping["ETO"] = objKrk.Eto;
                drDateMapping["KYUREKI"] = "旧 " + Util.ToString(objKrk.Month) + "." + Util.ToString(objKrk.Day);
                drDateMapping["MAGE"] = objKrk.MageNoon;

                // 潮の干満の時刻を設定
                string tmpBuf = string.Empty;
                DataRow[] arrShiomi;
                arrShiomi = this._dtShiomi.Select("DATE = '" + datTmpDate.ToString("yyyy/MM/dd") + "'");
                if (arrShiomi.Length > 0)
                {
                    tmpBuf = "満潮：";
                    for (int i = 1; i <= 4; i++)
                    {
                        if (!ValChk.IsEmpty(arrShiomi[0]["MAN_TIME" + i.ToString()]))
                        {
                            if (i > 1) tmpBuf += " ";
                            tmpBuf += Util.ToString(arrShiomi[0]["MAN_TIME" + i.ToString()]);
                            tmpBuf += "(";
                            tmpBuf += Util.ToString(arrShiomi[0]["MAN_CHOI" + i.ToString()]);
                            tmpBuf += "cm)";
                        }
                    }

                    drDateMapping["MANCHO_INFO"] = tmpBuf;

                    tmpBuf = "干潮：";
                    for (int i = 1; i <= 4; i++)
                    {
                        if (!ValChk.IsEmpty(arrShiomi[0]["KAN_TIME" + i.ToString()]))
                        {
                            if (i > 1) tmpBuf += " ";
                            tmpBuf += Util.ToString(arrShiomi[0]["KAN_TIME" + i.ToString()]);
                            tmpBuf += "(";
                            tmpBuf += Util.ToString(arrShiomi[0]["KAN_CHOI" + i.ToString()]);
                            tmpBuf += "cm)";
                        }
                    }

                    drDateMapping["KANCHO_INFO"] = tmpBuf;
                }

                this._dtDateMapping.Rows.Add(drDateMapping);

                // 日曜の日付を処理する場合、結果用のテーブルの新規行オブジェクトを作成
                if (colIdx == 0)
                {
                    drResult = dtResult.NewRow();
                }

                // メモが登録されているかどうかをチェックする
                dpc = new DbParamCollection();
                dpc.SetParam("@DATE", SqlDbType.DateTime, datTmpDate);
                dpc.SetParam("@TARGET_USER", SqlDbType.VarChar, 30, this.UInfo.UserId);
                if (this.Dba.GetDataTableByConditionWithParams("SEQ", "TB_CM_MEMO", where, dpc).Rows.Count > 0)
                {
                    memoExists = true;
                }
                else
                {
                    memoExists = false;
                }

                // 曜日によってdrResultに設定する日付をかえる
                switch (datTmpDate.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        drResult["SUN"] = datTmpDate.Day;
                        if (memoExists) drResult["SUN"] = Util.ToString(drResult["SUN"]) + Environment.NewLine +  "●";
                        break;

                    case DayOfWeek.Monday:
                        drResult["MON"] = datTmpDate.Day;
                        if (memoExists) drResult["MON"] = Util.ToString(drResult["MON"]) + Environment.NewLine + "●";
                        break;

                    case DayOfWeek.Tuesday:
                        drResult["TUE"] = datTmpDate.Day;
                        if (memoExists) drResult["TUE"] = Util.ToString(drResult["TUE"]) + Environment.NewLine + "●";
                        break;

                    case DayOfWeek.Wednesday:
                        drResult["WED"] = datTmpDate.Day;
                        if (memoExists) drResult["WED"] = Util.ToString(drResult["WED"]) + Environment.NewLine + "●";
                        break;

                    case DayOfWeek.Thursday:
                        drResult["THU"] = datTmpDate.Day;
                        if (memoExists) drResult["THU"] = Util.ToString(drResult["THU"]) + Environment.NewLine + "●";
                        break;

                    case DayOfWeek.Friday:
                        drResult["FRI"] = datTmpDate.Day;
                        if (memoExists) drResult["FRI"] = Util.ToString(drResult["FRI"]) + Environment.NewLine + "●";
                        break;

                    case DayOfWeek.Saturday:
                        drResult["SAT"] = datTmpDate.Day;
                        if (memoExists) drResult["SAT"] = Util.ToString(drResult["SAT"]) + Environment.NewLine + "●";
                        break;
                }

                // セル及び日付をひとつずらす
                colIdx++;
                if (colIdx > 6)
                {
                    // 翌週のセルに移動
                    rowIdx++;
                    colIdx = 0;

                    dtResult.Rows.Add(drResult);
                }
                datTmpDate = datTmpDate.AddDays(1);
            }

            return dtResult;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            // 西暦日付と和暦日付をセットで返す
            string strAdDate = string.Empty;
            string strJpDate = string.Empty;
            string gengo = string.Empty;
            string iGengo = string.Empty;
            string jpNen = string.Empty;
            string month = string.Empty;
            string day = string.Empty;

            // 選択中のセルの位置を元に日付を取得
            DataRow[] aryMapping = this._dtDateMapping.Select(
                "COL_IDX = " + this.dgvList.CurrentCell.ColumnIndex.ToString()
                + " AND ROW_IDX = " + this.dgvList.CurrentCell.RowIndex.ToString());

            // 取得した情報から戻り値の書式をフォーマット
            if (aryMapping.Length > 0)
            {
                // 日付の表示(祝日の場合はその内容も)
                DateTime targetDate = Util.ToDate(aryMapping[0]["DATE"]);
                strAdDate = Util.ToDateStr(aryMapping[0]["DATE"]);
                string[] aryJpDate = Util.ConvJpDate(targetDate, this.Dba);
                strJpDate = aryJpDate[0] + aryJpDate[2] + "年" + aryJpDate[3] + "月" + aryJpDate[4] + "日";
                gengo = aryJpDate[0];
                iGengo = aryJpDate[1];
                jpNen = aryJpDate[2];
            }

            // 戻り値をセットして画面を閉じる
            // 西暦日付全体(0)、和暦日付全体(1)、元号正式名(2)、元号略(3)、和暦年(4)、月(5)、日(6)
            this.OutData = new string[7] { 
                strAdDate,
                strJpDate,
                gengo,
                iGengo,
                jpNen,
                month,
                day
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
