﻿namespace jp.co.fsi.cm.cmcm1041
{
    partial class CMCM1042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEnter = new System.Windows.Forms.Button();
            this.txtTitle01 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTitle01 = new System.Windows.Forms.Label();
            this.txtTitle02 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTitle02 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(7, 4);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(7, 4);
            this.btnF1.Margin = new System.Windows.Forms.Padding(5);
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Location = new System.Drawing.Point(92, 4);
            this.btnF2.Margin = new System.Windows.Forms.Padding(5);
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Location = new System.Drawing.Point(177, 4);
            this.btnF3.Margin = new System.Windows.Forms.Padding(5);
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Location = new System.Drawing.Point(263, 4);
            this.btnF4.Margin = new System.Windows.Forms.Padding(5);
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Location = new System.Drawing.Point(348, 4);
            this.btnF5.Margin = new System.Windows.Forms.Padding(5);
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(519, 4);
            this.btnF7.Margin = new System.Windows.Forms.Padding(5);
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(433, 4);
            this.btnF6.Margin = new System.Windows.Forms.Padding(5);
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(604, 4);
            this.btnF8.Margin = new System.Windows.Forms.Padding(5);
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Location = new System.Drawing.Point(689, 4);
            this.btnF9.Margin = new System.Windows.Forms.Padding(5);
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Location = new System.Drawing.Point(945, 4);
            this.btnF12.Margin = new System.Windows.Forms.Padding(5);
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Location = new System.Drawing.Point(860, 4);
            this.btnF11.Margin = new System.Windows.Forms.Padding(5);
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Location = new System.Drawing.Point(775, 4);
            this.btnF10.Margin = new System.Windows.Forms.Padding(5);
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.btnEnter);
            this.pnlDebug.Location = new System.Drawing.Point(16, 120);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(485, 73);
            this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(517, 31);
            this.lblTitle.TabIndex = 999;
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnter.Location = new System.Drawing.Point(92, 4);
            this.btnEnter.Margin = new System.Windows.Forms.Padding(4);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(87, 60);
            this.btnEnter.TabIndex = 903;
            this.btnEnter.TabStop = false;
            this.btnEnter.Text = "Enter\r\n\r\n決定";
            this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEnter.UseVisualStyleBackColor = true;
            // 
            // txtTitle01
            // 
            this.txtTitle01.AllowDrop = true;
            this.txtTitle01.AutoSizeFromLength = false;
            this.txtTitle01.DisplayLength = null;
            this.txtTitle01.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTitle01.Location = new System.Drawing.Point(135, 4);
            this.txtTitle01.Margin = new System.Windows.Forms.Padding(4);
            this.txtTitle01.MaxLength = 4;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Size = new System.Drawing.Size(51, 23);
            this.txtTitle01.TabIndex = 1;
            this.txtTitle01.Text = "0";
            this.txtTitle01.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTitle01.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle01_Validating);
            // 
            // lblTitle01
            // 
            this.lblTitle01.BackColor = System.Drawing.Color.Silver;
            this.lblTitle01.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTitle01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle01.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle01.Location = new System.Drawing.Point(0, 0);
            this.lblTitle01.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.lblTitle01.Size = new System.Drawing.Size(447, 32);
            this.lblTitle01.TabIndex = 0;
            this.lblTitle01.Tag = "CHANGE";
            this.lblTitle01.Text = "商品区分";
            this.lblTitle01.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTitle02
            // 
            this.txtTitle02.AllowDrop = true;
            this.txtTitle02.AutoSizeFromLength = false;
            this.txtTitle02.DisplayLength = null;
            this.txtTitle02.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTitle02.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTitle02.Location = new System.Drawing.Point(135, 4);
            this.txtTitle02.Margin = new System.Windows.Forms.Padding(4);
            this.txtTitle02.MaxLength = 15;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Size = new System.Drawing.Size(308, 23);
            this.txtTitle02.TabIndex = 3;
            // 
            // lblTitle02
            // 
            this.lblTitle02.BackColor = System.Drawing.Color.Silver;
            this.lblTitle02.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTitle02.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle02.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle02.Location = new System.Drawing.Point(0, 0);
            this.lblTitle02.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle02.Name = "lblTitle02";
            this.lblTitle02.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.lblTitle02.Size = new System.Drawing.Size(447, 33);
            this.lblTitle02.TabIndex = 2;
            this.lblTitle02.Tag = "CHANGE";
            this.lblTitle02.Text = "商品区分4名称";
            this.lblTitle02.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(16, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(455, 80);
            this.fsiTableLayoutPanel1.TabIndex = 1000;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtTitle02);
            this.fsiPanel2.Controls.Add(this.lblTitle02);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 43);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(447, 33);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtTitle01);
            this.fsiPanel1.Controls.Add(this.lblTitle01);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(447, 32);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // CMCM1042
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 210);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.MinimizeBox = false;
            this.Name = "CMCM1042";
            this.ShowFButton = true;
            this.ShowTitle = false;
            this.Text = "商品区分";
            this.Load += new System.EventHandler(this.CMCM1042_Load);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        protected System.Windows.Forms.Button btnEnter;
        private common.controls.FsiTextBox txtTitle01;
        private System.Windows.Forms.Label lblTitle01;
        private common.controls.FsiTextBox txtTitle02;
        private System.Windows.Forms.Label lblTitle02;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}
