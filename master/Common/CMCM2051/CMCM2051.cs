﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.cm.cmcm2051
{
    /// <summary>
    /// 自動仕訳の設定(CMCM2051)
    /// </summary>
    public partial class CMCM2051 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region 変数
        private int SHIHARAI_CD_MAX = 4;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2051()
        {
            InitializeComponent();

            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            string shishoCd = "1"; //UInfo.ShishoCd;
#else
            string shishoCd = UInfo.ShishoCd;
#endif
            // 支払
            int SHIHARAI = 0;
            try
            {
                SHIHARAI = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Han, "CMCM2051", "Setting", "SHIHARAI")));
            }
            catch (Exception)
            {
                SHIHARAI = 0;
            }
            if (SHIHARAI == 1)
            {
                this.SHIHARAI_CD_MAX = 5;
                this.lblTorihikiKbnResults.Text = "1:売上　2:仕入　3:セリ　4:控除　5:支払";
            }
            else
            {
                this.SHIHARAI_CD_MAX = 4;
                this.lblTorihikiKbnResults.Text = "1:売 上　2:仕 入　3:セ リ　4:控 除";
            }

            this.txtShishoCd.Text = shishoCd;
            this.txtShishoCd.Enabled = (this.txtShishoCd.Text == "1") ? true : false;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, this.txtShishoCd.Text);

            this.txtTorihikiKbn.Text = "1"; // 取引区分
            this.txtSetteiKbn.Text = "1"; //　設定区分

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // 取引区分にフォーカス
            this.txtTorihikiKbn.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShishoCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // ダイアログとしての処理結果を返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtShishoCd": // 水揚支所
                    String[] result = this.openSearchWindow("CMCM2031", "TB_CM_SHISHO", this.txtShishoCd.Text);
                    if (!ValChk.IsEmpty(result[0]))
                    {
                        this.txtShishoCd.Text = result[0];
                        this.lblShishoNm.Text = result[1];
                    }
                    break;
                default:
                    // 取引区分にフォーカスを戻す
                    this.txtTorihikiKbn.Focus();
                    this.txtTorihikiKbn.SelectAll();
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 仕訳コード登録画面を立ち上げる
            if (!IsValidTorihikiKbn())
            {
                return;
            }
            if (!IsValidSetteiKbn())
            {
                return;
            }

            // 商品登録画面の起動
            EditShiwake(string.Empty);
        }
#endregion

#region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtShishoCd.Text, this.lblShishoNm.Text, this.txtShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtShishoCd.SelectAll();
                this.txtShishoCd.Focus();
            }
        }

        /// <summary>
        /// 取引区分検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTorihikiKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTorihikiKbn())
            {
                e.Cancel = true;
                this.txtTorihikiKbn.SelectAll();
            }
            else
            {
                //// 入力された情報を元に検索する
                //SearchData(false);
            }
        }

        /// <summary>
        /// 設定区分検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSetteiKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSetteiKbn())
            {
                e.Cancel = true;
                this.txtSetteiKbn.SelectAll();
            }
            else
            {
                // 入力された情報を元に検索する
                SearchData(false);
            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    EditShiwake(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
                    e.Handled = true;
                }
                catch (Exception) { }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditShiwake(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
        }
#endregion

#region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtShishoCd.Text) || Equals(this.txtShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            string shishoCd = this.txtShishoCd.Text;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);

            if (ValChk.IsEmpty(this.lblShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 取引区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTorihikiKbn()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtTorihikiKbn.Text))
            {
                this.txtTorihikiKbn.Text = "1";
            }

            // 1~5のみ入力を許可
            if (!ValChk.IsNumber(this.txtTorihikiKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtTorihikiKbn.Text);
            //if (intval == 0 || intval > 5)
            if (intval == 0 || intval > this.SHIHARAI_CD_MAX)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 設定区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSetteiKbn()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtSetteiKbn.Text))
            {
                this.txtSetteiKbn.Text = "1";
            }

            // 1~2のみ入力を許可
            if (!ValChk.IsNumber(this.txtSetteiKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtSetteiKbn.Text);
            if (intval == 0 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtShishoCd.Focus();
                this.txtShishoCd.SelectAll();
                return false;
            }

            // 取引区分の入力チェック
            if (!IsValidTorihikiKbn())
            {
                this.txtTorihikiKbn.Focus();
                this.txtTorihikiKbn.SelectAll();
                return false;
            }

            // 設定区分の入力チェック
            if (!IsValidSetteiKbn())
            {
                this.txtSetteiKbn.Focus();
                this.txtSetteiKbn.SelectAll();
                return false;
            }


            return true;
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_CM_SHISHO"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            string table;
            if (this.txtSetteiKbn.Text == "1")
            {
                table = "TB_HN_ZIDO_SHIWAKE_SETTEI_A";
            }
            else
            {
                table = "TB_HN_ZIDO_SHIWAKE_SETTEI_B";
            }
            string shishoCd = this.txtShishoCd.Text;
            // 商品テーブルからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            StringBuilder where = new StringBuilder("A.KAISHA_CD = @KAISHA_CD");
            where.Append(" AND A.SHISHO_CD = @SHISHO_CD ");
            if (isInitial)
            {
                //初期処理の場合、検索結果がヒットしないようにあり得ない検索条件を設定する
                where.Append(" AND A.DENPYO_KUBUN = -1");
            }
            else
            {
                // 初期処理でない場合、入力された取引区分から検索する
                if (!ValChk.IsEmpty(this.txtTorihikiKbn.Text))
                {
                    where.Append(" AND A.DENPYO_KUBUN = @DENPYO_KUBUN");
                    dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, this.txtTorihikiKbn.Text);
                }
            }
            string cols = "A.SHIWAKE_CD AS コード";
            cols += ", A.SHIWAKE_NM AS 仕訳名称";
            cols += ", B.KANJO_KAMOKU_NM AS 勘定科目名";
            string from = table + " AS A LEFT JOIN VI_ZM_KANJO_KAMOKU AS B ON A.KAISHA_CD = B.KAISHA_CD AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND B.KAIKEI_NENDO = @KAIKEI_NENDO ";

            DataTable dtShiwake =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "A.DENPYO_KUBUN", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtShiwake.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                //dtShiwake.Rows.Add(dtShiwake.NewRow());
            }

            this.dgvList.DataSource = dtShiwake;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ 明朝", 12F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 280;
            this.dgvList.Columns[2].Width = 230;
        }

        /// <summary>
        /// 商品を追加編集する
        /// </summary>
        /// <param name="code">商品コード(空：新規登録、以外：編集)</param>
        private void EditShiwake(string code)
        {
            CMCM2052 frmCMCM2052;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmCMCM2052 = new CMCM2052("1", this.txtTorihikiKbn.Text, this.txtSetteiKbn.Text);
                frmCMCM2052.SHISHO_CD = this.txtShishoCd.Text;
            }
            else
            {
                // 編集モードで登録画面を起動
                frmCMCM2052 = new CMCM2052("2", this.txtTorihikiKbn.Text, this.txtSetteiKbn.Text);
                frmCMCM2052.InData = code;
                frmCMCM2052.SHISHO_CD = this.txtShishoCd.Text;
            }

            DialogResult result = frmCMCM2052.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }
#endregion
    }
}
