﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm2061
{
    /// <summary>
    /// 摘要の登録(CMCM2061)
    /// </summary>
    public partial class CMCM2061 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region 変数
        private bool gridEnterFlag;          // 
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2061()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region プロパティ
        // 支所プロパティ
        public int ShishoCode { get; set; }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める741, 533
                this.Size = new Size(714, 533);
                //this.dgvList.Size = new Size(540, 278);
                // フォームの配置を上へ移動する
                //this.lblKanaNm.Location = new System.Drawing.Point(12, 11);
                //this.txtKanaNm.Location = new System.Drawing.Point(89, 13);
                //this.txtKanaNm.Location = new System.Drawing.Point(65, 13);
                //this.lblMizuageShisho.Location = new System.Drawing.Point(242, 11);
                //this.txtMizuageShishoCd.Location = new System.Drawing.Point(278, 13);
                //this.lblMizuageShishoNm.Location = new System.Drawing.Point(332, 13);
                //this.lblMizuageShishoNm.Location = new System.Drawing.Point(315, 13);
                //this.dgvList.Location = new System.Drawing.Point(12, 44);
                // EscapeとF1のみ表示
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF4.Location = this.btnF3.Location;
                this.btnF3.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // 支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
            this.ShishoCode = 1;
#else
            if (this.InData != null && !ValChk.IsEmpty(this.InData))
            {
                string inData = (string)this.InData;
                if (inData.Length != 0)
                    this.ShishoCode = Util.ToInt(inData);
            }
#endif
            this.txtMizuageShishoCd.Text = this.ShishoCode.ToString();
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.ShishoCode == 1 && this.Par1 == "") ? true : false; 
            this.Text = this.lblTitle.Text;
            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            //// カナ名にフォーカス
            //this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaNm.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            //// 支所にフォーカス時のみF1を有効にする
            //switch (this.ActiveCtlNm)
            //{
            //    case "txtMizuageShishoCd":
            //        this.btnF1.Enabled = true;
            //        break;
            //    default:
            //        this.btnF1.Enabled = false;
            //        break;
            //}
            // 支所フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtKanaNm":
                    this.btnF1.Enabled = false;
                    break;
                default:
                    this.btnF1.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            int flg = 0;
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                        flg = 1;
                    }
                    break;
                case "txtKanaNm":
                    {
                        // カナ名にフォーカスを戻す
                        this.txtKanaNm.Focus();
                        this.txtKanaNm.SelectAll();
                        flg = 1;
                        break;
                    }
            }
            // グリッド中からのF1対応
            if (gridEnterFlag && flg == 0)
            {
                // カナ名にフォーカスを戻す
                this.txtKanaNm.Focus();
                this.txtKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 摘要登録画面の起動
            EditTekiyo(string.Empty);
        }
#endregion

#region イベント
        /// <summary>
        /// 支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaNm_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない

            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditTekiyo(Util.ToString(this.dgvList.SelectedRows[0].Cells["摘要コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditTekiyo(Util.ToString(this.dgvList.SelectedRows[0].Cells["摘要コード"].Value));
            }
        }

        /// <summary>
        /// グリッドEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Enter(object sender, EventArgs e)
        {
            gridEnterFlag = true;
            this.btnF1.Enabled = true;
            /*
            if (this.MenuFrm != null)
                ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = this.btnF1.Enabled;
                */
        }

        /// <summary>
        /// グリッドLeave時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Leave(object sender, EventArgs e)
        {
            gridEnterFlag = false;
            //try
            //{
            //    this.dgvList.CurrentCell = null;
            //}
            //catch (Exception) { }
        }

        /// <summary>
        /// グリッドCellClick時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            gridEnterFlag = true;
            this.btnF1.Enabled = true;
            /*
            if (this.MenuFrm != null)
                ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = this.btnF1.Enabled;
                */
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                //// 水揚支所名称を表示する
                //this.txtMizuageShishoCd.Text = "0";
                //this.lblMizuageShishoNm.Text = "全て";
                //return true;

                // 更新処理時は必須
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            this.ShishoCode = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));

            return true;
        }
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 摘要マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            // 初期処理でない場合、入力されたカナ名から検索する
            if (!ValChk.IsEmpty(this.txtKanaNm.Text))
            {
                where.Append(" AND TEKIYO_KANA_NM LIKE @TEKIYO_KANA_NM");
                // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                dpc.SetParam("@TEKIYO_KANA_NM", SqlDbType.VarChar, 30, "%" + this.txtKanaNm.Text + "%");
            }
            string cols = "TEKIYO_CD AS 摘要コード";
            cols += ", TEKIYO_NM AS 摘要名";
            cols += ", TEKIYO_KANA_NM AS 摘要カナ名";
            string from = "TB_HN_TEKIYO";

            DataTable dtTekiyo =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "TEKIYO_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtTekiyo.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtTekiyo.Rows.Add(dtTekiyo.NewRow());
            }

            this.dgvList.DataSource = dtTekiyo;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[1].Width = 230;
            this.dgvList.Columns[2].Width = 200;
        }

        /// <summary>
        /// 摘要を追加編集する
        /// </summary>
        /// <param name="code">摘要コード(空：新規登録、以外：編集)</param>
        private void EditTekiyo(string code)
        {
            CMCM2062 frmCMCM2062;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmCMCM2062 = new CMCM2062("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmCMCM2062 = new CMCM2062("2");
                frmCMCM2062.InData = code;
            }
            frmCMCM2062.Par2 = this.txtMizuageShishoCd.Text;

            DialogResult result = frmCMCM2062.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["摘要コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["摘要コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["摘要名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["摘要カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
