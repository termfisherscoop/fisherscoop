﻿namespace jp.co.fsi.cm.cmcm2061
{
    partial class CMCM2062
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTekiyoCd = new System.Windows.Forms.Label();
            this.txtTekiyoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTekiyoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTekiyoNm = new System.Windows.Forms.Label();
            this.txtTekiyoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTekiyoKanaNm = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 88);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.pnlDebug.Size = new System.Drawing.Size(554, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(544, 31);
            this.lblTitle.Text = "";
            // 
            // lblTekiyoCd
            // 
            this.lblTekiyoCd.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyoCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTekiyoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyoCd.Location = new System.Drawing.Point(0, 0);
            this.lblTekiyoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTekiyoCd.Name = "lblTekiyoCd";
            this.lblTekiyoCd.Size = new System.Drawing.Size(520, 33);
            this.lblTekiyoCd.TabIndex = 1;
            this.lblTekiyoCd.Tag = "CHANGE";
            this.lblTekiyoCd.Text = "摘要コード";
            this.lblTekiyoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoCd
            // 
            this.txtTekiyoCd.AutoSizeFromLength = false;
            this.txtTekiyoCd.DisplayLength = null;
            this.txtTekiyoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoCd.Location = new System.Drawing.Point(99, 5);
            this.txtTekiyoCd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTekiyoCd.MaxLength = 4;
            this.txtTekiyoCd.Name = "txtTekiyoCd";
            this.txtTekiyoCd.Size = new System.Drawing.Size(69, 23);
            this.txtTekiyoCd.TabIndex = 2;
            this.txtTekiyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiyoCd.TextChanged += new System.EventHandler(this.txtTekiyoCd_TextChanged);
            this.txtTekiyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
            // 
            // txtTekiyoNm
            // 
            this.txtTekiyoNm.AutoSizeFromLength = false;
            this.txtTekiyoNm.DisplayLength = null;
            this.txtTekiyoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTekiyoNm.Location = new System.Drawing.Point(99, 5);
            this.txtTekiyoNm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTekiyoNm.MaxLength = 40;
            this.txtTekiyoNm.Name = "txtTekiyoNm";
            this.txtTekiyoNm.Size = new System.Drawing.Size(299, 23);
            this.txtTekiyoNm.TabIndex = 4;
            this.txtTekiyoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoNm_Validating);
            // 
            // lblTekiyoNm
            // 
            this.lblTekiyoNm.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyoNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTekiyoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyoNm.Location = new System.Drawing.Point(0, 0);
            this.lblTekiyoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTekiyoNm.Name = "lblTekiyoNm";
            this.lblTekiyoNm.Size = new System.Drawing.Size(520, 33);
            this.lblTekiyoNm.TabIndex = 3;
            this.lblTekiyoNm.Tag = "CHANGE";
            this.lblTekiyoNm.Text = "摘　要  名";
            this.lblTekiyoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoKanaNm
            // 
            this.txtTekiyoKanaNm.AutoSizeFromLength = false;
            this.txtTekiyoKanaNm.DisplayLength = null;
            this.txtTekiyoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtTekiyoKanaNm.Location = new System.Drawing.Point(99, 6);
            this.txtTekiyoKanaNm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTekiyoKanaNm.MaxLength = 30;
            this.txtTekiyoKanaNm.Name = "txtTekiyoKanaNm";
            this.txtTekiyoKanaNm.Size = new System.Drawing.Size(299, 23);
            this.txtTekiyoKanaNm.TabIndex = 6;
            this.txtTekiyoKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTekiyoKanaNm_KeyDown);
            this.txtTekiyoKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoKanaNm_Validating);
            // 
            // lblTekiyoKanaNm
            // 
            this.lblTekiyoKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyoKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTekiyoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyoKanaNm.Location = new System.Drawing.Point(0, 0);
            this.lblTekiyoKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTekiyoKanaNm.Name = "lblTekiyoKanaNm";
            this.lblTekiyoKanaNm.Size = new System.Drawing.Size(520, 36);
            this.lblTekiyoKanaNm.TabIndex = 5;
            this.lblTekiyoKanaNm.Tag = "CHANGE";
            this.lblTekiyoKanaNm.Text = "摘要カナ名";
            this.lblTekiyoKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(219, 5);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 906;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.Text = "9999";
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(267, 4);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(123, 24);
            this.lblMizuageShishoNm.TabIndex = 907;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShisho.Location = new System.Drawing.Point(176, 1);
            this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(44, 33);
            this.lblMizuageShisho.TabIndex = 905;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 12);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(528, 124);
            this.fsiTableLayoutPanel1.TabIndex = 908;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtTekiyoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.lblTekiyoCd);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(520, 33);
            this.fsiPanel1.TabIndex = 0;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtTekiyoNm);
            this.fsiPanel2.Controls.Add(this.lblTekiyoNm);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 44);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(520, 33);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtTekiyoKanaNm);
            this.fsiPanel3.Controls.Add(this.lblTekiyoKanaNm);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 84);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(520, 36);
            this.fsiPanel3.TabIndex = 2;
            // 
            // CMCM2062
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 225);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.Name = "CMCM2062";
            this.ShowFButton = true;
            this.Text = "摘要の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTekiyoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoNm;
        private System.Windows.Forms.Label lblTekiyoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoKanaNm;
        private System.Windows.Forms.Label lblTekiyoKanaNm;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}