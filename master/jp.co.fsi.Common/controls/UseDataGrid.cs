﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace jp.co.fsi.common.controls
{
	public partial class UseDataGrid : UserControl
	{

		private int intheadColumnCount = 1;
		private int intBodyColumnCount = 1;
		private int intfootColumnCount = 1;

		private int intheadRowCount = 1;
		private int intBodyRowCount = 1;
		private int intfootRowCount = 1;

		private Button[] btnHeadCollection;

		private FsiExTextBox[] txtBodyCollection;

		private FsiExTextBox[] txtFootCollection;

		[DisplayName("ヘッダ列カウント")]
		public int HeadColumnCount
		{
			get { return intheadColumnCount; }
			set
			{
				intheadColumnCount = value;
				headTbl.ColumnCount = value;

				if (null == btnHeadCollection) return;

				SetTbl(
					intheadColumnCount,
					intheadRowCount,
					headTbl, btnHeadCollection);
			}
		}

		[DisplayName("本文列カウント")]
		public int BodyColumnCount
		{
			get { return intBodyColumnCount; }
			set
			{

				intBodyColumnCount = value;
				bodyTbl.ColumnCount = value;

				if (null == txtBodyCollection) return;
				SetTbl(
					intBodyColumnCount,
					intBodyRowCount,
					bodyTbl, txtBodyCollection);
			}
		}

		[DisplayName("フッター列カウント")]
		public int FootColumnCount
		{
			get { return intfootColumnCount; }
			set
			{
				intfootColumnCount = value;
				footTbl.ColumnCount = value;

				if (null == txtFootCollection) return;
				SetTbl(
					intfootColumnCount,
					intfootRowCount,
					footTbl, txtFootCollection);
			}
		}

		[DisplayName("ヘッダ行カウント")]
		public int HeadRowCount
		{
			get { return intheadRowCount; }
			set
			{
				intheadRowCount = value;
				headTbl.RowCount = value;

				if (null == btnHeadCollection) return;
				SetTbl(
					intheadColumnCount,
					intheadRowCount,
					headTbl, btnHeadCollection);
			}
		}

		[DisplayName("本文行カウント")]
		public int BodyRowCount
		{
			get { return intBodyRowCount; }
			set
			{
				intBodyRowCount = value;
				bodyTbl.RowCount = value;
			}
		}

		[DisplayName("フッター行カウント")]
		public int FootRowCount
		{
			get { return intfootRowCount; }
			set
			{
				intfootRowCount = value;
				footTbl.RowCount = value;
			}
		}

		[DisplayName("ヘッダオブジェクトコレクション")]
		public Button[] HeadCollection
		{
			get
			{
				return btnHeadCollection;
			}
			set
			{
				btnHeadCollection = value;

				SetTbl(
					intheadColumnCount, 
					intheadRowCount, 
					headTbl, btnHeadCollection);
			}
		}

		[DisplayName("本文オブジェクトコレクション")]
		public FsiExTextBox[] BodyCollection
		{
			get
			{
				return txtBodyCollection;
			}

			set
			{
				txtBodyCollection = value;
				SetTbl(
					intBodyColumnCount,
					intBodyRowCount,
					bodyTbl, txtBodyCollection);
			}
		}

		[DisplayName("フッターオブジェクトコレクション")]
		public FsiExTextBox[] FootCollection
		{
			get
			{
				return txtFootCollection;
			}

			set
			{
				txtFootCollection = value;
				SetTbl(
					intfootColumnCount,
					intfootRowCount,
					footTbl, txtFootCollection);
			}
		}

		public UseDataGrid()
		{
			InitializeComponent();
		}

		protected override void OnLoad(EventArgs e)
		{
			// ヘッダテーブルの生成
			headTbl.ColumnCount = intheadColumnCount;
			headTbl.RowCount = intheadRowCount;
			bodyTbl.ColumnCount = intBodyColumnCount;
			bodyTbl.RowCount = intBodyRowCount;
			footTbl.ColumnCount = intfootColumnCount;
			footTbl.RowCount = intfootRowCount;

			if (null != HeadCollection)
			{
				if (0 < HeadCollection.Length)
				{
					SetTbl(
						intheadColumnCount,
						intheadRowCount,
						headTbl, btnHeadCollection);
				}
			}

			if (null != BodyCollection)
			{
				if (0 < BodyCollection.Length)
				{
					SetTbl(
						intBodyColumnCount,
						intBodyRowCount,
						bodyTbl, txtBodyCollection);

				}

			}

			if (null != FootCollection)
			{
				if (0 < FootCollection.Length)
				{
					SetTbl(
						intfootColumnCount,
						intfootRowCount,
						footTbl, txtFootCollection);
				}
			}
			base.OnLoad(e);
		}


		#region プライベートメソッド

		private void SetTbl(int row,int col,TableLayoutPanel tbls,object[] objs)
		{

			if (1 == row)
			{
				for (int i = 0; i < col; i++)
				{


					if (objs is Button[])
					{
						tbls.Controls.Add(((Button[])objs)[i], i, 0);
					}
					else if (objs is FsiExTextBox[])
					{
						tbls.Controls.Add(((FsiExTextBox[])objs)[i], i, 0);
					}

				}
			}
			else
			{
				for (int i = 0; i < row; i++)
				{
					for (int j = 0; j < col; j++)
					{
						Panel pnl00 = new Panel();
						pnl00.Dock = DockStyle.Fill;

						if (objs is Button[])
						{
							pnl00.Controls.Add(((Button[])objs)[j]);
						}
						else if (objs is FsiExTextBox[])
						{
							pnl00.Controls.Add(((FsiExTextBox[])objs)[j]);
						}

						tbls.Controls.Add(pnl00, j, i);
					}
				}
			}


		}

		#endregion

	}
}
