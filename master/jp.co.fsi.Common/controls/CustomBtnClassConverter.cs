﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace jp.co.fsi.common
{
	public class CustomBtnClassConverter: ExpandableObjectConverter
	{

		public CustomBtnClassConverter()
		{

		}

		public override bool CanConvertTo(
			ITypeDescriptorContext context,
			Type destinationType)
		{

			if (destinationType == typeof(Button))
			{
				return true;

			}
			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(
			ITypeDescriptorContext context,
			CultureInfo culture,
			object value,
			Type destinationType)
		{
			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool CanConvertFrom(
			ITypeDescriptorContext context,
			Type sourceType)
		{

			if (sourceType == typeof(Button))
			{
				return true;
			}

			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(
			ITypeDescriptorContext context,
			CultureInfo culture, object value)
		{
			return base.ConvertFrom(context, culture, value);
		}



	}
}
