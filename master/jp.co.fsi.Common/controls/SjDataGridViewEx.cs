﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data; // 'System.InvalidOperationException'対策

namespace jp.co.fsi.common.controls
{
    /// <summary>
    /// DataGridViewを拡張し、Enterキーが押された時にTabキーが押されたのと同じ動作をするようにした
    /// （現在のセルを隣のセルに移動する）
    /// </summary>
    public class SjDataGridViewEx : DataGridView
    {
        /// <summary>
        /// Enterキー、Tabキー、左右カーソルキーが押された時に、
        /// リードオンリーのセルをスキップする
        /// </summary>
        /// <param name="keyData">キーコード</param>
        /// <returns>true：スキップあり、false：スキップなし</returns>
        private bool skipReadonlyCell(Keys keyData)
        {
            // Enterキー、Tabキー、左右カーソルキー以外はスキップしない
            switch (keyData & Keys.KeyCode)
            {
                case Keys.Enter:
                case Keys.Tab:
                case Keys.Left:
                case Keys.Right:
                    break;
                default:
                    return false;
            }

            int fromIndex = this.CurrentCell.ColumnIndex;
            int toIndex = fromIndex;
            int addIndex = 1;
            int maxCellIndex = this.CurrentRow.Cells.Count - 1;
            int MaxRowIndex = this.Rows.Count - 1;
            DataGridViewCell nextCell = this.CurrentRow.Cells[toIndex];

            // Shiftキー又は左カーソルキーが押されている場合は左方向に移動させる
            if ((keyData & Keys.Shift) == Keys.Shift
             || (keyData & Keys.KeyCode) == Keys.Left)
            {
                addIndex = -1;
            }

            // 左端で左方向の移動の場合は移動しない
            if (toIndex <= 0 && -1 == addIndex)
            {
                nextCell = this.CurrentRow.Cells[0];
                this.CurrentCell = nextCell;
                return true;
            }

            // 右端で右方向の移動の場合は改行する
            if (maxCellIndex <= toIndex && 1 == addIndex)
            {
                if (this.CurrentRow.Index < MaxRowIndex)
                {
                nextCell = this.Rows.SharedRow(this.CurrentRow.Index + 1).Cells[0];
                this.CurrentCell = nextCell;
                return true;
                }
                nextCell = this.Rows.SharedRow(this.CurrentRow.Index).Cells[0];
                toIndex = 0;
            }

            do
            {
                // 列（セル）を１つ移動する
                toIndex += addIndex;

                // 右端を超えた場合は改行する
                if (maxCellIndex < toIndex)
                {
                    // 最終行の右端なら処理終了
                    if (this.Rows.Count <= this.CurrentRow.Index + 1)
                    {
                        break;
                    }

                    // 改行する
                    // 下の方法ではうまくいかないので、下キーのイベントを投げるか？
                    nextCell = this.Rows.SharedRow(this.CurrentRow.Index + 1).Cells[0];
                    this.CurrentCell = nextCell;
                    toIndex = 0;
                }
                else
                {
                    // 判定対象のセルを取得する
                    nextCell = this.CurrentRow.Cells[toIndex];
                }

                // 入力可能なセル 又は 左端の場合は、そのセルに遷移する
                if (!nextCell.ReadOnly || !nextCell.Visible || (toIndex + addIndex) <= 0)
                {
                    break;
                }
            } while (0 <= toIndex && toIndex <= maxCellIndex);

            // 次の入力可能セルにフォーカスする
            this.CurrentCell = nextCell;

            return true;

        }

        [System.Security.Permissions.UIPermission(
            System.Security.Permissions.SecurityAction.Demand,
            Window = System.Security.Permissions.UIPermissionWindow.AllWindows)]
        protected override bool ProcessDialogKey(Keys keyData)
        {
            try
            {
                // Enterキー、Tabキー、左右カーソルキー押下時にリードオンリーのセルをスキップする
                if (this.skipReadonlyCell(keyData))
                {
                    return true;
                }

                //Enterキーが押された時は、Tabキーが押されたようにする
                if ((keyData & Keys.KeyCode) == Keys.Enter)
                {
                    return this.ProcessTabKey(keyData);
                }
                return base.ProcessDialogKey(keyData);

            }
            catch (InvalidOperationException exp)
            {
                /* 入力エラー時に'System.InvalidOperationException'が
                 * 発生した場合は処理せずに終了する。
                 * 'System.InvalidOperationException'が出る理由は、
                 * 「セル値の変更をコミットまたは中止できないため、操作は成功しませんでした。」
                 * らしい。
                */
                return true;
            }

        }

        [System.Security.Permissions.SecurityPermission(
            System.Security.Permissions.SecurityAction.Demand,
            Flags = System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)]
        protected override bool ProcessDataGridViewKey(KeyEventArgs e)
        {
            try
            {
                // Enterキー、Tabキー、左右カーソルキー押下時にリードオンリーのセルをスキップする
                if (this.skipReadonlyCell(e.KeyCode))
                {
                    return true;
                }

                //Enterキーが押された時は、Tabキーが押されたようにする
                if (e.KeyCode == Keys.Enter)
                {
                    return this.ProcessTabKey(e.KeyCode);
                }
                return base.ProcessDataGridViewKey(e);

            }
            catch (InvalidOperationException exp)
            {
                /* 入力エラー時に'System.InvalidOperationException'が
                 * 発生した場合は処理せずに終了する。
                 * 'System.InvalidOperationException'が出る理由は、
                 * 「セル値の変更をコミットまたは中止できないため、操作は成功しませんでした。」
                 * らしい。
                */
                return true;
            }

        }

    }

    /// <summary>
    /// マウスホイールでスクロール用クラス
    ///   ※KOBE1011のMessageForwarder.csをコピーした
    /// </summary>
    public class MessageForwarder : NativeWindow, IMessageFilter
    {
        private Control _Control;
        private Control _PreviousParent;
        private HashSet<int> _Messages;
        private bool _IsMouseOverControl;

        public MessageForwarder(Control control, int message)
            : this(control, new int[] { message })
        {
            ; // 処理なし
        }

        public MessageForwarder(Control control, IEnumerable<int> messages)
        {
            _Control = control;
            AssignHandle(control.Handle);
            _Messages = new HashSet<int>(messages);
            _PreviousParent = control.Parent;
            _IsMouseOverControl = false;

            control.ParentChanged += new EventHandler(control_ParentChanged);
            control.MouseEnter += new EventHandler(control_MouseEnter);
            control.MouseLeave += new EventHandler(control_MouseLeave);
            control.Leave += new EventHandler(control_Leave);

            if (control.Parent != null)
            {
                Application.AddMessageFilter(this);
            }
        }

        public bool PreFilterMessage(ref Message m)
        {
            if (_Messages.Contains(m.Msg) &&
                _Control.CanFocus &&
                !_Control.Focused &&
                _IsMouseOverControl)
            {
                m.HWnd = _Control.Handle;
                WndProc(ref m);
                return true;
            }

            return false;
        }

        void control_ParentChanged(object sender, EventArgs e)
        {
            if (_Control.Parent == null)
            {
                Application.RemoveMessageFilter(this);
            }
            else
            {
                if (_PreviousParent == null)
                {
                    Application.AddMessageFilter(this);
                }
            }
            _PreviousParent = _Control.Parent;
        }

        void control_MouseEnter(object sender, EventArgs e)
        {
            _IsMouseOverControl = true;
        }

        void control_MouseLeave(object sender, EventArgs e)
        {
            _IsMouseOverControl = false;
        }

        void control_Leave(object sender, EventArgs e)
        {
            _IsMouseOverControl = false;
        }
    }
}
