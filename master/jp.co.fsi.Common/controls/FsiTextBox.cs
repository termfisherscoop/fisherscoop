﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace jp.co.fsi.common.controls
{
    /// <summary>
    /// テキストボックスを拡張したコントロールです。
    /// </summary>
    public class FsiTextBox : TextBox
    {
        #region プロパティ
        /// <summary>
        /// 最大桁数
        /// </summary>
        [Browsable(true)]
        [Description("最大桁数です。")]
        public override int MaxLength
        {
            get
            {
                return base.MaxLength;
            }
            set
            {
                base.MaxLength = value;
                if (this._autoSizeFromLength)
                {
                    ChangeSize();
                }
            }
        }

        private Nullable<int> _displayLength = null;
        /// <summary>
        /// 表示桁数
        /// </summary>
        [Browsable(true)]
        [Description("表示桁数です。")]
        public Nullable<int> DisplayLength
        {
            get
            {
                return this._displayLength;
            }
            set
            {
                this._displayLength = value;
                //if (this._autoSizeFromLength)
                //{
                //    ChangeSize();
                //}
            }
        }

        private bool _autoSizeFromLength = false;
        /// <summary>
        /// 桁数から幅を自動調整するかを設定します。
        /// ※MSゴシック10ポイント基準です。
        /// </summary>
        [Browsable(true)]
        [Description("桁数から幅を自動調整するかを設定します。")]
        public bool AutoSizeFromLength
        {
            get
            {
                return this._autoSizeFromLength;
            }
            set
            {
                this._autoSizeFromLength = value;
                if (value)
                {
                    ChangeSize();
                }
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public FsiTextBox() : base()
        {
            // デフォルトのフォントサイズをMSゴシック10ptに設定
            //this.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
        }
        #endregion

        #region privateメソッド

        protected override void OnEnabledChanged(EventArgs e)
        {

            if (this.Enabled)
            {
                this.BackColor = System.Drawing.Color.White;
            }
            else
            {
                this.BackColor = System.Drawing.Color.LightCyan;
            }

            base.OnEnabledChanged(e);
        }

        protected override void OnEnter(EventArgs e)
        {
			base.OnEnter(e);
			this.SelectAll();
        }

		protected override void OnGotFocus(EventArgs e)
		{
			base.OnGotFocus(e);
			this.SelectAll();
		}

		/// <summary>
		/// サイズ変更
		/// </summary>
		private void ChangeSize()
        {
            if (this._autoSizeFromLength)
            {
                int baseLength = 0;

                if (this._displayLength == null)
                {
                    baseLength = this.MaxLength;
                }
                else
                {
                    baseLength = this._displayLength.Value;
                }

                this.Size = new System.Drawing.Size(baseLength * 7 + 6, this.Size.Height);
            }
        }
        #endregion

    }
}
