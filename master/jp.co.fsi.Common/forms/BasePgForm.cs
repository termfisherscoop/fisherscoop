﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.common.forms
{
    /// <summary>
    /// 基底フォームクラス
    /// </summary>
    public partial class BasePgForm : BaseForm
    {
        #region private変数
        /// <summary>
        /// プログラム単位の設定情報
        /// </summary>
        DataRow _drPgConf;

        public delegate void FuncBtnEnabledHandler(object sender, string btnName);

        public event FuncBtnEnabledHandler FuncBtnEnableChanged;

        public string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

        #endregion

        #region プロパティ
        private string _par1;
        /// <summary>
        /// 引数1
        /// </summary>
        public string Par1
        {
            get
            {
                return this._par1;
            }
            set
            {
                this._par1 = value;
            }
        }

        private string _par2;
        /// <summary>
        /// 引数2
        /// </summary>
        public string Par2
        {
            get
            {
                return this._par2;
            }
            set
            {
                this._par2 = value;
            }
        }

        private string _par3;
        /// <summary>
        /// 引数3
        /// </summary>
        public string Par3
        {
            get
            {
                return this._par3;
            }
            set
            {
                this._par3 = value;
            }
        }

        private object _inData;
        /// <summary>
        /// 入力データ
        /// </summary>
        public object InData
        {
            get
            {
                return this._inData;
            }
            set
            {
                this._inData = value;
            }
        }

        private object _outData;
        /// <summary>
        /// 出力データ
        /// </summary>
        public object OutData
        {
            get
            {
                return this._outData;
            }
            set
            {
                this._outData = value;
            }
        }

        private bool _showFButton = false;
        /// <summary>
        /// ファンクションボタンを表示するかどうか
        /// </summary>
        [Browsable(true)]
        [Description("ファンクションボタンを表示するかどうかを設定します。")]
        public bool ShowFButton
        {
            get
            {
                return this._showFButton;
            }
            set
            {
                this._showFButton = value;
                this.pnlDebug.Visible = ((this.MenuFrm == null && this.Owner == null) || this._showFButton);
            }
        }

        private bool _showTitle = true;
        /// <summary>
        /// 機能タイトルを画面に表示するかどうか
        /// </summary>
        [Browsable(true)]
        [Description("機能タイトルを画面に表示するかどうかを設定します。")]
        public bool ShowTitle
        {
            get
            {
                return this._showTitle;
            }
            set
            {
                this._showTitle = value;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public BasePgForm() : this(null)
        {
        }

        /// <summary>
        /// コンストラクタ(引数1つver.)
        /// </summary>
        /// <param name="par1">引数1</param>
        public BasePgForm(string par1) : this(par1, null)
        {
        }

        /// <summary>
        /// コンストラクタ(引数2つver.)
        /// </summary>
        /// <param name="par1">引数1</param>
        /// <param name="par2">引数2</param>
        public BasePgForm(string par1, string par2) : this(par1, par2, null)
        {
        }

        /// <summary>
        /// コンストラクタ(引数3つver.)
        /// </summary>
        /// <param name="par1">引数1</param>
        /// <param name="par2">引数2</param>
        /// <param name="par3">引数3</param>
        public BasePgForm(string par1, string par2, string par3)
        {
            if (!ValChk.IsEmpty(par1))
            {
                this._par1 = par1;
            }

            if (!ValChk.IsEmpty(par2))
            {
                this._par2 = par2;
            }

            if (!ValChk.IsEmpty(par3))
            {
                this._par3 = par3;
            }

            // リサイズを禁止する
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;

            InitializeComponent();
        }
        #endregion

        #region protectedメソッド
        /// <summary>
        /// 初期処理
        /// </summary>
        /// <remarks>必要に応じて各継承クラスにてオーバーライド</remarks>
        protected override bool InitBase()
        {
            if (!base.InitBase())
            {
                return false;
            }

            // ファイルを元に設定ファイルからプログラム名を取得
            string clsName = "";
            try
            {
                clsName = this.Name;
            }
            catch (Exception)
            {
                // デザイナのために、アセンブリ情報が取得された場合だけ(実プログラムの動作時のみ)
                // asNameに値を代入する
            }
            this._drPgConf = MInfo.LoadPgConf(clsName, this._par1, this._par2, this._par3);

            // デザイナのために、設定が取れた場合だけ(実プログラムの動作時のみ)設定を反映する
            if (this._drPgConf != null)
            {
                // 取得したプログラム名をフォームのタイトルとヘッダー部のラベルに表示する
                this.Text = Util.ToString(this._drPgConf["PrgNm"]);
                this.lblTitle.Text = Util.ToString(this._drPgConf["PrgNm"]);

                this.lblTitle.Height = 32;

                // 取得したファンクション情報をボタンに反映する
                if (!ValChk.IsEmpty(this._drPgConf["CapEsc"]))
                {
                    this.btnEsc.Text = "Esc" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapEsc"]);
                }
                this.btnF1.Text = "F1" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF1"]);
                this.btnF2.Text = "F2" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF2"]);
                this.btnF3.Text = "F3" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF3"]);
                this.btnF4.Text = "F4" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF4"]);
                this.btnF5.Text = "F5" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF5"]);
                this.btnF6.Text = "F6" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF6"]);
                this.btnF7.Text = "F7" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF7"]);
                this.btnF8.Text = "F8" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF8"]);
                this.btnF9.Text = "F9" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF9"]);
                this.btnF10.Text = "F10" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF10"]);
                this.btnF11.Text = "F11" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF11"]);
                this.btnF12.Text = "F12" + Environment.NewLine + Environment.NewLine + Util.ToString(this._drPgConf["CapF12"]);

                // キャプションが空ならDisable
                this.btnF1.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF1"]);
                this.btnF2.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF2"]);
                this.btnF3.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF3"]);
                this.btnF4.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF4"]);
                this.btnF5.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF5"]);
                this.btnF6.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF6"]);
                this.btnF7.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF7"]);
                this.btnF8.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF8"]);
                this.btnF9.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF9"]);
                this.btnF10.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF10"]);
                this.btnF11.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF11"]);
                this.btnF12.Enabled = !ValChk.IsEmpty(this._drPgConf["CapF12"]);

                base.isBtnF1Enable = this.btnF1.Enabled;
                base.isBtnF2Enable = this.btnF2.Enabled;
                base.isBtnF3Enable = this.btnF3.Enabled;
                base.isBtnF4Enable = this.btnF4.Enabled;
                base.isBtnF5Enable = this.btnF5.Enabled;
                base.isBtnF6Enable = this.btnF6.Enabled;
                base.isBtnF7Enable = this.btnF7.Enabled;
                base.isBtnF8Enable = this.btnF8.Enabled;
                base.isBtnF9Enable = this.btnF9.Enabled;
                base.isBtnF10Enable = this.btnF10.Enabled;
                base.isBtnF11Enable = this.btnF11.Enabled;
                base.isBtnF12Enable = this.btnF12.Enabled;

            }

            // 画面タイトルの表示制御
            this.lblTitle.Visible = this._showTitle;


            // 親フォームがあればフッター部のファンクション情報を隠す(デバッグ用)
            // TODO:仮実装かもしんない
            this.pnlDebug.Visible = ((this.MenuFrm == null && this.Owner == null) || this._showFButton);

            return true;
        }


        /// <summary>
        /// Beep音制御
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyPress(KeyPressEventArgs e)
        {

            if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Escape)
            {

                e.Handled = true;

            }

            base.OnKeyPress(e);
        }

        #endregion

        #region イベント
        /// <summary>
        /// (デバッグ用)Escキークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEsc_Click(object sender, EventArgs e)
        {
            this.PressEsc();
        }

        /// <summary>
        /// (デバッグ用)F1キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF1_Click(object sender, EventArgs e)
        {
            this.PressF1();
        }

        /// <summary>
        /// (デバッグ用)F2キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF2_Click(object sender, EventArgs e)
        {
            this.PressF2();
        }

        /// <summary>
        /// (デバッグ用)F3キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF3_Click(object sender, EventArgs e)
        {
            this.PressF3();
        }

        /// <summary>
        /// (デバッグ用)F4キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF4_Click(object sender, EventArgs e)
        {
            this.PressF4();
        }

        /// <summary>
        /// (デバッグ用)F5キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF5_Click(object sender, EventArgs e)
        {
            this.PressF5();
        }

        /// <summary>
        /// (デバッグ用)F6キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF6_Click(object sender, EventArgs e)
        {
            this.PressF6();
        }

        /// <summary>
        /// (デバッグ用)F7キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF7_Click(object sender, EventArgs e)
        {
            this.PressF7();
        }

        /// <summary>
        /// (デバッグ用)F8キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF8_Click(object sender, EventArgs e)
        {
            this.PressF8();
        }

        /// <summary>
        /// (デバッグ用)F9キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF9_Click(object sender, EventArgs e)
        {
            this.PressF9();
        }

        /// <summary>
        /// (デバッグ用)F10キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF10_Click(object sender, EventArgs e)
        {
            this.PressF10();
        }

        /// <summary>
        /// (デバッグ用)F11キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF11_Click(object sender, EventArgs e)
        {
            this.PressF11();
        }

        /// <summary>
        /// (デバッグ用)F12キークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF12_Click(object sender, EventArgs e)
        {
            this.PressF12();
        }
        #endregion

        #region privateメソッド
        #endregion

        #region ボタン活性・非活性イベント
        private void btn_EnabledChanged(object sender, EventArgs e)
        {

            string strbName = string.Empty;

            switch (((Button)sender).Name)
            {

                case "btnEsc":

                    strbName = "ESC";

                    break;
                case "btnF1":

                    strbName = "F1";

                    break;
                case "btnF2":

                    strbName = "F2";

                    break;
                case "btnF3":

                    strbName = "F3";

                    break;
                case "btnF4":

                    strbName = "F4";

                    break;
                case "btnF5":

                    strbName = "F5";

                    break;
                case "btnF6":

                    strbName = "F6";

                    break;
                case "btnF7":

                    strbName = "F7";

                    break;
                case "btnF8":

                    strbName = "F8";

                    break;
                case "btnF9":

                    strbName = "F9";

                    break;
                case "btnF10":

                    strbName = "F10";

                    break;
                case "btnF11":

                    strbName = "F11";

                    break;
                case "btnF12":

                    strbName = "F12";

                    break;
            }

            if (null != FuncBtnEnableChanged)
            {
                FuncBtnEnableChanged(sender, strbName);
            }

        }

        #endregion

    }
}
