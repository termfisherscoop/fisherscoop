﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

using jp.co.fsi.common.util;

namespace jp.co.fsi.common.forms
{
    /// <summary>
    /// 印刷設定画面です。
    /// </summary>
    public partial class PrintSettingForm : BaseForm
    {
        #region private変数
        /// <summary>
        /// 設定を表示する帳票IDの配列
        /// </summary>
        private string[] _repIds;

        /// <summary>
        /// 現在設定中の帳票ID
        /// </summary>
        private string _curRepId;

        /// <summary>
        /// 設定が反映されたか
        /// </summary>
        private bool _settingCommitted = false;

        /// <summary>
        /// ReportInfoクラスのインスタンス
        /// </summary>
        private ReportPrintInfo _repInfo;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="repIds">帳票IDの配列</param>
        public PrintSettingForm(string[] repIds) : base()
        {
            // 帳票IDのリストを保持
            this._repIds = repIds;
            // ReportInfoクラスのインスタンスを保持
            this._repInfo = new ReportPrintInfo();

            BindGotFocusEvent();
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] tmpAry;

            // 帳票IDのリストボックスを表示
            this.cmbRepId.Items.AddRange(this._repIds);

            // プリンタのリストを取得
            ArrayList alPrinter = new ArrayList();
            alPrinter.Add("(通常使うプリンタ)");
            foreach (string s in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                alPrinter.Add(s);
            }

            // 出力プリンタのリストを表示
            tmpAry = (string[])alPrinter.ToArray(typeof(string));
            this.cmbPrinter.Items.AddRange(tmpAry);

            // 手差しのリストを表示
            tmpAry = new string[2] { "(規定値)", "○" };
            this.cmbManual.Items.AddRange(tmpAry);

            // カラーのリストを表示
            tmpAry = new string[3] { "(規定値)", "○", "×" };
            this.cmbColor.Items.AddRange(tmpAry);

            // 両面印刷のリストを表示
            tmpAry = new string[2] { "(規定値)", "○" };
            this.cmbDuplex.Items.AddRange(tmpAry);

            // 帳票IDの1件目の設定を表示する
            this.cmbRepId.SelectedIndex = 0;
            this._curRepId = this._repIds[0];
            DispSetting(this._repIds[0]);
        }

        /// <summary>
        /// Escボタン押下時処理
        /// </summary>
        public override void PressEsc()
        {
            // 画面終了時処理を呼び出す
            this.Close();
        }

        /// <summary>
        /// F6ボタン押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (Msg.ConfOKCancel("設定を保存します。よろしいですか？") == DialogResult.Cancel)
            {
                return;
            }

            // ドキュメント名の必須入力チェック
            if (ValChk.IsEmpty(this.txtDocumentName.Text))
            {
                Msg.Notice("ドキュメント名を入力して下さい。");
                this.txtDocumentName.Focus();
                return;
            }

            // 画面入力した設定を保存する
            string repId = this.cmbRepId.SelectedItem.ToString();
            string outPrinter = string.Empty;
            if (this.cmbPrinter.SelectedIndex != 0)
            {
                outPrinter = this.cmbPrinter.SelectedItem.ToString();
            }
            string manualPaper = string.Empty;
            if (this.cmbManual.SelectedIndex != 0)
            {
                manualPaper = this.cmbManual.SelectedItem.ToString();
            }
            string colorPrint = string.Empty;
            if (this.cmbColor.SelectedIndex != 0)
            {
                colorPrint = this.cmbColor.SelectedItem.ToString();
            }
            string duplex = string.Empty;
            if (this.cmbDuplex.SelectedIndex != 0)
            {
                duplex = this.cmbDuplex.SelectedItem.ToString();
            }
            
            // 設定を保存
            this._repInfo.SetPrinterSetting(repId, this.txtDocumentName.Text, outPrinter,
                manualPaper, colorPrint, duplex);

            // CSVファイルに保存
            this._repInfo.SaveSettings();

            // 設定反映済みフラグを立てる
            this._settingCommitted = true;

            // 反映完了メッセージを表示
            Msg.Info("設定を保存しました。");
        }
        #endregion

        #region イベント
        /// <summary>
        /// フォームが閉じられる前に発生します。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintSettingForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {

            if (_settingCommitted ==false) 
            {
                e.Cancel = (Msg.ConfOKCancel("設定を終了します。よろしいですか？"
                + Environment.NewLine + "※保存していない設定は帳票出力時に反映されません。") == DialogResult.Cancel);

            }
        }

        /// <summary>
        /// 帳票IDコンボボックスの値変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRepId_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 初期表示時には実行しない
            if (this._curRepId == null) return;

            if (!this._curRepId.Equals(this.cmbRepId.SelectedItem.ToString()) && !_settingCommitted)
            {
                if (Msg.ConfOKCancel("帳票ID変更前の設定が保存されていませんが、よろしいですか？") == DialogResult.Cancel)
                {
                    for (int i = 0; i < this.cmbRepId.Items.Count; i++)
                    {
                        if (this.cmbRepId.Items[i].ToString().Equals(this._curRepId))
                        {
                            this.cmbRepId.SelectedIndex = i;
                            break;
                        }
                    }
                    return;
                }
            }

            // 現在編集中の帳票IDを保持する
            this._curRepId = this.cmbRepId.SelectedItem.ToString();

            // 設定反映済みフラグをオフにする
            this._settingCommitted = false;

            // 変更後の設定を表示する
            DispSetting(this.cmbRepId.SelectedItem.ToString());
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEsc_Click(object sender, System.EventArgs e)
        {
            PressEsc();
        }

        /// <summary>
        /// F6ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF6_Click(object sender, System.EventArgs e)
        {
            PressF6();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 設定を表示する
        /// </summary>
        /// <param name="repId">帳票ID</param>
        private void DispSetting(string repId)
        {
            // 引数に指定された印刷設定から設定情報を取得
            DataRow drSetting = this._repInfo.GetPrintSetting(repId, false);

            if (drSetting == null)
            {
                // ドキュメント名
                this.txtDocumentName.Text = string.Empty;

                // 出力プリンタ
                this.cmbPrinter.SelectedIndex = 0;

                // 手差し
                this.cmbManual.SelectedIndex = 0;

                // カラー
                this.cmbColor.SelectedIndex = 0;

                // 両面印刷
                this.cmbDuplex.SelectedIndex = 0;
            }
            else
            {
                // ドキュメント名
                this.txtDocumentName.Text = Util.ToString(drSetting["ドキュメント名"]);

                // 出力プリンタ
                bool couldFind = false;
                for (int i = 1; i < this.cmbPrinter.Items.Count; i++)
                {
                    if (this.cmbPrinter.Items[i].ToString().Equals(Util.ToString(drSetting["出力プリンタ"])))
                    {
                        this.cmbPrinter.SelectedIndex = i;
                        couldFind = true;
                        break;
                    }
                }
                if (!couldFind)
                {
                    // 指定されたプリンタが見つからない場合は通常使用するプリンタ
                    this.cmbPrinter.SelectedIndex = 0;
                }

                // 手差し
                if (Util.ToString(drSetting["手差し"]).Equals("○"))
                {
                    this.cmbManual.SelectedIndex = 1;
                }
                else
                {
                    this.cmbManual.SelectedIndex = 0;
                }

                // カラー
                if (Util.ToString(drSetting["カラー"]).Equals("○"))
                {
                    this.cmbColor.SelectedIndex = 1;
                }
                else if (Util.ToString(drSetting["カラー"]).Equals("×"))
                {
                    this.cmbColor.SelectedIndex = 2;
                }
                else
                {
                    this.cmbColor.SelectedIndex = 0;
                }

                // 両面印刷
                if (Util.ToString(drSetting["両面印刷"]).Equals("○"))
                {
                    this.cmbDuplex.SelectedIndex = 1;
                }
                else
                {
                    this.cmbDuplex.SelectedIndex = 0;
                }
            }
        }
		#endregion

		private void PrintSettingForm_Load(object sender, EventArgs e)
		{

		}
	}
}
