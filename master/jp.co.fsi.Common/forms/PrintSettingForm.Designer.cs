﻿namespace jp.co.fsi.common.forms
{
    partial class PrintSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnEsc = new System.Windows.Forms.Button();
            this.btnF6 = new System.Windows.Forms.Button();
            this.lblRepId = new System.Windows.Forms.Label();
            this.lblDocName = new System.Windows.Forms.Label();
            this.lblOutPrinter = new System.Windows.Forms.Label();
            this.cmbRepId = new System.Windows.Forms.ComboBox();
            this.lblManual = new System.Windows.Forms.Label();
            this.lblColor = new System.Windows.Forms.Label();
            this.lblDuplex = new System.Windows.Forms.Label();
            this.cmbPrinter = new System.Windows.Forms.ComboBox();
            this.cmbManual = new System.Windows.Forms.ComboBox();
            this.cmbColor = new System.Windows.Forms.ComboBox();
            this.cmbDuplex = new System.Windows.Forms.ComboBox();
            this.txtDocumentName = new System.Windows.Forms.TextBox();
            this.lblDocumentNmExp = new System.Windows.Forms.Label();
            this.lblManualExp = new System.Windows.Forms.Label();
            this.lblColorExp = new System.Windows.Forms.Label();
            this.lblDuplexExp = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.LightCyan;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(627, 31);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "プリンタ設定";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnEsc
            // 
            this.btnEsc.BackColor = System.Drawing.Color.SkyBlue;
            this.btnEsc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEsc.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEsc.ForeColor = System.Drawing.Color.Navy;
            this.btnEsc.Location = new System.Drawing.Point(16, 257);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(4);
            this.btnEsc.Name = "btnEsc";
            this.btnEsc.Size = new System.Drawing.Size(87, 60);
            this.btnEsc.TabIndex = 13;
            this.btnEsc.TabStop = false;
            this.btnEsc.Text = "Esc\r\n\r\n終了";
            this.btnEsc.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEsc.UseVisualStyleBackColor = false;
            this.btnEsc.Click += new System.EventHandler(this.btnEsc_Click);
            // 
            // btnF6
            // 
            this.btnF6.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnF6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF6.ForeColor = System.Drawing.Color.Navy;
            this.btnF6.Location = new System.Drawing.Point(111, 257);
            this.btnF6.Margin = new System.Windows.Forms.Padding(4);
            this.btnF6.Name = "btnF6";
            this.btnF6.Size = new System.Drawing.Size(87, 60);
            this.btnF6.TabIndex = 14;
            this.btnF6.TabStop = false;
            this.btnF6.Text = "F6\r\n\r\n保存";
            this.btnF6.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF6.UseVisualStyleBackColor = false;
            this.btnF6.Click += new System.EventHandler(this.btnF6_Click);
            // 
            // lblRepId
            // 
            this.lblRepId.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRepId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRepId.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblRepId.Location = new System.Drawing.Point(0, 0);
            this.lblRepId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRepId.Name = "lblRepId";
            this.lblRepId.Size = new System.Drawing.Size(606, 28);
            this.lblRepId.TabIndex = 1;
            this.lblRepId.Tag = "CHANGE";
            this.lblRepId.Text = "帳票ＩＤ";
            this.lblRepId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDocName
            // 
            this.lblDocName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDocName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDocName.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDocName.Location = new System.Drawing.Point(0, 0);
            this.lblDocName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDocName.Name = "lblDocName";
            this.lblDocName.Size = new System.Drawing.Size(606, 28);
            this.lblDocName.TabIndex = 3;
            this.lblDocName.Tag = "CHANGE";
            this.lblDocName.Text = "ドキュメント名";
            this.lblDocName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOutPrinter
            // 
            this.lblOutPrinter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblOutPrinter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOutPrinter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblOutPrinter.Location = new System.Drawing.Point(0, 0);
            this.lblOutPrinter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOutPrinter.Name = "lblOutPrinter";
            this.lblOutPrinter.Size = new System.Drawing.Size(606, 28);
            this.lblOutPrinter.TabIndex = 5;
            this.lblOutPrinter.Tag = "CHANGE";
            this.lblOutPrinter.Text = "出力プリンタ";
            this.lblOutPrinter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbRepId
            // 
            this.cmbRepId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRepId.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbRepId.FormattingEnabled = true;
            this.cmbRepId.Location = new System.Drawing.Point(129, 5);
            this.cmbRepId.Margin = new System.Windows.Forms.Padding(4);
            this.cmbRepId.Name = "cmbRepId";
            this.cmbRepId.Size = new System.Drawing.Size(160, 24);
            this.cmbRepId.TabIndex = 2;
            this.cmbRepId.SelectedIndexChanged += new System.EventHandler(this.cmbRepId_SelectedIndexChanged);
            // 
            // lblManual
            // 
            this.lblManual.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblManual.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblManual.Location = new System.Drawing.Point(0, 0);
            this.lblManual.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblManual.Name = "lblManual";
            this.lblManual.Size = new System.Drawing.Size(606, 28);
            this.lblManual.TabIndex = 7;
            this.lblManual.Tag = "CHANGE";
            this.lblManual.Text = "手差し";
            this.lblManual.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblColor
            // 
            this.lblColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblColor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblColor.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblColor.Location = new System.Drawing.Point(0, 0);
            this.lblColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(606, 28);
            this.lblColor.TabIndex = 9;
            this.lblColor.Tag = "CHANGE";
            this.lblColor.Text = "カラー印刷";
            this.lblColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDuplex
            // 
            this.lblDuplex.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDuplex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDuplex.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDuplex.Location = new System.Drawing.Point(0, 0);
            this.lblDuplex.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDuplex.Name = "lblDuplex";
            this.lblDuplex.Size = new System.Drawing.Size(606, 34);
            this.lblDuplex.TabIndex = 11;
            this.lblDuplex.Tag = "CHANGE";
            this.lblDuplex.Text = "両面印刷";
            this.lblDuplex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbPrinter
            // 
            this.cmbPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrinter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbPrinter.FormattingEnabled = true;
            this.cmbPrinter.Location = new System.Drawing.Point(131, 5);
            this.cmbPrinter.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPrinter.Name = "cmbPrinter";
            this.cmbPrinter.Size = new System.Drawing.Size(444, 24);
            this.cmbPrinter.TabIndex = 6;
            // 
            // cmbManual
            // 
            this.cmbManual.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbManual.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbManual.FormattingEnabled = true;
            this.cmbManual.Location = new System.Drawing.Point(129, 5);
            this.cmbManual.Margin = new System.Windows.Forms.Padding(4);
            this.cmbManual.Name = "cmbManual";
            this.cmbManual.Size = new System.Drawing.Size(101, 24);
            this.cmbManual.TabIndex = 8;
            // 
            // cmbColor
            // 
            this.cmbColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbColor.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbColor.FormattingEnabled = true;
            this.cmbColor.Location = new System.Drawing.Point(131, 4);
            this.cmbColor.Margin = new System.Windows.Forms.Padding(4);
            this.cmbColor.Name = "cmbColor";
            this.cmbColor.Size = new System.Drawing.Size(101, 24);
            this.cmbColor.TabIndex = 10;
            // 
            // cmbDuplex
            // 
            this.cmbDuplex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDuplex.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDuplex.FormattingEnabled = true;
            this.cmbDuplex.Location = new System.Drawing.Point(129, 4);
            this.cmbDuplex.Margin = new System.Windows.Forms.Padding(4);
            this.cmbDuplex.Name = "cmbDuplex";
            this.cmbDuplex.Size = new System.Drawing.Size(101, 24);
            this.cmbDuplex.TabIndex = 12;
            // 
            // txtDocumentName
            // 
            this.txtDocumentName.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDocumentName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtDocumentName.Location = new System.Drawing.Point(129, 4);
            this.txtDocumentName.Margin = new System.Windows.Forms.Padding(4);
            this.txtDocumentName.Name = "txtDocumentName";
            this.txtDocumentName.Size = new System.Drawing.Size(280, 23);
            this.txtDocumentName.TabIndex = 4;
            // 
            // lblDocumentNmExp
            // 
            this.lblDocumentNmExp.AutoSize = true;
            this.lblDocumentNmExp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDocumentNmExp.ForeColor = System.Drawing.Color.Red;
            this.lblDocumentNmExp.Location = new System.Drawing.Point(417, 7);
            this.lblDocumentNmExp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDocumentNmExp.Name = "lblDocumentNmExp";
            this.lblDocumentNmExp.Size = new System.Drawing.Size(56, 16);
            this.lblDocumentNmExp.TabIndex = 15;
            this.lblDocumentNmExp.Text = "(必須)";
            // 
            // lblManualExp
            // 
            this.lblManualExp.AutoSize = true;
            this.lblManualExp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblManualExp.Location = new System.Drawing.Point(259, 8);
            this.lblManualExp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblManualExp.Name = "lblManualExp";
            this.lblManualExp.Size = new System.Drawing.Size(104, 16);
            this.lblManualExp.TabIndex = 16;
            this.lblManualExp.Tag = "CHANGE";
            this.lblManualExp.Text = "(○：手差し)";
            // 
            // lblColorExp
            // 
            this.lblColorExp.AutoSize = true;
            this.lblColorExp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblColorExp.Location = new System.Drawing.Point(259, 7);
            this.lblColorExp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblColorExp.Name = "lblColorExp";
            this.lblColorExp.Size = new System.Drawing.Size(184, 16);
            this.lblColorExp.TabIndex = 17;
            this.lblColorExp.Tag = "CHANGE";
            this.lblColorExp.Text = "(○：カラー、×：白黒)";
            // 
            // lblDuplexExp
            // 
            this.lblDuplexExp.AutoSize = true;
            this.lblDuplexExp.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDuplexExp.Location = new System.Drawing.Point(259, 8);
            this.lblDuplexExp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDuplexExp.Name = "lblDuplexExp";
            this.lblDuplexExp.Size = new System.Drawing.Size(157, 19);
            this.lblDuplexExp.TabIndex = 18;
            this.lblDuplexExp.Tag = "CHANGE";
            this.lblDuplexExp.Text = "(○：両面印刷)";
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(6, 36);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 6;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(614, 217);
            this.fsiTableLayoutPanel1.TabIndex = 19;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.cmbDuplex);
            this.fsiPanel6.Controls.Add(this.lblDuplexExp);
            this.fsiPanel6.Controls.Add(this.lblDuplex);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 179);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(606, 34);
            this.fsiPanel6.TabIndex = 5;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.cmbColor);
            this.fsiPanel5.Controls.Add(this.lblColorExp);
            this.fsiPanel5.Controls.Add(this.lblColor);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 144);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(606, 28);
            this.fsiPanel5.TabIndex = 4;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.cmbManual);
            this.fsiPanel4.Controls.Add(this.lblManualExp);
            this.fsiPanel4.Controls.Add(this.lblManual);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 109);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(606, 28);
            this.fsiPanel4.TabIndex = 3;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.cmbPrinter);
            this.fsiPanel3.Controls.Add(this.lblOutPrinter);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 74);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(606, 28);
            this.fsiPanel3.TabIndex = 2;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtDocumentName);
            this.fsiPanel2.Controls.Add(this.lblDocumentNmExp);
            this.fsiPanel2.Controls.Add(this.lblDocName);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 39);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(606, 28);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.cmbRepId);
            this.fsiPanel1.Controls.Add(this.lblRepId);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(606, 28);
            this.fsiPanel1.TabIndex = 0;
            // 
            // PrintSettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(627, 323);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.btnF6);
            this.Controls.Add(this.btnEsc);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.Name = "PrintSettingForm";
            this.Text = "プリンタ設定";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PrintSettingForm_FormClosing);
            this.Load += new System.EventHandler(this.PrintSettingForm_Load);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label lblTitle;
        public System.Windows.Forms.Button btnEsc;
        public System.Windows.Forms.Button btnF6;
        private System.Windows.Forms.Label lblRepId;
        private System.Windows.Forms.Label lblDocName;
        private System.Windows.Forms.Label lblOutPrinter;
        private System.Windows.Forms.ComboBox cmbRepId;
        private System.Windows.Forms.Label lblManual;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label lblDuplex;
        private System.Windows.Forms.ComboBox cmbPrinter;
        private System.Windows.Forms.ComboBox cmbManual;
        private System.Windows.Forms.ComboBox cmbColor;
        private System.Windows.Forms.ComboBox cmbDuplex;
        private System.Windows.Forms.TextBox txtDocumentName;
        private System.Windows.Forms.Label lblDocumentNmExp;
        private System.Windows.Forms.Label lblManualExp;
        private System.Windows.Forms.Label lblColorExp;
        private System.Windows.Forms.Label lblDuplexExp;
		private FsiTableLayoutPanel fsiTableLayoutPanel1;
		private FsiPanel fsiPanel6;
		private FsiPanel fsiPanel5;
		private FsiPanel fsiPanel4;
		private FsiPanel fsiPanel3;
		private FsiPanel fsiPanel2;
		private FsiPanel fsiPanel1;
	}
}