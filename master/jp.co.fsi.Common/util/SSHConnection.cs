﻿using jp.co.fsi.common.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Renci.SshNet;

namespace jp.co.fsi.common
{
    public static class SSHConnection
    {

        public static SshClient ssh = null;

        public static string SSHConnect()
        {

            string messages = string.Empty;

            ConfigLoader config = new ConfigLoader();

            string strSSHHost = config.LoadSSHConfig("SSH", "SSHHost");
            string strSSHPort = config.LoadSSHConfig("SSH", "SSHPort");
            string strSSHLocalHost = config.LoadSSHConfig("SSH", "SSHLocalHost");
            string strSSHLocalPort = config.LoadSSHConfig("SSH", "SSHLocalPort");
            string strSSHFwdPort = config.LoadSSHConfig("SSH", "SSHFwdPort");
            string strSSHUser = config.LoadSSHConfig("SSH", "SSHUser");
            string strSSHPassword = config.LoadSSHConfig("SSH", "SSHPassword");

            try
            {

                ConnectionInfo info = new ConnectionInfo(
                    strSSHHost,
                    int.Parse(strSSHPort),
                    strSSHUser,
                    new AuthenticationMethod[]
                    {
                        new PasswordAuthenticationMethod(
                        strSSHUser,
                        strSSHPassword)
                    }
                    );

                ssh = new SshClient(info);

                ssh.Connect();

                if (!ssh.IsConnected)
                {
                    messages = "SSH接続されていません。";
                }

                // ポート転送設定追加
                ForwardedPortLocal forwardSql =
                    new ForwardedPortLocal(
                        strSSHLocalHost, 
                        uint.Parse(strSSHLocalPort), 
                        strSSHHost, 
                        uint.Parse(strSSHFwdPort)); // SQLServer用

                ssh.AddForwardedPort(forwardSql);

                forwardSql.Start();


            }
            catch (Exception ex)
            {
                messages = "SSH接続時にエラーが発生しました。システム管理者へ問い合わせてください。"+Environment.NewLine + ex.Message;
                ssh.Dispose();

            }

            return messages;

        }

        public static string SSHDisConnect()
        {

            string messages = string.Empty;

            try
            {

                ssh.Disconnect();

                ssh.Dispose();

            }
            catch (Exception ex)
            {
                messages = "SSH切断時にエラーが発生しました。" + Environment.NewLine + ex.Message;
            }

            return messages;

        }


    }
}
