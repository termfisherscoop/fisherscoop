﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.util;
using jp.co.fsi.common.userinfo;
using System.Security.Cryptography;
using System.Net;

namespace jp.co.fsi.common.riyoshalog
{
    /// <summary>
    /// ユーティリティクラス
    /// </summary>
    public static class Riyoshalog
    {
        /// <summary>
        /// ログ取得（TB_CM_RIYOSHA_LOGにINSERT）
        /// </summary>
        /// <param name="kaisha_cd">会社コード</param>
        /// <param name="kubun">区分</param>
        /// <param name="txtdata">実行したSQL文</param>
        /// <param name="Dba">データアクセス</param>
        public static void Insertlog(int kaisha_cd, int kubun, string txtdata, DbAccess Dba)
        {
            #region 準備
            string HostName = "";     // ホスト名
            string IpAdress = "";     // IPアドレス
            DateTime registdate = DateTime.Now; // 実行日付;

            ConfigLoader cLoader = new ConfigLoader();
            // 会社コード取得
            int tantoshaCd = Util.ToInt(cLoader.LoadCommonConfig("UserInfo", "usercd"));
            #endregion

            // ホスト名を取得する
            HostName = Dns.GetHostName();

            // ホスト名からIPアドレスを取得する
            IPAddress[] adrList = Dns.GetHostAddresses(HostName);
            foreach (IPAddress address in adrList)
            {
                IpAdress = address.ToString();
            }
            #region INSERTSQL
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append(" INSERT INTO TB_CM_RIYOSHA_LOG (");
            sql.Append(" KAISHA_CD,");
            sql.Append(" HOST_NAME,");
            sql.Append(" IP_ADRESS,");
            sql.Append(" TANTOSHA_CD,");
            sql.Append(" SQL,");
            sql.Append(" KUBUN,");
            sql.Append(" REGIST_DATE");
            sql.Append(" )");
            sql.Append(" VALUES (");
            sql.Append(" @KAISHA_CD,");
            sql.Append(" @HOST_NAME,");
            sql.Append(" @IP_ADRESS,");
            sql.Append(" @TANTOSHA_CD,");
            sql.Append(" @SQL,");
            sql.Append(" @KUBUN,");
            sql.Append(" @REGIST_DATE");
            sql.Append(" )");
            #endregion

            #region 登録パラメータの設定
            dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, kaisha_cd);
            dpc.SetParam("@HOST_NAME", SqlDbType.VarChar, 50, HostName);// ホスト名
            dpc.SetParam("@IP_ADRESS", SqlDbType.VarChar, 50, IpAdress);// IPアドレス
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Int, 4, tantoshaCd);// 担当者コード
            dpc.SetParam("@SQL", SqlDbType.VarChar, 200, txtdata); // 実行SQL文
            dpc.SetParam("@KUBUN", SqlDbType.Int, 4, kubun); //区分
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, registdate);
            #endregion

            Dba.ModifyBySql(Util.ToString(sql), dpc);
        }
    }
}
