﻿using System;
using System.Text.RegularExpressions;

namespace jp.co.fsi.common.util
{
    /// <summary>
    /// 値チェッカークラス
    /// </summary>
    public static class IsValid
    {
        #region publicメソッド
        /// <summary>
        /// 日付(年)の入力チェック
        /// </summary>
        /// <param name="val">判定する値</param>
        /// <param name="length">最大入力文字数</param>
        /// <returns>true:空、false:空でない</returns>
        public static bool IsYear(object val, int length)
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(val, length))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 数字チェック
            if (!ValChk.IsNumber(val))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            return true;
        }
        public static int SetYear(object val)
        {
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(val))
            {
                return 0;
            }

            return Util.ToInt(val);
        }

        /// <summary>
        /// 日付(月)の入力チェック
        /// </summary>
        /// <param name="val">判定する値</param>
        /// <param name="length">最大入力文字数</param>
        /// <returns>true:半角、false:半角でない</returns>
        public static bool IsMonth(object val, int length)
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(val, length))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 数字チェック
            if (!ValChk.IsNumber(val))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            return true;
        }
        public static int SetMonth(object val)
        {
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(val))
            {
                return 1;
            }
            int month = Util.ToInt(val);
            // 12を超える月が入力された場合、12月として処理
            if (12 < month)
            {
                return 12;
            }
            // 1未満場合、1月として処理
            else if (month < 1)
            {
                return 1;
            }

            return Util.ToInt(val);
        }

        /// <summary>
        /// 日付(日)の入力チェック
        /// </summary>
        /// <param name="val">判定する値</param>
        /// <param name="length">最大入力文字数</param>
        /// <returns>true:半角、false:半角でない</returns>
        public static bool IsDay(object val, int length)
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(val, length))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 数値チェック
            if (!ValChk.IsNumber(val))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            return true;
        }
        public static int SetDay(object val)
        {
            // 空の場合は、1日として処理
            if (ValChk.IsEmpty(val))
            {
                return 1;
            }
            // 1より小さい日が入力された場合、1日として処理
            if (Util.ToInt(val) < 1)
            {
                return 1;
            }

            return Util.ToInt(val);
        }

        /// <summary>
        /// 支所CDの入力チェック
        /// </summary>
        /// <param name="val">判定する値</param>
        /// <param name="length">最大入力文字数</param>
        /// <returns>true:半角、false:半角でない</returns>
        public static bool IsValidShishoCd(object val, object name, int length)
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(val, length))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 数値チェック
            if (!ValChk.IsNumber(val))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            return true;
        }
        #endregion
    }
}
