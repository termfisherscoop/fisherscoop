﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.fsi.common
{
	public enum Sort
	{
		Asc,
		Desc
	}

	public class SortHelperOfHierarchicalTabIndices : IComparer<IHasHierarchicalTabIndices>
	{

        private int togleNum = 1;
        public SortHelperOfHierarchicalTabIndices() { }
        public SortHelperOfHierarchicalTabIndices(Sort sort)
        {
            switch (sort)
            {
                case Sort.Asc:
                    break;
                case Sort.Desc:
                    togleNum = -1;
                    break;
                default:
                    togleNum = 1;
                    break;
            }
        }

        private int Compare(IntPtr hwdx, IntPtr hwdy)
        {
            var h = PlatformInvoker.GetWindow(hwdx, (uint)PlatformInvoker.GetWindowCmd.GW_HWNDNEXT);
            while (h != default(IntPtr))
            {
                if (h == hwdy) return -1 * togleNum;
                h = PlatformInvoker.GetWindow(h, (uint)PlatformInvoker.GetWindowCmd.GW_HWNDNEXT);
            }

            h = PlatformInvoker.GetWindow(hwdx, (uint)PlatformInvoker.GetWindowCmd.GW_HWNDPREV);
            while (h != default(IntPtr))
            {
                if (h == hwdy) return 1 * togleNum;
                h = PlatformInvoker.GetWindow(h, (uint)PlatformInvoker.GetWindowCmd.GW_HWNDPREV);
            }
            return 0;
        }

        public int Compare(IHasHierarchicalTabIndices x, IHasHierarchicalTabIndices y)
		{
            using (IEnumerator<int> enumerator1 = x.GetEnumerator())
            using (IEnumerator<int> enumerator2 = y.GetEnumerator())
            {
                bool e1 = enumerator1.MoveNext();
                bool e2 = enumerator2.MoveNext();

                while (e1 && e2)
                {
                    int compare = enumerator1.Current.CompareTo(enumerator2.Current) * togleNum;
                    if (compare != 0) return compare;

                    e1 = enumerator1.MoveNext();
                    e2 = enumerator2.MoveNext();
                }
                if (!e1 && !e2) return Compare(x.Handle, y.Handle);
                if (!e1) return -1 * togleNum;
                if (!e2) return 1 * togleNum;
            }

            return 0;
        }
	}
}
