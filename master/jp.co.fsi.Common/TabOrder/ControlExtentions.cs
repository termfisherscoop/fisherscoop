﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jp.co.fsi.common
{
	public static class ControlExtentions
	{

        public static IEnumerable<Control> GetAllControls(this Control self)
        {
            foreach (Control c in self.Controls)
            {
                yield return c;
                foreach (Control a in GetAllControls(c))
                    yield return a;
            }
        }

        public static IEnumerable<int> GetHierarchicalTabindices(this Control self)
        {
            var s = new Stack<int>();
            s.Push(self.TabIndex);
            var parent = self.Parent;
            while (CheckParent(parent))
            {
                s.Push(parent.TabIndex);
                parent = parent.Parent;
            }

            while (s.Count != 0)
                yield return s.Pop();
        }

        public static string GetHierarchicalTabIndicesString(this Control self)
        {
            var sb = new StringBuilder();
            foreach (var item in self.GetHierarchicalTabindices())
                sb.AppendFormat("{0},", item.ToString());
            return Regex.Replace(sb.ToString(), ",$", "");
        }

        private static bool CheckParent(Control target)
        {
            //このあたりの実装は、特定のインターフェイスによって
            //判定するようにしたほうがよいかもしれません、今回はとりあえずこれで
            if (target == null) return false;
            if (target is Form) return false;
            return true;
        }
    }
}
