﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Upload : System.Web.UI.Page
{
    readonly log4net.ILog log =
     log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    protected void Page_Load(object sender, EventArgs e)
    {
        // 格納先情報を取得
        string strFilePath = Request["FILE_PATH"];

        // アップロードファイルを取得
        HttpPostedFile posted = Request.Files[0];

        try
        {
            // ファイルの存在チェック
            if (new System.IO.FileInfo(strFilePath).Exists)
            {
                // 存在する場合削除する
                System.IO.File.Delete(strFilePath);
            }

            // APサーバ上にファイルを保存
            posted.SaveAs(strFilePath);

            // アップしたファイルのサイズを表示
            Response.Write(posted.ContentLength);

        }
        catch (Exception ex)
        {
            log.Error(ex.Message);
            log.Error(ex.StackTrace);

        }


    }
}