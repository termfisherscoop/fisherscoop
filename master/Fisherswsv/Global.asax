﻿<%@ Application Language="C#" %>
<%@ Import namespace="log4net" %>
<%@ Import namespace="log4net.Config" %>
<%@ Import namespace="log4net.Util" %>
<%@ Import namespace="MINLogics" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // アプリケーションのスタートアップで実行するコードです
        log4net.Config.XmlConfigurator.Configure();
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  アプリケーションのシャットダウンで実行するコードです
        Exception objErr = Server.GetLastError().InnerException;
    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // ハンドルされていないエラーが発生したときに実行するコードです
        Exception objErr = Server.GetLastError().InnerException;
    }

    void Session_Start(object sender, EventArgs e) 
    {
        string ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        // log4net MDC登録
        //   初期表示はIPアドレスを設定
        log4net.MDC.Set("UserId", ip);
        // APP変数へログインユーザIDを保持
        string userId = Application[ip] as string;
        if (userId != null)
        {
            // log4net MDC登録
            //   ログインユーザが設定されている場合、ユーザIDを設定
            log4net.MDC.Set("UserId", userId);
        }
    }

    void Session_End(object sender, EventArgs e) 
    {
        // セッションが終了したときに実行するコードです 
        // メモ: Web.config ファイル内で sessionstate モードが InProc に設定されているときのみ、
        // Session_End イベントが発生します。session モードが StateServer か、または SQLServer に 
        // 設定されている場合、イベントは発生しません。

    }
       
</script>
