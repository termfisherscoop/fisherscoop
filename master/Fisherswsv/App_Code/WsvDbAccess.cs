﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;


/// <summary>
/// WsvDbAccess の概要の説明です
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// この Web サービスを、スクリプトから ASP.NET AJAX を使用して呼び出せるようにするには、次の行のコメントを解除します。
// [System.Web.Script.Services.ScriptService]
public class WsvDbAccess : System.Web.Services.WebService
{

    public WsvDbAccess()
    {

        //デザインされたコンポーネントを使用する場合、次の行のコメントを解除してください 
        //InitializeComponent(); 
    }

    [WebMethod]
    public DataTable GetDataTableByCondition(string cols, string table)
    {

        DbAccess dbs = new DbAccess();
        return dbs.GetDataTableByCondition(cols, table);

    }

    public DataTable GetDataTableByCondition(string cols, string table, string where)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetDataTableByCondition(cols, table, where);
    }
    public DataTable GetDataTableByCondition(string cols, string table, string where, string order)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetDataTableByCondition(cols, table, where, order);
    }
    public DataTable GetDataTableByCondition(string cols, string table, string where, string order, string group)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetDataTableByCondition(cols, table, where, order, group);
    }
    public DataTable GetDataTableByConditionWithParams(string cols, string table, string where, DbParamCollection dpc)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetDataTableByConditionWithParams(cols, table, where, dpc);

    }
    public DataTable GetDataTableByConditionWithParams(string cols, string table, string where, string order, DbParamCollection dpc)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetDataTableByConditionWithParams(cols, table, where, order, dpc);
    }
    public DataTable GetDataTableByConditionWithParams(string cols, string table, string where, string order, string group, DbParamCollection dpc)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetDataTableByConditionWithParams(cols, table, where, order, group, dpc);
    }
    public DataTable GetDataTableFromSql(string sql)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetDataTableFromSql(sql);
    }

    public DataTable GetDataTableFromSqlWithParams(string sql, DbParamCollection dpc)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetDataTableFromSqlWithParams(sql, dpc);

    }
    public int Insert(string table, DbParamCollection dpc)
    {

        DbAccess dbs = new DbAccess();
        return dbs.Insert(table, dpc);

    }
    public int Update(string table, DbParamCollection setDpc, string where, DbParamCollection whereDpc)
    {
        DbAccess dbs = new DbAccess();
        return dbs.Update(table, setDpc, where, whereDpc);
    }
    public int Delete(string table, string where, DbParamCollection whereDpc)
    {
        DbAccess dbs = new DbAccess();
        return dbs.Delete(table, where, whereDpc);
    }

    public int ModifyBySql(string sql, DbParamCollection dpc)
    {
        DbAccess dbs = new DbAccess();
        return dbs.ModifyBySql(sql, dpc);

    }
    public DataTable GetCdNmList(UserInfo uInfo, string tableNm, string sCode)
    {

        DbAccess dbs = new DbAccess();
        return dbs.GetCdNmList(uInfo, tableNm, sCode);
    }
    public string GetName(UserInfo uInfo, string tableNm, string scode, string code)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetName(uInfo, tableNm, scode, code);
    }

    public string GetName(UserInfo uInfo, string tableNm, string scode, string code, decimal kbn = -1)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetName(uInfo, tableNm, scode, code, kbn = -1);
    }
    public string GetHojoKmkNm(UserInfo uInfo, decimal shishoCd, string kanjoKmkCd, string code)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetHojoKmkNm(uInfo, shishoCd, kanjoKmkCd, code);
    }

    public string GetBukaNm(UserInfo uInfo, string bumonCd, string code)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetBukaNm(uInfo, bumonCd, code);
    }
    public string GetShiwakeNm(UserInfo uInfo, decimal shishoCd, string tekiyoCd, string shiwakeCd)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetShiwakeNm(uInfo, shishoCd, tekiyoCd, shiwakeCd);
    }
    public string GetGinkoShitenNm(string ginkoCd, string shitenCd)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetGinkoShitenNm(ginkoCd, shitenCd);

    }
    public string GetKyuyoKbnNm(string shubetsuCd, string kubun)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetKyuyoKbnNm(shubetsuCd, kubun);
    }

    public string GetTanabanNm(UserInfo uInfo, string code)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetTanabanNm(uInfo, code);
    }
    public DataTable GetKaikeiSettingsByDate(string kaishaCd, DateTime tgtDate)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetKaikeiSettingsByDate(kaishaCd, tgtDate);
    }
    public DataTable GetKaikeiSettingsByKessanKi(string kaishaCd, int kessanKi)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetKaikeiSettingsByKessanKi(kaishaCd, kessanKi);
    }
    public int GetHNDenpyoNo(UserInfo uInfo, decimal shishoCd, int denpyoKbn1, int denpyoKbn2)
    {
        DbAccess dbs = new DbAccess();
        return dbs.GetHNDenpyoNo(uInfo, shishoCd, denpyoKbn1, denpyoKbn2);
    }

    public int UpdateHNDenpyoNo(UserInfo uInfo, decimal shishoCd, int denpyoKbn1, int denpyoKbn2, int denpyoNo)
    {
        DbAccess dbs = new DbAccess();
        return dbs.UpdateHNDenpyoNo(uInfo, shishoCd, denpyoKbn1, denpyoKbn2, denpyoNo);
    }

    public int UpdateZMDenpyoNo(UserInfo uInfo, decimal shishoCd, int denpyoKbn, int denpyoNo)
    {
        DbAccess dbs = new DbAccess();
        return dbs.UpdateZMDenpyoNo(uInfo, shishoCd, denpyoKbn, denpyoNo);
    }
    public void DeleteWork(string tbNm, string GUID)
    {
        DbAccess dbs = new DbAccess();
        dbs.DeleteWork(tbNm, GUID);
    }

    #region プライベート

    private string[] GetMstCondition(UserInfo uInfo, string tableNm, ref DbParamCollection dpc,
        bool isToGetName, string scode, string code)
    {
        return GetMstCondition(uInfo, tableNm, ref dpc, isToGetName, scode, code, -1);
    }
    private string[] GetMstCondition(UserInfo uInfo, string tableNm, ref DbParamCollection dpc,
        bool isToGetName, string scode, string code, decimal kbn = -1)
    {
        string[] condition = new string[7];

        // 引数に指定するテーブル名と実際のテーブル名が異なるケースがあるため、
        // 結果の配列として返す(大抵のケースは一致している)
        condition[6] = tableNm;

        switch (tableNm)
        {
            #region "共通マスタ"
            case "TB_CM_SHISHO":    //Y.TAKADA 2017/10/03 ADD↓
                condition[0] = "SHISHO_CD";
                condition[1] = "支所コード";
                condition[2] = "SHISHO_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;
            //Y.TAKADA 2017/10/03 ADD↑

            case "TB_CM_BUMON":
                condition[0] = "BUMON_CD";
                condition[1] = "コード";
                condition[2] = "BUMON_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_CM_TANTOSHA":
                condition[0] = "TANTOSHA_CD";
                condition[1] = "コード";
                condition[2] = "TANTOSHA_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_CM_TORIHIKISAKI":
                condition[0] = "TORIHIKISAKI_CD";
                condition[1] = "コード";
                condition[2] = "TORIHIKISAKI_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;
            #endregion
            #region "セリ・購買テーブル"
            case "TB_HN_GYOHO_MST":
                condition[0] = "GYOHO_CD";
                condition[1] = "コード";
                condition[2] = "GYOHO_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_HN_CHIKU_MST":
                condition[0] = "CHIKU_CD";
                condition[1] = "コード";
                condition[2] = "CHIKU_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_HN_TEKIYO":
                condition[0] = "TEKIYO_CD";
                condition[1] = "コード";
                condition[2] = "TEKIYO_NM";
                condition[3] = "名称";
                //Y.TAKADA EDIT 20171003 ↓
                //condition[4] = "KAISHA_CD = @KAISHA_CD";
                condition[4] = "KAISHA_CD = @KAISHA_CD AND ";
                condition[4] += "SHISHO_CD = @SHISHO_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                //Y.TAKADA EDIT 20171003 ↑
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_HN_F_TORIHIKI_KUBUN1":
                condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                condition[1] = "区分";
                condition[2] = "TORIHIKI_KUBUN_NM";
                condition[3] = "名称";
                condition[4] = "DENPYO_KUBUN = 1";
                if (isToGetName)
                {
                    condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                break;

            case "TB_HN_F_TORIHIKI_KUBUN2":
                condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                condition[1] = "区分";
                condition[2] = "TORIHIKI_KUBUN_NM";
                condition[3] = "名称";
                condition[4] = "DENPYO_KUBUN = 2";
                if (isToGetName)
                {
                    condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                break;

            case "TB_HN_F_TORIHIKI_KUBUN3":
                condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                condition[1] = "区分";
                condition[2] = "TORIHIKI_KUBUN_NM";
                condition[3] = "名称";
                condition[4] = "DENPYO_KUBUN = 3";
                if (isToGetName)
                {
                    condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                break;

            case "TB_HN_F_TORIHIKI_KUBUN4":
                condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                condition[1] = "区分";
                condition[2] = "TORIHIKI_KUBUN_NM";
                condition[3] = "名称";
                condition[4] = "DENPYO_KUBUN = 4";
                if (isToGetName)
                {
                    condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                break;

            case "TB_HN_F_TORIHIKI_KUBUN5":
                condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                condition[1] = "区分";
                condition[2] = "TORIHIKI_KUBUN_NM";
                condition[3] = "名称";
                condition[4] = "DENPYO_KUBUN = 5";
                if (isToGetName)
                {
                    condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                break;

            case "TB_HN_F_TORIHIKI_KUBUN7":
                condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                condition[1] = "区分";
                condition[2] = "TORIHIKI_KUBUN_NM";
                condition[3] = "名称";
                condition[4] = "DENPYO_KUBUN = 7";
                if (isToGetName)
                {
                    condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                break;

            case "TB_HN_F_TORIHIKI_KUBUN8":
                condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                condition[1] = "区分";
                condition[2] = "TORIHIKI_KUBUN_NM";
                condition[3] = "名称";
                condition[4] = "DENPYO_KUBUN = 8";
                if (isToGetName)
                {
                    condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                break;

            case "TB_HN_SHOHIN":
                condition[0] = "SHOHIN_CD";
                condition[1] = "コード";
                condition[2] = "SHOHIN_NM";
                condition[3] = "名称";
                //Y.TAKADA EDIT 20171003 ↓
                //condition[4] = "KAISHA_CD = @KAISHA_CD";
                //dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                condition[4] = "KAISHA_CD = @KAISHA_CD AND ";
                condition[4] += "SHISHO_CD = @SHISHO_CD  ";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                //Y.TAKADA EDIT 20171003 ↑
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                }
                condition[5] = condition[0];
                break;
            case "TB_HN_COMBO_DATA_SEISAN":
                condition[0] = "CD";
                condition[1] = "区分";
                condition[2] = "NM";
                condition[3] = "名称";
                condition[4] = "CD >= 1 AND KUBUN = 24";
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_COMBO_DATA";
                break;

            case "TB_HN_COMBO_DATA_MIZUAGE":
                condition[0] = "CD";
                condition[1] = "区分";
                condition[2] = "NM";
                condition[3] = "名称";
                condition[4] = "CD >= 1 AND KUBUN = 25";
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_COMBO_DATA";
                break;

            case "TB_HN_TESURYO_RITSU_MST_NIUKE":
                condition[0] = "URIBA_CD";
                condition[1] = "コード";
                condition[2] = "URIBA_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD AND SEISAN_KUBUN = 2";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                //condition[6] = "TB_HN_TESURYO_RITSU_MST";
                condition[6] = "TB_HN_TESURYO_RITSU_MST";
                break;
            case "TB_HN_FUNE_NM_MST":
                condition[0] = "FUNE_NM_CD";
                condition[1] = "コード";
                condition[2] = "FUNE_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                }
                condition[5] = condition[0];
                condition[6] = "TB_HN_FUNE_NM_MST";
                break;

            #endregion
            #region "セリ・購買View"
            case "VI_HN_FUNANUSHI":
            case "VI_HN_NAKAGAI":
            case "VI_HN_SHIIRESK":
            //case "VI_HN_TORIHIKISAKI_JOHO":
            case "VI_HN_TOKUISAKI":
                condition[0] = "TORIHIKISAKI_CD";
                condition[1] = "コード";
                condition[2] = "TORIHIKISAKI_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;
            case "VI_HN_TORIHIKISAKI_JOHO":
                condition[0] = "TORIHIKISAKI_CD";
                condition[1] = "コード";
                condition[2] = "TORIHIKISAKI_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETU_KUBUN = @SHUBETU_KUBUN";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                dpc.SetParam("@SHUBETU_KUBUN", SqlDbType.Decimal, 4, kbn);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "VI_HN_URIAGE_SHIWAKE":
            case "VI_HN_SHIIRE_SHIWAKE":
            case "VI_HN_SHOKUDO_URIAGE":
            case "VI_HN_SHOKUDO_SHIIRE":
                condition[0] = "SHIWAKE_CD";
                condition[1] = "コード";
                condition[2] = "SHIWAKE_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "VI_HN_SHOHIN_KBN1":
            case "VI_HN_SHOHIN_KBN2":
            case "VI_HN_SHOHIN_KBN3":
            case "VI_HN_SHOHIN_KBN4":
            case "VI_HN_SHOHIN_KBN5":
            // 20150105 kinjo-Add:名護版カスタマイズ↓
            case "VI_HN_SHOHIN_KBN6":
            case "VI_HN_SHOHIN_KBN7":
            case "VI_HN_SHOHIN_KBN8":
                // 20150105 kinjo-Add:名護版カスタマイズ↑

                condition[0] = "SHOHIN_KUBUN";
                condition[1] = "区分";
                condition[2] = "SHOHIN_KUBUN_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;
            case "VI_HN_SHOHIN":
                condition[0] = "SHOHIN_CD";
                condition[1] = "コード";
                condition[2] = "SHOHIN_NM";
                condition[3] = "名称";
                //Y.TAKADA EDIT 20171003 ↓
                //condition[4] = "KAISHA_CD = @KAISHA_CD AND BARCODE1 = 0";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                condition[4] += " AND SHISHO_CD = @SHISHO_CD ";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                //Y.TAKADA EDIT 20171003 ↑
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                }
                condition[5] = condition[0];
                break;

            case "VI_HN_SHOHIN2":
                condition[0] = "SHOHIN_CD";
                condition[1] = "コード";
                condition[2] = "SHOHIN_NM";
                condition[3] = "名称";
                //Y.TAKADA EDIT 20171003 ↓
                //condition[4] = "KAISHA_CD = @KAISHA_CD AND SHOHIN_KUBUN5 <> 1 AND (BARCODE1 <> 999 AND BARCODE1 <> 888)";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                condition[4] += " AND SHISHO_CD = @SHISHO_CD ";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                }
                condition[5] = condition[0];
                condition[6] = "VI_HN_SHOHIN";
                break;

            case "VI_HN_SHOHIN_SHIIRE":
                condition[0] = "SHOHIN_CD";
                condition[1] = "コード";
                condition[2] = "SHOHIN_NM";
                condition[3] = "名称";
                //condition[4] = "KAISHA_CD = @KAISHA_CD AND (BARCODE1 IS NULL OR BARCODE1 != 999)";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                condition[4] += " AND SHISHO_CD = @SHISHO_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                }
                condition[5] = condition[0];
                condition[6] = "VI_HN_SHOHIN";
                break;

            case "VI_HN_GYOSHU":
                condition[0] = "GYOSHU_CD";
                condition[1] = "コード";
                condition[2] = "GYOSHU_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                condition[4] += " AND SHISHO_CD = @SHISHO_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                condition[6] = "VI_HN_GYOSHU";
                break;
            case "VI_HN_HANBAI_KUBUN_NM":
                condition[0] = "KUBUN";
                condition[1] = "区分";
                condition[2] = "NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 2";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                }
                condition[5] = condition[0];
                condition[6] = "VI_HN_HANBAI_KUBUN_NM";
                break;

            case "VI_HN_HANBAI_KUBUN_NM2":
                condition[0] = "KUBUN";
                condition[1] = "区分";
                condition[2] = "NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 1";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                }
                condition[5] = condition[0];
                condition[6] = "VI_HN_HANBAI_KUBUN_NM";
                break;

            case "VI_HN_HANBAI_KUBUN_NM3":
                condition[0] = "KUBUN";
                condition[1] = "区分";
                condition[2] = "NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 1 AND KUBUN_SHUBETSU = 7";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                }
                condition[5] = condition[0];
                condition[6] = "VI_HN_HANBAI_KUBUN_NM";
                break;

            case "VI_HN_HANBAI_KUBUN_NM4":
                condition[0] = "KUBUN";
                condition[1] = "区分";
                condition[2] = "NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 7";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                }
                condition[5] = condition[0];
                condition[6] = "VI_HN_HANBAI_KUBUN_NM";
                break;


            #endregion
            #region "財務"
            case "TB_ZM_KANJO_KAMOKU":
                condition[0] = "KANJO_KAMOKU_CD";
                condition[1] = "コード";
                condition[2] = "KANJO_KAMOKU_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 6, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_ZM_F_ZEI_KUBUN":
                condition[0] = "ZEI_KUBUN";
                condition[1] = "区分";
                condition[2] = "ZEI_KUBUN_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 2, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_ZM_F_JIGYO_KUBUN":
                condition[0] = "JIGYO_KUBUN";
                condition[1] = "区分";
                condition[2] = "JIGYO_KUBUN_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO":
                condition[0] = "SHOHIZEI_NYURYOKU_HOHO";
                condition[1] = "区分";
                condition[2] = "SHOHIZEI_NYURYOKU_HOHO_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_ZM_F_KAZEI_HOHO":
                condition[0] = "KAZEI_HOHO";
                condition[1] = "区分";
                condition[2] = "KAZEI_HOHO_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_ZM_F_KOJO_HOHO":
                condition[0] = "KOJO_HOHO";
                condition[1] = "区分";
                condition[2] = "KOJO_HOHO_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_ZM_F_TAISHAKU_KUBUN":
                condition[0] = "TAISHAKU_KUBUN";
                condition[1] = "区分";
                condition[2] = "TAISHAKU_KUBUN_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_ZM_F_HASU_SHORI":
                condition[0] = "HASU_SHORI";
                condition[1] = "区分";
                condition[2] = "HASU_SHORI_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_ZM_F_KAMOKU_KUBUN":
                condition[0] = "KAMOKU_KUBUN";
                condition[1] = "区分";
                condition[2] = "KAMOKU_KUBUN_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;
            case "TB_ZM_TEKIYO":
                condition[0] = "TEKIYO_CD";
                condition[1] = "コード";
                condition[2] = "TEKIYO_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                condition[4] += " AND SHISHO_CD = @SHISHO_CD";
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            //case "TB_ZM_KOJI":
            //    condition[0] = "KOJI_CD";
            //    condition[1] = "コード";
            //    condition[2] = "KOJI_NM";
            //    condition[3] = "名称";
            //    condition[4] = "KAISHA_CD = @KAISHA_CD";
            //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
            //    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
            //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
            //    if (isToGetName)
            //    {
            //        condition[4] += " AND " + condition[0] + " = @" + condition[0];
            //        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
            //    }
            //    condition[5] = condition[0];
            //    break;

            //case "TB_ZM_KOSHU":
            //    condition[0] = "KOSHU_CD";
            //    condition[1] = "コード";
            //    condition[2] = "KOSHU_NM";
            //    condition[3] = "名称";
            //    condition[4] = "KAISHA_CD = @KAISHA_CD";
            //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
            //    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
            //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
            //    if (isToGetName)
            //    {
            //        condition[4] += " AND " + condition[0] + " = @" + condition[0];
            //        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
            //    }
            //    condition[5] = condition[0];
            //    break;

            case "TB_ZM_F_KAMOKU_BUNRUI":
                condition[0] = "KAMOKU_BUNRUI_CD";
                condition[1] = "コード";
                condition[2] = "KAMOKU_BUNRUI_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                }
                condition[5] = condition[0];
                break;

            #endregion
            #region "給与"
            case "TB_KY_SHAIN_JOHO":
                condition[0] = "SHAIN_CD";
                condition[1] = "コード";
                condition[2] = "SHAIN_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 6, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_BUMON":
                condition[0] = "BUMON_CD";
                condition[1] = "コード";
                condition[2] = "BUMON_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_YAKUSHOKU":
                condition[0] = "YAKUSHOKU_CD";
                condition[1] = "コード";
                condition[2] = "YAKUSHOKU_NM";
                condition[3] = "名称";
                condition[4] = "KAISHA_CD = @KAISHA_CD";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] += " AND " + condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_SHICHOSON":
                condition[0] = "SHICHOSON_CD";
                condition[1] = "コード";
                condition[2] = "SHICHOSON_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 6, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_GINKO":
                condition[0] = "GINKO_CD";
                condition[1] = "コード";
                condition[2] = "GINKO_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_F_KOYO_HOKEN_KEISAN_HOHO":
                condition[0] = "KOYO_HOKEN_KEISAN_HOHO";
                condition[1] = "コード";
                condition[2] = "KOYO_HOKEN_KEISAN_HOHO_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_F_KOYO_HOKEN_GYOSHU":
                condition[0] = "KOYO_HOKEN_GYOSHU";
                condition[1] = "コード";
                condition[2] = "KOYO_HOKEN_GYOSHU_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_F_SHOTOKUZEI_KEISAN_HOHO":
                condition[0] = "SHOTOKUZEI_KEISAN_HOHO";
                condition[1] = "コード";
                condition[2] = "SHOTOKUZEI_KEISAN_HOHO_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_F_KYUYO_KEITAI":
                condition[0] = "KYUYO_KEITAI";
                condition[1] = "コード";
                condition[2] = "KYUYO_KEITAI_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_F_HONNIN_KUBUN1":
                condition[0] = "HONNIN_KUBUN1";
                condition[1] = "区分";
                condition[2] = "HONNIN_KUBUN1_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_F_HONNIN_KUBUN2":
                condition[0] = "HONNIN_KUBUN2";
                condition[1] = "区分";
                condition[2] = "HONNIN_KUBUN2_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_F_HONNIN_KUBUN3":
                condition[0] = "HONNIN_KUBUN3";
                condition[1] = "区分";
                condition[2] = "HONNIN_KUBUN3_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;

            case "TB_KY_F_HAIGUSHA_KUBUN":
                condition[0] = "HAIGUSHA_KUBUN";
                condition[1] = "区分";
                condition[2] = "HAIGUSHA_KUBUN_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;
            #endregion
            #region
            case "TB_EDI_SHOHIN":
                condition[0] = "SHOHIN_CD";
                condition[1] = "コード";
                condition[2] = "SHOHIN_NM";
                condition[3] = "名称";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                }
                condition[5] = condition[0];
                break;
            case "TB_EDI_TORIHIKISAKI":
                condition[0] = "TORIHIKISAKI_CD";
                condition[1] = "コード";
                condition[2] = "TORIHIKISAKI_NM";
                condition[3] = "名称";
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 8, code);
                }
                condition[5] = condition[0];
                break;
            case "VI_EDI_SHOHIN_KUBUN1":
            case "VI_EDI_SHOHIN_KUBUN2":
            case "VI_EDI_SHOHIN_KUBUN3":
            case "VI_EDI_SHOHIN_KUBUN4":
            case "VI_EDI_SHOHIN_KUBUN5":
                condition[0] = "SHOHIN_KUBUN";
                condition[1] = "区分";
                condition[2] = "SHOHIN_KUBUN_NM";
                condition[3] = "名称";
                if (isToGetName)
                {
                    condition[4] = condition[0] + " = @" + condition[0];
                    dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                }
                condition[5] = condition[0];
                break;
            #endregion
            default:
                break;
        }

        return condition;
    }

    #endregion
}
