﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hnyr1021
{
    /// <summary>
    /// 操業制限損失保証一覧表(HNYR1021)
    /// </summary>
    public partial class HNYR1021 : BasePgForm
    {
        #region 定数
        private const int prtCols = 23;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNYR1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 年指定
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];

            // ラジオボタンのデフォルト値を設定
            this.rdoKamiki.Checked = true;
            this.rdoShimoki.Checked = false;
            // フォーカス設定
            this.txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 年始定、船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNYR1021R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年指定の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                this.txtDateYear.Text = Util.ToString(IsValid.SetYear(this.txtDateYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYear.Text))
            {
                this.txtDateYear.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba));

            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJp();
            // 年月日(自)の正しい和暦への変換処理
            SetJp();

            // 船主コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba);
        }
        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba));
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNYR1021R rpt = new HNYR1021R(dtOutput);
                    string hanki = rdoKamiki.Checked ? " (上期)" : " (下期)";
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text + hanki;
                    rpt.Document.Name = this.lblTitle.Text + hanki;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 日付範囲を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    "1", "1", this.Dba);
            // 表示日付設定
            string hyojiDate = this.lblDateGengo.Text + this.txtDateYear.Text + "年分";
                        
            // 上期下期選択情報の取得,の設定
            string hankiFr;
            string hankiTo;
            string hanki;
            int tsuki01;
            int tsuki02;
            int tsuki03;
            int tsuki04;
            int tsuki05;
            int tsuki06;
            if (rdoKamiki.Checked)
            {
                hankiFr = tmpDate.Year + "/04/01";
                hankiTo = tmpDate.Year + "/09/30";
                hanki = "上期";
                tsuki01 = 4;
                tsuki02 = 5;
                tsuki03 = 6;
                tsuki04 = 7;
                tsuki05 = 8;
                tsuki06 = 9;
            }
            else
            {
                hankiFr = tmpDate.Year + "/10/01";
                hankiTo = tmpDate.Year + 1 + "/03/31";
                hanki = "下期";
                tsuki01 = 10;
                tsuki02 = 11;
                tsuki03 = 12;
                tsuki04 = 1;
                tsuki05 = 2;
                tsuki06 = 3;
            }

            // 船主コード設定
            string FUNANUSHI_CD_FR;
            string FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            #region 支所コードの退避
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            #endregion
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            // han.VI_操業制限損失保証一覧表(VI_HN_SOGYO_SIGN_SNST_HS_ICRH)の日付範囲から発生しているデータを取得
            //Sql.Append("SELECT KAISHA_CD,");
            Sql.Append("SELECT KAISHA_CD,");
            if (shishoCd != "0")
            {
                Sql.Append("    SHISHO_CD,");
            }
            else
            {
                Sql.Append("  0 AS SHISHO_CD,");
            }
            Sql.Append("      SENSHU_CD,");
            Sql.Append("      SENSHU_NM,");
            Sql.Append("      SHUKEI_NEN,");
            Sql.Append("      SHUKEI_TSUKI,");
            Sql.Append("      SUM(SURYO_1GATSU) AS SURYO_1GATSU,");
            Sql.Append("      SUM(SURYO_2GATSU) AS SURYO_2GATSU,");
            Sql.Append("      SUM(SURYO_3GATSU) AS SURYO_3GATSU,");
            Sql.Append("      SUM(SURYO_4GATSU) AS SURYO_4GATSU,");
            Sql.Append("      SUM(SURYO_5GATSU) AS SURYO_5GATSU,");
            Sql.Append("      SUM(SURYO_6GATSU) AS SURYO_6GATSU,");
            Sql.Append("      SUM(SURYO_7GATSU) AS SURYO_7GATSU,");
            Sql.Append("      SUM(SURYO_8GATSU) AS SURYO_8GATSU,");
            Sql.Append("      SUM(SURYO_9GATSU) AS SURYO_9GATSU,");
            Sql.Append("      SUM(SURYO_10GATSU) AS SURYO_10GATSU,");
            Sql.Append("      SUM(SURYO_11GATSU) AS SURYO_11GATSU,");
            Sql.Append("      SUM(SURYO_12GATSU) AS SURYO_12GATSU,");
            Sql.Append("      SUM(KINGAKU_1GATSU) AS KINGAKU_1GATSU,");
            Sql.Append("      SUM(KINGAKU_2GATSU) AS KINGAKU_2GATSU,");
            Sql.Append("      SUM(KINGAKU_3GATSU) AS KINGAKU_3GATSU,");
            Sql.Append("      SUM(KINGAKU_4GATSU) AS KINGAKU_4GATSU,");
            Sql.Append("      SUM(KINGAKU_5GATSU) AS KINGAKU_5GATSU,");
            Sql.Append("      SUM(KINGAKU_6GATSU) AS KINGAKU_6GATSU,");
            Sql.Append("      SUM(KINGAKU_7GATSU) AS KINGAKU_7GATSU,");
            Sql.Append("      SUM(KINGAKU_8GATSU) AS KINGAKU_8GATSU,");
            Sql.Append("      SUM(KINGAKU_9GATSU) AS KINGAKU_9GATSU,");
            Sql.Append("      SUM(KINGAKU_10GATSU) AS KINGAKU_10GATSU,");
            Sql.Append("      SUM(KINGAKU_11GATSU) AS KINGAKU_11GATSU,");
            Sql.Append("      SUM(KINGAKU_12GATSU) AS KINGAKU_12GATSU");
            Sql.Append("  FROM");
            Sql.Append("      VI_HN_SOGYO_SIGN_SNST_HS_ICRH");
            Sql.Append("  WHERE");
            Sql.Append("      KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("      SHISHO_CD = @SHISHO_CD AND");
            Sql.Append("	  SENSHU_CD BETWEEN @SENSHU_CD_FR AND @SENSHU_CD_TO AND");
            Sql.Append("	  SHUKEI_NEN = @KAIKEI_NENDO");
            Sql.Append("  GROUP BY ");
            Sql.Append("      KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append("      SHISHO_CD,");
            Sql.Append("      SENSHU_CD,");
            Sql.Append("	  SENSHU_NM,");
            Sql.Append("	  SHUKEI_NEN,");
            Sql.Append("	  SHUKEI_TSUKI");
            Sql.Append("  ORDER BY");
            Sql.Append("      KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append("      SHISHO_CD,");
            Sql.Append("      SENSHU_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, tmpDate.Year);
            dpc.SetParam("@SENSHU_CD_FR", SqlDbType.Decimal, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@SENSHU_CD_TO", SqlDbType.Decimal, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion
            
            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    // 船主CDが次回の船主CDと不一致の場合実行
                    if (i == dtMainLoop.Rows.Count - 1 || Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[i + 1]["SENSHU_CD"]))
                    {
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                        Sql.Append(") ");
                        #endregion

                        #region データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojiDate); // 年指定
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hanki); // 半期判断
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tsuki01); // 月
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tsuki02); // 月
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tsuki03); // 月
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tsuki04); // 月
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, tsuki05); // 月
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, tsuki06); // 月
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主CD
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                        if (rdoKamiki.Checked)
                        {
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_4GATSU"], 1)); // 数量04
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_5GATSU"], 1)); // 数量05
                            dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_6GATSU"], 1)); // 数量06
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_7GATSU"], 1)); // 数量07
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_8GATSU"], 1)); // 数量08
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_9GATSU"], 1)); // 数量09
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_4GATSU"])); // 金額04
                            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_5GATSU"])); // 金額05
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_6GATSU"])); // 金額06
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_7GATSU"])); // 金額07
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_8GATSU"])); // 金額08
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_9GATSU"])); // 金額09
                        }
                        else
                        {
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_10GATSU"], 1)); // 数量10
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_11GATSU"], 1)); // 数量11
                            dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_12GATSU"], 1)); // 数量12
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_1GATSU"], 1)); // 数量01
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_2GATSU"], 1)); // 数量02
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_3GATSU"], 1)); // 数量03
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_10GATSU"])); // 金額10
                            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_11GATSU"])); // 金額11
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_12GATSU"])); // 金額12
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_1GATSU"])); // 金額01
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_2GATSU"])); // 金額02
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_3GATSU"])); // 金額03
                        }
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHISHO_CD"])); // 支所コード
                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion
                    }

                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
