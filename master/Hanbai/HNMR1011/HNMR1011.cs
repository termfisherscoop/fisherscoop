﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1011
{
    /// <summary>
    /// 水揚仕切月計表(HNMR1011)
    /// </summary>
    public partial class HNMR1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 精算区分
        /// </summary>
        // 精算区分の値を要確認
        private const int ALL = 0; // 全て
        private const int CHIKUGAI = 1; // 地区外
        private const int HAMAURI = 2;  // 浜売り
        private const int CHIKUNAI = 3; // 地区内

        private const int prtCols = 25;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲前
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            // 日付範囲後
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            // 精算区分地区内ｾﾘにチェック
            this.txtSeisanKbn.Text = "0";
            IsValidSeisanKbn();

            // 初期フォーカス
            txtDateYearFr.Focus();
            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtSeisanKbn":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                #endregion


                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        "1",
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        "1",
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                            }
                        }
                    }
                    break;
                case "txtSeisanKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtSeisanKbn.Text = outData[0];
                                this.lblSeisanKbnNm.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNMR1011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        private void txtSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeisanKbn())
            {
                e.Cancel = true;
                this.txtSeisanKbn.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        private void txtSeisanKbn_KeyDown(object sender, KeyEventArgs e)
        {
            // 共済加入区分を使用しない場合
            if (!this.fsiPanel4.Visible)
            {
                if (e.KeyCode == Keys.Enter && this.Flg)
                {
                    // Enter処理を無効化
                    this._dtFlg = false;

                    // 全項目を再度入力値チェック
                    if (!ValidateAll())
                    {
                        // エラーありの場合ここで処理終了
                        return;
                    }

                    if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                    {
                        // ﾌﾟﾚﾋﾞｭｰ処理
                        DoPrint(true);
                    }
                    else
                    {
                        this.txtSeisanKbn.Focus();
                    }
                }
            }
        }

        private void txtKyosai_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKyosaiKbn())
            {
                e.Cancel = true;
                this.txtKyosai.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 共済加入区分のEnter押下時処理
        /// (画面上最後のコントロール)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyosai_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtKyosai.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        private bool IsValidSeisanKbn()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtSeisanKbn.Text, this.txtSeisanKbn.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text) || this.txtSeisanKbn.Text == "0")
            {
                this.txtSeisanKbn.Text = "0";
                this.lblSeisanKbnNm.Text = "全て";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                Msg.Notice("精算区分は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", this.txtMizuageShishoCd.Text, this.txtSeisanKbn.Text);
                if (ValChk.IsEmpty(this.lblSeisanKbnNm.Text))
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        private bool IsValidKyosaiKbn()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtKyosai.Text, this.txtKyosai.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKyosai.Text))
            {
                Msg.Notice("共済加入区分は数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 精算区分の入力チェック
            if (!IsValidSeisanKbn())
            {
                this.txtSeisanKbn.Focus();
                this.txtSeisanKbn.SelectAll();
                return false;
            }

            // 共済加入区分の入力チェック
            if (!IsValidKyosaiKbn())
            {
                this.txtKyosai.Focus();
                this.txtKyosai.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNMR1011R rpt = new HNMR1011R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.Text;
                    rpt.Document.Name = this.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {

            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, "1", this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, "1", this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);

            // 表示日付設定
            string hyojiDate;
            if (tmpDateFr == tmpDateTo)
            {
                hyojiDate = string.Format( tmpDateFr.Date.ToString("yyyy/MM") +"～"+ tmpDateTo2.Date.ToString("yyyy/MM"));
            }
            else
            {
                hyojiDate = string.Format(tmpDateFr.Date.ToString("yyyy/MM") +"～"+ tmpDateTo.Date.ToString("yyyy/MM"));
            }
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            string RptType = Util.ToString( this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "ReportType", "Type"));
            string RptNo = Util.ToString( this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "ReportType", "No"));
            //int seisanKbn = 0; // 精算区分
            string seisanKbn = this.txtSeisanKbn.Text;
            string seisanKbnNm = this.lblSeisanKbnNm.Text; // 精算区分名

            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // han.VI_水揚仕切月計表(VI_HN_MIZUAGE_SKR_GEKKEIHYO)の日付範囲から発生しているデータを取得
            // データ取得ストアド実行
            Sql.Append(" EXEC MIZUAGE_SHIKIRI_GEKKEIHYO");
            Sql.Append(" @KAISHA_CD,");
            Sql.Append(" @SHISHO_CD,");
            Sql.Append(" @DATE_FR,");
            Sql.Append(" @DATE_TO,");
            Sql.Append(" @RPT_TYPE,");
            Sql.Append(" @RPT_NO,");
            Sql.Append(" @SEISAN_KUBUN");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo2.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@RPT_TYPE", SqlDbType.Decimal, 4, RptType);
            dpc.SetParam("@RPT_NO", SqlDbType.Decimal, 4, RptNo);
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 4, seisanKbn);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                int i = 0; // ループ用カウント変数
                // 印刷日付
                string[] arrJpDate = Util.ConvJpDate(DateTime.Today, this.Dba);
                string printDate = string.Format("{0:yyyy/MM/dd}", DateTime.Today);

                decimal sashihikiKingaku; // 差引金額

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojiDate);
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dr["CHIKU_CD"]);
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["CHIKU_NM"]);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SENSHU_CD"]);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["SENSHU_NM"]);

                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["KENSU"]);// 日数
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["SURYO"]);// 数量
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["ZEINUKI_KINGAKU"]);// 水揚金額
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["SHOHIZEI"]);// 消費税
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["HANBAI_MIBARAIKIN"]); // 販売未払金
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU1"]);// 漁協手数料
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU2"]);// 箱代
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU3"]);// その他の控除額
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU4"]);// 積立金
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU5"]);// 預り金
                    sashihikiKingaku = Util.ToDecimal(dr["HANBAI_MIBARAIKIN"]) - Util.ToDecimal(dr["KOJO_KOMOKU1"])
                                     - Util.ToDecimal(dr["KOJO_KOMOKU2"]) - Util.ToDecimal(dr["KOJO_KOMOKU3"])
                                     - Util.ToDecimal(dr["KOJO_KOMOKU4"]) - Util.ToDecimal(dr["KOJO_KOMOKU5"])
                                     - Util.ToDecimal(dr["KOJO_KOMOKU6"]);
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, sashihikiKingaku);// 差引金額
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, seisanKbnNm);// 精算区分名
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, printDate);// 日付

                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU1_NAME"]);
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU2_NAME"]);
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU3_NAME"]);
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU4_NAME"]);
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU5_NAME"]);
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dr["KOJO_KOMOKU6_NAME"]);
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, dr["SHISHO_CD"]);

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }

            }


            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;

        }

        #endregion
    }
}
