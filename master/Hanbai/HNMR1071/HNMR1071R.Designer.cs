﻿namespace jp.co.fsi.hn.hnmr1071
{
    /// <summary>
    /// HNMR1071R の概要の説明です。
    /// </summary>
    partial class HNMR1071R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNMR1071R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle06 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKaishaNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateHani = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFunanushiNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHasseibi = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHasseiSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSengetuZandaka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTougetuKarikata = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTougetuKashikata = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblZandaka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShouhizei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtHasseibi = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHasseisuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKarikata = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKashikata = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZandaka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtTotal07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtFunanushiCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFunanushiNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZenZandaka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtSubTotal07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblGyoshuCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGyoshuNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHani)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFunanushiNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseibi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseiSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSengetuZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKarikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKashikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShouhizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasseibi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasseisuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKarikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKashikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZenZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo1,
            this.lblPage,
            this.txtPageCount,
            this.lblTitle06,
            this.txtTitleName,
            this.txtKaishaNm,
            this.txtDate,
            this.txtDateHani,
            this.lblCd,
            this.lblFunanushiNm,
            this.lblHasseibi,
            this.lblHasseiSuryo,
            this.lblSengetuZandaka,
            this.lblTougetuKarikata,
            this.lblTougetuKashikata,
            this.lblZandaka,
            this.lblShouhizei,
            this.line1,
            this.line2});
            this.pageHeader.Height = 1.201F;
            this.pageHeader.Name = "pageHeader";
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1574803F;
            this.reportInfo1.Left = 7.791733F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.reportInfo1.Top = 0.3295276F;
            this.reportInfo1.Width = 0.75F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1574803F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 8.857473F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.3295276F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1574803F;
            this.txtPageCount.Left = 8.541733F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtPageCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.3295276F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // lblTitle06
            // 
            this.lblTitle06.DataField = "ITEM20";
            this.lblTitle06.Height = 0.2F;
            this.lblTitle06.HyperLink = null;
            this.lblTitle06.Left = 7.912213F;
            this.lblTitle06.Name = "lblTitle06";
            this.lblTitle06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblTitle06.Text = "【税込】";
            this.lblTitle06.Top = 0.4870079F;
            this.lblTitle06.Width = 1F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 3.522835F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle; ddo-char-set: 1";
            this.txtTitleName.Text = "販売未払金元帳(月報)";
            this.txtTitleName.Top = 0.1185039F;
            this.txtTitleName.Width = 2.833465F;
            // 
            // txtKaishaNm
            // 
            this.txtKaishaNm.DataField = "ITEM01";
            this.txtKaishaNm.Height = 0.2F;
            this.txtKaishaNm.Left = 0.5326772F;
            this.txtKaishaNm.MultiLine = false;
            this.txtKaishaNm.Name = "txtKaishaNm";
            this.txtKaishaNm.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; ddo-char-set: 128";
            this.txtKaishaNm.Text = "txtKaishaNm";
            this.txtKaishaNm.Top = 0.2870079F;
            this.txtKaishaNm.Width = 3.866536F;
            // 
            // txtDate
            // 
            this.txtDate.DataField = "ITEM02";
            this.txtDate.Height = 0.2F;
            this.txtDate.Left = 0.5326772F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-align: left; dd" +
    "o-char-set: 128";
            this.txtDate.Text = "txtDate";
            this.txtDate.Top = 0.5582678F;
            this.txtDate.Visible = false;
            this.txtDate.Width = 2.833465F;
            // 
            // txtDateHani
            // 
            this.txtDateHani.DataField = "ITEM03";
            this.txtDateHani.Height = 0.2F;
            this.txtDateHani.Left = 3.574804F;
            this.txtDateHani.Name = "txtDateHani";
            this.txtDateHani.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.txtDateHani.Text = "txtDateHani";
            this.txtDateHani.Top = 0.4870079F;
            this.txtDateHani.Width = 2.833465F;
            // 
            // lblCd
            // 
            this.lblCd.Height = 0.2F;
            this.lblCd.HyperLink = null;
            this.lblCd.Left = 0.1019685F;
            this.lblCd.Name = "lblCd";
            this.lblCd.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center";
            this.lblCd.Text = "コード";
            this.lblCd.Top = 0.9795277F;
            this.lblCd.Width = 0.6826771F;
            // 
            // lblFunanushiNm
            // 
            this.lblFunanushiNm.Height = 0.2F;
            this.lblFunanushiNm.HyperLink = null;
            this.lblFunanushiNm.Left = 0.992126F;
            this.lblFunanushiNm.Name = "lblFunanushiNm";
            this.lblFunanushiNm.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left";
            this.lblFunanushiNm.Text = "船 主 氏 名";
            this.lblFunanushiNm.Top = 0.9795277F;
            this.lblFunanushiNm.Width = 1.11378F;
            // 
            // lblHasseibi
            // 
            this.lblHasseibi.Height = 0.2F;
            this.lblHasseibi.HyperLink = null;
            this.lblHasseibi.Left = 2.141339F;
            this.lblHasseibi.Name = "lblHasseibi";
            this.lblHasseibi.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center";
            this.lblHasseibi.Text = "発 生 日";
            this.lblHasseibi.Top = 0.9795277F;
            this.lblHasseibi.Width = 0.8562994F;
            // 
            // lblHasseiSuryo
            // 
            this.lblHasseiSuryo.Height = 0.2F;
            this.lblHasseiSuryo.HyperLink = null;
            this.lblHasseiSuryo.Left = 2.879528F;
            this.lblHasseiSuryo.Name = "lblHasseiSuryo";
            this.lblHasseiSuryo.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblHasseiSuryo.Text = "発生数量";
            this.lblHasseiSuryo.Top = 0.9795277F;
            this.lblHasseiSuryo.Width = 0.9700789F;
            // 
            // lblSengetuZandaka
            // 
            this.lblSengetuZandaka.Height = 0.2F;
            this.lblSengetuZandaka.HyperLink = null;
            this.lblSengetuZandaka.Left = 3.82874F;
            this.lblSengetuZandaka.Name = "lblSengetuZandaka";
            this.lblSengetuZandaka.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblSengetuZandaka.Text = "前月残高";
            this.lblSengetuZandaka.Top = 0.9795277F;
            this.lblSengetuZandaka.Width = 1.037008F;
            // 
            // lblTougetuKarikata
            // 
            this.lblTougetuKarikata.Height = 0.2F;
            this.lblTougetuKarikata.HyperLink = null;
            this.lblTougetuKarikata.Left = 4.791733F;
            this.lblTougetuKarikata.Name = "lblTougetuKarikata";
            this.lblTougetuKarikata.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblTougetuKarikata.Text = "当月借方発生";
            this.lblTougetuKarikata.Top = 0.9795277F;
            this.lblTougetuKarikata.Width = 1.229134F;
            // 
            // lblTougetuKashikata
            // 
            this.lblTougetuKashikata.Height = 0.2F;
            this.lblTougetuKashikata.HyperLink = null;
            this.lblTougetuKashikata.Left = 6.042914F;
            this.lblTougetuKashikata.Name = "lblTougetuKashikata";
            this.lblTougetuKashikata.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblTougetuKashikata.Text = "当月貸方発生";
            this.lblTougetuKashikata.Top = 0.9795277F;
            this.lblTougetuKashikata.Width = 1.168899F;
            // 
            // lblZandaka
            // 
            this.lblZandaka.Height = 0.2F;
            this.lblZandaka.HyperLink = null;
            this.lblZandaka.Left = 7.465749F;
            this.lblZandaka.Name = "lblZandaka";
            this.lblZandaka.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblZandaka.Text = "残    高";
            this.lblZandaka.Top = 0.9795277F;
            this.lblZandaka.Width = 0.9279528F;
            // 
            // lblShouhizei
            // 
            this.lblShouhizei.Height = 0.2F;
            this.lblShouhizei.HyperLink = null;
            this.lblShouhizei.Left = 8.519686F;
            this.lblShouhizei.Name = "lblShouhizei";
            this.lblShouhizei.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblShouhizei.Text = "消 費 税";
            this.lblShouhizei.Top = 0.9795277F;
            this.lblShouhizei.Width = 0.9023628F;
            // 
            // line1
            // 
            this.line1.Height = 8.940697E-08F;
            this.line1.Left = 3.522835F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.4059054F;
            this.line1.Width = 2.833463F;
            this.line1.X1 = 3.522835F;
            this.line1.X2 = 6.356298F;
            this.line1.Y1 = 0.4059055F;
            this.line1.Y2 = 0.4059054F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 1.179528F;
            this.line2.Width = 9.422048F;
            this.line2.X1 = 0F;
            this.line2.X2 = 9.422048F;
            this.line2.Y1 = 1.179528F;
            this.line2.Y2 = 1.179528F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM19";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtHasseibi,
            this.txtHasseisuryo,
            this.txtKarikata,
            this.txtKashikata,
            this.txtZandaka,
            this.textBox14});
            this.detail.Height = 0.2188976F;
            this.detail.Name = "detail";
            // 
            // txtHasseibi
            // 
            this.txtHasseibi.DataField = "ITEM07";
            this.txtHasseibi.Height = 0.2F;
            this.txtHasseibi.Left = 2.141339F;
            this.txtHasseibi.MultiLine = false;
            this.txtHasseibi.Name = "txtHasseibi";
            this.txtHasseibi.OutputFormat = resources.GetString("txtHasseibi.OutputFormat");
            this.txtHasseibi.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: center";
            this.txtHasseibi.Text = "txtHasseibi";
            this.txtHasseibi.Top = 0F;
            this.txtHasseibi.Width = 0.8562992F;
            // 
            // txtHasseisuryo
            // 
            this.txtHasseisuryo.DataField = "ITEM08";
            this.txtHasseisuryo.Height = 0.2F;
            this.txtHasseisuryo.Left = 2.938583F;
            this.txtHasseisuryo.MultiLine = false;
            this.txtHasseisuryo.Name = "txtHasseisuryo";
            this.txtHasseisuryo.OutputFormat = resources.GetString("txtHasseisuryo.OutputFormat");
            this.txtHasseisuryo.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right";
            this.txtHasseisuryo.Text = "txtHasseisuryo";
            this.txtHasseisuryo.Top = 1.862645E-09F;
            this.txtHasseisuryo.Width = 0.9110243F;
            // 
            // txtKarikata
            // 
            this.txtKarikata.DataField = "ITEM09";
            this.txtKarikata.Height = 0.2F;
            this.txtKarikata.Left = 4.791733F;
            this.txtKarikata.MultiLine = false;
            this.txtKarikata.Name = "txtKarikata";
            this.txtKarikata.OutputFormat = resources.GetString("txtKarikata.OutputFormat");
            this.txtKarikata.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right";
            this.txtKarikata.Text = "txtKarikata";
            this.txtKarikata.Top = 0F;
            this.txtKarikata.Width = 1.229134F;
            // 
            // txtKashikata
            // 
            this.txtKashikata.DataField = "ITEM10";
            this.txtKashikata.Height = 0.2F;
            this.txtKashikata.Left = 6.042914F;
            this.txtKashikata.MultiLine = false;
            this.txtKashikata.Name = "txtKashikata";
            this.txtKashikata.OutputFormat = resources.GetString("txtKashikata.OutputFormat");
            this.txtKashikata.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right";
            this.txtKashikata.Text = "txtKashikata";
            this.txtKashikata.Top = 0.01889764F;
            this.txtKashikata.Width = 1.168899F;
            // 
            // txtZandaka
            // 
            this.txtZandaka.DataField = "ITEM11";
            this.txtZandaka.Height = 0.2F;
            this.txtZandaka.Left = 7.211812F;
            this.txtZandaka.MultiLine = false;
            this.txtZandaka.Name = "txtZandaka";
            this.txtZandaka.OutputFormat = resources.GetString("txtZandaka.OutputFormat");
            this.txtZandaka.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right";
            this.txtZandaka.Text = "txtZandaka";
            this.txtZandaka.Top = 0.01889764F;
            this.txtZandaka.Width = 1.18189F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM12";
            this.textBox14.Height = 0.2F;
            this.textBox14.Left = 8.316538F;
            this.textBox14.MultiLine = false;
            this.textBox14.Name = "textBox14";
            this.textBox14.OutputFormat = resources.GetString("textBox14.OutputFormat");
            this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right";
            this.textBox14.Text = "textBox14";
            this.textBox14.Top = 0F;
            this.textBox14.Width = 1.105511F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotal07,
            this.txtTotal08,
            this.txtTotal09,
            this.txtTotal10,
            this.txtTotal12,
            this.txtTotal11,
            this.txtTotal01,
            this.txtTotal02,
            this.txtTotal03,
            this.txtTotal04,
            this.txtTotal05,
            this.txtTotal06,
            this.label1,
            this.label2});
            this.reportFooter1.Height = 0.5314305F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // txtTotal07
            // 
            this.txtTotal07.DataField = "ITEM13";
            this.txtTotal07.Height = 0.2F;
            this.txtTotal07.Left = 2.646851F;
            this.txtTotal07.MultiLine = false;
            this.txtTotal07.Name = "txtTotal07";
            this.txtTotal07.OutputFormat = resources.GetString("txtTotal07.OutputFormat");
            this.txtTotal07.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal07.Text = "txtTotal07";
            this.txtTotal07.Top = 0.215748F;
            this.txtTotal07.Width = 1.202756F;
            // 
            // txtTotal08
            // 
            this.txtTotal08.DataField = "ITEM14";
            this.txtTotal08.Height = 0.2F;
            this.txtTotal08.Left = 3.842127F;
            this.txtTotal08.MultiLine = false;
            this.txtTotal08.Name = "txtTotal08";
            this.txtTotal08.OutputFormat = resources.GetString("txtTotal08.OutputFormat");
            this.txtTotal08.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal08.Text = "txtTotal08";
            this.txtTotal08.Top = 0.215748F;
            this.txtTotal08.Width = 1.023622F;
            // 
            // txtTotal09
            // 
            this.txtTotal09.DataField = "ITEM15";
            this.txtTotal09.Height = 0.2F;
            this.txtTotal09.Left = 4.791733F;
            this.txtTotal09.MultiLine = false;
            this.txtTotal09.Name = "txtTotal09";
            this.txtTotal09.OutputFormat = resources.GetString("txtTotal09.OutputFormat");
            this.txtTotal09.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal09.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal09.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal09.Text = "txtTotal09";
            this.txtTotal09.Top = 0.215748F;
            this.txtTotal09.Width = 1.229134F;
            // 
            // txtTotal10
            // 
            this.txtTotal10.DataField = "ITEM16";
            this.txtTotal10.Height = 0.2F;
            this.txtTotal10.Left = 6.042914F;
            this.txtTotal10.MultiLine = false;
            this.txtTotal10.Name = "txtTotal10";
            this.txtTotal10.OutputFormat = resources.GetString("txtTotal10.OutputFormat");
            this.txtTotal10.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal10.Text = "txtTotal10";
            this.txtTotal10.Top = 0.215748F;
            this.txtTotal10.Width = 1.168899F;
            // 
            // txtTotal12
            // 
            this.txtTotal12.DataField = "ITEM18";
            this.txtTotal12.Height = 0.2F;
            this.txtTotal12.Left = 8.316538F;
            this.txtTotal12.MultiLine = false;
            this.txtTotal12.Name = "txtTotal12";
            this.txtTotal12.OutputFormat = resources.GetString("txtTotal12.OutputFormat");
            this.txtTotal12.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal12.Text = "txtTotal12";
            this.txtTotal12.Top = 0.215748F;
            this.txtTotal12.Width = 1.105511F;
            // 
            // txtTotal11
            // 
            this.txtTotal11.DataField = "ITEM07+ITEM09-ITEM08";
            this.txtTotal11.Height = 0.2F;
            this.txtTotal11.Left = 7.211812F;
            this.txtTotal11.MultiLine = false;
            this.txtTotal11.Name = "txtTotal11";
            this.txtTotal11.OutputFormat = resources.GetString("txtTotal11.OutputFormat");
            this.txtTotal11.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal11.Text = "txtTotal11";
            this.txtTotal11.Top = 0.215748F;
            this.txtTotal11.Width = 1.18189F;
            // 
            // txtTotal01
            // 
            this.txtTotal01.DataField = "ITEM08";
            this.txtTotal01.Height = 0.2F;
            this.txtTotal01.Left = 2.646851F;
            this.txtTotal01.MultiLine = false;
            this.txtTotal01.Name = "txtTotal01";
            this.txtTotal01.OutputFormat = resources.GetString("txtTotal01.OutputFormat");
            this.txtTotal01.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal01.Text = "txtTotal01";
            this.txtTotal01.Top = 0F;
            this.txtTotal01.Width = 1.202756F;
            // 
            // txtTotal02
            // 
            this.txtTotal02.DataField = "ITEM06";
            this.txtTotal02.Height = 0.2F;
            this.txtTotal02.Left = 3.842126F;
            this.txtTotal02.MultiLine = false;
            this.txtTotal02.Name = "txtTotal02";
            this.txtTotal02.OutputFormat = resources.GetString("txtTotal02.OutputFormat");
            this.txtTotal02.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal02.Text = "txtTotal02";
            this.txtTotal02.Top = 0F;
            this.txtTotal02.Width = 1.023623F;
            // 
            // txtTotal03
            // 
            this.txtTotal03.DataField = "ITEM09";
            this.txtTotal03.Height = 0.2F;
            this.txtTotal03.Left = 4.791733F;
            this.txtTotal03.MultiLine = false;
            this.txtTotal03.Name = "txtTotal03";
            this.txtTotal03.OutputFormat = resources.GetString("txtTotal03.OutputFormat");
            this.txtTotal03.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal03.Text = "txtTotal03";
            this.txtTotal03.Top = 0F;
            this.txtTotal03.Width = 1.229134F;
            // 
            // txtTotal04
            // 
            this.txtTotal04.DataField = "ITEM10";
            this.txtTotal04.Height = 0.2F;
            this.txtTotal04.Left = 6.042914F;
            this.txtTotal04.MultiLine = false;
            this.txtTotal04.Name = "txtTotal04";
            this.txtTotal04.OutputFormat = resources.GetString("txtTotal04.OutputFormat");
            this.txtTotal04.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal04.Text = "txtTotal04";
            this.txtTotal04.Top = 0F;
            this.txtTotal04.Width = 1.168899F;
            // 
            // txtTotal05
            // 
            this.txtTotal05.DataField = "ITEM07+ITEM09-ITEM08";
            this.txtTotal05.Height = 0.2F;
            this.txtTotal05.Left = 7.211812F;
            this.txtTotal05.MultiLine = false;
            this.txtTotal05.Name = "txtTotal05";
            this.txtTotal05.OutputFormat = resources.GetString("txtTotal05.OutputFormat");
            this.txtTotal05.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal05.Text = "txtTotal05";
            this.txtTotal05.Top = 0F;
            this.txtTotal05.Width = 1.18189F;
            // 
            // txtTotal06
            // 
            this.txtTotal06.DataField = "ITEM12";
            this.txtTotal06.Height = 0.2F;
            this.txtTotal06.Left = 8.316538F;
            this.txtTotal06.MultiLine = false;
            this.txtTotal06.Name = "txtTotal06";
            this.txtTotal06.OutputFormat = resources.GetString("txtTotal06.OutputFormat");
            this.txtTotal06.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotal06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal06.Text = "txtTotal06";
            this.txtTotal06.Top = 0F;
            this.txtTotal06.Width = 1.105511F;
            // 
            // label1
            // 
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.9921259F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; ddo-char-set: 128";
            this.label1.Text = "【月計合計】";
            this.label1.Top = 0F;
            this.label1.Width = 1.260236F;
            // 
            // label2
            // 
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.9921257F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; ddo-char-set: 128";
            this.label2.Text = "【累計合計】";
            this.label2.Top = 0.215748F;
            this.label2.Width = 1.260236F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFunanushiCd,
            this.txtFunanushiNm,
            this.txtZenZandaka});
            this.groupHeader1.DataField = "ITEM04";
            this.groupHeader1.Height = 0.2188976F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.Format += new System.EventHandler(this.groupHeader1_Format);
            // 
            // txtFunanushiCd
            // 
            this.txtFunanushiCd.DataField = "ITEM04";
            this.txtFunanushiCd.Height = 0.2F;
            this.txtFunanushiCd.Left = 0.1019685F;
            this.txtFunanushiCd.MultiLine = false;
            this.txtFunanushiCd.Name = "txtFunanushiCd";
            this.txtFunanushiCd.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center";
            this.txtFunanushiCd.Text = "txtFunanushiCd";
            this.txtFunanushiCd.Top = 0.01889764F;
            this.txtFunanushiCd.Width = 0.6826771F;
            // 
            // txtFunanushiNm
            // 
            this.txtFunanushiNm.DataField = "ITEM05";
            this.txtFunanushiNm.Height = 0.2F;
            this.txtFunanushiNm.Left = 0.992126F;
            this.txtFunanushiNm.MultiLine = false;
            this.txtFunanushiNm.Name = "txtFunanushiNm";
            this.txtFunanushiNm.Style = "font-family: ＭＳ 明朝; font-size: 12pt";
            this.txtFunanushiNm.Text = "txtFunanushiNm";
            this.txtFunanushiNm.Top = 0.01889764F;
            this.txtFunanushiNm.Width = 1.260237F;
            // 
            // txtZenZandaka
            // 
            this.txtZenZandaka.DataField = "ITEM06";
            this.txtZenZandaka.Height = 0.2F;
            this.txtZenZandaka.Left = 3.956701F;
            this.txtZenZandaka.MultiLine = false;
            this.txtZenZandaka.Name = "txtZenZandaka";
            this.txtZenZandaka.OutputFormat = resources.GetString("txtZenZandaka.OutputFormat");
            this.txtZenZandaka.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right";
            this.txtZenZandaka.Text = "txtZenZandaka";
            this.txtZenZandaka.Top = 0.01889764F;
            this.txtZenZandaka.Width = 0.9090548F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSubTotal07,
            this.txtSubTotal08,
            this.txtSubTotal09,
            this.txtSubTotal10,
            this.txtSubTotal12,
            this.txtSubTotal11,
            this.txtSubTotal01,
            this.txtSubTotal02,
            this.txtSubTotal03,
            this.txtSubTotal04,
            this.txtSubTotal05,
            this.txtSubTotal06,
            this.lblGyoshuCd,
            this.lblGyoshuNm,
            this.line3});
            this.groupFooter1.Height = 0.4728346F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // txtSubTotal07
            // 
            this.txtSubTotal07.DataField = "ITEM13";
            this.txtSubTotal07.Height = 0.2F;
            this.txtSubTotal07.Left = 2.824016F;
            this.txtSubTotal07.MultiLine = false;
            this.txtSubTotal07.Name = "txtSubTotal07";
            this.txtSubTotal07.OutputFormat = resources.GetString("txtSubTotal07.OutputFormat");
            this.txtSubTotal07.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal07.SummaryGroup = "groupHeader1";
            this.txtSubTotal07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal07.Text = "txtSubTotal07";
            this.txtSubTotal07.Top = 0.253937F;
            this.txtSubTotal07.Width = 1.025591F;
            // 
            // txtSubTotal08
            // 
            this.txtSubTotal08.DataField = "ITEM14";
            this.txtSubTotal08.Height = 0.2F;
            this.txtSubTotal08.Left = 3.842127F;
            this.txtSubTotal08.MultiLine = false;
            this.txtSubTotal08.Name = "txtSubTotal08";
            this.txtSubTotal08.OutputFormat = resources.GetString("txtSubTotal08.OutputFormat");
            this.txtSubTotal08.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal08.SummaryGroup = "groupHeader1";
            this.txtSubTotal08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal08.Text = "txtSubTotal08";
            this.txtSubTotal08.Top = 0.253937F;
            this.txtSubTotal08.Width = 1.023622F;
            // 
            // txtSubTotal09
            // 
            this.txtSubTotal09.DataField = "ITEM15";
            this.txtSubTotal09.Height = 0.2F;
            this.txtSubTotal09.Left = 4.791733F;
            this.txtSubTotal09.MultiLine = false;
            this.txtSubTotal09.Name = "txtSubTotal09";
            this.txtSubTotal09.OutputFormat = resources.GetString("txtSubTotal09.OutputFormat");
            this.txtSubTotal09.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal09.SummaryGroup = "groupHeader1";
            this.txtSubTotal09.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal09.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal09.Text = "txtSubTotal09";
            this.txtSubTotal09.Top = 0.253937F;
            this.txtSubTotal09.Width = 1.229134F;
            // 
            // txtSubTotal10
            // 
            this.txtSubTotal10.DataField = "ITEM16";
            this.txtSubTotal10.Height = 0.2F;
            this.txtSubTotal10.Left = 6.042914F;
            this.txtSubTotal10.MultiLine = false;
            this.txtSubTotal10.Name = "txtSubTotal10";
            this.txtSubTotal10.OutputFormat = resources.GetString("txtSubTotal10.OutputFormat");
            this.txtSubTotal10.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal10.SummaryGroup = "groupHeader1";
            this.txtSubTotal10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal10.Text = "txtSubTotal10";
            this.txtSubTotal10.Top = 0.253937F;
            this.txtSubTotal10.Width = 1.168899F;
            // 
            // txtSubTotal12
            // 
            this.txtSubTotal12.DataField = "ITEM18";
            this.txtSubTotal12.Height = 0.2F;
            this.txtSubTotal12.Left = 8.316538F;
            this.txtSubTotal12.MultiLine = false;
            this.txtSubTotal12.Name = "txtSubTotal12";
            this.txtSubTotal12.OutputFormat = resources.GetString("txtSubTotal12.OutputFormat");
            this.txtSubTotal12.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal12.SummaryGroup = "groupHeader1";
            this.txtSubTotal12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal12.Text = "txtSubTotal12";
            this.txtSubTotal12.Top = 0.253937F;
            this.txtSubTotal12.Width = 1.105511F;
            // 
            // txtSubTotal11
            // 
            this.txtSubTotal11.DataField = "ITEM07+ITEM09-ITEM08";
            this.txtSubTotal11.Height = 0.2F;
            this.txtSubTotal11.Left = 7.211812F;
            this.txtSubTotal11.MultiLine = false;
            this.txtSubTotal11.Name = "txtSubTotal11";
            this.txtSubTotal11.OutputFormat = resources.GetString("txtSubTotal11.OutputFormat");
            this.txtSubTotal11.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal11.SummaryGroup = "groupHeader1";
            this.txtSubTotal11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal11.Text = "txtSubTotal11";
            this.txtSubTotal11.Top = 0.253937F;
            this.txtSubTotal11.Width = 1.18189F;
            // 
            // txtSubTotal01
            // 
            this.txtSubTotal01.DataField = "ITEM08";
            this.txtSubTotal01.Height = 0.2F;
            this.txtSubTotal01.Left = 2.824016F;
            this.txtSubTotal01.MultiLine = false;
            this.txtSubTotal01.Name = "txtSubTotal01";
            this.txtSubTotal01.OutputFormat = resources.GetString("txtSubTotal01.OutputFormat");
            this.txtSubTotal01.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal01.SummaryGroup = "groupHeader1";
            this.txtSubTotal01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal01.Text = "txtSubTotal01";
            this.txtSubTotal01.Top = 0.05393701F;
            this.txtSubTotal01.Width = 1.025591F;
            // 
            // txtSubTotal02
            // 
            this.txtSubTotal02.DataField = "ITEM06";
            this.txtSubTotal02.Height = 0.2F;
            this.txtSubTotal02.Left = 3.842126F;
            this.txtSubTotal02.MultiLine = false;
            this.txtSubTotal02.Name = "txtSubTotal02";
            this.txtSubTotal02.OutputFormat = resources.GetString("txtSubTotal02.OutputFormat");
            this.txtSubTotal02.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal02.SummaryGroup = "groupHeader1";
            this.txtSubTotal02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal02.Text = "txtSubTotal02";
            this.txtSubTotal02.Top = 0.05393701F;
            this.txtSubTotal02.Width = 1.023623F;
            // 
            // txtSubTotal03
            // 
            this.txtSubTotal03.DataField = "ITEM09";
            this.txtSubTotal03.Height = 0.2F;
            this.txtSubTotal03.Left = 4.791733F;
            this.txtSubTotal03.MultiLine = false;
            this.txtSubTotal03.Name = "txtSubTotal03";
            this.txtSubTotal03.OutputFormat = resources.GetString("txtSubTotal03.OutputFormat");
            this.txtSubTotal03.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal03.SummaryGroup = "groupHeader1";
            this.txtSubTotal03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal03.Text = "txtSubTotal03";
            this.txtSubTotal03.Top = 0.05393701F;
            this.txtSubTotal03.Width = 1.229134F;
            // 
            // txtSubTotal04
            // 
            this.txtSubTotal04.DataField = "ITEM10";
            this.txtSubTotal04.Height = 0.2F;
            this.txtSubTotal04.Left = 6.042914F;
            this.txtSubTotal04.MultiLine = false;
            this.txtSubTotal04.Name = "txtSubTotal04";
            this.txtSubTotal04.OutputFormat = resources.GetString("txtSubTotal04.OutputFormat");
            this.txtSubTotal04.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal04.SummaryGroup = "groupHeader1";
            this.txtSubTotal04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal04.Text = "txtSubTotal04";
            this.txtSubTotal04.Top = 0.05393701F;
            this.txtSubTotal04.Width = 1.168899F;
            // 
            // txtSubTotal05
            // 
            this.txtSubTotal05.DataField = "ITEM07+ITEM09-ITEM08";
            this.txtSubTotal05.Height = 0.2F;
            this.txtSubTotal05.Left = 7.211812F;
            this.txtSubTotal05.MultiLine = false;
            this.txtSubTotal05.Name = "txtSubTotal05";
            this.txtSubTotal05.OutputFormat = resources.GetString("txtSubTotal05.OutputFormat");
            this.txtSubTotal05.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal05.SummaryGroup = "groupHeader1";
            this.txtSubTotal05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal05.Text = "txtSubTotal05";
            this.txtSubTotal05.Top = 0.05393701F;
            this.txtSubTotal05.Width = 1.18189F;
            // 
            // txtSubTotal06
            // 
            this.txtSubTotal06.DataField = "ITEM12";
            this.txtSubTotal06.Height = 0.2F;
            this.txtSubTotal06.Left = 8.316538F;
            this.txtSubTotal06.MultiLine = false;
            this.txtSubTotal06.Name = "txtSubTotal06";
            this.txtSubTotal06.OutputFormat = resources.GetString("txtSubTotal06.OutputFormat");
            this.txtSubTotal06.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtSubTotal06.SummaryGroup = "groupHeader1";
            this.txtSubTotal06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal06.Text = "txtSubTotal06";
            this.txtSubTotal06.Top = 0.05393701F;
            this.txtSubTotal06.Width = 1.105511F;
            // 
            // lblGyoshuCd
            // 
            this.lblGyoshuCd.Height = 0.2F;
            this.lblGyoshuCd.HyperLink = null;
            this.lblGyoshuCd.Left = 1.249606F;
            this.lblGyoshuCd.Name = "lblGyoshuCd";
            this.lblGyoshuCd.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; ddo-char-set: 128";
            this.lblGyoshuCd.Text = "月 計";
            this.lblGyoshuCd.Top = 0.05393701F;
            this.lblGyoshuCd.Width = 0.6425193F;
            // 
            // lblGyoshuNm
            // 
            this.lblGyoshuNm.Height = 0.2F;
            this.lblGyoshuNm.HyperLink = null;
            this.lblGyoshuNm.Left = 1.249606F;
            this.lblGyoshuNm.Name = "lblGyoshuNm";
            this.lblGyoshuNm.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; ddo-char-set: 128";
            this.lblGyoshuNm.Text = "累 計";
            this.lblGyoshuNm.Top = 0.253937F;
            this.lblGyoshuNm.Width = 0.5799211F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0.01220473F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.4539371F;
            this.line3.Width = 9.422049F;
            this.line3.X1 = 0.01220473F;
            this.line3.X2 = 9.434254F;
            this.line3.Y1 = 0.4539371F;
            this.line3.Y2 = 0.4539371F;
            // 
            // HNMR1071R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 9.446198F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHani)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFunanushiNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseibi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseiSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSengetuZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKarikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKashikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShouhizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasseibi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasseisuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKarikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKashikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZenZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKaishaNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateHani;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFunanushiNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHasseibi;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHasseiSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSengetuZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTougetuKarikata;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTougetuKashikata;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShouhizei;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHasseibi;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHasseisuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKarikata;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKashikata;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal06;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunanushiCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunanushiNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZenZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal06;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuNm;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
    }
}
