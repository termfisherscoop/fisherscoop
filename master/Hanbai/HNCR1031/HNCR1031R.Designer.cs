﻿namespace jp.co.fsi.hn.hncr1031
{
    /// <summary>
    /// SectionReport1 の概要の説明です。
    /// </summary>
    partial class HNCR1031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNCR1031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday_tate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtGyoshuCdFr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblBet = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGyoshuCdTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCdFr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCdTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtToday_tate,
            this.txtTitleName,
            this.line1,
            this.txtTitle02,
            this.txtTitle03,
            this.txtTitle01,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.txtGyoshuCdFr,
            this.lblBet,
            this.txtGyoshuCdTo,
            this.txtTotalCount,
            this.txtCompanyName});
            this.pageHeader.Height = 0.9895834F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtToday_tate
            // 
            this.txtToday_tate.Height = 0.1968504F;
            this.txtToday_tate.Left = 5.909449F;
            this.txtToday_tate.MultiLine = false;
            this.txtToday_tate.Name = "txtToday_tate";
            this.txtToday_tate.OutputFormat = resources.GetString("txtToday_tate.OutputFormat");
            this.txtToday_tate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtToday_tate.Text = "yyyy/MM/dd";
            this.txtToday_tate.Top = 0.376378F;
            this.txtToday_tate.Width = 1.181102F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 2.484252F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle; ddo-char-set: 1";
            this.txtTitleName.Text = "魚種マスタ一覧";
            this.txtTitleName.Top = 0F;
            this.txtTitleName.Width = 2.218898F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.631496F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2874016F;
            this.line1.Width = 1.941733F;
            this.line1.X1 = 2.631496F;
            this.line1.X2 = 4.573229F;
            this.line1.Y1 = 0.2874016F;
            this.line1.Y2 = 0.2874016F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.3937008F;
            this.txtTitle02.Left = 1.058661F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.txtTitle02.Text = "名　　　称";
            this.txtTitle02.Top = 0.5929134F;
            this.txtTitle02.Width = 3.005512F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.3937008F;
            this.txtTitle03.Left = 4.08504F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.txtTitle03.Text = "カ　ナ　名";
            this.txtTitle03.Top = 0.5929134F;
            this.txtTitle03.Width = 3.005512F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.3937008F;
            this.txtTitle01.Left = 0.01181102F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.txtTitle01.Text = "コード";
            this.txtTitle01.Top = 0.5929134F;
            this.txtTitle01.Width = 1.04685F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.5929134F;
            this.line2.Width = 7.090551F;
            this.line2.X1 = 0F;
            this.line2.X2 = 7.090551F;
            this.line2.Y1 = 0.5929134F;
            this.line2.Y2 = 0.5929134F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.9866142F;
            this.line3.Width = 7.090549F;
            this.line3.X1 = 0F;
            this.line3.X2 = 7.090549F;
            this.line3.Y1 = 0.9866142F;
            this.line3.Y2 = 0.9866142F;
            // 
            // line4
            // 
            this.line4.Height = 0.3937008F;
            this.line4.Left = 0.003937008F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.5929134F;
            this.line4.Width = 0F;
            this.line4.X1 = 0.003937008F;
            this.line4.X2 = 0.003937008F;
            this.line4.Y1 = 0.5929134F;
            this.line4.Y2 = 0.9866142F;
            // 
            // line5
            // 
            this.line5.Height = 0.3937005F;
            this.line5.Left = 1.054724F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.5929134F;
            this.line5.Width = 0F;
            this.line5.X1 = 1.054724F;
            this.line5.X2 = 1.054724F;
            this.line5.Y1 = 0.5929134F;
            this.line5.Y2 = 0.9866139F;
            // 
            // line6
            // 
            this.line6.Height = 0.3937005F;
            this.line6.Left = 4.075983F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.5929134F;
            this.line6.Width = 9.536743E-07F;
            this.line6.X1 = 4.075984F;
            this.line6.X2 = 4.075983F;
            this.line6.Y1 = 0.5929134F;
            this.line6.Y2 = 0.9866139F;
            // 
            // line7
            // 
            this.line7.Height = 0.3937005F;
            this.line7.Left = 7.086614F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.5929134F;
            this.line7.Width = 0F;
            this.line7.X1 = 7.086614F;
            this.line7.X2 = 7.086614F;
            this.line7.Y1 = 0.5929134F;
            this.line7.Y2 = 0.9866139F;
            // 
            // txtGyoshuCdFr
            // 
            this.txtGyoshuCdFr.DataField = "ITEM02";
            this.txtGyoshuCdFr.Height = 0.1968504F;
            this.txtGyoshuCdFr.Left = 0.03149607F;
            this.txtGyoshuCdFr.MultiLine = false;
            this.txtGyoshuCdFr.Name = "txtGyoshuCdFr";
            this.txtGyoshuCdFr.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 1";
            this.txtGyoshuCdFr.Text = null;
            this.txtGyoshuCdFr.Top = 0.376378F;
            this.txtGyoshuCdFr.Width = 1.270866F;
            // 
            // lblBet
            // 
            this.lblBet.Height = 0.1968504F;
            this.lblBet.HyperLink = null;
            this.lblBet.Left = 1.302362F;
            this.lblBet.Name = "lblBet";
            this.lblBet.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.lblBet.Text = "～";
            this.lblBet.Top = 0.376378F;
            this.lblBet.Width = 0.2110236F;
            // 
            // txtGyoshuCdTo
            // 
            this.txtGyoshuCdTo.DataField = "ITEM03";
            this.txtGyoshuCdTo.Height = 0.1968504F;
            this.txtGyoshuCdTo.Left = 1.544882F;
            this.txtGyoshuCdTo.MultiLine = false;
            this.txtGyoshuCdTo.Name = "txtGyoshuCdTo";
            this.txtGyoshuCdTo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 1";
            this.txtGyoshuCdTo.Text = null;
            this.txtGyoshuCdTo.Top = 0.376378F;
            this.txtGyoshuCdTo.Width = 1.23937F;
            // 
            // txtTotalCount
            // 
            this.txtTotalCount.DataField = "ITEM07";
            this.txtTotalCount.Height = 0.1448327F;
            this.txtTotalCount.Left = 0F;
            this.txtTotalCount.MultiLine = false;
            this.txtTotalCount.Name = "txtTotalCount";
            this.txtTotalCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtTotalCount.Text = null;
            this.txtTotalCount.Top = 0F;
            this.txtTotalCount.Visible = false;
            this.txtTotalCount.Width = 0.6374032F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 4.70315F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtCompanyName.Text = null;
            this.txtCompanyName.Top = 0.07874016F;
            this.txtCompanyName.Width = 2.348032F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.txtValue01,
            this.txtValue02,
            this.txtValue03,
            this.line12});
            this.detail.Height = 0.2350722F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // line8
            // 
            this.line8.Height = 0.2362205F;
            this.line8.Left = 0.003937008F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 0.003937008F;
            this.line8.X2 = 0.003937008F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.2362205F;
            // 
            // line9
            // 
            this.line9.Height = 0.2362205F;
            this.line9.Left = 1.054724F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 1.054724F;
            this.line9.X2 = 1.054724F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.2362205F;
            // 
            // line10
            // 
            this.line10.Height = 0.2362205F;
            this.line10.Left = 4.075984F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 4.075984F;
            this.line10.X2 = 4.075984F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.2362205F;
            // 
            // line11
            // 
            this.line11.Height = 0.2362205F;
            this.line11.Left = 7.086614F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 7.086614F;
            this.line11.X2 = 7.086614F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.2362205F;
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM04";
            this.txtValue01.Height = 0.2362205F;
            this.txtValue01.Left = 0.03149606F;
            this.txtValue01.MultiLine = false;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0F;
            this.txtValue01.Width = 0.9996063F;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM05";
            this.txtValue02.Height = 0.2362205F;
            this.txtValue02.Left = 1.074409F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0F;
            this.txtValue02.Width = 2.974016F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM06";
            this.txtValue03.Height = 0.2362205F;
            this.txtValue03.Left = 4.092914F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue03.Text = null;
            this.txtValue03.Top = 0F;
            this.txtValue03.Width = 2.974016F;
            // 
            // line12
            // 
            this.line12.Height = 0F;
            this.line12.Left = 0F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.2362205F;
            this.line12.Visible = false;
            this.line12.Width = 7.090549F;
            this.line12.X1 = 0F;
            this.line12.X2 = 7.090549F;
            this.line12.Y1 = 0.2362205F;
            this.line12.Y2 = 0.2362205F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // HNCR1031R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5905512F;
            this.PageSettings.Margins.Left = 0.511811F;
            this.PageSettings.Margins.Right = 0.511811F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.090551F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCdFr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCdTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday_tate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuCdFr;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBet;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuCdTo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCount;
    }
}
