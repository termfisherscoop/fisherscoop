﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnse1011
{
    /// <summary>
    /// セリ入力[入力設定](ZMMR1013)
    /// </summary>
    public partial class HNSE1014 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSE1014()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトル非表示
            this.lblTitle.Visible = false;

            // 画面の初期表示
            InitDisp();

            // 初期フォーカス
            this.chk02.Focus();

        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            if (Msg.ConfYesNo("保存しますか？") == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            // 保存処理
            // 内部で保持してるっぽい
            SaveSettings();

            // 画面を閉じる
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 初期情報の表示
        /// </summary>
        private void InitDisp()
        {
            this.chk01.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk01")) ? true : false;
            this.chk02.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk02")) ? true : false;
            this.chk03.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk03")) ? true : false;
            this.chk04.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk04")) ? true : false;
            this.chk05.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk05")) ? true : false;
            this.chk06.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk06")) ? true : false;
            this.chk07.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk07")) ? true : false;
            this.chk08.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk08")) ? true : false;
        }

        /// <summary>
        /// 設定を保存する
        /// </summary>
        private void SaveSettings()
        {
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk01",
                this.chk01.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk02",
                this.chk02.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk03",
                this.chk03.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk04",
                this.chk04.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk05",
                this.chk05.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk06",
                this.chk06.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk07",
                this.chk07.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk08",
                this.chk08.Checked ? "1" : "0");

            // 設定を保存する
            this.Config.SaveConfig();
        }
        #endregion

    }
}
