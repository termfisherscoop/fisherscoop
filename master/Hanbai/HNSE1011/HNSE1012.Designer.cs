﻿namespace jp.co.fsi.hn.hnse1011
{
    partial class HNSE1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblFunanishiNmFr = new System.Windows.Forms.Label();
			this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiCd = new System.Windows.Forms.Label();
			this.lblDayFr = new System.Windows.Forms.Label();
			this.lblMonthFr = new System.Windows.Forms.Label();
			this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblYearFr = new System.Windows.Forms.Label();
			this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoFr = new System.Windows.Forms.Label();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.lblDateBet = new System.Windows.Forms.Label();
			this.lblDayTo = new System.Windows.Forms.Label();
			this.lblMonthTo = new System.Windows.Forms.Label();
			this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblYearTo = new System.Windows.Forms.Label();
			this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoTo = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.lblFunanushiNmTo = new System.Windows.Forms.Label();
			this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.btnEnter = new System.Windows.Forms.Button();
			this.gbxSeisanKbn = new System.Windows.Forms.GroupBox();
			this.rdoBashoNm03 = new System.Windows.Forms.RadioButton();
			this.rdoBashoNm02 = new System.Windows.Forms.RadioButton();
			this.rdoBashoNm01 = new System.Windows.Forms.RadioButton();
			this.lblFr = new System.Windows.Forms.Label();
			this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanKbnNm = new System.Windows.Forms.Label();
			this.lblSeisanKbnNmTitle = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.panel3 = new jp.co.fsi.common.FsiPanel();
			this.panel2 = new jp.co.fsi.common.FsiPanel();
			this.dtDenDateTo = new jp.co.fsi.ClientCommon.FsiDate();
			this.dtDenDateFrom = new jp.co.fsi.ClientCommon.FsiDate();
			this.panel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.gbxSeisanKbn.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(4, 68);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(176, 68);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF2
			// 
			this.btnF2.Location = new System.Drawing.Point(91, 68);
			this.btnF2.Margin = new System.Windows.Forms.Padding(5);
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Location = new System.Drawing.Point(261, 68);
			this.btnF3.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF4
			// 
			this.btnF4.Location = new System.Drawing.Point(347, 68);
			this.btnF4.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF5
			// 
			this.btnF5.Location = new System.Drawing.Point(432, 68);
			this.btnF5.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF7
			// 
			this.btnF7.Location = new System.Drawing.Point(603, 68);
			this.btnF7.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(517, 68);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF8
			// 
			this.btnF8.Location = new System.Drawing.Point(688, 68);
			this.btnF8.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF9
			// 
			this.btnF9.Location = new System.Drawing.Point(773, 68);
			this.btnF9.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF12
			// 
			this.btnF12.Location = new System.Drawing.Point(1031, 68);
			this.btnF12.Margin = new System.Windows.Forms.Padding(5);
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Location = new System.Drawing.Point(944, 68);
			this.btnF11.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF10
			// 
			this.btnF10.Location = new System.Drawing.Point(859, 68);
			this.btnF10.Margin = new System.Windows.Forms.Padding(5);
			// 
			// pnlDebug
			// 
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Location = new System.Drawing.Point(7, 488);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1111, 133);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1134, 31);
			this.lblTitle.Text = "伝票検索";
			// 
			// lblFunanishiNmFr
			// 
			this.lblFunanishiNmFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanishiNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanishiNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanishiNmFr.Location = new System.Drawing.Point(191, 8);
			this.lblFunanishiNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanishiNmFr.Name = "lblFunanishiNmFr";
			this.lblFunanishiNmFr.Size = new System.Drawing.Size(267, 24);
			this.lblFunanishiNmFr.TabIndex = 23;
			this.lblFunanishiNmFr.Tag = "DISPNAME";
			this.lblFunanishiNmFr.Text = "先　頭";
			this.lblFunanishiNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdFr
			// 
			this.txtFunanushiCdFr.AutoSizeFromLength = true;
			this.txtFunanushiCdFr.DisplayLength = null;
			this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCdFr.Location = new System.Drawing.Point(132, 9);
			this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiCdFr.MaxLength = 4;
			this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
			this.txtFunanushiCdFr.Size = new System.Drawing.Size(53, 23);
			this.txtFunanushiCdFr.TabIndex = 5;
			this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
			// 
			// lblFunanushiCd
			// 
			this.lblFunanushiCd.BackColor = System.Drawing.Color.SeaShell;
			this.lblFunanushiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCd.Location = new System.Drawing.Point(0, 0);
			this.lblFunanushiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCd.Name = "lblFunanushiCd";
			this.lblFunanushiCd.Size = new System.Drawing.Size(1105, 40);
			this.lblFunanushiCd.TabIndex = 21;
			this.lblFunanushiCd.Tag = "CHANGE";
			this.lblFunanushiCd.Text = "船 主 C D";
			this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDayFr
			// 
			this.lblDayFr.AutoSize = true;
			this.lblDayFr.BackColor = System.Drawing.Color.Silver;
			this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayFr.Location = new System.Drawing.Point(431, 15);
			this.lblDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDayFr.Name = "lblDayFr";
			this.lblDayFr.Size = new System.Drawing.Size(24, 16);
			this.lblDayFr.TabIndex = 11;
			this.lblDayFr.Tag = "CHANGE";
			this.lblDayFr.Text = "日";
			this.lblDayFr.Visible = false;
			// 
			// lblMonthFr
			// 
			this.lblMonthFr.AutoSize = true;
			this.lblMonthFr.BackColor = System.Drawing.Color.SeaShell;
			this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthFr.Location = new System.Drawing.Point(339, 15);
			this.lblMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonthFr.Name = "lblMonthFr";
			this.lblMonthFr.Size = new System.Drawing.Size(24, 16);
			this.lblMonthFr.TabIndex = 9;
			this.lblMonthFr.Tag = "CHANGE";
			this.lblMonthFr.Text = "月";
			this.lblMonthFr.Visible = false;
			// 
			// txtDayFr
			// 
			this.txtDayFr.AutoSizeFromLength = false;
			this.txtDayFr.DisplayLength = null;
			this.txtDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDayFr.Location = new System.Drawing.Point(365, 10);
			this.txtDayFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDayFr.MaxLength = 2;
			this.txtDayFr.Name = "txtDayFr";
			this.txtDayFr.Size = new System.Drawing.Size(52, 23);
			this.txtDayFr.TabIndex = 10;
			this.txtDayFr.TabStop = false;
			this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayFr.Visible = false;
			// 
			// lblYearFr
			// 
			this.lblYearFr.AutoSize = true;
			this.lblYearFr.BackColor = System.Drawing.Color.SeaShell;
			this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearFr.Location = new System.Drawing.Point(249, 15);
			this.lblYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYearFr.Name = "lblYearFr";
			this.lblYearFr.Size = new System.Drawing.Size(24, 16);
			this.lblYearFr.TabIndex = 7;
			this.lblYearFr.Tag = "CHANGE";
			this.lblYearFr.Text = "年";
			this.lblYearFr.Visible = false;
			// 
			// txtMonthFr
			// 
			this.txtMonthFr.AutoSizeFromLength = false;
			this.txtMonthFr.DisplayLength = null;
			this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonthFr.Location = new System.Drawing.Point(280, 10);
			this.txtMonthFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonthFr.MaxLength = 2;
			this.txtMonthFr.Name = "txtMonthFr";
			this.txtMonthFr.Size = new System.Drawing.Size(52, 23);
			this.txtMonthFr.TabIndex = 8;
			this.txtMonthFr.TabStop = false;
			this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthFr.Visible = false;
			// 
			// txtYearFr
			// 
			this.txtYearFr.AutoSizeFromLength = false;
			this.txtYearFr.DisplayLength = null;
			this.txtYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYearFr.Location = new System.Drawing.Point(191, 10);
			this.txtYearFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtYearFr.MaxLength = 2;
			this.txtYearFr.Name = "txtYearFr";
			this.txtYearFr.Size = new System.Drawing.Size(52, 23);
			this.txtYearFr.TabIndex = 6;
			this.txtYearFr.TabStop = false;
			this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtYearFr.Visible = false;
			// 
			// lblGengoFr
			// 
			this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengoFr.Location = new System.Drawing.Point(133, 10);
			this.lblGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengoFr.Name = "lblGengoFr";
			this.lblGengoFr.Size = new System.Drawing.Size(53, 24);
			this.lblGengoFr.TabIndex = 5;
			this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblGengoFr.Visible = false;
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(13, 153);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(1108, 333);
			this.dgvList.TabIndex = 28;
			this.dgvList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvList_CellMouseDoubleClick);
			this.dgvList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvList_Scroll);
			this.dgvList.Enter += new System.EventHandler(this.dgvList_Enter);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			this.dgvList.Leave += new System.EventHandler(this.dgvList_Leave);
			// 
			// lblDateBet
			// 
			this.lblDateBet.BackColor = System.Drawing.Color.Transparent;
			this.lblDateBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateBet.Location = new System.Drawing.Point(383, 9);
			this.lblDateBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateBet.Name = "lblDateBet";
			this.lblDateBet.Size = new System.Drawing.Size(24, 27);
			this.lblDateBet.TabIndex = 12;
			this.lblDateBet.Tag = "CHANGE";
			this.lblDateBet.Text = "～";
			this.lblDateBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDayTo
			// 
			this.lblDayTo.AutoSize = true;
			this.lblDayTo.BackColor = System.Drawing.Color.Silver;
			this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayTo.Location = new System.Drawing.Point(789, 15);
			this.lblDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDayTo.Name = "lblDayTo";
			this.lblDayTo.Size = new System.Drawing.Size(24, 16);
			this.lblDayTo.TabIndex = 20;
			this.lblDayTo.Tag = "CHANGE";
			this.lblDayTo.Text = "日";
			this.lblDayTo.Visible = false;
			// 
			// lblMonthTo
			// 
			this.lblMonthTo.AutoSize = true;
			this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthTo.Location = new System.Drawing.Point(704, 15);
			this.lblMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonthTo.Name = "lblMonthTo";
			this.lblMonthTo.Size = new System.Drawing.Size(24, 16);
			this.lblMonthTo.TabIndex = 18;
			this.lblMonthTo.Tag = "CHANGE";
			this.lblMonthTo.Text = "月";
			this.lblMonthTo.Visible = false;
			// 
			// txtDayTo
			// 
			this.txtDayTo.AutoSizeFromLength = false;
			this.txtDayTo.DisplayLength = null;
			this.txtDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDayTo.Location = new System.Drawing.Point(735, 10);
			this.txtDayTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDayTo.MaxLength = 2;
			this.txtDayTo.Name = "txtDayTo";
			this.txtDayTo.Size = new System.Drawing.Size(52, 23);
			this.txtDayTo.TabIndex = 19;
			this.txtDayTo.TabStop = false;
			this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayTo.Visible = false;
			// 
			// lblYearTo
			// 
			this.lblYearTo.AutoSize = true;
			this.lblYearTo.BackColor = System.Drawing.Color.Silver;
			this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearTo.Location = new System.Drawing.Point(615, 15);
			this.lblYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYearTo.Name = "lblYearTo";
			this.lblYearTo.Size = new System.Drawing.Size(24, 16);
			this.lblYearTo.TabIndex = 16;
			this.lblYearTo.Tag = "CHANGE";
			this.lblYearTo.Text = "年";
			this.lblYearTo.Visible = false;
			// 
			// txtMonthTo
			// 
			this.txtMonthTo.AutoSizeFromLength = false;
			this.txtMonthTo.DisplayLength = null;
			this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonthTo.Location = new System.Drawing.Point(645, 10);
			this.txtMonthTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonthTo.MaxLength = 2;
			this.txtMonthTo.Name = "txtMonthTo";
			this.txtMonthTo.Size = new System.Drawing.Size(52, 23);
			this.txtMonthTo.TabIndex = 17;
			this.txtMonthTo.TabStop = false;
			this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthTo.Visible = false;
			// 
			// txtYearTo
			// 
			this.txtYearTo.AutoSizeFromLength = false;
			this.txtYearTo.DisplayLength = null;
			this.txtYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYearTo.Location = new System.Drawing.Point(556, 10);
			this.txtYearTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtYearTo.MaxLength = 2;
			this.txtYearTo.Name = "txtYearTo";
			this.txtYearTo.Size = new System.Drawing.Size(52, 23);
			this.txtYearTo.TabIndex = 15;
			this.txtYearTo.TabStop = false;
			this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtYearTo.Visible = false;
			// 
			// lblGengoTo
			// 
			this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengoTo.Location = new System.Drawing.Point(496, 10);
			this.lblGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengoTo.Name = "lblGengoTo";
			this.lblGengoTo.Size = new System.Drawing.Size(53, 24);
			this.lblGengoTo.TabIndex = 14;
			this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblGengoTo.Visible = false;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(463, 7);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(24, 27);
			this.label1.TabIndex = 24;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "～";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiNmTo
			// 
			this.lblFunanushiNmTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiNmTo.Location = new System.Drawing.Point(555, 8);
			this.lblFunanushiNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiNmTo.Name = "lblFunanushiNmTo";
			this.lblFunanushiNmTo.Size = new System.Drawing.Size(267, 24);
			this.lblFunanushiNmTo.TabIndex = 27;
			this.lblFunanushiNmTo.Tag = "DISPNAME";
			this.lblFunanushiNmTo.Text = "最　後";
			this.lblFunanushiNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdTo
			// 
			this.txtFunanushiCdTo.AutoSizeFromLength = true;
			this.txtFunanushiCdTo.DisplayLength = null;
			this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCdTo.Location = new System.Drawing.Point(494, 9);
			this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiCdTo.MaxLength = 4;
			this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
			this.txtFunanushiCdTo.Size = new System.Drawing.Size(53, 23);
			this.txtFunanushiCdTo.TabIndex = 6;
			this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
			this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
			// 
			// btnEnter
			// 
			this.btnEnter.BackColor = System.Drawing.Color.SkyBlue;
			this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEnter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnEnter.ForeColor = System.Drawing.Color.Navy;
			this.btnEnter.Location = new System.Drawing.Point(91, 68);
			this.btnEnter.Margin = new System.Windows.Forms.Padding(4);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new System.Drawing.Size(87, 60);
			this.btnEnter.TabIndex = 906;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n決定";
			this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = false;
			this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
			// 
			// gbxSeisanKbn
			// 
			this.gbxSeisanKbn.Controls.Add(this.rdoBashoNm03);
			this.gbxSeisanKbn.Controls.Add(this.rdoBashoNm02);
			this.gbxSeisanKbn.Controls.Add(this.rdoBashoNm01);
			this.gbxSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
			this.gbxSeisanKbn.ForeColor = System.Drawing.SystemColors.ControlText;
			this.gbxSeisanKbn.Location = new System.Drawing.Point(879, 16);
			this.gbxSeisanKbn.Margin = new System.Windows.Forms.Padding(4);
			this.gbxSeisanKbn.Name = "gbxSeisanKbn";
			this.gbxSeisanKbn.Padding = new System.Windows.Forms.Padding(4);
			this.gbxSeisanKbn.Size = new System.Drawing.Size(149, 116);
			this.gbxSeisanKbn.TabIndex = 24;
			this.gbxSeisanKbn.TabStop = false;
			this.gbxSeisanKbn.Text = "精算区分";
			this.gbxSeisanKbn.Visible = false;
			// 
			// rdoBashoNm03
			// 
			this.rdoBashoNm03.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoBashoNm03.Location = new System.Drawing.Point(21, 87);
			this.rdoBashoNm03.Margin = new System.Windows.Forms.Padding(4);
			this.rdoBashoNm03.Name = "rdoBashoNm03";
			this.rdoBashoNm03.Size = new System.Drawing.Size(89, 23);
			this.rdoBashoNm03.TabIndex = 2;
			this.rdoBashoNm03.TabStop = true;
			this.rdoBashoNm03.Text = "地元";
			this.rdoBashoNm03.UseVisualStyleBackColor = true;
			this.rdoBashoNm03.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoBashoNm_KeyDown);
			// 
			// rdoBashoNm02
			// 
			this.rdoBashoNm02.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoBashoNm02.Location = new System.Drawing.Point(21, 56);
			this.rdoBashoNm02.Margin = new System.Windows.Forms.Padding(4);
			this.rdoBashoNm02.Name = "rdoBashoNm02";
			this.rdoBashoNm02.Size = new System.Drawing.Size(89, 23);
			this.rdoBashoNm02.TabIndex = 1;
			this.rdoBashoNm02.TabStop = true;
			this.rdoBashoNm02.Text = "県外";
			this.rdoBashoNm02.UseVisualStyleBackColor = true;
			this.rdoBashoNm02.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoBashoNm_KeyDown);
			// 
			// rdoBashoNm01
			// 
			this.rdoBashoNm01.Checked = true;
			this.rdoBashoNm01.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoBashoNm01.Location = new System.Drawing.Point(21, 25);
			this.rdoBashoNm01.Margin = new System.Windows.Forms.Padding(4);
			this.rdoBashoNm01.Name = "rdoBashoNm01";
			this.rdoBashoNm01.Size = new System.Drawing.Size(89, 23);
			this.rdoBashoNm01.TabIndex = 0;
			this.rdoBashoNm01.TabStop = true;
			this.rdoBashoNm01.Text = "県内";
			this.rdoBashoNm01.UseVisualStyleBackColor = true;
			this.rdoBashoNm01.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoBashoNm_KeyDown);
			// 
			// lblFr
			// 
			this.lblFr.BackColor = System.Drawing.Color.SeaShell;
			this.lblFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFr.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFr.Location = new System.Drawing.Point(0, 0);
			this.lblFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFr.Name = "lblFr";
			this.lblFr.Size = new System.Drawing.Size(1105, 40);
			this.lblFr.TabIndex = 4;
			this.lblFr.Tag = "CHANGE";
			this.lblFr.Text = "伝 票 日 付";
			this.lblFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanKbn
			// 
			this.txtSeisanKbn.AutoSizeFromLength = true;
			this.txtSeisanKbn.DisplayLength = null;
			this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSeisanKbn.Location = new System.Drawing.Point(87, 9);
			this.txtSeisanKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanKbn.MaxLength = 1;
			this.txtSeisanKbn.Name = "txtSeisanKbn";
			this.txtSeisanKbn.Size = new System.Drawing.Size(67, 23);
			this.txtSeisanKbn.TabIndex = 2;
			this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSeisanKbn_KeyDown);
			this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
			// 
			// lblSeisanKbnNm
			// 
			this.lblSeisanKbnNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanKbnNm.Location = new System.Drawing.Point(162, 8);
			this.lblSeisanKbnNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
			this.lblSeisanKbnNm.Size = new System.Drawing.Size(283, 24);
			this.lblSeisanKbnNm.TabIndex = 3;
			this.lblSeisanKbnNm.Tag = "DISPNAME";
			this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeisanKbnNmTitle
			// 
			this.lblSeisanKbnNmTitle.BackColor = System.Drawing.Color.SeaShell;
			this.lblSeisanKbnNmTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanKbnNmTitle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblSeisanKbnNmTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanKbnNmTitle.Location = new System.Drawing.Point(0, 0);
			this.lblSeisanKbnNmTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanKbnNmTitle.Name = "lblSeisanKbnNmTitle";
			this.lblSeisanKbnNmTitle.Size = new System.Drawing.Size(1105, 40);
			this.lblSeisanKbnNmTitle.TabIndex = 1;
			this.lblSeisanKbnNmTitle.Tag = "CHANGE";
			this.lblSeisanKbnNmTitle.Text = "精算区分";
			this.lblSeisanKbnNmTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 5);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1113, 142);
			this.tableLayoutPanel1.TabIndex = 902;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.txtFunanushiCdFr);
			this.panel3.Controls.Add(this.lblFunanishiNmFr);
			this.panel3.Controls.Add(this.lblFunanushiNmTo);
			this.panel3.Controls.Add(this.label1);
			this.panel3.Controls.Add(this.txtFunanushiCdTo);
			this.panel3.Controls.Add(this.lblFunanushiCd);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(4, 98);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(1105, 40);
			this.panel3.TabIndex = 2;
			this.panel3.Tag = "CHANGE";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.dtDenDateTo);
			this.panel2.Controls.Add(this.dtDenDateFrom);
			this.panel2.Controls.Add(this.lblGengoFr);
			this.panel2.Controls.Add(this.lblGengoTo);
			this.panel2.Controls.Add(this.txtYearFr);
			this.panel2.Controls.Add(this.txtYearTo);
			this.panel2.Controls.Add(this.txtMonthTo);
			this.panel2.Controls.Add(this.txtMonthFr);
			this.panel2.Controls.Add(this.lblYearTo);
			this.panel2.Controls.Add(this.txtDayTo);
			this.panel2.Controls.Add(this.lblMonthFr);
			this.panel2.Controls.Add(this.lblMonthTo);
			this.panel2.Controls.Add(this.lblDateBet);
			this.panel2.Controls.Add(this.lblDayFr);
			this.panel2.Controls.Add(this.lblDayTo);
			this.panel2.Controls.Add(this.txtDayFr);
			this.panel2.Controls.Add(this.lblYearFr);
			this.panel2.Controls.Add(this.lblFr);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(4, 51);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1105, 40);
			this.panel2.TabIndex = 1;
			this.panel2.Tag = "CHANGE";
			// 
			// dtDenDateTo
			// 
			this.dtDenDateTo.BackColor = System.Drawing.Color.SkyBlue;
			this.dtDenDateTo.Day = "";
			this.dtDenDateTo.DbConnect = null;
			this.dtDenDateTo.DispFormat = jp.co.fsi.ClientCommon.DispDateFormat.年月日;
			this.dtDenDateTo.ExecBtn = null;
			this.dtDenDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.dtDenDateTo.Gengo = "";
			this.dtDenDateTo.JpDate = null;
			this.dtDenDateTo.Location = new System.Drawing.Point(414, 7);
			this.dtDenDateTo.Month = "";
			this.dtDenDateTo.Name = "dtDenDateTo";
			this.dtDenDateTo.OpenDialogExe = "";
			this.dtDenDateTo.SeirekiYmd = null;
			this.dtDenDateTo.Size = new System.Drawing.Size(245, 28);
			this.dtDenDateTo.SYear = "";
			this.dtDenDateTo.TabIndex = 4;
			this.dtDenDateTo.TabStop = true;
			this.dtDenDateTo.Tag = "CHANGE";
			this.dtDenDateTo.WYear = "";
			// 
			// dtDenDateFrom
			// 
			this.dtDenDateFrom.BackColor = System.Drawing.Color.SkyBlue;
			this.dtDenDateFrom.Day = "";
			this.dtDenDateFrom.DbConnect = null;
			this.dtDenDateFrom.DispFormat = jp.co.fsi.ClientCommon.DispDateFormat.年月日;
			this.dtDenDateFrom.ExecBtn = null;
			this.dtDenDateFrom.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.dtDenDateFrom.Gengo = "";
			this.dtDenDateFrom.JpDate = null;
			this.dtDenDateFrom.Location = new System.Drawing.Point(133, 7);
			this.dtDenDateFrom.Month = "";
			this.dtDenDateFrom.Name = "dtDenDateFrom";
			this.dtDenDateFrom.OpenDialogExe = "";
			this.dtDenDateFrom.SeirekiYmd = null;
			this.dtDenDateFrom.Size = new System.Drawing.Size(245, 28);
			this.dtDenDateFrom.SYear = "";
			this.dtDenDateFrom.TabIndex = 3;
			this.dtDenDateFrom.TabStop = true;
			this.dtDenDateFrom.Tag = "CHANGE";
			this.dtDenDateFrom.WYear = "";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.txtSeisanKbn);
			this.panel1.Controls.Add(this.lblSeisanKbnNm);
			this.panel1.Controls.Add(this.lblSeisanKbnNmTitle);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(4, 4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1105, 40);
			this.panel1.TabIndex = 0;
			this.panel1.Tag = "CHANGE";
			// 
			// HNSE1012
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1134, 626);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.gbxSeisanKbn);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNSE1012";
			this.ShowFButton = true;
			this.Text = "伝票検索";
			this.Shown += new System.EventHandler(this.HNSE1012_Shown);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.gbxSeisanKbn, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.gbxSeisanKbn.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblFunanishiNmFr;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCd;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthFr;
        private common.controls.FsiTextBox txtDayFr;
        private System.Windows.Forms.Label lblYearFr;
        private common.controls.FsiTextBox txtMonthFr;
        private common.controls.FsiTextBox txtYearFr;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblDateBet;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private common.controls.FsiTextBox txtDayTo;
        private System.Windows.Forms.Label lblYearTo;
        private common.controls.FsiTextBox txtMonthTo;
        private common.controls.FsiTextBox txtYearTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFunanushiNmTo;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        protected System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.GroupBox gbxSeisanKbn;
        private System.Windows.Forms.RadioButton rdoBashoNm03;
        private System.Windows.Forms.RadioButton rdoBashoNm02;
        private System.Windows.Forms.RadioButton rdoBashoNm01;
        private System.Windows.Forms.Label lblFr;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label lblSeisanKbnNm;
        private System.Windows.Forms.Label lblSeisanKbnNmTitle;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel1;
        private jp.co.fsi.common.FsiPanel panel3;
        private jp.co.fsi.common.FsiPanel panel2;
        private jp.co.fsi.common.FsiPanel panel1;
        private ClientCommon.FsiDate dtDenDateFrom;
        private ClientCommon.FsiDate dtDenDateTo;
    }
}