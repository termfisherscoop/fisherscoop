﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1091
{
    /// <summary>
    /// 手数料率マスタ登録(HNCM1092)
    /// </summary>
    public partial class HNCM1092 : BasePgForm
    {
        #region ログ
        readonly static log4net.ILog log =
            log4net.LogManager.GetLogger(
                System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion


        // 精算区分
        public decimal seisanKubun = (decimal)0;

        private bool _lastControlCheck = false;         // 

        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1092()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1092(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // フォームのキャプションにラベルタイトルのtextを設定する
            this.Text = this.lblTitle.Text;
            // タイトルは非表示
            this.lblTitle.Visible = false;
            // Esc,F3,F6のみ表示
            this.ShowFButton = true;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF3.Location = this.btnF2.Location;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnEsc.Visible = true;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            this.ShowFButton = true;

            // 引数：Par1／モード(1:新規、2:変更)、InData：売場コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理（削除処理）
        /// </summary>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            try
            {
                this.Dba.BeginTransaction();

                //// 論理削除
                ArrayList delParams = this.SetHnTesuryoRitsuDelParams();
                //this.Dba.Update("TB_HN_TESURYO_RITSU_MST",
                //    (DbParamCollection)delParams[1],
                //    "KAISHA_CD = @KAISHA_CD AND SEISAN_KUBUN = @SEISAN_KUBUN AND URIBA_CD = @URIBA_CD",
                //    (DbParamCollection)delParams[0]);

                // データ削除
                // 手数料率マスタ
                // Delete版
                this.Dba.Delete("TB_HN_TESURYO_RITSU_MST",
                    "KAISHA_CD = @KAISHA_CD AND SEISAN_KUBUN = @SEISAN_KUBUN AND URIBA_CD = @URIBA_CD",
                    (DbParamCollection)delParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();

                // DialogResultに「OK」をセットし結果を返却
                this.DialogResult = DialogResult.OK;

            }
            catch (Exception e)
            {
                // ロールバック
                this.Dba.Rollback();

                // 失敗通知ダイアログ
                Msg.Error("削除に失敗しました。");
            }
            finally
            {
                this.Close();
            }
        }

        /// <summary>
        /// F6キー押下時処理（登録処理）
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string modeText = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新");
            string msg = modeText + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTori = this.SetCmToriParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 手数料率マスタ
                    this.Dba.Insert("TB_HN_TESURYO_RITSU_MST", (DbParamCollection)alParamsCmTori[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 手数料率マスタ
                    this.Dba.Update("TB_HN_TESURYO_RITSU_MST",
                        (DbParamCollection)alParamsCmTori[1],
                        "KAISHA_CD = @KAISHA_CD AND SEISAN_KUBUN = @SEISAN_KUBUN AND URIBA_CD = @URIBA_CD",
                        (DbParamCollection)alParamsCmTori[0]);

                }

                // トランザクションをコミット
                this.Dba.Commit();

                // DialogResultに「OK」をセットし結果を返却
                this.DialogResult = DialogResult.OK;

            }
            catch (Exception e)
            {
                // ロールバック
                this.Dba.Rollback();

                // 失敗通知ダイアログ
                Msg.Error(modeText + "が失敗しました。");
            }
            finally
            {
                // 閉じる
                this.Close();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidKubun())
            {
                e.Cancel = true;
                this.txtKubun.SelectAll();
            }
        }

        /// <summary>
        /// コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidCd())
            {
                e.Cancel = true;
                this.txtCd.SelectAll();
            }
        }

        /// <summary>
        /// 名称の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNm_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidNm())
            {
                e.Cancel = true;
                this.txtNm.SelectAll();
            }
        }

        /// <summary>
        /// カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidKanaNm())
            {
                e.Cancel = true;
                this.txtKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 手数料率の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTesuryoRitsu_Validating(object sender, CancelEventArgs e)
        {
            this._lastControlCheck = false;
            if (!IsValidTesuryoRitsu())
            {
                e.Cancel = true;
                this.txtTesuryoRitsu.SelectAll();
                return;
            }
            this._lastControlCheck = true;
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled && this._lastControlCheck)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 削除ボタン非表示
            this.btnF3.Enabled = false;

            // 区分に初期フォーカス
            this.txtKubun.Focus();
            this.txtKubun.SelectAll();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");       // 会社コード
            cols.Append(" ,A.SEISAN_KUBUN");  // 精算区分
            cols.Append(" ,A.URIBA_CD");      // 売場コード
            cols.Append(" ,A.URIBA_NM");      // 売場名
            cols.Append(" ,A.URIBA_KANA_NM"); // 売場カナ名
            cols.Append(" ,A.TESURYO");       // 手数料
            //cols.Append(" ,A.SHORI_FLG");     // 処理フラグ

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_TESURYO_RITSU_MST AS A");

            // 会社コードと精算区分と売場区分をパラメータに設定
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 3, this.seisanKubun);
            dpc.SetParam("@URIBA_CD", SqlDbType.Decimal, 5, this.InData);

            StringBuilder where = new StringBuilder();
            where.Append("     A.KAISHA_CD = @KAISHA_CD");       // 会社コード
            where.Append(" AND A.SEISAN_KUBUN = @SEISAN_KUBUN"); // 精算区分
            where.Append(" AND A.URIBA_CD = @URIBA_CD");         // 売場コード
            //where.Append(" AND A.SHORI_FLG != 2");               // 処理フラグ

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    where.ToString(),
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
                return;
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtKubun.Text = Util.ToString(drDispData["SEISAN_KUBUN"]); // 精算区分
            this.txtCd.Text = Util.ToString(drDispData["URIBA_CD"]); // 売場コード
            this.txtNm.Text = Util.ToString(drDispData["URIBA_NM"]); // 売場名称
            this.txtKanaNm.Text = Util.ToString(drDispData["URIBA_KANA_NM"]); // 売場カナ名
            this.txtTesuryoRitsu.Text = Util.ToString(drDispData["TESURYO"]); // 手数料率

            // 区分とコードを入力不可にする
            this.lblKubun.Enabled = false;
            this.txtKubun.Enabled = false;
            this.lblCd.Enabled = false;
            this.txtCd.Enabled = false;

            // 手数料率の存在チェック
            bool ret = NiukeCd_Check(this.txtKubun.Text, this.txtCd.Text);
            // セリ伝票で使われていなければ
            // 削除ボタンを押下可能にする
            this.btnF3.Enabled = ret;

            // 名称に初期フォーカス
            this.txtNm.Focus();
            this.txtNm.SelectAll();
        }

        /// <summary>
        /// 区分の入力チェック
        /// </summary>
        /// <returns></returns>
        private bool IsValidKubun()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKubun.Text) || this.txtKubun.Text == "0")
            {
                this.txtKubun.Text = "1";
            }
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtKubun.Text, this.txtKubun.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値チェック
            else if (!ValChk.IsNumber(this.txtKubun.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 指定最大数値より大きい場合は、指定最大数値として処理
            if (Util.ToInt(this.txtKubun.Text) > 4)
            {
                this.txtKubun.Text = "4";
            }

            // 指定範囲外の値が入力された場合、エラーを表示する
            if (Util.ToInt(this.txtKubun.Text) < 1 || 4 < Util.ToInt(this.txtKubun.Text))
            {
                Msg.Error("指定範囲内で入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtCd.Text))
            {
                this.txtCd.Text = "0";
            }
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtKubun.Text, this.txtKubun.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値チェック
            else if (!ValChk.IsNumber(this.txtCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = this.SetUniqueKeyParams();
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SEISAN_KUBUN = @SEISAN_KUBUN");
            where.Append(" AND URIBA_CD = @URIBA_CD");
            //where.Append(" AND SHORI_FLG != 2");
            DataTable dtUribaCd =
                this.Dba.GetDataTableByConditionWithParams("URIBA_CD",
                    "TB_HN_TESURYO_RITSU_MST", Util.ToString(where), dpc);
            if (0 < dtUribaCd.Rows.Count)
            {
                Msg.Error("既に存在するコードと重複しています。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 名称の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private bool isValidNm()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtNm.Text, this.txtNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKanaNm()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtKanaNm.Text, this.txtKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 手数料率の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTesuryoRitsu()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtTesuryoRitsu.Text))
            {
                this.txtCd.Text = "0.00";
                return true;
            }
            // 9999999.99%より大きい場合は、9999999.99として処理
            //if (!ValChk.IsDecNumWithinLength(this.txtTesuryoRitsu.Text, 7, 2, true))
            if (!ValChk.IsDecNumWithinLength(this.txtTesuryoRitsu.Text, 2, 2, true))
            {
                Msg.Notice("99.99%以内の数値で入力してください。");
                return false;
            }

            // 入力値をフォーマット
            this.txtTesuryoRitsu.Text = Util.FormatNum(this.txtTesuryoRitsu.Text, 2);

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 新規登録モードの場合
            if (MODE_NEW.Equals(this.Par1))
            {
                // 区分チェック
                if (!this.IsValidKubun())
                {
                    this.txtKubun.Focus();
                    return false;
                }

                // コードチェック
                if (!this.IsValidCd())
                {
                    this.txtCd.Focus();
                    return false;
                }
            }

            // 名称のチェック
            if (!isValidNm())
            {
                this.txtNm.Focus();
                return false;
            }


            // カナ名のチェック
            if (!IsValidKanaNm())
            {
                this.txtKanaNm.Focus();
                return false;
            }

            // 手数料率のチェック
            if (!IsValidTesuryoRitsu())
            {
                this.txtTesuryoRitsu.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_CM_TORIHIKISAKIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規登録時
                // 会社コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                // 精算区分を更新パラメータに設定
                updParam.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 3, this.txtKubun.Text);
                // 売場コード
                updParam.SetParam("@URIBA_CD", SqlDbType.Decimal, 5, this.txtCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと取引先コードと売場コードをWhere句のパラメータに設定
                DbParamCollection whereParam = this.SetUniqueKeyParams();
                alParams.Add(whereParam);
            }

            // 取引先名
            updParam.SetParam("@URIBA_NM", SqlDbType.VarChar, 40, this.txtNm.Text);
            // 取引先カナ名
            updParam.SetParam("@URIBA_KANA_NM", SqlDbType.VarChar, 40, this.txtKanaNm.Text);
            // 手数料率
            updParam.SetParam("@TESURYO", SqlDbType.Decimal, 11, this.txtTesuryoRitsu.Text);
            // 更新日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
            // TODO:処理FLGには0を更新だはず
            updParam.SetParam("@SHORI_FLG", SqlDbType.VarChar, 3, "0");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_TESURYO_RITSU_MSTのユニークキー（またはPK）を返す
        /// </summary>
        /// <returns>
        /// </returns>
        private DbParamCollection SetUniqueKeyParams()
        {
            // 会社コードと精算区分と売場区分を削除パラメータに設定
            DbParamCollection dpcDenpyo = new DbParamCollection();
            dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpcDenpyo.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 3, this.txtKubun.Text);
            dpcDenpyo.SetParam("@URIBA_CD", SqlDbType.Decimal, 5, this.txtCd.Text);

            return dpcDenpyo;
        }

        /// <summary>
        /// 手数料マスタ削除用パラメータの設定
        /// </summary>
        /// <returns></returns>
        private ArrayList SetHnTesuryoRitsuDelParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 会社コードと取引先コードと売場コードをWhere句のパラメータに設定
            DbParamCollection whereParam = this.SetUniqueKeyParams();
            alParams.Add(whereParam);

            // 更新日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

            //// TODO:処理FLGには0を更新だはず
            //updParam.SetParam("@SHORI_FLG", SqlDbType.VarChar, 3, "2");

            alParams.Add(updParam);

            return alParams;
        }

        private bool NiukeCd_Check(string SeisanKbn, string UribaCd)
        {
            StringBuilder sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            sql.Append(" SELECT ");
            sql.Append(" COUNT(*) as COUNT");
            sql.Append(" FROM VI_HN_SHIKIRI_MEISAI ");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            sql.Append(" SEISAN_KUBUN = @SEISAN_KUBUN AND ");
            sql.Append(" NIUKENIN_CD = @URIBA_CD ");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 4, SeisanKbn);
            dpc.SetParam("@URIBA_CD", SqlDbType.Decimal, 4, UribaCd);

            DataTable tbl = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (tbl.Rows.Count > 0)
            {
                // カウントが１件でもあれば削除不可にする
                if (Util.ToDecimal( tbl.Rows[0]["COUNT"]) > 0)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}
