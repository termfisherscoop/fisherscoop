﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1091
{
    /// <summary>
    /// 手数料率の登録(HNCM1091)
    /// </summary>
    public partial class HNCM1091 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "荷受人の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1091()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {

            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(598, 450);
                //this.dgvList.Size = new Size(580, 296);
                //this.lblKanaNm.Location = new System.Drawing.Point(12, 14);
                //this.txtKanaNm.Location = new System.Drawing.Point(93, 16);
                //this.dgvList.Location = new System.Drawing.Point(12, 50);
                // Escapeのみ表示とF1のみ表示
                this.ShowFButton = true;
               this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
                this.btnEnter.Text = "Enter" + "\n\r" + "\n\r" + "決定";
                // ラベルタイトルに画面のtextを設定
                this.lblTitle.Text = this.Text;
            }
            //else
            //{
            //    // メニューから呼ばれた場合は、EscapeとF4をクリック許可する（表示はF11まで）
            //    this.ShowFButton = true;
            //    this.btnEsc.Location = this.btnF1.Location;
            //    this.btnF1.Location = this.btnF2.Location;

            //    this.btnEnter.Enabled = false; // 変更ボタンクリック不可
            //    this.btnF1.Visible = true;
            //    this.btnF4.Visible = true;

            //    this.btnF1.Enabled = false;    // 検索ボタンクリック不可
            //    this.btnF4.Enabled = true;     // 追加ボタンクリック可能

            //}

            this.Text = lblTitle.Text;
            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // カナ名にフォーカス
            this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaNm.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        ///   「検索」は、検索キーワード欄にフォーカスを移動する。
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaNm.Focus();
            this.txtKanaNm.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        ///     追加処理を行う
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ手数料率登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 新規登録モードで登録画面を起動
                HNCM1092 frmHNCM1092 = new HNCM1092(MODE_NEW);

                DialogResult result = frmHNCM1092.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    // 一覧再表示
                    this.listRefrash("", 0);
                }
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //TODO:何かチェックが必要なのかもしれない
                // 入力された情報を元に検索する
                SearchData(false);
            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    this.EditTesuryo(Util.ToString(this.dgvList.SelectedRows[0].Cells["売場コード"].Value)
                        , Util.ToDecimal(this.dgvList.SelectedRows[0].Cells["精算区分"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                this.EditTesuryo(Util.ToString(this.dgvList.SelectedRows[0].Cells["売場コード"].Value)
                    , Util.ToDecimal(this.dgvList.SelectedRows[0].Cells["精算区分"].Value));
            }
        }

        /// <summary>
        /// ファンクションボタンの「Enter 変更」」
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, System.EventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                this.EditTesuryo(Util.ToString(this.dgvList.SelectedRows[0].Cells["売場コード"].Value)
                    , Util.ToDecimal(this.dgvList.SelectedRows[0].Cells["精算区分"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 手数料率マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd); // 会社コードは1
            StringBuilder where = new StringBuilder("TM_HN.KAISHA_CD = @KAISHA_CD");
            //where.Append(" AND TM_HN.SHORI_FLG != 2"); // 論理削除されていないもの
            //if (isInitial)
            //{
            //    // 初期処理の場合、検索結果がヒットしないようにあり得ない検索条件を設定する
            //    where.Append(" AND TM_HN.URIBA_CD = -1");
            //}
            //else
            //{
            //    // 初期処理でない場合、カナ名が入力されていれば、カナ名から検索する
            //    if (!ValChk.IsEmpty(this.txtKanaNm.Text))
            //    {
            //        where.Append(" AND TM_HN.URIBA_KANA_NM LIKE @URIBA_KANA_NM");
            //        // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
            //        string kanaNm = "%" + this.txtKanaNm.Text + "%";
            //        dpc.SetParam("@URIBA_KANA_NM", SqlDbType.VarChar, kanaNm.Length, kanaNm);
            //    }
            //}
                // 初期処理でない場合、カナ名が入力されていれば、カナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaNm.Text))
                {
                    where.Append(" AND TM_HN.URIBA_KANA_NM LIKE @URIBA_KANA_NM");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    string kanaNm = "%" + this.txtKanaNm.Text + "%";
                    dpc.SetParam("@URIBA_KANA_NM", SqlDbType.VarChar, kanaNm.Length, kanaNm);
                }
            if (MODE_CD_SRC.Equals(this.Par1) && !ValChk.IsEmpty(this.Par2))
            {
                where.Append(" AND TM_HN.SEISAN_KUBUN = @SEISAN_KUBUN");
                dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, Util.ToInt(this.Par2));
            }

            string cols = "TM_HN.SEISAN_KUBUN AS 精算区分";
            cols += ", TM_HN.URIBA_CD AS 売場コード";
            cols += ", TM_HN.URIBA_NM AS 売場名称";
            cols += ", TM_HN.URIBA_KANA_NM AS 売場カナ名";
            cols += ", TM_HN.TESURYO AS 手数料率";
            string from = "TB_HN_TESURYO_RITSU_MST AS TM_HN";

            DataTable dtShiire =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "TM_HN.SEISAN_KUBUN", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtShiire.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtShiire.Rows.Add(dtShiire.NewRow());

                // 検索一覧が表示されない場合は、Enterボタンをクリック不可にする
                this.btnEnter.Enabled = false;
            }
            else
            {
                // 検索一覧が表示されたら、Enterボタンと検索ボタンをクリック可能にする
                this.btnEnter.Enabled = true;
                this.btnF1.Enabled = true;     // 検索ボタンクリック可能
            }

            this.dgvList.DataSource = dtShiire;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // 明細の列（セル）幅を設定する
            this.dgvList.Columns[0].Width = 90; // 精算区分
            this.dgvList.Columns[1].Width = 100; // 売場コード
            this.dgvList.Columns[2].Width = 148; // 売場名称
            this.dgvList.Columns[3].Width = 140; // 売場カナ名
            this.dgvList.Columns[4].Width = 75; // 手数料率

            // 明細の列（セル）書式を設定する
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        /// <summary>
        /// 手数料率を追加編集する
        /// </summary>
        /// <param name="code">売場コード(空：新規登録、以外：編集)</param>
        /// <param name="seisanKubun">精算区分(0：新規登録、以外：入力値)</param>
        private void EditTesuryo(string code, decimal seisanKubun)
        {
            HNCM1092 frmHNCM1092;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHNCM1092 = new HNCM1092(MODE_NEW);
            }
            else
            {
                // 編集モードで登録画面を起動
                frmHNCM1092 = new HNCM1092(MODE_EDIT);
                frmHNCM1092.InData = code;
                frmHNCM1092.seisanKubun = seisanKubun;
            }

            DialogResult result = frmHNCM1092.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                this.listRefrash(code, seisanKubun);
            }
        }

        /// <summary>
        /// 一覧再表示
        /// </summary>
        /// <param name="code">売場コード(空：新規登録、以外：編集)</param>
        /// <param name="seisanKubun">精算区分(0：新規登録、以外：入力値)</param>
        private void listRefrash(string code, decimal seisanKubun)
        {
            // データを再検索する
            SearchData(false);

            if (!ValChk.IsEmpty(code) && 0 < seisanKubun)
            {
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["売場コード"].Value))
                        && seisanKubun == Util.ToDecimal(this.dgvList.Rows[i].Cells["精算区分"].Value))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }
            }
            // Gridに再度フォーカスをセット
            this.ActiveControl = this.dgvList;
            this.dgvList.Focus();
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["精算区分"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["売場コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["売場名称"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["売場カナ名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["手数料率"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
