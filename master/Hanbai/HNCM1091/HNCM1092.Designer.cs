﻿namespace jp.co.fsi.hn.hncm1091
{
    partial class HNCM1092
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblSelectCd = new System.Windows.Forms.Label();
			this.txtCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKubun = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTesuryoRitsu = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblCd = new System.Windows.Forms.Label();
			this.lblKubun = new System.Windows.Forms.Label();
			this.lblTesuryoRitsu = new System.Windows.Forms.Label();
			this.lblKanaNm = new System.Windows.Forms.Label();
			this.lblNm = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF3
			// 
			this.btnF3.Text = "F3\r\n\r\nさくじょ";
			// 
			// btnF6
			// 
			this.btnF6.Text = "F6\r\n\r\nとうろく";
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(7, 215);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(571, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(561, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblSelectCd
			// 
			this.lblSelectCd.BackColor = System.Drawing.Color.Silver;
			this.lblSelectCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSelectCd.Location = new System.Drawing.Point(251, 2);
			this.lblSelectCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSelectCd.Name = "lblSelectCd";
			this.lblSelectCd.Size = new System.Drawing.Size(277, 24);
			this.lblSelectCd.TabIndex = 2;
			this.lblSelectCd.Tag = "CHANGE";
			this.lblSelectCd.Text = "1:県内 2:県外 3:地元 4:パヤオ";
			this.lblSelectCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCd
			// 
			this.txtCd.AllowDrop = true;
			this.txtCd.AutoSizeFromLength = false;
			this.txtCd.BackColor = System.Drawing.Color.White;
			this.txtCd.DisplayLength = null;
			this.txtCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtCd.Location = new System.Drawing.Point(169, 3);
			this.txtCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtCd.MaxLength = 3;
			this.txtCd.Name = "txtCd";
			this.txtCd.Size = new System.Drawing.Size(76, 23);
			this.txtCd.TabIndex = 4;
			this.txtCd.Text = "0";
			this.txtCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtCd_Validating);
			// 
			// txtKubun
			// 
			this.txtKubun.AutoSizeFromLength = true;
			this.txtKubun.BackColor = System.Drawing.SystemColors.Window;
			this.txtKubun.DisplayLength = null;
			this.txtKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKubun.Location = new System.Drawing.Point(169, 2);
			this.txtKubun.Margin = new System.Windows.Forms.Padding(4);
			this.txtKubun.MaxLength = 1;
			this.txtKubun.Name = "txtKubun";
			this.txtKubun.Size = new System.Drawing.Size(76, 23);
			this.txtKubun.TabIndex = 1;
			this.txtKubun.Text = "1";
			this.txtKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtKubun_Validating);
			// 
			// txtTesuryoRitsu
			// 
			this.txtTesuryoRitsu.AllowDrop = true;
			this.txtTesuryoRitsu.AutoSizeFromLength = false;
			this.txtTesuryoRitsu.BackColor = System.Drawing.Color.White;
			this.txtTesuryoRitsu.DisplayLength = null;
			this.txtTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTesuryoRitsu.Location = new System.Drawing.Point(169, 3);
			this.txtTesuryoRitsu.Margin = new System.Windows.Forms.Padding(4);
			this.txtTesuryoRitsu.MaxLength = 5;
			this.txtTesuryoRitsu.Name = "txtTesuryoRitsu";
			this.txtTesuryoRitsu.Size = new System.Drawing.Size(127, 23);
			this.txtTesuryoRitsu.TabIndex = 10;
			this.txtTesuryoRitsu.Text = "0.00";
			this.txtTesuryoRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTesuryoRitsu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
			this.txtTesuryoRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtTesuryoRitsu_Validating);
			// 
			// txtKanaNm
			// 
			this.txtKanaNm.AllowDrop = true;
			this.txtKanaNm.AutoSizeFromLength = false;
			this.txtKanaNm.BackColor = System.Drawing.Color.White;
			this.txtKanaNm.DisplayLength = null;
			this.txtKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtKanaNm.Location = new System.Drawing.Point(169, 2);
			this.txtKanaNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanaNm.MaxLength = 20;
			this.txtKanaNm.Name = "txtKanaNm";
			this.txtKanaNm.Size = new System.Drawing.Size(319, 23);
			this.txtKanaNm.TabIndex = 8;
			this.txtKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaNm_Validating);
			// 
			// txtNm
			// 
			this.txtNm.AllowDrop = true;
			this.txtNm.AutoSizeFromLength = false;
			this.txtNm.BackColor = System.Drawing.Color.White;
			this.txtNm.DisplayLength = null;
			this.txtNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtNm.Location = new System.Drawing.Point(169, 2);
			this.txtNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtNm.MaxLength = 20;
			this.txtNm.Name = "txtNm";
			this.txtNm.Size = new System.Drawing.Size(319, 23);
			this.txtNm.TabIndex = 6;
			this.txtNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtNm_Validating);
			// 
			// lblCd
			// 
			this.lblCd.BackColor = System.Drawing.Color.Silver;
			this.lblCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCd.Location = new System.Drawing.Point(0, 0);
			this.lblCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCd.Name = "lblCd";
			this.lblCd.Size = new System.Drawing.Size(532, 28);
			this.lblCd.TabIndex = 3;
			this.lblCd.Tag = "CHANGE";
			this.lblCd.Text = "コ　　ー　　ド";
			this.lblCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKubun
			// 
			this.lblKubun.BackColor = System.Drawing.Color.Silver;
			this.lblKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKubun.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKubun.Location = new System.Drawing.Point(0, 0);
			this.lblKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKubun.Name = "lblKubun";
			this.lblKubun.Size = new System.Drawing.Size(532, 28);
			this.lblKubun.TabIndex = 0;
			this.lblKubun.Tag = "CHANGE";
			this.lblKubun.Text = "区　　　　　分";
			this.lblKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTesuryoRitsu
			// 
			this.lblTesuryoRitsu.BackColor = System.Drawing.Color.Silver;
			this.lblTesuryoRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTesuryoRitsu.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTesuryoRitsu.Location = new System.Drawing.Point(0, 0);
			this.lblTesuryoRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTesuryoRitsu.Name = "lblTesuryoRitsu";
			this.lblTesuryoRitsu.Size = new System.Drawing.Size(532, 29);
			this.lblTesuryoRitsu.TabIndex = 9;
			this.lblTesuryoRitsu.Tag = "CHANGE";
			this.lblTesuryoRitsu.Text = "手　数　料　率";
			this.lblTesuryoRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKanaNm
			// 
			this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanaNm.Name = "lblKanaNm";
			this.lblKanaNm.Size = new System.Drawing.Size(532, 28);
			this.lblKanaNm.TabIndex = 7;
			this.lblKanaNm.Tag = "CHANGE";
			this.lblKanaNm.Text = "カ　　ナ　　名";
			this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblNm
			// 
			this.lblNm.BackColor = System.Drawing.Color.Silver;
			this.lblNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNm.Location = new System.Drawing.Point(0, 0);
			this.lblNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNm.Name = "lblNm";
			this.lblNm.Size = new System.Drawing.Size(532, 28);
			this.lblNm.TabIndex = 5;
			this.lblNm.Tag = "CHANGE";
			this.lblNm.Text = "名　　　　　称";
			this.lblNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(12, 35);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 5;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(542, 187);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.txtTesuryoRitsu);
			this.fsiPanel5.Controls.Add(this.lblTesuryoRitsu);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(5, 153);
			this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(532, 29);
			this.fsiPanel5.TabIndex = 4;
			this.fsiPanel5.Tag = "CHANGE";
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtKanaNm);
			this.fsiPanel4.Controls.Add(this.lblKanaNm);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(5, 116);
			this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(532, 28);
			this.fsiPanel4.TabIndex = 3;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtNm);
			this.fsiPanel3.Controls.Add(this.lblNm);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 79);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(532, 28);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtCd);
			this.fsiPanel2.Controls.Add(this.lblCd);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 42);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(532, 28);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtKubun);
			this.fsiPanel1.Controls.Add(this.lblSelectCd);
			this.fsiPanel1.Controls.Add(this.lblKubun);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(532, 28);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNCM1092
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(561, 353);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1092";
			this.ShowFButton = true;
			this.Text = "手数料率の登録";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private System.Windows.Forms.Label lblNm;
        private System.Windows.Forms.Label lblCd;
        private System.Windows.Forms.Label lblKubun;
        private System.Windows.Forms.Label lblTesuryoRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTesuryoRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKubun;
        private System.Windows.Forms.Label lblSelectCd;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}