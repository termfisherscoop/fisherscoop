﻿namespace jp.co.fsi.hn.hncm1091
{
    partial class HNCM1091
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblKanaNm = new System.Windows.Forms.Label();
			this.txtKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.btnEnter = new System.Windows.Forms.Button();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.fsiPanel1.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(5, 5);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(92, 65);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF2
			// 
			this.btnF2.Location = new System.Drawing.Point(180, 65);
			this.btnF2.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF3
			// 
			this.btnF3.Location = new System.Drawing.Point(268, 65);
			this.btnF3.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF4
			// 
			this.btnF4.Location = new System.Drawing.Point(356, 65);
			this.btnF4.Margin = new System.Windows.Forms.Padding(5);
			this.btnF4.Text = "F4\r\n\r\n追加";
			// 
			// btnF5
			// 
			this.btnF5.Location = new System.Drawing.Point(444, 65);
			this.btnF5.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF7
			// 
			this.btnF7.Location = new System.Drawing.Point(620, 65);
			this.btnF7.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(532, 65);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF8
			// 
			this.btnF8.Location = new System.Drawing.Point(708, 65);
			this.btnF8.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF9
			// 
			this.btnF9.Location = new System.Drawing.Point(796, 65);
			this.btnF9.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF12
			// 
			this.btnF12.Location = new System.Drawing.Point(1063, 65);
			this.btnF12.Margin = new System.Windows.Forms.Padding(5);
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Location = new System.Drawing.Point(975, 65);
			this.btnF11.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF10
			// 
			this.btnF10.Location = new System.Drawing.Point(885, 65);
			this.btnF10.Margin = new System.Windows.Forms.Padding(5);
			// 
			// pnlDebug
			// 
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(7, 250);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(589, 145);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(578, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "手数料率の登録";
			// 
			// lblKanaNm
			// 
			this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanaNm.Name = "lblKanaNm";
			this.lblKanaNm.Size = new System.Drawing.Size(554, 31);
			this.lblKanaNm.TabIndex = 0;
			this.lblKanaNm.Tag = "CHANGE";
			this.lblKanaNm.Text = "カ　ナ　名";
			this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKanaNm
			// 
			this.txtKanaNm.AllowDrop = true;
			this.txtKanaNm.AutoSizeFromLength = false;
			this.txtKanaNm.DisplayLength = null;
			this.txtKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtKanaNm.Location = new System.Drawing.Point(137, 4);
			this.txtKanaNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanaNm.MaxLength = 30;
			this.txtKanaNm.Name = "txtKanaNm";
			this.txtKanaNm.Size = new System.Drawing.Size(224, 23);
			this.txtKanaNm.TabIndex = 1;
			this.txtKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanaName_KeyDown);
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(12, 80);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(561, 200);
			this.dgvList.TabIndex = 2;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// btnEnter
			// 
			this.btnEnter.BackColor = System.Drawing.Color.SkyBlue;
			this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEnter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.btnEnter.ForeColor = System.Drawing.Color.Navy;
			this.btnEnter.Location = new System.Drawing.Point(4, 65);
			this.btnEnter.Margin = new System.Windows.Forms.Padding(5);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new System.Drawing.Size(87, 60);
			this.btnEnter.TabIndex = 907;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n変更";
			this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = false;
			this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtKanaNm);
			this.fsiPanel1.Controls.Add(this.lblKanaNm);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(554, 31);
			this.fsiPanel1.TabIndex = 1000;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(9, 35);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 1;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(564, 41);
			this.fsiTableLayoutPanel1.TabIndex = 1001;
			// 
			// HNCM1091
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(578, 407);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1091";
			this.Par1 = "";
			this.Par2 = "";
			this.ShowFButton = true;
			this.Text = "ReportSample";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaNm;
        private System.Windows.Forms.DataGridView dgvList;
        private common.FsiPanel fsiPanel1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        public System.Windows.Forms.Button btnEnter;
    }
}