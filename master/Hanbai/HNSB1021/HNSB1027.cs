﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Text;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsb1021
{
    /// <summary>
    /// 仕訳データ参照<支払データ>(HNSB1027)
    /// </summary>
    public partial class HNSB1027 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// HNSB1021(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        HNSB1021 _pForm;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSB1027(HNSB1021 frm)
        {
            this._pForm = frm;

            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 期間の初期表示
            string tmpTerm = " ";
            string[] aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["SeisanDtFr"]), this.Dba);
            tmpTerm += aryJpDate[5];
            aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["SeisanDtTo"]), this.Dba);
            tmpTerm += " ～ " + aryJpDate[5];
            this.lblTerm.Text = tmpTerm;

            // 行の高さを設定する
            this.dgvList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvList.ColumnHeadersHeight = 20;
            this.dgvList.RowTemplate.Height = 18;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;

            // 表示用にデータを編集
            DataTable dtDBData = GetShiharaiSwkData();
            DataTable dtDisp = EditDataList(dtDBData);

            this.dgvList.DataSource = dtDisp;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 190;
            this.dgvList.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[1].Width = 100;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[2].Width = 80;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[3].Width = 80;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[4].Width = 100;
        }
        #endregion

        #region イベント
        /// <summary>
        /// フォーム表示後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HNSB1027_Shown(object sender, EventArgs e)
        {
            if (this.dgvList.RowCount == 0)
            {
                Msg.Info("該当データがありません。");
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }

        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 仕訳対象の支払データを取得
        /// </summary>
        /// <returns>対象データ</returns>
        private DataTable GetShiharaiSwkData()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  A.HOJO_KAMOKU_CD AS FUNANUSHI_CD ");
            sql.Append(" ,B.SENSHU_NM AS FUNANUSHI ");
            sql.Append(" ,B.ZEIKOMI_MIZUAGE_KINGAKU AS ZEIKOMI_MIZUAGE_KINGAKU ");
            sql.Append(" ,B.KOJO_KOMOKU1 AS KOJO_KOMOKU1 ");
            sql.Append(" ,B.KOJO_KOMOKU2 AS KOJO_KOMOKU2 ");
            sql.Append(" ,A.ZEIKOMI_KINGAKU AS SASHIHIKI_SEISAN_GAKU ");
            sql.Append("FROM ");
            sql.Append("  TB_ZM_SHIWAKE_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("  TB_HN_KOJO_DATA AS B ON ");
            sql.Append("    A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND A.SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND A.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND A.HOJO_KAMOKU_CD = B.SENSHU_CD ");
            sql.Append("AND A.DENPYO_BANGO IN ( ");
            sql.Append("      SELECT ");
            sql.Append("        SHIWAKE_DENPYO_BANGO ");
            sql.Append("      FROM ");
            sql.Append("        TB_HN_ZIDO_SHIWAKE_RIREKI ");
            sql.Append("      WHERE ");
            sql.Append("        KAISHA_CD = @KAISHA_CD ");
            sql.Append("    AND SHISHO_CD = @SHISHO_CD ");
            sql.Append("    AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("    AND DENPYO_KUBUN = @DENPYO_KUBUN ");
            sql.Append("    AND DENPYO_BANGO = @DENPYO_BANGO) ");
            sql.Append("AND B.SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO ");
            sql.Append("AND B.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            sql.Append("AND B.SEISAN_KUBUN = 3 ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(this._pForm.Condition["ShishoCode"]));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, this._pForm.Condition["SakuseiKbn"]);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, this._pForm.PackDpyNo);
            dpc.SetParam("@SEISAN_BI_FR", SqlDbType.DateTime, this._pForm.Condition["SeisanDtFr"]);
            dpc.SetParam("@SEISAN_BI_TO", SqlDbType.DateTime, this._pForm.Condition["SeisanDtTo"]);
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 5, this._pForm.Condition["FunanushiCdFr"]);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 5, this._pForm.Condition["FunanushiCdTo"]);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData">DBから取得したデータ</param>
        /// <returns>表示用のデータ</returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 列の定義を作成
            dtResult.Columns.Add("船　　主", typeof(string));
            dtResult.Columns.Add("税込み水揚金額", typeof(string));
            dtResult.Columns.Add("漁協手数料", typeof(string));
            dtResult.Columns.Add("箱　代", typeof(string));
            dtResult.Columns.Add("差引精算額", typeof(string));

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();
                drResult["船　　主"] = dtData.Rows[i]["FUNANUSHI"];
                drResult["税込み水揚金額"] = Util.FormatNum(dtData.Rows[i]["ZEIKOMI_MIZUAGE_KINGAKU"]);
                drResult["漁協手数料"] = Util.FormatNum(dtData.Rows[i]["KOJO_KOMOKU1"]);
                drResult["箱　代"] = Util.FormatNum(dtData.Rows[i]["KOJO_KOMOKU2"]);
                drResult["差引精算額"] = Util.FormatNum(dtData.Rows[i]["SASHIHIKI_SEISAN_GAKU"]);

                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }
        #endregion
    }
}
