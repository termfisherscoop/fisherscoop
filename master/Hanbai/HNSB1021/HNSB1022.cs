﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsb1021
{
    /// <summary>
    /// 作成済仕訳データ検索(HNSB1022)
    /// </summary>
    public partial class HNSB1022 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// HNSB1021(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        HNSB1021 _pForm;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSB1022(HNSB1021 frm)
        {
            this._pForm = frm;

            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // データ取得のSQLを発行してGridに反映
            DataTable dtList = new DataTable();
            try
            {
                dtList = GetTB_HN_ZIDO_SHIWAKE_RIREKI();
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                return;
            }

            // 取得したデータを表示用に編集
            DataTable dtDsp = EditDataList(dtList);

            this.dgvList.DataSource = dtDsp;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvList.ColumnHeadersHeight = 20;
            this.dgvList.Columns[0].Width = 80;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 200;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[2].Width = 80;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[3].Width = 300;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[4].Width = 300;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[5].Visible = false;
            this.dgvList.Columns[6].Visible = false;
            this.dgvList.Columns[7].Visible = false;
            this.dgvList.Columns[8].Visible = false;
            this.dgvList.Columns[9].Visible = false;
            this.dgvList.Columns[10].Visible = false;
            this.dgvList.Columns[11].Visible = false;
            this.dgvList.Columns[12].Visible = false;
            this.dgvList.Columns[13].Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            ReturnVal();
        }

        /// <summary>
        /// フォーム表示後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Shown(object sender, EventArgs e)
        {
            if (this.dgvList.RowCount == 0)
            {
                Msg.Info("該当データがありません。");
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 自動仕訳履歴のデータを取得
        /// </summary>
        /// <returns>自動仕訳履歴から取得したデータ</returns>
        private DataTable GetTB_HN_ZIDO_SHIWAKE_RIREKI()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" A.DENPYO_BANGO AS 伝票番号,");
            sql.Append(" A.DENPYO_DATE AS 伝票日付,");
            sql.Append(" MIN(A.SHIWAKE_DENPYO_BANGO) AS 仕訳伝票番号,");
            sql.Append(" MAX(A.SHORI_KUBUN) AS 処理区分,");
            sql.Append(" MAX(CASE A.SHORI_KUBUN");
            sql.Append("  WHEN 1 THEN 'セリ'");
            sql.Append("  WHEN 2 THEN '控除'");
            sql.Append("  WHEN 3 THEN '締日基準'");
            sql.Append("  ELSE '' ");
            sql.Append("  END) AS 処理区分名称,");
            sql.Append(" MAX(A.KAISHI_DENPYO_DATE) AS 開始伝票日付,");
            sql.Append(" MAX(A.SHURYO_DENPYO_DATE) AS 終了伝票日付,");
            sql.Append(" MAX(A.KAISHI_SEIKYUSAKI_CD) AS 開始請求先コード,");
            sql.Append(" MAX(CASE");
            sql.Append("  WHEN ISNULL(A.KAISHI_SEIKYUSAKI_CD, '') = '' THEN '先　頭'");
            sql.Append("  WHEN A.KAISHI_SEIKYUSAKI_CD = 0 THEN '先　頭'");
            sql.Append("  ELSE B.TORIHIKISAKI_NM END) AS 開始請求先名,");
            sql.Append(" MAX(A.SHURYO_SEIKYUSAKI_CD) AS 終了請求先コード,");
            sql.Append(" MAX(CASE");
            sql.Append("  WHEN ISNULL(A.SHURYO_SEIKYUSAKI_CD, '') = '' THEN '最　後'");
            sql.Append("  WHEN A.SHURYO_SEIKYUSAKI_CD = 9999 THEN '最　後'");
            sql.Append("  ELSE C.TORIHIKISAKI_NM END) AS 終了請求先名,");
            sql.Append(" MAX(A.TEKIYO_CD) AS 摘要コード,");
            sql.Append(" MAX(A.TEKIYO) AS 摘要名 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_ZIDO_SHIWAKE_RIREKI AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_CM_TORIHIKISAKI AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KAISHI_SEIKYUSAKI_CD = B.TORIHIKISAKI_CD ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_CM_TORIHIKISAKI AS C ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
            sql.Append(" A.SHURYO_SEIKYUSAKI_CD = C.TORIHIKISAKI_CD ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.DENPYO_KUBUN = @DENPYO_KUBUN AND");
            sql.Append(" A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO ");
            sql.Append("GROUP BY");
            sql.Append(" A.DENPYO_DATE,");
            sql.Append(" A.DENPYO_BANGO,");
            sql.Append(" A.SHORI_KUBUN ");
            sql.Append("ORDER BY");
            sql.Append(" A.DENPYO_DATE DESC,");
            sql.Append(" A.DENPYO_BANGO DESC ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(this._pForm.Condition["ShishoCode"]));
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, this._pForm.Condition["SakuseiKbn"]);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 日付を編集する際に用いるワーク
            string[] aryDate;
            string tmpDate;
            // 請求先範囲を編集する際に用いるワーク
            string tmpStr;

            // 列の定義を作成
            dtResult.Columns.Add("仕訳伝票№", typeof(int));
            dtResult.Columns.Add("日 付", typeof(string));
            dtResult.Columns.Add("作成区分", typeof(string));
            dtResult.Columns.Add("セリ仕訳日付範囲", typeof(string));
            dtResult.Columns.Add("仕訳対象船主名", typeof(string));
            dtResult.Columns.Add("返却用_伝票番号", typeof(string));
            dtResult.Columns.Add("返却用_伝票日付", typeof(string));
            dtResult.Columns.Add("返却用_処理区分", typeof(string));
            dtResult.Columns.Add("返却用_開始伝票日付", typeof(string));
            dtResult.Columns.Add("返却用_終了伝票日付", typeof(string));
            dtResult.Columns.Add("返却用_開始請求先コード", typeof(string));
            dtResult.Columns.Add("返却用_終了請求先コード", typeof(string));
            dtResult.Columns.Add("返却用_摘要コード", typeof(string));
            dtResult.Columns.Add("返却用_摘要", typeof(string));

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();
                drResult["仕訳伝票№"] = dtData.Rows[i]["仕訳伝票番号"];
                aryDate = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["伝票日付"]), this.Dba);
                tmpDate = aryDate[0] + Util.ToInt(aryDate[2]).ToString("00") + "年" + aryDate[3].PadLeft(2, ' ') + "月" + aryDate[4].PadLeft(2, ' ') + "日";
                drResult["日 付"] = tmpDate;
                drResult["作成区分"] = dtData.Rows[i]["処理区分名称"];
                aryDate = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["開始伝票日付"]), this.Dba);
                tmpDate = aryDate[0] + Util.ToInt(aryDate[2]).ToString("00") + "年" + aryDate[3].PadLeft(2, ' ') + "月" + aryDate[4].PadLeft(2, ' ') + "日";
                aryDate = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["終了伝票日付"]), this.Dba);
                tmpDate = tmpDate + "～" + aryDate[0] + Util.ToInt(aryDate[2]).ToString("00") + "年" + aryDate[3].PadLeft(2, ' ') + "月" + aryDate[4].PadLeft(2, ' ') + "日";
                drResult["セリ仕訳日付範囲"] = tmpDate;
                if (ValChk.IsEmpty(dtData.Rows[i]["開始請求先コード"]))
                {
                    tmpStr = Util.ToString(dtData.Rows[i]["開始請求先名"]) + "          ";
                }
                else
                {
                    tmpStr = Util.ToString(dtData.Rows[i]["開始請求先名"]) + new String(' ', 20 - Util.GetByteLength(dtData.Rows[i]["開始請求先名"].ToString()));
                }
                tmpStr = tmpStr + "～" + Util.ToString(dtData.Rows[i]["終了請求先名"]);
                drResult["仕訳対象船主名"] = tmpStr;
                drResult["返却用_伝票番号"] = Util.ToString(dtData.Rows[i]["伝票番号"]);
                drResult["返却用_伝票日付"] = Util.ToDateStr(dtData.Rows[i]["伝票日付"]);
                drResult["返却用_処理区分"] = Util.ToString(dtData.Rows[i]["処理区分"]);
                drResult["返却用_開始伝票日付"] = Util.ToDateStr(dtData.Rows[i]["開始伝票日付"]);
                drResult["返却用_終了伝票日付"] = Util.ToDateStr(dtData.Rows[i]["終了伝票日付"]);
                drResult["返却用_開始請求先コード"] = Util.ToString(dtData.Rows[i]["開始請求先コード"]);
                drResult["返却用_終了請求先コード"] = Util.ToString(dtData.Rows[i]["終了請求先コード"]);
                drResult["返却用_摘要コード"] = Util.ToInt(dtData.Rows[i]["摘要コード"]) == 0 ? string.Empty : Util.ToString(dtData.Rows[i]["摘要コード"]);
                drResult["返却用_摘要"] = Util.ToString(dtData.Rows[i]["摘要名"]);
                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[10] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_伝票番号"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_伝票日付"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_処理区分"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_開始伝票日付"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_終了伝票日付"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_開始請求先コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_終了請求先コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_摘要コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_摘要"].Value),
                Util.ToString(this._pForm.Condition["SakuseiKbn"])
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
