﻿namespace jp.co.fsi.hn.hnsb1021
{
    partial class HNSB1025
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.rdbSeikyuDp = new System.Windows.Forms.RadioButton();
			this.rdbTanitsuDp = new System.Windows.Forms.RadioButton();
			this.rdbFukugoDp = new System.Windows.Forms.RadioButton();
			this.lblBumonNm = new System.Windows.Forms.Label();
			this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTekiyo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTekiyoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.gpbShimebi = new System.Windows.Forms.GroupBox();
			this.chkSmbGenHnDp = new System.Windows.Forms.CheckBox();
			this.chkSmbGenToriDp = new System.Windows.Forms.CheckBox();
			this.chkSmbKakeHnDp = new System.Windows.Forms.CheckBox();
			this.chkSmbKakeToriDp = new System.Windows.Forms.CheckBox();
			this.gpbGenkinKake = new System.Windows.Forms.GroupBox();
			this.chkGenKakeGenHnDp = new System.Windows.Forms.CheckBox();
			this.chkGenKakeGenToriDp = new System.Windows.Forms.CheckBox();
			this.chkGenKakeKakeHnDp = new System.Windows.Forms.CheckBox();
			this.chkGenKakeKakeToriDp = new System.Windows.Forms.CheckBox();
			this.gpbGenkin = new System.Windows.Forms.GroupBox();
			this.chkGenGenHnDp = new System.Windows.Forms.CheckBox();
			this.chkGenGenToriDp = new System.Windows.Forms.CheckBox();
			this.chkGenKakeHnDp = new System.Windows.Forms.CheckBox();
			this.chkGenKakeToriDp = new System.Windows.Forms.CheckBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.gpbShimebi.SuspendLayout();
			this.gpbGenkinKake.SuspendLayout();
			this.gpbGenkin.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(4, 65);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(89, 65);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF2
			// 
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Visible = false;
			// 
			// btnF4
			// 
			this.btnF4.Visible = false;
			// 
			// btnF5
			// 
			this.btnF5.Visible = false;
			// 
			// btnF7
			// 
			this.btnF7.Visible = false;
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(175, 65);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF8
			// 
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Visible = false;
			// 
			// btnF12
			// 
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 112);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1177, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1188, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "仕訳データ作成 動作設定";
			// 
			// rdbSeikyuDp
			// 
			this.rdbSeikyuDp.AutoSize = true;
			this.rdbSeikyuDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdbSeikyuDp.Location = new System.Drawing.Point(880, 7);
			this.rdbSeikyuDp.Margin = new System.Windows.Forms.Padding(4);
			this.rdbSeikyuDp.Name = "rdbSeikyuDp";
			this.rdbSeikyuDp.Size = new System.Drawing.Size(266, 20);
			this.rdbSeikyuDp.TabIndex = 2;
			this.rdbSeikyuDp.TabStop = true;
			this.rdbSeikyuDp.Tag = "CHANGE";
			this.rdbSeikyuDp.Text = "請求先毎に仕訳伝票を作成する。";
			this.rdbSeikyuDp.UseVisualStyleBackColor = true;
			// 
			// rdbTanitsuDp
			// 
			this.rdbTanitsuDp.AutoSize = true;
			this.rdbTanitsuDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdbTanitsuDp.Location = new System.Drawing.Point(509, 7);
			this.rdbTanitsuDp.Margin = new System.Windows.Forms.Padding(4);
			this.rdbTanitsuDp.Name = "rdbTanitsuDp";
			this.rdbTanitsuDp.Size = new System.Drawing.Size(362, 20);
			this.rdbTanitsuDp.TabIndex = 1;
			this.rdbTanitsuDp.TabStop = true;
			this.rdbTanitsuDp.Tag = "CHANGE";
			this.rdbTanitsuDp.Text = "一枚の仕訳伝票で、単一仕訳伝票を作成する。";
			this.rdbTanitsuDp.UseVisualStyleBackColor = true;
			// 
			// rdbFukugoDp
			// 
			this.rdbFukugoDp.AutoSize = true;
			this.rdbFukugoDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdbFukugoDp.Location = new System.Drawing.Point(140, 7);
			this.rdbFukugoDp.Margin = new System.Windows.Forms.Padding(4);
			this.rdbFukugoDp.Name = "rdbFukugoDp";
			this.rdbFukugoDp.Size = new System.Drawing.Size(362, 20);
			this.rdbFukugoDp.TabIndex = 0;
			this.rdbFukugoDp.TabStop = true;
			this.rdbFukugoDp.Tag = "CHANGE";
			this.rdbFukugoDp.Text = "一枚の仕訳伝票で、複合仕訳伝票を作成する。";
			this.rdbFukugoDp.UseVisualStyleBackColor = true;
			// 
			// lblBumonNm
			// 
			this.lblBumonNm.BackColor = System.Drawing.Color.Silver;
			this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBumonNm.Location = new System.Drawing.Point(191, 4);
			this.lblBumonNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBumonNm.Name = "lblBumonNm";
			this.lblBumonNm.Size = new System.Drawing.Size(288, 27);
			this.lblBumonNm.TabIndex = 1;
			this.lblBumonNm.Tag = "DISPNAME";
			this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBumonCd
			// 
			this.txtBumonCd.AutoSizeFromLength = true;
			this.txtBumonCd.DisplayLength = null;
			this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtBumonCd.Location = new System.Drawing.Point(137, 6);
			this.txtBumonCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtBumonCd.MaxLength = 4;
			this.txtBumonCd.Name = "txtBumonCd";
			this.txtBumonCd.Size = new System.Drawing.Size(44, 23);
			this.txtBumonCd.TabIndex = 0;
			this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
			// 
			// txtTekiyo
			// 
			this.txtTekiyo.AutoSizeFromLength = true;
			this.txtTekiyo.DisplayLength = null;
			this.txtTekiyo.Enabled = false;
			this.txtTekiyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTekiyo.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtTekiyo.Location = new System.Drawing.Point(191, 6);
			this.txtTekiyo.Margin = new System.Windows.Forms.Padding(4);
			this.txtTekiyo.MaxLength = 30;
			this.txtTekiyo.Name = "txtTekiyo";
			this.txtTekiyo.Size = new System.Drawing.Size(287, 23);
			this.txtTekiyo.TabIndex = 1;
			this.txtTekiyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo_Validating);
			// 
			// txtTekiyoCd
			// 
			this.txtTekiyoCd.AutoSizeFromLength = true;
			this.txtTekiyoCd.DisplayLength = null;
			this.txtTekiyoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTekiyoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTekiyoCd.Location = new System.Drawing.Point(137, 6);
			this.txtTekiyoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtTekiyoCd.MaxLength = 4;
			this.txtTekiyoCd.Name = "txtTekiyoCd";
			this.txtTekiyoCd.Size = new System.Drawing.Size(44, 23);
			this.txtTekiyoCd.TabIndex = 0;
			this.txtTekiyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// gpbShimebi
			// 
			this.gpbShimebi.Controls.Add(this.chkSmbGenHnDp);
			this.gpbShimebi.Controls.Add(this.chkSmbGenToriDp);
			this.gpbShimebi.Controls.Add(this.chkSmbKakeHnDp);
			this.gpbShimebi.Controls.Add(this.chkSmbKakeToriDp);
			this.gpbShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
			this.gpbShimebi.Location = new System.Drawing.Point(496, 348);
			this.gpbShimebi.Margin = new System.Windows.Forms.Padding(4);
			this.gpbShimebi.Name = "gpbShimebi";
			this.gpbShimebi.Padding = new System.Windows.Forms.Padding(4);
			this.gpbShimebi.Size = new System.Drawing.Size(643, 69);
			this.gpbShimebi.TabIndex = 2;
			this.gpbShimebi.TabStop = false;
			this.gpbShimebi.Text = "締日基準";
			this.gpbShimebi.Visible = false;
			// 
			// chkSmbGenHnDp
			// 
			this.chkSmbGenHnDp.AutoSize = true;
			this.chkSmbGenHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkSmbGenHnDp.Location = new System.Drawing.Point(459, 29);
			this.chkSmbGenHnDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkSmbGenHnDp.Name = "chkSmbGenHnDp";
			this.chkSmbGenHnDp.Size = new System.Drawing.Size(123, 20);
			this.chkSmbGenHnDp.TabIndex = 3;
			this.chkSmbGenHnDp.Text = "現金返品伝票";
			this.chkSmbGenHnDp.UseVisualStyleBackColor = true;
			// 
			// chkSmbGenToriDp
			// 
			this.chkSmbGenToriDp.AutoSize = true;
			this.chkSmbGenToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkSmbGenToriDp.Location = new System.Drawing.Point(301, 29);
			this.chkSmbGenToriDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkSmbGenToriDp.Name = "chkSmbGenToriDp";
			this.chkSmbGenToriDp.Size = new System.Drawing.Size(123, 20);
			this.chkSmbGenToriDp.TabIndex = 2;
			this.chkSmbGenToriDp.Text = "現金取引伝票";
			this.chkSmbGenToriDp.UseVisualStyleBackColor = true;
			// 
			// chkSmbKakeHnDp
			// 
			this.chkSmbKakeHnDp.AutoSize = true;
			this.chkSmbKakeHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkSmbKakeHnDp.Location = new System.Drawing.Point(155, 29);
			this.chkSmbKakeHnDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkSmbKakeHnDp.Name = "chkSmbKakeHnDp";
			this.chkSmbKakeHnDp.Size = new System.Drawing.Size(107, 20);
			this.chkSmbKakeHnDp.TabIndex = 1;
			this.chkSmbKakeHnDp.Text = "掛返品伝票";
			this.chkSmbKakeHnDp.UseVisualStyleBackColor = true;
			// 
			// chkSmbKakeToriDp
			// 
			this.chkSmbKakeToriDp.AutoSize = true;
			this.chkSmbKakeToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkSmbKakeToriDp.Location = new System.Drawing.Point(9, 29);
			this.chkSmbKakeToriDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkSmbKakeToriDp.Name = "chkSmbKakeToriDp";
			this.chkSmbKakeToriDp.Size = new System.Drawing.Size(107, 20);
			this.chkSmbKakeToriDp.TabIndex = 0;
			this.chkSmbKakeToriDp.Text = "掛取引伝票";
			this.chkSmbKakeToriDp.UseVisualStyleBackColor = true;
			// 
			// gpbGenkinKake
			// 
			this.gpbGenkinKake.Controls.Add(this.chkGenKakeGenHnDp);
			this.gpbGenkinKake.Controls.Add(this.chkGenKakeGenToriDp);
			this.gpbGenkinKake.Controls.Add(this.chkGenKakeKakeHnDp);
			this.gpbGenkinKake.Controls.Add(this.chkGenKakeKakeToriDp);
			this.gpbGenkinKake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
			this.gpbGenkinKake.Location = new System.Drawing.Point(545, 271);
			this.gpbGenkinKake.Margin = new System.Windows.Forms.Padding(4);
			this.gpbGenkinKake.Name = "gpbGenkinKake";
			this.gpbGenkinKake.Padding = new System.Windows.Forms.Padding(4);
			this.gpbGenkinKake.Size = new System.Drawing.Size(643, 69);
			this.gpbGenkinKake.TabIndex = 1;
			this.gpbGenkinKake.TabStop = false;
			this.gpbGenkinKake.Text = "現金、掛取引";
			this.gpbGenkinKake.Visible = false;
			// 
			// chkGenKakeGenHnDp
			// 
			this.chkGenKakeGenHnDp.AutoSize = true;
			this.chkGenKakeGenHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkGenKakeGenHnDp.Location = new System.Drawing.Point(459, 29);
			this.chkGenKakeGenHnDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkGenKakeGenHnDp.Name = "chkGenKakeGenHnDp";
			this.chkGenKakeGenHnDp.Size = new System.Drawing.Size(123, 20);
			this.chkGenKakeGenHnDp.TabIndex = 3;
			this.chkGenKakeGenHnDp.Text = "現金返品伝票";
			this.chkGenKakeGenHnDp.UseVisualStyleBackColor = true;
			// 
			// chkGenKakeGenToriDp
			// 
			this.chkGenKakeGenToriDp.AutoSize = true;
			this.chkGenKakeGenToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkGenKakeGenToriDp.Location = new System.Drawing.Point(301, 29);
			this.chkGenKakeGenToriDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkGenKakeGenToriDp.Name = "chkGenKakeGenToriDp";
			this.chkGenKakeGenToriDp.Size = new System.Drawing.Size(123, 20);
			this.chkGenKakeGenToriDp.TabIndex = 2;
			this.chkGenKakeGenToriDp.Text = "現金取引伝票";
			this.chkGenKakeGenToriDp.UseVisualStyleBackColor = true;
			// 
			// chkGenKakeKakeHnDp
			// 
			this.chkGenKakeKakeHnDp.AutoSize = true;
			this.chkGenKakeKakeHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkGenKakeKakeHnDp.Location = new System.Drawing.Point(155, 29);
			this.chkGenKakeKakeHnDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkGenKakeKakeHnDp.Name = "chkGenKakeKakeHnDp";
			this.chkGenKakeKakeHnDp.Size = new System.Drawing.Size(107, 20);
			this.chkGenKakeKakeHnDp.TabIndex = 1;
			this.chkGenKakeKakeHnDp.Text = "掛返品伝票";
			this.chkGenKakeKakeHnDp.UseVisualStyleBackColor = true;
			// 
			// chkGenKakeKakeToriDp
			// 
			this.chkGenKakeKakeToriDp.AutoSize = true;
			this.chkGenKakeKakeToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkGenKakeKakeToriDp.Location = new System.Drawing.Point(9, 29);
			this.chkGenKakeKakeToriDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkGenKakeKakeToriDp.Name = "chkGenKakeKakeToriDp";
			this.chkGenKakeKakeToriDp.Size = new System.Drawing.Size(107, 20);
			this.chkGenKakeKakeToriDp.TabIndex = 0;
			this.chkGenKakeKakeToriDp.Text = "掛取引伝票";
			this.chkGenKakeKakeToriDp.UseVisualStyleBackColor = true;
			// 
			// gpbGenkin
			// 
			this.gpbGenkin.Controls.Add(this.chkGenGenHnDp);
			this.gpbGenkin.Controls.Add(this.chkGenGenToriDp);
			this.gpbGenkin.Controls.Add(this.chkGenKakeHnDp);
			this.gpbGenkin.Controls.Add(this.chkGenKakeToriDp);
			this.gpbGenkin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
			this.gpbGenkin.Location = new System.Drawing.Point(536, 194);
			this.gpbGenkin.Margin = new System.Windows.Forms.Padding(4);
			this.gpbGenkin.Name = "gpbGenkin";
			this.gpbGenkin.Padding = new System.Windows.Forms.Padding(4);
			this.gpbGenkin.Size = new System.Drawing.Size(643, 69);
			this.gpbGenkin.TabIndex = 0;
			this.gpbGenkin.TabStop = false;
			this.gpbGenkin.Text = "現金取引";
			this.gpbGenkin.Visible = false;
			// 
			// chkGenGenHnDp
			// 
			this.chkGenGenHnDp.AutoSize = true;
			this.chkGenGenHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkGenGenHnDp.Location = new System.Drawing.Point(459, 28);
			this.chkGenGenHnDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkGenGenHnDp.Name = "chkGenGenHnDp";
			this.chkGenGenHnDp.Size = new System.Drawing.Size(123, 20);
			this.chkGenGenHnDp.TabIndex = 3;
			this.chkGenGenHnDp.Text = "現金返品伝票";
			this.chkGenGenHnDp.UseVisualStyleBackColor = true;
			// 
			// chkGenGenToriDp
			// 
			this.chkGenGenToriDp.AutoSize = true;
			this.chkGenGenToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkGenGenToriDp.Location = new System.Drawing.Point(301, 28);
			this.chkGenGenToriDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkGenGenToriDp.Name = "chkGenGenToriDp";
			this.chkGenGenToriDp.Size = new System.Drawing.Size(123, 20);
			this.chkGenGenToriDp.TabIndex = 2;
			this.chkGenGenToriDp.Text = "現金取引伝票";
			this.chkGenGenToriDp.UseVisualStyleBackColor = true;
			// 
			// chkGenKakeHnDp
			// 
			this.chkGenKakeHnDp.AutoSize = true;
			this.chkGenKakeHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkGenKakeHnDp.Location = new System.Drawing.Point(155, 28);
			this.chkGenKakeHnDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkGenKakeHnDp.Name = "chkGenKakeHnDp";
			this.chkGenKakeHnDp.Size = new System.Drawing.Size(107, 20);
			this.chkGenKakeHnDp.TabIndex = 1;
			this.chkGenKakeHnDp.Text = "掛返品伝票";
			this.chkGenKakeHnDp.UseVisualStyleBackColor = true;
			// 
			// chkGenKakeToriDp
			// 
			this.chkGenKakeToriDp.AutoSize = true;
			this.chkGenKakeToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkGenKakeToriDp.Location = new System.Drawing.Point(9, 28);
			this.chkGenKakeToriDp.Margin = new System.Windows.Forms.Padding(4);
			this.chkGenKakeToriDp.Name = "chkGenKakeToriDp";
			this.chkGenKakeToriDp.Size = new System.Drawing.Size(107, 20);
			this.chkGenKakeToriDp.TabIndex = 0;
			this.chkGenKakeToriDp.Text = "掛取引伝票";
			this.chkGenKakeToriDp.UseVisualStyleBackColor = true;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(17, 34);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1162, 121);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.rdbSeikyuDp);
			this.fsiPanel1.Controls.Add(this.rdbFukugoDp);
			this.fsiPanel1.Controls.Add(this.rdbTanitsuDp);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(1154, 32);
			this.fsiPanel1.TabIndex = 0;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtBumonCd);
			this.fsiPanel2.Controls.Add(this.lblBumonNm);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 43);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(1154, 32);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtTekiyo);
			this.fsiPanel3.Controls.Add(this.txtTekiyoCd);
			this.fsiPanel3.Controls.Add(this.label3);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 82);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(1154, 35);
			this.fsiPanel3.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(1154, 32);
			this.label1.TabIndex = 0;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "仕訳伝票作成";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(1154, 32);
			this.label2.TabIndex = 1;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "省略時設定部門";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(1154, 35);
			this.label3.TabIndex = 1;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "摘要CD";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// HNSB1025
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1188, 256);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.gpbShimebi);
			this.Controls.Add(this.gpbGenkinKake);
			this.Controls.Add(this.gpbGenkin);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNSB1025";
			this.ShowFButton = true;
			this.ShowTitle = false;
			this.Text = "仕訳データ作成 動作設定";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.gpbGenkin, 0);
			this.Controls.SetChildIndex(this.gpbGenkinKake, 0);
			this.Controls.SetChildIndex(this.gpbShimebi, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.gpbShimebi.ResumeLayout(false);
			this.gpbShimebi.PerformLayout();
			this.gpbGenkinKake.ResumeLayout(false);
			this.gpbGenkinKake.PerformLayout();
			this.gpbGenkin.ResumeLayout(false);
			this.gpbGenkin.PerformLayout();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton rdbFukugoDp;
        private System.Windows.Forms.RadioButton rdbTanitsuDp;
        private System.Windows.Forms.RadioButton rdbSeikyuDp;
        private common.controls.FsiTextBox txtTekiyo;
        private common.controls.FsiTextBox txtTekiyoCd;
        private System.Windows.Forms.Label lblBumonNm;
        private common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.GroupBox gpbShimebi;
        private System.Windows.Forms.CheckBox chkSmbGenHnDp;
        private System.Windows.Forms.CheckBox chkSmbGenToriDp;
        private System.Windows.Forms.CheckBox chkSmbKakeHnDp;
        private System.Windows.Forms.CheckBox chkSmbKakeToriDp;
        private System.Windows.Forms.GroupBox gpbGenkinKake;
        private System.Windows.Forms.CheckBox chkGenKakeGenHnDp;
        private System.Windows.Forms.CheckBox chkGenKakeGenToriDp;
        private System.Windows.Forms.CheckBox chkGenKakeKakeHnDp;
        private System.Windows.Forms.CheckBox chkGenKakeKakeToriDp;
        private System.Windows.Forms.GroupBox gpbGenkin;
        private System.Windows.Forms.CheckBox chkGenGenHnDp;
        private System.Windows.Forms.CheckBox chkGenGenToriDp;
        private System.Windows.Forms.CheckBox chkGenKakeHnDp;
        private System.Windows.Forms.CheckBox chkGenKakeToriDp;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
	}
}