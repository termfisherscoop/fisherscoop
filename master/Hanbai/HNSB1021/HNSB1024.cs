﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Text;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsb1021
{
    /// <summary>
    /// 仕訳データ参照<控除データ>(HNSB1024)
    /// </summary>
    public partial class HNSB1024 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// HNSB1021(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        HNSB1021 _pForm;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSB1024(HNSB1021 frm)
        {
            this._pForm = frm;

            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 期間の初期表示
            string tmpTerm = " ";
            string[] aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["SeisanDtFr"]), this.Dba);
            tmpTerm += aryJpDate[5];
            aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["SeisanDtTo"]), this.Dba);
            tmpTerm += " ～ " + aryJpDate[5];
            this.lblTerm.Text = tmpTerm;

            // 行の高さを設定する
            this.dgvList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvList.ColumnHeadersHeight = 20;
            this.dgvList.RowTemplate.Height = 18;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;

            // 控除設定を取得する
            DataTable dtKojoKmkStg = GetKojoKmkStg();

            // 表示用にデータを編集
            DataTable dtDBData = GetKojoSwkData();
            DataTable dtDisp = EditDataList(dtDBData);

            this.dgvList.DataSource = dtDisp;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            DataRow[] aryKojoSetting;

            this.dgvList.Columns[0].Width = 190;
            this.dgvList.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[1].Width = 80;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            // 設定に従って見出しを表示する
            for (int i = 1; i <= 35; i++)
            {
                // 対象のカラムの設定有無を確認
                aryKojoSetting = dtKojoKmkStg.Select("SETTEI_CD = " + Util.ToString(i));
                if (aryKojoSetting.Length > 0)
                {
                    this.dgvList.Columns[(i + 1)].HeaderText = Util.ToString(aryKojoSetting[0]["KOJO_KOMOKU_NM"]);
                }
                else
                {
                    this.dgvList.Columns[(i + 1)].HeaderText = string.Empty;
                }

                this.dgvList.Columns[(i + 1)].Width = 80;
                this.dgvList.Columns[(i + 1)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// フォーム表示後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HNSB1024_Shown(object sender, EventArgs e)
        {
            if (this.dgvList.RowCount == 0)
            {
                Msg.Info("該当データがありません。");
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }

        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 控除科目設定を取得
        /// </summary>
        /// <returns>控除科目設定</returns>
        private DataTable GetKojoKmkStg()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(this._pForm.Condition["ShishoCode"]));

            //DataTable dtResult = this.Dba.GetDataTableByConditionWithParams("SETTEI_CD, KOJO_KOMOKU_NM",
            //    "TB_HN_KOJO_KAMOKU_SETTEI", "KAISHA_CD = @KAISHA_CD", "SETTEI_CD", dpc);
            DataTable dtResult = this.Dba.GetDataTableByConditionWithParams("SETTEI_CD, KOJO_KOMOKU_NM",
                "TB_HN_KOJO_KAMOKU_SETTEI", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD", "SETTEI_CD", dpc);

            return dtResult;
        }

        /// <summary>
        /// 仕訳対象の控除データを取得
        /// </summary>
        /// <returns>対象データ</returns>
        private DataTable GetKojoSwkData()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  SENSHU_CD ");
            sql.Append(" ,GROUPING(SENSHU_CD) as gp ");
            sql.Append(" ,SPACE(4 - LEN(SENSHU_CD)) + CAST(SENSHU_CD AS VARCHAR) + ' ' + MIN(SENSHU_NM) AS 船主 ");
            sql.Append(" ,SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU2) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU4) + SUM(KOJO_KOMOKU5) ");
            sql.Append("  + SUM(KOJO_KOMOKU6) + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) ");
            sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) ");
            sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) ");
            sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) ");
            sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) ");
            sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) AS 合計 ");
            sql.Append(" ,SUM(KOJO_KOMOKU1) AS kj1 ");
            sql.Append(" ,SUM(KOJO_KOMOKU2) AS kj2 ");
            sql.Append(" ,SUM(KOJO_KOMOKU3) AS kj3 ");
            sql.Append(" ,SUM(KOJO_KOMOKU4) AS kj4 ");
            sql.Append(" ,SUM(KOJO_KOMOKU5) AS kj5 ");
            sql.Append(" ,SUM(KOJO_KOMOKU6) AS kj6 ");
            sql.Append(" ,SUM(KOJO_KOMOKU7) AS kj7 ");
            sql.Append(" ,SUM(KOJO_KOMOKU8) AS kj8 ");
            sql.Append(" ,SUM(KOJO_KOMOKU9) AS kj9 ");
            sql.Append(" ,SUM(KOJO_KOMOKU10) AS kj10 ");
            sql.Append(" ,SUM(KOJO_KOMOKU11) AS kj11 ");
            sql.Append(" ,SUM(KOJO_KOMOKU12) AS kj12 ");
            sql.Append(" ,SUM(KOJO_KOMOKU13) AS kj13 ");
            sql.Append(" ,SUM(KOJO_KOMOKU14) AS kj14 ");
            sql.Append(" ,SUM(KOJO_KOMOKU15) AS kj15 ");
            sql.Append(" ,SUM(KOJO_KOMOKU16) AS kj16 ");
            sql.Append(" ,SUM(KOJO_KOMOKU17) AS kj17 ");
            sql.Append(" ,SUM(KOJO_KOMOKU18) AS kj18 ");
            sql.Append(" ,SUM(KOJO_KOMOKU19) AS kj19 ");
            sql.Append(" ,SUM(KOJO_KOMOKU20) AS kj20 ");
            sql.Append(" ,SUM(KOJO_KOMOKU21) AS kj21 ");
            sql.Append(" ,SUM(KOJO_KOMOKU22) AS kj22 ");
            sql.Append(" ,SUM(KOJO_KOMOKU23) AS kj23 ");
            sql.Append(" ,SUM(KOJO_KOMOKU24) AS kj24 ");
            sql.Append(" ,SUM(KOJO_KOMOKU25) AS kj25 ");
            sql.Append(" ,SUM(KOJO_KOMOKU26) AS kj26 ");
            sql.Append(" ,SUM(KOJO_KOMOKU27) AS kj27 ");
            sql.Append(" ,SUM(KOJO_KOMOKU28) AS kj28 ");
            sql.Append(" ,SUM(KOJO_KOMOKU29) AS kj29 ");
            sql.Append(" ,SUM(KOJO_KOMOKU30) AS kj30 ");
            sql.Append(" ,SUM(KOJO_KOMOKU31) AS kj31 ");
            sql.Append(" ,SUM(KOJO_KOMOKU32) AS kj32 ");
            sql.Append(" ,SUM(KOJO_KOMOKU33) AS kj33 ");
            sql.Append(" ,SUM(KOJO_KOMOKU34) AS kj34 ");
            sql.Append(" ,SUM(KOJO_KOMOKU35) AS kj35 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_KOJO_DATA ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO ");
            sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            if (this._pForm.Mode == 1)
            {
                sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 ");
            }
            sql.Append("GROUP BY ");
            sql.Append("  SENSHU_CD WITH ROLLUP ");
            sql.Append("HAVING ");
            sql.Append("  SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU2) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU4) + SUM(KOJO_KOMOKU5) ");
            sql.Append("  + SUM(KOJO_KOMOKU6) + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) ");
            sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) ");
            sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) ");
            sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) ");
            sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) ");
            sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) > 0 ");
            sql.Append("ORDER BY ");
            sql.Append("  gp DESC ");
            sql.Append(" ,SENSHU_CD ASC ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(this._pForm.Condition["ShishoCode"]));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@SEISAN_BI_FR", SqlDbType.DateTime, this._pForm.Condition["SeisanDtFr"]);
            dpc.SetParam("@SEISAN_BI_TO", SqlDbType.DateTime, this._pForm.Condition["SeisanDtTo"]);
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 5, this._pForm.Condition["FunanushiCdFr"]);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 5, this._pForm.Condition["FunanushiCdTo"]);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData">DBから取得したデータ</param>
        /// <returns>表示用のデータ</returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 列の定義を作成
            dtResult.Columns.Add("船　　主", typeof(string));
            dtResult.Columns.Add("控除合計", typeof(string));
            for (int i = 1; i <= 35; i++)
            {
                dtResult.Columns.Add("控除項目" + Util.ToString(i), typeof(string));
            }

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();
                drResult["船　　主"] = dtData.Rows[i]["船主"];
                drResult["控除合計"] = Util.FormatNum(dtData.Rows[i]["合計"]);
                for (int j = 1; j <= 35; j++)
                {
                    drResult["控除項目" + Util.ToString(j)] = Util.FormatNum(dtData.Rows[i]["kj" + Util.ToString(j)]);
                }

                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }
        #endregion
    }
}
