﻿namespace jp.co.fsi.hn.hnsb1021
{
    partial class HNSB1023
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblTerm = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMizuageUriage = new System.Windows.Forms.Label();
            this.lblMizuageZei = new System.Windows.Forms.Label();
            this.lblKakeUriage = new System.Windows.Forms.Label();
            this.lblKakeZei = new System.Windows.Forms.Label();
            this.lblKeiUriage = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(4, 65);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(89, 65);
            this.btnF1.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(175, 65);
            this.btnF6.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 511);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(852, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(863, 31);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "仕訳データ参照";
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.EnableHeadersVisualStyles = false;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(17, 44);
            this.dgvList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(813, 421);
            this.dgvList.TabIndex = 1;
            // 
            // lblTerm
            // 
            this.lblTerm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTerm.ForeColor = System.Drawing.Color.Blue;
            this.lblTerm.Location = new System.Drawing.Point(17, 8);
            this.lblTerm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTerm.Name = "lblTerm";
            this.lblTerm.Size = new System.Drawing.Size(393, 32);
            this.lblTerm.TabIndex = 0;
            this.lblTerm.Text = " 平成 21年 4月 1日 ～ 平成 22年 3月31日";
            this.lblTerm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(17, 464);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 30);
            this.label1.TabIndex = 2;
            this.label1.Text = "[ 合 計 ] ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMizuageUriage
            // 
            this.lblMizuageUriage.BackColor = System.Drawing.Color.White;
            this.lblMizuageUriage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMizuageUriage.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageUriage.ForeColor = System.Drawing.Color.Black;
            this.lblMizuageUriage.Location = new System.Drawing.Point(216, 464);
            this.lblMizuageUriage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageUriage.Name = "lblMizuageUriage";
            this.lblMizuageUriage.Size = new System.Drawing.Size(123, 30);
            this.lblMizuageUriage.TabIndex = 3;
            this.lblMizuageUriage.Text = "-999,999,999";
            this.lblMizuageUriage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMizuageZei
            // 
            this.lblMizuageZei.BackColor = System.Drawing.Color.White;
            this.lblMizuageZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMizuageZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageZei.ForeColor = System.Drawing.Color.Black;
            this.lblMizuageZei.Location = new System.Drawing.Point(339, 464);
            this.lblMizuageZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageZei.Name = "lblMizuageZei";
            this.lblMizuageZei.Size = new System.Drawing.Size(123, 30);
            this.lblMizuageZei.TabIndex = 4;
            this.lblMizuageZei.Text = "-999,999,999";
            this.lblMizuageZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKakeUriage
            // 
            this.lblKakeUriage.BackColor = System.Drawing.Color.White;
            this.lblKakeUriage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKakeUriage.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKakeUriage.ForeColor = System.Drawing.Color.Black;
            this.lblKakeUriage.Location = new System.Drawing.Point(461, 464);
            this.lblKakeUriage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKakeUriage.Name = "lblKakeUriage";
            this.lblKakeUriage.Size = new System.Drawing.Size(123, 30);
            this.lblKakeUriage.TabIndex = 5;
            this.lblKakeUriage.Text = "-999,999,999";
            this.lblKakeUriage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKakeZei
            // 
            this.lblKakeZei.BackColor = System.Drawing.Color.White;
            this.lblKakeZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKakeZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKakeZei.ForeColor = System.Drawing.Color.Black;
            this.lblKakeZei.Location = new System.Drawing.Point(584, 464);
            this.lblKakeZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKakeZei.Name = "lblKakeZei";
            this.lblKakeZei.Size = new System.Drawing.Size(123, 30);
            this.lblKakeZei.TabIndex = 6;
            this.lblKakeZei.Text = "-999,999,999";
            this.lblKakeZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKeiUriage
            // 
            this.lblKeiUriage.BackColor = System.Drawing.Color.White;
            this.lblKeiUriage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKeiUriage.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKeiUriage.ForeColor = System.Drawing.Color.Black;
            this.lblKeiUriage.Location = new System.Drawing.Point(707, 464);
            this.lblKeiUriage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKeiUriage.Name = "lblKeiUriage";
            this.lblKeiUriage.Size = new System.Drawing.Size(123, 30);
            this.lblKeiUriage.TabIndex = 7;
            this.lblKeiUriage.Text = "-999,999,999";
            this.lblKeiUriage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // HNSB1023
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 648);
            this.Controls.Add(this.lblKeiUriage);
            this.Controls.Add(this.lblKakeZei);
            this.Controls.Add(this.lblKakeUriage);
            this.Controls.Add(this.lblMizuageZei);
            this.Controls.Add(this.lblMizuageUriage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTerm);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "HNSB1023";
            this.ShowFButton = true;
            this.ShowTitle = false;
            this.Text = "仕訳データ参照";
            this.Shown += new System.EventHandler(this.HNSB1023_Shown);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblTerm, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblMizuageUriage, 0);
            this.Controls.SetChildIndex(this.lblMizuageZei, 0);
            this.Controls.SetChildIndex(this.lblKakeUriage, 0);
            this.Controls.SetChildIndex(this.lblKakeZei, 0);
            this.Controls.SetChildIndex(this.lblKeiUriage, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblTerm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMizuageUriage;
        private System.Windows.Forms.Label lblMizuageZei;
        private System.Windows.Forms.Label lblKakeUriage;
        private System.Windows.Forms.Label lblKakeZei;
        private System.Windows.Forms.Label lblKeiUriage;



    }
}