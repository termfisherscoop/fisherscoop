﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsb1021
{
    /// <summary>
    /// 仕訳データ参照(HNSB1023)
    /// </summary>
    public partial class HNSB1023 : BasePgForm
    {
        #region 構造体
        /// <summary>
        /// 合計情報
        /// </summary>
        private struct Summary
        {
            public decimal mizuageUriage;
            public decimal mizuageZei;
            public decimal keiUriage;

            /// <summary>
            /// 金額をクリア
            /// </summary>
            public void Clear()
            {
                mizuageUriage = 0;
                mizuageZei = 0;
                keiUriage = 0;
            }
        }

        // 請求先単位の合計を保持する変数
        Summary _sumSeikyuInfo = new Summary();

        // 全レコードの合計を保持する変数
        Summary _sumTotalInfo = new Summary();
        #endregion

        #region private変数
        /// <summary>
        /// HNSB1021(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        HNSB1021 _pForm;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSB1023(HNSB1021 frm)
        {
            this._pForm = frm;

			InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 期間の初期表示
            string tmpTerm = " ";
            string[] aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["SeisanDtFr"]), this._pForm.Dba);
            tmpTerm += aryJpDate[5];
            aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["SeisanDtTo"]), this._pForm.Dba);
            tmpTerm += " ～ " + aryJpDate[5];
            this.lblTerm.Text = tmpTerm;

            // 表示用にデータを編集
            HNSB1021DA da = new HNSB1021DA(this.UInfo, this._pForm.Dba, this.Config);
            // 支所コード設定
            //da.ShishoCode = Util.ToInt(this._pForm.Condition["ShishoCode"]);
            DataTable dtDBData = da.GetSeriSwkTgtData(this._pForm.Mode, this._pForm.Condition);
            DataTable dtDisp = EditDataList(dtDBData);

            this.dgvList.DataSource = dtDisp;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 190;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[1].Width = 100;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[2].Width = 100;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[3].Width = 100;
            this.dgvList.Columns[3].HeaderText = string.Empty;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[4].Width = 100;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[5].Width = 100;
            this.dgvList.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // 合計値の表示
            this.lblMizuageUriage.Text = Util.FormatNum(this._sumTotalInfo.mizuageUriage);
            this.lblMizuageZei.Text = Util.FormatNum(this._sumTotalInfo.mizuageZei);
            this.lblKakeUriage.Text = "0";
            this.lblKakeZei.Text = "0";
            this.lblKeiUriage.Text = Util.FormatNum(this._sumTotalInfo.keiUriage);

            // InData在りの場合は更新モード起動、その場合は更新処理は不可
            if (this.InData != null && !ValChk.IsEmpty(this.InData))
            {
                string inData = (string)this.InData;
                if (inData.Length != 0)
                    this.btnF6.Enabled = false;
            }
        }

		public override void PressEsc()
		{
			this.Close();
		}
		/// <summary>
		/// F1キー押下時処理
		/// </summary>
		public override void PressF1()
        {
            // 仕訳形式画面を起動
            HNSB1028 frm1028 = new HNSB1028(this._pForm, this);
            frm1028.ShowDialog(this);
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled) return;

            // 登録処理
            string modeNm = this._pForm.Mode == 2 ? "更新" : "登録";
            if (Msg.ConfYesNo(modeNm + "しますか？") == DialogResult.No)
            {
                // 「いいえ」が押されたら処理終了
                return;
            }

            // 更新中メッセージ表示
            HNSB1026 msgFrm = new HNSB1026();
            msgFrm.Show();
            msgFrm.Refresh();

            bool result = false;
            bool dataExists = false;

            try
            {
                this.Dba.BeginTransaction();

                // 更新処理を実行
                HNSB1021DA da = new HNSB1021DA(this.UInfo, this._pForm.Dba, this.Config);

                // セリ販売の更新処理
                result = da.MakeSeriSwkData(this._pForm.Mode, this._pForm.PackDpyNo,
                    this._pForm.Condition, ref dataExists);


                // 更新終了後、メッセージを閉じる
                msgFrm.Close();

                // 更新に失敗していればその旨表示する
                if (result)
                {
					this._pForm.Dba.Commit();
                }
                else
                {
					this._pForm.Dba.Rollback();
                    Msg.Error("更新に失敗しました。" + Environment.NewLine + "もう一度やり直して下さい。");
                }
            }
            finally
            {
                this.Dba.Rollback();
            }

            if (result)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        #endregion

        #region イベント
        /// <summary>
        /// フォーム表示後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HNSB1023_Shown(object sender, EventArgs e)
        {
            if (this.dgvList.RowCount == 0)
            {
                Msg.Info("該当データがありません。");
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData">DBから取得したデータ</param>
        /// <returns>表示用のデータ</returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 列の定義を作成
            dtResult.Columns.Add("船主名", typeof(string));
            dtResult.Columns.Add("売上金額", typeof(string));
            dtResult.Columns.Add("現金消費税", typeof(string));
            dtResult.Columns.Add("掛売上", typeof(string));
            dtResult.Columns.Add("掛消費税", typeof(string));
            dtResult.Columns.Add("合計金額", typeof(string));

            // 船主CDブレイク判断用ワーク
            string prevFunanushiCd = string.Empty;
            string prevFunanushiNm = string.Empty;

            // 合計を格納する構造体をクリア
            this._sumSeikyuInfo.Clear();
            this._sumTotalInfo.Clear();

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                if (!ValChk.IsEmpty(prevFunanushiCd) && !Util.ToString(dtData.Rows[i]["船主CD"]).Equals(prevFunanushiCd))
                {
                    // 会員番号ブレイク時、表示用データをセットする
                    drResult = dtResult.NewRow();

                    drResult["船主名"] = prevFunanushiNm;
                    drResult["売上金額"] = Util.FormatNum(this._sumSeikyuInfo.mizuageUriage);
                    drResult["現金消費税"] = Util.FormatNum(this._sumSeikyuInfo.mizuageZei);
                    drResult["掛売上"] = "0";
                    drResult["掛消費税"] = "0";
                    drResult["合計金額"] = Util.FormatNum(this._sumSeikyuInfo.keiUriage);

                    dtResult.Rows.Add(drResult);
                    this._sumSeikyuInfo.Clear();
                }

                this._sumSeikyuInfo.mizuageUriage += Util.ToDecimal(dtData.Rows[i]["売上金額"]);
                this._sumSeikyuInfo.mizuageZei += Util.ToDecimal(dtData.Rows[i]["消費税"]);
                this._sumTotalInfo.mizuageUriage += Util.ToDecimal(dtData.Rows[i]["売上金額"]);
                this._sumTotalInfo.mizuageZei += Util.ToDecimal(dtData.Rows[i]["消費税"]);
                this._sumSeikyuInfo.keiUriage += (Util.ToDecimal(dtData.Rows[i]["売上金額"]) + Util.ToDecimal(dtData.Rows[i]["消費税"]));
                this._sumTotalInfo.keiUriage += (Util.ToDecimal(dtData.Rows[i]["売上金額"]) + Util.ToDecimal(dtData.Rows[i]["消費税"]));

                prevFunanushiCd = Util.ToString(dtData.Rows[i]["船主CD"]);
                prevFunanushiNm = Util.ToString(dtData.Rows[i]["船主名称"]);
            }

            // 最終行の追加
            if (dtData.Rows.Count > 0)
            {
                drResult = dtResult.NewRow();

                drResult["船主名"] = prevFunanushiNm;
                drResult["売上金額"] = Util.FormatNum(this._sumSeikyuInfo.mizuageUriage);
                drResult["現金消費税"] = Util.FormatNum(this._sumSeikyuInfo.mizuageZei);
                drResult["掛売上"] = "0";
                drResult["掛消費税"] = "0";
                drResult["合計金額"] = Util.FormatNum(this._sumSeikyuInfo.keiUriage);

                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }
        #endregion
    }
}
