﻿namespace jp.co.fsi.hn.hncm1021
{
    partial class HNCM1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtChikuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbChikuCd = new System.Windows.Forms.Label();
            this.txtChikuNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbChikuNm = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n保存";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(8, 77);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(542, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(531, 31);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // txtChikuCd
            // 
            this.txtChikuCd.AutoSizeFromLength = true;
            this.txtChikuCd.DisplayLength = null;
            this.txtChikuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChikuCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtChikuCd.Location = new System.Drawing.Point(119, 4);
            this.txtChikuCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtChikuCd.MaxLength = 4;
            this.txtChikuCd.Name = "txtChikuCd";
            this.txtChikuCd.Size = new System.Drawing.Size(44, 23);
            this.txtChikuCd.TabIndex = 1;
            this.txtChikuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChikuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCd_Validating);
            // 
            // lbChikuCd
            // 
            this.lbChikuCd.BackColor = System.Drawing.Color.Silver;
            this.lbChikuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbChikuCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbChikuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbChikuCd.Location = new System.Drawing.Point(0, 0);
            this.lbChikuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbChikuCd.Name = "lbChikuCd";
            this.lbChikuCd.Size = new System.Drawing.Size(512, 31);
            this.lbChikuCd.TabIndex = 0;
            this.lbChikuCd.Tag = "CHANGE";
            this.lbChikuCd.Text = "地区コード";
            this.lbChikuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChikuNm
            // 
            this.txtChikuNm.AutoSizeFromLength = false;
            this.txtChikuNm.DisplayLength = null;
            this.txtChikuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChikuNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtChikuNm.Location = new System.Drawing.Point(119, 4);
            this.txtChikuNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtChikuNm.MaxLength = 40;
            this.txtChikuNm.Name = "txtChikuNm";
            this.txtChikuNm.Size = new System.Drawing.Size(361, 23);
            this.txtChikuNm.TabIndex = 3;
            this.txtChikuNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChikuNm_KeyDown);
            this.txtChikuNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuNm_Validating);
            // 
            // lbChikuNm
            // 
            this.lbChikuNm.BackColor = System.Drawing.Color.Silver;
            this.lbChikuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbChikuNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbChikuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbChikuNm.Location = new System.Drawing.Point(0, 0);
            this.lbChikuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbChikuNm.Name = "lbChikuNm";
            this.lbChikuNm.Size = new System.Drawing.Size(512, 32);
            this.lbChikuNm.TabIndex = 2;
            this.lbChikuNm.Tag = "CHANGE";
            this.lbChikuNm.Text = "地区名";
            this.lbChikuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(8, 37);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(520, 78);
            this.fsiTableLayoutPanel1.TabIndex = 1000;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtChikuNm);
            this.fsiPanel2.Controls.Add(this.lbChikuNm);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 42);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(512, 32);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtChikuCd);
            this.fsiPanel1.Controls.Add(this.lbChikuCd);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(512, 31);
            this.fsiPanel1.TabIndex = 0;
            // 
            // HNCM1022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 214);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "HNCM1022";
            this.ShowFButton = true;
            this.Text = "地区の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtChikuCd;
        private System.Windows.Forms.Label lbChikuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtChikuNm;
        private System.Windows.Forms.Label lbChikuNm;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    };
}