﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1021
{
    /// <summary>
    ///　地区マスタの登録(HNCM1021)
    /// </summary>
    public partial class HNCM1021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "地区マスタの検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1021()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // 地区CDを表示
                this.lblChikuCd.Visible = true;
                this.txtChikuCd.Visible = true;
                // サイズを縮める448, 626
                this.Size = new Size(448, 733);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // 地区名にフォーカス
            this.txtChikuNm.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }

            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // 地区名にフォーカスを戻す
            this.txtChikuNm.Focus();
            this.txtChikuNm.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ地区登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 地区登録画面の起動
                EditChiku(string.Empty);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 地区CD検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChikuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidChikuCd())
            {
                e.Cancel = true;
                this.txtChikuCd.SelectAll();
            }
        }

        /// <summary>
        /// 地区名でのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChikuNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // 入力された情報を元に検索する
                SearchData(false);
            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditChiku(Util.ToString(this.dgvList.SelectedRows[0].Cells["地区コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditChiku(Util.ToString(this.dgvList.SelectedRows[0].Cells["地区コード"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 地区CDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidChikuCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtChikuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 地区マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");    
            if (!isInitial)
            {
                // 初期処理でない場合、入力された地区CDから検索する
                if (!ValChk.IsEmpty(this.txtChikuCd.Text))
                {
                    where.Append(" AND HN.CHIKU_CD = @CHIKU_CD");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 5, this.txtChikuCd.Text);
                }
                // 初期処理でない場合、入力された地区名から検索する
                if (!ValChk.IsEmpty(this.txtChikuNm.Text))
                {
                    where.Append(" AND HN.CHIKU_NM LIKE @CHIKU_NM");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@CHIKU_NM", SqlDbType.VarChar, 32, "%" + this.txtChikuNm.Text + "%");
                }
            }

            string cols = "HN.CHIKU_CD AS 地区コード";
            cols += ", HN.CHIKU_NM AS 地区名";
            string from = "TB_HN_CHIKU_MST AS HN";

            DataTable dtChiku =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "HN.CHIKU_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtChiku.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtChiku.Rows.Add(dtChiku.NewRow());
            }

            this.dgvList.DataSource = dtChiku;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 100;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 300;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        /// <summary>
        /// 地区を追加編集する
        /// </summary>
        /// <param name="code">地区コード(空：新規登録、以外：編集)</param>
        private void EditChiku(string code)
        {
            HNCM1022 frmHNCM1022;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHNCM1022 = new HNCM1022("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmHNCM1022 = new HNCM1022("2");
                frmHNCM1022.InData = code;
            }

            DialogResult result = frmHNCM1022.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["地区コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[2]{ 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["地区コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["地区名"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
