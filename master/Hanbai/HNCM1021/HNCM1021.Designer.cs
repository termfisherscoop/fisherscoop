﻿namespace jp.co.fsi.hn.hncm1021
{
    partial class HNCM1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblChikuNm = new System.Windows.Forms.Label();
			this.txtChikuNm = new System.Windows.Forms.TextBox();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.txtChikuCd = new System.Windows.Forms.TextBox();
			this.lblChikuCd = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 472);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(438, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(428, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "地区の登録";
			// 
			// lblChikuNm
			// 
			this.lblChikuNm.BackColor = System.Drawing.Color.Silver;
			this.lblChikuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblChikuNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblChikuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblChikuNm.Location = new System.Drawing.Point(0, 0);
			this.lblChikuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblChikuNm.Name = "lblChikuNm";
			this.lblChikuNm.Size = new System.Drawing.Size(403, 32);
			this.lblChikuNm.TabIndex = 2;
			this.lblChikuNm.Tag = "CHANGE";
			this.lblChikuNm.Text = "地　区　名";
			this.lblChikuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtChikuNm
			// 
			this.txtChikuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtChikuNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtChikuNm.Location = new System.Drawing.Point(105, 3);
			this.txtChikuNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtChikuNm.MaxLength = 20;
			this.txtChikuNm.Name = "txtChikuNm";
			this.txtChikuNm.Size = new System.Drawing.Size(239, 23);
			this.txtChikuNm.TabIndex = 3;
			this.txtChikuNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChikuNm_KeyDown);
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dgvList.DefaultCellStyle = dataGridViewCellStyle2;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(8, 131);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(410, 395);
			this.dgvList.TabIndex = 4;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// txtChikuCd
			// 
			this.txtChikuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtChikuCd.ImeMode = System.Windows.Forms.ImeMode.Alpha;
			this.txtChikuCd.Location = new System.Drawing.Point(105, 4);
			this.txtChikuCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtChikuCd.MaxLength = 7;
			this.txtChikuCd.Name = "txtChikuCd";
			this.txtChikuCd.Size = new System.Drawing.Size(239, 23);
			this.txtChikuCd.TabIndex = 1;
			this.txtChikuCd.Visible = false;
			this.txtChikuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCd_Validating);
			// 
			// lblChikuCd
			// 
			this.lblChikuCd.BackColor = System.Drawing.Color.Silver;
			this.lblChikuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblChikuCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblChikuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblChikuCd.Location = new System.Drawing.Point(0, 0);
			this.lblChikuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblChikuCd.Name = "lblChikuCd";
			this.lblChikuCd.Size = new System.Drawing.Size(403, 32);
			this.lblChikuCd.TabIndex = 0;
			this.lblChikuCd.Tag = "CHANGE";
			this.lblChikuCd.Text = "地　区　CD";
			this.lblChikuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 49);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(411, 79);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtChikuNm);
			this.fsiPanel2.Controls.Add(this.lblChikuNm);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 43);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(403, 32);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtChikuCd);
			this.fsiPanel1.Controls.Add(this.lblChikuCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(403, 32);
			this.fsiPanel1.TabIndex = 0;
			// 
			// HNCM1021
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(428, 608);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1021";
			this.Text = "地区の登録";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblChikuNm;
        private System.Windows.Forms.TextBox txtChikuNm;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.TextBox txtChikuCd;
        private System.Windows.Forms.Label lblChikuCd;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}