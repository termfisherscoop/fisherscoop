﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1021
{
    /// <summary>
    /// 地区の登録(HNCM1022)
    /// </summary>
    public partial class HNCM1022 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1022()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1022(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：地区コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF3.Location = this.btnF2.Location;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モード時
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();

            try
            {
                this.Dba.BeginTransaction();

                // データ削除
                // 地区マスタ
                this.Dba.Delete("TB_HN_CHIKU_MST",
                    "KAISHA_CD = @KAISHA_CD AND CHIKU_CD = @CHIKU_CD",
                    (DbParamCollection)delParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamshnChiku = SetHnChikuParams();

            try
            {
                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.取引先マスタ
                    this.Dba.Insert("TB_HN_CHIKU_MST", (DbParamCollection)alParamshnChiku[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.取引先マスタ
                    this.Dba.Update("TB_HN_CHIKU_MST",
                        (DbParamCollection)alParamshnChiku[1],
                        "KAISHA_CD = @KAISHA_CD AND CHIKU_CD = @CHIKU_CD",
                        (DbParamCollection)alParamshnChiku[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 地区コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChikuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidChikuCd())
            {
                e.Cancel = true;
                this.txtChikuCd.SelectAll();
            }
        }

        /// <summary>
        /// 地区名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChikuNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidChikuNm())
            {
                e.Cancel = true;
                this.txtChikuNm.SelectAll();
            }
        }

        private void txtChikuNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 地区コードの初期値を取得
            // 地区の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder();
            DataTable dtMaxChiku =
                this.Dba.GetDataTableByConditionWithParams("MAX(CHIKU_CD) AS MAX_CD",
                    "TB_HN_CHIKU_MST", Util.ToString(where), dpc);
            if (dtMaxChiku.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxChiku.Rows[0]["MAX_CD"]))
            {
                this.txtChikuCd.Text = Util.ToString(Util.ToInt(dtMaxChiku.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtChikuCd.Text = "1";
            }

            // 地区名に初期フォーカス
            this.ActiveControl = this.txtChikuNm;
            this.txtChikuNm.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.CHIKU_CD");
            cols.Append(" ,A.CHIKU_NM");
            cols.Append(" ,A.REGIST_DATE");
            cols.Append(" ,A.UPDATE_DATE");
            cols.Append(" ,A.SHORI_FLG");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_CHIKU_MST AS A");


            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.CHIKU_CD = @CHIKU_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtChikuCd.Text = Util.ToString(drDispData["CHIKU_CD"]);
            this.txtChikuNm.Text = Util.ToString(drDispData["CHIKU_NM"]);

            // 地区先コードは入力不可
            this.lbChikuCd.Enabled = false;
            this.txtChikuCd.Enabled = false;
        }

        /// <summary>
        /// 地区コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidChikuCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtChikuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, this.txtChikuCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND CHIKU_CD = @CHIKU_CD");
            DataTable dtChiku =
                this.Dba.GetDataTableByConditionWithParams("CHIKU_CD",
                    "TB_HN_CHIKU_MST", Util.ToString(where), dpc);
            if (dtChiku.Rows.Count > 0)
            {
                Msg.Error("既に存在する地区コードと重複しています。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 地区の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidChikuNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtChikuNm.Text, this.txtChikuNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 地区コードのチェック
                if (!IsValidChikuCd())
                {
                    this.txtChikuCd.Focus();
                    return false;
                }
            }

            // 地区名のチェック
            if (!IsValidChikuNm())
            {
                this.txtChikuNm.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_CHIKU_MSTに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetHnChikuParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと地区コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, this.txtChikuCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと地区コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, this.txtChikuCd.Text);
                alParams.Add(whereParam);
            }

            // 地区名
            updParam.SetParam("@CHIKU_NM", SqlDbType.VarChar, 40, this.txtChikuNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_CHIKU_MSTからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 会社コードと地区コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcDenpyo = new DbParamCollection();
            dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcDenpyo.SetParam("@CHIKU_CD", SqlDbType.VarChar, 6, this.txtChikuCd.Text);

            alParams.Add(dpcDenpyo);

            return alParams;
        }
        #endregion
    }
}



