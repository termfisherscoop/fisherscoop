﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1101
{
    /// <summary>
    /// 船主変換マスタメンテ(変更・追加)(HNCM1102)
    /// </summary>
    public partial class HNCM1102 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1102()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1102(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：名護漁協コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            this.gbxGinozaGyo.Text = UInfo.KaishaNm;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtFunanushiCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタン押下時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtFunanushiCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.ActiveCtlNm == "txtFunanushiCd")
                            {
                                frm.InData = this.txtFunanushiCd.Text;
                            }
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtFunanushiCd")
                                {
                                    // 船主CDが入力されたら県漁連船CD、名を有効にする。
                                    // 船主名は変更出来てはいけないため無効。
                                    this.txtKenFunaCd.Enabled = true;
                                    this.txtKenFunaNm.Enabled = true;
                                    this.txtFunanushiNm.Enabled = false;

                                    this.txtFunanushiCd.Text = result[0];
                                    this.txtFunanushiNm.Text = result[1];
                                    this.txtKenFunaNm.Text = result[1];

                                    this.txtKenFunaCd.Focus();

                                    // 船主CDを非活性にする
                                    this.txtFunanushiCd.Enabled = false;

                                    // 削除ボタンを表示する
                                    this.btnF3.Enabled = true;
                                }

                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                DbParamCollection whereParam;
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@SENSHU_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);
                this.Dba.Delete("TB_HN_SENSHU_CD_HENKAN_MST",
                    "KAISHA_CD = @KAISHA_CD AND SENSHU_CD = @SENSHU_CD",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            DataTable a = GetSenshuCd(Util.ToDecimal(this.txtFunanushiCd.Text));

            if (Util.ToDecimal(a.Rows[0]["KENSU"]) == 0)
            {
                this.Par1 = "1";
            }
            else
            {
                this.Par1 = "2";
            }
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmToroku = SetCmTorokuParams();

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.商品マスタ // 船主CD変換マスタメンテ
                    this.Dba.Insert("TB_HN_SENSHU_CD_HENKAN_MST", (DbParamCollection)alParamsCmToroku[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.商品マスタ // 船主CD変換マスタメンテ
                    this.Dba.Update("TB_HN_SENSHU_CD_HENKAN_MST",
                        (DbParamCollection)alParamsCmToroku[1],
                        // SENSHU_CDとSENSHU_CD_WHEREがある
                        // 更新だからWHEREの方から取ってくる。
                        "KAISHA_CD = @KAISHA_CD AND SENSHU_CD = @SENSHU_CD_WHERE",
                        (DbParamCollection)alParamsCmToroku[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            Msg.Info("登録しました。");
            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 県漁連船主CDの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenFunaCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKenFunaCd())
            {
                e.Cancel = true;
                this.txtKenFunaCd.SelectAll();
            }
        }

        /// <summary>
        /// 県漁連船主名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenFunaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKenFunaNm())
            {
                e.Cancel = true;
                this.txtKenFunaNm.SelectAll();
            }
        }

        /// <summary>
        /// 県漁連船主名Enter押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenFunaNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Par1 == "1")
            {
                this.PressF6();
            }
        }

        private void txtFunanushiCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushi())
            {
                e.Cancel = true;
                this.txtFunanushiCd.Focus();
                this.txtFunanushiCd.SelectAll();
            }
        }

        /// <summary>
        /// 船主CDEnter押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!IsValidFunanushi())
                {
                    this.txtFunanushiCd.Focus();
                }
            }
        }

        /// <summary>
        /// 船主名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiNm())
            {
                e.Cancel = true;
                this.txtFunanushiNm.SelectAll();
            }
        }

        private void txtFunanushiNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装
            // 初期値を設定
            this.txtKenFunaCd.Text = "0";
            this.txtFunanushiCd.Text = "0";
            // コントロール制御
            this.txtKenFunaCd.Enabled = false;
            this.txtKenFunaNm.Enabled = false;
            this.txtFunanushiNm.Enabled = false;

            // 県漁連船主CDに初期フォーカス
            this.ActiveControl = this.txtFunanushiCd;
            this.txtFunanushiCd.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            Array indata = (Array)InData;
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append("    ,KEN_GYOREN_SENSHU_CD");
            cols.Append("    ,KEN_GYOREN_SENSHU_NM");
            cols.Append("    ,SENSHU_CD");
            cols.Append("    ,SENSHU_NM");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_SENSHU_CD_HENKAN_MST");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KEN_GYOREN_SENSHU_CD", SqlDbType.Decimal, 6, indata.GetValue(0));
            dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 6, indata.GetValue(1));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND KEN_GYOREN_SENSHU_CD = @KEN_GYOREN_SENSHU_CD AND SENSHU_CD = @SENSHU_CD ",
                    //"KAISHA_CD = @KAISHA_CD AND KEN_GYOREN_SENSHU_CD = @KEN_GYOREN_SENSHU_CD ",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            else
            {
                // 最初は県漁連船CD、名、船主名を有効にする
                this.txtKenFunaCd.Enabled = true;
                this.txtKenFunaNm.Enabled = true;
                this.txtFunanushiNm.Enabled = true;

                // 取得した内容を表示
                DataRow drDispData = dtDispData.Rows[0];
                this.txtKenFunaCd.Text = Util.ToString(drDispData["KEN_GYOREN_SENSHU_CD"]);
                this.txtKenFunaNm.Text = Util.ToString(drDispData["KEN_GYOREN_SENSHU_NM"]);
                this.txtFunanushiCd.Text = Util.ToString(drDispData["SENSHU_CD"]);
                this.txtFunanushiNm.Text = Util.ToString(drDispData["SENSHU_NM"]);

                // 県漁連船主CDにフォーカス
                this.ActiveControl = this.txtKenFunaCd;
                this.txtKenFunaCd.Focus();
                this.txtKenFunaCd.SelectAll();

            }
        }

        /// <summary>
        /// 県漁連船主CDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKenFunaCd()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtKenFunaCd.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }

            // 0はエラーとする
            if (this.txtKenFunaCd.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKenFunaCd.Text))
            {
                Msg.Error("数字で入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 県漁連船主名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKenFunaNm()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtKenFunaNm.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }
            // 0はエラーとする
            if (this.txtKenFunaNm.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtKenFunaNm.Text, this.txtKenFunaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 船主名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiNm()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtFunanushiNm.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }
            // 0はエラーとする
            if (this.txtFunanushiNm.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtFunanushiNm.Text, this.txtFunanushiNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 船主CDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushi()
        {
            // 未入力はエラー
            if (ValChk.IsEmpty(this.txtFunanushiCd.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }
            // 0はエラーとする
            if (this.txtFunanushiCd.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtFunanushiCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 新規の場合
            if (MODE_NEW == (this.Par1))
            {
                // 空でない場合、入力された船主CDから検索する
                if (!ValChk.IsEmpty(this.txtFunanushiCd.Text))
                {
                    // 現在DBに登録されている値をtextboxに表示
                    StringBuilder cols = new StringBuilder();
                    cols.Append("KAISHA_CD");
                    cols.Append("    ,TORIHIKISAKI_CD");
                    cols.Append("    ,TORIHIKISAKI_NM");

                    StringBuilder from = new StringBuilder();
                    from.Append("VI_HN_TORIHIKISAKI_JOHO");

                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
                    DataTable dtDispData =
                        this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), Util.ToString(from),
                            "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                            dpc);

                    if (dtDispData.Rows.Count == 0)
                    {
                        Msg.Error("データがありません。");
                        // 船主CDにFocusを当てる
                        this.txtFunanushiCd.Focus();
                        this.txtFunanushiCd.SelectAll();
                        return false;
                    }
                    else
                    {
                        // 取得した内容を表示
                        DataRow drDispData = dtDispData.Rows[0];
                        this.txtFunanushiCd.Text = Util.ToString(drDispData["TORIHIKISAKI_CD"]);
                        this.txtFunanushiNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);
                        if (this.txtKenFunaNm.Text == "")
                            this.txtKenFunaNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);

                        // 県漁連船CD、名を有効にする
                        this.txtKenFunaCd.Enabled = true;
                        this.txtKenFunaNm.Enabled = true;
                        // 県漁連船CDにFocusを当てる
                        this.txtKenFunaCd.Focus();

                        // 船主CDを非活性にする
                        this.txtFunanushiCd.Enabled = false;
                        this.txtFunanushiNm.Enabled = false;
                        // 削除ボタンを表示する
                        this.btnF3.Enabled = true;
                    }
                }
            }
            // 編集の場合
            else if (MODE_EDIT.Equals(this.Par1))
            {
               /**/ // 空でない場合、入力された船主CDから検索する
                if (!ValChk.IsEmpty(this.txtFunanushiCd.Text))
                {

                    // 現在DBに登録されている値をtextboxに表示
                    StringBuilder cols = new StringBuilder();
                    cols.Append("KAISHA_CD");
                    cols.Append("    ,TORIHIKISAKI_CD");
                    cols.Append("    ,TORIHIKISAKI_NM");

                    StringBuilder from = new StringBuilder();
                    from.Append("VI_HN_TORIHIKISAKI_JOHO");

                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
                    DataTable dtDispData =
                        this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), Util.ToString(from),
                            "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                            dpc);


                    if (dtDispData.Rows.Count == 0)
                    {
                        Msg.Error("データがありません。");
                        // 船主名を無効にする
                        this.txtFunanushiNm.Enabled = false;
                        // 県漁連船CD、名を有効にする
                        this.txtKenFunaCd.Enabled = false;
                        this.txtKenFunaNm.Enabled = false;
                        // 全部一旦消してからデータ入れるため
                        this.txtFunanushiNm.Text = "";
                        this.txtKenFunaCd.Text = "";
                        this.txtKenFunaNm.Text = "";
                        // 船主CDにFocusを当てる
                        this.txtFunanushiCd.Focus();
                        this.txtFunanushiCd.SelectAll();
                        return false;
                    }
                    else
                    {
                        // 取得した内容を表示
                        DataRow drDispData = dtDispData.Rows[0];
                        this.txtFunanushiCd.Text = Util.ToString(drDispData["TORIHIKISAKI_CD"]);
                        if (this.txtFunanushiNm.Text == "")
                            this.txtFunanushiNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);
                        if (this.txtKenFunaNm.Text == "")
                            this.txtKenFunaNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);

                        // 県漁連船CD、名を有効にする
                        this.txtKenFunaCd.Enabled = true;
                        this.txtKenFunaNm.Enabled = true;
                        //// 船主名を無効にする
                        //this.txtFunanushiNm.Enabled = false;
                        //// 県漁連船CDにFocusを当てる
                        //this.txtKenFunaCd.Focus();
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 県漁連船主CDのチェック
            if (!IsValidKenFunaCd())
            {
                this.txtKenFunaCd.Focus();
                return false;
            }

            // 県漁連船主名のチェック
            if (!IsValidKenFunaNm())
            {
                this.txtKenFunaNm.Focus();
                return false;
            }

            // 船主名のチェック
            if (!IsValidFunanushiNm())
            {
                this.txtFunanushiNm.Focus();
                return false;
            }

            // 船主CDのチェック
            if (!IsValidFunanushi())
            {
                this.txtFunanushiCd.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TM_HN_SENSHU_CD_HENKAN_MSTに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmTorokuParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();
            // 登録
            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社CD,船主CDをパラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SENSHU_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
                // 処理フラグ
                updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, 0);
            }
            // 更新
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社CDをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SENSHU_CD_WHERE", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
                alParams.Add(whereParam);
                // 処理フラグ
                updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, 1);
            }
            
            // 名護船主CD
            updParam.SetParam("@KEN_GYOREN_SENSHU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtKenFunaCd.Text));
            // 名護船主名
            updParam.SetParam("@KEN_GYOREN_SENSHU_NM", SqlDbType.VarChar, 40, this.txtKenFunaNm.Text);
            // 船主名
            updParam.SetParam("@SENSHU_NM", SqlDbType.VarChar, 40, this.txtFunanushiNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 船主コードの存在チェック(宜野座)
        /// </summary>
        /// <param name="senshuCd">船主コード</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetSenshuCd(decimal senshuCd)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  COUNT(*) AS KENSU ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_SENSHU_CD_HENKAN_MST ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD    = @KAISHA_CD ");
            sql.Append("AND SENSHU_CD = @SENSHU_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 6, senshuCd);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }
        #endregion
    }
}
