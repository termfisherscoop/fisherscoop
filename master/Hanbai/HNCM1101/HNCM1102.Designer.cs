﻿namespace jp.co.fsi.hn.hncm1101
{
    partial class HNCM1102
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKenFunaCd = new System.Windows.Forms.Label();
            this.txtKenFunaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKenFunaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenFunaNm = new System.Windows.Forms.Label();
            this.txtFunanushiNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiNm = new System.Windows.Forms.Label();
            this.txtFunanushiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCd = new System.Windows.Forms.Label();
            this.gbxNagoGyo = new System.Windows.Forms.GroupBox();
            this.gbxGinozaGyo = new System.Windows.Forms.GroupBox();
            this.pnlDebug.SuspendLayout();
            this.gbxNagoGyo.SuspendLayout();
            this.gbxGinozaGyo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(260, 23);
            this.lblTitle.Text = "";
            // 
            // btnEsc
            // 
            this.btnEsc.Text = "Esc\r\n\r\n戻る";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 166);
            this.pnlDebug.Size = new System.Drawing.Size(293, 100);
            // 
            // lblKenFunaCd
            // 
            this.lblKenFunaCd.BackColor = System.Drawing.Color.Silver;
            this.lblKenFunaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenFunaCd.Location = new System.Drawing.Point(10, 20);
            this.lblKenFunaCd.Name = "lblKenFunaCd";
            this.lblKenFunaCd.Size = new System.Drawing.Size(153, 25);
            this.lblKenFunaCd.TabIndex = 0;
            this.lblKenFunaCd.Text = "名護船主CD";
            this.lblKenFunaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKenFunaCd
            // 
            this.txtKenFunaCd.AutoSizeFromLength = false;
            this.txtKenFunaCd.DisplayLength = null;
            this.txtKenFunaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenFunaCd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtKenFunaCd.Location = new System.Drawing.Point(109, 22);
            this.txtKenFunaCd.MaxLength = 6;
            this.txtKenFunaCd.Name = "txtKenFunaCd";
            this.txtKenFunaCd.Size = new System.Drawing.Size(52, 20);
            this.txtKenFunaCd.TabIndex = 1;
            this.txtKenFunaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKenFunaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenFunaCd_Validating);
            // 
            // txtKenFunaNm
            // 
            this.txtKenFunaNm.AutoSizeFromLength = false;
            this.txtKenFunaNm.DisplayLength = null;
            this.txtKenFunaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenFunaNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKenFunaNm.Location = new System.Drawing.Point(109, 56);
            this.txtKenFunaNm.MaxLength = 20;
            this.txtKenFunaNm.Name = "txtKenFunaNm";
            this.txtKenFunaNm.Size = new System.Drawing.Size(135, 20);
            this.txtKenFunaNm.TabIndex = 3;
            this.txtKenFunaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKenFunaNm_KeyDown);
            this.txtKenFunaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenFunaNm_Validating);
            // 
            // lblKenFunaNm
            // 
            this.lblKenFunaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKenFunaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKenFunaNm.Location = new System.Drawing.Point(10, 54);
            this.lblKenFunaNm.Name = "lblKenFunaNm";
            this.lblKenFunaNm.Size = new System.Drawing.Size(236, 25);
            this.lblKenFunaNm.TabIndex = 2;
            this.lblKenFunaNm.Text = "名護船主名";
            this.lblKenFunaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiNm
            // 
            this.txtFunanushiNm.AutoSizeFromLength = false;
            this.txtFunanushiNm.DisplayLength = null;
            this.txtFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFunanushiNm.Location = new System.Drawing.Point(109, 58);
            this.txtFunanushiNm.MaxLength = 20;
            this.txtFunanushiNm.Name = "txtFunanushiNm";
            this.txtFunanushiNm.Size = new System.Drawing.Size(135, 20);
            this.txtFunanushiNm.TabIndex = 3;
            this.txtFunanushiNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiNm_KeyDown);
            this.txtFunanushiNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiNm_Validating);
            // 
            // lblFunanushiNm
            // 
            this.lblFunanushiNm.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiNm.Location = new System.Drawing.Point(10, 56);
            this.lblFunanushiNm.Name = "lblFunanushiNm";
            this.lblFunanushiNm.Size = new System.Drawing.Size(236, 25);
            this.lblFunanushiNm.TabIndex = 2;
            this.lblFunanushiNm.Text = "船 主 名";
            this.lblFunanushiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCd
            // 
            this.txtFunanushiCd.AutoSizeFromLength = false;
            this.txtFunanushiCd.DisplayLength = null;
            this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtFunanushiCd.Location = new System.Drawing.Point(109, 24);
            this.txtFunanushiCd.MaxLength = 6;
            this.txtFunanushiCd.Name = "txtFunanushiCd";
            this.txtFunanushiCd.Size = new System.Drawing.Size(52, 20);
            this.txtFunanushiCd.TabIndex = 1;
            this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCd_KeyDown);
            this.txtFunanushiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCd_Validating);
            // 
            // lblFunanushiCd
            // 
            this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCd.Location = new System.Drawing.Point(10, 22);
            this.lblFunanushiCd.Name = "lblFunanushiCd";
            this.lblFunanushiCd.Size = new System.Drawing.Size(153, 25);
            this.lblFunanushiCd.TabIndex = 0;
            this.lblFunanushiCd.Text = "船主CD";
            this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxNagoGyo
            // 
            this.gbxNagoGyo.Controls.Add(this.txtKenFunaNm);
            this.gbxNagoGyo.Controls.Add(this.txtKenFunaCd);
            this.gbxNagoGyo.Controls.Add(this.lblKenFunaCd);
            this.gbxNagoGyo.Controls.Add(this.lblKenFunaNm);
            this.gbxNagoGyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxNagoGyo.Location = new System.Drawing.Point(12, 12);
            this.gbxNagoGyo.Name = "gbxNagoGyo";
            this.gbxNagoGyo.Size = new System.Drawing.Size(261, 91);
            this.gbxNagoGyo.TabIndex = 0;
            this.gbxNagoGyo.TabStop = false;
            this.gbxNagoGyo.Text = "名護漁協";
            // 
            // gbxGinozaGyo
            // 
            this.gbxGinozaGyo.Controls.Add(this.txtFunanushiNm);
            this.gbxGinozaGyo.Controls.Add(this.txtFunanushiCd);
            this.gbxGinozaGyo.Controls.Add(this.lblFunanushiNm);
            this.gbxGinozaGyo.Controls.Add(this.lblFunanushiCd);
            this.gbxGinozaGyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxGinozaGyo.Location = new System.Drawing.Point(12, 109);
            this.gbxGinozaGyo.Name = "gbxGinozaGyo";
            this.gbxGinozaGyo.Size = new System.Drawing.Size(261, 94);
            this.gbxGinozaGyo.TabIndex = 1;
            this.gbxGinozaGyo.TabStop = false;
            this.gbxGinozaGyo.Text = "漁協";
            // 
            // HNCM1102
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 269);
            this.Controls.Add(this.gbxGinozaGyo);
            this.Controls.Add(this.gbxNagoGyo);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1102";
            this.ShowFButton = true;
            this.Text = "船主変換マスタ入力";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxNagoGyo, 0);
            this.Controls.SetChildIndex(this.gbxGinozaGyo, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxNagoGyo.ResumeLayout(false);
            this.gbxNagoGyo.PerformLayout();
            this.gbxGinozaGyo.ResumeLayout(false);
            this.gbxGinozaGyo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKenFunaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKenFunaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKenFunaNm;
        private System.Windows.Forms.Label lblKenFunaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiNm;
        private System.Windows.Forms.Label lblFunanushiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCd;
        private System.Windows.Forms.GroupBox gbxNagoGyo;
        private System.Windows.Forms.GroupBox gbxGinozaGyo;


    }
}