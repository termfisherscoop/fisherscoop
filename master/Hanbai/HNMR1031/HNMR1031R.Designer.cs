﻿namespace jp.co.fsi.hn.hnmr1031
{
    /// <summary>
    /// HNMR1031R の概要の説明です。
    /// </summary>
    partial class HNMR1031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNMR1031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle06 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle05 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.page = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblGyoshuBrCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGyoshuNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMizuageSr = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeikinTk = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMizuageKg = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTitle03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtGyoshuBrCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoshuNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMizuageSr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeikinTk = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMizuageKg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtTotalMizuageSr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalHeikinTk = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalMizuageKg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuBrCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMizuageSr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeikinTk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMizuageKg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuBrCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageSr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeikinTk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageKg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalMizuageSr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHeikinTk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalMizuageKg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle01,
            this.lblTitle02,
            this.textBox1,
            this.textBox2,
            this.lblTitle06,
            this.lblTitle05,
            this.page,
            this.lblGyoshuBrCd,
            this.lblGyoshuNm,
            this.lblMizuageSr,
            this.lblHeikinTk,
            this.lblMizuageKg,
            this.line1,
            this.line2,
            this.lblTitle03,
            this.txtToday});
            this.pageHeader.Height = 1.291667F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.2625984F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 2.676772F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold";
            this.lblTitle01.Text = "＊＊＊ 魚種分類別水揚月計表 ＊＊＊";
            this.lblTitle01.Top = 0.4177166F;
            this.lblTitle01.Width = 3.583464F;
            // 
            // lblTitle02
            // 
            this.lblTitle02.Height = 0.2F;
            this.lblTitle02.HyperLink = null;
            this.lblTitle02.Left = 1.02756F;
            this.lblTitle02.Name = "lblTitle02";
            this.lblTitle02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 128";
            this.lblTitle02.Text = "～";
            this.lblTitle02.Top = 0.2177165F;
            this.lblTitle02.Width = 0.1874015F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM01";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 0.1106304F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 128";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.2177165F;
            this.textBox1.Width = 0.8440944F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM02";
            this.textBox2.Height = 0.2F;
            this.textBox2.Left = 1.27441F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 128";
            this.textBox2.Text = null;
            this.textBox2.Top = 0.2177165F;
            this.textBox2.Width = 0.8437006F;
            // 
            // lblTitle06
            // 
            this.lblTitle06.Height = 0.2F;
            this.lblTitle06.HyperLink = null;
            this.lblTitle06.Left = 7.782284F;
            this.lblTitle06.Name = "lblTitle06";
            this.lblTitle06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 128";
            this.lblTitle06.Text = "【税抜き】";
            this.lblTitle06.Top = 0.480315F;
            this.lblTitle06.Visible = false;
            this.lblTitle06.Width = 1F;
            // 
            // lblTitle05
            // 
            this.lblTitle05.Height = 0.2F;
            this.lblTitle05.HyperLink = null;
            this.lblTitle05.Left = 8.38504F;
            this.lblTitle05.Name = "lblTitle05";
            this.lblTitle05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 128";
            this.lblTitle05.Text = "頁";
            this.lblTitle05.Top = 0.2177165F;
            this.lblTitle05.Width = 0.1976394F;
            // 
            // page
            // 
            this.page.Height = 0.2F;
            this.page.Left = 7.968504F;
            this.page.Name = "page";
            this.page.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 128";
            this.page.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.page.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page.Text = null;
            this.page.Top = 0.2177165F;
            this.page.Width = 0.4165363F;
            // 
            // lblGyoshuBrCd
            // 
            this.lblGyoshuBrCd.Height = 0.2F;
            this.lblGyoshuBrCd.HyperLink = null;
            this.lblGyoshuBrCd.Left = 0.822441F;
            this.lblGyoshuBrCd.MultiLine = false;
            this.lblGyoshuBrCd.Name = "lblGyoshuBrCd";
            this.lblGyoshuBrCd.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 128";
            this.lblGyoshuBrCd.Text = "魚種分類コード";
            this.lblGyoshuBrCd.Top = 1.044095F;
            this.lblGyoshuBrCd.Width = 1.155906F;
            // 
            // lblGyoshuNm
            // 
            this.lblGyoshuNm.Height = 0.2F;
            this.lblGyoshuNm.HyperLink = null;
            this.lblGyoshuNm.Left = 2.305512F;
            this.lblGyoshuNm.Name = "lblGyoshuNm";
            this.lblGyoshuNm.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; ddo-" +
    "char-set: 128";
            this.lblGyoshuNm.Text = "魚  種  名";
            this.lblGyoshuNm.Top = 1.044095F;
            this.lblGyoshuNm.Width = 1.398425F;
            // 
            // lblMizuageSr
            // 
            this.lblMizuageSr.Height = 0.2F;
            this.lblMizuageSr.HyperLink = null;
            this.lblMizuageSr.Left = 4.062205F;
            this.lblMizuageSr.MultiLine = false;
            this.lblMizuageSr.Name = "lblMizuageSr";
            this.lblMizuageSr.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 128";
            this.lblMizuageSr.Text = "水揚数量(㎏)";
            this.lblMizuageSr.Top = 1.044095F;
            this.lblMizuageSr.Width = 1.051969F;
            // 
            // lblHeikinTk
            // 
            this.lblHeikinTk.Height = 0.2F;
            this.lblHeikinTk.HyperLink = null;
            this.lblHeikinTk.Left = 5.756693F;
            this.lblHeikinTk.MultiLine = false;
            this.lblHeikinTk.Name = "lblHeikinTk";
            this.lblHeikinTk.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 128";
            this.lblHeikinTk.Text = "平均単価\r\n";
            this.lblHeikinTk.Top = 1.044095F;
            this.lblHeikinTk.Width = 0.7783471F;
            // 
            // lblMizuageKg
            // 
            this.lblMizuageKg.Height = 0.2F;
            this.lblMizuageKg.HyperLink = null;
            this.lblMizuageKg.Left = 7.125985F;
            this.lblMizuageKg.MultiLine = false;
            this.lblMizuageKg.Name = "lblMizuageKg";
            this.lblMizuageKg.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 128";
            this.lblMizuageKg.Text = "水揚金額";
            this.lblMizuageKg.Top = 1.044095F;
            this.lblMizuageKg.Width = 0.9688971F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.5031496F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.9818906F;
            this.line1.Width = 7.947244F;
            this.line1.X1 = 0.5031496F;
            this.line1.X2 = 8.450394F;
            this.line1.Y1 = 0.9818906F;
            this.line1.Y2 = 0.9818906F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.5031496F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 1.244095F;
            this.line2.Width = 7.947244F;
            this.line2.X1 = 0.5031496F;
            this.line2.X2 = 8.450394F;
            this.line2.Y1 = 1.244095F;
            this.line2.Y2 = 1.244095F;
            // 
            // lblTitle03
            // 
            this.lblTitle03.Height = 0.2F;
            this.lblTitle03.HyperLink = null;
            this.lblTitle03.Left = 2.11811F;
            this.lblTitle03.Name = "lblTitle03";
            this.lblTitle03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 128";
            this.lblTitle03.Text = "分";
            this.lblTitle03.Top = 0.2177165F;
            this.lblTitle03.Width = 0.1874015F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM11";
            this.txtToday.Height = 0.2F;
            this.txtToday.Left = 6.759843F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 128";
            this.txtToday.Text = "yyyy/MM/dd";
            this.txtToday.Top = 0.2177165F;
            this.txtToday.Width = 1.208662F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGyoshuBrCd,
            this.txtGyoshuNm,
            this.txtMizuageSr,
            this.txtHeikinTk,
            this.txtMizuageKg});
            this.detail.Height = 0.2813976F;
            this.detail.Name = "detail";
            // 
            // txtGyoshuBrCd
            // 
            this.txtGyoshuBrCd.DataField = "ITEM03";
            this.txtGyoshuBrCd.Height = 0.2F;
            this.txtGyoshuBrCd.Left = 0.822441F;
            this.txtGyoshuBrCd.Name = "txtGyoshuBrCd";
            this.txtGyoshuBrCd.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: center; ddo-char-set: 128";
            this.txtGyoshuBrCd.Text = null;
            this.txtGyoshuBrCd.Top = 0.03464567F;
            this.txtGyoshuBrCd.Width = 1.155906F;
            // 
            // txtGyoshuNm
            // 
            this.txtGyoshuNm.DataField = "ITEM04";
            this.txtGyoshuNm.Height = 0.2F;
            this.txtGyoshuNm.Left = 2.305512F;
            this.txtGyoshuNm.Name = "txtGyoshuNm";
            this.txtGyoshuNm.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 128";
            this.txtGyoshuNm.Text = null;
            this.txtGyoshuNm.Top = 0.03464567F;
            this.txtGyoshuNm.Width = 1.62441F;
            // 
            // txtMizuageSr
            // 
            this.txtMizuageSr.DataField = "ITEM05";
            this.txtMizuageSr.Height = 0.2F;
            this.txtMizuageSr.Left = 4.062205F;
            this.txtMizuageSr.Name = "txtMizuageSr";
            this.txtMizuageSr.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtMizuageSr.Text = null;
            this.txtMizuageSr.Top = 0.03464567F;
            this.txtMizuageSr.Width = 1.051969F;
            // 
            // txtHeikinTk
            // 
            this.txtHeikinTk.DataField = "ITEM06";
            this.txtHeikinTk.Height = 0.2F;
            this.txtHeikinTk.Left = 5.673229F;
            this.txtHeikinTk.Name = "txtHeikinTk";
            this.txtHeikinTk.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtHeikinTk.Text = "123456789";
            this.txtHeikinTk.Top = 0.03464567F;
            this.txtHeikinTk.Width = 0.8618107F;
            // 
            // txtMizuageKg
            // 
            this.txtMizuageKg.DataField = "ITEM07";
            this.txtMizuageKg.Height = 0.2F;
            this.txtMizuageKg.Left = 7.125985F;
            this.txtMizuageKg.Name = "txtMizuageKg";
            this.txtMizuageKg.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtMizuageKg.Text = "1324567890";
            this.txtMizuageKg.Top = 0.03464567F;
            this.txtMizuageKg.Width = 0.9688973F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotalMizuageSr,
            this.txtTotalHeikinTk,
            this.txtTotalMizuageKg,
            this.lblTotal,
            this.line3});
            this.reportFooter1.Height = 0.2918471F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // txtTotalMizuageSr
            // 
            this.txtTotalMizuageSr.DataField = "ITEM05";
            this.txtTotalMizuageSr.Height = 0.2F;
            this.txtTotalMizuageSr.Left = 4.062205F;
            this.txtTotalMizuageSr.Name = "txtTotalMizuageSr";
            this.txtTotalMizuageSr.OutputFormat = resources.GetString("txtTotalMizuageSr.OutputFormat");
            this.txtTotalMizuageSr.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotalMizuageSr.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotalMizuageSr.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotalMizuageSr.Text = null;
            this.txtTotalMizuageSr.Top = 0.03976378F;
            this.txtTotalMizuageSr.Width = 1.05197F;
            // 
            // txtTotalHeikinTk
            // 
            this.txtTotalHeikinTk.DataField = "ITEM08";
            this.txtTotalHeikinTk.Height = 0.2F;
            this.txtTotalHeikinTk.Left = 5.673229F;
            this.txtTotalHeikinTk.Name = "txtTotalHeikinTk";
            this.txtTotalHeikinTk.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotalHeikinTk.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotalHeikinTk.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotalHeikinTk.Text = null;
            this.txtTotalHeikinTk.Top = 0.03976378F;
            this.txtTotalHeikinTk.Width = 0.8618107F;
            // 
            // txtTotalMizuageKg
            // 
            this.txtTotalMizuageKg.DataField = "ITEM07";
            this.txtTotalMizuageKg.Height = 0.2F;
            this.txtTotalMizuageKg.Left = 6.770473F;
            this.txtTotalMizuageKg.Name = "txtTotalMizuageKg";
            this.txtTotalMizuageKg.OutputFormat = resources.GetString("txtTotalMizuageKg.OutputFormat");
            this.txtTotalMizuageKg.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; ddo-char-set: 128";
            this.txtTotalMizuageKg.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotalMizuageKg.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotalMizuageKg.Text = null;
            this.txtTotalMizuageKg.Top = 0.03976378F;
            this.txtTotalMizuageKg.Width = 1.32441F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.2F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 1.565354F;
            this.lblTotal.MultiLine = false;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 128";
            this.lblTotal.Text = "[合計]";
            this.lblTotal.Top = 0.03976378F;
            this.lblTotal.Width = 0.8125986F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0.5031496F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0F;
            this.line3.Width = 7.947245F;
            this.line3.X1 = 0.5031496F;
            this.line3.X2 = 8.450395F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0F;
            // 
            // HNMR1031R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.5905512F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 8.937011F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuBrCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMizuageSr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeikinTk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMizuageKg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuBrCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageSr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeikinTk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageKg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalMizuageSr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHeikinTk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalMizuageKg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox page;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuBrCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMizuageSr;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeikinTk;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMizuageKg;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuBrCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMizuageSr;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeikinTk;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMizuageKg;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalMizuageSr;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalHeikinTk;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalMizuageKg;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
    }
}
