﻿namespace jp.co.fsi.hn.hncm1051
{
    partial class HNCM1053
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblGyoshuCdTo = new System.Windows.Forms.Label();
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.txtGyoshuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGyoshuCdFr = new System.Windows.Forms.Label();
			this.txtGyoshuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF2
			// 
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Visible = false;
			// 
			// btnF7
			// 
			this.btnF7.Visible = false;
			// 
			// btnF6
			// 
			this.btnF6.Visible = false;
			// 
			// btnF8
			// 
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 119);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(794, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(783, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblGyoshuCdTo
			// 
			this.lblGyoshuCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblGyoshuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblGyoshuCdTo.Location = new System.Drawing.Point(452, 19);
			this.lblGyoshuCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyoshuCdTo.Name = "lblGyoshuCdTo";
			this.lblGyoshuCdTo.Size = new System.Drawing.Size(289, 24);
			this.lblGyoshuCdTo.TabIndex = 4;
			this.lblGyoshuCdTo.Tag = "DISPNAME";
			this.lblGyoshuCdTo.Text = "最　後";
			this.lblGyoshuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblCodeBet.Location = new System.Drawing.Point(360, 19);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 24);
			this.lblCodeBet.TabIndex = 2;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyoshuCdFr
			// 
			this.txtGyoshuCdFr.AutoSizeFromLength = false;
			this.txtGyoshuCdFr.DisplayLength = null;
			this.txtGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtGyoshuCdFr.Location = new System.Drawing.Point(4, 20);
			this.txtGyoshuCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyoshuCdFr.MaxLength = 4;
			this.txtGyoshuCdFr.Name = "txtGyoshuCdFr";
			this.txtGyoshuCdFr.Size = new System.Drawing.Size(52, 23);
			this.txtGyoshuCdFr.TabIndex = 0;
			this.txtGyoshuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyoshuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdFr_Validating);
			// 
			// lblGyoshuCdFr
			// 
			this.lblGyoshuCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblGyoshuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblGyoshuCdFr.Location = new System.Drawing.Point(62, 19);
			this.lblGyoshuCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyoshuCdFr.Name = "lblGyoshuCdFr";
			this.lblGyoshuCdFr.Size = new System.Drawing.Size(289, 24);
			this.lblGyoshuCdFr.TabIndex = 1;
			this.lblGyoshuCdFr.Tag = "DISPNAME";
			this.lblGyoshuCdFr.Text = "先　頭";
			this.lblGyoshuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyoshuCdTo
			// 
			this.txtGyoshuCdTo.AutoSizeFromLength = false;
			this.txtGyoshuCdTo.DisplayLength = null;
			this.txtGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtGyoshuCdTo.Location = new System.Drawing.Point(395, 20);
			this.txtGyoshuCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyoshuCdTo.MaxLength = 4;
			this.txtGyoshuCdTo.Name = "txtGyoshuCdTo";
			this.txtGyoshuCdTo.Size = new System.Drawing.Size(52, 23);
			this.txtGyoshuCdTo.TabIndex = 3;
			this.txtGyoshuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyoshuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdTo_Validating);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(6, 34);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 1;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(771, 57);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.lblGyoshuCdTo);
			this.fsiPanel1.Controls.Add(this.txtGyoshuCdFr);
			this.fsiPanel1.Controls.Add(this.txtGyoshuCdTo);
			this.fsiPanel1.Controls.Add(this.lblCodeBet);
			this.fsiPanel1.Controls.Add(this.lblGyoshuCdFr);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(763, 49);
			this.fsiPanel1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(763, 49);
			this.label1.TabIndex = 1;
			this.label1.Text = "魚種CD範囲";
			// 
			// HNCM1053
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(783, 256);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1053";
			this.ShowFButton = true;
			this.Text = "";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblGyoshuCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtGyoshuCdFr;
        private System.Windows.Forms.Label lblGyoshuCdFr;
        private common.controls.FsiTextBox txtGyoshuCdTo;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label1;
    }
}