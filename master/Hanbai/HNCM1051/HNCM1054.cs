﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1051
{
    /// <summary>
    /// 魚種マスタ初期設定(HNCM1054)
    /// </summary>
    public partial class HNCM1054 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1054()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            InitDisp();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShiwakeKbn":
                case "txtJimotoShiwakeCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            System.Reflection.Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtShiwakeKbn":
                case "txtJimotoShiwakeCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2051.CMCM2053");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.ActiveCtlNm == "txtShiwakeKbn")
                            {
                                frm.InData = this.txtShiwakeKbn.Text;
                            }
                            else if (this.ActiveCtlNm == "txtJimotoShiwakeCd")
                            {
                                frm.InData = this.txtJimotoShiwakeCd.Text;
                            }
                            frm.Par1 = "3";
                            frm.Par2 = this.Par1;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtShiwakeKbn")
                                {
                                    this.txtShiwakeKbn.Text = outData[0];
                                    this.lblShiwakeKbnNm.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtJimotoShiwakeCd")
                                {
                                    this.txtJimotoShiwakeCd.Text = outData[0];
                                    this.lblJimotoShiwakeNm.Text = outData[1];
                                }

                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = "登録しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            SaveData();

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 仕訳区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiwakeKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiwakeKbn())
            {
                e.Cancel = true;
                this.txtShiwakeKbn.SelectAll();
            }
        }

        /// <summary>
        /// 地元仕訳コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJimotoShiwakeCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJimotoShiwake())
            {
                e.Cancel = true;
                this.txtJimotoShiwakeCd.SelectAll();
            }
        }

        private void txtJimotoShiwakeCd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 初期設定値の表示
        /// </summary>
        private void InitDisp()
        {
            // 初期値、入力制御を実装
            // 仕訳区分の初期値を設定
            string ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShiwakeCd"));
            this.txtShiwakeKbn.Text = ret;
            this.lblShiwakeKbnNm.Text =  this.GetZidShiwakeSetteiBNm(this.txtShiwakeKbn.Text);
            // 地元仕訳コードの初期値を設定
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "JimotoShiwakeCd"));
            this.txtJimotoShiwakeCd.Text = ret;
            this.lblJimotoShiwakeNm.Text = this.GetZidShiwakeSetteiBNm(this.txtJimotoShiwakeCd.Text);

            // 魚種CDに初期フォーカス
            this.ActiveControl = this.txtShiwakeKbn;
            this.txtShiwakeKbn.Focus();
        }

        private void SaveData()
        {
            // 初期値、入力制御を実装
            string ret = "";
            // 仕訳区分の初期値を設定
            ret = this.txtShiwakeKbn.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShiwakeCd", ret);
            // 地元仕訳コードの初期値を設定
            ret = this.txtJimotoShiwakeCd.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "JimotoShiwakeCd", ret);

            // 設定値の保存
            this.Config.SaveConfig();
        }
        /// <summary>
        /// 仕訳区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiwakeKbn()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShiwakeKbn.Text))
            {
                this.txtShiwakeKbn.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiwakeKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtShiwakeKbn.Text))
            {
                this.lblShiwakeKbnNm.Text = string.Empty;
            }
            else
            {
                string ShiwakeKbn = this.GetZidShiwakeSetteiBNm(this.txtShiwakeKbn.Text);
                if (ValChk.IsEmpty(ShiwakeKbn))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblShiwakeKbnNm.Text = ShiwakeKbn;
            }

            return true;
        }

        /// <summary>
        /// 地元仕訳コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJimotoShiwake()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtJimotoShiwakeCd.Text))
            {
                this.txtJimotoShiwakeCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtJimotoShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtJimotoShiwakeCd.Text))
            {
                this.lblJimotoShiwakeNm.Text = string.Empty;
            }
            else
            {
                string JimotoShiwake = this.GetZidShiwakeSetteiBNm(this.txtJimotoShiwakeCd.Text);
                if (ValChk.IsEmpty(JimotoShiwake))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblJimotoShiwakeNm.Text = JimotoShiwake;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 仕訳区分のチェック
            if (!IsValidShiwakeKbn())
            {
                this.txtShiwakeKbn.Focus();
                return false;
            }

            // 地元仕訳コードのチェック
            if (!IsValidJimotoShiwake())
            {
                this.txtJimotoShiwakeCd.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 自動仕訳設定Ｂテーブルから仕訳名を取得
        /// </summary>
        /// <param name="code">仕訳コード</param>
        /// <returns>
        /// 仕訳名
        /// </returns>
        private string GetZidShiwakeSetteiBNm(string code)
        {
            string ret = "";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 3);
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 6, code);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
            where.Append(" AND SHIWAKE_CD = @SHIWAKE_CD");
            DataTable dtNm =
                this.Dba.GetDataTableByConditionWithParams("SHIWAKE_NM AS NM",
                    "VI_HN_ZIDO_SHIWAKE_SETTEI_B", Util.ToString(where), dpc);
            if (dtNm.Rows.Count > 0 && !ValChk.IsEmpty(dtNm.Rows[0]["NM"]))
            {
                ret = Util.ToString(dtNm.Rows[0]["NM"]);
            }
            
            return ret;
        }
        #endregion

    }
}
