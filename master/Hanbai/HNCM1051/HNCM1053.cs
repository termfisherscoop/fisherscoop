﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

//using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1051
{
    /// <summary>
    /// 魚種マスタ一覧(HNCM1053)
    /// </summary>
    public partial class HNCM1053 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1053()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // ボタンの位置調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;
            // フォーカス設定
            this.txtGyoshuCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            //魚種CDに、
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGyoshuCdFr":
                case "txtGyoshuCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }
        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtGyoshuCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("hncm1051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1051.hncm1051");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtGyoshuCdFr.Text = outData[0];
                            }
                        }
                    }
                    break;

                case "txtGyoshuCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("hncm1051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1051.hncm1051");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtGyoshuCdTo.Text = outData[0];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }
        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // プレビュー処理
            DoPrint(true);
        }
        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        #endregion

        #region イベント
        /// <summary>
        /// 魚種コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshuCdFr())
            {
                e.Cancel = true;
                this.txtGyoshuCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 魚種コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshuCdTo())
            {
                e.Cancel = true;
                this.txtGyoshuCdTo.SelectAll();
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 魚種コードの入力チェック(先頭）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyoshuCdFr()
        {
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.txtGyoshuCdFr.Text))
            {
                this.lblGyoshuCdFr.Text = "先　頭";
                return true;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyoshuCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "TB_HN_GYOSHU", "",this.txtGyoshuCdFr.Text);
            this.lblGyoshuCdFr.Text = name;
            return true;
        }
        /// <summary>
        /// 魚種コードの入力チェック(最後）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyoshuCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtGyoshuCdTo.Text))
            {
                this.lblGyoshuCdTo.Text = "最　後";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyoshuCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "TB_HN_GYOSHU", "",this.txtGyoshuCdTo.Text);
            this.lblGyoshuCdTo.Text = name;
            return true;
        }
        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 魚種コードのチェック
            if (!IsValidGyoshuCdFr())
            {
                this.txtGyoshuCdFr.Focus();
                this.txtGyoshuCdFr.SelectAll();
                return false;
            }

            // 魚種コードのチェック
            if (!IsValidGyoshuCdTo())
            {
                this.txtGyoshuCdTo.Focus();
                this.txtGyoshuCdTo.SelectAll();
                return false;
            }
            return true;
        }


        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            bool dataFlag;
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                //Report rpt = new Report();
                //rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "hncm1051.mdb"), "R_hncm1051", this.UnqId, isPreview);
            }

            // ワークテーブルに作成したデータを削除
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                this.Dba.Delete("PR_HN_TBL", "GUID = @GUID", dpc);
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備

            // 魚種コード設定
            string gyoshuCdFr;
            string gyoshuCdTo;
            string gyoshuNmFr;
            string gyoshuNmTo;
            if (Util.ToDecimal(txtGyoshuCdFr.Text) > 0)
            {
                gyoshuCdFr = txtGyoshuCdFr.Text;
            }
            else
            {
                gyoshuCdFr = "0";
            }
            if (Util.ToDecimal(txtGyoshuCdTo.Text) > 0)
            {
                gyoshuCdTo = txtGyoshuCdTo.Text;
            }
            else
            {
                gyoshuCdTo = "9999";
            }
            gyoshuNmFr = lblGyoshuCdFr.Text;
            gyoshuNmTo = lblGyoshuCdTo.Text;
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            //VI_HN_TORIHIKISAKI_JOHOからデータ取得
            sql.Append(" SELECT");
            sql.Append("  A.GYOSHU_BUNRUI_CD    AS  魚種分類コード ");
            sql.Append(" ,A.GYOSHU_BUNRUI_NM    AS  魚種分類名称 ");
            sql.Append(" ,B.GYOSHU_CD           AS  魚種コード ");
            sql.Append(" ,B.GYOSHU_NM           AS  魚種名 ");
            sql.Append(" ,B.GYOSHU_KANA_NM      AS  魚種カナ名 ");
            sql.Append(" FROM ");
            sql.Append(" TB_HN_GYOSHU_BUNRUI_MST AS A ");
            sql.Append(" LEFT JOIN TB_HN_GYOSHU  AS B ");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append(" AND B.GYOSHU_BUNRUI_CD = A.GYOSHU_BUNRUI_CD ");
            sql.Append(" WHERE");
            sql.Append(" B.KAISHA_CD = @KAISHA_CD ");
            sql.Append(" B.GYOSHU_CD BETWEEN @GYOSHU_CD_FR AND @GYOSHU_CD_TO");
            sql.Append(" ORDER BY ");
            sql.Append(" A.GYOSHU_BUNRUI_CD, ");
            sql.Append(" B.GYOSHU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOSHU_CD_FR", SqlDbType.Decimal, 6, gyoshuCdFr);
            dpc.SetParam("@GYOSHU_CD_TO", SqlDbType.Decimal, 6, gyoshuCdTo);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("INSERT INTO PR_HN_TBL(");
                    sql.Append("  GUID");
                    sql.Append(" ,SORT");
                    sql.Append(" ,ITEM01");
                    sql.Append(" ,ITEM02");
                    sql.Append(" ,ITEM03");
                    sql.Append(" ,ITEM04");
                    sql.Append(" ,ITEM05");
                    sql.Append(" ,ITEM06");
                    sql.Append(" ,ITEM07");
                    sql.Append(" ,ITEM08");
                    sql.Append(" ,ITEM09");
                    sql.Append(") ");
                    sql.Append("VALUES(");
                    sql.Append("  @GUID");
                    sql.Append(" ,@SORT");
                    sql.Append(" ,@ITEM01");
                    sql.Append(" ,@ITEM02");
                    sql.Append(" ,@ITEM03");
                    sql.Append(" ,@ITEM04");
                    sql.Append(" ,@ITEM05");
                    sql.Append(" ,@ITEM06");
                    sql.Append(" ,@ITEM07");
                    sql.Append(" ,@ITEM08");
                    sql.Append(" ,@ITEM09");
                    sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, gyoshuCdFr); // 魚種コード（先頭）
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, gyoshuCdTo); // 魚種コード（最後）
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, gyoshuNmFr); // 魚種名（先頭）
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, gyoshuNmTo); // 魚種名（最後）
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["魚種分類コード"]); // 魚種分類コード
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["魚種分類名称"]);// 魚種分類名称
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["魚種コード"]);// 魚種コード
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["魚種名"]); // 魚種名称
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion

                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}

