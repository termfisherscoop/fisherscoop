﻿namespace jp.co.fsi.hn.hncm1051
{
    partial class HNCM1052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblGyosyuCd = new System.Windows.Forms.Label();
			this.txtGyosyuCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtGyosyuNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGyosyuNm = new System.Windows.Forms.Label();
			this.txtGyosyuKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGyosyuKanaNm = new System.Windows.Forms.Label();
			this.txtShiwakeKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiwakeKbn = new System.Windows.Forms.Label();
			this.lblShiwakeKbnNm = new System.Windows.Forms.Label();
			this.lblJimotoShiwakeNm = new System.Windows.Forms.Label();
			this.txtJimotoShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblJimotoShiwakeCd = new System.Windows.Forms.Label();
			this.lblGyosyuBunruiNm = new System.Windows.Forms.Label();
			this.txtGyosyuBunrui = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGyosyuBunrui = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel7.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 262);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1150, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1139, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblGyosyuCd
			// 
			this.lblGyosyuCd.BackColor = System.Drawing.Color.Silver;
			this.lblGyosyuCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGyosyuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyosyuCd.Location = new System.Drawing.Point(0, 0);
			this.lblGyosyuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyosyuCd.Name = "lblGyosyuCd";
			this.lblGyosyuCd.Size = new System.Drawing.Size(526, 34);
			this.lblGyosyuCd.TabIndex = 0;
			this.lblGyosyuCd.Tag = "CHANGE";
			this.lblGyosyuCd.Text = "魚　種　C　D";
			this.lblGyosyuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyosyuCd
			// 
			this.txtGyosyuCd.AutoSizeFromLength = false;
			this.txtGyosyuCd.DisplayLength = null;
			this.txtGyosyuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyosyuCd.Location = new System.Drawing.Point(148, 5);
			this.txtGyosyuCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyosyuCd.MaxLength = 15;
			this.txtGyosyuCd.Name = "txtGyosyuCd";
			this.txtGyosyuCd.Size = new System.Drawing.Size(69, 23);
			this.txtGyosyuCd.TabIndex = 1;
			this.txtGyosyuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyosyuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyosyuCd_Validating);
			// 
			// txtGyosyuNm
			// 
			this.txtGyosyuNm.AutoSizeFromLength = false;
			this.txtGyosyuNm.DisplayLength = null;
			this.txtGyosyuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyosyuNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtGyosyuNm.Location = new System.Drawing.Point(148, 5);
			this.txtGyosyuNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyosyuNm.MaxLength = 40;
			this.txtGyosyuNm.Name = "txtGyosyuNm";
			this.txtGyosyuNm.Size = new System.Drawing.Size(299, 23);
			this.txtGyosyuNm.TabIndex = 3;
			this.txtGyosyuNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyosyuNm_Validating);
			// 
			// lblGyosyuNm
			// 
			this.lblGyosyuNm.BackColor = System.Drawing.Color.Silver;
			this.lblGyosyuNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGyosyuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyosyuNm.Location = new System.Drawing.Point(0, 0);
			this.lblGyosyuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyosyuNm.Name = "lblGyosyuNm";
			this.lblGyosyuNm.Size = new System.Drawing.Size(526, 34);
			this.lblGyosyuNm.TabIndex = 2;
			this.lblGyosyuNm.Tag = "CHANGE";
			this.lblGyosyuNm.Text = "魚　種　名";
			this.lblGyosyuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyosyuKanaNm
			// 
			this.txtGyosyuKanaNm.AutoSizeFromLength = false;
			this.txtGyosyuKanaNm.DisplayLength = null;
			this.txtGyosyuKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyosyuKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtGyosyuKanaNm.Location = new System.Drawing.Point(148, 5);
			this.txtGyosyuKanaNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyosyuKanaNm.MaxLength = 15;
			this.txtGyosyuKanaNm.Name = "txtGyosyuKanaNm";
			this.txtGyosyuKanaNm.Size = new System.Drawing.Size(299, 23);
			this.txtGyosyuKanaNm.TabIndex = 5;
			this.txtGyosyuKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyosyuKanaNm_Validating);
			// 
			// lblGyosyuKanaNm
			// 
			this.lblGyosyuKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblGyosyuKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGyosyuKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyosyuKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblGyosyuKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyosyuKanaNm.Name = "lblGyosyuKanaNm";
			this.lblGyosyuKanaNm.Size = new System.Drawing.Size(526, 34);
			this.lblGyosyuKanaNm.TabIndex = 4;
			this.lblGyosyuKanaNm.Tag = "CHANGE";
			this.lblGyosyuKanaNm.Text = "魚 種 カ ナ 名";
			this.lblGyosyuKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiwakeKbn
			// 
			this.txtShiwakeKbn.AutoSizeFromLength = false;
			this.txtShiwakeKbn.DisplayLength = null;
			this.txtShiwakeKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiwakeKbn.Location = new System.Drawing.Point(148, 6);
			this.txtShiwakeKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiwakeKbn.MaxLength = 6;
			this.txtShiwakeKbn.Name = "txtShiwakeKbn";
			this.txtShiwakeKbn.Size = new System.Drawing.Size(69, 23);
			this.txtShiwakeKbn.TabIndex = 7;
			this.txtShiwakeKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiwakeKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiwakeKbn_Validating);
			// 
			// lblShiwakeKbn
			// 
			this.lblShiwakeKbn.BackColor = System.Drawing.Color.Silver;
			this.lblShiwakeKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShiwakeKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiwakeKbn.Location = new System.Drawing.Point(0, 0);
			this.lblShiwakeKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiwakeKbn.Name = "lblShiwakeKbn";
			this.lblShiwakeKbn.Size = new System.Drawing.Size(526, 34);
			this.lblShiwakeKbn.TabIndex = 6;
			this.lblShiwakeKbn.Tag = "CHANGE";
			this.lblShiwakeKbn.Text = "仕　訳　区　分";
			this.lblShiwakeKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShiwakeKbnNm
			// 
			this.lblShiwakeKbnNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblShiwakeKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiwakeKbnNm.Location = new System.Drawing.Point(224, 6);
			this.lblShiwakeKbnNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiwakeKbnNm.Name = "lblShiwakeKbnNm";
			this.lblShiwakeKbnNm.Size = new System.Drawing.Size(281, 24);
			this.lblShiwakeKbnNm.TabIndex = 8;
			this.lblShiwakeKbnNm.Tag = "DISPNAME";
			this.lblShiwakeKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJimotoShiwakeNm
			// 
			this.lblJimotoShiwakeNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblJimotoShiwakeNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJimotoShiwakeNm.Location = new System.Drawing.Point(224, 5);
			this.lblJimotoShiwakeNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJimotoShiwakeNm.Name = "lblJimotoShiwakeNm";
			this.lblJimotoShiwakeNm.Size = new System.Drawing.Size(281, 24);
			this.lblJimotoShiwakeNm.TabIndex = 11;
			this.lblJimotoShiwakeNm.Tag = "DISPNAME";
			this.lblJimotoShiwakeNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtJimotoShiwakeCd
			// 
			this.txtJimotoShiwakeCd.AutoSizeFromLength = false;
			this.txtJimotoShiwakeCd.DisplayLength = null;
			this.txtJimotoShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtJimotoShiwakeCd.Location = new System.Drawing.Point(148, 5);
			this.txtJimotoShiwakeCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtJimotoShiwakeCd.MaxLength = 6;
			this.txtJimotoShiwakeCd.Name = "txtJimotoShiwakeCd";
			this.txtJimotoShiwakeCd.Size = new System.Drawing.Size(69, 23);
			this.txtJimotoShiwakeCd.TabIndex = 10;
			this.txtJimotoShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtJimotoShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtJimotoShiwakeCd_Validating);
			// 
			// lblJimotoShiwakeCd
			// 
			this.lblJimotoShiwakeCd.BackColor = System.Drawing.Color.Silver;
			this.lblJimotoShiwakeCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJimotoShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJimotoShiwakeCd.Location = new System.Drawing.Point(0, 0);
			this.lblJimotoShiwakeCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJimotoShiwakeCd.Name = "lblJimotoShiwakeCd";
			this.lblJimotoShiwakeCd.Size = new System.Drawing.Size(526, 34);
			this.lblJimotoShiwakeCd.TabIndex = 9;
			this.lblJimotoShiwakeCd.Tag = "CHANGE";
			this.lblJimotoShiwakeCd.Text = "地元仕訳コード";
			this.lblJimotoShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblGyosyuBunruiNm
			// 
			this.lblGyosyuBunruiNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblGyosyuBunruiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyosyuBunruiNm.Location = new System.Drawing.Point(224, 5);
			this.lblGyosyuBunruiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyosyuBunruiNm.Name = "lblGyosyuBunruiNm";
			this.lblGyosyuBunruiNm.Size = new System.Drawing.Size(281, 24);
			this.lblGyosyuBunruiNm.TabIndex = 14;
			this.lblGyosyuBunruiNm.Tag = "DISPNAME";
			this.lblGyosyuBunruiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyosyuBunrui
			// 
			this.txtGyosyuBunrui.AutoSizeFromLength = false;
			this.txtGyosyuBunrui.DisplayLength = null;
			this.txtGyosyuBunrui.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyosyuBunrui.Location = new System.Drawing.Point(148, 6);
			this.txtGyosyuBunrui.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyosyuBunrui.MaxLength = 5;
			this.txtGyosyuBunrui.Name = "txtGyosyuBunrui";
			this.txtGyosyuBunrui.Size = new System.Drawing.Size(69, 23);
			this.txtGyosyuBunrui.TabIndex = 13;
			this.txtGyosyuBunrui.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyosyuBunrui.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGyosyuBunrui_KeyDown);
			this.txtGyosyuBunrui.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyosyuBunrui_Validating);
			// 
			// lblGyosyuBunrui
			// 
			this.lblGyosyuBunrui.BackColor = System.Drawing.Color.Silver;
			this.lblGyosyuBunrui.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGyosyuBunrui.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyosyuBunrui.Location = new System.Drawing.Point(0, 0);
			this.lblGyosyuBunrui.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyosyuBunrui.Name = "lblGyosyuBunrui";
			this.lblGyosyuBunrui.Size = new System.Drawing.Size(526, 35);
			this.lblGyosyuBunrui.TabIndex = 12;
			this.lblGyosyuBunrui.Tag = "CHANGE";
			this.lblGyosyuBunrui.Text = "魚　種　分　類";
			this.lblGyosyuBunrui.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(148, 5);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1001;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(222, 4);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 1002;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(526, 34);
			this.lblMizuageShisho.TabIndex = 1000;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(9, 11);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 7;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(534, 289);
			this.fsiTableLayoutPanel1.TabIndex = 1003;
			// 
			// fsiPanel7
			// 
			this.fsiPanel7.Controls.Add(this.lblGyosyuBunruiNm);
			this.fsiPanel7.Controls.Add(this.txtGyosyuBunrui);
			this.fsiPanel7.Controls.Add(this.lblGyosyuBunrui);
			this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel7.Location = new System.Drawing.Point(4, 250);
			this.fsiPanel7.Name = "fsiPanel7";
			this.fsiPanel7.Size = new System.Drawing.Size(526, 35);
			this.fsiPanel7.TabIndex = 6;
			this.fsiPanel7.Tag = "CHANGE";
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.txtJimotoShiwakeCd);
			this.fsiPanel6.Controls.Add(this.lblJimotoShiwakeNm);
			this.fsiPanel6.Controls.Add(this.lblJimotoShiwakeCd);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel6.Location = new System.Drawing.Point(4, 209);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(526, 34);
			this.fsiPanel6.TabIndex = 5;
			this.fsiPanel6.Tag = "CHANGE";
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.txtShiwakeKbn);
			this.fsiPanel5.Controls.Add(this.lblShiwakeKbnNm);
			this.fsiPanel5.Controls.Add(this.lblShiwakeKbn);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 168);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(526, 34);
			this.fsiPanel5.TabIndex = 4;
			this.fsiPanel5.Tag = "CHANGE";
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtGyosyuKanaNm);
			this.fsiPanel4.Controls.Add(this.lblGyosyuKanaNm);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 127);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(526, 34);
			this.fsiPanel4.TabIndex = 3;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtGyosyuNm);
			this.fsiPanel3.Controls.Add(this.lblGyosyuNm);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 86);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(526, 34);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtGyosyuCd);
			this.fsiPanel2.Controls.Add(this.lblGyosyuCd);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 45);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(526, 34);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(526, 34);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNCM1052
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1139, 399);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1052";
			this.ShowFButton = true;
			this.Text = "";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel7.ResumeLayout(false);
			this.fsiPanel7.PerformLayout();
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblGyosyuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyosyuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyosyuNm;
        private System.Windows.Forms.Label lblGyosyuNm;
        private jp.co.fsi.common.controls.FsiTextBox txtGyosyuKanaNm;
        private System.Windows.Forms.Label lblGyosyuKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShiwakeKbn;
        private System.Windows.Forms.Label lblShiwakeKbn;
        private System.Windows.Forms.Label lblShiwakeKbnNm;
        private System.Windows.Forms.Label lblJimotoShiwakeNm;
        private common.controls.FsiTextBox txtJimotoShiwakeCd;
        private System.Windows.Forms.Label lblJimotoShiwakeCd;
        private System.Windows.Forms.Label lblGyosyuBunruiNm;
        private common.controls.FsiTextBox txtGyosyuBunrui;
        private System.Windows.Forms.Label lblGyosyuBunrui;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}