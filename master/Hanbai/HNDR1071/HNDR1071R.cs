﻿using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hndr1071
{
    /// <summary>
    /// HNDR1071R の概要の説明です。
    /// </summary>
    public partial class HNDR1071R : BaseReport
    {
        public HNDR1071R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void grpFooterChiku_Format(object sender, System.EventArgs e)
        {
            this.txtShokeiTanka.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.txtShokeiMizuageKingaku.Text) / Util.ToDecimal(this.txtSyokeiSuryo.Text)));
        }

        private void grpFooterSeribi_Format(object sender, System.EventArgs e)
        {
            this.txtSokeiTanka.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.txtSokeiMizuageKingaku.Text) / Util.ToDecimal(this.txtSokeiSuryo.Text)));
        }
    }
}
