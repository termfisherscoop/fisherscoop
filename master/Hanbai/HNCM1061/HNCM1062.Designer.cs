﻿namespace jp.co.fsi.hn.hncm1061
{
    partial class HNCM1062
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txtFuneNmCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFuneNmCd = new System.Windows.Forms.Label();
			this.lblGyoshubnNM = new System.Windows.Forms.Label();
			this.txtFuneKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGyoshubnKana = new System.Windows.Forms.Label();
			this.txtFuneNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTonSu = new System.Windows.Forms.Label();
			this.txtTonSu = new jp.co.fsi.common.controls.FsiTextBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 159);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(481, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(470, 31);
			this.lblTitle.Text = "";
			// 
			// txtFuneNmCd
			// 
			this.txtFuneNmCd.AutoSizeFromLength = true;
			this.txtFuneNmCd.DisplayLength = null;
			this.txtFuneNmCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFuneNmCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtFuneNmCd.Location = new System.Drawing.Point(103, 2);
			this.txtFuneNmCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtFuneNmCd.MaxLength = 5;
			this.txtFuneNmCd.Name = "txtFuneNmCd";
			this.txtFuneNmCd.Size = new System.Drawing.Size(73, 23);
			this.txtFuneNmCd.TabIndex = 2;
			this.txtFuneNmCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFuneNmCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuneNmCd_Validating);
			// 
			// lblFuneNmCd
			// 
			this.lblFuneNmCd.BackColor = System.Drawing.Color.Silver;
			this.lblFuneNmCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFuneNmCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFuneNmCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFuneNmCd.Location = new System.Drawing.Point(0, 0);
			this.lblFuneNmCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFuneNmCd.Name = "lblFuneNmCd";
			this.lblFuneNmCd.Size = new System.Drawing.Size(442, 28);
			this.lblFuneNmCd.TabIndex = 1;
			this.lblFuneNmCd.Tag = "CHANGE";
			this.lblFuneNmCd.Text = "船名コード";
			this.lblFuneNmCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblGyoshubnNM
			// 
			this.lblGyoshubnNM.BackColor = System.Drawing.Color.Silver;
			this.lblGyoshubnNM.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGyoshubnNM.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyoshubnNM.Location = new System.Drawing.Point(0, 0);
			this.lblGyoshubnNM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyoshubnNM.Name = "lblGyoshubnNM";
			this.lblGyoshubnNM.Size = new System.Drawing.Size(442, 28);
			this.lblGyoshubnNM.TabIndex = 3;
			this.lblGyoshubnNM.Tag = "CHANGE";
			this.lblGyoshubnNM.Text = "船　　名";
			this.lblGyoshubnNM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFuneKanaNm
			// 
			this.txtFuneKanaNm.AutoSizeFromLength = false;
			this.txtFuneKanaNm.DisplayLength = null;
			this.txtFuneKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFuneKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtFuneKanaNm.Location = new System.Drawing.Point(103, 2);
			this.txtFuneKanaNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtFuneKanaNm.MaxLength = 20;
			this.txtFuneKanaNm.Name = "txtFuneKanaNm";
			this.txtFuneKanaNm.Size = new System.Drawing.Size(229, 23);
			this.txtFuneKanaNm.TabIndex = 6;
			this.txtFuneKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuneKanaNm_Validating);
			// 
			// lblGyoshubnKana
			// 
			this.lblGyoshubnKana.BackColor = System.Drawing.Color.Silver;
			this.lblGyoshubnKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyoshubnKana.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGyoshubnKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyoshubnKana.Location = new System.Drawing.Point(0, 0);
			this.lblGyoshubnKana.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyoshubnKana.Name = "lblGyoshubnKana";
			this.lblGyoshubnKana.Size = new System.Drawing.Size(442, 28);
			this.lblGyoshubnKana.TabIndex = 5;
			this.lblGyoshubnKana.Tag = "CHANGE";
			this.lblGyoshubnKana.Text = "船カナ名";
			this.lblGyoshubnKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFuneNm
			// 
			this.txtFuneNm.AutoSizeFromLength = false;
			this.txtFuneNm.DisplayLength = null;
			this.txtFuneNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFuneNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtFuneNm.Location = new System.Drawing.Point(103, 3);
			this.txtFuneNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtFuneNm.MaxLength = 20;
			this.txtFuneNm.Name = "txtFuneNm";
			this.txtFuneNm.Size = new System.Drawing.Size(229, 23);
			this.txtFuneNm.TabIndex = 4;
			this.txtFuneNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuneNm_Validating);
			// 
			// lblTonSu
			// 
			this.lblTonSu.BackColor = System.Drawing.Color.Silver;
			this.lblTonSu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTonSu.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTonSu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTonSu.Location = new System.Drawing.Point(0, 0);
			this.lblTonSu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTonSu.Name = "lblTonSu";
			this.lblTonSu.Size = new System.Drawing.Size(442, 28);
			this.lblTonSu.TabIndex = 7;
			this.lblTonSu.Tag = "CHANGE";
			this.lblTonSu.Text = "トン数";
			this.lblTonSu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTonSu
			// 
			this.txtTonSu.AutoSizeFromLength = false;
			this.txtTonSu.DisplayLength = null;
			this.txtTonSu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTonSu.ImeMode = System.Windows.Forms.ImeMode.Alpha;
			this.txtTonSu.Location = new System.Drawing.Point(103, 2);
			this.txtTonSu.Margin = new System.Windows.Forms.Padding(4);
			this.txtTonSu.MaxLength = 5;
			this.txtTonSu.Name = "txtTonSu";
			this.txtTonSu.Size = new System.Drawing.Size(73, 23);
			this.txtTonSu.TabIndex = 8;
			this.txtTonSu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTonSu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTonSu_KeyDown);
			this.txtTonSu.Validating += new System.ComponentModel.CancelEventHandler(this.txtTonSu_Validating);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(11, 38);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 4;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(450, 141);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtTonSu);
			this.fsiPanel4.Controls.Add(this.lblTonSu);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 109);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(442, 28);
			this.fsiPanel4.TabIndex = 3;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtFuneKanaNm);
			this.fsiPanel3.Controls.Add(this.lblGyoshubnKana);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 74);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(442, 28);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtFuneNm);
			this.fsiPanel2.Controls.Add(this.lblGyoshubnNM);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 39);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(442, 28);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtFuneNmCd);
			this.fsiPanel1.Controls.Add(this.lblFuneNmCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(442, 28);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNCM1062
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(470, 296);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1062";
			this.ShowFButton = true;
			this.Text = "船名マスタの登録";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtFuneNmCd;
        private System.Windows.Forms.Label lblFuneNmCd;
        private System.Windows.Forms.Label lblGyoshubnNM;
        private common.controls.FsiTextBox txtFuneKanaNm;
        private System.Windows.Forms.Label lblGyoshubnKana;
        private common.controls.FsiTextBox txtFuneNm;
        private System.Windows.Forms.Label lblTonSu;
        private common.controls.FsiTextBox txtTonSu;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    };
}