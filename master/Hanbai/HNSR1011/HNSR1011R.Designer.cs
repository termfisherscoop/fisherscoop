﻿namespace jp.co.fsi.hn.hnsr1011
{
    /// <summary>
    /// HNSR1011Rの概要の説明です。
    /// </summary>
    partial class HNSR1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNSR1011R));
			this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblSeribi = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSeriDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPrintDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDenpyouNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblYamaNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSenshuCD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSenshuNM = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGyohouCD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGyoshuCD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMizuageSuryou = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTanka = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUriageGingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblShouhiZei = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGoukeiKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNakagaininCD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNakagaininNM = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGyoshuNM = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtDenpyouBangou = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYamaNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGyoshu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtShouhiZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGoukeiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNakagaininCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNakagaininNM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSenshuCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSenshuNM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGyohou = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGyoshuCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.ghMSenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.gfMSenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblShoukei = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMMizuageSuryou = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMUriageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMShouhiZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMGhoukeiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMKensu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblKensu = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.ghSSenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.gfSSenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSMizuageSuryou = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSUriageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSShouhiZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSGhoukeiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUchiwakeTen = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUchiwakeKinTen = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUchiwakeShoTen = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUchiwakeEight = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUchiwakeKinEight = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUchiwakeShoEight = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblSeribi)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeriDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDenpyouNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYamaNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSenshuCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSenshuNM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGyohouCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMizuageSuryou)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTanka)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUriageGingaku)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblShouhiZei)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGoukeiKingaku)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNakagaininCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNakagaininNM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDenpyouBangou)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYamaNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGyoshu)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShouhiZei)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGoukeiKingaku)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNakagaininCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNakagaininNM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSenshuCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSenshuNM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGyohou)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblShoukei)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMMizuageSuryou)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMUriageKingaku)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMShouhiZei)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMGhoukeiKingaku)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMKensu)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblKensu)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTanka)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSMizuageSuryou)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSUriageKingaku)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSShouhiZei)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSGhoukeiKingaku)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSTanka)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeTen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeKinTen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeShoTen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeEight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeKinEight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeShoEight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.CanGrow = false;
			this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSeribi,
            this.txtSeriDate,
            this.lblTitle,
            this.txtPrintDate,
            this.txtPage,
            this.lblPage,
            this.lblDenpyouNo,
            this.line1,
            this.lblYamaNo,
            this.lblSenshuCD,
            this.lblSenshuNM,
            this.lblGyohouCD,
            this.lblGyoshuCD,
            this.lblMizuageSuryou,
            this.lblTanka,
            this.lblUriageGingaku,
            this.lblShouhiZei,
            this.lblGoukeiKingaku,
            this.lblNakagaininCD,
            this.lblNakagaininNM,
            this.lblGyoshuNM,
            this.label3,
            this.label9});
			this.pageHeader.Height = 1.135416F;
			this.pageHeader.Name = "pageHeader";
			// 
			// lblSeribi
			// 
			this.lblSeribi.Height = 0.1688976F;
			this.lblSeribi.HyperLink = null;
			this.lblSeribi.Left = 0.07874016F;
			this.lblSeribi.MultiLine = false;
			this.lblSeribi.Name = "lblSeribi";
			this.lblSeribi.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
			this.lblSeribi.Text = "セリ日";
			this.lblSeribi.Top = 0.2362205F;
			this.lblSeribi.Width = 0.5102364F;
			// 
			// txtSeriDate
			// 
			this.txtSeriDate.DataField = "ITEM01";
			this.txtSeriDate.Height = 0.2F;
			this.txtSeriDate.Left = 0.5889765F;
			this.txtSeriDate.MultiLine = false;
			this.txtSeriDate.Name = "txtSeriDate";
			this.txtSeriDate.OutputFormat = resources.GetString("txtSeriDate.OutputFormat");
			this.txtSeriDate.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
			this.txtSeriDate.SummaryGroup = "ghMSenshu";
			this.txtSeriDate.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtSeriDate.Text = "YYYY/MM/DD";
			this.txtSeriDate.Top = 0.2206693F;
			this.txtSeriDate.Width = 0.7708662F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.2106299F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 5.675396F;
			this.lblTitle.MultiLine = false;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: middle";
			this.lblTitle.Text = "セリ日報";
			this.lblTitle.Top = 0.1248032F;
			this.lblTitle.Width = 1.208268F;
			// 
			// txtPrintDate
			// 
			this.txtPrintDate.DataField = "ITEM16";
			this.txtPrintDate.Height = 0.2F;
			this.txtPrintDate.Left = 10.89095F;
			this.txtPrintDate.MultiLine = false;
			this.txtPrintDate.Name = "txtPrintDate";
			this.txtPrintDate.OutputFormat = resources.GetString("txtPrintDate.OutputFormat");
			this.txtPrintDate.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle; d" +
    "do-char-set: 1";
			this.txtPrintDate.Text = "YYYY/MM/DD";
			this.txtPrintDate.Top = 0.2673229F;
			this.txtPrintDate.Width = 1.166142F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2F;
			this.txtPage.Left = 12.05709F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.OutputFormat = resources.GetString("txtPage.OutputFormat");
			this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle; d" +
    "do-char-set: 1";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = "ZZ9";
			this.txtPage.Top = 0.2673229F;
			this.txtPage.Width = 0.3334641F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1688976F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 12.39055F;
			this.lblPage.MultiLine = false;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
			this.lblPage.Text = "頁";
			this.lblPage.Top = 0.2828741F;
			this.lblPage.Width = 0.1562996F;
			// 
			// lblDenpyouNo
			// 
			this.lblDenpyouNo.Height = 0.1688976F;
			this.lblDenpyouNo.HyperLink = null;
			this.lblDenpyouNo.Left = 0F;
			this.lblDenpyouNo.Name = "lblDenpyouNo";
			this.lblDenpyouNo.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle";
			this.lblDenpyouNo.Text = "伝票番号";
			this.lblDenpyouNo.Top = 0.8830709F;
			this.lblDenpyouNo.Width = 0.8137796F;
			// 
			// line1
			// 
			this.line1.Height = 0.0001020432F;
			this.line1.Left = 0F;
			this.line1.LineWeight = 1F;
			this.line1.Name = "line1";
			this.line1.Top = 1.068898F;
			this.line1.Width = 13.16102F;
			this.line1.X1 = 0F;
			this.line1.X2 = 13.16102F;
			this.line1.Y1 = 1.069F;
			this.line1.Y2 = 1.068898F;
			// 
			// lblYamaNo
			// 
			this.lblYamaNo.Height = 0.1688976F;
			this.lblYamaNo.HyperLink = null;
			this.lblYamaNo.Left = 0.7614174F;
			this.lblYamaNo.Name = "lblYamaNo";
			this.lblYamaNo.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle";
			this.lblYamaNo.Text = "山NO";
			this.lblYamaNo.Top = 0.8830709F;
			this.lblYamaNo.Width = 0.4692914F;
			// 
			// lblSenshuCD
			// 
			this.lblSenshuCD.Height = 0.1688976F;
			this.lblSenshuCD.HyperLink = null;
			this.lblSenshuCD.Left = 1.230709F;
			this.lblSenshuCD.Name = "lblSenshuCD";
			this.lblSenshuCD.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle";
			this.lblSenshuCD.Text = "船主CD";
			this.lblSenshuCD.Top = 0.8830709F;
			this.lblSenshuCD.Width = 0.6145668F;
			// 
			// lblSenshuNM
			// 
			this.lblSenshuNM.Height = 0.1688976F;
			this.lblSenshuNM.HyperLink = null;
			this.lblSenshuNM.Left = 1.908268F;
			this.lblSenshuNM.Name = "lblSenshuNM";
			this.lblSenshuNM.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; vertical-align: middle";
			this.lblSenshuNM.Text = "船主名称";
			this.lblSenshuNM.Top = 0.8830709F;
			this.lblSenshuNM.Width = 0.8023622F;
			// 
			// lblGyohouCD
			// 
			this.lblGyohouCD.Height = 0.1688976F;
			this.lblGyohouCD.HyperLink = null;
			this.lblGyohouCD.Left = 3.01063F;
			this.lblGyohouCD.Name = "lblGyohouCD";
			this.lblGyohouCD.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: center; verti" +
    "cal-align: middle; ddo-char-set: 128";
			this.lblGyohouCD.Text = "漁法CD";
			this.lblGyohouCD.Top = 0.8830709F;
			this.lblGyohouCD.Width = 0.6771655F;
			// 
			// lblGyoshuCD
			// 
			this.lblGyoshuCD.Height = 0.1688976F;
			this.lblGyoshuCD.HyperLink = null;
			this.lblGyoshuCD.Left = 3.590551F;
			this.lblGyoshuCD.Name = "lblGyoshuCD";
			this.lblGyoshuCD.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle";
			this.lblGyoshuCD.Text = "漁種CD";
			this.lblGyoshuCD.Top = 0.8830709F;
			this.lblGyoshuCD.Width = 0.698032F;
			// 
			// lblMizuageSuryou
			// 
			this.lblMizuageSuryou.Height = 0.1688976F;
			this.lblMizuageSuryou.HyperLink = null;
			this.lblMizuageSuryou.Left = 5.519292F;
			this.lblMizuageSuryou.Name = "lblMizuageSuryou";
			this.lblMizuageSuryou.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle";
			this.lblMizuageSuryou.Text = "水揚数量Kg";
			this.lblMizuageSuryou.Top = 0.8830709F;
			this.lblMizuageSuryou.Width = 1.112205F;
			// 
			// lblTanka
			// 
			this.lblTanka.Height = 0.1688976F;
			this.lblTanka.HyperLink = null;
			this.lblTanka.Left = 6.501F;
			this.lblTanka.Name = "lblTanka";
			this.lblTanka.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle";
			this.lblTanka.Text = "単価";
			this.lblTanka.Top = 0.8830709F;
			this.lblTanka.Width = 0.5314965F;
			// 
			// lblUriageGingaku
			// 
			this.lblUriageGingaku.Height = 0.1688976F;
			this.lblUriageGingaku.HyperLink = null;
			this.lblUriageGingaku.Left = 7.195488F;
			this.lblUriageGingaku.Name = "lblUriageGingaku";
			this.lblUriageGingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle";
			this.lblUriageGingaku.Text = "売上金額";
			this.lblUriageGingaku.Top = 0.8830709F;
			this.lblUriageGingaku.Width = 0.802362F;
			// 
			// lblShouhiZei
			// 
			this.lblShouhiZei.Height = 0.1688976F;
			this.lblShouhiZei.HyperLink = null;
			this.lblShouhiZei.Left = 8.125F;
			this.lblShouhiZei.Name = "lblShouhiZei";
			this.lblShouhiZei.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle";
			this.lblShouhiZei.Text = "消費税";
			this.lblShouhiZei.Top = 0.8830709F;
			this.lblShouhiZei.Width = 0.802362F;
			// 
			// lblGoukeiKingaku
			// 
			this.lblGoukeiKingaku.Height = 0.1688976F;
			this.lblGoukeiKingaku.HyperLink = null;
			this.lblGoukeiKingaku.Left = 9.027F;
			this.lblGoukeiKingaku.Name = "lblGoukeiKingaku";
			this.lblGoukeiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle";
			this.lblGoukeiKingaku.Text = "合計金額";
			this.lblGoukeiKingaku.Top = 0.8830709F;
			this.lblGoukeiKingaku.Width = 0.8023624F;
			// 
			// lblNakagaininCD
			// 
			this.lblNakagaininCD.Height = 0.1688976F;
			this.lblNakagaininCD.HyperLink = null;
			this.lblNakagaininCD.Left = 11.02205F;
			this.lblNakagaininCD.Name = "lblNakagaininCD";
			this.lblNakagaininCD.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: center; verti" +
    "cal-align: middle";
			this.lblNakagaininCD.Text = "仲買人CD";
			this.lblNakagaininCD.Top = 0.8830709F;
			this.lblNakagaininCD.Width = 0.8228347F;
			// 
			// lblNakagaininNM
			// 
			this.lblNakagaininNM.Height = 0.1688976F;
			this.lblNakagaininNM.HyperLink = null;
			this.lblNakagaininNM.Left = 11.84488F;
			this.lblNakagaininNM.Name = "lblNakagaininNM";
			this.lblNakagaininNM.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; vertical-align: middle";
			this.lblNakagaininNM.Text = "仲買人名称";
			this.lblNakagaininNM.Top = 0.8830709F;
			this.lblNakagaininNM.Width = 0.9059055F;
			// 
			// lblGyoshuNM
			// 
			this.lblGyoshuNM.Height = 0.1688976F;
			this.lblGyoshuNM.HyperLink = null;
			this.lblGyoshuNM.Left = 4.563386F;
			this.lblGyoshuNM.Name = "lblGyoshuNM";
			this.lblGyoshuNM.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; vertical-align: middle";
			this.lblGyoshuNM.Text = "魚種名称";
			this.lblGyoshuNM.Top = 0.8830709F;
			this.lblGyoshuNM.Width = 0.9503944F;
			// 
			// label3
			// 
			this.label3.Height = 0.1688976F;
			this.label3.HyperLink = null;
			this.label3.Left = 9.830001F;
			this.label3.Name = "label3";
			this.label3.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: center; verti" +
    "cal-align: middle";
			this.label3.Text = "税率";
			this.label3.Top = 0.883F;
			this.label3.Width = 0.8023625F;
			// 
			// label9
			// 
			this.label9.Height = 0.2F;
			this.label9.HyperLink = null;
			this.label9.Left = 9.987F;
			this.label9.Name = "label9";
			this.label9.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
			this.label9.Text = "「*」は軽減税率であることを示します。";
			this.label9.Top = 0.553F;
			this.label9.Width = 3.042001F;
			// 
			// detail
			// 
			this.detail.CanGrow = false;
			this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDenpyouBangou,
            this.txtYamaNo,
            this.txtGyoshu,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.txtShouhiZei,
            this.txtGoukeiKingaku,
            this.txtNakagaininCD,
            this.txtNakagaininNM,
            this.textBox3,
            this.textBox4,
            this.textBox6,
            this.textBox10,
            this.textBox14,
            this.textBox15,
            this.textBox16});
			this.detail.Height = 0.219F;
			this.detail.KeepTogether = true;
			this.detail.Name = "detail";
			// 
			// txtDenpyouBangou
			// 
			this.txtDenpyouBangou.DataField = "ITEM02";
			this.txtDenpyouBangou.Height = 0.2F;
			this.txtDenpyouBangou.Left = 0.08307087F;
			this.txtDenpyouBangou.MultiLine = false;
			this.txtDenpyouBangou.Name = "txtDenpyouBangou";
			this.txtDenpyouBangou.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.txtDenpyouBangou.Text = "ZZZZ9";
			this.txtDenpyouBangou.Top = 0F;
			this.txtDenpyouBangou.Width = 0.8137796F;
			// 
			// txtYamaNo
			// 
			this.txtYamaNo.DataField = "ITEM03";
			this.txtYamaNo.Height = 0.2F;
			this.txtYamaNo.Left = 0.8137796F;
			this.txtYamaNo.MultiLine = false;
			this.txtYamaNo.Name = "txtYamaNo";
			this.txtYamaNo.OutputFormat = resources.GetString("txtYamaNo.OutputFormat");
			this.txtYamaNo.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.txtYamaNo.Text = "ZZ9";
			this.txtYamaNo.Top = 0F;
			this.txtYamaNo.Width = 0.4692914F;
			// 
			// txtGyoshu
			// 
			this.txtGyoshu.DataField = "ITEM08";
			this.txtGyoshu.Height = 0.2F;
			this.txtGyoshu.Left = 4.478741F;
			this.txtGyoshu.MultiLine = false;
			this.txtGyoshu.Name = "txtGyoshu";
			this.txtGyoshu.OutputFormat = resources.GetString("txtGyoshu.OutputFormat");
			this.txtGyoshu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
			this.txtGyoshu.Text = "ＮＮＮＮＮＮＮＮＮ";
			this.txtGyoshu.Top = 0F;
			this.txtGyoshu.Width = 1.285039F;
			// 
			// textBox7
			// 
			this.textBox7.DataField = "ITEM09";
			this.textBox7.Height = 0.2F;
			this.textBox7.Left = 5.72756F;
			this.textBox7.MultiLine = false;
			this.textBox7.Name = "textBox7";
			this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
			this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.textBox7.Text = "ZZZ,ZZ9.9";
			this.textBox7.Top = 0F;
			this.textBox7.Width = 0.7102361F;
			// 
			// textBox8
			// 
			this.textBox8.DataField = "ITEM10";
			this.textBox8.Height = 0.2F;
			this.textBox8.Left = 6.501F;
			this.textBox8.MultiLine = false;
			this.textBox8.Name = "textBox8";
			this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
			this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.textBox8.Text = "ZZZ,ZZ9";
			this.textBox8.Top = 0F;
			this.textBox8.Width = 0.5314965F;
			// 
			// textBox9
			// 
			this.textBox9.DataField = "ITEM12";
			this.textBox9.Height = 0.2F;
			this.textBox9.Left = 7.195488F;
			this.textBox9.MultiLine = false;
			this.textBox9.Name = "textBox9";
			this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
			this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.textBox9.Text = "ZZZ,ZZZ,ZZ9";
			this.textBox9.Top = 0F;
			this.textBox9.Width = 0.8023624F;
			// 
			// txtShouhiZei
			// 
			this.txtShouhiZei.DataField = "ITEM11";
			this.txtShouhiZei.Height = 0.2F;
			this.txtShouhiZei.Left = 8.124998F;
			this.txtShouhiZei.MultiLine = false;
			this.txtShouhiZei.Name = "txtShouhiZei";
			this.txtShouhiZei.OutputFormat = resources.GetString("txtShouhiZei.OutputFormat");
			this.txtShouhiZei.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtShouhiZei.Text = "ZZZ,ZZZ,ZZ9";
			this.txtShouhiZei.Top = 0F;
			this.txtShouhiZei.Visible = false;
			this.txtShouhiZei.Width = 0.8023625F;
			// 
			// txtGoukeiKingaku
			// 
			this.txtGoukeiKingaku.DataField = "ITEM12";
			this.txtGoukeiKingaku.Height = 0.2F;
			this.txtGoukeiKingaku.Left = 9.027F;
			this.txtGoukeiKingaku.MultiLine = false;
			this.txtGoukeiKingaku.Name = "txtGoukeiKingaku";
			this.txtGoukeiKingaku.OutputFormat = resources.GetString("txtGoukeiKingaku.OutputFormat");
			this.txtGoukeiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtGoukeiKingaku.Text = "ZZZ,ZZZ,ZZ9";
			this.txtGoukeiKingaku.Top = 0F;
			this.txtGoukeiKingaku.Width = 0.8023625F;
			// 
			// txtNakagaininCD
			// 
			this.txtNakagaininCD.DataField = "ITEM13";
			this.txtNakagaininCD.Height = 0.2F;
			this.txtNakagaininCD.Left = 11.02205F;
			this.txtNakagaininCD.MultiLine = false;
			this.txtNakagaininCD.Name = "txtNakagaininCD";
			this.txtNakagaininCD.OutputFormat = resources.GetString("txtNakagaininCD.OutputFormat");
			this.txtNakagaininCD.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.txtNakagaininCD.Text = "ZZZZ9";
			this.txtNakagaininCD.Top = 0F;
			this.txtNakagaininCD.Width = 0.8228347F;
			// 
			// txtNakagaininNM
			// 
			this.txtNakagaininNM.DataField = "ITEM14";
			this.txtNakagaininNM.Height = 0.2F;
			this.txtNakagaininNM.Left = 11.84488F;
			this.txtNakagaininNM.MultiLine = false;
			this.txtNakagaininNM.Name = "txtNakagaininNM";
			this.txtNakagaininNM.OutputFormat = resources.GetString("txtNakagaininNM.OutputFormat");
			this.txtNakagaininNM.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
			this.txtNakagaininNM.Text = "ＮＮＮＮＮＮＮＮＮ";
			this.txtNakagaininNM.Top = 0.0188977F;
			this.txtNakagaininNM.Width = 1.316142F;
			// 
			// textBox3
			// 
			this.textBox3.DataField = "ITEM18";
			this.textBox3.Height = 0.2F;
			this.textBox3.Left = 9.968F;
			this.textBox3.MultiLine = false;
			this.textBox3.Name = "textBox3";
			this.textBox3.OutputFormat = resources.GetString("textBox3.OutputFormat");
			this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.textBox3.Text = "NNNN";
			this.textBox3.Top = 0F;
			this.textBox3.Width = 0.5314965F;
			// 
			// textBox4
			// 
			this.textBox4.DataField = "ITEM19";
			this.textBox4.Height = 0.2F;
			this.textBox4.Left = 1.845F;
			this.textBox4.MultiLine = false;
			this.textBox4.Name = "textBox4";
			this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
			this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.textBox4.Text = "ZZZZ9";
			this.textBox4.Top = 0F;
			this.textBox4.Visible = false;
			this.textBox4.Width = 0.146F;
			// 
			// textBox6
			// 
			this.textBox6.DataField = "ITEM20";
			this.textBox6.Height = 0.2F;
			this.textBox6.Left = 2.014F;
			this.textBox6.MultiLine = false;
			this.textBox6.Name = "textBox6";
			this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
			this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.textBox6.Text = "ZZZZ9";
			this.textBox6.Top = 0F;
			this.textBox6.Visible = false;
			this.textBox6.Width = 0.146F;
			// 
			// textBox10
			// 
			this.textBox10.DataField = "ITEM21";
			this.textBox10.Height = 0.2F;
			this.textBox10.Left = 2.159F;
			this.textBox10.MultiLine = false;
			this.textBox10.Name = "textBox10";
			this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
			this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.textBox10.Text = "ZZZZ9";
			this.textBox10.Top = 0F;
			this.textBox10.Visible = false;
			this.textBox10.Width = 0.146F;
			// 
			// textBox14
			// 
			this.textBox14.DataField = "ITEM22";
			this.textBox14.Height = 0.2F;
			this.textBox14.Left = 2.305F;
			this.textBox14.MultiLine = false;
			this.textBox14.Name = "textBox14";
			this.textBox14.OutputFormat = resources.GetString("textBox14.OutputFormat");
			this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.textBox14.Text = "ZZZZ9";
			this.textBox14.Top = 0F;
			this.textBox14.Visible = false;
			this.textBox14.Width = 0.146F;
			// 
			// textBox15
			// 
			this.textBox15.DataField = "ITEM23";
			this.textBox15.Height = 0.2F;
			this.textBox15.Left = 2.565F;
			this.textBox15.MultiLine = false;
			this.textBox15.Name = "textBox15";
			this.textBox15.OutputFormat = resources.GetString("textBox15.OutputFormat");
			this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.textBox15.Text = "ZZZZ9";
			this.textBox15.Top = 0F;
			this.textBox15.Visible = false;
			this.textBox15.Width = 0.146F;
			// 
			// textBox16
			// 
			this.textBox16.DataField = "ITEM24";
			this.textBox16.Height = 0.2F;
			this.textBox16.Left = 2.865F;
			this.textBox16.MultiLine = false;
			this.textBox16.Name = "textBox16";
			this.textBox16.OutputFormat = resources.GetString("textBox16.OutputFormat");
			this.textBox16.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.textBox16.Text = "ZZZZ9";
			this.textBox16.Top = 0F;
			this.textBox16.Visible = false;
			this.textBox16.Width = 0.146F;
			// 
			// txtSenshuCD
			// 
			this.txtSenshuCD.DataField = "ITEM04";
			this.txtSenshuCD.Height = 0.2F;
			this.txtSenshuCD.Left = 1.303543F;
			this.txtSenshuCD.MultiLine = false;
			this.txtSenshuCD.Name = "txtSenshuCD";
			this.txtSenshuCD.OutputFormat = resources.GetString("txtSenshuCD.OutputFormat");
			this.txtSenshuCD.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtSenshuCD.Text = "ZZZZZ9";
			this.txtSenshuCD.Top = 0.008267717F;
			this.txtSenshuCD.Width = 0.4480315F;
			// 
			// txtSenshuNM
			// 
			this.txtSenshuNM.DataField = "ITEM05";
			this.txtSenshuNM.Height = 0.2F;
			this.txtSenshuNM.Left = 1.908268F;
			this.txtSenshuNM.MultiLine = false;
			this.txtSenshuNM.Name = "txtSenshuNM";
			this.txtSenshuNM.OutputFormat = resources.GetString("txtSenshuNM.OutputFormat");
			this.txtSenshuNM.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
			this.txtSenshuNM.Text = "ＮＮＮＮＮＮＮＮＮ";
			this.txtSenshuNM.Top = 0F;
			this.txtSenshuNM.Width = 1.300788F;
			// 
			// txtGyohou
			// 
			this.txtGyohou.DataField = "ITEM06";
			this.txtGyohou.Height = 0.2F;
			this.txtGyohou.Left = 3.01063F;
			this.txtGyohou.MultiLine = false;
			this.txtGyohou.Name = "txtGyohou";
			this.txtGyohou.OutputFormat = resources.GetString("txtGyohou.OutputFormat");
			this.txtGyohou.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.txtGyohou.Text = "ZZZ9";
			this.txtGyohou.Top = 0.008267717F;
			this.txtGyohou.Width = 0.6771655F;
			// 
			// txtGyoshuCD
			// 
			this.txtGyoshuCD.DataField = "ITEM07";
			this.txtGyoshuCD.Height = 0.2F;
			this.txtGyoshuCD.Left = 3.687796F;
			this.txtGyoshuCD.MultiLine = false;
			this.txtGyoshuCD.Name = "txtGyoshuCD";
			this.txtGyoshuCD.OutputFormat = resources.GetString("txtGyoshuCD.OutputFormat");
			this.txtGyoshuCD.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.txtGyoshuCD.Text = "ZZZZ9";
			this.txtGyoshuCD.Top = 0.008267717F;
			this.txtGyoshuCD.Width = 0.698032F;
			// 
			// pageFooter
			// 
			this.pageFooter.CanGrow = false;
			this.pageFooter.Height = 0F;
			this.pageFooter.Name = "pageFooter";
			// 
			// reportHeader1
			// 
			this.reportHeader1.CanGrow = false;
			this.reportHeader1.Height = 0F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// reportFooter1
			// 
			this.reportFooter1.CanGrow = false;
			this.reportFooter1.Height = 0F;
			this.reportFooter1.Name = "reportFooter1";
			// 
			// ghMSenshu
			// 
			this.ghMSenshu.CanGrow = false;
			this.ghMSenshu.DataField = "ITEM01";
			this.ghMSenshu.Height = 0F;
			this.ghMSenshu.Name = "ghMSenshu";
			this.ghMSenshu.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.ghMSenshu.UnderlayNext = true;
			// 
			// gfMSenshu
			// 
			this.gfMSenshu.CanGrow = false;
			this.gfMSenshu.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblShoukei,
            this.txtMMizuageSuryou,
            this.txtMUriageKingaku,
            this.txtMShouhiZei,
            this.txtMGhoukeiKingaku,
            this.txtMKensu,
            this.lblKensu,
            this.txtMTanka,
            this.label6,
            this.textBox1,
            this.textBox2,
            this.textBox5,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.label8,
            this.label11,
            this.label12});
			this.gfMSenshu.Height = 0.9548227F;
			this.gfMSenshu.Name = "gfMSenshu";
			this.gfMSenshu.Format += new System.EventHandler(this.gfMSenshu_Format);
			this.gfMSenshu.BeforePrint += new System.EventHandler(this.gfMSenshu_BeforePrint);
			// 
			// lblShoukei
			// 
			this.lblShoukei.Height = 0.2F;
			this.lblShoukei.HyperLink = null;
			this.lblShoukei.Left = 2.159449F;
			this.lblShoukei.Name = "lblShoukei";
			this.lblShoukei.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
			this.lblShoukei.Text = "＊＊＊ 合　計 ＊＊＊";
			this.lblShoukei.Top = 0F;
			this.lblShoukei.Width = 1.843898F;
			// 
			// txtMMizuageSuryou
			// 
			this.txtMMizuageSuryou.DataField = "ITEM09";
			this.txtMMizuageSuryou.Height = 0.2F;
			this.txtMMizuageSuryou.Left = 5.72756F;
			this.txtMMizuageSuryou.MultiLine = false;
			this.txtMMizuageSuryou.Name = "txtMMizuageSuryou";
			this.txtMMizuageSuryou.OutputFormat = resources.GetString("txtMMizuageSuryou.OutputFormat");
			this.txtMMizuageSuryou.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtMMizuageSuryou.SummaryGroup = "ghMSenshu";
			this.txtMMizuageSuryou.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtMMizuageSuryou.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtMMizuageSuryou.Text = "ZZZ,ZZ9.9";
			this.txtMMizuageSuryou.Top = 0.04330709F;
			this.txtMMizuageSuryou.Width = 0.7102361F;
			// 
			// txtMUriageKingaku
			// 
			this.txtMUriageKingaku.DataField = "ITEM12";
			this.txtMUriageKingaku.Height = 0.2F;
			this.txtMUriageKingaku.Left = 7.195488F;
			this.txtMUriageKingaku.MultiLine = false;
			this.txtMUriageKingaku.Name = "txtMUriageKingaku";
			this.txtMUriageKingaku.OutputFormat = resources.GetString("txtMUriageKingaku.OutputFormat");
			this.txtMUriageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtMUriageKingaku.SummaryGroup = "ghMSenshu";
			this.txtMUriageKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtMUriageKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtMUriageKingaku.Text = "ZZZ,ZZZ,ZZ9";
			this.txtMUriageKingaku.Top = 0.04330709F;
			this.txtMUriageKingaku.Width = 0.8023625F;
			// 
			// txtMShouhiZei
			// 
			this.txtMShouhiZei.DataField = "ITEM11";
			this.txtMShouhiZei.Height = 0.2F;
			this.txtMShouhiZei.Left = 8.125439F;
			this.txtMShouhiZei.MultiLine = false;
			this.txtMShouhiZei.Name = "txtMShouhiZei";
			this.txtMShouhiZei.OutputFormat = resources.GetString("txtMShouhiZei.OutputFormat");
			this.txtMShouhiZei.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtMShouhiZei.SummaryGroup = "ghMSenshu";
			this.txtMShouhiZei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtMShouhiZei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtMShouhiZei.Text = "ZZZ,ZZZ,ZZ9";
			this.txtMShouhiZei.Top = 0.04330709F;
			this.txtMShouhiZei.Width = 0.8023625F;
			// 
			// txtMGhoukeiKingaku
			// 
			this.txtMGhoukeiKingaku.DataField = "ITEM12";
			this.txtMGhoukeiKingaku.Height = 0.2F;
			this.txtMGhoukeiKingaku.Left = 9.027441F;
			this.txtMGhoukeiKingaku.MultiLine = false;
			this.txtMGhoukeiKingaku.Name = "txtMGhoukeiKingaku";
			this.txtMGhoukeiKingaku.OutputFormat = resources.GetString("txtMGhoukeiKingaku.OutputFormat");
			this.txtMGhoukeiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtMGhoukeiKingaku.SummaryGroup = "ghMSenshu";
			this.txtMGhoukeiKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtMGhoukeiKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtMGhoukeiKingaku.Text = "ZZZ,ZZZ,ZZ9";
			this.txtMGhoukeiKingaku.Top = 0.04330709F;
			this.txtMGhoukeiKingaku.Width = 0.8023625F;
			// 
			// txtMKensu
			// 
			this.txtMKensu.Height = 0.2F;
			this.txtMKensu.Left = 4.789371F;
			this.txtMKensu.MultiLine = false;
			this.txtMKensu.Name = "txtMKensu";
			this.txtMKensu.OutputFormat = resources.GetString("txtMKensu.OutputFormat");
			this.txtMKensu.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtMKensu.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
			this.txtMKensu.SummaryGroup = "ghMSenshu";
			this.txtMKensu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtMKensu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtMKensu.Text = "ZZZ,ZZ9";
			this.txtMKensu.Top = 0.04330709F;
			this.txtMKensu.Width = 0.553937F;
			// 
			// lblKensu
			// 
			this.lblKensu.Height = 0.1688976F;
			this.lblKensu.HyperLink = null;
			this.lblKensu.Left = 5.362992F;
			this.lblKensu.MultiLine = false;
			this.lblKensu.Name = "lblKensu";
			this.lblKensu.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
			this.lblKensu.Text = "件";
			this.lblKensu.Top = 0.04330709F;
			this.lblKensu.Width = 0.1562996F;
			// 
			// txtMTanka
			// 
			this.txtMTanka.DataField = "ITEM10";
			this.txtMTanka.Height = 0.2F;
			this.txtMTanka.Left = 6.501F;
			this.txtMTanka.MultiLine = false;
			this.txtMTanka.Name = "txtMTanka";
			this.txtMTanka.OutputFormat = resources.GetString("txtMTanka.OutputFormat");
			this.txtMTanka.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtMTanka.Text = "ZZZ,ZZ9";
			this.txtMTanka.Top = 0.04330709F;
			this.txtMTanka.Width = 0.5314965F;
			// 
			// label6
			// 
			this.label6.Height = 0.2F;
			this.label6.HyperLink = null;
			this.label6.Left = 10.346F;
			this.label6.Name = "label6";
			this.label6.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: center; text-justify: auto; v" +
    "ertical-align: middle";
			this.label6.Text = "＜＜　　内訳消費税　　＞＞";
			this.label6.Top = 0.043F;
			this.label6.Width = 2.425F;
			// 
			// textBox1
			// 
			this.textBox1.DataField = "ITEM19";
			this.textBox1.Height = 0.2F;
			this.textBox1.Left = 10.486F;
			this.textBox1.MultiLine = false;
			this.textBox1.Name = "textBox1";
			this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
			this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.textBox1.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Max;
			this.textBox1.SummaryGroup = "ghMSenshu";
			this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox1.Text = "NNNN";
			this.textBox1.Top = 0.495F;
			this.textBox1.Width = 0.3440003F;
			// 
			// textBox2
			// 
			this.textBox2.DataField = "ITEM20";
			this.textBox2.Height = 0.2F;
			this.textBox2.Left = 11.043F;
			this.textBox2.MultiLine = false;
			this.textBox2.Name = "textBox2";
			this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
			this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.textBox2.SummaryGroup = "ghMSenshu";
			this.textBox2.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox2.Text = "ZZZ,ZZZ,ZZ9";
			this.textBox2.Top = 0.495F;
			this.textBox2.Width = 0.8023625F;
			// 
			// textBox5
			// 
			this.textBox5.DataField = "ITEM21";
			this.textBox5.Height = 0.2F;
			this.textBox5.Left = 11.969F;
			this.textBox5.MultiLine = false;
			this.textBox5.Name = "textBox5";
			this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
			this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.textBox5.SummaryGroup = "ghMSenshu";
			this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox5.Text = "ZZZ,ZZZ,ZZ9";
			this.textBox5.Top = 0.495F;
			this.textBox5.Width = 0.8023625F;
			// 
			// textBox11
			// 
			this.textBox11.DataField = "ITEM22";
			this.textBox11.Height = 0.2F;
			this.textBox11.Left = 10.486F;
			this.textBox11.MultiLine = false;
			this.textBox11.Name = "textBox11";
			this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
			this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.textBox11.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Max;
			this.textBox11.SummaryGroup = "ghMSenshu";
			this.textBox11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox11.Text = "NNNN";
			this.textBox11.Top = 0.715F;
			this.textBox11.Width = 0.3440003F;
			// 
			// textBox12
			// 
			this.textBox12.DataField = "ITEM23";
			this.textBox12.Height = 0.2F;
			this.textBox12.Left = 11.043F;
			this.textBox12.MultiLine = false;
			this.textBox12.Name = "textBox12";
			this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
			this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.textBox12.SummaryGroup = "ghMSenshu";
			this.textBox12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox12.Text = "ZZZ,ZZZ,ZZ9";
			this.textBox12.Top = 0.715F;
			this.textBox12.Width = 0.8023625F;
			// 
			// textBox13
			// 
			this.textBox13.DataField = "ITEM24";
			this.textBox13.Height = 0.2F;
			this.textBox13.Left = 11.969F;
			this.textBox13.MultiLine = false;
			this.textBox13.Name = "textBox13";
			this.textBox13.OutputFormat = resources.GetString("textBox13.OutputFormat");
			this.textBox13.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.textBox13.SummaryGroup = "ghMSenshu";
			this.textBox13.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox13.Text = "ZZZ,ZZZ,ZZ9";
			this.textBox13.Top = 0.715F;
			this.textBox13.Width = 0.8023625F;
			// 
			// label8
			// 
			this.label8.Height = 0.2F;
			this.label8.HyperLink = null;
			this.label8.Left = 10.837F;
			this.label8.Name = "label8";
			this.label8.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
			this.label8.Text = "%";
			this.label8.Top = 0.495F;
			this.label8.Width = 0.1549997F;
			// 
			// label11
			// 
			this.label11.Height = 0.2F;
			this.label11.HyperLink = null;
			this.label11.Left = 10.837F;
			this.label11.Name = "label11";
			this.label11.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
			this.label11.Text = "%";
			this.label11.Top = 0.715F;
			this.label11.Width = 0.1549997F;
			// 
			// label12
			// 
			this.label12.Height = 0.2F;
			this.label12.HyperLink = null;
			this.label12.Left = 10.486F;
			this.label12.Name = "label12";
			this.label12.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: left; text-justify: auto; ver" +
    "tical-align: middle";
			this.label12.Text = "税率　　　  金額　　  消費税";
			this.label12.Top = 0.243F;
			this.label12.Width = 2.488001F;
			// 
			// line2
			// 
			this.line2.Height = 0F;
			this.line2.Left = 0F;
			this.line2.LineWeight = 1F;
			this.line2.Name = "line2";
			this.line2.Top = 0.8750001F;
			this.line2.Width = 13.16102F;
			this.line2.X1 = 0F;
			this.line2.X2 = 13.16102F;
			this.line2.Y1 = 0.8750001F;
			this.line2.Y2 = 0.8750001F;
			// 
			// ghSSenshu
			// 
			this.ghSSenshu.CanGrow = false;
			this.ghSSenshu.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSenshuCD,
            this.txtSenshuNM,
            this.txtGyohou,
            this.txtGyoshuCD});
			this.ghSSenshu.DataField = "ITEM15";
			this.ghSSenshu.Height = 0.2082677F;
			this.ghSSenshu.Name = "ghSSenshu";
			this.ghSSenshu.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.ghSSenshu.UnderlayNext = true;
			// 
			// gfSSenshu
			// 
			this.gfSSenshu.CanGrow = false;
			this.gfSSenshu.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.txtSMizuageSuryou,
            this.txtSUriageKingaku,
            this.txtSShouhiZei,
            this.txtSGhoukeiKingaku,
            this.line2,
            this.txtSTanka,
            this.label2,
            this.txtUchiwakeTen,
            this.txtUchiwakeKinTen,
            this.txtUchiwakeShoTen,
            this.txtUchiwakeEight,
            this.txtUchiwakeKinEight,
            this.txtUchiwakeShoEight,
            this.label5,
            this.label4,
            this.label7});
			this.gfSSenshu.Height = 0.9964274F;
			this.gfSSenshu.Name = "gfSSenshu";
			this.gfSSenshu.Format += new System.EventHandler(this.gfSSenshu_Format);
			this.gfSSenshu.BeforePrint += new System.EventHandler(this.gfSSenshu_BeforePrint);
			// 
			// label1
			// 
			this.label1.Height = 0.2F;
			this.label1.HyperLink = null;
			this.label1.Left = 2.159889F;
			this.label1.Name = "label1";
			this.label1.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
			this.label1.Text = "＊＊＊ 小　計 ＊＊＊";
			this.label1.Top = 0.0509055F;
			this.label1.Width = 1.843898F;
			// 
			// txtSMizuageSuryou
			// 
			this.txtSMizuageSuryou.DataField = "ITEM09";
			this.txtSMizuageSuryou.Height = 0.2F;
			this.txtSMizuageSuryou.Left = 5.728F;
			this.txtSMizuageSuryou.MultiLine = false;
			this.txtSMizuageSuryou.Name = "txtSMizuageSuryou";
			this.txtSMizuageSuryou.OutputFormat = resources.GetString("txtSMizuageSuryou.OutputFormat");
			this.txtSMizuageSuryou.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtSMizuageSuryou.SummaryGroup = "ghSSenshu";
			this.txtSMizuageSuryou.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtSMizuageSuryou.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtSMizuageSuryou.Text = "ZZZ,ZZ9.9";
			this.txtSMizuageSuryou.Top = 0.09500001F;
			this.txtSMizuageSuryou.Width = 0.7102361F;
			// 
			// txtSUriageKingaku
			// 
			this.txtSUriageKingaku.DataField = "ITEM12";
			this.txtSUriageKingaku.Height = 0.2F;
			this.txtSUriageKingaku.Left = 7.195929F;
			this.txtSUriageKingaku.MultiLine = false;
			this.txtSUriageKingaku.Name = "txtSUriageKingaku";
			this.txtSUriageKingaku.OutputFormat = resources.GetString("txtSUriageKingaku.OutputFormat");
			this.txtSUriageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtSUriageKingaku.SummaryGroup = "ghSSenshu";
			this.txtSUriageKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtSUriageKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtSUriageKingaku.Text = "ZZZ,ZZZ,ZZ9";
			this.txtSUriageKingaku.Top = 0.09500001F;
			this.txtSUriageKingaku.Width = 0.8023625F;
			// 
			// txtSShouhiZei
			// 
			this.txtSShouhiZei.DataField = "ITEM11";
			this.txtSShouhiZei.Height = 0.2F;
			this.txtSShouhiZei.Left = 8.125439F;
			this.txtSShouhiZei.MultiLine = false;
			this.txtSShouhiZei.Name = "txtSShouhiZei";
			this.txtSShouhiZei.OutputFormat = resources.GetString("txtSShouhiZei.OutputFormat");
			this.txtSShouhiZei.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtSShouhiZei.SummaryGroup = "ghSSenshu";
			this.txtSShouhiZei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtSShouhiZei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtSShouhiZei.Text = "ZZZ,ZZZ,ZZ9";
			this.txtSShouhiZei.Top = 0.09500001F;
			this.txtSShouhiZei.Width = 0.8023625F;
			// 
			// txtSGhoukeiKingaku
			// 
			this.txtSGhoukeiKingaku.DataField = "ITEM12";
			this.txtSGhoukeiKingaku.Height = 0.2F;
			this.txtSGhoukeiKingaku.Left = 9.027441F;
			this.txtSGhoukeiKingaku.MultiLine = false;
			this.txtSGhoukeiKingaku.Name = "txtSGhoukeiKingaku";
			this.txtSGhoukeiKingaku.OutputFormat = resources.GetString("txtSGhoukeiKingaku.OutputFormat");
			this.txtSGhoukeiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtSGhoukeiKingaku.SummaryGroup = "ghSSenshu";
			this.txtSGhoukeiKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtSGhoukeiKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtSGhoukeiKingaku.Text = "ZZZ,ZZZ,ZZ9";
			this.txtSGhoukeiKingaku.Top = 0.09500001F;
			this.txtSGhoukeiKingaku.Width = 0.8023625F;
			// 
			// txtSTanka
			// 
			this.txtSTanka.DataField = "ITEM10";
			this.txtSTanka.Height = 0.2F;
			this.txtSTanka.Left = 6.501441F;
			this.txtSTanka.MultiLine = false;
			this.txtSTanka.Name = "txtSTanka";
			this.txtSTanka.OutputFormat = resources.GetString("txtSTanka.OutputFormat");
			this.txtSTanka.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtSTanka.Text = "ZZZ,ZZ9";
			this.txtSTanka.Top = 0.09500001F;
			this.txtSTanka.Width = 0.5314965F;
			// 
			// label2
			// 
			this.label2.Height = 0.2F;
			this.label2.HyperLink = null;
			this.label2.Left = 10.325F;
			this.label2.Name = "label2";
			this.label2.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: center; text-justify: auto; v" +
    "ertical-align: middle";
			this.label2.Text = "＜＜　　内訳消費税　　＞＞";
			this.label2.Top = 0.035F;
			this.label2.Width = 2.425F;
			// 
			// txtUchiwakeTen
			// 
			this.txtUchiwakeTen.DataField = "ITEM19";
			this.txtUchiwakeTen.Height = 0.2F;
			this.txtUchiwakeTen.Left = 10.486F;
			this.txtUchiwakeTen.MultiLine = false;
			this.txtUchiwakeTen.Name = "txtUchiwakeTen";
			this.txtUchiwakeTen.OutputFormat = resources.GetString("txtUchiwakeTen.OutputFormat");
			this.txtUchiwakeTen.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtUchiwakeTen.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Max;
			this.txtUchiwakeTen.SummaryGroup = "ghSSenshu";
			this.txtUchiwakeTen.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtUchiwakeTen.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtUchiwakeTen.Text = "NNNN";
			this.txtUchiwakeTen.Top = 0.445F;
			this.txtUchiwakeTen.Width = 0.3440003F;
			// 
			// txtUchiwakeKinTen
			// 
			this.txtUchiwakeKinTen.DataField = "ITEM20";
			this.txtUchiwakeKinTen.Height = 0.2F;
			this.txtUchiwakeKinTen.Left = 11.022F;
			this.txtUchiwakeKinTen.MultiLine = false;
			this.txtUchiwakeKinTen.Name = "txtUchiwakeKinTen";
			this.txtUchiwakeKinTen.OutputFormat = resources.GetString("txtUchiwakeKinTen.OutputFormat");
			this.txtUchiwakeKinTen.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtUchiwakeKinTen.SummaryGroup = "ghSSenshu";
			this.txtUchiwakeKinTen.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtUchiwakeKinTen.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtUchiwakeKinTen.Text = "ZZZ,ZZZ,ZZ9";
			this.txtUchiwakeKinTen.Top = 0.445F;
			this.txtUchiwakeKinTen.Width = 0.8023625F;
			// 
			// txtUchiwakeShoTen
			// 
			this.txtUchiwakeShoTen.DataField = "ITEM21";
			this.txtUchiwakeShoTen.Height = 0.2F;
			this.txtUchiwakeShoTen.Left = 11.948F;
			this.txtUchiwakeShoTen.MultiLine = false;
			this.txtUchiwakeShoTen.Name = "txtUchiwakeShoTen";
			this.txtUchiwakeShoTen.OutputFormat = resources.GetString("txtUchiwakeShoTen.OutputFormat");
			this.txtUchiwakeShoTen.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtUchiwakeShoTen.SummaryGroup = "ghSSenshu";
			this.txtUchiwakeShoTen.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtUchiwakeShoTen.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtUchiwakeShoTen.Text = "ZZZ,ZZZ,ZZ9";
			this.txtUchiwakeShoTen.Top = 0.445F;
			this.txtUchiwakeShoTen.Width = 0.8023625F;
			// 
			// txtUchiwakeEight
			// 
			this.txtUchiwakeEight.DataField = "ITEM22";
			this.txtUchiwakeEight.Height = 0.2F;
			this.txtUchiwakeEight.Left = 10.486F;
			this.txtUchiwakeEight.MultiLine = false;
			this.txtUchiwakeEight.Name = "txtUchiwakeEight";
			this.txtUchiwakeEight.OutputFormat = resources.GetString("txtUchiwakeEight.OutputFormat");
			this.txtUchiwakeEight.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtUchiwakeEight.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Max;
			this.txtUchiwakeEight.SummaryGroup = "ghSSenshu";
			this.txtUchiwakeEight.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtUchiwakeEight.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtUchiwakeEight.Text = "NNNN";
			this.txtUchiwakeEight.Top = 0.645F;
			this.txtUchiwakeEight.Width = 0.3440003F;
			// 
			// txtUchiwakeKinEight
			// 
			this.txtUchiwakeKinEight.DataField = "ITEM23";
			this.txtUchiwakeKinEight.Height = 0.2F;
			this.txtUchiwakeKinEight.Left = 11.022F;
			this.txtUchiwakeKinEight.MultiLine = false;
			this.txtUchiwakeKinEight.Name = "txtUchiwakeKinEight";
			this.txtUchiwakeKinEight.OutputFormat = resources.GetString("txtUchiwakeKinEight.OutputFormat");
			this.txtUchiwakeKinEight.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtUchiwakeKinEight.SummaryGroup = "ghSSenshu";
			this.txtUchiwakeKinEight.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtUchiwakeKinEight.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtUchiwakeKinEight.Text = "ZZZ,ZZZ,ZZ9";
			this.txtUchiwakeKinEight.Top = 0.645F;
			this.txtUchiwakeKinEight.Width = 0.8023625F;
			// 
			// txtUchiwakeShoEight
			// 
			this.txtUchiwakeShoEight.DataField = "ITEM24";
			this.txtUchiwakeShoEight.Height = 0.2F;
			this.txtUchiwakeShoEight.Left = 11.948F;
			this.txtUchiwakeShoEight.MultiLine = false;
			this.txtUchiwakeShoEight.Name = "txtUchiwakeShoEight";
			this.txtUchiwakeShoEight.OutputFormat = resources.GetString("txtUchiwakeShoEight.OutputFormat");
			this.txtUchiwakeShoEight.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
			this.txtUchiwakeShoEight.SummaryGroup = "ghSSenshu";
			this.txtUchiwakeShoEight.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtUchiwakeShoEight.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtUchiwakeShoEight.Text = "ZZZ,ZZZ,ZZ9";
			this.txtUchiwakeShoEight.Top = 0.645F;
			this.txtUchiwakeShoEight.Width = 0.8023625F;
			// 
			// label5
			// 
			this.label5.Height = 0.2F;
			this.label5.HyperLink = null;
			this.label5.Left = 10.486F;
			this.label5.Name = "label5";
			this.label5.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: left; text-justify: auto; ver" +
    "tical-align: middle";
			this.label5.Text = "税率　　　  金額　　  消費税";
			this.label5.Top = 0.245F;
			this.label5.Width = 2.488001F;
			// 
			// label4
			// 
			this.label4.Height = 0.2F;
			this.label4.HyperLink = null;
			this.label4.Left = 10.837F;
			this.label4.Name = "label4";
			this.label4.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
			this.label4.Text = "%";
			this.label4.Top = 0.445F;
			this.label4.Width = 0.1549997F;
			// 
			// label7
			// 
			this.label7.Height = 0.2F;
			this.label7.HyperLink = null;
			this.label7.Left = 10.83F;
			this.label7.Name = "label7";
			this.label7.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
			this.label7.Text = "%";
			this.label7.Top = 0.645F;
			this.label7.Width = 0.1549997F;
			// 
			// HNSR1011R
			// 
			this.MasterReport = false;
			this.PageSettings.DefaultPaperSize = false;
			this.PageSettings.Margins.Bottom = 0.3937007F;
			this.PageSettings.Margins.Left = 0.7874016F;
			this.PageSettings.Margins.Right = 0.1968504F;
			this.PageSettings.Margins.Top = 0.5905512F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 13.89764F;
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
			this.PageSettings.PaperWidth = 9.84252F;
			this.PrintWidth = 13.3212F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.ghMSenshu);
			this.Sections.Add(this.ghSSenshu);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.gfSSenshu);
			this.Sections.Add(this.gfMSenshu);
			this.Sections.Add(this.pageFooter);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblSeribi)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeriDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDenpyouNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYamaNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSenshuCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSenshuNM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGyohouCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMizuageSuryou)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTanka)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUriageGingaku)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblShouhiZei)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGoukeiKingaku)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNakagaininCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNakagaininNM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDenpyouBangou)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYamaNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGyoshu)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShouhiZei)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGoukeiKingaku)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNakagaininCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNakagaininNM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSenshuCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSenshuNM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGyohou)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblShoukei)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMMizuageSuryou)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMUriageKingaku)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMShouhiZei)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMGhoukeiKingaku)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMKensu)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblKensu)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTanka)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSMizuageSuryou)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSUriageKingaku)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSShouhiZei)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSGhoukeiKingaku)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSTanka)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeTen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeKinTen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeShoTen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeEight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeKinEight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUchiwakeShoEight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblSeribi;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeriDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrintDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDenpyouNo;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYamaNo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSenshuCD;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSenshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyohouCD;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuCD;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMizuageSuryou;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblUriageGingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShouhiZei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGoukeiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNakagaininCD;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNakagaininNM;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDenpyouBangou;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghMSenshu;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfMSenshu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShoukei;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYamaNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSenshuCD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSenshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohou;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuCD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShouhiZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGoukeiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNakagaininCD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNakagaininNM;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMMizuageSuryou;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMUriageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMShouhiZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMGhoukeiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghSSenshu;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfSSenshu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMKensu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKensu;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSMizuageSuryou;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSUriageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSShouhiZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGhoukeiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSTanka;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUchiwakeTen;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUchiwakeKinTen;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUchiwakeShoTen;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUchiwakeEight;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUchiwakeKinEight;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUchiwakeShoEight;
		private GrapeCity.ActiveReports.SectionReportModel.Label label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
		private GrapeCity.ActiveReports.SectionReportModel.Label label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label label12;
	}
}
