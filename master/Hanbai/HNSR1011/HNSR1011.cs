﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hnsr1011
{
    /// <summary>
    /// セリ日報(HNSR1011)
    /// </summary>
    public partial class HNSR1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 精算区分(地区内ｾﾘ)
        /// </summary>
        private const string CHIKUNAI = "3";

        /// <summary>
        /// 精算区分(浜売り)
        /// </summary>
        private const string HAMA_URI = "2";

        /// <summary>
        /// 精算区分(地区外ｾﾘ)
        /// </summary>
        private const string CHIKUGAI = "1";

        /// <summary>
        /// 印刷ワークテーブル使用列数
        /// </summary>
        public const int prtCols = 24;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            #if DEBUG
                this.txtMizuageShishoCd.Text = "1"; //Uinfo.shishoCd;
            #else
                this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
            #endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲前
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = jpDate[4];
            // 日付範囲後
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];
            // 精算区分地区内ｾﾘにチェック
            this.txtSeisanKbn.Text = "0";
            IsValidSeisanKbn();

            // フォーカス設定
            this.txtDateYearFr.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtSeisanKbn":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // アセンブリのロード
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtSeisanKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtSeisanKbn.Text = outData[0];
                                this.lblSeisanKbnNm.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false ,true);
            }
        }
        //public override void PressF8()
        //{
        //    // 全項目を再度入力値チェック
        //    if (!ValidateAll())
        //    {
        //        // エラーありの場合ここで処理終了
        //        return;
        //    }

        //    if (Msg.ConfNmYesNo("テキスト出力", "実行しますか？") == DialogResult.Yes)
        //    {
        //        // 印刷処理
        //        DoPrint(false, false, false, true);
        //    }
        //}

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNSR1011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
            }
        }

        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;

                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 日(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 日(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckJpTo();
                SetJpTo();

                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 日(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtDateDayTo.Focus();
                }
            }
        }

        /// <summary>
        /// 精算区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", this.txtMizuageShishoCd.Text, this.txtSeisanKbn.Text);
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text) || this.txtSeisanKbn.Text == "0")
            {
                this.txtSeisanKbn.Text = "0";
                this.lblSeisanKbnNm.Text = "全て";
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        private bool IsValidSeisanKbn()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtSeisanKbn.Text, this.txtSeisanKbn.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text) || this.txtSeisanKbn.Text == "0")
            {
                this.txtSeisanKbn.Text = "0";
                this.lblSeisanKbnNm.Text = "全て";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                Msg.Notice("精算区分は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", this.txtMizuageShishoCd.Text, this.txtSeisanKbn.Text);
                if (ValChk.IsEmpty(this.lblSeisanKbnNm.Text))
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 精算区分の入力チェック
            if (!IsValidSeisanKbn())
            {
                this.txtSeisanKbn.Focus();
                this.txtSeisanKbn.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_CM_SHISHO"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 2);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            bool dataFlag;

            try
            {
#if DEBUG
                //ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNSR1011R rpt = new HNSR1011R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;
                    //if (isCsv)
                    //{
                    //    GrapeCity.ActiveReports.Export.Xml.Section.TextExport txt = new GrapeCity.ActiveReports.Export.Xml.Section.TextExport();
                    //    txt.TextDelimiter = ",";
                    //    txt.Encoding = System.Text.Encoding.Default;
                    //    rpt.Run();
                    //    string saveFileName = Util.GetSavePath(rpt.Document.Name, 3);
                    //    if (!ValChk.IsEmpty(saveFileName))
                    //    {
                    //        txt.Export(rpt.Document, saveFileName);
                    //        Msg.InfoNm("テキスト出力", "保存しました。");
                    //        Util.OpenFolder(saveFileName);
                    //    }
                    //}
                    //else if (isExcel)
                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    //if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                // デバッグ時はデータの確認をしたいので、データが残るようにする。
                this.Dba.Commit();
#else
                // 印刷後なので、帳票出力用ワークテーブルをロールバックで元に戻す
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            #region "精算区分設定"
            // 精算区分設定
            string seisanKubunSettei = this.txtSeisanKbn.Text;
            #endregion

            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // han.VI_仕切明細(VI_HN_SHIKIRI_MEISAI)の日付範囲から発生しているデータを取得
            Sql.AppendLine("SELECT");
            Sql.AppendLine(" SERIBI,");
            Sql.AppendLine(" DENPYO_BANGO,");
            Sql.AppendLine(" YAMA_NO,");
            Sql.AppendLine(" SENSHU_CD,");
            Sql.AppendLine(" SENSHU_NM,");
            Sql.AppendLine(" GYOHO_CD,");
            Sql.AppendLine(" GYOSHU_CD,");
            Sql.AppendLine(" GYOSHU_NM,");
            Sql.AppendLine(" SURYO,");
            Sql.AppendLine(" TANKA,");
            Sql.AppendLine(" GOKEI_SHOHIZEI,");
            //Sql.Append(" CASE WHEN SEISAN_KUBUN = 2 OR SHOHIZEI_NYURYOKU_HOHO = 3 THEN 0 ELSE GOKEI_SHOHIZEI END AS GOKEI_SHOHIZEI,");
            Sql.AppendLine(" KINGAKU,");
            Sql.AppendLine(" NAKAGAININ_CD,");
            Sql.AppendLine(" NAKAGAININ_NM,");
            Sql.AppendLine(" SHOHIZEI_FURIWAKE,");
            Sql.AppendLine(" CASE WHEN ZEI_KUBUN = 50 THEN '*'+ CAST(ZEI_RITSU AS VARCHAR) ELSE CAST(ZEI_RITSU AS VARCHAR) END AS KEIGENFLG,");
            Sql.AppendLine(" UCHI_ZEIRITSU,");
            Sql.AppendLine(" UCHI_KIN,");
            Sql.AppendLine(" UCHI_SHOHIZEI,");
            Sql.AppendLine(" UCHI_KEIZEIRITSU,");
            Sql.AppendLine(" UCHI_KEIKIN,");
            Sql.AppendLine(" UCHI_KEISHOHIZEI ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" VI_HN_SHIKIRI_MEISAI ");
            Sql.AppendLine("WHERE");
            Sql.AppendLine(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.AppendLine(" SERIBI BETWEEN @DATE_FR AND @DATE_TO ");
            Sql.AppendLine(" AND KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
            {
                Sql.AppendLine(" AND SHISHO_CD = @SHISHO_CD");
            }
            if (seisanKubunSettei != "0")
            {
                Sql.AppendLine(" AND SEISAN_KUBUN = @SEISAN_KUBUN");
            }
            Sql.AppendLine(" ORDER BY");
            Sql.AppendLine(" KAISHA_CD ASC,");
            Sql.AppendLine(" SHISHO_CD ASC,");
            Sql.AppendLine(" SERIBI ASC,");
            Sql.AppendLine(" SENSHU_CD ASC,");
            Sql.AppendLine(" DENPYO_BANGO ASC,");
            Sql.AppendLine(" YAMA_NO ASC");

            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKubunSettei);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }

            #region メインループ準備処理
            int dbSORT = 1;
            int i = 0; // ループ用カウント変数
            string tmpFunanushiCd; // 船主CD変数
            //int tmpFunanushiCdNow = 0; // 比較用船主CD変数
            //int tmpFunanushiCdBefore = -1; // 比較用船主CD変数
            string tmpFunanushiNm; // 船主名称変数
            string tmpGyohoCd; // 漁法CD変数
            string tmpShohizei; // 消費税変数
            string printDate = this.convAdToJpDateString(DateTime.Today); // 印刷日付
            decimal mizuageSuryo = 0; // 数量の端数処理用
            #endregion

            while (dtMainLoop.Rows.Count > i)
            {
                dpc = new DbParamCollection();
                Sql = new StringBuilder();

                #region データの準備
                tmpFunanushiCd = Util.ToString(dtMainLoop.Rows[i]["SENSHU_CD"]);
                tmpFunanushiNm = Util.ToString(dtMainLoop.Rows[i]["SENSHU_NM"]);
                tmpGyohoCd = Util.ToString(dtMainLoop.Rows[i]["GYOHO_CD"]);
                tmpShohizei = Util.ToString(dtMainLoop.Rows[i]["GOKEI_SHOHIZEI"]);

                // 水揚数量の小数点第二位を切り捨てる
                mizuageSuryo = Util.ToDecimal(dtMainLoop.Rows[i]["SURYO"]) * 10;
                mizuageSuryo = (decimal)((int)(mizuageSuryo)) / 10;
                #endregion

                #region インサートテーブル
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                Sql.Append(") ");
                #endregion

                #region データ登録
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                dbSORT++;
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SERIBI"]); // セリ日
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["DENPYO_BANGO"]); // 伝票番号
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YAMA_NO"]); // 山NO
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFunanushiCd); // 船主CD（印字用）
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpFunanushiNm); // 船主名称
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpGyohoCd); // 漁法CD
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOSHU_CD"]); // 魚種CD
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOSHU_NM"]); // 魚種名称
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, mizuageSuryo); // 水揚数量Kg
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TANKA"]); // 単価
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, tmpShohizei); // 合計消費税
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU"]); // 金額
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人CD
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_NM"]); // 仲買人名称
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主CD（制御用）
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, printDate); // 印刷日付
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, shishoCd); // 支所コード

                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KEIGENFLG"]); // 軽減フラグ

                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["UCHI_ZEIRITSU"]); // 標準税率
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["UCHI_KIN"]); // 標準金額
                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["UCHI_SHOHIZEI"]); // 標準消費税

                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["UCHI_KEIZEIRITSU"]); // 軽減税率
                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["UCHI_KEIKIN"]); // 軽減金額
                dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["UCHI_KEISHOHIZEI"]); // 軽減消費税


                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                #endregion

                i++;
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 西暦→和暦変換 Util.ConvJpDate(DateTime.Now, this.Dba)
        /// </summary>
        /// <returns></returns>
        private string convAdToJpDateString(DateTime targetDate)
        {
            string[] arrJpDate = Util.ConvJpDate(targetDate, this.Dba);
            return string.Format("{0}{1}年{2:D2}月{3:D2}日", arrJpDate[0], arrJpDate[2], Util.ToInt(arrJpDate[3]), Util.ToInt(arrJpDate[4]));
        }

        //private void SetExcelSetting(GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xls)
        //{
        //    // MinColumnWidth エクスポートされたファイル内における、空の列の最小幅（Twips単位）を取得または設定します。
        //    float MinColWidth = (float)(Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "EXCEL", "MinColumnWidth")));
        //    xls.MinColumnWidth = MinColWidth;

        //    //用紙の設定をいったんリセットする。
        //    xls.PageSettings.Reset();
        //    // 用紙の向き
        //    int PageOrientation = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "EXCEL", "Orientation"));
        //    xls.PageSettings.Orientation = (GrapeCity.ActiveReports.Document.Section.PageOrientation)(PageOrientation);
        //    // 用紙サイズ
        //    int PageSize = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "EXCEL", "PaperSize"));
        //    xls.PageSettings.PaperSize = (GrapeCity.ActiveReports.Export.Excel.Section.PaperSizes)(PageSize);

        //}

        #endregion

    }
}
        