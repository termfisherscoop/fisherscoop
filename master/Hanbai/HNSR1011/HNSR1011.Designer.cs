﻿namespace jp.co.fsi.hn.hnsr1011
{
    partial class HNSR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblCodeBetDate = new System.Windows.Forms.Label();
			this.lblDayTo = new System.Windows.Forms.Label();
			this.lblMonthTo = new System.Windows.Forms.Label();
			this.lblYearTo = new System.Windows.Forms.Label();
			this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoTo = new System.Windows.Forms.Label();
			this.lblDayFr = new System.Windows.Forms.Label();
			this.lblMonthFr = new System.Windows.Forms.Label();
			this.lblYearFr = new System.Windows.Forms.Label();
			this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoFr = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanKbnNm = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 609);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 32);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "セリ日報";
			// 
			// lblCodeBetDate
			// 
			this.lblCodeBetDate.AutoSize = true;
			this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
			this.lblCodeBetDate.Location = new System.Drawing.Point(393, 7);
			this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblCodeBetDate.Name = "lblCodeBetDate";
			this.lblCodeBetDate.Size = new System.Drawing.Size(24, 16);
			this.lblCodeBetDate.TabIndex = 8;
			this.lblCodeBetDate.Tag = "CHANGE";
			this.lblCodeBetDate.Text = "～";
			this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblDayTo
			// 
			this.lblDayTo.AutoSize = true;
			this.lblDayTo.BackColor = System.Drawing.Color.Silver;
			this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayTo.ForeColor = System.Drawing.Color.Black;
			this.lblDayTo.Location = new System.Drawing.Point(672, 7);
			this.lblDayTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDayTo.Name = "lblDayTo";
			this.lblDayTo.Size = new System.Drawing.Size(24, 16);
			this.lblDayTo.TabIndex = 16;
			this.lblDayTo.Tag = "CHANGE";
			this.lblDayTo.Text = "日";
			this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMonthTo
			// 
			this.lblMonthTo.AutoSize = true;
			this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthTo.ForeColor = System.Drawing.Color.Black;
			this.lblMonthTo.Location = new System.Drawing.Point(599, 7);
			this.lblMonthTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMonthTo.Name = "lblMonthTo";
			this.lblMonthTo.Size = new System.Drawing.Size(24, 16);
			this.lblMonthTo.TabIndex = 14;
			this.lblMonthTo.Tag = "CHANGE";
			this.lblMonthTo.Text = "月";
			this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearTo
			// 
			this.lblYearTo.AutoSize = true;
			this.lblYearTo.BackColor = System.Drawing.Color.Silver;
			this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearTo.ForeColor = System.Drawing.Color.Black;
			this.lblYearTo.Location = new System.Drawing.Point(528, 7);
			this.lblYearTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblYearTo.Name = "lblYearTo";
			this.lblYearTo.Size = new System.Drawing.Size(24, 16);
			this.lblYearTo.TabIndex = 12;
			this.lblYearTo.Tag = "CHANGE";
			this.lblYearTo.Text = "年";
			this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateDayTo
			// 
			this.txtDateDayTo.AutoSizeFromLength = false;
			this.txtDateDayTo.DisplayLength = null;
			this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateDayTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateDayTo.Location = new System.Drawing.Point(628, 3);
			this.txtDateDayTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateDayTo.MaxLength = 2;
			this.txtDateDayTo.Name = "txtDateDayTo";
			this.txtDateDayTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateDayTo.TabIndex = 8;
			this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateDayTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDayTo_KeyDown);
			this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
			// 
			// txtDateYearTo
			// 
			this.txtDateYearTo.AutoSizeFromLength = false;
			this.txtDateYearTo.DisplayLength = null;
			this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateYearTo.Location = new System.Drawing.Point(486, 3);
			this.txtDateYearTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYearTo.MaxLength = 2;
			this.txtDateYearTo.Name = "txtDateYearTo";
			this.txtDateYearTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearTo.TabIndex = 6;
			this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
			// 
			// txtDateMonthTo
			// 
			this.txtDateMonthTo.AutoSizeFromLength = false;
			this.txtDateMonthTo.DisplayLength = null;
			this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateMonthTo.Location = new System.Drawing.Point(556, 3);
			this.txtDateMonthTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateMonthTo.MaxLength = 2;
			this.txtDateMonthTo.Name = "txtDateMonthTo";
			this.txtDateMonthTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthTo.TabIndex = 7;
			this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
			// 
			// lblDateGengoTo
			// 
			this.lblDateGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengoTo.Location = new System.Drawing.Point(426, 3);
			this.lblDateGengoTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengoTo.Name = "lblDateGengoTo";
			this.lblDateGengoTo.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengoTo.TabIndex = 10;
			this.lblDateGengoTo.Tag = "DISPNAME";
			this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDayFr
			// 
			this.lblDayFr.AutoSize = true;
			this.lblDayFr.BackColor = System.Drawing.Color.Silver;
			this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayFr.ForeColor = System.Drawing.Color.Black;
			this.lblDayFr.Location = new System.Drawing.Point(356, 7);
			this.lblDayFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDayFr.Name = "lblDayFr";
			this.lblDayFr.Size = new System.Drawing.Size(24, 16);
			this.lblDayFr.TabIndex = 7;
			this.lblDayFr.Tag = "CHANGE";
			this.lblDayFr.Text = "日";
			this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMonthFr
			// 
			this.lblMonthFr.AutoSize = true;
			this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthFr.ForeColor = System.Drawing.Color.Black;
			this.lblMonthFr.Location = new System.Drawing.Point(283, 7);
			this.lblMonthFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMonthFr.Name = "lblMonthFr";
			this.lblMonthFr.Size = new System.Drawing.Size(24, 16);
			this.lblMonthFr.TabIndex = 5;
			this.lblMonthFr.Tag = "CHANGE";
			this.lblMonthFr.Text = "月";
			this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearFr
			// 
			this.lblYearFr.AutoSize = true;
			this.lblYearFr.BackColor = System.Drawing.Color.Silver;
			this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearFr.ForeColor = System.Drawing.Color.Black;
			this.lblYearFr.Location = new System.Drawing.Point(211, 7);
			this.lblYearFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblYearFr.Name = "lblYearFr";
			this.lblYearFr.Size = new System.Drawing.Size(24, 16);
			this.lblYearFr.TabIndex = 3;
			this.lblYearFr.Tag = "CHANGE";
			this.lblYearFr.Text = "年";
			this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateDayFr
			// 
			this.txtDateDayFr.AutoSizeFromLength = false;
			this.txtDateDayFr.DisplayLength = null;
			this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateDayFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateDayFr.Location = new System.Drawing.Point(312, 3);
			this.txtDateDayFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateDayFr.MaxLength = 2;
			this.txtDateDayFr.Name = "txtDateDayFr";
			this.txtDateDayFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateDayFr.TabIndex = 5;
			this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
			// 
			// txtDateYearFr
			// 
			this.txtDateYearFr.AutoSizeFromLength = false;
			this.txtDateYearFr.DisplayLength = null;
			this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateYearFr.Location = new System.Drawing.Point(169, 3);
			this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYearFr.MaxLength = 2;
			this.txtDateYearFr.Name = "txtDateYearFr";
			this.txtDateYearFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearFr.TabIndex = 3;
			this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
			// 
			// txtDateMonthFr
			// 
			this.txtDateMonthFr.AutoSizeFromLength = false;
			this.txtDateMonthFr.DisplayLength = null;
			this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateMonthFr.Location = new System.Drawing.Point(240, 3);
			this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateMonthFr.MaxLength = 2;
			this.txtDateMonthFr.Name = "txtDateMonthFr";
			this.txtDateMonthFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthFr.TabIndex = 4;
			this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
			// 
			// lblDateGengoFr
			// 
			this.lblDateGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengoFr.Location = new System.Drawing.Point(109, 3);
			this.lblDateGengoFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengoFr.Name = "lblDateGengoFr";
			this.lblDateGengoFr.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengoFr.TabIndex = 1;
			this.lblDateGengoFr.Tag = "DISPNAME";
			this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(106, 2);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(156, 1);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(701, 29);
			this.lblMizuageShisho.TabIndex = 0;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "水揚支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanKbn
			// 
			this.txtSeisanKbn.AutoSizeFromLength = true;
			this.txtSeisanKbn.DisplayLength = null;
			this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSeisanKbn.Location = new System.Drawing.Point(109, 3);
			this.txtSeisanKbn.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeisanKbn.MaxLength = 5;
			this.txtSeisanKbn.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtSeisanKbn.Name = "txtSeisanKbn";
			this.txtSeisanKbn.Size = new System.Drawing.Size(67, 23);
			this.txtSeisanKbn.TabIndex = 2;
			this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
			// 
			// lblSeisanKbnNm
			// 
			this.lblSeisanKbnNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanKbnNm.Location = new System.Drawing.Point(181, 2);
			this.lblSeisanKbnNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
			this.lblSeisanKbnNm.Size = new System.Drawing.Size(283, 24);
			this.lblSeisanKbnNm.TabIndex = 6;
			this.lblSeisanKbnNm.Tag = "DISPNAME";
			this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(701, 30);
			this.label1.TabIndex = 0;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "精算区分";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(701, 31);
			this.label2.TabIndex = 0;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "日付範囲";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(711, 118);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblCodeBetDate);
			this.fsiPanel3.Controls.Add(this.lblDayTo);
			this.fsiPanel3.Controls.Add(this.lblDateGengoFr);
			this.fsiPanel3.Controls.Add(this.lblMonthTo);
			this.fsiPanel3.Controls.Add(this.txtDateMonthFr);
			this.fsiPanel3.Controls.Add(this.lblYearTo);
			this.fsiPanel3.Controls.Add(this.txtDateYearFr);
			this.fsiPanel3.Controls.Add(this.txtDateDayTo);
			this.fsiPanel3.Controls.Add(this.txtDateDayFr);
			this.fsiPanel3.Controls.Add(this.txtDateYearTo);
			this.fsiPanel3.Controls.Add(this.lblYearFr);
			this.fsiPanel3.Controls.Add(this.txtDateMonthTo);
			this.fsiPanel3.Controls.Add(this.lblMonthFr);
			this.fsiPanel3.Controls.Add(this.lblDateGengoTo);
			this.fsiPanel3.Controls.Add(this.lblDayFr);
			this.fsiPanel3.Controls.Add(this.label2);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 82);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(701, 31);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtSeisanKbn);
			this.fsiPanel2.Controls.Add(this.lblSeisanKbnNm);
			this.fsiPanel2.Controls.Add(this.label1);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 43);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(701, 30);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(701, 29);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNSR1011
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNSR1011";
			this.Text = "";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }
        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private common.controls.FsiTextBox txtDateDayTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label lblSeisanKbnNm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}
