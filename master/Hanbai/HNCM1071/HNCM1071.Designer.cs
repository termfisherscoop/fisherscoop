﻿namespace jp.co.fsi.hn.hncm1071
{
    partial class HNCM1071
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblMeisho = new System.Windows.Forms.Label();
			this.txtMeisho = new System.Windows.Forms.TextBox();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.btnEnter = new System.Windows.Forms.Button();
			this.txtShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShishoNm = new System.Windows.Forms.Label();
			this.lblShisho = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF1
			// 
			this.btnF1.TabIndex = 2;
			// 
			// btnF2
			// 
			this.btnF2.TabIndex = 3;
			// 
			// btnF3
			// 
			this.btnF3.TabIndex = 4;
			// 
			// btnF4
			// 
			this.btnF4.TabIndex = 5;
			// 
			// btnF5
			// 
			this.btnF5.TabIndex = 6;
			// 
			// btnF7
			// 
			this.btnF7.TabIndex = 8;
			// 
			// btnF6
			// 
			this.btnF6.TabIndex = 7;
			// 
			// btnF8
			// 
			this.btnF8.TabIndex = 9;
			// 
			// btnF9
			// 
			this.btnF9.TabIndex = 10;
			// 
			// btnF12
			// 
			this.btnF12.TabIndex = 13;
			// 
			// btnF11
			// 
			this.btnF11.TabIndex = 12;
			// 
			// btnF10
			// 
			this.btnF10.TabIndex = 11;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Location = new System.Drawing.Point(7, 623);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(908, 133);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(898, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "控除項目の登録";
			// 
			// lblMeisho
			// 
			this.lblMeisho.BackColor = System.Drawing.Color.Silver;
			this.lblMeisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMeisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMeisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMeisho.Location = new System.Drawing.Point(0, 0);
			this.lblMeisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMeisho.Name = "lblMeisho";
			this.lblMeisho.Size = new System.Drawing.Size(864, 33);
			this.lblMeisho.TabIndex = 0;
			this.lblMeisho.Tag = "CHANGE";
			this.lblMeisho.Text = "名　　　称";
			this.lblMeisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMeisho
			// 
			this.txtMeisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMeisho.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtMeisho.Location = new System.Drawing.Point(119, 4);
			this.txtMeisho.Margin = new System.Windows.Forms.Padding(4);
			this.txtMeisho.MaxLength = 30;
			this.txtMeisho.Name = "txtMeisho";
			this.txtMeisho.Size = new System.Drawing.Size(239, 23);
			this.txtMeisho.TabIndex = 1;
			this.txtMeisho.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(16, 141);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(872, 479);
			this.dgvList.TabIndex = 2;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// btnEnter
			// 
			this.btnEnter.BackColor = System.Drawing.Color.SkyBlue;
			this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEnter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnEnter.ForeColor = System.Drawing.Color.Navy;
			this.btnEnter.Location = new System.Drawing.Point(89, 4);
			this.btnEnter.Margin = new System.Windows.Forms.Padding(4);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new System.Drawing.Size(87, 60);
			this.btnEnter.TabIndex = 1;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n決定";
			this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = false;
			// 
			// txtShishoCd
			// 
			this.txtShishoCd.AutoSizeFromLength = true;
			this.txtShishoCd.DisplayLength = null;
			this.txtShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShishoCd.Location = new System.Drawing.Point(119, 4);
			this.txtShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtShishoCd.MaxLength = 4;
			this.txtShishoCd.Name = "txtShishoCd";
			this.txtShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtShishoCd.TabIndex = 1001;
			this.txtShishoCd.TabStop = false;
			this.txtShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCd_Validating);
			// 
			// lblShishoNm
			// 
			this.lblShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShishoNm.ForeColor = System.Drawing.Color.Black;
			this.lblShishoNm.Location = new System.Drawing.Point(166, 3);
			this.lblShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShishoNm.Name = "lblShishoNm";
			this.lblShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblShishoNm.TabIndex = 1002;
			this.lblShishoNm.Tag = "DISPNAME";
			this.lblShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShisho
			// 
			this.lblShisho.BackColor = System.Drawing.Color.Silver;
			this.lblShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShisho.Location = new System.Drawing.Point(0, 0);
			this.lblShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShisho.Name = "lblShisho";
			this.lblShisho.Size = new System.Drawing.Size(864, 33);
			this.lblShisho.TabIndex = 1000;
			this.lblShisho.Tag = "CHANGE";
			this.lblShisho.Text = "支　　　所";
			this.lblShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(16, 53);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(872, 81);
			this.fsiTableLayoutPanel1.TabIndex = 1003;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtMeisho);
			this.fsiPanel2.Controls.Add(this.lblMeisho);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 44);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(864, 33);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtShishoCd);
			this.fsiPanel1.Controls.Add(this.lblShishoNm);
			this.fsiPanel1.Controls.Add(this.lblShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(864, 33);
			this.fsiPanel1.TabIndex = 0;
			// 
			// HNCM1071
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(898, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1071";
			this.Par1 = "";
			this.Text = "控除科目の登録";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMeisho;
        private System.Windows.Forms.TextBox txtMeisho;
        private System.Windows.Forms.DataGridView dgvList;
        protected System.Windows.Forms.Button btnEnter;
        private common.controls.FsiTextBox txtShishoCd;
        private System.Windows.Forms.Label lblShishoNm;
        private System.Windows.Forms.Label lblShisho;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}