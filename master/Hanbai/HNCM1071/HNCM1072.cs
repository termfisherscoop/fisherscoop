﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1071
{
    /// <summary>
    /// 控除の設定(HANC9122)
    /// </summary>
    public partial class HNCM1072 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1072()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1072(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region 変数

        private string _KanjoKamokCd = "";    // 勘定科目CD
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：設定コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            string shishoCd = this.Par2;
            this.txtShishoCd.Text = shishoCd;
            this.lblShishoNm.Text = Dba.GetName(UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);
            this.txtShishoCd.Enabled = (shishoCd == "1") ? true : false;

            // Esc,F1,F3,F6のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF3.Location = this.btnF3.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnEsc.Visible = true;
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtKojoKomokSettei":
                case "txtKojoTaishoShikriKomok":
                case "txtKanjoKamokCd":
                case "txtHojoKamokCd":
                case "txtBumonCd":
                case "txtZeiKbn":
                case "txtJigyoKbn":
                    this.btnF1.Enabled = true;
                    break;
                case "txtShishoCd":
                    this.btnF1.Enabled = (this.txtShishoCd.Text == "1") ? true : false;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtKojoKomokSettei":
                    //アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1071.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1071.HNCM1073"); ;
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtKanjoKamokCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKojoKomokSettei.Text = result[0];
                            }
                        }
                    }
                    break;

                case "txtKojoTaishoShikriKomok":
                    //アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1071.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1071.HNCM1073"); ;
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.InData = this.txtKanjoKamokCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKojoTaishoShikriKomok.Text = result[0];
                            }
                        }
                    }
                    break;

                case "txtKanjoKamokCd":
                    //アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"ZMCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033"); ;
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.InData = this.txtKanjoKamokCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKanjoKamokCd.Text = result[0];
                                this.lblKanjoKamokuCd.Text = result[1];

                                this.IsValidKanjoKamokCd(false);
                            }
                        }
                    }
                    break;

                case "txtHojoKamokCd":
                    //アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"ZMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.zm.zmcm1041.ZMCM1044");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.Par2 = this.txtShishoCd.Text;
                            frm.InData = this.txtKanjoKamokCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHojoKamokCd.Text = result[0];
                                this.lblHojoKamokuCd.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtBumonCd":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("CMCM2041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_CM_BUMON";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtBumonCd.Text = result[0];
                                this.lblBumonnCd.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtZeiKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"ZMCM1061.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.zm.zmcm1061.ZMCM1061");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "TB_ZM_F_ZEI_KUBUN";
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtZeiKbn.Text = result[0];
                                this.lblZeiKbnn.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtJigyoKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_JIGYO_KUBUN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtJigyoKbn.Text = result[0];
                                this.lblJigyoKubn.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtShishoCd":

                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtShishoCd.Text = result[0];
                                this.lblShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                case "txtShohizeiNyuryokuHoho":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO";
                            frm.InData = this.txtShohizeiNyuryokuHoho.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (String[])frm.OutData;
                                this.txtShohizeiNyuryokuHoho.Text = result[0];
                                this.lblShzNrkHohoNm.Text = result[1];

                                // 次の項目(締日)にフォーカス
                                this.txtScript.Focus();
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            // 削除する前に再度コードの存在チェック
            if (IsCodeUsing())
            {
                Msg.Error("指定した設定コードは削除できません。");
                return;
            }
            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }
            // 削除処理
            try
            {
                // データ削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.InData);
                //this.Dba.Delete("TM_HN_KOJO_KAMOKU_SETTEI", "KAISHA_CD = @KAISHA_CD AND SETTEI_CD = @SETTEI_CD", dpc);
                this.Dba.Delete("TB_HN_KOJO_KAMOKU_SETTEI", "KAISHA_CD = @KAISHA_CD AND SETTEI_CD = @SETTEI_CD", dpc);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKojo = SetKojoParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    if (IsCodeExists())
                    {
                        Msg.Error("設定情報が登録済みです。");
                        return;
                    }

                    // データ登録
                    // 控除項目設定
                    this.Dba.Insert("TB_HN_KOJO_KAMOKU_SETTEI", (DbParamCollection)alParamsKojo[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {

                    // データ更新
                    this.Dba.Update("TB_HN_KOJO_KAMOKU_SETTEI",
                    (DbParamCollection)alParamsKojo[1],
                    "KAISHA_CD = @KAISHA_CD AND SETTEI_CD = @SETTEI_CD AND SHISHO_CD = @SHISHO_CD",
                    (DbParamCollection)alParamsKojo[0]);

                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtShishoCd.Text, this.lblShishoNm.Text, this.txtShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtShishoCd.SelectAll();
                this.txtShishoCd.Focus();
            }
        }

        /// <summary>
        /// 設定コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSetteiCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSetteiCd())
            {
                e.Cancel = true;
                this.txtSetteiCd.SelectAll();
            }

        }

        /// <summary>
        /// 控除項目設定の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojoKomokSettei_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKojoKomokSettei())
            {
                e.Cancel = true;
                this.txtKojoKomokSettei.SelectAll();
            }
        }

        /// <summary>
        /// 控除項目名称の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojoKomokNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKojoKomokNm())
            {
                e.Cancel = true;
                this.txtKojoKomokNm.SelectAll();
            }
        }

        /// <summary>
        /// 控除対象仕切項目コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojoTaishoShikriKomok_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKojoTaishoShikriKomok())
            {
                e.Cancel = true;
                this.txtKojoTaishoShikriKomok.SelectAll();
            }
        }

        /// <summary>
        /// 計算符号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKeisanhugo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKeisanhugo())
            {
                e.Cancel = true;
                this.txtKeisanFugo.SelectAll();
            }
        }

        /// <summary>
        /// 勘定科目コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKanjoKamokCd(false))
            {
                e.Cancel = true;
                this.txtKanjoKamokCd.SelectAll();
            }
        }

        /// <summary>
        /// 補助科目コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHojoKamokCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidHojoKamokCd())
            {
                e.Cancel = true;
                this.txtHojoKamokCd.SelectAll();
            }
        }

        /// <summary>
        /// 部門コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonCd())
            {
                e.Cancel = true;
                this.txtBumonCd.SelectAll();
            }
        }

        /// <summary>
        /// 税区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZeiKbn())
            {
                e.Cancel = true;
                this.txtZeiKbn.SelectAll();
            }
        }

        /// <summary>
        /// 事業区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZigyoKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJigyoKbn())
            {
                e.Cancel = true;
                this.txtJigyoKbn.SelectAll();
            }
        }

        /// <summary>
        /// 貸借区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTaishakKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTaishakKbn())
            {
                e.Cancel = true;
                this.txtTaishakKbn.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 設定コードの初期値を取得
            // 設定コードの中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = 1");
            where.Append(" AND KAISHA_CD = @KAISHA_CD");
            DataTable dtMaxSettei =
                //this.Dba.GetDataTableByConditionWithParams("MAX(SETTEI_CD) AS MAX_CD",
                //    "TM_HN_KOJO_KAMOKU_SETTEI", Util.ToString(where), dpc);
                this.Dba.GetDataTableByConditionWithParams("MAX(SETTEI_CD) AS MAX_CD",
                    "TB_HN_KOJO_KAMOKU_SETTEI", Util.ToString(where), dpc);
            if (dtMaxSettei.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxSettei.Rows[0]["MAX_CD"]))
            {
                this.txtSetteiCd.Text = Util.ToString(Util.ToInt(dtMaxSettei.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtSetteiCd.Text = "1";
            }
            //支所コードの設定
            string shishoCd = this.Par2;
            this.txtShishoCd.Text = shishoCd;
            this.lblShishoNm.Text = Dba.GetName(UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);
            this.txtShishoCd.Enabled = (shishoCd == "1") ? true : false;

            // 各項目の初期値を設定
            // 計算符号0
            this.txtKeisanFugo.Text = "1";
            // 勘定科目コード0
            this.txtKanjoKamokCd.Text = "0";
            // 補助科目コード0
            this.txtHojoKamokCd.Text = "0";
            // 部門コード0
            this.txtBumonCd.Text = "0";
            // 税区分0
            this.txtZeiKbn.Text = "0";
            // 事業区分0
            this.txtJigyoKbn.Text = "0";
            // 貸借区分1
            this.txtTaishakKbn.Text = "1";
            //計算区分
            this.chkKeisanKbn.Checked = false;
            //消費税入力方法
            this.txtShohizeiNyuryokuHoho.Text = "2";
            this.lblShzNrkHohoNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", shishoCd, this.txtShohizeiNyuryokuHoho.Text);
            //計算式
            this.txtScript.Text = "";

            this._KanjoKamokCd = "";
            //精算処理項目の設定
            chageEnable();

            // 削除ボタンを無効に設定
            this.btnF3.Enabled = false;
            // 控除項目設定に初期フォーカス
            //this.ActiveControl = this.txtKojoKomokSettei;
            //this.txtKojoKomokSettei.Focus();
            this.ActiveControl = this.txtSetteiCd;
            this.txtSetteiCd.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.SETTEI_CD");
            cols.Append(" ,A.KOJO_KOMOKU_SETTEI");
            cols.Append(" ,A.KOJO_KOMOKU_NM");
            cols.Append(" ,A.KOJO_TAISHO_SHIKIRI_KOMOKU");
            cols.Append(" ,A.KEISAN_FUGO");
            cols.Append(" ,A.KANJO_KAMOKU_CD");
            cols.Append(" ,A.HOJO_KAMOKU_CD");
            cols.Append(" ,A.BUMON_CD");
            cols.Append(" ,A.ZEI_KUBUN");
            cols.Append(" ,A.JIGYO_KUBUN");
            cols.Append(" ,A.TAISHAKU_KUBUN");
            cols.Append(" ,A.SHISHO_CD");
            cols.Append(" ,A.KEISAN_KBN");
            cols.Append(" ,A.SEISAN_KBN");
            cols.Append(" ,A.TANKA");
            cols.Append(" ,A.SHOHIZEI_NYURYOKU_HOHO");
            cols.Append(" ,A.SCRIPT");
            cols.Append(" ,A.SHIKIRISHO_SEISAN_KBN");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_KOJO_KAMOKU_SETTEI AS A");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));
            string shishoCd = this.Par2;
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.SETTEI_CD = @SETTEI_CD AND SHISHO_CD = @SHISHO_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtSetteiCd.Text = Util.ToString(drDispData["SETTEI_CD"]);
            this.txtKojoKomokSettei.Text = Util.ToString(drDispData["KOJO_KOMOKU_SETTEI"]);
            this.txtKojoKomokNm.Text = Util.ToString(drDispData["KOJO_KOMOKU_NM"]);
            this.txtKojoTaishoShikriKomok.Text = Util.ToString(drDispData["KOJO_TAISHO_SHIKIRI_KOMOKU"]);
            this.txtKeisanFugo.Text = Util.ToString(drDispData["KEISAN_FUGO"]);
            this.txtKanjoKamokCd.Text = Util.ToString(drDispData["KANJO_KAMOKU_CD"]);
            this.txtHojoKamokCd.Text = Util.ToString(drDispData["HOJO_KAMOKU_CD"]);
            this.txtBumonCd.Text = Util.ToString(drDispData["BUMON_CD"]);
            this.txtZeiKbn.Text = Util.ToString(drDispData["ZEI_KUBUN"]);
            this.txtJigyoKbn.Text = Util.ToString(drDispData["JIGYO_KUBUN"]);
            this.txtTaishakKbn.Text = Util.ToString(drDispData["TAISHAKU_KUBUN"]);
            this.lblKanjoKamokuCd.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", null, this.txtKanjoKamokCd.Text);
            this.lblHojoKamokuCd.Text = this.Dba.GetHojoKmkNm(this.UInfo, Util.ToDecimal(shishoCd), this.txtKanjoKamokCd.Text,  this.txtHojoKamokCd.Text);
            this.lblBumonnCd.Text = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", null, this.txtBumonCd.Text);
            this.lblZeiKbnn.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_ZEI_KUBUN", null, txtZeiKbn.Text);
            this.lblJigyoKubn.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_JIGYO_KUBUN", null, txtJigyoKbn.Text);
            this.txtShishoCd.Text = Util.ToString(drDispData["SHISHO_CD"]);
            this.lblShishoNm.Text = Dba.GetName(UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);
            this.chkKeisanKbn.Checked = Util.ToDecimal(drDispData["KEISAN_KBN"]) == 1 ? true : false;
            this.txtKomoku.Text = Util.ToString(drDispData["TANKA"]);
            this.txtSeisanKbn.Text = Util.ToString(drDispData["SEISAN_KBN"]);
            this.txtShohizeiNyuryokuHoho.Text = Util.ToString(drDispData["SHOHIZEI_NYURYOKU_HOHO"]);
            if (this.txtShohizeiNyuryokuHoho.Text != "")
            {
                this.lblShzNrkHohoNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", shishoCd, this.txtShohizeiNyuryokuHoho.Text);
            }
            this.txtScript.Text = Util.ToString(drDispData["SCRIPT"]);
            this.txtShikiriSeisanKbn.Text = Util.ToString(drDispData["SHIKIRISHO_SEISAN_KBN"]);
            //精算処理項目の設定
            chageEnable();
            // 削除ボタンを有効に設定
            this.btnF3.Enabled = true;
            //　設定コード、控除項目設定は入力不可　
            this.txtSetteiCd.Enabled = false;
            this.txtKojoKomokSettei.Enabled = false;

            this._KanjoKamokCd = this.txtKanjoKamokCd.Text;
            this.IsValidKanjoKamokCd(false);
        }

        /// <summary>
        /// 支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 控除項目ではすべては不可とする
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtShishoCd.Text) || Equals(this.txtShishoCd.Text, "0"))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtShishoCd.Text, this.txtShishoCd.Text);

            if (ValChk.IsEmpty(this.lblShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 設定コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSetteiCd()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtSetteiCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 入力が0の場合にエラー表示
            else if (this.txtSetteiCd.Text == "0")
            {
                Msg.Error("ｾﾞﾛは使用できません。。");
                return false;
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtSetteiCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 6, this.txtSetteiCd.Text);
            string shishoCd = this.txtShishoCd.Text;
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SETTEI_CD = @SETTEI_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            DataTable dtSettei =
                //this.Dba.GetDataTableByConditionWithParams("SETTEI_CD",
                //    "TM_HN_KOJO_KAMOKU_SETTEI", Util.ToString(where), dpc);
                this.Dba.GetDataTableByConditionWithParams("SETTEI_CD",
                    "TB_HN_KOJO_KAMOKU_SETTEI", Util.ToString(where), dpc);
            if (dtSettei.Rows.Count > 0)
            {
                Msg.Error("既に存在する設定コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 控除項目設定の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKojoKomokSettei()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtKojoKomokSettei.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 15バイトを超えていたらエラー
            else if (!ValChk.IsWithinLength(this.txtKojoKomokSettei.Text, this.txtKojoKomokSettei.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            //else
            //{
            //    // TB_控除区分マスタテーブルからデータを取得
            //    DbParamCollection dpc = new DbParamCollection();
            //    StringBuilder sql = new StringBuilder();
            //    sql.Append("SELECT");
            //    sql.Append(" KUBUN_NM ");
            //    sql.Append("FROM ");
            //    sql.Append(" TB_HN_KOJO_KUBUN_MST ");
            //    sql.Append("WHERE");
            //    sql.Append(" KAISHA_CD = @KAISHA_CD");
            //    sql.Append(" AND KOJO_KUBUN = 1");
            //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            //    DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            //    // 控除項目設定候補セット
            //    ArrayList kojoKomokuNmList = new ArrayList();
            //    foreach (DataRow dr in dtResult.Rows)
            //    {
            //        kojoKomokuNmList.Add(dr["KUBUN_NM"]);
            //    }

            //    // 入力値が候補外の場合エラー
            //    if (!kojoKomokuNmList.Contains(this.txtKojoKomokSettei.Text))
            //    {
            //        Msg.Error("入力に誤りがあります。");
            //        return false;
            //    }
            //}

            return true;
        }

        /// <summary>
        /// 控除項目名称の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKojoKomokNm()
        {
            // 15バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtKojoKomokNm.Text, this.txtKojoKomokNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 控除対象仕切項目入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKojoTaishoShikriKomok()
        {
            // 15バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtKojoTaishoShikriKomok.Text, this.txtKojoTaishoShikriKomok.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            else
            {
                // TODO:F1キー押下時の検索画面に表示される項目以外が入力された場合、エラーにすべきでは。
                // TODO:現状、控除対象仕切項が何に使用されているのか不明な為、チェックしておりません。
            }

            return true;
        }

        /// <summary>
        /// 計算符号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKeisanhugo()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtKeisanFugo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 入力が1・2の以外の場合にエラー表示
            else if (!(this.txtKeisanFugo.Text == "1" || this.txtKeisanFugo.Text == "2"))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKanjoKamokCd(bool all)
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtKanjoKamokCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtKanjoKamokCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            this.lblKanjoKamokuCd.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", null, this.txtKanjoKamokCd.Text);

            // 勘定科目名の取得
            StringBuilder cols = new StringBuilder();
            cols.Append("*  ");

            StringBuilder from = new StringBuilder();
            from.Append(" VI_ZM_KANJO_KAMOKU");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokCd.Text);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            else if (all == false)
            {
                //入力不可の解除
                this.txtHojoKamokCd.Enabled = true;
                this.txtBumonCd.Enabled = true;

                // 取得した内容を表示
                DataRow drDispData = dtDispData.Rows[0];
                this.txtBumonCd.Text = "0";
                this.lblBumonnCd.Text = "";
                // 部門有無が0の場合、部門コードの入力不可
                if ("0".Equals(Util.ToString(drDispData["BUMON_UMU"])))
                {
                    this.txtBumonCd.Enabled = false;
                }
                // 補助有無が0の場合、補助科目コードの入力不可
                if ("0".Equals(Util.ToString(drDispData["HOJO_KAMOKU_UMU"])))
                {
                    this.txtHojoKamokCd.Enabled = false;
                }
            }

            if (this._KanjoKamokCd != this.txtKanjoKamokCd.Text)
            {
                this.lblHojoKamokuCd.Text = this.Dba.GetHojoKmkNm(this.UInfo, 0, this.txtKanjoKamokCd.Text, this.txtHojoKamokCd.Text);
                this._KanjoKamokCd = this.txtKanjoKamokCd.Text;
            }

            // 入力が空の場合にエラー表示
            // TODO:現状、勘定科目コードが何に使用されているか不明の為、空を許可
            /*if (ValChk.IsEmpty(this.lblKanjoKamokuCd.Text))
            {
                Msg.Error("勘定科目コードを入力して下さい。");
                return false;
            }*/

            return true;
        }

        /// <summary>
        /// 補助科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHojoKamokCd()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtHojoKamokCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            else if (this.txtHojoKamokCd.Text == "-1")
            {
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtHojoKamokCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            //this.lblHojoKamokuCd.Text = this.Dba.GetHojoKmkNm(this.UInfo, Util.ToDecimal( this.txtShishoCd.Text), this.txtKanjoKamokCd.Text, this.txtHojoKamokCd.Text);
            // 補助科目名の取得
            StringBuilder cols = new StringBuilder();
            cols.Append("     HOJO_KAMOKU_NM       AS HOJO_KAMOKU_NM");

            StringBuilder from = new StringBuilder();
            from.Append(" VI_ZM_HOJO_KAMOKU");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokCd.Text);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtHojoKamokCd.Text);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                // 存在しないコードを入力されたらエラー
                // 但し-1と0は許可
                if ("-1".Equals(this.txtHojoKamokCd.Text) || "0".Equals(this.txtHojoKamokCd.Text))
                {
                    this.lblHojoKamokuCd.Text = string.Empty;
                }
                else
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            else
            {
                // 取得した内容を表示
                DataRow drDispData = dtDispData.Rows[0];
                this.lblHojoKamokuCd.Text = Util.ToString(drDispData["HOJO_KAMOKU_NM"]);
            }

            return true;
        }

        /// <summary>
        /// 部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonCd()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtBumonCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtBumonCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            this.lblBumonnCd.Text = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", null, this.txtBumonCd.Text);

            return true;
        }

        /// <summary>
        /// 税区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidZeiKbn()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtZeiKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtZeiKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            this.lblZeiKbnn.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_ZEI_KUBUN", null, txtZeiKbn.Text);

            return true;
        }

        /// <summary>
        /// 事業区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJigyoKbn()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtJigyoKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtJigyoKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            this.lblJigyoKubn.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_JIGYO_KUBUN", null, txtJigyoKbn.Text);

            return true;
        }

        /// <summary>
        /// 貸借区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTaishakKbn()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtTaishakKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 入力が1・2以外の場合エラー表示
            else if (!(this.txtTaishakKbn.Text == "1" || this.txtTaishakKbn.Text == "2" ))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税入力方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiNyuryokuHoho()
        {
            string shishoCd = this.txtShishoCd.Text;

            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShohizeiNyuryokuHoho.Text))
            {
                this.txtShohizeiNyuryokuHoho.Text = "0";
                return true;
            }

            // 0,1,2,3のみ入力を許可
            if (!ValChk.IsNumber(this.txtShohizeiNyuryokuHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            if (this.txtShohizeiNyuryokuHoho.Text != "0")
            {
                // 登録済みの値のみ許可
                string name = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", shishoCd, this.txtShohizeiNyuryokuHoho.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                this.lblShzNrkHohoNm.Text = name;
            }
            return true;
        }

        /// <summary>
        /// スクリプトの入力チェック
        /// </summary>
        /// <returns>true:OK, false:NG</returns>
        private bool IsValidScript()
        {
            if (chkKeisanKbn.Checked == true && this.txtScript.Text == "")
            {
                Msg.Error("計算を行う場合は、計算式を入力してください。");
                return false;
                
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 設定コードのチェック
                if (!IsValidSetteiCd())
                {
                    this.txtSetteiCd.Focus();
                    return false;
                }
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 控除項目設定のチェック
                if (!IsValidKojoKomokSettei())
                {
                    this.txtKojoKomokSettei.Focus();
                    return false;
                }
            }

            // 控除項目名称のチェック
            if (!IsValidKojoKomokNm())
            {
                this.txtKojoKomokNm.Focus();
                return false;
            }

            // 控除対象仕切項目のチェック
            if (!IsValidKojoTaishoShikriKomok())
            {
                this.txtKojoTaishoShikriKomok.Focus();
                return false;
            }

            // 計算符号のチェック
            if (!IsValidKeisanhugo())
            {
                this.txtKeisanFugo.Focus();
                return false;
            }

            // 勘定科目コードのチェック
            if (!IsValidKanjoKamokCd(true))
            {
                this.txtKanjoKamokCd.Focus();
                return false;
            }

            // 補助科目コードのチェック
            if (!IsValidHojoKamokCd())
            {
                this.txtHojoKamokCd.Focus();
                return false;
            }

            // 部門コードのチェック
            if (!IsValidBumonCd())
            {
                this.txtBumonCd.Focus();
                return false;
            }

            // 税区分のチェック
            if (!IsValidZeiKbn())
            {
                this.txtZeiKbn.Focus();
                return false;
            }

            // 事業区分のチェック
            if (!IsValidJigyoKbn())
            {
                this.txtJigyoKbn.Focus();
                return false;
            }

            // 貸借区分のチェック
            if (!IsValidTaishakKbn())
            {
                this.txtTaishakKbn.Focus();
                return false;
            }

            // 消費税入力方法のチェック
            if (!IsValidShohizeiNyuryokuHoho())
            {
                this.txtShohizeiNyuryokuHoho.Focus();
                return false;
            }

            //計算式のチェック
            if (!IsValidScript())
            {
                this.txtScript.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// TM_HN_KOJO_KAMOKU_SETTEIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKojoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();
            //支所コードの取得
            string shishoCd = this.txtShishoCd.Text;

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと設定コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtSetteiCd.Text);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと設定コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtSetteiCd.Text);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                alParams.Add(whereParam);
            }
            // 控除項目設定
            updParam.SetParam("@KOJO_KOMOKU_SETTEI", SqlDbType.VarChar, 40, this.txtKojoKomokSettei.Text);
            // 控除項目名称
            updParam.SetParam("@KOJO_KOMOKU_NM", SqlDbType.VarChar, 40, this.txtKojoKomokNm.Text);
            // 控除対象仕切項目
            updParam.SetParam("@KOJO_TAISHO_SHIKIRI_KOMOKU", SqlDbType.VarChar, 40, this.txtKojoTaishoShikriKomok.Text);
            // 計算符号
            updParam.SetParam("@KEISAN_FUGO", SqlDbType.VarChar, 1, this.txtKeisanFugo.Text);
            // 勘定科目コード
            updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.VarChar, 6, this.txtKanjoKamokCd.Text);
            // 補助科目コード
            updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.VarChar, 10, this.txtHojoKamokCd.Text);
            // 部門コード
            updParam.SetParam("@BUMON_CD", SqlDbType.VarChar, 7, this.txtBumonCd.Text);
            // 税区分
            updParam.SetParam("@ZEI_KUBUN", SqlDbType.VarChar, 4, this.txtZeiKbn.Text);
            // 事業区分
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.VarChar, 3, this.txtJigyoKbn.Text);
            // 貸借区分
            updParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.VarChar, 3, this.txtTaishakKbn.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            //計算区分
            updParam.SetParam("@KEISAN_KBN", SqlDbType.Decimal, 1, this.chkKeisanKbn.Checked);
            //消費税入力方法
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, this.txtShohizeiNyuryokuHoho.Text);
            //計算式
            updParam.SetParam("@SCRIPT", SqlDbType.VarChar, 100, this.txtScript.Text);
            //計算項目
            updParam.SetParam("@TANKA", SqlDbType.Decimal, 11, Util.ToDecimal( this.txtKomoku.Text));
            //精算区分
            updParam.SetParam("@SEISAN_KBN", SqlDbType.VarChar, 20, this.txtSeisanKbn.Text);
            //仕切書精算区分
            updParam.SetParam("@SHIKIRISHO_SEISAN_KBN", SqlDbType.VarChar, 20, this.txtShikiriSeisanKbn.Text);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 対象の設定コードがマスタ登録されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeExists()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtSetteiCd.Text);
            //DataTable dtViHojokamoku = this.Dba.GetDataTableByConditionWithParams("*", "TM_HN_KOJO_KAMOKU_SETTEI", "KAISHA_CD = @KAISHA_CD AND SETTEI_CD = @SETTEI_CD", dpc);
            DataTable dtViHojokamoku = this.Dba.GetDataTableByConditionWithParams("*", "TB_HN_KOJO_KAMOKU_SETTEI", "KAISHA_CD = @KAISHA_CD AND SETTEI_CD = @SETTEI_CD", dpc);

            if (dtViHojokamoku.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 会社情報テーブルの最初に登録された会計終了日より前に登録・更新された設定コードは削除できない。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeUsing()
        {
            //最初に登録されている会計年度を取得
            StringBuilder from = new StringBuilder();
            from.Append("TB_ZM_KAISHA_JOHO AS A");

            DbParamCollection dpc = new DbParamCollection();

            DataTable dtKaikei = this.Dba.GetDataTableByConditionWithParams("MIN(KAIKEI_NENDO) AS 会計年度", from.ToString(),"", dpc);

            // 削除可否チェック
            // 指定した設定コードが使用されているかどうかをチェック
            from = new StringBuilder();
            from.Append("TB_ZM_KAISHA_JOHO AS A");
            from.Append(" INNER JOIN");
            //from.Append(" TM_HN_KOJO_KAMOKU_SETTEI AS B");
            from.Append(" TB_HN_KOJO_KAMOKU_SETTEI AS B");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD");

            StringBuilder where = new StringBuilder();
            where.Append("A.KAISHA_CD = @KAISHA_CD ");
            where.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND B.REGIST_DATE > A.KAIKEI_KIKAN_SHURYOBI ");
            where.Append(" AND B.SETTEI_CD = @SETTEI_CD ");
            where.Append(" OR  A.KAISHA_CD = @KAISHA_CD ");
            where.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND B.UPDATE_DATE > A.KAIKEI_KIKAN_SHURYOBI ");
            where.Append(" AND B.SETTEI_CD = @SETTEI_CD");

            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, Util.ToString(dtKaikei.Rows[0]["会計年度"]));

            DataTable dtKjkmk = this.Dba.GetDataTableByConditionWithParams("B.SETTEI_CD", from.ToString(), where.ToString(), dpc);


            if (dtKjkmk.Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        private void chkKeisanKbn_CheckedChanged(object sender, EventArgs e)
        {
            chageEnable();
        }

        private void chageEnable()
        {
            if (this.chkKeisanKbn.Checked == true)
            {
                this.txtShohizeiNyuryokuHoho.Enabled = true;
                this.txtKomoku.Enabled = true;
                this.txtScript.Enabled = true;
                this.txtSeisanKbn.Enabled = true;
            }
            else
            {
                this.txtShohizeiNyuryokuHoho.Enabled = false;
                this.txtKomoku.Enabled = false;
                this.txtScript.Enabled = false;
                this.txtSeisanKbn.Enabled = false;
            }
        }

		private void txtJigyoKbn_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
