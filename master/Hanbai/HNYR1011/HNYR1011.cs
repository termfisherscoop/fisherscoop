﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnyr1011
{
    /// <summary>
    /// 組合員別水揚年計表(HNYR1011)
    /// </summary>
    public partial class HNYR1011 : BasePgForm
    {
        #region 定数
        private const int prtCols = 31;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNYR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1"; //Uinfo.shishoCd;
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 年指定
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];

            // フォーカス設定
            this.txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 年始定、船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNYR1011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年指定の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                this.txtDateYear.Text = Util.ToString(IsValid.SetYear(this.txtDateYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdFr.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdTo.Text);
            }

            return true;
        }
        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba);
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba));
        }
        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNYR1011R rpt = new HNYR1011R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 日付範囲を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    "1", "1", this.Dba);
            // 表示日付設定
            string hyojiDate = this.lblDateGengo.Text + this.txtDateYear.Text + "年分";

            // 船主コード設定
            string FUNANUSHI_CD_FR;
            string FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            #region 支所コードの退避
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            #endregion
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            // han.VI_水揚仕切月計表(VI_HN_MIZUAGE_SKR_GEKKEIHYO)の日付範囲から発生しているデータを取得
            Sql.Append(" SELECT");
            if (shishoCd != "0")
            {
                Sql.Append(" SHISHO_CD,");
            }
            else
            {
                Sql.Append(" 0 AS SHISHO_CD,");
            }
            Sql.Append(" SENSHU_CD,");
            Sql.Append(" SENSHU_NM,");
            Sql.Append(" SUM(\"1GATSU_SURYO\") AS \"1GATSU_SURYO\",");
            Sql.Append(" SUM(\"2GATSU_SURYO\") AS \"2GATSU_SURYO\",");
            Sql.Append(" SUM(\"3GATSU_SURYO\") AS \"3GATSU_SURYO\",");
            Sql.Append(" SUM(\"4GATSU_SURYO\") AS \"4GATSU_SURYO\",");
            Sql.Append(" SUM(\"5GATSU_SURYO\") AS \"5GATSU_SURYO\",");
            Sql.Append(" SUM(\"6GATSU_SURYO\") AS \"6GATSU_SURYO\",");
            Sql.Append(" SUM(\"7GATSU_SURYO\") AS \"7GATSU_SURYO\",");
            Sql.Append(" SUM(\"8GATSU_SURYO\") AS \"8GATSU_SURYO\",");
            Sql.Append(" SUM(\"9GATSU_SURYO\") AS \"9GATSU_SURYO\",");
            Sql.Append(" SUM(\"10GATSU_SURYO\") AS \"10GATSU_SURYO\",");
            Sql.Append(" SUM(\"11GATSU_SURYO\") AS \"11GATSU_SURYO\",");
            Sql.Append(" SUM(\"12GATSU_SURYO\") AS \"12GATSU_SURYO\",");
            Sql.Append(" SUM(\"1GATSU_KINGAKU\") AS \"1GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"2GATSU_KINGAKU\") AS \"2GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"3GATSU_KINGAKU\") AS \"3GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"4GATSU_KINGAKU\") AS \"4GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"5GATSU_KINGAKU\") AS \"5GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"6GATSU_KINGAKU\") AS \"6GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"7GATSU_KINGAKU\") AS \"7GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"8GATSU_KINGAKU\") AS \"8GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"9GATSU_KINGAKU\") AS \"9GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"10GATSU_KINGAKU\") AS \"10GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"11GATSU_KINGAKU\") AS \"11GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"12GATSU_KINGAKU\") AS \"12GATSU_KINGAKU\",");
            Sql.Append(" SUM(\"1GATSU_SURYO\") + SUM(\"2GATSU_SURYO\") + SUM(\"3GATSU_SURYO\") +");
            Sql.Append(" SUM(\"4GATSU_SURYO\") + SUM(\"5GATSU_SURYO\") + SUM(\"6GATSU_SURYO\") +");
            Sql.Append(" SUM(\"7GATSU_SURYO\") + SUM(\"8GATSU_SURYO\") + SUM(\"9GATSU_SURYO\") +");
            Sql.Append(" SUM(\"10GATSU_SURYO\") + SUM(\"11GATSU_SURYO\") + SUM(\"12GATSU_SURYO\") AS GOKEI_SURYO,");
            Sql.Append(" SUM(\"1GATSU_KINGAKU\") + SUM(\"2GATSU_KINGAKU\") + SUM(\"3GATSU_KINGAKU\") +");
            Sql.Append(" SUM(\"4GATSU_KINGAKU\") + SUM(\"5GATSU_KINGAKU\") + SUM(\"6GATSU_KINGAKU\") +");
            Sql.Append(" SUM(\"7GATSU_KINGAKU\") + SUM(\"8GATSU_KINGAKU\") + SUM(\"9GATSU_KINGAKU\") +");
            Sql.Append(" SUM(\"10GATSU_KINGAKU\") + SUM(\"11GATSU_KINGAKU\") + SUM(\"12GATSU_KINGAKU\") AS GOKEI_KINGAKU,");
            Sql.Append(" SUM(NENKAN_TESURYO) AS NENKAN_TESURYO");
            Sql.Append(" FROM");
            Sql.Append(" VI_HN_KUMIAIIN_BT_MZAG_NNKIH");
            Sql.Append(" WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" SHUKEI_NEN = @SHUKEI_NEN AND");
            Sql.Append(" SENSHU_CD BETWEEN @SENSHU_CD_FR AND @SENSHU_CD_TO");
            Sql.Append(" GROUP BY");
            Sql.Append(" KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD,");
            Sql.Append(" SENSHU_CD, SENSHU_NM");
            Sql.Append(" ORDER BY");
            Sql.Append(" KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD,");
            Sql.Append(" SENSHU_CD");
            dpc.SetParam("@SHUKEI_NEN", SqlDbType.Decimal, 4, tmpDate.Year);
            dpc.SetParam("@SENSHU_CD_FR", SqlDbType.Decimal, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@SENSHU_CD_TO", SqlDbType.Decimal, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                    Sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojiDate); // 年指定
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主CD
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["1GATSU_SURYO"], 2)); // 1月数量
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["2GATSU_SURYO"], 2)); // 2月数量
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["3GATSU_SURYO"], 2)); // 3月数量
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["4GATSU_SURYO"], 2)); // 4月数量
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["5GATSU_SURYO"], 2)); // 5月数量
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["6GATSU_SURYO"], 2)); // 6月数量
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["7GATSU_SURYO"], 2)); // 7月数量
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["8GATSU_SURYO"], 2)); // 8月数量
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["9GATSU_SURYO"], 2)); // 9月数量
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["10GATSU_SURYO"], 2)); // 10月数量
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["11GATSU_SURYO"], 2)); // 11月数量
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["12GATSU_SURYO"], 2)); // 12月数量
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["1GATSU_KINGAKU"])); // 1月金額
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["2GATSU_KINGAKU"])); // 2月金額
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["3GATSU_KINGAKU"])); // 3月金額
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["4GATSU_KINGAKU"])); // 4月金額
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["5GATSU_KINGAKU"])); // 5月金額
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["6GATSU_KINGAKU"])); // 6月金額
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["7GATSU_KINGAKU"])); // 7月金額
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["8GATSU_KINGAKU"])); // 8月金額
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["9GATSU_KINGAKU"])); // 9月金額
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["10GATSU_KINGAKU"])); // 10月金額
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["11GATSU_KINGAKU"])); // 11月金額
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["12GATSU_KINGAKU"])); // 12月金額
                    dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["GOKEI_SURYO"], 2)); // 数量合計
                    dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["GOKEI_KINGAKU"])); // 金額合計
                    dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["NENKAN_TESURYO"])); // 年間手数料
                    dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHISHO_CD"]); // 船主CD

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion

                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;

        }
        #endregion
    }
}
