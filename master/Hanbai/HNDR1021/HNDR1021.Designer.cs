﻿namespace jp.co.fsi.hn.hndr1021
{
    partial class HNDR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblSeriDateDay = new System.Windows.Forms.Label();
			this.lblSeriDateMonth = new System.Windows.Forms.Label();
			this.lblSeriDateYear = new System.Windows.Forms.Label();
			this.txtSeriDateDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeriDateYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeriDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeriDateGengo = new System.Windows.Forms.Label();
			this.lblSeriDate = new System.Windows.Forms.Label();
			this.lblFunanushiCdTo = new System.Windows.Forms.Label();
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiCdFr = new System.Windows.Forms.Label();
			this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiharaiDateDay = new System.Windows.Forms.Label();
			this.lblShiharaiDateMonth = new System.Windows.Forms.Label();
			this.lblShiharaiDateYear = new System.Windows.Forms.Label();
			this.txtShiharaiDateDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShiharaiDateYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShiharaiDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiharaiDateGengo = new System.Windows.Forms.Label();
			this.lblShiharaiDate = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanKbnNm = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(9, 812);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "水揚仕切書出力";
			// 
			// lblSeriDateDay
			// 
			this.lblSeriDateDay.AutoSize = true;
			this.lblSeriDateDay.BackColor = System.Drawing.Color.Silver;
			this.lblSeriDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriDateDay.ForeColor = System.Drawing.Color.Black;
			this.lblSeriDateDay.Location = new System.Drawing.Point(407, 0);
			this.lblSeriDateDay.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSeriDateDay.MinimumSize = new System.Drawing.Size(32, 32);
			this.lblSeriDateDay.Name = "lblSeriDateDay";
			this.lblSeriDateDay.Size = new System.Drawing.Size(32, 32);
			this.lblSeriDateDay.TabIndex = 7;
			this.lblSeriDateDay.Tag = "CHANGE";
			this.lblSeriDateDay.Text = "日";
			this.lblSeriDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeriDateMonth
			// 
			this.lblSeriDateMonth.AutoSize = true;
			this.lblSeriDateMonth.BackColor = System.Drawing.Color.Silver;
			this.lblSeriDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriDateMonth.ForeColor = System.Drawing.Color.Black;
			this.lblSeriDateMonth.Location = new System.Drawing.Point(331, 0);
			this.lblSeriDateMonth.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSeriDateMonth.MinimumSize = new System.Drawing.Size(32, 32);
			this.lblSeriDateMonth.Name = "lblSeriDateMonth";
			this.lblSeriDateMonth.Size = new System.Drawing.Size(32, 32);
			this.lblSeriDateMonth.TabIndex = 5;
			this.lblSeriDateMonth.Tag = "CHANGE";
			this.lblSeriDateMonth.Text = "月";
			this.lblSeriDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeriDateYear
			// 
			this.lblSeriDateYear.AutoSize = true;
			this.lblSeriDateYear.BackColor = System.Drawing.Color.Silver;
			this.lblSeriDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriDateYear.ForeColor = System.Drawing.Color.Black;
			this.lblSeriDateYear.Location = new System.Drawing.Point(249, 0);
			this.lblSeriDateYear.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSeriDateYear.MinimumSize = new System.Drawing.Size(32, 32);
			this.lblSeriDateYear.Name = "lblSeriDateYear";
			this.lblSeriDateYear.Size = new System.Drawing.Size(32, 32);
			this.lblSeriDateYear.TabIndex = 3;
			this.lblSeriDateYear.Tag = "CHANGE";
			this.lblSeriDateYear.Text = "年";
			this.lblSeriDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeriDateDay
			// 
			this.txtSeriDateDay.AutoSizeFromLength = false;
			this.txtSeriDateDay.DisplayLength = null;
			this.txtSeriDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriDateDay.ForeColor = System.Drawing.Color.Black;
			this.txtSeriDateDay.Location = new System.Drawing.Point(359, 3);
			this.txtSeriDateDay.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeriDateDay.MaxLength = 2;
			this.txtSeriDateDay.Name = "txtSeriDateDay";
			this.txtSeriDateDay.Size = new System.Drawing.Size(39, 23);
			this.txtSeriDateDay.TabIndex = 4;
			this.txtSeriDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeriDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateDay_Validating);
			// 
			// txtSeriDateYear
			// 
			this.txtSeriDateYear.AutoSizeFromLength = false;
			this.txtSeriDateYear.DisplayLength = null;
			this.txtSeriDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriDateYear.ForeColor = System.Drawing.Color.Black;
			this.txtSeriDateYear.Location = new System.Drawing.Point(203, 3);
			this.txtSeriDateYear.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeriDateYear.MaxLength = 2;
			this.txtSeriDateYear.Name = "txtSeriDateYear";
			this.txtSeriDateYear.Size = new System.Drawing.Size(39, 23);
			this.txtSeriDateYear.TabIndex = 2;
			this.txtSeriDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeriDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateYear_Validating);
			// 
			// txtSeriDateMonth
			// 
			this.txtSeriDateMonth.AutoSizeFromLength = false;
			this.txtSeriDateMonth.DisplayLength = null;
			this.txtSeriDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriDateMonth.ForeColor = System.Drawing.Color.Black;
			this.txtSeriDateMonth.Location = new System.Drawing.Point(284, 3);
			this.txtSeriDateMonth.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeriDateMonth.MaxLength = 2;
			this.txtSeriDateMonth.Name = "txtSeriDateMonth";
			this.txtSeriDateMonth.Size = new System.Drawing.Size(39, 23);
			this.txtSeriDateMonth.TabIndex = 3;
			this.txtSeriDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeriDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateMonth_Validating);
			// 
			// lblSeriDateGengo
			// 
			this.lblSeriDateGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeriDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeriDateGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriDateGengo.ForeColor = System.Drawing.Color.Black;
			this.lblSeriDateGengo.Location = new System.Drawing.Point(144, 2);
			this.lblSeriDateGengo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSeriDateGengo.Name = "lblSeriDateGengo";
			this.lblSeriDateGengo.Size = new System.Drawing.Size(55, 24);
			this.lblSeriDateGengo.TabIndex = 1;
			this.lblSeriDateGengo.Tag = "DISPNAME";
			this.lblSeriDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeriDate
			// 
			this.lblSeriDate.BackColor = System.Drawing.Color.Silver;
			this.lblSeriDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeriDate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblSeriDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriDate.ForeColor = System.Drawing.Color.Black;
			this.lblSeriDate.Location = new System.Drawing.Point(0, 0);
			this.lblSeriDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSeriDate.Name = "lblSeriDate";
			this.lblSeriDate.Size = new System.Drawing.Size(895, 31);
			this.lblSeriDate.TabIndex = 1;
			this.lblSeriDate.Tag = "CHANGE";
			this.lblSeriDate.Text = "セリ日付";
			this.lblSeriDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiCdTo
			// 
			this.lblFunanushiCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
			this.lblFunanushiCdTo.Location = new System.Drawing.Point(580, 3);
			this.lblFunanushiCdTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
			this.lblFunanushiCdTo.Size = new System.Drawing.Size(283, 24);
			this.lblFunanushiCdTo.TabIndex = 4;
			this.lblFunanushiCdTo.Tag = "DISPNAME";
			this.lblFunanushiCdTo.Text = "最　後";
			this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBet.ForeColor = System.Drawing.Color.Black;
			this.lblCodeBet.Location = new System.Drawing.Point(493, 1);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 32);
			this.lblCodeBet.TabIndex = 2;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdFr
			// 
			this.txtFunanushiCdFr.AutoSizeFromLength = false;
			this.txtFunanushiCdFr.DisplayLength = null;
			this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
			this.txtFunanushiCdFr.Location = new System.Drawing.Point(143, 4);
			this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtFunanushiCdFr.MaxLength = 4;
			this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
			this.txtFunanushiCdFr.Size = new System.Drawing.Size(53, 23);
			this.txtFunanushiCdFr.TabIndex = 9;
			this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
			// 
			// lblFunanushiCdFr
			// 
			this.lblFunanushiCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
			this.lblFunanushiCdFr.Location = new System.Drawing.Point(200, 3);
			this.lblFunanushiCdFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
			this.lblFunanushiCdFr.Size = new System.Drawing.Size(283, 24);
			this.lblFunanushiCdFr.TabIndex = 1;
			this.lblFunanushiCdFr.Tag = "DISPNAME";
			this.lblFunanushiCdFr.Text = "先　頭";
			this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdTo
			// 
			this.txtFunanushiCdTo.AutoSizeFromLength = false;
			this.txtFunanushiCdTo.DisplayLength = null;
			this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
			this.txtFunanushiCdTo.Location = new System.Drawing.Point(523, 4);
			this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtFunanushiCdTo.MaxLength = 4;
			this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
			this.txtFunanushiCdTo.Size = new System.Drawing.Size(52, 23);
			this.txtFunanushiCdTo.TabIndex = 10;
			this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
			this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
			// 
			// lblShiharaiDateDay
			// 
			this.lblShiharaiDateDay.AutoSize = true;
			this.lblShiharaiDateDay.BackColor = System.Drawing.Color.Silver;
			this.lblShiharaiDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiharaiDateDay.ForeColor = System.Drawing.Color.Black;
			this.lblShiharaiDateDay.Location = new System.Drawing.Point(404, 0);
			this.lblShiharaiDateDay.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShiharaiDateDay.MinimumSize = new System.Drawing.Size(32, 32);
			this.lblShiharaiDateDay.Name = "lblShiharaiDateDay";
			this.lblShiharaiDateDay.Size = new System.Drawing.Size(32, 32);
			this.lblShiharaiDateDay.TabIndex = 7;
			this.lblShiharaiDateDay.Tag = "CHANGE";
			this.lblShiharaiDateDay.Text = "日";
			this.lblShiharaiDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShiharaiDateMonth
			// 
			this.lblShiharaiDateMonth.AutoSize = true;
			this.lblShiharaiDateMonth.BackColor = System.Drawing.Color.Silver;
			this.lblShiharaiDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiharaiDateMonth.ForeColor = System.Drawing.Color.Black;
			this.lblShiharaiDateMonth.Location = new System.Drawing.Point(328, 0);
			this.lblShiharaiDateMonth.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShiharaiDateMonth.MinimumSize = new System.Drawing.Size(32, 32);
			this.lblShiharaiDateMonth.Name = "lblShiharaiDateMonth";
			this.lblShiharaiDateMonth.Size = new System.Drawing.Size(32, 32);
			this.lblShiharaiDateMonth.TabIndex = 5;
			this.lblShiharaiDateMonth.Tag = "CHANGE";
			this.lblShiharaiDateMonth.Text = "月";
			this.lblShiharaiDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShiharaiDateYear
			// 
			this.lblShiharaiDateYear.AutoSize = true;
			this.lblShiharaiDateYear.BackColor = System.Drawing.Color.Silver;
			this.lblShiharaiDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiharaiDateYear.ForeColor = System.Drawing.Color.Black;
			this.lblShiharaiDateYear.Location = new System.Drawing.Point(249, 0);
			this.lblShiharaiDateYear.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShiharaiDateYear.MinimumSize = new System.Drawing.Size(32, 32);
			this.lblShiharaiDateYear.Name = "lblShiharaiDateYear";
			this.lblShiharaiDateYear.Size = new System.Drawing.Size(32, 32);
			this.lblShiharaiDateYear.TabIndex = 3;
			this.lblShiharaiDateYear.Tag = "CHANGE";
			this.lblShiharaiDateYear.Text = "年";
			this.lblShiharaiDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiharaiDateDay
			// 
			this.txtShiharaiDateDay.AutoSizeFromLength = false;
			this.txtShiharaiDateDay.DisplayLength = null;
			this.txtShiharaiDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiharaiDateDay.ForeColor = System.Drawing.Color.Black;
			this.txtShiharaiDateDay.Location = new System.Drawing.Point(359, 4);
			this.txtShiharaiDateDay.Margin = new System.Windows.Forms.Padding(5);
			this.txtShiharaiDateDay.MaxLength = 2;
			this.txtShiharaiDateDay.Name = "txtShiharaiDateDay";
			this.txtShiharaiDateDay.Size = new System.Drawing.Size(39, 23);
			this.txtShiharaiDateDay.TabIndex = 7;
			this.txtShiharaiDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiharaiDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiDateDay_Validating);
			// 
			// txtShiharaiDateYear
			// 
			this.txtShiharaiDateYear.AutoSizeFromLength = false;
			this.txtShiharaiDateYear.DisplayLength = null;
			this.txtShiharaiDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiharaiDateYear.ForeColor = System.Drawing.Color.Black;
			this.txtShiharaiDateYear.Location = new System.Drawing.Point(203, 4);
			this.txtShiharaiDateYear.Margin = new System.Windows.Forms.Padding(5);
			this.txtShiharaiDateYear.MaxLength = 2;
			this.txtShiharaiDateYear.Name = "txtShiharaiDateYear";
			this.txtShiharaiDateYear.Size = new System.Drawing.Size(39, 23);
			this.txtShiharaiDateYear.TabIndex = 5;
			this.txtShiharaiDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiharaiDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiDateYear_Validating);
			// 
			// txtShiharaiDateMonth
			// 
			this.txtShiharaiDateMonth.AutoSizeFromLength = false;
			this.txtShiharaiDateMonth.DisplayLength = null;
			this.txtShiharaiDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiharaiDateMonth.ForeColor = System.Drawing.Color.Black;
			this.txtShiharaiDateMonth.Location = new System.Drawing.Point(284, 4);
			this.txtShiharaiDateMonth.Margin = new System.Windows.Forms.Padding(5);
			this.txtShiharaiDateMonth.MaxLength = 2;
			this.txtShiharaiDateMonth.Name = "txtShiharaiDateMonth";
			this.txtShiharaiDateMonth.Size = new System.Drawing.Size(39, 23);
			this.txtShiharaiDateMonth.TabIndex = 6;
			this.txtShiharaiDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiharaiDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiDateMonth_Validating);
			// 
			// lblShiharaiDateGengo
			// 
			this.lblShiharaiDateGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblShiharaiDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShiharaiDateGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiharaiDateGengo.ForeColor = System.Drawing.Color.Black;
			this.lblShiharaiDateGengo.Location = new System.Drawing.Point(144, 3);
			this.lblShiharaiDateGengo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShiharaiDateGengo.Name = "lblShiharaiDateGengo";
			this.lblShiharaiDateGengo.Size = new System.Drawing.Size(55, 24);
			this.lblShiharaiDateGengo.TabIndex = 1;
			this.lblShiharaiDateGengo.Tag = "DISPNAME";
			this.lblShiharaiDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShiharaiDate
			// 
			this.lblShiharaiDate.BackColor = System.Drawing.Color.Silver;
			this.lblShiharaiDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShiharaiDate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShiharaiDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiharaiDate.ForeColor = System.Drawing.Color.Black;
			this.lblShiharaiDate.Location = new System.Drawing.Point(0, 0);
			this.lblShiharaiDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShiharaiDate.Name = "lblShiharaiDate";
			this.lblShiharaiDate.Size = new System.Drawing.Size(895, 31);
			this.lblShiharaiDate.TabIndex = 1;
			this.lblShiharaiDate.Tag = "CHANGE";
			this.lblShiharaiDate.Text = "支払予定日";
			this.lblShiharaiDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(144, 4);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(53, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(201, 2);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(895, 31);
			this.lblMizuageShisho.TabIndex = 0;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "水揚支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanKbn
			// 
			this.txtSeisanKbn.AutoSizeFromLength = true;
			this.txtSeisanKbn.DisplayLength = null;
			this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSeisanKbn.Location = new System.Drawing.Point(144, 4);
			this.txtSeisanKbn.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeisanKbn.MaxLength = 4;
			this.txtSeisanKbn.Name = "txtSeisanKbn";
			this.txtSeisanKbn.Size = new System.Drawing.Size(53, 23);
			this.txtSeisanKbn.TabIndex = 8;
			this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
			// 
			// lblSeisanKbnNm
			// 
			this.lblSeisanKbnNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanKbnNm.Location = new System.Drawing.Point(201, 2);
			this.lblSeisanKbnNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
			this.lblSeisanKbnNm.Size = new System.Drawing.Size(283, 24);
			this.lblSeisanKbnNm.TabIndex = 4;
			this.lblSeisanKbnNm.Tag = "DISPNAME";
			this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(895, 31);
			this.label1.TabIndex = 4;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "精算区分";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Silver;
			this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label5.Location = new System.Drawing.Point(0, 0);
			this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(895, 34);
			this.label5.TabIndex = 4;
			this.label5.Tag = "CHANGE";
			this.label5.Text = "船主CD範囲";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 5;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(905, 204);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.txtFunanushiCdFr);
			this.fsiPanel5.Controls.Add(this.txtFunanushiCdTo);
			this.fsiPanel5.Controls.Add(this.lblFunanushiCdTo);
			this.fsiPanel5.Controls.Add(this.lblFunanushiCdFr);
			this.fsiPanel5.Controls.Add(this.lblCodeBet);
			this.fsiPanel5.Controls.Add(this.label5);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(5, 165);
			this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(895, 34);
			this.fsiPanel5.TabIndex = 4;
			this.fsiPanel5.Tag = "CHANGE";
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtSeisanKbn);
			this.fsiPanel4.Controls.Add(this.lblSeisanKbnNm);
			this.fsiPanel4.Controls.Add(this.label1);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(5, 125);
			this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(895, 31);
			this.fsiPanel4.TabIndex = 3;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblShiharaiDateGengo);
			this.fsiPanel3.Controls.Add(this.txtShiharaiDateMonth);
			this.fsiPanel3.Controls.Add(this.txtShiharaiDateYear);
			this.fsiPanel3.Controls.Add(this.txtShiharaiDateDay);
			this.fsiPanel3.Controls.Add(this.lblShiharaiDateYear);
			this.fsiPanel3.Controls.Add(this.lblShiharaiDateMonth);
			this.fsiPanel3.Controls.Add(this.lblShiharaiDateDay);
			this.fsiPanel3.Controls.Add(this.lblShiharaiDate);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 85);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(895, 31);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblSeriDateGengo);
			this.fsiPanel2.Controls.Add(this.txtSeriDateMonth);
			this.fsiPanel2.Controls.Add(this.lblSeriDateDay);
			this.fsiPanel2.Controls.Add(this.txtSeriDateYear);
			this.fsiPanel2.Controls.Add(this.lblSeriDateMonth);
			this.fsiPanel2.Controls.Add(this.txtSeriDateDay);
			this.fsiPanel2.Controls.Add(this.lblSeriDateYear);
			this.fsiPanel2.Controls.Add(this.lblSeriDate);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 45);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(895, 31);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(895, 31);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNDR1021
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNDR1021";
			this.Text = "";
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateYear;
        private System.Windows.Forms.Label lblSeriDateGengo;
        private System.Windows.Forms.Label lblSeriDate;
        private System.Windows.Forms.Label lblSeriDateDay;
        private System.Windows.Forms.Label lblSeriDateMonth;
        private System.Windows.Forms.Label lblSeriDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateDay;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateMonth;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblShiharaiDateDay;
        private System.Windows.Forms.Label lblShiharaiDateMonth;
        private System.Windows.Forms.Label lblShiharaiDateYear;
        private common.controls.FsiTextBox txtShiharaiDateDay;
        private common.controls.FsiTextBox txtShiharaiDateYear;
        private common.controls.FsiTextBox txtShiharaiDateMonth;
        private System.Windows.Forms.Label lblShiharaiDateGengo;
        private System.Windows.Forms.Label lblShiharaiDate;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label lblSeisanKbnNm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}