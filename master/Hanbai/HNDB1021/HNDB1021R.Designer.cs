﻿namespace jp.co.fsi.hn.hndb1021
{
    /// <summary>
    /// HNDB1021R の概要の説明です。
    /// </summary>
    partial class HNDB1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNDB1021R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtPrintDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFunanushiCD = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblFunanishiNM = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKouzaBango = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKubun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFurikaeGaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTekiyo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKisanbi = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShurui = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtSenshuCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSenshuNM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txKouzaBango = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKubun = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFurikaegaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTekiyo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKisanbi = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShurui = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtNinzu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblMei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGoukeiFurikaegaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFunanushiCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFunanishiNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKouzaBango)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFurikaeGaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTekiyo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKisanbi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShurui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txKouzaBango)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFurikaegaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTekiyo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKisanbi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShurui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNinzu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoukeiFurikaegaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtPrintDate,
            this.txtPage,
            this.lblPage,
            this.lblFunanushiCD,
            this.line1,
            this.lblFunanishiNM,
            this.lblKouzaBango,
            this.lblKubun,
            this.lblFurikaeGaku,
            this.lblTekiyo,
            this.lblKisanbi,
            this.lblShurui,
            this.lblMessage,
            this.label3});
            this.pageHeader.Height = 0.8912402F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtPrintDate
            // 
            this.txtPrintDate.CanGrow = false;
            this.txtPrintDate.DataField = "today";
            this.txtPrintDate.Height = 0.2F;
            this.txtPrintDate.Left = 9.335828F;
            this.txtPrintDate.MultiLine = false;
            this.txtPrintDate.Name = "txtPrintDate";
            this.txtPrintDate.OutputFormat = resources.GetString("txtPrintDate.OutputFormat");
            this.txtPrintDate.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle; d" +
    "do-char-set: 1";
            this.txtPrintDate.Text = "YYYY/MM/DD";
            this.txtPrintDate.Top = 0.3129921F;
            this.txtPrintDate.Width = 0.8535433F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.2F;
            this.txtPage.Left = 10.33701F;
            this.txtPage.MultiLine = false;
            this.txtPage.Name = "txtPage";
            this.txtPage.OutputFormat = resources.GetString("txtPage.OutputFormat");
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle; d" +
    "do-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "ZZ9";
            this.txtPage.Top = 0.3129921F;
            this.txtPage.Width = 0.3334641F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1688976F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.67047F;
            this.lblPage.MultiLine = false;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.3129921F;
            this.lblPage.Width = 0.1562996F;
            // 
            // lblFunanushiCD
            // 
            this.lblFunanushiCD.Height = 0.1665354F;
            this.lblFunanushiCD.HyperLink = null;
            this.lblFunanushiCD.Left = 0.2362205F;
            this.lblFunanushiCD.MultiLine = false;
            this.lblFunanushiCD.Name = "lblFunanushiCD";
            this.lblFunanushiCD.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: right; vert" +
    "ical-align: bottom; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap";
            this.lblFunanushiCD.Text = "船主CD";
            this.lblFunanushiCD.Top = 0.6795276F;
            this.lblFunanushiCD.Width = 0.4992126F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 3F;
            this.line1.Name = "line1";
            this.line1.Top = 0.888189F;
            this.line1.Width = 10.82677F;
            this.line1.X1 = 0F;
            this.line1.X2 = 10.82677F;
            this.line1.Y1 = 0.888189F;
            this.line1.Y2 = 0.888189F;
            // 
            // lblFunanishiNM
            // 
            this.lblFunanishiNM.Height = 0.1665354F;
            this.lblFunanishiNM.HyperLink = null;
            this.lblFunanishiNM.Left = 0.5574803F;
            this.lblFunanishiNM.MultiLine = false;
            this.lblFunanishiNM.Name = "lblFunanishiNM";
            this.lblFunanishiNM.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap" +
    "";
            this.lblFunanishiNM.Text = "船主名";
            this.lblFunanishiNM.Top = 0.6795276F;
            this.lblFunanishiNM.Width = 1.249213F;
            // 
            // lblKouzaBango
            // 
            this.lblKouzaBango.Height = 0.1665354F;
            this.lblKouzaBango.HyperLink = null;
            this.lblKouzaBango.Left = 2.547638F;
            this.lblKouzaBango.MultiLine = false;
            this.lblKouzaBango.Name = "lblKouzaBango";
            this.lblKouzaBango.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: right; vert" +
    "ical-align: bottom; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap";
            this.lblKouzaBango.Text = "口座番号";
            this.lblKouzaBango.Top = 0.6795276F;
            this.lblKouzaBango.Width = 0.65625F;
            // 
            // lblKubun
            // 
            this.lblKubun.Height = 0.1665354F;
            this.lblKubun.HyperLink = null;
            this.lblKubun.Left = 3.512992F;
            this.lblKubun.MultiLine = false;
            this.lblKubun.Name = "lblKubun";
            this.lblKubun.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: left; verti" +
    "cal-align: bottom; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap";
            this.lblKubun.Text = "区 分";
            this.lblKubun.Top = 0.6795276F;
            this.lblKubun.Width = 0.4255903F;
            // 
            // lblFurikaeGaku
            // 
            this.lblFurikaeGaku.Height = 0.1665354F;
            this.lblFurikaeGaku.HyperLink = null;
            this.lblFurikaeGaku.Left = 4.448819F;
            this.lblFurikaeGaku.MultiLine = false;
            this.lblFurikaeGaku.Name = "lblFurikaeGaku";
            this.lblFurikaeGaku.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap" +
    "";
            this.lblFurikaeGaku.Text = "振替額";
            this.lblFurikaeGaku.Top = 0.6795276F;
            this.lblFurikaeGaku.Width = 0.802362F;
            // 
            // lblTekiyo
            // 
            this.lblTekiyo.Height = 0.1665354F;
            this.lblTekiyo.HyperLink = null;
            this.lblTekiyo.Left = 5.116142F;
            this.lblTekiyo.MultiLine = false;
            this.lblTekiyo.Name = "lblTekiyo";
            this.lblTekiyo.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap" +
    "";
            this.lblTekiyo.Text = "摘   要";
            this.lblTekiyo.Top = 0.6795276F;
            this.lblTekiyo.Width = 0.9586616F;
            // 
            // lblKisanbi
            // 
            this.lblKisanbi.Height = 0.1665354F;
            this.lblKisanbi.HyperLink = null;
            this.lblKisanbi.Left = 6.027559F;
            this.lblKisanbi.MultiLine = false;
            this.lblKisanbi.Name = "lblKisanbi";
            this.lblKisanbi.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap" +
    "";
            this.lblKisanbi.Text = "起算日";
            this.lblKisanbi.Top = 0.6795276F;
            this.lblKisanbi.Width = 1.269685F;
            // 
            // lblShurui
            // 
            this.lblShurui.Height = 0.1665354F;
            this.lblShurui.HyperLink = null;
            this.lblShurui.Left = 7.927559F;
            this.lblShurui.MultiLine = false;
            this.lblShurui.Name = "lblShurui";
            this.lblShurui.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap" +
    "";
            this.lblShurui.Text = "種   類";
            this.lblShurui.Top = 0.6795276F;
            this.lblShurui.Width = 0.8114171F;
            // 
            // lblMessage
            // 
            this.lblMessage.Height = 0.1665354F;
            this.lblMessage.HyperLink = null;
            this.lblMessage.Left = 9.055119F;
            this.lblMessage.MultiLine = false;
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: left; verti" +
    "cal-align: bottom; white-space: nowrap; ddo-char-set: 1; ddo-wrap-mode: nowrap";
            this.lblMessage.Text = "メッセージ";
            this.lblMessage.Top = 0.6795276F;
            this.lblMessage.Width = 1.771654F;
            // 
            // label3
            // 
            this.label3.Height = 0.3129921F;
            this.label3.HyperLink = null;
            this.label3.Left = 0F;
            this.label3.MultiLine = false;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold; text-align: center; verti" +
    "cal-align: middle; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.label3.Text = "＊＊＊　信用自動振込一覧表　＊＊＊";
            this.label3.Top = 0F;
            this.label3.Width = 10.82677F;
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSenshuCD,
            this.txtSenshuNM,
            this.txKouzaBango,
            this.txtKubun,
            this.txtFurikaegaku,
            this.txtTekiyo,
            this.txtKisanbi,
            this.txtShurui});
            this.detail.Height = 0.2362205F;
            this.detail.Name = "detail";
            // 
            // txtSenshuCD
            // 
            this.txtSenshuCD.CanGrow = false;
            this.txtSenshuCD.DataField = "ITEM01";
            this.txtSenshuCD.Height = 0.2F;
            this.txtSenshuCD.Left = 0.2362205F;
            this.txtSenshuCD.MultiLine = false;
            this.txtSenshuCD.Name = "txtSenshuCD";
            this.txtSenshuCD.OutputFormat = resources.GetString("txtSenshuCD.OutputFormat");
            this.txtSenshuCD.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSenshuCD.Text = "ZZZZZ9";
            this.txtSenshuCD.Top = 0.01968504F;
            this.txtSenshuCD.Width = 0.4480315F;
            // 
            // txtSenshuNM
            // 
            this.txtSenshuNM.CanGrow = false;
            this.txtSenshuNM.DataField = "ITEM02";
            this.txtSenshuNM.Height = 0.2F;
            this.txtSenshuNM.Left = 0.9507875F;
            this.txtSenshuNM.MultiLine = false;
            this.txtSenshuNM.Name = "txtSenshuNM";
            this.txtSenshuNM.OutputFormat = resources.GetString("txtSenshuNM.OutputFormat");
            this.txtSenshuNM.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtSenshuNM.Text = "ＮＮＮＮＮＮＮＮＮ";
            this.txtSenshuNM.Top = 0.01968504F;
            this.txtSenshuNM.Width = 1.300788F;
            // 
            // txKouzaBango
            // 
            this.txKouzaBango.CanGrow = false;
            this.txKouzaBango.DataField = "ITEM03";
            this.txKouzaBango.Height = 0.2F;
            this.txKouzaBango.Left = 2.543307F;
            this.txKouzaBango.MultiLine = false;
            this.txKouzaBango.Name = "txKouzaBango";
            this.txKouzaBango.OutputFormat = resources.GetString("txKouzaBango.OutputFormat");
            this.txKouzaBango.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txKouzaBango.Text = "9999999";
            this.txKouzaBango.Top = 0.01968504F;
            this.txKouzaBango.Width = 0.6251967F;
            // 
            // txtKubun
            // 
            this.txtKubun.CanGrow = false;
            this.txtKubun.DataField = "ITEM04";
            this.txtKubun.Height = 0.2F;
            this.txtKubun.Left = 3.512992F;
            this.txtKubun.MultiLine = false;
            this.txtKubun.Name = "txtKubun";
            this.txtKubun.OutputFormat = resources.GetString("txtKubun.OutputFormat");
            this.txtKubun.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtKubun.Text = "ＮＮ";
            this.txtKubun.Top = 0.01968504F;
            this.txtKubun.Width = 0.3736219F;
            // 
            // txtFurikaegaku
            // 
            this.txtFurikaegaku.CanGrow = false;
            this.txtFurikaegaku.DataField = "ITEM05";
            this.txtFurikaegaku.Height = 0.2F;
            this.txtFurikaegaku.Left = 4.251969F;
            this.txtFurikaegaku.MultiLine = false;
            this.txtFurikaegaku.Name = "txtFurikaegaku";
            this.txtFurikaegaku.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtFurikaegaku.Text = "ZZZ,ZZZ,ZZ9";
            this.txtFurikaegaku.Top = 0.01968504F;
            this.txtFurikaegaku.Width = 0.8023625F;
            // 
            // txtTekiyo
            // 
            this.txtTekiyo.CanGrow = false;
            this.txtTekiyo.DataField = "ITEM06";
            this.txtTekiyo.Height = 0.2F;
            this.txtTekiyo.Left = 5.340551F;
            this.txtTekiyo.MultiLine = false;
            this.txtTekiyo.Name = "txtTekiyo";
            this.txtTekiyo.OutputFormat = resources.GetString("txtTekiyo.OutputFormat");
            this.txtTekiyo.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtTekiyo.Text = "999999999999";
            this.txtTekiyo.Top = 0.01968504F;
            this.txtTekiyo.Width = 0.9586614F;
            // 
            // txtKisanbi
            // 
            this.txtKisanbi.CanGrow = false;
            this.txtKisanbi.DataField = "ITEM07";
            this.txtKisanbi.Height = 0.2F;
            this.txtKisanbi.Left = 6.430315F;
            this.txtKisanbi.MultiLine = false;
            this.txtKisanbi.Name = "txtKisanbi";
            this.txtKisanbi.OutputFormat = resources.GetString("txtKisanbi.OutputFormat");
            this.txtKisanbi.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtKisanbi.Text = "ＮＮＮＮＮＮＮＮ";
            this.txtKisanbi.Top = 0.01968504F;
            this.txtKisanbi.Width = 1.269685F;
            // 
            // txtShurui
            // 
            this.txtShurui.CanGrow = false;
            this.txtShurui.DataField = "ITEM08";
            this.txtShurui.Height = 0.2F;
            this.txtShurui.Left = 7.939371F;
            this.txtShurui.MultiLine = false;
            this.txtShurui.Name = "txtShurui";
            this.txtShurui.OutputFormat = resources.GetString("txtShurui.OutputFormat");
            this.txtShurui.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: center; vertical-align: middle";
            this.txtShurui.Text = "ＮＮＮＮＮ";
            this.txtShurui.Top = 0.01968504F;
            this.txtShurui.Width = 0.8114173F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.CanGrow = false;
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label2,
            this.txtNinzu,
            this.lblMei,
            this.txtGoukeiFurikaegaku});
            this.reportFooter1.Height = 0.284252F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // label2
            // 
            this.label2.Height = 0.1665354F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.8464568F;
            this.label2.MultiLine = false;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; white-space: nowrap; d" +
    "do-wrap-mode: nowrap";
            this.label2.Text = "＊＊＊ 合 計 ＊＊＊";
            this.label2.Top = 0.07874016F;
            this.label2.Width = 1.530315F;
            // 
            // txtNinzu
            // 
            this.txtNinzu.CanGrow = false;
            this.txtNinzu.DataField = "ITEM03";
            this.txtNinzu.Height = 0.1665354F;
            this.txtNinzu.Left = 2.596457F;
            this.txtNinzu.MultiLine = false;
            this.txtNinzu.Name = "txtNinzu";
            this.txtNinzu.OutputFormat = resources.GetString("txtNinzu.OutputFormat");
            this.txtNinzu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtNinzu.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtNinzu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtNinzu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtNinzu.Text = "Z,ZZ9";
            this.txtNinzu.Top = 0.07874016F;
            this.txtNinzu.Width = 0.396063F;
            // 
            // lblMei
            // 
            this.lblMei.Height = 0.1665354F;
            this.lblMei.HyperLink = null;
            this.lblMei.Left = 2.99252F;
            this.lblMei.MultiLine = false;
            this.lblMei.Name = "lblMei";
            this.lblMei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; white-space: nowrap; dd" +
    "o-wrap-mode: nowrap";
            this.lblMei.Text = "名";
            this.lblMei.Top = 0.07874016F;
            this.lblMei.Width = 0.1759844F;
            // 
            // txtGoukeiFurikaegaku
            // 
            this.txtGoukeiFurikaegaku.CanGrow = false;
            this.txtGoukeiFurikaegaku.DataField = "ITEM05";
            this.txtGoukeiFurikaegaku.Height = 0.2F;
            this.txtGoukeiFurikaegaku.Left = 3.960237F;
            this.txtGoukeiFurikaegaku.MultiLine = false;
            this.txtGoukeiFurikaegaku.Name = "txtGoukeiFurikaegaku";
            this.txtGoukeiFurikaegaku.OutputFormat = resources.GetString("txtGoukeiFurikaegaku.OutputFormat");
            this.txtGoukeiFurikaegaku.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGoukeiFurikaegaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGoukeiFurikaegaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGoukeiFurikaegaku.Text = "ZZZ,ZZZ,ZZZ,ZZ9";
            this.txtGoukeiFurikaegaku.Top = 0.07874016F;
            this.txtGoukeiFurikaegaku.Width = 1.094095F;
            // 
            // HNDB1021R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937007F;
            this.PageSettings.Margins.Left = 1.338583F;
            this.PageSettings.Margins.Right = 2.165354F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 10.82677F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            this.DataInitialize += new System.EventHandler(this.HNDB1021R_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFunanushiCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFunanishiNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKouzaBango)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFurikaeGaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTekiyo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKisanbi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShurui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txKouzaBango)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFurikaegaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTekiyo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKisanbi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShurui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNinzu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoukeiFurikaegaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrintDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFunanushiCD;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFunanishiNM;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKouzaBango;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKubun;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFurikaeGaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTekiyo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKisanbi;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShurui;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMessage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSenshuCD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSenshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txKouzaBango;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKubun;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFurikaegaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTekiyo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKisanbi;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShurui;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNinzu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGoukeiFurikaegaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
    }
}
