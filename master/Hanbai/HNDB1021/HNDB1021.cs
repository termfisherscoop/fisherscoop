﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hndb1021
{
    /// <summary>
    /// 信用連帯(HNDB1021)
    /// </summary>
    public partial class HNDB1021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 文字コード(SJIS)
        /// </summary>
        private const int CHR_CD_SJIS = 932;

        /// <summary>
        /// 文字コード(日本語カナEBCDIC)
        /// </summary>
        private const int CHR_CD_EBCDIC = 20290;

        /// <summary>
        /// 作成区分
        /// </summary>
        private const int FUTSU_MATOME = 99;
        private const int FUTSU_FURIKOMI = 1;
        private const int TSUMITATE_FURIKOMI = 2;
        private const int AZUKARI_FURIKOMI = 3;
        private const int HONIN_TSUMITATE_FURIKOMI = 4;

        /// <summary>
        /// 出力ファイルのレコード長
        /// </summary>
        private const int RECODE_LENGTH = 256;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNDB1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 日付項目すべてシステム日付を表示する
            string[] arrJpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
            this.lblGengo.Text = arrJpDate[0];
            this.txtFurikomibiYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];

            // 先頭項目にフォーカス
            this.txtDateYearFr.Focus();
            this.txtDateYearFr.SelectAll();

            string toPath = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNDB1021", "Setting", "WorkDir");
            lbl保存先.Text = "ファイル保存先は【" + toPath + "】です。"+Environment.NewLine + "HNDB1021 WorkDir";

        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtFurikomibiYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                SetJpDateFr(arrJpDate);
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblGengoTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                SetJpDateTo(arrJpDate);
                            }
                        }
                    }
                    break;

                case "txtFurikomibiYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblGengo.Text,
                                        this.txtFurikomibiYear.Text,
                                        this.txtMonth.Text,
                                        this.txtDay.Text,
                                        this.Dba);
                                SetJpDateTo(arrJpDate);
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目入力チェック
            if (!ValidateAll())
            {
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目入力チェック
            if (!ValidateAll())
            {
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // FD作成処理
            OutputText();
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNDB1021R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 日付範囲(自)・年の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckJpDateFr();
                SetJpDateFr();
            }
        }

        /// <summary>
        /// 日付範囲(自)・月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckJpDateFr();
                SetJpDateFr();
            }
        }

        /// <summary>
        /// 日付範囲(自)・日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckJpDateFr();
                SetJpDateFr();
            }
        }

        /// <summary>
        /// 日付範囲(至)・年の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckJpDateTo();
                SetJpDateTo();
            }
        }

        /// <summary>
        /// 日付範囲(至)・月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckJpDateTo();
                SetJpDateTo();
            }
        }

        /// <summary>
        /// 日付範囲(至)・日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckJpDateTo();
                SetJpDateTo();
            }
        }

        /// <summary>
        /// 摘要の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyo(false))
            {
                e.Cancel = true;
                this.txtTekiyo.SelectAll();
            }
        }

        /// <summary>
        /// 振込日・年の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtFurikomibiYear.Text, this.txtFurikomibiYear.MaxLength))
            {
                e.Cancel = true;
                this.txtFurikomibiYear.SelectAll();
            }
            else
            {
                this.txtFurikomibiYear.Text = Util.ToString(IsValid.SetYear(this.txtFurikomibiYear.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 振込日・月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtMonth.SelectAll();
            }
            else
            {
                this.txtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtMonth.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 振込日・日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDay.SelectAll();
            }
            else
            {
                this.txtDay.Text = Util.ToString(IsValid.SetDay(this.txtDay.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 普通振込のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoFutsu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // FD作成処理
                OutputText();
            }
        }

        /// <summary>
        /// 積立振込のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoTsumitate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // FD作成処理
                OutputText();
            }
        }

        /// <summary>
        /// 預り金振込のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoAzukarikin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // FD作成処理
                OutputText();
            }
        }

        /// <summary>
        /// 本人積立振込のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoHonninTsumitate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // FD作成処理
                OutputText();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }


        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 摘要の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTekiyo(bool doRequiredChk)
        {
            // 必須チェック(引数がtrueの場合のみ)
            if (doRequiredChk)
            {
                if (ValChk.IsEmpty(this.txtTekiyo.Text))
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }

            // 半角チェック
            if (!ValChk.IsHalfChar(this.txtTekiyo.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 桁数チェック
            if (!ValChk.IsWithinLength(this.txtTekiyo.Text, 12))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(振込日)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtFurikomibiYear.Text,
                this.txtMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDay.Text) > lastDayInMonth)
            {
                this.txtDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(振込日)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblGengo.Text, this.txtFurikomibiYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日の月末入力チェック処理
            CheckJpDateFr();
            // 年月日の正しい和暦への変換処理
            SetJpDateFr();

            // 日付範囲(至)
            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日の月末入力チェック処理
            CheckJpDateTo();
            // 年月日の正しい和暦への変換処理
            SetJpDateTo();

            // 摘要
            if (!IsValidTekiyo(true))
            {
                this.txtTekiyo.Focus();
                this.txtTekiyo.SelectAll();
                return false;
            }

            // 振込日
            // 年
            if (!IsValid.IsYear(this.txtFurikomibiYear.Text, this.txtFurikomibiYear.MaxLength))
            {
                this.txtFurikomibiYear.Focus();
                this.txtFurikomibiYear.SelectAll();
                return false;
            }
            // 月
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                this.txtMonth.Focus();
                this.txtMonth.SelectAll();
                return false;
            }
            // 日
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                this.txtDay.Focus();
                this.txtDay.SelectAll();
                return false;
            }
            // 年月日の月末入力チェック処理
            CheckJpDate();
            // 年月日の正しい和暦への変換処理
            SetJpDate();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtFurikomibiYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            // 日付項目を西暦に変換
            DateTime dateFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime dateTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime furikomiBi = Util.ConvAdDate(this.lblGengo.Text, this.txtFurikomibiYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba);
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;

            // データの存在チェック
            bool dtCheck = this.GetExistCheckData(dateFr, dateTo, Util.ToDecimal(shishoCd));
            if (!dtCheck)
            {
                Msg.Info("該当データがありません。");
                return;
            }

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // ワークからデータを削除
                this.Dba.Delete("WT_HN_SHINYO_DATA", null, null);

                // ワークにデータを登録
                InsertShinyouWk(dateFr, dateTo, furikomiBi, Util.ToDecimal(shishoCd));

                // 帳票出力用データを取得
                DataTable dtOutput = this.GetPrintData();

                // 印刷用ワークにデータを登録
                MakeWkData(dtOutput);

                // 取得列の定義
                StringBuilder cols = new StringBuilder();
                cols.Append("  ITEM01"); // 船主コード
                cols.Append(" ,ITEM02"); // 船主名称
                cols.Append(" ,ITEM03"); // 口座番号
                cols.Append(" ,ITEM04"); // 区分
                cols.Append(" ,ITEM05"); // 振替額
                cols.Append(" ,ITEM06"); // 概要
                cols.Append(" ,ITEM07"); // 起算日
                cols.Append(" ,ITEM08"); // 種類（口座種類）

                // バインドパラメータの設定
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                // データの取得
                dtOutput = this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);


                // データ存在チェック
                if (0 == dtOutput.Rows.Count)
                {
                    Msg.Info("該当データがありません。");
                    return;
                }

                HNDB1021R rpt = new HNDB1021R(dtOutput);

                if (isPreview)
                {
                    // プレビュー画面表示
                    PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                    pFrm.WindowState = FormWindowState.Maximized;
                    pFrm.Show();
                }
                else
                {
                    // 直接印刷
                    rpt.Run(false);
                    rpt.Document.Print(true, true, false);
                }

            }
            catch (Exception e)
            {
                // ロールバック
                //this.Dba.Rollback();

                Msg.Notice(e.Message);

                // 処理を中止
                Msg.Error("出力時にエラーが発生しました。処理を中止します。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

        }

        /// <summary>
        /// テキスト出力をする
        /// </summary>
        private void OutputText()
        {
            // 全項目入力チェック
            if (!ValidateAll())
            {
                return;
            }

            // 日付項目を西暦に変換
            DateTime dateFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime dateTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime furikomiBi = Util.ConvAdDate(this.lblGengo.Text, this.txtFurikomibiYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba);

            decimal shishoCd = Util.ToDecimal( (this.txtMizuageShishoCd.Text == "") ? "0" : this.txtMizuageShishoCd.Text);

            // データの存在チェック
            bool dtCheck = this.GetExistCheckData(dateFr, dateTo, shishoCd);
            if (!dtCheck)
            {
                Msg.Info("該当データがありません。");
                return;
            }

			// 確認メッセージ「信用データをテキスト出力します。よろしいですか？」
			//            if (Msg.ConfOKCancel("信用データをワークエリアに書き出します。よろしいですか？") == DialogResult.Cancel)
			//            {
			//                return;
			//            }

			// OKならファイル保存ダイアログを表示
			//            SaveFileDialog sfd = new SaveFileDialog();
			/*            sfd.InitialDirectory = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNDB1021", "Setting", "SaveDir");
						sfd.FileName = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNDB1021", "Setting", "DefFileNm");
						sfd.Filter = @"すべて(*.*)|*.*";
						sfd.FilterIndex = 1;
						sfd.RestoreDirectory = true;
						sfd.Title = @"信用連帯データの保存";
						sfd.OverwritePrompt = true;*/

			//            string fromPath = string.Empty;
			//string fromPath = @"E:\"; 

			string chkPath = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNDB1021", "Setting", "SaveDir");
			string fromPath = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNDB1021", "Setting", "SaveDir") +
                this.Config.LoadPgConfig(Constants.SubSys.Han, "HNDB1021", "Setting", "DefFileNm");

			// フォルダ存在チェック
			// 無い場合は作成する。
			if (!Directory.Exists(chkPath))
			{
				Directory.CreateDirectory(chkPath);
			}

            //            if (sfd.ShowDialog() == DialogResult.OK)
            //            {
            //                fromPath += sfd.FileName;
            //                fromPath += sfd.FileName;
            //            }
            //           else
            //            {
            //                return;
            //            }

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // ワークからデータを削除
                this.Dba.Delete("WT_HN_SHINYO_DATA", null, null);

                // ワークにデータを登録
                int insertRecs = InsertShinyouWk(dateFr, dateTo, furikomiBi, shishoCd);

                // コミット
                this.Dba.Commit();
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);

                // 処理を中止
                Msg.Error("出力時にエラーが発生しました。処理を中止します。");
				this.Dba.Rollback();
			}
			finally
            {
                // ロールバック
				// 最後にロールバック？？？？？
				// 例外エラーでのロールバックはわかるけど、、、、、、
				// ??ロールバックしたらメモリ開放されると思ってる？？？？
				// .Net わかってて書いてる？？？？
				// やばい、、、、、、
//                this.Dba.Rollback();
            }

            // ワークから出力データを取得
            DataTable dtOutput = GetOutputData();

            if (0 == dtOutput.Rows.Count)
            {
                Msg.Info("該当データがありません。");
                return;
            }

            // テキスト出力 （config.xmlの「FileEnc」が2ならEBCDIC、それ以外は Shift JIS
            Encoding enc;
            if (this.Config.LoadPgConfig(Constants.SubSys.Han, "HNDB1021", "Setting", "FileEnc").Equals("2"))
            {
                enc = Encoding.GetEncoding(CHR_CD_EBCDIC);
            }
            else
            {
                enc = Encoding.GetEncoding(CHR_CD_SJIS);
            }
            StreamWriter writer =
                new StreamWriter(fromPath, false, enc);
            writer.Write(GetFileContent(dtOutput));
            writer.Close();

            // 出力後、ファイルをコピー
            string fName = Path.GetFileName(fromPath);
			chkPath = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNDB1021", "Setting", "WorkDir");

            string toPath = Path.Combine(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNDB1021", "Setting", "WorkDir"), fName);

			// フォルダが無ければ作成する。
			if (!Directory.Exists(chkPath))
			{
				Directory.CreateDirectory(chkPath);
			}

            // コピー元とコピー先が同じディレクトリの場合は、コピーしない（ファイルの排他エラーで落ちる）
            if (!fromPath.Equals(toPath))
            {
                File.Copy(fromPath, toPath, true);
            }

            // 処理完了メッセージ
            Msg.Info("信用データをワークエリアに書き出しました。");
        }

        /// <summary>
        /// 存在チェックのデータを取得します。
        /// </summary>
        /// <param name="dateFr">日付(自)</param>
        /// <param name="dateTo">日付(至)</param>
        /// <returns>true=データ有り、false＝データ無し</returns>
        private bool GetExistCheckData(DateTime dateFr, DateTime dateTo, Decimal shishoCd)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  COUNT(*) ");
            sql.Append("FROM ");
            sql.Append("  VI_HN_FURIKAE_OYOBI_SHUKKIN ");
            sql.Append("WHERE ");
            sql.Append("  KAISHA_CD = @KAISHA_CD ");
            if (this.txtMizuageShishoCd.Text != "0")
                sql.Append(" AND SHISHO_CD = @SHISHO_CD ");
            sql.Append("  AND (SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO) ");
            sql.Append("  AND ( ");
            sql.Append("    JIMOTO_HANBAI_MIBARAIKIN - JIMOTO_TESURYO <> 0 ");
            sql.Append("    OR JIMOTOKOJO_KOMOKU4 <> 0 ");
            sql.Append("    OR JIMOTOKOJO_KOMOKU5 <> 0 ");
            sql.Append("    OR JIMOTOKOJO_KOMOKU6 <> 0 ");
            sql.Append("  ) ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SEISAN_BI_FR", SqlDbType.DateTime, dateFr);
            dpc.SetParam("@SEISAN_BI_TO", SqlDbType.DateTime, dateTo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            if (Util.ToInt(dtResult.Rows[0].ItemArray[0]) == 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 信用データワークに登録します。
        /// </summary>
        /// <param name="dateFr">日付範囲(自)</param>
        /// <param name="dateTo">日付範囲(至)</param>
        /// <param name="furikomiBi">振込日</param>
        /// <returns>登録件数</returns>
        private int InsertShinyouWk(DateTime dateFr, DateTime dateTo, DateTime furikomiBi, Decimal shishoCd)
        {
            StringBuilder sql = new StringBuilder();
 
            sql.AppendLine("INSERT INTO WT_HN_SHINYO_DATA ( ");
            sql.AppendLine("SEISAN_BANGO, ");
            sql.AppendLine("SENSHU_CD, ");
            sql.AppendLine("SEISAN_KUBUN, ");
            sql.AppendLine("KOZA_SHURUI, ");
            sql.AppendLine("KAIKEI_NENDO, ");
            sql.AppendLine("KOZA_BANGO, ");
            sql.AppendLine("FURIKAEGAKU, ");
            sql.AppendLine("TORIHIKIBI, ");
            sql.AppendLine("KANA_GAIYO, ");
            sql.AppendLine("SEISHIKI_SENSHU_NM ");
            if (this.getSakuseiKubun() == 99)
            {
                sql.AppendLine(",SENSHU_KANA_NM ");

            }
            sql.AppendLine(") ");
            sql.AppendLine("SELECT ");
            sql.AppendLine("  A.SEISAN_BANGO, ");
            sql.AppendLine("  A.SENSHU_CD, ");
            sql.AppendLine("  A.SEISAN_KUBUN, ");
            sql.AppendLine("  A.KOZA_SHURUI, ");
            sql.AppendLine("  A.KAIKEI_NENDO, ");
            sql.AppendLine("  A.KOZA_BANGO, ");
            sql.AppendLine("  A.FURIKAEGAKU, ");
            sql.AppendLine("  @TORIHIKIBI, ");
            sql.AppendLine("  @KANA_GAIYO, ");
            sql.AppendLine("  A.SENSHU_NM ");
            if (this.getSakuseiKubun() == 99)
            {
                sql.AppendLine(",A.DATA_KBN ");

            }
            sql.AppendLine("FROM ");

            // 作成区分に対応するView名を設定する
            switch (this.getSakuseiKubun())
            {
                //TODO 20181205 SJ
                case FUTSU_MATOME:
                    sql.AppendLine(" VI_HN_SHINYO_DATA_MATOME AS A ");  //VI_信用データ振込まとめ
                    break;
                case FUTSU_FURIKOMI: // 普通振込
                    sql.AppendLine("  VI_HN_SHINYO_DATA_FUTSU AS A "); // VI_信用データ普通
                    break;
                case TSUMITATE_FURIKOMI: // 積立振込
                    sql.AppendLine("  VI_HN_SHINYO_DATA_TSUMITATE AS A "); // VI_信用データ積立
                    break;
                case AZUKARI_FURIKOMI: // 預り金振込
                    sql.AppendLine("  VI_HN_SHINYO_DATA_AZUKARIKIN AS A "); // VI_信用データ預り金
                    break;
                case HONIN_TSUMITATE_FURIKOMI: // 本人積立振込
                    sql.AppendLine("  VI_HN_SHINYO_DATA_HONIN_TMTT AS A "); // VI_信用データ本人積立
                    break;
                default: // その他
                    sql.AppendLine("  VI_HN_SHINYO_DATA_FUTSU AS A "); // VI_信用データ普通
                    break;
            }

            sql.AppendLine("WHERE ");
            sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != 0)
                sql.AppendLine(" A.SHISHO_CD = @SHISHO_CD AND ");
            sql.AppendLine(" A.SEISAN_BI Between @SEISAN_BI_FR AND @SEISAN_BI_TO "); // 会計年度は使わない

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKIBI", SqlDbType.DateTime, furikomiBi);
            dpc.SetParam("@KANA_GAIYO", SqlDbType.Char, 12, this.txtTekiyo.Text);
            dpc.SetParam("@SEISAN_BI_FR", SqlDbType.DateTime, dateFr);
            dpc.SetParam("@SEISAN_BI_TO", SqlDbType.DateTime, dateTo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            return this.Dba.ModifyBySql(sql.ToString(), dpc);
        }

        /// <summary>
        /// 帳票出力用データを取得します。
        /// </summary>
        /// <returns>取得したデータ</returns>
        private DataTable GetPrintData()
        {
            int kozaShurui = this.getSakuseiKubun();

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  MIN(SEISAN_BANGO) AS SEISAN_BANGO ");
            sql.Append(" ,SENSHU_CD ");
            sql.Append(" ,SEISAN_KUBUN ");
            sql.Append(" ,KOZA_SHURUI ");
            sql.Append(" ,KOZA_BANGO ");
            sql.Append(" ,SUM(FURIKAEGAKU) AS FURIKAEGAKU ");
            sql.Append(" ,TORIHIKIBI ");
            sql.Append(" ,KANA_GAIYO ");
            sql.Append(" ,SEISHIKI_SENSHU_NM ");
            sql.Append(" ,SENSHU_KANA_NM ");
            sql.Append("FROM ");
            sql.Append("  WT_HN_SHINYO_DATA ");

            if (kozaShurui.ToString() != "99")
            {
                sql.Append("WHERE ");

                sql.Append("  KOZA_SHURUI = " + kozaShurui.ToString() + " ");

            }

            sql.Append("GROUP BY ");
            sql.Append("  SENSHU_CD ");
            sql.Append(" ,SEISAN_KUBUN ");
            sql.Append(" ,KOZA_SHURUI ");
            sql.Append(" ,KOZA_BANGO ");
            sql.Append(" ,TORIHIKIBI ");
            sql.Append(" ,KANA_GAIYO ");
            sql.Append(" ,SEISHIKI_SENSHU_NM,SENSHU_KANA_NM ");

            sql.Append(" ORDER BY ");
            sql.Append("  SENSHU_CD,KOZA_SHURUI,SENSHU_KANA_NM ");


            DataTable dtResult = this.Dba.GetDataTableFromSql(sql.ToString());

            return dtResult;
        }

        /// <summary>
        /// 出力のデータを取得します。
        /// </summary>
        /// <returns>取得したデータ</returns>
        private DataTable GetOutputData()
        {
            // 作成区分
            int sakuseiKubun = this.getSakuseiKubun();

            StringBuilder sql = new StringBuilder();

            // TODO 2018-12-06 帳票はサマリされ。データ出力ではサマリなしで生成されているのを
            // 帳票とファイルの中身を一致させた。
            #region 修正 2018-12-06 SJ
            /*            sql.Append("SELECT ");
                        sql.Append("  SEISAN_BANGO ");
                        sql.Append(" ,SENSHU_CD ");
                        sql.Append(" ,SEISAN_KUBUN ");
                        sql.Append(" ,KOZA_SHURUI ");
                        sql.Append(" ,KOZA_BANGO ");
                        sql.Append(" ,FURIKAEGAKU ");
                        sql.Append(" ,TORIHIKIBI ");
                        sql.Append(" ,KANA_GAIYO ");
                        sql.Append(" ,SEISHIKI_SENSHU_NM ");
                        sql.Append("FROM ");
                        sql.Append("  WT_HN_SHINYO_DATA ");
                        if (sakuseiKubun.ToString() != "99")
                        {
                            sql.Append("WHERE ");

                            sql.Append("  KOZA_SHURUI = " + sakuseiKubun.ToString() + " ");

                        }

                        sql.Append("ORDER BY ");
                        sql.Append(" SENSHU_CD,KOZA_SHURUI ");*/
            #endregion
            sql.Append("SELECT ");
            sql.Append("  MIN(SEISAN_BANGO) AS SEISAN_BANGO ");
            sql.Append(" ,SENSHU_CD ");
            sql.Append(" ,SEISAN_KUBUN ");
            sql.Append(" ,KOZA_SHURUI ");
            sql.Append(" ,KOZA_BANGO ");
            sql.Append(" ,SUM(FURIKAEGAKU) AS FURIKAEGAKU ");
            sql.Append(" ,TORIHIKIBI ");
            sql.Append(" ,KANA_GAIYO ");
            sql.Append(" ,SEISHIKI_SENSHU_NM ");
            sql.Append(" ,SENSHU_KANA_NM ");
            sql.Append("FROM ");
            sql.Append("  WT_HN_SHINYO_DATA ");

            if (sakuseiKubun.ToString() != "99")
            {
                sql.Append("WHERE ");

                sql.Append("  KOZA_SHURUI = " + sakuseiKubun.ToString() + " ");

            }

            sql.Append("GROUP BY ");
            sql.Append("  SENSHU_CD ");
            sql.Append(" ,SEISAN_KUBUN ");
            sql.Append(" ,KOZA_SHURUI ");
            sql.Append(" ,KOZA_BANGO ");
            sql.Append(" ,TORIHIKIBI ");
            sql.Append(" ,KANA_GAIYO ");
            sql.Append(" ,SEISHIKI_SENSHU_NM,SENSHU_KANA_NM ");

            sql.Append(" ORDER BY ");
            sql.Append("  SENSHU_CD,KOZA_SHURUI,SENSHU_KANA_NM ");


            DataTable dtResult = this.Dba.GetDataTableFromSql(sql.ToString());

            return dtResult;
        }

        /// <summary>
        /// テキスト出力する中身のデータを取得します。
        /// </summary>
        /// <param name="dtTargetData">出力対象のデータ</param>
        /// <returns>ファイルの中身</returns>
        private string GetFileContent(DataTable dtTargetData)
        {
            StringBuilder sbRec = new StringBuilder();
            StringBuilder sbResult = new StringBuilder();

            // ヘッダーレコードの書き出し

            // 格納件数(4桁)
            sbRec.Append(dtTargetData.Rows.Count.ToString("0000"));

            // データ格納最終レコードNO(4桁)
            sbRec.Append((dtTargetData.Rows.Count + 1).ToString("0000"));

            // 伝送処理済最終レコードNO(4桁:0001固定)
            sbRec.Append("0001");

            // データ格納最大連番
            // A(1桁:F固定)
            sbRec.Append("F");
            // B(4桁)
            sbRec.Append(dtTargetData.Rows.Count.ToString("0000"));

            // 伝送処理済最大連番
            // A(1桁:F固定)
            sbRec.Append("F");
            // B(4桁:0000固定)
            sbRec.Append("0000");

            // 残りのスペース設定
            int fillerSpaceLength = RECODE_LENGTH - sbRec.Length;
            sbRec.Append(new String((char)32, fillerSpaceLength));

            sbResult.Append(Util.ToString(sbRec));


            // 明細レコードの書き出し
            for (int i = 0; i < dtTargetData.Rows.Count; i++)
            {
                sbRec = new StringBuilder();

                // ※注：普通預金の入金前提で作っている。当座預金や出金が発生するなら変更が必要
                // コントロール情報
                // ファイル情報(1桁:0x01固定)
                sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] { 1 }));

                // データ長(1桁:0x4D固定 ※恐らく普通預金と当座預金の入金処理しか発生しない前提。)
                //sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] {77}));
                // TODO:読み取りエラーが発生する。元のプログラムに合わせる。ひょっとして必要な情報の桁数?
                sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] { 40 }));

                // データ部Ⅰ
                // メッセージID(1桁:6固定)
                sbRec.Append("6");

                // 科目(2桁:※普通預金は07)
                sbRec.Append("07");

                // 区分(2桁:※入金は01)
                sbRec.Append("01");

                // 取引コード(3桁:※入金振替は960)
                sbRec.Append("960");

                // オペレータID(2桁:スペース固定)
                sbRec.Append(new String((char)32, 2));

                // 役席区分(1桁:0固定)
                sbRec.Append("0");

                // 媒体情報(1桁:0固定)
                sbRec.Append("0");

                // モード情報(1桁:0x00固定)
                sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] { 0 }));

                // 完了/連続区分(1桁:0固定)
                sbRec.Append("0");

                // ＭＳ情報(1桁:0固定)
                sbRec.Append("0");

                // 端末ネット種別(1桁:0固定)
                sbRec.Append("0");

                // ＭＳ内ＲＣ(2桁:00固定)
                sbRec.Append("00");

                // 画面番号(4桁:※普通貯金入金は1702)
                sbRec.Append("1702");

                // 電文区分(1桁:0固定)
                sbRec.Append("0");

                // 端末内取引連番(4桁:スペース固定)
                sbRec.Append(new String((char)32, 4));

                // ボイド区分(1桁:0固定)
                sbRec.Append("0");

                // 閉局区分(1桁:0固定)
                sbRec.Append("0");

                // スペース(3桁)
                sbRec.Append(new String((char)32, 3));

                // データ部Ⅱ
                // ※普通or当座の入金しか発生しない前提にしているが、違ったらレイアウトそのものを条件分岐する必要あり
                // ＵＳ(1桁:0x1F固定)
                sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] { 31 }));

                // ＵＳ(1桁:0x1F固定)
                sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] { 31 }));

                // 取引日(6桁：和暦)
                sbRec.Append(this.txtFurikomibiYear.Text.Trim().PadLeft(2, '0')
                    + this.txtMonth.Text.Trim().PadLeft(2, '0')
                    + this.txtDay.Text.Trim().PadLeft(2, '0'));

                // ＵＳ(1桁:0x1F固定)
                sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] { 31 }));

                // 口座番号(7桁) ※通常7桁のはずだが、そうでなければ7桁に補正
                sbRec.Append(Util.ToString(dtTargetData.Rows[i]["KOZA_BANGO"]).Trim().PadLeft(7, '0'));

                // ＵＳ(1桁:0x1F固定)
                sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] { 31 }));

                // 入金金額(0づめ11桁)
                sbRec.Append(Util.ToDecimal(dtTargetData.Rows[i]["FURIKAEGAKU"]).ToString("00000000000"));

                // ＵＳ(1桁:0x1F固定)
                sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] { 31 }));

                // 摘要コード(3桁:スペース固定)
                sbRec.Append(new String((char)32, 3));

                // ＵＳ(1桁:0x1F固定)
                sbRec.Append(System.Text.Encoding.ASCII.GetString(new byte[] { 31 }));

                // カナ摘要(12桁) ※足りない分スペースを後ろに埋める
                sbRec.Append(Util.ToString(dtTargetData.Rows[i]["KANA_GAIYO"]));

                // スペース(177桁) ※普通・当座入金前提
                fillerSpaceLength = RECODE_LENGTH - sbRec.Length;
                sbRec.Append(new String((char)32, fillerSpaceLength));

                sbResult.Append(Util.ToString(sbRec));
            }

            return Util.ToString(sbResult);
        }

        /// <summary>
        /// 印刷ワークのデータを作成します。
        /// </summary>
        /// <param name="dtPrintData">帳票出力のためのデータ</param>
        private bool MakeWkData(DataTable dtPrintData)
        {
            string kozaShuruiNm = "";
            DbParamCollection dpc = new DbParamCollection();

            for (int i = 0; i < dtPrintData.Rows.Count; i++)
            {
                dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.Int, (i + 1));
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtPrintData.Rows[i]["SENSHU_CD"]);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtPrintData.Rows[i]["SEISHIKI_SENSHU_NM"]);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, Util.ToInt(dtPrintData.Rows[i]["KOZA_BANGO"]).ToString("D7"));
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "入金"); // 区分
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.ToDecimal(dtPrintData.Rows[i]["FURIKAEGAKU"]).ToString("#,##0"));
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtPrintData.Rows[i]["KANA_GAIYO"]);
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200,
                    this.lblGengo.Text + this.txtFurikomibiYear.Text.Trim().PadLeft(2, '0') + "年"
                    + this.txtMonth.Text.Trim().PadLeft(2, '0') + "月"
                    + this.txtDay.Text.Trim().PadLeft(2, '0') + "日");
                // 種類
                switch (Util.ToInt(dtPrintData.Rows[i]["KOZA_SHURUI"]))
                {
                    case FUTSU_MATOME: // 振込まとめ TODO 20181205 SJ
                        kozaShuruiNm = "振込まとめ";
                        break;
                    case FUTSU_FURIKOMI:
                        kozaShuruiNm = "普通"; //普通振込
                        break;
                    case TSUMITATE_FURIKOMI: // 積立振込
                        kozaShuruiNm = "積立";
                        break;
                    case AZUKARI_FURIKOMI: // 預り金振込
                        kozaShuruiNm = "預り金";
                        break;
                    case HONIN_TSUMITATE_FURIKOMI: // 本人積立振込
                        kozaShuruiNm = "本人積立金";
                        break;
                    default:
                        kozaShuruiNm = ""; // その他は空欄
                        break;
                }
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, kozaShuruiNm);

                this.Dba.Insert("PR_HN_TBL", dpc);
            }

            return true;
        }

        /// <summary>
        /// 作成区分
        /// </summary>
        /// <returns></returns>
        private int getSakuseiKubun()
        {
            // 作成区分が「振込」の場合は99を返す
            if (this.rdoFutsuMATOME.Checked)
            {
                return FUTSU_MATOME;
            }

            // 作成区分が「普通振込」の場合は1を返す
            if (this.rdoFutsu.Checked)
            {
                return FUTSU_FURIKOMI;
            }

            // 作成区分が「積立振込」の場合は2を返す
            if (this.rdoTsumitate.Checked)
            {
                return TSUMITATE_FURIKOMI;
            }

            // 作成区分が「預り金振込」の場合は3を返す
            if (this.rdoAzukarikin.Checked)
            {
                return AZUKARI_FURIKOMI;
            }

            // 作成区分が「本人積立振込」の場合は4を返す
            if (this.rdoHonninTsumitate.Checked)
            {
                return HONIN_TSUMITATE_FURIKOMI;
            }

            // 作成区分が上記以外なら0を返す。
            return 0;
        }
        #endregion
    }
}
