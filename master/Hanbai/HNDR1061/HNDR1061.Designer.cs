﻿namespace jp.co.fsi.hn.hndr1061
{
    partial class HNDR1061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblDateGengo = new System.Windows.Forms.Label();
			this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.labelDateYear = new System.Windows.Forms.Label();
			this.lblDateMonth = new System.Windows.Forms.Label();
			this.lblDateDay = new System.Windows.Forms.Label();
			this.lblFunanushiCdTo = new System.Windows.Forms.Label();
			this.lblFunanushiCdFr = new System.Windows.Forms.Label();
			this.lblFunanushiCdBet = new System.Windows.Forms.Label();
			this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(9, 376);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.Text = "";
			// 
			// lblDateGengo
			// 
			this.lblDateGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengo.Location = new System.Drawing.Point(144, 2);
			this.lblDateGengo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengo.Name = "lblDateGengo";
			this.lblDateGengo.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengo.TabIndex = 1;
			this.lblDateGengo.Tag = "DISPNAME";
			this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateMonth
			// 
			this.txtDateMonth.AutoSizeFromLength = false;
			this.txtDateMonth.DisplayLength = null;
			this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonth.Location = new System.Drawing.Point(275, 3);
			this.txtDateMonth.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateMonth.MaxLength = 2;
			this.txtDateMonth.Name = "txtDateMonth";
			this.txtDateMonth.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonth.TabIndex = 3;
			this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
			// 
			// txtDateYear
			// 
			this.txtDateYear.AutoSizeFromLength = false;
			this.txtDateYear.DisplayLength = null;
			this.txtDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYear.Location = new System.Drawing.Point(207, 3);
			this.txtDateYear.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYear.MaxLength = 2;
			this.txtDateYear.Name = "txtDateYear";
			this.txtDateYear.Size = new System.Drawing.Size(39, 23);
			this.txtDateYear.TabIndex = 2;
			this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
			// 
			// txtDateDay
			// 
			this.txtDateDay.AutoSizeFromLength = false;
			this.txtDateDay.DisplayLength = null;
			this.txtDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateDay.Location = new System.Drawing.Point(349, 3);
			this.txtDateDay.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateDay.MaxLength = 2;
			this.txtDateDay.Name = "txtDateDay";
			this.txtDateDay.Size = new System.Drawing.Size(39, 23);
			this.txtDateDay.TabIndex = 4;
			this.txtDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDay_Validating);
			// 
			// labelDateYear
			// 
			this.labelDateYear.AutoSize = true;
			this.labelDateYear.BackColor = System.Drawing.Color.Silver;
			this.labelDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.labelDateYear.Location = new System.Drawing.Point(248, 7);
			this.labelDateYear.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.labelDateYear.Name = "labelDateYear";
			this.labelDateYear.Size = new System.Drawing.Size(24, 16);
			this.labelDateYear.TabIndex = 3;
			this.labelDateYear.Tag = "CHANGE";
			this.labelDateYear.Text = "年";
			this.labelDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateMonth
			// 
			this.lblDateMonth.AutoSize = true;
			this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateMonth.Location = new System.Drawing.Point(317, 7);
			this.lblDateMonth.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateMonth.Name = "lblDateMonth";
			this.lblDateMonth.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonth.TabIndex = 5;
			this.lblDateMonth.Tag = "CHANGE";
			this.lblDateMonth.Text = "月";
			this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateDay
			// 
			this.lblDateDay.AutoSize = true;
			this.lblDateDay.BackColor = System.Drawing.Color.Silver;
			this.lblDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateDay.Location = new System.Drawing.Point(392, 7);
			this.lblDateDay.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateDay.Name = "lblDateDay";
			this.lblDateDay.Size = new System.Drawing.Size(24, 16);
			this.lblDateDay.TabIndex = 7;
			this.lblDateDay.Tag = "CHANGE";
			this.lblDateDay.Text = "日";
			this.lblDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiCdTo
			// 
			this.lblFunanushiCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdTo.Location = new System.Drawing.Point(563, 2);
			this.lblFunanushiCdTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
			this.lblFunanushiCdTo.Size = new System.Drawing.Size(248, 24);
			this.lblFunanushiCdTo.TabIndex = 13;
			this.lblFunanushiCdTo.Tag = "DISPNAME";
			this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiCdFr
			// 
			this.lblFunanushiCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdFr.Location = new System.Drawing.Point(209, 2);
			this.lblFunanushiCdFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
			this.lblFunanushiCdFr.Size = new System.Drawing.Size(248, 24);
			this.lblFunanushiCdFr.TabIndex = 10;
			this.lblFunanushiCdFr.Tag = "DISPNAME";
			this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiCdBet
			// 
			this.lblFunanushiCdBet.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushiCdBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdBet.Location = new System.Drawing.Point(465, -1);
			this.lblFunanushiCdBet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblFunanushiCdBet.Name = "lblFunanushiCdBet";
			this.lblFunanushiCdBet.Size = new System.Drawing.Size(23, 32);
			this.lblFunanushiCdBet.TabIndex = 11;
			this.lblFunanushiCdBet.Tag = "CHANGE";
			this.lblFunanushiCdBet.Text = "～";
			this.lblFunanushiCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdTo
			// 
			this.txtFunanushiCdTo.AutoSizeFromLength = false;
			this.txtFunanushiCdTo.DisplayLength = null;
			this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdTo.Location = new System.Drawing.Point(495, 3);
			this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtFunanushiCdTo.MaxLength = 4;
			this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
			this.txtFunanushiCdTo.Size = new System.Drawing.Size(64, 23);
			this.txtFunanushiCdTo.TabIndex = 6;
			this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
			this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
			// 
			// txtFunanushiCdFr
			// 
			this.txtFunanushiCdFr.AutoSizeFromLength = false;
			this.txtFunanushiCdFr.DisplayLength = null;
			this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdFr.Location = new System.Drawing.Point(144, 3);
			this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtFunanushiCdFr.MaxLength = 4;
			this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
			this.txtFunanushiCdFr.Size = new System.Drawing.Size(64, 23);
			this.txtFunanushiCdFr.TabIndex = 5;
			this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(145, 4);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(192, 3);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label1.MinimumSize = new System.Drawing.Size(0, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(886, 32);
			this.label1.TabIndex = 10;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "水揚支所";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label2.MinimumSize = new System.Drawing.Size(0, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(886, 32);
			this.label2.TabIndex = 10;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "日付範囲";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label3.MinimumSize = new System.Drawing.Size(0, 32);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(886, 33);
			this.label3.TabIndex = 10;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "船主CD範囲";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 43);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.4F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(896, 125);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(886, 32);
			this.fsiPanel1.TabIndex = 903;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtFunanushiCdFr);
			this.fsiPanel3.Controls.Add(this.lblFunanushiCdTo);
			this.fsiPanel3.Controls.Add(this.txtFunanushiCdTo);
			this.fsiPanel3.Controls.Add(this.lblFunanushiCdFr);
			this.fsiPanel3.Controls.Add(this.lblFunanushiCdBet);
			this.fsiPanel3.Controls.Add(this.label3);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 87);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(886, 33);
			this.fsiPanel3.TabIndex = 905;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblDateGengo);
			this.fsiPanel2.Controls.Add(this.txtDateMonth);
			this.fsiPanel2.Controls.Add(this.txtDateYear);
			this.fsiPanel2.Controls.Add(this.txtDateDay);
			this.fsiPanel2.Controls.Add(this.lblDateDay);
			this.fsiPanel2.Controls.Add(this.labelDateYear);
			this.fsiPanel2.Controls.Add(this.lblDateMonth);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 46);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(886, 32);
			this.fsiPanel2.TabIndex = 904;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// HNDR1061
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 309);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNDR1061";
			this.Par1 = "12";
			this.Text = "";
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblDateGengo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDay;
        private System.Windows.Forms.Label labelDateYear;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label lblDateDay;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblFunanushiCdBet;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
    }
}