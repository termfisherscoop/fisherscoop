﻿namespace jp.co.fsi.hn.hndr1061
{
    /// <summary>
    /// SectionReport1 の概要の説明です。
    /// </summary>
    partial class HNDR1061R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNDR1061R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle05 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle06 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle07 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblTotalTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGenkin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue09Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFurikomi = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue08Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue10Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGenkin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFurikomi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtCompanyName,
            this.txtTitleName,
            this.line1,
            this.lblDate,
            this.txtDate,
            this.lblTitle01,
            this.lblTitle02,
            this.lblTitle03,
            this.lblTitle04,
            this.lblTitle05,
            this.lblTitle06,
            this.lblTitle07,
            this.line2,
            this.label1,
            this.textBox1,
            this.textBox2});
            this.pageHeader.Height = 1.208333F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.1968504F;
            this.txtToday.Left = 8.138583F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtToday.Text = "yyyy/MM/dd";
            this.txtToday.Top = 0.3598425F;
            this.txtToday.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 9.707481F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.3598425F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 9.391733F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtPageCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.3598425F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.2070866F;
            this.txtCompanyName.Left = 0F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 128";
            this.txtCompanyName.Text = null;
            this.txtCompanyName.Top = 0F;
            this.txtCompanyName.Width = 3.659055F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 3.87874F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle";
            this.txtTitleName.Text = "出金チェックリスト";
            this.txtTitleName.Top = 0.07244095F;
            this.txtTitleName.Width = 2.833465F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 4.212598F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.3598426F;
            this.line1.Width = 2.204725F;
            this.line1.X1 = 4.212598F;
            this.line1.X2 = 6.417323F;
            this.line1.Y1 = 0.3598426F;
            this.line1.Y2 = 0.3598426F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1968504F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 0F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.lblDate.Text = "日付範囲　：　";
            this.lblDate.Top = 0.556693F;
            this.lblDate.Width = 0.9614174F;
            // 
            // txtDate
            // 
            this.txtDate.DataField = "ITEM02";
            this.txtDate.Height = 0.1968504F;
            this.txtDate.Left = 0.9614174F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 128";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.556693F;
            this.txtDate.Width = 1.531102F;
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.1968504F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 0.3157481F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.lblTitle01.Text = "伝票番号";
            this.lblTitle01.Top = 0.9618111F;
            this.lblTitle01.Width = 0.8259843F;
            // 
            // lblTitle02
            // 
            this.lblTitle02.Height = 0.1968504F;
            this.lblTitle02.HyperLink = null;
            this.lblTitle02.Left = 1.314567F;
            this.lblTitle02.Name = "lblTitle02";
            this.lblTitle02.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.lblTitle02.Text = "出 金 日";
            this.lblTitle02.Top = 0.9618111F;
            this.lblTitle02.Width = 0.9614174F;
            // 
            // lblTitle03
            // 
            this.lblTitle03.Height = 0.1968504F;
            this.lblTitle03.HyperLink = null;
            this.lblTitle03.Left = 2.49252F;
            this.lblTitle03.Name = "lblTitle03";
            this.lblTitle03.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ver" +
    "tical-align: middle; ddo-char-set: 128";
            this.lblTitle03.Text = "船主CD";
            this.lblTitle03.Top = 0.9618111F;
            this.lblTitle03.Width = 0.9614174F;
            // 
            // lblTitle04
            // 
            this.lblTitle04.Height = 0.1968504F;
            this.lblTitle04.HyperLink = null;
            this.lblTitle04.Left = 3.733071F;
            this.lblTitle04.Name = "lblTitle04";
            this.lblTitle04.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.lblTitle04.Text = "船　主　氏　名";
            this.lblTitle04.Top = 0.9618111F;
            this.lblTitle04.Width = 1.211417F;
            // 
            // lblTitle05
            // 
            this.lblTitle05.Height = 0.1968504F;
            this.lblTitle05.HyperLink = null;
            this.lblTitle05.Left = 6.137402F;
            this.lblTitle05.Name = "lblTitle05";
            this.lblTitle05.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: middle; ddo-char-set: 128";
            this.lblTitle05.Text = "出金区分";
            this.lblTitle05.Top = 0.9618111F;
            this.lblTitle05.Width = 0.9614174F;
            // 
            // lblTitle06
            // 
            this.lblTitle06.Height = 0.1968504F;
            this.lblTitle06.HyperLink = null;
            this.lblTitle06.Left = 7.45315F;
            this.lblTitle06.Name = "lblTitle06";
            this.lblTitle06.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ver" +
    "tical-align: middle; ddo-char-set: 128";
            this.lblTitle06.Text = "出金金額";
            this.lblTitle06.Top = 0.9618111F;
            this.lblTitle06.Width = 0.8051186F;
            // 
            // lblTitle07
            // 
            this.lblTitle07.Height = 0.1968504F;
            this.lblTitle07.HyperLink = null;
            this.lblTitle07.Left = 8.562205F;
            this.lblTitle07.Name = "lblTitle07";
            this.lblTitle07.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.lblTitle07.Text = "摘　　　要";
            this.lblTitle07.Top = 0.9618111F;
            this.lblTitle07.Width = 0.9614174F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 1.178347F;
            this.line2.Width = 10.11417F;
            this.line2.X1 = 0F;
            this.line2.X2 = 10.11417F;
            this.line2.Y1 = 1.178347F;
            this.line2.Y2 = 1.178347F;
            // 
            // label1
            // 
            this.label1.Height = 0.1968504F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.label1.Text = "支　所：";
            this.label1.Top = 0.2070866F;
            this.label1.Width = 0.5968505F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM12";
            this.textBox1.Height = 0.1968504F;
            this.textBox1.Left = 0.5968505F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 128";
            this.textBox1.Text = "9999";
            this.textBox1.Top = 0.2070866F;
            this.textBox1.Width = 0.364567F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM13";
            this.textBox2.Height = 0.1968504F;
            this.textBox2.Left = 0.9614174F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 128";
            this.textBox2.Text = null;
            this.textBox2.Top = 0.2070866F;
            this.textBox2.Width = 2.624803F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM12";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue01,
            this.txtValue02,
            this.txtValue03,
            this.txtValue04,
            this.txtValue05,
            this.txtValue07,
            this.txtValue08,
            this.txtValue09,
            this.txtValue06});
            this.detail.Height = 0.2083333F;
            this.detail.Name = "detail";
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM03";
            this.txtValue01.Height = 0.1968504F;
            this.txtValue01.Left = 0.3157481F;
            this.txtValue01.MultiLine = false;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0F;
            this.txtValue01.Width = 0.8259844F;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM04";
            this.txtValue02.Height = 0.1968504F;
            this.txtValue02.Left = 1.314567F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0F;
            this.txtValue02.Width = 0.9614173F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM05";
            this.txtValue03.Height = 0.1968504F;
            this.txtValue03.Left = 2.49252F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue03.Text = null;
            this.txtValue03.Top = 0F;
            this.txtValue03.Width = 0.9614174F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM06";
            this.txtValue04.Height = 0.1968504F;
            this.txtValue04.Left = 3.733071F;
            this.txtValue04.MultiLine = false;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0F;
            this.txtValue04.Width = 2.013386F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM07";
            this.txtValue05.Height = 0.1968504F;
            this.txtValue05.Left = 6.137402F;
            this.txtValue05.MultiLine = false;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0F;
            this.txtValue05.Width = 0.9614172F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM09";
            this.txtValue07.Height = 0.1968504F;
            this.txtValue07.Left = 8.852363F;
            this.txtValue07.MultiLine = false;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center; vertical-align: midd" +
    "le";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0F;
            this.txtValue07.Visible = false;
            this.txtValue07.Width = 0.2322838F;
            // 
            // txtValue08
            // 
            this.txtValue08.DataField = "ITEM10";
            this.txtValue08.Height = 0.1968504F;
            this.txtValue08.Left = 8.852363F;
            this.txtValue08.MultiLine = false;
            this.txtValue08.Name = "txtValue08";
            this.txtValue08.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center; vertical-align: midd" +
    "le";
            this.txtValue08.Text = null;
            this.txtValue08.Top = 0F;
            this.txtValue08.Visible = false;
            this.txtValue08.Width = 0.2322838F;
            // 
            // txtValue09
            // 
            this.txtValue09.DataField = "ITEM11";
            this.txtValue09.Height = 0.1968504F;
            this.txtValue09.Left = 8.562205F;
            this.txtValue09.MultiLine = false;
            this.txtValue09.Name = "txtValue09";
            this.txtValue09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue09.Text = null;
            this.txtValue09.Top = 0F;
            this.txtValue09.Width = 1.440552F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM08";
            this.txtValue06.Height = 0.1968504F;
            this.txtValue06.Left = 7.215748F;
            this.txtValue06.MultiLine = false;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue06.Text = null;
            this.txtValue06.Top = 0F;
            this.txtValue06.Width = 1.04252F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM02";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.Visible = false;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTotalTitle,
            this.lblGenkin,
            this.txtValue09Total,
            this.lblFurikomi,
            this.txtValue08Total,
            this.lblTotal,
            this.txtValue10Total});
            this.groupFooter1.Height = 0.34375F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // lblTotalTitle
            // 
            this.lblTotalTitle.Height = 0.1968504F;
            this.lblTotalTitle.HyperLink = null;
            this.lblTotalTitle.Left = 0F;
            this.lblTotalTitle.Name = "lblTotalTitle";
            this.lblTotalTitle.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; vertical-align: middle" +
    "; ddo-char-set: 128";
            this.lblTotalTitle.Text = "＊ ＊ 合計 ＊ ＊　";
            this.lblTotalTitle.Top = 0.07283465F;
            this.lblTotalTitle.Width = 1.314567F;
            // 
            // lblGenkin
            // 
            this.lblGenkin.Height = 0.1968504F;
            this.lblGenkin.HyperLink = null;
            this.lblGenkin.Left = 2.624803F;
            this.lblGenkin.Name = "lblGenkin";
            this.lblGenkin.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; vertical-align: middle" +
    "; ddo-char-set: 128";
            this.lblGenkin.Text = "現金　";
            this.lblGenkin.Top = 0.07283465F;
            this.lblGenkin.Width = 0.565748F;
            // 
            // txtValue09Total
            // 
            this.txtValue09Total.DataField = "ITEM09";
            this.txtValue09Total.Height = 0.1968504F;
            this.txtValue09Total.Left = 3.190552F;
            this.txtValue09Total.MultiLine = false;
            this.txtValue09Total.Name = "txtValue09Total";
            this.txtValue09Total.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue09Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtValue09Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue09Total.Text = null;
            this.txtValue09Total.Top = 0.07283465F;
            this.txtValue09Total.Width = 1.075984F;
            // 
            // lblFurikomi
            // 
            this.lblFurikomi.Height = 0.1968504F;
            this.lblFurikomi.HyperLink = null;
            this.lblFurikomi.Left = 4.848819F;
            this.lblFurikomi.Name = "lblFurikomi";
            this.lblFurikomi.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; vertical-align: middle" +
    "; ddo-char-set: 128";
            this.lblFurikomi.Text = "振込";
            this.lblFurikomi.Top = 0.07283465F;
            this.lblFurikomi.Width = 0.5551183F;
            // 
            // txtValue08Total
            // 
            this.txtValue08Total.DataField = "ITEM08";
            this.txtValue08Total.Height = 0.1968504F;
            this.txtValue08Total.Left = 7.215748F;
            this.txtValue08Total.MultiLine = false;
            this.txtValue08Total.Name = "txtValue08Total";
            this.txtValue08Total.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue08Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtValue08Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue08Total.Text = null;
            this.txtValue08Total.Top = 0.07283465F;
            this.txtValue08Total.Width = 1.04252F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1968504F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 6.66063F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; vertical-align: middle" +
    "; ddo-char-set: 128";
            this.lblTotal.Text = "合計";
            this.lblTotal.Top = 0.07283465F;
            this.lblTotal.Width = 0.5551183F;
            // 
            // txtValue10Total
            // 
            this.txtValue10Total.DataField = "ITEM10";
            this.txtValue10Total.Height = 0.1968504F;
            this.txtValue10Total.Left = 5.403937F;
            this.txtValue10Total.MultiLine = false;
            this.txtValue10Total.Name = "txtValue10Total";
            this.txtValue10Total.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue10Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtValue10Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue10Total.Text = null;
            this.txtValue10Total.Top = 0.07283465F;
            this.txtValue10Total.Width = 1.013386F;
            // 
            // HNDR1061R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.7874016F;
            this.PageSettings.Margins.Right = 0.7874016F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.11417F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGenkin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFurikomi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGenkin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue09Total;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFurikomi;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08Total;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue10Total;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
    }
}
