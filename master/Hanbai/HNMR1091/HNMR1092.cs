﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1091
{
    /// <summary>
    /// 項目設定(HNMR1092)
    /// </summary>
    public partial class HNMR1092 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// SKDE1065(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        //String _pSHUKEIHYO;
        #endregion

        #region 定数
        /// <summary>
        /// データ取得用
        /// </summary>
        private const int POSITION_START_NO = 1;
        //private const int POSITION_END_NO = 8;

        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region プロパティ
        /// <summary>
        /// タイトルを格納する用データテーブル
        /// </summary>
        private DataTable _dtTitle = new DataTable();
        public DataTable Title
        {
            get
            {
                return this._dtTitle;
            }
        }

        /// <summary>
        /// 集計区分を格納する用データテーブル
        /// </summary>
        private DataTable _dtShukeiKubun = new DataTable();
        public DataTable ShukeiKubun
        {
            get
            {
                return this._dtShukeiKubun;
            }
        }

        /// <summary>
        /// 選択項目を格納する用データテーブル
        /// </summary>
        private DataTable _dtSentakuKomoku = new DataTable();
        public DataTable SentakuKomoku
        {
            get
            {
                return this._dtSentakuKomoku;
            }
        }

        /// <summary>
        /// 選択候補を格納する用データテーブル
        /// </summary>
        private DataTable _dtSentakuKoho = new DataTable();
        public DataTable SentakuKoho
        {
            get
            {
                return this._dtSentakuKoho;
            }
        }

        /// <summary>
        /// タイトルNoを格納する用変数
        /// </summary>
        private Decimal _dtPositionNo = new Decimal();
        public Decimal PositionNo
        {
            get
            {
                return this._dtPositionNo;
            }
        }

        private decimal _dtPosition_End_No;
        public decimal Position_End_No
        {
            get
            {
                return this._dtPosition_End_No;
            }
            set
            {
                Position_End_No = this._dtPosition_End_No;
            }
        }

        private Decimal _dtReportType = new decimal();
        public decimal ReportType
        {
            get
            {
                return this._dtReportType;
            }
            set
            {
                ReportType = this._dtReportType;
            }

        }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1092()//
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // サイズを縮める
                // フォームの配置を上へ移動する
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Visible = true;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }
            if (this.Par3 == "")
            {
                this.Par3 = "1";
            }
            this._dtReportType = Util.ToDecimal(this.Par3);

            // Escapeのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF1.Visible = true;
            this.btnF1.Enabled = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 画面の初期表示時
            DispDataSetTitle();
            DispDataSetSentakuKomoku();
            DispDataSetShukeiKubun();
            DispDataSetSentakuKoho();

            this.txtShukeiCd.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShukeiCd"
:                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.InsertTB_HN_SHUKEIHYO_SETTEI(0);
            // 選択項目を登録する
            this.InsertTB_HN_SHUKEIHYO_SETTEI_MEISAI();

            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        public override void PressF1()
        {
            // 売上集計選択画面
            HNMR1093 frmHNMR1093 = new HNMR1093();
            frmHNMR1093.Par2 = this.Par2;
            frmHNMR1093.Par3 = Util.ToString(this.ReportType);
            frmHNMR1093.InData = this.InData;

            frmHNMR1093.ShowDialog(this);

            if (frmHNMR1093.DialogResult == DialogResult.OK)
            {
                string[] result = (string[])frmHNMR1093.OutData;

                this.txtShukeiCd.Text = result[0];
                this.lblShukeiNm.Text = result[1];
            }
            frmHNMR1093.Dispose();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 集計表コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShukeiCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShukeihyoCd())
            {
                this.btnF3.Enabled = false;
                e.Cancel = true;
                this.txtShukeiCd.SelectAll();
            }
            else
            {
                // 集計コードに一致するデータがあるかを確認
                DataTable dtResult = this.GetShukeiSetteiData();
                if (dtResult.Rows.Count == 0)
                {
                    this.lblShukeiNm.Text = "";
                }
                else
                {
                    // タイトルをセットする ※現状、POSITIONが1でない場合、固定で0
                    int juni = 0;
                    this.SetTitle(juni);
                    // 選択項目をセットする
                    this.SetSentakuKomoku();
                    // 集計区分をセットする
//                    this.SetShukeiKubun();
                    // タイトルNoを保持
                    this._dtPositionNo = Util.ToDecimal(this.txtShukeiCd.Text);

                    //設定を保存してからタイトルを設定(;'∀')
                    this.lblShukeiNm.Text = dtResult.Rows[0]["RETSU_NM"].ToString();

                }
            }
        }

        /// <summary>
        /// タイトルが変更された場合の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShukeiCd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //// タイトルをセットする ※現状、POSITIONが1でない場合、固定で0
                //int juni = 0;
                //this.SetTitle(juni);
                //// 選択項目をセットする
                //this.SetSentakuKomoku();
                //// 集計区分をセットする
                //this.SetShukeiKubun();

                //// タイトルNoを保持
                //this._dtPositionNo = Util.ToDecimal( this.txtShukeiCd.Text);
            }
        }


        /// <summary>
        /// 選択した項目の移動処理(左→右)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRMove_Click(object sender, System.EventArgs e)
        {
            int[] selectNum = new int[this.lbxShukeiKubun.SelectedIndices.Count];

            for (int i = 0; i < this.lbxShukeiKubun.SelectedIndices.Count; i++)
            {
                selectNum[i] = this.lbxShukeiKubun.SelectedIndices[i];
            }

            #region 選択タイトル値が2以上の場合
            if (this.PositionNo > 1)
            {
                // 選択した項目をセット
                string ShukeiKubunNm;
                string filter;
                DataRow[] dataRows;
                DataTable kubunList = new DataTable();
                kubunList.Columns.Add("num", Type.GetType("System.Int32"));
                kubunList.Columns.Add("cd", Type.GetType("System.Int32"));
                kubunList.Columns.Add("nm", Type.GetType("System.String"));
                DataRow row;
                for (int i = 0; i < selectNum.Length; i++)
                {
                    // 集計区分名称を取得
                    ShukeiKubunNm = this.lbxShukeiKubun.Items[selectNum[i]].ToString();
                    // 集計区分情報を取得
                    filter = "position = " + this.PositionNo + " and nm = \'" + ShukeiKubunNm + "\'";
                    dataRows = this.ShukeiKubun.Select(filter);
                    row = kubunList.NewRow();
                    row["num"] = i;
                    row["cd"] = Util.ToInt(dataRows[0]["cd"]);
                    row["nm"] = Util.ToString(dataRows[0]["nm"]);
                    kubunList.Rows.Add(row);
                }

                // 選択項目の情報を取得
                DataRow[] dataRowsSentaku = this.SentakuKomoku.Select();

                // 選択した項目の追加
                dataRows = kubunList.Select();
                foreach (DataRow dr in dataRows)
                {
                    this.lbxSentakuKomoku.Items.Add(this.lbxShukeiKubun.Items[selectNum[Util.ToInt(dr["num"])]]);

                    // 選択項目に情報をセット
                    row = SentakuKomoku.NewRow();
                    row["position"] = this.PositionNo;
                    row["cd"] = Util.ToInt(dr["cd"]);
                    row["nm"] = Util.ToString(dr["nm"]);
                    SentakuKomoku.Rows.Add(row);

                    // 集計区分の情報を取得
                    filter = "position = " + this.PositionNo + " and nm = \'" + Util.ToString(dr["nm"]) + "\' and cd = \'" + Util.ToString(dr["cd"]) + "\'";
                    DataRow[] dataRowsShukei = this.ShukeiKubun.Select(filter);

                    // 集計区分から削除する
                    foreach (DataRow drSK in dataRowsShukei)
                    {
                        drSK.Delete();
                    }
                }
            }
            #endregion
            #region 選択タイトル値が1の場合
            else
            {
                // 選択した項目の追加
                for (int i = 0; i < selectNum.Length; i++)
                {
                    this.lbxSentakuKomoku.Items.Add(this.lbxShukeiKubun.Items[selectNum[i]]);
                }
            }
            #endregion

            int l = 1;
            // 選択した項目の削除
            for (int i = 0; i < selectNum.Length; i++)
            {
                if (i == 0)
                {
                    this.lbxShukeiKubun.Items.Remove(this.lbxShukeiKubun.Items[selectNum[i]]);
                }
                else
                {
                    this.lbxShukeiKubun.Items.Remove(this.lbxShukeiKubun.Items[selectNum[i] - l]);
                    l++;
                }
            }
        }

        /// <summary>
        /// 選択した項目の移動処理(右→左)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLMove_Click(object sender, System.EventArgs e)
        {
            {
                int[] selectNum = new int[this.lbxSentakuKomoku.SelectedIndices.Count];
                for (int i = 0; i < this.lbxSentakuKomoku.SelectedIndices.Count; i++)
                {
                    selectNum[i] = this.lbxSentakuKomoku.SelectedIndices[i];
                }

                // 選択した項目の追加
                string shukeiKubunNm;
                string filter;
                DataRow[] dataRows;
                DataRow row;
                for (int i = 0; i < selectNum.Length; i++)
                {
                    this.lbxShukeiKubun.Items.Add(this.lbxSentakuKomoku.Items[selectNum[i]]);

                    if (this.PositionNo != 1)
                    {
                        // 集計区分名称を取得
                        shukeiKubunNm = this.lbxSentakuKomoku.Items[selectNum[i]].ToString();
                        // 選択項目の情報を取得
                        filter = "position = " + this.PositionNo + " and nm = \'" + shukeiKubunNm + "\'";
                        dataRows = this.SentakuKomoku.Select(filter);

                        // 集計区分に情報をセット
                        row = ShukeiKubun.NewRow();
                        row["position"] = this.PositionNo;
                        row["cd"] = Util.ToInt(dataRows[0]["cd"]);
                        row["nm"] = shukeiKubunNm;
                        this.ShukeiKubun.Rows.Add(row);

                        // 選択項目から削除する
                        foreach (DataRow dr in dataRows)
                        {
                            dr.Delete();
                        }
                    }
                }

                int l = 1;
                // 選択した項目の削除
                for (int i = 0; i < selectNum.Length; i++)
                {
                    if (i == 0)
                    {
                        this.lbxSentakuKomoku.Items.Remove(this.lbxSentakuKomoku.Items[selectNum[i]]);
                    }
                    else
                    {
                        this.lbxSentakuKomoku.Items.Remove(this.lbxSentakuKomoku.Items[selectNum[i] - l]);
                        l++;
                    }
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 集計表コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidShukeihyoCd()
        {
            // 空の場合、エラーメッセージを表示し、フォーカスを移動しない
            if (ValChk.IsEmpty(this.txtShukeiCd.Text))
            {
                Msg.Notice("数値を入力してください。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShukeiCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0の場合、エラーメッセージを表示し、フォーカスを移動しない
            if (this.txtShukeiCd.Text == "0")
            {
                Msg.Notice("0より大きい数値を入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 画面から取得したデータを格納(タイトル)
        /// </summary>
        private void SetTitle(int juni)
        {
            // タイトルをDBに反映する
            // 集計名が入っていないならインサートさせるな！
            if (string.IsNullOrEmpty(this.lblShukeiNm.Text))
            {
                this.InsertTB_HN_SHUKEIHYO_SETTEI(juni);
            }

            // タイトルをDataTableに登録する
            int num = (int)this.PositionNo - 1;
            this.Title.Rows[num]["TITLE"] = this.lblShukeiNm.Text;

            // タイトルを表示する
            String filter = "position = " + Util.ToDecimal(this.txtShukeiCd.Text);
            DataRow[] dataRows = this.Title.Select(filter);

            if (dataRows.Length == 0) return;

            String titleNm = Util.ToString(dataRows[0]["title"]);
            this.lblShukeiNm.Text = titleNm;
        }
        
        /// <summary>
        /// 画面から取得したデータを格納(集計区分)
        /// </summary>
        private void SetShukeiKubun()
        {
            // 集計区分を表示する
            this.lbxShukeiKubun.Items.Clear();
            string filter = "position = " + Util.ToDecimal(this.txtShukeiCd.Text);
            DataRow[] dataRows = this.ShukeiKubun.Select(filter);
            foreach (DataRow dr in dataRows)
            {
                // 選択項目にセットする
                this.lbxShukeiKubun.Items.Add(Util.ToString(dr["nm"]));
            }
        }

        /// <summary>
        /// 画面から取得したデータを格納(選択項目)
        /// </summary>
        private void SetSentakuKomoku()
        {
            // 選択項目をDBに反映する
            this.InsertTB_HN_SHUKEIHYO_SETTEI_MEISAI();

            // 選択項目を表示する
            this.lbxSentakuKomoku.Items.Clear();
            string filter = "position = " + Util.ToDecimal(this.txtShukeiCd.Text);
            DataRow[] dataRows = this.SentakuKomoku.Select(filter);
            foreach (DataRow dr in dataRows)
            {
                // 選択項目にセットする
                if (Util.ToInt(dr["cd"]) != 0)
                {
                    this.lbxSentakuKomoku.Items.Add(Util.ToString(dr["nm"]));
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(タイトル)
        /// 画面に反映(タイトル)
        /// </summary>
        private void DispDataSetTitle()
        {
            // 対象データテーブルにカラムを2列ずつ定義
            this.Title.Columns.Add("position", Type.GetType("System.Int32"));
            this.Title.Columns.Add("title", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;
            string title = "";
            string titleNo = "";
            string titleNm = "";
            int cnt = 1;
            // タイトルをデータテーブルに格納
            dtResult = this.GetTB_HN_SHUKEIHYO_SETTEI();
            if (dtResult.Rows.Count > 0)
            {
                this._dtPosition_End_No = dtResult.Rows.Count;
                foreach (DataRow dr in dtResult.Rows)
                {
                    title = dr["TITLE"].ToString();
                    // 1件目のタイトルをセット
                    if (cnt == POSITION_START_NO)
                    {
                        titleNo = cnt.ToString();
                        titleNm = dr["TITLE"].ToString();
                    }

                    row = this.Title.NewRow();
                    row["position"] = cnt;
                    row["title"] = title;
                    this.Title.Rows.Add(row);
                    cnt++;
                }

            }
            // タイトルをセット
            this.txtShukeiCd.Text = titleNo;
            this.lblShukeiNm.Text = titleNm;
            _dtPositionNo = (Decimal)POSITION_START_NO;
        }

        /// <summary>
        /// DBから取得したデータを格納(集計区分)
        /// 画面に反映(集計区分)
        /// </summary>
        private void DispDataSetShukeiKubun()
        {
            // 対象データテーブルにカラムを4列ずつ定義
            this.ShukeiKubun.Columns.Add("position", Type.GetType("System.Int32"));
            this.ShukeiKubun.Columns.Add("cd", Type.GetType("System.Int32"));
            this.ShukeiKubun.Columns.Add("nm", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;

            // 項目名をデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= Position_End_No; cnt++)
            {
                dtResult = this.GetTB_HN_SHUKEIHYO_KOMOKU(cnt, true);
                foreach (DataRow dr in dtResult.Rows)
                {
                    row = this.ShukeiKubun.NewRow();
                    row["position"] = cnt;
                    row["cd"] = Util.ToInt(dr["KOMOKU_CD"]);
                    row["nm"] = Util.ToString(dr["KOMOKU_NM"]);
                    this.ShukeiKubun.Rows.Add(row);

                    // 1件目のデータをセット
                    if (cnt == POSITION_START_NO)
                    {
                        // 集計区分にセットする
                        this.lbxShukeiKubun.Items.Add(Util.ToString(dr["KOMOKU_NM"]));
                    }
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(選択項目)
        /// 画面に反映(選択項目)
        /// </summary>
        private void DispDataSetSentakuKomoku()
        {
            // 対象データテーブルにカラムを4列ずつ定義
            this.SentakuKomoku.Columns.Add("position", Type.GetType("System.Int32"));
            this.SentakuKomoku.Columns.Add("cd", Type.GetType("System.Int32"));
            this.SentakuKomoku.Columns.Add("nm", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;

            // 項目名をデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= Position_End_No; cnt++)
            {
                dtResult = this.GetVI_HN_SHUKEIHYO_SETTEI_MEISAI(cnt);
                foreach (DataRow dr in dtResult.Rows)
                {
                    row = this.SentakuKomoku.NewRow();
                    row["position"] = cnt;
                    row["cd"] = Util.ToInt(dr["SHUKEI_CD"]);
                    row["nm"] = Util.ToString(dr["KOMOKU_NM"]);
                    this.SentakuKomoku.Rows.Add(row);

                    // 1件目のデータをセット
                    if (cnt == POSITION_START_NO)
                    {
                        // 選択項目にセットする
                        if (Util.ToInt(dr["KOMOKU_CD"]) != 0)
                        {
                            this.lbxSentakuKomoku.Items.Add(Util.ToString(dr["KOMOKU_NM"]));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(選択項目候補)
        /// </summary>
        private void DispDataSetSentakuKoho()
        {
            // 対象データテーブルにカラムを3列ずつ定義
            this.SentakuKoho.Columns.Add("position", Type.GetType("System.Int32"));
            this.SentakuKoho.Columns.Add("cd", Type.GetType("System.Int32"));
            this.SentakuKoho.Columns.Add("nm", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;

            // 項目名をデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= Position_End_No; cnt++)
            {
                dtResult = this.GetTB_HN_SHUKEIHYO_KOMOKU(cnt, false);
                foreach (DataRow dr in dtResult.Rows)
                {
                    row = this.SentakuKoho.NewRow();
                    row["position"] = cnt;
                    row["cd"] = Util.ToInt(dr["KOMOKU_CD"]);
                    row["nm"] = Util.ToString(dr["KOMOKU_NM"]);
                    this.SentakuKoho.Rows.Add(row);
                }
            }
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetTB_HN_SHUKEIHYO_SETTEI()
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this._dtReportType);
            dpc.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);

            Sql.Append("SELECT RETSU_NM AS TITLE ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_RPT_RETSU_KBN ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" RPT_TYPE = @RPT_TYPE AND");
            Sql.Append(" RPT_NO = @RPT_NO ");
            Sql.Append("ORDER BY");
            Sql.Append(" RETSU_NO");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetVI_HN_SHUKEIHYO_SETTEI_MEISAI(int cnt)
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this._dtReportType);
            dpc.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);
            dpc.SetParam("@KOMOKU_CD", SqlDbType.Decimal, 4, cnt);

            Sql.Append("SELECT");
            Sql.Append(" KOMOKU_CD,");
            Sql.Append(" RIGHT('0000' + CAST(SHUKEI_CD AS varchar(4)), 4) + ' ' + ISNULL(SHUKEI_NM, '') AS KOMOKU_NM,");
            //Sql.Append(" SHUKEI_NM AS KOMOKU_NM,");
            Sql.Append(" SHUKEI_CD ");
            Sql.Append("FROM");
            if (Util.ToString(this.InData) == "1")
            {
                Sql.Append(" VI_HN_RPT_GYOSHUBUNRUI_MEISAI ");
            }
            else
            {
                Sql.Append(" VI_HN_RPT_GYOHO_MEISAI ");
            }
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD  = @KAISHA_CD AND");
            Sql.Append(" RPT_TYPE   = @RPT_TYPE AND ");
            Sql.Append(" RPT_NO     = @RPT_NO AND ");
            Sql.Append(" KOMOKU_CD  = @KOMOKU_CD ");
            Sql.Append("ORDER BY ");
            Sql.Append(" SHUKEI_CD");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetTB_HN_SHUKEIHYO_KOMOKU(int cnt, bool flg)
        {
            String notCd = "";

            // 集計区分表示用のデータを取得する場合
            if (flg)
            {
                String filter = "position = " + cnt;
                DataRow[] dataRowsSentakuKomoku = this.SentakuKomoku.Select(filter);
                ArrayList cdList = new ArrayList();
                foreach (DataRow dr in dataRowsSentakuKomoku)
                {
                    // 選択項目にセットする
                    cdList.Add(Util.ToString(dr.ItemArray[1].ToString()));
                }
                string[] cds = (string[])cdList.ToArray(typeof(string));
                notCd = String.Join(",", cds);
            }

            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            Sql.Append("SELECT");
            Sql.Append(" KOMOKU_CD,");
            Sql.Append(" RIGHT('0000' + CAST(KOMOKU_CD AS varchar(4)), 4) + ' ' + ISNULL(KOMOKU_NM, '') AS KOMOKU_NM ");
            //Sql.Append(" KOMOKU_NM ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_GSB_OR_GHO_KOMOKU ");
            Sql.Append("WHERE");
            Sql.Append(" RPT_TYPE  = @RPT_TYPE");
            if (notCd != "")
            {
                if (this.Par3 == "2")
                {
                    Sql.Append(" AND KOMOKU_CD NOT IN (" + notCd + ") ");
                }
                else
                {
                    Sql.Append(" AND RETSU_NO NOT IN (" + notCd + ") ");
                }
            }
            Sql.Append(" ORDER BY");
            Sql.Append(" KOMOKU_CD ASC ");
            dpc.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this._dtReportType);
            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを登録(タイトル)
        /// </summary>
        private void InsertTB_HN_SHUKEIHYO_SETTEI(int juni)
        {
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // TB_集計表設定削除
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@RPT_TYPE", SqlDbType.Decimal, 4, this._dtReportType);
                whereParam.SetParam("@RPT_NO", SqlDbType.Decimal, 4, this.InData);
                whereParam.SetParam("@RETSU_NO", SqlDbType.Decimal, 4, this.PositionNo);
                this.Dba.Delete("TB_HN_RPT_RETSU_KBN",
                    "KAISHA_CD = @KAISHA_CD AND RPT_NO = @RPT_NO AND RETSU_NO = @RETSU_NO",
                    whereParam);

                // TB_集計表設定登録
                DbParamCollection updParam = new DbParamCollection();
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@RPT_TYPE", SqlDbType.Decimal, 4, this._dtReportType);
                updParam.SetParam("@RPT_NO", SqlDbType.Decimal, 4, this.InData);
                updParam.SetParam("@RETSU_NO", SqlDbType.Decimal, 4, this.PositionNo);
                updParam.SetParam("@RETSU_NM", SqlDbType.VarChar, 20, this.lblShukeiNm.Text);

                this.Dba.Insert("TB_HN_RPT_RETSU_KBN", updParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// データを登録(選択項目)
        /// </summary>
        private void InsertTB_HN_SHUKEIHYO_SETTEI_MEISAI()
        {
            // 削除
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // TB_集計表設定明細削除
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this._dtReportType);
                whereParam.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);
                whereParam.SetParam("@RETSU_NO", SqlDbType.Decimal, 2, this.PositionNo);
                this.Dba.Delete("TB_HN_RPT_RETSU_MEISAI",
                    "KAISHA_CD = @KAISHA_CD AND RPT_TYPE = @RPT_TYPE AND RPT_NO = @RPT_NO AND RETSU_NO = @RETSU_NO ",
                    whereParam);

                // TB_集計表設定明細登録
                DbParamCollection updParam;
                String SentakuKomokuNm;
                int titleCd = 0;

                for (int cnt = 0; cnt < this.lbxSentakuKomoku.Items.Count; cnt++)
                {
                    SentakuKomokuNm = this.lbxSentakuKomoku.Items[cnt].ToString();

                    String filter = "nm = \'" + SentakuKomokuNm + "\'";
                    DataRow[] dataRowsTitle = SentakuKoho.Select(filter);
                    if (dataRowsTitle.Length > 0)
                    {
                        titleCd = Util.ToInt(dataRowsTitle[0]["cd"]);
                    }

                    updParam = new DbParamCollection();

                    updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this._dtReportType);
                    updParam.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);
                    updParam.SetParam("@RETSU_NO", SqlDbType.Decimal, 4, this.PositionNo);
                    updParam.SetParam("@SHUKEI_CD", SqlDbType.Decimal, 4, titleCd);

                    this.Dba.Insert("TB_HN_RPT_RETSU_MEISAI", updParam);
                }
                // 選択項目が0件の場合、項目コードを0として1件登録する
                if (this.lbxSentakuKomoku.Items.Count == 0)
                {
                    updParam = new DbParamCollection();

                    updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this._dtReportType);
                    updParam.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);
                    updParam.SetParam("@RETSU_NO", SqlDbType.Decimal, 4, this.PositionNo);
                    updParam.SetParam("@SHUKEI_CD", SqlDbType.Decimal, 2, titleCd);

                    this.Dba.Insert("TB_HN_RPT_RETSU_MEISAI", updParam);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }
        /// <summary>
        /// 集計表設定データの有無を取得
        /// </summary>
        /// <returns>集計表設定の取得したデータ</returns>
        private DataTable GetShukeiSetteiData()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" RETSU_NM ");
            sql.Append("FROM");
            sql.Append(" TB_HN_RPT_RETSU_KBN ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND RPT_TYPE  = @RPT_TYPE");
            sql.Append(" AND RPT_NO  = @RPT_NO");
            sql.Append(" AND RETSU_NO = @RETSU_NO ");
            sql.Append("ORDER BY");
            sql.Append(" RETSU_NO ASC ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this._dtReportType);
            dpc.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);
            dpc.SetParam("@RETSU_NO", SqlDbType.Decimal, 4, this.txtShukeiCd.Text);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtResult;
        }

        #endregion
    }
}
