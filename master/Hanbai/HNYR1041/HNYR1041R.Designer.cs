﻿namespace jp.co.fsi.hn.hnyr1041
{
    /// <summary>
    /// HNYR1041R の概要の説明です。
    /// </summary>
    partial class HNYR1041R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNYR1041R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.直線0 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.頁 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.年度 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.手数料計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.船主コード = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.船主名 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.回数計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.数量計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.金額計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.ラベル127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.手数料総合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.合計25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.回数総合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.数量総合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.金額総合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.頁)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.年度)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.手数料計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主コード)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.回数計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.数量計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.金額計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.手数料総合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.回数総合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.数量総合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.金額総合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線0,
            this.txtToday,
            this.ラベル3,
            this.頁,
            this.ラベル6,
            this.年度,
            this.ラベル8,
            this.ラベル9,
            this.ラベル10,
            this.ラベル11,
            this.ラベル12,
            this.ラベル13,
            this.ラベル14,
            this.ラベル15,
            this.ラベル16,
            this.ラベル17,
            this.ラベル18,
            this.ラベル19,
            this.ラベル20,
            this.ラベル21,
            this.ラベル22,
            this.ラベル23,
            this.ラベル24,
            this.ラベル25,
            this.ラベル26,
            this.ラベル27,
            this.ラベル29});
            this.pageHeader.Height = 0.7805555F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // 直線0
            // 
            this.直線0.Height = 0F;
            this.直線0.Left = 0F;
            this.直線0.LineWeight = 2F;
            this.直線0.Name = "直線0";
            this.直線0.Tag = "";
            this.直線0.Top = 0.7666667F;
            this.直線0.Width = 13.3875F;
            this.直線0.X1 = 0F;
            this.直線0.X2 = 13.3875F;
            this.直線0.Y1 = 0.7666667F;
            this.直線0.Y2 = 0.7666667F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.15625F;
            this.txtToday.Left = 10.15748F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.txtToday.Tag = "";
            this.txtToday.Text = null;
            this.txtToday.Top = 0F;
            this.txtToday.Width = 1.297968F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.15625F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 11.50732F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "作成";
            this.ラベル3.Top = 0F;
            this.ラベル3.Width = 0.4188013F;
            // 
            // 頁
            // 
            this.頁.Height = 0.15625F;
            this.頁.Left = 11.968F;
            this.頁.Name = "頁";
            this.頁.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.頁.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.頁.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.頁.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.頁.Tag = "";
            this.頁.Text = null;
            this.頁.Top = -9.313226E-09F;
            this.頁.Width = 0.4729443F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.15625F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 12.49606F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "頁";
            this.ラベル6.Top = 0F;
            this.ラベル6.Width = 0.2938013F;
            // 
            // 年度
            // 
            this.年度.DataField = "ITEM01";
            this.年度.Height = 0.15625F;
            this.年度.Left = 0.1979167F;
            this.年度.Name = "年度";
            this.年度.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.年度.Tag = "";
            this.年度.Text = "ITEM01";
            this.年度.Top = -9.313226E-09F;
            this.年度.Width = 0.7958333F;
            // 
            // ラベル8
            // 
            this.ラベル8.Height = 0.15625F;
            this.ラベル8.HyperLink = null;
            this.ラベル8.Left = 1.09375F;
            this.ラベル8.Name = "ラベル8";
            this.ラベル8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル8.Tag = "";
            this.ラベル8.Text = "分";
            this.ラベル8.Top = -9.313226E-09F;
            this.ラベル8.Width = 0.1770833F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.2291667F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 5.322917F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "***　組合員別資格申請書　***";
            this.ラベル9.Top = -9.313226E-09F;
            this.ラベル9.Width = 2.770833F;
            // 
            // ラベル10
            // 
            this.ラベル10.Height = 0.15625F;
            this.ラベル10.HyperLink = null;
            this.ラベル10.Left = 0.03503937F;
            this.ラベル10.Name = "ラベル10";
            this.ラベル10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル10.Tag = "";
            this.ラベル10.Text = "船主コード";
            this.ラベル10.Top = 0.5870079F;
            this.ラベル10.Width = 0.7255906F;
            // 
            // ラベル11
            // 
            this.ラベル11.Height = 0.15625F;
            this.ラベル11.HyperLink = null;
            this.ラベル11.Left = 0.8437008F;
            this.ラベル11.Name = "ラベル11";
            this.ラベル11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル11.Tag = "";
            this.ラベル11.Text = "船主名";
            this.ラベル11.Top = 0.5870079F;
            this.ラベル11.Width = 0.5588096F;
            // 
            // ラベル12
            // 
            this.ラベル12.Height = 0.15625F;
            this.ラベル12.HyperLink = null;
            this.ラベル12.Left = 2.239764F;
            this.ラベル12.Name = "ラベル12";
            this.ラベル12.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル12.Tag = "";
            this.ラベル12.Text = "上段：回数・数量";
            this.ラベル12.Top = 0.3850394F;
            this.ラベル12.Width = 1.199472F;
            // 
            // ラベル13
            // 
            this.ラベル13.Height = 0.15625F;
            this.ラベル13.HyperLink = null;
            this.ラベル13.Left = 2.239747F;
            this.ラベル13.Name = "ラベル13";
            this.ラベル13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル13.Tag = "";
            this.ラベル13.Text = "下段：金額";
            this.ラベル13.Top = 0.5869861F;
            this.ラベル13.Width = 0.8088096F;
            // 
            // ラベル14
            // 
            this.ラベル14.Height = 0.15625F;
            this.ラベル14.HyperLink = null;
            this.ラベル14.Left = 3.541848F;
            this.ラベル14.Name = "ラベル14";
            this.ラベル14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル14.Tag = "";
            this.ラベル14.Text = "１月";
            this.ラベル14.Top = 0.3850394F;
            this.ラベル14.Width = 0.4494727F;
            // 
            // ラベル15
            // 
            this.ラベル15.Height = 0.15625F;
            this.ラベル15.HyperLink = null;
            this.ラベル15.Left = 3.541831F;
            this.ラベル15.Name = "ラベル15";
            this.ラベル15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル15.Tag = "";
            this.ラベル15.Text = "７月";
            this.ラベル15.Top = 0.5869861F;
            this.ラベル15.Width = 0.4338096F;
            // 
            // ラベル16
            // 
            this.ラベル16.Height = 0.15625F;
            this.ラベル16.HyperLink = null;
            this.ラベル16.Left = 4.927264F;
            this.ラベル16.Name = "ラベル16";
            this.ラベル16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル16.Tag = "";
            this.ラベル16.Text = "２月";
            this.ラベル16.Top = 0.3850394F;
            this.ラベル16.Width = 0.4494727F;
            // 
            // ラベル17
            // 
            this.ラベル17.Height = 0.15625F;
            this.ラベル17.HyperLink = null;
            this.ラベル17.Left = 11.84391F;
            this.ラベル17.Name = "ラベル17";
            this.ラベル17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル17.Tag = "";
            this.ラベル17.Text = "（手数料）";
            this.ラベル17.Top = 0.5869861F;
            this.ラベル17.Width = 0.8088096F;
            // 
            // ラベル18
            // 
            this.ラベル18.Height = 0.15625F;
            this.ラベル18.HyperLink = null;
            this.ラベル18.Left = 6.302264F;
            this.ラベル18.Name = "ラベル18";
            this.ラベル18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル18.Tag = "";
            this.ラベル18.Text = "３月";
            this.ラベル18.Top = 0.3850394F;
            this.ラベル18.Width = 0.4494727F;
            // 
            // ラベル19
            // 
            this.ラベル19.Height = 0.15625F;
            this.ラベル19.HyperLink = null;
            this.ラベル19.Left = 7.656431F;
            this.ラベル19.Name = "ラベル19";
            this.ラベル19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル19.Tag = "";
            this.ラベル19.Text = "４月";
            this.ラベル19.Top = 0.3850394F;
            this.ラベル19.Width = 0.4494727F;
            // 
            // ラベル20
            // 
            this.ラベル20.Height = 0.15625F;
            this.ラベル20.HyperLink = null;
            this.ラベル20.Left = 9.031431F;
            this.ラベル20.Name = "ラベル20";
            this.ラベル20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル20.Tag = "";
            this.ラベル20.Text = "５月";
            this.ラベル20.Top = 0.3850394F;
            this.ラベル20.Width = 0.4494727F;
            // 
            // ラベル21
            // 
            this.ラベル21.Height = 0.15625F;
            this.ラベル21.HyperLink = null;
            this.ラベル21.Left = 12.05226F;
            this.ラベル21.Name = "ラベル21";
            this.ラベル21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル21.Tag = "";
            this.ラベル21.Text = "年計";
            this.ラベル21.Top = 0.3850394F;
            this.ラベル21.Width = 0.4494727F;
            // 
            // ラベル22
            // 
            this.ラベル22.Height = 0.15625F;
            this.ラベル22.HyperLink = null;
            this.ラベル22.Left = 10.41685F;
            this.ラベル22.Name = "ラベル22";
            this.ラベル22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル22.Tag = "";
            this.ラベル22.Text = "６月";
            this.ラベル22.Top = 0.3850394F;
            this.ラベル22.Width = 0.4494727F;
            // 
            // ラベル23
            // 
            this.ラベル23.Height = 0.15625F;
            this.ラベル23.HyperLink = null;
            this.ラベル23.Left = 11.53143F;
            this.ラベル23.Name = "ラベル23";
            this.ラベル23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル23.Tag = "";
            this.ラベル23.Text = "回数計";
            this.ラベル23.Top = 0.3850394F;
            this.ラベル23.Width = 0.5744727F;
            // 
            // ラベル24
            // 
            this.ラベル24.Height = 0.15625F;
            this.ラベル24.HyperLink = null;
            this.ラベル24.Left = 4.927247F;
            this.ラベル24.Name = "ラベル24";
            this.ラベル24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル24.Tag = "";
            this.ラベル24.Text = "８月";
            this.ラベル24.Top = 0.5869861F;
            this.ラベル24.Width = 0.4338096F;
            // 
            // ラベル25
            // 
            this.ラベル25.Height = 0.15625F;
            this.ラベル25.HyperLink = null;
            this.ラベル25.Left = 6.302247F;
            this.ラベル25.Name = "ラベル25";
            this.ラベル25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル25.Tag = "";
            this.ラベル25.Text = "９月";
            this.ラベル25.Top = 0.5869861F;
            this.ラベル25.Width = 0.4338096F;
            // 
            // ラベル26
            // 
            this.ラベル26.Height = 0.15625F;
            this.ラベル26.HyperLink = null;
            this.ラベル26.Left = 8.968914F;
            this.ラベル26.Name = "ラベル26";
            this.ラベル26.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル26.Tag = "";
            this.ラベル26.Text = "１１月";
            this.ラベル26.Top = 0.5869861F;
            this.ラベル26.Width = 0.5588096F;
            // 
            // ラベル27
            // 
            this.ラベル27.Height = 0.15625F;
            this.ラベル27.HyperLink = null;
            this.ラベル27.Left = 10.35919F;
            this.ラベル27.Name = "ラベル27";
            this.ラベル27.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル27.Tag = "";
            this.ラベル27.Text = "１２月";
            this.ラベル27.Top = 0.5869861F;
            this.ラベル27.Width = 0.5588096F;
            // 
            // ラベル29
            // 
            this.ラベル29.Height = 0.15625F;
            this.ラベル29.HyperLink = null;
            this.ラベル29.Left = 7.593914F;
            this.ラベル29.Name = "ラベル29";
            this.ラベル29.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル29.Tag = "";
            this.ラベル29.Text = "１０月";
            this.ラベル29.Top = 0.5869861F;
            this.ラベル29.Width = 0.5588096F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.手数料計,
            this.ラベル73,
            this.船主コード,
            this.船主名,
            this.テキスト1,
            this.テキスト13,
            this.テキスト25,
            this.テキスト2,
            this.テキスト14,
            this.テキスト26,
            this.テキスト3,
            this.テキスト15,
            this.テキスト27,
            this.テキスト4,
            this.テキスト16,
            this.テキスト28,
            this.テキスト5,
            this.テキスト17,
            this.テキスト29,
            this.テキスト6,
            this.テキスト18,
            this.テキスト30,
            this.テキスト7,
            this.テキスト19,
            this.テキスト31,
            this.テキスト8,
            this.テキスト20,
            this.テキスト32,
            this.テキスト9,
            this.テキスト21,
            this.テキスト33,
            this.テキスト10,
            this.テキスト22,
            this.テキスト34,
            this.テキスト11,
            this.テキスト23,
            this.テキスト35,
            this.テキスト12,
            this.テキスト24,
            this.テキスト36,
            this.回数計,
            this.数量計,
            this.金額計});
            this.detail.Height = 0.9686843F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // 手数料計
            // 
            this.手数料計.DataField = "ITEM40";
            this.手数料計.Height = 0.15625F;
            this.手数料計.Left = 11.41746F;
            this.手数料計.Name = "手数料計";
            this.手数料計.OutputFormat = resources.GetString("手数料計.OutputFormat");
            this.手数料計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.手数料計.Tag = "";
            this.手数料計.Text = "ITEM40";
            this.手数料計.Top = 0.7835138F;
            this.手数料計.Width = 0.9568396F;
            // 
            // ラベル73
            // 
            this.ラベル73.Height = 0.1622047F;
            this.ラベル73.HyperLink = null;
            this.ラベル73.Left = 11.33675F;
            this.ラベル73.Name = "ラベル73";
            this.ラベル73.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル73.Tag = "";
            this.ラベル73.Text = "(                    )";
            this.ラベル73.Top = 0.7839075F;
            this.ラベル73.Width = 1.615278F;
            // 
            // 船主コード
            // 
            this.船主コード.DataField = "ITEM02";
            this.船主コード.Height = 0.15625F;
            this.船主コード.Left = 0.1980315F;
            this.船主コード.Name = "船主コード";
            this.船主コード.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.船主コード.Tag = "";
            this.船主コード.Text = "ITEM02";
            this.船主コード.Top = 0F;
            this.船主コード.Width = 0.5625656F;
            // 
            // 船主名
            // 
            this.船主名.DataField = "ITEM03";
            this.船主名.Height = 0.15625F;
            this.船主名.Left = 0.8437008F;
            this.船主名.Name = "船主名";
            this.船主名.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.船主名.Tag = "";
            this.船主名.Text = "ITEM03";
            this.船主名.Top = -9.313226E-09F;
            this.船主名.Width = 1.912205F;
            // 
            // テキスト1
            // 
            this.テキスト1.DataField = "ITEM04";
            this.テキスト1.Height = 0.15625F;
            this.テキスト1.Left = 2.968584F;
            this.テキスト1.Name = "テキスト1";
            this.テキスト1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト1.Tag = "";
            this.テキスト1.Text = "ITEM04";
            this.テキスト1.Top = -9.313226E-09F;
            this.テキスト1.Width = 0.3895833F;
            // 
            // テキスト13
            // 
            this.テキスト13.DataField = "ITEM16";
            this.テキスト13.Height = 0.15625F;
            this.テキスト13.Left = 3.354001F;
            this.テキスト13.Name = "テキスト13";
            this.テキスト13.OutputFormat = resources.GetString("テキスト13.OutputFormat");
            this.テキスト13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト13.Tag = "";
            this.テキスト13.Text = "ITEM16";
            this.テキスト13.Top = -9.313226E-09F;
            this.テキスト13.Width = 0.7125F;
            // 
            // テキスト25
            // 
            this.テキスト25.DataField = "ITEM28";
            this.テキスト25.Height = 0.15625F;
            this.テキスト25.Left = 3.14252F;
            this.テキスト25.Name = "テキスト25";
            this.テキスト25.OutputFormat = resources.GetString("テキスト25.OutputFormat");
            this.テキスト25.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト25.Tag = "";
            this.テキスト25.Text = "ITEM28";
            this.テキスト25.Top = 0.2007874F;
            this.テキスト25.Width = 0.7125F;
            // 
            // テキスト2
            // 
            this.テキスト2.DataField = "ITEM05";
            this.テキスト2.Height = 0.15625F;
            this.テキスト2.Left = 4.458168F;
            this.テキスト2.Name = "テキスト2";
            this.テキスト2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト2.Tag = "";
            this.テキスト2.Text = "ITEM05";
            this.テキスト2.Top = -9.313226E-09F;
            this.テキスト2.Width = 0.3895833F;
            // 
            // テキスト14
            // 
            this.テキスト14.DataField = "ITEM17";
            this.テキスト14.Height = 0.15625F;
            this.テキスト14.Left = 4.843584F;
            this.テキスト14.Name = "テキスト14";
            this.テキスト14.OutputFormat = resources.GetString("テキスト14.OutputFormat");
            this.テキスト14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト14.Tag = "";
            this.テキスト14.Text = "ITEM17";
            this.テキスト14.Top = -9.313226E-09F;
            this.テキスト14.Width = 0.7125F;
            // 
            // テキスト26
            // 
            this.テキスト26.DataField = "ITEM29";
            this.テキスト26.Height = 0.15625F;
            this.テキスト26.Left = 4.620473F;
            this.テキスト26.Name = "テキスト26";
            this.テキスト26.OutputFormat = resources.GetString("テキスト26.OutputFormat");
            this.テキスト26.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト26.Tag = "";
            this.テキスト26.Text = "ITEM29";
            this.テキスト26.Top = 0.2007874F;
            this.テキスト26.Width = 0.7125F;
            // 
            // テキスト3
            // 
            this.テキスト3.DataField = "ITEM06";
            this.テキスト3.Height = 0.15625F;
            this.テキスト3.Left = 5.854001F;
            this.テキスト3.Name = "テキスト3";
            this.テキスト3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト3.Tag = "";
            this.テキスト3.Text = "ITEM06";
            this.テキスト3.Top = -9.313226E-09F;
            this.テキスト3.Width = 0.3895833F;
            // 
            // テキスト15
            // 
            this.テキスト15.DataField = "ITEM18";
            this.テキスト15.Height = 0.15625F;
            this.テキスト15.Left = 6.239418F;
            this.テキスト15.Name = "テキスト15";
            this.テキスト15.OutputFormat = resources.GetString("テキスト15.OutputFormat");
            this.テキスト15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト15.Tag = "";
            this.テキスト15.Text = "ITEM18";
            this.テキスト15.Top = -9.313226E-09F;
            this.テキスト15.Width = 0.7125F;
            // 
            // テキスト27
            // 
            this.テキスト27.DataField = "ITEM30";
            this.テキスト27.Height = 0.15625F;
            this.テキスト27.Left = 6.046063F;
            this.テキスト27.Name = "テキスト27";
            this.テキスト27.OutputFormat = resources.GetString("テキスト27.OutputFormat");
            this.テキスト27.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト27.Tag = "";
            this.テキスト27.Text = "ITEM30";
            this.テキスト27.Top = 0.2007874F;
            this.テキスト27.Width = 0.7125F;
            // 
            // テキスト4
            // 
            this.テキスト4.DataField = "ITEM07";
            this.テキスト4.Height = 0.15625F;
            this.テキスト4.Left = 7.218584F;
            this.テキスト4.Name = "テキスト4";
            this.テキスト4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト4.Tag = "";
            this.テキスト4.Text = "ITEM07";
            this.テキスト4.Top = -9.313226E-09F;
            this.テキスト4.Width = 0.3895833F;
            // 
            // テキスト16
            // 
            this.テキスト16.DataField = "ITEM19";
            this.テキスト16.Height = 0.15625F;
            this.テキスト16.Left = 7.604001F;
            this.テキスト16.Name = "テキスト16";
            this.テキスト16.OutputFormat = resources.GetString("テキスト16.OutputFormat");
            this.テキスト16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト16.Tag = "";
            this.テキスト16.Text = "ITEM19";
            this.テキスト16.Top = -9.313226E-09F;
            this.テキスト16.Width = 0.7125F;
            // 
            // テキスト28
            // 
            this.テキスト28.DataField = "ITEM31";
            this.テキスト28.Height = 0.15625F;
            this.テキスト28.Left = 7.375591F;
            this.テキスト28.Name = "テキスト28";
            this.テキスト28.OutputFormat = resources.GetString("テキスト28.OutputFormat");
            this.テキスト28.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト28.Tag = "";
            this.テキスト28.Text = "ITEM31";
            this.テキスト28.Top = 0.2007874F;
            this.テキスト28.Width = 0.7125F;
            // 
            // テキスト5
            // 
            this.テキスト5.DataField = "ITEM08";
            this.テキスト5.Height = 0.15625F;
            this.テキスト5.Left = 8.551917F;
            this.テキスト5.Name = "テキスト5";
            this.テキスト5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト5.Tag = "";
            this.テキスト5.Text = "ITEM08";
            this.テキスト5.Top = -9.313226E-09F;
            this.テキスト5.Width = 0.3895833F;
            // 
            // テキスト17
            // 
            this.テキスト17.DataField = "ITEM20";
            this.テキスト17.Height = 0.15625F;
            this.テキスト17.Left = 8.937334F;
            this.テキスト17.Name = "テキスト17";
            this.テキスト17.OutputFormat = resources.GetString("テキスト17.OutputFormat");
            this.テキスト17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト17.Tag = "";
            this.テキスト17.Text = "ITEM20";
            this.テキスト17.Top = -9.313226E-09F;
            this.テキスト17.Width = 0.7125F;
            // 
            // テキスト29
            // 
            this.テキスト29.DataField = "ITEM32";
            this.テキスト29.Height = 0.15625F;
            this.テキスト29.Left = 8.716536F;
            this.テキスト29.Name = "テキスト29";
            this.テキスト29.OutputFormat = resources.GetString("テキスト29.OutputFormat");
            this.テキスト29.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト29.Tag = "";
            this.テキスト29.Text = "ITEM32";
            this.テキスト29.Top = 0.2007874F;
            this.テキスト29.Width = 0.7125F;
            // 
            // テキスト6
            // 
            this.テキスト6.DataField = "ITEM09";
            this.テキスト6.Height = 0.15625F;
            this.テキスト6.Left = 9.989417F;
            this.テキスト6.Name = "テキスト6";
            this.テキスト6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト6.Tag = "";
            this.テキスト6.Text = "ITEM09";
            this.テキスト6.Top = -9.313226E-09F;
            this.テキスト6.Width = 0.3895833F;
            // 
            // テキスト18
            // 
            this.テキスト18.DataField = "ITEM21";
            this.テキスト18.Height = 0.15625F;
            this.テキスト18.Left = 10.37483F;
            this.テキスト18.Name = "テキスト18";
            this.テキスト18.OutputFormat = resources.GetString("テキスト18.OutputFormat");
            this.テキスト18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト18.Tag = "";
            this.テキスト18.Text = "ITEM21";
            this.テキスト18.Top = -9.313226E-09F;
            this.テキスト18.Width = 0.7125F;
            // 
            // テキスト30
            // 
            this.テキスト30.DataField = "ITEM33";
            this.テキスト30.Height = 0.15625F;
            this.テキスト30.Left = 10.16575F;
            this.テキスト30.Name = "テキスト30";
            this.テキスト30.OutputFormat = resources.GetString("テキスト30.OutputFormat");
            this.テキスト30.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト30.Tag = "";
            this.テキスト30.Text = "ITEM33";
            this.テキスト30.Top = 0.2007874F;
            this.テキスト30.Width = 0.7125F;
            // 
            // テキスト7
            // 
            this.テキスト7.DataField = "ITEM10";
            this.テキスト7.Height = 0.15625F;
            this.テキスト7.Left = 2.968898F;
            this.テキスト7.Name = "テキスト7";
            this.テキスト7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト7.Tag = "";
            this.テキスト7.Text = "ITEM10";
            this.テキスト7.Top = 0.396063F;
            this.テキスト7.Width = 0.3895833F;
            // 
            // テキスト19
            // 
            this.テキスト19.DataField = "ITEM22";
            this.テキスト19.Height = 0.15625F;
            this.テキスト19.Left = 3.354315F;
            this.テキスト19.Name = "テキスト19";
            this.テキスト19.OutputFormat = resources.GetString("テキスト19.OutputFormat");
            this.テキスト19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト19.Tag = "";
            this.テキスト19.Text = "ITEM22";
            this.テキスト19.Top = 0.396063F;
            this.テキスト19.Width = 0.7125F;
            // 
            // テキスト31
            // 
            this.テキスト31.DataField = "ITEM34";
            this.テキスト31.Height = 0.15625F;
            this.テキスト31.Left = 3.152575F;
            this.テキスト31.Name = "テキスト31";
            this.テキスト31.OutputFormat = resources.GetString("テキスト31.OutputFormat");
            this.テキスト31.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト31.Tag = "";
            this.テキスト31.Text = "ITEM34";
            this.テキスト31.Top = 0.5877953F;
            this.テキスト31.Width = 0.7125F;
            // 
            // テキスト8
            // 
            this.テキスト8.DataField = "ITEM11";
            this.テキスト8.Height = 0.15625F;
            this.テキスト8.Left = 4.468898F;
            this.テキスト8.Name = "テキスト8";
            this.テキスト8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト8.Tag = "";
            this.テキスト8.Text = "ITEM11";
            this.テキスト8.Top = 0.396063F;
            this.テキスト8.Width = 0.3895833F;
            // 
            // テキスト20
            // 
            this.テキスト20.DataField = "ITEM23";
            this.テキスト20.Height = 0.15625F;
            this.テキスト20.Left = 4.854315F;
            this.テキスト20.Name = "テキスト20";
            this.テキスト20.OutputFormat = resources.GetString("テキスト20.OutputFormat");
            this.テキスト20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト20.Tag = "";
            this.テキスト20.Text = "ITEM23";
            this.テキスト20.Top = 0.396063F;
            this.テキスト20.Width = 0.7125F;
            // 
            // テキスト32
            // 
            this.テキスト32.DataField = "ITEM35";
            this.テキスト32.Height = 0.15625F;
            this.テキスト32.Left = 4.640945F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.OutputFormat = resources.GetString("テキスト32.OutputFormat");
            this.テキスト32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "ITEM35";
            this.テキスト32.Top = 0.5877953F;
            this.テキスト32.Width = 0.7125F;
            // 
            // テキスト9
            // 
            this.テキスト9.DataField = "ITEM12";
            this.テキスト9.Height = 0.15625F;
            this.テキスト9.Left = 5.854315F;
            this.テキスト9.Name = "テキスト9";
            this.テキスト9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト9.Tag = "";
            this.テキスト9.Text = "ITEM12";
            this.テキスト9.Top = 0.396063F;
            this.テキスト9.Width = 0.3895833F;
            // 
            // テキスト21
            // 
            this.テキスト21.DataField = "ITEM24";
            this.テキスト21.Height = 0.15625F;
            this.テキスト21.Left = 6.239732F;
            this.テキスト21.Name = "テキスト21";
            this.テキスト21.OutputFormat = resources.GetString("テキスト21.OutputFormat");
            this.テキスト21.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト21.Tag = "";
            this.テキスト21.Text = "ITEM24";
            this.テキスト21.Top = 0.396063F;
            this.テキスト21.Width = 0.7125F;
            // 
            // テキスト33
            // 
            this.テキスト33.DataField = "ITEM36";
            this.テキスト33.Height = 0.15625F;
            this.テキスト33.Left = 6.045702F;
            this.テキスト33.Name = "テキスト33";
            this.テキスト33.OutputFormat = resources.GetString("テキスト33.OutputFormat");
            this.テキスト33.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト33.Tag = "";
            this.テキスト33.Text = "ITEM36";
            this.テキスト33.Top = 0.5877953F;
            this.テキスト33.Width = 0.7125F;
            // 
            // テキスト10
            // 
            this.テキスト10.DataField = "ITEM13";
            this.テキスト10.Height = 0.15625F;
            this.テキスト10.Left = 7.218898F;
            this.テキスト10.Name = "テキスト10";
            this.テキスト10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト10.Tag = "";
            this.テキスト10.Text = "ITEM13";
            this.テキスト10.Top = 0.396063F;
            this.テキスト10.Width = 0.3895833F;
            // 
            // テキスト22
            // 
            this.テキスト22.DataField = "ITEM25";
            this.テキスト22.Height = 0.15625F;
            this.テキスト22.Left = 7.604315F;
            this.テキスト22.Name = "テキスト22";
            this.テキスト22.OutputFormat = resources.GetString("テキスト22.OutputFormat");
            this.テキスト22.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト22.Tag = "";
            this.テキスト22.Text = "ITEM25";
            this.テキスト22.Top = 0.396063F;
            this.テキスト22.Width = 0.7125F;
            // 
            // テキスト34
            // 
            this.テキスト34.DataField = "ITEM37";
            this.テキスト34.Height = 0.15625F;
            this.テキスト34.Left = 7.37523F;
            this.テキスト34.Name = "テキスト34";
            this.テキスト34.OutputFormat = resources.GetString("テキスト34.OutputFormat");
            this.テキスト34.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト34.Tag = "";
            this.テキスト34.Text = "ITEM37";
            this.テキスト34.Top = 0.5877953F;
            this.テキスト34.Width = 0.7125F;
            // 
            // テキスト11
            // 
            this.テキスト11.DataField = "ITEM14";
            this.テキスト11.Height = 0.15625F;
            this.テキスト11.Left = 8.552231F;
            this.テキスト11.Name = "テキスト11";
            this.テキスト11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト11.Tag = "";
            this.テキスト11.Text = "ITEM14";
            this.テキスト11.Top = 0.396063F;
            this.テキスト11.Width = 0.3895833F;
            // 
            // テキスト23
            // 
            this.テキスト23.DataField = "ITEM26";
            this.テキスト23.Height = 0.15625F;
            this.テキスト23.Left = 8.937649F;
            this.テキスト23.Name = "テキスト23";
            this.テキスト23.OutputFormat = resources.GetString("テキスト23.OutputFormat");
            this.テキスト23.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト23.Tag = "";
            this.テキスト23.Text = "ITEM26";
            this.テキスト23.Top = 0.396063F;
            this.テキスト23.Width = 0.7125F;
            // 
            // テキスト35
            // 
            this.テキスト35.DataField = "ITEM38";
            this.テキスト35.Height = 0.15625F;
            this.テキスト35.Left = 8.716173F;
            this.テキスト35.Name = "テキスト35";
            this.テキスト35.OutputFormat = resources.GetString("テキスト35.OutputFormat");
            this.テキスト35.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト35.Tag = "";
            this.テキスト35.Text = "ITEM38";
            this.テキスト35.Top = 0.5877953F;
            this.テキスト35.Width = 0.7125F;
            // 
            // テキスト12
            // 
            this.テキスト12.DataField = "ITEM15";
            this.テキスト12.Height = 0.15625F;
            this.テキスト12.Left = 9.989731F;
            this.テキスト12.Name = "テキスト12";
            this.テキスト12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト12.Tag = "";
            this.テキスト12.Text = "ITEM15";
            this.テキスト12.Top = 0.396063F;
            this.テキスト12.Width = 0.3895833F;
            // 
            // テキスト24
            // 
            this.テキスト24.DataField = "ITEM27";
            this.テキスト24.Height = 0.15625F;
            this.テキスト24.Left = 10.37514F;
            this.テキスト24.Name = "テキスト24";
            this.テキスト24.OutputFormat = resources.GetString("テキスト24.OutputFormat");
            this.テキスト24.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト24.Tag = "";
            this.テキスト24.Text = "ITEM27";
            this.テキスト24.Top = 0.396063F;
            this.テキスト24.Width = 0.7125F;
            // 
            // テキスト36
            // 
            this.テキスト36.DataField = "ITEM39";
            this.テキスト36.Height = 0.15625F;
            this.テキスト36.Left = 10.16539F;
            this.テキスト36.Name = "テキスト36";
            this.テキスト36.OutputFormat = resources.GetString("テキスト36.OutputFormat");
            this.テキスト36.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト36.Tag = "";
            this.テキスト36.Text = "ITEM39";
            this.テキスト36.Top = 0.5877953F;
            this.テキスト36.Width = 0.7125F;
            // 
            // 回数計
            // 
            this.回数計.Height = 0.15625F;
            this.回数計.Left = 11.4165F;
            this.回数計.Name = "回数計";
            this.回数計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.回数計.Tag = "";
            this.回数計.Text = null;
            this.回数計.Top = -9.313226E-09F;
            this.回数計.Width = 0.5562499F;
            // 
            // 数量計
            // 
            this.数量計.Height = 0.15625F;
            this.数量計.Left = 11.65603F;
            this.数量計.Name = "数量計";
            this.数量計.OutputFormat = resources.GetString("数量計.OutputFormat");
            this.数量計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.数量計.Tag = "";
            this.数量計.Text = null;
            this.数量計.Top = 0.396063F;
            this.数量計.Width = 1.18125F;
            // 
            // 金額計
            // 
            this.金額計.Height = 0.15625F;
            this.金額計.Left = 11.41181F;
            this.金額計.Name = "金額計";
            this.金額計.OutputFormat = resources.GetString("金額計.OutputFormat");
            this.金額計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.金額計.Tag = "";
            this.金額計.Text = null;
            this.金額計.Top = 0.5877953F;
            this.金額計.Width = 1.18125F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル127,
            this.手数料総合計,
            this.直線76,
            this.合計25,
            this.合計1,
            this.合計7,
            this.合計26,
            this.合計2,
            this.合計8,
            this.合計27,
            this.合計3,
            this.合計9,
            this.合計28,
            this.合計4,
            this.合計10,
            this.合計29,
            this.合計5,
            this.合計11,
            this.合計30,
            this.合計6,
            this.合計12,
            this.合計31,
            this.合計13,
            this.合計19,
            this.合計32,
            this.合計14,
            this.合計20,
            this.合計33,
            this.合計15,
            this.合計21,
            this.合計34,
            this.合計16,
            this.合計22,
            this.合計35,
            this.合計17,
            this.合計23,
            this.合計36,
            this.合計18,
            this.合計24,
            this.回数総合計,
            this.数量総合計,
            this.金額総合計});
            this.reportFooter1.Height = 1.427778F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // ラベル127
            // 
            this.ラベル127.Height = 0.15625F;
            this.ラベル127.HyperLink = null;
            this.ラベル127.Left = 11.30763F;
            this.ラベル127.Name = "ラベル127";
            this.ラベル127.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル127.Tag = "";
            this.ラベル127.Text = "(                    )";
            this.ラベル127.Top = 0.8716536F;
            this.ラベル127.Width = 1.644396F;
            // 
            // 手数料総合計
            // 
            this.手数料総合計.DataField = "ITEM40";
            this.手数料総合計.Height = 0.15625F;
            this.手数料総合計.Left = 11.41732F;
            this.手数料総合計.Name = "手数料総合計";
            this.手数料総合計.OutputFormat = resources.GetString("手数料総合計.OutputFormat");
            this.手数料総合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.手数料総合計.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.手数料総合計.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.手数料総合計.Tag = "";
            this.手数料総合計.Text = null;
            this.手数料総合計.Top = 0.8716537F;
            this.手数料総合計.Width = 0.9568405F;
            // 
            // 直線76
            // 
            this.直線76.Height = 0F;
            this.直線76.Left = 0F;
            this.直線76.LineWeight = 1F;
            this.直線76.Name = "直線76";
            this.直線76.Tag = "";
            this.直線76.Top = 0F;
            this.直線76.Width = 13.3875F;
            this.直線76.X1 = 0F;
            this.直線76.X2 = 13.3875F;
            this.直線76.Y1 = 0F;
            this.直線76.Y2 = 0F;
            // 
            // 合計25
            // 
            this.合計25.DataField = "ITEM04";
            this.合計25.Height = 0.15625F;
            this.合計25.Left = 2.958334F;
            this.合計25.Name = "合計25";
            this.合計25.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計25.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計25.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計25.Tag = "";
            this.合計25.Text = "ITEM04";
            this.合計25.Top = 1.028035F;
            this.合計25.Width = 0.3895833F;
            // 
            // 合計1
            // 
            this.合計1.DataField = "ITEM16";
            this.合計1.Height = 0.15625F;
            this.合計1.Left = 2.992126F;
            this.合計1.Name = "合計1";
            this.合計1.OutputFormat = resources.GetString("合計1.OutputFormat");
            this.合計1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計1.Tag = "";
            this.合計1.Text = "ITEM16";
            this.合計1.Top = 0.05208333F;
            this.合計1.Width = 1.064124F;
            // 
            // 合計7
            // 
            this.合計7.DataField = "ITEM28";
            this.合計7.Height = 0.15625F;
            this.合計7.Left = 2.755906F;
            this.合計7.Name = "合計7";
            this.合計7.OutputFormat = resources.GetString("合計7.OutputFormat");
            this.合計7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計7.Tag = "";
            this.合計7.Text = "ITEM28";
            this.合計7.Top = 0.2558891F;
            this.合計7.Width = 1.088484F;
            // 
            // 合計26
            // 
            this.合計26.DataField = "ITEM05";
            this.合計26.Height = 0.15625F;
            this.合計26.Left = 4.458334F;
            this.合計26.Name = "合計26";
            this.合計26.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計26.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計26.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計26.Tag = "";
            this.合計26.Text = "ITEM05";
            this.合計26.Top = 1.028035F;
            this.合計26.Width = 0.3895833F;
            // 
            // 合計2
            // 
            this.合計2.DataField = "ITEM17";
            this.合計2.Height = 0.15625F;
            this.合計2.Left = 4.481709F;
            this.合計2.Name = "合計2";
            this.合計2.OutputFormat = resources.GetString("合計2.OutputFormat");
            this.合計2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計2.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計2.Tag = "";
            this.合計2.Text = "ITEM17";
            this.合計2.Top = 0.04166667F;
            this.合計2.Width = 1.064124F;
            // 
            // 合計8
            // 
            this.合計8.DataField = "ITEM29";
            this.合計8.Height = 0.15625F;
            this.合計8.Left = 4.233859F;
            this.合計8.Name = "合計8";
            this.合計8.OutputFormat = resources.GetString("合計8.OutputFormat");
            this.合計8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計8.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計8.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計8.Tag = "";
            this.合計8.Text = "ITEM29";
            this.合計8.Top = 0.2454725F;
            this.合計8.Width = 1.088484F;
            // 
            // 合計27
            // 
            this.合計27.DataField = "ITEM06";
            this.合計27.Height = 0.15625F;
            this.合計27.Left = 5.864584F;
            this.合計27.Name = "合計27";
            this.合計27.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計27.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計27.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計27.Tag = "";
            this.合計27.Text = "ITEM06";
            this.合計27.Top = 1.028035F;
            this.合計27.Width = 0.3895833F;
            // 
            // 合計3
            // 
            this.合計3.DataField = "ITEM18";
            this.合計3.Height = 0.15625F;
            this.合計3.Left = 5.877543F;
            this.合計3.Name = "合計3";
            this.合計3.OutputFormat = resources.GetString("合計3.OutputFormat");
            this.合計3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計3.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計3.Tag = "";
            this.合計3.Text = "ITEM18";
            this.合計3.Top = 0.04166667F;
            this.合計3.Width = 1.064124F;
            // 
            // 合計9
            // 
            this.合計9.DataField = "ITEM30";
            this.合計9.Height = 0.15625F;
            this.合計9.Left = 5.65945F;
            this.合計9.Name = "合計9";
            this.合計9.OutputFormat = resources.GetString("合計9.OutputFormat");
            this.合計9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計9.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計9.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計9.Tag = "";
            this.合計9.Text = "ITEM30";
            this.合計9.Top = 0.2454725F;
            this.合計9.Width = 1.088484F;
            // 
            // 合計28
            // 
            this.合計28.DataField = "ITEM07";
            this.合計28.Height = 0.15625F;
            this.合計28.Left = 7.197918F;
            this.合計28.Name = "合計28";
            this.合計28.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計28.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計28.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計28.Tag = "";
            this.合計28.Text = "ITEM07";
            this.合計28.Top = 1.028035F;
            this.合計28.Width = 0.3895833F;
            // 
            // 合計4
            // 
            this.合計4.DataField = "ITEM19";
            this.合計4.Height = 0.15625F;
            this.合計4.Left = 7.242126F;
            this.合計4.Name = "合計4";
            this.合計4.OutputFormat = resources.GetString("合計4.OutputFormat");
            this.合計4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計4.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計4.Tag = "";
            this.合計4.Text = "ITEM19";
            this.合計4.Top = 0.04166667F;
            this.合計4.Width = 1.064123F;
            // 
            // 合計10
            // 
            this.合計10.DataField = "ITEM31";
            this.合計10.Height = 0.15625F;
            this.合計10.Left = 6.988977F;
            this.合計10.Name = "合計10";
            this.合計10.OutputFormat = resources.GetString("合計10.OutputFormat");
            this.合計10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計10.Tag = "";
            this.合計10.Text = "ITEM31";
            this.合計10.Top = 0.2454725F;
            this.合計10.Width = 1.088484F;
            // 
            // 合計29
            // 
            this.合計29.DataField = "ITEM08";
            this.合計29.Height = 0.15625F;
            this.合計29.Left = 8.541733F;
            this.合計29.Name = "合計29";
            this.合計29.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計29.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計29.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計29.Tag = "";
            this.合計29.Text = "ITEM08";
            this.合計29.Top = 0.9922901F;
            this.合計29.Width = 0.3895833F;
            // 
            // 合計5
            // 
            this.合計5.DataField = "ITEM20";
            this.合計5.Height = 0.15625F;
            this.合計5.Left = 8.575459F;
            this.合計5.Name = "合計5";
            this.合計5.OutputFormat = resources.GetString("合計5.OutputFormat");
            this.合計5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計5.Tag = "";
            this.合計5.Text = "ITEM20";
            this.合計5.Top = 0.04166667F;
            this.合計5.Width = 1.064123F;
            // 
            // 合計11
            // 
            this.合計11.DataField = "ITEM32";
            this.合計11.Height = 0.15625F;
            this.合計11.Left = 8.329922F;
            this.合計11.Name = "合計11";
            this.合計11.OutputFormat = resources.GetString("合計11.OutputFormat");
            this.合計11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計11.Tag = "";
            this.合計11.Text = "ITEM32";
            this.合計11.Top = 0.2454725F;
            this.合計11.Width = 1.088484F;
            // 
            // 合計30
            // 
            this.合計30.DataField = "ITEM09";
            this.合計30.Height = 0.15625F;
            this.合計30.Left = 9.968751F;
            this.合計30.Name = "合計30";
            this.合計30.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計30.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計30.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計30.Tag = "";
            this.合計30.Text = "ITEM09";
            this.合計30.Top = 1.028035F;
            this.合計30.Width = 0.3895833F;
            // 
            // 合計6
            // 
            this.合計6.DataField = "ITEM21";
            this.合計6.Height = 0.15625F;
            this.合計6.Left = 10.01296F;
            this.合計6.Name = "合計6";
            this.合計6.OutputFormat = resources.GetString("合計6.OutputFormat");
            this.合計6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計6.Tag = "";
            this.合計6.Text = "ITEM21";
            this.合計6.Top = 0.04166667F;
            this.合計6.Width = 1.064123F;
            // 
            // 合計12
            // 
            this.合計12.DataField = "ITEM33";
            this.合計12.Height = 0.15625F;
            this.合計12.Left = 9.779135F;
            this.合計12.Name = "合計12";
            this.合計12.OutputFormat = resources.GetString("合計12.OutputFormat");
            this.合計12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計12.Tag = "";
            this.合計12.Text = "ITEM33";
            this.合計12.Top = 0.2454725F;
            this.合計12.Width = 1.088484F;
            // 
            // 合計31
            // 
            this.合計31.DataField = "ITEM10";
            this.合計31.Height = 0.15625F;
            this.合計31.Left = 2.958317F;
            this.合計31.Name = "合計31";
            this.合計31.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計31.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計31.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計31.Tag = "";
            this.合計31.Text = "ITEM11";
            this.合計31.Top = 1.220473F;
            this.合計31.Width = 0.3895833F;
            // 
            // 合計13
            // 
            this.合計13.DataField = "ITEM22";
            this.合計13.Height = 0.15625F;
            this.合計13.Left = 2.992061F;
            this.合計13.Name = "合計13";
            this.合計13.OutputFormat = resources.GetString("合計13.OutputFormat");
            this.合計13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計13.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計13.Tag = "";
            this.合計13.Text = "ITEM22";
            this.合計13.Top = 0.4554462F;
            this.合計13.Width = 1.064124F;
            // 
            // 合計19
            // 
            this.合計19.DataField = "ITEM34";
            this.合計19.Height = 0.15625F;
            this.合計19.Left = 2.766388F;
            this.合計19.Name = "合計19";
            this.合計19.OutputFormat = resources.GetString("合計19.OutputFormat");
            this.合計19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計19.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計19.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計19.Tag = "";
            this.合計19.Text = "ITEM34";
            this.合計19.Top = 0.6826444F;
            this.合計19.Width = 1.088484F;
            // 
            // 合計32
            // 
            this.合計32.DataField = "ITEM11";
            this.合計32.Height = 0.15625F;
            this.合計32.Left = 4.458317F;
            this.合計32.Name = "合計32";
            this.合計32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計32.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計32.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計32.Tag = "";
            this.合計32.Text = "ITEM11";
            this.合計32.Top = 1.220473F;
            this.合計32.Width = 0.3895833F;
            // 
            // 合計14
            // 
            this.合計14.DataField = "ITEM23";
            this.合計14.Height = 0.15625F;
            this.合計14.Left = 4.492062F;
            this.合計14.Name = "合計14";
            this.合計14.OutputFormat = resources.GetString("合計14.OutputFormat");
            this.合計14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計14.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計14.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計14.Tag = "";
            this.合計14.Text = "ITEM23";
            this.合計14.Top = 0.4450296F;
            this.合計14.Width = 1.064124F;
            // 
            // 合計20
            // 
            this.合計20.DataField = "ITEM35";
            this.合計20.Height = 0.15625F;
            this.合計20.Left = 4.254759F;
            this.合計20.Name = "合計20";
            this.合計20.OutputFormat = resources.GetString("合計20.OutputFormat");
            this.合計20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計20.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計20.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計20.Tag = "";
            this.合計20.Text = "ITEM35";
            this.合計20.Top = 0.6722277F;
            this.合計20.Width = 1.088484F;
            // 
            // 合計33
            // 
            this.合計33.DataField = "ITEM12";
            this.合計33.Height = 0.15625F;
            this.合計33.Left = 5.864567F;
            this.合計33.Name = "合計33";
            this.合計33.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計33.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計33.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計33.Tag = "";
            this.合計33.Text = "ITEM12";
            this.合計33.Top = 1.220473F;
            this.合計33.Width = 0.3895833F;
            // 
            // 合計15
            // 
            this.合計15.DataField = "ITEM24";
            this.合計15.Height = 0.15625F;
            this.合計15.Left = 5.877479F;
            this.合計15.Name = "合計15";
            this.合計15.OutputFormat = resources.GetString("合計15.OutputFormat");
            this.合計15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計15.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計15.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計15.Tag = "";
            this.合計15.Text = "ITEM24";
            this.合計15.Top = 0.4450296F;
            this.合計15.Width = 1.064124F;
            // 
            // 合計21
            // 
            this.合計21.DataField = "ITEM36";
            this.合計21.Height = 0.15625F;
            this.合計21.Left = 5.659515F;
            this.合計21.Name = "合計21";
            this.合計21.OutputFormat = resources.GetString("合計21.OutputFormat");
            this.合計21.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計21.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計21.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計21.Tag = "";
            this.合計21.Text = "ITEM36";
            this.合計21.Top = 0.6722277F;
            this.合計21.Width = 1.088484F;
            // 
            // 合計34
            // 
            this.合計34.DataField = "ITEM13";
            this.合計34.Height = 0.15625F;
            this.合計34.Left = 7.197901F;
            this.合計34.Name = "合計34";
            this.合計34.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計34.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計34.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計34.Tag = "";
            this.合計34.Text = "ITEM13";
            this.合計34.Top = 1.220473F;
            this.合計34.Width = 0.3895833F;
            // 
            // 合計16
            // 
            this.合計16.DataField = "ITEM25";
            this.合計16.Height = 0.15625F;
            this.合計16.Left = 7.242062F;
            this.合計16.Name = "合計16";
            this.合計16.OutputFormat = resources.GetString("合計16.OutputFormat");
            this.合計16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計16.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計16.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計16.Tag = "";
            this.合計16.Text = "ITEM25";
            this.合計16.Top = 0.4450296F;
            this.合計16.Width = 1.064123F;
            // 
            // 合計22
            // 
            this.合計22.DataField = "ITEM37";
            this.合計22.Height = 0.15625F;
            this.合計22.Left = 6.989043F;
            this.合計22.Name = "合計22";
            this.合計22.OutputFormat = resources.GetString("合計22.OutputFormat");
            this.合計22.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計22.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計22.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計22.Tag = "";
            this.合計22.Text = "ITEM37";
            this.合計22.Top = 0.6722277F;
            this.合計22.Width = 1.088484F;
            // 
            // 合計35
            // 
            this.合計35.DataField = "ITEM14";
            this.合計35.Height = 0.15625F;
            this.合計35.Left = 8.541716F;
            this.合計35.Name = "合計35";
            this.合計35.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計35.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計35.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計35.Tag = "";
            this.合計35.Text = "ITEM14";
            this.合計35.Top = 1.184728F;
            this.合計35.Width = 0.3895833F;
            // 
            // 合計17
            // 
            this.合計17.DataField = "ITEM26";
            this.合計17.Height = 0.15625F;
            this.合計17.Left = 8.575395F;
            this.合計17.Name = "合計17";
            this.合計17.OutputFormat = resources.GetString("合計17.OutputFormat");
            this.合計17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計17.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計17.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計17.Tag = "";
            this.合計17.Text = "ITEM26";
            this.合計17.Top = 0.4450296F;
            this.合計17.Width = 1.064123F;
            // 
            // 合計23
            // 
            this.合計23.DataField = "ITEM38";
            this.合計23.Height = 0.15625F;
            this.合計23.Left = 8.329988F;
            this.合計23.Name = "合計23";
            this.合計23.OutputFormat = resources.GetString("合計23.OutputFormat");
            this.合計23.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計23.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計23.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計23.Tag = "";
            this.合計23.Text = "ITEM38";
            this.合計23.Top = 0.6289206F;
            this.合計23.Width = 1.088484F;
            // 
            // 合計36
            // 
            this.合計36.DataField = "ITEM15";
            this.合計36.Height = 0.15625F;
            this.合計36.Left = 9.968735F;
            this.合計36.Name = "合計36";
            this.合計36.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.合計36.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計36.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計36.Tag = "";
            this.合計36.Text = "ITEM15";
            this.合計36.Top = 1.220473F;
            this.合計36.Width = 0.3895833F;
            // 
            // 合計18
            // 
            this.合計18.DataField = "ITEM27";
            this.合計18.Height = 0.15625F;
            this.合計18.Left = 10.01289F;
            this.合計18.Name = "合計18";
            this.合計18.OutputFormat = resources.GetString("合計18.OutputFormat");
            this.合計18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計18.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計18.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計18.Tag = "";
            this.合計18.Text = "ITEM27";
            this.合計18.Top = 0.4450296F;
            this.合計18.Width = 1.064123F;
            // 
            // 合計24
            // 
            this.合計24.DataField = "ITEM39";
            this.合計24.Height = 0.15625F;
            this.合計24.Left = 9.779201F;
            this.合計24.Name = "合計24";
            this.合計24.OutputFormat = resources.GetString("合計24.OutputFormat");
            this.合計24.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計24.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.合計24.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.合計24.Tag = "";
            this.合計24.Text = "ITEM39";
            this.合計24.Top = 0.6722277F;
            this.合計24.Width = 1.088484F;
            // 
            // 回数総合計
            // 
            this.回数総合計.Height = 0.15625F;
            this.回数総合計.Left = 11.41667F;
            this.回数総合計.Name = "回数総合計";
            this.回数総合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.回数総合計.Tag = "";
            this.回数総合計.Text = null;
            this.回数総合計.Top = 1.028035F;
            this.回数総合計.Width = 0.5562499F;
            // 
            // 数量総合計
            // 
            this.数量総合計.Height = 0.15625F;
            this.数量総合計.Left = 11.49606F;
            this.数量総合計.Name = "数量総合計";
            this.数量総合計.OutputFormat = resources.GetString("数量総合計.OutputFormat");
            this.数量総合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.数量総合計.Tag = "";
            this.数量総合計.Text = null;
            this.数量総合計.Top = 0.4450296F;
            this.数量総合計.Width = 1.341436F;
            // 
            // 金額総合計
            // 
            this.金額総合計.Height = 0.15625F;
            this.金額総合計.Left = 11.25163F;
            this.金額総合計.Name = "金額総合計";
            this.金額総合計.OutputFormat = resources.GetString("金額総合計.OutputFormat");
            this.金額総合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.金額総合計.Tag = "";
            this.金額総合計.Text = null;
            this.金額総合計.Top = 0.6720473F;
            this.金額総合計.Width = 1.341436F;
            // 
            // HNYR1041R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.5708662F;
            this.PageSettings.Margins.Right = 0.5708662F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.18898F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.頁)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.年度)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.手数料計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主コード)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.回数計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.数量計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.金額計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.手数料総合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.回数総合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.数量総合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.金額総合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Line 直線0;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 頁;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 年度;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル8;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル10;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル11;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル12;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル13;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル14;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル15;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル16;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル17;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル18;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル19;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル20;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル21;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル22;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル23;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル24;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル25;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル26;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル27;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 船主コード;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 船主名;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 回数計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 数量計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 金額計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 手数料計;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル73;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 回数総合計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 数量総合計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 金額総合計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 手数料総合計;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル127;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
    }
}
