﻿using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsr1021
{
    /// <summary>
    /// HNSR1021R の概要の説明です。
    /// </summary>
    public partial class HNSR1021R : BaseReport
    {

        public HNSR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void groupFooter1_Format(object sender, System.EventArgs e)
        {

            if (null == textBox1.Value)
            {
                textBox10.Value = 0;
            }
            else if (string.IsNullOrEmpty(textBox1.Value.ToString()))
            {
                textBox10.Value = 0;
            }
            else if ("0" == textBox1.Value.ToString())
            {
                textBox10.Value = 0;
            }
            else
            {
                textBox10.Value = Util.ToInt(textBox8.Value) / Util.ToDecimal(textBox1.Value);
            }
        }

        private void groupFooter2_Format(object sender, System.EventArgs e)
        {

            if (null == テキスト56.Value)
            {
                textBox11.Value = 0;
            }
            else if (string.IsNullOrEmpty(テキスト56.Value.ToString()))
            {
                textBox11.Value = 0;
            }
            else if ("0" == テキスト56.Value.ToString())
            {
                textBox11.Value = 0;
            }
            else
            {
                textBox11.Value = Util.ToInt(textBox6.Value) / Util.ToDecimal(テキスト56.Value);
            }
        }
    }
}
