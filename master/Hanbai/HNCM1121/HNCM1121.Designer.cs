﻿namespace jp.co.fsi.hn.hncm1121
{
    partial class HNCM1121
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNakagaiNm = new System.Windows.Forms.Label();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.txtNakagaiNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "仲買人CD変換マスタの登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblNakagaiNm
            // 
            this.lblNakagaiNm.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaiNm.Location = new System.Drawing.Point(12, 48);
            this.lblNakagaiNm.Name = "lblNakagaiNm";
            this.lblNakagaiNm.Size = new System.Drawing.Size(306, 25);
            this.lblNakagaiNm.TabIndex = 1;
            this.lblNakagaiNm.Text = "仲 買 人 名";
            this.lblNakagaiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Location = new System.Drawing.Point(12, 77);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(540, 296);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // txtNakagaiNm
            // 
            this.txtNakagaiNm.AllowDrop = true;
            this.txtNakagaiNm.AutoSizeFromLength = false;
            this.txtNakagaiNm.DisplayLength = null;
            this.txtNakagaiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNakagaiNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtNakagaiNm.Location = new System.Drawing.Point(98, 50);
            this.txtNakagaiNm.Name = "txtNakagaiNm";
            this.txtNakagaiNm.Size = new System.Drawing.Size(215, 20);
            this.txtNakagaiNm.TabIndex = 2;
            this.txtNakagaiNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaNm_Validating);
            // 
            // HNCM1121
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.txtNakagaiNm);
            this.Controls.Add(this.lblNakagaiNm);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1121";
            this.Text = "仲買人コード変換マスタメンテ";
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblNakagaiNm, 0);
            this.Controls.SetChildIndex(this.txtNakagaiNm, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNakagaiNm;
        private System.Windows.Forms.DataGridView dgvList;
        private jp.co.fsi.common.controls.FsiTextBox txtNakagaiNm;


    }
}