﻿namespace jp.co.fsi.hn.hncm1121
{
    partial class HNCM1122
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKenNakaCd = new System.Windows.Forms.Label();
            this.txtKenNakaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKenNakaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenNakaNm = new System.Windows.Forms.Label();
            this.txtNakagaiNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblNakagaiNm = new System.Windows.Forms.Label();
            this.txtNakagaiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblNakagaiCd = new System.Windows.Forms.Label();
            this.gbxNagoGyo = new System.Windows.Forms.GroupBox();
            this.gbxGinozaGyo = new System.Windows.Forms.GroupBox();
            this.pnlDebug.SuspendLayout();
            this.gbxNagoGyo.SuspendLayout();
            this.gbxGinozaGyo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(274, 23);
            this.lblTitle.Text = "";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 166);
            this.pnlDebug.Size = new System.Drawing.Size(307, 100);
            // 
            // lblKenNakaCd
            // 
            this.lblKenNakaCd.BackColor = System.Drawing.Color.Silver;
            this.lblKenNakaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenNakaCd.Location = new System.Drawing.Point(10, 20);
            this.lblKenNakaCd.Name = "lblKenNakaCd";
            this.lblKenNakaCd.Size = new System.Drawing.Size(159, 25);
            this.lblKenNakaCd.TabIndex = 1;
            this.lblKenNakaCd.Text = "名護仲買人CD";
            this.lblKenNakaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKenNakaCd
            // 
            this.txtKenNakaCd.AutoSizeFromLength = false;
            this.txtKenNakaCd.DisplayLength = null;
            this.txtKenNakaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenNakaCd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtKenNakaCd.Location = new System.Drawing.Point(114, 22);
            this.txtKenNakaCd.MaxLength = 6;
            this.txtKenNakaCd.Name = "txtKenNakaCd";
            this.txtKenNakaCd.Size = new System.Drawing.Size(52, 20);
            this.txtKenNakaCd.TabIndex = 1;
            this.txtKenNakaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKenNakaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenNakaCd_Validating);
            // 
            // txtKenNakaNm
            // 
            this.txtKenNakaNm.AutoSizeFromLength = false;
            this.txtKenNakaNm.DisplayLength = null;
            this.txtKenNakaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenNakaNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKenNakaNm.Location = new System.Drawing.Point(114, 56);
            this.txtKenNakaNm.MaxLength = 20;
            this.txtKenNakaNm.Name = "txtKenNakaNm";
            this.txtKenNakaNm.Size = new System.Drawing.Size(135, 20);
            this.txtKenNakaNm.TabIndex = 2;
            this.txtKenNakaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKenNakaNm_KeyDown);
            this.txtKenNakaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenNakaNm_Validating);
            // 
            // lblKenNakaNm
            // 
            this.lblKenNakaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKenNakaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKenNakaNm.Location = new System.Drawing.Point(10, 54);
            this.lblKenNakaNm.Name = "lblKenNakaNm";
            this.lblKenNakaNm.Size = new System.Drawing.Size(243, 25);
            this.lblKenNakaNm.TabIndex = 3;
            this.lblKenNakaNm.Text = "名護仲買人名";
            this.lblKenNakaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNakagaiNm
            // 
            this.txtNakagaiNm.AutoSizeFromLength = false;
            this.txtNakagaiNm.DisplayLength = null;
            this.txtNakagaiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNakagaiNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtNakagaiNm.Location = new System.Drawing.Point(114, 58);
            this.txtNakagaiNm.MaxLength = 20;
            this.txtNakagaiNm.Name = "txtNakagaiNm";
            this.txtNakagaiNm.Size = new System.Drawing.Size(135, 20);
            this.txtNakagaiNm.TabIndex = 4;
            this.txtNakagaiNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNakagaiNm_KeyDown);
            this.txtNakagaiNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaiNm_Validating);
            // 
            // lblNakagaiNm
            // 
            this.lblNakagaiNm.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaiNm.Location = new System.Drawing.Point(10, 56);
            this.lblNakagaiNm.Name = "lblNakagaiNm";
            this.lblNakagaiNm.Size = new System.Drawing.Size(243, 25);
            this.lblNakagaiNm.TabIndex = 5;
            this.lblNakagaiNm.Text = "仲 買 人 名";
            this.lblNakagaiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNakagaiCd
            // 
            this.txtNakagaiCd.AutoSizeFromLength = false;
            this.txtNakagaiCd.DisplayLength = null;
            this.txtNakagaiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNakagaiCd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtNakagaiCd.Location = new System.Drawing.Point(114, 24);
            this.txtNakagaiCd.MaxLength = 6;
            this.txtNakagaiCd.Name = "txtNakagaiCd";
            this.txtNakagaiCd.Size = new System.Drawing.Size(52, 20);
            this.txtNakagaiCd.TabIndex = 3;
            this.txtNakagaiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaiCd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNakagaiCd_KeyDown);
            this.txtNakagaiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaiCd_Validating);
            // 
            // lblNakagaiCd
            // 
            this.lblNakagaiCd.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaiCd.Location = new System.Drawing.Point(10, 22);
            this.lblNakagaiCd.Name = "lblNakagaiCd";
            this.lblNakagaiCd.Size = new System.Drawing.Size(159, 25);
            this.lblNakagaiCd.TabIndex = 7;
            this.lblNakagaiCd.Text = "仲買人CD";
            this.lblNakagaiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxNagoGyo
            // 
            this.gbxNagoGyo.Controls.Add(this.txtKenNakaNm);
            this.gbxNagoGyo.Controls.Add(this.txtKenNakaCd);
            this.gbxNagoGyo.Controls.Add(this.lblKenNakaCd);
            this.gbxNagoGyo.Controls.Add(this.lblKenNakaNm);
            this.gbxNagoGyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxNagoGyo.Location = new System.Drawing.Point(12, 12);
            this.gbxNagoGyo.Name = "gbxNagoGyo";
            this.gbxNagoGyo.Size = new System.Drawing.Size(275, 91);
            this.gbxNagoGyo.TabIndex = 902;
            this.gbxNagoGyo.TabStop = false;
            this.gbxNagoGyo.Text = "名護漁協";
            // 
            // gbxGinozaGyo
            // 
            this.gbxGinozaGyo.Controls.Add(this.txtNakagaiNm);
            this.gbxGinozaGyo.Controls.Add(this.txtNakagaiCd);
            this.gbxGinozaGyo.Controls.Add(this.lblNakagaiNm);
            this.gbxGinozaGyo.Controls.Add(this.lblNakagaiCd);
            this.gbxGinozaGyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxGinozaGyo.Location = new System.Drawing.Point(12, 109);
            this.gbxGinozaGyo.Name = "gbxGinozaGyo";
            this.gbxGinozaGyo.Size = new System.Drawing.Size(275, 94);
            this.gbxGinozaGyo.TabIndex = 903;
            this.gbxGinozaGyo.TabStop = false;
            this.gbxGinozaGyo.Text = "漁協";
            // 
            // HNCM1122
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 269);
            this.Controls.Add(this.gbxGinozaGyo);
            this.Controls.Add(this.gbxNagoGyo);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1122";
            this.ShowFButton = true;
            this.Text = "仲買人変換マスタ入力";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxNagoGyo, 0);
            this.Controls.SetChildIndex(this.gbxGinozaGyo, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxNagoGyo.ResumeLayout(false);
            this.gbxNagoGyo.PerformLayout();
            this.gbxGinozaGyo.ResumeLayout(false);
            this.gbxGinozaGyo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKenNakaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKenNakaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKenNakaNm;
        private System.Windows.Forms.Label lblKenNakaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtNakagaiNm;
        private System.Windows.Forms.Label lblNakagaiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtNakagaiCd;
        private System.Windows.Forms.Label lblNakagaiCd;
        private System.Windows.Forms.GroupBox gbxNagoGyo;
        private System.Windows.Forms.GroupBox gbxGinozaGyo;


    }
}