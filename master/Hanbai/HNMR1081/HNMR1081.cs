﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1081
{
    /// <summary>
    /// 販売未収金元帳(HNMR1081)
    /// </summary>
    public partial class HNMR1081 : BasePgForm
    {
        #region 構造体
        public struct MishuRecord
        {
            //public decimal  NAKAGAININ_CD; //コード
            public string NAKAGAININ_NM; //名称
            //public DateTime DENPYO_DATE; //発生日
            public decimal SURYO;//数量
            public decimal KISHU_ZAN;//期首残高
            public decimal KARIKATA;//借方発生
            public decimal KASHIKATA;//貸方発生
            public decimal ZANDAKA;//残高
            public decimal SHOHI_ZEI;//消費税

            public void Clear()
            {
                //NAKAGAININ_CD = 0;  //コード
                NAKAGAININ_NM = ""; //名称
                //DENPYO_DATE = DateTime.Now(); //発生日
                SURYO   = 0;        //数量
                KISHU_ZAN   = 0;    //期首残高
                KARIKATA = 0;       //借方発生
                KASHIKATA = 0;      //貸方発生
                ZANDAKA = 0;        //残高
                SHOHI_ZEI = 0;      //消費税
            }
        }
        #endregion

        private Dictionary<decimal, Dictionary<DateTime, MishuRecord>> ReportData;

        #region 定数
        private const int prtCols = 19;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        // 元帳区分変数
        private decimal MOTOCHO_KUBUN = 11;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1081()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = jpDate[4];
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            rdoZeiKomi.Checked = true;

            // 画面引数が設定されていたら、引数を使用する。
            MOTOCHO_KUBUN = (Util.ToDecimal(this.Par1) == 0) ? MOTOCHO_KUBUN : Util.ToDecimal(this.Par1);
            // 初期フォーカス
            txtDateYearFr.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年),仲買人CDにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtNakagaininCdFr":
                case "txtNakagaininCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                #endregion

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdFr.Text = outData[0];
                                this.lblNakagaininCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdTo.Text = outData[0];
                                this.lblNakagaininCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNMR1081R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 仲買人コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdFr())
            {
                e.Cancel = true;
                this.txtNakagaininCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdTo())
            {
                e.Cancel = true;
                this.txtNakagaininCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 仲買人コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtNakagaininCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 日付範囲の入力チェック(会計年度)
        /// </summary>
        private bool CheckDate()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 日付範囲に該当する会計年度を取得
            ArrayList kaikeiNendoList = Util.GetKaikeiNendo(tmpDateFr, tmpDateTo, this.Dba);
            // 複数年度をまたいでいた場合、エラーとする
            if (kaikeiNendoList.Count > 1)
            {
                Msg.Error("会計年度をまたぐ日付範囲は設定できません。");
                return false;
            }
            else if (kaikeiNendoList.Count == 0)
            {
                Msg.Error("該当する会計年度がありません。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仲買人コード(自)の入力チェック
        /// </summary>
        private bool IsValidNakagaininCdFr()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdFr.Text))
            {
                this.lblNakagaininCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtNakagaininCdFr.Text))
                {
                    Msg.Error("仲買人コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblNakagaininCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", "", this.txtNakagaininCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 仲買人コード(至)の入力チェック
        /// </summary>
        private bool IsValidNakagaininCdTo()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdTo.Text))
            {
                this.lblNakagaininCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtNakagaininCdTo.Text))
                {
                    Msg.Error("仲買人コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblNakagaininCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", "", this.txtNakagaininCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            // 年度またぎチェック
            if (!CheckDate())
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }

            // 仲買人コード(自)の入力チェック
            if (!IsValidNakagaininCdFr())
            {
                this.txtNakagaininCdFr.Focus();
                this.txtNakagaininCdFr.SelectAll();
                return false;
            }
            // 仲買人コード(至)の入力チェック
            if (!IsValidNakagaininCdTo())
            {
                this.txtNakagaininCdTo.Focus();
                this.txtNakagaininCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        ///// <summary>
        ///// 検索サブウィンドウオープン
        ///// </summary>
        ///// <param name="moduleName">例："COMC8111"</param>
        ///// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        ///// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        ///// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        ///// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        //private String[] openSearchWindow(String moduleName, String para1, String indata)
        //{
        //    string[] result = { "", "" };

        //    // ネームスペースに使うモジュール名の小文字
        //    string lowerModuleName = moduleName.ToLower();

        //    // ネームスペースの末尾
        //    string nameSpace = lowerModuleName.Substring(0, 3);

        //    // アセンブリのロード
        //    Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

        //    // フォーム作成
        //    string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
        //    Type t = asm.GetType(moduleNameSpace);

        //    if (t != null)
        //    {
        //        Object obj = Activator.CreateInstance(t);
        //        if (obj != null)
        //        {
        //            BasePgForm frm = (BasePgForm)obj;
        //            frm.Par1 = para1;
        //            frm.InData = indata;
        //            frm.ShowDialog(this);

        //            if (frm.DialogResult == DialogResult.OK)
        //            {
        //                string[] ret = (string[])frm.OutData;
        //                result[0] = ret[0];
        //                result[1] = ret[1];
        //                return result;
        //            }
        //        }
        //    }

        //    return result;
        //}

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNMR1081R rpt = new HNMR1081R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.Text;
                    rpt.Document.Name = this.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpjpDateTo2 = Util.ConvJpDate(tmpDateTo2, this.Dba);

            // 日付範囲に該当する会計年度を取得 ※入力チェックにて確認しているため、該当会計年度は1つの想定。
            ArrayList kaikeiNendoList = Util.GetKaikeiNendo(tmpDateFr, tmpDateTo, this.Dba);
            int kaikeiNendo = int.Parse(kaikeiNendoList[0].ToString());

            // 表示する日付を設定する
            string hyojiDate; // 表示用日付
            string dateFrChk = string.Format("{0}{1}{2}{3}", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4]);
            string dateToChk = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            //string dateFrChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], "1");
            string dateFrChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], "1");
            string dateToChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo2[4]);
            if (dateFrChk == dateFrChk2 && dateToChk == dateToChk2)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = "("+ string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日"+")", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // ループ用カウント変数
            int i = 1; 
            int l = 1; 
            int count = 0;

            // 帳票表示用変数
            Decimal suryo = 0;
            Decimal shohizei = 0;
            Decimal karikataHassei = 0;
            Decimal kashikataHassei = 0;
            Decimal zandaka = 0;
            Decimal zenZandaka = 0;
            string nakagaininCd = "";
            string hasseibi = "";

            // 仲買人コード設定
            string nakagaininCdFr;
            string nakagaininCdTo;
            if (Util.ToDecimal(txtNakagaininCdFr.Text) > 0)
            {
                nakagaininCdFr = txtNakagaininCdFr.Text;
            }
            else
            {
                nakagaininCdFr = "0";
            }
            if (Util.ToDecimal(txtNakagaininCdTo.Text) > 0)
            {
                nakagaininCdTo = txtNakagaininCdTo.Text;
            }
            else
            {
                nakagaininCdTo = "9999";
            }
            #region 支所コードの退避
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            #endregion

            // 選択した日付に発生しているデータの取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            Sql.Append(" SELECT ");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd!="0")
            {
                Sql.Append(" A.SHISHO_CD,");
            }
            else
            {
                Sql.Append(" 0 AS SHISHO_CD,");
            }
            Sql.Append(" A.NAKAGAININ_CD,");
            Sql.Append(" A.NAKAGAININ_NM,");
            Sql.Append(" A.DENPYO_DATE,");
            Sql.Append(" A.SURYO,");
            Sql.Append(" A.KINGAKU,");
            Sql.Append(" A.SHOHIZEI,");
            Sql.Append(" 0 AS NYUKIN_KINGAKU,");
            Sql.Append(" B.ZANDAKA");
            Sql.Append(" FROM ");
            Sql.Append(" (SELECT ");
            Sql.Append(" KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD,");
            Sql.Append(" DENPYO_DATE,");
            Sql.Append(" NAKAGAININ_CD,");
            Sql.Append(" NAKAGAININ_NM,");
            Sql.Append(" SURYO AS SURYO,");
            Sql.Append(" ZEINUKI_KINGAKU AS KINGAKU,");
            Sql.Append(" dbo.FNC_MarumeS(ZEINUKI_KINGAKU * (ZEI_RITSU / 100), 0, SHOHIZEI_HASU_SHORI) AS SHOHIZEI ");
            Sql.Append(" FROM VI_HN_HIBETSU_NAKAGAININ_DENPYO");
            Sql.Append(" WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_DATE >= @DENPYO_DATE_FR AND");
            Sql.Append(" DENPYO_DATE <= @DENPYO_DATE_TO AND");
            Sql.Append(" NAKAGAININ_CD >= @NAKAGAININ_CD_FR AND ");
            Sql.Append(" NAKAGAININ_CD <= @NAKAGAININ_CD_TO) A");
            Sql.Append(" LEFT JOIN (");
            Sql.Append(" SELECT ");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.HOJO_KAMOKU_CD AS NAKAGAININ_CD,");
            Sql.Append(" SUM(CASE WHEN A.TAISHAKU_KUBUN = C.TAISHAKU_KUBUN THEN A.ZEINUKI_KINGAKU ELSE -1 * A.ZEINUKI_KINGAKU END) AS ZANDAKA");
            Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI A");
            Sql.Append(" INNER JOIN TB_HN_KISHU_ZAN_KAMOKU B");
            Sql.Append(" ON	A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD");
            Sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            Sql.Append(" AND B.MOTOCHO_KUBUN = @MOTOCHO_KUBUN ");
            Sql.Append(" LEFT JOIN TB_ZM_KANJO_KAMOKU C");
            Sql.Append(" ON	A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append(" AND A.KAIKEI_NENDO = C.KAIKEI_NENDO");
            Sql.Append(" AND A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append(" A.HOJO_KAMOKU_CD >= @NAKAGAININ_CD_FR AND ");
            Sql.Append(" A.HOJO_KAMOKU_CD <= @NAKAGAININ_CD_TO AND");
            Sql.Append(" A.DENPYO_DATE < @DENPYO_DATE_FR");
            Sql.Append(" GROUP BY");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.HOJO_KAMOKU_CD ) B");
            Sql.Append(" ON	A.KAISHA_CD = B.KAISHA_CD");
            if (shishoCd != "0")
                Sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD");
            Sql.Append(" AND A.NAKAGAININ_CD = B.NAKAGAININ_CD");
            Sql.Append(" UNION ALL");
            Sql.Append(" SELECT ");
            Sql.Append(" C.KAISHA_CD,");
            if (shishoCd != "0")
            {
                Sql.Append(" C.SHISHO_CD,");
            }
            else
            {
                Sql.Append(" 0 AS SHISHO_CD,");
            }
            Sql.Append(" C.NAKAGAININ_CD,");
            Sql.Append(" C.NAKAGAININ_NM,");
            Sql.Append(" C.DENPYO_DATE,");
            Sql.Append(" C.SURYO,");
            Sql.Append(" C.KINGAKU,");
            Sql.Append(" C.SHOHIZEI,");
            Sql.Append(" C.NYUKIN_KINGAKU,");
            Sql.Append(" D.ZANDAKA");
            Sql.Append(" FROM ");
            Sql.Append(" (SELECT ");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.TORIHIKISAKI_CD AS NAKAGAININ_CD,");
            Sql.Append(" MAX(A.TORIHIKISAKI_NM) AS NAKAGAININ_NM,");
            Sql.Append(" A.DENPYO_DATE,");
            Sql.Append(" 0	AS SURYO,");
            Sql.Append(" 0	AS KINGAKU,");
            Sql.Append(" 0	AS SHOHIZEI,");
            Sql.Append(" SUM(A.ZEIKOMI_KINGAKU) AS NYUKIN_KINGAKU,");
            Sql.Append(" 0	AS ZANDAKA");
            Sql.Append(" FROM VI_HN_NAKAGAININ_NYUKIN A");
            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append(" A.DENPYO_DATE >= @DENPYO_DATE_FR AND");
            Sql.Append(" A.DENPYO_DATE <= @DENPYO_DATE_TO AND ");
            Sql.Append(" A.TORIHIKISAKI_CD >= @NAKAGAININ_CD_FR AND ");
            Sql.Append(" A.TORIHIKISAKI_CD <= @NAKAGAININ_CD_TO AND ");
            Sql.Append(" A.MOTOCHO_KUBUN = @MOTOCHO_KUBUN");
            Sql.Append(" GROUP BY");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.DENPYO_DATE,");
            Sql.Append(" A.TORIHIKISAKI_CD) C");
            Sql.Append(" LEFT JOIN (");
            Sql.Append(" SELECT ");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.HOJO_KAMOKU_CD AS NAKAGAININ_CD,");
            Sql.Append(" SUM(CASE WHEN A.TAISHAKU_KUBUN = C.TAISHAKU_KUBUN THEN A.ZEINUKI_KINGAKU ELSE -1 * A.ZEINUKI_KINGAKU END) AS ZANDAKA");
            Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI A");
            Sql.Append(" INNER JOIN TB_HN_KISHU_ZAN_KAMOKU B");
            Sql.Append(" ON	A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" AND	A.SHISHO_CD = B.SHISHO_CD");
            Sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            Sql.Append(" AND B.MOTOCHO_KUBUN = @MOTOCHO_KUBUN");
            Sql.Append(" LEFT JOIN TB_ZM_KANJO_KAMOKU C");
            Sql.Append(" ON	A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append(" AND A.KAIKEI_NENDO = C.KAIKEI_NENDO");
            Sql.Append(" AND A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append(" A.HOJO_KAMOKU_CD >= @NAKAGAININ_CD_FR AND ");
            Sql.Append(" A.HOJO_KAMOKU_CD <= @NAKAGAININ_CD_TO AND");
            Sql.Append(" A.DENPYO_DATE < @DENPYO_DATE_FR");
            Sql.Append(" GROUP BY");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.HOJO_KAMOKU_CD ) D");
            Sql.Append(" ON	C.KAISHA_CD = D.KAISHA_CD");
            if (shishoCd != "0")
                Sql.Append(" AND C.SHISHO_CD = D.SHISHO_CD");
            Sql.Append(" AND C.NAKAGAININ_CD = D.NAKAGAININ_CD");
            Sql.Append(" ORDER BY");
            Sql.Append(" KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD,");
            Sql.Append(" NAKAGAININ_CD,");
            Sql.Append(" DENPYO_DATE");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@NAKAGAININ_CD_FR", SqlDbType.Decimal, 4, nakagaininCdFr);
            dpc.SetParam("@NAKAGAININ_CD_TO", SqlDbType.Decimal, 4, nakagaininCdTo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                Decimal RzengetsuZan = 0;
                Decimal RkariKingaku = 0;
                Decimal RkashiKingaku = 0;
                Decimal Rshohizei = 0;
                Decimal Rsuryo = 0;

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    if (!nakagaininCd.Equals(Util.ToString(dr["NAKAGAININ_CD"])) || !hasseibi.Equals(Util.ToString(dr["DENPYO_DATE"])))
                    {
                        #region コメント（不要？）
                        /*
                       //仲買人の日付毎のデータ数を取得
                       dpc = new DbParamCollection();
                       Sql = new StringBuilder();
                       Sql.Append(" SELECT ");
                       Sql.Append("     A.NAKAGAININ_CD   AS NAKAGAININ_CD, ");
                       Sql.Append("     A.NAKAGAININ_NM AS NAKAGAININ_NM, ");
                       Sql.Append("     A.SERIBI     AS SERIBI , ");
                       Sql.Append("     A.SURYO       AS SURYO, ");
                       Sql.Append("     A.KINGAKU       AS KINGAKU, ");
                       Sql.Append("     A.SHOHIZEI     AS SHOHIZEI, ");
                       Sql.Append("     0            AS ZEIKOMI_KINGAKU, ");
                       Sql.Append("     B.ZANDAKA       AS ZANDAKA ");
                       Sql.Append(" FROM ");
                       Sql.Append("     VI_HN_SNGDIKN_MISI_KEN_SIKS AS A ");
                       Sql.Append(" LEFT OUTER JOIN ");
                       Sql.Append("     (SELECT A.KAISHA_CD, ");
                       Sql.Append("     A.TORIHIKISAKI_CD AS KAIIN_BANGO, ");
                       Sql.Append("     MIN(A.TORIHIKISAKI_NM) AS KAIIN_NM,");
                       Sql.Append("     MIN(E.SHOHIZEI_HASU_SHORI) AS SHOHIZEI_HASU_SHORI,");
                       Sql.Append("     MIN(2) AS SHOHIZEI_TENKA_HOHO,");
                       Sql.Append("     MIN(A.DENWA_BANGO) AS DENWA_BANGO,");
                       Sql.Append("     MIN(A.FAX_BANGO) AS FAX_BANGO,");
                       Sql.Append("      SUM(CASE WHEN C.TAISHAKU_KUBUN = D .TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * - 1) ");
                       Sql.Append("     END) AS ZANDAKA ");
                       Sql.Append("     FROM  VI_HN_TORIHIKISAKI_JOHO AS A ");
                       Sql.Append("     LEFT OUTER JOIN ");
                       Sql.Append("     TB_HN_KISHU_ZAN_KAMOKU AS B ");
                       Sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD AND ");
                       Sql.Append("      2 = B.MOTOCHO_KUBUN ");
                       Sql.Append("     LEFT OUTER JOIN ");
                       Sql.Append("     TB_ZM_SHIWAKE_MEISAI AS C ");
                       Sql.Append("     ON A.KAISHA_CD = C.KAISHA_CD AND ");
                       Sql.Append("      A.TORIHIKISAKI_CD = C.HOJO_KAMOKU_CD AND ");
                       Sql.Append("      B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
                       Sql.Append("     LEFT OUTER JOIN ");
                       Sql.Append("     TB_ZM_KANJO_KAMOKU AS D ");
                       Sql.Append("     ON B.KAISHA_CD = D .KAISHA_CD AND ");
                       Sql.Append("      B.KANJO_KAMOKU_CD = D .KANJO_KAMOKU_CD AND");
                       Sql.Append(" 	 C.KAIKEI_NENDO = D.KAIKEI_NENDO");
                       Sql.Append("     LEFT OUTER JOIN ");
                       Sql.Append("      TB_ZM_SHOHIZEI_JOHO AS E ");
                       Sql.Append("     ON A.KAISHA_CD = E.KAISHA_CD AND");
                       Sql.Append("       E.KESSANKI = @KESSANKI AND ");
                       Sql.Append(" 	  C.KAIKEI_NENDO = E.KAIKEI_NENDO");
                       Sql.Append("      WHERE ");
                       Sql.Append("     A.TORIHIKISAKI_CD BETWEEN '0' AND '9999' AND ");
                       Sql.Append("     A.TORIHIKISAKI_KUBUN2 = 2 AND ");
                       Sql.Append("     C.DENPYO_DATE < @DENPYO_DATE_FR AND");
                       Sql.Append(" 	C.KAIKEI_NENDO = @KAIKEI_NENDO");
                       Sql.Append("      GROUP BY A.KAISHA_CD, A.TORIHIKISAKI_CD) AS B  ");
                       Sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD AND ");
                       Sql.Append("     A.NAKAGAININ_CD = B.KAIIN_BANGO ");
                       Sql.Append(" WHERE ");
                       Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND ");
                       Sql.Append("     A.SERIBI Between @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND ");
                       Sql.Append("     NAKAGAININ_CD Between @NAKAGAININ_CD_FR AND @NAKAGAININ_CD_TO ");
                       Sql.Append("     AND A.SEISAN_KUBUN = 3 ");
                       Sql.Append(" UNION ALL ");
                       Sql.Append(" SELECT ");
                       Sql.Append("     TORIHIKISAKI_CD  AS NAKAGAININ_CD, ");
                       Sql.Append("     TORIHIKISAKI_NM      AS NAKAGAININ_NM, ");
                       Sql.Append("     DENPYO_DATE      AS SERIBI, ");
                       Sql.Append("     0             AS SURYO, ");
                       Sql.Append("     0             AS KINGAKU, ");
                       Sql.Append("     0             AS SHOHIZEI, ");
                       Sql.Append("     SUM(ZEIKOMI_KINGAKU) AS ZEIKOMI_KINGAKU, ");
                       Sql.Append("     B.ZANDAKA         AS ZANDAKA ");
                       Sql.Append(" FROM ");
                       Sql.Append("     VI_HN_NAKAGAININ_NYUKIN AS A ");
                       Sql.Append(" LEFT OUTER JOIN ");
                       Sql.Append("     (SELECT A.KAISHA_CD, A.TORIHIKISAKI_CD AS KAIIN_BANGO, MIN(A.TORIHIKISAKI_NM) AS KAIIN_NM,");
                       Sql.Append("     MIN(E.SHOHIZEI_HASU_SHORI) AS SHOHIZEI_HASU_SHORI,");
                       Sql.Append("     MIN(2) AS SHOHIZEI_TENKA_HOHO,");
                       Sql.Append("     MIN(A.DENWA_BANGO) AS DENWA_BANGO,");
                       Sql.Append("     MIN(A.FAX_BANGO) AS FAX_BANGO,");
                       Sql.Append("      SUM(CASE WHEN C.TAISHAKU_KUBUN = D .TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * - 1) ");
                       Sql.Append("     END) AS ZANDAKA ");
                       Sql.Append("     FROM  ");
                       Sql.Append("     VI_HN_TORIHIKISAKI_JOHO AS A ");
                       Sql.Append("     LEFT OUTER JOIN ");
                       Sql.Append("     TB_HN_KISHU_ZAN_KAMOKU AS B ");
                       Sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD AND ");
                       Sql.Append("      2 = B.MOTOCHO_KUBUN ");
                       Sql.Append("     LEFT OUTER JOIN ");
                       Sql.Append("     TB_ZM_SHIWAKE_MEISAI AS C ");
                       Sql.Append("     ON A.KAISHA_CD = C.KAISHA_CD AND ");
                       Sql.Append("      A.TORIHIKISAKI_CD = C.HOJO_KAMOKU_CD AND ");
                       Sql.Append("      B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
                       Sql.Append("     LEFT OUTER JOIN ");
                       Sql.Append("     TB_ZM_KANJO_KAMOKU AS D ");
                       Sql.Append("     ON B.KAISHA_CD = D .KAISHA_CD AND ");
                       Sql.Append("      B.KANJO_KAMOKU_CD = D .KANJO_KAMOKU_CD AND");
                       Sql.Append(" 	 C.KAIKEI_NENDO = D.KAIKEI_NENDO");
                       Sql.Append("     LEFT OUTER JOIN ");
                       Sql.Append("      TB_ZM_SHOHIZEI_JOHO AS E ");
                       Sql.Append("     ON A.KAISHA_CD = E.KAISHA_CD AND");
                       Sql.Append("       E.KESSANKI = @KESSANKI AND ");
                       Sql.Append(" 	  C.KAIKEI_NENDO = E.KAIKEI_NENDO");
                       Sql.Append("      WHERE ");
                       Sql.Append("     A.TORIHIKISAKI_CD BETWEEN '0' AND '9999' AND ");
                       Sql.Append("     A.TORIHIKISAKI_KUBUN2 = 2 AND");
                       Sql.Append("     C.DENPYO_DATE < @DENPYO_DATE_FR AND");
                       Sql.Append(" 	C.KAIKEI_NENDO = @KAIKEI_NENDO");
                       Sql.Append("      GROUP BY A.KAISHA_CD, A.TORIHIKISAKI_CD) AS B  ");
                       Sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD AND ");
                       Sql.Append("     A.TORIHIKISAKI_CD = B.KAIIN_BANGO ");
                       Sql.Append(" WHERE ");
                       Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND ");
                       Sql.Append("     A.DENPYO_DATE Between @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND ");
                       Sql.Append("     A.TORIHIKISAKI_CD = @NAKAGAININ_CD_FR  ");
                       Sql.Append(" GROUP BY TORIHIKISAKI_CD, TORIHIKISAKI_NM, DENPYO_DATE, ZANDAKA");
                       Sql.Append(" ORDER BY NAKAGAININ_CD, SERIBI");

                       dpc.SetParam("@KAISHA_CD", Sql.Type.Decimal, 4, this.UInfo.KaishaCd);
                       dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                       dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 2, this.UInfo.KessanKi);
                       dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, Util.ToDate(dr["SERIBI"]));
                       dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, Util.ToDate(dr["SERIBI"]));
                       dpc.SetParam("@NAKAGAININ_CD_FR", SqlDbType.VarChar, 4, Util.ToString(dr["NAKAGAININ_CD"]));
                       dpc.SetParam("@NAKAGAININ_CD_TO", SqlDbType.VarChar, 4, Util.ToString(dr["NAKAGAININ_CD"]));

                        DataTable dtCount = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                        count = dtCount.Rows.Count;
                        */
                        #endregion
                        ////DataRow[] dtCount = dtMainLoop.Select("NAKAGAININ_CD = " + "\'" + Util.ToDecimal(dr["NAKAGAININ_CD"]) + "\' AND SERIBI = " + string.Format(dr["SERIBI"].ToString(), "#{0}#"));
                        //DataRow[] dtCount = dtMainLoop.Select("NAKAGAININ_CD = " + "\'" + Util.ToDecimal(dr["NAKAGAININ_CD"]) + "\' AND SERIBI = " + "\'" + dr["SERIBI"] + "\'");
                        DataRow[] dtCount = dtMainLoop.Select("NAKAGAININ_CD = " + "\'" + Util.ToDecimal(dr["NAKAGAININ_CD"]) + "\' AND DENPYO_DATE = " + "\'" + dr["DENPYO_DATE"] + "\'");
                        count = dtCount.Length;
                    }

                    if (nakagaininCd == "" || !nakagaininCd.Equals(Util.ToString(dr["NAKAGAININ_CD"])))
                    {
                        zandaka = Util.ToDecimal(dr["ZANDAKA"]);
                        zenZandaka = Util.ToDecimal(dr["ZANDAKA"]);
                        RzengetsuZan = 0;
                        RkariKingaku = 0;
                        RkashiKingaku = 0;
                        Rshohizei = 0;
                        Rsuryo = 0;

                        // 累計　前月残高
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append(" SELECT ");
                        Sql.Append(" C.KANJO_KAMOKU_CD AS KAIIN_BANGO,");
                        Sql.Append(" SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA ");
                        Sql.Append(" ,SUM(CASE WHEN C.DENPYO_DATE < @DATE_FR  THEN         (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END)     ELSE 0 END)     AS KISHU_ZAN");
                        Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI  AS C ");
                        Sql.Append(" INNER JOIN TB_HN_KISHU_ZAN_KAMOKU  AS B ");
                        Sql.Append(" ON	 C.KAISHA_CD = B.KAISHA_CD ");
                        Sql.Append(" AND C.SHISHO_CD = B.SHISHO_CD");
                        Sql.Append(" AND C.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
                        Sql.Append(" AND B.MOTOCHO_KUBUN = @MOTOCHO_KUBUN ");
                        Sql.Append(" AND C.DENPYO_KUBUN = 0  ");
                        Sql.Append(" LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS A ");
                        Sql.Append(" ON  A.KAISHA_CD     = C.KAISHA_CD ");
                        Sql.Append(" AND A.TORIHIKISAKI_CD = C.HOJO_KAMOKU_CD ");
                        Sql.Append(" LEFT OUTER JOIN       TB_ZM_KANJO_KAMOKU  AS D ");
                        Sql.Append(" ON	C.KAISHA_CD     = D.KAISHA_CD ");
                        Sql.Append(" AND C.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD ");
                        Sql.Append(" AND C.KAIKEI_NENDO = D.KAIKEI_NENDO ");
                        Sql.Append(" WHERE");
                        Sql.Append("    C.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
                        Sql.Append("    A.KAISHA_CD = @KAISHA_CD AND");
                        if (shishoCd != "0")
                            Sql.Append("    C.SHISHO_CD = @SHISHO_CD AND");
                        Sql.Append("    C.HOJO_KAMOKU_CD   = @TORIHIKISAKI_CD ");
                        Sql.Append(" GROUP BY");
                        Sql.Append("     C.KANJO_KAMOKU_CD");

                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 4, this.UInfo.KessanKi);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                        dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 5, dr["NAKAGAININ_CD"]);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                        dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);

                        DataTable dtRZenZan = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                        // 累計　前月残高の計算
                        foreach (DataRow dr2 in dtRZenZan.Rows)
                        {
                            //RzengetsuZan += Util.ToDecimal(dr2["ZANDAKA"]);
                            RzengetsuZan += Util.ToDecimal(dr2["KISHU_ZAN"]);
                        }

                        // 累計　当月貸方発生
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append(" SELECT ");
                        Sql.Append(" B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
                        Sql.Append(" SUM(B.ZEIKOMI_KINGAKU) AS ZEIKOMI_KINGAKU");
                        Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS B ");
                        Sql.Append(" INNER JOIN TB_HN_NYUKIN_KAMOKU AS A ");
                        Sql.Append(" ON  B.KAISHA_CD = A.KAISHA_CD");
                        Sql.Append(" AND B.SHISHO_CD = A.SHISHO_CD");
                        Sql.Append(" AND B.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD ");
                        Sql.Append(" AND 1           = B.DENPYO_KUBUN   ");
                        Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C ");
                        Sql.Append(" ON 	A.KAISHA_CD     	= C.KAISHA_CD");
                        Sql.Append(" AND A.KANJO_KAMOKU_CD 	= C.KANJO_KAMOKU_CD");
                        Sql.Append(" AND B.KAIKEI_NENDO = C.KAIKEI_NENDO ");
                        Sql.Append(" WHERE");
                        Sql.Append("     B.KAISHA_CD     = @KAISHA_CD AND");
                        if (shishoCd != "0")
                            Sql.Append("     B.SHISHO_CD     = @SHISHO_CD AND");
                        Sql.Append("     A.MOTOCHO_KUBUN       = @MOTOCHO_KUBUN AND");
                        Sql.Append(" 	 B.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                        Sql.Append("     B.HOJO_KAMOKU_CD  = @HOJO_KAMOKU_CD AND");
                        Sql.Append("     B.DENPYO_DATE >= @DATE_FR AND ");
                        Sql.Append("     B.DENPYO_DATE <= @DATE_TO AND ");
                        Sql.Append("     B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN");
                        Sql.Append(" GROUP BY ");
                        Sql.Append(" B.HOJO_KAMOKU_CD ");

                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                        dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/04/01"));
                        dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 5, dr["NAKAGAININ_CD"]);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                        dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);

                        DataTable dtRKarikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                        // 累計　借方金額の計算
                        foreach (DataRow dr2 in dtRKarikata.Rows)
                        {
                            RkariKingaku += Util.ToDecimal(dr2["ZEIKOMI_KINGAKU"]);
                        }

                        // 累計　数量、当月借方、消費税
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append(" SELECT ");
                        Sql.Append(" A.NAKAGAININ_CD	AS TORIHIKISAKI_CD,");
                        Sql.Append(" A.ZEI_RITSU     AS ZEI_RITSU,");
                        Sql.Append(" SUM(A.SURYO) 	AS MIZUAGE_SURYO,");
                        Sql.Append(" SUM(A.ZEIKOMI_KINGAKU) AS ZEIKOMI_KINGAKU,");
                        Sql.Append(" SUM(A.ZEINUKI_KINGAKU) AS ZEINUKI_KINGAKU,");
                        Sql.Append(" dbo.FNC_MarumeS(SUM(A.ZEINUKI_KINGAKU) * (A.ZEI_RITSU / 100), 0, A.SHOHIZEI_HASU_SHORI) AS SHOHIZEI ");
                        Sql.Append(" FROM VI_HN_HIBETSU_NAKAGAININ_DENPYO A ");
                        Sql.Append(" WHERE ");
                        Sql.Append(" A.KAISHA_CD     = @KAISHA_CD AND ");
                        if (shishoCd != "0")
                            Sql.Append(" A.SHISHO_CD     = @SHISHO_CD AND ");
                        Sql.Append(" A.DENPYO_DATE   >= @DATE_FR AND ");
                        Sql.Append(" A.DENPYO_DATE   <= @DATE_TO AND ");
                        Sql.Append(" A.NAKAGAININ_CD = @NAKAGAININ_CD ");
                        Sql.Append(" GROUP BY ");
                        Sql.Append(" A.NAKAGAININ_CD,");
                        Sql.Append(" A.SHOHIZEI_HASU_SHORI,");
                        Sql.Append(" A.ZEI_RITSU ");
                        Sql.Append(" ORDER BY ");
                        Sql.Append(" A.NAKAGAININ_CD ");

                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                        dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/04/01"));
                        dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 5, dr["NAKAGAININ_CD"]);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

                        DataTable dtRKashikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                        // 累計　数量、消費税、借方金額の計算
                        foreach (DataRow dr2 in dtRKashikata.Rows)
                        {
                            Rsuryo += Util.ToDecimal(dr2["MIZUAGE_SURYO"]);
                            Rshohizei += Util.ToDecimal(dr2["SHOHIZEI"]);
                            if (this.rdoZeiKomi.Checked == true)
                            {
                                RkashiKingaku += Util.ToDecimal(dr2["ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr2["SHOHIZEI"]);
                            }
                            else
                            {
                                RkashiKingaku += Util.ToDecimal(dr2["ZEINUKI_KINGAKU"]);
                            }
                        }
                    }

                    // 金額の積立
                    suryo += Util.ToDecimal(dr["SURYO"]);
                    if (this.rdoZeiKomi.Checked == true)
                    {
                        karikataHassei += Util.ToDecimal(dr["KINGAKU"]) + Util.ToDecimal(dr["SHOHIZEI"]);
                    }
                    else
                    {
                        karikataHassei += Util.ToDecimal(dr["KINGAKU"]); 
                    }
                    kashikataHassei += Util.ToDecimal(dr["NYUKIN_KINGAKU"]);
                    shohizei += Util.ToDecimal(dr["SHOHIZEI"]); 

                    if (count == l)
                    {
                        // 取得したデータをワークテーブルに更新をする
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyojiDate);
                        // データを設定
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["NAKAGAININ_CD"]);
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["NAKAGAININ_NM"]);
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["DENPYO_DATE"]);
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(suryo, 1));
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, zenZandaka);
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, karikataHassei);
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, kashikataHassei);
                        // 残高の計算
                        if (this.rdoZeiKomi.Checked == true)
                        {
                            zandaka += Util.Round(karikataHassei, 0) - kashikataHassei;
                        }
                        else
                        {
                            zandaka += Util.Round(karikataHassei, 0) - kashikataHassei + shohizei;
                        }
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, zandaka);
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, shohizei);
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(Rsuryo, 1));
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, RzengetsuZan);
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, RkashiKingaku);
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, RkariKingaku);
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Rshohizei);
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, (this.rdoZeiKomi.Checked == true ? "【税込】" : "【税抜】"));
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Rshohizei);
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dr["SHISHO_CD"]);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                        // 金額の初期化
                        suryo = 0;
                        karikataHassei = 0;
                        kashikataHassei = 0;
                        shohizei = 0;
                        zenZandaka = 0;
                        RzengetsuZan = 0;
                        RkariKingaku = 0;
                        RkashiKingaku = 0;
                        Rshohizei = 0;
                        Rsuryo = 0;

                        i++;
                        l = 0;
                    }

                    // 仲買人コード,発生日の保持
                    nakagaininCd = Util.ToString(dr["NAKAGAININ_CD"]);
                    //hasseibi = Util.ToString(dr["SERIBI"]);
                    hasseibi = Util.ToString(dr["DENPYO_DATE"]);
                    
                    l++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }
            
            return dataFlag;

        }
        #endregion

        private void fsiPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
