﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.hn.hnmr1081
{
    /// <summary>
    /// HNMR1081R の帳票
    /// </summary>
    public partial class HNMR1081R : BaseReport
    {

        public HNMR1081R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// グループフッターの設定(月計)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void groupFooter1_Format(object sender, EventArgs e)
        {
            this.txtSubTotal05.Text = Util.FormatNum( Util.ToDecimal( this.txtSubTotal02.Text) 
                                    + Util.ToDecimal(this.txtSubTotal03.Text)
                                    - Util.ToDecimal(this.txtSubTotal04.Text));

            this.txtSubTotal11.Text = Util.FormatNum(Util.ToDecimal(this.txtSubTotal08.Text)
                        + Util.ToDecimal(this.txtSubTotal09.Text)
                        - Util.ToDecimal(this.txtSubTotal10.Text));

            //this.txtSubTotal02.Text = Util.FormatNum(this.txtSubTotal02.Text);
            //this.txtSubTotal03.Text = Util.FormatNum(this.txtSubTotal03.Text);
            //this.txtSubTotal04.Text = Util.FormatNum(this.txtSubTotal04.Text);
            //this.txtSubTotal05.Text = Util.FormatNum(this.txtSubTotal05.Text);
            //this.txtSubTotal06.Text = Util.FormatNum(this.txtSubTotal06.Text);
            //this.txtSubTotal07.Text = Util.FormatNum(this.txtSubTotal07.Text);
            //this.txtSubTotal08.Text = Util.FormatNum(this.txtSubTotal08.Text);
            //this.txtSubTotal09.Text = Util.FormatNum(this.txtSubTotal09.Text);
            //this.txtSubTotal10.Text = Util.FormatNum(this.txtSubTotal10.Text);
            //this.txtSubTotal11.Text = Util.FormatNum(this.txtSubTotal11.Text);
            //this.txtSubTotal12.Text = Util.FormatNum(this.txtSubTotal12.Text);
            ////this.txtSubTotal12.Text = Util.FormatNum(this.txtSubTotal12.Text);
            //// 合計金額が0の場合、空表示とする
            //if (this.txtSubTotal02.Text == "0")
            //{
            //    this.txtSubTotal02.Text = "";
            //}
            //if (this.txtSubTotal03.Text == "0")
            //{
            //    this.txtSubTotal03.Text = "";
            //}
            //if (this.txtSubTotal04.Text == "0")
            //{
            //    this.txtSubTotal04.Text = "";
            //}
            //if (this.txtSubTotal05.Text == "0")
            //{
            //    this.txtSubTotal05.Text = "";
            //}
            //if (this.txtSubTotal06.Text == "0")
            //{
            //    this.txtSubTotal06.Text = "";
            //}
            //if (this.txtSubTotal07.Text == "0")
            //{
            //    this.txtSubTotal07.Text = "";
            //}
            //if (this.txtSubTotal08.Text == "0")
            //{
            //    this.txtSubTotal08.Text = "";
            //}
            //if (this.txtSubTotal09.Text == "0")
            //{
            //    this.txtSubTotal09.Text = "";
            //}
            //if (this.txtSubTotal10.Text == "0")
            //{
            //    this.txtSubTotal10.Text = "";
            //}
            ////if (this.txtSubTotal12.Text == "0")
            ////{
            ////    this.txtSubTotal12.Text = "";
            ////}
            //if (this.txtSubTotal11.Text == "0")
            //{
            //    this.txtSubTotal11.Text = "";
            //}
            //if (this.txtSubTotal12.Text == "0")
            //{
            //    this.txtSubTotal12.Text = "";
            //}
        }

        /// <summary>
        /// ページフッターの設定(合計)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportFooter1_Format(object sender, EventArgs e)
        {
            this.txtTotal05.Text = Util.FormatNum(Util.ToDecimal(this.txtTotal02.Text)
                                    + Util.ToDecimal(this.txtTotal03.Text)
                                    - Util.ToDecimal(this.txtTotal04.Text));

            this.txtTotal11.Text = Util.FormatNum(Util.ToDecimal(this.txtTotal08.Text)
                        + Util.ToDecimal(this.txtTotal09.Text)
                        - Util.ToDecimal(this.txtTotal10.Text));

            //// 合計金額をフォーマット
            //this.txtTotal01.Text = Util.FormatNum(this.txtTotal01.Text);
            //this.txtTotal02.Text = Util.FormatNum(this.txtTotal02.Text);
            //this.txtTotal03.Text = Util.FormatNum(this.txtTotal03.Text);
            //this.txtTotal04.Text = Util.FormatNum(this.txtTotal04.Text);
            //this.txtTotal05.Text = Util.FormatNum(this.txtTotal05.Text);
            //this.txtTotal06.Text = Util.FormatNum(this.txtTotal06.Text);
            //this.txtTotal07.Text = Util.FormatNum(this.txtTotal07.Text);
            //this.txtTotal08.Text = Util.FormatNum(this.txtTotal08.Text);
            //this.txtTotal09.Text = Util.FormatNum(this.txtTotal09.Text);
            //this.txtTotal10.Text = Util.FormatNum(this.txtTotal10.Text);
            //this.txtTotal11.Text = Util.FormatNum(this.txtTotal11.Text);
            //this.txtTotal12.Text = Util.FormatNum(this.txtTotal12.Text);
            //// 合計金額が0の場合、空表示とする
            //if (this.txtTotal01.Text == "0")
            //{
            //    this.txtTotal01.Text = "";
            //}
            //if (this.txtTotal02.Text == "0")
            //{
            //    this.txtTotal02.Text = "";
            //}
            //if (this.txtTotal03.Text == "0")
            //{
            //    this.txtTotal03.Text = "";
            //}
            //if (this.txtTotal04.Text == "0")
            //{
            //    this.txtTotal04.Text = "";
            //}
            //if (this.txtTotal05.Text == "0")
            //{
            //    this.txtTotal05.Text = "";
            //}
            //if (this.txtTotal06.Text == "0")
            //{
            //    this.txtTotal06.Text = "";
            //}
            //if (this.txtTotal07.Text == "0")
            //{
            //    this.txtTotal07.Text = "";
            //}
            //if (this.txtTotal08.Text == "0")
            //{
            //    this.txtTotal08.Text = "";
            //}
            //if (this.txtTotal09.Text == "0")
            //{
            //    this.txtTotal09.Text = "";
            //}
            //if (this.txtTotal10.Text == "0")
            //{
            //    this.txtTotal10.Text = "";
            //}
            //if (this.txtTotal11.Text == "0")
            //{
            //    this.txtTotal11.Text = "";
            //}
            //if (this.txtTotal12.Text == "0")
            //{
            //    this.txtTotal12.Text = "";
            //}
        }
    }
}
