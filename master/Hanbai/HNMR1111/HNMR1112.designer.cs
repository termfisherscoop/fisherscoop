﻿namespace jp.co.fsi.hn.hnmr1111
{
    partial class HNMR1112
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.btnLMove = new System.Windows.Forms.Button();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.lbxSentakuKomoku = new System.Windows.Forms.ListBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.btnRMove = new System.Windows.Forms.Button();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.lbxShukeiKubun = new System.Windows.Forms.ListBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF1
            // 
            this.btnF1.Text = "F1";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4";
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6";
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 527);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(743, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(732, 31);
            this.lblTitle.Text = "";
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(30, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 1;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(653, 472);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.btnLMove);
            this.fsiPanel1.Controls.Add(this.fsiPanel3);
            this.fsiPanel1.Controls.Add(this.btnRMove);
            this.fsiPanel1.Controls.Add(this.fsiPanel2);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(645, 464);
            this.fsiPanel1.TabIndex = 0;
            // 
            // btnLMove
            // 
            this.btnLMove.BackColor = System.Drawing.Color.Silver;
            this.btnLMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLMove.Location = new System.Drawing.Point(294, 233);
            this.btnLMove.Margin = new System.Windows.Forms.Padding(4);
            this.btnLMove.Name = "btnLMove";
            this.btnLMove.Size = new System.Drawing.Size(59, 52);
            this.btnLMove.TabIndex = 3;
            this.btnLMove.Text = "＜";
            this.btnLMove.UseVisualStyleBackColor = false;
            this.btnLMove.Click += new System.EventHandler(this.btnLMove_Click);
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.lbxSentakuKomoku);
            this.fsiPanel3.Controls.Add(this.Label2);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel3.Location = new System.Drawing.Point(442, 0);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(203, 464);
            this.fsiPanel3.TabIndex = 1;
            // 
            // lbxSentakuKomoku
            // 
            this.lbxSentakuKomoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxSentakuKomoku.FormattingEnabled = true;
            this.lbxSentakuKomoku.ItemHeight = 16;
            this.lbxSentakuKomoku.Location = new System.Drawing.Point(0, 24);
            this.lbxSentakuKomoku.Margin = new System.Windows.Forms.Padding(4);
            this.lbxSentakuKomoku.Name = "lbxSentakuKomoku";
            this.lbxSentakuKomoku.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxSentakuKomoku.Size = new System.Drawing.Size(203, 440);
            this.lbxSentakuKomoku.TabIndex = 903;
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.Color.Silver;
            this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Label2.Location = new System.Drawing.Point(0, 0);
            this.Label2.MinimumSize = new System.Drawing.Size(0, 24);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(203, 24);
            this.Label2.TabIndex = 3;
            this.Label2.Tag = "CHANGE";
            this.Label2.Text = "期首残科目";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRMove
            // 
            this.btnRMove.BackColor = System.Drawing.Color.Silver;
            this.btnRMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRMove.Location = new System.Drawing.Point(294, 161);
            this.btnRMove.Margin = new System.Windows.Forms.Padding(4);
            this.btnRMove.Name = "btnRMove";
            this.btnRMove.Size = new System.Drawing.Size(59, 52);
            this.btnRMove.TabIndex = 2;
            this.btnRMove.Text = "＞";
            this.btnRMove.UseVisualStyleBackColor = false;
            this.btnRMove.Click += new System.EventHandler(this.btnRMove_Click);
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lbxShukeiKubun);
            this.fsiPanel2.Controls.Add(this.Label1);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel2.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(203, 464);
            this.fsiPanel2.TabIndex = 0;
            // 
            // lbxShukeiKubun
            // 
            this.lbxShukeiKubun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxShukeiKubun.FormattingEnabled = true;
            this.lbxShukeiKubun.ItemHeight = 16;
            this.lbxShukeiKubun.Location = new System.Drawing.Point(0, 24);
            this.lbxShukeiKubun.Margin = new System.Windows.Forms.Padding(4);
            this.lbxShukeiKubun.Name = "lbxShukeiKubun";
            this.lbxShukeiKubun.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxShukeiKubun.Size = new System.Drawing.Size(203, 440);
            this.lbxShukeiKubun.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.Silver;
            this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Label1.Location = new System.Drawing.Point(0, 0);
            this.Label1.MinimumSize = new System.Drawing.Size(0, 24);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(203, 24);
            this.Label1.TabIndex = 4;
            this.Label1.Tag = "CHANGE";
            this.Label1.Text = "科目一覧";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNMR1112
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 664);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "HNMR1112";
            this.ShowFButton = true;
            this.Text = "期首残科目設定";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }




        #endregion

        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.ListBox lbxShukeiKubun;
        private System.Windows.Forms.Button btnLMove;
        private System.Windows.Forms.Button btnRMove;
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.ListBox lbxSentakuKomoku;
    }
}