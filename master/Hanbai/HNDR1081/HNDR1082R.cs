﻿using System;
using System.Drawing;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;


namespace jp.co.fsi.hn.hndr1081
{
    /// <summary>
    /// HNDR1081R の概要の説明です。
    /// </summary>
    public partial class HNDR1082R : BaseReport
    {

        public HNDR1082R(DataTable tgtData)
            : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
            }

        private void groupFooter2_Format(object sender, EventArgs e)
        {
            this.textBox71.Text = Util.FormatNum(Util.ToDecimal(this.textBox72.Value) / Util.ToDecimal(this.textBox70.Value));
        }

        private void groupFooter1_Format(object sender, EventArgs e)
        {
            this.textBox34.Text = Util.FormatNum(Util.ToDecimal(this.textBox35.Value) / Util.ToDecimal(this.textBox20.Value));
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }
    }
}