﻿namespace jp.co.fsi.hn.hndr1081
{
    /// <summary>
    /// HNDR1082R の概要の説明です。
    /// </summary>
    partial class HNDR1081R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNDR1081R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ITEM01,
            this.ラベル3,
            this.ラベル5,
            this.ラベル6,
            this.ラベル7,
            this.ラベル8,
            this.ラベル13,
            this.ラベル14,
            this.ラベル15,
            this.ラベル16,
            this.ラベル17,
            this.ラベル18,
            this.ラベル19,
            this.ラベル20,
            this.ラベル21,
            this.ラベル22,
            this.ラベル23,
            this.ラベル25,
            this.ラベル27,
            this.txtToday,
            this.テキスト30,
            this.直線34,
            this.テキスト110,
            this.textBox15,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox7});
            this.pageHeader.Height = 1.145965F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // ITEM01
            // 
            this.ITEM01.CanGrow = false;
            this.ITEM01.DataField = "ITEM26";
            this.ITEM01.Height = 0.1562992F;
            this.ITEM01.Left = 0.7122048F;
            this.ITEM01.MultiLine = false;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.OutputFormat = resources.GetString("ITEM01.OutputFormat");
            this.ITEM01.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128; ddo-shrink-to-fit: none";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM26";
            this.ITEM01.Top = 0.07283464F;
            this.ITEM01.Width = 0.5448983F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.15625F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 0.07874016F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "支　所：";
            this.ラベル3.Top = 0.3854331F;
            this.ラベル3.Width = 0.6334482F;
            // 
            // ラベル5
            // 
            this.ラベル5.Height = 0.15625F;
            this.ラベル5.HyperLink = null;
            this.ラベル5.Left = 0F;
            this.ラベル5.Name = "ラベル5";
            this.ラベル5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル5.Tag = "";
            this.ラベル5.Text = "コード";
            this.ラベル5.Top = 0.9366142F;
            this.ラベル5.Width = 0.5511811F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.15625F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 0.5720473F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; ddo-char-set: 128";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "組合員";
            this.ラベル6.Top = 0.9374017F;
            this.ラベル6.Width = 1.338583F;
            // 
            // ラベル7
            // 
            this.ラベル7.Height = 0.15625F;
            this.ラベル7.HyperLink = null;
            this.ラベル7.Left = 2.059449F;
            this.ラベル7.Name = "ラベル7";
            this.ラベル7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル7.Tag = "";
            this.ラベル7.Text = "精算日付";
            this.ラベル7.Top = 0.9366142F;
            this.ラベル7.Width = 0.802264F;
            // 
            // ラベル8
            // 
            this.ラベル8.Height = 0.15625F;
            this.ラベル8.HyperLink = null;
            this.ラベル8.Left = 3.022441F;
            this.ラベル8.Name = "ラベル8";
            this.ラベル8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル8.Tag = "";
            this.ラベル8.Text = "数量";
            this.ラベル8.Top = 0.9366142F;
            this.ラベル8.Width = 0.5625F;
            // 
            // ラベル13
            // 
            this.ラベル13.Height = 0.15625F;
            this.ラベル13.HyperLink = null;
            this.ラベル13.Left = 3.833465F;
            this.ラベル13.Name = "ラベル13";
            this.ラベル13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル13.Tag = "";
            this.ラベル13.Text = "水揚金額";
            this.ラベル13.Top = 0.9374017F;
            this.ラベル13.Width = 0.8331695F;
            // 
            // ラベル14
            // 
            this.ラベル14.Height = 0.15625F;
            this.ラベル14.HyperLink = null;
            this.ラベル14.Left = 4.924016F;
            this.ラベル14.Name = "ラベル14";
            this.ラベル14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル14.Tag = "";
            this.ラベル14.Text = "消費税";
            this.ラベル14.Top = 0.9366142F;
            this.ラベル14.Width = 0.6875F;
            // 
            // ラベル15
            // 
            this.ラベル15.Height = 0.15625F;
            this.ラベル15.HyperLink = null;
            this.ラベル15.Left = 5.91063F;
            this.ラベル15.Name = "ラベル15";
            this.ラベル15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル15.Tag = "";
            this.ラベル15.Text = "税込合計";
            this.ラベル15.Top = 0.9374017F;
            this.ラベル15.Width = 0.6875F;
            // 
            // ラベル16
            // 
            this.ラベル16.Height = 0.15625F;
            this.ラベル16.HyperLink = null;
            this.ラベル16.Left = 6.534646F;
            this.ラベル16.Name = "ラベル16";
            this.ラベル16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル16.Tag = "";
            this.ラベル16.Text = "漁協手数料";
            this.ラベル16.Top = 0.2673229F;
            this.ラベル16.Visible = false;
            this.ラベル16.Width = 0.7807087F;
            // 
            // ラベル17
            // 
            this.ラベル17.Height = 0.15625F;
            this.ラベル17.HyperLink = null;
            this.ラベル17.Left = 7.488189F;
            this.ラベル17.Name = "ラベル17";
            this.ラベル17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル17.Tag = "";
            this.ラベル17.Text = "箱代";
            this.ラベル17.Top = 0.2665354F;
            this.ラベル17.Visible = false;
            this.ラベル17.Width = 0.5625F;
            // 
            // ラベル18
            // 
            this.ラベル18.Height = 0.15625F;
            this.ラベル18.HyperLink = null;
            this.ラベル18.Left = 8.071653F;
            this.ラベル18.Name = "ラベル18";
            this.ラベル18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル18.Tag = "";
            this.ラベル18.Text = "その他の控除";
            this.ラベル18.Top = 0.2673229F;
            this.ラベル18.Visible = false;
            this.ラベル18.Width = 0.8602362F;
            // 
            // ラベル19
            // 
            this.ラベル19.Height = 0.15625F;
            this.ラベル19.HyperLink = null;
            this.ラベル19.Left = 9.26181F;
            this.ラベル19.Name = "ラベル19";
            this.ラベル19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル19.Tag = "";
            this.ラベル19.Text = "積立金";
            this.ラベル19.Top = 0.2665354F;
            this.ラベル19.Visible = false;
            this.ラベル19.Width = 0.4375F;
            // 
            // ラベル20
            // 
            this.ラベル20.Height = 0.15625F;
            this.ラベル20.HyperLink = null;
            this.ラベル20.Left = 9.855513F;
            this.ラベル20.Name = "ラベル20";
            this.ラベル20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル20.Tag = "";
            this.ラベル20.Text = "預り金";
            this.ラベル20.Top = 0.2673229F;
            this.ラベル20.Visible = false;
            this.ラベル20.Width = 0.541831F;
            // 
            // ラベル21
            // 
            this.ラベル21.Height = 0.15625F;
            this.ラベル21.HyperLink = null;
            this.ラベル21.Left = 10.44409F;
            this.ラベル21.Name = "ラベル21";
            this.ラベル21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル21.Tag = "";
            this.ラベル21.Text = "本人積立金";
            this.ラベル21.Top = 0.2665354F;
            this.ラベル21.Visible = false;
            this.ラベル21.Width = 0.708662F;
            // 
            // ラベル22
            // 
            this.ラベル22.Height = 0.15625F;
            this.ラベル22.HyperLink = null;
            this.ラベル22.Left = 11.54213F;
            this.ラベル22.Name = "ラベル22";
            this.ラベル22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル22.Tag = "";
            this.ラベル22.Text = "差引金額";
            this.ラベル22.Top = 0.9366142F;
            this.ラベル22.Width = 0.7086611F;
            // 
            // ラベル23
            // 
            this.ラベル23.Height = 0.15625F;
            this.ラベル23.HyperLink = null;
            this.ラベル23.Left = 12.28347F;
            this.ラベル23.Name = "ラベル23";
            this.ラベル23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; ddo-char-set: 128";
            this.ラベル23.Tag = "";
            this.ラベル23.Text = "精算番号";
            this.ラベル23.Top = 0.9366142F;
            this.ラベル23.Visible = false;
            this.ラベル23.Width = 0.6455717F;
            // 
            // ラベル25
            // 
            this.ラベル25.Height = 0.15625F;
            this.ラベル25.HyperLink = null;
            this.ラベル25.Left = 11.41732F;
            this.ラベル25.Name = "ラベル25";
            this.ラベル25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル25.Tag = "";
            this.ラベル25.Text = "日付：";
            this.ラベル25.Top = 0.2291339F;
            this.ラベル25.Width = 0.4988527F;
            // 
            // ラベル27
            // 
            this.ラベル27.Height = 0.15625F;
            this.ラベル27.HyperLink = null;
            this.ラベル27.Left = 11.29527F;
            this.ラベル27.Name = "ラベル27";
            this.ラベル27.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル27.Tag = "";
            this.ラベル27.Text = "ページ：";
            this.ラベル27.Top = 0.4236221F;
            this.ラベル27.Width = 0.6238527F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.15625F;
            this.txtToday.Left = 11.98228F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128; ddo-shrink-to-fit: none";
            this.txtToday.Tag = "";
            this.txtToday.Text = null;
            this.txtToday.Top = 0.2291339F;
            this.txtToday.Width = 1.117601F;
            // 
            // テキスト30
            // 
            this.テキスト30.Height = 0.15625F;
            this.テキスト30.Left = 12.10748F;
            this.テキスト30.Name = "テキスト30";
            this.テキスト30.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 128";
            this.テキスト30.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト30.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.テキスト30.Tag = "";
            this.テキスト30.Text = null;
            this.テキスト30.Top = 0.4236221F;
            this.テキスト30.Width = 0.9926014F;
            // 
            // 直線34
            // 
            this.直線34.Height = 0F;
            this.直線34.Left = 0F;
            this.直線34.LineWeight = 2F;
            this.直線34.Name = "直線34";
            this.直線34.Tag = "";
            this.直線34.Top = 1.093766F;
            this.直線34.Width = 13.14961F;
            this.直線34.X1 = 0F;
            this.直線34.X2 = 13.14961F;
            this.直線34.Y1 = 1.093766F;
            this.直線34.Y2 = 1.093766F;
            // 
            // テキスト110
            // 
            this.テキスト110.Height = 0.2291667F;
            this.テキスト110.Left = 5.098763F;
            this.テキスト110.Name = "テキスト110";
            this.テキスト110.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: bold; text-align: center; ddo-char-set: 128";
            this.テキスト110.Tag = "";
            this.テキスト110.Text = "振替及出金チェックリスト";
            this.テキスト110.Top = 0F;
            this.テキスト110.Width = 2.952083F;
            // 
            // textBox15
            // 
            this.textBox15.CanGrow = false;
            this.textBox15.DataField = "ITEM20";
            this.textBox15.Height = 0.3228346F;
            this.textBox15.Left = 6.676772F;
            this.textBox15.MultiLine = false;
            this.textBox15.Name = "textBox15";
            this.textBox15.OutputFormat = resources.GetString("textBox15.OutputFormat");
            this.textBox15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox15.Tag = "";
            this.textBox15.Text = "ITEM20";
            this.textBox15.Top = 0.7582678F;
            this.textBox15.Width = 0.9968505F;
            // 
            // textBox21
            // 
            this.textBox21.CanGrow = false;
            this.textBox21.DataField = "ITEM21";
            this.textBox21.Height = 0.3228346F;
            this.textBox21.Left = 7.721261F;
            this.textBox21.MultiLine = false;
            this.textBox21.Name = "textBox21";
            this.textBox21.OutputFormat = resources.GetString("textBox21.OutputFormat");
            this.textBox21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox21.Tag = "";
            this.textBox21.Text = "ITEM21";
            this.textBox21.Top = 0.7582678F;
            this.textBox21.Width = 0.9377947F;
            // 
            // textBox22
            // 
            this.textBox22.CanGrow = false;
            this.textBox22.DataField = "ITEM22";
            this.textBox22.Height = 0.1562992F;
            this.textBox22.Left = 8.409056F;
            this.textBox22.MultiLine = false;
            this.textBox22.Name = "textBox22";
            this.textBox22.OutputFormat = resources.GetString("textBox22.OutputFormat");
            this.textBox22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox22.Tag = "";
            this.textBox22.Text = "ITEM22";
            this.textBox22.Top = 0.9248032F;
            this.textBox22.Width = 0.8811026F;
            // 
            // textBox23
            // 
            this.textBox23.CanGrow = false;
            this.textBox23.DataField = "ITEM23";
            this.textBox23.Height = 0.1562992F;
            this.textBox23.Left = 9.327953F;
            this.textBox23.MultiLine = false;
            this.textBox23.Name = "textBox23";
            this.textBox23.OutputFormat = resources.GetString("textBox23.OutputFormat");
            this.textBox23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox23.Tag = "";
            this.textBox23.Text = "ITEM23";
            this.textBox23.Top = 0.9248032F;
            this.textBox23.Width = 0.7295278F;
            // 
            // textBox24
            // 
            this.textBox24.CanGrow = false;
            this.textBox24.DataField = "ITEM24";
            this.textBox24.Height = 0.1562992F;
            this.textBox24.Left = 10.12008F;
            this.textBox24.MultiLine = false;
            this.textBox24.Name = "textBox24";
            this.textBox24.OutputFormat = resources.GetString("textBox24.OutputFormat");
            this.textBox24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox24.Tag = "";
            this.textBox24.Text = "ITEM24";
            this.textBox24.Top = 0.9248032F;
            this.textBox24.Width = 0.6669292F;
            // 
            // textBox25
            // 
            this.textBox25.CanGrow = false;
            this.textBox25.DataField = "ITEM25";
            this.textBox25.Height = 0.1562992F;
            this.textBox25.Left = 10.7815F;
            this.textBox25.MultiLine = false;
            this.textBox25.Name = "textBox25";
            this.textBox25.OutputFormat = resources.GetString("textBox25.OutputFormat");
            this.textBox25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox25.Tag = "";
            this.textBox25.Text = "ITEM25";
            this.textBox25.Top = 0.9248032F;
            this.textBox25.Width = 0.7295249F;
            // 
            // textBox7
            // 
            this.textBox7.CanGrow = false;
            this.textBox7.DataField = "ITEM26";
            this.textBox7.Height = 0.15625F;
            this.textBox7.Left = 0.5929134F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM26";
            this.textBox7.Top = 0.3854331F;
            this.textBox7.Width = 0.494882F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox5,
            this.textBox6,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox1});
            this.detail.Height = 0.301099F;
            this.detail.Name = "detail";
            // 
            // textBox5
            // 
            this.textBox5.CanGrow = false;
            this.textBox5.DataField = "ITEM01";
            this.textBox5.Height = 0.15625F;
            this.textBox5.Left = 2.059449F;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 128";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM01";
            this.textBox5.Top = 0.006692916F;
            this.textBox5.Width = 0.8022585F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM07";
            this.textBox6.Height = 0.15625F;
            this.textBox6.Left = 2.876378F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM07\r\n";
            this.textBox6.Top = 0.006692916F;
            this.textBox6.Width = 0.7086616F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM09";
            this.textBox8.Height = 0.15625F;
            this.textBox8.Left = 3.585042F;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox8.Tag = "";
            this.textBox8.Text = "ITEM09";
            this.textBox8.Top = 0.006692901F;
            this.textBox8.Width = 1.081497F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM10";
            this.textBox9.Height = 0.15625F;
            this.textBox9.Left = 4.687405F;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox9.Tag = "";
            this.textBox9.Text = "ITEM10";
            this.textBox9.Top = 0.006692901F;
            this.textBox9.Width = 0.9240174F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM11";
            this.textBox10.Height = 0.15625F;
            this.textBox10.Left = 5.632286F;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox10.Tag = "";
            this.textBox10.Text = "ITEM11";
            this.textBox10.Top = 0.006692901F;
            this.textBox10.Width = 0.9657488F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM12";
            this.textBox11.Height = 0.15625F;
            this.textBox11.Left = 6.661024F;
            this.textBox11.Name = "textBox11";
            this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
            this.textBox11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox11.Tag = "";
            this.textBox11.Text = "ITEM12";
            this.textBox11.Top = 0.006692914F;
            this.textBox11.Width = 1.023622F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM13";
            this.textBox12.Height = 0.15625F;
            this.textBox12.Left = 7.700394F;
            this.textBox12.Name = "textBox12";
            this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
            this.textBox12.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox12.Tag = "";
            this.textBox12.Text = "ITEM13";
            this.textBox12.Top = 0.006692916F;
            this.textBox12.Width = 0.7086611F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM14";
            this.textBox13.Height = 0.15625F;
            this.textBox13.Left = 8.450788F;
            this.textBox13.Name = "textBox13";
            this.textBox13.OutputFormat = resources.GetString("textBox13.OutputFormat");
            this.textBox13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox13.Tag = "";
            this.textBox13.Text = "ITEM14";
            this.textBox13.Top = 0.006692916F;
            this.textBox13.Width = 0.8393698F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM15";
            this.textBox14.Height = 0.15625F;
            this.textBox14.Left = 9.327953F;
            this.textBox14.Name = "textBox14";
            this.textBox14.OutputFormat = resources.GetString("textBox14.OutputFormat");
            this.textBox14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox14.Tag = "";
            this.textBox14.Text = "ITEM15";
            this.textBox14.Top = 0.006692916F;
            this.textBox14.Width = 0.7295274F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM17";
            this.textBox16.Height = 0.15625F;
            this.textBox16.Left = 10.7815F;
            this.textBox16.Name = "textBox16";
            this.textBox16.OutputFormat = resources.GetString("textBox16.OutputFormat");
            this.textBox16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox16.Tag = "";
            this.textBox16.Text = "ITEM17";
            this.textBox16.Top = 0.006692901F;
            this.textBox16.Width = 0.7295275F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM18";
            this.textBox17.Height = 0.15625F;
            this.textBox17.Left = 11.54213F;
            this.textBox17.Name = "textBox17";
            this.textBox17.OutputFormat = resources.GetString("textBox17.OutputFormat");
            this.textBox17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox17.Tag = "";
            this.textBox17.Text = "ITEM18";
            this.textBox17.Top = 0.006692916F;
            this.textBox17.Width = 0.7086611F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM19";
            this.textBox18.Height = 0.15625F;
            this.textBox18.Left = 12.28347F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 128";
            this.textBox18.Tag = "";
            this.textBox18.Text = "ITEM19";
            this.textBox18.Top = 0F;
            this.textBox18.Visible = false;
            this.textBox18.Width = 0.6299162F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM16";
            this.textBox1.Height = 0.15625F;
            this.textBox1.Left = 10.08858F;
            this.textBox1.Name = "textBox1";
            this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
            this.textBox1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM16";
            this.textBox1.Top = 0.006692916F;
            this.textBox1.Width = 0.6669292F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM04";
            this.textBox3.Height = 0.15625F;
            this.textBox3.Left = 0F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 128";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM04";
            this.textBox3.Top = 0F;
            this.textBox3.Width = 0.5511811F;
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.DataField = "ITEM05";
            this.textBox4.Height = 0.15625F;
            this.textBox4.Left = 0.5720473F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM05";
            this.textBox4.Top = 0F;
            this.textBox4.Width = 1.317717F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0.21875F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM26";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label2,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox20,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40});
            this.groupFooter1.Height = 0.3543307F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // label2
            // 
            this.label2.Height = 0.15625F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.2362205F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 128";
            this.label2.Text = "総計";
            this.label2.Top = 0.08543307F;
            this.label2.Width = 1.020833F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM15";
            this.textBox30.Height = 0.15625F;
            this.textBox30.Left = 9.327951F;
            this.textBox30.Name = "textBox30";
            this.textBox30.OutputFormat = resources.GetString("textBox30.OutputFormat");
            this.textBox30.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox30.SummaryGroup = "groupHeader1";
            this.textBox30.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox30.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox30.Tag = "";
            this.textBox30.Text = "ITEM15";
            this.textBox30.Top = 0.08543307F;
            this.textBox30.Width = 0.7295274F;
            // 
            // textBox31
            // 
            this.textBox31.DataField = "ITEM16";
            this.textBox31.Height = 0.15625F;
            this.textBox31.Left = 10.08858F;
            this.textBox31.Name = "textBox31";
            this.textBox31.OutputFormat = resources.GetString("textBox31.OutputFormat");
            this.textBox31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox31.SummaryGroup = "groupHeader1";
            this.textBox31.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox31.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox31.Tag = "";
            this.textBox31.Text = "ITEM16";
            this.textBox31.Top = 0.08543312F;
            this.textBox31.Width = 0.6669292F;
            // 
            // textBox32
            // 
            this.textBox32.DataField = "ITEM17";
            this.textBox32.Height = 0.15625F;
            this.textBox32.Left = 10.7815F;
            this.textBox32.Name = "textBox32";
            this.textBox32.OutputFormat = resources.GetString("textBox32.OutputFormat");
            this.textBox32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox32.SummaryGroup = "groupHeader1";
            this.textBox32.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox32.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox32.Tag = "";
            this.textBox32.Text = "ITEM17";
            this.textBox32.Top = 0.08543307F;
            this.textBox32.Width = 0.7295275F;
            // 
            // textBox33
            // 
            this.textBox33.DataField = "ITEM18";
            this.textBox33.Height = 0.15625F;
            this.textBox33.Left = 11.54213F;
            this.textBox33.Name = "textBox33";
            this.textBox33.OutputFormat = resources.GetString("textBox33.OutputFormat");
            this.textBox33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox33.SummaryGroup = "groupHeader1";
            this.textBox33.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox33.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox33.Tag = "";
            this.textBox33.Text = "ITEM18";
            this.textBox33.Top = 0.08543309F;
            this.textBox33.Width = 0.7086611F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM07";
            this.textBox20.Height = 0.15625F;
            this.textBox20.Left = 2.876378F;
            this.textBox20.Name = "textBox20";
            this.textBox20.OutputFormat = resources.GetString("textBox20.OutputFormat");
            this.textBox20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox20.SummaryGroup = "groupHeader1";
            this.textBox20.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox20.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox20.Tag = "";
            this.textBox20.Text = "ITEM07\r\n";
            this.textBox20.Top = 0.08543307F;
            this.textBox20.Width = 0.7086616F;
            // 
            // textBox35
            // 
            this.textBox35.DataField = "ITEM09";
            this.textBox35.Height = 0.15625F;
            this.textBox35.Left = 3.58504F;
            this.textBox35.Name = "textBox35";
            this.textBox35.OutputFormat = resources.GetString("textBox35.OutputFormat");
            this.textBox35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox35.SummaryGroup = "groupHeader1";
            this.textBox35.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox35.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox35.Tag = "";
            this.textBox35.Text = "ITEM09";
            this.textBox35.Top = 0.08543307F;
            this.textBox35.Width = 1.081497F;
            // 
            // textBox36
            // 
            this.textBox36.DataField = "ITEM10";
            this.textBox36.Height = 0.15625F;
            this.textBox36.Left = 4.687402F;
            this.textBox36.Name = "textBox36";
            this.textBox36.OutputFormat = resources.GetString("textBox36.OutputFormat");
            this.textBox36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox36.SummaryGroup = "groupHeader1";
            this.textBox36.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox36.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox36.Tag = "";
            this.textBox36.Text = "ITEM10";
            this.textBox36.Top = 0.08543307F;
            this.textBox36.Width = 0.9240174F;
            // 
            // textBox37
            // 
            this.textBox37.DataField = "ITEM11";
            this.textBox37.Height = 0.15625F;
            this.textBox37.Left = 5.632284F;
            this.textBox37.Name = "textBox37";
            this.textBox37.OutputFormat = resources.GetString("textBox37.OutputFormat");
            this.textBox37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox37.SummaryGroup = "groupHeader1";
            this.textBox37.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox37.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox37.Tag = "";
            this.textBox37.Text = "ITEM11";
            this.textBox37.Top = 0.08543307F;
            this.textBox37.Width = 0.9657488F;
            // 
            // textBox38
            // 
            this.textBox38.DataField = "ITEM12";
            this.textBox38.Height = 0.15625F;
            this.textBox38.Left = 6.650001F;
            this.textBox38.Name = "textBox38";
            this.textBox38.OutputFormat = resources.GetString("textBox38.OutputFormat");
            this.textBox38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox38.SummaryGroup = "groupHeader1";
            this.textBox38.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox38.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox38.Tag = "";
            this.textBox38.Text = "ITEM12";
            this.textBox38.Top = 0.08543307F;
            this.textBox38.Width = 1.023622F;
            // 
            // textBox39
            // 
            this.textBox39.DataField = "ITEM13";
            this.textBox39.Height = 0.15625F;
            this.textBox39.Left = 7.700387F;
            this.textBox39.Name = "textBox39";
            this.textBox39.OutputFormat = resources.GetString("textBox39.OutputFormat");
            this.textBox39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox39.SummaryGroup = "groupHeader1";
            this.textBox39.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox39.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox39.Tag = "";
            this.textBox39.Text = "ITEM13";
            this.textBox39.Top = 0.08543307F;
            this.textBox39.Width = 0.7086611F;
            // 
            // textBox40
            // 
            this.textBox40.DataField = "ITEM14";
            this.textBox40.Height = 0.15625F;
            this.textBox40.Left = 8.450788F;
            this.textBox40.Name = "textBox40";
            this.textBox40.OutputFormat = resources.GetString("textBox40.OutputFormat");
            this.textBox40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox40.SummaryGroup = "groupHeader1";
            this.textBox40.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox40.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox40.Tag = "";
            this.textBox40.Text = "ITEM14";
            this.textBox40.Top = 0.08543309F;
            this.textBox40.Width = 0.8393679F;
            // 
            // groupHeader2
            // 
            this.groupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox3,
            this.textBox4});
            this.groupHeader2.DataField = "ITEM04";
            this.groupHeader2.Height = 0.15625F;
            this.groupHeader2.Name = "groupHeader2";
            this.groupHeader2.UnderlayNext = true;
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line2,
            this.label5,
            this.textBox52,
            this.textBox67,
            this.textBox68,
            this.textBox70,
            this.textBox72,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.textBox76,
            this.textBox77,
            this.textBox2});
            this.groupFooter2.Height = 0.3022473F;
            this.groupFooter2.Name = "groupFooter2";
            this.groupFooter2.Format += new System.EventHandler(this.groupFooter2_Format);
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Tag = "";
            this.line2.Top = 0.2547244F;
            this.line2.Width = 13.1496F;
            this.line2.X1 = 0F;
            this.line2.X2 = 13.1496F;
            this.line2.Y1 = 0.2547244F;
            this.line2.Y2 = 0.2547244F;
            // 
            // label5
            // 
            this.label5.Height = 0.15625F;
            this.label5.HyperLink = null;
            this.label5.Left = 0.2362205F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 128";
            this.label5.Text = "小計";
            this.label5.Top = 0.01338583F;
            this.label5.Width = 1.020833F;
            // 
            // textBox52
            // 
            this.textBox52.DataField = "ITEM15";
            this.textBox52.Height = 0.15625F;
            this.textBox52.Left = 9.327953F;
            this.textBox52.Name = "textBox52";
            this.textBox52.OutputFormat = resources.GetString("textBox52.OutputFormat");
            this.textBox52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox52.SummaryGroup = "groupHeader2";
            this.textBox52.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox52.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox52.Tag = "";
            this.textBox52.Text = "ITEM15";
            this.textBox52.Top = 0.006692914F;
            this.textBox52.Width = 0.7295274F;
            // 
            // textBox67
            // 
            this.textBox67.DataField = "ITEM17";
            this.textBox67.Height = 0.15625F;
            this.textBox67.Left = 10.7815F;
            this.textBox67.Name = "textBox67";
            this.textBox67.OutputFormat = resources.GetString("textBox67.OutputFormat");
            this.textBox67.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox67.SummaryGroup = "groupHeader2";
            this.textBox67.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox67.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox67.Tag = "";
            this.textBox67.Text = "ITEM17";
            this.textBox67.Top = 0.01338582F;
            this.textBox67.Width = 0.7295275F;
            // 
            // textBox68
            // 
            this.textBox68.DataField = "ITEM18";
            this.textBox68.Height = 0.15625F;
            this.textBox68.Left = 11.54213F;
            this.textBox68.Name = "textBox68";
            this.textBox68.OutputFormat = resources.GetString("textBox68.OutputFormat");
            this.textBox68.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox68.SummaryGroup = "groupHeader2";
            this.textBox68.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox68.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox68.Tag = "";
            this.textBox68.Text = "ITEM18";
            this.textBox68.Top = 0.006692924F;
            this.textBox68.Width = 0.7086611F;
            // 
            // textBox70
            // 
            this.textBox70.DataField = "ITEM07";
            this.textBox70.Height = 0.15625F;
            this.textBox70.Left = 2.876378F;
            this.textBox70.Name = "textBox70";
            this.textBox70.OutputFormat = resources.GetString("textBox70.OutputFormat");
            this.textBox70.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox70.SummaryGroup = "groupHeader2";
            this.textBox70.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox70.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox70.Tag = "";
            this.textBox70.Text = "ITEM07\r\n";
            this.textBox70.Top = 0.006692916F;
            this.textBox70.Width = 0.7086616F;
            // 
            // textBox72
            // 
            this.textBox72.DataField = "ITEM09";
            this.textBox72.Height = 0.15625F;
            this.textBox72.Left = 3.585043F;
            this.textBox72.Name = "textBox72";
            this.textBox72.OutputFormat = resources.GetString("textBox72.OutputFormat");
            this.textBox72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox72.SummaryGroup = "groupHeader2";
            this.textBox72.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox72.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox72.Tag = "";
            this.textBox72.Text = "ITEM09";
            this.textBox72.Top = 0.006692909F;
            this.textBox72.Width = 1.081497F;
            // 
            // textBox73
            // 
            this.textBox73.DataField = "ITEM10";
            this.textBox73.Height = 0.15625F;
            this.textBox73.Left = 4.687404F;
            this.textBox73.Name = "textBox73";
            this.textBox73.OutputFormat = resources.GetString("textBox73.OutputFormat");
            this.textBox73.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox73.SummaryGroup = "groupHeader2";
            this.textBox73.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox73.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox73.Tag = "";
            this.textBox73.Text = "ITEM10";
            this.textBox73.Top = 0.006692909F;
            this.textBox73.Width = 0.9240174F;
            // 
            // textBox74
            // 
            this.textBox74.DataField = "ITEM11";
            this.textBox74.Height = 0.15625F;
            this.textBox74.Left = 5.632285F;
            this.textBox74.Name = "textBox74";
            this.textBox74.OutputFormat = resources.GetString("textBox74.OutputFormat");
            this.textBox74.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox74.SummaryGroup = "groupHeader2";
            this.textBox74.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox74.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox74.Tag = "";
            this.textBox74.Text = "ITEM11";
            this.textBox74.Top = 0.006692909F;
            this.textBox74.Width = 0.9657488F;
            // 
            // textBox75
            // 
            this.textBox75.DataField = "ITEM12";
            this.textBox75.Height = 0.15625F;
            this.textBox75.Left = 6.665749F;
            this.textBox75.Name = "textBox75";
            this.textBox75.OutputFormat = resources.GetString("textBox75.OutputFormat");
            this.textBox75.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox75.SummaryGroup = "groupHeader2";
            this.textBox75.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox75.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox75.Tag = "";
            this.textBox75.Text = "ITEM12";
            this.textBox75.Top = 0.006692914F;
            this.textBox75.Width = 1.023622F;
            // 
            // textBox76
            // 
            this.textBox76.DataField = "ITEM13";
            this.textBox76.Height = 0.15625F;
            this.textBox76.Left = 7.700387F;
            this.textBox76.Name = "textBox76";
            this.textBox76.OutputFormat = resources.GetString("textBox76.OutputFormat");
            this.textBox76.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox76.SummaryGroup = "groupHeader2";
            this.textBox76.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox76.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox76.Tag = "";
            this.textBox76.Text = "ITEM13";
            this.textBox76.Top = 0.006692914F;
            this.textBox76.Width = 0.7086611F;
            // 
            // textBox77
            // 
            this.textBox77.DataField = "ITEM14";
            this.textBox77.Height = 0.15625F;
            this.textBox77.Left = 8.450788F;
            this.textBox77.Name = "textBox77";
            this.textBox77.OutputFormat = resources.GetString("textBox77.OutputFormat");
            this.textBox77.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox77.SummaryGroup = "groupHeader2";
            this.textBox77.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox77.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox77.Tag = "";
            this.textBox77.Text = "ITEM14";
            this.textBox77.Top = 0.006692925F;
            this.textBox77.Width = 0.8393679F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM16";
            this.textBox2.Height = 0.15625F;
            this.textBox2.Left = 10.08858F;
            this.textBox2.Name = "textBox2";
            this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
            this.textBox2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 128";
            this.textBox2.SummaryGroup = "groupHeader2";
            this.textBox2.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM16";
            this.textBox2.Top = 0.01338586F;
            this.textBox2.Width = 0.6669292F;
            // 
            // HNDR1081R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937007F;
            this.PageSettings.Margins.Left = 0.3937007F;
            this.PageSettings.Margins.Right = 0.7874016F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.14961F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル5;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル7;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル8;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル13;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル14;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル15;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル16;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル17;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル18;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル19;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル20;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル21;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル22;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル23;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル25;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト30;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト110;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox70;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox72;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
    }
}
