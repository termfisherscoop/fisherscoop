﻿namespace jp.co.fsi.hn.hndr1051
{
    /// <summary>
    /// HNDR1051R の概要の説明です。
    /// </summary>
    partial class HNDR1051R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNDR1051R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle05 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle06 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle07 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTitle08 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblTotalTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGenkin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue09Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFurikomi = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue08Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue10Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGenkin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFurikomi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtCompanyName,
            this.lblTitle01,
            this.lblTitle02,
            this.lblTitle03,
            this.lblTitle04,
            this.lblTitle05,
            this.lblTitle06,
            this.lblTitle07,
            this.txtTitleName,
            this.line1,
            this.lblTitle08,
            this.line2,
            this.textBox1,
            this.label1,
            this.textBox2});
            this.pageHeader.Height = 1.364583F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.1968504F;
            this.txtToday.Left = 11.02795F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 128";
            this.txtToday.Text = "ggyy年M月d日";
            this.txtToday.Top = 0.3362205F;
            this.txtToday.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 12.59686F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 128";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.3362205F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 12.28111F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 128";
            this.txtPageCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.3362205F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.2488189F;
            this.txtCompanyName.Left = 1.335889E-05F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 128";
            this.txtCompanyName.Text = null;
            this.txtCompanyName.Top = 0.2322836F;
            this.txtCompanyName.Width = 3.263386F;
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.1968504F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 0.737415F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.lblTitle01.Text = "伝票番号";
            this.lblTitle01.Top = 1.135039F;
            this.lblTitle01.Width = 0.7322836F;
            // 
            // lblTitle02
            // 
            this.lblTitle02.Height = 0.1968504F;
            this.lblTitle02.HyperLink = null;
            this.lblTitle02.Left = 1.883084F;
            this.lblTitle02.Name = "lblTitle02";
            this.lblTitle02.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.lblTitle02.Text = "入金日";
            this.lblTitle02.Top = 1.135039F;
            this.lblTitle02.Width = 0.5968503F;
            // 
            // lblTitle03
            // 
            this.lblTitle03.Height = 0.1968504F;
            this.lblTitle03.HyperLink = null;
            this.lblTitle03.Left = 2.992533F;
            this.lblTitle03.Name = "lblTitle03";
            this.lblTitle03.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ver" +
    "tical-align: middle; ddo-char-set: 128";
            this.lblTitle03.Text = "仲買人CD";
            this.lblTitle03.Top = 1.135039F;
            this.lblTitle03.Width = 0.8259839F;
            // 
            // lblTitle04
            // 
            this.lblTitle04.Height = 0.1968504F;
            this.lblTitle04.HyperLink = null;
            this.lblTitle04.Left = 4.127573F;
            this.lblTitle04.Name = "lblTitle04";
            this.lblTitle04.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.lblTitle04.Text = "仲買人氏名";
            this.lblTitle04.Top = 1.135039F;
            this.lblTitle04.Width = 1.367716F;
            // 
            // lblTitle05
            // 
            this.lblTitle05.Height = 0.1968504F;
            this.lblTitle05.HyperLink = null;
            this.lblTitle05.Left = 6.249227F;
            this.lblTitle05.Name = "lblTitle05";
            this.lblTitle05.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.lblTitle05.Text = "入金区分";
            this.lblTitle05.Top = 1.135039F;
            this.lblTitle05.Width = 0.7322833F;
            // 
            // lblTitle06
            // 
            this.lblTitle06.Height = 0.1968504F;
            this.lblTitle06.HyperLink = null;
            this.lblTitle06.Left = 7.644896F;
            this.lblTitle06.Name = "lblTitle06";
            this.lblTitle06.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ver" +
    "tical-align: middle; ddo-char-set: 128";
            this.lblTitle06.Text = "入金金額";
            this.lblTitle06.Top = 1.135039F;
            this.lblTitle06.Width = 0.6594496F;
            // 
            // lblTitle07
            // 
            this.lblTitle07.Height = 0.1968504F;
            this.lblTitle07.HyperLink = null;
            this.lblTitle07.Left = 8.989778F;
            this.lblTitle07.Name = "lblTitle07";
            this.lblTitle07.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ver" +
    "tical-align: middle; ddo-char-set: 128";
            this.lblTitle07.Text = "残     高";
            this.lblTitle07.Top = 1.135039F;
            this.lblTitle07.Width = 0.7740164F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 4.961223F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 17.25pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle; ddo-char-set: 128";
            this.txtTitleName.Text = "入金チェックリスト";
            this.txtTitleName.Top = 0.2456693F;
            this.txtTitleName.Width = 2.833465F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 4.961223F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.5330709F;
            this.line1.Width = 2.833465F;
            this.line1.X1 = 4.961223F;
            this.line1.X2 = 7.794688F;
            this.line1.Y1 = 0.5330709F;
            this.line1.Y2 = 0.5330709F;
            // 
            // lblTitle08
            // 
            this.lblTitle08.Height = 0.1968504F;
            this.lblTitle08.HyperLink = null;
            this.lblTitle08.Left = 10.06655F;
            this.lblTitle08.Name = "lblTitle08";
            this.lblTitle08.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: left; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.lblTitle08.Text = "摘　　　要";
            this.lblTitle08.Top = 1.135039F;
            this.lblTitle08.Width = 0.9614174F;
            // 
            // line2
            // 
            this.line2.Height = 9.536743E-07F;
            this.line2.Left = 1.370907E-05F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 1.351574F;
            this.line2.Width = 12.81221F;
            this.line2.X1 = 1.370907E-05F;
            this.line2.X2 = 12.81222F;
            this.line2.Y1 = 1.351575F;
            this.line2.Y2 = 1.351574F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM13";
            this.textBox1.Height = 0.2488189F;
            this.textBox1.Left = 0.9437143F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 128";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.4811024F;
            this.textBox1.Width = 3.263386F;
            // 
            // label1
            // 
            this.label1.Height = 0.1968504F;
            this.label1.HyperLink = null;
            this.label1.Left = 1.335144E-05F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 128";
            this.label1.Text = "支　所：";
            this.label1.Top = 0.5330709F;
            this.label1.Width = 0.6177166F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM12";
            this.textBox2.Height = 0.1968504F;
            this.textBox2.Left = 0.6177301F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 128";
            this.textBox2.Text = "9999";
            this.textBox2.Top = 0.5330709F;
            this.textBox2.Width = 0.3259841F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM12";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue02,
            this.txtValue03,
            this.txtValue04,
            this.txtValue05,
            this.txtValue06,
            this.txtValue8,
            this.txtValue01,
            this.txtValue07});
            this.detail.Height = 0.2292652F;
            this.detail.Name = "detail";
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM03";
            this.txtValue02.Height = 0.1968504F;
            this.txtValue02.Left = 1.883071F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue02.Text = "2015.06/20";
            this.txtValue02.Top = 0F;
            this.txtValue02.Width = 0.9114174F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM04";
            this.txtValue03.Height = 0.1968504F;
            this.txtValue03.Left = 2.99252F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue03.Text = null;
            this.txtValue03.Top = 0F;
            this.txtValue03.Width = 0.825984F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM05";
            this.txtValue04.Height = 0.1968504F;
            this.txtValue04.Left = 4.127559F;
            this.txtValue04.MultiLine = false;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; text-justify: auto; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0F;
            this.txtValue04.Width = 1.99252F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM06";
            this.txtValue05.Height = 0.1968504F;
            this.txtValue05.Left = 6.249213F;
            this.txtValue05.MultiLine = false;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0F;
            this.txtValue05.Width = 0.732283F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM07";
            this.txtValue06.Height = 0.1968504F;
            this.txtValue06.Left = 7.272441F;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue06.Text = null;
            this.txtValue06.Top = 0F;
            this.txtValue06.Width = 1.031891F;
            // 
            // txtValue8
            // 
            this.txtValue8.DataField = "ITEM09";
            this.txtValue8.Height = 0.1968504F;
            this.txtValue8.Left = 10.06654F;
            this.txtValue8.MultiLine = false;
            this.txtValue8.Name = "txtValue8";
            this.txtValue8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue8.Text = null;
            this.txtValue8.Top = 0F;
            this.txtValue8.Width = 1.817323F;
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM02";
            this.txtValue01.Height = 0.1968504F;
            this.txtValue01.Left = 0.7374016F;
            this.txtValue01.MultiLine = false;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0F;
            this.txtValue01.Width = 0.7322836F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM08";
            this.txtValue07.Height = 0.1968504F;
            this.txtValue07.Left = 8.731891F;
            this.txtValue07.MultiLine = false;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0F;
            this.txtValue07.Width = 1.03189F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.Height = 0.01041667F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTotalTitle,
            this.lblGenkin,
            this.txtValue09Total,
            this.lblFurikomi,
            this.txtValue08Total,
            this.lblTotal,
            this.txtValue10Total});
            this.groupFooter1.Height = 0.2291666F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // lblTotalTitle
            // 
            this.lblTotalTitle.Height = 0.1968504F;
            this.lblTotalTitle.HyperLink = null;
            this.lblTotalTitle.Left = 0.4322835F;
            this.lblTotalTitle.Name = "lblTotalTitle";
            this.lblTotalTitle.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; vertical-align: middle" +
    "";
            this.lblTotalTitle.Text = "＊ ＊ 合計 ＊ ＊　";
            this.lblTotalTitle.Top = 0F;
            this.lblTotalTitle.Width = 1.318504F;
            // 
            // lblGenkin
            // 
            this.lblGenkin.Height = 0.1968504F;
            this.lblGenkin.HyperLink = null;
            this.lblGenkin.Left = 2.22874F;
            this.lblGenkin.Name = "lblGenkin";
            this.lblGenkin.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; vertical-align: middle" +
    "";
            this.lblGenkin.Text = "現金　";
            this.lblGenkin.Top = 0F;
            this.lblGenkin.Width = 0.565748F;
            // 
            // txtValue09Total
            // 
            this.txtValue09Total.DataField = "ITEM10";
            this.txtValue09Total.Height = 0.1968504F;
            this.txtValue09Total.Left = 2.794488F;
            this.txtValue09Total.Name = "txtValue09Total";
            this.txtValue09Total.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue09Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtValue09Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue09Total.Text = null;
            this.txtValue09Total.Top = 0F;
            this.txtValue09Total.Width = 1.117717F;
            // 
            // lblFurikomi
            // 
            this.lblFurikomi.Height = 0.1968504F;
            this.lblFurikomi.HyperLink = null;
            this.lblFurikomi.Left = 4.282677F;
            this.lblFurikomi.Name = "lblFurikomi";
            this.lblFurikomi.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; vertical-align: middle" +
    "";
            this.lblFurikomi.Text = "振込";
            this.lblFurikomi.Top = 0F;
            this.lblFurikomi.Width = 0.5551183F;
            // 
            // txtValue08Total
            // 
            this.txtValue08Total.DataField = "ITEM07";
            this.txtValue08Total.Height = 0.1968504F;
            this.txtValue08Total.Left = 6.970473F;
            this.txtValue08Total.Name = "txtValue08Total";
            this.txtValue08Total.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue08Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtValue08Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue08Total.Text = null;
            this.txtValue08Total.Top = 0F;
            this.txtValue08Total.Width = 1.333858F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1968504F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 6.415355F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; vertical-align: middle" +
    "";
            this.lblTotal.Text = "合計";
            this.lblTotal.Top = 0F;
            this.lblTotal.Width = 0.5551183F;
            // 
            // txtValue10Total
            // 
            this.txtValue10Total.DataField = "ITEM11";
            this.txtValue10Total.Height = 0.1968504F;
            this.txtValue10Total.Left = 4.846063F;
            this.txtValue10Total.Name = "txtValue10Total";
            this.txtValue10Total.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue10Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtValue10Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue10Total.Text = null;
            this.txtValue10Total.Top = 0F;
            this.txtValue10Total.Width = 1.055118F;
            // 
            // HNDR1051R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.7874016F;
            this.PageSettings.Margins.Right = 0.7874016F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 12.75591F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGenkin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFurikomi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGenkin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue09Total;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFurikomi;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08Total;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue10Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
    }
}
