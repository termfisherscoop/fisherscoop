﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1011
{
    /// <summary>
    /// 仲買人の登録(HNCM1011)
    /// </summary>
    public partial class HNCM1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";

        /// <summary>
        /// SQL関連
        /// </summary>
        private const int KAISHA_CDDE = 1; // 会社コード
        private const int TORIHIKISAKI_KUBUN2 = 2; // 取引先区分２

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "仲買人の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(663, 530);
                // フォームの配置を上へ移動する
                //this.lblKanaNm.Location = new System.Drawing.Point(14, 14);
                //this.txtKanaName.Location = new System.Drawing.Point(95, 16);
                //this.ckboxAllHyoji.Location = new System.Drawing.Point(451, 14);
                //this.dgvList.Location = new System.Drawing.Point(12, 53);
                // Escapeのみ表示とF1のみ表示
                this.ShowFButton = true;

                this.btnEsc.Location = this.btnEnter.Location;
                this.btnEnter.Visible = false;

                //this.btnEsc.Location = this.btnF1.Location;
                //this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }
            else
            {
                // メニューから呼ばれた場合は、EscapeとF4をクリック許可する（表示はF11まで）
                this.ShowFButton = false;
                this.btnEnter.Enabled = false; // 変更ボタンクリック不可
                this.btnF1.Enabled = false;     // 検索ボタンクリック可能
                this.btnF4.Enabled = true;    // 追加ボタンクリック不可
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // カナ名にフォーカス
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        ///   「検索」は、検索キーワード欄にフォーカスを移動する。
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        ///     追加処理を行う
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ仲買人登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 新規登録モードで登録画面を起動
                HNCM1012 frmHNCM1012 = new HNCM1012(MODE_NEW);

                DialogResult result = frmHNCM1012.ShowDialog(this);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // メンテ機能で立ち上げている場合のみ仲買人マスタ一覧を立ち上げる
            /*if (ValChk.IsEmpty(this.Par1))
            {
                // 仲買人マスタ一覧の起動
                ItiranNakagai();
            }*/
        }

        public override void PressF9()
        {
            ShowConfig();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 表示ボタンの変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckboxAllHyoji_CheckedChanged(object sender, System.EventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない

            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditFunanushi(Util.ToString(this.dgvList.SelectedRows[0].Cells["仲買人コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditFunanushi(Util.ToString(this.dgvList.SelectedRows[0].Cells["仲買人コード"].Value));
            }
        }

        /// <summary>
        /// ファンクションボタンの「Enter 変更」」
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, System.EventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditFunanushi(Util.ToString(this.dgvList.SelectedRows[0].Cells["仲買人コード"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 仲買人マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, KAISHA_CDDE); // 会社コードは1
            StringBuilder where = new StringBuilder("VI_HN.TORIHIKISAKI_KUBUN2 = " + TORIHIKISAKI_KUBUN2.ToString()); // 取引先区分２は2
            where.Append(" AND VI_HN.KAISHA_CD = @KAISHA_CD");
            if (isInitial)
            {
                // 初期処理の場合　表示フラグが1の仲買人は表示しない
                where.Append(" AND VI_HN.HYOJI_FLG = 0");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaName.Text))
                {
                    where.Append(" AND VI_HN.TORIHIKISAKI_KANA_NM LIKE @TORIHIKISAKI_KANA_NM");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    string kanaNm = "%" + this.txtKanaName.Text + "%";
                    dpc.SetParam("@TORIHIKISAKI_KANA_NM", SqlDbType.VarChar, kanaNm.Length, kanaNm);
                }
                if (this.ckboxAllHyoji.Checked == false)
                {
                    where.Append(" AND VI_HN.HYOJI_FLG = 0");
                }
            }

            string cols = "VI_HN.TORIHIKISAKI_CD AS 仲買人コード";
            cols += ", VI_HN.TORIHIKISAKI_NM AS 正式仲買人名";
            cols += ", VI_HN.TORIHIKISAKI_KANA_NM AS 仲買人カナ名";
            string from = "VI_HN_TORIHIKISAKI_JOHO AS VI_HN";

            DataTable dtShiire =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "VI_HN.TORIHIKISAKI_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtShiire.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtShiire.Rows.Add(dtShiire.NewRow());

                // 検索一覧が表示されない場合は、Enterボタンをクリック不可にする
                this.btnEnter.Enabled = false;
            }
            else
            {
                // 検索一覧が表示されたら、Enterボタンと検索ボタンをクリック可能にする
                this.btnEnter.Enabled = true;
                this.btnF1.Enabled = true;     // 検索ボタンクリック可能
            }

            this.dgvList.DataSource = dtShiire;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 200;
            this.dgvList.Columns[2].Width = 300;
        }

        /// <summary>
        /// 仲買人を追加編集する
        /// </summary>
        /// <param name="code">仲買人コード(空：新規登録、以外：編集)</param>
        private void EditFunanushi(string code)
        {
            HNCM1012 frmHNCM1012;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHNCM1012 = new HNCM1012(MODE_NEW);
            }
            else
            {
                // 編集モードで登録画面を起動
                frmHNCM1012 = new HNCM1012(MODE_EDIT);
                frmHNCM1012.InData = code;
            }

            DialogResult result = frmHNCM1012.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["仲買人コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 仲買人マスタ一覧画面を起動する
        /// </summary>
        /// <param name="code">仲買人コード()</param>
        private void ItiranNakagai()
        {
            HNCM1013 frmHNCM1013;
            // 仲買人マスタ一覧画面を起動
            frmHNCM1013 = new HNCM1013();

            DialogResult result = frmHNCM1013.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仲買人コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["正式仲買人名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仲買人カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 初期値設定画面を表示する
        /// </summary>
        private void ShowConfig()
        {
            HNCM1014 frmHNCM1014;

            // 初期値設定画面を起動
            frmHNCM1014 = new HNCM1014();

            DialogResult result = frmHNCM1014.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // フォーカスをセット
                this.ActiveControl.Focus();
            }
        }
        #endregion
    }
}
