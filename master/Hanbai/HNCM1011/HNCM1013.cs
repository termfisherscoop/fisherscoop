﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common;
using jp.co.fsi.common.access;

namespace jp.co.fsi.hn.hncm1011
{
    /// <summary>
    /// 仲買人マスタ一覧表(HNCM1013)
    /// </summary>
    public partial class HNCM1013 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1013()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // ボタンの位置調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;
            // フォーカス設定
            this.txtNakagaininCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            //仲買人CDに、
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtNakagaininCdFr":
                case "txtNakagaininCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtNakagaininCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdFr.Text = outData[0];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdTo.Text = outData[0];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // プレビュー処理
            DoPrint(true);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 仲買人コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdFr())
            {
                e.Cancel = true;
                this.txtNakagaininCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdTo())
            {
                e.Cancel = true;
                this.txtNakagaininCdTo.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 仲買人コードの入力チェック(先頭）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNakagaininCdFr()
        {
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.txtNakagaininCdFr.Text))
            {
                this.lblNakagaininCdFr.Text = "先　頭";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtNakagaininCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", "", this.txtNakagaininCdFr.Text);
            this.lblNakagaininCdFr.Text = name;
            return true;
        }

        /// <summary>
        /// 仲買人コードの入力チェック(最後）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNakagaininCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtNakagaininCdTo.Text))
            {
                this.lblNakagaininCdTo.Text = "最　後";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtNakagaininCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", null, this.txtNakagaininCdTo.Text);
            this.lblNakagaininCdTo.Text = name;

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 仲買人コードのチェック
            if (!IsValidNakagaininCdFr())
            {
                this.txtNakagaininCdFr.Focus();
                this.txtNakagaininCdFr.SelectAll();
                return false;
            }

            // 仲買人コードのチェック
            if (!IsValidNakagaininCdTo())
            {
                this.txtNakagaininCdTo.Focus();
                this.txtNakagaininCdTo.SelectAll();
                return false;
            }

            return true;
        }


        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            bool dataFlag;

            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                Report rpt = new Report();
                rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "HNCM1011.mdb"), "R_HNCM1011", this.UnqId, isPreview);
            }

            // ワークテーブルに作成したデータを削除
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                this.Dba.Delete("PR_HN_TBL", "GUID = @GUID", dpc);
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備

            // 仲買人コード設定
            string NAKAGAININ_CD_FR;
            string NAKAGAININ_CD_TO;
            string NAKAGAININ_NM_FR;
            string NAKAGAININ_NM_TO;
            if (Util.ToDecimal(txtNakagaininCdFr.Text) > 0)
            {
                NAKAGAININ_CD_FR = txtNakagaininCdFr.Text;
            }
            else
            {
                NAKAGAININ_CD_FR = "0";
            }
            if (Util.ToDecimal(txtNakagaininCdTo.Text) > 0)
            {
                NAKAGAININ_CD_TO = txtNakagaininCdTo.Text;
            }
            else
            {
                NAKAGAININ_CD_TO = "9999";
            }
            NAKAGAININ_NM_FR = lblNakagaininCdFr.Text;
            NAKAGAININ_NM_TO = lblNakagaininCdTo.Text;
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            string NakagaininJusho;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            //VI_HN_TORIHIKISAKI_JOHOからデータ取得
            Sql.Append(" SELECT");
            Sql.Append("  *  ");
            Sql.Append(" FROM ");
            Sql.Append(" VI_HN_TORIHIKISAKI_JOHO ");
            Sql.Append(" WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" TORIHIKISAKI_KUBUN2 = 2 AND");
            Sql.Append(" TORIHIKISAKI_CD BETWEEN @NAKAGAININ_CD_FR AND @NAKAGAININ_CD_TO");
            Sql.Append(" ORDER BY ");
            Sql.Append(" KAISHA_CD, ");
            Sql.Append(" TORIHIKISAKI_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@NAKAGAININ_CD_FR", SqlDbType.Decimal, 6, NAKAGAININ_CD_FR);
            dpc.SetParam("@NAKAGAININ_CD_TO", SqlDbType.Decimal, 6, NAKAGAININ_CD_TO);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, NAKAGAININ_CD_FR); // 仲買人コード（先頭）
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, NAKAGAININ_CD_TO); // 仲買人コード（最後）
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, NAKAGAININ_NM_FR); // 仲買人名（先頭）
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, NAKAGAININ_NM_TO); // 仲買人名（最後）
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]); // 仲買人コード
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_NM"]); // 正式仲買人名
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_KANA_NM"]);// 仲買人カナ名
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["RYAKUSHO"]);// 仲買人略称名
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YUBIN_BANGO1"]); // 郵便番号1
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YUBIN_BANGO2"]); // 郵便番号2
                    NakagaininJusho = Util.ToString(dtMainLoop.Rows[i]["JUSHO1"]) + "" + Util.ToString(dtMainLoop.Rows[i]["JUSHO2"]);
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, NakagaininJusho); // 住所
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["DENWA_BANGO"]); // 電話番号
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FAX_BANGO"]); // FAX番号
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_HASU_SHORI"]); // 金額計算区分
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion

                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;

            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}

