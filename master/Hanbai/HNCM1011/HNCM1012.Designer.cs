﻿namespace jp.co.fsi.hn.hncm1011
{
    partial class HNCM1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblNakagaininCd = new System.Windows.Forms.Label();
			this.txtNakagaininCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txthyoji = new jp.co.fsi.common.controls.FsiTextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.FsiTextBox1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMyNumber = new jp.co.fsi.common.controls.FsiTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtKaishuBi = new jp.co.fsi.common.controls.FsiTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
			this.lblShzHsSrMemo = new System.Windows.Forms.Label();
			this.lblSkshKskMemo = new System.Windows.Forms.Label();
			this.lblSkshHkMemo = new System.Windows.Forms.Label();
			this.lblShimebiMemo = new System.Windows.Forms.Label();
			this.lblSeikyusakiNm = new System.Windows.Forms.Label();
			this.txtKaishuTsuki = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeikyushoKeishiki = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeikyushoHakko = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShimebi = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeikyusakiCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
			this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
			this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
			this.lblTantoshaNm = new System.Windows.Forms.Label();
			this.lblAddBar = new System.Windows.Forms.Label();
			this.txtYubinBango2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKingakuHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtFax = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTel = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtYubinBango1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRyNakagaininNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNakagaininKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNakagaininNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKaishuTsuki = new System.Windows.Forms.Label();
			this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
			this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
			this.lblSeikyushoKeishiki = new System.Windows.Forms.Label();
			this.lblSeikyushoHakko = new System.Windows.Forms.Label();
			this.lblShimebi = new System.Windows.Forms.Label();
			this.lblSeikyusakiCd = new System.Windows.Forms.Label();
			this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
			this.lblKingakuHasuShori = new System.Windows.Forms.Label();
			this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
			this.lblTantoshaCd = new System.Windows.Forms.Label();
			this.lblFax = new System.Windows.Forms.Label();
			this.lblTel = new System.Windows.Forms.Label();
			this.lblJusho2 = new System.Windows.Forms.Label();
			this.lblJusho1 = new System.Windows.Forms.Label();
			this.lblYubinBango = new System.Windows.Forms.Label();
			this.lblRyNakagaininNm = new System.Windows.Forms.Label();
			this.lblNakagaininKanaNm = new System.Windows.Forms.Label();
			this.lblNakagaininNm = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel13 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel13.SuspendLayout();
			this.fsiPanel12.SuspendLayout();
			this.fsiPanel11.SuspendLayout();
			this.fsiPanel10.SuspendLayout();
			this.fsiPanel9.SuspendLayout();
			this.fsiPanel8.SuspendLayout();
			this.fsiPanel7.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF3
			// 
			this.btnF3.Text = "F3\r\n\r\n削除";
			// 
			// btnF6
			// 
			this.btnF6.Text = "F6\r\n\r\n登録";
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(7, 503);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1045, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1034, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblNakagaininCd
			// 
			this.lblNakagaininCd.BackColor = System.Drawing.Color.Silver;
			this.lblNakagaininCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblNakagaininCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininCd.Location = new System.Drawing.Point(0, 0);
			this.lblNakagaininCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininCd.Name = "lblNakagaininCd";
			this.lblNakagaininCd.Size = new System.Drawing.Size(1002, 27);
			this.lblNakagaininCd.TabIndex = 0;
			this.lblNakagaininCd.Tag = "CHANGE";
			this.lblNakagaininCd.Text = "仲 買 人 C D";
			this.lblNakagaininCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNakagaininCd
			// 
			this.txtNakagaininCd.AllowDrop = true;
			this.txtNakagaininCd.AutoSizeFromLength = false;
			this.txtNakagaininCd.DisplayLength = null;
			this.txtNakagaininCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNakagaininCd.Location = new System.Drawing.Point(154, 2);
			this.txtNakagaininCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtNakagaininCd.MaxLength = 4;
			this.txtNakagaininCd.Name = "txtNakagaininCd";
			this.txtNakagaininCd.Size = new System.Drawing.Size(44, 23);
			this.txtNakagaininCd.TabIndex = 1;
			this.txtNakagaininCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNakagaininCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCd_Validating);
			// 
			// txthyoji
			// 
			this.txthyoji.AutoSizeFromLength = false;
			this.txthyoji.BackColor = System.Drawing.Color.White;
			this.txthyoji.DisplayLength = null;
			this.txthyoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txthyoji.Location = new System.Drawing.Point(660, 1);
			this.txthyoji.Margin = new System.Windows.Forms.Padding(5);
			this.txthyoji.MaxLength = 1;
			this.txthyoji.Name = "txthyoji";
			this.txthyoji.Size = new System.Drawing.Size(41, 23);
			this.txthyoji.TabIndex = 23;
			this.txthyoji.Text = "0";
			this.txthyoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txthyoji.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txthyoji_KeyDown);
			this.txthyoji.Validating += new System.ComponentModel.CancelEventHandler(this.txthyoji_Validating);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label4.Location = new System.Drawing.Point(708, 6);
			this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(200, 16);
			this.label4.TabIndex = 59;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "0:表示する 1:表示しない ";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.BackColor = System.Drawing.Color.Silver;
			this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label5.Location = new System.Drawing.Point(531, 5);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 57;
			this.label5.Tag = "CHANGE";
			this.label5.Text = "一覧表示";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.SkyBlue;
			this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.button1.ForeColor = System.Drawing.Color.Navy;
			this.button1.Location = new System.Drawing.Point(817, 1);
			this.button1.Margin = new System.Windows.Forms.Padding(4);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(100, 23);
			this.button1.TabIndex = 56;
			this.button1.TabStop = false;
			this.button1.Text = "公開";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Visible = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// FsiTextBox1
			// 
			this.FsiTextBox1.AutoSizeFromLength = false;
			this.FsiTextBox1.DisplayLength = null;
			this.FsiTextBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.FsiTextBox1.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.FsiTextBox1.Location = new System.Drawing.Point(660, 1);
			this.FsiTextBox1.Margin = new System.Windows.Forms.Padding(5);
			this.FsiTextBox1.MaxLength = 15;
			this.FsiTextBox1.Name = "FsiTextBox1";
			this.FsiTextBox1.ReadOnly = true;
			this.FsiTextBox1.Size = new System.Drawing.Size(151, 23);
			this.FsiTextBox1.TabIndex = 22;
			this.FsiTextBox1.Text = "************";
			this.FsiTextBox1.Visible = false;
			// 
			// txtMyNumber
			// 
			this.txtMyNumber.AutoSizeFromLength = false;
			this.txtMyNumber.DisplayLength = null;
			this.txtMyNumber.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMyNumber.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtMyNumber.Location = new System.Drawing.Point(660, 1);
			this.txtMyNumber.Margin = new System.Windows.Forms.Padding(4);
			this.txtMyNumber.MaxLength = 12;
			this.txtMyNumber.Name = "txtMyNumber";
			this.txtMyNumber.ReadOnly = true;
			this.txtMyNumber.Size = new System.Drawing.Size(151, 23);
			this.txtMyNumber.TabIndex = 54;
			this.txtMyNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtMyNumber_Validating);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(531, 5);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(104, 16);
			this.label3.TabIndex = 53;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "マイナンバー";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKaishuBi
			// 
			this.txtKaishuBi.AllowDrop = true;
			this.txtKaishuBi.AutoSizeFromLength = false;
			this.txtKaishuBi.BackColor = System.Drawing.Color.White;
			this.txtKaishuBi.DisplayLength = null;
			this.txtKaishuBi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKaishuBi.Location = new System.Drawing.Point(913, 2);
			this.txtKaishuBi.Margin = new System.Windows.Forms.Padding(5);
			this.txtKaishuBi.MaxLength = 2;
			this.txtKaishuBi.Name = "txtKaishuBi";
			this.txtKaishuBi.Size = new System.Drawing.Size(43, 23);
			this.txtKaishuBi.TabIndex = 21;
			this.txtKaishuBi.Text = "99";
			this.txtKaishuBi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKaishuBi.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuBi_Validating);
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(910, -2);
			this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(76, 33);
			this.label2.TabIndex = 52;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "日";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(708, 4);
			this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(200, 16);
			this.label1.TabIndex = 50;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "0:当月 1:翌月 2:翌々月…";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShzTnkHohoMemo
			// 
			this.lblShzTnkHohoMemo.AutoSize = true;
			this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(708, 5);
			this.lblShzTnkHohoMemo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
			this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(264, 16);
			this.lblShzTnkHohoMemo.TabIndex = 47;
			this.lblShzTnkHohoMemo.Tag = "CHANGE";
			this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
			this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShzHsSrMemo
			// 
			this.lblShzHsSrMemo.AutoSize = true;
			this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzHsSrMemo.Location = new System.Drawing.Point(708, 4);
			this.lblShzHsSrMemo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
			this.lblShzHsSrMemo.Size = new System.Drawing.Size(264, 16);
			this.lblShzHsSrMemo.TabIndex = 44;
			this.lblShzHsSrMemo.Tag = "CHANGE";
			this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
			this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSkshKskMemo
			// 
			this.lblSkshKskMemo.AutoSize = true;
			this.lblSkshKskMemo.BackColor = System.Drawing.Color.Silver;
			this.lblSkshKskMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSkshKskMemo.Location = new System.Drawing.Point(708, 5);
			this.lblSkshKskMemo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSkshKskMemo.Name = "lblSkshKskMemo";
			this.lblSkshKskMemo.Size = new System.Drawing.Size(144, 16);
			this.lblSkshKskMemo.TabIndex = 41;
			this.lblSkshKskMemo.Tag = "CHANGE";
			this.lblSkshKskMemo.Text = "1:合計型 2:明細型";
			this.lblSkshKskMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSkshHkMemo
			// 
			this.lblSkshHkMemo.AutoSize = true;
			this.lblSkshHkMemo.BackColor = System.Drawing.Color.Silver;
			this.lblSkshHkMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSkshHkMemo.Location = new System.Drawing.Point(708, 4);
			this.lblSkshHkMemo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSkshHkMemo.Name = "lblSkshHkMemo";
			this.lblSkshHkMemo.Size = new System.Drawing.Size(128, 16);
			this.lblSkshHkMemo.TabIndex = 38;
			this.lblSkshHkMemo.Tag = "CHANGE";
			this.lblSkshHkMemo.Text = "1:する 2:しない";
			this.lblSkshHkMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShimebiMemo
			// 
			this.lblShimebiMemo.AutoSize = true;
			this.lblShimebiMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShimebiMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShimebiMemo.Location = new System.Drawing.Point(708, 5);
			this.lblShimebiMemo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShimebiMemo.Name = "lblShimebiMemo";
			this.lblShimebiMemo.Size = new System.Drawing.Size(152, 16);
			this.lblShimebiMemo.TabIndex = 35;
			this.lblShimebiMemo.Tag = "CHANGE";
			this.lblShimebiMemo.Text = "1～28 ※末締めは99";
			this.lblShimebiMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyusakiNm
			// 
			this.lblSeikyusakiNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeikyusakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeikyusakiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyusakiNm.Location = new System.Drawing.Point(753, 1);
			this.lblSeikyusakiNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSeikyusakiNm.Name = "lblSeikyusakiNm";
			this.lblSeikyusakiNm.Size = new System.Drawing.Size(230, 24);
			this.lblSeikyusakiNm.TabIndex = 32;
			this.lblSeikyusakiNm.Tag = "DISPNAME";
			this.lblSeikyusakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKaishuTsuki
			// 
			this.txtKaishuTsuki.AllowDrop = true;
			this.txtKaishuTsuki.AutoSizeFromLength = false;
			this.txtKaishuTsuki.BackColor = System.Drawing.Color.White;
			this.txtKaishuTsuki.DisplayLength = null;
			this.txtKaishuTsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKaishuTsuki.Location = new System.Drawing.Point(660, 2);
			this.txtKaishuTsuki.Margin = new System.Windows.Forms.Padding(5);
			this.txtKaishuTsuki.MaxLength = 1;
			this.txtKaishuTsuki.Name = "txtKaishuTsuki";
			this.txtKaishuTsuki.Size = new System.Drawing.Size(41, 23);
			this.txtKaishuTsuki.TabIndex = 20;
			this.txtKaishuTsuki.Text = "0";
			this.txtKaishuTsuki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKaishuTsuki.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuTsuki_Validating);
			// 
			// txtShohizeiTenkaHoho
			// 
			this.txtShohizeiTenkaHoho.AllowDrop = true;
			this.txtShohizeiTenkaHoho.AutoSizeFromLength = false;
			this.txtShohizeiTenkaHoho.BackColor = System.Drawing.Color.White;
			this.txtShohizeiTenkaHoho.DisplayLength = null;
			this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(660, 2);
			this.txtShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(5);
			this.txtShohizeiTenkaHoho.MaxLength = 1;
			this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
			this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(41, 23);
			this.txtShohizeiTenkaHoho.TabIndex = 19;
			this.txtShohizeiTenkaHoho.Text = "2";
			this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
			// 
			// txtShohizeiHasuShori
			// 
			this.txtShohizeiHasuShori.AllowDrop = true;
			this.txtShohizeiHasuShori.AutoSizeFromLength = false;
			this.txtShohizeiHasuShori.BackColor = System.Drawing.Color.White;
			this.txtShohizeiHasuShori.DisplayLength = null;
			this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiHasuShori.Location = new System.Drawing.Point(660, 2);
			this.txtShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(5);
			this.txtShohizeiHasuShori.MaxLength = 1;
			this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
			this.txtShohizeiHasuShori.Size = new System.Drawing.Size(41, 23);
			this.txtShohizeiHasuShori.TabIndex = 18;
			this.txtShohizeiHasuShori.Text = "2";
			this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
			// 
			// txtSeikyushoKeishiki
			// 
			this.txtSeikyushoKeishiki.AllowDrop = true;
			this.txtSeikyushoKeishiki.AutoSizeFromLength = false;
			this.txtSeikyushoKeishiki.BackColor = System.Drawing.Color.White;
			this.txtSeikyushoKeishiki.DisplayLength = null;
			this.txtSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeikyushoKeishiki.Location = new System.Drawing.Point(660, 2);
			this.txtSeikyushoKeishiki.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeikyushoKeishiki.MaxLength = 1;
			this.txtSeikyushoKeishiki.Name = "txtSeikyushoKeishiki";
			this.txtSeikyushoKeishiki.Size = new System.Drawing.Size(41, 23);
			this.txtSeikyushoKeishiki.TabIndex = 17;
			this.txtSeikyushoKeishiki.Text = "2";
			this.txtSeikyushoKeishiki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeikyushoKeishiki.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoKeishiki_Validating);
			// 
			// txtSeikyushoHakko
			// 
			this.txtSeikyushoHakko.AllowDrop = true;
			this.txtSeikyushoHakko.AutoSizeFromLength = false;
			this.txtSeikyushoHakko.BackColor = System.Drawing.Color.White;
			this.txtSeikyushoHakko.DisplayLength = null;
			this.txtSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeikyushoHakko.Location = new System.Drawing.Point(660, 2);
			this.txtSeikyushoHakko.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeikyushoHakko.MaxLength = 1;
			this.txtSeikyushoHakko.Name = "txtSeikyushoHakko";
			this.txtSeikyushoHakko.Size = new System.Drawing.Size(41, 23);
			this.txtSeikyushoHakko.TabIndex = 16;
			this.txtSeikyushoHakko.Text = "2";
			this.txtSeikyushoHakko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeikyushoHakko.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoHakko_Validating);
			// 
			// txtShimebi
			// 
			this.txtShimebi.AllowDrop = true;
			this.txtShimebi.AutoSizeFromLength = false;
			this.txtShimebi.BackColor = System.Drawing.Color.White;
			this.txtShimebi.DisplayLength = null;
			this.txtShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShimebi.Location = new System.Drawing.Point(660, 2);
			this.txtShimebi.Margin = new System.Windows.Forms.Padding(5);
			this.txtShimebi.MaxLength = 2;
			this.txtShimebi.Name = "txtShimebi";
			this.txtShimebi.Size = new System.Drawing.Size(41, 23);
			this.txtShimebi.TabIndex = 15;
			this.txtShimebi.Text = "99";
			this.txtShimebi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShimebi.Validating += new System.ComponentModel.CancelEventHandler(this.txtShimebi_Validating);
			// 
			// txtSeikyusakiCd
			// 
			this.txtSeikyusakiCd.AutoSizeFromLength = true;
			this.txtSeikyusakiCd.BackColor = System.Drawing.SystemColors.Window;
			this.txtSeikyusakiCd.DisplayLength = null;
			this.txtSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeikyusakiCd.Location = new System.Drawing.Point(660, 2);
			this.txtSeikyusakiCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeikyusakiCd.MaxLength = 4;
			this.txtSeikyusakiCd.Name = "txtSeikyusakiCd";
			this.txtSeikyusakiCd.Size = new System.Drawing.Size(89, 23);
			this.txtSeikyusakiCd.TabIndex = 14;
			this.txtSeikyusakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblShzNrkHohoNm
			// 
			this.lblShzNrkHohoNm.AutoSize = true;
			this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.Silver;
			this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzNrkHohoNm.Location = new System.Drawing.Point(179, 6);
			this.lblShzNrkHohoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
			this.lblShzNrkHohoNm.Size = new System.Drawing.Size(216, 16);
			this.lblShzNrkHohoNm.TabIndex = 29;
			this.lblShzNrkHohoNm.Tag = "CHANGE";
			this.lblShzNrkHohoNm.Text = "税抜き入力（自動計算あり）";
			this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKgkHsShrMemo
			// 
			this.lblKgkHsShrMemo.AutoSize = true;
			this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKgkHsShrMemo.Location = new System.Drawing.Point(179, 5);
			this.lblKgkHsShrMemo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
			this.lblKgkHsShrMemo.Size = new System.Drawing.Size(248, 16);
			this.lblKgkHsShrMemo.TabIndex = 26;
			this.lblKgkHsShrMemo.Tag = "CHANGE";
			this.lblKgkHsShrMemo.Text = "1:切捨て 2:四捨五入 3:切り上げ";
			this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTnkStkHohoMemo
			// 
			this.lblTnkStkHohoMemo.AutoSize = true;
			this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(179, 6);
			this.lblTnkStkHohoMemo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
			this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(248, 16);
			this.lblTnkStkHohoMemo.TabIndex = 23;
			this.lblTnkStkHohoMemo.Tag = "CHANGE";
			this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
			this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTantoshaNm
			// 
			this.lblTantoshaNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoshaNm.Location = new System.Drawing.Point(236, 1);
			this.lblTantoshaNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTantoshaNm.Name = "lblTantoshaNm";
			this.lblTantoshaNm.Size = new System.Drawing.Size(283, 24);
			this.lblTantoshaNm.TabIndex = 20;
			this.lblTantoshaNm.Tag = "DISPNAME";
			this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAddBar
			// 
			this.lblAddBar.AutoSize = true;
			this.lblAddBar.BackColor = System.Drawing.Color.Silver;
			this.lblAddBar.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblAddBar.Location = new System.Drawing.Point(211, 8);
			this.lblAddBar.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblAddBar.Name = "lblAddBar";
			this.lblAddBar.Size = new System.Drawing.Size(16, 16);
			this.lblAddBar.TabIndex = 8;
			this.lblAddBar.Tag = "CHANGE";
			this.lblAddBar.Text = "-";
			// 
			// txtYubinBango2
			// 
			this.txtYubinBango2.AllowDrop = true;
			this.txtYubinBango2.AutoSizeFromLength = false;
			this.txtYubinBango2.BackColor = System.Drawing.Color.White;
			this.txtYubinBango2.DisplayLength = null;
			this.txtYubinBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYubinBango2.Location = new System.Drawing.Point(235, 2);
			this.txtYubinBango2.Margin = new System.Windows.Forms.Padding(5);
			this.txtYubinBango2.MaxLength = 4;
			this.txtYubinBango2.Name = "txtYubinBango2";
			this.txtYubinBango2.Size = new System.Drawing.Size(60, 23);
			this.txtYubinBango2.TabIndex = 5;
			this.txtYubinBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango2_Validating);
			// 
			// txtShohizeiNyuryokuHoho
			// 
			this.txtShohizeiNyuryokuHoho.AllowDrop = true;
			this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
			this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.White;
			this.txtShohizeiNyuryokuHoho.DisplayLength = null;
			this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(154, 3);
			this.txtShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(5);
			this.txtShohizeiNyuryokuHoho.MaxLength = 1;
			this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
			this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(19, 23);
			this.txtShohizeiNyuryokuHoho.TabIndex = 13;
			this.txtShohizeiNyuryokuHoho.Text = "2";
			this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtKingakuHasuShori
			// 
			this.txtKingakuHasuShori.AllowDrop = true;
			this.txtKingakuHasuShori.AutoSizeFromLength = false;
			this.txtKingakuHasuShori.BackColor = System.Drawing.Color.White;
			this.txtKingakuHasuShori.DisplayLength = null;
			this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKingakuHasuShori.Location = new System.Drawing.Point(154, 2);
			this.txtKingakuHasuShori.Margin = new System.Windows.Forms.Padding(5);
			this.txtKingakuHasuShori.MaxLength = 1;
			this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
			this.txtKingakuHasuShori.Size = new System.Drawing.Size(19, 23);
			this.txtKingakuHasuShori.TabIndex = 12;
			this.txtKingakuHasuShori.Text = "2";
			this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
			// 
			// txtTankaShutokuHoho
			// 
			this.txtTankaShutokuHoho.AllowDrop = true;
			this.txtTankaShutokuHoho.AutoSizeFromLength = false;
			this.txtTankaShutokuHoho.BackColor = System.Drawing.Color.White;
			this.txtTankaShutokuHoho.DisplayLength = null;
			this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTankaShutokuHoho.Location = new System.Drawing.Point(154, 2);
			this.txtTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(5);
			this.txtTankaShutokuHoho.MaxLength = 1;
			this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
			this.txtTankaShutokuHoho.Size = new System.Drawing.Size(19, 23);
			this.txtTankaShutokuHoho.TabIndex = 11;
			this.txtTankaShutokuHoho.Text = "0";
			this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
			// 
			// txtTantoshaCd
			// 
			this.txtTantoshaCd.AllowDrop = true;
			this.txtTantoshaCd.AutoSizeFromLength = false;
			this.txtTantoshaCd.BackColor = System.Drawing.Color.White;
			this.txtTantoshaCd.DisplayLength = null;
			this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTantoshaCd.Location = new System.Drawing.Point(154, 2);
			this.txtTantoshaCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtTantoshaCd.MaxLength = 4;
			this.txtTantoshaCd.Name = "txtTantoshaCd";
			this.txtTantoshaCd.Size = new System.Drawing.Size(76, 23);
			this.txtTantoshaCd.TabIndex = 10;
			this.txtTantoshaCd.Text = "0";
			this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
			// 
			// txtFax
			// 
			this.txtFax.AllowDrop = true;
			this.txtFax.AutoSizeFromLength = false;
			this.txtFax.BackColor = System.Drawing.Color.White;
			this.txtFax.DisplayLength = null;
			this.txtFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFax.Location = new System.Drawing.Point(154, 2);
			this.txtFax.Margin = new System.Windows.Forms.Padding(5);
			this.txtFax.MaxLength = 15;
			this.txtFax.Name = "txtFax";
			this.txtFax.Size = new System.Drawing.Size(367, 23);
			this.txtFax.TabIndex = 9;
			this.txtFax.Validating += new System.ComponentModel.CancelEventHandler(this.txtFax_Validating);
			// 
			// txtTel
			// 
			this.txtTel.AllowDrop = true;
			this.txtTel.AutoSizeFromLength = false;
			this.txtTel.BackColor = System.Drawing.Color.White;
			this.txtTel.DisplayLength = null;
			this.txtTel.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTel.Location = new System.Drawing.Point(154, 2);
			this.txtTel.Margin = new System.Windows.Forms.Padding(5);
			this.txtTel.MaxLength = 15;
			this.txtTel.Name = "txtTel";
			this.txtTel.Size = new System.Drawing.Size(367, 23);
			this.txtTel.TabIndex = 8;
			this.txtTel.Validating += new System.ComponentModel.CancelEventHandler(this.txtTel_Validating);
			// 
			// txtJusho2
			// 
			this.txtJusho2.AllowDrop = true;
			this.txtJusho2.AutoSizeFromLength = false;
			this.txtJusho2.BackColor = System.Drawing.Color.White;
			this.txtJusho2.DisplayLength = null;
			this.txtJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtJusho2.Location = new System.Drawing.Point(154, 2);
			this.txtJusho2.Margin = new System.Windows.Forms.Padding(5);
			this.txtJusho2.MaxLength = 30;
			this.txtJusho2.Name = "txtJusho2";
			this.txtJusho2.Size = new System.Drawing.Size(367, 23);
			this.txtJusho2.TabIndex = 7;
			this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho2_Validating);
			// 
			// txtJusho1
			// 
			this.txtJusho1.AllowDrop = true;
			this.txtJusho1.AutoSizeFromLength = false;
			this.txtJusho1.BackColor = System.Drawing.Color.White;
			this.txtJusho1.DisplayLength = null;
			this.txtJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtJusho1.Location = new System.Drawing.Point(154, 2);
			this.txtJusho1.Margin = new System.Windows.Forms.Padding(5);
			this.txtJusho1.MaxLength = 30;
			this.txtJusho1.Name = "txtJusho1";
			this.txtJusho1.Size = new System.Drawing.Size(367, 23);
			this.txtJusho1.TabIndex = 6;
			this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho1_Validating);
			// 
			// txtYubinBango1
			// 
			this.txtYubinBango1.AllowDrop = true;
			this.txtYubinBango1.AutoSizeFromLength = false;
			this.txtYubinBango1.BackColor = System.Drawing.Color.White;
			this.txtYubinBango1.DisplayLength = null;
			this.txtYubinBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYubinBango1.Location = new System.Drawing.Point(154, 2);
			this.txtYubinBango1.Margin = new System.Windows.Forms.Padding(5);
			this.txtYubinBango1.MaxLength = 3;
			this.txtYubinBango1.Name = "txtYubinBango1";
			this.txtYubinBango1.Size = new System.Drawing.Size(51, 23);
			this.txtYubinBango1.TabIndex = 4;
			this.txtYubinBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango1_Validating);
			// 
			// txtRyNakagaininNm
			// 
			this.txtRyNakagaininNm.AllowDrop = true;
			this.txtRyNakagaininNm.AutoSizeFromLength = false;
			this.txtRyNakagaininNm.BackColor = System.Drawing.Color.White;
			this.txtRyNakagaininNm.DisplayLength = null;
			this.txtRyNakagaininNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRyNakagaininNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtRyNakagaininNm.Location = new System.Drawing.Point(154, 2);
			this.txtRyNakagaininNm.Margin = new System.Windows.Forms.Padding(5);
			this.txtRyNakagaininNm.MaxLength = 20;
			this.txtRyNakagaininNm.Name = "txtRyNakagaininNm";
			this.txtRyNakagaininNm.Size = new System.Drawing.Size(368, 23);
			this.txtRyNakagaininNm.TabIndex = 3;
			this.txtRyNakagaininNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtRyNakagaininNm_Validating);
			// 
			// txtNakagaininKanaNm
			// 
			this.txtNakagaininKanaNm.AllowDrop = true;
			this.txtNakagaininKanaNm.AutoSizeFromLength = false;
			this.txtNakagaininKanaNm.BackColor = System.Drawing.Color.White;
			this.txtNakagaininKanaNm.DisplayLength = null;
			this.txtNakagaininKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNakagaininKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtNakagaininKanaNm.Location = new System.Drawing.Point(154, 2);
			this.txtNakagaininKanaNm.Margin = new System.Windows.Forms.Padding(5);
			this.txtNakagaininKanaNm.MaxLength = 30;
			this.txtNakagaininKanaNm.Name = "txtNakagaininKanaNm";
			this.txtNakagaininKanaNm.Size = new System.Drawing.Size(368, 23);
			this.txtNakagaininKanaNm.TabIndex = 2;
			this.txtNakagaininKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininKanaNm_Validating);
			// 
			// txtNakagaininNm
			// 
			this.txtNakagaininNm.AllowDrop = true;
			this.txtNakagaininNm.AutoSizeFromLength = false;
			this.txtNakagaininNm.BackColor = System.Drawing.Color.White;
			this.txtNakagaininNm.DisplayLength = null;
			this.txtNakagaininNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNakagaininNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtNakagaininNm.Location = new System.Drawing.Point(154, 1);
			this.txtNakagaininNm.Margin = new System.Windows.Forms.Padding(5);
			this.txtNakagaininNm.MaxLength = 40;
			this.txtNakagaininNm.Name = "txtNakagaininNm";
			this.txtNakagaininNm.Size = new System.Drawing.Size(368, 23);
			this.txtNakagaininNm.TabIndex = 1;
			this.txtNakagaininNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininNm_Validating);
			// 
			// lblKaishuTsuki
			// 
			this.lblKaishuTsuki.AutoSize = true;
			this.lblKaishuTsuki.BackColor = System.Drawing.Color.Silver;
			this.lblKaishuTsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKaishuTsuki.Location = new System.Drawing.Point(531, 5);
			this.lblKaishuTsuki.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKaishuTsuki.Name = "lblKaishuTsuki";
			this.lblKaishuTsuki.Size = new System.Drawing.Size(56, 16);
			this.lblKaishuTsuki.TabIndex = 48;
			this.lblKaishuTsuki.Tag = "CHANGE";
			this.lblKaishuTsuki.Text = "回収日";
			this.lblKaishuTsuki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiTenkaHoho
			// 
			this.lblShohizeiTenkaHoho.AutoSize = true;
			this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(531, 3);
			this.lblShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
			this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(120, 16);
			this.lblShohizeiTenkaHoho.TabIndex = 45;
			this.lblShohizeiTenkaHoho.Tag = "CHANGE";
			this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
			this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiHasuShori
			// 
			this.lblShohizeiHasuShori.AutoSize = true;
			this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiHasuShori.Location = new System.Drawing.Point(531, 5);
			this.lblShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
			this.lblShohizeiHasuShori.Size = new System.Drawing.Size(120, 16);
			this.lblShohizeiHasuShori.TabIndex = 42;
			this.lblShohizeiHasuShori.Tag = "CHANGE";
			this.lblShohizeiHasuShori.Text = "消費税端数処理";
			this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyushoKeishiki
			// 
			this.lblSeikyushoKeishiki.AutoSize = true;
			this.lblSeikyushoKeishiki.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyushoKeishiki.Location = new System.Drawing.Point(531, 5);
			this.lblSeikyushoKeishiki.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoKeishiki.Name = "lblSeikyushoKeishiki";
			this.lblSeikyushoKeishiki.Size = new System.Drawing.Size(88, 16);
			this.lblSeikyushoKeishiki.TabIndex = 39;
			this.lblSeikyushoKeishiki.Tag = "CHANGE";
			this.lblSeikyushoKeishiki.Text = "請求書形式";
			this.lblSeikyushoKeishiki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyushoHakko
			// 
			this.lblSeikyushoHakko.AutoSize = true;
			this.lblSeikyushoHakko.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyushoHakko.Location = new System.Drawing.Point(531, 5);
			this.lblSeikyushoHakko.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoHakko.Name = "lblSeikyushoHakko";
			this.lblSeikyushoHakko.Size = new System.Drawing.Size(88, 16);
			this.lblSeikyushoHakko.TabIndex = 36;
			this.lblSeikyushoHakko.Tag = "CHANGE";
			this.lblSeikyushoHakko.Text = "請求書発行";
			this.lblSeikyushoHakko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShimebi
			// 
			this.lblShimebi.AutoSize = true;
			this.lblShimebi.BackColor = System.Drawing.Color.Silver;
			this.lblShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShimebi.Location = new System.Drawing.Point(531, 5);
			this.lblShimebi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShimebi.Name = "lblShimebi";
			this.lblShimebi.Size = new System.Drawing.Size(40, 16);
			this.lblShimebi.TabIndex = 33;
			this.lblShimebi.Tag = "CHANGE";
			this.lblShimebi.Text = "締日";
			this.lblShimebi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyusakiCd
			// 
			this.lblSeikyusakiCd.AutoSize = true;
			this.lblSeikyusakiCd.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyusakiCd.Location = new System.Drawing.Point(531, 5);
			this.lblSeikyusakiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyusakiCd.Name = "lblSeikyusakiCd";
			this.lblSeikyusakiCd.Size = new System.Drawing.Size(104, 16);
			this.lblSeikyusakiCd.TabIndex = 30;
			this.lblSeikyusakiCd.Tag = "CHANGE";
			this.lblSeikyusakiCd.Text = "請求先コード";
			this.lblSeikyusakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiNyuryokuHoho
			// 
			this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohizeiNyuryokuHoho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(0, 0);
			this.lblShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
			this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(1002, 30);
			this.lblShohizeiNyuryokuHoho.TabIndex = 27;
			this.lblShohizeiNyuryokuHoho.Tag = "CHANGE";
			this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
			this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKingakuHasuShori
			// 
			this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblKingakuHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKingakuHasuShori.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKingakuHasuShori.Location = new System.Drawing.Point(0, 0);
			this.lblKingakuHasuShori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
			this.lblKingakuHasuShori.Size = new System.Drawing.Size(1002, 27);
			this.lblKingakuHasuShori.TabIndex = 24;
			this.lblKingakuHasuShori.Tag = "CHANGE";
			this.lblKingakuHasuShori.Text = "金額端数処理";
			this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTankaShutokuHoho
			// 
			this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblTankaShutokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTankaShutokuHoho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTankaShutokuHoho.Location = new System.Drawing.Point(0, 0);
			this.lblTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
			this.lblTankaShutokuHoho.Size = new System.Drawing.Size(1002, 27);
			this.lblTankaShutokuHoho.TabIndex = 21;
			this.lblTankaShutokuHoho.Tag = "CHANGE";
			this.lblTankaShutokuHoho.Text = "単価取得方法";
			this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTantoshaCd
			// 
			this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
			this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoshaCd.Location = new System.Drawing.Point(0, 0);
			this.lblTantoshaCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaCd.Name = "lblTantoshaCd";
			this.lblTantoshaCd.Size = new System.Drawing.Size(1002, 27);
			this.lblTantoshaCd.TabIndex = 18;
			this.lblTantoshaCd.Tag = "CHANGE";
			this.lblTantoshaCd.Text = "担当者コード";
			this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFax
			// 
			this.lblFax.BackColor = System.Drawing.Color.Silver;
			this.lblFax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFax.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFax.Location = new System.Drawing.Point(0, 0);
			this.lblFax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFax.Name = "lblFax";
			this.lblFax.Size = new System.Drawing.Size(1002, 27);
			this.lblFax.TabIndex = 16;
			this.lblFax.Tag = "CHANGE";
			this.lblFax.Text = "ＦＡＸ番号";
			this.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTel
			// 
			this.lblTel.BackColor = System.Drawing.Color.Silver;
			this.lblTel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTel.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTel.Location = new System.Drawing.Point(0, 0);
			this.lblTel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTel.Name = "lblTel";
			this.lblTel.Size = new System.Drawing.Size(1002, 27);
			this.lblTel.TabIndex = 14;
			this.lblTel.Tag = "CHANGE";
			this.lblTel.Text = "電話番号";
			this.lblTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJusho2
			// 
			this.lblJusho2.BackColor = System.Drawing.Color.Silver;
			this.lblJusho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJusho2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJusho2.Location = new System.Drawing.Point(0, 0);
			this.lblJusho2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJusho2.Name = "lblJusho2";
			this.lblJusho2.Size = new System.Drawing.Size(1002, 27);
			this.lblJusho2.TabIndex = 12;
			this.lblJusho2.Tag = "CHANGE";
			this.lblJusho2.Text = "住所２";
			this.lblJusho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJusho1
			// 
			this.lblJusho1.BackColor = System.Drawing.Color.Silver;
			this.lblJusho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJusho1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJusho1.Location = new System.Drawing.Point(0, 0);
			this.lblJusho1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJusho1.Name = "lblJusho1";
			this.lblJusho1.Size = new System.Drawing.Size(1002, 27);
			this.lblJusho1.TabIndex = 10;
			this.lblJusho1.Tag = "CHANGE";
			this.lblJusho1.Text = "住所１";
			this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYubinBango
			// 
			this.lblYubinBango.BackColor = System.Drawing.Color.Silver;
			this.lblYubinBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblYubinBango.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblYubinBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYubinBango.Location = new System.Drawing.Point(0, 0);
			this.lblYubinBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYubinBango.Name = "lblYubinBango";
			this.lblYubinBango.Size = new System.Drawing.Size(1002, 27);
			this.lblYubinBango.TabIndex = 6;
			this.lblYubinBango.Tag = "CHANGE";
			this.lblYubinBango.Text = "郵便番号";
			this.lblYubinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblRyNakagaininNm
			// 
			this.lblRyNakagaininNm.BackColor = System.Drawing.Color.Silver;
			this.lblRyNakagaininNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblRyNakagaininNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblRyNakagaininNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblRyNakagaininNm.Location = new System.Drawing.Point(0, 0);
			this.lblRyNakagaininNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblRyNakagaininNm.Name = "lblRyNakagaininNm";
			this.lblRyNakagaininNm.Size = new System.Drawing.Size(1002, 27);
			this.lblRyNakagaininNm.TabIndex = 4;
			this.lblRyNakagaininNm.Tag = "CHANGE";
			this.lblRyNakagaininNm.Text = "略称仲買人名";
			this.lblRyNakagaininNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblNakagaininKanaNm
			// 
			this.lblNakagaininKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblNakagaininKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblNakagaininKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblNakagaininKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininKanaNm.Name = "lblNakagaininKanaNm";
			this.lblNakagaininKanaNm.Size = new System.Drawing.Size(1002, 27);
			this.lblNakagaininKanaNm.TabIndex = 2;
			this.lblNakagaininKanaNm.Tag = "CHANGE";
			this.lblNakagaininKanaNm.Text = "仲買人カナ名";
			this.lblNakagaininKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblNakagaininKanaNm.Click += new System.EventHandler(this.lblNakagaininKanaNm_Click);
			// 
			// lblNakagaininNm
			// 
			this.lblNakagaininNm.BackColor = System.Drawing.Color.Silver;
			this.lblNakagaininNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblNakagaininNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininNm.Location = new System.Drawing.Point(0, 0);
			this.lblNakagaininNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininNm.Name = "lblNakagaininNm";
			this.lblNakagaininNm.Size = new System.Drawing.Size(1002, 27);
			this.lblNakagaininNm.TabIndex = 0;
			this.lblNakagaininNm.Tag = "CHANGE";
			this.lblNakagaininNm.Text = "正式仲買人名";
			this.lblNakagaininNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel13, 0, 12);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel12, 0, 11);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel11, 0, 10);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel10, 0, 9);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel9, 0, 8);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 7);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(15, 55);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 13;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1010, 446);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel13
			// 
			this.fsiPanel13.Controls.Add(this.lblShzNrkHohoNm);
			this.fsiPanel13.Controls.Add(this.txtShohizeiNyuryokuHoho);
			this.fsiPanel13.Controls.Add(this.lblShohizeiNyuryokuHoho);
			this.fsiPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel13.Location = new System.Drawing.Point(4, 412);
			this.fsiPanel13.Name = "fsiPanel13";
			this.fsiPanel13.Size = new System.Drawing.Size(1002, 30);
			this.fsiPanel13.TabIndex = 12;
			// 
			// fsiPanel12
			// 
			this.fsiPanel12.Controls.Add(this.lblKgkHsShrMemo);
			this.fsiPanel12.Controls.Add(this.txtKingakuHasuShori);
			this.fsiPanel12.Controls.Add(this.lblKingakuHasuShori);
			this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel12.Location = new System.Drawing.Point(4, 378);
			this.fsiPanel12.Name = "fsiPanel12";
			this.fsiPanel12.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel12.TabIndex = 11;
			// 
			// fsiPanel11
			// 
			this.fsiPanel11.Controls.Add(this.txtTankaShutokuHoho);
			this.fsiPanel11.Controls.Add(this.lblTnkStkHohoMemo);
			this.fsiPanel11.Controls.Add(this.lblTankaShutokuHoho);
			this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel11.Location = new System.Drawing.Point(4, 344);
			this.fsiPanel11.Name = "fsiPanel11";
			this.fsiPanel11.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel11.TabIndex = 10;
			// 
			// fsiPanel10
			// 
			this.fsiPanel10.Controls.Add(this.txthyoji);
			this.fsiPanel10.Controls.Add(this.label4);
			this.fsiPanel10.Controls.Add(this.txtTantoshaCd);
			this.fsiPanel10.Controls.Add(this.lblTantoshaNm);
			this.fsiPanel10.Controls.Add(this.label5);
			this.fsiPanel10.Controls.Add(this.lblTantoshaCd);
			this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel10.Location = new System.Drawing.Point(4, 310);
			this.fsiPanel10.Name = "fsiPanel10";
			this.fsiPanel10.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel10.TabIndex = 9;
			// 
			// fsiPanel9
			// 
			this.fsiPanel9.Controls.Add(this.txtFax);
			this.fsiPanel9.Controls.Add(this.label3);
			this.fsiPanel9.Controls.Add(this.button1);
			this.fsiPanel9.Controls.Add(this.FsiTextBox1);
			this.fsiPanel9.Controls.Add(this.txtMyNumber);
			this.fsiPanel9.Controls.Add(this.lblFax);
			this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel9.Location = new System.Drawing.Point(4, 276);
			this.fsiPanel9.Name = "fsiPanel9";
			this.fsiPanel9.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel9.TabIndex = 8;
			// 
			// fsiPanel8
			// 
			this.fsiPanel8.Controls.Add(this.txtKaishuBi);
			this.fsiPanel8.Controls.Add(this.label2);
			this.fsiPanel8.Controls.Add(this.txtTel);
			this.fsiPanel8.Controls.Add(this.lblKaishuTsuki);
			this.fsiPanel8.Controls.Add(this.txtKaishuTsuki);
			this.fsiPanel8.Controls.Add(this.label1);
			this.fsiPanel8.Controls.Add(this.lblTel);
			this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel8.Location = new System.Drawing.Point(4, 242);
			this.fsiPanel8.Name = "fsiPanel8";
			this.fsiPanel8.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel8.TabIndex = 7;
			// 
			// fsiPanel7
			// 
			this.fsiPanel7.Controls.Add(this.txtJusho2);
			this.fsiPanel7.Controls.Add(this.lblShohizeiTenkaHoho);
			this.fsiPanel7.Controls.Add(this.txtShohizeiTenkaHoho);
			this.fsiPanel7.Controls.Add(this.lblShzTnkHohoMemo);
			this.fsiPanel7.Controls.Add(this.lblJusho2);
			this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel7.Location = new System.Drawing.Point(4, 208);
			this.fsiPanel7.Name = "fsiPanel7";
			this.fsiPanel7.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel7.TabIndex = 6;
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.txtJusho1);
			this.fsiPanel6.Controls.Add(this.lblShohizeiHasuShori);
			this.fsiPanel6.Controls.Add(this.txtShohizeiHasuShori);
			this.fsiPanel6.Controls.Add(this.lblShzHsSrMemo);
			this.fsiPanel6.Controls.Add(this.lblJusho1);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel6.Location = new System.Drawing.Point(4, 174);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel6.TabIndex = 5;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.txtYubinBango1);
			this.fsiPanel5.Controls.Add(this.lblSeikyushoKeishiki);
			this.fsiPanel5.Controls.Add(this.txtYubinBango2);
			this.fsiPanel5.Controls.Add(this.lblAddBar);
			this.fsiPanel5.Controls.Add(this.txtSeikyushoKeishiki);
			this.fsiPanel5.Controls.Add(this.lblSkshKskMemo);
			this.fsiPanel5.Controls.Add(this.lblYubinBango);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 140);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel5.TabIndex = 4;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtRyNakagaininNm);
			this.fsiPanel4.Controls.Add(this.lblSeikyushoHakko);
			this.fsiPanel4.Controls.Add(this.lblSkshHkMemo);
			this.fsiPanel4.Controls.Add(this.txtSeikyushoHakko);
			this.fsiPanel4.Controls.Add(this.lblRyNakagaininNm);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 106);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel4.TabIndex = 3;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtNakagaininKanaNm);
			this.fsiPanel3.Controls.Add(this.lblShimebi);
			this.fsiPanel3.Controls.Add(this.txtShimebi);
			this.fsiPanel3.Controls.Add(this.lblShimebiMemo);
			this.fsiPanel3.Controls.Add(this.lblNakagaininKanaNm);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 72);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtNakagaininNm);
			this.fsiPanel2.Controls.Add(this.lblSeikyusakiCd);
			this.fsiPanel2.Controls.Add(this.lblSeikyusakiNm);
			this.fsiPanel2.Controls.Add(this.txtSeikyusakiCd);
			this.fsiPanel2.Controls.Add(this.lblNakagaininNm);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 38);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtNakagaininCd);
			this.fsiPanel1.Controls.Add(this.lblNakagaininCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(1002, 27);
			this.fsiPanel1.TabIndex = 0;
			// 
			// HNCM1012
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1034, 641);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1012";
			this.ShowFButton = true;
			this.Text = "ReportSample";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel13.ResumeLayout(false);
			this.fsiPanel13.PerformLayout();
			this.fsiPanel12.ResumeLayout(false);
			this.fsiPanel12.PerformLayout();
			this.fsiPanel11.ResumeLayout(false);
			this.fsiPanel11.PerformLayout();
			this.fsiPanel10.ResumeLayout(false);
			this.fsiPanel10.PerformLayout();
			this.fsiPanel9.ResumeLayout(false);
			this.fsiPanel9.PerformLayout();
			this.fsiPanel8.ResumeLayout(false);
			this.fsiPanel8.PerformLayout();
			this.fsiPanel7.ResumeLayout(false);
			this.fsiPanel7.PerformLayout();
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblNakagaininCd;
        private jp.co.fsi.common.controls.FsiTextBox txtNakagaininCd;
        private System.Windows.Forms.Label lblFax;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.Label lblJusho2;
        private System.Windows.Forms.Label lblJusho1;
        private System.Windows.Forms.Label lblYubinBango;
        private System.Windows.Forms.Label lblRyNakagaininNm;
        private System.Windows.Forms.Label lblNakagaininKanaNm;
        private System.Windows.Forms.Label lblNakagaininNm;
        private System.Windows.Forms.Label lblKaishuTsuki;
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblSeikyushoKeishiki;
        private System.Windows.Forms.Label lblSeikyushoHakko;
        private System.Windows.Forms.Label lblShimebi;
        private System.Windows.Forms.Label lblSeikyusakiCd;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private System.Windows.Forms.Label lblTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtNakagaininNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtKingakuHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtTankaShutokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFax;
        private jp.co.fsi.common.controls.FsiTextBox txtTel;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho2;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho1;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango1;
        private jp.co.fsi.common.controls.FsiTextBox txtRyNakagaininNm;
        private jp.co.fsi.common.controls.FsiTextBox txtNakagaininKanaNm;
        private System.Windows.Forms.Label lblAddBar;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango2;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuTsuki;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiTenkaHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoKeishiki;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoHakko;
        private jp.co.fsi.common.controls.FsiTextBox txtShimebi;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyusakiCd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
        private System.Windows.Forms.Label lblShzHsSrMemo;
        private System.Windows.Forms.Label lblSkshKskMemo;
        private System.Windows.Forms.Label lblSkshHkMemo;
        private System.Windows.Forms.Label lblShimebiMemo;
        private System.Windows.Forms.Label lblSeikyusakiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuBi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private common.controls.FsiTextBox FsiTextBox1;
        private common.controls.FsiTextBox txtMyNumber;
        private System.Windows.Forms.Label label3;
        private common.controls.FsiTextBox txthyoji;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel13;
		private common.FsiPanel fsiPanel12;
		private common.FsiPanel fsiPanel11;
		private common.FsiPanel fsiPanel10;
		private common.FsiPanel fsiPanel9;
		private common.FsiPanel fsiPanel8;
		private common.FsiPanel fsiPanel7;
		private common.FsiPanel fsiPanel6;
		private common.FsiPanel fsiPanel5;
		private common.FsiPanel fsiPanel4;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
	}
}