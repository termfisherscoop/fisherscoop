﻿namespace jp.co.fsi.hn.hndr1011
{
    partial class HNDR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblSeikyushoDateDay = new System.Windows.Forms.Label();
			this.lblSeikyushoDateMonth = new System.Windows.Forms.Label();
			this.lblSeikyushoDateYear = new System.Windows.Forms.Label();
			this.txtSeikyushoDateDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeikyushoDateYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeikyushoDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeikyushoDateGengo = new System.Windows.Forms.Label();
			this.lblNakagaininCdTo = new System.Windows.Forms.Label();
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.txtNakagaininCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblNakagaininCdFr = new System.Windows.Forms.Label();
			this.txtNakagaininCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(9, 1004);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1805, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1366, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "鮮魚代金明細書兼請求書出力";
			// 
			// lblSeikyushoDateDay
			// 
			this.lblSeikyushoDateDay.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyushoDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyushoDateDay.ForeColor = System.Drawing.Color.Black;
			this.lblSeikyushoDateDay.Location = new System.Drawing.Point(405, 3);
			this.lblSeikyushoDateDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoDateDay.Name = "lblSeikyushoDateDay";
			this.lblSeikyushoDateDay.Size = new System.Drawing.Size(27, 24);
			this.lblSeikyushoDateDay.TabIndex = 7;
			this.lblSeikyushoDateDay.Tag = "CHANGE";
			this.lblSeikyushoDateDay.Text = "日";
			this.lblSeikyushoDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyushoDateMonth
			// 
			this.lblSeikyushoDateMonth.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyushoDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyushoDateMonth.ForeColor = System.Drawing.Color.Black;
			this.lblSeikyushoDateMonth.Location = new System.Drawing.Point(335, 2);
			this.lblSeikyushoDateMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoDateMonth.Name = "lblSeikyushoDateMonth";
			this.lblSeikyushoDateMonth.Size = new System.Drawing.Size(20, 25);
			this.lblSeikyushoDateMonth.TabIndex = 5;
			this.lblSeikyushoDateMonth.Tag = "CHANGE";
			this.lblSeikyushoDateMonth.Text = "月";
			this.lblSeikyushoDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyushoDateYear
			// 
			this.lblSeikyushoDateYear.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyushoDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyushoDateYear.ForeColor = System.Drawing.Color.Black;
			this.lblSeikyushoDateYear.Location = new System.Drawing.Point(261, 0);
			this.lblSeikyushoDateYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoDateYear.Name = "lblSeikyushoDateYear";
			this.lblSeikyushoDateYear.Size = new System.Drawing.Size(23, 28);
			this.lblSeikyushoDateYear.TabIndex = 3;
			this.lblSeikyushoDateYear.Tag = "CHANGE";
			this.lblSeikyushoDateYear.Text = "年";
			this.lblSeikyushoDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeikyushoDateDay
			// 
			this.txtSeikyushoDateDay.AutoSizeFromLength = false;
			this.txtSeikyushoDateDay.DisplayLength = null;
			this.txtSeikyushoDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeikyushoDateDay.ForeColor = System.Drawing.Color.Black;
			this.txtSeikyushoDateDay.Location = new System.Drawing.Point(364, 2);
			this.txtSeikyushoDateDay.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeikyushoDateDay.MaxLength = 2;
			this.txtSeikyushoDateDay.Name = "txtSeikyushoDateDay";
			this.txtSeikyushoDateDay.Size = new System.Drawing.Size(39, 23);
			this.txtSeikyushoDateDay.TabIndex = 4;
			this.txtSeikyushoDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeikyushoDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoDateDay_Validating);
			// 
			// txtSeikyushoDateYear
			// 
			this.txtSeikyushoDateYear.AutoSizeFromLength = false;
			this.txtSeikyushoDateYear.DisplayLength = null;
			this.txtSeikyushoDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeikyushoDateYear.ForeColor = System.Drawing.Color.Black;
			this.txtSeikyushoDateYear.Location = new System.Drawing.Point(219, 2);
			this.txtSeikyushoDateYear.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeikyushoDateYear.MaxLength = 2;
			this.txtSeikyushoDateYear.Name = "txtSeikyushoDateYear";
			this.txtSeikyushoDateYear.Size = new System.Drawing.Size(39, 23);
			this.txtSeikyushoDateYear.TabIndex = 2;
			this.txtSeikyushoDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeikyushoDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoDateYear_Validating);
			// 
			// txtSeikyushoDateMonth
			// 
			this.txtSeikyushoDateMonth.AutoSizeFromLength = false;
			this.txtSeikyushoDateMonth.DisplayLength = null;
			this.txtSeikyushoDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeikyushoDateMonth.ForeColor = System.Drawing.Color.Black;
			this.txtSeikyushoDateMonth.Location = new System.Drawing.Point(292, 2);
			this.txtSeikyushoDateMonth.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeikyushoDateMonth.MaxLength = 2;
			this.txtSeikyushoDateMonth.Name = "txtSeikyushoDateMonth";
			this.txtSeikyushoDateMonth.Size = new System.Drawing.Size(39, 23);
			this.txtSeikyushoDateMonth.TabIndex = 3;
			this.txtSeikyushoDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeikyushoDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoDateMonth_Validating);
			// 
			// lblSeikyushoDateGengo
			// 
			this.lblSeikyushoDateGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeikyushoDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeikyushoDateGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyushoDateGengo.ForeColor = System.Drawing.Color.Black;
			this.lblSeikyushoDateGengo.Location = new System.Drawing.Point(159, 2);
			this.lblSeikyushoDateGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoDateGengo.Name = "lblSeikyushoDateGengo";
			this.lblSeikyushoDateGengo.Size = new System.Drawing.Size(55, 24);
			this.lblSeikyushoDateGengo.TabIndex = 1;
			this.lblSeikyushoDateGengo.Tag = "DISPNAME";
			this.lblSeikyushoDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblNakagaininCdTo
			// 
			this.lblNakagaininCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininCdTo.ForeColor = System.Drawing.Color.Black;
			this.lblNakagaininCdTo.Location = new System.Drawing.Point(593, 3);
			this.lblNakagaininCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
			this.lblNakagaininCdTo.Size = new System.Drawing.Size(269, 24);
			this.lblNakagaininCdTo.TabIndex = 4;
			this.lblNakagaininCdTo.Tag = "DISPNAME";
			this.lblNakagaininCdTo.Text = "最　後";
			this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBet.ForeColor = System.Drawing.Color.Black;
			this.lblCodeBet.Location = new System.Drawing.Point(497, 4);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 27);
			this.lblCodeBet.TabIndex = 2;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNakagaininCdFr
			// 
			this.txtNakagaininCdFr.AutoSizeFromLength = false;
			this.txtNakagaininCdFr.DisplayLength = null;
			this.txtNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNakagaininCdFr.ForeColor = System.Drawing.Color.Black;
			this.txtNakagaininCdFr.Location = new System.Drawing.Point(160, 3);
			this.txtNakagaininCdFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtNakagaininCdFr.MaxLength = 4;
			this.txtNakagaininCdFr.Name = "txtNakagaininCdFr";
			this.txtNakagaininCdFr.Size = new System.Drawing.Size(52, 23);
			this.txtNakagaininCdFr.TabIndex = 5;
			this.txtNakagaininCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNakagaininCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
			// 
			// lblNakagaininCdFr
			// 
			this.lblNakagaininCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininCdFr.ForeColor = System.Drawing.Color.Black;
			this.lblNakagaininCdFr.Location = new System.Drawing.Point(217, 3);
			this.lblNakagaininCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
			this.lblNakagaininCdFr.Size = new System.Drawing.Size(269, 24);
			this.lblNakagaininCdFr.TabIndex = 1;
			this.lblNakagaininCdFr.Tag = "DISPNAME";
			this.lblNakagaininCdFr.Text = "先　頭";
			this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNakagaininCdTo
			// 
			this.txtNakagaininCdTo.AutoSizeFromLength = false;
			this.txtNakagaininCdTo.DisplayLength = null;
			this.txtNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNakagaininCdTo.ForeColor = System.Drawing.Color.Black;
			this.txtNakagaininCdTo.Location = new System.Drawing.Point(536, 5);
			this.txtNakagaininCdTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtNakagaininCdTo.MaxLength = 4;
			this.txtNakagaininCdTo.Name = "txtNakagaininCdTo";
			this.txtNakagaininCdTo.Size = new System.Drawing.Size(52, 23);
			this.txtNakagaininCdTo.TabIndex = 6;
			this.txtNakagaininCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNakagaininCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNakagaininCdTo_KeyDown);
			this.txtNakagaininCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(159, 3);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(211, 2);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(900, 123);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblNakagaininCdTo);
			this.fsiPanel3.Controls.Add(this.lblCodeBet);
			this.fsiPanel3.Controls.Add(this.txtNakagaininCdFr);
			this.fsiPanel3.Controls.Add(this.txtNakagaininCdTo);
			this.fsiPanel3.Controls.Add(this.lblNakagaininCdFr);
			this.fsiPanel3.Controls.Add(this.label3);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 85);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(890, 33);
			this.fsiPanel3.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(890, 33);
			this.label3.TabIndex = 1;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "仲買人CD範囲";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblSeikyushoDateDay);
			this.fsiPanel2.Controls.Add(this.lblSeikyushoDateMonth);
			this.fsiPanel2.Controls.Add(this.lblSeikyushoDateGengo);
			this.fsiPanel2.Controls.Add(this.lblSeikyushoDateYear);
			this.fsiPanel2.Controls.Add(this.txtSeikyushoDateMonth);
			this.fsiPanel2.Controls.Add(this.txtSeikyushoDateDay);
			this.fsiPanel2.Controls.Add(this.txtSeikyushoDateYear);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 45);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(890, 31);
			this.fsiPanel2.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(890, 31);
			this.label2.TabIndex = 1;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "請求書発効日";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(890, 31);
			this.fsiPanel1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(890, 31);
			this.label1.TabIndex = 1;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "水揚支所";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// HNDR1011
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1366, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNDR1011";
			this.Text = "";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoDateYear;
        private System.Windows.Forms.Label lblSeikyushoDateGengo;
        private System.Windows.Forms.Label lblSeikyushoDateDay;
        private System.Windows.Forms.Label lblSeikyushoDateMonth;
        private System.Windows.Forms.Label lblSeikyushoDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoDateDay;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoDateMonth;
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtNakagaininCdFr;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.FsiTextBox txtNakagaininCdTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label3;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label1;
    }
}