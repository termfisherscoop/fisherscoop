﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hndr1011
{

    /// <summary>
    /// 鮮魚代金明細兼請求書(HANR2011)
    /// </summary>
    public partial class HNDR1011 : BasePgForm
    {

        #region 構造体
        /// <summary>
        /// 印刷ワークテーブルのデータ構造体
        /// </summary>
        struct NumVals
        {
            public decimal SURYO;
            public decimal KINGAKU;
            public decimal SHOHIZEI;

            public void Clear()
            {
                SURYO = 0;
                KINGAKU = 0;
                SHOHIZEI = 0;
            }
        }
        /// <summary>
        /// 会社・支所情報構造体
        /// </summary>
        struct KaishaVals
        {
            public string KaishaNm;
            public string KaishaAddr;
            public string KaishaTel;
            public string KaishaFax;
            public string ShishoNm;
        }
        // 会社・支所情報変数（出力時に取得）
        private KaishaVals Kjoho;

        //消費税コレクション
        private Dictionary<decimal, decimal> ZeiList;
        private Dictionary<decimal, decimal> ZeiBetsuList;

        // 軽減税率対応
        private Dictionary<decimal, decimal> ZeiList8;
        private Dictionary<decimal, decimal> ZeiBetsuList8;

        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        //定数
        public const int prtCols = 31;

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNDR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1"; 
#else
            this.txtMizuageShishoCd.Text = UInfo.ShishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            string[] shiharaiDate = Util.ConvJpDate(DateTime.Now.AddDays(5), this.Dba);
            // 日付(請求書発効日)
            lblSeikyushoDateGengo.Text = jpDate[0];
            txtSeikyushoDateYear.Text = jpDate[2];
            txtSeikyushoDateMonth.Text = jpDate[3];
            txtSeikyushoDateDay.Text = jpDate[4];

            // フォーカス設定
            this.txtSeikyushoDateYear.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtSeikyushoDateYear":
                case "txtNakagaininCdFr":
                case "txtNakagaininCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtSeikyushoDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeikyushoDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSeikyushoDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblSeikyushoDateGengo.Text, this.txtSeikyushoDateYear.Text,
                                    this.txtSeikyushoDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtSeikyushoDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtSeikyushoDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblSeikyushoDateGengo.Text,
                                        this.txtSeikyushoDateYear.Text,
                                        this.txtSeikyushoDateMonth.Text,
                                        this.txtSeikyushoDateDay.Text,
                                        this.Dba);
                                this.lblSeikyushoDateGengo.Text = arrJpDate[0];
                                this.txtSeikyushoDateYear.Text = arrJpDate[2];
                                this.txtSeikyushoDateMonth.Text = arrJpDate[3];
                                this.txtSeikyushoDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdFr.Text = outData[0];
                                this.lblNakagaininCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdTo":
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1011.exe");
                    // フォーム作成
                    // アセンブリのロード
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdTo.Text = outData[0];
                                this.lblNakagaininCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANR2011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(請求書発行)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeikyushoDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeikyushoDateYear())
            {
                e.Cancel = true;
                this.txtSeikyushoDateYear.SelectAll();
            }
            else
            {
                CheckSeikyushoDate();
                SetSeikyushoDate();
            }
        }

        /// <summary>
        /// 月(請求書発行)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeikyushoDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeikyushoDateMonth())
            {
                e.Cancel = true;
                this.txtSeikyushoDateMonth.SelectAll();
            }
            else
            {
                CheckSeikyushoDate();
                SetSeikyushoDate();
            }
        }

        /// <summary>
        /// 日(請求書発行)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeikyushoDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeikyushoDateDay())
            {
                e.Cancel = true;
                this.txtSeikyushoDateDay.SelectAll();
            }
            else
            {
                CheckSeikyushoDate();
                SetSeikyushoDate();
            }
        }

        /// <summary>
        /// 仲買人コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtNakagaininCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtNakagaininCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 仲買人コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtNakagaininCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        #region チェックメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年(請求書発行)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeikyushoDateYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtSeikyushoDateYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtSeikyushoDateYear.Text))
            {
                this.txtSeikyushoDateYear.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(請求書発行)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeikyushoDateMonth()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtSeikyushoDateMonth.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtSeikyushoDateMonth.Text))
            {
                this.txtSeikyushoDateMonth.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtSeikyushoDateMonth.Text) > 12)
                {
                    this.txtSeikyushoDateMonth.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtSeikyushoDateMonth.Text) < 1)
                {
                    this.txtSeikyushoDateMonth.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(請求書発行)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeikyushoDateDay()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtSeikyushoDateDay.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtSeikyushoDateDay.Text))
            {
                // 空の場合、1日として処理
                this.txtSeikyushoDateDay.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtSeikyushoDateDay.Text) < 1)
                {
                    this.txtSeikyushoDateDay.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(請求書発行)の月末入力チェック
        /// </summary>
        /// 
        private void CheckSeikyushoDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeikyushoDateGengo.Text, this.txtSeikyushoDateYear.Text,
                this.txtSeikyushoDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeikyushoDateDay.Text) > lastDayInMonth)
            {
                this.txtSeikyushoDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(請求書発行)の正しい和暦への変換処理
        /// 支払期日の設定
        /// </summary>
        /// 
        private void SetSeikyushoDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            string[] arrJpDate = Util.FixJpDate(this.lblSeikyushoDateGengo.Text, this.txtSeikyushoDateYear.Text,
                   this.txtSeikyushoDateMonth.Text, this.txtSeikyushoDateDay.Text, this.Dba);
            SetSeikyushoDate(arrJpDate);
        }

        /// <summary>
        /// 仲買人コード(自)の入力チェック
        /// </summary>
        /// 
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdFr.Text))
            {
                this.lblNakagaininCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtNakagaininCdFr.Text))
            {
                Msg.Error("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblNakagaininCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtMizuageShishoCd.Text, this.txtNakagaininCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 仲買人コード(至)の入力チェック
        /// </summary>
        /// 
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdTo.Text))
            {
                this.lblNakagaininCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtNakagaininCdTo.Text))
            {
                Msg.Error("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblNakagaininCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtMizuageShishoCd.Text, this.txtNakagaininCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(請求書発行)のチェック
            if (!IsValidSeikyushoDateYear())
            {
                this.txtSeikyushoDateYear.Focus();
                this.txtSeikyushoDateYear.SelectAll();
                return false;
            }
            // 月(請求書発行)のチェック
            if (!IsValidSeikyushoDateMonth())
            {
                this.txtSeikyushoDateMonth.Focus();
                this.txtSeikyushoDateMonth.SelectAll();
                return false;
            }
            // 日(請求書発行)のチェック
            if (!IsValidSeikyushoDateDay())
            {
                this.txtSeikyushoDateDay.Focus();
                this.txtSeikyushoDateDay.SelectAll();
                return false;
            }
            // 年月日(請求書発行)の月末入力チェック処理
            CheckSeikyushoDate();
            // 年月日(請求書発行)の正しい和暦への変換処理
            SetSeikyushoDate();

            // 仲買人コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtNakagaininCdFr.Focus();
                this.txtNakagaininCdFr.SelectAll();
                return false;
            }
            // 仲買人コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtNakagaininCdTo.Focus();
                this.txtNakagaininCdTo.SelectAll();
                return false;
            }

            return true;
        }
        #endregion
        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetSeikyushoDate(string[] arrJpDate)
        {
            this.lblSeikyushoDateGengo.Text = arrJpDate[0];
            this.txtSeikyushoDateYear.Text = arrJpDate[2];
            this.txtSeikyushoDateMonth.Text = arrJpDate[3];
            this.txtSeikyushoDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_CM_SHISHO"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false)
        {
            try
            {
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
                
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNDR1011R rpt = new HNDR1011R(dtOutput);
                    HNDR1012R rpt2 = new HNDR1012R(dtOutput);
                    rpt.Document.Name = this.lblTitle.Text;
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;

                    rpt2.Document.Name = this.lblTitle.Text;
                    rpt2.Document.Printer.DocumentName = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run(false);
                        rpt2.Run(false);

                        int idxPage = 1;
                        foreach (var doc in rpt2.Document.Pages)
                        {
                            rpt.Document.Pages.Insert(idxPage, doc);

                            idxPage += 2;

                        }


                        //foreach (var pg in rpt2.Document.Pages)
                        //{

                        //    rpt.Document.Pages.Add(pg);

                        //}

                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        //rpt.Run();
                        rpt.Run(false);
                        rpt2.Run(false);

                        int idxPage = 1;
                        foreach (var doc in rpt2.Document.Pages)
                        {
                            rpt.Document.Pages.Insert(idxPage, doc);

                            idxPage += 2;

                        }

                        //foreach (var pg in rpt2.Document.Pages)
                        //{

                        //    rpt.Document.Pages.Add(pg);

                        //}



                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示


                        PreviewForm pFrm = new PreviewForm(rpt, rpt2,this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region ワークテーブル作成準備
            // 日付(請求書発効日)を西暦にして取得
            DateTime tmpSKSDate = Util.ConvAdDate(this.lblSeikyushoDateGengo.Text, this.txtSeikyushoDateYear.Text,
                    this.txtSeikyushoDateMonth.Text, this.txtSeikyushoDateDay.Text, this.Dba);
            // 日付(請求書支払日)を取得
            DateTime tmpSKSDateTo = tmpSKSDate.AddDays(7);
            // 日付を和暦で保持
            string[] tmpSKSWareki = Util.ConvJpDate(tmpSKSDate, this.Dba);
            string[] tmpSKSWarekiTo = Util.ConvJpDate(tmpSKSDateTo, this.Dba);
            // 日付の0詰め設定
            if (tmpSKSWareki[3].Length == 1)
            {
                tmpSKSWareki[3] = "0" + tmpSKSWareki[3];
            }
            if (tmpSKSWareki[4].Length == 1)
            {
                tmpSKSWareki[4] = "0" + tmpSKSWareki[4];
            }
            if (tmpSKSWarekiTo[3].Length == 1)
            {
                tmpSKSWarekiTo[3] = "0" + tmpSKSWarekiTo[3];
            }
            if (tmpSKSWarekiTo[4].Length == 1)
            {
                tmpSKSWarekiTo[4] = "0" + tmpSKSWarekiTo[4];
            }
            // 仲買人コード設定
            string NAKAGAININ_CD_FR;
            string NAKAGAININ_CD_TO;
            if (Util.ToDecimal(txtNakagaininCdFr.Text) > 0)
            {
                NAKAGAININ_CD_FR = txtNakagaininCdFr.Text;
            }
            else
            {
                NAKAGAININ_CD_FR = "0";
            }
            if (Util.ToDecimal(txtNakagaininCdTo.Text) > 0)
            {
                NAKAGAININ_CD_TO = txtNakagaininCdTo.Text;
            }
            else
            {
                NAKAGAININ_CD_TO = "9999";
            }
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            #endregion

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            #region 会社情報取得
            getKaishaJoho(shishoCd);
            #endregion

            #region　メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            // han.VI_鮮魚代金明細兼請求書(VI_HN_SNGDIKN_MISI_KEN_SIKS)の日付から発生しているデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" A.KAISHA_CD,");
            Sql.Append(" A.SENSHU_NM,");
            Sql.Append(" A.YAMA_NO,");
            Sql.Append(" A.GYOSHU_NM,");
            Sql.Append(" A.SURYO,");
            Sql.Append(" A.TANKA,");
            Sql.Append(" A.KINGAKU,");
            Sql.Append(" A.ZEINUKI_KINGAKU,");
            Sql.Append(" A.MIZUAGE_ZEINUKI_KINGAKU,");
            Sql.Append(" dbo.FNC_MarumeS(A.KINGAKU * (A.ZEI_RITSU/100), 0, A.NAKAGAI_SHOHIZEI_HASU_SHORI) AS SHOHIZEI,");
            Sql.Append(" A.MIZUAGE_SHOHIZEIGAKU,");
            Sql.Append(" A.NAKAGAININ_CD,");
            Sql.Append(" A.NAKAGAININ_NM,");
            Sql.Append(" B.ZANDAKA,");
            Sql.Append(" C.ZEIKOMI_KINGAKU,");
            Sql.Append(" A.ZEI_RITSU,");
// 軽減税率用に追加 2020-03-09
            Sql.Append(" A.ZEI_KUBUN,");
// 軽減税率用に追加 2020-03-09
            Sql.Append(" A.NAKAGAI_SHOHIZEI_HASU_SHORI,");
            Sql.Append(" A.NAKAGAI_SHOHIZEI_NYURYOKU_HOHO ");

            Sql.Append(" FROM");
            Sql.Append(" VI_HN_SNGDIKN_MISI_KEN_SIKS AS A");
            Sql.Append(" LEFT OUTER JOIN");
            Sql.Append(" (SELECT");
            Sql.Append("    A.KAISHA_CD,");
            Sql.Append("    A.TORIHIKISAKI_CD AS KAIIN_BANGO,");
            Sql.Append("    SUM( CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE C.ZEIKOMI_KINGAKU * -1 END) AS ZANDAKA");
            Sql.Append("  FROM");
            Sql.Append("    (SELECT");
            Sql.Append("       A.KAISHA_CD AS KAISHA_CD,");
            Sql.Append("       A.TORIHIKISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append("       A.TORIHIKISAKI_NM AS TORIHIKISAKI_NM,");
            Sql.Append("       A.DENWA_BANGO AS DENWA_BANGO,");
            Sql.Append("       A.FAX_BANGO AS FAX_BANGO,");
            Sql.Append("       B.TORIHIKISAKI_KUBUN2 AS TORIHIKISAKI_KUBUN2");
            Sql.Append("     FROM");
            Sql.Append("       TB_CM_TORIHIKISAKI AS A");
            Sql.Append("     LEFT OUTER JOIN TB_HN_TORIHIKISAKI_JOHO AS B");
            Sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append("     AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD) AS A");
            Sql.Append("  LEFT OUTER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B");
            Sql.Append("  ON A.KAISHA_CD = B.KAISHA_CD AND");
            Sql.Append("  2 = B.MOTOCHO_KUBUN");
            Sql.Append("  LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS C");
            Sql.Append("  ON A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append("  AND A.TORIHIKISAKI_CD = C.HOJO_KAMOKU_CD");
            Sql.Append("  AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            Sql.Append("  AND C.KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append("  LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS D");
            Sql.Append("  ON B.KAISHA_CD = D .KAISHA_CD");
            Sql.Append("  AND B.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD");
            Sql.Append("  AND D.KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append("  WHERE A.TORIHIKISAKI_CD BETWEEN '0' AND '9999'");
            Sql.Append("  AND A.TORIHIKISAKI_KUBUN2 = 2");
            Sql.Append("  AND C.DENPYO_DATE < @DATE_SKS");
            Sql.Append("  GROUP BY");
            Sql.Append("    A.KAISHA_CD,");
            Sql.Append("    A.TORIHIKISAKI_CD) AS B");
            Sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" AND A.NAKAGAININ_CD = B.KAIIN_BANGO");
            Sql.Append(" LEFT OUTER JOIN");
            Sql.Append(" (SELECT");
            Sql.Append("    B.KAISHA_CD AS KAISHA_CD,");
            Sql.Append("    B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append("    SUM(B.ZEIKOMI_KINGAKU) AS ZEIKOMI_KINGAKU,");
            Sql.Append("    B.DENPYO_DATE AS DENPYO_DATE");
            Sql.Append("  FROM TB_HN_NYUKIN_KAMOKU AS A");
            Sql.Append("  LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B");
            Sql.Append("  ON A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append("  AND 1 = B.DENPYO_KUBUN");
            Sql.Append("  AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            Sql.Append("  AND B.KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append("  LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C");
            Sql.Append("  ON A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append("  AND A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            Sql.Append("  AND C.KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append("  WHERE A.MOTOCHO_KUBUN = 1");
            Sql.Append("  AND B.HOJO_KAMOKU_CD BETWEEN '0' AND '9999'");
            Sql.Append("  AND B.TAISHAKU_KUBUN <> C.TAISHAKU_KUBUN");
            Sql.Append("  AND B.SHIWAKE_SAKUSEI_KUBUN IS NULL");
            Sql.Append("  GROUP BY");
            Sql.Append("    B.KAISHA_CD, B.HOJO_KAMOKU_CD, B.DENPYO_DATE) AS C");
            Sql.Append(" ON A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append(" AND A.NAKAGAININ_CD = C.TORIHIKISAKI_CD");
            Sql.Append(" AND A.SERIBI = C.DENPYO_DATE");
            Sql.Append(" WHERE A.KAISHA_CD = @KAISHA_CD");
            Sql.Append(" AND A.SERIBI = @DATE_SKS");
            Sql.Append(" AND NAKAGAININ_CD BETWEEN @NAKAGAININ_CD_FR AND @NAKAGAININ_CD_TO");
            // 支所コードが指定されていなければ条件に含めない
            if (shishoCd != "0")
            {
                Sql.Append(" AND A.SHISHO_CD = @SHISHO_CD");
            }
            Sql.Append(" ORDER BY");
            Sql.Append("   KAISHA_CD ASC,");
            Sql.Append("   SHISHO_CD ASC,");
            Sql.Append("   NAKAGAININ_CD ASC,");
            Sql.Append("   YAMA_NO ASC");

            //条件パラメータ設定
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_SKS", SqlDbType.VarChar, 10, tmpSKSDate.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@NAKAGAININ_CD_FR", SqlDbType.VarChar, 6, NAKAGAININ_CD_FR);
            dpc.SetParam("@NAKAGAININ_CD_TO", SqlDbType.VarChar, 6, NAKAGAININ_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                NumVals GOKEI = new NumVals(); // 伝票グループ

                #region ループ準備処理
                int dbSORT = 1;
                int i = 0; // ループ用カウント変数
                int j = 1; // 改ページ用カウント変数
                int k = 0; // 改ページ判断用カウント変数
                int before = -1; // 改ページ比較用カウント変数
                int next = 1; // 改ページ比較用カウント変数
                int n = 0; // 空行表示カウント変数
                int lastNum = i; // 最終残高表示用変数
                int flag = 0;  // 空行表示判断用変数

                string strBkNakagainin = string.Empty;


                #endregion

                while (dtMainLoop.Rows.Count > i)
                {

                    if (string.IsNullOrEmpty(strBkNakagainin))
                    {
                        //税率別消費税額の計算辞書
                        ZeiList = new Dictionary<decimal, decimal>();
                        ZeiBetsuList = new Dictionary<decimal, decimal>();

                        // 軽減税率対応
                        ZeiList8 = new Dictionary<decimal, decimal>();
                        ZeiBetsuList8 = new Dictionary<decimal, decimal>();

                        strBkNakagainin = dtMainLoop.Rows[i]["NAKAGAININ_CD"].ToString();

                    }
                    else if (strBkNakagainin != dtMainLoop.Rows[i]["NAKAGAININ_CD"].ToString())
                    {
                        //税率別消費税額の計算辞書
                        ZeiList = new Dictionary<decimal, decimal>();
                        ZeiBetsuList = new Dictionary<decimal, decimal>();

                        // 軽減税率対応
                        ZeiList8 = new Dictionary<decimal, decimal>();
                        ZeiBetsuList8 = new Dictionary<decimal, decimal>();

                        strBkNakagainin = dtMainLoop.Rows[i]["NAKAGAININ_CD"].ToString();

                    }

                    //dpc = new DbParamCollection();
                    //Sql = new StringBuilder();
                    #region インサートテーブル
                    Sql = Insertsql(); //new StringBuilder();
                    dpc = new DbParamCollection();
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.PadZero(j, 4)); // 改ページ用カウント
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSKSWareki[2]); // 年
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSKSWareki[3]); // 月
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSKSWareki[4]); // 日
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, tmpSKSWareki[0]); // 年号
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人No
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_NM"]); // 仲買人名称
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YAMA_NO"]); // 山No
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOSHU_NM"]); // 魚種名称
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["TANKA"])); // 単価

                    if ("50" == Util.FormatNum(dtMainLoop.Rows[i]["ZEI_KUBUN"]))
                    {
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, "*" + " " + Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU"])); // 金額
                    }
                    else
                    {
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU"])); // 金額
                    }

                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIZEI"]); // 消費税
                    //dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIZEI_FURIWAKE"]); // 消費税
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_SHOHIZEIGAKU"])); // 消費税額
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, "");
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "");
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "");
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "");
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, "");
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToInt(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU"])); // 税込み金額
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, tmpSKSWarekiTo[2]); // 年
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, tmpSKSWarekiTo[3]); // 月
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpSKSWarekiTo[4]); // 日
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, tmpSKSWarekiTo[0]); // 年号
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, shishoCd);          // 支所コード
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Kjoho.KaishaNm);    // 会社名
                    dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Kjoho.KaishaAddr);  // 住所
                    dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Kjoho.KaishaTel);   // 電話番号
                    dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Kjoho.ShishoNm);    // 支所名
                    dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, "");    // 消費税内訳

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion

                    // 合計金額の足しこみ
                    GOKEI.SURYO += Util.ToDecimal(dtMainLoop.Rows[i]["SURYO"]);
                    GOKEI.KINGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]);
                    //GOKEI.SHOHIZEI += Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI_FURIWAKE"]);
                    GOKEI.SHOHIZEI += Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                    decimal Zeigaku = Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                    //税率毎消費税金額
                    if (Util.ToDecimal(dtMainLoop.Rows[i]["ZEI_RITSU"]) != 0)
                    {
                        decimal ZeiRitsu = Util.ToDecimal(dtMainLoop.Rows[i]["ZEI_RITSU"]);


                        if (50 == Util.ToDecimal(dtMainLoop.Rows[i]["ZEI_KUBUN"]))
                        {
                            if (ZeiBetsuList8.ContainsKey(ZeiRitsu) == true)
                            {
                                ZeiBetsuList8[ZeiRitsu] += Util.ToDecimal(dtMainLoop.Rows[i]["ZEINUKI_KINGAKU"]);
                            }
                            else
                            {
                                ZeiBetsuList8.Add(ZeiRitsu, Util.ToDecimal(dtMainLoop.Rows[i]["ZEINUKI_KINGAKU"]));
                            }
                        }
                        else
                        {
                            if (ZeiBetsuList.ContainsKey(ZeiRitsu) == true)
                            {
                                ZeiBetsuList[ZeiRitsu] += Util.ToDecimal(dtMainLoop.Rows[i]["ZEINUKI_KINGAKU"]);
                            }
                            else
                            {
                                ZeiBetsuList.Add(ZeiRitsu, Util.ToDecimal(dtMainLoop.Rows[i]["ZEINUKI_KINGAKU"]));
                            }
                        }

                    }

                    // 現在の仲買人コードと前の仲買人コードが一致しない場合、改ページ判断用カウント変数Kを初期化
                    if (i != 0 && Util.ToInt(dtMainLoop.Rows[i]["NAKAGAININ_CD"]) == Util.ToInt(dtMainLoop.Rows[before]["NAKAGAININ_CD"]))
                    {
                        k++;
                    }
                    else
                    {
                        k = 1;
                    }

                    if (k % 15 == 0)
                    {
                        #region インサートテーブル
                        Sql = Insertsql(); //new StringBuilder();
                        dpc = new DbParamCollection();
                        #endregion

                        #region 小計データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.PadZero(j, 4)); // 改ページ用カウント
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSKSWareki[2]); // 年
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSKSWareki[3]); // 月
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSKSWareki[4]); // 日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, tmpSKSWareki[0]); // 年号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人No
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[lastNum]["NAKAGAININ_NM"]); // 仲買人名称
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "小計");
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SURYO, 1)); // 数量


                        if (0 == GOKEI.SURYO)
                        {
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, 0); // 単価
                        }
                        else
                        {
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU / GOKEI.SURYO)); // 単価
                        }


                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU)); // 金額
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, tmpSKSWarekiTo[2]); // 年
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, tmpSKSWarekiTo[3]); // 月
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpSKSWarekiTo[4]); // 日
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, tmpSKSWarekiTo[0]); // 年号
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, shishoCd); // 支所コード
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Kjoho.KaishaNm);    // 会社名
                        dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Kjoho.KaishaAddr);  // 住所
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Kjoho.KaishaTel);   // 電話番号
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Kjoho.ShishoNm);    // 支所名
                        dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, "");    // 消費税内訳

                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                        for (int id = 13; id < 20; id++)
                        {
                            string wk = "@ITEM" + id.ToString();
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        j++;

                        if (Util.ToInt(dtMainLoop.Rows.Count) == next || Util.ToInt(dtMainLoop.Rows[i]["NAKAGAININ_CD"]) != Util.ToInt(dtMainLoop.Rows[next]["NAKAGAININ_CD"]))
                        {
                            n = -1;
                        }
                    }
                    else if (k % 15 == 14 && (Util.ToInt(dtMainLoop.Rows.Count) == next || Util.ToInt(dtMainLoop.Rows[i]["NAKAGAININ_CD"]) != Util.ToInt(dtMainLoop.Rows[next]["NAKAGAININ_CD"])))
                    {
                        #region インサートテーブル
                        Sql = Insertsql(); //new StringBuilder();
                        dpc = new DbParamCollection();
                        #endregion

                        #region データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.PadZero(j, 4)); // 改ページ用カウント
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSKSWareki[2]); // 年
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSKSWareki[3]); // 月
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSKSWareki[4]); // 日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, tmpSKSWareki[0]); // 年号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人No
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[lastNum]["NAKAGAININ_NM"]); // 仲買人名称
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, ""); // 数量
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, ""); // 単価
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, ""); // 金額
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, ""); // 消費税
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[lastNum]["ZEIKOMI_KINGAKU"])); // 税込み金額
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, tmpSKSWarekiTo[2]); // 年
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, tmpSKSWarekiTo[3]); // 月
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpSKSWarekiTo[4]); // 日
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, tmpSKSWarekiTo[0]); // 年号
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, shishoCd); // 支所コード
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Kjoho.KaishaNm);    // 会社名
                        dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Kjoho.KaishaAddr);  // 住所
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Kjoho.KaishaTel);   // 電話番号
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Kjoho.ShishoNm);    // 支所名
                        dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, "");    // 消費税内訳
                        for (int id = 7; id < 9; id++)
                        {
                            string wk = "@ITEM" + Util.PadZero(id.ToString(), 2);
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }
                        for (int id = 13; id < 17; id++)
                        {
                            string wk = "@ITEM" + id.ToString();
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
#endregion

                        #region インサートテーブル
                        Sql = Insertsql(); // new StringBuilder();
                        dpc = new DbParamCollection();
                        #endregion

                        #region 小計データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.PadZero(j, 4)); // 改ページ用カウント
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSKSWareki[2]); // 年
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSKSWareki[3]); // 月
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSKSWareki[4]); // 日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, tmpSKSWareki[0]); // 年号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人No
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[lastNum]["NAKAGAININ_NM"]); // 仲買人名称
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "小計");
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SURYO, 1)); // 数量


                        if (0 == GOKEI.SURYO)
                        {
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200,0); // 単価
                        }
                        else
                        {
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU / GOKEI.SURYO)); // 単価
                        }

                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU)); // 金額
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, tmpSKSWarekiTo[2]); // 年
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, tmpSKSWarekiTo[3]); // 月
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpSKSWarekiTo[4]); // 日
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, tmpSKSWarekiTo[0]); // 年号
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, shishoCd); // 支所コード
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Kjoho.KaishaNm);    // 会社名
                        dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Kjoho.KaishaAddr);  // 住所
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Kjoho.KaishaTel);   // 電話番号
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Kjoho.ShishoNm);    // 支所名
                        dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, "");    // 消費税内訳

                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                        for (int id = 13; id < 20; id++)
                        {
                            string wk = "@ITEM" + id.ToString();
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        j++;
                        n = -1;
                    }

                    // 空行表示準備
                    if (Util.ToInt(dtMainLoop.Rows.Count) != next && Util.ToInt(dtMainLoop.Rows[i]["NAKAGAININ_CD"]) == Util.ToInt(dtMainLoop.Rows[next]["NAKAGAININ_CD"]))
                    {
                        n++;
                        if (n % 15 == 0)
                        {
                            n = 0;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }

                    lastNum = i;

                    if (flag == 1)
                    {
                        while (n <= 11)
                        {
                            #region インサートテーブル
                            Sql = Insertsql(); //new StringBuilder();
                            dpc = new DbParamCollection();
                            #endregion

                            #region データ登録
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.PadZero(j, 4)); // 改ページ用カウント
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSKSWareki[2]); // 年
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSKSWareki[3]); // 月
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSKSWareki[4]); // 日
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, tmpSKSWareki[0]); // 年号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人No
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[lastNum]["NAKAGAININ_NM"]); // 仲買人名称
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, ""); // 数量
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, ""); // 単価
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, ""); // 金額
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, ""); // 消費税
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[lastNum]["ZEIKOMI_KINGAKU"])); // 税込み金額
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, tmpSKSWarekiTo[2]); // 年
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, tmpSKSWarekiTo[3]); // 月
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpSKSWarekiTo[4]); // 日
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, tmpSKSWarekiTo[0]); // 年号
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, shishoCd); // 支所コード
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Kjoho.KaishaNm);    // 会社名
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Kjoho.KaishaAddr);  // 住所
                            dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Kjoho.KaishaTel);   // 電話番号
                            dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Kjoho.ShishoNm);    // 支所名
                            dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, "");    // 消費税内訳
                            for (int id = 7; id < 9; id++)
                            {
                                string wk = "@ITEM" + Util.PadZero(id.ToString(), 2);
                                dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                            }
                            for (int id = 13; id < 17; id++)
                            {
                                string wk = "@ITEM" + id.ToString();
                                dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                            }

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            #endregion

                            n++;
                        }

                        #region インサートテーブル
                        Sql = Insertsql(); //new StringBuilder();
                        dpc = new DbParamCollection();
                        #endregion

                        //消費税計算通常
                        decimal gzeigaku = 0;
                        decimal gZeinukiKingaku = 0;
                        decimal zeigaku = 0;
                        decimal ZeinukiKingaku = 0;
                        foreach (KeyValuePair<decimal, decimal> dic in ZeiBetsuList)
                        {
                            TaxUtil.ROUND_CATEGORY HASU_SHORI = new TaxUtil.ROUND_CATEGORY();
                            switch (Util.ToDecimal(dtMainLoop.Rows[i]["NAKAGAI_SHOHIZEI_HASU_SHORI"]))
                            {
                                case 1:
                                    HASU_SHORI = TaxUtil.ROUND_CATEGORY.DOWN;
                                    break;
                                case 2:
                                    HASU_SHORI = TaxUtil.ROUND_CATEGORY.ADJUST;
                                    break;
                                case 3:
                                    HASU_SHORI = TaxUtil.ROUND_CATEGORY.UP;
                                    break;
                            }
                            
                            //if (Util.ToDecimal(dtMainLoop.Rows[i]["NAKAGAI_SHOHIZEI_NYURYOKU_HOHO"]) == 3)
                            //{
                            //    zeigaku += TaxUtil.CalcFraction(dic.Value - (dic.Value / (100 + dic.Key) * 100), 0, HASU_SHORI);
                            //    ZeinukiKingaku += dic.Value - zeigaku;
                            //}
                            //else
                            //{
                            ZeinukiKingaku += dic.Value;
                            gZeinukiKingaku += dic.Value;

                            zeigaku += TaxUtil.CalcFraction(dic.Value * (dic.Key / 100), 0, HASU_SHORI);
                            gzeigaku += TaxUtil.CalcFraction(dic.Value * (dic.Key / 100), 0, HASU_SHORI);

                            //}

                            if (ZeiList.ContainsKey(dic.Key) == true)
                            {
                                ZeiList[dic.Key] += zeigaku;
                            }
                            else
                            {
                                ZeiList.Add(dic.Key, zeigaku);
                            }

                        }

                        zeigaku = 0;
                        ZeinukiKingaku = 0;
                        foreach (KeyValuePair<decimal, decimal> dic in ZeiBetsuList8)
                        {
                            TaxUtil.ROUND_CATEGORY HASU_SHORI = new TaxUtil.ROUND_CATEGORY();
                            switch (Util.ToDecimal(dtMainLoop.Rows[i]["NAKAGAI_SHOHIZEI_HASU_SHORI"]))
                            {
                                case 1:
                                    HASU_SHORI = TaxUtil.ROUND_CATEGORY.DOWN;
                                    break;
                                case 2:
                                    HASU_SHORI = TaxUtil.ROUND_CATEGORY.ADJUST;
                                    break;
                                case 3:
                                    HASU_SHORI = TaxUtil.ROUND_CATEGORY.UP;
                                    break;
                            }

                            //if (Util.ToDecimal(dtMainLoop.Rows[i]["NAKAGAI_SHOHIZEI_NYURYOKU_HOHO"]) == 3)
                            //{
                            //    zeigaku += TaxUtil.CalcFraction(dic.Value - (dic.Value / (100 + dic.Key) * 100), 0, HASU_SHORI);
                            //    ZeinukiKingaku += dic.Value - zeigaku;
                            //}
                            //else
                            //{
                            ZeinukiKingaku += dic.Value;
                            gZeinukiKingaku += dic.Value;

                            zeigaku += TaxUtil.CalcFraction(dic.Value * (dic.Key / 100), 0, HASU_SHORI);
                            gzeigaku += TaxUtil.CalcFraction(dic.Value * (dic.Key / 100), 0, HASU_SHORI);

                            //}

                            if (ZeiList8.ContainsKey(dic.Key) == true)
                            {
                                ZeiList8[dic.Key] += zeigaku;
                            }
                            else
                            {
                                ZeiList8.Add(dic.Key, zeigaku);
                            }
                        }


                        #region 税抜合計データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.PadZero(j, 4)); // 改ページ用カウント
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSKSWareki[2]); // 年
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSKSWareki[3]); // 月
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSKSWareki[4]); // 日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, tmpSKSWareki[0]); // 年号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人No
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[lastNum]["NAKAGAININ_NM"]); // 仲買人名称
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "税抜合計");
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "");
                        //dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, GOKEI.KINGAKU); // 税抜合計
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(gZeinukiKingaku)); // 税抜合計
                        
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]))); // 残高
                        //dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]) + Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]) - Util.ToDecimal(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU"]));
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]) + (gZeinukiKingaku + gzeigaku));
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, tmpSKSWarekiTo[2]); // 年
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, tmpSKSWarekiTo[3]); // 月
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpSKSWarekiTo[4]); // 日
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, tmpSKSWarekiTo[0]); // 年号
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, shishoCd); // 支所コード
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Kjoho.KaishaNm);    // 会社名
                        dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Kjoho.KaishaAddr);  // 住所
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Kjoho.KaishaTel);   // 電話番号
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Kjoho.ShishoNm);    // 支所名
                        dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, "");    // 消費税内訳

                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                        for (int id = 13; id < 15; id++)
                        {
                            string wk = "@ITEM" + id.ToString();
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }
                        for (int id = 17; id < 20; id++)
                        {
                            string wk = "@ITEM" + id.ToString();
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 消費税合計データ登録
                        dpc = new DbParamCollection();
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.PadZero(j, 4)); // 改ページ用カウント
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSKSWareki[2]); // 年
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSKSWareki[3]); // 月
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSKSWareki[4]); // 日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, tmpSKSWareki[0]); // 年号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人No
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[lastNum]["NAKAGAININ_NM"]); // 仲買人名称
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "消費税");
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "");
                        //dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHOHIZEI)); // 金額
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(gzeigaku)); // 金額

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]))); // 残高
                        //dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]) + Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]) - Util.ToDecimal(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU"]));
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]) + (gZeinukiKingaku + gzeigaku));
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, tmpSKSWarekiTo[2]); // 年
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, tmpSKSWarekiTo[3]); // 月
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpSKSWarekiTo[4]); // 日
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, tmpSKSWarekiTo[0]); // 年号
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, shishoCd); // 支所コード
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Kjoho.KaishaNm);    // 会社名
                        dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Kjoho.KaishaAddr);  // 住所
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Kjoho.KaishaTel);   // 電話番号
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Kjoho.ShishoNm);    // 支所名
                        dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, "");    // 消費税内訳
                        
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                        for (int id = 13; id < 15; id++)
                        {
                            string wk = "@ITEM" + id.ToString();
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }
                        for (int id = 17; id < 20; id++)
                        {
                            string wk = "@ITEM" + id.ToString();
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 合計データ登録
                        dpc = new DbParamCollection();
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.PadZero(j, 4)); // 改ページ用カウント
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSKSWareki[2]); // 年
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSKSWareki[3]); // 月
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSKSWareki[4]); // 日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, tmpSKSWareki[0]); // 年号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人No
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[lastNum]["NAKAGAININ_NM"]); // 仲買人名称
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "合計");
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SURYO, 1)); // 数量

                        if (0 == GOKEI.SURYO)
                        {
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200,0); // 単価
                        }
                        else
                        {
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU / GOKEI.SURYO)); // 単価
                        }
                        //dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, GOKEI.KINGAKU + Util.ToDecimal(Util.FormatNum(GOKEI.SHOHIZEI))); // 金額
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(gZeinukiKingaku + gzeigaku)); // 金額
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]))); // 残高
                        //dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]) + GOKEI.KINGAKU + Util.ToDecimal(Util.FormatNum(GOKEI.SHOHIZEI)) - Util.ToDecimal(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU"]));
                        //dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, GOKEI.KINGAKU + Util.ToDecimal(Util.FormatNum(GOKEI.SHOHIZEI)));
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]) + (gZeinukiKingaku + gzeigaku));
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(gZeinukiKingaku + gzeigaku)); // 金額
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, tmpSKSWarekiTo[2]); // 年
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, tmpSKSWarekiTo[3]); // 月
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpSKSWarekiTo[4]); // 日
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, tmpSKSWarekiTo[0]); // 年号
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, shishoCd); // 支所コード
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Kjoho.KaishaNm);    // 会社名
                        dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Kjoho.KaishaAddr);  // 住所
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Kjoho.KaishaTel);   // 電話番号
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Kjoho.ShishoNm);    // 支所名

                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                        for (int id = 13; id < 15; id++)
                        {
                            string wk = "@ITEM" + id.ToString();
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }
                        for (int id = 17; id < 20; id++)
                        {
                            string wk = "@ITEM" + id.ToString();
                            dpc.SetParam(wk, SqlDbType.VarChar, 200, "");
                        }
                        string ZeiUtiwake;

                        string Utiwake =string.Empty;

                        string sBuf = string.Empty;

                        int intPer10 = 0;
                        int intVal10 = 0;
                        int intPer8 = 0;
                        int intVal8 = 0;


                        if ((ZeiList8.Count > 0) && (ZeiList.Count > 0))
                        {
                            Utiwake =
                                @"
内訳税抜き  {0}% {1} 円
内訳消費税  {2}% {3} 円
内訳税抜き  *{4}% {5} 円
内訳消費税  *{6}% {7} 円";

                            int intk10 = 0;
                            int intk8 = 0;

                            foreach (KeyValuePair<decimal,decimal> keys in ZeiBetsuList)
                            {
                                intk10 = int.Parse(keys.Key.ToString());
                                intVal10 = int.Parse(keys.Value.ToString());
                            }

                            foreach (KeyValuePair<decimal, decimal> keys in ZeiList)
                            {
                                intPer10 = int.Parse(keys.Value.ToString());
                            }

                            foreach (KeyValuePair<decimal, decimal> keys in ZeiBetsuList8)
                            {
                                intk8 = int.Parse(keys.Key.ToString());
                                intVal8 = int.Parse(keys.Value.ToString());
                            }

                            foreach (KeyValuePair<decimal, decimal> keys in ZeiList8)
                            {
                                intPer8 = int.Parse(keys.Value.ToString());
                            }

                            sBuf = string.Format(Utiwake, intk10,
                                                          string.Format("{0:N0}", intVal10).PadLeft(10,' '),
                                                          intk10,
                                                          string.Format("{0:N0}", intPer10).PadLeft(10, ' '), 
                                                          intk8,
                                                          string.Format("{0:N0}", intVal8).PadLeft(10, ' '),
                                                          intk8,
                                                          string.Format("{0:N0}", intPer8).PadLeft(10, ' '));

                        }
                        else if ((ZeiList8.Count == 0) && (ZeiList.Count > 0))
                        {
                            Utiwake =
                                @"
内訳税抜き  {0}% {1} 円
内訳消費税  {2}% {3} 円";

                            int intk = 0;

                            foreach (KeyValuePair<decimal, decimal> keys in ZeiBetsuList)
                            {
                                intk = int.Parse(keys.Key.ToString());
                                intVal10 = int.Parse(keys.Value.ToString());
                            }

                            foreach (KeyValuePair<decimal, decimal> keys in ZeiList)
                            {
                                intPer10 = int.Parse(keys.Value.ToString());
                            }

                            sBuf = string.Format(Utiwake, intk,string.Format("{0:N0}", intVal10).PadLeft(10, ' '), intk, string.Format("{0:N0}", intPer10).PadLeft(10, ' '));

                        }
                        else if ((ZeiList8.Count > 0) && (ZeiList.Count == 0))
                        {
                            Utiwake =
                                @"
内訳税抜き *{0}% {1} 円
内訳消費税 *{2}% {3} 円";

                            int intk = 0;

                            foreach (KeyValuePair<decimal, decimal> keys in ZeiBetsuList8)
                            {
                                intk = int.Parse(keys.Key.ToString());

                                intVal8 = int.Parse(keys.Value.ToString());
                            }

                            foreach (KeyValuePair<decimal, decimal> keys in ZeiList8)
                            {
                                intPer8 = int.Parse(keys.Value.ToString());
                            }

                            sBuf = string.Format(Utiwake, intk, string.Format("{0:N0}", intVal8).PadLeft(10, ' '), intk, string.Format("{0:N0}", intPer8).PadLeft(10, ' '));

                        }

                        //string shoUtiwake = string.Empty;

                        //foreach (KeyValuePair<decimal, decimal> dic in ZeiList)
                        //{
                        //    shoUtiwake += "消費税率(" + string.Format("{0, 2}", dic.Key) + "%) " +
                        //        Util.FormatNum(dic.Value) + "円\r\n";
                        //}

                        ZeiUtiwake = Utiwake;
                        dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, sBuf);  // 消費税内訳

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                    }

                    // 現在の仲買人コードと次の仲買人コードが一致しない場合、nを初期化
                    if (Util.ToInt(dtMainLoop.Rows.Count) != next && Util.ToInt(dtMainLoop.Rows[i]["NAKAGAININ_CD"]) != Util.ToInt(dtMainLoop.Rows[next]["NAKAGAININ_CD"]))
                    {
                        n = 0;
                        GOKEI.Clear();
                        ZeiList.Clear();
                        ZeiBetsuList.Clear();
                    }
                    i++;
                    before++;
                    next++;
                    flag = 0;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;

        }

        /// <summary>
        /// インサートSQL生成
        /// </summary>
        /// <returns>インサート要SQL</returns>
        private StringBuilder Insertsql()
        {
            StringBuilder _sql = new StringBuilder();
            _sql.Append("INSERT INTO PR_HN_TBL(");
            _sql.Append("  GUID");
            _sql.Append(" ,SORT");
            _sql.Append(" ,ITEM01");
            _sql.Append(" ,ITEM02");
            _sql.Append(" ,ITEM03");
            _sql.Append(" ,ITEM04");
            _sql.Append(" ,ITEM05");
            _sql.Append(" ,ITEM06");
            _sql.Append(" ,ITEM07");
            _sql.Append(" ,ITEM08");
            _sql.Append(" ,ITEM09");
            _sql.Append(" ,ITEM10");
            _sql.Append(" ,ITEM11");
            _sql.Append(" ,ITEM12");
            _sql.Append(" ,ITEM13");
            _sql.Append(" ,ITEM14");
            _sql.Append(" ,ITEM16");
            _sql.Append(" ,ITEM15");
            _sql.Append(" ,ITEM17");
            _sql.Append(" ,ITEM18");
            _sql.Append(" ,ITEM19");
            _sql.Append(" ,ITEM20");
            _sql.Append(" ,ITEM21");
            _sql.Append(" ,ITEM22");
            _sql.Append(" ,ITEM23");
            _sql.Append(" ,ITEM24");
            _sql.Append(" ,ITEM25");
            _sql.Append(" ,ITEM26");
            _sql.Append(" ,ITEM27");
            _sql.Append(" ,ITEM28");
            _sql.Append(" ,ITEM29");
            _sql.Append(" ,ITEM30");
            _sql.Append(" ,ITEM31");
            _sql.Append(") ");
            _sql.Append("VALUES(");
            _sql.Append("  @GUID");
            _sql.Append(" ,@SORT");
            _sql.Append(" ,@ITEM01");
            _sql.Append(" ,@ITEM02");
            _sql.Append(" ,@ITEM03");
            _sql.Append(" ,@ITEM04");
            _sql.Append(" ,@ITEM05");
            _sql.Append(" ,@ITEM06");
            _sql.Append(" ,@ITEM07");
            _sql.Append(" ,@ITEM08");
            _sql.Append(" ,@ITEM09");
            _sql.Append(" ,@ITEM10");
            _sql.Append(" ,@ITEM11");
            _sql.Append(" ,@ITEM12");
            _sql.Append(" ,@ITEM13");
            _sql.Append(" ,@ITEM14");
            _sql.Append(" ,@ITEM16");
            _sql.Append(" ,@ITEM15");
            _sql.Append(" ,@ITEM17");
            _sql.Append(" ,@ITEM18");
            _sql.Append(" ,@ITEM19");
            _sql.Append(" ,@ITEM20");
            _sql.Append(" ,@ITEM21");
            _sql.Append(" ,@ITEM22");
            _sql.Append(" ,@ITEM23");
            _sql.Append(" ,@ITEM24");
            _sql.Append(" ,@ITEM25");
            _sql.Append(" ,@ITEM26");
            _sql.Append(" ,@ITEM27");
            _sql.Append(" ,@ITEM28");
            _sql.Append(" ,@ITEM29");
            _sql.Append(" ,@ITEM30");
            _sql.Append(" ,@ITEM31");
            _sql.Append(") ");

            return _sql;
        }
        /// <summary>
        /// 会社・支所情報の取得
        /// </summary>
        /// <param name="shishoCd"></param>
        private void getKaishaJoho(string shishoCd)
        {
            // 支所コードが０：全ての場合は強制的に１にする
            shishoCd = (shishoCd == "0") ? "1" : shishoCd;

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT * FROM VI_CM_SHISHO_KAISHA_JOHO ");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            sql.Append(" SHISHO_CD = @SHISHO_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            if (dt.Rows.Count != 0)
            {
                Kjoho.KaishaNm = Util.ToString(dt.Rows[0]["KAISHA_NM"]);
                Kjoho.ShishoNm = Util.ToString(dt.Rows[0]["SHISHO_NM"]);
                if (shishoCd == "1")
                {
                    Kjoho.KaishaAddr = HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_JUSHO1"])) + " " +
                        HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_JUSHO2"])) + " " +
                        HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_JUSHO3"]));
                    Kjoho.KaishaTel = HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_DENWA_BANGO"]));
                    Kjoho.KaishaFax = HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_FAX_BANGO"]));
                }
                else
                {
                    Kjoho.KaishaAddr = (Util.ToString(dt.Rows[0]["SHISHO_JUSHO1"])) + " " +
                        HanToZenNum(Util.ToString(dt.Rows[0]["SHISHO_JUSHO2"])) + " " +
                        HanToZenNum(Util.ToString(dt.Rows[0]["SHISHO_JUSHO3"]));
                    Kjoho.KaishaTel = HanToZenNum(Util.ToString(dt.Rows[0]["SHISHO_DENWA_BANGO"]));
                    Kjoho.KaishaFax = HanToZenNum(Util.ToString(dt.Rows[0]["SHISHO_FAX_BANGO"]));
                }
            }
            else
            {
                Kjoho.KaishaNm = UInfo.KaishaNm;
                Kjoho.ShishoNm = UInfo.ShishoNm;
            }
            
        }
        /// <summary>
        /// 半角数字を全角数字に変換する。
        /// </summary>
        /// <param name="s">変換する文字列</param>
        /// <returns>変換後の文字列</returns>
        static public string HanToZenNum(string s)
        {
            string ret = s;
            ret = ret.Replace("0", "０");
            ret = ret.Replace("1", "１");
            ret = ret.Replace("2", "２");
            ret = ret.Replace("3", "３");
            ret = ret.Replace("4", "４");
            ret = ret.Replace("5", "５");
            ret = ret.Replace("6", "６");
            ret = ret.Replace("7", "７");
            ret = ret.Replace("8", "８");
            ret = ret.Replace("9", "９");
            ret = ret.Replace("-", "―");
            return ret;
        }
        /// <summary>
        /// 印刷用テーブル取得SQL
        /// </summary>
        /// <param name="itemCnt"></param>
        /// <returns>colsArray</returns>
        private StringBuilder colsArray(int itemCnt)
        {

            StringBuilder cols = new StringBuilder();
            string itemNm = "ITEM";
            string wk;
            for (int i = 1; i < itemCnt + 1; i++)
            {
                if (i == 1)
                {
                    wk = "  " + itemNm + Util.PadZero(i, 2);
                }
                else
                {
                    wk = " ," + itemNm + Util.PadZero(i, 2);
                }
                cols.Append(wk);
            }
            return cols;
        }
        #endregion
    }
}
