﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hndr1101
{
    /// <summary>
    /// 仲買人別売上明細書(HNDR1101)
    /// </summary>
    public partial class HNDR1101 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 精算区分(地区内ｾﾘ)
        /// </summary>
        private const string CHIKUNAI = "3";

        /// <summary>
        /// 精算区分(浜売り)
        /// </summary>
        private const string HAMA_URI = "2";

        /// <summary>
        /// 精算区分(地区外ｾﾘ)
        /// </summary>
        private const string CHIKUGAI = "1";

        /// <summary>
        /// 印刷ワークテーブル使用列数
        /// </summary>
        public const int prtCols = 16;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNDR1101()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            #if DEBUG
                this.txtMizuageShishoCd.Text = "1"; //Uinfo.shishoCd;
            #else
                this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
            #endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲前
            lblGengoFr.Text = jpDate[0];
            txtYearFr.Text = jpDate[2];
            txtMonthFr.Text = jpDate[3];
            txtDayFr.Text = jpDate[4];
            // 日付範囲後
            lblGengoTo.Text = jpDate[0];
            txtYearTo.Text = jpDate[2];
            txtMonthTo.Text = jpDate[3];
            txtDayTo.Text = jpDate[4];
            //// 精算区分地区内ｾﾘにチェック
            //rdoChikunai.Checked = true;

            // フォーカス設定
            this.txtYearFr.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYearFr":
                case "txtYearTo":
                case "txtNakagaininCdFr":
                case "txtNakagaininCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                #endregion

                #region 元号
                case "txtYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpFr();
                            }
                        }
                    }
                    break;
                #endregion

                #region 元号
                case "txtYearTo":
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // アセンブリのロード
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblGengoTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpTo();
                            }
                        }
                    }
                    break;
                #endregion

                #region 仲買人
                case "txtNakagaininCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdFr.Text = outData[0];
                                this.lblNakagaininCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion

                #region 仲買人
                case "txtNakagaininCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdTo.Text = outData[0];
                                this.lblNakagaininCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false ,true);
            }
        }
        //public override void PressF8()
        //{
        //    // 全項目を再度入力値チェック
        //    if (!ValidateAll())
        //    {
        //        // エラーありの場合ここで処理終了
        //        return;
        //    }

        //    if (Msg.ConfNmYesNo("テキスト出力", "実行しますか？") == DialogResult.Yes)
        //    {
        //        // 印刷処理
        //        DoPrint(false, false, false, true);
        //    }
        //}

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNDR11011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtYearFr.SelectAll();
            }
            else
            {
                this.txtYearFr.Text = Util.ToString(IsValid.SetYear(this.txtYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthFr.SelectAll();
            }
            else
            {
                this.txtMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 日(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayFr.SelectAll();
            }
            else
            {
                this.txtDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDayFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtYearTo.SelectAll();
            }
            else
            {
                this.txtYearTo.Text = Util.ToString(IsValid.SetYear(this.txtYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthTo.SelectAll();
            }
            else
            {
                this.txtMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 日(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                this.txtDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDayTo.Text));
                CheckJpTo();
                SetJpTo();

                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 日(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter && this.Flg)
            //{
            //    // Enter処理を無効化
            //    this._dtFlg = false;

            //    // 全項目を再度入力値チェック
            //    if (!ValidateAll())
            //    {
            //        // エラーありの場合ここで処理終了
            //        return;
            //    }

            //    if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            //    {
            //        // ﾌﾟﾚﾋﾞｭｰ処理
            //        DoPrint(true);
            //    }
            //    else
            //    {
            //        this.txtDayTo.Focus();
            //    }
            //}
        }

        /// <summary>
        /// 仲買人コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdFr())
            {
                e.Cancel = true;
                this.txtNakagaininCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdTo())
            {
                e.Cancel = true;
                this.txtNakagaininCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 仲買人コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtNakagaininCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayFr.Text) > lastDayInMonth)
            {
                this.txtDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayTo.Text) > lastDayInMonth)
            {
                this.txtDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                this.txtYearFr.Focus();
                this.txtYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                this.txtMonthFr.Focus();
                this.txtMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                this.txtDayFr.Focus();
                this.txtDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                this.txtYearTo.Focus();
                this.txtYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                this.txtMonthTo.Focus();
                this.txtMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                this.txtDayTo.Focus();
                this.txtDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            // 仲買人コード(自)の入力チェック
            if (!IsValidNakagaininCdFr())
            {
                this.txtNakagaininCdFr.Focus();
                this.txtNakagaininCdFr.SelectAll();
                return false;
            }
            // 仲買人コード(至)の入力チェック
            if (!IsValidNakagaininCdTo())
            {
                this.txtNakagaininCdTo.Focus();
                this.txtNakagaininCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }


        /// <summary>
        /// 仲買人コード(自)の入力チェック
        /// </summary>
        private bool IsValidNakagaininCdFr()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdFr.Text))
            {
                this.lblNakagaininCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtNakagaininCdFr.Text))
            {
                Msg.Error("仲買人コードは数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblNakagaininCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtMizuageShishoCd.Text, this.txtNakagaininCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 仲買人コード(至)の入力チェック
        /// </summary>
        private bool IsValidNakagaininCdTo()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdTo.Text))
            {
                this.lblNakagaininCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtNakagaininCdTo.Text))
            {
                Msg.Error("仲買人コードは数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblNakagaininCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtMizuageShishoCd.Text, this.txtNakagaininCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            bool dataFlag;

            try
            {
#if DEBUG
                //ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNDR11011R rpt = new HNDR11011R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;
                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    //if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                // デバッグ時はデータの確認をしたいので、データが残るようにする。
                this.Dba.Commit();
#else
                // 印刷後なので、帳票出力用ワークテーブルをロールバックで元に戻す
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                    this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                    this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);
            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            #region "精算区分設定"
            // 精算区分設定
            //string seisanKubunSettei;
            //seisanKubunSettei = CHIKUNAI;
            //if (rdoChikunai.Checked)
            //{
            //    seisanKubunSettei = CHIKUNAI;
            //}
            //else if (rdoHamauri.Checked)
            //{
            //    seisanKubunSettei = HAMA_URI;
            //}
            //else if (rdoChikugai.Checked)
            //{
            //    seisanKubunSettei = CHIKUGAI;
            //}
            // 仲買人コード設定
            string nakagaininCdFr;
            string nakagaininCdTo;
            if (Util.ToDecimal(txtNakagaininCdFr.Text) > 0)
            {
                nakagaininCdFr = txtNakagaininCdFr.Text;
            }
            else
            {
                nakagaininCdFr = "0";
            }
            if (Util.ToDecimal(txtNakagaininCdTo.Text) > 0)
            {
                nakagaininCdTo = txtNakagaininCdTo.Text;
            }
            else
            {
                nakagaininCdTo = "9999";
            }
            string nakagai_shohizei_tenka_hoho;
            string row_num;
            #endregion  

            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            #region 仕切明細からダラダラ
            // han.VI_仕切明細(VI_HN_SHIKIRI_MEISAI)の日付範囲から発生しているデータを取得
            Sql.Append("SELECT");
            Sql.Append(" SHISHO_CD,");
            Sql.Append(" SERIBI,");
            Sql.Append(" YAMA_NO,");
            Sql.Append(" NAKAGAININ_CD,");
            Sql.Append(" NAKAGAININ_NM,");
            Sql.Append(" GYOSHU_CD,");
            Sql.Append(" GYOSHU_NM,");
            Sql.Append(" SURYO,");
            Sql.Append(" TANKA,");
            Sql.Append(" GOKEI_SHOHIZEI,");
            Sql.Append(" SHOHIZEI,");
            //Sql.Append(" CASE WHEN SEISAN_KUBUN = 2 OR SHOHIZEI_NYURYOKU_HOHO = 3 THEN 0 ELSE GOKEI_SHOHIZEI END AS GOKEI_SHOHIZEI,");
            Sql.Append(" KINGAKU ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHIKIRI_MEISAI ");
            Sql.Append("WHERE");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" SERIBI BETWEEN @DATE_FR AND @DATE_TO ");
            Sql.Append(" AND KAISHA_CD = @KAISHA_CD");
            if (this.txtMizuageShishoCd.Text != "0")
            {
                Sql.Append(" AND SHISHO_CD = @SHISHO_CD");
            }
            //Sql.Append(" AND SEISAN_KUBUN = @SEISAN_KUBUN");
            Sql.Append(" AND NAKAGAININ_CD BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO ");

            Sql.Append(" ORDER BY");
            Sql.Append(" KAISHA_CD ASC,");
            Sql.Append(" SHISHO_CD ASC,");
            Sql.Append(" SERIBI ASC,");
            Sql.Append(" NAKAGAININ_CD ASC,");
            Sql.Append(" YAMA_NO ASC");
            #endregion

            Sql = new StringBuilder();
            #region マスタ設定の抽出
            Sql.Append("SELECT");
            Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.SERIBI,");
            Sql.Append(" A.YAMA_NO,");
            Sql.Append(" A.NAKAGAININ_CD,");
            Sql.Append(" A.NAKAGAININ_NM,");
            Sql.Append(" A.GYOSHU_CD,");
            Sql.Append(" A.GYOSHU_NM,");
            Sql.Append(" A.SURYO,");
            Sql.Append(" A.TANKA,");
            Sql.Append(" A.SHOHIZEI,");
            Sql.Append(" A.KINGAKU,");
            Sql.Append(" ISNULL(A.NAKAGAI_SHOHIZEI_TENKA_HOHO, 1) AS NAKAGAI_SHOHIZEI_TENKA_HOHO,");

            Sql.Append(" ROW_NUMBER() OVER (PARTITION BY A.NAKAGAININ_CD ORDER BY A.NAKAGAININ_CD, A.YAMA_NO) AS ROW_NUM,");

            Sql.Append(" CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            Sql.Append("      THEN A.KINGAKU - A.SHOHIZEI ");
            Sql.Append("      ELSE A.KINGAKU END AS ZEINUKI_KINGAKU,");
            Sql.Append(" dbo.FNC_MarumeS((CASE WHEN A.NAKAGAI_SHOHIZEI_NYURYOKU_HOHO = 3 ");
            Sql.Append("      THEN A.KINGAKU - A.SHOHIZEI ");
            Sql.Append("      ELSE A.KINGAKU END) * (A.ZEI_RITSU / 100), 0, A.NAKAGAI_SHOHIZEI_HASU_SHORI) AS MEISAI_SHOHIZEI,");

            Sql.Append(" B.DENPYO_SHOHIZEI ");

            Sql.Append("FROM");
            Sql.Append(" VI_HN_SNGDIKN_MISI_KEN_SIKS AS A ");

            #region 伝票転嫁（セリ日毎）分の抽出
            Sql.Append(" LEFT OUTER JOIN ");
            Sql.Append(" (");
            Sql.Append("     SELECT ");
            Sql.Append("     S.KAISHA_CD,");
            Sql.Append("     S.SHISHO_CD,");
            Sql.Append("     S.SERIBI,");
            Sql.Append("     S.NAKAGAININ_CD,");
            Sql.Append("     SUM(CASE WHEN S.NAKAGAI_SHOHIZEI_NYURYOKU_HOHO = 3 ");
            Sql.Append("         THEN S.KINGAKU - S.SHOHIZEI ");
            Sql.Append("         ELSE S.KINGAKU END) AS DENPYO_ZEINUKI_KINGAKU,");
            Sql.Append("     dbo.FNC_MarumeS(SUM(CASE WHEN S.NAKAGAI_SHOHIZEI_NYURYOKU_HOHO = 3 ");
            Sql.Append("         THEN S.KINGAKU - S.SHOHIZEI ");
            Sql.Append("         ELSE S.KINGAKU END) * (S.ZEI_RITSU / 100), 0, ISNULL(S.NAKAGAI_SHOHIZEI_HASU_SHORI, 2)) AS DENPYO_SHOHIZEI ");
            Sql.Append("     FROM ");
            Sql.Append("     VI_HN_SNGDIKN_MISI_KEN_SIKS AS S ");
            Sql.Append("     WHERE S.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append("     S.SERIBI BETWEEN @DATE_FR AND @DATE_TO ");
            Sql.Append("     AND S.KAISHA_CD = @KAISHA_CD");
            if (this.txtMizuageShishoCd.Text != "0")
            {
                Sql.Append("     AND S.SHISHO_CD = @SHISHO_CD");
            }
            Sql.Append("     AND     S.NAKAGAININ_CD BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO ");

            Sql.Append("     GROUP BY");
            Sql.Append("      S.KAISHA_CD,");
            Sql.Append("      S.SHISHO_CD,");
            Sql.Append("      S.SERIBI,");
            Sql.Append("      S.NAKAGAININ_CD,");
            Sql.Append("      S.ZEI_RITSU,");
            Sql.Append("      S.NAKAGAI_SHOHIZEI_HASU_SHORI ");
            Sql.Append(" ) AS B ");
            Sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append("    AND A.SHISHO_CD = B.SHISHO_CD ");
            Sql.Append("    AND A.NAKAGAININ_CD = B.NAKAGAININ_CD ");
            Sql.Append("    AND A.SERIBI = B.SERIBI ");
            #endregion

            Sql.Append("WHERE");
            Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" A.SERIBI BETWEEN @DATE_FR AND @DATE_TO ");
            Sql.Append(" AND A.KAISHA_CD = @KAISHA_CD");
            if (this.txtMizuageShishoCd.Text != "0")
            {
                Sql.Append(" AND A.SHISHO_CD = @SHISHO_CD");
            }
            Sql.Append(" AND A.NAKAGAININ_CD BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO ");

            Sql.Append(" ORDER BY");
            Sql.Append(" A.KAISHA_CD ASC,");
            Sql.Append(" A.SHISHO_CD ASC,");
            Sql.Append(" A.SERIBI ASC,");
            Sql.Append(" A.NAKAGAININ_CD ASC,");
            Sql.Append(" A.YAMA_NO ASC");
            #endregion

            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            //dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKubunSettei);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 4, nakagaininCdFr);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 4, nakagaininCdTo);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }

            #region メインループ準備処理
            int dbSORT = 1;
            int i = 0; // ループ用カウント変数
            string tmpShohizei; // 消費税変数
            string tmpTtlKingaku; // 合計金額変数
            string printDate = this.convAdToJpDateString(DateTime.Today); // 印刷日付
            decimal mizuageSuryo = 0; // 数量の端数処理用
            string shishoseribi = "";
            #endregion

            while (dtMainLoop.Rows.Count > i)
            {
                dpc = new DbParamCollection();
                Sql = new StringBuilder();

                #region データの準備
                tmpShohizei = Util.ToString(dtMainLoop.Rows[i]["SHOHIZEI"]);
                tmpTtlKingaku = Util.ToString(Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]) + Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]));

                // 水揚数量の小数点第二位を切り捨てる
                mizuageSuryo = Util.ToDecimal(dtMainLoop.Rows[i]["SURYO"]) * 10;
                mizuageSuryo = (decimal)((int)(mizuageSuryo)) / 10;
                // 支所＋セリ日
                shishoseribi = Util.ToInt(Util.ToString(dtMainLoop.Rows[i]["SHISHO_CD"])).ToString("0000") + "-" + Util.ToDate(dtMainLoop.Rows[i]["SERIBI"]).ToString("yyyy/MM/dd");
                // マスタ設定による消費税処理
                nakagai_shohizei_tenka_hoho = Util.ToString(dtMainLoop.Rows[i]["NAKAGAI_SHOHIZEI_TENKA_HOHO"]);
                row_num = Util.ToString(dtMainLoop.Rows[i]["ROW_NUM"]);
                if (nakagai_shohizei_tenka_hoho == "1")
                {
                    tmpShohizei = Util.ToString(dtMainLoop.Rows[i]["MEISAI_SHOHIZEI"]);
                }
                else
                {
                    if (row_num == "1")
                    {
                        tmpShohizei = Util.ToString(dtMainLoop.Rows[i]["DENPYO_SHOHIZEI"]);
                    }
                    else
                    {
                        tmpShohizei = "0";
                    }
                }
                #endregion

                #region インサートテーブル
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                Sql.Append(") ");
                #endregion

                #region データ登録
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                dbSORT++;
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SERIBI"]); // セリ日
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人CD
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_NM"]); // 仲買人名称
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YAMA_NO"]); // 山NO
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOSHU_CD"]); // 魚種CD
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOSHU_NM"]); // 魚種名称
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, mizuageSuryo); // 水揚数量Kg
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TANKA"]); // 単価
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU"]); // 金額
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, tmpShohizei); // 消費税
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, tmpTtlKingaku); // 合計金額
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, printDate); // 印刷日付
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人CD（制御用）
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, shishoseribi); // 支所＋セリ日（制御用）
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, shishoCd); // 支所コード
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, nakagai_shohizei_tenka_hoho); // 仲買人消費税転嫁方法

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                #endregion

                i++;
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 西暦→和暦変換 Util.ConvJpDate(DateTime.Now, this.Dba)
        /// </summary>
        /// <returns></returns>
        private string convAdToJpDateString(DateTime targetDate)
        {
            string[] arrJpDate = Util.ConvJpDate(targetDate, this.Dba);
            return string.Format("{0}{1}年{2:D2}月{3:D2}日", arrJpDate[0], arrJpDate[2], Util.ToInt(arrJpDate[3]), Util.ToInt(arrJpDate[4]));
        }

        #endregion

        private void HNDR1101_KeyDown(object sender, KeyEventArgs e)
        {
            {
                switch (e.KeyCode)
                {
                    case Keys.Right:
                        FocusNext();
                        break;
                    case Keys.Left:
                        FocusPrev();
                        break;
                }
            }
        }

        private void FocusNext()
        {
            if (txtMizuageShishoCd.Focused || txtYearFr.Focused || txtMonthFr.Focused || txtDayFr.Focused || txtYearTo.Focused || txtMonthTo.Focused || txtDayTo.Focused|| txtNakagaininCdFr.Focused|| txtNakagaininCdTo.Focused)
            {
                SelectNextControl(ActiveControl, true, true, true, true);
            }
        }

        private void FocusPrev()
        {
            if (txtMizuageShishoCd.Focused || txtYearFr.Focused || txtMonthFr.Focused || txtDayFr.Focused || txtYearTo.Focused || txtMonthTo.Focused || txtDayTo.Focused || txtNakagaininCdFr.Focused || txtNakagaininCdTo.Focused)
            {
                SelectNextControl(ActiveControl, false, true, true, true);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
        