﻿namespace jp.co.fsi.hn.hncm1031
{
    partial class HNCM1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblGyohoKana = new System.Windows.Forms.Label();
			this.txtGyohoKana = new System.Windows.Forms.TextBox();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 438);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(757, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(747, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "漁法の登録";
			// 
			// lblGyohoKana
			// 
			this.lblGyohoKana.BackColor = System.Drawing.Color.Silver;
			this.lblGyohoKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyohoKana.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGyohoKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyohoKana.Location = new System.Drawing.Point(0, 0);
			this.lblGyohoKana.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyohoKana.Name = "lblGyohoKana";
			this.lblGyohoKana.Size = new System.Drawing.Size(701, 26);
			this.lblGyohoKana.TabIndex = 0;
			this.lblGyohoKana.Tag = "CHANGE";
			this.lblGyohoKana.Text = "カ　ナ　名";
			this.lblGyohoKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyohoKana
			// 
			this.txtGyohoKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyohoKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtGyohoKana.Location = new System.Drawing.Point(98, 1);
			this.txtGyohoKana.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyohoKana.MaxLength = 20;
			this.txtGyohoKana.Name = "txtGyohoKana";
			this.txtGyohoKana.Size = new System.Drawing.Size(239, 23);
			this.txtGyohoKana.TabIndex = 1;
			this.txtGyohoKana.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoName_Validating);
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(23, 83);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(707, 395);
			this.dgvList.TabIndex = 2;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(21, 46);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 1;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(709, 34);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtGyohoKana);
			this.fsiPanel1.Controls.Add(this.lblGyohoKana);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(701, 26);
			this.fsiPanel1.TabIndex = 0;
			// 
			// HNCM1031
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(747, 576);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1031";
			this.Par1 = "";
			this.Text = "漁法の登録";
			this.Load += new System.EventHandler(this.HNCM1031_Load);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblGyohoKana;
        private System.Windows.Forms.TextBox txtGyohoKana;
        private System.Windows.Forms.DataGridView dgvList;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
    }
}