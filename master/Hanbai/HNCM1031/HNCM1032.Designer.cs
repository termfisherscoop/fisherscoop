﻿namespace jp.co.fsi.hn.hncm1031
{
    partial class HNCM1032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtGyohoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyohoCd = new System.Windows.Forms.Label();
            this.txtGyohoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyohoKana = new System.Windows.Forms.Label();
            this.txtGyohoKana = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyohoNM = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 114);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(561, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(551, 31);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "漁法マスタ登録";
            // 
            // txtGyohoCd
            // 
            this.txtGyohoCd.AutoSizeFromLength = true;
            this.txtGyohoCd.DisplayLength = null;
            this.txtGyohoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyohoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGyohoCd.Location = new System.Drawing.Point(107, 4);
            this.txtGyohoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtGyohoCd.MaxLength = 4;
            this.txtGyohoCd.Name = "txtGyohoCd";
            this.txtGyohoCd.Size = new System.Drawing.Size(44, 23);
            this.txtGyohoCd.TabIndex = 1;
            this.txtGyohoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyohoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoCd_Validating);
            // 
            // lbGyohoCd
            // 
            this.lbGyohoCd.BackColor = System.Drawing.Color.Silver;
            this.lbGyohoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyohoCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbGyohoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbGyohoCd.Location = new System.Drawing.Point(0, 0);
            this.lbGyohoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbGyohoCd.Name = "lbGyohoCd";
            this.lbGyohoCd.Size = new System.Drawing.Size(526, 34);
            this.lbGyohoCd.TabIndex = 0;
            this.lbGyohoCd.Tag = "CHANGE";
            this.lbGyohoCd.Text = "漁法コード";
            this.lbGyohoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyohoNm
            // 
            this.txtGyohoNm.AutoSizeFromLength = false;
            this.txtGyohoNm.DisplayLength = null;
            this.txtGyohoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyohoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtGyohoNm.Location = new System.Drawing.Point(107, 4);
            this.txtGyohoNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtGyohoNm.MaxLength = 40;
            this.txtGyohoNm.Name = "txtGyohoNm";
            this.txtGyohoNm.Size = new System.Drawing.Size(367, 23);
            this.txtGyohoNm.TabIndex = 3;
            this.txtGyohoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoNm_Validating);
            // 
            // lbGyohoKana
            // 
            this.lbGyohoKana.BackColor = System.Drawing.Color.Silver;
            this.lbGyohoKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyohoKana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbGyohoKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbGyohoKana.Location = new System.Drawing.Point(0, 0);
            this.lbGyohoKana.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbGyohoKana.Name = "lbGyohoKana";
            this.lbGyohoKana.Size = new System.Drawing.Size(526, 35);
            this.lbGyohoKana.TabIndex = 4;
            this.lbGyohoKana.Tag = "CHANGE";
            this.lbGyohoKana.Text = "漁法カナ名";
            this.lbGyohoKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyohoKana
            // 
            this.txtGyohoKana.AutoSizeFromLength = false;
            this.txtGyohoKana.DisplayLength = null;
            this.txtGyohoKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyohoKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtGyohoKana.Location = new System.Drawing.Point(107, 5);
            this.txtGyohoKana.Margin = new System.Windows.Forms.Padding(4);
            this.txtGyohoKana.MaxLength = 20;
            this.txtGyohoKana.Name = "txtGyohoKana";
            this.txtGyohoKana.Size = new System.Drawing.Size(367, 23);
            this.txtGyohoKana.TabIndex = 5;
            this.txtGyohoKana.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGyohoKana_KeyDown);
            this.txtGyohoKana.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoKana_Validating);
            // 
            // lbGyohoNM
            // 
            this.lbGyohoNM.BackColor = System.Drawing.Color.Silver;
            this.lbGyohoNM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbGyohoNM.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbGyohoNM.Location = new System.Drawing.Point(0, 0);
            this.lbGyohoNM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbGyohoNM.Name = "lbGyohoNM";
            this.lbGyohoNM.Size = new System.Drawing.Size(526, 34);
            this.lbGyohoNM.TabIndex = 2;
            this.lbGyohoNM.Tag = "CHANGE";
            this.lbGyohoNM.Text = "漁法名";
            this.lbGyohoNM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 39);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(534, 125);
            this.fsiTableLayoutPanel1.TabIndex = 1000;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtGyohoKana);
            this.fsiPanel3.Controls.Add(this.lbGyohoKana);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 86);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(526, 35);
            this.fsiPanel3.TabIndex = 2;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtGyohoNm);
            this.fsiPanel2.Controls.Add(this.lbGyohoNM);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 45);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(526, 34);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtGyohoCd);
            this.fsiPanel1.Controls.Add(this.lbGyohoCd);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(526, 34);
            this.fsiPanel1.TabIndex = 0;
            // 
            // HNCM1032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 251);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "HNCM1032";
            this.ShowFButton = true;
            this.Text = "漁法マスタ登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtGyohoCd;
        private System.Windows.Forms.Label lbGyohoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoNm;
        private System.Windows.Forms.Label lbGyohoKana;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoKana;
        private System.Windows.Forms.Label lbGyohoNM;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    };
}