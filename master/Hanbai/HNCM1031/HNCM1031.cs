﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1031
{
    /// <summary>
    ///　漁法マスタの登録(HNCM1031)
    /// </summary>
    public partial class HNCM1031 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "漁法の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1031()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(767, 677);
                // フォームの配置を上へ移動する
//                this.lblGyohoKana.Location = new System.Drawing.Point(12, 11);
//                this.txtGyohoKana.Location = new System.Drawing.Point(99, 13);
//                this.dgvList.Location = new System.Drawing.Point(12, 49);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // 漁法名にフォーカス
            this.txtGyohoKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtGyohoKana.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }

            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // 漁法名にフォーカスを戻す
            this.txtGyohoKana.Focus();
            this.txtGyohoKana.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ漁法マスタ登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 漁法マスタ登録画面の起動
                EditGyoho(string.Empty);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 漁法名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyohoName_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない

            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditGyoho(Util.ToString(this.dgvList.SelectedRows[0].Cells["漁法コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditGyoho(Util.ToString(this.dgvList.SelectedRows[0].Cells["漁法コード"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 漁法マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = 1");
            where.Append(" AND HN.SHORI_FLG != 2");
            if (isInitial)
            {
                // 初期処理の場合、検索結果がヒットしないようにあり得ない検索条件を設定する
                //where.Append(" AND HN.GYOHO_CD = -1");
            }
            else
            {
                // 初期処理でない場合、入力された漁法名から検索する
                if (!ValChk.IsEmpty(this.txtGyohoKana.Text))
                {
                    where.Append(" AND HN.GYOHO_KANA LIKE @GYOHO_KANA");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@GYOHO_KANA", SqlDbType.VarChar, 22, "%" + this.txtGyohoKana.Text + "%");
                }
            }

            string cols = "HN.GYOHO_CD AS 漁法コード";
            cols += ", HN.GYOHO_NM AS 漁法名";
            cols += ", HN.GYOHO_KANA AS 漁法カナ名";
            string from = "TB_HN_GYOHO_MST AS HN";

            DataTable dtGyoho =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "HN.GYOHO_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtGyoho.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtGyoho.Rows.Add(dtGyoho.NewRow());
            }

            this.dgvList.DataSource = dtGyoho;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 350;
            this.dgvList.Columns[2].Width = 220;
        }

        /// <summary>
        /// 漁法を追加編集する
        /// </summary>
        /// <param name="code">漁法コード(空：新規登録、以外：編集)</param>
        private void EditGyoho(string code)
        {
            HNCM1032 frmHNCM1032;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHNCM1032 = new HNCM1032("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmHNCM1032 = new HNCM1032("2");
                frmHNCM1032.InData = code;
            }

            DialogResult result = frmHNCM1032.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["漁法コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }

                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3]{ 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["漁法コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["漁法名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["漁法カナ名"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
		#endregion

		private void HNCM1031_Load(object sender, System.EventArgs e)
		{

		}
	}
}
