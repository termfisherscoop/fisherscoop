﻿namespace jp.co.fsi.hn.hnse1021
{
    partial class HNSE1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.lblFunanushiCd = new System.Windows.Forms.Label();
			this.lblFunanushiCdResults = new System.Windows.Forms.Label();
			this.txtFunanushiCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeisanbiMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanbiMonth = new System.Windows.Forms.Label();
			this.txtSeisanbiDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisaibiYear = new System.Windows.Forms.Label();
			this.txtSeisanbiYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanbiGengo = new System.Windows.Forms.Label();
			this.lblSeisanbiDay = new System.Windows.Forms.Label();
			this.lblSeisabi = new System.Windows.Forms.Label();
			this.txtDenpyoMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoMonthFr = new System.Windows.Forms.Label();
			this.txtDenpyoDayFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoYearFr = new System.Windows.Forms.Label();
			this.txtDenpyoYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoGengoFr = new System.Windows.Forms.Label();
			this.lblDenpyoDayFr = new System.Windows.Forms.Label();
			this.lblDenpyoFr = new System.Windows.Forms.Label();
			this.txtDenpyoMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoMonthTo = new System.Windows.Forms.Label();
			this.txtDenpyoDayTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoYearTo = new System.Windows.Forms.Label();
			this.txtDenpyoYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoGengoTo = new System.Windows.Forms.Label();
			this.lblDenpyoDayTo = new System.Windows.Forms.Label();
			this.lblDenpyoBet = new System.Windows.Forms.Label();
			this.btnEnter = new System.Windows.Forms.Button();
			this.tableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.panel3 = new jp.co.fsi.common.FsiPanel();
			this.panel2 = new jp.co.fsi.common.FsiPanel();
			this.panel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Location = new System.Drawing.Point(7, 422);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(733, 133);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			// 
			// lblTitle
			// 
			this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(722, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.Location = new System.Drawing.Point(10, 143);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(700, 336);
			this.dgvList.TabIndex = 28;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// lblFunanushiCd
			// 
			this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCd.Location = new System.Drawing.Point(0, 0);
			this.lblFunanushiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCd.Name = "lblFunanushiCd";
			this.lblFunanushiCd.Size = new System.Drawing.Size(696, 35);
			this.lblFunanushiCd.TabIndex = 0;
			this.lblFunanushiCd.Tag = "CHANGE";
			this.lblFunanushiCd.Text = "船 主 C D";
			this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiCdResults
			// 
			this.lblFunanushiCdResults.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdResults.Location = new System.Drawing.Point(174, 4);
			this.lblFunanushiCdResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCdResults.Name = "lblFunanushiCdResults";
			this.lblFunanushiCdResults.Size = new System.Drawing.Size(249, 24);
			this.lblFunanushiCdResults.TabIndex = 2;
			this.lblFunanushiCdResults.Tag = "DISPNAME";
			this.lblFunanushiCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCd
			// 
			this.txtFunanushiCd.AutoSizeFromLength = false;
			this.txtFunanushiCd.DisplayLength = null;
			this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCd.Location = new System.Drawing.Point(100, 4);
			this.txtFunanushiCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiCd.MaxLength = 4;
			this.txtFunanushiCd.Name = "txtFunanushiCd";
			this.txtFunanushiCd.Size = new System.Drawing.Size(68, 23);
			this.txtFunanushiCd.TabIndex = 1;
			this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCd_Validating);
			// 
			// txtSeisanbiMonth
			// 
			this.txtSeisanbiMonth.AutoSizeFromLength = false;
			this.txtSeisanbiMonth.DisplayLength = null;
			this.txtSeisanbiMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanbiMonth.Location = new System.Drawing.Point(228, 5);
			this.txtSeisanbiMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanbiMonth.MaxLength = 2;
			this.txtSeisanbiMonth.Name = "txtSeisanbiMonth";
			this.txtSeisanbiMonth.Size = new System.Drawing.Size(39, 23);
			this.txtSeisanbiMonth.TabIndex = 7;
			this.txtSeisanbiMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanbiMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanbiMonth_Validating);
			// 
			// lblSeisanbiMonth
			// 
			this.lblSeisanbiMonth.BackColor = System.Drawing.Color.Silver;
			this.lblSeisanbiMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanbiMonth.Location = new System.Drawing.Point(271, 6);
			this.lblSeisanbiMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanbiMonth.Name = "lblSeisanbiMonth";
			this.lblSeisanbiMonth.Size = new System.Drawing.Size(20, 25);
			this.lblSeisanbiMonth.TabIndex = 8;
			this.lblSeisanbiMonth.Tag = "CHANGE";
			this.lblSeisanbiMonth.Text = "月";
			this.lblSeisanbiMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanbiDay
			// 
			this.txtSeisanbiDay.AutoSizeFromLength = false;
			this.txtSeisanbiDay.DisplayLength = null;
			this.txtSeisanbiDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanbiDay.Location = new System.Drawing.Point(295, 5);
			this.txtSeisanbiDay.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanbiDay.MaxLength = 2;
			this.txtSeisanbiDay.Name = "txtSeisanbiDay";
			this.txtSeisanbiDay.Size = new System.Drawing.Size(39, 23);
			this.txtSeisanbiDay.TabIndex = 9;
			this.txtSeisanbiDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanbiDay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSeisanbiDay_KeyDown);
			this.txtSeisanbiDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanbiDay_Validating);
			// 
			// lblSeisaibiYear
			// 
			this.lblSeisaibiYear.BackColor = System.Drawing.Color.Silver;
			this.lblSeisaibiYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisaibiYear.Location = new System.Drawing.Point(204, 3);
			this.lblSeisaibiYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisaibiYear.Name = "lblSeisaibiYear";
			this.lblSeisaibiYear.Size = new System.Drawing.Size(23, 28);
			this.lblSeisaibiYear.TabIndex = 6;
			this.lblSeisaibiYear.Tag = "CHANGE";
			this.lblSeisaibiYear.Text = "年";
			this.lblSeisaibiYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanbiYear
			// 
			this.txtSeisanbiYear.AutoSizeFromLength = false;
			this.txtSeisanbiYear.DisplayLength = null;
			this.txtSeisanbiYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanbiYear.Location = new System.Drawing.Point(161, 5);
			this.txtSeisanbiYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanbiYear.MaxLength = 2;
			this.txtSeisanbiYear.Name = "txtSeisanbiYear";
			this.txtSeisanbiYear.Size = new System.Drawing.Size(39, 23);
			this.txtSeisanbiYear.TabIndex = 5;
			this.txtSeisanbiYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanbiYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanbiYear_Validating);
			// 
			// lblSeisanbiGengo
			// 
			this.lblSeisanbiGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeisanbiGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanbiGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanbiGengo.Location = new System.Drawing.Point(100, 5);
			this.lblSeisanbiGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanbiGengo.Name = "lblSeisanbiGengo";
			this.lblSeisanbiGengo.Size = new System.Drawing.Size(55, 24);
			this.lblSeisanbiGengo.TabIndex = 4;
			this.lblSeisanbiGengo.Tag = "DISPNAME";
			this.lblSeisanbiGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeisanbiDay
			// 
			this.lblSeisanbiDay.BackColor = System.Drawing.Color.Silver;
			this.lblSeisanbiDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanbiDay.Location = new System.Drawing.Point(337, 5);
			this.lblSeisanbiDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanbiDay.Name = "lblSeisanbiDay";
			this.lblSeisanbiDay.Size = new System.Drawing.Size(27, 24);
			this.lblSeisanbiDay.TabIndex = 10;
			this.lblSeisanbiDay.Tag = "CHANGE";
			this.lblSeisanbiDay.Text = "日";
			this.lblSeisanbiDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeisabi
			// 
			this.lblSeisabi.BackColor = System.Drawing.Color.Silver;
			this.lblSeisabi.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblSeisabi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisabi.Location = new System.Drawing.Point(0, 0);
			this.lblSeisabi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisabi.Name = "lblSeisabi";
			this.lblSeisabi.Size = new System.Drawing.Size(696, 35);
			this.lblSeisabi.TabIndex = 3;
			this.lblSeisabi.Tag = "CHANGE";
			this.lblSeisabi.Text = "精  算  日";
			this.lblSeisabi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDenpyoMonthFr
			// 
			this.txtDenpyoMonthFr.AutoSizeFromLength = false;
			this.txtDenpyoMonthFr.DisplayLength = null;
			this.txtDenpyoMonthFr.Enabled = false;
			this.txtDenpyoMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDenpyoMonthFr.Location = new System.Drawing.Point(227, 6);
			this.txtDenpyoMonthFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDenpyoMonthFr.MaxLength = 2;
			this.txtDenpyoMonthFr.Name = "txtDenpyoMonthFr";
			this.txtDenpyoMonthFr.ReadOnly = true;
			this.txtDenpyoMonthFr.Size = new System.Drawing.Size(39, 23);
			this.txtDenpyoMonthFr.TabIndex = 15;
			this.txtDenpyoMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblDenpyoMonthFr
			// 
			this.lblDenpyoMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoMonthFr.Location = new System.Drawing.Point(270, 8);
			this.lblDenpyoMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoMonthFr.Name = "lblDenpyoMonthFr";
			this.lblDenpyoMonthFr.Size = new System.Drawing.Size(20, 25);
			this.lblDenpyoMonthFr.TabIndex = 16;
			this.lblDenpyoMonthFr.Tag = "CHANGE";
			this.lblDenpyoMonthFr.Text = "月";
			this.lblDenpyoMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDenpyoDayFr
			// 
			this.txtDenpyoDayFr.AutoSizeFromLength = false;
			this.txtDenpyoDayFr.DisplayLength = null;
			this.txtDenpyoDayFr.Enabled = false;
			this.txtDenpyoDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDenpyoDayFr.Location = new System.Drawing.Point(294, 6);
			this.txtDenpyoDayFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDenpyoDayFr.MaxLength = 2;
			this.txtDenpyoDayFr.Name = "txtDenpyoDayFr";
			this.txtDenpyoDayFr.ReadOnly = true;
			this.txtDenpyoDayFr.Size = new System.Drawing.Size(39, 23);
			this.txtDenpyoDayFr.TabIndex = 17;
			this.txtDenpyoDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblDenpyoYearFr
			// 
			this.lblDenpyoYearFr.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoYearFr.Location = new System.Drawing.Point(203, 5);
			this.lblDenpyoYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoYearFr.Name = "lblDenpyoYearFr";
			this.lblDenpyoYearFr.Size = new System.Drawing.Size(23, 28);
			this.lblDenpyoYearFr.TabIndex = 14;
			this.lblDenpyoYearFr.Tag = "CHANGE";
			this.lblDenpyoYearFr.Text = "年";
			this.lblDenpyoYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDenpyoYearFr
			// 
			this.txtDenpyoYearFr.AutoSizeFromLength = false;
			this.txtDenpyoYearFr.DisplayLength = null;
			this.txtDenpyoYearFr.Enabled = false;
			this.txtDenpyoYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDenpyoYearFr.Location = new System.Drawing.Point(160, 6);
			this.txtDenpyoYearFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDenpyoYearFr.MaxLength = 2;
			this.txtDenpyoYearFr.Name = "txtDenpyoYearFr";
			this.txtDenpyoYearFr.ReadOnly = true;
			this.txtDenpyoYearFr.Size = new System.Drawing.Size(39, 23);
			this.txtDenpyoYearFr.TabIndex = 13;
			this.txtDenpyoYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblDenpyoGengoFr
			// 
			this.lblDenpyoGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblDenpyoGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDenpyoGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoGengoFr.Location = new System.Drawing.Point(100, 5);
			this.lblDenpyoGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoGengoFr.Name = "lblDenpyoGengoFr";
			this.lblDenpyoGengoFr.Size = new System.Drawing.Size(55, 24);
			this.lblDenpyoGengoFr.TabIndex = 12;
			this.lblDenpyoGengoFr.Tag = "DISPNAME";
			this.lblDenpyoGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDenpyoDayFr
			// 
			this.lblDenpyoDayFr.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoDayFr.Location = new System.Drawing.Point(336, 6);
			this.lblDenpyoDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoDayFr.Name = "lblDenpyoDayFr";
			this.lblDenpyoDayFr.Size = new System.Drawing.Size(27, 24);
			this.lblDenpyoDayFr.TabIndex = 18;
			this.lblDenpyoDayFr.Tag = "CHANGE";
			this.lblDenpyoDayFr.Text = "日";
			this.lblDenpyoDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDenpyoFr
			// 
			this.lblDenpyoFr.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoFr.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDenpyoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoFr.Location = new System.Drawing.Point(0, 0);
			this.lblDenpyoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoFr.Name = "lblDenpyoFr";
			this.lblDenpyoFr.Size = new System.Drawing.Size(696, 35);
			this.lblDenpyoFr.TabIndex = 11;
			this.lblDenpyoFr.Tag = "CHANGE";
			this.lblDenpyoFr.Text = "伝 票 日 付";
			this.lblDenpyoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDenpyoMonthTo
			// 
			this.txtDenpyoMonthTo.AutoSizeFromLength = false;
			this.txtDenpyoMonthTo.DisplayLength = null;
			this.txtDenpyoMonthTo.Enabled = false;
			this.txtDenpyoMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDenpyoMonthTo.Location = new System.Drawing.Point(535, 5);
			this.txtDenpyoMonthTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDenpyoMonthTo.MaxLength = 2;
			this.txtDenpyoMonthTo.Name = "txtDenpyoMonthTo";
			this.txtDenpyoMonthTo.ReadOnly = true;
			this.txtDenpyoMonthTo.Size = new System.Drawing.Size(39, 23);
			this.txtDenpyoMonthTo.TabIndex = 24;
			this.txtDenpyoMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblDenpyoMonthTo
			// 
			this.lblDenpyoMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoMonthTo.Location = new System.Drawing.Point(580, 6);
			this.lblDenpyoMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoMonthTo.Name = "lblDenpyoMonthTo";
			this.lblDenpyoMonthTo.Size = new System.Drawing.Size(20, 25);
			this.lblDenpyoMonthTo.TabIndex = 25;
			this.lblDenpyoMonthTo.Tag = "CHANGE";
			this.lblDenpyoMonthTo.Text = "月";
			this.lblDenpyoMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDenpyoDayTo
			// 
			this.txtDenpyoDayTo.AutoSizeFromLength = false;
			this.txtDenpyoDayTo.DisplayLength = null;
			this.txtDenpyoDayTo.Enabled = false;
			this.txtDenpyoDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDenpyoDayTo.Location = new System.Drawing.Point(604, 5);
			this.txtDenpyoDayTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDenpyoDayTo.MaxLength = 2;
			this.txtDenpyoDayTo.Name = "txtDenpyoDayTo";
			this.txtDenpyoDayTo.ReadOnly = true;
			this.txtDenpyoDayTo.Size = new System.Drawing.Size(39, 23);
			this.txtDenpyoDayTo.TabIndex = 26;
			this.txtDenpyoDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblDenpyoYearTo
			// 
			this.lblDenpyoYearTo.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoYearTo.Location = new System.Drawing.Point(508, 4);
			this.lblDenpyoYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoYearTo.Name = "lblDenpyoYearTo";
			this.lblDenpyoYearTo.Size = new System.Drawing.Size(23, 28);
			this.lblDenpyoYearTo.TabIndex = 23;
			this.lblDenpyoYearTo.Tag = "CHANGE";
			this.lblDenpyoYearTo.Text = "年";
			this.lblDenpyoYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDenpyoYearTo
			// 
			this.txtDenpyoYearTo.AutoSizeFromLength = false;
			this.txtDenpyoYearTo.DisplayLength = null;
			this.txtDenpyoYearTo.Enabled = false;
			this.txtDenpyoYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDenpyoYearTo.Location = new System.Drawing.Point(465, 5);
			this.txtDenpyoYearTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDenpyoYearTo.MaxLength = 2;
			this.txtDenpyoYearTo.Name = "txtDenpyoYearTo";
			this.txtDenpyoYearTo.ReadOnly = true;
			this.txtDenpyoYearTo.Size = new System.Drawing.Size(39, 23);
			this.txtDenpyoYearTo.TabIndex = 22;
			this.txtDenpyoYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblDenpyoGengoTo
			// 
			this.lblDenpyoGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDenpyoGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDenpyoGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoGengoTo.Location = new System.Drawing.Point(405, 5);
			this.lblDenpyoGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoGengoTo.Name = "lblDenpyoGengoTo";
			this.lblDenpyoGengoTo.Size = new System.Drawing.Size(55, 24);
			this.lblDenpyoGengoTo.TabIndex = 21;
			this.lblDenpyoGengoTo.Tag = "DISPNAME";
			this.lblDenpyoGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDenpyoDayTo
			// 
			this.lblDenpyoDayTo.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoDayTo.Location = new System.Drawing.Point(647, 5);
			this.lblDenpyoDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoDayTo.Name = "lblDenpyoDayTo";
			this.lblDenpyoDayTo.Size = new System.Drawing.Size(27, 24);
			this.lblDenpyoDayTo.TabIndex = 27;
			this.lblDenpyoDayTo.Tag = "CHANGE";
			this.lblDenpyoDayTo.Text = "日";
			this.lblDenpyoDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDenpyoBet
			// 
			this.lblDenpyoBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoBet.Location = new System.Drawing.Point(369, 4);
			this.lblDenpyoBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoBet.Name = "lblDenpyoBet";
			this.lblDenpyoBet.Size = new System.Drawing.Size(21, 31);
			this.lblDenpyoBet.TabIndex = 19;
			this.lblDenpyoBet.Tag = "CHANGE";
			this.lblDenpyoBet.Text = "～";
			this.lblDenpyoBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnEnter
			// 
			this.btnEnter.BackColor = System.Drawing.Color.SkyBlue;
			this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEnter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnEnter.ForeColor = System.Drawing.Color.Navy;
			this.btnEnter.Location = new System.Drawing.Point(89, 4);
			this.btnEnter.Margin = new System.Windows.Forms.Padding(4);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new System.Drawing.Size(87, 60);
			this.btnEnter.TabIndex = 907;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n決定";
			this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = false;
			this.btnEnter.Visible = false;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(7, 12);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(704, 127);
			this.tableLayoutPanel1.TabIndex = 1000;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.lblDenpyoGengoFr);
			this.panel3.Controls.Add(this.lblDenpyoBet);
			this.panel3.Controls.Add(this.lblDenpyoDayFr);
			this.panel3.Controls.Add(this.txtDenpyoMonthTo);
			this.panel3.Controls.Add(this.txtDenpyoYearFr);
			this.panel3.Controls.Add(this.lblDenpyoMonthTo);
			this.panel3.Controls.Add(this.lblDenpyoYearFr);
			this.panel3.Controls.Add(this.txtDenpyoDayTo);
			this.panel3.Controls.Add(this.txtDenpyoDayFr);
			this.panel3.Controls.Add(this.lblDenpyoYearTo);
			this.panel3.Controls.Add(this.lblDenpyoMonthFr);
			this.panel3.Controls.Add(this.txtDenpyoYearTo);
			this.panel3.Controls.Add(this.txtDenpyoMonthFr);
			this.panel3.Controls.Add(this.lblDenpyoGengoTo);
			this.panel3.Controls.Add(this.lblDenpyoDayTo);
			this.panel3.Controls.Add(this.lblDenpyoFr);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(4, 88);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(696, 35);
			this.panel3.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.lblSeisanbiGengo);
			this.panel2.Controls.Add(this.lblSeisanbiDay);
			this.panel2.Controls.Add(this.txtSeisanbiYear);
			this.panel2.Controls.Add(this.lblSeisaibiYear);
			this.panel2.Controls.Add(this.txtSeisanbiDay);
			this.panel2.Controls.Add(this.lblSeisanbiMonth);
			this.panel2.Controls.Add(this.txtSeisanbiMonth);
			this.panel2.Controls.Add(this.lblSeisabi);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(4, 46);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(696, 35);
			this.panel2.TabIndex = 1;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.txtFunanushiCd);
			this.panel1.Controls.Add(this.lblFunanushiCdResults);
			this.panel1.Controls.Add(this.lblFunanushiCd);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(4, 4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(696, 35);
			this.panel1.TabIndex = 0;
			// 
			// HNSE1022
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(722, 559);
			this.Controls.Add(this.dgvList);
			this.Controls.Add(this.tableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNSE1022";
			this.ShowFButton = true;
			this.Text = "";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCdResults;
        private common.controls.FsiTextBox txtFunanushiCd;
        private common.controls.FsiTextBox txtSeisanbiMonth;
        private System.Windows.Forms.Label lblSeisanbiMonth;
        private common.controls.FsiTextBox txtSeisanbiDay;
        private System.Windows.Forms.Label lblSeisaibiYear;
        private common.controls.FsiTextBox txtSeisanbiYear;
        private System.Windows.Forms.Label lblSeisanbiGengo;
        private System.Windows.Forms.Label lblSeisanbiDay;
        private System.Windows.Forms.Label lblSeisabi;
        private common.controls.FsiTextBox txtDenpyoMonthFr;
        private System.Windows.Forms.Label lblDenpyoMonthFr;
        private common.controls.FsiTextBox txtDenpyoDayFr;
        private System.Windows.Forms.Label lblDenpyoYearFr;
        private common.controls.FsiTextBox txtDenpyoYearFr;
        private System.Windows.Forms.Label lblDenpyoGengoFr;
        private System.Windows.Forms.Label lblDenpyoDayFr;
        private System.Windows.Forms.Label lblDenpyoFr;
        private common.controls.FsiTextBox txtDenpyoMonthTo;
        private System.Windows.Forms.Label lblDenpyoMonthTo;
        private common.controls.FsiTextBox txtDenpyoDayTo;
        private System.Windows.Forms.Label lblDenpyoYearTo;
        private common.controls.FsiTextBox txtDenpyoYearTo;
        private System.Windows.Forms.Label lblDenpyoGengoTo;
        private System.Windows.Forms.Label lblDenpyoDayTo;
        private System.Windows.Forms.Label lblDenpyoBet;
        protected System.Windows.Forms.Button btnEnter;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel1;
        private jp.co.fsi.common.FsiPanel panel3;
        private jp.co.fsi.common.FsiPanel panel2;
        private jp.co.fsi.common.FsiPanel panel1;
    }
}