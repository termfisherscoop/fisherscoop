﻿namespace jp.co.fsi.hn.hndr1031
{
    /// <summary>
    /// HNDR1031R の概要の説明です。
    /// </summary>
    partial class HNDR1031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNDR1031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.date = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.page = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ITEM02,
            this.ラベル3,
            this.ラベル25,
            this.直線34,
            this.テキスト110,
            this.ラベル130,
            this.ラベル132,
            this.ラベル133,
            this.ラベル134,
            this.ラベル135,
            this.ラベル136,
            this.ラベル137,
            this.date,
            this.page,
            this.textBox9});
            this.pageHeader.Height = 0.8263779F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ITEM02
            // 
            this.ITEM02.DataField = "ITEM02";
            this.ITEM02.Height = 0.1666064F;
            this.ITEM02.Left = 0.7484252F;
            this.ITEM02.MultiLine = false;
            this.ITEM02.Name = "ITEM02";
            this.ITEM02.OutputFormat = resources.GetString("ITEM02.OutputFormat");
            this.ITEM02.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.ITEM02.Tag = "";
            this.ITEM02.Text = "9999/99/99";
            this.ITEM02.Top = 0.2649606F;
            this.ITEM02.Width = 0.8791341F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.1700788F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 0.1748032F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "セリ日";
            this.ラベル3.Top = 0.2632246F;
            this.ラベル3.Width = 0.5625F;
            // 
            // ラベル25
            // 
            this.ラベル25.Height = 0.15625F;
            this.ラベル25.HyperLink = null;
            this.ラベル25.Left = 8.351969F;
            this.ラベル25.Name = "ラベル25";
            this.ラベル25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル25.Tag = "";
            this.ラベル25.Text = "頁";
            this.ラベル25.Top = 0.1771654F;
            this.ラベル25.Width = 0.3543307F;
            // 
            // 直線34
            // 
            this.直線34.Height = 1.645088E-05F;
            this.直線34.Left = 0.1574804F;
            this.直線34.LineWeight = 2F;
            this.直線34.Name = "直線34";
            this.直線34.Tag = "";
            this.直線34.Top = 0.7515749F;
            this.直線34.Width = 8.854725F;
            this.直線34.X1 = 0.1574804F;
            this.直線34.X2 = 9.012205F;
            this.直線34.Y1 = 0.7515749F;
            this.直線34.Y2 = 0.7515914F;
            // 
            // テキスト110
            // 
            this.テキスト110.DataField = "ITEM01";
            this.テキスト110.Height = 0.2291667F;
            this.テキスト110.Left = 3.329528F;
            this.テキスト110.Name = "テキスト110";
            this.テキスト110.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 16pt; font-" +
    "weight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト110.Tag = "";
            this.テキスト110.Text = "ITEM01";
            this.テキスト110.Top = 1.192093E-07F;
            this.テキスト110.Width = 2.952083F;
            // 
            // ラベル130
            // 
            this.ラベル130.Height = 0.1933071F;
            this.ラベル130.HyperLink = null;
            this.ラベル130.Left = 0.2236221F;
            this.ラベル130.Name = "ラベル130";
            this.ラベル130.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル130.Tag = "";
            this.ラベル130.Text = "魚種CD";
            this.ラベル130.Top = 0.5307087F;
            this.ラベル130.Width = 0.6023623F;
            // 
            // ラベル132
            // 
            this.ラベル132.Height = 0.1919017F;
            this.ラベル132.HyperLink = null;
            this.ラベル132.Left = 2.756693F;
            this.ラベル132.Name = "ラベル132";
            this.ラベル132.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align:" +
    " right; text-decoration: none; ddo-char-set: 1";
            this.ラベル132.Tag = "";
            this.ラベル132.Text = "本数";
            this.ラベル132.Top = 0.5307087F;
            this.ラベル132.Width = 0.5729167F;
            // 
            // ラベル133
            // 
            this.ラベル133.Height = 0.1919017F;
            this.ラベル133.HyperLink = null;
            this.ラベル133.Left = 3.498819F;
            this.ラベル133.Name = "ラベル133";
            this.ラベル133.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル133.Tag = "";
            this.ラベル133.Text = "水揚数量";
            this.ラベル133.Top = 0.5307087F;
            this.ラベル133.Width = 0.749976F;
            // 
            // ラベル134
            // 
            this.ラベル134.Height = 0.1919017F;
            this.ラベル134.HyperLink = null;
            this.ラベル134.Left = 4.637008F;
            this.ラベル134.Name = "ラベル134";
            this.ラベル134.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル134.Tag = "";
            this.ラベル134.Text = "水揚金額";
            this.ラベル134.Top = 0.5307087F;
            this.ラベル134.Width = 0.8125098F;
            // 
            // ラベル135
            // 
            this.ラベル135.Height = 0.1919017F;
            this.ラベル135.HyperLink = null;
            this.ラベル135.Left = 5.862205F;
            this.ラベル135.Name = "ラベル135";
            this.ラベル135.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル135.Tag = "";
            this.ラベル135.Text = "高値";
            this.ラベル135.Top = 0.5307087F;
            this.ラベル135.Width = 0.5729167F;
            // 
            // ラベル136
            // 
            this.ラベル136.Height = 0.1919017F;
            this.ラベル136.HyperLink = null;
            this.ラベル136.Left = 6.947639F;
            this.ラベル136.Name = "ラベル136";
            this.ラベル136.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル136.Tag = "";
            this.ラベル136.Text = "中値";
            this.ラベル136.Top = 0.5307087F;
            this.ラベル136.Width = 0.5520833F;
            // 
            // ラベル137
            // 
            this.ラベル137.Height = 0.1919017F;
            this.ラベル137.HyperLink = null;
            this.ラベル137.Left = 8.020472F;
            this.ラベル137.Name = "ラベル137";
            this.ラベル137.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル137.Tag = "";
            this.ラベル137.Text = "安値";
            this.ラベル137.Top = 0.5322835F;
            this.ラベル137.Width = 0.5520833F;
            // 
            // date
            // 
            this.date.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.date.Height = 0.1561351F;
            this.date.Left = 6.963387F;
            this.date.Name = "date";
            this.date.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 128";
            this.date.Top = 0.1771654F;
            this.date.Width = 1.073048F;
            // 
            // page
            // 
            this.page.Height = 0.1545603F;
            this.page.Left = 7.866929F;
            this.page.Name = "page";
            this.page.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.page.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.page.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page.Text = "page";
            this.page.Top = 0.1787402F;
            this.page.Width = 0.4850378F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM11";
            this.textBox9.Height = 0.1915572F;
            this.textBox9.Left = 0.9440945F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold";
            this.textBox9.Text = "ITEM11";
            this.textBox9.Top = 0.5326772F;
            this.textBox9.Width = 1.239485F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM12";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Height = 0F;
            this.detail.Name = "detail";
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM02";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.line1,
            this.label1});
            this.groupFooter1.Height = 0.2501313F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM05";
            this.textBox11.Height = 0.1666667F;
            this.textBox11.Left = 2.705906F;
            this.textBox11.MultiLine = false;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox11.SummaryGroup = "groupHeader1";
            this.textBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox11.Tag = "";
            this.textBox11.Text = "ITEM05";
            this.textBox11.Top = 0.08346463F;
            this.textBox11.Width = 0.623622F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM06";
            this.textBox12.Height = 0.1666667F;
            this.textBox12.Left = 3.625174F;
            this.textBox12.MultiLine = false;
            this.textBox12.Name = "textBox12";
            this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
            this.textBox12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox12.SummaryGroup = "groupHeader1";
            this.textBox12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox12.Tag = "";
            this.textBox12.Text = "ITEM06";
            this.textBox12.Top = 0.07322841F;
            this.textBox12.Width = 0.623622F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM07";
            this.textBox13.Height = 0.1666667F;
            this.textBox13.Left = 4.315355F;
            this.textBox13.MultiLine = false;
            this.textBox13.Name = "textBox13";
            this.textBox13.OutputFormat = resources.GetString("textBox13.OutputFormat");
            this.textBox13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox13.SummaryGroup = "groupHeader1";
            this.textBox13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox13.Tag = "";
            this.textBox13.Text = "9,999,999,999";
            this.textBox13.Top = 0.08346457F;
            this.textBox13.Width = 1.134164F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM08";
            this.textBox14.Height = 0.1666667F;
            this.textBox14.Left = 5.603232F;
            this.textBox14.MultiLine = false;
            this.textBox14.Name = "textBox14";
            this.textBox14.OutputFormat = resources.GetString("textBox14.OutputFormat");
            this.textBox14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox14.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Max;
            this.textBox14.SummaryGroup = "groupHeader1";
            this.textBox14.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox14.Tag = "";
            this.textBox14.Text = "ITEM08";
            this.textBox14.Top = 0.08346457F;
            this.textBox14.Width = 0.8318896F;
            // 
            // textBox15
            // 
            this.textBox15.Height = 0.1666667F;
            this.textBox15.Left = 6.667833F;
            this.textBox15.MultiLine = false;
            this.textBox15.Name = "textBox15";
            this.textBox15.OutputFormat = resources.GetString("textBox15.OutputFormat");
            this.textBox15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox15.SummaryGroup = "groupHeader1";
            this.textBox15.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox15.Tag = "";
            this.textBox15.Text = "ITEM09";
            this.textBox15.Top = 0.07322835F;
            this.textBox15.Width = 0.8318896F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM10";
            this.textBox16.Height = 0.1666667F;
            this.textBox16.Left = 7.740667F;
            this.textBox16.MultiLine = false;
            this.textBox16.Name = "textBox16";
            this.textBox16.OutputFormat = resources.GetString("textBox16.OutputFormat");
            this.textBox16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox16.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Min;
            this.textBox16.SummaryGroup = "groupHeader1";
            this.textBox16.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox16.Tag = "";
            this.textBox16.Text = "ITEM10";
            this.textBox16.Top = 0.08346457F;
            this.textBox16.Width = 0.8318896F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.1748032F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 5.960464E-08F;
            this.line1.Width = 8.854545F;
            this.line1.X1 = 0.1748032F;
            this.line1.X2 = 9.029347F;
            this.line1.Y1 = 5.960464E-08F;
            this.line1.Y2 = 5.960464E-08F;
            // 
            // label1
            // 
            this.label1.Height = 0.1666667F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.9440945F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 12pt; ddo-char-set: 1";
            this.label1.Text = "合計";
            this.label1.Top = 0.07322841F;
            this.label1.Width = 0.6041667F;
            // 
            // groupHeader2
            // 
            this.groupHeader2.DataField = "ITEM03";
            this.groupHeader2.Height = 0F;
            this.groupHeader2.Name = "groupHeader2";
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8});
            this.groupFooter2.Height = 0.2709974F;
            this.groupFooter2.Name = "groupFooter2";
            this.groupFooter2.Format += new System.EventHandler(this.groupFooter2_Format);
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM03";
            this.textBox1.Height = 0.1666667F;
            this.textBox1.Left = 0.2141732F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM03";
            this.textBox1.Top = 5.960464E-08F;
            this.textBox1.Width = 0.6236221F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM04";
            this.textBox2.Height = 0.1666667F;
            this.textBox2.Left = 0.9440945F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM04";
            this.textBox2.Top = 0.01023628F;
            this.textBox2.Width = 2.385433F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM05";
            this.textBox3.Height = 0.1666667F;
            this.textBox3.Left = 2.705906F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox3.SummaryGroup = "groupHeader2";
            this.textBox3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM05";
            this.textBox3.Top = 0.01023628F;
            this.textBox3.Width = 0.623622F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM06";
            this.textBox4.Height = 0.1666667F;
            this.textBox4.Left = 3.625174F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox4.SummaryGroup = "groupHeader2";
            this.textBox4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM06";
            this.textBox4.Top = 5.960464E-08F;
            this.textBox4.Width = 0.623622F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM07";
            this.textBox5.Height = 0.1666667F;
            this.textBox5.Left = 4.315355F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox5.SummaryGroup = "groupHeader2";
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox5.Tag = "";
            this.textBox5.Text = "999,999,999";
            this.textBox5.Top = 0.01023622F;
            this.textBox5.Width = 1.134164F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM08";
            this.textBox6.Height = 0.1666667F;
            this.textBox6.Left = 5.603232F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox6.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Max;
            this.textBox6.SummaryGroup = "groupHeader2";
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM08";
            this.textBox6.Top = 0.01023622F;
            this.textBox6.Width = 0.8318896F;
            // 
            // textBox7
            // 
            this.textBox7.Height = 0.1666667F;
            this.textBox7.Left = 6.667833F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox7.SummaryGroup = "groupHeader2";
            this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM09";
            this.textBox7.Top = 0.01023622F;
            this.textBox7.Width = 0.8318896F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM10";
            this.textBox8.Height = 0.1666667F;
            this.textBox8.Left = 7.740667F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox8.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Min;
            this.textBox8.SummaryGroup = "groupHeader2";
            this.textBox8.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox8.Tag = "";
            this.textBox8.Text = "ITEM10";
            this.textBox8.Top = 0F;
            this.textBox8.Width = 0.8318896F;
            // 
            // HNDR1031R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937007F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 9.02953F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM02;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル25;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト110;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル130;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル133;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル134;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル135;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル136;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル137;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo date;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox page;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル132;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
    }
}
