﻿namespace jp.co.fsi.hn.hndr1031
{
    partial class HNDR1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblDateGengoFr = new System.Windows.Forms.Label();
			this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.labelDateYearFr = new System.Windows.Forms.Label();
			this.lblDateMonthFr = new System.Windows.Forms.Label();
			this.lblDateDayFr = new System.Windows.Forms.Label();
			this.lblDateDayTo = new System.Windows.Forms.Label();
			this.lblDateMonthTo = new System.Windows.Forms.Label();
			this.lblDateBet = new System.Windows.Forms.Label();
			this.labelDateYearTo = new System.Windows.Forms.Label();
			this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoTo = new System.Windows.Forms.Label();
			this.rdoGyoshuBunrui = new System.Windows.Forms.RadioButton();
			this.rdoGyoshu = new System.Windows.Forms.RadioButton();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.lblSeisanKbnNm = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(9, 812);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblDateGengoFr
			// 
			this.lblDateGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoFr.Location = new System.Drawing.Point(97, 2);
			this.lblDateGengoFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengoFr.Name = "lblDateGengoFr";
			this.lblDateGengoFr.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengoFr.TabIndex = 1;
			this.lblDateGengoFr.Tag = "DISPNAME";
			this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblDateGengoFr.Click += new System.EventHandler(this.lblDateGengoFr_Click);
			// 
			// txtDateMonthFr
			// 
			this.txtDateMonthFr.AutoSizeFromLength = false;
			this.txtDateMonthFr.DisplayLength = null;
			this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthFr.Location = new System.Drawing.Point(237, 3);
			this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateMonthFr.MaxLength = 2;
			this.txtDateMonthFr.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtDateMonthFr.Name = "txtDateMonthFr";
			this.txtDateMonthFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthFr.TabIndex = 3;
			this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
			// 
			// txtDateYearFr
			// 
			this.txtDateYearFr.AutoSizeFromLength = false;
			this.txtDateYearFr.DisplayLength = null;
			this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearFr.Location = new System.Drawing.Point(157, 3);
			this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYearFr.MaxLength = 2;
			this.txtDateYearFr.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtDateYearFr.Name = "txtDateYearFr";
			this.txtDateYearFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearFr.TabIndex = 2;
			this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
			// 
			// txtDateDayFr
			// 
			this.txtDateDayFr.AutoSizeFromLength = false;
			this.txtDateDayFr.DisplayLength = null;
			this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateDayFr.Location = new System.Drawing.Point(324, 3);
			this.txtDateDayFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateDayFr.MaxLength = 2;
			this.txtDateDayFr.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtDateDayFr.Name = "txtDateDayFr";
			this.txtDateDayFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateDayFr.TabIndex = 4;
			this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
			// 
			// labelDateYearFr
			// 
			this.labelDateYearFr.AutoSize = true;
			this.labelDateYearFr.BackColor = System.Drawing.Color.Silver;
			this.labelDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.labelDateYearFr.Location = new System.Drawing.Point(203, 7);
			this.labelDateYearFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.labelDateYearFr.Name = "labelDateYearFr";
			this.labelDateYearFr.Size = new System.Drawing.Size(24, 16);
			this.labelDateYearFr.TabIndex = 3;
			this.labelDateYearFr.Tag = "CHANGE";
			this.labelDateYearFr.Text = "年";
			this.labelDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateMonthFr
			// 
			this.lblDateMonthFr.AutoSize = true;
			this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateMonthFr.Location = new System.Drawing.Point(284, 7);
			this.lblDateMonthFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateMonthFr.Name = "lblDateMonthFr";
			this.lblDateMonthFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthFr.TabIndex = 5;
			this.lblDateMonthFr.Tag = "CHANGE";
			this.lblDateMonthFr.Text = "月";
			this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateDayFr
			// 
			this.lblDateDayFr.AutoSize = true;
			this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateDayFr.Location = new System.Drawing.Point(367, 7);
			this.lblDateDayFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateDayFr.Name = "lblDateDayFr";
			this.lblDateDayFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateDayFr.TabIndex = 7;
			this.lblDateDayFr.Tag = "CHANGE";
			this.lblDateDayFr.Text = "日";
			this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateDayTo
			// 
			this.lblDateDayTo.AutoSize = true;
			this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateDayTo.Location = new System.Drawing.Point(701, 7);
			this.lblDateDayTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateDayTo.Name = "lblDateDayTo";
			this.lblDateDayTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateDayTo.TabIndex = 16;
			this.lblDateDayTo.Tag = "CHANGE";
			this.lblDateDayTo.Text = "日";
			this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateMonthTo
			// 
			this.lblDateMonthTo.AutoSize = true;
			this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateMonthTo.Location = new System.Drawing.Point(619, 7);
			this.lblDateMonthTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateMonthTo.Name = "lblDateMonthTo";
			this.lblDateMonthTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthTo.TabIndex = 14;
			this.lblDateMonthTo.Tag = "CHANGE";
			this.lblDateMonthTo.Text = "月";
			this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateBet
			// 
			this.lblDateBet.AutoSize = true;
			this.lblDateBet.BackColor = System.Drawing.Color.Silver;
			this.lblDateBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateBet.Location = new System.Drawing.Point(401, 7);
			this.lblDateBet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateBet.Name = "lblDateBet";
			this.lblDateBet.Size = new System.Drawing.Size(24, 16);
			this.lblDateBet.TabIndex = 8;
			this.lblDateBet.Tag = "CHANGE";
			this.lblDateBet.Text = "～";
			this.lblDateBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelDateYearTo
			// 
			this.labelDateYearTo.AutoSize = true;
			this.labelDateYearTo.BackColor = System.Drawing.Color.Silver;
			this.labelDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.labelDateYearTo.Location = new System.Drawing.Point(540, 7);
			this.labelDateYearTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.labelDateYearTo.Name = "labelDateYearTo";
			this.labelDateYearTo.Size = new System.Drawing.Size(24, 16);
			this.labelDateYearTo.TabIndex = 12;
			this.labelDateYearTo.Tag = "CHANGE";
			this.labelDateYearTo.Text = "年";
			this.labelDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateDayTo
			// 
			this.txtDateDayTo.AutoSizeFromLength = false;
			this.txtDateDayTo.DisplayLength = null;
			this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateDayTo.Location = new System.Drawing.Point(659, 3);
			this.txtDateDayTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateDayTo.MaxLength = 2;
			this.txtDateDayTo.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtDateDayTo.Name = "txtDateDayTo";
			this.txtDateDayTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateDayTo.TabIndex = 7;
			this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
			// 
			// txtDateYearTo
			// 
			this.txtDateYearTo.AutoSizeFromLength = false;
			this.txtDateYearTo.DisplayLength = null;
			this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearTo.Location = new System.Drawing.Point(495, 3);
			this.txtDateYearTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYearTo.MaxLength = 2;
			this.txtDateYearTo.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtDateYearTo.Name = "txtDateYearTo";
			this.txtDateYearTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearTo.TabIndex = 5;
			this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
			// 
			// txtDateMonthTo
			// 
			this.txtDateMonthTo.AutoSizeFromLength = false;
			this.txtDateMonthTo.DisplayLength = null;
			this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthTo.Location = new System.Drawing.Point(573, 3);
			this.txtDateMonthTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateMonthTo.MaxLength = 2;
			this.txtDateMonthTo.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtDateMonthTo.Name = "txtDateMonthTo";
			this.txtDateMonthTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthTo.TabIndex = 6;
			this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
			// 
			// lblDateGengoTo
			// 
			this.lblDateGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoTo.Location = new System.Drawing.Point(436, 2);
			this.lblDateGengoTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengoTo.Name = "lblDateGengoTo";
			this.lblDateGengoTo.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengoTo.TabIndex = 10;
			this.lblDateGengoTo.Tag = "DISPNAME";
			this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// rdoGyoshuBunrui
			// 
			this.rdoGyoshuBunrui.AutoSize = true;
			this.rdoGyoshuBunrui.BackColor = System.Drawing.Color.Silver;
			this.rdoGyoshuBunrui.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoGyoshuBunrui.Location = new System.Drawing.Point(188, 6);
			this.rdoGyoshuBunrui.Margin = new System.Windows.Forms.Padding(5);
			this.rdoGyoshuBunrui.Name = "rdoGyoshuBunrui";
			this.rdoGyoshuBunrui.Size = new System.Drawing.Size(90, 20);
			this.rdoGyoshuBunrui.TabIndex = 1;
			this.rdoGyoshuBunrui.TabStop = true;
			this.rdoGyoshuBunrui.Tag = "CHANGE";
			this.rdoGyoshuBunrui.Text = "魚種分類";
			this.rdoGyoshuBunrui.UseVisualStyleBackColor = false;
			// 
			// rdoGyoshu
			// 
			this.rdoGyoshu.AutoSize = true;
			this.rdoGyoshu.BackColor = System.Drawing.Color.Silver;
			this.rdoGyoshu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoGyoshu.Location = new System.Drawing.Point(97, 6);
			this.rdoGyoshu.Margin = new System.Windows.Forms.Padding(5);
			this.rdoGyoshu.Name = "rdoGyoshu";
			this.rdoGyoshu.Size = new System.Drawing.Size(58, 20);
			this.rdoGyoshu.TabIndex = 0;
			this.rdoGyoshu.TabStop = true;
			this.rdoGyoshu.Tag = "CHANGE";
			this.rdoGyoshu.Text = "魚種";
			this.rdoGyoshu.UseVisualStyleBackColor = false;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(99, 4);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(144, 3);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanKbn
			// 
			this.txtSeisanKbn.AutoSizeFromLength = true;
			this.txtSeisanKbn.DisplayLength = null;
			this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSeisanKbn.Location = new System.Drawing.Point(99, 5);
			this.txtSeisanKbn.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeisanKbn.MaxLength = 5;
			this.txtSeisanKbn.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtSeisanKbn.Name = "txtSeisanKbn";
			this.txtSeisanKbn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtSeisanKbn.Size = new System.Drawing.Size(67, 23);
			this.txtSeisanKbn.TabIndex = 8;
			this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSeisanKbn_KeyDown);
			this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
			// 
			// label11
			// 
			this.label11.BackColor = System.Drawing.Color.Silver;
			this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label11.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label11.Location = new System.Drawing.Point(0, 0);
			this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(798, 32);
			this.label11.TabIndex = 6;
			this.label11.Tag = "CHANGE";
			this.label11.Text = "表示";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label12
			// 
			this.label12.BackColor = System.Drawing.Color.Silver;
			this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label12.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label12.Location = new System.Drawing.Point(0, 0);
			this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(798, 32);
			this.label12.TabIndex = 6;
			this.label12.Tag = "CHANGE";
			this.label12.Text = "水揚支所";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label13
			// 
			this.label13.BackColor = System.Drawing.Color.Silver;
			this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label13.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label13.Location = new System.Drawing.Point(0, 0);
			this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(798, 32);
			this.label13.TabIndex = 6;
			this.label13.Tag = "CHANGE";
			this.label13.Text = "セリ日付";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label14
			// 
			this.label14.BackColor = System.Drawing.Color.Silver;
			this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label14.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label14.Location = new System.Drawing.Point(0, 0);
			this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(798, 35);
			this.label14.TabIndex = 6;
			this.label14.Tag = "CHANGE";
			this.label14.Text = "精算区分";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 4;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(808, 168);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lblSeisanKbnNm);
			this.fsiPanel4.Controls.Add(this.txtSeisanKbn);
			this.fsiPanel4.Controls.Add(this.label14);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.fsiPanel4.Location = new System.Drawing.Point(5, 128);
			this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(798, 35);
			this.fsiPanel4.TabIndex = 3;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblDateGengoFr);
			this.fsiPanel3.Controls.Add(this.lblDateDayTo);
			this.fsiPanel3.Controls.Add(this.txtDateMonthFr);
			this.fsiPanel3.Controls.Add(this.txtDateYearFr);
			this.fsiPanel3.Controls.Add(this.lblDateMonthTo);
			this.fsiPanel3.Controls.Add(this.txtDateDayFr);
			this.fsiPanel3.Controls.Add(this.lblDateDayFr);
			this.fsiPanel3.Controls.Add(this.labelDateYearFr);
			this.fsiPanel3.Controls.Add(this.labelDateYearTo);
			this.fsiPanel3.Controls.Add(this.lblDateMonthFr);
			this.fsiPanel3.Controls.Add(this.txtDateDayTo);
			this.fsiPanel3.Controls.Add(this.lblDateGengoTo);
			this.fsiPanel3.Controls.Add(this.txtDateYearTo);
			this.fsiPanel3.Controls.Add(this.txtDateMonthTo);
			this.fsiPanel3.Controls.Add(this.lblDateBet);
			this.fsiPanel3.Controls.Add(this.label13);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.fsiPanel3.Location = new System.Drawing.Point(5, 87);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(798, 32);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.rdoGyoshu);
			this.fsiPanel2.Controls.Add(this.rdoGyoshuBunrui);
			this.fsiPanel2.Controls.Add(this.label11);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.fsiPanel2.Location = new System.Drawing.Point(5, 46);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(798, 32);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.label12);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(798, 32);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// lblSeisanKbnNm
			// 
			this.lblSeisanKbnNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanKbnNm.Location = new System.Drawing.Point(172, 4);
			this.lblSeisanKbnNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
			this.lblSeisanKbnNm.Size = new System.Drawing.Size(283, 24);
			this.lblSeisanKbnNm.TabIndex = 9;
			this.lblSeisanKbnNm.Tag = "DISPNAME";
			this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// HNDR1031
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNDR1031";
			this.Text = "";
			this.Load += new System.EventHandler(this.HNDR1031_Load);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.Label lblDateGengoFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayFr;
        private System.Windows.Forms.Label labelDateYearFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label labelDateYearTo;
        private common.controls.FsiTextBox txtDateDayTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateBet;
        private System.Windows.Forms.RadioButton rdoGyoshuBunrui;
        private System.Windows.Forms.RadioButton rdoGyoshu;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
		private System.Windows.Forms.Label lblSeisanKbnNm;
	}
}
