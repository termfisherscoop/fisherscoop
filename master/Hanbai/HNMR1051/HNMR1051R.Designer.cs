﻿namespace jp.co.fsi.hn.hnmr1051
{
    /// <summary>
    /// HNMR1051R の概要の説明です。
    /// </summary>
    partial class HNMR1051R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNMR1051R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtFromYearMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblWave = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToYearMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPrintDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptPayaoNO = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoshuNM = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptMizuageSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptHeikinTanka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptMizuageKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeaderUpper = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lnHeaderUnder = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtGyoshuNM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMizuageSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeikinTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMizuageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoshuBunruiCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblGTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGMizuageSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGHeikinTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGMizuageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghMPayao = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtPayaoNO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfMPayao = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblPayaoTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMMizuageSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMHeikinTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMMizuageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghSGyoshuBunrui = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.gfSGyoshuBunrui = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblFutougou1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGyoshuBunruiNM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSMizuageSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSHeikinTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSMizuageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromYearMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToYearMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptPayaoNO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoshuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptMizuageSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptHeikinTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptMizuageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeikinTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuBunruiCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMizuageSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHeikinTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMizuageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayaoNO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPayaoTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMizuageSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMHeikinTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMizuageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFutougou1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuBunruiNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSMizuageSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSHeikinTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSMizuageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFromYearMonth,
            this.lblWave,
            this.txtToYearMonth,
            this.txtPageCount,
            this.txtPrintDate,
            this.lblPage,
            this.lblTitle,
            this.lblCptPayaoNO,
            this.lblCptGyoshuNM,
            this.lblCptMizuageSuryo,
            this.lblCptHeikinTanka,
            this.lblCptMizuageKingaku,
            this.lnHeaderUpper,
            this.lnHeaderUnder});
            this.pageHeader.Height = 0.808235F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtFromYearMonth
            // 
            this.txtFromYearMonth.CanGrow = false;
            this.txtFromYearMonth.DataField = "ITEM09";
            this.txtFromYearMonth.Height = 0.1582677F;
            this.txtFromYearMonth.Left = 0.0937008F;
            this.txtFromYearMonth.MultiLine = false;
            this.txtFromYearMonth.Name = "txtFromYearMonth";
            this.txtFromYearMonth.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtFromYearMonth.Text = "平成88年12月";
            this.txtFromYearMonth.Top = 0F;
            this.txtFromYearMonth.Width = 0.8956694F;
            // 
            // lblWave
            // 
            this.lblWave.Height = 0.1582677F;
            this.lblWave.HyperLink = null;
            this.lblWave.Left = 0.9893702F;
            this.lblWave.Name = "lblWave";
            this.lblWave.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: center; vertical-align: middle";
            this.lblWave.Text = "～";
            this.lblWave.Top = 0F;
            this.lblWave.Width = 0.2708662F;
            // 
            // txtToYearMonth
            // 
            this.txtToYearMonth.CanGrow = false;
            this.txtToYearMonth.DataField = "ITEM10";
            this.txtToYearMonth.Height = 0.1582677F;
            this.txtToYearMonth.Left = 1.260236F;
            this.txtToYearMonth.MultiLine = false;
            this.txtToYearMonth.Name = "txtToYearMonth";
            this.txtToYearMonth.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtToYearMonth.Text = "平成88年12月";
            this.txtToYearMonth.Top = 0F;
            this.txtToYearMonth.Width = 0.8956689F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.CanGrow = false;
            this.txtPageCount.Height = 0.1582677F;
            this.txtPageCount.Left = 7.916536F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "888";
            this.txtPageCount.Top = 0F;
            this.txtPageCount.Width = 0.3125978F;
            // 
            // txtPrintDate
            // 
            this.txtPrintDate.CanGrow = false;
            this.txtPrintDate.DataField = "ITEM11";
            this.txtPrintDate.Height = 0.1582677F;
            this.txtPrintDate.Left = 6.751969F;
            this.txtPrintDate.MultiLine = false;
            this.txtPrintDate.Name = "txtPrintDate";
            this.txtPrintDate.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtPrintDate.Text = "平成88年12月30日";
            this.txtPrintDate.Top = 0F;
            this.txtPrintDate.Width = 1.164567F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1582677F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 8.229136F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0F;
            this.lblPage.Width = 0.1562996F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.3062992F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 2.803544F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold; text-align: center; text-" +
    "decoration: underline";
            this.lblTitle.Text = "パヤオ別魚種別分類別水揚月報";
            this.lblTitle.Top = 0F;
            this.lblTitle.Width = 3.078347F;
            // 
            // lblCptPayaoNO
            // 
            this.lblCptPayaoNO.Height = 0.1582677F;
            this.lblCptPayaoNO.HyperLink = null;
            this.lblCptPayaoNO.Left = 0.0937008F;
            this.lblCptPayaoNO.Name = "lblCptPayaoNO";
            this.lblCptPayaoNO.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptPayaoNO.Text = "パヤオ番号";
            this.lblCptPayaoNO.Top = 0.6259843F;
            this.lblCptPayaoNO.Width = 0.7291338F;
            // 
            // lblCptGyoshuNM
            // 
            this.lblCptGyoshuNM.Height = 0.1582677F;
            this.lblCptGyoshuNM.HyperLink = null;
            this.lblCptGyoshuNM.Left = 1.023622F;
            this.lblCptGyoshuNM.Name = "lblCptGyoshuNM";
            this.lblCptGyoshuNM.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; vertical-align: middle";
            this.lblCptGyoshuNM.Text = "魚種名";
            this.lblCptGyoshuNM.Top = 0.6259843F;
            this.lblCptGyoshuNM.Width = 0.4850394F;
            // 
            // lblCptMizuageSuryo
            // 
            this.lblCptMizuageSuryo.Height = 0.1582677F;
            this.lblCptMizuageSuryo.HyperLink = null;
            this.lblCptMizuageSuryo.Left = 4.883071F;
            this.lblCptMizuageSuryo.Name = "lblCptMizuageSuryo";
            this.lblCptMizuageSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptMizuageSuryo.Text = "水揚数量(Kg)";
            this.lblCptMizuageSuryo.Top = 0.6259843F;
            this.lblCptMizuageSuryo.Width = 0.9165359F;
            // 
            // lblCptHeikinTanka
            // 
            this.lblCptHeikinTanka.Height = 0.1582677F;
            this.lblCptHeikinTanka.HyperLink = null;
            this.lblCptHeikinTanka.Left = 6.038189F;
            this.lblCptHeikinTanka.Name = "lblCptHeikinTanka";
            this.lblCptHeikinTanka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptHeikinTanka.Text = "平均単価";
            this.lblCptHeikinTanka.Top = 0.6259843F;
            this.lblCptHeikinTanka.Width = 0.9165361F;
            // 
            // lblCptMizuageKingaku
            // 
            this.lblCptMizuageKingaku.Height = 0.1582677F;
            this.lblCptMizuageKingaku.HyperLink = null;
            this.lblCptMizuageKingaku.Left = 7.468898F;
            this.lblCptMizuageKingaku.Name = "lblCptMizuageKingaku";
            this.lblCptMizuageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptMizuageKingaku.Text = "水揚金額";
            this.lblCptMizuageKingaku.Top = 0.6259843F;
            this.lblCptMizuageKingaku.Width = 0.9165361F;
            // 
            // lnHeaderUpper
            // 
            this.lnHeaderUpper.Height = 0F;
            this.lnHeaderUpper.Left = 0.0937008F;
            this.lnHeaderUpper.LineWeight = 2F;
            this.lnHeaderUpper.Name = "lnHeaderUpper";
            this.lnHeaderUpper.Top = 0.5972441F;
            this.lnHeaderUpper.Width = 8.489765F;
            this.lnHeaderUpper.X1 = 0.0937008F;
            this.lnHeaderUpper.X2 = 8.583466F;
            this.lnHeaderUpper.Y1 = 0.5972441F;
            this.lnHeaderUpper.Y2 = 0.5972441F;
            // 
            // lnHeaderUnder
            // 
            this.lnHeaderUnder.Height = 0F;
            this.lnHeaderUnder.Left = 0.0937008F;
            this.lnHeaderUnder.LineWeight = 2F;
            this.lnHeaderUnder.Name = "lnHeaderUnder";
            this.lnHeaderUnder.Top = 0.7874017F;
            this.lnHeaderUnder.Width = 8.489764F;
            this.lnHeaderUnder.X1 = 0.0937008F;
            this.lnHeaderUnder.X2 = 8.583465F;
            this.lnHeaderUnder.Y1 = 0.7874017F;
            this.lnHeaderUnder.Y2 = 0.7874017F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM12";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGyoshuNM,
            this.txtMizuageSuryo,
            this.txtHeikinTanka,
            this.txtMizuageKingaku,
            this.txtGyoshuBunruiCD});
            this.detail.Height = 0.1968504F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            // 
            // txtGyoshuNM
            // 
            this.txtGyoshuNM.CanGrow = false;
            this.txtGyoshuNM.DataField = "ITEM05";
            this.txtGyoshuNM.Height = 0.1582677F;
            this.txtGyoshuNM.Left = 1.023622F;
            this.txtGyoshuNM.MultiLine = false;
            this.txtGyoshuNM.Name = "txtGyoshuNM";
            this.txtGyoshuNM.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtGyoshuNM.Text = "ＮＮＮＮＮＮＮＮ";
            this.txtGyoshuNM.Top = 0F;
            this.txtGyoshuNM.Width = 3.501969F;
            // 
            // txtMizuageSuryo
            // 
            this.txtMizuageSuryo.CanGrow = false;
            this.txtMizuageSuryo.DataField = "ITEM06";
            this.txtMizuageSuryo.Height = 0.1582677F;
            this.txtMizuageSuryo.Left = 4.77874F;
            this.txtMizuageSuryo.Name = "txtMizuageSuryo";
            this.txtMizuageSuryo.OutputFormat = resources.GetString("txtMizuageSuryo.OutputFormat");
            this.txtMizuageSuryo.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtMizuageSuryo.Text = "1234567890.8";
            this.txtMizuageSuryo.Top = 0F;
            this.txtMizuageSuryo.Width = 1.020866F;
            // 
            // txtHeikinTanka
            // 
            this.txtHeikinTanka.CanGrow = false;
            this.txtHeikinTanka.DataField = "Amount";
            this.txtHeikinTanka.Height = 0.1582677F;
            this.txtHeikinTanka.Left = 5.933859F;
            this.txtHeikinTanka.Name = "txtHeikinTanka";
            this.txtHeikinTanka.OutputFormat = resources.GetString("txtHeikinTanka.OutputFormat");
            this.txtHeikinTanka.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtHeikinTanka.Text = "ZZZ,ZZZ,ZZ9";
            this.txtHeikinTanka.Top = 0F;
            this.txtHeikinTanka.Width = 1.020867F;
            // 
            // txtMizuageKingaku
            // 
            this.txtMizuageKingaku.CanGrow = false;
            this.txtMizuageKingaku.DataField = "ITEM08";
            this.txtMizuageKingaku.Height = 0.1582677F;
            this.txtMizuageKingaku.Left = 7.208268F;
            this.txtMizuageKingaku.Name = "txtMizuageKingaku";
            this.txtMizuageKingaku.OutputFormat = resources.GetString("txtMizuageKingaku.OutputFormat");
            this.txtMizuageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtMizuageKingaku.Text = "-ZZZ,ZZZ,ZZZ,ZZ9";
            this.txtMizuageKingaku.Top = 0.008267725F;
            this.txtMizuageKingaku.Width = 1.177165F;
            // 
            // txtGyoshuBunruiCD
            // 
            this.txtGyoshuBunruiCD.CanGrow = false;
            this.txtGyoshuBunruiCD.DataField = "ITEM02";
            this.txtGyoshuBunruiCD.Height = 0.1582677F;
            this.txtGyoshuBunruiCD.Left = 0.3232284F;
            this.txtGyoshuBunruiCD.Name = "txtGyoshuBunruiCD";
            this.txtGyoshuBunruiCD.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoshuBunruiCD.Text = "999";
            this.txtGyoshuBunruiCD.Top = 0.008267717F;
            this.txtGyoshuBunruiCD.Visible = false;
            this.txtGyoshuBunruiCD.Width = 0.4996065F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblGTotal,
            this.txtGMizuageSuryo,
            this.txtGHeikinTanka,
            this.txtGMizuageKingaku,
            this.line2});
            this.reportFooter1.Height = 0.3229167F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // lblGTotal
            // 
            this.lblGTotal.Height = 0.1582677F;
            this.lblGTotal.HyperLink = null;
            this.lblGTotal.Left = 2.155906F;
            this.lblGTotal.Name = "lblGTotal";
            this.lblGTotal.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
            this.lblGTotal.Text = "合　　計";
            this.lblGTotal.Top = 0.07874016F;
            this.lblGTotal.Width = 0.6996062F;
            // 
            // txtGMizuageSuryo
            // 
            this.txtGMizuageSuryo.CanGrow = false;
            this.txtGMizuageSuryo.DataField = "ITEM06";
            this.txtGMizuageSuryo.Height = 0.1582677F;
            this.txtGMizuageSuryo.Left = 4.77874F;
            this.txtGMizuageSuryo.MultiLine = false;
            this.txtGMizuageSuryo.Name = "txtGMizuageSuryo";
            this.txtGMizuageSuryo.OutputFormat = resources.GetString("txtGMizuageSuryo.OutputFormat");
            this.txtGMizuageSuryo.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtGMizuageSuryo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGMizuageSuryo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGMizuageSuryo.Text = "1234567890.8";
            this.txtGMizuageSuryo.Top = 0.07874016F;
            this.txtGMizuageSuryo.Width = 1.020867F;
            // 
            // txtGHeikinTanka
            // 
            this.txtGHeikinTanka.CanGrow = false;
            this.txtGHeikinTanka.DataField = "ITEM07";
            this.txtGHeikinTanka.Height = 0.1582677F;
            this.txtGHeikinTanka.Left = 5.933859F;
            this.txtGHeikinTanka.MultiLine = false;
            this.txtGHeikinTanka.Name = "txtGHeikinTanka";
            this.txtGHeikinTanka.OutputFormat = resources.GetString("txtGHeikinTanka.OutputFormat");
            this.txtGHeikinTanka.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtGHeikinTanka.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGHeikinTanka.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGHeikinTanka.Top = 0.07874016F;
            this.txtGHeikinTanka.Width = 1.020867F;
            // 
            // txtGMizuageKingaku
            // 
            this.txtGMizuageKingaku.CanGrow = false;
            this.txtGMizuageKingaku.DataField = "ITEM08";
            this.txtGMizuageKingaku.Height = 0.1582677F;
            this.txtGMizuageKingaku.Left = 7.208268F;
            this.txtGMizuageKingaku.MultiLine = false;
            this.txtGMizuageKingaku.Name = "txtGMizuageKingaku";
            this.txtGMizuageKingaku.OutputFormat = resources.GetString("txtGMizuageKingaku.OutputFormat");
            this.txtGMizuageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtGMizuageKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGMizuageKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGMizuageKingaku.Text = "-ZZZ,ZZZ,ZZZ,ZZ9";
            this.txtGMizuageKingaku.Top = 0.07874016F;
            this.txtGMizuageKingaku.Width = 1.177165F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.0937008F;
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Top = 0F;
            this.line2.Width = 8.489764F;
            this.line2.X1 = 0.0937008F;
            this.line2.X2 = 8.583465F;
            this.line2.Y1 = 0F;
            this.line2.Y2 = 0F;
            // 
            // ghMPayao
            // 
            this.ghMPayao.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtPayaoNO});
            this.ghMPayao.DataField = "ITEM01";
            this.ghMPayao.Height = 0.1770833F;
            this.ghMPayao.Name = "ghMPayao";
            // 
            // txtPayaoNO
            // 
            this.txtPayaoNO.CanGrow = false;
            this.txtPayaoNO.DataField = "ITEM01";
            this.txtPayaoNO.Height = 0.1582677F;
            this.txtPayaoNO.Left = 0.1874016F;
            this.txtPayaoNO.MultiLine = false;
            this.txtPayaoNO.Name = "txtPayaoNO";
            this.txtPayaoNO.Style = "font-family: ＭＳ 明朝; font-size: 11pt; text-align: right; vertical-align: middle";
            this.txtPayaoNO.Text = "999";
            this.txtPayaoNO.Top = 0F;
            this.txtPayaoNO.Width = 0.6354333F;
            // 
            // gfMPayao
            // 
            this.gfMPayao.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line1,
            this.lblPayaoTotal,
            this.txtMMizuageSuryo,
            this.txtMHeikinTanka,
            this.txtMMizuageKingaku});
            this.gfMPayao.Height = 0.28125F;
            this.gfMPayao.Name = "gfMPayao";
            // 
            // line1
            // 
            this.line1.Height = 7.450581E-09F;
            this.line1.Left = 0.0937008F;
            this.line1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0F;
            this.line1.Width = 8.489764F;
            this.line1.X1 = 0.0937008F;
            this.line1.X2 = 8.583465F;
            this.line1.Y1 = 0F;
            this.line1.Y2 = 7.450581E-09F;
            // 
            // lblPayaoTotal
            // 
            this.lblPayaoTotal.Height = 0.1582677F;
            this.lblPayaoTotal.HyperLink = null;
            this.lblPayaoTotal.Left = 2.155906F;
            this.lblPayaoTotal.Name = "lblPayaoTotal";
            this.lblPayaoTotal.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
            this.lblPayaoTotal.Text = "パヤオ計";
            this.lblPayaoTotal.Top = 0.06062993F;
            this.lblPayaoTotal.Width = 0.6996062F;
            // 
            // txtMMizuageSuryo
            // 
            this.txtMMizuageSuryo.CanGrow = false;
            this.txtMMizuageSuryo.DataField = "ITEM06";
            this.txtMMizuageSuryo.Height = 0.1582677F;
            this.txtMMizuageSuryo.Left = 4.77874F;
            this.txtMMizuageSuryo.MultiLine = false;
            this.txtMMizuageSuryo.Name = "txtMMizuageSuryo";
            this.txtMMizuageSuryo.OutputFormat = resources.GetString("txtMMizuageSuryo.OutputFormat");
            this.txtMMizuageSuryo.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtMMizuageSuryo.SummaryGroup = "ghMPayao";
            this.txtMMizuageSuryo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtMMizuageSuryo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtMMizuageSuryo.Text = "1234567890.8";
            this.txtMMizuageSuryo.Top = 0.06062993F;
            this.txtMMizuageSuryo.Width = 1.020867F;
            // 
            // txtMHeikinTanka
            // 
            this.txtMHeikinTanka.CanGrow = false;
            this.txtMHeikinTanka.DataField = "ITEM07";
            this.txtMHeikinTanka.Height = 0.1582677F;
            this.txtMHeikinTanka.Left = 5.933859F;
            this.txtMHeikinTanka.MultiLine = false;
            this.txtMHeikinTanka.Name = "txtMHeikinTanka";
            this.txtMHeikinTanka.OutputFormat = resources.GetString("txtMHeikinTanka.OutputFormat");
            this.txtMHeikinTanka.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtMHeikinTanka.SummaryGroup = "ghMPayao";
            this.txtMHeikinTanka.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtMHeikinTanka.Text = "ZZZ,ZZZ,ZZ9";
            this.txtMHeikinTanka.Top = 0.06062993F;
            this.txtMHeikinTanka.Width = 1.020867F;
            // 
            // txtMMizuageKingaku
            // 
            this.txtMMizuageKingaku.CanGrow = false;
            this.txtMMizuageKingaku.DataField = "ITEM08";
            this.txtMMizuageKingaku.Height = 0.1582677F;
            this.txtMMizuageKingaku.Left = 7.208268F;
            this.txtMMizuageKingaku.MultiLine = false;
            this.txtMMizuageKingaku.Name = "txtMMizuageKingaku";
            this.txtMMizuageKingaku.OutputFormat = resources.GetString("txtMMizuageKingaku.OutputFormat");
            this.txtMMizuageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtMMizuageKingaku.SummaryGroup = "ghMPayao";
            this.txtMMizuageKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtMMizuageKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtMMizuageKingaku.Text = "-ZZZ,ZZZ,ZZZ,ZZ9";
            this.txtMMizuageKingaku.Top = 0.06062994F;
            this.txtMMizuageKingaku.Width = 1.177165F;
            // 
            // ghSGyoshuBunrui
            // 
            this.ghSGyoshuBunrui.DataField = "ITEM02";
            this.ghSGyoshuBunrui.Height = 0F;
            this.ghSGyoshuBunrui.Name = "ghSGyoshuBunrui";
            // 
            // gfSGyoshuBunrui
            // 
            this.gfSGyoshuBunrui.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblFutougou1,
            this.txtGyoshuBunruiNM,
            this.txtSMizuageSuryo,
            this.txtSHeikinTanka,
            this.txtSMizuageKingaku});
            this.gfSGyoshuBunrui.Height = 0.2395833F;
            this.gfSGyoshuBunrui.Name = "gfSGyoshuBunrui";
            // 
            // lblFutougou1
            // 
            this.lblFutougou1.Height = 0.1582677F;
            this.lblFutougou1.HyperLink = null;
            this.lblFutougou1.Left = 2.466929F;
            this.lblFutougou1.Name = "lblFutougou1";
            this.lblFutougou1.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: top";
            this.lblFutougou1.Text = "＜                  計＞";
            this.lblFutougou1.Top = 0F;
            this.lblFutougou1.Width = 1.944095F;
            // 
            // txtGyoshuBunruiNM
            // 
            this.txtGyoshuBunruiNM.CanGrow = false;
            this.txtGyoshuBunruiNM.DataField = "ITEM03";
            this.txtGyoshuBunruiNM.Height = 0.1582677F;
            this.txtGyoshuBunruiNM.Left = 2.67874F;
            this.txtGyoshuBunruiNM.MultiLine = false;
            this.txtGyoshuBunruiNM.Name = "txtGyoshuBunruiNM";
            this.txtGyoshuBunruiNM.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: left; vertical-align: top";
            this.txtGyoshuBunruiNM.Text = "魚類ごとの合";
            this.txtGyoshuBunruiNM.Top = 0F;
            this.txtGyoshuBunruiNM.Width = 1.282284F;
            // 
            // txtSMizuageSuryo
            // 
            this.txtSMizuageSuryo.CanGrow = false;
            this.txtSMizuageSuryo.DataField = "ITEM06";
            this.txtSMizuageSuryo.Height = 0.1582677F;
            this.txtSMizuageSuryo.Left = 4.77874F;
            this.txtSMizuageSuryo.MultiLine = false;
            this.txtSMizuageSuryo.Name = "txtSMizuageSuryo";
            this.txtSMizuageSuryo.OutputFormat = resources.GetString("txtSMizuageSuryo.OutputFormat");
            this.txtSMizuageSuryo.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtSMizuageSuryo.SummaryGroup = "ghSGyoshuBunrui";
            this.txtSMizuageSuryo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSMizuageSuryo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSMizuageSuryo.Text = "1234567890.8";
            this.txtSMizuageSuryo.Top = 0F;
            this.txtSMizuageSuryo.Width = 1.020867F;
            // 
            // txtSHeikinTanka
            // 
            this.txtSHeikinTanka.CanGrow = false;
            this.txtSHeikinTanka.DataField = "Amount";
            this.txtSHeikinTanka.Height = 0.1582677F;
            this.txtSHeikinTanka.Left = 5.933859F;
            this.txtSHeikinTanka.MultiLine = false;
            this.txtSHeikinTanka.Name = "txtSHeikinTanka";
            this.txtSHeikinTanka.OutputFormat = resources.GetString("txtSHeikinTanka.OutputFormat");
            this.txtSHeikinTanka.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtSHeikinTanka.SummaryGroup = "ghSGyoshuBunrui";
            this.txtSHeikinTanka.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSHeikinTanka.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSHeikinTanka.Top = 0F;
            this.txtSHeikinTanka.Width = 1.020867F;
            // 
            // txtSMizuageKingaku
            // 
            this.txtSMizuageKingaku.CanGrow = false;
            this.txtSMizuageKingaku.DataField = "ITEM08";
            this.txtSMizuageKingaku.Height = 0.1582677F;
            this.txtSMizuageKingaku.Left = 7.208268F;
            this.txtSMizuageKingaku.MultiLine = false;
            this.txtSMizuageKingaku.Name = "txtSMizuageKingaku";
            this.txtSMizuageKingaku.OutputFormat = resources.GetString("txtSMizuageKingaku.OutputFormat");
            this.txtSMizuageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right; vertical-align: middle";
            this.txtSMizuageKingaku.SummaryGroup = "ghSGyoshuBunrui";
            this.txtSMizuageKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSMizuageKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSMizuageKingaku.Text = "-ZZZ,ZZZ,ZZZ,ZZ9";
            this.txtSMizuageKingaku.Top = 7.450581E-09F;
            this.txtSMizuageKingaku.Width = 1.177165F;
            // 
            // HNMR1051R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.7362205F;
            this.PageSettings.Margins.Right = 0.7362205F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 8.645669F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.ghMPayao);
            this.Sections.Add(this.ghSGyoshuBunrui);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfSGyoshuBunrui);
            this.Sections.Add(this.gfMPayao);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.HNMR1051R_FetchData);
            this.DataInitialize += new System.EventHandler(this.HNMR1051R_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtFromYearMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToYearMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptPayaoNO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoshuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptMizuageSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptHeikinTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptMizuageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeikinTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuBunruiCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMizuageSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHeikinTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMizuageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayaoNO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPayaoTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMizuageSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMHeikinTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMizuageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFutougou1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuBunruiNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSMizuageSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSHeikinTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSMizuageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFromYearMonth;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblWave;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToYearMonth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrintDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptPayaoNO;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptMizuageSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptHeikinTanka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptMizuageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeaderUpper;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeaderUnder;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMizuageSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeikinTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMizuageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGMizuageSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGHeikinTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGMizuageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghMPayao;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayaoNO;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfMPayao;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPayaoTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMMizuageSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMHeikinTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMMizuageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghSGyoshuBunrui;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfSGyoshuBunrui;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFutougou1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuBunruiNM;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSMizuageSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSHeikinTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSMizuageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuBunruiCD;
    }
}
