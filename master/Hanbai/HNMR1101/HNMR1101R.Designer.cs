﻿namespace jp.co.fsi.hn.hnrm1101
{
    /// <summary>
    /// HANR3111R の概要の説明です。
    /// </summary>
    partial class HNMR1101R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNMR1101R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtFromYearMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblWave = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToYearMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPrintDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptSenshuNM = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptNissu = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiSuryo_01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiKingaku_01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeaderUpper = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lnHeaderUnder = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblChikuNM = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGyohoNM1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCptGyoruiSuryo_02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiKingaku_02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiSuryo_03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiKingaku_03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiSuryo_04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiKingaku_04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiSuryo_05 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiKingaku_05 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiSuryo_06 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiKingaku_06 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiSuryo_07 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiKingaku_07 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiSuryo_08 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptGyoruiKingaku_08 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGyohoNM2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyohoNM3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyohoNM4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyohoNM5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyohoNM6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyohoNM7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyohoNM8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtSenshuNM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiSuryo_01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiKingaku_01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiSuryo_02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNissu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiKingaku_02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiSuryo_03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiKingaku_03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiSuryo_04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiKingaku_04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiSuryo_05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiKingaku_05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiSuryo_06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiKingaku_06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiSuryo_07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiKingaku_07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiSuryo_08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoruiKingaku_08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnDeteilUnder = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblGTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGGyoruiSuryo_01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiKingaku_01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiSuryo_02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtGNissu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiSuryo_03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiSuryo_04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiSuryo_05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiSuryo_06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiSuryo_07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiSuryo_08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiKingaku_02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiKingaku_03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiKingaku_04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiKingaku_05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiKingaku_06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiKingaku_07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGGyoruiKingaku_08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghSChiku = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtChikuNM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblDainari = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShounari = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.gfSChiku = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblChikuKei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSNissu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiSuryo_01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiKingaku_01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiSuryo_02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiKingaku_02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiSuryo_03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiKingaku_03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiSuryo_04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiKingaku_04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiSuryo_05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiKingaku_05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiSuryo_06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiKingaku_06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiSuryo_07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiKingaku_07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiSuryo_08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGyoruiKingaku_08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromYearMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToYearMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptSenshuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptNissu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChikuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNissu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGNissu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChikuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDainari)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShounari)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChikuKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSNissu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.CanGrow = false;
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFromYearMonth,
            this.lblWave,
            this.txtToYearMonth,
            this.txtPrintDate,
            this.lblTitle,
            this.lblCptSenshuNM,
            this.lblCptNissu,
            this.lblCptGyoruiSuryo_01,
            this.lblCptGyoruiKingaku_01,
            this.lnHeaderUpper,
            this.lnHeaderUnder,
            this.lblChikuNM,
            this.txtGyohoNM1,
            this.lblCptGyoruiSuryo_02,
            this.lblCptGyoruiKingaku_02,
            this.lblCptGyoruiSuryo_03,
            this.lblCptGyoruiKingaku_03,
            this.lblCptGyoruiSuryo_04,
            this.lblCptGyoruiKingaku_04,
            this.lblCptGyoruiSuryo_05,
            this.lblCptGyoruiKingaku_05,
            this.lblCptGyoruiSuryo_06,
            this.lblCptGyoruiKingaku_06,
            this.lblCptGyoruiSuryo_07,
            this.lblCptGyoruiKingaku_07,
            this.lblCptGyoruiSuryo_08,
            this.lblCptGyoruiKingaku_08,
            this.txtGyohoNM2,
            this.txtGyohoNM3,
            this.txtGyohoNM4,
            this.txtGyohoNM5,
            this.txtGyohoNM6,
            this.txtGyohoNM7,
            this.txtGyohoNM8});
            this.pageHeader.Height = 1.082677F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtFromYearMonth
            // 
            this.txtFromYearMonth.CanGrow = false;
            this.txtFromYearMonth.DataField = "printDateFr";
            this.txtFromYearMonth.Height = 0.1582677F;
            this.txtFromYearMonth.Left = 0.3393704F;
            this.txtFromYearMonth.MultiLine = false;
            this.txtFromYearMonth.Name = "txtFromYearMonth";
            this.txtFromYearMonth.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtFromYearMonth.Text = "平成88年12月";
            this.txtFromYearMonth.Top = 0F;
            this.txtFromYearMonth.Width = 0.8956694F;
            // 
            // lblWave
            // 
            this.lblWave.Height = 0.1582677F;
            this.lblWave.HyperLink = null;
            this.lblWave.Left = 1.23504F;
            this.lblWave.Name = "lblWave";
            this.lblWave.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: center; vertical-align: middle";
            this.lblWave.Text = "～";
            this.lblWave.Top = 0F;
            this.lblWave.Width = 0.2708662F;
            // 
            // txtToYearMonth
            // 
            this.txtToYearMonth.CanGrow = false;
            this.txtToYearMonth.DataField = "printDateTo";
            this.txtToYearMonth.Height = 0.1582677F;
            this.txtToYearMonth.Left = 1.505906F;
            this.txtToYearMonth.MultiLine = false;
            this.txtToYearMonth.Name = "txtToYearMonth";
            this.txtToYearMonth.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtToYearMonth.Text = "平成88年12月";
            this.txtToYearMonth.Top = 0F;
            this.txtToYearMonth.Width = 0.8956689F;
            // 
            // txtPrintDate
            // 
            this.txtPrintDate.CanGrow = false;
            this.txtPrintDate.DataField = "today";
            this.txtPrintDate.Height = 0.1582677F;
            this.txtPrintDate.Left = 11.37441F;
            this.txtPrintDate.MultiLine = false;
            this.txtPrintDate.Name = "txtPrintDate";
            this.txtPrintDate.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtPrintDate.Text = "平成88年12月30日";
            this.txtPrintDate.Top = 0F;
            this.txtPrintDate.Width = 1.164567F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.2F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 0.3393705F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: underline";
            this.lblTitle.Text = "地区別個人別業態別漁獲高月報";
            this.lblTitle.Top = 0.1582677F;
            this.lblTitle.Width = 12.19961F;
            // 
            // lblCptSenshuNM
            // 
            this.lblCptSenshuNM.Height = 0.1582677F;
            this.lblCptSenshuNM.HyperLink = null;
            this.lblCptSenshuNM.Left = 0.5996066F;
            this.lblCptSenshuNM.Name = "lblCptSenshuNM";
            this.lblCptSenshuNM.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptSenshuNM.Text = "略称船主名";
            this.lblCptSenshuNM.Top = 0.7401575F;
            this.lblCptSenshuNM.Width = 0.7291338F;
            // 
            // lblCptNissu
            // 
            this.lblCptNissu.Height = 0.1582677F;
            this.lblCptNissu.HyperLink = null;
            this.lblCptNissu.Left = 2.804725F;
            this.lblCptNissu.Name = "lblCptNissu";
            this.lblCptNissu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptNissu.Text = "日数";
            this.lblCptNissu.Top = 0.7401575F;
            this.lblCptNissu.Width = 0.3543307F;
            // 
            // lblCptGyoruiSuryo_01
            // 
            this.lblCptGyoruiSuryo_01.Height = 0.1582677F;
            this.lblCptGyoruiSuryo_01.HyperLink = null;
            this.lblCptGyoruiSuryo_01.Left = 3.379528F;
            this.lblCptGyoruiSuryo_01.Name = "lblCptGyoruiSuryo_01";
            this.lblCptGyoruiSuryo_01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiSuryo_01.Text = "水揚数量";
            this.lblCptGyoruiSuryo_01.Top = 0.7401575F;
            this.lblCptGyoruiSuryo_01.Width = 1.141732F;
            // 
            // lblCptGyoruiKingaku_01
            // 
            this.lblCptGyoruiKingaku_01.Height = 0.1582677F;
            this.lblCptGyoruiKingaku_01.HyperLink = null;
            this.lblCptGyoruiKingaku_01.Left = 3.379528F;
            this.lblCptGyoruiKingaku_01.Name = "lblCptGyoruiKingaku_01";
            this.lblCptGyoruiKingaku_01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiKingaku_01.Text = "水揚金額";
            this.lblCptGyoruiKingaku_01.Top = 0.8976378F;
            this.lblCptGyoruiKingaku_01.Width = 1.141732F;
            // 
            // lnHeaderUpper
            // 
            this.lnHeaderUpper.Height = 5.960464E-08F;
            this.lnHeaderUpper.Left = 0.3393704F;
            this.lnHeaderUpper.LineWeight = 2F;
            this.lnHeaderUpper.Name = "lnHeaderUpper";
            this.lnHeaderUpper.Top = 0.7110236F;
            this.lnHeaderUpper.Width = 12.97047F;
            this.lnHeaderUpper.X1 = 0.3393704F;
            this.lnHeaderUpper.X2 = 13.30984F;
            this.lnHeaderUpper.Y1 = 0.7110237F;
            this.lnHeaderUpper.Y2 = 0.7110236F;
            // 
            // lnHeaderUnder
            // 
            this.lnHeaderUnder.Height = 1.192093E-07F;
            this.lnHeaderUnder.Left = 0.3393704F;
            this.lnHeaderUnder.LineWeight = 2F;
            this.lnHeaderUnder.Name = "lnHeaderUnder";
            this.lnHeaderUnder.Top = 1.062992F;
            this.lnHeaderUnder.Width = 12.97047F;
            this.lnHeaderUnder.X1 = 0.3393704F;
            this.lnHeaderUnder.X2 = 13.30984F;
            this.lnHeaderUnder.Y1 = 1.062992F;
            this.lnHeaderUnder.Y2 = 1.062992F;
            // 
            // lblChikuNM
            // 
            this.lblChikuNM.Height = 0.1582677F;
            this.lblChikuNM.HyperLink = null;
            this.lblChikuNM.Left = 0.5059059F;
            this.lblChikuNM.Name = "lblChikuNM";
            this.lblChikuNM.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: left; verti" +
    "cal-align: middle";
            this.lblChikuNM.Text = "地区名";
            this.lblChikuNM.Top = 0.3582678F;
            this.lblChikuNM.Width = 0.5326772F;
            // 
            // txtGyohoNM1
            // 
            this.txtGyohoNM1.CanGrow = false;
            this.txtGyohoNM1.DataField = "GYOHO_01";
            this.txtGyohoNM1.Height = 0.1582677F;
            this.txtGyohoNM1.Left = 3.379528F;
            this.txtGyohoNM1.MultiLine = false;
            this.txtGyohoNM1.Name = "txtGyohoNM1";
            this.txtGyohoNM1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.txtGyohoNM1.Text = "ＮＮＮＮＮＮ類";
            this.txtGyohoNM1.Top = 0.5196851F;
            this.txtGyohoNM1.Width = 1.141732F;
            // 
            // lblCptGyoruiSuryo_02
            // 
            this.lblCptGyoruiSuryo_02.Height = 0.1582677F;
            this.lblCptGyoruiSuryo_02.HyperLink = null;
            this.lblCptGyoruiSuryo_02.Left = 4.584252F;
            this.lblCptGyoruiSuryo_02.Name = "lblCptGyoruiSuryo_02";
            this.lblCptGyoruiSuryo_02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiSuryo_02.Text = "水揚数量";
            this.lblCptGyoruiSuryo_02.Top = 0.73937F;
            this.lblCptGyoruiSuryo_02.Width = 1.141732F;
            // 
            // lblCptGyoruiKingaku_02
            // 
            this.lblCptGyoruiKingaku_02.Height = 0.1582677F;
            this.lblCptGyoruiKingaku_02.HyperLink = null;
            this.lblCptGyoruiKingaku_02.Left = 4.584252F;
            this.lblCptGyoruiKingaku_02.Name = "lblCptGyoruiKingaku_02";
            this.lblCptGyoruiKingaku_02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiKingaku_02.Text = "水揚金額";
            this.lblCptGyoruiKingaku_02.Top = 0.8976377F;
            this.lblCptGyoruiKingaku_02.Width = 1.141732F;
            // 
            // lblCptGyoruiSuryo_03
            // 
            this.lblCptGyoruiSuryo_03.Height = 0.1582677F;
            this.lblCptGyoruiSuryo_03.HyperLink = null;
            this.lblCptGyoruiSuryo_03.Left = 5.753544F;
            this.lblCptGyoruiSuryo_03.Name = "lblCptGyoruiSuryo_03";
            this.lblCptGyoruiSuryo_03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiSuryo_03.Text = "水揚数量";
            this.lblCptGyoruiSuryo_03.Top = 0.7401574F;
            this.lblCptGyoruiSuryo_03.Width = 1.141732F;
            // 
            // lblCptGyoruiKingaku_03
            // 
            this.lblCptGyoruiKingaku_03.Height = 0.1582677F;
            this.lblCptGyoruiKingaku_03.HyperLink = null;
            this.lblCptGyoruiKingaku_03.Left = 5.753544F;
            this.lblCptGyoruiKingaku_03.Name = "lblCptGyoruiKingaku_03";
            this.lblCptGyoruiKingaku_03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiKingaku_03.Text = "水揚金額";
            this.lblCptGyoruiKingaku_03.Top = 0.9047244F;
            this.lblCptGyoruiKingaku_03.Width = 1.141732F;
            // 
            // lblCptGyoruiSuryo_04
            // 
            this.lblCptGyoruiSuryo_04.Height = 0.1582677F;
            this.lblCptGyoruiSuryo_04.HyperLink = null;
            this.lblCptGyoruiSuryo_04.Left = 6.966142F;
            this.lblCptGyoruiSuryo_04.Name = "lblCptGyoruiSuryo_04";
            this.lblCptGyoruiSuryo_04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiSuryo_04.Text = "水揚数量";
            this.lblCptGyoruiSuryo_04.Top = 0.7464566F;
            this.lblCptGyoruiSuryo_04.Width = 1.141732F;
            // 
            // lblCptGyoruiKingaku_04
            // 
            this.lblCptGyoruiKingaku_04.Height = 0.1582677F;
            this.lblCptGyoruiKingaku_04.HyperLink = null;
            this.lblCptGyoruiKingaku_04.Left = 6.966142F;
            this.lblCptGyoruiKingaku_04.Name = "lblCptGyoruiKingaku_04";
            this.lblCptGyoruiKingaku_04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiKingaku_04.Text = "水揚金額";
            this.lblCptGyoruiKingaku_04.Top = 0.8976377F;
            this.lblCptGyoruiKingaku_04.Width = 1.141732F;
            // 
            // lblCptGyoruiSuryo_05
            // 
            this.lblCptGyoruiSuryo_05.Height = 0.1582677F;
            this.lblCptGyoruiSuryo_05.HyperLink = null;
            this.lblCptGyoruiSuryo_05.Left = 8.202756F;
            this.lblCptGyoruiSuryo_05.Name = "lblCptGyoruiSuryo_05";
            this.lblCptGyoruiSuryo_05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiSuryo_05.Text = "水揚数量";
            this.lblCptGyoruiSuryo_05.Top = 0.7401574F;
            this.lblCptGyoruiSuryo_05.Width = 1.141732F;
            // 
            // lblCptGyoruiKingaku_05
            // 
            this.lblCptGyoruiKingaku_05.Height = 0.1582677F;
            this.lblCptGyoruiKingaku_05.HyperLink = null;
            this.lblCptGyoruiKingaku_05.Left = 8.202756F;
            this.lblCptGyoruiKingaku_05.Name = "lblCptGyoruiKingaku_05";
            this.lblCptGyoruiKingaku_05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiKingaku_05.Text = "水揚金額";
            this.lblCptGyoruiKingaku_05.Top = 0.8976377F;
            this.lblCptGyoruiKingaku_05.Width = 1.141732F;
            // 
            // lblCptGyoruiSuryo_06
            // 
            this.lblCptGyoruiSuryo_06.Height = 0.1582677F;
            this.lblCptGyoruiSuryo_06.HyperLink = null;
            this.lblCptGyoruiSuryo_06.Left = 9.427166F;
            this.lblCptGyoruiSuryo_06.Name = "lblCptGyoruiSuryo_06";
            this.lblCptGyoruiSuryo_06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiSuryo_06.Text = "水揚数量";
            this.lblCptGyoruiSuryo_06.Top = 0.73937F;
            this.lblCptGyoruiSuryo_06.Width = 1.141732F;
            // 
            // lblCptGyoruiKingaku_06
            // 
            this.lblCptGyoruiKingaku_06.Height = 0.1582677F;
            this.lblCptGyoruiKingaku_06.HyperLink = null;
            this.lblCptGyoruiKingaku_06.Left = 9.427166F;
            this.lblCptGyoruiKingaku_06.Name = "lblCptGyoruiKingaku_06";
            this.lblCptGyoruiKingaku_06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiKingaku_06.Text = "水揚金額";
            this.lblCptGyoruiKingaku_06.Top = 0.8976377F;
            this.lblCptGyoruiKingaku_06.Width = 1.141732F;
            // 
            // lblCptGyoruiSuryo_07
            // 
            this.lblCptGyoruiSuryo_07.Height = 0.1582677F;
            this.lblCptGyoruiSuryo_07.HyperLink = null;
            this.lblCptGyoruiSuryo_07.Left = 10.62992F;
            this.lblCptGyoruiSuryo_07.Name = "lblCptGyoruiSuryo_07";
            this.lblCptGyoruiSuryo_07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiSuryo_07.Text = "水揚数量";
            this.lblCptGyoruiSuryo_07.Top = 0.7393701F;
            this.lblCptGyoruiSuryo_07.Width = 1.141732F;
            // 
            // lblCptGyoruiKingaku_07
            // 
            this.lblCptGyoruiKingaku_07.Height = 0.1582677F;
            this.lblCptGyoruiKingaku_07.HyperLink = null;
            this.lblCptGyoruiKingaku_07.Left = 10.62992F;
            this.lblCptGyoruiKingaku_07.Name = "lblCptGyoruiKingaku_07";
            this.lblCptGyoruiKingaku_07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiKingaku_07.Text = "水揚金額";
            this.lblCptGyoruiKingaku_07.Top = 0.8976378F;
            this.lblCptGyoruiKingaku_07.Width = 1.141732F;
            // 
            // lblCptGyoruiSuryo_08
            // 
            this.lblCptGyoruiSuryo_08.Height = 0.1582677F;
            this.lblCptGyoruiSuryo_08.HyperLink = null;
            this.lblCptGyoruiSuryo_08.Left = 11.83819F;
            this.lblCptGyoruiSuryo_08.Name = "lblCptGyoruiSuryo_08";
            this.lblCptGyoruiSuryo_08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiSuryo_08.Text = "水揚数量";
            this.lblCptGyoruiSuryo_08.Top = 0.73937F;
            this.lblCptGyoruiSuryo_08.Width = 1.141732F;
            // 
            // lblCptGyoruiKingaku_08
            // 
            this.lblCptGyoruiKingaku_08.Height = 0.1582677F;
            this.lblCptGyoruiKingaku_08.HyperLink = null;
            this.lblCptGyoruiKingaku_08.Left = 11.83819F;
            this.lblCptGyoruiKingaku_08.Name = "lblCptGyoruiKingaku_08";
            this.lblCptGyoruiKingaku_08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCptGyoruiKingaku_08.Text = "水揚金額";
            this.lblCptGyoruiKingaku_08.Top = 0.8976377F;
            this.lblCptGyoruiKingaku_08.Width = 1.141732F;
            // 
            // txtGyohoNM2
            // 
            this.txtGyohoNM2.CanGrow = false;
            this.txtGyohoNM2.DataField = "GYOHO_02";
            this.txtGyohoNM2.Height = 0.1582677F;
            this.txtGyohoNM2.Left = 4.584252F;
            this.txtGyohoNM2.MultiLine = false;
            this.txtGyohoNM2.Name = "txtGyohoNM2";
            this.txtGyohoNM2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.txtGyohoNM2.Text = "ＮＮＮＮＮＮ類";
            this.txtGyohoNM2.Top = 0.5165355F;
            this.txtGyohoNM2.Width = 1.141732F;
            // 
            // txtGyohoNM3
            // 
            this.txtGyohoNM3.CanGrow = false;
            this.txtGyohoNM3.DataField = "GYOHO_03";
            this.txtGyohoNM3.Height = 0.1582677F;
            this.txtGyohoNM3.Left = 5.753544F;
            this.txtGyohoNM3.MultiLine = false;
            this.txtGyohoNM3.Name = "txtGyohoNM3";
            this.txtGyohoNM3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.txtGyohoNM3.Text = "ＮＮＮＮＮＮ類";
            this.txtGyohoNM3.Top = 0.5165355F;
            this.txtGyohoNM3.Width = 1.141732F;
            // 
            // txtGyohoNM4
            // 
            this.txtGyohoNM4.CanGrow = false;
            this.txtGyohoNM4.DataField = "GYOHO_04";
            this.txtGyohoNM4.Height = 0.1582677F;
            this.txtGyohoNM4.Left = 6.966142F;
            this.txtGyohoNM4.MultiLine = false;
            this.txtGyohoNM4.Name = "txtGyohoNM4";
            this.txtGyohoNM4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.txtGyohoNM4.Text = "ＮＮＮＮＮＮ類";
            this.txtGyohoNM4.Top = 0.5165355F;
            this.txtGyohoNM4.Width = 1.141732F;
            // 
            // txtGyohoNM5
            // 
            this.txtGyohoNM5.CanGrow = false;
            this.txtGyohoNM5.DataField = "GYOHO_05";
            this.txtGyohoNM5.Height = 0.1582677F;
            this.txtGyohoNM5.Left = 8.202363F;
            this.txtGyohoNM5.MultiLine = false;
            this.txtGyohoNM5.Name = "txtGyohoNM5";
            this.txtGyohoNM5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.txtGyohoNM5.Text = "ＮＮＮＮＮＮ類";
            this.txtGyohoNM5.Top = 0.5165355F;
            this.txtGyohoNM5.Width = 1.141732F;
            // 
            // txtGyohoNM6
            // 
            this.txtGyohoNM6.CanGrow = false;
            this.txtGyohoNM6.DataField = "GYOHO_06";
            this.txtGyohoNM6.Height = 0.1582677F;
            this.txtGyohoNM6.Left = 9.426772F;
            this.txtGyohoNM6.MultiLine = false;
            this.txtGyohoNM6.Name = "txtGyohoNM6";
            this.txtGyohoNM6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.txtGyohoNM6.Text = "ＮＮＮＮＮＮ類";
            this.txtGyohoNM6.Top = 0.5165355F;
            this.txtGyohoNM6.Width = 1.141732F;
            // 
            // txtGyohoNM7
            // 
            this.txtGyohoNM7.CanGrow = false;
            this.txtGyohoNM7.DataField = "GYOHO_07";
            this.txtGyohoNM7.Height = 0.1582677F;
            this.txtGyohoNM7.Left = 10.62953F;
            this.txtGyohoNM7.MultiLine = false;
            this.txtGyohoNM7.Name = "txtGyohoNM7";
            this.txtGyohoNM7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.txtGyohoNM7.Text = "ＮＮＮＮＮＮ類";
            this.txtGyohoNM7.Top = 0.5196851F;
            this.txtGyohoNM7.Width = 1.141732F;
            // 
            // txtGyohoNM8
            // 
            this.txtGyohoNM8.CanGrow = false;
            this.txtGyohoNM8.DataField = "GYOHO_08";
            this.txtGyohoNM8.Height = 0.1582677F;
            this.txtGyohoNM8.Left = 11.83819F;
            this.txtGyohoNM8.MultiLine = false;
            this.txtGyohoNM8.Name = "txtGyohoNM8";
            this.txtGyohoNM8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.txtGyohoNM8.Text = "ＮＮＮＮＮＮ類";
            this.txtGyohoNM8.Top = 0.5165355F;
            this.txtGyohoNM8.Width = 1.141732F;
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSenshuNM,
            this.txtGyoruiSuryo_01,
            this.txtGyoruiKingaku_01,
            this.txtGyoruiSuryo_02,
            this.txtNissu,
            this.txtGyoruiKingaku_02,
            this.txtGyoruiSuryo_03,
            this.txtGyoruiKingaku_03,
            this.txtGyoruiSuryo_04,
            this.txtGyoruiKingaku_04,
            this.txtGyoruiSuryo_05,
            this.txtGyoruiKingaku_05,
            this.txtGyoruiSuryo_06,
            this.txtGyoruiKingaku_06,
            this.txtGyoruiSuryo_07,
            this.txtGyoruiKingaku_07,
            this.txtGyoruiSuryo_08,
            this.txtGyoruiKingaku_08});
            this.detail.Height = 0.3307087F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            // 
            // txtSenshuNM
            // 
            this.txtSenshuNM.CanGrow = false;
            this.txtSenshuNM.DataField = "ITEM04";
            this.txtSenshuNM.Height = 0.1582677F;
            this.txtSenshuNM.Left = 0.7811027F;
            this.txtSenshuNM.MultiLine = false;
            this.txtSenshuNM.Name = "txtSenshuNM";
            this.txtSenshuNM.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtSenshuNM.Text = "ＮＮＮＮＮＮＮＮＮＮＮＮ";
            this.txtSenshuNM.Top = 0.01968504F;
            this.txtSenshuNM.Width = 1.745669F;
            // 
            // txtGyoruiSuryo_01
            // 
            this.txtGyoruiSuryo_01.CanGrow = false;
            this.txtGyoruiSuryo_01.DataField = "ITEM06";
            this.txtGyoruiSuryo_01.Height = 0.1582677F;
            this.txtGyoruiSuryo_01.Left = 3.379528F;
            this.txtGyoruiSuryo_01.MultiLine = false;
            this.txtGyoruiSuryo_01.Name = "txtGyoruiSuryo_01";
            this.txtGyoruiSuryo_01.OutputFormat = resources.GetString("txtGyoruiSuryo_01.OutputFormat");
            this.txtGyoruiSuryo_01.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiSuryo_01.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGyoruiSuryo_01.Top = 0.01968504F;
            this.txtGyoruiSuryo_01.Width = 1.141732F;
            // 
            // txtGyoruiKingaku_01
            // 
            this.txtGyoruiKingaku_01.CanGrow = false;
            this.txtGyoruiKingaku_01.DataField = "ITEM07";
            this.txtGyoruiKingaku_01.Height = 0.1582677F;
            this.txtGyoruiKingaku_01.Left = 3.379528F;
            this.txtGyoruiKingaku_01.MultiLine = false;
            this.txtGyoruiKingaku_01.Name = "txtGyoruiKingaku_01";
            this.txtGyoruiKingaku_01.OutputFormat = resources.GetString("txtGyoruiKingaku_01.OutputFormat");
            this.txtGyoruiKingaku_01.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiKingaku_01.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGyoruiKingaku_01.Top = 0.1673228F;
            this.txtGyoruiKingaku_01.Width = 0.9330708F;
            // 
            // txtGyoruiSuryo_02
            // 
            this.txtGyoruiSuryo_02.CanGrow = false;
            this.txtGyoruiSuryo_02.DataField = "ITEM08";
            this.txtGyoruiSuryo_02.Height = 0.1582677F;
            this.txtGyoruiSuryo_02.Left = 4.584252F;
            this.txtGyoruiSuryo_02.MultiLine = false;
            this.txtGyoruiSuryo_02.Name = "txtGyoruiSuryo_02";
            this.txtGyoruiSuryo_02.OutputFormat = resources.GetString("txtGyoruiSuryo_02.OutputFormat");
            this.txtGyoruiSuryo_02.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiSuryo_02.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGyoruiSuryo_02.Top = 0.01968498F;
            this.txtGyoruiSuryo_02.Width = 1.141732F;
            // 
            // txtNissu
            // 
            this.txtNissu.CanGrow = false;
            this.txtNissu.DataField = "ITEM05";
            this.txtNissu.Height = 0.1582677F;
            this.txtNissu.Left = 2.765355F;
            this.txtNissu.MultiLine = false;
            this.txtNissu.Name = "txtNissu";
            this.txtNissu.OutputFormat = resources.GetString("txtNissu.OutputFormat");
            this.txtNissu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtNissu.Text = "Z,ZZ9";
            this.txtNissu.Top = 0.01968504F;
            this.txtNissu.Width = 0.3937008F;
            // 
            // txtGyoruiKingaku_02
            // 
            this.txtGyoruiKingaku_02.CanGrow = false;
            this.txtGyoruiKingaku_02.DataField = "ITEM09";
            this.txtGyoruiKingaku_02.Height = 0.1582677F;
            this.txtGyoruiKingaku_02.Left = 4.584252F;
            this.txtGyoruiKingaku_02.MultiLine = false;
            this.txtGyoruiKingaku_02.Name = "txtGyoruiKingaku_02";
            this.txtGyoruiKingaku_02.OutputFormat = resources.GetString("txtGyoruiKingaku_02.OutputFormat");
            this.txtGyoruiKingaku_02.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiKingaku_02.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGyoruiKingaku_02.Top = 0.1673227F;
            this.txtGyoruiKingaku_02.Width = 0.9330708F;
            // 
            // txtGyoruiSuryo_03
            // 
            this.txtGyoruiSuryo_03.CanGrow = false;
            this.txtGyoruiSuryo_03.DataField = "ITEM10";
            this.txtGyoruiSuryo_03.Height = 0.1582677F;
            this.txtGyoruiSuryo_03.Left = 5.753544F;
            this.txtGyoruiSuryo_03.MultiLine = false;
            this.txtGyoruiSuryo_03.Name = "txtGyoruiSuryo_03";
            this.txtGyoruiSuryo_03.OutputFormat = resources.GetString("txtGyoruiSuryo_03.OutputFormat");
            this.txtGyoruiSuryo_03.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiSuryo_03.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGyoruiSuryo_03.Top = 0.01968498F;
            this.txtGyoruiSuryo_03.Width = 1.141732F;
            // 
            // txtGyoruiKingaku_03
            // 
            this.txtGyoruiKingaku_03.CanGrow = false;
            this.txtGyoruiKingaku_03.DataField = "ITEM11";
            this.txtGyoruiKingaku_03.Height = 0.1582677F;
            this.txtGyoruiKingaku_03.Left = 5.753544F;
            this.txtGyoruiKingaku_03.MultiLine = false;
            this.txtGyoruiKingaku_03.Name = "txtGyoruiKingaku_03";
            this.txtGyoruiKingaku_03.OutputFormat = resources.GetString("txtGyoruiKingaku_03.OutputFormat");
            this.txtGyoruiKingaku_03.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiKingaku_03.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGyoruiKingaku_03.Top = 0.1673227F;
            this.txtGyoruiKingaku_03.Width = 0.9330708F;
            // 
            // txtGyoruiSuryo_04
            // 
            this.txtGyoruiSuryo_04.CanGrow = false;
            this.txtGyoruiSuryo_04.DataField = "ITEM12";
            this.txtGyoruiSuryo_04.Height = 0.1582677F;
            this.txtGyoruiSuryo_04.Left = 6.966142F;
            this.txtGyoruiSuryo_04.MultiLine = false;
            this.txtGyoruiSuryo_04.Name = "txtGyoruiSuryo_04";
            this.txtGyoruiSuryo_04.OutputFormat = resources.GetString("txtGyoruiSuryo_04.OutputFormat");
            this.txtGyoruiSuryo_04.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiSuryo_04.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGyoruiSuryo_04.Top = 0.01968498F;
            this.txtGyoruiSuryo_04.Width = 1.141732F;
            // 
            // txtGyoruiKingaku_04
            // 
            this.txtGyoruiKingaku_04.CanGrow = false;
            this.txtGyoruiKingaku_04.DataField = "ITEM13";
            this.txtGyoruiKingaku_04.Height = 0.1582677F;
            this.txtGyoruiKingaku_04.Left = 6.966142F;
            this.txtGyoruiKingaku_04.MultiLine = false;
            this.txtGyoruiKingaku_04.Name = "txtGyoruiKingaku_04";
            this.txtGyoruiKingaku_04.OutputFormat = resources.GetString("txtGyoruiKingaku_04.OutputFormat");
            this.txtGyoruiKingaku_04.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiKingaku_04.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGyoruiKingaku_04.Top = 0.1673227F;
            this.txtGyoruiKingaku_04.Width = 0.9330708F;
            // 
            // txtGyoruiSuryo_05
            // 
            this.txtGyoruiSuryo_05.CanGrow = false;
            this.txtGyoruiSuryo_05.DataField = "ITEM14";
            this.txtGyoruiSuryo_05.Height = 0.1582677F;
            this.txtGyoruiSuryo_05.Left = 8.202756F;
            this.txtGyoruiSuryo_05.MultiLine = false;
            this.txtGyoruiSuryo_05.Name = "txtGyoruiSuryo_05";
            this.txtGyoruiSuryo_05.OutputFormat = resources.GetString("txtGyoruiSuryo_05.OutputFormat");
            this.txtGyoruiSuryo_05.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiSuryo_05.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGyoruiSuryo_05.Top = 0.009055059F;
            this.txtGyoruiSuryo_05.Width = 1.141732F;
            // 
            // txtGyoruiKingaku_05
            // 
            this.txtGyoruiKingaku_05.CanGrow = false;
            this.txtGyoruiKingaku_05.DataField = "ITEM15";
            this.txtGyoruiKingaku_05.Height = 0.1582677F;
            this.txtGyoruiKingaku_05.Left = 8.202363F;
            this.txtGyoruiKingaku_05.MultiLine = false;
            this.txtGyoruiKingaku_05.Name = "txtGyoruiKingaku_05";
            this.txtGyoruiKingaku_05.OutputFormat = resources.GetString("txtGyoruiKingaku_05.OutputFormat");
            this.txtGyoruiKingaku_05.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiKingaku_05.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGyoruiKingaku_05.Top = 0.1673227F;
            this.txtGyoruiKingaku_05.Width = 0.9330708F;
            // 
            // txtGyoruiSuryo_06
            // 
            this.txtGyoruiSuryo_06.CanGrow = false;
            this.txtGyoruiSuryo_06.DataField = "ITEM16";
            this.txtGyoruiSuryo_06.Height = 0.1582677F;
            this.txtGyoruiSuryo_06.Left = 9.427166F;
            this.txtGyoruiSuryo_06.MultiLine = false;
            this.txtGyoruiSuryo_06.Name = "txtGyoruiSuryo_06";
            this.txtGyoruiSuryo_06.OutputFormat = resources.GetString("txtGyoruiSuryo_06.OutputFormat");
            this.txtGyoruiSuryo_06.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiSuryo_06.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGyoruiSuryo_06.Top = 0.01968498F;
            this.txtGyoruiSuryo_06.Width = 1.141732F;
            // 
            // txtGyoruiKingaku_06
            // 
            this.txtGyoruiKingaku_06.CanGrow = false;
            this.txtGyoruiKingaku_06.DataField = "ITEM17";
            this.txtGyoruiKingaku_06.Height = 0.1582677F;
            this.txtGyoruiKingaku_06.Left = 9.427166F;
            this.txtGyoruiKingaku_06.MultiLine = false;
            this.txtGyoruiKingaku_06.Name = "txtGyoruiKingaku_06";
            this.txtGyoruiKingaku_06.OutputFormat = resources.GetString("txtGyoruiKingaku_06.OutputFormat");
            this.txtGyoruiKingaku_06.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiKingaku_06.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGyoruiKingaku_06.Top = 0.1673227F;
            this.txtGyoruiKingaku_06.Width = 0.9330708F;
            // 
            // txtGyoruiSuryo_07
            // 
            this.txtGyoruiSuryo_07.CanGrow = false;
            this.txtGyoruiSuryo_07.DataField = "ITEM18";
            this.txtGyoruiSuryo_07.Height = 0.1582677F;
            this.txtGyoruiSuryo_07.Left = 10.62992F;
            this.txtGyoruiSuryo_07.MultiLine = false;
            this.txtGyoruiSuryo_07.Name = "txtGyoruiSuryo_07";
            this.txtGyoruiSuryo_07.OutputFormat = resources.GetString("txtGyoruiSuryo_07.OutputFormat");
            this.txtGyoruiSuryo_07.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiSuryo_07.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGyoruiSuryo_07.Top = 0.01968504F;
            this.txtGyoruiSuryo_07.Width = 1.141732F;
            // 
            // txtGyoruiKingaku_07
            // 
            this.txtGyoruiKingaku_07.CanGrow = false;
            this.txtGyoruiKingaku_07.DataField = "ITEM19";
            this.txtGyoruiKingaku_07.Height = 0.1582677F;
            this.txtGyoruiKingaku_07.Left = 10.62992F;
            this.txtGyoruiKingaku_07.MultiLine = false;
            this.txtGyoruiKingaku_07.Name = "txtGyoruiKingaku_07";
            this.txtGyoruiKingaku_07.OutputFormat = resources.GetString("txtGyoruiKingaku_07.OutputFormat");
            this.txtGyoruiKingaku_07.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiKingaku_07.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGyoruiKingaku_07.Top = 0.1673228F;
            this.txtGyoruiKingaku_07.Width = 0.9330708F;
            // 
            // txtGyoruiSuryo_08
            // 
            this.txtGyoruiSuryo_08.CanGrow = false;
            this.txtGyoruiSuryo_08.DataField = "ITEM20";
            this.txtGyoruiSuryo_08.Height = 0.1582677F;
            this.txtGyoruiSuryo_08.Left = 11.83819F;
            this.txtGyoruiSuryo_08.MultiLine = false;
            this.txtGyoruiSuryo_08.Name = "txtGyoruiSuryo_08";
            this.txtGyoruiSuryo_08.OutputFormat = resources.GetString("txtGyoruiSuryo_08.OutputFormat");
            this.txtGyoruiSuryo_08.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiSuryo_08.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGyoruiSuryo_08.Top = 0.01968498F;
            this.txtGyoruiSuryo_08.Width = 1.141732F;
            // 
            // txtGyoruiKingaku_08
            // 
            this.txtGyoruiKingaku_08.CanGrow = false;
            this.txtGyoruiKingaku_08.DataField = "ITEM21";
            this.txtGyoruiKingaku_08.Height = 0.1582677F;
            this.txtGyoruiKingaku_08.Left = 11.83819F;
            this.txtGyoruiKingaku_08.MultiLine = false;
            this.txtGyoruiKingaku_08.Name = "txtGyoruiKingaku_08";
            this.txtGyoruiKingaku_08.OutputFormat = resources.GetString("txtGyoruiKingaku_08.OutputFormat");
            this.txtGyoruiKingaku_08.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoruiKingaku_08.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGyoruiKingaku_08.Top = 0.1673227F;
            this.txtGyoruiKingaku_08.Width = 0.9330708F;
            // 
            // lnDeteilUnder
            // 
            this.lnDeteilUnder.Height = 0F;
            this.lnDeteilUnder.Left = 0.322835F;
            this.lnDeteilUnder.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.lnDeteilUnder.LineWeight = 1F;
            this.lnDeteilUnder.Name = "lnDeteilUnder";
            this.lnDeteilUnder.Top = 0F;
            this.lnDeteilUnder.Width = 12.98701F;
            this.lnDeteilUnder.X1 = 0.322835F;
            this.lnDeteilUnder.X2 = 13.30984F;
            this.lnDeteilUnder.Y1 = 0F;
            this.lnDeteilUnder.Y2 = 0F;
            // 
            // pageFooter
            // 
            this.pageFooter.CanGrow = false;
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.CanGrow = false;
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.CanGrow = false;
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblGTotal,
            this.txtGGyoruiSuryo_01,
            this.txtGGyoruiKingaku_01,
            this.txtGGyoruiSuryo_02,
            this.line2,
            this.txtGNissu,
            this.txtGGyoruiSuryo_03,
            this.txtGGyoruiSuryo_04,
            this.txtGGyoruiSuryo_05,
            this.txtGGyoruiSuryo_06,
            this.txtGGyoruiSuryo_07,
            this.txtGGyoruiSuryo_08,
            this.txtGGyoruiKingaku_02,
            this.txtGGyoruiKingaku_03,
            this.txtGGyoruiKingaku_04,
            this.txtGGyoruiKingaku_05,
            this.txtGGyoruiKingaku_06,
            this.txtGGyoruiKingaku_07,
            this.txtGGyoruiKingaku_08});
            this.reportFooter1.Height = 0.3740157F;
            this.reportFooter1.KeepTogether = true;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // lblGTotal
            // 
            this.lblGTotal.Height = 0.1582677F;
            this.lblGTotal.HyperLink = null;
            this.lblGTotal.Left = 1.429528F;
            this.lblGTotal.Name = "lblGTotal";
            this.lblGTotal.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
            this.lblGTotal.Text = "総  合  計";
            this.lblGTotal.Top = 0.03937008F;
            this.lblGTotal.Width = 0.8748031F;
            // 
            // txtGGyoruiSuryo_01
            // 
            this.txtGGyoruiSuryo_01.CanGrow = false;
            this.txtGGyoruiSuryo_01.DataField = "ITEM06";
            this.txtGGyoruiSuryo_01.Height = 0.1582677F;
            this.txtGGyoruiSuryo_01.Left = 3.379528F;
            this.txtGGyoruiSuryo_01.MultiLine = false;
            this.txtGGyoruiSuryo_01.Name = "txtGGyoruiSuryo_01";
            this.txtGGyoruiSuryo_01.OutputFormat = resources.GetString("txtGGyoruiSuryo_01.OutputFormat");
            this.txtGGyoruiSuryo_01.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiSuryo_01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiSuryo_01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiSuryo_01.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGGyoruiSuryo_01.Top = 0.03937008F;
            this.txtGGyoruiSuryo_01.Width = 1.141732F;
            // 
            // txtGGyoruiKingaku_01
            // 
            this.txtGGyoruiKingaku_01.CanGrow = false;
            this.txtGGyoruiKingaku_01.DataField = "ITEM07";
            this.txtGGyoruiKingaku_01.Height = 0.1582677F;
            this.txtGGyoruiKingaku_01.Left = 3.379528F;
            this.txtGGyoruiKingaku_01.MultiLine = false;
            this.txtGGyoruiKingaku_01.Name = "txtGGyoruiKingaku_01";
            this.txtGGyoruiKingaku_01.OutputFormat = resources.GetString("txtGGyoruiKingaku_01.OutputFormat");
            this.txtGGyoruiKingaku_01.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiKingaku_01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiKingaku_01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiKingaku_01.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGGyoruiKingaku_01.Top = 0.2066929F;
            this.txtGGyoruiKingaku_01.Width = 0.9330708F;
            // 
            // txtGGyoruiSuryo_02
            // 
            this.txtGGyoruiSuryo_02.CanGrow = false;
            this.txtGGyoruiSuryo_02.DataField = "ITEM08";
            this.txtGGyoruiSuryo_02.Height = 0.1582677F;
            this.txtGGyoruiSuryo_02.Left = 4.584252F;
            this.txtGGyoruiSuryo_02.MultiLine = false;
            this.txtGGyoruiSuryo_02.Name = "txtGGyoruiSuryo_02";
            this.txtGGyoruiSuryo_02.OutputFormat = resources.GetString("txtGGyoruiSuryo_02.OutputFormat");
            this.txtGGyoruiSuryo_02.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiSuryo_02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiSuryo_02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiSuryo_02.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGGyoruiSuryo_02.Top = 0.03937002F;
            this.txtGGyoruiSuryo_02.Width = 1.141732F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.3393704F;
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Top = 0F;
            this.line2.Width = 12.97047F;
            this.line2.X1 = 0.3393704F;
            this.line2.X2 = 13.30984F;
            this.line2.Y1 = 0F;
            this.line2.Y2 = 0F;
            // 
            // txtGNissu
            // 
            this.txtGNissu.CanGrow = false;
            this.txtGNissu.DataField = "ITEM05";
            this.txtGNissu.Height = 0.1582677F;
            this.txtGNissu.Left = 2.304331F;
            this.txtGNissu.MultiLine = false;
            this.txtGNissu.Name = "txtGNissu";
            this.txtGNissu.OutputFormat = resources.GetString("txtGNissu.OutputFormat");
            this.txtGNissu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGNissu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGNissu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGNissu.Text = "ZZZ,ZZZ,ZZZ";
            this.txtGNissu.Top = 0.03937008F;
            this.txtGNissu.Width = 0.8547246F;
            // 
            // txtGGyoruiSuryo_03
            // 
            this.txtGGyoruiSuryo_03.CanGrow = false;
            this.txtGGyoruiSuryo_03.DataField = "ITEM10";
            this.txtGGyoruiSuryo_03.Height = 0.1582677F;
            this.txtGGyoruiSuryo_03.Left = 5.753544F;
            this.txtGGyoruiSuryo_03.MultiLine = false;
            this.txtGGyoruiSuryo_03.Name = "txtGGyoruiSuryo_03";
            this.txtGGyoruiSuryo_03.OutputFormat = resources.GetString("txtGGyoruiSuryo_03.OutputFormat");
            this.txtGGyoruiSuryo_03.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiSuryo_03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiSuryo_03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiSuryo_03.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGGyoruiSuryo_03.Top = 0.03937002F;
            this.txtGGyoruiSuryo_03.Width = 1.141732F;
            // 
            // txtGGyoruiSuryo_04
            // 
            this.txtGGyoruiSuryo_04.CanGrow = false;
            this.txtGGyoruiSuryo_04.DataField = "ITEM12";
            this.txtGGyoruiSuryo_04.Height = 0.1582677F;
            this.txtGGyoruiSuryo_04.Left = 6.966142F;
            this.txtGGyoruiSuryo_04.MultiLine = false;
            this.txtGGyoruiSuryo_04.Name = "txtGGyoruiSuryo_04";
            this.txtGGyoruiSuryo_04.OutputFormat = resources.GetString("txtGGyoruiSuryo_04.OutputFormat");
            this.txtGGyoruiSuryo_04.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiSuryo_04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiSuryo_04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiSuryo_04.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGGyoruiSuryo_04.Top = 0.03937002F;
            this.txtGGyoruiSuryo_04.Width = 1.141732F;
            // 
            // txtGGyoruiSuryo_05
            // 
            this.txtGGyoruiSuryo_05.CanGrow = false;
            this.txtGGyoruiSuryo_05.DataField = "ITEM14";
            this.txtGGyoruiSuryo_05.Height = 0.1582677F;
            this.txtGGyoruiSuryo_05.Left = 8.202363F;
            this.txtGGyoruiSuryo_05.MultiLine = false;
            this.txtGGyoruiSuryo_05.Name = "txtGGyoruiSuryo_05";
            this.txtGGyoruiSuryo_05.OutputFormat = resources.GetString("txtGGyoruiSuryo_05.OutputFormat");
            this.txtGGyoruiSuryo_05.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiSuryo_05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiSuryo_05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiSuryo_05.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGGyoruiSuryo_05.Top = 0.03937002F;
            this.txtGGyoruiSuryo_05.Width = 1.141732F;
            // 
            // txtGGyoruiSuryo_06
            // 
            this.txtGGyoruiSuryo_06.CanGrow = false;
            this.txtGGyoruiSuryo_06.DataField = "ITEM16";
            this.txtGGyoruiSuryo_06.Height = 0.1582677F;
            this.txtGGyoruiSuryo_06.Left = 9.426772F;
            this.txtGGyoruiSuryo_06.MultiLine = false;
            this.txtGGyoruiSuryo_06.Name = "txtGGyoruiSuryo_06";
            this.txtGGyoruiSuryo_06.OutputFormat = resources.GetString("txtGGyoruiSuryo_06.OutputFormat");
            this.txtGGyoruiSuryo_06.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiSuryo_06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiSuryo_06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiSuryo_06.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGGyoruiSuryo_06.Top = 0.03937002F;
            this.txtGGyoruiSuryo_06.Width = 1.141732F;
            // 
            // txtGGyoruiSuryo_07
            // 
            this.txtGGyoruiSuryo_07.CanGrow = false;
            this.txtGGyoruiSuryo_07.DataField = "ITEM18";
            this.txtGGyoruiSuryo_07.Height = 0.1582677F;
            this.txtGGyoruiSuryo_07.Left = 10.62992F;
            this.txtGGyoruiSuryo_07.MultiLine = false;
            this.txtGGyoruiSuryo_07.Name = "txtGGyoruiSuryo_07";
            this.txtGGyoruiSuryo_07.OutputFormat = resources.GetString("txtGGyoruiSuryo_07.OutputFormat");
            this.txtGGyoruiSuryo_07.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiSuryo_07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiSuryo_07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiSuryo_07.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGGyoruiSuryo_07.Top = 0.03937008F;
            this.txtGGyoruiSuryo_07.Width = 1.141732F;
            // 
            // txtGGyoruiSuryo_08
            // 
            this.txtGGyoruiSuryo_08.CanGrow = false;
            this.txtGGyoruiSuryo_08.DataField = "ITEM20";
            this.txtGGyoruiSuryo_08.Height = 0.1582677F;
            this.txtGGyoruiSuryo_08.Left = 11.83819F;
            this.txtGGyoruiSuryo_08.MultiLine = false;
            this.txtGGyoruiSuryo_08.Name = "txtGGyoruiSuryo_08";
            this.txtGGyoruiSuryo_08.OutputFormat = resources.GetString("txtGGyoruiSuryo_08.OutputFormat");
            this.txtGGyoruiSuryo_08.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiSuryo_08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiSuryo_08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiSuryo_08.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtGGyoruiSuryo_08.Top = 0.03937002F;
            this.txtGGyoruiSuryo_08.Width = 1.141732F;
            // 
            // txtGGyoruiKingaku_02
            // 
            this.txtGGyoruiKingaku_02.CanGrow = false;
            this.txtGGyoruiKingaku_02.DataField = "ITEM09";
            this.txtGGyoruiKingaku_02.Height = 0.1582677F;
            this.txtGGyoruiKingaku_02.Left = 4.584252F;
            this.txtGGyoruiKingaku_02.MultiLine = false;
            this.txtGGyoruiKingaku_02.Name = "txtGGyoruiKingaku_02";
            this.txtGGyoruiKingaku_02.OutputFormat = resources.GetString("txtGGyoruiKingaku_02.OutputFormat");
            this.txtGGyoruiKingaku_02.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiKingaku_02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiKingaku_02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiKingaku_02.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGGyoruiKingaku_02.Top = 0.2066928F;
            this.txtGGyoruiKingaku_02.Width = 0.9330708F;
            // 
            // txtGGyoruiKingaku_03
            // 
            this.txtGGyoruiKingaku_03.CanGrow = false;
            this.txtGGyoruiKingaku_03.DataField = "ITEM11";
            this.txtGGyoruiKingaku_03.Height = 0.1582677F;
            this.txtGGyoruiKingaku_03.Left = 5.753544F;
            this.txtGGyoruiKingaku_03.MultiLine = false;
            this.txtGGyoruiKingaku_03.Name = "txtGGyoruiKingaku_03";
            this.txtGGyoruiKingaku_03.OutputFormat = resources.GetString("txtGGyoruiKingaku_03.OutputFormat");
            this.txtGGyoruiKingaku_03.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiKingaku_03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiKingaku_03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiKingaku_03.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGGyoruiKingaku_03.Top = 0.2066928F;
            this.txtGGyoruiKingaku_03.Width = 0.9330708F;
            // 
            // txtGGyoruiKingaku_04
            // 
            this.txtGGyoruiKingaku_04.CanGrow = false;
            this.txtGGyoruiKingaku_04.DataField = "ITEM13";
            this.txtGGyoruiKingaku_04.Height = 0.1582677F;
            this.txtGGyoruiKingaku_04.Left = 6.966142F;
            this.txtGGyoruiKingaku_04.MultiLine = false;
            this.txtGGyoruiKingaku_04.Name = "txtGGyoruiKingaku_04";
            this.txtGGyoruiKingaku_04.OutputFormat = resources.GetString("txtGGyoruiKingaku_04.OutputFormat");
            this.txtGGyoruiKingaku_04.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiKingaku_04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiKingaku_04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiKingaku_04.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGGyoruiKingaku_04.Top = 0.2066928F;
            this.txtGGyoruiKingaku_04.Width = 0.9330708F;
            // 
            // txtGGyoruiKingaku_05
            // 
            this.txtGGyoruiKingaku_05.CanGrow = false;
            this.txtGGyoruiKingaku_05.DataField = "ITEM15";
            this.txtGGyoruiKingaku_05.Height = 0.1582677F;
            this.txtGGyoruiKingaku_05.Left = 8.202363F;
            this.txtGGyoruiKingaku_05.MultiLine = false;
            this.txtGGyoruiKingaku_05.Name = "txtGGyoruiKingaku_05";
            this.txtGGyoruiKingaku_05.OutputFormat = resources.GetString("txtGGyoruiKingaku_05.OutputFormat");
            this.txtGGyoruiKingaku_05.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiKingaku_05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiKingaku_05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiKingaku_05.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGGyoruiKingaku_05.Top = 0.2066928F;
            this.txtGGyoruiKingaku_05.Width = 0.9330708F;
            // 
            // txtGGyoruiKingaku_06
            // 
            this.txtGGyoruiKingaku_06.CanGrow = false;
            this.txtGGyoruiKingaku_06.DataField = "ITEM17";
            this.txtGGyoruiKingaku_06.Height = 0.1582677F;
            this.txtGGyoruiKingaku_06.Left = 9.426772F;
            this.txtGGyoruiKingaku_06.MultiLine = false;
            this.txtGGyoruiKingaku_06.Name = "txtGGyoruiKingaku_06";
            this.txtGGyoruiKingaku_06.OutputFormat = resources.GetString("txtGGyoruiKingaku_06.OutputFormat");
            this.txtGGyoruiKingaku_06.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiKingaku_06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiKingaku_06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiKingaku_06.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGGyoruiKingaku_06.Top = 0.2066928F;
            this.txtGGyoruiKingaku_06.Width = 0.9330708F;
            // 
            // txtGGyoruiKingaku_07
            // 
            this.txtGGyoruiKingaku_07.CanGrow = false;
            this.txtGGyoruiKingaku_07.DataField = "ITEM19";
            this.txtGGyoruiKingaku_07.Height = 0.1582677F;
            this.txtGGyoruiKingaku_07.Left = 10.62992F;
            this.txtGGyoruiKingaku_07.MultiLine = false;
            this.txtGGyoruiKingaku_07.Name = "txtGGyoruiKingaku_07";
            this.txtGGyoruiKingaku_07.OutputFormat = resources.GetString("txtGGyoruiKingaku_07.OutputFormat");
            this.txtGGyoruiKingaku_07.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiKingaku_07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiKingaku_07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiKingaku_07.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGGyoruiKingaku_07.Top = 0.2066929F;
            this.txtGGyoruiKingaku_07.Width = 0.9330708F;
            // 
            // txtGGyoruiKingaku_08
            // 
            this.txtGGyoruiKingaku_08.CanGrow = false;
            this.txtGGyoruiKingaku_08.DataField = "ITEM21";
            this.txtGGyoruiKingaku_08.Height = 0.1582677F;
            this.txtGGyoruiKingaku_08.Left = 11.83819F;
            this.txtGGyoruiKingaku_08.MultiLine = false;
            this.txtGGyoruiKingaku_08.Name = "txtGGyoruiKingaku_08";
            this.txtGGyoruiKingaku_08.OutputFormat = resources.GetString("txtGGyoruiKingaku_08.OutputFormat");
            this.txtGGyoruiKingaku_08.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGGyoruiKingaku_08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGGyoruiKingaku_08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGGyoruiKingaku_08.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGGyoruiKingaku_08.Top = 0.2066928F;
            this.txtGGyoruiKingaku_08.Width = 0.9330708F;
            // 
            // ghSChiku
            // 
            this.ghSChiku.CanGrow = false;
            this.ghSChiku.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtChikuNM,
            this.lblDainari,
            this.lblShounari});
            this.ghSChiku.DataField = "ITEM01";
            this.ghSChiku.Height = 0.1574803F;
            this.ghSChiku.KeepTogether = true;
            this.ghSChiku.Name = "ghSChiku";
            // 
            // txtChikuNM
            // 
            this.txtChikuNM.CanGrow = false;
            this.txtChikuNM.DataField = "ITEM03";
            this.txtChikuNM.Height = 0.1582677F;
            this.txtChikuNM.Left = 0.5996068F;
            this.txtChikuNM.MultiLine = false;
            this.txtChikuNM.Name = "txtChikuNM";
            this.txtChikuNM.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtChikuNM.Text = "ＮＮＮＮＮＮＮＮ";
            this.txtChikuNM.Top = 0.01968504F;
            this.txtChikuNM.Width = 2.104331F;
            // 
            // lblDainari
            // 
            this.lblDainari.Height = 0.1582677F;
            this.lblDainari.HyperLink = null;
            this.lblDainari.Left = 0.3929133F;
            this.lblDainari.Name = "lblDainari";
            this.lblDainari.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.lblDainari.Text = "＜";
            this.lblDainari.Top = 0.01968504F;
            this.lblDainari.Width = 0.1685039F;
            // 
            // lblShounari
            // 
            this.lblShounari.Height = 0.1582677F;
            this.lblShounari.HyperLink = null;
            this.lblShounari.Left = 2.765355F;
            this.lblShounari.Name = "lblShounari";
            this.lblShounari.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.lblShounari.Text = "＞";
            this.lblShounari.Top = 0.01968504F;
            this.lblShounari.Width = 0.168504F;
            // 
            // gfSChiku
            // 
            this.gfSChiku.CanGrow = false;
            this.gfSChiku.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblChikuKei,
            this.txtSNissu,
            this.txtSGyoruiSuryo_01,
            this.txtSGyoruiKingaku_01,
            this.txtSGyoruiSuryo_02,
            this.txtSGyoruiKingaku_02,
            this.txtSGyoruiSuryo_03,
            this.txtSGyoruiKingaku_03,
            this.txtSGyoruiSuryo_04,
            this.txtSGyoruiKingaku_04,
            this.txtSGyoruiSuryo_05,
            this.txtSGyoruiKingaku_05,
            this.txtSGyoruiSuryo_06,
            this.txtSGyoruiKingaku_06,
            this.txtSGyoruiSuryo_07,
            this.txtSGyoruiKingaku_07,
            this.txtSGyoruiSuryo_08,
            this.txtSGyoruiKingaku_08,
            this.lnDeteilUnder});
            this.gfSChiku.Height = 0.3740157F;
            this.gfSChiku.KeepTogether = true;
            this.gfSChiku.Name = "gfSChiku";
            // 
            // lblChikuKei
            // 
            this.lblChikuKei.Height = 0.1582677F;
            this.lblChikuKei.HyperLink = null;
            this.lblChikuKei.Left = 1.429528F;
            this.lblChikuKei.Name = "lblChikuKei";
            this.lblChikuKei.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
            this.lblChikuKei.Text = "［地 区 計］";
            this.lblChikuKei.Top = 0.03937008F;
            this.lblChikuKei.Width = 0.8748025F;
            // 
            // txtSNissu
            // 
            this.txtSNissu.CanGrow = false;
            this.txtSNissu.DataField = "ITEM05";
            this.txtSNissu.Height = 0.1582677F;
            this.txtSNissu.Left = 2.643308F;
            this.txtSNissu.MultiLine = false;
            this.txtSNissu.Name = "txtSNissu";
            this.txtSNissu.OutputFormat = resources.GetString("txtSNissu.OutputFormat");
            this.txtSNissu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSNissu.SummaryGroup = "ghSChiku";
            this.txtSNissu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSNissu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSNissu.Text = "ZZZ,ZZZ";
            this.txtSNissu.Top = 0.03937008F;
            this.txtSNissu.Width = 0.515748F;
            // 
            // txtSGyoruiSuryo_01
            // 
            this.txtSGyoruiSuryo_01.CanGrow = false;
            this.txtSGyoruiSuryo_01.DataField = "ITEM06";
            this.txtSGyoruiSuryo_01.Height = 0.1582677F;
            this.txtSGyoruiSuryo_01.Left = 3.379528F;
            this.txtSGyoruiSuryo_01.MultiLine = false;
            this.txtSGyoruiSuryo_01.Name = "txtSGyoruiSuryo_01";
            this.txtSGyoruiSuryo_01.OutputFormat = resources.GetString("txtSGyoruiSuryo_01.OutputFormat");
            this.txtSGyoruiSuryo_01.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiSuryo_01.SummaryGroup = "ghSChiku";
            this.txtSGyoruiSuryo_01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiSuryo_01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiSuryo_01.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtSGyoruiSuryo_01.Top = 0.03937008F;
            this.txtSGyoruiSuryo_01.Width = 1.141732F;
            // 
            // txtSGyoruiKingaku_01
            // 
            this.txtSGyoruiKingaku_01.CanGrow = false;
            this.txtSGyoruiKingaku_01.DataField = "ITEM07";
            this.txtSGyoruiKingaku_01.Height = 0.1582677F;
            this.txtSGyoruiKingaku_01.Left = 3.379528F;
            this.txtSGyoruiKingaku_01.MultiLine = false;
            this.txtSGyoruiKingaku_01.Name = "txtSGyoruiKingaku_01";
            this.txtSGyoruiKingaku_01.OutputFormat = resources.GetString("txtSGyoruiKingaku_01.OutputFormat");
            this.txtSGyoruiKingaku_01.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiKingaku_01.SummaryGroup = "ghSChiku";
            this.txtSGyoruiKingaku_01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiKingaku_01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiKingaku_01.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSGyoruiKingaku_01.Top = 0.2066929F;
            this.txtSGyoruiKingaku_01.Width = 0.9330708F;
            // 
            // txtSGyoruiSuryo_02
            // 
            this.txtSGyoruiSuryo_02.CanGrow = false;
            this.txtSGyoruiSuryo_02.DataField = "ITEM08";
            this.txtSGyoruiSuryo_02.Height = 0.1582677F;
            this.txtSGyoruiSuryo_02.Left = 4.584252F;
            this.txtSGyoruiSuryo_02.MultiLine = false;
            this.txtSGyoruiSuryo_02.Name = "txtSGyoruiSuryo_02";
            this.txtSGyoruiSuryo_02.OutputFormat = resources.GetString("txtSGyoruiSuryo_02.OutputFormat");
            this.txtSGyoruiSuryo_02.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiSuryo_02.SummaryGroup = "ghSChiku";
            this.txtSGyoruiSuryo_02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiSuryo_02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiSuryo_02.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtSGyoruiSuryo_02.Top = 0.03937002F;
            this.txtSGyoruiSuryo_02.Width = 1.141732F;
            // 
            // txtSGyoruiKingaku_02
            // 
            this.txtSGyoruiKingaku_02.CanGrow = false;
            this.txtSGyoruiKingaku_02.DataField = "ITEM09";
            this.txtSGyoruiKingaku_02.Height = 0.1582677F;
            this.txtSGyoruiKingaku_02.Left = 4.584252F;
            this.txtSGyoruiKingaku_02.MultiLine = false;
            this.txtSGyoruiKingaku_02.Name = "txtSGyoruiKingaku_02";
            this.txtSGyoruiKingaku_02.OutputFormat = resources.GetString("txtSGyoruiKingaku_02.OutputFormat");
            this.txtSGyoruiKingaku_02.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiKingaku_02.SummaryGroup = "ghSChiku";
            this.txtSGyoruiKingaku_02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiKingaku_02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiKingaku_02.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSGyoruiKingaku_02.Top = 0.2066928F;
            this.txtSGyoruiKingaku_02.Width = 0.9330708F;
            // 
            // txtSGyoruiSuryo_03
            // 
            this.txtSGyoruiSuryo_03.CanGrow = false;
            this.txtSGyoruiSuryo_03.DataField = "ITEM10";
            this.txtSGyoruiSuryo_03.Height = 0.1582677F;
            this.txtSGyoruiSuryo_03.Left = 5.753544F;
            this.txtSGyoruiSuryo_03.MultiLine = false;
            this.txtSGyoruiSuryo_03.Name = "txtSGyoruiSuryo_03";
            this.txtSGyoruiSuryo_03.OutputFormat = resources.GetString("txtSGyoruiSuryo_03.OutputFormat");
            this.txtSGyoruiSuryo_03.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiSuryo_03.SummaryGroup = "ghSChiku";
            this.txtSGyoruiSuryo_03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiSuryo_03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiSuryo_03.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtSGyoruiSuryo_03.Top = 0.03937002F;
            this.txtSGyoruiSuryo_03.Width = 1.141732F;
            // 
            // txtSGyoruiKingaku_03
            // 
            this.txtSGyoruiKingaku_03.CanGrow = false;
            this.txtSGyoruiKingaku_03.DataField = "ITEM11";
            this.txtSGyoruiKingaku_03.Height = 0.1582677F;
            this.txtSGyoruiKingaku_03.Left = 5.753544F;
            this.txtSGyoruiKingaku_03.MultiLine = false;
            this.txtSGyoruiKingaku_03.Name = "txtSGyoruiKingaku_03";
            this.txtSGyoruiKingaku_03.OutputFormat = resources.GetString("txtSGyoruiKingaku_03.OutputFormat");
            this.txtSGyoruiKingaku_03.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiKingaku_03.SummaryGroup = "ghSChiku";
            this.txtSGyoruiKingaku_03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiKingaku_03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiKingaku_03.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSGyoruiKingaku_03.Top = 0.2066928F;
            this.txtSGyoruiKingaku_03.Width = 0.9330708F;
            // 
            // txtSGyoruiSuryo_04
            // 
            this.txtSGyoruiSuryo_04.CanGrow = false;
            this.txtSGyoruiSuryo_04.DataField = "ITEM12";
            this.txtSGyoruiSuryo_04.Height = 0.1582677F;
            this.txtSGyoruiSuryo_04.Left = 6.966142F;
            this.txtSGyoruiSuryo_04.MultiLine = false;
            this.txtSGyoruiSuryo_04.Name = "txtSGyoruiSuryo_04";
            this.txtSGyoruiSuryo_04.OutputFormat = resources.GetString("txtSGyoruiSuryo_04.OutputFormat");
            this.txtSGyoruiSuryo_04.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiSuryo_04.SummaryGroup = "ghSChiku";
            this.txtSGyoruiSuryo_04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiSuryo_04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiSuryo_04.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtSGyoruiSuryo_04.Top = 0.03937002F;
            this.txtSGyoruiSuryo_04.Width = 1.141732F;
            // 
            // txtSGyoruiKingaku_04
            // 
            this.txtSGyoruiKingaku_04.CanGrow = false;
            this.txtSGyoruiKingaku_04.DataField = "ITEM13";
            this.txtSGyoruiKingaku_04.Height = 0.1582677F;
            this.txtSGyoruiKingaku_04.Left = 6.966142F;
            this.txtSGyoruiKingaku_04.MultiLine = false;
            this.txtSGyoruiKingaku_04.Name = "txtSGyoruiKingaku_04";
            this.txtSGyoruiKingaku_04.OutputFormat = resources.GetString("txtSGyoruiKingaku_04.OutputFormat");
            this.txtSGyoruiKingaku_04.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiKingaku_04.SummaryGroup = "ghSChiku";
            this.txtSGyoruiKingaku_04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiKingaku_04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiKingaku_04.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSGyoruiKingaku_04.Top = 0.2066928F;
            this.txtSGyoruiKingaku_04.Width = 0.9330708F;
            // 
            // txtSGyoruiSuryo_05
            // 
            this.txtSGyoruiSuryo_05.CanGrow = false;
            this.txtSGyoruiSuryo_05.DataField = "ITEM14";
            this.txtSGyoruiSuryo_05.Height = 0.1582677F;
            this.txtSGyoruiSuryo_05.Left = 8.202363F;
            this.txtSGyoruiSuryo_05.MultiLine = false;
            this.txtSGyoruiSuryo_05.Name = "txtSGyoruiSuryo_05";
            this.txtSGyoruiSuryo_05.OutputFormat = resources.GetString("txtSGyoruiSuryo_05.OutputFormat");
            this.txtSGyoruiSuryo_05.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiSuryo_05.SummaryGroup = "ghSChiku";
            this.txtSGyoruiSuryo_05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiSuryo_05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiSuryo_05.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtSGyoruiSuryo_05.Top = 0.03937002F;
            this.txtSGyoruiSuryo_05.Width = 1.141732F;
            // 
            // txtSGyoruiKingaku_05
            // 
            this.txtSGyoruiKingaku_05.CanGrow = false;
            this.txtSGyoruiKingaku_05.DataField = "ITEM15";
            this.txtSGyoruiKingaku_05.Height = 0.1582677F;
            this.txtSGyoruiKingaku_05.Left = 8.202756F;
            this.txtSGyoruiKingaku_05.MultiLine = false;
            this.txtSGyoruiKingaku_05.Name = "txtSGyoruiKingaku_05";
            this.txtSGyoruiKingaku_05.OutputFormat = resources.GetString("txtSGyoruiKingaku_05.OutputFormat");
            this.txtSGyoruiKingaku_05.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiKingaku_05.SummaryGroup = "ghSChiku";
            this.txtSGyoruiKingaku_05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiKingaku_05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiKingaku_05.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSGyoruiKingaku_05.Top = 0.2066928F;
            this.txtSGyoruiKingaku_05.Width = 0.9330708F;
            // 
            // txtSGyoruiSuryo_06
            // 
            this.txtSGyoruiSuryo_06.CanGrow = false;
            this.txtSGyoruiSuryo_06.DataField = "ITEM16";
            this.txtSGyoruiSuryo_06.Height = 0.1582677F;
            this.txtSGyoruiSuryo_06.Left = 9.427166F;
            this.txtSGyoruiSuryo_06.MultiLine = false;
            this.txtSGyoruiSuryo_06.Name = "txtSGyoruiSuryo_06";
            this.txtSGyoruiSuryo_06.OutputFormat = resources.GetString("txtSGyoruiSuryo_06.OutputFormat");
            this.txtSGyoruiSuryo_06.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiSuryo_06.SummaryGroup = "ghSChiku";
            this.txtSGyoruiSuryo_06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiSuryo_06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiSuryo_06.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtSGyoruiSuryo_06.Top = 0.03937002F;
            this.txtSGyoruiSuryo_06.Width = 1.141732F;
            // 
            // txtSGyoruiKingaku_06
            // 
            this.txtSGyoruiKingaku_06.CanGrow = false;
            this.txtSGyoruiKingaku_06.DataField = "ITEM17";
            this.txtSGyoruiKingaku_06.Height = 0.1582677F;
            this.txtSGyoruiKingaku_06.Left = 9.427166F;
            this.txtSGyoruiKingaku_06.MultiLine = false;
            this.txtSGyoruiKingaku_06.Name = "txtSGyoruiKingaku_06";
            this.txtSGyoruiKingaku_06.OutputFormat = resources.GetString("txtSGyoruiKingaku_06.OutputFormat");
            this.txtSGyoruiKingaku_06.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiKingaku_06.SummaryGroup = "ghSChiku";
            this.txtSGyoruiKingaku_06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiKingaku_06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiKingaku_06.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSGyoruiKingaku_06.Top = 0.2066928F;
            this.txtSGyoruiKingaku_06.Width = 0.9330708F;
            // 
            // txtSGyoruiSuryo_07
            // 
            this.txtSGyoruiSuryo_07.CanGrow = false;
            this.txtSGyoruiSuryo_07.DataField = "ITEM18";
            this.txtSGyoruiSuryo_07.Height = 0.1582677F;
            this.txtSGyoruiSuryo_07.Left = 10.62992F;
            this.txtSGyoruiSuryo_07.MultiLine = false;
            this.txtSGyoruiSuryo_07.Name = "txtSGyoruiSuryo_07";
            this.txtSGyoruiSuryo_07.OutputFormat = resources.GetString("txtSGyoruiSuryo_07.OutputFormat");
            this.txtSGyoruiSuryo_07.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiSuryo_07.SummaryGroup = "ghSChiku";
            this.txtSGyoruiSuryo_07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiSuryo_07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiSuryo_07.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtSGyoruiSuryo_07.Top = 0.03937008F;
            this.txtSGyoruiSuryo_07.Width = 1.141732F;
            // 
            // txtSGyoruiKingaku_07
            // 
            this.txtSGyoruiKingaku_07.CanGrow = false;
            this.txtSGyoruiKingaku_07.DataField = "ITEM19";
            this.txtSGyoruiKingaku_07.Height = 0.1582677F;
            this.txtSGyoruiKingaku_07.Left = 10.62992F;
            this.txtSGyoruiKingaku_07.MultiLine = false;
            this.txtSGyoruiKingaku_07.Name = "txtSGyoruiKingaku_07";
            this.txtSGyoruiKingaku_07.OutputFormat = resources.GetString("txtSGyoruiKingaku_07.OutputFormat");
            this.txtSGyoruiKingaku_07.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiKingaku_07.SummaryGroup = "ghSChiku";
            this.txtSGyoruiKingaku_07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiKingaku_07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiKingaku_07.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSGyoruiKingaku_07.Top = 0.2066929F;
            this.txtSGyoruiKingaku_07.Width = 0.9330708F;
            // 
            // txtSGyoruiSuryo_08
            // 
            this.txtSGyoruiSuryo_08.CanGrow = false;
            this.txtSGyoruiSuryo_08.DataField = "ITEM20";
            this.txtSGyoruiSuryo_08.Height = 0.1582677F;
            this.txtSGyoruiSuryo_08.Left = 11.83819F;
            this.txtSGyoruiSuryo_08.MultiLine = false;
            this.txtSGyoruiSuryo_08.Name = "txtSGyoruiSuryo_08";
            this.txtSGyoruiSuryo_08.OutputFormat = resources.GetString("txtSGyoruiSuryo_08.OutputFormat");
            this.txtSGyoruiSuryo_08.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiSuryo_08.SummaryGroup = "ghSChiku";
            this.txtSGyoruiSuryo_08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiSuryo_08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiSuryo_08.Text = "ZZZ,ZZZ,ZZ9.99";
            this.txtSGyoruiSuryo_08.Top = 0.03937002F;
            this.txtSGyoruiSuryo_08.Width = 1.141732F;
            // 
            // txtSGyoruiKingaku_08
            // 
            this.txtSGyoruiKingaku_08.CanGrow = false;
            this.txtSGyoruiKingaku_08.DataField = "ITEM21";
            this.txtSGyoruiKingaku_08.Height = 0.1582677F;
            this.txtSGyoruiKingaku_08.Left = 11.83819F;
            this.txtSGyoruiKingaku_08.MultiLine = false;
            this.txtSGyoruiKingaku_08.Name = "txtSGyoruiKingaku_08";
            this.txtSGyoruiKingaku_08.OutputFormat = resources.GetString("txtSGyoruiKingaku_08.OutputFormat");
            this.txtSGyoruiKingaku_08.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGyoruiKingaku_08.SummaryGroup = "ghSChiku";
            this.txtSGyoruiKingaku_08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGyoruiKingaku_08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGyoruiKingaku_08.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSGyoruiKingaku_08.Top = 0.2066928F;
            this.txtSGyoruiKingaku_08.Width = 0.9330708F;
            // 
            // HNMR1101R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.2755905F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.40387F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghSChiku);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfSChiku);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtFromYearMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToYearMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptSenshuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptNissu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChikuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiSuryo_08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptGyoruiKingaku_08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNM8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNissu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiSuryo_08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoruiKingaku_08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGNissu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiSuryo_08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGGyoruiKingaku_08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChikuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDainari)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShounari)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChikuKei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSNissu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiSuryo_08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGyoruiKingaku_08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFromYearMonth;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblWave;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToYearMonth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrintDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptSenshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptNissu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiSuryo_01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiKingaku_01;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeaderUpper;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeaderUnder;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSenshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiSuryo_01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiKingaku_01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiSuryo_02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiSuryo_01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiKingaku_01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiSuryo_02;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghSChiku;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfSChiku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSNissu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiSuryo_01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiKingaku_01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiSuryo_02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNissu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblChikuNM;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoNM1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiSuryo_02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiKingaku_02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiSuryo_03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiKingaku_03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiSuryo_04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiKingaku_04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiSuryo_05;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiKingaku_05;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiSuryo_06;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiKingaku_06;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiSuryo_07;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiKingaku_07;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiSuryo_08;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptGyoruiKingaku_08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiKingaku_02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiSuryo_03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiKingaku_03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiSuryo_04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiKingaku_04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiSuryo_05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiKingaku_05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiSuryo_06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiKingaku_06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiSuryo_07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiKingaku_07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiSuryo_08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoruiKingaku_08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtChikuNM;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDainari;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShounari;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblChikuKei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiKingaku_02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiSuryo_03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiKingaku_03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiSuryo_04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiKingaku_04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiSuryo_05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiKingaku_05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiSuryo_06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiKingaku_06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiSuryo_07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiKingaku_07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiSuryo_08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGyoruiKingaku_08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoNM2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoNM3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoNM4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoNM5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoNM6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoNM7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoNM8;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnDeteilUnder;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGNissu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiSuryo_03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiSuryo_04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiSuryo_05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiSuryo_06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiSuryo_07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiSuryo_08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiKingaku_02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiKingaku_03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiKingaku_04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiKingaku_05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiKingaku_06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiKingaku_07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGGyoruiKingaku_08;
    }
}
