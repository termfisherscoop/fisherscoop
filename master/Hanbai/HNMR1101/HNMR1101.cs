﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hnrm1101
{
    /// <summary>
    /// メイン 地区別個人別業態別漁獲高月報(HNMR1101)   ※実は累計出力！
    /// </summary>
    public partial class HNMR1101 : BasePgForm
    {
        #region 定数
        public const int prtCols = 35;

        private const int RPT_NO_B4 = 2;
        private const int RPT_NO_A4 = 3;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        public string RPT_TYPE;
        public string RPT_NO;

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1101()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1"; //Uinfo.shishoCd;
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            RPT_TYPE = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "ReportType", "Type"));
            RPT_NO = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "ReportType", "No"));

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲前
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            // 日付範囲後
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        "1",
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        "1",
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F8キー押下時処理
        /// 帳票項目設定画面表示
        /// </summary>
        public override void PressF8()
        {
            // 魚種分類設定登録画面の起動
            EditGyoho(string.Empty);
        }
        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNMR1101R" });
            psForm.ShowDialog();
        }

        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckJpTo();
                SetJpTo();
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 月(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtDateMonthTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, "1", this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, "1", this.Dba);

            // 終了年月 ＜ 開始年月ならエラーにする（※SQL高速化の副作用で、入力チェックでエラーにしなければならない）
            if (tmpDateTo < tmpDateFr)
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                Msg.Error("開始年月より終了年月が古いです。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
        }

        /// <summary>
        /// 漁法を追加編集する
        /// </summary>
        /// <param name="code">漁法コード(空：新規登録、以外：編集)</param>
        private void EditGyoho(string code)
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            // アセンブリのロード
            asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNMR1091.exe");
            // フォーム作成
            t = asm.GetType("jp.co.fsi.hn.hnmr1091.HNMR1092");
            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    //frm.Par1 = "1";
                    frm.Par2 = this.txtMizuageShishoCd.Text;
                    frm.Par3 = RPT_TYPE;
                    frm.InData = RPT_NO;
                    frm.ShowDialog(this);
                    if (frm.DialogResult == DialogResult.OK)
                    {
                        result = (String[])frm.OutData;
                        // 開始年に再度フォーカスをセット
                        this.ActiveControl = this.txtDateYearFr;
                        this.txtDateYearFr.Focus();
                    }
                }
            }

            }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            bool dataFlag;
            try
            {
#if DEBUG
                //ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                HNMR1101R rpt = null;

                HNMR1101R rpt2 = null;


                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 印字日付設定
                    string printDateFr = string.Format("{0}{1}年{2}月", this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text);
                    string printDateTo = string.Format("{0}{1}年{2}月", this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text);
                    string[] todayDate = Util.ConvJpDate(DateTime.Now, this.Dba);
                    string today = string.Format("{0}{1}年{2}月{3}日", todayDate[0], todayDate[2], todayDate[3], todayDate[4]);

                    //表示名称の取得
                    int RPT_NO = (this.btnYoshiB4.Checked == true) ? RPT_NO_B4 : RPT_NO_A4;
                    //集計名称の取得
                    string wkColName = ViewName(RPT_NO);
                    string[] colNames = wkColName.Split(',');
                    int MainCount = (colNames.Length % 8) == 0 ? (colNames.Length / 8) : (colNames.Length / 8) + 1;
                    int ColCount = 6;
                    int wkCount = 0;
                    int ViewCount;
                    int NmCnt = 0;
                    int LastCnt = 0;
                    int repCnt = 1;

                    //if (btnYoshiB4.Checked)
                    //{
                        //全体は設定名称が９でレポート出力
                        for (int mainCnt = 1; mainCnt <= MainCount; mainCnt++)
                        {

                            // 取得列の定義
                            StringBuilder cols = new StringBuilder();
                            cols.Append("  ITEM01"); // 船主CD
                            cols.Append(" ,ITEM02"); // 地区CD
                            cols.Append(" ,ITEM03"); // 船主名称
                            cols.Append(" ,ITEM04"); // 地区名
                            cols.Append(" ,ITEM05"); // 日数
                            ViewCount = 6;
                            for (int i = ColCount; i < ColCount + 16; i++)
                            {
                                cols.Append(" ,ITEM" + Util.PadZero(i, 2) + " AS ITEM" + Util.PadZero(ViewCount, 2));
                                ViewCount++;
                                wkCount = i;
                            }
                            ColCount = wkCount + 1;
                            //列見出しの設定
                            for (int si = 1; si < 9; si++)
                            {
                                //名称数の範囲内で
                                if (NmCnt < colNames.Length -1 )
                                {
                                    string nm = colNames[NmCnt];
                                    if (nm != "")
                                    {
                                        //項目名称にアンダースコアがある場合は括弧に置き換える
                                        if (nm.IndexOf("_") > 0)
                                        {
                                            nm = nm.Replace("_", "(");
                                            nm = nm.TrimEnd('(');
                                            nm += ")";
                                        }
                                        cols.Append(" ,'" + nm + "' AS GYOHO_" + Util.PadZero(si, 2));
                                    }
                                    NmCnt++;
                                }
                                else
                                {
                                    if (mainCnt == MainCount && si == 8)
                                    {
                                        //最後のページの最後の項目は何もしない
                                    }else
                                    {
                                        //列見出しを空白で更新
                                        cols.Append(" ,'' AS GYOHO_" + Util.PadZero(si, 2));
                                    }
                                }
                                LastCnt = si;
                            }
                            if (NmCnt == colNames.Length -1)
                            {
                                cols.Append(" ,'☆合計☆' AS GYOHO_" + Util.PadZero(LastCnt, 2));
                            }
                            cols.Append(" ,'" + printDateFr + "' AS printDateFr"); // 印刷対象日（開始）
                            cols.Append(" ,'" + printDateTo + "' AS printDateTo"); // 印刷対象日（終了）
                            cols.Append(" ,'" + today + "' AS today"); // 印刷実行日

                            // バインドパラメータの設定
                            DbParamCollection dpc = new DbParamCollection();
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                            if (mainCnt == 1)
                            {
                                // データの取得
                                DataTable dtOutput1 = this.Dba.GetDataTableByConditionWithParams(
                                    Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                            // 帳票オブジェクトをインスタンス化

                            rpt = new HNMR1101R(dtOutput1);
                            //rpt.Document.Printer.DocumentName = this.lblTitle.Text + " (" + repCnt + ")";
                            // TODO 2018-11-30 ファイル名を()なしにする

                            rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                                //rpt.Document.Name = this.lblTitle.Text + " (" + repCnt + ")";
                                rpt.Document.Name = this.lblTitle.Text;

                            }

                            else if (mainCnt == 2)
                            {
                                // データの取得
                                DataTable dtOutput2 = this.Dba.GetDataTableByConditionWithParams(
                                    Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                                // 帳票オブジェクトをインスタンス化
                                rpt2 = new HNMR1101R(dtOutput2);
                                rpt2.Document.Printer.DocumentName = this.lblTitle.Text + " (" + repCnt + ")";
                                rpt2.Document.Name = this.lblTitle.Text + " (" + repCnt + ")";

                                if (isExcel)
                                {
                                    rpt.Run(false);
                                    rpt2.Run(false);

                                    rpt.Document.Pages.AddRange(rpt2.Document.Pages);

                                    var font = new System.Drawing.Font("ＭＳ 明朝", 10);


                                    Single x = rpt.PageSettings.Margins.Left + 5.89F;
                                    Single y = rpt.PageSettings.Margins.Top;
                                    Single width = 7.38F;
                                    Single height = 0.19F;
                                    var drawRect = new System.Drawing.RectangleF(x, y, width, height);

                                    for (int i = 0; i < rpt.Document.Pages.Count; i++)
                                    {
                                        rpt.Document.Pages[i].ForeColor = System.Drawing.Color.Black;
                                        rpt.Document.Pages[i].Font = font;
                                        rpt.Document.Pages[i].TextAlignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                                        rpt.Document.Pages[i].VerticalTextAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Middle;

                                        rpt.Document.Pages[i].DrawText((i + 1).ToString() + "頁", drawRect);

                                    }



                                    GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                                    //SetExcelSetting(xlsExport1);
                                    rpt.Run();



                                    string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                                    if (!ValChk.IsEmpty(saveFileName))
                                    {
                                        xlsExport1.Export(rpt.Document, saveFileName);
                                        Msg.InfoNm("EXCEL出力", "保存しました。");
                                        Util.OpenFolder(saveFileName);
                                    }
                                }
                                else if (isPdf)
                                {
                                    GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();

                                    rpt.Run(false);
                                    rpt2.Run(false);

                                    rpt.Document.Pages.AddRange(rpt2.Document.Pages);

                                    var font = new System.Drawing.Font("ＭＳ 明朝", 10);

                                    Single x = rpt.PageSettings.Margins.Left + 5.00F;
                                    Single y = rpt.PageSettings.Margins.Top;
                                    Single width = 7.38F;
                                    Single height = 0.19F;
                                    var drawRect = new System.Drawing.RectangleF(x, y, width, height);

                                    string str = "頁";


                                    for (int i = 0; i < rpt.Document.Pages.Count; i++)
                                    {
                                        rpt.Document.Pages[i].ForeColor = System.Drawing.Color.Black;
                                        rpt.Document.Pages[i].Font = font;
                                        rpt.Document.Pages[i].TextAlignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                                        rpt.Document.Pages[i].VerticalTextAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Middle;

                                        rpt.Document.Pages[i].DrawText((i + 1).ToString() + str, drawRect);

                                    }

                                    string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                                    if (!ValChk.IsEmpty(saveFileName))
                                    {
                                        p.Export(rpt.Document, saveFileName);
                                        Msg.InfoNm("PDF出力", "保存しました。");
                                        Util.OpenFolder(saveFileName);
                                    }
                                }
                                else if (isPreview)
                                {
                                     PreviewForm pFrm = null;
                                    // プレビュー画面表示
                                    if (1 == mainCnt)
                                    {
                                        pFrm = new PreviewForm(rpt, this.UnqId);

                                    }
                                    else
                                    {
                                        pFrm = new PreviewForm(rpt, rpt2, this.UnqId, "1");

                                    }


                                    pFrm.WindowState = FormWindowState.Maximized;
                                    pFrm.Show();
                                }
                                else
                                {
                                    // 直接印刷
                                    rpt.Run(false);

                                    if (null != rpt2)
                                    {
                                        rpt2.Run(false);
                                        rpt.Document.Pages.AddRange(rpt2.Document.Pages);


                                        var font = new System.Drawing.Font("ＭＳ 明朝", 10);

                                        Single x = rpt.PageSettings.Margins.Left + 5.89F;
                                        Single y = rpt.PageSettings.Margins.Top;
                                        Single width = 7.38F;
                                        Single height = 0.19F;
                                        var drawRect = new System.Drawing.RectangleF(x, y, width, height);

                                        string str = "頁";


                                        for (int i = 0; i < rpt.Document.Pages.Count; i++)
                                        {
                                            rpt.Document.Pages[i].ForeColor = System.Drawing.Color.Black;
                                            rpt.Document.Pages[i].Font = font;
                                            rpt.Document.Pages[i].TextAlignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                                            rpt.Document.Pages[i].VerticalTextAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Middle;

                                            rpt.Document.Pages[i].DrawText((i + 1).ToString() + str, drawRect);

                                        }

                                    }

                                    rpt.Document.Print(true, true, false);

                                }
                            }
                            repCnt++;
                        }
                    }
//                    else
 //                   {
                        //// ２つ目
                        //// 取得列の定義
                        //StringBuilder cols = new StringBuilder();
                        //cols.Clear();
                        //cols.Append("  ITEM01"); // 船主CD
                        //cols.Append(" ,ITEM02"); // 地区CD
                        //cols.Append(" ,ITEM03"); // 船主名称
                        //cols.Append(" ,ITEM04"); // 地区名
                        //cols.Append(" ,CONVERT(decimal(9, 0), ITEM05) AS ITEM05"); // 日数
                        //cols.Append(" ,CONVERT(decimal(9, 2), ITEM22) AS ITEM06"); // 素潜り数量
                        //cols.Append(" ,CONVERT(decimal(9, 0), ITEM23) AS ITEM07"); // 素潜り金額
                        //cols.Append(" ,CONVERT(decimal(9, 2), ITEM24) AS ITEM08"); // 近海鮪延縄数量
                        //cols.Append(" ,CONVERT(decimal(9, 0), ITEM25) AS ITEM09"); // 近海鮪延縄金額
                        //cols.Append(" ,CONVERT(decimal(9, 2), ITEM26) AS ITEM10"); // 曳網（パヤオ）数量
                        //cols.Append(" ,CONVERT(decimal(9, 0), ITEM27) AS ITEM11"); // 曳網（パヤオ）金額
                        //cols.Append(" ,CONVERT(decimal(9, 2), ITEM28) AS ITEM12"); // 延縄数量
                        //cols.Append(" ,CONVERT(decimal(9, 0), ITEM29) AS ITEM13"); // 延縄金額
                        //cols.Append(" ,CONVERT(decimal(9, 2), ITEM30) AS ITEM14"); // その他漁法数量
                        //cols.Append(" ,CONVERT(decimal(9, 0), ITEM31) AS ITEM15"); // その他漁法金額
                        //cols.Append(" ,CONVERT(decimal(9, 2), ITEM32) AS ITEM16"); // 数量合計
                        //cols.Append(" ,CONVERT(decimal(9, 0), ITEM33) AS ITEM17"); // 金額合計
                        //cols.Append(" ,'素潜り' AS GYOHO_01"); // 素潜り見出し
                        //cols.Append(" ,'近海鮪延縄' AS GYOHO_02"); // 近海鮪延縄見出し
                        //cols.Append(" ,'曳網（パヤオ）' AS GYOHO_03"); // 曳網（パヤオ）見出し
                        //cols.Append(" ,'延縄' AS GYOHO_04"); // エビ類見出し
                        //cols.Append(" ,'その他漁法' AS GYOHO_05"); // その他漁法見出し
                        //cols.Append(" ,'合計' AS GYOHO_06"); // 合計見出し
                        //cols.Append(" ,'" + printDateFr + "' AS printDateFr"); // 印刷対象日（開始）
                        //cols.Append(" ,'" + printDateTo + "' AS printDateTo"); // 印刷対象日（終了）
                        //cols.Append(" ,'" + today + "' AS today"); // 印刷実行日

                        //// バインドパラメータの設定は、１回目と同じ
                        //DbParamCollection dpc = new DbParamCollection();
                        //dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                        //// データの取得
                        //DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        //    Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                        //// ２つ目の帳票オブジェクトをインスタンス化
                        //HNMR1102R rpt2 = new HNMR1102R(dtOutput);
                        //rpt2.Document.Printer.DocumentName = this.Text;
                        //rpt2.Document.Name = this.Text;

                        //if (isPreview)
                        //{
                        //    // プレビュー画面表示
                        //    PreviewForm pFrm = new PreviewForm(rpt2, this.UnqId);
                        //    pFrm.WindowState = FormWindowState.Maximized;
                        //    pFrm.Show();
                        //}
                        //else
                        //{
                        //    // 直接印刷
                        //    rpt2.Run(false);
                        //    rpt2.Document.Print(true, true, false);
                        //}
   //                 }
                //}
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                // 印刷後なので、帳票出力用ワークテーブルをロールバックで元に戻す
                this.Dba.Rollback();
#endif       
            }

        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, "1", this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, "1", this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            //レポート番号
            int RPT_NO = (btnYoshiB4.Checked == true) ? RPT_NO_B4 : RPT_NO_A4;
            //列名退避
            string ColName = "";
            string PivotName = "";
            string colNm = "";
            int ColsCount = 0;
            //設定テーブルから名称を取得
            DbParamCollection dps = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            Sql.Append(" SELECT * FROM TB_HN_RPT_RETSU_KBN ");
            Sql.Append(" WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append(" RPT_TYPE = @RPT_TYPE AND ");
            Sql.Append(" RPT_NO = @RPT_NO ");
            Sql.Append(" ORDER BY");
            Sql.Append(" RETSU_NO");
            dps.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dps.SetParam("@RPT_TYPE", SqlDbType.Int, 1, RPT_TYPE);
            dps.SetParam("@RPT_NO", SqlDbType.Int, 1, RPT_NO);
            DataTable dtSettei = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dps);
            if (dtSettei.Rows.Count != 0)
            {
                ColsCount = dtSettei.Rows.Count;
                foreach (DataRow dr in dtSettei.Rows)
                {
                    if (ColName != "")
                    {
                        ColName += ",";
                        PivotName += ",";
                    }
                    ColName += " B." + dr["RETSU_NM"] + " AS " + dr["RETSU_NM"] + "数量,";
                    ColName += " C." + dr["RETSU_NM"] + " AS " + dr["RETSU_NM"] + "金額 ";
                    if (colNm.Length == 0)
                    {

                    }
                    colNm += dr["RETSU_NM"] + "数量,";
                    colNm += dr["RETSU_NM"] + "金額,";
                    PivotName += " [" + dr["RETSU_NM"] + "]";
                }
            }
            else
            {
                Msg.Notice("帳票項目設定がされていません、項目設定を行って下さい。");
                return false;
            }
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            Sql = new StringBuilder();

            //// han.VI_地区別個人別魚類別漁獲高月報(VI_HN_GYORUIBETSU_GKKDK_GKKI)の日付範囲に該当するデータを取得
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 3);
            dpc.SetParam("@RPT_TYPE", SqlDbType.Decimal, 4, RPT_TYPE);
            dpc.SetParam("@RPT_NO", SqlDbType.Decimal, 4, RPT_NO);
            dpc.SetParam("@DATE_FR_TSUKI", SqlDbType.DateTime, 10, tmpDateFr.Date);
            dpc.SetParam("@DATE_TO_TSUKI", SqlDbType.DateTime, 10, tmpDateTo2.Date);

            DataTable dtMainLoop =
                this.Dba.GetDataTableFromSqlWithParams("EXEC GYOTAIBETSU_GKKDK_GKI @KAISHA_CD, @SHISHO_CD, @RPT_TYPE, @RPT_NO, @DATE_FR_TSUKI, @DATE_TO_TSUKI, @DENPYO_KUBUN ", dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }

            int i = 0; // SORT順

            foreach (DataRow dr in dtMainLoop.Rows)
            {
                Sql = new StringBuilder();
                dpc = new DbParamCollection();

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr["CHIKU_CD"]);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dr["SENSHU_CD"]);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["CHIKU_NM"]);
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SENSHU_NM"]);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["NISSU"]);
                int idx = 6;
                colNm = colNm.TrimEnd(',');
                string[] paramName = colNm.Split(',');
                foreach (string nm in paramName)
                {
                    if (nm != "")
                    {
                        dpc.SetParam("@ITEM" + Util.PadZero(idx, 2), SqlDbType.VarChar, 200, Util.ToDecimal(Util.ToString(dr[nm])));
                        idx++;
                    }
                }
                int pageNum = (paramName.Length  / 16) + 1;
                int maxCount = idx +  (pageNum * 16) - paramName.Length -2;
                for (int ri = idx; ri < maxCount; ri++)
                {
                    dpc.SetParam("@ITEM" + Util.PadZero(ri, 2), SqlDbType.VarChar, 200, 0);
                    idx++;
                }
                dpc.SetParam("@ITEM" + Util.ToString(Util.PadZero(idx, 2)) , SqlDbType.VarChar, 200, dr["SURYO_GOKEI"]);
                idx++;
                dpc.SetParam("@ITEM" + Util.ToString(Util.PadZero(idx, 2)), SqlDbType.VarChar, 200, dr["KINGAKU_GOKEI"]);
                idx++;
                dpc.SetParam("@ITEM" + Util.ToString(Util.PadZero(idx, 2)), SqlDbType.VarChar, 200, dr["SHISHO_CD"]);
                idx++;
                dpc.SetParam("@ITEM" + Util.ToString(Util.PadZero(idx, 2)), SqlDbType.VarChar, 200, dr["SHISHO_NM"]);

                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ," + Util.ColsArray(idx, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(idx, "@"));
                Sql.Append(") ");

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "count(*) AS rows",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (Util.ToInt(tmpdtPR_HN_TBL.Rows[0].ItemArray[0]) > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;

        }

        private string ViewName(int RPT_NO)
        {
            string viewName = "";

            DbParamCollection dps = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            Sql.Append(" SELECT * FROM TB_HN_RPT_RETSU_KBN ");
            Sql.Append(" WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append(" RPT_TYPE = @RPT_TYPE AND ");
            Sql.Append(" RPT_NO = @RPT_NO");
            Sql.Append(" ORDER BY");
            Sql.Append(" RETSU_NO");
            dps.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dps.SetParam("@RPT_TYPE", SqlDbType.Int, 1, RPT_TYPE);
            dps.SetParam("@RPT_NO", SqlDbType.Int, 1, RPT_NO);
            DataTable dtSettei = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dps);
            if (dtSettei.Rows.Count != 0)
            {
                foreach (DataRow dr in dtSettei.Rows)
                {
                    viewName += dr["RETSU_NM"] + ",";
                }

            }
            return viewName;
        }

        #endregion

        #region 用紙変更
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_CheckedChanged(object sender, EventArgs e)
        {

            if (((RadioButton)sender).Name == "btnYoshiB4")
            {
                if (((RadioButton)sender).Checked)
                {
                    RPT_NO = RPT_NO_B4.ToString();
                }
            }
            else
            {
                if (((RadioButton)sender).Checked)
                {
                    RPT_NO = RPT_NO_A4.ToString();
                }
            }
        }
        #endregion
    }
}
