﻿namespace jp.co.fsi.hn.hnrm1101
{
    partial class HNMR1101
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblCodeBetDate = new System.Windows.Forms.Label();
			this.lblDateMonthTo = new System.Windows.Forms.Label();
			this.lblDateYearTo = new System.Windows.Forms.Label();
			this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoTo = new System.Windows.Forms.Label();
			this.lblDateMonthFr = new System.Windows.Forms.Label();
			this.lblDateYearFr = new System.Windows.Forms.Label();
			this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoFr = new System.Windows.Forms.Label();
			this.btnYoshiA4 = new System.Windows.Forms.RadioButton();
			this.btnYoshiB4 = new System.Windows.Forms.RadioButton();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF4
			// 
			this.btnF4.Text = "F4\r\n\r\nプレビュー";
			// 
			// btnF7
			// 
			this.btnF7.Text = "F7EXCEL出力";
			// 
			// btnF6
			// 
			this.btnF6.Text = "F6\r\n\r\nPDF出力";
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 603);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1124, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1113, 41);
			this.lblTitle.Text = "地区別個人別業態別漁獲高月報";
			// 
			// lblCodeBetDate
			// 
			this.lblCodeBetDate.AutoSize = true;
			this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
			this.lblCodeBetDate.Location = new System.Drawing.Point(309, 8);
			this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblCodeBetDate.Name = "lblCodeBetDate";
			this.lblCodeBetDate.Size = new System.Drawing.Size(24, 16);
			this.lblCodeBetDate.TabIndex = 902;
			this.lblCodeBetDate.Tag = "CHANGE";
			this.lblCodeBetDate.Text = "～";
			this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblDateMonthTo
			// 
			this.lblDateMonthTo.AutoSize = true;
			this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateMonthTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateMonthTo.Location = new System.Drawing.Point(549, 8);
			this.lblDateMonthTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateMonthTo.Name = "lblDateMonthTo";
			this.lblDateMonthTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthTo.TabIndex = 13;
			this.lblDateMonthTo.Tag = "CHANGE";
			this.lblDateMonthTo.Text = "月";
			this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateYearTo
			// 
			this.lblDateYearTo.AutoSize = true;
			this.lblDateYearTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateYearTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateYearTo.Location = new System.Drawing.Point(479, 8);
			this.lblDateYearTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateYearTo.Name = "lblDateYearTo";
			this.lblDateYearTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateYearTo.TabIndex = 11;
			this.lblDateYearTo.Tag = "CHANGE";
			this.lblDateYearTo.Text = "年";
			this.lblDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateYearTo
			// 
			this.txtDateYearTo.AutoSizeFromLength = false;
			this.txtDateYearTo.DisplayLength = null;
			this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateYearTo.Location = new System.Drawing.Point(436, 3);
			this.txtDateYearTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYearTo.MaxLength = 2;
			this.txtDateYearTo.Name = "txtDateYearTo";
			this.txtDateYearTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearTo.TabIndex = 10;
			this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
			// 
			// txtDateMonthTo
			// 
			this.txtDateMonthTo.AutoSizeFromLength = false;
			this.txtDateMonthTo.DisplayLength = null;
			this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateMonthTo.Location = new System.Drawing.Point(507, 3);
			this.txtDateMonthTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateMonthTo.MaxLength = 2;
			this.txtDateMonthTo.Name = "txtDateMonthTo";
			this.txtDateMonthTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthTo.TabIndex = 12;
			this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDateMonthTo_KeyDown);
			this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
			// 
			// lblDateGengoTo
			// 
			this.lblDateGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengoTo.Location = new System.Drawing.Point(376, 2);
			this.lblDateGengoTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengoTo.Name = "lblDateGengoTo";
			this.lblDateGengoTo.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengoTo.TabIndex = 8;
			this.lblDateGengoTo.Tag = "DISPNAME";
			this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateMonthFr
			// 
			this.lblDateMonthFr.AutoSize = true;
			this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateMonthFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateMonthFr.Location = new System.Drawing.Point(268, 8);
			this.lblDateMonthFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateMonthFr.Name = "lblDateMonthFr";
			this.lblDateMonthFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthFr.TabIndex = 5;
			this.lblDateMonthFr.Tag = "CHANGE";
			this.lblDateMonthFr.Text = "月";
			this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateYearFr
			// 
			this.lblDateYearFr.AutoSize = true;
			this.lblDateYearFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateYearFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateYearFr.Location = new System.Drawing.Point(197, 8);
			this.lblDateYearFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateYearFr.Name = "lblDateYearFr";
			this.lblDateYearFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateYearFr.TabIndex = 3;
			this.lblDateYearFr.Tag = "CHANGE";
			this.lblDateYearFr.Text = "年";
			this.lblDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateYearFr
			// 
			this.txtDateYearFr.AutoSizeFromLength = false;
			this.txtDateYearFr.DisplayLength = null;
			this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateYearFr.Location = new System.Drawing.Point(155, 3);
			this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYearFr.MaxLength = 2;
			this.txtDateYearFr.Name = "txtDateYearFr";
			this.txtDateYearFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearFr.TabIndex = 2;
			this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
			// 
			// txtDateMonthFr
			// 
			this.txtDateMonthFr.AutoSizeFromLength = false;
			this.txtDateMonthFr.DisplayLength = null;
			this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateMonthFr.Location = new System.Drawing.Point(225, 3);
			this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateMonthFr.MaxLength = 2;
			this.txtDateMonthFr.Name = "txtDateMonthFr";
			this.txtDateMonthFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthFr.TabIndex = 4;
			this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
			// 
			// lblDateGengoFr
			// 
			this.lblDateGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengoFr.Location = new System.Drawing.Point(95, 2);
			this.lblDateGengoFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengoFr.Name = "lblDateGengoFr";
			this.lblDateGengoFr.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengoFr.TabIndex = 1;
			this.lblDateGengoFr.Tag = "DISPNAME";
			this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnYoshiA4
			// 
			this.btnYoshiA4.AutoSize = true;
			this.btnYoshiA4.BackColor = System.Drawing.Color.Silver;
			this.btnYoshiA4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnYoshiA4.Location = new System.Drawing.Point(200, 5);
			this.btnYoshiA4.Margin = new System.Windows.Forms.Padding(5);
			this.btnYoshiA4.Name = "btnYoshiA4";
			this.btnYoshiA4.Size = new System.Drawing.Size(58, 20);
			this.btnYoshiA4.TabIndex = 1;
			this.btnYoshiA4.Tag = "CHANGE";
			this.btnYoshiA4.Text = "Ａ４";
			this.btnYoshiA4.UseVisualStyleBackColor = false;
			this.btnYoshiA4.CheckedChanged += new System.EventHandler(this.btn_CheckedChanged);
			// 
			// btnYoshiB4
			// 
			this.btnYoshiB4.AutoSize = true;
			this.btnYoshiB4.BackColor = System.Drawing.Color.Silver;
			this.btnYoshiB4.Checked = true;
			this.btnYoshiB4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnYoshiB4.Location = new System.Drawing.Point(95, 5);
			this.btnYoshiB4.Margin = new System.Windows.Forms.Padding(5);
			this.btnYoshiB4.Name = "btnYoshiB4";
			this.btnYoshiB4.Size = new System.Drawing.Size(58, 20);
			this.btnYoshiB4.TabIndex = 0;
			this.btnYoshiB4.TabStop = true;
			this.btnYoshiB4.Tag = "CHANGE";
			this.btnYoshiB4.Text = "Ｂ４";
			this.btnYoshiB4.UseVisualStyleBackColor = false;
			this.btnYoshiB4.CheckedChanged += new System.EventHandler(this.btn_CheckedChanged);
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(95, 3);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(144, 2);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 32);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(889, 29);
			this.lblMizuageShisho.TabIndex = 0;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "水揚支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(899, 117);
			this.fsiTableLayoutPanel1.TabIndex = 903;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.btnYoshiA4);
			this.fsiPanel3.Controls.Add(this.btnYoshiB4);
			this.fsiPanel3.Controls.Add(this.label2);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 82);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(889, 30);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(889, 30);
			this.label2.TabIndex = 0;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "用紙";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblDateGengoFr);
			this.fsiPanel2.Controls.Add(this.lblCodeBetDate);
			this.fsiPanel2.Controls.Add(this.txtDateMonthFr);
			this.fsiPanel2.Controls.Add(this.txtDateYearFr);
			this.fsiPanel2.Controls.Add(this.lblDateMonthTo);
			this.fsiPanel2.Controls.Add(this.lblDateYearFr);
			this.fsiPanel2.Controls.Add(this.lblDateYearTo);
			this.fsiPanel2.Controls.Add(this.lblDateMonthFr);
			this.fsiPanel2.Controls.Add(this.txtDateYearTo);
			this.fsiPanel2.Controls.Add(this.lblDateGengoTo);
			this.fsiPanel2.Controls.Add(this.txtDateMonthTo);
			this.fsiPanel2.Controls.Add(this.label1);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 43);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(889, 30);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(889, 30);
			this.label1.TabIndex = 0;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "日付範囲";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(889, 29);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNMR1101
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1113, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNMR1101";
			this.Text = "";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label lblDateYearTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.RadioButton btnYoshiA4;
        private System.Windows.Forms.RadioButton btnYoshiB4;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label1;
        private common.FsiPanel fsiPanel1;
    }
}