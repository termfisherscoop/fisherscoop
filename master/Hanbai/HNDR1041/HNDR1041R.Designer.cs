﻿namespace jp.co.fsi.hn.hndr1041
{
    /// <summary>
    /// HNDR1041R の概要の説明です。
    /// </summary>
    partial class HNDR1041R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNDR1041R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.直線54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.テキスト12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル0,
            this.ラベル1,
            this.ラベル2,
            this.ラベル3,
            this.ラベル4,
            this.ラベル5,
            this.ラベル6,
            this.ラベル7,
            this.ラベル8,
            this.ラベル9,
            this.ラベル10,
            this.ラベル11,
            this.直線13,
            this.ラベル19,
            this.reportInfo1,
            this.textBox1,
            this.テキスト01});
            this.pageHeader.Height = 0.8422245F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.2291667F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 4.033859F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "* * * 　個人別鮮魚代金明細兼請求書  * * *";
            this.ラベル0.Top = 0F;
            this.ラベル0.Width = 4.604167F;
            // 
            // ラベル1
            // 
            this.ラベル1.Height = 0.2236221F;
            this.ラベル1.HyperLink = null;
            this.ラベル1.Left = 0.07755906F;
            this.ラベル1.Name = "ラベル1";
            this.ラベル1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.ラベル1.Tag = "";
            this.ラベル1.Text = "セリ日：";
            this.ラベル1.Top = 0.2291339F;
            this.ラベル1.Width = 1.112489F;
            // 
            // ラベル2
            // 
            this.ラベル2.Height = 0.2433071F;
            this.ラベル2.HyperLink = null;
            this.ラベル2.Left = 0.07755906F;
            this.ラベル2.Name = "ラベル2";
            this.ラベル2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル2.Tag = "";
            this.ラベル2.Text = "仲買人CD";
            this.ラベル2.Top = 0.4791339F;
            this.ラベル2.Width = 0.826378F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.2433071F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 0.9649607F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "略称仲買人名";
            this.ラベル3.Top = 0.4791339F;
            this.ラベル3.Width = 1.463779F;
            // 
            // ラベル4
            // 
            this.ラベル4.Height = 0.2433071F;
            this.ラベル4.HyperLink = null;
            this.ラベル4.Left = 3.909055F;
            this.ラベル4.Name = "ラベル4";
            this.ラベル4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: center; ddo-char-set: 128";
            this.ラベル4.Tag = "";
            this.ラベル4.Text = "件数";
            this.ラベル4.Top = 0.4791339F;
            this.ラベル4.Width = 0.4480309F;
            // 
            // ラベル5
            // 
            this.ラベル5.Height = 0.2433071F;
            this.ラベル5.HyperLink = null;
            this.ラベル5.Left = 4.633071F;
            this.ラベル5.Name = "ラベル5";
            this.ラベル5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル5.Tag = "";
            this.ラベル5.Text = "買上数量";
            this.ラベル5.Top = 0.4791339F;
            this.ラベル5.Width = 0.9311023F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.2433071F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 5.620079F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "単価";
            this.ラベル6.Top = 0.4791339F;
            this.ラベル6.Width = 0.6047244F;
            // 
            // ラベル7
            // 
            this.ラベル7.Height = 0.2433071F;
            this.ラベル7.HyperLink = null;
            this.ラベル7.Left = 6.295276F;
            this.ラベル7.Name = "ラベル7";
            this.ラベル7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル7.Tag = "";
            this.ラベル7.Text = "買上金額";
            this.ラベル7.Top = 0.4791339F;
            this.ラベル7.Width = 0.8480315F;
            // 
            // ラベル8
            // 
            this.ラベル8.Height = 0.2433071F;
            this.ラベル8.HyperLink = null;
            this.ラベル8.Left = 7.345276F;
            this.ラベル8.Name = "ラベル8";
            this.ラベル8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル8.Tag = "";
            this.ラベル8.Text = "消費税";
            this.ラベル8.Top = 0.4791339F;
            this.ラベル8.Width = 0.6401577F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.2696851F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 8.346457F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "税込金額";
            this.ラベル9.Top = 0.4791339F;
            this.ラベル9.Width = 0.9566097F;
            // 
            // ラベル10
            // 
            this.ラベル10.Height = 0.2433071F;
            this.ラベル10.HyperLink = null;
            this.ラベル10.Left = 10.01536F;
            this.ラベル10.Name = "ラベル10";
            this.ラベル10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル10.Tag = "";
            this.ラベル10.Text = "前回請求額";
            this.ラベル10.Top = 0.4791339F;
            this.ラベル10.Width = 1.183859F;
            // 
            // ラベル11
            // 
            this.ラベル11.Height = 0.2433071F;
            this.ラベル11.HyperLink = null;
            this.ラベル11.Left = 11.59488F;
            this.ラベル11.Name = "ラベル11";
            this.ラベル11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル11.Tag = "";
            this.ラベル11.Text = "合計請求額";
            this.ラベル11.Top = 0.4791339F;
            this.ラベル11.Width = 1.132283F;
            // 
            // 直線13
            // 
            this.直線13.Height = 0.004467487F;
            this.直線13.Left = 0.04026699F;
            this.直線13.LineWeight = 2F;
            this.直線13.Name = "直線13";
            this.直線13.Tag = "";
            this.直線13.Top = 0.7856901F;
            this.直線13.Width = 13.2302F;
            this.直線13.X1 = 0.04026699F;
            this.直線13.X2 = 13.27047F;
            this.直線13.Y1 = 0.7856901F;
            this.直線13.Y2 = 0.7901576F;
            // 
            // ラベル19
            // 
            this.ラベル19.Height = 0.2236221F;
            this.ラベル19.HyperLink = null;
            this.ラベル19.Left = 12.86417F;
            this.ラベル19.Name = "ラベル19";
            this.ラベル19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.ラベル19.Tag = "";
            this.ラベル19.Text = "頁";
            this.ラベル19.Top = 0.005511811F;
            this.ラベル19.Width = 0.4063005F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.2291339F;
            this.reportInfo1.Left = 11.02756F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal";
            this.reportInfo1.Top = 6.053597E-09F;
            this.reportInfo1.Width = 1.29882F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.25F;
            this.textBox1.Left = 12.32638F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-align: right";
            this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox1.Text = "999";
            this.textBox1.Top = 6.053597E-09F;
            this.textBox1.Width = 0.5102358F;
            // 
            // テキスト01
            // 
            this.テキスト01.DataField = "ITEM01";
            this.テキスト01.Height = 0.2501312F;
            this.テキスト01.Left = 0.903937F;
            this.テキスト01.Name = "テキスト01";
            this.テキスト01.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト01.Tag = "";
            this.テキスト01.Text = "ITEM01";
            this.テキスト01.Top = 0.2291339F;
            this.テキスト01.Width = 1.18125F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト02,
            this.テキスト03,
            this.テキスト04,
            this.テキスト05,
            this.テキスト06,
            this.テキスト07,
            this.テキスト08,
            this.テキスト09,
            this.テキスト10,
            this.テキスト11,
            this.ラベル52,
            this.textBox2});
            this.detail.Height = 0.3929134F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // テキスト02
            // 
            this.テキスト02.DataField = "ITEM02";
            this.テキスト02.Height = 0.2082677F;
            this.テキスト02.Left = 0.2236221F;
            this.テキスト02.Name = "テキスト02";
            this.テキスト02.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 128";
            this.テキスト02.Tag = "";
            this.テキスト02.Text = "ITEM02";
            this.テキスト02.Top = 0F;
            this.テキスト02.Width = 0.680315F;
            // 
            // テキスト03
            // 
            this.テキスト03.DataField = "ITEM03";
            this.テキスト03.Height = 0.2082677F;
            this.テキスト03.Left = 0.9649607F;
            this.テキスト03.Name = "テキスト03";
            this.テキスト03.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト03.Tag = "";
            this.テキスト03.Text = "ITEM03";
            this.テキスト03.Top = 0F;
            this.テキスト03.Width = 2.494095F;
            // 
            // テキスト04
            // 
            this.テキスト04.DataField = "ITEM04";
            this.テキスト04.Height = 0.2082677F;
            this.テキスト04.Left = 3.596457F;
            this.テキスト04.Name = "テキスト04";
            this.テキスト04.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト04.Tag = "";
            this.テキスト04.Text = "ITEM04";
            this.テキスト04.Top = 0F;
            this.テキスト04.Width = 0.5177164F;
            // 
            // テキスト05
            // 
            this.テキスト05.DataField = "ITEM05";
            this.テキスト05.Height = 0.2082677F;
            this.テキスト05.Left = 4.890158F;
            this.テキスト05.Name = "テキスト05";
            this.テキスト05.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト05.Tag = "";
            this.テキスト05.Text = "ITEM05";
            this.テキスト05.Top = 0F;
            this.テキスト05.Width = 0.6740155F;
            // 
            // テキスト06
            // 
            this.テキスト06.Height = 0.15625F;
            this.テキスト06.Left = 5.616262F;
            this.テキスト06.Name = "テキスト06";
            this.テキスト06.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト06.Tag = "";
            this.テキスト06.Text = "=[ITEM06]/[ITEM05]";
            this.テキスト06.Top = 0F;
            this.テキスト06.Width = 0.6085415F;
            // 
            // テキスト07
            // 
            this.テキスト07.DataField = "ITEM06";
            this.テキスト07.Height = 0.2188976F;
            this.テキスト07.Left = 6.295276F;
            this.テキスト07.Name = "テキスト07";
            this.テキスト07.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト07.Tag = "";
            this.テキスト07.Text = "ITEM06";
            this.テキスト07.Top = 0F;
            this.テキスト07.Width = 0.8480315F;
            // 
            // テキスト08
            // 
            this.テキスト08.DataField = "ITEM07";
            this.テキスト08.Height = 0.2188976F;
            this.テキスト08.Left = 7.254725F;
            this.テキスト08.Name = "テキスト08";
            this.テキスト08.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト08.Tag = "";
            this.テキスト08.Text = "ITEM07";
            this.テキスト08.Top = 0F;
            this.テキスト08.Width = 0.7228346F;
            // 
            // テキスト09
            // 
            this.テキスト09.Height = 0.15625F;
            this.テキスト09.Left = 8.184376F;
            this.テキスト09.Name = "テキスト09";
            this.テキスト09.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト09.Tag = "";
            this.テキスト09.Text = "=CLng([ITEM06])+CLng([ITEM07])";
            this.テキスト09.Top = 0F;
            this.テキスト09.Width = 1.11869F;
            // 
            // テキスト10
            // 
            this.テキスト10.DataField = "ITEM08";
            this.テキスト10.Height = 0.2082677F;
            this.テキスト10.Left = 9.901581F;
            this.テキスト10.Name = "テキスト10";
            this.テキスト10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト10.Tag = "";
            this.テキスト10.Text = "ITEM08";
            this.テキスト10.Top = 0F;
            this.テキスト10.Width = 1.297638F;
            // 
            // テキスト11
            // 
            this.テキスト11.Height = 0.15625F;
            this.テキスト11.Left = 11.52028F;
            this.テキスト11.Name = "テキスト11";
            this.テキスト11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト11.Tag = "";
            this.テキスト11.Text = "=CLng([ITEM06])+CLng([ITEM07])+CLng([ITEM08])";
            this.テキスト11.Top = 0F;
            this.テキスト11.Width = 1.20688F;
            // 
            // ラベル52
            // 
            this.ラベル52.Height = 0.2082677F;
            this.ラベル52.HyperLink = null;
            this.ラベル52.Left = 4.171658F;
            this.ラベル52.Name = "ラベル52";
            this.ラベル52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル52.Tag = "";
            this.ラベル52.Text = "件";
            this.ラベル52.Top = 0F;
            this.ラベル52.Width = 0.2169294F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM11";
            this.textBox2.Height = 0.2708662F;
            this.textBox2.Left = 12.80158F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM11";
            this.textBox2.Top = 0F;
            this.textBox2.Visible = false;
            this.textBox2.Width = 0.7539368F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0.0001148283F;
            this.pageFooter.Name = "pageFooter";
            // 
            // 直線54
            // 
            this.直線54.Height = 0F;
            this.直線54.Left = 0.1370079F;
            this.直線54.LineWeight = 2F;
            this.直線54.Name = "直線54";
            this.直線54.Tag = "";
            this.直線54.Top = 0F;
            this.直線54.Width = 13.13346F;
            this.直線54.X1 = 0.1370079F;
            this.直線54.X2 = 13.27047F;
            this.直線54.Y1 = 0F;
            this.直線54.Y2 = 0F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト12,
            this.ラベル53,
            this.テキスト13,
            this.テキスト14,
            this.テキスト15,
            this.テキスト16,
            this.テキスト17,
            this.テキスト18,
            this.テキスト19,
            this.ラベル69,
            this.直線54});
            this.reportFooter1.Height = 0.3855971F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // テキスト12
            // 
            this.テキスト12.DataField = "ITEM04";
            this.テキスト12.Height = 0.2602362F;
            this.テキスト12.Left = 3.480315F;
            this.テキスト12.Name = "テキスト12";
            this.テキスト12.OutputFormat = resources.GetString("テキスト12.OutputFormat");
            this.テキスト12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト12.Tag = "";
            this.テキスト12.Text = "ITEM04";
            this.テキスト12.Top = 0.08110237F;
            this.テキスト12.Width = 0.6338577F;
            // 
            // ラベル53
            // 
            this.ラベル53.Height = 0.2082677F;
            this.ラベル53.HyperLink = null;
            this.ラベル53.Left = 4.17174F;
            this.ラベル53.Name = "ラベル53";
            this.ラベル53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル53.Tag = "";
            this.ラベル53.Text = "件";
            this.ラベル53.Top = 0.08110237F;
            this.ラベル53.Width = 0.2168474F;
            // 
            // テキスト13
            // 
            this.テキスト13.DataField = "ITEM05";
            this.テキスト13.Height = 0.2602362F;
            this.テキスト13.Left = 4.810236F;
            this.テキスト13.Name = "テキスト13";
            this.テキスト13.OutputFormat = resources.GetString("テキスト13.OutputFormat");
            this.テキスト13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト13.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト13.Tag = "";
            this.テキスト13.Text = "ITEM05";
            this.テキスト13.Top = 0.08110237F;
            this.テキスト13.Width = 0.7539372F;
            // 
            // テキスト14
            // 
            this.テキスト14.Height = 0.15625F;
            this.テキスト14.Left = 5.616366F;
            this.テキスト14.Name = "テキスト14";
            this.テキスト14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト14.Tag = "";
            this.テキスト14.Text = "=Sum([ITEM06])/Sum([ITEM05])";
            this.テキスト14.Top = 0.08110237F;
            this.テキスト14.Width = 0.6084375F;
            // 
            // テキスト16
            // 
            this.テキスト16.DataField = "ITEM07";
            this.テキスト16.Height = 0.2082677F;
            this.テキスト16.Left = 7.337402F;
            this.テキスト16.Name = "テキスト16";
            this.テキスト16.OutputFormat = resources.GetString("テキスト16.OutputFormat");
            this.テキスト16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト16.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト16.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト16.Tag = "";
            this.テキスト16.Text = "ITEM07";
            this.テキスト16.Top = 0.08110237F;
            this.テキスト16.Width = 0.6401572F;
            // 
            // テキスト17
            // 
            this.テキスト17.Height = 0.15625F;
            this.テキスト17.Left = 8.163647F;
            this.テキスト17.Name = "テキスト17";
            this.テキスト17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト17.Tag = "";
            this.テキスト17.Text = "=Sum([ITEM06])+Sum([ITEM07])";
            this.テキスト17.Top = 0.08110237F;
            this.テキスト17.Width = 1.13942F;
            // 
            // テキスト18
            // 
            this.テキスト18.DataField = "ITEM08";
            this.テキスト18.Height = 0.2082677F;
            this.テキスト18.Left = 9.954731F;
            this.テキスト18.Name = "テキスト18";
            this.テキスト18.OutputFormat = resources.GetString("テキスト18.OutputFormat");
            this.テキスト18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト18.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト18.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト18.Tag = "";
            this.テキスト18.Text = "ITEM08";
            this.テキスト18.Top = 0.08110237F;
            this.テキスト18.Width = 1.244488F;
            // 
            // テキスト19
            // 
            this.テキスト19.Height = 0.15625F;
            this.テキスト19.Left = 11.50997F;
            this.テキスト19.Name = "テキスト19";
            this.テキスト19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト19.Tag = "";
            this.テキスト19.Text = "=Sum([ITEM06])+Sum([ITEM07])+Sum([ITEM08])";
            this.テキスト19.Top = 0.08110237F;
            this.テキスト19.Width = 1.217196F;
            // 
            // ラベル69
            // 
            this.ラベル69.Height = 0.2602362F;
            this.ラベル69.HyperLink = null;
            this.ラベル69.Left = 1.299727F;
            this.ラベル69.Name = "ラベル69";
            this.ラベル69.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル69.Tag = "";
            this.ラベル69.Text = "***　総合計　***";
            this.ラベル69.Top = 0.08110237F;
            this.ラベル69.Width = 1.697911F;
            // 
            // テキスト15
            // 
            this.テキスト15.DataField = "ITEM06";
            this.テキスト15.Height = 0.2082677F;
            this.テキスト15.Left = 6.357874F;
            this.テキスト15.Name = "テキスト15";
            this.テキスト15.OutputFormat = resources.GetString("テキスト15.OutputFormat");
            this.テキスト15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト15.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト15.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト15.Tag = "";
            this.テキスト15.Text = "ITEM06";
            this.テキスト15.Top = 0.08110237F;
            this.テキスト15.Width = 0.7854333F;
            // 
            // HNDR1041R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5905512F;
            this.PageSettings.Margins.Left = 0.8543308F;
            this.PageSettings.Margins.Right = 0.03937008F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.43578F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト01;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル4;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル5;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル7;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル8;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル10;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル11;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線13;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト11;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト12;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト19;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル69;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線54;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト15;
    }
}
