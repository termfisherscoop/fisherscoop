﻿namespace jp.co.fsi.hn.hnmr1131
{
    partial class HNMR1131
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblShukeihyoNm = new System.Windows.Forms.Label();
            this.txtShukeihyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShukeihyo = new System.Windows.Forms.Label();
            this.txtChikuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtChikuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet3 = new System.Windows.Forms.Label();
            this.lblJuni = new System.Windows.Forms.Label();
            this.txtJuni = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyoshuCdTo = new System.Windows.Forms.Label();
            this.lblGyoshuCdFr = new System.Windows.Forms.Label();
            this.lblGyoshuCd = new System.Windows.Forms.Label();
            this.txtGyoshuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGyoshuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet6 = new System.Windows.Forms.Label();
            this.lblGyoshuBunruiCdTo = new System.Windows.Forms.Label();
            this.lblGyoshuBunruiCdFr = new System.Windows.Forms.Label();
            this.lblGyoshuBunruiCd = new System.Windows.Forms.Label();
            this.txtGyoshuBunruiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGyoshuBunruiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet5 = new System.Windows.Forms.Label();
            this.lblChikuCdTo = new System.Windows.Forms.Label();
            this.lblChikuCdFr = new System.Windows.Forms.Label();
            this.lblChikuCd = new System.Windows.Forms.Label();
            this.lblGyohoCdTo = new System.Windows.Forms.Label();
            this.txtGyohoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet2 = new System.Windows.Forms.Label();
            this.lblGyohoCdFr = new System.Windows.Forms.Label();
            this.txtGyohoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyohoCd = new System.Windows.Forms.Label();
            this.lblSenshuCdTo = new System.Windows.Forms.Label();
            this.txtSenshuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.lblSenshuCdFr = new System.Windows.Forms.Label();
            this.txtSenshuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSenshuCd = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblSeisanKubun = new System.Windows.Forms.Label();
            this.txtSeisanKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4\r\n\r\nプレビュー";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\nEXCEL";
            // 
            // btnF8
            // 
            this.btnF8.Text = "F8\r\n\r\n";
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12\r\n\r\n項目設定";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(9, 506);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1095, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1085, 31);
            this.lblTitle.Text = "セリ集計表";
            // 
            // lblShukeihyoNm
            // 
            this.lblShukeihyoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblShukeihyoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShukeihyoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukeihyoNm.Location = new System.Drawing.Point(196, 3);
            this.lblShukeihyoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShukeihyoNm.Name = "lblShukeihyoNm";
            this.lblShukeihyoNm.Size = new System.Drawing.Size(283, 24);
            this.lblShukeihyoNm.TabIndex = 2;
            this.lblShukeihyoNm.Tag = "DISPNAME";
            this.lblShukeihyoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShukeihyo
            // 
            this.txtShukeihyo.AutoSizeFromLength = true;
            this.txtShukeihyo.DisplayLength = null;
            this.txtShukeihyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShukeihyo.Location = new System.Drawing.Point(129, 4);
            this.txtShukeihyo.Margin = new System.Windows.Forms.Padding(4);
            this.txtShukeihyo.MaxLength = 4;
            this.txtShukeihyo.Name = "txtShukeihyo";
            this.txtShukeihyo.Size = new System.Drawing.Size(63, 23);
            this.txtShukeihyo.TabIndex = 1;
            this.txtShukeihyo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShukeihyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShukeihyo_Validating);
            // 
            // lblShukeihyo
            // 
            this.lblShukeihyo.BackColor = System.Drawing.Color.Silver;
            this.lblShukeihyo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShukeihyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukeihyo.Location = new System.Drawing.Point(532, 183);
            this.lblShukeihyo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShukeihyo.Name = "lblShukeihyo";
            this.lblShukeihyo.Size = new System.Drawing.Size(107, 27);
            this.lblShukeihyo.TabIndex = 0;
            this.lblShukeihyo.Text = "集 計 表";
            this.lblShukeihyo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChikuCdTo
            // 
            this.txtChikuCdTo.AutoSizeFromLength = true;
            this.txtChikuCdTo.DisplayLength = null;
            this.txtChikuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChikuCdTo.Location = new System.Drawing.Point(607, 77);
            this.txtChikuCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtChikuCdTo.MaxLength = 4;
            this.txtChikuCdTo.Name = "txtChikuCdTo";
            this.txtChikuCdTo.Size = new System.Drawing.Size(63, 23);
            this.txtChikuCdTo.TabIndex = 16;
            this.txtChikuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChikuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCdTo_Validating);
            // 
            // txtChikuCdFr
            // 
            this.txtChikuCdFr.AutoSizeFromLength = true;
            this.txtChikuCdFr.DisplayLength = null;
            this.txtChikuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChikuCdFr.Location = new System.Drawing.Point(264, 77);
            this.txtChikuCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtChikuCdFr.MaxLength = 4;
            this.txtChikuCdFr.Name = "txtChikuCdFr";
            this.txtChikuCdFr.Size = new System.Drawing.Size(63, 23);
            this.txtChikuCdFr.TabIndex = 13;
            this.txtChikuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChikuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCdFr_Validating);
            // 
            // lblCodeBet3
            // 
            this.lblCodeBet3.AutoSize = true;
            this.lblCodeBet3.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet3.Location = new System.Drawing.Point(579, 80);
            this.lblCodeBet3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet3.Name = "lblCodeBet3";
            this.lblCodeBet3.Size = new System.Drawing.Size(24, 16);
            this.lblCodeBet3.TabIndex = 15;
            this.lblCodeBet3.Tag = "CHANGE";
            this.lblCodeBet3.Text = "～";
            this.lblCodeBet3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJuni
            // 
            this.lblJuni.AutoSize = true;
            this.lblJuni.BackColor = System.Drawing.Color.Silver;
            this.lblJuni.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJuni.Location = new System.Drawing.Point(183, 8);
            this.lblJuni.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJuni.Name = "lblJuni";
            this.lblJuni.Size = new System.Drawing.Size(56, 16);
            this.lblJuni.TabIndex = 1;
            this.lblJuni.Tag = "CHANGE";
            this.lblJuni.Text = "位まで";
            this.lblJuni.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJuni
            // 
            this.txtJuni.AutoSizeFromLength = true;
            this.txtJuni.DisplayLength = null;
            this.txtJuni.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJuni.Location = new System.Drawing.Point(129, 5);
            this.txtJuni.Margin = new System.Windows.Forms.Padding(4);
            this.txtJuni.MaxLength = 4;
            this.txtJuni.Name = "txtJuni";
            this.txtJuni.Size = new System.Drawing.Size(44, 23);
            this.txtJuni.TabIndex = 0;
            this.txtJuni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJuni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtJuni_KeyDown);
            this.txtJuni.Validating += new System.ComponentModel.CancelEventHandler(this.txtJuni_Validating);
            // 
            // lblGyoshuCdTo
            // 
            this.lblGyoshuCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGyoshuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuCdTo.Location = new System.Drawing.Point(675, 143);
            this.lblGyoshuCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGyoshuCdTo.Name = "lblGyoshuCdTo";
            this.lblGyoshuCdTo.Size = new System.Drawing.Size(240, 24);
            this.lblGyoshuCdTo.TabIndex = 29;
            this.lblGyoshuCdTo.Tag = "DISPNAME";
            this.lblGyoshuCdTo.Text = "最　後";
            this.lblGyoshuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuCdFr
            // 
            this.lblGyoshuCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGyoshuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuCdFr.Location = new System.Drawing.Point(332, 143);
            this.lblGyoshuCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGyoshuCdFr.Name = "lblGyoshuCdFr";
            this.lblGyoshuCdFr.Size = new System.Drawing.Size(240, 24);
            this.lblGyoshuCdFr.TabIndex = 26;
            this.lblGyoshuCdFr.Tag = "DISPNAME";
            this.lblGyoshuCdFr.Text = "先　頭";
            this.lblGyoshuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuCd
            // 
            this.lblGyoshuCd.AutoSize = true;
            this.lblGyoshuCd.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuCd.Location = new System.Drawing.Point(128, 147);
            this.lblGyoshuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGyoshuCd.Name = "lblGyoshuCd";
            this.lblGyoshuCd.Size = new System.Drawing.Size(88, 16);
            this.lblGyoshuCd.TabIndex = 24;
            this.lblGyoshuCd.Tag = "CHANGE";
            this.lblGyoshuCd.Text = "魚      種";
            this.lblGyoshuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuCdTo
            // 
            this.txtGyoshuCdTo.AutoSizeFromLength = true;
            this.txtGyoshuCdTo.DisplayLength = null;
            this.txtGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuCdTo.Location = new System.Drawing.Point(607, 144);
            this.txtGyoshuCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtGyoshuCdTo.MaxLength = 4;
            this.txtGyoshuCdTo.Name = "txtGyoshuCdTo";
            this.txtGyoshuCdTo.Size = new System.Drawing.Size(63, 23);
            this.txtGyoshuCdTo.TabIndex = 28;
            this.txtGyoshuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGyoshuCdTo_KeyDown);
            this.txtGyoshuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdTo_Validating);
            // 
            // txtGyoshuCdFr
            // 
            this.txtGyoshuCdFr.AutoSizeFromLength = true;
            this.txtGyoshuCdFr.DisplayLength = null;
            this.txtGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuCdFr.Location = new System.Drawing.Point(264, 144);
            this.txtGyoshuCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtGyoshuCdFr.MaxLength = 4;
            this.txtGyoshuCdFr.Name = "txtGyoshuCdFr";
            this.txtGyoshuCdFr.Size = new System.Drawing.Size(63, 23);
            this.txtGyoshuCdFr.TabIndex = 25;
            this.txtGyoshuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdFr_Validating);
            // 
            // lblCodeBet6
            // 
            this.lblCodeBet6.AutoSize = true;
            this.lblCodeBet6.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet6.Location = new System.Drawing.Point(579, 147);
            this.lblCodeBet6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet6.Name = "lblCodeBet6";
            this.lblCodeBet6.Size = new System.Drawing.Size(24, 16);
            this.lblCodeBet6.TabIndex = 27;
            this.lblCodeBet6.Tag = "CHANGE";
            this.lblCodeBet6.Text = "～";
            this.lblCodeBet6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuBunruiCdTo
            // 
            this.lblGyoshuBunruiCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGyoshuBunruiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuBunruiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuBunruiCdTo.Location = new System.Drawing.Point(675, 111);
            this.lblGyoshuBunruiCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGyoshuBunruiCdTo.Name = "lblGyoshuBunruiCdTo";
            this.lblGyoshuBunruiCdTo.Size = new System.Drawing.Size(240, 24);
            this.lblGyoshuBunruiCdTo.TabIndex = 23;
            this.lblGyoshuBunruiCdTo.Tag = "DISPNAME";
            this.lblGyoshuBunruiCdTo.Text = "最　後";
            this.lblGyoshuBunruiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuBunruiCdFr
            // 
            this.lblGyoshuBunruiCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGyoshuBunruiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuBunruiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuBunruiCdFr.Location = new System.Drawing.Point(332, 111);
            this.lblGyoshuBunruiCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGyoshuBunruiCdFr.Name = "lblGyoshuBunruiCdFr";
            this.lblGyoshuBunruiCdFr.Size = new System.Drawing.Size(240, 24);
            this.lblGyoshuBunruiCdFr.TabIndex = 20;
            this.lblGyoshuBunruiCdFr.Tag = "DISPNAME";
            this.lblGyoshuBunruiCdFr.Text = "先　頭";
            this.lblGyoshuBunruiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuBunruiCd
            // 
            this.lblGyoshuBunruiCd.AutoSize = true;
            this.lblGyoshuBunruiCd.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuBunruiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuBunruiCd.Location = new System.Drawing.Point(128, 114);
            this.lblGyoshuBunruiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGyoshuBunruiCd.Name = "lblGyoshuBunruiCd";
            this.lblGyoshuBunruiCd.Size = new System.Drawing.Size(88, 16);
            this.lblGyoshuBunruiCd.TabIndex = 18;
            this.lblGyoshuBunruiCd.Tag = "CHANGE";
            this.lblGyoshuBunruiCd.Text = "魚 種分 類";
            this.lblGyoshuBunruiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuBunruiCdTo
            // 
            this.txtGyoshuBunruiCdTo.AutoSizeFromLength = true;
            this.txtGyoshuBunruiCdTo.DisplayLength = null;
            this.txtGyoshuBunruiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuBunruiCdTo.Location = new System.Drawing.Point(607, 111);
            this.txtGyoshuBunruiCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtGyoshuBunruiCdTo.MaxLength = 4;
            this.txtGyoshuBunruiCdTo.Name = "txtGyoshuBunruiCdTo";
            this.txtGyoshuBunruiCdTo.Size = new System.Drawing.Size(63, 23);
            this.txtGyoshuBunruiCdTo.TabIndex = 22;
            this.txtGyoshuBunruiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuBunruiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuBunruiCdTo_Validating);
            // 
            // txtGyoshuBunruiCdFr
            // 
            this.txtGyoshuBunruiCdFr.AutoSizeFromLength = true;
            this.txtGyoshuBunruiCdFr.DisplayLength = null;
            this.txtGyoshuBunruiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuBunruiCdFr.Location = new System.Drawing.Point(264, 111);
            this.txtGyoshuBunruiCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtGyoshuBunruiCdFr.MaxLength = 4;
            this.txtGyoshuBunruiCdFr.Name = "txtGyoshuBunruiCdFr";
            this.txtGyoshuBunruiCdFr.Size = new System.Drawing.Size(63, 23);
            this.txtGyoshuBunruiCdFr.TabIndex = 19;
            this.txtGyoshuBunruiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuBunruiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuBunruiCdFr_Validating);
            // 
            // lblCodeBet5
            // 
            this.lblCodeBet5.AutoSize = true;
            this.lblCodeBet5.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet5.Location = new System.Drawing.Point(579, 114);
            this.lblCodeBet5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet5.Name = "lblCodeBet5";
            this.lblCodeBet5.Size = new System.Drawing.Size(24, 16);
            this.lblCodeBet5.TabIndex = 21;
            this.lblCodeBet5.Tag = "CHANGE";
            this.lblCodeBet5.Text = "～";
            this.lblCodeBet5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblChikuCdTo
            // 
            this.lblChikuCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblChikuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChikuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChikuCdTo.Location = new System.Drawing.Point(675, 77);
            this.lblChikuCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblChikuCdTo.Name = "lblChikuCdTo";
            this.lblChikuCdTo.Size = new System.Drawing.Size(240, 24);
            this.lblChikuCdTo.TabIndex = 17;
            this.lblChikuCdTo.Tag = "DISPNAME";
            this.lblChikuCdTo.Text = "最　後";
            this.lblChikuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblChikuCdFr
            // 
            this.lblChikuCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblChikuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChikuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChikuCdFr.Location = new System.Drawing.Point(332, 77);
            this.lblChikuCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblChikuCdFr.Name = "lblChikuCdFr";
            this.lblChikuCdFr.Size = new System.Drawing.Size(240, 24);
            this.lblChikuCdFr.TabIndex = 14;
            this.lblChikuCdFr.Tag = "DISPNAME";
            this.lblChikuCdFr.Text = "先　頭";
            this.lblChikuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblChikuCd
            // 
            this.lblChikuCd.AutoSize = true;
            this.lblChikuCd.BackColor = System.Drawing.Color.Silver;
            this.lblChikuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChikuCd.Location = new System.Drawing.Point(128, 80);
            this.lblChikuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblChikuCd.Name = "lblChikuCd";
            this.lblChikuCd.Size = new System.Drawing.Size(88, 16);
            this.lblChikuCd.TabIndex = 12;
            this.lblChikuCd.Tag = "CHANGE";
            this.lblChikuCd.Text = "地      区";
            this.lblChikuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyohoCdTo
            // 
            this.lblGyohoCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGyohoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyohoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyohoCdTo.Location = new System.Drawing.Point(675, 44);
            this.lblGyohoCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGyohoCdTo.Name = "lblGyohoCdTo";
            this.lblGyohoCdTo.Size = new System.Drawing.Size(240, 24);
            this.lblGyohoCdTo.TabIndex = 11;
            this.lblGyohoCdTo.Tag = "DISPNAME";
            this.lblGyohoCdTo.Text = "最　後";
            this.lblGyohoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyohoCdTo
            // 
            this.txtGyohoCdTo.AutoSizeFromLength = true;
            this.txtGyohoCdTo.DisplayLength = null;
            this.txtGyohoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyohoCdTo.Location = new System.Drawing.Point(607, 44);
            this.txtGyohoCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtGyohoCdTo.MaxLength = 4;
            this.txtGyohoCdTo.Name = "txtGyohoCdTo";
            this.txtGyohoCdTo.Size = new System.Drawing.Size(63, 23);
            this.txtGyohoCdTo.TabIndex = 10;
            this.txtGyohoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyohoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoCdTo_Validating);
            // 
            // lblCodeBet2
            // 
            this.lblCodeBet2.AutoSize = true;
            this.lblCodeBet2.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet2.Location = new System.Drawing.Point(579, 47);
            this.lblCodeBet2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet2.Name = "lblCodeBet2";
            this.lblCodeBet2.Size = new System.Drawing.Size(24, 16);
            this.lblCodeBet2.TabIndex = 9;
            this.lblCodeBet2.Tag = "CHANGE";
            this.lblCodeBet2.Text = "～";
            this.lblCodeBet2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyohoCdFr
            // 
            this.lblGyohoCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGyohoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyohoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyohoCdFr.Location = new System.Drawing.Point(332, 44);
            this.lblGyohoCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGyohoCdFr.Name = "lblGyohoCdFr";
            this.lblGyohoCdFr.Size = new System.Drawing.Size(240, 24);
            this.lblGyohoCdFr.TabIndex = 8;
            this.lblGyohoCdFr.Tag = "DISPNAME";
            this.lblGyohoCdFr.Text = "先　頭";
            this.lblGyohoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyohoCdFr
            // 
            this.txtGyohoCdFr.AutoSizeFromLength = true;
            this.txtGyohoCdFr.DisplayLength = null;
            this.txtGyohoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyohoCdFr.Location = new System.Drawing.Point(264, 44);
            this.txtGyohoCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtGyohoCdFr.MaxLength = 4;
            this.txtGyohoCdFr.Name = "txtGyohoCdFr";
            this.txtGyohoCdFr.Size = new System.Drawing.Size(63, 23);
            this.txtGyohoCdFr.TabIndex = 7;
            this.txtGyohoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyohoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoCdFr_Validating);
            // 
            // lblGyohoCd
            // 
            this.lblGyohoCd.AutoSize = true;
            this.lblGyohoCd.BackColor = System.Drawing.Color.Silver;
            this.lblGyohoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyohoCd.Location = new System.Drawing.Point(128, 47);
            this.lblGyohoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGyohoCd.Name = "lblGyohoCd";
            this.lblGyohoCd.Size = new System.Drawing.Size(88, 16);
            this.lblGyohoCd.TabIndex = 6;
            this.lblGyohoCd.Tag = "CHANGE";
            this.lblGyohoCd.Text = "魚      法";
            this.lblGyohoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSenshuCdTo
            // 
            this.lblSenshuCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblSenshuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSenshuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSenshuCdTo.Location = new System.Drawing.Point(675, 11);
            this.lblSenshuCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSenshuCdTo.Name = "lblSenshuCdTo";
            this.lblSenshuCdTo.Size = new System.Drawing.Size(240, 24);
            this.lblSenshuCdTo.TabIndex = 5;
            this.lblSenshuCdTo.Tag = "DISPNAME";
            this.lblSenshuCdTo.Text = "最　後";
            this.lblSenshuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSenshuCdTo
            // 
            this.txtSenshuCdTo.AutoSizeFromLength = true;
            this.txtSenshuCdTo.DisplayLength = null;
            this.txtSenshuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSenshuCdTo.Location = new System.Drawing.Point(607, 11);
            this.txtSenshuCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtSenshuCdTo.MaxLength = 4;
            this.txtSenshuCdTo.Name = "txtSenshuCdTo";
            this.txtSenshuCdTo.Size = new System.Drawing.Size(63, 23);
            this.txtSenshuCdTo.TabIndex = 4;
            this.txtSenshuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSenshuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSenshuCdTo_Validating);
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.AutoSize = true;
            this.lblCodeBet1.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet1.Location = new System.Drawing.Point(579, 14);
            this.lblCodeBet1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(24, 16);
            this.lblCodeBet1.TabIndex = 3;
            this.lblCodeBet1.Tag = "CHANGE";
            this.lblCodeBet1.Text = "～";
            this.lblCodeBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSenshuCdFr
            // 
            this.lblSenshuCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblSenshuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSenshuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSenshuCdFr.Location = new System.Drawing.Point(332, 11);
            this.lblSenshuCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSenshuCdFr.Name = "lblSenshuCdFr";
            this.lblSenshuCdFr.Size = new System.Drawing.Size(240, 24);
            this.lblSenshuCdFr.TabIndex = 2;
            this.lblSenshuCdFr.Tag = "DISPNAME";
            this.lblSenshuCdFr.Text = "先　頭";
            this.lblSenshuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSenshuCdFr
            // 
            this.txtSenshuCdFr.AutoSizeFromLength = true;
            this.txtSenshuCdFr.DisplayLength = null;
            this.txtSenshuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSenshuCdFr.Location = new System.Drawing.Point(264, 11);
            this.txtSenshuCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtSenshuCdFr.MaxLength = 4;
            this.txtSenshuCdFr.Name = "txtSenshuCdFr";
            this.txtSenshuCdFr.Size = new System.Drawing.Size(63, 23);
            this.txtSenshuCdFr.TabIndex = 1;
            this.txtSenshuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSenshuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSenshuCdFr_Validating);
            // 
            // lblSenshuCd
            // 
            this.lblSenshuCd.AutoSize = true;
            this.lblSenshuCd.BackColor = System.Drawing.Color.Silver;
            this.lblSenshuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSenshuCd.Location = new System.Drawing.Point(128, 14);
            this.lblSenshuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSenshuCd.Name = "lblSenshuCd";
            this.lblSenshuCd.Size = new System.Drawing.Size(88, 16);
            this.lblSenshuCd.TabIndex = 0;
            this.lblSenshuCd.Tag = "CHANGE";
            this.lblSenshuCd.Text = "船      主";
            this.lblSenshuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(129, 5);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(63, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(196, 4);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeisanKubun
            // 
            this.lblSeisanKubun.AutoSize = true;
            this.lblSeisanKubun.BackColor = System.Drawing.Color.Silver;
            this.lblSeisanKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeisanKubun.Location = new System.Drawing.Point(196, 9);
            this.lblSeisanKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSeisanKubun.Name = "lblSeisanKubun";
            this.lblSeisanKubun.Size = new System.Drawing.Size(216, 16);
            this.lblSeisanKubun.TabIndex = 1;
            this.lblSeisanKubun.Tag = "CHANGE";
            this.lblSeisanKubun.Text = "1:地区外 2:浜売り 3:地区内";
            this.lblSeisanKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeisanKubun
            // 
            this.txtSeisanKubun.AutoSizeFromLength = false;
            this.txtSeisanKubun.DisplayLength = null;
            this.txtSeisanKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeisanKubun.Location = new System.Drawing.Point(129, 6);
            this.txtSeisanKubun.Margin = new System.Windows.Forms.Padding(4);
            this.txtSeisanKubun.MaxLength = 1;
            this.txtSeisanKubun.Name = "txtSeisanKubun";
            this.txtSeisanKubun.Size = new System.Drawing.Size(63, 23);
            this.txtSeisanKubun.TabIndex = 0;
            this.txtSeisanKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeisanKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKubun_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.MinimumSize = new System.Drawing.Size(0, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1061, 33);
            this.label1.TabIndex = 902;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "水揚支所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1061, 33);
            this.label2.TabIndex = 902;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "集計表種別";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1061, 33);
            this.label3.TabIndex = 902;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "日付範囲";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(1061, 32);
            this.label4.TabIndex = 902;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "順位";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.MinimumSize = new System.Drawing.Size(0, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(1061, 35);
            this.label5.TabIndex = 902;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "精算区分";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.Location = new System.Drawing.Point(129, 4);
            this.lblGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(53, 24);
            this.lblGengoFr.TabIndex = 1;
            this.lblGengoFr.Tag = "DISPNAME";
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.Location = new System.Drawing.Point(481, 4);
            this.lblGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(53, 24);
            this.lblGengoTo.TabIndex = 10;
            this.lblGengoTo.Tag = "DISPNAME";
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearFr.Location = new System.Drawing.Point(187, 5);
            this.txtYearFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(43, 23);
            this.txtYearFr.TabIndex = 2;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearTo.Location = new System.Drawing.Point(539, 5);
            this.txtYearTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(43, 23);
            this.txtYearTo.TabIndex = 11;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.Location = new System.Drawing.Point(269, 5);
            this.txtMonthFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(43, 23);
            this.txtMonthFr.TabIndex = 4;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.Location = new System.Drawing.Point(621, 5);
            this.txtMonthTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(43, 23);
            this.txtMonthTo.TabIndex = 13;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // lblYearFr
            // 
            this.lblYearFr.AutoSize = true;
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.Location = new System.Drawing.Point(231, 8);
            this.lblYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(24, 16);
            this.lblYearFr.TabIndex = 3;
            this.lblYearFr.Tag = "CHANGE";
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.Location = new System.Drawing.Point(353, 5);
            this.txtDayFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(43, 23);
            this.txtDayFr.TabIndex = 6;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTo.Location = new System.Drawing.Point(707, 5);
            this.txtDayTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(43, 23);
            this.txtDayTo.TabIndex = 15;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // lblYearTo
            // 
            this.lblYearTo.AutoSize = true;
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.Location = new System.Drawing.Point(583, 8);
            this.lblYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(24, 16);
            this.lblYearTo.TabIndex = 12;
            this.lblYearTo.Tag = "CHANGE";
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.AutoSize = true;
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.Location = new System.Drawing.Point(313, 8);
            this.lblMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(24, 16);
            this.lblMonthFr.TabIndex = 5;
            this.lblMonthFr.Tag = "CHANGE";
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.AutoSize = true;
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.Location = new System.Drawing.Point(667, 8);
            this.lblMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(24, 16);
            this.lblMonthTo.TabIndex = 14;
            this.lblMonthTo.Tag = "CHANGE";
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.AutoSize = true;
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.Location = new System.Drawing.Point(397, 8);
            this.lblDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(24, 16);
            this.lblDayFr.TabIndex = 7;
            this.lblDayFr.Tag = "CHANGE";
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.AutoSize = true;
            this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(441, 8);
            this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(24, 16);
            this.lblCodeBetDate.TabIndex = 8;
            this.lblCodeBetDate.Tag = "CHANGE";
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.AutoSize = true;
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTo.Location = new System.Drawing.Point(752, 8);
            this.lblDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(24, 16);
            this.lblDayTo.TabIndex = 16;
            this.lblDayTo.Tag = "CHANGE";
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.MinimumSize = new System.Drawing.Size(0, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(1061, 176);
            this.label6.TabIndex = 903;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "コード範囲";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 36);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 6;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.46832F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.917356F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.7438F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.19284F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.29477F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.38292F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1071, 392);
            this.fsiTableLayoutPanel1.TabIndex = 904;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.lblSenshuCd);
            this.fsiPanel6.Controls.Add(this.lblCodeBet3);
            this.fsiPanel6.Controls.Add(this.lblGyoshuCdTo);
            this.fsiPanel6.Controls.Add(this.txtChikuCdFr);
            this.fsiPanel6.Controls.Add(this.lblGyoshuCdFr);
            this.fsiPanel6.Controls.Add(this.txtChikuCdTo);
            this.fsiPanel6.Controls.Add(this.lblGyoshuCd);
            this.fsiPanel6.Controls.Add(this.txtSenshuCdFr);
            this.fsiPanel6.Controls.Add(this.txtGyoshuCdTo);
            this.fsiPanel6.Controls.Add(this.lblSenshuCdFr);
            this.fsiPanel6.Controls.Add(this.txtGyoshuCdFr);
            this.fsiPanel6.Controls.Add(this.lblCodeBet1);
            this.fsiPanel6.Controls.Add(this.lblCodeBet6);
            this.fsiPanel6.Controls.Add(this.txtSenshuCdTo);
            this.fsiPanel6.Controls.Add(this.lblGyoshuBunruiCdTo);
            this.fsiPanel6.Controls.Add(this.lblSenshuCdTo);
            this.fsiPanel6.Controls.Add(this.lblGyoshuBunruiCdFr);
            this.fsiPanel6.Controls.Add(this.lblGyohoCd);
            this.fsiPanel6.Controls.Add(this.lblGyoshuBunruiCd);
            this.fsiPanel6.Controls.Add(this.txtGyohoCdFr);
            this.fsiPanel6.Controls.Add(this.txtGyoshuBunruiCdTo);
            this.fsiPanel6.Controls.Add(this.lblGyohoCdFr);
            this.fsiPanel6.Controls.Add(this.txtGyoshuBunruiCdFr);
            this.fsiPanel6.Controls.Add(this.lblCodeBet2);
            this.fsiPanel6.Controls.Add(this.lblCodeBet5);
            this.fsiPanel6.Controls.Add(this.txtGyohoCdTo);
            this.fsiPanel6.Controls.Add(this.lblChikuCdTo);
            this.fsiPanel6.Controls.Add(this.lblGyohoCdTo);
            this.fsiPanel6.Controls.Add(this.lblChikuCdFr);
            this.fsiPanel6.Controls.Add(this.lblChikuCd);
            this.fsiPanel6.Controls.Add(this.label6);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(5, 211);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(1061, 176);
            this.fsiPanel6.TabIndex = 5;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblSeisanKubun);
            this.fsiPanel5.Controls.Add(this.txtSeisanKubun);
            this.fsiPanel5.Controls.Add(this.label5);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(5, 167);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(1061, 35);
            this.fsiPanel5.TabIndex = 4;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtJuni);
            this.fsiPanel4.Controls.Add(this.lblJuni);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 127);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(1061, 31);
            this.fsiPanel4.TabIndex = 3;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.lblGengoFr);
            this.fsiPanel3.Controls.Add(this.lblGengoTo);
            this.fsiPanel3.Controls.Add(this.txtYearFr);
            this.fsiPanel3.Controls.Add(this.txtYearTo);
            this.fsiPanel3.Controls.Add(this.txtMonthFr);
            this.fsiPanel3.Controls.Add(this.txtMonthTo);
            this.fsiPanel3.Controls.Add(this.lblDayTo);
            this.fsiPanel3.Controls.Add(this.lblYearFr);
            this.fsiPanel3.Controls.Add(this.txtDayFr);
            this.fsiPanel3.Controls.Add(this.txtDayTo);
            this.fsiPanel3.Controls.Add(this.lblCodeBetDate);
            this.fsiPanel3.Controls.Add(this.lblYearTo);
            this.fsiPanel3.Controls.Add(this.lblMonthFr);
            this.fsiPanel3.Controls.Add(this.lblMonthTo);
            this.fsiPanel3.Controls.Add(this.lblDayFr);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 85);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(1061, 33);
            this.fsiPanel3.TabIndex = 2;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtShukeihyo);
            this.fsiPanel2.Controls.Add(this.lblShukeihyoNm);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 46);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(1061, 30);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(1061, 32);
            this.fsiPanel1.TabIndex = 0;
            // 
            // HNMR1131
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1085, 439);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.lblShukeihyo);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "HNMR1131";
            this.Text = "セリ集計表";
            this.Controls.SetChildIndex(this.lblShukeihyo, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtChikuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtChikuCdFr;
        private System.Windows.Forms.Label lblCodeBet3;
        private jp.co.fsi.common.controls.FsiTextBox txtSenshuCdFr;
        private System.Windows.Forms.Label lblSenshuCd;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.Label lblSenshuCdFr;
        private System.Windows.Forms.Label lblSenshuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtSenshuCdTo;
        private System.Windows.Forms.Label lblCodeBet2;
        private System.Windows.Forms.Label lblGyohoCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoCdFr;
        private System.Windows.Forms.Label lblGyohoCd;
        private System.Windows.Forms.Label lblChikuCdTo;
        private System.Windows.Forms.Label lblChikuCdFr;
        private System.Windows.Forms.Label lblChikuCd;
        private System.Windows.Forms.Label lblGyohoCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoCdTo;
        private System.Windows.Forms.Label lblShukeihyoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShukeihyo;
        private System.Windows.Forms.Label lblShukeihyo;
        private System.Windows.Forms.Label lblJuni;
        private jp.co.fsi.common.controls.FsiTextBox txtJuni;
        private System.Windows.Forms.Label lblGyoshuCdTo;
        private System.Windows.Forms.Label lblGyoshuCdFr;
        private System.Windows.Forms.Label lblGyoshuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuCdFr;
        private System.Windows.Forms.Label lblCodeBet6;
        private System.Windows.Forms.Label lblGyoshuBunruiCdTo;
        private System.Windows.Forms.Label lblGyoshuBunruiCdFr;
        private System.Windows.Forms.Label lblGyoshuBunruiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuBunruiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuBunruiCdFr;
        private System.Windows.Forms.Label lblCodeBet5;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblSeisanKubun;
        private common.controls.FsiTextBox txtSeisanKubun;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblGengoTo;
        private common.controls.FsiTextBox txtYearFr;
        private common.controls.FsiTextBox txtYearTo;
        private common.controls.FsiTextBox txtMonthFr;
        private common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblYearFr;
        private common.controls.FsiTextBox txtDayFr;
        private common.controls.FsiTextBox txtDayTo;
        private System.Windows.Forms.Label lblYearTo;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label label6;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}