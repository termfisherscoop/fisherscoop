﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1131
{
    /// <summary>
    /// HNMR11311Rの帳票
    /// </summary>
    public partial class HNMR11311R : BaseReport
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        public HNMR11311R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }
    }
}
