﻿namespace jp.co.fsi.hn.hndr1091
{
    /// <summary>
    /// HANR2021R の概要の説明です。
    /// </summary>
    partial class HNDR1091R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNDR1091R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPageCnt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFunaPageCnt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKensu1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSuryo1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAvgTanka1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKingaku1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKoujo1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKoujoNm1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKojoKin1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtSebshuCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunaPageCnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKensu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAvgTanka1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingaku1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKoujo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKoujoNm1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKojoKin1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSebshuCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label7,
            this.lblTitle,
            this.label9,
            this.label10,
            this.label11,
            this.label12,
            this.label13,
            this.label14,
            this.label15,
            this.label18,
            this.textBox6,
            this.txtPageCnt,
            this.label20,
            this.txtFunaPageCnt,
            this.textBox1,
            this.line1,
            this.line2,
            this.label4,
            this.label5,
            this.label1,
            this.label2,
            this.label3,
            this.line10,
            this.line42,
            this.line43,
            this.line44,
            this.line45,
            this.label6,
            this.line28,
            this.line37,
            this.textBox2,
            this.label17});
            this.pageHeader.Height = 2.368212F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // label7
            // 
            this.label7.Height = 0.3334646F;
            this.label7.HyperLink = null;
            this.label7.Left = 3.570079F;
            this.label7.Name = "label7";
            this.label7.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label7.Text = "控　除";
            this.label7.Top = 2.025197F;
            this.label7.Width = 0.8661417F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.3125F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 2.509243F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align: justify; text" +
    "-justify: distribute-all-lines";
            this.lblTitle.Text = "支払明細書";
            this.lblTitle.Top = 0.1645669F;
            this.lblTitle.Width = 2.07792F;
            // 
            // label9
            // 
            this.label9.CharacterSpacing = 1F;
            this.label9.DataField = "ITEM22";
            this.label9.Height = 0.1981628F;
            this.label9.HyperLink = null;
            this.label9.Left = 4.035433F;
            this.label9.Name = "label9";
            this.label9.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; text-align: justify; text-justify: distri" +
    "bute-all-lines; ddo-char-set: 1";
            this.label9.Text = "名護漁業協同組合";
            this.label9.Top = 0.9937009F;
            this.label9.Width = 2.531102F;
            // 
            // label10
            // 
            this.label10.Height = 0.1770833F;
            this.label10.HyperLink = null;
            this.label10.Left = 4.066536F;
            this.label10.Name = "label10";
            this.label10.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: justify; text-justify: distribute" +
    "-all-lines; vertical-align: middle; ddo-char-set: 1";
            this.label10.Text = "住 所";
            this.label10.Top = 1.48189F;
            this.label10.Width = 0.4136322F;
            // 
            // label11
            // 
            this.label11.DataField = "ITEM23";
            this.label11.Height = 0.1770833F;
            this.label11.HyperLink = null;
            this.label11.Left = 4.537008F;
            this.label11.Name = "label11";
            this.label11.Style = "font-family: ＭＳ 明朝; font-size: 9pt; vertical-align: middle; ddo-char-set: 1";
            this.label11.Text = "沖縄県名護市城三丁目１番１号";
            this.label11.Top = 1.48189F;
            this.label11.Width = 2.409843F;
            // 
            // label12
            // 
            this.label12.Height = 0.1770833F;
            this.label12.HyperLink = null;
            this.label12.Left = 4.066536F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: justify; text-justify: distribute" +
    "-all-lines; vertical-align: middle; ddo-char-set: 1";
            this.label12.Text = "TEL";
            this.label12.Top = 1.659056F;
            this.label12.Width = 0.4136322F;
            // 
            // label13
            // 
            this.label13.Height = 0.1770833F;
            this.label13.HyperLink = null;
            this.label13.Left = 4.066536F;
            this.label13.Name = "label13";
            this.label13.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: justify; text-justify: distribute" +
    "-all-lines; vertical-align: middle; ddo-char-set: 1";
            this.label13.Text = "ｾﾘ市場";
            this.label13.Top = 1.836221F;
            this.label13.Visible = false;
            this.label13.Width = 0.4136322F;
            // 
            // label14
            // 
            this.label14.DataField = "ITEM24";
            this.label14.Height = 0.1770833F;
            this.label14.HyperLink = null;
            this.label14.Left = 4.480316F;
            this.label14.Name = "label14";
            this.label14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 128";
            this.label14.Text = "（０９８０）５２－２８１２";
            this.label14.Top = 1.659056F;
            this.label14.Width = 2.466536F;
            // 
            // label15
            // 
            this.label15.DataField = "ITEM25";
            this.label15.Height = 0.1770833F;
            this.label15.HyperLink = null;
            this.label15.Left = 4.480316F;
            this.label15.Name = "label15";
            this.label15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 128";
            this.label15.Text = "（０９８０）５１－０７２７";
            this.label15.Top = 1.848032F;
            this.label15.Visible = false;
            this.label15.Width = 2.466536F;
            // 
            // label18
            // 
            this.label18.DataField = "ITEM125";
            this.label18.Height = 0.1770833F;
            this.label18.HyperLink = null;
            this.label18.Left = 4.066536F;
            this.label18.Name = "label18";
            this.label18.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: justify; text-justify: auto; ver" +
    "tical-align: middle; ddo-char-set: 128";
            this.label18.Text = "本所";
            this.label18.Top = 1.262599F;
            this.label18.Width = 1.760236F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM27";
            this.textBox6.Height = 0.1875F;
            this.textBox6.Left = 5.241733F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM27";
            this.textBox6.Top = 0.5818898F;
            this.textBox6.Width = 1.531496F;
            // 
            // txtPageCnt
            // 
            this.txtPageCnt.Height = 0.1740158F;
            this.txtPageCnt.Left = 6.102363F;
            this.txtPageCnt.Name = "txtPageCnt";
            this.txtPageCnt.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtPageCnt.SummaryGroup = "groupHeader2";
            this.txtPageCnt.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtPageCnt.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCnt.Text = "page";
            this.txtPageCnt.Top = 0.1645669F;
            this.txtPageCnt.Width = 0.28125F;
            // 
            // label20
            // 
            this.label20.Height = 0.1740158F;
            this.label20.HyperLink = null;
            this.label20.Left = 6.362599F;
            this.label20.Name = "label20";
            this.label20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label20.Text = "/";
            this.label20.Top = 0.1645669F;
            this.label20.Width = 0.1295276F;
            // 
            // txtFunaPageCnt
            // 
            this.txtFunaPageCnt.Height = 0.1740158F;
            this.txtFunaPageCnt.Left = 6.492127F;
            this.txtFunaPageCnt.Name = "txtFunaPageCnt";
            this.txtFunaPageCnt.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.txtFunaPageCnt.SummaryGroup = "groupHeader2";
            this.txtFunaPageCnt.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtFunaPageCnt.Text = "page";
            this.txtFunaPageCnt.Top = 0.1645669F;
            this.txtFunaPageCnt.Width = 0.28125F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM11";
            this.textBox1.Height = 0.21875F;
            this.textBox1.Left = 0.1377952F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox1.Tag = "船主名";
            this.textBox1.Text = "ITEM11";
            this.textBox1.Top = 0.5507874F;
            this.textBox1.Width = 2.0563F;
            // 
            // line1
            // 
            this.line1.Height = 1.370907E-06F;
            this.line1.Left = 0.1342521F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.7811021F;
            this.line1.Width = 2.059842F;
            this.line1.X1 = 0.1342521F;
            this.line1.X2 = 2.194094F;
            this.line1.Y1 = 0.7811035F;
            this.line1.Y2 = 0.7811021F;
            // 
            // line2
            // 
            this.line2.Height = 1.430511E-06F;
            this.line2.Left = 0.1342521F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.8106292F;
            this.line2.Width = 2.059842F;
            this.line2.X1 = 0.1342521F;
            this.line2.X2 = 2.194094F;
            this.line2.Y1 = 0.8106306F;
            this.line2.Y2 = 0.8106292F;
            // 
            // label4
            // 
            this.label4.Height = 0.3334646F;
            this.label4.HyperLink = null;
            this.label4.Left = 1.837795F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label4.Text = "平均単価";
            this.label4.Top = 2.025197F;
            this.label4.Width = 0.8661419F;
            // 
            // label5
            // 
            this.label5.Height = 0.3334646F;
            this.label5.HyperLink = null;
            this.label5.Left = 2.703937F;
            this.label5.Name = "label5";
            this.label5.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label5.Text = "水揚金額";
            this.label5.Top = 2.025197F;
            this.label5.Width = 0.8661419F;
            // 
            // label1
            // 
            this.label1.Height = 0.3334646F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.03937008F;
            this.label1.Name = "label1";
            this.label1.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label1.Text = "月日";
            this.label1.Top = 2.025197F;
            this.label1.Width = 0.4519685F;
            // 
            // label2
            // 
            this.label2.Height = 0.3334646F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.4913386F;
            this.label2.Name = "label2";
            this.label2.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label2.Text = "件数";
            this.label2.Top = 2.025197F;
            this.label2.Width = 0.3937008F;
            // 
            // label3
            // 
            this.label3.Height = 0.3334646F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.8850394F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label3.Text = "数量";
            this.label3.Top = 2.025197F;
            this.label3.Width = 0.9527559F;
            // 
            // line10
            // 
            this.line10.Height = 0.3333161F;
            this.line10.Left = 1.837795F;
            this.line10.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 2.025197F;
            this.line10.Width = 0F;
            this.line10.X1 = 1.837795F;
            this.line10.X2 = 1.837795F;
            this.line10.Y1 = 2.025197F;
            this.line10.Y2 = 2.358513F;
            // 
            // line42
            // 
            this.line42.Height = 0.3333149F;
            this.line42.Left = 4.655906F;
            this.line42.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line42.LineWeight = 1F;
            this.line42.Name = "line42";
            this.line42.Top = 2.025197F;
            this.line42.Width = 0F;
            this.line42.X1 = 4.655906F;
            this.line42.X2 = 4.655906F;
            this.line42.Y1 = 2.025197F;
            this.line42.Y2 = 2.358512F;
            // 
            // line43
            // 
            this.line43.Height = 0.3333149F;
            this.line43.Left = 0.8850394F;
            this.line43.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line43.LineWeight = 1F;
            this.line43.Name = "line43";
            this.line43.Top = 2.025197F;
            this.line43.Width = 0F;
            this.line43.X1 = 0.8850394F;
            this.line43.X2 = 0.8850394F;
            this.line43.Y1 = 2.025197F;
            this.line43.Y2 = 2.358512F;
            // 
            // line44
            // 
            this.line44.Height = 0.3333149F;
            this.line44.Left = 0.4913386F;
            this.line44.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line44.LineWeight = 1F;
            this.line44.Name = "line44";
            this.line44.Top = 2.025197F;
            this.line44.Width = 0F;
            this.line44.X1 = 0.4913386F;
            this.line44.X2 = 0.4913386F;
            this.line44.Y1 = 2.025197F;
            this.line44.Y2 = 2.358512F;
            // 
            // line45
            // 
            this.line45.Height = 0.3333149F;
            this.line45.Left = 2.703937F;
            this.line45.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line45.LineWeight = 1F;
            this.line45.Name = "line45";
            this.line45.Top = 2.025197F;
            this.line45.Width = 0F;
            this.line45.X1 = 2.703937F;
            this.line45.X2 = 2.703937F;
            this.line45.Y1 = 2.025197F;
            this.line45.Y2 = 2.358512F;
            // 
            // label6
            // 
            this.label6.Height = 0.3334646F;
            this.label6.HyperLink = null;
            this.label6.Left = 4.436221F;
            this.label6.Name = "label6";
            this.label6.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label6.Text = "控  除  内  訳";
            this.label6.Top = 2.025197F;
            this.label6.Width = 2.65F;
            // 
            // line28
            // 
            this.line28.Height = 0.3333149F;
            this.line28.Left = 3.570079F;
            this.line28.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 2.025197F;
            this.line28.Width = 0F;
            this.line28.X1 = 3.570079F;
            this.line28.X2 = 3.570079F;
            this.line28.Y1 = 2.025197F;
            this.line28.Y2 = 2.358512F;
            // 
            // line37
            // 
            this.line37.Height = 0.3333151F;
            this.line37.Left = 4.436221F;
            this.line37.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line37.LineWeight = 1F;
            this.line37.Name = "line37";
            this.line37.Top = 2.027953F;
            this.line37.Width = 0F;
            this.line37.X1 = 4.436221F;
            this.line37.X2 = 4.436221F;
            this.line37.Y1 = 2.027953F;
            this.line37.Y2 = 2.361268F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM26";
            this.textBox2.Height = 0.1875F;
            this.textBox2.Left = 0.1377953F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM26";
            this.textBox2.Top = 1.77441F;
            this.textBox2.Width = 3.833859F;
            // 
            // label17
            // 
            this.label17.DataField = "ITEM126";
            this.label17.Height = 0.5972441F;
            this.label17.HyperLink = null;
            this.label17.Left = 0.07519685F;
            this.label17.Name = "label17";
            this.label17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label17.Text = "消費税率( 8%) \\9,999円 消費税率(10%) \\9,999円";
            this.label17.Top = 0.8846458F;
            this.label17.Visible = false;
            this.label17.Width = 1.80315F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDate1,
            this.txtKensu1,
            this.txtSuryo1,
            this.txtAvgTanka1,
            this.txtKingaku1,
            this.txtKoujo1,
            this.txtKoujoNm1,
            this.txtKojoKin1,
            this.line3,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line11,
            this.line18,
            this.line29,
            this.line30,
            this.line4,
            this.line36,
            this.line40,
            this.line41,
            this.line46,
            this.line47,
            this.line48,
            this.line49,
            this.line50,
            this.line51,
            this.line52});
            this.detail.Height = 0.3179192F;
            this.detail.Name = "detail";
            this.detail.RepeatToFill = true;
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // txtDate1
            // 
            this.txtDate1.DataField = "ITEM13";
            this.txtDate1.Height = 0.2362205F;
            this.txtDate1.Left = 0.03937008F;
            this.txtDate1.Name = "txtDate1";
            this.txtDate1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
            this.txtDate1.Tag = "";
            this.txtDate1.Text = null;
            this.txtDate1.Top = 0.03937008F;
            this.txtDate1.Width = 0.4519685F;
            // 
            // txtKensu1
            // 
            this.txtKensu1.DataField = "ITEM14";
            this.txtKensu1.Height = 0.2362205F;
            this.txtKensu1.Left = 0.4913386F;
            this.txtKensu1.MultiLine = false;
            this.txtKensu1.Name = "txtKensu1";
            this.txtKensu1.OutputFormat = resources.GetString("txtKensu1.OutputFormat");
            this.txtKensu1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.txtKensu1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.txtKensu1.Tag = "";
            this.txtKensu1.Text = null;
            this.txtKensu1.Top = 0.03937008F;
            this.txtKensu1.Width = 0.3937008F;
            // 
            // txtSuryo1
            // 
            this.txtSuryo1.DataField = "ITEM15";
            this.txtSuryo1.Height = 0.2362205F;
            this.txtSuryo1.Left = 0.8929135F;
            this.txtSuryo1.Name = "txtSuryo1";
            this.txtSuryo1.OutputFormat = resources.GetString("txtSuryo1.OutputFormat");
            this.txtSuryo1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.txtSuryo1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.txtSuryo1.Tag = "";
            this.txtSuryo1.Text = "9,999,999.99";
            this.txtSuryo1.Top = 0.03937008F;
            this.txtSuryo1.Width = 0.9448819F;
            // 
            // txtAvgTanka1
            // 
            this.txtAvgTanka1.DataField = "ITEM16";
            this.txtAvgTanka1.Height = 0.2362205F;
            this.txtAvgTanka1.Left = 1.837795F;
            this.txtAvgTanka1.Name = "txtAvgTanka1";
            this.txtAvgTanka1.OutputFormat = resources.GetString("txtAvgTanka1.OutputFormat");
            this.txtAvgTanka1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.txtAvgTanka1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.txtAvgTanka1.Tag = "";
            this.txtAvgTanka1.Text = "999,999,999";
            this.txtAvgTanka1.Top = 0.03937008F;
            this.txtAvgTanka1.Width = 0.8661417F;
            // 
            // txtKingaku1
            // 
            this.txtKingaku1.DataField = "ITEM17";
            this.txtKingaku1.Height = 0.2362205F;
            this.txtKingaku1.Left = 2.703937F;
            this.txtKingaku1.Name = "txtKingaku1";
            this.txtKingaku1.OutputFormat = resources.GetString("txtKingaku1.OutputFormat");
            this.txtKingaku1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.txtKingaku1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.txtKingaku1.Tag = "";
            this.txtKingaku1.Text = "999,999,999";
            this.txtKingaku1.Top = 0.03937008F;
            this.txtKingaku1.Width = 0.8661417F;
            // 
            // txtKoujo1
            // 
            this.txtKoujo1.DataField = "ITEM18";
            this.txtKoujo1.Height = 0.2362205F;
            this.txtKoujo1.Left = 3.570079F;
            this.txtKoujo1.Name = "txtKoujo1";
            this.txtKoujo1.OutputFormat = resources.GetString("txtKoujo1.OutputFormat");
            this.txtKoujo1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.txtKoujo1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.txtKoujo1.Tag = "";
            this.txtKoujo1.Text = "999,999,999";
            this.txtKoujo1.Top = 0.03937008F;
            this.txtKoujo1.Width = 0.8661417F;
            // 
            // txtKoujoNm1
            // 
            this.txtKoujoNm1.CanGrow = false;
            this.txtKoujoNm1.DataField = "ITEM19";
            this.txtKoujoNm1.Height = 0.2362205F;
            this.txtKoujoNm1.Left = 4.436221F;
            this.txtKoujoNm1.MultiLine = false;
            this.txtKoujoNm1.Name = "txtKoujoNm1";
            this.txtKoujoNm1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.txtKoujoNm1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 128";
            this.txtKoujoNm1.Tag = "";
            this.txtKoujoNm1.Text = null;
            this.txtKoujoNm1.Top = 0.03937008F;
            this.txtKoujoNm1.Width = 1.7563F;
            // 
            // txtKojoKin1
            // 
            this.txtKojoKin1.DataField = "ITEM20";
            this.txtKojoKin1.Height = 0.2362205F;
            this.txtKojoKin1.Left = 6.220079F;
            this.txtKojoKin1.Name = "txtKojoKin1";
            this.txtKojoKin1.OutputFormat = resources.GetString("txtKojoKin1.OutputFormat");
            this.txtKojoKin1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.txtKojoKin1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.txtKojoKin1.Tag = "";
            this.txtKojoKin1.Text = "999,999,999";
            this.txtKojoKin1.Top = 0.03937008F;
            this.txtKojoKin1.Width = 0.8661417F;
            // 
            // line3
            // 
            this.line3.Height = 0.3149603F;
            this.line3.Left = 6.220079F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.003543307F;
            this.line3.Width = 0F;
            this.line3.X1 = 6.220079F;
            this.line3.X2 = 6.220079F;
            this.line3.Y1 = 0.003543307F;
            this.line3.Y2 = 0.3185036F;
            // 
            // line5
            // 
            this.line5.Height = 0.3149607F;
            this.line5.Left = 0.4913386F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 0.4913386F;
            this.line5.X2 = 0.4913386F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0.3149607F;
            // 
            // line6
            // 
            this.line6.Height = 0.3149607F;
            this.line6.Left = 0.8850394F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 0.8850394F;
            this.line6.X2 = 0.8850394F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 0.3149607F;
            // 
            // line7
            // 
            this.line7.Height = 0.3149607F;
            this.line7.Left = 1.837795F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 1.837795F;
            this.line7.X2 = 1.837795F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.3149607F;
            // 
            // line8
            // 
            this.line8.Height = 0.3149607F;
            this.line8.Left = 2.703937F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0.0001809597F;
            this.line8.X1 = 2.703937F;
            this.line8.X2 = 2.704118F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.3149607F;
            // 
            // line9
            // 
            this.line9.Height = 0.3149607F;
            this.line9.Left = 4.436221F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 4.436221F;
            this.line9.X2 = 4.436221F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.3149607F;
            // 
            // line11
            // 
            this.line11.Height = 0F;
            this.line11.Left = 0.03937008F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0.3149607F;
            this.line11.Width = 7.05F;
            this.line11.X1 = 0.03937008F;
            this.line11.X2 = 7.08937F;
            this.line11.Y1 = 0.3149607F;
            this.line11.Y2 = 0.3149607F;
            // 
            // line18
            // 
            this.line18.Height = 0.3070867F;
            this.line18.Left = 7.086221F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 9.536743E-07F;
            this.line18.X1 = 7.086221F;
            this.line18.X2 = 7.086222F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.3070867F;
            // 
            // line29
            // 
            this.line29.Height = 0.3149607F;
            this.line29.Left = 3.570079F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 0F;
            this.line29.Width = 0F;
            this.line29.X1 = 3.570079F;
            this.line29.X2 = 3.570079F;
            this.line29.Y1 = 0F;
            this.line29.Y2 = 0.3149607F;
            // 
            // line30
            // 
            this.line30.Height = 0.3149603F;
            this.line30.Left = 0.03937008F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 0.003543307F;
            this.line30.Width = 0F;
            this.line30.X1 = 0.03937008F;
            this.line30.X2 = 0.03937008F;
            this.line30.Y1 = 0.003543307F;
            this.line30.Y2 = 0.3185036F;
            // 
            // line4
            // 
            this.line4.Height = 0.3149598F;
            this.line4.Left = 1.617323F;
            this.line4.LineColor = System.Drawing.Color.Gray;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 1.617323F;
            this.line4.X2 = 1.617323F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0.3149598F;
            // 
            // line36
            // 
            this.line36.Height = 0.3149598F;
            this.line36.Left = 1.344882F;
            this.line36.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Top = 0F;
            this.line36.Width = 0F;
            this.line36.X1 = 1.344882F;
            this.line36.X2 = 1.344882F;
            this.line36.Y1 = 0F;
            this.line36.Y2 = 0.3149598F;
            // 
            // line40
            // 
            this.line40.Height = 0.3149598F;
            this.line40.Left = 1.072441F;
            this.line40.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line40.LineWeight = 1F;
            this.line40.Name = "line40";
            this.line40.Top = 0F;
            this.line40.Width = 0F;
            this.line40.X1 = 1.072441F;
            this.line40.X2 = 1.072441F;
            this.line40.Y1 = 0F;
            this.line40.Y2 = 0.3149598F;
            // 
            // line41
            // 
            this.line41.Height = 0.3149598F;
            this.line41.Left = 2.140158F;
            this.line41.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line41.LineWeight = 1F;
            this.line41.Name = "line41";
            this.line41.Top = 0F;
            this.line41.Width = 0F;
            this.line41.X1 = 2.140158F;
            this.line41.X2 = 2.140158F;
            this.line41.Y1 = 0F;
            this.line41.Y2 = 0.3149598F;
            // 
            // line46
            // 
            this.line46.Height = 0.3149598F;
            this.line46.Left = 2.411811F;
            this.line46.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line46.LineWeight = 1F;
            this.line46.Name = "line46";
            this.line46.Top = 0F;
            this.line46.Width = 0F;
            this.line46.X1 = 2.411811F;
            this.line46.X2 = 2.411811F;
            this.line46.Y1 = 0F;
            this.line46.Y2 = 0.3149598F;
            // 
            // line47
            // 
            this.line47.Height = 0.3149598F;
            this.line47.Left = 3.012599F;
            this.line47.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line47.LineWeight = 1F;
            this.line47.Name = "line47";
            this.line47.Top = 0F;
            this.line47.Width = 0F;
            this.line47.X1 = 3.012599F;
            this.line47.X2 = 3.012599F;
            this.line47.Y1 = 0F;
            this.line47.Y2 = 0.3149598F;
            // 
            // line48
            // 
            this.line48.Height = 0.3149598F;
            this.line48.Left = 3.280315F;
            this.line48.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line48.LineWeight = 1F;
            this.line48.Name = "line48";
            this.line48.Top = 0F;
            this.line48.Width = 0F;
            this.line48.X1 = 3.280315F;
            this.line48.X2 = 3.280315F;
            this.line48.Y1 = 0F;
            this.line48.Y2 = 0.3149598F;
            // 
            // line49
            // 
            this.line49.Height = 0.3149598F;
            this.line49.Left = 3.87126F;
            this.line49.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line49.LineWeight = 1F;
            this.line49.Name = "line49";
            this.line49.Top = 0F;
            this.line49.Width = 0F;
            this.line49.X1 = 3.87126F;
            this.line49.X2 = 3.87126F;
            this.line49.Y1 = 0F;
            this.line49.Y2 = 0.3149598F;
            // 
            // line50
            // 
            this.line50.Height = 0.3149598F;
            this.line50.Left = 4.142914F;
            this.line50.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line50.LineWeight = 1F;
            this.line50.Name = "line50";
            this.line50.Top = 0F;
            this.line50.Width = 0F;
            this.line50.X1 = 4.142914F;
            this.line50.X2 = 4.142914F;
            this.line50.Y1 = 0F;
            this.line50.Y2 = 0.3149598F;
            // 
            // line51
            // 
            this.line51.Height = 0.3149598F;
            this.line51.Left = 6.788977F;
            this.line51.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line51.LineWeight = 1F;
            this.line51.Name = "line51";
            this.line51.Top = 0F;
            this.line51.Width = 9.536743E-07F;
            this.line51.X1 = 6.788977F;
            this.line51.X2 = 6.788978F;
            this.line51.Y1 = 0F;
            this.line51.Y2 = 0.3149598F;
            // 
            // line52
            // 
            this.line52.Height = 0.3149598F;
            this.line52.Left = 6.515749F;
            this.line52.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line52.LineWeight = 1F;
            this.line52.Name = "line52";
            this.line52.Top = 0F;
            this.line52.Width = 0F;
            this.line52.X1 = 6.515749F;
            this.line52.X2 = 6.515749F;
            this.line52.Y1 = 0F;
            this.line52.Y2 = 0.3149598F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // txtSebshuCd
            // 
            this.txtSebshuCd.DataField = "ITEM10";
            this.txtSebshuCd.Height = 0.21875F;
            this.txtSebshuCd.Left = 2.363386F;
            this.txtSebshuCd.Name = "txtSebshuCd";
            this.txtSebshuCd.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.txtSebshuCd.Tag = "";
            this.txtSebshuCd.Text = "ITEM10";
            this.txtSebshuCd.Top = 0.4188976F;
            this.txtSebshuCd.Width = 0.7156007F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM04";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // groupHeader2
            // 
            this.groupHeader2.DataField = "ITEM10";
            this.groupHeader2.Height = 0F;
            this.groupHeader2.Name = "groupHeader2";
            this.groupHeader2.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.line12,
            this.line13,
            this.line14,
            this.line16,
            this.line17,
            this.line19,
            this.line20,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.line31,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line38,
            this.line39,
            this.line53,
            this.textBox12,
            this.label8,
            this.line54,
            this.txtSebshuCd});
            this.groupFooter2.Height = 0.6874999F;
            this.groupFooter2.Name = "groupFooter2";
            this.groupFooter2.Format += new System.EventHandler(this.groupFooter2_Format);
            // 
            // textBox3
            // 
            this.textBox3.Height = 0.2362205F;
            this.textBox3.Left = 0.03937008F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox3.Tag = "";
            this.textBox3.Text = "合計";
            this.textBox3.Top = 0.03937008F;
            this.textBox3.Width = 0.4519686F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM08";
            this.textBox4.Height = 0.2362205F;
            this.textBox4.Left = 0.4913386F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox4.SummaryGroup = "groupHeader2";
            this.textBox4.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox4.Tag = "";
            this.textBox4.Text = null;
            this.textBox4.Top = 0.03937008F;
            this.textBox4.Width = 0.3937007F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM15";
            this.textBox5.Height = 0.2362205F;
            this.textBox5.Left = 0.8929132F;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.textBox5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox5.SummaryGroup = "groupHeader2";
            this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox5.Tag = "";
            this.textBox5.Text = "9,999,999.99";
            this.textBox5.Top = 0.03937008F;
            this.textBox5.Width = 0.944882F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM16";
            this.textBox7.Height = 0.2362205F;
            this.textBox7.Left = 1.837795F;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.textBox7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox7.SummaryGroup = "groupHeader2";
            this.textBox7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox7.Tag = "";
            this.textBox7.Text = "999,999,999";
            this.textBox7.Top = 0.03937008F;
            this.textBox7.Width = 0.8661417F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM17";
            this.textBox8.Height = 0.2362205F;
            this.textBox8.Left = 2.703937F;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.textBox8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox8.SummaryGroup = "groupHeader2";
            this.textBox8.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox8.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox8.Tag = "";
            this.textBox8.Text = "999,999,999";
            this.textBox8.Top = 0.03937008F;
            this.textBox8.Width = 0.8661417F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM18";
            this.textBox9.Height = 0.2362205F;
            this.textBox9.Left = 3.570079F;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.textBox9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox9.SummaryGroup = "groupHeader2";
            this.textBox9.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox9.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox9.Tag = "";
            this.textBox9.Text = "999,999,999";
            this.textBox9.Top = 0.03937008F;
            this.textBox9.Width = 0.8661417F;
            // 
            // textBox10
            // 
            this.textBox10.CanGrow = false;
            this.textBox10.Height = 0.2362205F;
            this.textBox10.Left = 4.436221F;
            this.textBox10.MultiLine = false;
            this.textBox10.Name = "textBox10";
            this.textBox10.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.textBox10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox10.Tag = "";
            this.textBox10.Text = null;
            this.textBox10.Top = 0.03937008F;
            this.textBox10.Width = 1.7563F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM20";
            this.textBox11.Height = 0.2362205F;
            this.textBox11.Left = 6.220079F;
            this.textBox11.Name = "textBox11";
            this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
            this.textBox11.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.textBox11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox11.SummaryGroup = "groupHeader2";
            this.textBox11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox11.Tag = "";
            this.textBox11.Text = "999,999,999";
            this.textBox11.Top = 0.03937008F;
            this.textBox11.Width = 0.8661417F;
            // 
            // line12
            // 
            this.line12.Height = 0.3149603F;
            this.line12.Left = 6.220079F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.003543315F;
            this.line12.Width = 0F;
            this.line12.X1 = 6.220079F;
            this.line12.X2 = 6.220079F;
            this.line12.Y1 = 0.003543315F;
            this.line12.Y2 = 0.3185036F;
            // 
            // line13
            // 
            this.line13.Height = 0.3149607F;
            this.line13.Left = 0.4913386F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 7.450581E-09F;
            this.line13.Width = 0F;
            this.line13.X1 = 0.4913386F;
            this.line13.X2 = 0.4913386F;
            this.line13.Y1 = 7.450581E-09F;
            this.line13.Y2 = 0.3149607F;
            // 
            // line14
            // 
            this.line14.Height = 0.3149607F;
            this.line14.Left = 0.8850396F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 7.450581E-09F;
            this.line14.Width = 0F;
            this.line14.X1 = 0.8850396F;
            this.line14.X2 = 0.8850396F;
            this.line14.Y1 = 7.450581E-09F;
            this.line14.Y2 = 0.3149607F;
            // 
            // line16
            // 
            this.line16.Height = 0.3149607F;
            this.line16.Left = 1.837795F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 7.450581E-09F;
            this.line16.Width = 0F;
            this.line16.X1 = 1.837795F;
            this.line16.X2 = 1.837795F;
            this.line16.Y1 = 7.450581E-09F;
            this.line16.Y2 = 0.3149607F;
            // 
            // line17
            // 
            this.line17.Height = 0.3149607F;
            this.line17.Left = 2.703937F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 7.450581E-09F;
            this.line17.Width = 0.0001809597F;
            this.line17.X1 = 2.703937F;
            this.line17.X2 = 2.704118F;
            this.line17.Y1 = 7.450581E-09F;
            this.line17.Y2 = 0.3149607F;
            // 
            // line19
            // 
            this.line19.Height = 0.3149607F;
            this.line19.Left = 4.436221F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 7.450581E-09F;
            this.line19.Width = 0F;
            this.line19.X1 = 4.436221F;
            this.line19.X2 = 4.436221F;
            this.line19.Y1 = 7.450581E-09F;
            this.line19.Y2 = 0.3149607F;
            // 
            // line20
            // 
            this.line20.Height = 0F;
            this.line20.Left = 0.03937008F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.3149607F;
            this.line20.Width = 7.049998F;
            this.line20.X1 = 0.03937008F;
            this.line20.X2 = 7.089368F;
            this.line20.Y1 = 0.3149607F;
            this.line20.Y2 = 0.3149607F;
            // 
            // line22
            // 
            this.line22.Height = 0.3070867F;
            this.line22.Left = 7.086222F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 7.450581E-09F;
            this.line22.Width = 0F;
            this.line22.X1 = 7.086222F;
            this.line22.X2 = 7.086222F;
            this.line22.Y1 = 7.450581E-09F;
            this.line22.Y2 = 0.3070867F;
            // 
            // line23
            // 
            this.line23.Height = 0.3149607F;
            this.line23.Left = 3.570079F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 7.450581E-09F;
            this.line23.Width = 0F;
            this.line23.X1 = 3.570079F;
            this.line23.X2 = 3.570079F;
            this.line23.Y1 = 7.450581E-09F;
            this.line23.Y2 = 0.3149607F;
            // 
            // line24
            // 
            this.line24.Height = 0.3149603F;
            this.line24.Left = 0.03937008F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.003543315F;
            this.line24.Width = 0F;
            this.line24.X1 = 0.03937008F;
            this.line24.X2 = 0.03937008F;
            this.line24.Y1 = 0.003543315F;
            this.line24.Y2 = 0.3185036F;
            // 
            // line25
            // 
            this.line25.Height = 0.3149598F;
            this.line25.Left = 1.617323F;
            this.line25.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 7.450581E-09F;
            this.line25.Width = 0F;
            this.line25.X1 = 1.617323F;
            this.line25.X2 = 1.617323F;
            this.line25.Y1 = 7.450581E-09F;
            this.line25.Y2 = 0.3149598F;
            // 
            // line26
            // 
            this.line26.Height = 0.3149598F;
            this.line26.Left = 1.344882F;
            this.line26.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 7.450581E-09F;
            this.line26.Width = 0F;
            this.line26.X1 = 1.344882F;
            this.line26.X2 = 1.344882F;
            this.line26.Y1 = 7.450581E-09F;
            this.line26.Y2 = 0.3149598F;
            // 
            // line27
            // 
            this.line27.Height = 0.3149598F;
            this.line27.Left = 1.072441F;
            this.line27.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 7.450581E-09F;
            this.line27.Width = 0F;
            this.line27.X1 = 1.072441F;
            this.line27.X2 = 1.072441F;
            this.line27.Y1 = 7.450581E-09F;
            this.line27.Y2 = 0.3149598F;
            // 
            // line31
            // 
            this.line31.Height = 0.3149598F;
            this.line31.Left = 2.140157F;
            this.line31.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 7.450581E-09F;
            this.line31.Width = 0F;
            this.line31.X1 = 2.140157F;
            this.line31.X2 = 2.140157F;
            this.line31.Y1 = 7.450581E-09F;
            this.line31.Y2 = 0.3149598F;
            // 
            // line32
            // 
            this.line32.Height = 0.3149598F;
            this.line32.Left = 2.411811F;
            this.line32.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Top = 7.450581E-09F;
            this.line32.Width = 0F;
            this.line32.X1 = 2.411811F;
            this.line32.X2 = 2.411811F;
            this.line32.Y1 = 7.450581E-09F;
            this.line32.Y2 = 0.3149598F;
            // 
            // line33
            // 
            this.line33.Height = 0.3149598F;
            this.line33.Left = 3.012599F;
            this.line33.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Top = 7.450581E-09F;
            this.line33.Width = 0F;
            this.line33.X1 = 3.012599F;
            this.line33.X2 = 3.012599F;
            this.line33.Y1 = 7.450581E-09F;
            this.line33.Y2 = 0.3149598F;
            // 
            // line34
            // 
            this.line34.Height = 0.3149598F;
            this.line34.Left = 3.280314F;
            this.line34.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Top = 7.450581E-09F;
            this.line34.Width = 0F;
            this.line34.X1 = 3.280314F;
            this.line34.X2 = 3.280314F;
            this.line34.Y1 = 7.450581E-09F;
            this.line34.Y2 = 0.3149598F;
            // 
            // line35
            // 
            this.line35.Height = 0.3149598F;
            this.line35.Left = 3.871259F;
            this.line35.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Top = 7.450581E-09F;
            this.line35.Width = 0F;
            this.line35.X1 = 3.871259F;
            this.line35.X2 = 3.871259F;
            this.line35.Y1 = 7.450581E-09F;
            this.line35.Y2 = 0.3149598F;
            // 
            // line38
            // 
            this.line38.Height = 0.3149598F;
            this.line38.Left = 4.142914F;
            this.line38.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line38.LineWeight = 1F;
            this.line38.Name = "line38";
            this.line38.Top = 7.450581E-09F;
            this.line38.Width = 0F;
            this.line38.X1 = 4.142914F;
            this.line38.X2 = 4.142914F;
            this.line38.Y1 = 7.450581E-09F;
            this.line38.Y2 = 0.3149598F;
            // 
            // line39
            // 
            this.line39.Height = 0.3149598F;
            this.line39.Left = 6.788977F;
            this.line39.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line39.LineWeight = 1F;
            this.line39.Name = "line39";
            this.line39.Top = 7.450581E-09F;
            this.line39.Width = 9.536743E-07F;
            this.line39.X1 = 6.788977F;
            this.line39.X2 = 6.788978F;
            this.line39.Y1 = 7.450581E-09F;
            this.line39.Y2 = 0.3149598F;
            // 
            // line53
            // 
            this.line53.Height = 0.3149598F;
            this.line53.Left = 6.515749F;
            this.line53.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.line53.LineWeight = 1F;
            this.line53.Name = "line53";
            this.line53.Top = 7.450581E-09F;
            this.line53.Width = 0F;
            this.line53.X1 = 6.515749F;
            this.line53.X2 = 6.515749F;
            this.line53.Y1 = 7.450581E-09F;
            this.line53.Y2 = 0.3149598F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM10";
            this.textBox12.Height = 0.21875F;
            this.textBox12.Left = 5.424804F;
            this.textBox12.Name = "textBox12";
            this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
            this.textBox12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox12.Tag = "";
            this.textBox12.Text = "ITEM10";
            this.textBox12.Top = 0.4287401F;
            this.textBox12.Width = 1.215355F;
            // 
            // label8
            // 
            this.label8.Height = 0.2287402F;
            this.label8.HyperLink = null;
            this.label8.Left = 4.367717F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: bold; text-align: left; text" +
    "-justify: distribute-all-lines";
            this.label8.Text = "差引支払額";
            this.label8.Top = 0.4188977F;
            this.label8.Width = 1.057087F;
            // 
            // line54
            // 
            this.line54.Height = 0F;
            this.line54.Left = 4.367717F;
            this.line54.LineWeight = 1F;
            this.line54.Name = "line54";
            this.line54.Top = 0.6476378F;
            this.line54.Width = 2.272441F;
            this.line54.X1 = 4.367717F;
            this.line54.X2 = 6.640158F;
            this.line54.Y1 = 0.6476378F;
            this.line54.Y2 = 0.6476378F;
            // 
            // HNDR1091R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.5905512F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.096405F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            this.ReportStart += new System.EventHandler(this.HNDR1091R_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunaPageCnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKensu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAvgTanka1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingaku1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKoujo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKoujoNm1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKojoKin1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSebshuCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKensu1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSuryo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAvgTanka1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKingaku1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKoujo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKoujoNm1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKojoKin1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCnt;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunaPageCnt;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line45;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSebshuCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line line46;
        private GrapeCity.ActiveReports.SectionReportModel.Line line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line line49;
        private GrapeCity.ActiveReports.SectionReportModel.Line line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line line52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line line50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line53;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line54;
    }
}
