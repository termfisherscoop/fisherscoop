﻿using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hndr1091
{
    /// <summary>
    /// HANR1021R の概要の説明です。
    /// </summary>
    public partial class HNDR1091R : BaseReport
    {
        private bool ColorFlag = false;

        public HNDR1091R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {
            try
            {
                if (this.ColorFlag)
                {
                    // 奇数行
                    detail.BackColor = System.Drawing.Color.Transparent;
                }
                else
                {
                    // 偶数行
                    detail.BackColor = System.Drawing.Color.LightCyan;
                }
                ColorFlag = !ColorFlag;
            }
            finally
            {
            }
        }

        private void pageHeader_Format(object sender, System.EventArgs e)
        {

        }

        private void HNDR1091R_ReportStart(object sender, System.EventArgs e)
        {

        }

        private void groupFooter2_Format(object sender, System.EventArgs e)
        {
            textBox12.Text = Util.ToString(Util.ToDecimal(textBox8.Text)  - Util.ToDecimal(textBox9.Text));
        }
    }
}
