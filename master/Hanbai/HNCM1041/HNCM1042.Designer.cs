﻿namespace jp.co.fsi.hn.hncm1041
{
    partial class HNCM1042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txtGyoshubnCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lbGyoshubnCd = new System.Windows.Forms.Label();
			this.txtGyoshubnNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lbGyoshubnKana = new System.Windows.Forms.Label();
			this.txtGyoshubnKana = new jp.co.fsi.common.controls.FsiTextBox();
			this.lbGyoshubnNM = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 118);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(477, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(466, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "魚種分類マスタ登録";
			// 
			// txtGyoshubnCd
			// 
			this.txtGyoshubnCd.AutoSizeFromLength = true;
			this.txtGyoshubnCd.DisplayLength = null;
			this.txtGyoshubnCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyoshubnCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtGyoshubnCd.Location = new System.Drawing.Point(132, 4);
			this.txtGyoshubnCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyoshubnCd.MaxLength = 7;
			this.txtGyoshubnCd.Name = "txtGyoshubnCd";
			this.txtGyoshubnCd.Size = new System.Drawing.Size(72, 23);
			this.txtGyoshubnCd.TabIndex = 1;
			this.txtGyoshubnCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyoshubnCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshubnCd_Validating);
			// 
			// lbGyoshubnCd
			// 
			this.lbGyoshubnCd.BackColor = System.Drawing.Color.Silver;
			this.lbGyoshubnCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbGyoshubnCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbGyoshubnCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lbGyoshubnCd.Location = new System.Drawing.Point(0, 0);
			this.lbGyoshubnCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbGyoshubnCd.Name = "lbGyoshubnCd";
			this.lbGyoshubnCd.Size = new System.Drawing.Size(444, 33);
			this.lbGyoshubnCd.TabIndex = 0;
			this.lbGyoshubnCd.Tag = "CHANGE";
			this.lbGyoshubnCd.Text = "魚種分類コード";
			this.lbGyoshubnCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyoshubnNm
			// 
			this.txtGyoshubnNm.AutoSizeFromLength = false;
			this.txtGyoshubnNm.DisplayLength = null;
			this.txtGyoshubnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyoshubnNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtGyoshubnNm.Location = new System.Drawing.Point(132, 4);
			this.txtGyoshubnNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyoshubnNm.MaxLength = 20;
			this.txtGyoshubnNm.Name = "txtGyoshubnNm";
			this.txtGyoshubnNm.Size = new System.Drawing.Size(292, 23);
			this.txtGyoshubnNm.TabIndex = 3;
			this.txtGyoshubnNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshubnNm_Validating);
			// 
			// lbGyoshubnKana
			// 
			this.lbGyoshubnKana.BackColor = System.Drawing.Color.Silver;
			this.lbGyoshubnKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbGyoshubnKana.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbGyoshubnKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lbGyoshubnKana.Location = new System.Drawing.Point(0, 0);
			this.lbGyoshubnKana.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbGyoshubnKana.Name = "lbGyoshubnKana";
			this.lbGyoshubnKana.Size = new System.Drawing.Size(444, 35);
			this.lbGyoshubnKana.TabIndex = 4;
			this.lbGyoshubnKana.Tag = "CHANGE";
			this.lbGyoshubnKana.Text = "魚種分類カナ名";
			this.lbGyoshubnKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyoshubnKana
			// 
			this.txtGyoshubnKana.AutoSizeFromLength = false;
			this.txtGyoshubnKana.DisplayLength = null;
			this.txtGyoshubnKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyoshubnKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtGyoshubnKana.Location = new System.Drawing.Point(132, 5);
			this.txtGyoshubnKana.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyoshubnKana.MaxLength = 20;
			this.txtGyoshubnKana.Name = "txtGyoshubnKana";
			this.txtGyoshubnKana.Size = new System.Drawing.Size(292, 23);
			this.txtGyoshubnKana.TabIndex = 5;
			this.txtGyoshubnKana.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
			this.txtGyoshubnKana.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshubnKana_Validating);
			// 
			// lbGyoshubnNM
			// 
			this.lbGyoshubnNM.BackColor = System.Drawing.Color.Silver;
			this.lbGyoshubnNM.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbGyoshubnNM.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbGyoshubnNM.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lbGyoshubnNM.Location = new System.Drawing.Point(0, 0);
			this.lbGyoshubnNM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbGyoshubnNM.Name = "lbGyoshubnNM";
			this.lbGyoshubnNM.Size = new System.Drawing.Size(444, 33);
			this.lbGyoshubnNM.TabIndex = 2;
			this.lbGyoshubnNM.Tag = "CHANGE";
			this.lbGyoshubnNM.Text = "魚種分類名";
			this.lbGyoshubnNM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(8, 38);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(452, 123);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtGyoshubnKana);
			this.fsiPanel3.Controls.Add(this.lbGyoshubnKana);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 84);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(444, 35);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtGyoshubnNm);
			this.fsiPanel2.Controls.Add(this.lbGyoshubnNM);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 44);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(444, 33);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtGyoshubnCd);
			this.fsiPanel1.Controls.Add(this.lbGyoshubnCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(444, 33);
			this.fsiPanel1.TabIndex = 0;
			// 
			// HNCM1042
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(466, 255);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1042";
			this.ShowFButton = true;
			this.Text = "魚種分類マスタ登録";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtGyoshubnCd;
        private System.Windows.Forms.Label lbGyoshubnCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshubnNm;
        private System.Windows.Forms.Label lbGyoshubnKana;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshubnKana;
        private System.Windows.Forms.Label lbGyoshubnNM;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    };
}