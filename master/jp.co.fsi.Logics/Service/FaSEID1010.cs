﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace jp.co.fsi.Logics
{
    public class FaSEID1010
    {

        #region 変数
        private DaoSEID1010 dao;

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public FaSEID1010()
        {
            dao = new DaoSEID1010();
        }

        /// <summary>
        /// ここにＳＱＬ文などは書く
        /// </summary>
        /// <returns></returns>
        public DataSet SelectAccountCustomers()
        {

            // Conditionテーブルの作成

            DataTable loCondition = new DataTable();

            string strSql = @"
                 SELECT
                      会社コード
                      取引先コード
                      取引先名
                      取引先カナ名
                      郵便番号１
                      郵便番号２
                      住所１
                      住所２
                      住所３
                      電話番号
                      ＦＡＸ番号
                      担当者コード
                      締日
                      回収月
                      回収日
                      請求準備月
                      請求締月
                      請求書発行
                      請求書形式
                      消費税転嫁方法
                      消費税端数処理
                      略称
                      船名CD
                      漁法CD
                      地区CD
                      組合手数料率
                      普通口座番号
                      普通口座区分
                      積立区分
                      積立率
                      積立口座番号
                      積立口座区分
                      支所区分
                      正準区分
                      振込先区分
                      普通割合
                      その他割合
                      パヤオ割合
                      登録日付
                      更新日付
                      処理FLG
                      消費税転嫁方法活魚
                      消費税端数処理活魚
                      加入年月日
                      消費税転嫁方法活仕入
                      消費税端数処理活仕入
                      請求準備月_購買
                      請求締月_購買
                      締日_活魚
                      請求書発行_活魚
                      生年月日
                      脱退日
                      脱退理由
                      支払区分
                      預り金積立率
                      預り金口座番号
                      預り金口座区分
                      購買天引率
            ";

            return dao.SelectAccountCustomers(strSql, loCondition);

        }
    }
}
