﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.fsi.Logics.constants
{
    public static class Constants
    {
        #region 列挙体
        /// <summary>
        /// サブシステム
        /// </summary>
        public enum SubSys
        {
            /// <summary>
            /// 共通
            /// </summary>
            Com = 0,
            /// <summary>
            /// 販売
            /// </summary>
            Han = 1,
            /// <summary>
            /// 購買
            /// </summary>
            Kob = 2,
            /// <summary>
            /// 財務
            /// </summary>
            Zai = 3,
            /// <summary>
            /// 給与
            /// </summary>
            Kyu = 4,
            /// <summary>
            /// 食堂販売
            /// </summary>
            Skd = 5,
            /// <summary>
            /// テスト(開発用)
            /// </summary>
            Test = 9
        }
        #endregion

        #region 定数
        /// <summary>
        /// 伝票番号の増分
        /// </summary>
        public const int DP_NO_ZOBUN = 1;

        /// <summary>
        /// 伝票番号の最小値
        /// </summary>
        public const int DP_NO_MIN = 1;

        /// <summary>
        /// 伝票番号の最大値
        /// </summary>
        public const int DP_NO_MAX = 999999;

        /// <summary>
        /// 帳票MDBファイル配置のディレクトリ
        /// </summary>
        public const string REP_DIR = "Report";

        /// <summary>
        /// ログファイル配置のディレクトリ
        /// </summary>
        public const string LOG_DIR = "Log";

        /// <summary>
        /// エラーログのファイル名
        /// </summary>
        public const string ERR_LOG_NM = "error_log";

        /// <summary>
        /// 勘定科目コード(現金)
        /// </summary>
        public const int KANJO_KAMOKU_CD_GENKIN = 100;

        /// <summary>
        /// 稼働開始年度
        /// </summary>
        /// <remarks>
        /// ユーザーによって設定値が異なるので、見落とさないよう注意が必要
        /// </remarks>
        public const int SYSTEM_START_YEAR = 2015;
        #endregion
    }
}
