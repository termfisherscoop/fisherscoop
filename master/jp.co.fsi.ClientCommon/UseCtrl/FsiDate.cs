﻿using jp.co.fsi.common.controls;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace jp.co.fsi.ClientCommon
{

    public enum DispDateFormat
    {
        年月日 = 1,
        年月   = 2,
        年     = 3,
        月日   = 4,
        日     = 5
    }

    public delegate void ChangeOrgnDateEventHandler(object sender);


    public class FsiDate : FlowLayoutPanel
    {

        public event ChangeOrgnDateEventHandler ChangeOrgnDate;

        #region ログ
        readonly log4net.ILog log =
            log4net.LogManager.GetLogger(
                System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private string[] jpDate;

        private DispDateFormat enumFormat;

        private DbAccess d;

        public DbAccess DbConnect
        {
            get { return d; }
            set { d = value; }
        }

        #region Dispose実装
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion

        /// <summary>
        /// 元号
        /// </summary>
        private Label lblGengo;

        private FsiTextBox txtYear;

        private FsiTextBox txtMonth;

        private FsiTextBox txtDays;

        private Label lblYear;
        private Label lblMonth;
        private Label lblDay;

        private Button btnF1;

        private string strOpenDialogExe;

        // 日付変数
        string strSeirekiYmd;
        string strSYear=string.Empty;
        string strWYear = string.Empty;
        string strMonth = string.Empty;
        string strDays = string.Empty;
        string strGengo = string.Empty;

        bool isChanged;        

        public Button ExecBtn
        {
            get { return btnF1; }
            set 
            { 
                btnF1 = value;

                if (null == btnF1)
                    return;
            }
        }

        public DispDateFormat DispFormat
        {
            get { return enumFormat; }
            set { 
                
                enumFormat = value;

                if (0 == enumFormat)
                    enumFormat = DispDateFormat.年月日;

                changeField();
            }

        }
        
        public string[] JpDate
        {
            get { return jpDate; }
            set { 
                jpDate = value;

                if (null == jpDate)
                    return;

                if (0 < jpDate.Length)
                {
                    lblGengo.Text = jpDate[0];
                    txtYear.Text = jpDate[2];
                    txtMonth.Text = jpDate[3];
                    txtDays.Text = jpDate[4];

                    strGengo = jpDate[0];
                    strWYear = jpDate[2];
                    strMonth = jpDate[3];
                    strDays = jpDate[4];

                }
                else
                {
                    lblGengo.Text = string.Empty;
                    txtYear.Text = string.Empty;
                    txtMonth.Text = string.Empty;
                    txtDays.Text = string.Empty;

                    strGengo = "";
                    strWYear = "";
                    strMonth = "";
                    strDays = "";
                }
            }
        }
 
        public string SeirekiYmd
        {
            get { return strSeirekiYmd; }
            set { strSeirekiYmd = value;
           
            }
        }

        public string Gengo
        {
            get
            {
                return strGengo;
            }
            set 
            { 
                strGengo = value;

                lblGengo.Text = value;
            
            
            }
        }

        public string SYear
        {
            get { return strSYear; }
            set { 
                
                strSYear = value; 
            
            }
        }


        public string WYear
        {
            get { return strWYear; }
            set 
            { 
                strWYear = value;
                txtYear.Text = value;
            
            }
        }

        public string Month
        {
            get { return strMonth; }
            set { 
                strMonth = value;

                txtMonth.Text = strMonth;
            
            }
        }
        public string Day
        {
            get { return strDays; }
            set { 
                
                strDays = value;

                txtDays.Text = value;
            }
        }

        public string OpenDialogExe 
        {
            get { return strOpenDialogExe; }
            set 
            { 
                strOpenDialogExe = value; 
            } 
        }

        public FsiDate()
        {
            isChanged = false;
            initComponent();
        }

        #region コンポーネントイニシャライズ
        private void initComponent()
        {

            this.SuspendLayout();

            // 
            // FsiDate
            // 
            this.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                System.Drawing.FontStyle.Regular, 
                System.Drawing.GraphicsUnit.Point, ((byte)(128)));

            // 左から右に並べる
            this.FlowDirection = FlowDirection.LeftToRight;

            this.TabStop = true;

            lblGengo = new Label();
            lblGengo.Width = 53;
            lblGengo.Height = 23;
            lblGengo.AutoSize = false;
            lblGengo.BorderStyle = BorderStyle.Fixed3D;
            lblGengo.Margin = new Padding(3, 3, 3, 3);
            lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;

            lblGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte)(128)));

            lblGengo.Name = "lblGengo";

            lblGengo.BackColor = System.Drawing.Color.LightCyan;

            lblGengo.ForeColor = System.Drawing.Color.Navy;

            txtYear = new FsiTextBox();
            txtYear.Width = 30;
            txtYear.Height = 23;
            txtYear.Enter += Txt_Enter;
            txtYear.Validating += Txt_Validating;
            txtYear.TabIndex = 1;
            txtYear.MaxLength = 2;
            txtYear.TextAlign = HorizontalAlignment.Right;
            txtYear.Margin = new Padding(1, 3, 1, 3);
            txtYear.KeyDown += TxtYear_KeyDown;
            
            txtYear.Name = "txtYear";

            txtYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte)(128)));

            txtMonth = new FsiTextBox();
            txtMonth.Width = 30;
            txtMonth.Height = 23;
            txtMonth.Enter += Txt_Enter;
            txtMonth.Validating += Txt_Validating;
            txtMonth.TabIndex = 2;
            txtMonth.MaxLength = 2;
            txtMonth.TextAlign = HorizontalAlignment.Right;
            txtMonth.Margin = new Padding(1, 3, 1, 3);
            txtMonth.Name = "txtMonth";

            txtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte)(128)));


            txtDays = new FsiTextBox();
            txtDays.Width = 30;
            txtDays.Height = 23;
            txtDays.Enter += Txt_Enter;
            txtDays.Validating += Txt_Validating;
            txtDays.TabIndex = 3;
            txtDays.MaxLength = 2;
            txtDays.TextAlign = HorizontalAlignment.Right;
            txtDays.Margin = new Padding(1, 3, 1, 3);
            txtDays.Name = "txtDays";

            txtDays.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte)(128)));

            lblYear = new Label();
            lblYear.AutoSize = false;
            lblYear.Width = 24;
            lblYear.Height = 24;
            lblYear.Margin = new Padding(1, 3, 1, 3);
            lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            lblYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte)(128)));


            lblMonth = new Label();
            lblMonth.AutoSize = false;
            lblMonth.Width = 24;
            lblMonth.Height = 24;
            lblMonth.Margin = new Padding(1, 3, 1, 3);
            lblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            lblMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte)(128)));


            lblDay = new Label();
            lblDay.AutoSize = false;
            lblDay.Width = 24;
            lblDay.Height = 24;
            lblDay.Margin = new Padding(1, 3, 1, 3);
            lblDay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            lblDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte)(128)));

            lblYear.Text = "年";
            lblMonth.Text = "月";
            lblDay.Text = "日";

            if (enumFormat == 0)
                enumFormat = DispDateFormat.年月日;

            changeField();

            this.Controls.AddRange(
                new Control[] { lblGengo, 
                                txtYear , 
                                lblYear, 
                                txtMonth , 
                                lblMonth , 
                                txtDays, 
                                lblDay });

            this.ResumeLayout(false);

        }

        private void TxtYear_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.F1)
            {
                string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

                String[] result;

                result = this.openSearchWindow(stCurrentDir + @"\EXE\" + this.OpenDialogExe, "", this.lblGengo.Text);
                if (!ValChk.IsEmpty(result[0]))
                {
                    this.lblGengo.Text = result[1];

                    // 存在しない日付の場合、補正して存在する日付に戻す
                    SetJp();
                }
            }

        }

        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);

            if (null == lblYear) return;

            lblYear.Font = this.Font;
            lblMonth.Font = this.Font;
            lblDay.Font = this.Font;

            lblGengo.Font = this.Font;

            txtYear.Font = this.Font;
            txtMonth.Font = this.Font;
            txtDays.Font = this.Font;
        }


        protected override void OnBackColorChanged(EventArgs e)
        {
            base.OnBackColorChanged(e);

            if (null == lblYear) return;

            lblYear.BackColor = this.BackColor;
            lblMonth.BackColor = this.BackColor;
            lblDay.BackColor = this.BackColor;
        }

        private void Txt_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {

            string strMsgs = string.Empty;

            if (!IsOrgnDate(sender, ((FsiTextBox)sender).Name))
            {
                MessageBox.Show("日付妥当性エラーです。正しく日付を入力してください。",
                    "日付妥当性エラー",MessageBoxButtons.OK,MessageBoxIcon.Error);
                e.Cancel = true;

                if (string.IsNullOrEmpty(this.txtYear.Text))
                {
                    this.txtYear.Focus();
                }

                if ((this.txtYear.Text == "0"))
                {
                    this.txtYear.Focus();
                }

                if (string.IsNullOrEmpty(this.txtMonth.Text))
                {
                    this.txtMonth.Focus();
                }

                if ((this.txtMonth.Text == "0") || (Util.ToInt(this.txtMonth.Text) > 12))
                {
                    this.txtMonth.Focus();
                }

                if (string.IsNullOrEmpty(this.txtDays.Text))
                {
                    this.txtDays.Focus();
                }

                if ((this.txtDays.Text == "0") || (Util.ToInt(this.txtDays.Text) > 31))
                {
                    this.txtDays.Focus();
                }

            }
        }
        private void Txt_Enter(object sender, EventArgs e)
        {
            isChanged = false;
            ((FsiTextBox)sender).SelectAll();

            if (((FsiTextBox)sender).Name == "txtYear")
            {
                btnF1.Enabled = true;
            }
            else
            {
                btnF1.Enabled = false;
            }

        }
		#endregion

		protected override void OnGotFocus(EventArgs e)
		{
			txtYear.Focus();
			txtYear.SelectAll();
			base.OnGotFocus(e);
		}

		protected override void OnEnter(EventArgs e)
        {
            txtYear.Focus();
            txtYear.SelectAll();

        }

        private bool IsOrgnDate(object sender,string objName)
        {
            bool ret = true;

            FsiTextBox cText = (FsiTextBox)sender;

            try
            {
                switch(objName)
                {
                    case "txtYear":
                        if (!IsValid.IsYear(cText.Text, cText.MaxLength))
                        {
                            ret = false;
                        }
                        else
                        {
                            cText.Text = 
                                Util.ToString(IsValid.SetYear(cText.Text));
                            CheckJp();
                            SetJp();
                        }
                        break;
                    case "txtMonth":
                        if (!IsValid.IsMonth(cText.Text, cText.MaxLength))
                        {
                            ret = false;
                        }
                        else
                        {
                            cText.Text =
                                Util.ToString(IsValid.SetMonth(cText.Text));
                            CheckJp();
                            SetJp();
                        }
                        break;
                    case "txtDays":
                        if (!IsValid.IsDay(cText.Text, cText.MaxLength))
                        {
                            ret = false;
                        }
                        else
                        {
                            cText.Text =
                                Util.ToString(IsValid.SetDay(cText.Text));
                            CheckJp();
                            SetJp();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Exceptionエラー" + ex.Message);
                log.Error("日付コントロールでエラーが発生した。" + ex.Message, ex);
                ret = false;
            }
            return ret;
        }

        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)

            if (string.IsNullOrEmpty(this.txtMonth.Text))
            {
                this.txtMonth.Text = "1";
            }
            else if (Util.ToInt(this.txtMonth.Text) <1)
            {
                this.txtMonth.Text = "1";
            }
            else if (Util.ToInt(this.txtMonth.Text) > 12)
            {
                this.txtMonth.Text = "12";
            }

            if (string.IsNullOrEmpty(this.txtDays.Text))
            {
                this.txtDays.Text = "1";
            }
            else if (Util.ToInt(this.txtDays.Text) < 1)
            {
                this.txtDays.Text = "1";
            }

            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, "1", this.d);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDays.Text) > lastDayInMonth)
            {
                this.txtDays.Text = Util.ToString(lastDayInMonth);
            }
        }

        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, this.txtDays.Text, this.d));
        }

        private void SetJp(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDays.Text = arrJpDate[4];

            strGengo = arrJpDate[0];
            strWYear = arrJpDate[2];
            strMonth = arrJpDate[3];
            strDays = arrJpDate[4];

            if (null != ChangeOrgnDate)
            {
                ChangeOrgnDate(this);
            }
        }

        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();


            // ネームスペースの末尾
            //string nameSpace = lowerModuleName.Substring(0, 3);
            string nameSpace = lowerModuleName.Substring(0, 2);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            string moduleNameSpace = string.Empty;

            foreach (Type tt in asm.GetTypes())
            {

                moduleNameSpace = tt.Namespace;

            }

            moduleNameSpace += "." + this.OpenDialogExe;

            try
            {
                Type t = asm.GetType(moduleNameSpace);

                if (t != null)
                {
                    Object obj = Activator.CreateInstance(t);
                    if (obj != null)
                    {
                        BasePgForm frm = (BasePgForm)obj;
                        frm.Par1 = para1;
                        frm.InData = indata;
                        frm.ShowDialog(this);

                        if (frm.DialogResult == DialogResult.OK)
                        {
                            string[] ret = (string[])frm.OutData;
                            result[0] = ret[0];
                            result[1] = ret[1];
                            return result;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error("元号画面を開くときにエラーが発生した。"+Environment.NewLine + ex.Message,ex);
            }

            return result;
        }

        private void changeField()
        {
            switch (enumFormat)
            {
                case DispDateFormat.年月日:

                    lblGengo.Visible = true;
                    lblYear.Visible = true;
                    lblMonth.Visible = true;
                    lblDay.Visible = true;
                    txtYear.Visible = true;
                    txtMonth.Visible = true;
                    txtDays.Visible = true;
                    break;
                case DispDateFormat.年月:
                    lblGengo.Visible = true;
                    lblYear.Visible = true;
                    lblMonth.Visible = true;
                    lblDay.Visible = false;
                    txtYear.Visible = true;
                    txtMonth.Visible = true;
                    txtDays.Visible = false;
                    break;
                case DispDateFormat.日:
                    lblGengo.Visible = false;
                    lblYear.Visible = false;
                    lblMonth.Visible = false;
                    lblDay.Visible = true;
                    txtYear.Visible = false;
                    txtMonth.Visible = false;
                    txtDays.Visible = true;
                    break;
                case DispDateFormat.月日:
                    lblGengo.Visible = false;
                    lblYear.Visible = false;
                    lblMonth.Visible = true;
                    lblDay.Visible = false;
                    txtYear.Visible = false;
                    txtMonth.Visible = true;
                    txtDays.Visible = false;
                    break;
                case DispDateFormat.年:
                    lblGengo.Visible = true;
                    lblYear.Visible = true;
                    lblMonth.Visible = false;
                    lblDay.Visible = false;
                    txtYear.Visible = false;
                    txtMonth.Visible = false;
                    txtDays.Visible = false;
                    break;
            }
        }
    }
}
