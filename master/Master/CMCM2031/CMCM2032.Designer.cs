﻿namespace jp.co.fsi.cm.cmcm2031
{
    partial class CMCM2032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblShishoCd = new System.Windows.Forms.Label();
            this.txtShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblAddBar = new System.Windows.Forms.Label();
            this.txtYubinBango2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFax = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTel = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYubinBango1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShishoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShishoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFax = new System.Windows.Forms.Label();
            this.lblTel = new System.Windows.Forms.Label();
            this.lblJusho2 = new System.Windows.Forms.Label();
            this.lblJusho1 = new System.Windows.Forms.Label();
            this.lblYubinBango = new System.Windows.Forms.Label();
            this.lblShishoKanaNm = new System.Windows.Forms.Label();
            this.lblShishoNm = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.panel9 = new jp.co.fsi.common.FsiPanel();
            this.panel8 = new jp.co.fsi.common.FsiPanel();
            this.panel7 = new jp.co.fsi.common.FsiPanel();
            this.panel6 = new jp.co.fsi.common.FsiPanel();
            this.panel5 = new jp.co.fsi.common.FsiPanel();
            this.panel4 = new jp.co.fsi.common.FsiPanel();
            this.panel3 = new jp.co.fsi.common.FsiPanel();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4";
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5";
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12";
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(12, 373);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1133, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.None;
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1366, 31);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            this.lblTitle.Visible = false;
            // 
            // lblShishoCd
            // 
            this.lblShishoCd.BackColor = System.Drawing.Color.Silver;
            this.lblShishoCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoCd.Location = new System.Drawing.Point(0, 0);
            this.lblShishoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShishoCd.Name = "lblShishoCd";
            this.lblShishoCd.Size = new System.Drawing.Size(663, 30);
            this.lblShishoCd.TabIndex = 1;
            this.lblShishoCd.Tag = "CHANGE";
            this.lblShishoCd.Text = "支所コード";
            this.lblShishoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShishoCd
            // 
            this.txtShishoCd.AutoSizeFromLength = false;
            this.txtShishoCd.DisplayLength = null;
            this.txtShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoCd.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtShishoCd.Location = new System.Drawing.Point(144, 4);
            this.txtShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtShishoCd.MaxLength = 4;
            this.txtShishoCd.Name = "txtShishoCd";
            this.txtShishoCd.Size = new System.Drawing.Size(52, 23);
            this.txtShishoCd.TabIndex = 2;
            this.txtShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCd_Validating);
            // 
            // lblAddBar
            // 
            this.lblAddBar.AutoSize = true;
            this.lblAddBar.BackColor = System.Drawing.Color.Silver;
            this.lblAddBar.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblAddBar.Location = new System.Drawing.Point(204, 7);
            this.lblAddBar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddBar.Name = "lblAddBar";
            this.lblAddBar.Size = new System.Drawing.Size(16, 16);
            this.lblAddBar.TabIndex = 8;
            this.lblAddBar.Tag = "CHANGE";
            this.lblAddBar.Text = "-";
            // 
            // txtYubinBango2
            // 
            this.txtYubinBango2.AutoSizeFromLength = false;
            this.txtYubinBango2.BackColor = System.Drawing.Color.White;
            this.txtYubinBango2.DisplayLength = null;
            this.txtYubinBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango2.Location = new System.Drawing.Point(229, 3);
            this.txtYubinBango2.Margin = new System.Windows.Forms.Padding(4);
            this.txtYubinBango2.MaxLength = 4;
            this.txtYubinBango2.Name = "txtYubinBango2";
            this.txtYubinBango2.Size = new System.Drawing.Size(60, 23);
            this.txtYubinBango2.TabIndex = 9;
            this.txtYubinBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango2_Validating);
            // 
            // txtFax
            // 
            this.txtFax.AutoSizeFromLength = false;
            this.txtFax.BackColor = System.Drawing.Color.White;
            this.txtFax.DisplayLength = null;
            this.txtFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFax.Location = new System.Drawing.Point(144, 3);
            this.txtFax.Margin = new System.Windows.Forms.Padding(4);
            this.txtFax.MaxLength = 15;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(220, 23);
            this.txtFax.TabIndex = 17;
            this.txtFax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFax_KeyDown);
            this.txtFax.Validating += new System.ComponentModel.CancelEventHandler(this.txtFax_Validating);
            // 
            // txtTel
            // 
            this.txtTel.AutoSizeFromLength = false;
            this.txtTel.BackColor = System.Drawing.Color.White;
            this.txtTel.DisplayLength = null;
            this.txtTel.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTel.Location = new System.Drawing.Point(144, 3);
            this.txtTel.Margin = new System.Windows.Forms.Padding(4);
            this.txtTel.MaxLength = 15;
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(220, 23);
            this.txtTel.TabIndex = 15;
            this.txtTel.Validating += new System.ComponentModel.CancelEventHandler(this.txtTel_Validating);
            // 
            // txtJusho2
            // 
            this.txtJusho2.AutoSizeFromLength = false;
            this.txtJusho2.BackColor = System.Drawing.Color.White;
            this.txtJusho2.DisplayLength = null;
            this.txtJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho2.Location = new System.Drawing.Point(144, 3);
            this.txtJusho2.Margin = new System.Windows.Forms.Padding(4);
            this.txtJusho2.MaxLength = 30;
            this.txtJusho2.Name = "txtJusho2";
            this.txtJusho2.Size = new System.Drawing.Size(428, 23);
            this.txtJusho2.TabIndex = 13;
            this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho2_Validating);
            // 
            // txtJusho1
            // 
            this.txtJusho1.AutoSizeFromLength = false;
            this.txtJusho1.BackColor = System.Drawing.Color.White;
            this.txtJusho1.DisplayLength = null;
            this.txtJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho1.Location = new System.Drawing.Point(144, 4);
            this.txtJusho1.Margin = new System.Windows.Forms.Padding(4);
            this.txtJusho1.MaxLength = 30;
            this.txtJusho1.Name = "txtJusho1";
            this.txtJusho1.Size = new System.Drawing.Size(428, 23);
            this.txtJusho1.TabIndex = 11;
            this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho1_Validating);
            // 
            // txtYubinBango1
            // 
            this.txtYubinBango1.AutoSizeFromLength = false;
            this.txtYubinBango1.BackColor = System.Drawing.Color.White;
            this.txtYubinBango1.DisplayLength = null;
            this.txtYubinBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango1.Location = new System.Drawing.Point(144, 3);
            this.txtYubinBango1.Margin = new System.Windows.Forms.Padding(4);
            this.txtYubinBango1.MaxLength = 3;
            this.txtYubinBango1.Name = "txtYubinBango1";
            this.txtYubinBango1.Size = new System.Drawing.Size(51, 23);
            this.txtYubinBango1.TabIndex = 7;
            this.txtYubinBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango1_Validating);
            // 
            // txtShishoKanaNm
            // 
            this.txtShishoKanaNm.AutoSizeFromLength = false;
            this.txtShishoKanaNm.BackColor = System.Drawing.Color.White;
            this.txtShishoKanaNm.DisplayLength = null;
            this.txtShishoKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtShishoKanaNm.Location = new System.Drawing.Point(144, 3);
            this.txtShishoKanaNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtShishoKanaNm.MaxLength = 30;
            this.txtShishoKanaNm.Name = "txtShishoKanaNm";
            this.txtShishoKanaNm.Size = new System.Drawing.Size(319, 23);
            this.txtShishoKanaNm.TabIndex = 3;
            this.txtShishoKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoKanaNm_Validating);
            // 
            // txtShishoNm
            // 
            this.txtShishoNm.AutoSizeFromLength = false;
            this.txtShishoNm.BackColor = System.Drawing.Color.White;
            this.txtShishoNm.DisplayLength = null;
            this.txtShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtShishoNm.Location = new System.Drawing.Point(144, 3);
            this.txtShishoNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtShishoNm.MaxLength = 40;
            this.txtShishoNm.Name = "txtShishoNm";
            this.txtShishoNm.Size = new System.Drawing.Size(428, 23);
            this.txtShishoNm.TabIndex = 1;
            this.txtShishoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoNm_Validating);
            // 
            // lblFax
            // 
            this.lblFax.BackColor = System.Drawing.Color.Silver;
            this.lblFax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFax.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFax.Location = new System.Drawing.Point(0, 0);
            this.lblFax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFax.Name = "lblFax";
            this.lblFax.Size = new System.Drawing.Size(663, 31);
            this.lblFax.TabIndex = 16;
            this.lblFax.Tag = "CHANGE";
            this.lblFax.Text = "ＦＡＸ番号";
            this.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTel
            // 
            this.lblTel.BackColor = System.Drawing.Color.Silver;
            this.lblTel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTel.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTel.Location = new System.Drawing.Point(0, 0);
            this.lblTel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(663, 30);
            this.lblTel.TabIndex = 14;
            this.lblTel.Tag = "CHANGE";
            this.lblTel.Text = "電話番号";
            this.lblTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho2
            // 
            this.lblJusho2.BackColor = System.Drawing.Color.Silver;
            this.lblJusho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJusho2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho2.Location = new System.Drawing.Point(0, 0);
            this.lblJusho2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJusho2.Name = "lblJusho2";
            this.lblJusho2.Size = new System.Drawing.Size(663, 30);
            this.lblJusho2.TabIndex = 12;
            this.lblJusho2.Tag = "CHANGE";
            this.lblJusho2.Text = "住所２";
            this.lblJusho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho1
            // 
            this.lblJusho1.BackColor = System.Drawing.Color.Silver;
            this.lblJusho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJusho1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho1.Location = new System.Drawing.Point(0, 0);
            this.lblJusho1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJusho1.Name = "lblJusho1";
            this.lblJusho1.Size = new System.Drawing.Size(663, 30);
            this.lblJusho1.TabIndex = 10;
            this.lblJusho1.Tag = "CHANGE";
            this.lblJusho1.Text = "住所１";
            this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYubinBango
            // 
            this.lblYubinBango.BackColor = System.Drawing.Color.Silver;
            this.lblYubinBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYubinBango.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblYubinBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYubinBango.Location = new System.Drawing.Point(0, 0);
            this.lblYubinBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYubinBango.Name = "lblYubinBango";
            this.lblYubinBango.Size = new System.Drawing.Size(663, 30);
            this.lblYubinBango.TabIndex = 6;
            this.lblYubinBango.Tag = "CHANGE";
            this.lblYubinBango.Text = "郵便番号";
            this.lblYubinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShishoKanaNm
            // 
            this.lblShishoKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblShishoKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShishoKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShishoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoKanaNm.Location = new System.Drawing.Point(0, 0);
            this.lblShishoKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShishoKanaNm.Name = "lblShishoKanaNm";
            this.lblShishoKanaNm.Size = new System.Drawing.Size(663, 30);
            this.lblShishoKanaNm.TabIndex = 2;
            this.lblShishoKanaNm.Tag = "CHANGE";
            this.lblShishoKanaNm.Text = "支所カナ名";
            this.lblShishoKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShishoNm
            // 
            this.lblShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShishoNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoNm.Location = new System.Drawing.Point(0, 0);
            this.lblShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShishoNm.Name = "lblShishoNm";
            this.lblShishoNm.Size = new System.Drawing.Size(663, 30);
            this.lblShishoNm.TabIndex = 0;
            this.lblShishoNm.Tag = "CHANGE";
            this.lblShishoNm.Text = "支　所　名";
            this.lblShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel9, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 49);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(673, 314);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtFax);
            this.panel9.Controls.Add(this.lblFax);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(5, 278);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(663, 31);
            this.panel9.TabIndex = 7;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.txtTel);
            this.panel8.Controls.Add(this.lblTel);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(5, 239);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(663, 30);
            this.panel8.TabIndex = 6;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtJusho2);
            this.panel7.Controls.Add(this.lblJusho2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(5, 200);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(663, 30);
            this.panel7.TabIndex = 5;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.txtJusho1);
            this.panel6.Controls.Add(this.lblJusho1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(5, 161);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(663, 30);
            this.panel6.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblAddBar);
            this.panel5.Controls.Add(this.txtYubinBango1);
            this.panel5.Controls.Add(this.txtYubinBango2);
            this.panel5.Controls.Add(this.lblYubinBango);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(5, 122);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(663, 30);
            this.panel5.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtShishoKanaNm);
            this.panel4.Controls.Add(this.lblShishoKanaNm);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(5, 83);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(663, 30);
            this.panel4.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtShishoNm);
            this.panel3.Controls.Add(this.lblShishoNm);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(5, 44);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(663, 30);
            this.panel3.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtShishoCd);
            this.panel2.Controls.Add(this.lblShishoCd);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(5, 5);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(663, 30);
            this.panel2.TabIndex = 0;
            // 
            // CMCM2032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 506);
            this.Controls.Add(this.tableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "CMCM2032";
            this.Text = "支所の登録";
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblShishoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShishoCd;
        private System.Windows.Forms.Label lblFax;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.Label lblJusho2;
        private System.Windows.Forms.Label lblJusho1;
        private System.Windows.Forms.Label lblYubinBango;
        private System.Windows.Forms.Label lblShishoKanaNm;
        private System.Windows.Forms.Label lblShishoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShishoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtFax;
        private jp.co.fsi.common.controls.FsiTextBox txtTel;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho2;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho1;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango1;
        private jp.co.fsi.common.controls.FsiTextBox txtShishoKanaNm;
        private System.Windows.Forms.Label lblAddBar;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango2;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel1;
        private jp.co.fsi.common.FsiPanel panel9;
        private jp.co.fsi.common.FsiPanel panel8;
        private jp.co.fsi.common.FsiPanel panel7;
        private jp.co.fsi.common.FsiPanel panel6;
        private jp.co.fsi.common.FsiPanel panel5;
        private jp.co.fsi.common.FsiPanel panel4;
        private jp.co.fsi.common.FsiPanel panel3;
        private jp.co.fsi.common.FsiPanel panel2;
    }
}