﻿namespace jp.co.fsi.cm.cmcm2011
{
    partial class CMCM2012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblFunanushiCd = new System.Windows.Forms.Label();
			this.txtFunanushiCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.gbxSenshuInfo = new System.Windows.Forms.GroupBox();
			this.textBox14 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txthyoji = new jp.co.fsi.common.controls.FsiTextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.txtMyNumberHide = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMyNumber = new jp.co.fsi.common.controls.FsiTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtDattaiYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanyuYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtJpYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDattaiDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanyuDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDattaiMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanyuMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKanyuJpYMD = new System.Windows.Forms.Label();
			this.lblDattaiJpYMD = new System.Windows.Forms.Label();
			this.txtJpDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtJpMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblJpYMD = new System.Windows.Forms.Label();
			this.txtKaishuBi = new jp.co.fsi.common.controls.FsiTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.lblDattaiGengo = new System.Windows.Forms.Label();
			this.lblKanyuGengo = new System.Windows.Forms.Label();
			this.lblJpGengo = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
			this.lblShzHsSrMemo = new System.Windows.Forms.Label();
			this.lblSkshKskMemo = new System.Windows.Forms.Label();
			this.lblSkshHkMemo = new System.Windows.Forms.Label();
			this.lblShimebiMemo = new System.Windows.Forms.Label();
			this.lblSeikyusakiNm = new System.Windows.Forms.Label();
			this.txtKaishuTsuki = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeikyushoKeishiki = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeikyushoHakko = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShimebi = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeikyusakiCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
			this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
			this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
			this.lblTantoshaNm = new System.Windows.Forms.Label();
			this.lblAddBar = new System.Windows.Forms.Label();
			this.txtYubinBango2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDattaiRiyu = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKingakuHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtFax = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTel = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtYubinBango1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRyFunanushiNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtFunanushiKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtFunanushiNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDattaiRiyu = new System.Windows.Forms.Label();
			this.lblDattaiJp = new System.Windows.Forms.Label();
			this.lblKanyuJp = new System.Windows.Forms.Label();
			this.lblJp = new System.Windows.Forms.Label();
			this.lblKaishuTsuki = new System.Windows.Forms.Label();
			this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
			this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
			this.lblSeikyushoKeishiki = new System.Windows.Forms.Label();
			this.lblSeikyushoHakko = new System.Windows.Forms.Label();
			this.lblShimebi = new System.Windows.Forms.Label();
			this.lblSeikyusakiCd = new System.Windows.Forms.Label();
			this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
			this.lblKingakuHasuShori = new System.Windows.Forms.Label();
			this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
			this.lblTantoshaCd = new System.Windows.Forms.Label();
			this.lblFax = new System.Windows.Forms.Label();
			this.lblTel = new System.Windows.Forms.Label();
			this.lblJusho2 = new System.Windows.Forms.Label();
			this.lblJusho1 = new System.Windows.Forms.Label();
			this.lblYubinBango = new System.Windows.Forms.Label();
			this.lblRyFunanushiNm = new System.Windows.Forms.Label();
			this.lblFunanushiKanaNm = new System.Windows.Forms.Label();
			this.lblFunanushiNm = new System.Windows.Forms.Label();
			this.txtFutsuKozaKubun = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtFutsuKozaBango = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFutsuKozaBango = new System.Windows.Forms.Label();
			this.lblFutsuKozaKubunSelect = new System.Windows.Forms.Label();
			this.lblFutsuKozaKubun = new System.Windows.Forms.Label();
			this.txtTsumitateKozaKubun = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTsumitateRitsu = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTsumitateKozaBango = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTsumitateRitsu = new System.Windows.Forms.Label();
			this.lblTsumitateKozaBango = new System.Windows.Forms.Label();
			this.lblTsumitateKozaKubunMemo = new System.Windows.Forms.Label();
			this.lblTsumitateKozaKubun = new System.Windows.Forms.Label();
			this.txtKozaKubunAzukari = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTsumitateRitsuAzukari = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKozaBangoAzukari = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTsumitateRitsuAzukari = new System.Windows.Forms.Label();
			this.lblKozaBangoAzukari = new System.Windows.Forms.Label();
			this.lblKozaKubunAzukari = new System.Windows.Forms.Label();
			this.lblKozaKubunAzukariSelect = new System.Windows.Forms.Label();
			this.txtKozaKubunHonnin = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTsumitateRitsuHonnin = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKozaBangoHonnin = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTsumitateRitsuHonnin = new System.Windows.Forms.Label();
			this.lblKozaBangoHonnin = new System.Windows.Forms.Label();
			this.lblKozaKubunHonnin = new System.Windows.Forms.Label();
			this.lblKozaKubunHonninSelect = new System.Windows.Forms.Label();
			this.txtPayaoRitsu = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSonotaRitsu = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtFutsuRitsu = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFurikomisakiWariai = new System.Windows.Forms.Label();
			this.lblPayaoRitsu = new System.Windows.Forms.Label();
			this.lblSonotaRitsu = new System.Windows.Forms.Label();
			this.lblFutsuRitsu = new System.Windows.Forms.Label();
			this.lblShishoKubunMemo = new System.Windows.Forms.Label();
			this.lblSeijunKubunMemo = new System.Windows.Forms.Label();
			this.txtSeijunKubun = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeijunKubun = new System.Windows.Forms.Label();
			this.txtShishoKubun = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShishoKubun = new System.Windows.Forms.Label();
			this.lblShiharaiKubunMemo = new System.Windows.Forms.Label();
			this.txtShiharaiKubun = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiharaiKubun = new System.Windows.Forms.Label();
			this.txtKumiaiTesuryoRitsu = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKumiaiTesuryoRitsu = new System.Windows.Forms.Label();
			this.lblChikuCdSub = new System.Windows.Forms.Label();
			this.lblGyohoCdSub = new System.Windows.Forms.Label();
			this.txtChikuCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtGyohoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFuneNmCdSub = new System.Windows.Forms.Label();
			this.txtFuneNmCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblChikuCd = new System.Windows.Forms.Label();
			this.lblGyohoCd = new System.Windows.Forms.Label();
			this.lblFuneNmCd = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel22 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel21 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel20 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel19 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel18 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel17 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel16 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel15 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel3 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel26 = new jp.co.fsi.common.FsiPanel();
			this.label8 = new System.Windows.Forms.Label();
			this.fsiPanel25 = new jp.co.fsi.common.FsiPanel();
			this.label7 = new System.Windows.Forms.Label();
			this.fsiPanel24 = new jp.co.fsi.common.FsiPanel();
			this.label6 = new System.Windows.Forms.Label();
			this.fsiPanel23 = new jp.co.fsi.common.FsiPanel();
			this.label9 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.gbxSenshuInfo.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiTableLayoutPanel2.SuspendLayout();
			this.fsiPanel22.SuspendLayout();
			this.fsiPanel21.SuspendLayout();
			this.fsiPanel20.SuspendLayout();
			this.fsiPanel19.SuspendLayout();
			this.fsiPanel18.SuspendLayout();
			this.fsiPanel17.SuspendLayout();
			this.fsiPanel16.SuspendLayout();
			this.fsiPanel15.SuspendLayout();
			this.fsiTableLayoutPanel3.SuspendLayout();
			this.fsiPanel26.SuspendLayout();
			this.fsiPanel25.SuspendLayout();
			this.fsiPanel24.SuspendLayout();
			this.fsiPanel23.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(5, 5);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(5, 64);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF2
			// 
			this.btnF2.Location = new System.Drawing.Point(91, 64);
			this.btnF2.Margin = new System.Windows.Forms.Padding(5);
			this.btnF2.Text = "F2";
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Location = new System.Drawing.Point(177, 64);
			this.btnF3.Margin = new System.Windows.Forms.Padding(5);
			this.btnF3.Text = "F3\r\n\r\n削除";
			// 
			// btnF4
			// 
			this.btnF4.Location = new System.Drawing.Point(263, 64);
			this.btnF4.Margin = new System.Windows.Forms.Padding(5);
			this.btnF4.Text = "F4";
			this.btnF4.Visible = false;
			// 
			// btnF5
			// 
			this.btnF5.Location = new System.Drawing.Point(349, 64);
			this.btnF5.Margin = new System.Windows.Forms.Padding(5);
			this.btnF5.Text = "F5";
			this.btnF5.Visible = false;
			// 
			// btnF7
			// 
			this.btnF7.Location = new System.Drawing.Point(521, 64);
			this.btnF7.Margin = new System.Windows.Forms.Padding(5);
			this.btnF7.Visible = false;
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(435, 64);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			this.btnF6.Text = "F6\r\n\r\n登録";
			// 
			// btnF8
			// 
			this.btnF8.Location = new System.Drawing.Point(607, 64);
			this.btnF8.Margin = new System.Windows.Forms.Padding(5);
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Location = new System.Drawing.Point(693, 64);
			this.btnF9.Margin = new System.Windows.Forms.Padding(5);
			this.btnF9.Visible = false;
			// 
			// btnF12
			// 
			this.btnF12.Location = new System.Drawing.Point(951, 64);
			this.btnF12.Margin = new System.Windows.Forms.Padding(5);
			this.btnF12.Text = "F12";
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Location = new System.Drawing.Point(865, 64);
			this.btnF11.Margin = new System.Windows.Forms.Padding(5);
			this.btnF11.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Location = new System.Drawing.Point(779, 64);
			this.btnF10.Margin = new System.Windows.Forms.Padding(5);
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(19, 685);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1078, 129);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1126, 28);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			this.lblTitle.Visible = false;
			// 
			// lblFunanushiCd
			// 
			this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCd.Location = new System.Drawing.Point(0, 0);
			this.lblFunanushiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCd.Name = "lblFunanushiCd";
			this.lblFunanushiCd.Size = new System.Drawing.Size(1092, 31);
			this.lblFunanushiCd.TabIndex = 0;
			this.lblFunanushiCd.Tag = "CHANGE";
			this.lblFunanushiCd.Text = "船　主　C　D";
			this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCd
			// 
			this.txtFunanushiCd.AutoSizeFromLength = false;
			this.txtFunanushiCd.DisplayLength = null;
			this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtFunanushiCd.Location = new System.Drawing.Point(127, 4);
			this.txtFunanushiCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiCd.MaxLength = 4;
			this.txtFunanushiCd.Name = "txtFunanushiCd";
			this.txtFunanushiCd.Size = new System.Drawing.Size(52, 23);
			this.txtFunanushiCd.TabIndex = 1;
			this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCd_Validating);
			// 
			// gbxSenshuInfo
			// 
			this.gbxSenshuInfo.Controls.Add(this.textBox14);
			this.gbxSenshuInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.gbxSenshuInfo.Location = new System.Drawing.Point(831, 53);
			this.gbxSenshuInfo.Margin = new System.Windows.Forms.Padding(4);
			this.gbxSenshuInfo.Name = "gbxSenshuInfo";
			this.gbxSenshuInfo.Padding = new System.Windows.Forms.Padding(4);
			this.gbxSenshuInfo.Size = new System.Drawing.Size(48, 57);
			this.gbxSenshuInfo.TabIndex = 5;
			this.gbxSenshuInfo.TabStop = false;
			// 
			// textBox14
			// 
			this.textBox14.AllowDrop = true;
			this.textBox14.AutoSizeFromLength = false;
			this.textBox14.DisplayLength = null;
			this.textBox14.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.textBox14.Location = new System.Drawing.Point(1225, -199);
			this.textBox14.Margin = new System.Windows.Forms.Padding(4);
			this.textBox14.MaxLength = 4;
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new System.Drawing.Size(65, 23);
			this.textBox14.TabIndex = 938;
			// 
			// txthyoji
			// 
			this.txthyoji.AutoSizeFromLength = false;
			this.txthyoji.BackColor = System.Drawing.Color.White;
			this.txthyoji.DisplayLength = null;
			this.txthyoji.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txthyoji.Location = new System.Drawing.Point(126, 288);
			this.txthyoji.Margin = new System.Windows.Forms.Padding(4);
			this.txthyoji.MaxLength = 1;
			this.txthyoji.Name = "txthyoji";
			this.txthyoji.Size = new System.Drawing.Size(29, 23);
			this.txthyoji.TabIndex = 12;
			this.txthyoji.Text = "0";
			this.txthyoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txthyoji.Validating += new System.ComponentModel.CancelEventHandler(this.txthyoji_Validating);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label4.Location = new System.Drawing.Point(162, 291);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(200, 16);
			this.label4.TabIndex = 78;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "0:表示する 1:表示しない ";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.BackColor = System.Drawing.Color.Silver;
			this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label5.Location = new System.Drawing.Point(4, 293);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 76;
			this.label5.Tag = "CHANGE";
			this.label5.Text = "一覧表示";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.SkyBlue;
			this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.button1.ForeColor = System.Drawing.Color.Navy;
			this.button1.Location = new System.Drawing.Point(718, 274);
			this.button1.Margin = new System.Windows.Forms.Padding(4);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(100, 24);
			this.button1.TabIndex = 45;
			this.button1.TabStop = false;
			this.button1.Text = "公開";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtMyNumberHide
			// 
			this.txtMyNumberHide.AutoSizeFromLength = false;
			this.txtMyNumberHide.DisplayLength = null;
			this.txtMyNumberHide.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMyNumberHide.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtMyNumberHide.Location = new System.Drawing.Point(564, 274);
			this.txtMyNumberHide.Margin = new System.Windows.Forms.Padding(4);
			this.txtMyNumberHide.MaxLength = 15;
			this.txtMyNumberHide.Name = "txtMyNumberHide";
			this.txtMyNumberHide.ReadOnly = true;
			this.txtMyNumberHide.Size = new System.Drawing.Size(153, 23);
			this.txtMyNumberHide.TabIndex = 32;
			this.txtMyNumberHide.TabStop = false;
			this.txtMyNumberHide.Text = "************";
			this.txtMyNumberHide.Visible = false;
			// 
			// txtMyNumber
			// 
			this.txtMyNumber.AutoSizeFromLength = false;
			this.txtMyNumber.DisplayLength = null;
			this.txtMyNumber.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMyNumber.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtMyNumber.Location = new System.Drawing.Point(564, 274);
			this.txtMyNumber.Margin = new System.Windows.Forms.Padding(4);
			this.txtMyNumber.MaxLength = 12;
			this.txtMyNumber.Name = "txtMyNumber";
			this.txtMyNumber.ReadOnly = true;
			this.txtMyNumber.Size = new System.Drawing.Size(151, 23);
			this.txtMyNumber.TabIndex = 31;
			this.txtMyNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtMyNumber_Validating);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label3.Location = new System.Drawing.Point(448, 279);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(104, 16);
			this.label3.TabIndex = 72;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "マイナンバー";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDattaiYear
			// 
			this.txtDattaiYear.AutoSizeFromLength = false;
			this.txtDattaiYear.BackColor = System.Drawing.Color.White;
			this.txtDattaiYear.DisplayLength = null;
			this.txtDattaiYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDattaiYear.Location = new System.Drawing.Point(625, 223);
			this.txtDattaiYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtDattaiYear.MaxLength = 2;
			this.txtDattaiYear.Name = "txtDattaiYear";
			this.txtDattaiYear.Size = new System.Drawing.Size(41, 23);
			this.txtDattaiYear.TabIndex = 27;
			this.txtDattaiYear.Text = "0";
			this.txtDattaiYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDattaiYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDattaiYear_Validating);
			// 
			// txtKanyuYear
			// 
			this.txtKanyuYear.AutoSizeFromLength = false;
			this.txtKanyuYear.BackColor = System.Drawing.Color.White;
			this.txtKanyuYear.DisplayLength = null;
			this.txtKanyuYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKanyuYear.Location = new System.Drawing.Point(625, 197);
			this.txtKanyuYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanyuYear.MaxLength = 2;
			this.txtKanyuYear.Name = "txtKanyuYear";
			this.txtKanyuYear.Size = new System.Drawing.Size(41, 23);
			this.txtKanyuYear.TabIndex = 24;
			this.txtKanyuYear.Text = "0";
			this.txtKanyuYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKanyuYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanyuYear_Validating);
			// 
			// txtJpYear
			// 
			this.txtJpYear.AutoSizeFromLength = false;
			this.txtJpYear.BackColor = System.Drawing.Color.White;
			this.txtJpYear.DisplayLength = null;
			this.txtJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtJpYear.Location = new System.Drawing.Point(625, 172);
			this.txtJpYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtJpYear.MaxLength = 2;
			this.txtJpYear.Name = "txtJpYear";
			this.txtJpYear.Size = new System.Drawing.Size(41, 23);
			this.txtJpYear.TabIndex = 21;
			this.txtJpYear.Text = "0";
			this.txtJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpYear_Validating);
			// 
			// txtDattaiDay
			// 
			this.txtDattaiDay.AutoSizeFromLength = false;
			this.txtDattaiDay.BackColor = System.Drawing.Color.White;
			this.txtDattaiDay.DisplayLength = null;
			this.txtDattaiDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDattaiDay.Location = new System.Drawing.Point(775, 223);
			this.txtDattaiDay.Margin = new System.Windows.Forms.Padding(4);
			this.txtDattaiDay.MaxLength = 2;
			this.txtDattaiDay.Name = "txtDattaiDay";
			this.txtDattaiDay.Size = new System.Drawing.Size(41, 23);
			this.txtDattaiDay.TabIndex = 29;
			this.txtDattaiDay.Text = "0";
			this.txtDattaiDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDattaiDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDattaiDay_Validating);
			// 
			// txtKanyuDay
			// 
			this.txtKanyuDay.AutoSizeFromLength = false;
			this.txtKanyuDay.BackColor = System.Drawing.Color.White;
			this.txtKanyuDay.DisplayLength = null;
			this.txtKanyuDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKanyuDay.Location = new System.Drawing.Point(775, 197);
			this.txtKanyuDay.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanyuDay.MaxLength = 2;
			this.txtKanyuDay.Name = "txtKanyuDay";
			this.txtKanyuDay.Size = new System.Drawing.Size(41, 23);
			this.txtKanyuDay.TabIndex = 26;
			this.txtKanyuDay.Text = "0";
			this.txtKanyuDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKanyuDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanyuDay_Validating);
			// 
			// txtDattaiMonth
			// 
			this.txtDattaiMonth.AutoSizeFromLength = false;
			this.txtDattaiMonth.BackColor = System.Drawing.Color.White;
			this.txtDattaiMonth.DisplayLength = null;
			this.txtDattaiMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDattaiMonth.Location = new System.Drawing.Point(702, 223);
			this.txtDattaiMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtDattaiMonth.MaxLength = 2;
			this.txtDattaiMonth.Name = "txtDattaiMonth";
			this.txtDattaiMonth.Size = new System.Drawing.Size(41, 23);
			this.txtDattaiMonth.TabIndex = 28;
			this.txtDattaiMonth.Text = "0";
			this.txtDattaiMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDattaiMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDattaiMonth_Validating);
			// 
			// txtKanyuMonth
			// 
			this.txtKanyuMonth.AutoSizeFromLength = false;
			this.txtKanyuMonth.BackColor = System.Drawing.Color.White;
			this.txtKanyuMonth.DisplayLength = null;
			this.txtKanyuMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKanyuMonth.Location = new System.Drawing.Point(702, 197);
			this.txtKanyuMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanyuMonth.MaxLength = 2;
			this.txtKanyuMonth.Name = "txtKanyuMonth";
			this.txtKanyuMonth.Size = new System.Drawing.Size(41, 23);
			this.txtKanyuMonth.TabIndex = 25;
			this.txtKanyuMonth.Text = "0";
			this.txtKanyuMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKanyuMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanyuMonth_Validating);
			// 
			// lblKanyuJpYMD
			// 
			this.lblKanyuJpYMD.BackColor = System.Drawing.Color.Silver;
			this.lblKanyuJpYMD.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKanyuJpYMD.Location = new System.Drawing.Point(642, 196);
			this.lblKanyuJpYMD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanyuJpYMD.Name = "lblKanyuJpYMD";
			this.lblKanyuJpYMD.Size = new System.Drawing.Size(200, 27);
			this.lblKanyuJpYMD.TabIndex = 60;
			this.lblKanyuJpYMD.Tag = "CHANGE";
			this.lblKanyuJpYMD.Text = "年　　　 月　　　 日";
			this.lblKanyuJpYMD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblDattaiJpYMD
			// 
			this.lblDattaiJpYMD.BackColor = System.Drawing.Color.Silver;
			this.lblDattaiJpYMD.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDattaiJpYMD.Location = new System.Drawing.Point(642, 223);
			this.lblDattaiJpYMD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDattaiJpYMD.Name = "lblDattaiJpYMD";
			this.lblDattaiJpYMD.Size = new System.Drawing.Size(200, 27);
			this.lblDattaiJpYMD.TabIndex = 66;
			this.lblDattaiJpYMD.Tag = "CHANGE";
			this.lblDattaiJpYMD.Text = "年　　　 月 　　　日";
			this.lblDattaiJpYMD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// txtJpDay
			// 
			this.txtJpDay.AutoSizeFromLength = false;
			this.txtJpDay.BackColor = System.Drawing.Color.White;
			this.txtJpDay.DisplayLength = null;
			this.txtJpDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtJpDay.Location = new System.Drawing.Point(775, 172);
			this.txtJpDay.Margin = new System.Windows.Forms.Padding(4);
			this.txtJpDay.MaxLength = 2;
			this.txtJpDay.Name = "txtJpDay";
			this.txtJpDay.Size = new System.Drawing.Size(41, 23);
			this.txtJpDay.TabIndex = 23;
			this.txtJpDay.Text = "0";
			this.txtJpDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtJpDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpDay_Validating);
			// 
			// txtJpMonth
			// 
			this.txtJpMonth.AutoSizeFromLength = false;
			this.txtJpMonth.BackColor = System.Drawing.Color.White;
			this.txtJpMonth.DisplayLength = null;
			this.txtJpMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtJpMonth.Location = new System.Drawing.Point(702, 172);
			this.txtJpMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtJpMonth.MaxLength = 2;
			this.txtJpMonth.Name = "txtJpMonth";
			this.txtJpMonth.Size = new System.Drawing.Size(41, 23);
			this.txtJpMonth.TabIndex = 22;
			this.txtJpMonth.Text = "0";
			this.txtJpMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtJpMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpMonth_Validating);
			// 
			// lblJpYMD
			// 
			this.lblJpYMD.BackColor = System.Drawing.Color.Silver;
			this.lblJpYMD.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblJpYMD.Location = new System.Drawing.Point(642, 172);
			this.lblJpYMD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJpYMD.Name = "lblJpYMD";
			this.lblJpYMD.Size = new System.Drawing.Size(200, 27);
			this.lblJpYMD.TabIndex = 54;
			this.lblJpYMD.Tag = "CHANGE";
			this.lblJpYMD.Text = "年　　　 月　　 　日";
			this.lblJpYMD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// txtKaishuBi
			// 
			this.txtKaishuBi.AutoSizeFromLength = false;
			this.txtKaishuBi.BackColor = System.Drawing.Color.White;
			this.txtKaishuBi.DisplayLength = null;
			this.txtKaishuBi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKaishuBi.Location = new System.Drawing.Point(808, 145);
			this.txtKaishuBi.Margin = new System.Windows.Forms.Padding(4);
			this.txtKaishuBi.MaxLength = 2;
			this.txtKaishuBi.Name = "txtKaishuBi";
			this.txtKaishuBi.Size = new System.Drawing.Size(29, 23);
			this.txtKaishuBi.TabIndex = 20;
			this.txtKaishuBi.Text = "99";
			this.txtKaishuBi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKaishuBi.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuBi_Validating);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label2.Location = new System.Drawing.Point(842, 148);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 959;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "日";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblDattaiGengo
			// 
			this.lblDattaiGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDattaiGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDattaiGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDattaiGengo.Location = new System.Drawing.Point(567, 222);
			this.lblDattaiGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDattaiGengo.Name = "lblDattaiGengo";
			this.lblDattaiGengo.Size = new System.Drawing.Size(53, 24);
			this.lblDattaiGengo.TabIndex = 65;
			this.lblDattaiGengo.Tag = "DISPNAME";
			this.lblDattaiGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKanyuGengo
			// 
			this.lblKanyuGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblKanyuGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanyuGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKanyuGengo.Location = new System.Drawing.Point(567, 196);
			this.lblKanyuGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanyuGengo.Name = "lblKanyuGengo";
			this.lblKanyuGengo.Size = new System.Drawing.Size(53, 24);
			this.lblKanyuGengo.TabIndex = 59;
			this.lblKanyuGengo.Tag = "DISPNAME";
			this.lblKanyuGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJpGengo
			// 
			this.lblJpGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblJpGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJpGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblJpGengo.Location = new System.Drawing.Point(567, 171);
			this.lblJpGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJpGengo.Name = "lblJpGengo";
			this.lblJpGengo.Size = new System.Drawing.Size(53, 24);
			this.lblJpGengo.TabIndex = 53;
			this.lblJpGengo.Tag = "DISPNAME";
			this.lblJpGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label1.Location = new System.Drawing.Point(604, 149);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(200, 16);
			this.label1.TabIndex = 50;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "0:当月 1:翌月 2:翌々月…";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShzTnkHohoMemo
			// 
			this.lblShzTnkHohoMemo.AutoSize = true;
			this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(604, 124);
			this.lblShzTnkHohoMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
			this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(264, 16);
			this.lblShzTnkHohoMemo.TabIndex = 47;
			this.lblShzTnkHohoMemo.Tag = "CHANGE";
			this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
			this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShzHsSrMemo
			// 
			this.lblShzHsSrMemo.AutoSize = true;
			this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShzHsSrMemo.Location = new System.Drawing.Point(604, 99);
			this.lblShzHsSrMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
			this.lblShzHsSrMemo.Size = new System.Drawing.Size(264, 16);
			this.lblShzHsSrMemo.TabIndex = 44;
			this.lblShzHsSrMemo.Tag = "CHANGE";
			this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
			this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSkshKskMemo
			// 
			this.lblSkshKskMemo.AutoSize = true;
			this.lblSkshKskMemo.BackColor = System.Drawing.Color.Silver;
			this.lblSkshKskMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSkshKskMemo.Location = new System.Drawing.Point(604, 76);
			this.lblSkshKskMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSkshKskMemo.Name = "lblSkshKskMemo";
			this.lblSkshKskMemo.Size = new System.Drawing.Size(144, 16);
			this.lblSkshKskMemo.TabIndex = 41;
			this.lblSkshKskMemo.Tag = "CHANGE";
			this.lblSkshKskMemo.Text = "1:合計型 2:明細型";
			this.lblSkshKskMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSkshHkMemo
			// 
			this.lblSkshHkMemo.AutoSize = true;
			this.lblSkshHkMemo.BackColor = System.Drawing.Color.Silver;
			this.lblSkshHkMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSkshHkMemo.Location = new System.Drawing.Point(604, 53);
			this.lblSkshHkMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSkshHkMemo.Name = "lblSkshHkMemo";
			this.lblSkshHkMemo.Size = new System.Drawing.Size(128, 16);
			this.lblSkshHkMemo.TabIndex = 38;
			this.lblSkshHkMemo.Tag = "CHANGE";
			this.lblSkshHkMemo.Text = "1:する 2:しない";
			this.lblSkshHkMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShimebiMemo
			// 
			this.lblShimebiMemo.AutoSize = true;
			this.lblShimebiMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShimebiMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShimebiMemo.Location = new System.Drawing.Point(604, 30);
			this.lblShimebiMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShimebiMemo.Name = "lblShimebiMemo";
			this.lblShimebiMemo.Size = new System.Drawing.Size(152, 16);
			this.lblShimebiMemo.TabIndex = 35;
			this.lblShimebiMemo.Tag = "CHANGE";
			this.lblShimebiMemo.Text = "1～28 ※末締めは99";
			this.lblShimebiMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyusakiNm
			// 
			this.lblSeikyusakiNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeikyusakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeikyusakiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSeikyusakiNm.Location = new System.Drawing.Point(623, 1);
			this.lblSeikyusakiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyusakiNm.Name = "lblSeikyusakiNm";
			this.lblSeikyusakiNm.Size = new System.Drawing.Size(381, 24);
			this.lblSeikyusakiNm.TabIndex = 32;
			this.lblSeikyusakiNm.Tag = "DISPNAME";
			this.lblSeikyusakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKaishuTsuki
			// 
			this.txtKaishuTsuki.AutoSizeFromLength = false;
			this.txtKaishuTsuki.BackColor = System.Drawing.Color.White;
			this.txtKaishuTsuki.DisplayLength = null;
			this.txtKaishuTsuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKaishuTsuki.Location = new System.Drawing.Point(567, 145);
			this.txtKaishuTsuki.Margin = new System.Windows.Forms.Padding(4);
			this.txtKaishuTsuki.MaxLength = 1;
			this.txtKaishuTsuki.Name = "txtKaishuTsuki";
			this.txtKaishuTsuki.Size = new System.Drawing.Size(29, 23);
			this.txtKaishuTsuki.TabIndex = 19;
			this.txtKaishuTsuki.Text = "0";
			this.txtKaishuTsuki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKaishuTsuki.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuTsuki_Validating);
			// 
			// txtShohizeiTenkaHoho
			// 
			this.txtShohizeiTenkaHoho.AutoSizeFromLength = false;
			this.txtShohizeiTenkaHoho.BackColor = System.Drawing.Color.White;
			this.txtShohizeiTenkaHoho.DisplayLength = null;
			this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(567, 121);
			this.txtShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiTenkaHoho.MaxLength = 1;
			this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
			this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(29, 23);
			this.txtShohizeiTenkaHoho.TabIndex = 18;
			this.txtShohizeiTenkaHoho.Text = "2";
			this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
			// 
			// txtShohizeiHasuShori
			// 
			this.txtShohizeiHasuShori.AutoSizeFromLength = false;
			this.txtShohizeiHasuShori.BackColor = System.Drawing.Color.White;
			this.txtShohizeiHasuShori.DisplayLength = null;
			this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShohizeiHasuShori.Location = new System.Drawing.Point(567, 97);
			this.txtShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiHasuShori.MaxLength = 1;
			this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
			this.txtShohizeiHasuShori.Size = new System.Drawing.Size(29, 23);
			this.txtShohizeiHasuShori.TabIndex = 17;
			this.txtShohizeiHasuShori.Text = "2";
			this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
			// 
			// txtSeikyushoKeishiki
			// 
			this.txtSeikyushoKeishiki.AutoSizeFromLength = false;
			this.txtSeikyushoKeishiki.BackColor = System.Drawing.Color.White;
			this.txtSeikyushoKeishiki.DisplayLength = null;
			this.txtSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtSeikyushoKeishiki.Location = new System.Drawing.Point(567, 74);
			this.txtSeikyushoKeishiki.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeikyushoKeishiki.MaxLength = 1;
			this.txtSeikyushoKeishiki.Name = "txtSeikyushoKeishiki";
			this.txtSeikyushoKeishiki.Size = new System.Drawing.Size(29, 23);
			this.txtSeikyushoKeishiki.TabIndex = 16;
			this.txtSeikyushoKeishiki.Text = "2";
			this.txtSeikyushoKeishiki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeikyushoKeishiki.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoKeishiki_Validating);
			// 
			// txtSeikyushoHakko
			// 
			this.txtSeikyushoHakko.AutoSizeFromLength = false;
			this.txtSeikyushoHakko.BackColor = System.Drawing.Color.White;
			this.txtSeikyushoHakko.DisplayLength = null;
			this.txtSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtSeikyushoHakko.Location = new System.Drawing.Point(567, 50);
			this.txtSeikyushoHakko.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeikyushoHakko.MaxLength = 1;
			this.txtSeikyushoHakko.Name = "txtSeikyushoHakko";
			this.txtSeikyushoHakko.Size = new System.Drawing.Size(29, 23);
			this.txtSeikyushoHakko.TabIndex = 15;
			this.txtSeikyushoHakko.Text = "1";
			this.txtSeikyushoHakko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtShimebi
			// 
			this.txtShimebi.AutoSizeFromLength = false;
			this.txtShimebi.BackColor = System.Drawing.Color.White;
			this.txtShimebi.DisplayLength = null;
			this.txtShimebi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShimebi.Location = new System.Drawing.Point(567, 26);
			this.txtShimebi.Margin = new System.Windows.Forms.Padding(4);
			this.txtShimebi.MaxLength = 2;
			this.txtShimebi.Name = "txtShimebi";
			this.txtShimebi.Size = new System.Drawing.Size(29, 23);
			this.txtShimebi.TabIndex = 14;
			this.txtShimebi.Text = "99";
			this.txtShimebi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShimebi.Validating += new System.ComponentModel.CancelEventHandler(this.txtShimebi_Validating);
			// 
			// txtSeikyusakiCd
			// 
			this.txtSeikyusakiCd.AutoSizeFromLength = true;
			this.txtSeikyusakiCd.BackColor = System.Drawing.SystemColors.Window;
			this.txtSeikyusakiCd.DisplayLength = null;
			this.txtSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtSeikyusakiCd.Location = new System.Drawing.Point(567, 2);
			this.txtSeikyusakiCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeikyusakiCd.MaxLength = 4;
			this.txtSeikyusakiCd.Name = "txtSeikyusakiCd";
			this.txtSeikyusakiCd.Size = new System.Drawing.Size(52, 23);
			this.txtSeikyusakiCd.TabIndex = 13;
			this.txtSeikyusakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblShzNrkHohoNm
			// 
			this.lblShzNrkHohoNm.AutoSize = true;
			this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.Silver;
			this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShzNrkHohoNm.Location = new System.Drawing.Point(154, 268);
			this.lblShzNrkHohoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
			this.lblShzNrkHohoNm.Size = new System.Drawing.Size(216, 16);
			this.lblShzNrkHohoNm.TabIndex = 29;
			this.lblShzNrkHohoNm.Tag = "CHANGE";
			this.lblShzNrkHohoNm.Text = "税抜き入力（自動計算あり）";
			this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKgkHsShrMemo
			// 
			this.lblKgkHsShrMemo.AutoSize = true;
			this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKgkHsShrMemo.Location = new System.Drawing.Point(152, 243);
			this.lblKgkHsShrMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
			this.lblKgkHsShrMemo.Size = new System.Drawing.Size(248, 16);
			this.lblKgkHsShrMemo.TabIndex = 26;
			this.lblKgkHsShrMemo.Tag = "CHANGE";
			this.lblKgkHsShrMemo.Text = "1:切捨て 2:四捨五入 3:切り上げ";
			this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTnkStkHohoMemo
			// 
			this.lblTnkStkHohoMemo.AutoSize = true;
			this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(152, 219);
			this.lblTnkStkHohoMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
			this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(248, 16);
			this.lblTnkStkHohoMemo.TabIndex = 23;
			this.lblTnkStkHohoMemo.Tag = "CHANGE";
			this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
			this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTantoshaNm
			// 
			this.lblTantoshaNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTantoshaNm.Location = new System.Drawing.Point(182, 191);
			this.lblTantoshaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaNm.Name = "lblTantoshaNm";
			this.lblTantoshaNm.Size = new System.Drawing.Size(252, 24);
			this.lblTantoshaNm.TabIndex = 20;
			this.lblTantoshaNm.Tag = "DISPNAME";
			this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAddBar
			// 
			this.lblAddBar.AutoSize = true;
			this.lblAddBar.BackColor = System.Drawing.Color.Silver;
			this.lblAddBar.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblAddBar.Location = new System.Drawing.Point(179, 76);
			this.lblAddBar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblAddBar.Name = "lblAddBar";
			this.lblAddBar.Size = new System.Drawing.Size(16, 16);
			this.lblAddBar.TabIndex = 8;
			this.lblAddBar.Tag = "CHANGE";
			this.lblAddBar.Text = "-";
			// 
			// txtYubinBango2
			// 
			this.txtYubinBango2.AutoSizeFromLength = false;
			this.txtYubinBango2.BackColor = System.Drawing.Color.White;
			this.txtYubinBango2.DisplayLength = null;
			this.txtYubinBango2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtYubinBango2.Location = new System.Drawing.Point(195, 72);
			this.txtYubinBango2.Margin = new System.Windows.Forms.Padding(4);
			this.txtYubinBango2.MaxLength = 4;
			this.txtYubinBango2.Name = "txtYubinBango2";
			this.txtYubinBango2.Size = new System.Drawing.Size(60, 23);
			this.txtYubinBango2.TabIndex = 4;
			this.txtYubinBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango2_Validating);
			// 
			// txtDattaiRiyu
			// 
			this.txtDattaiRiyu.AutoSizeFromLength = false;
			this.txtDattaiRiyu.BackColor = System.Drawing.Color.White;
			this.txtDattaiRiyu.DisplayLength = null;
			this.txtDattaiRiyu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDattaiRiyu.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtDattaiRiyu.Location = new System.Drawing.Point(564, 249);
			this.txtDattaiRiyu.Margin = new System.Windows.Forms.Padding(4);
			this.txtDattaiRiyu.MaxLength = 40;
			this.txtDattaiRiyu.Name = "txtDattaiRiyu";
			this.txtDattaiRiyu.Size = new System.Drawing.Size(436, 23);
			this.txtDattaiRiyu.TabIndex = 30;
			this.txtDattaiRiyu.Validating += new System.ComponentModel.CancelEventHandler(this.txtDattaiRiyu_Validating);
			// 
			// txtShohizeiNyuryokuHoho
			// 
			this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
			this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.White;
			this.txtShohizeiNyuryokuHoho.DisplayLength = null;
			this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(126, 264);
			this.txtShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiNyuryokuHoho.MaxLength = 1;
			this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
			this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(19, 23);
			this.txtShohizeiNyuryokuHoho.TabIndex = 11;
			this.txtShohizeiNyuryokuHoho.Text = "2";
			this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiNyuryokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiNyuryokuHoho_Validating);
			// 
			// txtKingakuHasuShori
			// 
			this.txtKingakuHasuShori.AutoSizeFromLength = false;
			this.txtKingakuHasuShori.BackColor = System.Drawing.Color.White;
			this.txtKingakuHasuShori.DisplayLength = null;
			this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKingakuHasuShori.Location = new System.Drawing.Point(126, 240);
			this.txtKingakuHasuShori.Margin = new System.Windows.Forms.Padding(4);
			this.txtKingakuHasuShori.MaxLength = 1;
			this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
			this.txtKingakuHasuShori.Size = new System.Drawing.Size(19, 23);
			this.txtKingakuHasuShori.TabIndex = 10;
			this.txtKingakuHasuShori.Text = "2";
			this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
			// 
			// txtTankaShutokuHoho
			// 
			this.txtTankaShutokuHoho.AutoSizeFromLength = false;
			this.txtTankaShutokuHoho.BackColor = System.Drawing.Color.White;
			this.txtTankaShutokuHoho.DisplayLength = null;
			this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTankaShutokuHoho.Location = new System.Drawing.Point(126, 216);
			this.txtTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtTankaShutokuHoho.MaxLength = 1;
			this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
			this.txtTankaShutokuHoho.Size = new System.Drawing.Size(19, 23);
			this.txtTankaShutokuHoho.TabIndex = 9;
			this.txtTankaShutokuHoho.Text = "0";
			this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
			// 
			// txtTantoshaCd
			// 
			this.txtTantoshaCd.AutoSizeFromLength = false;
			this.txtTantoshaCd.BackColor = System.Drawing.Color.White;
			this.txtTantoshaCd.DisplayLength = null;
			this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTantoshaCd.Location = new System.Drawing.Point(126, 192);
			this.txtTantoshaCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtTantoshaCd.MaxLength = 4;
			this.txtTantoshaCd.Name = "txtTantoshaCd";
			this.txtTantoshaCd.Size = new System.Drawing.Size(52, 23);
			this.txtTantoshaCd.TabIndex = 8;
			this.txtTantoshaCd.Text = "0";
			this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
			// 
			// txtFax
			// 
			this.txtFax.AutoSizeFromLength = false;
			this.txtFax.BackColor = System.Drawing.Color.White;
			this.txtFax.DisplayLength = null;
			this.txtFax.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtFax.Location = new System.Drawing.Point(126, 168);
			this.txtFax.Margin = new System.Windows.Forms.Padding(4);
			this.txtFax.MaxLength = 15;
			this.txtFax.Name = "txtFax";
			this.txtFax.Size = new System.Drawing.Size(220, 23);
			this.txtFax.TabIndex = 10;
			this.txtFax.Validating += new System.ComponentModel.CancelEventHandler(this.txtFax_Validating);
			// 
			// txtTel
			// 
			this.txtTel.AutoSizeFromLength = false;
			this.txtTel.BackColor = System.Drawing.Color.White;
			this.txtTel.DisplayLength = null;
			this.txtTel.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTel.Location = new System.Drawing.Point(126, 144);
			this.txtTel.Margin = new System.Windows.Forms.Padding(4);
			this.txtTel.MaxLength = 15;
			this.txtTel.Name = "txtTel";
			this.txtTel.Size = new System.Drawing.Size(220, 23);
			this.txtTel.TabIndex = 7;
			this.txtTel.Validating += new System.ComponentModel.CancelEventHandler(this.txtTel_Validating);
			// 
			// txtJusho2
			// 
			this.txtJusho2.AutoSizeFromLength = false;
			this.txtJusho2.BackColor = System.Drawing.Color.White;
			this.txtJusho2.DisplayLength = null;
			this.txtJusho2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtJusho2.Location = new System.Drawing.Point(126, 120);
			this.txtJusho2.Margin = new System.Windows.Forms.Padding(4);
			this.txtJusho2.MaxLength = 30;
			this.txtJusho2.Name = "txtJusho2";
			this.txtJusho2.Size = new System.Drawing.Size(319, 23);
			this.txtJusho2.TabIndex = 6;
			this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho2_Validating);
			// 
			// txtJusho1
			// 
			this.txtJusho1.AutoSizeFromLength = false;
			this.txtJusho1.BackColor = System.Drawing.Color.White;
			this.txtJusho1.DisplayLength = null;
			this.txtJusho1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtJusho1.Location = new System.Drawing.Point(126, 96);
			this.txtJusho1.Margin = new System.Windows.Forms.Padding(4);
			this.txtJusho1.MaxLength = 30;
			this.txtJusho1.Name = "txtJusho1";
			this.txtJusho1.Size = new System.Drawing.Size(319, 23);
			this.txtJusho1.TabIndex = 5;
			this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho1_Validating);
			// 
			// txtYubinBango1
			// 
			this.txtYubinBango1.AutoSizeFromLength = false;
			this.txtYubinBango1.BackColor = System.Drawing.Color.White;
			this.txtYubinBango1.DisplayLength = null;
			this.txtYubinBango1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtYubinBango1.Location = new System.Drawing.Point(126, 72);
			this.txtYubinBango1.Margin = new System.Windows.Forms.Padding(4);
			this.txtYubinBango1.MaxLength = 3;
			this.txtYubinBango1.Name = "txtYubinBango1";
			this.txtYubinBango1.Size = new System.Drawing.Size(51, 23);
			this.txtYubinBango1.TabIndex = 3;
			this.txtYubinBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango1_Validating);
			// 
			// txtRyFunanushiNm
			// 
			this.txtRyFunanushiNm.AutoSizeFromLength = false;
			this.txtRyFunanushiNm.BackColor = System.Drawing.Color.White;
			this.txtRyFunanushiNm.DisplayLength = null;
			this.txtRyFunanushiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtRyFunanushiNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtRyFunanushiNm.Location = new System.Drawing.Point(126, 48);
			this.txtRyFunanushiNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtRyFunanushiNm.MaxLength = 20;
			this.txtRyFunanushiNm.Name = "txtRyFunanushiNm";
			this.txtRyFunanushiNm.Size = new System.Drawing.Size(319, 23);
			this.txtRyFunanushiNm.TabIndex = 2;
			this.txtRyFunanushiNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtRyFunanushiNm_Validating);
			// 
			// txtFunanushiKanaNm
			// 
			this.txtFunanushiKanaNm.AutoSizeFromLength = false;
			this.txtFunanushiKanaNm.BackColor = System.Drawing.Color.White;
			this.txtFunanushiKanaNm.DisplayLength = null;
			this.txtFunanushiKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtFunanushiKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtFunanushiKanaNm.Location = new System.Drawing.Point(126, 25);
			this.txtFunanushiKanaNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiKanaNm.MaxLength = 30;
			this.txtFunanushiKanaNm.Name = "txtFunanushiKanaNm";
			this.txtFunanushiKanaNm.Size = new System.Drawing.Size(319, 23);
			this.txtFunanushiKanaNm.TabIndex = 1;
			this.txtFunanushiKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiKanaNm_Validating);
			// 
			// txtFunanushiNm
			// 
			this.txtFunanushiNm.AutoSizeFromLength = false;
			this.txtFunanushiNm.BackColor = System.Drawing.Color.White;
			this.txtFunanushiNm.DisplayLength = null;
			this.txtFunanushiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtFunanushiNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtFunanushiNm.Location = new System.Drawing.Point(126, 2);
			this.txtFunanushiNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiNm.MaxLength = 40;
			this.txtFunanushiNm.Name = "txtFunanushiNm";
			this.txtFunanushiNm.Size = new System.Drawing.Size(319, 23);
			this.txtFunanushiNm.TabIndex = 0;
			this.txtFunanushiNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiNm_Validating);
			// 
			// lblDattaiRiyu
			// 
			this.lblDattaiRiyu.AutoSize = true;
			this.lblDattaiRiyu.BackColor = System.Drawing.Color.Silver;
			this.lblDattaiRiyu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDattaiRiyu.Location = new System.Drawing.Point(448, 254);
			this.lblDattaiRiyu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDattaiRiyu.Name = "lblDattaiRiyu";
			this.lblDattaiRiyu.Size = new System.Drawing.Size(72, 16);
			this.lblDattaiRiyu.TabIndex = 70;
			this.lblDattaiRiyu.Tag = "CHANGE";
			this.lblDattaiRiyu.Text = "脱退理由";
			this.lblDattaiRiyu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDattaiJp
			// 
			this.lblDattaiJp.AutoSize = true;
			this.lblDattaiJp.BackColor = System.Drawing.Color.Silver;
			this.lblDattaiJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDattaiJp.Location = new System.Drawing.Point(448, 228);
			this.lblDattaiJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDattaiJp.Name = "lblDattaiJp";
			this.lblDattaiJp.Size = new System.Drawing.Size(72, 16);
			this.lblDattaiJp.TabIndex = 64;
			this.lblDattaiJp.Tag = "CHANGE";
			this.lblDattaiJp.Text = "脱 退 日";
			this.lblDattaiJp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKanyuJp
			// 
			this.lblKanyuJp.AutoSize = true;
			this.lblKanyuJp.BackColor = System.Drawing.Color.Silver;
			this.lblKanyuJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKanyuJp.Location = new System.Drawing.Point(448, 202);
			this.lblKanyuJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanyuJp.Name = "lblKanyuJp";
			this.lblKanyuJp.Size = new System.Drawing.Size(72, 16);
			this.lblKanyuJp.TabIndex = 58;
			this.lblKanyuJp.Tag = "CHANGE";
			this.lblKanyuJp.Text = "加 入 日";
			this.lblKanyuJp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJp
			// 
			this.lblJp.AutoSize = true;
			this.lblJp.BackColor = System.Drawing.Color.Silver;
			this.lblJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblJp.Location = new System.Drawing.Point(448, 176);
			this.lblJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJp.Name = "lblJp";
			this.lblJp.Size = new System.Drawing.Size(72, 16);
			this.lblJp.TabIndex = 52;
			this.lblJp.Tag = "CHANGE";
			this.lblJp.Text = "生年月日";
			this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKaishuTsuki
			// 
			this.lblKaishuTsuki.AutoSize = true;
			this.lblKaishuTsuki.BackColor = System.Drawing.Color.Silver;
			this.lblKaishuTsuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKaishuTsuki.Location = new System.Drawing.Point(448, 150);
			this.lblKaishuTsuki.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKaishuTsuki.Name = "lblKaishuTsuki";
			this.lblKaishuTsuki.Size = new System.Drawing.Size(72, 16);
			this.lblKaishuTsuki.TabIndex = 48;
			this.lblKaishuTsuki.Tag = "CHANGE";
			this.lblKaishuTsuki.Text = "回 収 日";
			this.lblKaishuTsuki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiTenkaHoho
			// 
			this.lblShohizeiTenkaHoho.AutoSize = true;
			this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(448, 125);
			this.lblShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
			this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(120, 16);
			this.lblShohizeiTenkaHoho.TabIndex = 45;
			this.lblShohizeiTenkaHoho.Tag = "CHANGE";
			this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
			this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiHasuShori
			// 
			this.lblShohizeiHasuShori.AutoSize = true;
			this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShohizeiHasuShori.Location = new System.Drawing.Point(448, 102);
			this.lblShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
			this.lblShohizeiHasuShori.Size = new System.Drawing.Size(120, 16);
			this.lblShohizeiHasuShori.TabIndex = 42;
			this.lblShohizeiHasuShori.Tag = "CHANGE";
			this.lblShohizeiHasuShori.Text = "消費税端数処理";
			this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyushoKeishiki
			// 
			this.lblSeikyushoKeishiki.AutoSize = true;
			this.lblSeikyushoKeishiki.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSeikyushoKeishiki.Location = new System.Drawing.Point(448, 79);
			this.lblSeikyushoKeishiki.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoKeishiki.Name = "lblSeikyushoKeishiki";
			this.lblSeikyushoKeishiki.Size = new System.Drawing.Size(88, 16);
			this.lblSeikyushoKeishiki.TabIndex = 39;
			this.lblSeikyushoKeishiki.Tag = "CHANGE";
			this.lblSeikyushoKeishiki.Text = "請求書形式";
			this.lblSeikyushoKeishiki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyushoHakko
			// 
			this.lblSeikyushoHakko.AutoSize = true;
			this.lblSeikyushoHakko.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSeikyushoHakko.Location = new System.Drawing.Point(448, 53);
			this.lblSeikyushoHakko.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoHakko.Name = "lblSeikyushoHakko";
			this.lblSeikyushoHakko.Size = new System.Drawing.Size(88, 16);
			this.lblSeikyushoHakko.TabIndex = 36;
			this.lblSeikyushoHakko.Tag = "CHANGE";
			this.lblSeikyushoHakko.Text = "請求書発行";
			this.lblSeikyushoHakko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShimebi
			// 
			this.lblShimebi.AutoSize = true;
			this.lblShimebi.BackColor = System.Drawing.Color.Silver;
			this.lblShimebi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShimebi.Location = new System.Drawing.Point(448, 28);
			this.lblShimebi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShimebi.Name = "lblShimebi";
			this.lblShimebi.Size = new System.Drawing.Size(40, 16);
			this.lblShimebi.TabIndex = 33;
			this.lblShimebi.Tag = "CHANGE";
			this.lblShimebi.Text = "締日";
			this.lblShimebi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyusakiCd
			// 
			this.lblSeikyusakiCd.AutoSize = true;
			this.lblSeikyusakiCd.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSeikyusakiCd.Location = new System.Drawing.Point(448, 6);
			this.lblSeikyusakiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyusakiCd.Name = "lblSeikyusakiCd";
			this.lblSeikyusakiCd.Size = new System.Drawing.Size(104, 16);
			this.lblSeikyusakiCd.TabIndex = 3;
			this.lblSeikyusakiCd.Tag = "CHANGE";
			this.lblSeikyusakiCd.Text = "請求先コード";
			this.lblSeikyusakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiNyuryokuHoho
			// 
			this.lblShohizeiNyuryokuHoho.AutoSize = true;
			this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(4, 268);
			this.lblShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
			this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(120, 16);
			this.lblShohizeiNyuryokuHoho.TabIndex = 27;
			this.lblShohizeiNyuryokuHoho.Tag = "CHANGE";
			this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
			this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKingakuHasuShori
			// 
			this.lblKingakuHasuShori.AutoSize = true;
			this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKingakuHasuShori.Location = new System.Drawing.Point(4, 243);
			this.lblKingakuHasuShori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
			this.lblKingakuHasuShori.Size = new System.Drawing.Size(104, 16);
			this.lblKingakuHasuShori.TabIndex = 24;
			this.lblKingakuHasuShori.Tag = "CHANGE";
			this.lblKingakuHasuShori.Text = "金額端数処理";
			this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTankaShutokuHoho
			// 
			this.lblTankaShutokuHoho.AutoSize = true;
			this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTankaShutokuHoho.Location = new System.Drawing.Point(4, 219);
			this.lblTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
			this.lblTankaShutokuHoho.Size = new System.Drawing.Size(104, 16);
			this.lblTankaShutokuHoho.TabIndex = 21;
			this.lblTankaShutokuHoho.Tag = "CHANGE";
			this.lblTankaShutokuHoho.Text = "単価取得方法";
			this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTantoshaCd
			// 
			this.lblTantoshaCd.AutoSize = true;
			this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
			this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTantoshaCd.Location = new System.Drawing.Point(4, 194);
			this.lblTantoshaCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaCd.Name = "lblTantoshaCd";
			this.lblTantoshaCd.Size = new System.Drawing.Size(104, 16);
			this.lblTantoshaCd.TabIndex = 18;
			this.lblTantoshaCd.Tag = "CHANGE";
			this.lblTantoshaCd.Text = "担当者コード";
			this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFax
			// 
			this.lblFax.AutoSize = true;
			this.lblFax.BackColor = System.Drawing.Color.Silver;
			this.lblFax.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFax.Location = new System.Drawing.Point(4, 171);
			this.lblFax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFax.Name = "lblFax";
			this.lblFax.Size = new System.Drawing.Size(88, 16);
			this.lblFax.TabIndex = 16;
			this.lblFax.Tag = "CHANGE";
			this.lblFax.Text = "ＦＡＸ番号";
			this.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTel
			// 
			this.lblTel.AutoSize = true;
			this.lblTel.BackColor = System.Drawing.Color.Silver;
			this.lblTel.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTel.Location = new System.Drawing.Point(4, 150);
			this.lblTel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTel.Name = "lblTel";
			this.lblTel.Size = new System.Drawing.Size(72, 16);
			this.lblTel.TabIndex = 14;
			this.lblTel.Tag = "CHANGE";
			this.lblTel.Text = "電話番号";
			this.lblTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJusho2
			// 
			this.lblJusho2.AutoSize = true;
			this.lblJusho2.BackColor = System.Drawing.Color.Silver;
			this.lblJusho2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblJusho2.Location = new System.Drawing.Point(4, 124);
			this.lblJusho2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJusho2.Name = "lblJusho2";
			this.lblJusho2.Size = new System.Drawing.Size(56, 16);
			this.lblJusho2.TabIndex = 12;
			this.lblJusho2.Tag = "CHANGE";
			this.lblJusho2.Text = "住所２";
			this.lblJusho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJusho1
			// 
			this.lblJusho1.AutoSize = true;
			this.lblJusho1.BackColor = System.Drawing.Color.Silver;
			this.lblJusho1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblJusho1.Location = new System.Drawing.Point(4, 101);
			this.lblJusho1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJusho1.Name = "lblJusho1";
			this.lblJusho1.Size = new System.Drawing.Size(56, 16);
			this.lblJusho1.TabIndex = 10;
			this.lblJusho1.Tag = "CHANGE";
			this.lblJusho1.Text = "住所１";
			this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYubinBango
			// 
			this.lblYubinBango.AutoSize = true;
			this.lblYubinBango.BackColor = System.Drawing.Color.Silver;
			this.lblYubinBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblYubinBango.Location = new System.Drawing.Point(4, 79);
			this.lblYubinBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYubinBango.Name = "lblYubinBango";
			this.lblYubinBango.Size = new System.Drawing.Size(72, 16);
			this.lblYubinBango.TabIndex = 6;
			this.lblYubinBango.Tag = "CHANGE";
			this.lblYubinBango.Text = "郵便番号";
			this.lblYubinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblRyFunanushiNm
			// 
			this.lblRyFunanushiNm.BackColor = System.Drawing.Color.Silver;
			this.lblRyFunanushiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblRyFunanushiNm.Location = new System.Drawing.Point(4, 46);
			this.lblRyFunanushiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblRyFunanushiNm.Name = "lblRyFunanushiNm";
			this.lblRyFunanushiNm.Size = new System.Drawing.Size(102, 29);
			this.lblRyFunanushiNm.TabIndex = 4;
			this.lblRyFunanushiNm.Tag = "CHANGE";
			this.lblRyFunanushiNm.Text = "略称船主名";
			this.lblRyFunanushiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiKanaNm
			// 
			this.lblFunanushiKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushiKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFunanushiKanaNm.Location = new System.Drawing.Point(4, 24);
			this.lblFunanushiKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiKanaNm.Name = "lblFunanushiKanaNm";
			this.lblFunanushiKanaNm.Size = new System.Drawing.Size(88, 26);
			this.lblFunanushiKanaNm.TabIndex = 2;
			this.lblFunanushiKanaNm.Tag = "CHANGE";
			this.lblFunanushiKanaNm.Text = "船主カナ名";
			this.lblFunanushiKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiNm
			// 
			this.lblFunanushiNm.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFunanushiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFunanushiNm.Location = new System.Drawing.Point(0, 0);
			this.lblFunanushiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiNm.Name = "lblFunanushiNm";
			this.lblFunanushiNm.Size = new System.Drawing.Size(1092, 319);
			this.lblFunanushiNm.TabIndex = 4;
			this.lblFunanushiNm.Tag = "CHANGE";
			// 
			// txtFutsuKozaKubun
			// 
			this.txtFutsuKozaKubun.AutoSizeFromLength = false;
			this.txtFutsuKozaKubun.BackColor = System.Drawing.Color.White;
			this.txtFutsuKozaKubun.DisplayLength = null;
			this.txtFutsuKozaKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtFutsuKozaKubun.Location = new System.Drawing.Point(409, 5);
			this.txtFutsuKozaKubun.Margin = new System.Windows.Forms.Padding(4);
			this.txtFutsuKozaKubun.MaxLength = 1;
			this.txtFutsuKozaKubun.Name = "txtFutsuKozaKubun";
			this.txtFutsuKozaKubun.Size = new System.Drawing.Size(19, 23);
			this.txtFutsuKozaKubun.TabIndex = 47;
			this.txtFutsuKozaKubun.Text = "0";
			this.txtFutsuKozaKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtFutsuKozaKubun.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFutsuKozaKubun_KeyDown);
			this.txtFutsuKozaKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtFutsuKozaKubun_Validating);
			// 
			// txtFutsuKozaBango
			// 
			this.txtFutsuKozaBango.AutoSizeFromLength = false;
			this.txtFutsuKozaBango.BackColor = System.Drawing.Color.White;
			this.txtFutsuKozaBango.DisplayLength = null;
			this.txtFutsuKozaBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtFutsuKozaBango.Location = new System.Drawing.Point(153, 3);
			this.txtFutsuKozaBango.Margin = new System.Windows.Forms.Padding(4);
			this.txtFutsuKozaBango.MaxLength = 7;
			this.txtFutsuKozaBango.Name = "txtFutsuKozaBango";
			this.txtFutsuKozaBango.Size = new System.Drawing.Size(72, 23);
			this.txtFutsuKozaBango.TabIndex = 46;
			this.txtFutsuKozaBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtFutsuKozaBango_Validating);
			// 
			// lblFutsuKozaBango
			// 
			this.lblFutsuKozaBango.BackColor = System.Drawing.Color.Silver;
			this.lblFutsuKozaBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFutsuKozaBango.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFutsuKozaBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFutsuKozaBango.Location = new System.Drawing.Point(0, 0);
			this.lblFutsuKozaBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFutsuKozaBango.Name = "lblFutsuKozaBango";
			this.lblFutsuKozaBango.Size = new System.Drawing.Size(544, 31);
			this.lblFutsuKozaBango.TabIndex = 0;
			this.lblFutsuKozaBango.Tag = "CHANGE";
			this.lblFutsuKozaBango.Text = "普通口座　口座番号";
			this.lblFutsuKozaBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFutsuKozaKubunSelect
			// 
			this.lblFutsuKozaKubunSelect.AutoSize = true;
			this.lblFutsuKozaKubunSelect.BackColor = System.Drawing.Color.Silver;
			this.lblFutsuKozaKubunSelect.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFutsuKozaKubunSelect.Location = new System.Drawing.Point(431, 8);
			this.lblFutsuKozaKubunSelect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFutsuKozaKubunSelect.Name = "lblFutsuKozaKubunSelect";
			this.lblFutsuKozaKubunSelect.Size = new System.Drawing.Size(112, 16);
			this.lblFutsuKozaKubunSelect.TabIndex = 4;
			this.lblFutsuKozaKubunSelect.Tag = "CHANGE";
			this.lblFutsuKozaKubunSelect.Text = "1:普通 2:当座";
			this.lblFutsuKozaKubunSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFutsuKozaKubun
			// 
			this.lblFutsuKozaKubun.AutoSize = true;
			this.lblFutsuKozaKubun.BackColor = System.Drawing.Color.Silver;
			this.lblFutsuKozaKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFutsuKozaKubun.Location = new System.Drawing.Point(337, 8);
			this.lblFutsuKozaKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFutsuKozaKubun.Name = "lblFutsuKozaKubun";
			this.lblFutsuKozaKubun.Size = new System.Drawing.Size(72, 16);
			this.lblFutsuKozaKubun.TabIndex = 2;
			this.lblFutsuKozaKubun.Tag = "CHANGE";
			this.lblFutsuKozaKubun.Text = "口座区分";
			this.lblFutsuKozaKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTsumitateKozaKubun
			// 
			this.txtTsumitateKozaKubun.AutoSizeFromLength = false;
			this.txtTsumitateKozaKubun.BackColor = System.Drawing.Color.White;
			this.txtTsumitateKozaKubun.DisplayLength = null;
			this.txtTsumitateKozaKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTsumitateKozaKubun.Location = new System.Drawing.Point(409, 3);
			this.txtTsumitateKozaKubun.Margin = new System.Windows.Forms.Padding(4);
			this.txtTsumitateKozaKubun.MaxLength = 1;
			this.txtTsumitateKozaKubun.Name = "txtTsumitateKozaKubun";
			this.txtTsumitateKozaKubun.Size = new System.Drawing.Size(19, 23);
			this.txtTsumitateKozaKubun.TabIndex = 50;
			this.txtTsumitateKozaKubun.Text = "0";
			this.txtTsumitateKozaKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtTsumitateKozaKubun.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTsumitateKozaKubun_KeyDown);
			this.txtTsumitateKozaKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateKozaKubun_Validating);
			// 
			// txtTsumitateRitsu
			// 
			this.txtTsumitateRitsu.AutoSizeFromLength = false;
			this.txtTsumitateRitsu.BackColor = System.Drawing.Color.White;
			this.txtTsumitateRitsu.DisplayLength = null;
			this.txtTsumitateRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTsumitateRitsu.Location = new System.Drawing.Point(281, 4);
			this.txtTsumitateRitsu.Margin = new System.Windows.Forms.Padding(4);
			this.txtTsumitateRitsu.MaxLength = 5;
			this.txtTsumitateRitsu.Name = "txtTsumitateRitsu";
			this.txtTsumitateRitsu.Size = new System.Drawing.Size(55, 23);
			this.txtTsumitateRitsu.TabIndex = 49;
			this.txtTsumitateRitsu.Text = "100.0";
			this.txtTsumitateRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTsumitateRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateRitsu_Validating);
			// 
			// txtTsumitateKozaBango
			// 
			this.txtTsumitateKozaBango.AutoSizeFromLength = false;
			this.txtTsumitateKozaBango.BackColor = System.Drawing.Color.White;
			this.txtTsumitateKozaBango.DisplayLength = null;
			this.txtTsumitateKozaBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTsumitateKozaBango.Location = new System.Drawing.Point(153, 4);
			this.txtTsumitateKozaBango.Margin = new System.Windows.Forms.Padding(4);
			this.txtTsumitateKozaBango.MaxLength = 7;
			this.txtTsumitateKozaBango.Name = "txtTsumitateKozaBango";
			this.txtTsumitateKozaBango.Size = new System.Drawing.Size(72, 23);
			this.txtTsumitateKozaBango.TabIndex = 48;
			this.txtTsumitateKozaBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateKozaBango_Validating);
			// 
			// lblTsumitateRitsu
			// 
			this.lblTsumitateRitsu.BackColor = System.Drawing.Color.Silver;
			this.lblTsumitateRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTsumitateRitsu.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTsumitateRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTsumitateRitsu.Location = new System.Drawing.Point(0, 0);
			this.lblTsumitateRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTsumitateRitsu.Name = "lblTsumitateRitsu";
			this.lblTsumitateRitsu.Size = new System.Drawing.Size(544, 31);
			this.lblTsumitateRitsu.TabIndex = 0;
			this.lblTsumitateRitsu.Tag = "CHANGE";
			this.lblTsumitateRitsu.Text = "積立口座";
			this.lblTsumitateRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTsumitateKozaBango
			// 
			this.lblTsumitateKozaBango.AutoSize = true;
			this.lblTsumitateKozaBango.BackColor = System.Drawing.Color.Silver;
			this.lblTsumitateKozaBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTsumitateKozaBango.Location = new System.Drawing.Point(79, 10);
			this.lblTsumitateKozaBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTsumitateKozaBango.Name = "lblTsumitateKozaBango";
			this.lblTsumitateKozaBango.Size = new System.Drawing.Size(72, 16);
			this.lblTsumitateKozaBango.TabIndex = 2;
			this.lblTsumitateKozaBango.Tag = "CHANGE";
			this.lblTsumitateKozaBango.Text = "口座番号";
			this.lblTsumitateKozaBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTsumitateKozaKubunMemo
			// 
			this.lblTsumitateKozaKubunMemo.AutoSize = true;
			this.lblTsumitateKozaKubunMemo.BackColor = System.Drawing.Color.Silver;
			this.lblTsumitateKozaKubunMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTsumitateKozaKubunMemo.Location = new System.Drawing.Point(430, 7);
			this.lblTsumitateKozaKubunMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTsumitateKozaKubunMemo.Name = "lblTsumitateKozaKubunMemo";
			this.lblTsumitateKozaKubunMemo.Size = new System.Drawing.Size(112, 16);
			this.lblTsumitateKozaKubunMemo.TabIndex = 6;
			this.lblTsumitateKozaKubunMemo.Tag = "CHANGE";
			this.lblTsumitateKozaKubunMemo.Text = "1:普通 2:当座";
			this.lblTsumitateKozaKubunMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTsumitateKozaKubun
			// 
			this.lblTsumitateKozaKubun.AutoSize = true;
			this.lblTsumitateKozaKubun.BackColor = System.Drawing.Color.Silver;
			this.lblTsumitateKozaKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTsumitateKozaKubun.Location = new System.Drawing.Point(337, 7);
			this.lblTsumitateKozaKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTsumitateKozaKubun.Name = "lblTsumitateKozaKubun";
			this.lblTsumitateKozaKubun.Size = new System.Drawing.Size(72, 16);
			this.lblTsumitateKozaKubun.TabIndex = 4;
			this.lblTsumitateKozaKubun.Tag = "CHANGE";
			this.lblTsumitateKozaKubun.Text = "口座区分";
			this.lblTsumitateKozaKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKozaKubunAzukari
			// 
			this.txtKozaKubunAzukari.AutoSizeFromLength = false;
			this.txtKozaKubunAzukari.BackColor = System.Drawing.Color.White;
			this.txtKozaKubunAzukari.DisplayLength = null;
			this.txtKozaKubunAzukari.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKozaKubunAzukari.Location = new System.Drawing.Point(409, 24);
			this.txtKozaKubunAzukari.Margin = new System.Windows.Forms.Padding(4);
			this.txtKozaKubunAzukari.MaxLength = 1;
			this.txtKozaKubunAzukari.Name = "txtKozaKubunAzukari";
			this.txtKozaKubunAzukari.Size = new System.Drawing.Size(19, 23);
			this.txtKozaKubunAzukari.TabIndex = 53;
			this.txtKozaKubunAzukari.Text = "0";
			this.txtKozaKubunAzukari.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtKozaKubunAzukari.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKozaKubunAzukari_KeyDown);
			this.txtKozaKubunAzukari.Validating += new System.ComponentModel.CancelEventHandler(this.txtKozaKubunAzukari_Validating);
			// 
			// txtTsumitateRitsuAzukari
			// 
			this.txtTsumitateRitsuAzukari.AutoSizeFromLength = false;
			this.txtTsumitateRitsuAzukari.BackColor = System.Drawing.Color.White;
			this.txtTsumitateRitsuAzukari.DisplayLength = null;
			this.txtTsumitateRitsuAzukari.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTsumitateRitsuAzukari.Location = new System.Drawing.Point(281, 24);
			this.txtTsumitateRitsuAzukari.Margin = new System.Windows.Forms.Padding(4);
			this.txtTsumitateRitsuAzukari.MaxLength = 5;
			this.txtTsumitateRitsuAzukari.Name = "txtTsumitateRitsuAzukari";
			this.txtTsumitateRitsuAzukari.Size = new System.Drawing.Size(55, 23);
			this.txtTsumitateRitsuAzukari.TabIndex = 52;
			this.txtTsumitateRitsuAzukari.Text = "0.00";
			this.txtTsumitateRitsuAzukari.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTsumitateRitsuAzukari.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateRitsuAzukari_Validating);
			// 
			// txtKozaBangoAzukari
			// 
			this.txtKozaBangoAzukari.AutoSizeFromLength = false;
			this.txtKozaBangoAzukari.BackColor = System.Drawing.Color.White;
			this.txtKozaBangoAzukari.DisplayLength = null;
			this.txtKozaBangoAzukari.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKozaBangoAzukari.Location = new System.Drawing.Point(153, 24);
			this.txtKozaBangoAzukari.Margin = new System.Windows.Forms.Padding(4);
			this.txtKozaBangoAzukari.MaxLength = 7;
			this.txtKozaBangoAzukari.Name = "txtKozaBangoAzukari";
			this.txtKozaBangoAzukari.Size = new System.Drawing.Size(72, 23);
			this.txtKozaBangoAzukari.TabIndex = 51;
			this.txtKozaBangoAzukari.Validating += new System.ComponentModel.CancelEventHandler(this.txtKozaBangoAzukari_Validating);
			// 
			// lblTsumitateRitsuAzukari
			// 
			this.lblTsumitateRitsuAzukari.AutoSize = true;
			this.lblTsumitateRitsuAzukari.BackColor = System.Drawing.Color.Silver;
			this.lblTsumitateRitsuAzukari.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTsumitateRitsuAzukari.Location = new System.Drawing.Point(226, 28);
			this.lblTsumitateRitsuAzukari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTsumitateRitsuAzukari.Name = "lblTsumitateRitsuAzukari";
			this.lblTsumitateRitsuAzukari.Size = new System.Drawing.Size(56, 16);
			this.lblTsumitateRitsuAzukari.TabIndex = 0;
			this.lblTsumitateRitsuAzukari.Tag = "CHANGE";
			this.lblTsumitateRitsuAzukari.Text = "積立率";
			this.lblTsumitateRitsuAzukari.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKozaBangoAzukari
			// 
			this.lblKozaBangoAzukari.AutoSize = true;
			this.lblKozaBangoAzukari.BackColor = System.Drawing.Color.Silver;
			this.lblKozaBangoAzukari.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKozaBangoAzukari.Location = new System.Drawing.Point(78, 29);
			this.lblKozaBangoAzukari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKozaBangoAzukari.Name = "lblKozaBangoAzukari";
			this.lblKozaBangoAzukari.Size = new System.Drawing.Size(72, 16);
			this.lblKozaBangoAzukari.TabIndex = 2;
			this.lblKozaBangoAzukari.Tag = "CHANGE";
			this.lblKozaBangoAzukari.Text = "口座番号";
			this.lblKozaBangoAzukari.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKozaKubunAzukari
			// 
			this.lblKozaKubunAzukari.AutoSize = true;
			this.lblKozaKubunAzukari.BackColor = System.Drawing.Color.Silver;
			this.lblKozaKubunAzukari.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKozaKubunAzukari.Location = new System.Drawing.Point(335, 28);
			this.lblKozaKubunAzukari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKozaKubunAzukari.Name = "lblKozaKubunAzukari";
			this.lblKozaKubunAzukari.Size = new System.Drawing.Size(72, 16);
			this.lblKozaKubunAzukari.TabIndex = 4;
			this.lblKozaKubunAzukari.Tag = "CHANGE";
			this.lblKozaKubunAzukari.Text = "口座区分";
			this.lblKozaKubunAzukari.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKozaKubunAzukariSelect
			// 
			this.lblKozaKubunAzukariSelect.AutoSize = true;
			this.lblKozaKubunAzukariSelect.BackColor = System.Drawing.Color.Silver;
			this.lblKozaKubunAzukariSelect.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKozaKubunAzukariSelect.Location = new System.Drawing.Point(430, 28);
			this.lblKozaKubunAzukariSelect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKozaKubunAzukariSelect.Name = "lblKozaKubunAzukariSelect";
			this.lblKozaKubunAzukariSelect.Size = new System.Drawing.Size(112, 16);
			this.lblKozaKubunAzukariSelect.TabIndex = 6;
			this.lblKozaKubunAzukariSelect.Tag = "CHANGE";
			this.lblKozaKubunAzukariSelect.Text = "1:普通 2:当座";
			this.lblKozaKubunAzukariSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKozaKubunHonnin
			// 
			this.txtKozaKubunHonnin.AutoSizeFromLength = false;
			this.txtKozaKubunHonnin.BackColor = System.Drawing.Color.White;
			this.txtKozaKubunHonnin.DisplayLength = null;
			this.txtKozaKubunHonnin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKozaKubunHonnin.Location = new System.Drawing.Point(409, 5);
			this.txtKozaKubunHonnin.Margin = new System.Windows.Forms.Padding(4);
			this.txtKozaKubunHonnin.MaxLength = 1;
			this.txtKozaKubunHonnin.Name = "txtKozaKubunHonnin";
			this.txtKozaKubunHonnin.Size = new System.Drawing.Size(19, 23);
			this.txtKozaKubunHonnin.TabIndex = 56;
			this.txtKozaKubunHonnin.Text = "0";
			this.txtKozaKubunHonnin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtKozaKubunHonnin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKozaKubunHonnin_KeyDown);
			this.txtKozaKubunHonnin.Validating += new System.ComponentModel.CancelEventHandler(this.txtKozaKubunHonnin_Validating);
			// 
			// txtTsumitateRitsuHonnin
			// 
			this.txtTsumitateRitsuHonnin.AutoSizeFromLength = false;
			this.txtTsumitateRitsuHonnin.BackColor = System.Drawing.Color.White;
			this.txtTsumitateRitsuHonnin.DisplayLength = null;
			this.txtTsumitateRitsuHonnin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTsumitateRitsuHonnin.Location = new System.Drawing.Point(281, 6);
			this.txtTsumitateRitsuHonnin.Margin = new System.Windows.Forms.Padding(4);
			this.txtTsumitateRitsuHonnin.MaxLength = 5;
			this.txtTsumitateRitsuHonnin.Name = "txtTsumitateRitsuHonnin";
			this.txtTsumitateRitsuHonnin.Size = new System.Drawing.Size(55, 23);
			this.txtTsumitateRitsuHonnin.TabIndex = 55;
			this.txtTsumitateRitsuHonnin.Text = "0.00";
			this.txtTsumitateRitsuHonnin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTsumitateRitsuHonnin.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateRitsuHonnin_Validating);
			// 
			// txtKozaBangoHonnin
			// 
			this.txtKozaBangoHonnin.AutoSizeFromLength = false;
			this.txtKozaBangoHonnin.BackColor = System.Drawing.Color.White;
			this.txtKozaBangoHonnin.DisplayLength = null;
			this.txtKozaBangoHonnin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKozaBangoHonnin.Location = new System.Drawing.Point(154, 5);
			this.txtKozaBangoHonnin.Margin = new System.Windows.Forms.Padding(4);
			this.txtKozaBangoHonnin.MaxLength = 7;
			this.txtKozaBangoHonnin.Name = "txtKozaBangoHonnin";
			this.txtKozaBangoHonnin.Size = new System.Drawing.Size(72, 23);
			this.txtKozaBangoHonnin.TabIndex = 54;
			this.txtKozaBangoHonnin.Validating += new System.ComponentModel.CancelEventHandler(this.txtKozaBangoHonnin_Validating);
			// 
			// lblTsumitateRitsuHonnin
			// 
			this.lblTsumitateRitsuHonnin.AutoSize = true;
			this.lblTsumitateRitsuHonnin.BackColor = System.Drawing.Color.Silver;
			this.lblTsumitateRitsuHonnin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTsumitateRitsuHonnin.Location = new System.Drawing.Point(226, 9);
			this.lblTsumitateRitsuHonnin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTsumitateRitsuHonnin.Name = "lblTsumitateRitsuHonnin";
			this.lblTsumitateRitsuHonnin.Size = new System.Drawing.Size(56, 16);
			this.lblTsumitateRitsuHonnin.TabIndex = 0;
			this.lblTsumitateRitsuHonnin.Tag = "CHANGE";
			this.lblTsumitateRitsuHonnin.Text = "積立率";
			this.lblTsumitateRitsuHonnin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKozaBangoHonnin
			// 
			this.lblKozaBangoHonnin.AutoSize = true;
			this.lblKozaBangoHonnin.BackColor = System.Drawing.Color.Silver;
			this.lblKozaBangoHonnin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKozaBangoHonnin.Location = new System.Drawing.Point(78, 9);
			this.lblKozaBangoHonnin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKozaBangoHonnin.Name = "lblKozaBangoHonnin";
			this.lblKozaBangoHonnin.Size = new System.Drawing.Size(72, 16);
			this.lblKozaBangoHonnin.TabIndex = 2;
			this.lblKozaBangoHonnin.Tag = "CHANGE";
			this.lblKozaBangoHonnin.Text = "口座番号";
			this.lblKozaBangoHonnin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKozaKubunHonnin
			// 
			this.lblKozaKubunHonnin.AutoSize = true;
			this.lblKozaKubunHonnin.BackColor = System.Drawing.Color.Silver;
			this.lblKozaKubunHonnin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKozaKubunHonnin.Location = new System.Drawing.Point(335, 9);
			this.lblKozaKubunHonnin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKozaKubunHonnin.Name = "lblKozaKubunHonnin";
			this.lblKozaKubunHonnin.Size = new System.Drawing.Size(72, 16);
			this.lblKozaKubunHonnin.TabIndex = 4;
			this.lblKozaKubunHonnin.Tag = "CHANGE";
			this.lblKozaKubunHonnin.Text = "口座区分";
			this.lblKozaKubunHonnin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKozaKubunHonninSelect
			// 
			this.lblKozaKubunHonninSelect.AutoSize = true;
			this.lblKozaKubunHonninSelect.BackColor = System.Drawing.Color.Silver;
			this.lblKozaKubunHonninSelect.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKozaKubunHonninSelect.Location = new System.Drawing.Point(430, 10);
			this.lblKozaKubunHonninSelect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKozaKubunHonninSelect.Name = "lblKozaKubunHonninSelect";
			this.lblKozaKubunHonninSelect.Size = new System.Drawing.Size(112, 16);
			this.lblKozaKubunHonninSelect.TabIndex = 6;
			this.lblKozaKubunHonninSelect.Tag = "CHANGE";
			this.lblKozaKubunHonninSelect.Text = "1:普通 2:当座";
			this.lblKozaKubunHonninSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtPayaoRitsu
			// 
			this.txtPayaoRitsu.AutoSizeFromLength = false;
			this.txtPayaoRitsu.BackColor = System.Drawing.Color.White;
			this.txtPayaoRitsu.DisplayLength = null;
			this.txtPayaoRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtPayaoRitsu.Location = new System.Drawing.Point(439, 2);
			this.txtPayaoRitsu.Margin = new System.Windows.Forms.Padding(4);
			this.txtPayaoRitsu.MaxLength = 3;
			this.txtPayaoRitsu.Name = "txtPayaoRitsu";
			this.txtPayaoRitsu.Size = new System.Drawing.Size(72, 23);
			this.txtPayaoRitsu.TabIndex = 25;
			this.txtPayaoRitsu.Text = "0";
			this.txtPayaoRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtPayaoRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtPayaoRitsu_Validating);
			// 
			// txtSonotaRitsu
			// 
			this.txtSonotaRitsu.AutoSizeFromLength = false;
			this.txtSonotaRitsu.BackColor = System.Drawing.Color.White;
			this.txtSonotaRitsu.DisplayLength = null;
			this.txtSonotaRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtSonotaRitsu.Location = new System.Drawing.Point(300, 2);
			this.txtSonotaRitsu.Margin = new System.Windows.Forms.Padding(4);
			this.txtSonotaRitsu.MaxLength = 3;
			this.txtSonotaRitsu.Name = "txtSonotaRitsu";
			this.txtSonotaRitsu.Size = new System.Drawing.Size(72, 23);
			this.txtSonotaRitsu.TabIndex = 24;
			this.txtSonotaRitsu.Text = "0";
			this.txtSonotaRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSonotaRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtSonotaRitsu_Validating);
			// 
			// txtFutsuRitsu
			// 
			this.txtFutsuRitsu.AutoSizeFromLength = false;
			this.txtFutsuRitsu.BackColor = System.Drawing.Color.White;
			this.txtFutsuRitsu.DisplayLength = null;
			this.txtFutsuRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtFutsuRitsu.Location = new System.Drawing.Point(160, 2);
			this.txtFutsuRitsu.Margin = new System.Windows.Forms.Padding(4);
			this.txtFutsuRitsu.MaxLength = 3;
			this.txtFutsuRitsu.Name = "txtFutsuRitsu";
			this.txtFutsuRitsu.Size = new System.Drawing.Size(72, 23);
			this.txtFutsuRitsu.TabIndex = 23;
			this.txtFutsuRitsu.Text = "0";
			this.txtFutsuRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFutsuRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtFutsuRitsu_Validating);
			// 
			// lblFurikomisakiWariai
			// 
			this.lblFurikomisakiWariai.BackColor = System.Drawing.Color.Silver;
			this.lblFurikomisakiWariai.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFurikomisakiWariai.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFurikomisakiWariai.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFurikomisakiWariai.Location = new System.Drawing.Point(0, 0);
			this.lblFurikomisakiWariai.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFurikomisakiWariai.Name = "lblFurikomisakiWariai";
			this.lblFurikomisakiWariai.Size = new System.Drawing.Size(539, 33);
			this.lblFurikomisakiWariai.TabIndex = 20;
			this.lblFurikomisakiWariai.Tag = "CHANGE";
			this.lblFurikomisakiWariai.Text = "振込先割合";
			this.lblFurikomisakiWariai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblPayaoRitsu
			// 
			this.lblPayaoRitsu.AutoSize = true;
			this.lblPayaoRitsu.BackColor = System.Drawing.Color.White;
			this.lblPayaoRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblPayaoRitsu.Location = new System.Drawing.Point(377, 6);
			this.lblPayaoRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblPayaoRitsu.Name = "lblPayaoRitsu";
			this.lblPayaoRitsu.Size = new System.Drawing.Size(56, 16);
			this.lblPayaoRitsu.TabIndex = 25;
			this.lblPayaoRitsu.Tag = "CHANGE";
			this.lblPayaoRitsu.Text = "パヤオ";
			this.lblPayaoRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblSonotaRitsu
			// 
			this.lblSonotaRitsu.AutoSize = true;
			this.lblSonotaRitsu.BackColor = System.Drawing.Color.White;
			this.lblSonotaRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSonotaRitsu.Location = new System.Drawing.Point(236, 6);
			this.lblSonotaRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSonotaRitsu.Name = "lblSonotaRitsu";
			this.lblSonotaRitsu.Size = new System.Drawing.Size(56, 16);
			this.lblSonotaRitsu.TabIndex = 23;
			this.lblSonotaRitsu.Tag = "CHANGE";
			this.lblSonotaRitsu.Text = "その他";
			this.lblSonotaRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblFutsuRitsu
			// 
			this.lblFutsuRitsu.AutoSize = true;
			this.lblFutsuRitsu.BackColor = System.Drawing.Color.White;
			this.lblFutsuRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFutsuRitsu.Location = new System.Drawing.Point(117, 6);
			this.lblFutsuRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFutsuRitsu.Name = "lblFutsuRitsu";
			this.lblFutsuRitsu.Size = new System.Drawing.Size(40, 16);
			this.lblFutsuRitsu.TabIndex = 21;
			this.lblFutsuRitsu.Tag = "CHANGE";
			this.lblFutsuRitsu.Text = "普通";
			this.lblFutsuRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblShishoKubunMemo
			// 
			this.lblShishoKubunMemo.AutoSize = true;
			this.lblShishoKubunMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShishoKubunMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShishoKubunMemo.Location = new System.Drawing.Point(141, 5);
			this.lblShishoKubunMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShishoKubunMemo.Name = "lblShishoKubunMemo";
			this.lblShishoKubunMemo.Size = new System.Drawing.Size(112, 16);
			this.lblShishoKubunMemo.TabIndex = 16;
			this.lblShishoKubunMemo.Tag = "CHANGE";
			this.lblShishoKubunMemo.Text = "1:本社 2:支社";
			this.lblShishoKubunMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeijunKubunMemo
			// 
			this.lblSeijunKubunMemo.AutoSize = true;
			this.lblSeijunKubunMemo.BackColor = System.Drawing.Color.Silver;
			this.lblSeijunKubunMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSeijunKubunMemo.Location = new System.Drawing.Point(141, 5);
			this.lblSeijunKubunMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeijunKubunMemo.Name = "lblSeijunKubunMemo";
			this.lblSeijunKubunMemo.Size = new System.Drawing.Size(296, 16);
			this.lblSeijunKubunMemo.TabIndex = 19;
			this.lblSeijunKubunMemo.Tag = "CHANGE";
			this.lblSeijunKubunMemo.Text = "1:正組合員　2:準組合員　3:組合員以外";
			this.lblSeijunKubunMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeijunKubun
			// 
			this.txtSeijunKubun.AutoSizeFromLength = false;
			this.txtSeijunKubun.BackColor = System.Drawing.Color.White;
			this.txtSeijunKubun.DisplayLength = null;
			this.txtSeijunKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtSeijunKubun.Location = new System.Drawing.Point(119, 1);
			this.txtSeijunKubun.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeijunKubun.MaxLength = 1;
			this.txtSeijunKubun.Name = "txtSeijunKubun";
			this.txtSeijunKubun.Size = new System.Drawing.Size(19, 23);
			this.txtSeijunKubun.TabIndex = 22;
			this.txtSeijunKubun.Text = "0";
			this.txtSeijunKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtSeijunKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeijunKubun_Validating);
			// 
			// lblSeijunKubun
			// 
			this.lblSeijunKubun.BackColor = System.Drawing.Color.Silver;
			this.lblSeijunKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeijunKubun.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblSeijunKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSeijunKubun.Location = new System.Drawing.Point(0, 0);
			this.lblSeijunKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeijunKubun.Name = "lblSeijunKubun";
			this.lblSeijunKubun.Size = new System.Drawing.Size(539, 26);
			this.lblSeijunKubun.TabIndex = 17;
			this.lblSeijunKubun.Tag = "CHANGE";
			this.lblSeijunKubun.Text = "正準区分";
			this.lblSeijunKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShishoKubun
			// 
			this.txtShishoKubun.AutoSizeFromLength = false;
			this.txtShishoKubun.BackColor = System.Drawing.Color.White;
			this.txtShishoKubun.DisplayLength = null;
			this.txtShishoKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShishoKubun.Location = new System.Drawing.Point(119, 1);
			this.txtShishoKubun.Margin = new System.Windows.Forms.Padding(5);
			this.txtShishoKubun.MaxLength = 1;
			this.txtShishoKubun.Name = "txtShishoKubun";
			this.txtShishoKubun.Size = new System.Drawing.Size(19, 23);
			this.txtShishoKubun.TabIndex = 21;
			this.txtShishoKubun.Text = "0";
			this.txtShishoKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtShishoKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoKubun_Validating);
			// 
			// lblShishoKubun
			// 
			this.lblShishoKubun.BackColor = System.Drawing.Color.Silver;
			this.lblShishoKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShishoKubun.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShishoKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShishoKubun.Location = new System.Drawing.Point(0, 0);
			this.lblShishoKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShishoKubun.Name = "lblShishoKubun";
			this.lblShishoKubun.Size = new System.Drawing.Size(539, 26);
			this.lblShishoKubun.TabIndex = 14;
			this.lblShishoKubun.Tag = "CHANGE";
			this.lblShishoKubun.Text = "支所区分";
			this.lblShishoKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShiharaiKubunMemo
			// 
			this.lblShiharaiKubunMemo.AutoSize = true;
			this.lblShiharaiKubunMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShiharaiKubunMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShiharaiKubunMemo.Location = new System.Drawing.Point(141, 5);
			this.lblShiharaiKubunMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiharaiKubunMemo.Name = "lblShiharaiKubunMemo";
			this.lblShiharaiKubunMemo.Size = new System.Drawing.Size(176, 16);
			this.lblShiharaiKubunMemo.TabIndex = 13;
			this.lblShiharaiKubunMemo.Tag = "CHANGE";
			this.lblShiharaiKubunMemo.Text = "1:口座有り 2:口座無し";
			this.lblShiharaiKubunMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiharaiKubun
			// 
			this.txtShiharaiKubun.AutoSizeFromLength = false;
			this.txtShiharaiKubun.BackColor = System.Drawing.Color.White;
			this.txtShiharaiKubun.DisplayLength = null;
			this.txtShiharaiKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShiharaiKubun.Location = new System.Drawing.Point(119, 1);
			this.txtShiharaiKubun.Margin = new System.Windows.Forms.Padding(5);
			this.txtShiharaiKubun.MaxLength = 1;
			this.txtShiharaiKubun.Name = "txtShiharaiKubun";
			this.txtShiharaiKubun.Size = new System.Drawing.Size(19, 23);
			this.txtShiharaiKubun.TabIndex = 20;
			this.txtShiharaiKubun.Text = "0";
			this.txtShiharaiKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtShiharaiKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiKubun_Validating);
			// 
			// lblShiharaiKubun
			// 
			this.lblShiharaiKubun.BackColor = System.Drawing.Color.Silver;
			this.lblShiharaiKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShiharaiKubun.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShiharaiKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShiharaiKubun.Location = new System.Drawing.Point(0, 0);
			this.lblShiharaiKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiharaiKubun.Name = "lblShiharaiKubun";
			this.lblShiharaiKubun.Size = new System.Drawing.Size(539, 26);
			this.lblShiharaiKubun.TabIndex = 11;
			this.lblShiharaiKubun.Tag = "CHANGE";
			this.lblShiharaiKubun.Text = "支払区分";
			this.lblShiharaiKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKumiaiTesuryoRitsu
			// 
			this.txtKumiaiTesuryoRitsu.AutoSizeFromLength = false;
			this.txtKumiaiTesuryoRitsu.BackColor = System.Drawing.Color.White;
			this.txtKumiaiTesuryoRitsu.DisplayLength = null;
			this.txtKumiaiTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKumiaiTesuryoRitsu.Location = new System.Drawing.Point(119, 1);
			this.txtKumiaiTesuryoRitsu.Margin = new System.Windows.Forms.Padding(5);
			this.txtKumiaiTesuryoRitsu.MaxLength = 5;
			this.txtKumiaiTesuryoRitsu.Name = "txtKumiaiTesuryoRitsu";
			this.txtKumiaiTesuryoRitsu.Size = new System.Drawing.Size(73, 23);
			this.txtKumiaiTesuryoRitsu.TabIndex = 19;
			this.txtKumiaiTesuryoRitsu.Text = "0.00";
			this.txtKumiaiTesuryoRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKumiaiTesuryoRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtKumiaiTesuryoRitsu_Validating);
			// 
			// lblKumiaiTesuryoRitsu
			// 
			this.lblKumiaiTesuryoRitsu.BackColor = System.Drawing.Color.Silver;
			this.lblKumiaiTesuryoRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKumiaiTesuryoRitsu.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKumiaiTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKumiaiTesuryoRitsu.Location = new System.Drawing.Point(0, 0);
			this.lblKumiaiTesuryoRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKumiaiTesuryoRitsu.Name = "lblKumiaiTesuryoRitsu";
			this.lblKumiaiTesuryoRitsu.Size = new System.Drawing.Size(539, 26);
			this.lblKumiaiTesuryoRitsu.TabIndex = 9;
			this.lblKumiaiTesuryoRitsu.Tag = "CHANGE";
			this.lblKumiaiTesuryoRitsu.Text = "組合手数料率";
			this.lblKumiaiTesuryoRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblChikuCdSub
			// 
			this.lblChikuCdSub.BackColor = System.Drawing.Color.LightCyan;
			this.lblChikuCdSub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblChikuCdSub.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblChikuCdSub.Location = new System.Drawing.Point(196, 0);
			this.lblChikuCdSub.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblChikuCdSub.Name = "lblChikuCdSub";
			this.lblChikuCdSub.Size = new System.Drawing.Size(248, 24);
			this.lblChikuCdSub.TabIndex = 8;
			this.lblChikuCdSub.Tag = "DISPNAME";
			this.lblChikuCdSub.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblGyohoCdSub
			// 
			this.lblGyohoCdSub.BackColor = System.Drawing.Color.LightCyan;
			this.lblGyohoCdSub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyohoCdSub.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblGyohoCdSub.Location = new System.Drawing.Point(195, 0);
			this.lblGyohoCdSub.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyohoCdSub.Name = "lblGyohoCdSub";
			this.lblGyohoCdSub.Size = new System.Drawing.Size(248, 24);
			this.lblGyohoCdSub.TabIndex = 5;
			this.lblGyohoCdSub.Tag = "DISPNAME";
			this.lblGyohoCdSub.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtChikuCd
			// 
			this.txtChikuCd.AutoSizeFromLength = false;
			this.txtChikuCd.BackColor = System.Drawing.Color.White;
			this.txtChikuCd.DisplayLength = null;
			this.txtChikuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtChikuCd.Location = new System.Drawing.Point(119, 0);
			this.txtChikuCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtChikuCd.MaxLength = 5;
			this.txtChikuCd.Name = "txtChikuCd";
			this.txtChikuCd.Size = new System.Drawing.Size(73, 23);
			this.txtChikuCd.TabIndex = 18;
			this.txtChikuCd.Text = "0";
			this.txtChikuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtChikuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCd_Validating);
			// 
			// txtGyohoCd
			// 
			this.txtGyohoCd.AutoSizeFromLength = false;
			this.txtGyohoCd.BackColor = System.Drawing.Color.White;
			this.txtGyohoCd.DisplayLength = null;
			this.txtGyohoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtGyohoCd.Location = new System.Drawing.Point(119, 1);
			this.txtGyohoCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtGyohoCd.MaxLength = 5;
			this.txtGyohoCd.Name = "txtGyohoCd";
			this.txtGyohoCd.Size = new System.Drawing.Size(73, 23);
			this.txtGyohoCd.TabIndex = 17;
			this.txtGyohoCd.Text = "0";
			this.txtGyohoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyohoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoCd_Validating);
			// 
			// lblFuneNmCdSub
			// 
			this.lblFuneNmCdSub.BackColor = System.Drawing.Color.LightCyan;
			this.lblFuneNmCdSub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFuneNmCdSub.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFuneNmCdSub.Location = new System.Drawing.Point(195, 0);
			this.lblFuneNmCdSub.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFuneNmCdSub.Name = "lblFuneNmCdSub";
			this.lblFuneNmCdSub.Size = new System.Drawing.Size(248, 24);
			this.lblFuneNmCdSub.TabIndex = 2;
			this.lblFuneNmCdSub.Tag = "DISPNAME";
			this.lblFuneNmCdSub.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFuneNmCd
			// 
			this.txtFuneNmCd.AutoSizeFromLength = false;
			this.txtFuneNmCd.BackColor = System.Drawing.Color.White;
			this.txtFuneNmCd.DisplayLength = null;
			this.txtFuneNmCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFuneNmCd.Location = new System.Drawing.Point(119, 1);
			this.txtFuneNmCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtFuneNmCd.MaxLength = 5;
			this.txtFuneNmCd.Name = "txtFuneNmCd";
			this.txtFuneNmCd.Size = new System.Drawing.Size(73, 23);
			this.txtFuneNmCd.TabIndex = 16;
			this.txtFuneNmCd.Text = "0";
			this.txtFuneNmCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFuneNmCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuneNmCd_Validating);
			// 
			// lblChikuCd
			// 
			this.lblChikuCd.BackColor = System.Drawing.Color.Silver;
			this.lblChikuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblChikuCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblChikuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblChikuCd.Location = new System.Drawing.Point(0, 0);
			this.lblChikuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblChikuCd.Name = "lblChikuCd";
			this.lblChikuCd.Size = new System.Drawing.Size(539, 26);
			this.lblChikuCd.TabIndex = 6;
			this.lblChikuCd.Tag = "CHANGE";
			this.lblChikuCd.Text = "地 区 C D";
			this.lblChikuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblGyohoCd
			// 
			this.lblGyohoCd.BackColor = System.Drawing.Color.Silver;
			this.lblGyohoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyohoCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGyohoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblGyohoCd.Location = new System.Drawing.Point(0, 0);
			this.lblGyohoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyohoCd.Name = "lblGyohoCd";
			this.lblGyohoCd.Size = new System.Drawing.Size(539, 26);
			this.lblGyohoCd.TabIndex = 3;
			this.lblGyohoCd.Tag = "CHANGE";
			this.lblGyohoCd.Text = "漁 法 C D";
			this.lblGyohoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFuneNmCd
			// 
			this.lblFuneNmCd.BackColor = System.Drawing.Color.Silver;
			this.lblFuneNmCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFuneNmCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFuneNmCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFuneNmCd.Location = new System.Drawing.Point(0, 0);
			this.lblFuneNmCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFuneNmCd.Name = "lblFuneNmCd";
			this.lblFuneNmCd.Size = new System.Drawing.Size(539, 26);
			this.lblFuneNmCd.TabIndex = 0;
			this.lblFuneNmCd.Tag = "CHANGE";
			this.lblFuneNmCd.Text = "船 名 C D";
			this.lblFuneNmCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(19, 31);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.40262F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.59738F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1100, 365);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txthyoji);
			this.fsiPanel2.Controls.Add(this.label4);
			this.fsiPanel2.Controls.Add(this.button1);
			this.fsiPanel2.Controls.Add(this.txtMyNumber);
			this.fsiPanel2.Controls.Add(this.label5);
			this.fsiPanel2.Controls.Add(this.txtMyNumberHide);
			this.fsiPanel2.Controls.Add(this.lblShzNrkHohoNm);
			this.fsiPanel2.Controls.Add(this.label3);
			this.fsiPanel2.Controls.Add(this.txtShohizeiNyuryokuHoho);
			this.fsiPanel2.Controls.Add(this.txtDattaiRiyu);
			this.fsiPanel2.Controls.Add(this.lblKgkHsShrMemo);
			this.fsiPanel2.Controls.Add(this.txtKingakuHasuShori);
			this.fsiPanel2.Controls.Add(this.label9);
			this.fsiPanel2.Controls.Add(this.lblDattaiRiyu);
			this.fsiPanel2.Controls.Add(this.txtDattaiMonth);
			this.fsiPanel2.Controls.Add(this.lblShohizeiNyuryokuHoho);
			this.fsiPanel2.Controls.Add(this.lblDattaiGengo);
			this.fsiPanel2.Controls.Add(this.txtDattaiYear);
			this.fsiPanel2.Controls.Add(this.lblKingakuHasuShori);
			this.fsiPanel2.Controls.Add(this.txtDattaiDay);
			this.fsiPanel2.Controls.Add(this.lblDattaiJp);
			this.fsiPanel2.Controls.Add(this.lblTnkStkHohoMemo);
			this.fsiPanel2.Controls.Add(this.lblDattaiJpYMD);
			this.fsiPanel2.Controls.Add(this.txtKanyuDay);
			this.fsiPanel2.Controls.Add(this.txtTankaShutokuHoho);
			this.fsiPanel2.Controls.Add(this.txtKanyuMonth);
			this.fsiPanel2.Controls.Add(this.lblKanyuGengo);
			this.fsiPanel2.Controls.Add(this.lblKanyuJp);
			this.fsiPanel2.Controls.Add(this.txtKanyuYear);
			this.fsiPanel2.Controls.Add(this.lblTantoshaNm);
			this.fsiPanel2.Controls.Add(this.lblKanyuJpYMD);
			this.fsiPanel2.Controls.Add(this.txtTantoshaCd);
			this.fsiPanel2.Controls.Add(this.lblTankaShutokuHoho);
			this.fsiPanel2.Controls.Add(this.txtJpDay);
			this.fsiPanel2.Controls.Add(this.txtJpMonth);
			this.fsiPanel2.Controls.Add(this.txtJpYear);
			this.fsiPanel2.Controls.Add(this.lblJpGengo);
			this.fsiPanel2.Controls.Add(this.lblJp);
			this.fsiPanel2.Controls.Add(this.lblJpYMD);
			this.fsiPanel2.Controls.Add(this.txtFax);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Controls.Add(this.lblTantoshaCd);
			this.fsiPanel2.Controls.Add(this.txtKaishuBi);
			this.fsiPanel2.Controls.Add(this.label1);
			this.fsiPanel2.Controls.Add(this.txtKaishuTsuki);
			this.fsiPanel2.Controls.Add(this.lblKaishuTsuki);
			this.fsiPanel2.Controls.Add(this.txtTel);
			this.fsiPanel2.Controls.Add(this.lblShzTnkHohoMemo);
			this.fsiPanel2.Controls.Add(this.lblFax);
			this.fsiPanel2.Controls.Add(this.lblShohizeiTenkaHoho);
			this.fsiPanel2.Controls.Add(this.txtShohizeiTenkaHoho);
			this.fsiPanel2.Controls.Add(this.txtJusho2);
			this.fsiPanel2.Controls.Add(this.lblShzHsSrMemo);
			this.fsiPanel2.Controls.Add(this.txtShohizeiHasuShori);
			this.fsiPanel2.Controls.Add(this.lblTel);
			this.fsiPanel2.Controls.Add(this.lblShohizeiHasuShori);
			this.fsiPanel2.Controls.Add(this.txtJusho1);
			this.fsiPanel2.Controls.Add(this.lblJusho2);
			this.fsiPanel2.Controls.Add(this.txtSeikyushoKeishiki);
			this.fsiPanel2.Controls.Add(this.lblSkshKskMemo);
			this.fsiPanel2.Controls.Add(this.lblSeikyushoKeishiki);
			this.fsiPanel2.Controls.Add(this.txtYubinBango1);
			this.fsiPanel2.Controls.Add(this.lblJusho1);
			this.fsiPanel2.Controls.Add(this.txtYubinBango2);
			this.fsiPanel2.Controls.Add(this.lblSkshHkMemo);
			this.fsiPanel2.Controls.Add(this.lblAddBar);
			this.fsiPanel2.Controls.Add(this.txtSeikyushoHakko);
			this.fsiPanel2.Controls.Add(this.lblSeikyushoHakko);
			this.fsiPanel2.Controls.Add(this.txtRyFunanushiNm);
			this.fsiPanel2.Controls.Add(this.lblShimebiMemo);
			this.fsiPanel2.Controls.Add(this.txtShimebi);
			this.fsiPanel2.Controls.Add(this.lblYubinBango);
			this.fsiPanel2.Controls.Add(this.lblShimebi);
			this.fsiPanel2.Controls.Add(this.txtFunanushiKanaNm);
			this.fsiPanel2.Controls.Add(this.lblRyFunanushiNm);
			this.fsiPanel2.Controls.Add(this.txtFunanushiNm);
			this.fsiPanel2.Controls.Add(this.lblSeikyusakiCd);
			this.fsiPanel2.Controls.Add(this.txtSeikyusakiCd);
			this.fsiPanel2.Controls.Add(this.lblSeikyusakiNm);
			this.fsiPanel2.Controls.Add(this.lblFunanushiKanaNm);
			this.fsiPanel2.Controls.Add(this.lblFunanushiNm);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 42);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(1092, 319);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtFunanushiCd);
			this.fsiPanel1.Controls.Add(this.lblFunanushiCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(1092, 31);
			this.fsiPanel1.TabIndex = 0;
			// 
			// fsiTableLayoutPanel2
			// 
			this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel2.ColumnCount = 1;
			this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel22, 0, 7);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel21, 0, 6);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel20, 0, 5);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel19, 0, 4);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel18, 0, 3);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel17, 0, 2);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel16, 0, 1);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel15, 0, 0);
			this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(19, 399);
			this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
			this.fsiTableLayoutPanel2.RowCount = 8;
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
			this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(547, 272);
			this.fsiTableLayoutPanel2.TabIndex = 1001;
			this.fsiTableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.fsiTableLayoutPanel2_Paint);
			// 
			// fsiPanel22
			// 
			this.fsiPanel22.Controls.Add(this.lblFutsuRitsu);
			this.fsiPanel22.Controls.Add(this.txtPayaoRitsu);
			this.fsiPanel22.Controls.Add(this.txtFutsuRitsu);
			this.fsiPanel22.Controls.Add(this.lblPayaoRitsu);
			this.fsiPanel22.Controls.Add(this.txtSonotaRitsu);
			this.fsiPanel22.Controls.Add(this.lblSonotaRitsu);
			this.fsiPanel22.Controls.Add(this.lblFurikomisakiWariai);
			this.fsiPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel22.Location = new System.Drawing.Point(4, 235);
			this.fsiPanel22.Name = "fsiPanel22";
			this.fsiPanel22.Size = new System.Drawing.Size(539, 33);
			this.fsiPanel22.TabIndex = 7;
			// 
			// fsiPanel21
			// 
			this.fsiPanel21.Controls.Add(this.lblSeijunKubunMemo);
			this.fsiPanel21.Controls.Add(this.txtSeijunKubun);
			this.fsiPanel21.Controls.Add(this.lblSeijunKubun);
			this.fsiPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel21.Location = new System.Drawing.Point(4, 202);
			this.fsiPanel21.Name = "fsiPanel21";
			this.fsiPanel21.Size = new System.Drawing.Size(539, 26);
			this.fsiPanel21.TabIndex = 6;
			// 
			// fsiPanel20
			// 
			this.fsiPanel20.Controls.Add(this.txtShishoKubun);
			this.fsiPanel20.Controls.Add(this.lblShishoKubunMemo);
			this.fsiPanel20.Controls.Add(this.lblShishoKubun);
			this.fsiPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel20.Location = new System.Drawing.Point(4, 169);
			this.fsiPanel20.Name = "fsiPanel20";
			this.fsiPanel20.Size = new System.Drawing.Size(539, 26);
			this.fsiPanel20.TabIndex = 5;
			// 
			// fsiPanel19
			// 
			this.fsiPanel19.Controls.Add(this.lblShiharaiKubunMemo);
			this.fsiPanel19.Controls.Add(this.txtShiharaiKubun);
			this.fsiPanel19.Controls.Add(this.lblShiharaiKubun);
			this.fsiPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel19.Location = new System.Drawing.Point(4, 136);
			this.fsiPanel19.Name = "fsiPanel19";
			this.fsiPanel19.Size = new System.Drawing.Size(539, 26);
			this.fsiPanel19.TabIndex = 4;
			// 
			// fsiPanel18
			// 
			this.fsiPanel18.Controls.Add(this.txtKumiaiTesuryoRitsu);
			this.fsiPanel18.Controls.Add(this.lblKumiaiTesuryoRitsu);
			this.fsiPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel18.Location = new System.Drawing.Point(4, 103);
			this.fsiPanel18.Name = "fsiPanel18";
			this.fsiPanel18.Size = new System.Drawing.Size(539, 26);
			this.fsiPanel18.TabIndex = 3;
			// 
			// fsiPanel17
			// 
			this.fsiPanel17.Controls.Add(this.txtChikuCd);
			this.fsiPanel17.Controls.Add(this.lblChikuCdSub);
			this.fsiPanel17.Controls.Add(this.lblChikuCd);
			this.fsiPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel17.Location = new System.Drawing.Point(4, 70);
			this.fsiPanel17.Name = "fsiPanel17";
			this.fsiPanel17.Size = new System.Drawing.Size(539, 26);
			this.fsiPanel17.TabIndex = 2;
			// 
			// fsiPanel16
			// 
			this.fsiPanel16.Controls.Add(this.txtGyohoCd);
			this.fsiPanel16.Controls.Add(this.lblGyohoCdSub);
			this.fsiPanel16.Controls.Add(this.lblGyohoCd);
			this.fsiPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel16.Location = new System.Drawing.Point(4, 37);
			this.fsiPanel16.Name = "fsiPanel16";
			this.fsiPanel16.Size = new System.Drawing.Size(539, 26);
			this.fsiPanel16.TabIndex = 1;
			// 
			// fsiPanel15
			// 
			this.fsiPanel15.Controls.Add(this.txtFuneNmCd);
			this.fsiPanel15.Controls.Add(this.lblFuneNmCdSub);
			this.fsiPanel15.Controls.Add(this.lblFuneNmCd);
			this.fsiPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel15.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel15.Name = "fsiPanel15";
			this.fsiPanel15.Size = new System.Drawing.Size(539, 26);
			this.fsiPanel15.TabIndex = 0;
			// 
			// fsiTableLayoutPanel3
			// 
			this.fsiTableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel3.ColumnCount = 1;
			this.fsiTableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel26, 0, 3);
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel25, 0, 2);
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel24, 0, 1);
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel23, 0, 0);
			this.fsiTableLayoutPanel3.Location = new System.Drawing.Point(568, 399);
			this.fsiTableLayoutPanel3.Name = "fsiTableLayoutPanel3";
			this.fsiTableLayoutPanel3.RowCount = 4;
			this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.92032F));
			this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.92032F));
			this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.84063F));
			this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.31872F));
			this.fsiTableLayoutPanel3.Size = new System.Drawing.Size(552, 195);
			this.fsiTableLayoutPanel3.TabIndex = 1002;
			// 
			// fsiPanel26
			// 
			this.fsiPanel26.Controls.Add(this.txtKozaKubunHonnin);
			this.fsiPanel26.Controls.Add(this.lblKozaKubunHonninSelect);
			this.fsiPanel26.Controls.Add(this.lblKozaBangoHonnin);
			this.fsiPanel26.Controls.Add(this.lblKozaKubunHonnin);
			this.fsiPanel26.Controls.Add(this.txtTsumitateRitsuHonnin);
			this.fsiPanel26.Controls.Add(this.txtKozaBangoHonnin);
			this.fsiPanel26.Controls.Add(this.lblTsumitateRitsuHonnin);
			this.fsiPanel26.Controls.Add(this.label8);
			this.fsiPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel26.Location = new System.Drawing.Point(4, 156);
			this.fsiPanel26.Name = "fsiPanel26";
			this.fsiPanel26.Size = new System.Drawing.Size(544, 35);
			this.fsiPanel26.TabIndex = 3;
			// 
			// label8
			// 
			this.label8.BackColor = System.Drawing.Color.Silver;
			this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label8.Location = new System.Drawing.Point(0, 0);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(544, 35);
			this.label8.TabIndex = 1;
			this.label8.Tag = "CHANGE";
			this.label8.Text = "積立口座";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel25
			// 
			this.fsiPanel25.Controls.Add(this.txtKozaKubunAzukari);
			this.fsiPanel25.Controls.Add(this.lblKozaKubunAzukariSelect);
			this.fsiPanel25.Controls.Add(this.txtTsumitateRitsuAzukari);
			this.fsiPanel25.Controls.Add(this.txtKozaBangoAzukari);
			this.fsiPanel25.Controls.Add(this.lblKozaBangoAzukari);
			this.fsiPanel25.Controls.Add(this.lblKozaKubunAzukari);
			this.fsiPanel25.Controls.Add(this.lblTsumitateRitsuAzukari);
			this.fsiPanel25.Controls.Add(this.label7);
			this.fsiPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel25.Location = new System.Drawing.Point(4, 80);
			this.fsiPanel25.Name = "fsiPanel25";
			this.fsiPanel25.Size = new System.Drawing.Size(544, 69);
			this.fsiPanel25.TabIndex = 2;
			// 
			// label7
			// 
			this.label7.BackColor = System.Drawing.Color.Silver;
			this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label7.Location = new System.Drawing.Point(0, 0);
			this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(544, 69);
			this.label7.TabIndex = 1;
			this.label7.Tag = "CHANGE";
			this.label7.Text = "積立口座                                                          　\r\n預かり金";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel24
			// 
			this.fsiPanel24.Controls.Add(this.txtTsumitateKozaKubun);
			this.fsiPanel24.Controls.Add(this.txtTsumitateRitsu);
			this.fsiPanel24.Controls.Add(this.txtTsumitateKozaBango);
			this.fsiPanel24.Controls.Add(this.label6);
			this.fsiPanel24.Controls.Add(this.lblTsumitateKozaKubunMemo);
			this.fsiPanel24.Controls.Add(this.lblTsumitateKozaKubun);
			this.fsiPanel24.Controls.Add(this.lblTsumitateKozaBango);
			this.fsiPanel24.Controls.Add(this.lblTsumitateRitsu);
			this.fsiPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel24.Location = new System.Drawing.Point(4, 42);
			this.fsiPanel24.Name = "fsiPanel24";
			this.fsiPanel24.Size = new System.Drawing.Size(544, 31);
			this.fsiPanel24.TabIndex = 1;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.BackColor = System.Drawing.Color.Silver;
			this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label6.Location = new System.Drawing.Point(226, 8);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(56, 16);
			this.label6.TabIndex = 7;
			this.label6.Tag = "CHANGE";
			this.label6.Text = "口座率";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel23
			// 
			this.fsiPanel23.Controls.Add(this.txtFutsuKozaKubun);
			this.fsiPanel23.Controls.Add(this.txtFutsuKozaBango);
			this.fsiPanel23.Controls.Add(this.lblFutsuKozaKubunSelect);
			this.fsiPanel23.Controls.Add(this.lblFutsuKozaKubun);
			this.fsiPanel23.Controls.Add(this.lblFutsuKozaBango);
			this.fsiPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel23.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel23.Name = "fsiPanel23";
			this.fsiPanel23.Size = new System.Drawing.Size(544, 31);
			this.fsiPanel23.TabIndex = 0;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.BackColor = System.Drawing.Color.Silver;
			this.label9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label9.Location = new System.Drawing.Point(4, 5);
			this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(88, 16);
			this.label9.TabIndex = 960;
			this.label9.Tag = "CHANGE";
			this.label9.Text = "正式船主名";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// CMCM2012
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(1126, 818);
			this.Controls.Add(this.fsiTableLayoutPanel3);
			this.Controls.Add(this.fsiTableLayoutPanel2);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.gbxSenshuInfo);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "CMCM2012";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.Text = "船主マスタ登録";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.gbxSenshuInfo, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel3, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.pnlDebug.ResumeLayout(false);
			this.gbxSenshuInfo.ResumeLayout(false);
			this.gbxSenshuInfo.PerformLayout();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.fsiTableLayoutPanel2.ResumeLayout(false);
			this.fsiPanel22.ResumeLayout(false);
			this.fsiPanel22.PerformLayout();
			this.fsiPanel21.ResumeLayout(false);
			this.fsiPanel21.PerformLayout();
			this.fsiPanel20.ResumeLayout(false);
			this.fsiPanel20.PerformLayout();
			this.fsiPanel19.ResumeLayout(false);
			this.fsiPanel19.PerformLayout();
			this.fsiPanel18.ResumeLayout(false);
			this.fsiPanel18.PerformLayout();
			this.fsiPanel17.ResumeLayout(false);
			this.fsiPanel17.PerformLayout();
			this.fsiPanel16.ResumeLayout(false);
			this.fsiPanel16.PerformLayout();
			this.fsiPanel15.ResumeLayout(false);
			this.fsiPanel15.PerformLayout();
			this.fsiTableLayoutPanel3.ResumeLayout(false);
			this.fsiPanel26.ResumeLayout(false);
			this.fsiPanel26.PerformLayout();
			this.fsiPanel25.ResumeLayout(false);
			this.fsiPanel25.PerformLayout();
			this.fsiPanel24.ResumeLayout(false);
			this.fsiPanel24.PerformLayout();
			this.fsiPanel23.ResumeLayout(false);
			this.fsiPanel23.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblFunanushiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCd;
        private System.Windows.Forms.GroupBox gbxSenshuInfo;
        private System.Windows.Forms.Label lblFax;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.Label lblJusho2;
        private System.Windows.Forms.Label lblJusho1;
        private System.Windows.Forms.Label lblYubinBango;
        private System.Windows.Forms.Label lblRyFunanushiNm;
        private System.Windows.Forms.Label lblFunanushiKanaNm;
        private System.Windows.Forms.Label lblFunanushiNm;
        private System.Windows.Forms.Label lblDattaiRiyu;
        private System.Windows.Forms.Label lblDattaiJp;
        private System.Windows.Forms.Label lblKanyuJp;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblKaishuTsuki;
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblSeikyushoKeishiki;
        private System.Windows.Forms.Label lblSeikyushoHakko;
        private System.Windows.Forms.Label lblShimebi;
        private System.Windows.Forms.Label lblSeikyusakiCd;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private System.Windows.Forms.Label lblTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtDattaiRiyu;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtKingakuHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtTankaShutokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFax;
        private jp.co.fsi.common.controls.FsiTextBox txtTel;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho2;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho1;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango1;
        private jp.co.fsi.common.controls.FsiTextBox txtRyFunanushiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiKanaNm;
        private System.Windows.Forms.Label lblAddBar;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango2;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuTsuki;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiTenkaHoho;
        private jp.co.fsi.common.controls.FsiTextBox textBox14;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoKeishiki;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoHakko;
        private jp.co.fsi.common.controls.FsiTextBox txtShimebi;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyusakiCd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
        private System.Windows.Forms.Label lblShzHsSrMemo;
        private System.Windows.Forms.Label lblSkshKskMemo;
        private System.Windows.Forms.Label lblSkshHkMemo;
        private System.Windows.Forms.Label lblShimebiMemo;
        private System.Windows.Forms.Label lblSeikyusakiNm;
        private System.Windows.Forms.Label lblFutsuKozaKubun;
        private System.Windows.Forms.Label lblFutsuKozaBango;
        private System.Windows.Forms.Label lblChikuCd;
        private System.Windows.Forms.Label lblGyohoCd;
        private System.Windows.Forms.Label lblFuneNmCd;
        private System.Windows.Forms.Label lblDattaiGengo;
        private System.Windows.Forms.Label lblKanyuGengo;
        private System.Windows.Forms.Label lblJpGengo;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuBi;
        private System.Windows.Forms.Label label2;
        private jp.co.fsi.common.controls.FsiTextBox txtJpDay;
        private jp.co.fsi.common.controls.FsiTextBox txtJpMonth;
        private System.Windows.Forms.Label lblJpYMD;
        private jp.co.fsi.common.controls.FsiTextBox txtDattaiDay;
        private jp.co.fsi.common.controls.FsiTextBox txtKanyuDay;
        private jp.co.fsi.common.controls.FsiTextBox txtDattaiMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtKanyuMonth;
        private System.Windows.Forms.Label lblKanyuJpYMD;
        private System.Windows.Forms.Label lblDattaiJpYMD;
        private System.Windows.Forms.Label lblChikuCdSub;
        private System.Windows.Forms.Label lblGyohoCdSub;
        private jp.co.fsi.common.controls.FsiTextBox txtChikuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoCd;
        private System.Windows.Forms.Label lblFuneNmCdSub;
        private jp.co.fsi.common.controls.FsiTextBox txtFuneNmCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFutsuKozaKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtFutsuKozaBango;
        private System.Windows.Forms.Label lblFutsuKozaKubunSelect;
        private jp.co.fsi.common.controls.FsiTextBox txtKumiaiTesuryoRitsu;
        private System.Windows.Forms.Label lblKumiaiTesuryoRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtTsumitateKozaKubun;
        private System.Windows.Forms.Label lblTsumitateKozaKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtTsumitateKozaBango;
        private System.Windows.Forms.Label lblTsumitateKozaBango;
        private jp.co.fsi.common.controls.FsiTextBox txtTsumitateRitsu;
        private System.Windows.Forms.Label lblTsumitateRitsu;
        private System.Windows.Forms.Label lblShiharaiKubunMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtShiharaiKubun;
        private System.Windows.Forms.Label lblShiharaiKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtSeijunKubun;
        private System.Windows.Forms.Label lblSeijunKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtShishoKubun;
        private System.Windows.Forms.Label lblShishoKubun;
        private System.Windows.Forms.Label lblSeijunKubunMemo;
        private System.Windows.Forms.Label lblTsumitateKozaKubunMemo;
        private System.Windows.Forms.Label lblShishoKubunMemo;
        private System.Windows.Forms.Label lblPayaoRitsu;
        private System.Windows.Forms.Label lblSonotaRitsu;
        private System.Windows.Forms.Label lblFutsuRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtSonotaRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtFutsuRitsu;
        private System.Windows.Forms.Label lblFurikomisakiWariai;
        private jp.co.fsi.common.controls.FsiTextBox txtKozaBangoAzukari;
        private System.Windows.Forms.Label lblKozaBangoAzukari;
        private jp.co.fsi.common.controls.FsiTextBox txtTsumitateRitsuAzukari;
        private System.Windows.Forms.Label lblTsumitateRitsuAzukari;
        private jp.co.fsi.common.controls.FsiTextBox txtDattaiYear;
        private jp.co.fsi.common.controls.FsiTextBox txtKanyuYear;
        private jp.co.fsi.common.controls.FsiTextBox txtJpYear;
        private jp.co.fsi.common.controls.FsiTextBox txtKozaKubunAzukari;
        private System.Windows.Forms.Label lblKozaKubunAzukari;
        private System.Windows.Forms.Label lblKozaKubunAzukariSelect;
        private common.controls.FsiTextBox txtKozaKubunHonnin;
        private common.controls.FsiTextBox txtTsumitateRitsuHonnin;
        private common.controls.FsiTextBox txtKozaBangoHonnin;
        private System.Windows.Forms.Label lblTsumitateRitsuHonnin;
        private System.Windows.Forms.Label lblKozaBangoHonnin;
        private System.Windows.Forms.Label lblKozaKubunHonnin;
        private System.Windows.Forms.Label lblKozaKubunHonninSelect;
        private common.controls.FsiTextBox txtPayaoRitsu;
        private System.Windows.Forms.Button button1;
        private common.controls.FsiTextBox txtMyNumberHide;
        private common.controls.FsiTextBox txtMyNumber;
        private System.Windows.Forms.Label label3;
        private common.controls.FsiTextBox txthyoji;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
		private common.FsiPanel fsiPanel22;
		private common.FsiPanel fsiPanel21;
		private common.FsiPanel fsiPanel20;
		private common.FsiPanel fsiPanel19;
		private common.FsiPanel fsiPanel18;
		private common.FsiPanel fsiPanel17;
		private common.FsiPanel fsiPanel16;
		private common.FsiPanel fsiPanel15;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel3;
		private common.FsiPanel fsiPanel26;
		private common.FsiPanel fsiPanel25;
		private common.FsiPanel fsiPanel24;
		private System.Windows.Forms.Label label6;
		private common.FsiPanel fsiPanel23;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label9;
	}
}