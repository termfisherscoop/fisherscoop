﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Security.Cryptography;
using jp.co.fsi.common.mynumber;

namespace jp.co.fsi.cm.cmcm2011
{
    /// <summary>
    /// 船主マスタ登録(CMCM2012)
    /// </summary>
    public partial class CMCM2012 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";

        /// <summary>
        /// モード(購買)
        /// </summary>
        private const string MODE_KOBAI = "1";

        /// <summary>
        /// モード(販売)
        /// </summary>
        private const string MODE_HANBAI = "2";

        //パスワードに使用する文字
        private static readonly string passwordChars = "0123456789abcdefghijklmnopqrstuvwxyz";
        #endregion

        #region プロパティ
        /// <summary>
        /// 公開フラグ用変数
        /// </summary>
        private bool _numberFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._numberFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2012()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        /// <param name="par2">引数2</param>
        public CMCM2012(string par1, string par2)
            : base(par1, par2)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            DataRow roginUser = GetroginUserdetail();//ログインしている人のユーザ情報取得
            if (Util.ToInt(roginUser["USER_KUBUN"]).Equals(1))// 管理者権限のみ公開ボタンを表示
            {
                this.button1.Visible = true;
                this.txtMyNumber.ReadOnly = false;
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;

            //ボタン表示
            this.ShowFButton = true;

            // 引数：Par1／モード(1:新規、2:変更)、InData：仕入先コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                //F3非活性化
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
                //F3活性化
                //this.btnF3.Enabled = true;
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);


            // 元号をセット
            this.lblJpGengo.Text = jpDate[0];
            this.lblKanyuGengo.Text = jpDate[0];
            this.lblDattaiGengo.Text = jpDate[0];

            // ボタンの配置を調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;

            // ボタンを調整
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // フォーカス設定
            this.txtFunanushiNm.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 支所,郵便番号1・2,住所1・2,担当者CD,消費税入力方法,日付(年),船名CD,漁法CD,地区CDにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYubinBango1":
                case "txtYubinBango2":
                case "txtJusho1":
                case "txtJusho2":
                case "txtTantoshaCd":
                case "txtShohizeiNyuryokuHoho":
                case "txtJpYear":
                case "txtKanyuYear":
                case "txtDattaiYear":
                case "txtFuneNmCd":
                case "txtGyohoCd":
                case "txtChikuCd":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            switch (this.ActiveCtlNm)
            {
                case "txtYubinBango1":
                case "txtYubinBango2":
                case "txtJusho1":
                case "txtJusho2":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1031.CMCM1031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.ShowDialog();

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtYubinBango1.Text = outData[0];
                                this.txtYubinBango2.Text = outData[1];
                                this.txtJusho1.Text = outData[2];
                            }
                        }
                    }
                    break;

                case "txtTantoshaCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoshaCd.Text = outData[0];
                                this.lblTantoshaNm.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShohizeiNyuryokuHoho":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO";
                            frm.InData = this.txtShohizeiNyuryokuHoho.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtShohizeiNyuryokuHoho.Text = result[0];
                                this.lblShzNrkHohoNm.Text = result[1];

                                // 次の項目(締日)にフォーカス
                                this.txtShimebi.Focus();
                            }
                        }
                    }
                    break;

                case "txtJpYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                // 西暦に一度変換しないとおかしなことにならないか？？？？？？？
                                // 実際おかしなことになってるけど！！！！！！！！

                                string[] wa = null;

                                if (!string.IsNullOrEmpty(this.txtJpYear.Text))
                                {
                                    DateTime WaToSeiDate =
                                        Util.ConvAdDate(this.lblJpGengo.Text,
                                                        this.txtJpYear.Text,
                                                        this.txtJpMonth.Text,
                                                        this.txtJpDay.Text, this.Dba);
                                    //ConvAdDate
                                    result = (String[])frm.OutData;
                                    this.lblJpGengo.Text = result[1];

                                    // ここではじめて和暦変換しないとおかしくないか？
                                    wa = Util.ConvJpDate(WaToSeiDate, this.Dba);
                                    this.txtJpYear.Text = Util.ToString(IsValid.SetYear(wa[2]));
                                    this.txtJpMonth.Text = Util.ToString(IsValid.SetMonth(wa[3]));
                                    this.txtJpDay.Text = Util.ToString(IsValid.SetDay(wa[4]));

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblJpGengo.Text,
                                            this.txtJpYear.Text,
                                            this.txtJpMonth.Text,
                                            this.txtJpDay.Text,
                                            this.Dba);
                                    this.lblJpGengo.Text = arrJpDate[0];
                                    this.txtJpYear.Text = arrJpDate[2];
                                    this.txtJpMonth.Text = arrJpDate[3];
                                    this.txtJpDay.Text = arrJpDate[4];

                                }
                                else
                                {
                                    //ConvAdDate
                                    result = (String[])frm.OutData;
                                    this.lblJpGengo.Text = result[1];
                                }
                            }
                        }
                    }
                    break;

                case "txtKanyuYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {


                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblKanyuGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {

                                string[] wa = null;

                                if (!string.IsNullOrEmpty(this.txtKanyuYear.Text))
                                {
                                    DateTime WaToSeiDate =
                                        Util.ConvAdDate(this.lblJpGengo.Text,
                                                        this.txtKanyuYear.Text,
                                                        this.txtKanyuMonth.Text,
                                                        this.txtKanyuDay.Text, this.Dba);
                                    //ConvAdDate
                                    result = (String[])frm.OutData;
                                    this.lblKanyuGengo.Text = result[1];

                                    // ここではじめて和暦変換しないとおかしくないか？
                                    wa = Util.ConvJpDate(WaToSeiDate, this.Dba);
                                    this.txtKanyuYear.Text = Util.ToString(IsValid.SetYear(wa[2]));
                                    this.txtKanyuMonth.Text = Util.ToString(IsValid.SetMonth(wa[3]));
                                    this.txtKanyuDay.Text = Util.ToString(IsValid.SetDay(wa[4]));


                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblKanyuGengo.Text,
                                            this.txtKanyuYear.Text,
                                            this.txtJpMonth.Text,
                                            this.txtKanyuDay.Text,
                                            this.Dba);
                                    this.lblJpGengo.Text = arrJpDate[0];
                                    this.txtKanyuYear.Text = arrJpDate[2];
                                    this.txtKanyuMonth.Text = arrJpDate[3];
                                    this.txtKanyuDay.Text = arrJpDate[4];

                                }
                                else
                                {
                                    //ConvAdDate
                                    result = (String[])frm.OutData;
                                    this.lblKanyuGengo.Text = result[1];
                                }

                            }
                        }
                    }
                    break;

                case "txtDattaiYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDattaiGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {

                                string[] wa = null;

                                if (!string.IsNullOrEmpty(this.txtKanyuYear.Text))
                                {
                                    DateTime WaToSeiDate =
                                        Util.ConvAdDate(this.lblDattaiGengo.Text,
                                                        this.txtDattaiYear.Text,
                                                        this.txtDattaiMonth.Text,
                                                        this.txtDattaiDay.Text, this.Dba);
                                    //ConvAdDate
                                    result = (String[])frm.OutData;
                                    this.lblKanyuGengo.Text = result[1];

                                    // ここではじめて和暦変換しないとおかしくないか？
                                    wa = Util.ConvJpDate(WaToSeiDate, this.Dba);
                                    this.txtDattaiYear.Text = Util.ToString(IsValid.SetYear(wa[2]));
                                    this.txtDattaiMonth.Text = Util.ToString(IsValid.SetMonth(wa[3]));
                                    this.txtDattaiDay.Text = Util.ToString(IsValid.SetDay(wa[4]));


                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblDattaiGengo.Text,
                                            this.txtDattaiYear.Text,
                                            this.txtDattaiMonth.Text,
                                            this.txtDattaiDay.Text,
                                            this.Dba);
                                    this.lblDattaiGengo.Text = arrJpDate[0];
                                    this.txtDattaiYear.Text = arrJpDate[2];
                                    this.txtDattaiMonth.Text = arrJpDate[3];
                                    this.txtDattaiDay.Text = arrJpDate[4];

                                }
                                else
                                {
                                    //ConvAdDate
                                    result = (String[])frm.OutData;
                                    this.lblDattaiGengo.Text = result[1];
                                }

                            }
                        }
                    }
                    break;

                case "txtFuneNmCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1061.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1061.HNCM1061");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFuneNmCd.Text = outData[0];
                                this.lblFuneNmCdSub.Text = outData[1];

                                // 次の項目(漁法CD)にフォーカス
                                this.txtGyohoCd.Focus();
                            }
                        }
                    }
                    break;

                case "txtGyohoCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1031.HNCM1031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog();

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtGyohoCd.Text = result[0];
                                this.lblGyohoCdSub.Text = result[1];

                                // 次の項目(地区CD)にフォーカス
                                this.txtChikuCd.Focus();
                            }
                        }
                    }
                    break;

                case "txtChikuCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1021.HNCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtChikuCd.Text = outData[0];
                                this.lblChikuCdSub.Text = outData[1];

                                // 次の項目(地区CD)にフォーカス
                                this.txtKumiaiTesuryoRitsu.Focus();
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モード時
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }
            if (!IsValidFunanushiCd(this.txtFunanushiCd.Text))
            {
                Msg.Error("使用されている船主コードの為、削除出来ません。");
                return;
            }
            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@FUNANUSHI_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);
            dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 3);
            try
            {
                this.Dba.BeginTransaction();

                // マイナンバーデータ削除
                this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @FUNANUSHI_CD AND KUBUN = @KUBUN", dpc);
                this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @FUNANUSHI_CD AND KUBUN = @KUBUN", dpc);

                // 取引先
                this.Dba.Delete("TB_CM_TORIHIKISAKI",
                    "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                    (DbParamCollection)delParams[0]);
                // 取引先情報
                this.Dba.Delete("TB_HN_TORIHIKISAKI_JOHO",
                    "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                    (DbParamCollection)delParams[0]);
                // 納品先
                this.Dba.Delete("TB_HN_NOHINSAKI",
                    "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                    (DbParamCollection)delParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTori = SetCmToriParams();
            ArrayList alParamsHnTori = SetHnToriParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // マイナンバー登録
                    Mynumber.InsertNumber(Util.ToString(txtMyNumber.Text), 3, Util.ToInt(txtFunanushiCd.Text), 0, this.Dba);
                    // データ登録
                    // 共通.取引先マスタ
                    this.Dba.Insert("TB_CM_TORIHIKISAKI", (DbParamCollection)alParamsCmTori[0]);
                    // 販売.取引先情報
                    this.Dba.Insert("TB_HN_TORIHIKISAKI_JOHO", (DbParamCollection)alParamsHnTori[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // 削除用パラメータ
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 3);

                    // マイナンバー削除
                    this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND KUBUN = @KUBUN", dpc);
                    this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND KUBUN = @KUBUN", dpc);
                    //// マイナンバー更新
                    Mynumber.InsertNumber(Util.ToString(txtMyNumber.Text), 3, Util.ToInt(txtFunanushiCd.Text), 0, this.Dba);

                    // データ更新
                    // 共通.取引先マスタ
                    this.Dba.Update("TB_CM_TORIHIKISAKI",
                        (DbParamCollection)alParamsCmTori[1],
                        "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                        (DbParamCollection)alParamsCmTori[0]);
                    // 販売.取引先情報
                    this.Dba.Update("TB_HN_TORIHIKISAKI_JOHO",
                        (DbParamCollection)alParamsHnTori[1],
                        "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                        (DbParamCollection)alParamsHnTori[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// マイナンバーの公開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtMyNumberHide.Visible = false;
                this.txtMyNumber.ReadOnly = false;
                _numberFlg = true;
                this.button1.Text = "非公開";
            }
            else
            {
                this.txtMyNumberHide.Visible = true;
                this.txtMyNumber.ReadOnly = true;
                _numberFlg = false;
                this.button1.Text = "公開";
            }
        }

        /// <summary>
        /// 船主コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCd())
            {
                e.Cancel = true;
                this.txtFunanushiCd.SelectAll();
            }
        }

        /// <summary>
        /// 正式船主名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiNm())
            {
                e.Cancel = true;
                this.txtFunanushiNm.SelectAll();
            }
        }

        /// <summary>
        /// 船主カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiKanaNm())
            {
                e.Cancel = true;
                this.txtFunanushiKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 略称船主名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRyFunanushiNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidRyFunanushiNm())
            {
                e.Cancel = true;
                this.txtRyFunanushiNm.SelectAll();
            }
        }

        /// <summary>
        /// 郵便番号(上3桁)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango1())
            {
                e.Cancel = true;
                this.txtYubinBango1.SelectAll();
            }
        }

        /// <summary>
        /// 郵便番号(下4桁)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango2())
            {
                e.Cancel = true;
                this.txtYubinBango2.SelectAll();
            }
        }

        /// <summary>
        /// 住所１の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho1())
            {
                e.Cancel = true;
                this.txtJusho1.SelectAll();
            }
        }

        /// <summary>
        /// 住所２の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho2())
            {
                e.Cancel = true;
                this.txtJusho2.SelectAll();
            }
        }

        /// <summary>
        /// 電話番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTel_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTel())
            {
                e.Cancel = true;
                this.txtTel.SelectAll();
            }
        }

        /// <summary>
        /// FAX番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFax_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFax())
            {
                e.Cancel = true;
                this.txtFax.SelectAll();
            }
        }

        /// <summary>
        /// 担当者コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoshaCd())
            {
                e.Cancel = true;
                this.txtTantoshaCd.SelectAll();
            }
        }

        /// <summary>
        /// 単価取得方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTankaShutokuHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTankaShutokuHoho())
            {
                e.Cancel = true;
                this.txtTankaShutokuHoho.SelectAll();
            }
        }

        /// <summary>
        /// 金額端数処理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKingakuHasuShori_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKingakuHasuShori())
            {
                e.Cancel = true;
                this.txtKingakuHasuShori.SelectAll();
            }
        }

        /// <summary>
        /// 消費税入力方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiNyuryokuHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiNyuryokuHoho())
            {
                e.Cancel = true;
                this.txtShohizeiNyuryokuHoho.SelectAll();
            }
        }

        /// <summary>
        /// 締日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShimebi_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShimebi())
            {
                e.Cancel = true;
                this.txtShimebi.SelectAll();
            }
        }

        /// <summary>
        /// 請求書形式の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeikyushoKeishiki_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeikyushoKeishiki())
            {
                e.Cancel = true;
                this.txtSeikyushoKeishiki.SelectAll();
            }
        }

        /// <summary>
        /// 消費税端数処理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiHasuShori_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiHasuShori())
            {
                e.Cancel = true;
                this.txtShohizeiHasuShori.SelectAll();
            }
        }

        /// <summary>
        /// 消費税転嫁方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiTenkaHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiTenkaHoho())
            {
                e.Cancel = true;
                this.txtShohizeiTenkaHoho.SelectAll();
            }
        }

        /// <summary>
        /// 回収月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKaishuTsuki_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKaishuTsuki())
            {
                e.Cancel = true;
                this.txtKaishuTsuki.SelectAll();
            }
        }

        /// <summary>
        /// 回収日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKaishuBi_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKaishuBi())
            {
                e.Cancel = true;
                this.txtKaishuBi.SelectAll();
            }
        }

        /// <summary>
        /// 生年月日(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJpYear_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtJpYear.Text))
            {
                this.txtJpYear.Text = "";
                this.txtJpMonth.Text = "";
                this.txtJpDay.Text = "";
                return;
            }
            if (!IsValid.IsYear(this.txtJpYear.Text, this.txtJpYear.MaxLength))
            {
                e.Cancel = true;
                this.txtJpYear.SelectAll();
            }
            else
            {
                this.txtJpYear.Text = Util.ToString(IsValid.SetYear(this.txtJpYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 生年月日(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJpMonth_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtJpMonth.Text))
            {
                this.txtJpYear.Text = "";
                this.txtJpMonth.Text = "";
                this.txtJpDay.Text = "";
                return;
            }
            if (!IsValid.IsMonth(this.txtJpMonth.Text, this.txtJpMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtJpMonth.SelectAll();
            }
            else
            {
                this.txtJpMonth.Text = Util.ToString(IsValid.SetMonth(this.txtJpMonth.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 生年月日(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJpDay_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtJpDay.Text))
            {
                this.txtJpYear.Text = "";
                this.txtJpMonth.Text = "";
                this.txtJpDay.Text = "";
                return;
            }
            if (!IsValid.IsDay(this.txtJpDay.Text, this.txtJpDay.MaxLength))
            {
                e.Cancel = true;
                this.txtJpDay.SelectAll();
            }
            else
            {
                this.txtJpDay.Text = Util.ToString(IsValid.SetDay(this.txtJpDay.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 加入日(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanyuYear_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtKanyuYear.Text))
            {
                this.txtKanyuYear.Text = "";
                this.txtKanyuMonth.Text = "";
                this.txtKanyuDay.Text = "";
                return;
            }
            if (!IsValid.IsYear(this.txtKanyuYear.Text, this.txtKanyuYear.MaxLength))
            {
                e.Cancel = true;
                this.txtKanyuYear.SelectAll();
            }
            else
            {
                this.txtKanyuYear.Text = Util.ToString(IsValid.SetYear(this.txtKanyuYear.Text));
                CheckKanyu();
                SetKanyu();
            }
        }

        /// <summary>
        /// 加入日(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanyuMonth_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtKanyuMonth.Text))
            {
                this.txtKanyuYear.Text = "";
                this.txtKanyuMonth.Text = "";
                this.txtKanyuDay.Text = "";
                return;
            }
            if (!IsValid.IsMonth(this.txtKanyuMonth.Text, this.txtKanyuMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtKanyuMonth.SelectAll();
            }
            else
            {
                this.txtKanyuMonth.Text = Util.ToString(IsValid.SetMonth(this.txtKanyuMonth.Text));
                CheckKanyu();
                SetKanyu();
            }
        }

        /// <summary>
        /// 加入日(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanyuDay_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtKanyuDay.Text))
            {
                this.txtKanyuYear.Text = "";
                this.txtKanyuMonth.Text = "";
                this.txtKanyuDay.Text = "";
                return;
            }
            if (!IsValid.IsDay(this.txtKanyuDay.Text, this.txtKanyuDay.MaxLength))
            {
                e.Cancel = true;
                this.txtKanyuDay.SelectAll();
            }
            else
            {
                this.txtKanyuDay.Text = Util.ToString(IsValid.SetDay(this.txtKanyuDay.Text));
                CheckKanyu();
                SetKanyu();
            }
        }

        /// <summary>
        /// 脱退日(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDattaiYear_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtDattaiYear.Text))
            {
                this.txtDattaiYear.Text = "";
                this.txtDattaiMonth.Text = "";
                this.txtDattaiDay.Text = "";
                return;
            }
            if (!IsValid.IsYear(this.txtDattaiYear.Text, this.txtDattaiYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDattaiYear.SelectAll();
            }
            else
            {
                this.txtDattaiYear.Text = Util.ToString(IsValid.SetYear(this.txtDattaiYear.Text));
                CheckDattai();
                SetDattai();
            }
        }

        /// <summary>
        /// 脱退日(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDattaiMonth_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtDattaiMonth.Text))
            {
                this.txtDattaiYear.Text = "";
                this.txtDattaiMonth.Text = "";
                this.txtDattaiDay.Text = "";
                return;
            }
            if (!IsValid.IsMonth(this.txtDattaiMonth.Text, this.txtDattaiMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtDattaiMonth.SelectAll();
            }
            else
            {
                this.txtDattaiMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDattaiMonth.Text));
                CheckDattai();
                SetDattai();
            }
        }

        /// <summary>
        /// 脱退日(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDattaiDay_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtDattaiDay.Text))
            {
                this.txtDattaiYear.Text = "";
                this.txtDattaiMonth.Text = "";
                this.txtDattaiDay.Text = "";
                return;
            }
            if (!IsValid.IsDay(this.txtDattaiDay.Text, this.txtDattaiDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDattaiDay.SelectAll();
            }
            else
            {
                this.txtDattaiDay.Text = Util.ToString(IsValid.SetDay(this.txtDattaiDay.Text));
                CheckDattai();
                SetDattai();
            }
        }

        /// <summary>
        /// 脱退理由の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDattaiRiyu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDattaiRiyu())
            {
                e.Cancel = true;
                this.txtDattaiRiyu.SelectAll();
            }
        }

        /// <summary>
        /// 一覧表示の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txthyoji_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidhyoji())
            {
                e.Cancel = true;
                this.txthyoji.SelectAll();
            }
        }

        /// <summary>
        /// 船名コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuneNmCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFuneNmCd())
            {
                e.Cancel = true;
                this.txtFuneNmCd.SelectAll();
            }
        }

        /// <summary>
        /// 漁法コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyohoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyohoCd())
            {
                e.Cancel = true;
                this.txtGyohoCd.SelectAll();
            }
        }

        /// <summary>
        /// 地区コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChikuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidChikuCd())
            {
                e.Cancel = true;
                this.txtChikuCd.SelectAll();
            }
        }

        /// <summary>
        /// 組合手数料率の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKumiaiTesuryoRitsu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKumiaiTesuryoRitsu())
            {
                e.Cancel = true;
                this.txtKumiaiTesuryoRitsu.SelectAll();
            }
        }

        /// <summary>
        /// 支払区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiharaiKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiharaiKubun())
            {
                e.Cancel = true;
                this.txtShiharaiKubun.SelectAll();
            }
        }

        /// <summary>
        /// 支所区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShishoKubun())
            {
                e.Cancel = true;
                this.txtShishoKubun.SelectAll();
            }
        }

        /// <summary>
        /// 正準区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeijunKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeijunKubun())
            {
                e.Cancel = true;
                this.txtSeijunKubun.SelectAll();
            }
        }

        /// <summary>
        /// 振込先割合(普通)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFutsuRitsu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFutsuRitsu())
            {
                e.Cancel = true;
                this.txtFutsuRitsu.SelectAll();
            }
        }

        /// <summary>
        /// 振込先割合(その他)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSonotaRitsu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSonotaRitsu())
            {
                e.Cancel = true;
                this.txtSonotaRitsu.SelectAll();
            }
        }

        /// <summary>
        /// 振込先割合(パヤオ)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPayaoRitsu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidPayaoRitsu())
            {
                e.Cancel = true;
                this.txtPayaoRitsu.SelectAll();
            }
        }

        /// <summary>
        /// 普通口座番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFutsuKozaBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaBango(this.txtFutsuKozaBango.Text))
            {
                e.Cancel = true;
                this.txtFutsuKozaBango.SelectAll();
            }
        }

        /// <summary>
        /// 普通口座区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFutsuKozaKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaKubun(this.txtFutsuKozaKubun.Text, 1))
            {
                e.Cancel = true;
                this.txtFutsuKozaKubun.SelectAll();
            }
        }

        /// <summary>
        /// 積立率の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTsumitateRitsu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTsumitateRitsu(this.txtTsumitateRitsu.Text, 2))
            {
                e.Cancel = true;
                this.txtTsumitateRitsu.SelectAll();
            }
        }

        /// <summary>
        /// 積立口座番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTsumitateKozaBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaBango(this.txtTsumitateKozaBango.Text))
            {
                e.Cancel = true;
                this.txtTsumitateKozaBango.SelectAll();
            }
        }

        /// <summary>
        /// 積立口座区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTsumitateKozaKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaKubun(this.txtTsumitateKozaKubun.Text, 2))
            {
                e.Cancel = true;
                this.txtTsumitateKozaKubun.SelectAll();
            }
        }

        /// <summary>
        /// 積立率(預り金)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTsumitateRitsuAzukari_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTsumitateRitsu(this.txtTsumitateRitsuAzukari.Text, 3))
            {
                e.Cancel = true;
                this.txtTsumitateRitsuAzukari.SelectAll();
            }
        }

        /// <summary>
        /// 口座番号(預り金)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKozaBangoAzukari_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaBango(this.txtKozaBangoAzukari.Text))
            {
                e.Cancel = true;
                this.txtKozaBangoAzukari.SelectAll();
            }
        }

        /// <summary>
        /// 口座区分(預り金)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKozaKubunAzukari_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaKubun(this.txtKozaKubunAzukari.Text, 3))
            {
                e.Cancel = true;
                this.txtKozaKubunAzukari.SelectAll();
            }
        }

        /// <summary>
        /// 積立率(本人)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTsumitateRitsuHonnin_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTsumitateRitsu(this.txtTsumitateRitsuHonnin.Text, 4))
            {
                e.Cancel = true;
                this.txtTsumitateRitsuHonnin.SelectAll();
            }
        }

        /// <summary>
        /// 口座番号(本人)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKozaBangoHonnin_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaBango(this.txtKozaBangoHonnin.Text))
            {
                e.Cancel = true;
                this.txtKozaBangoHonnin.SelectAll();
            }
        }

        /// <summary>
        /// 口座区分(本人)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKozaKubunHonnin_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaKubun(this.txtKozaKubunHonnin.Text, 4))
            {
                e.Cancel = true;
                this.txtKozaKubunHonnin.SelectAll();
            }
        }

        /// <summary>
        /// マイナンバーの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMyNumber_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMyNumber.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMyNumber.SelectAll();
                e.Cancel = true;
                return;
            }

            if (!IsValidMyNumber())
            {
                e.Cancel = true;
                this.txtMyNumber.SelectAll();
            }
        }

        private void txtFutsuKozaKubun_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //int tbIdx = this.tbcKoza.SelectedIndex;
                //if (tbIdx + 1 <= this.tbcKoza.TabCount)
                //{
                //    this.tbcKoza.SelectedIndex = tbIdx + 1;
                //    this.ActiveControl = this.txtTsumitateRitsu;
                //    this.txtTsumitateRitsu.Focus();
                //}
            }
        }

        private void txtTsumitateKozaKubun_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //int tbIdx = this.tbcKoza.SelectedIndex;
                //if (tbIdx + 1 <= this.tbcKoza.TabCount)
                //{
                //    this.tbcKoza.SelectedIndex = tbIdx + 1;
                //    this.ActiveControl = this.txtTsumitateRitsuAzukari;
                //    this.txtTsumitateRitsuAzukari.Focus();
                //}
            }
        }

        private void txtKozaKubunAzukari_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //int tbIdx = this.tbcKoza.SelectedIndex;
                //if (tbIdx + 1 <= this.tbcKoza.TabCount)
                //{
                //    this.tbcKoza.SelectedIndex = tbIdx + 1;
                //    this.ActiveControl = this.txtTsumitateRitsuHonnin;
                //    this.txtTsumitateRitsuHonnin.Focus();
                //}
            }
        }

        private void txtKozaKubunHonnin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 船主コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtFunanushiCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 4バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtFunanushiCd.Text, this.txtFunanushiCd.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD");
            DataTable dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("TORIHIKISAKI_CD",
                    "TB_HN_TORIHIKISAKI_JOHO", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                Msg.Error("既に存在する仕入先コードと重複しています。");
                return false;
            }

            // OKの場合、請求先コードに入力値をコピーする
            this.txtSeikyusakiCd.Text = this.txtFunanushiCd.Text;

            return true;
        }
        /// <summary>
        /// 船主コードの存在チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiCd(string Code)
        {
            DataTable dtToriSk;
            DbParamCollection dpc;
            StringBuilder where;
            StringBuilder Sql;

            // 既に存在するコードを削除しようとした場合はエラーとする（セリデータ）
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
            where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND SENSHU_CD = @TORIHIKISAKI_CD");
            dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("SENSHU_CD",
                    "TB_HN_SHIKIRI_DATA", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                //Msg.Error("既に存在する仕入先コードと重複しています。");
                return false;
            }

            // 既に存在するコードを削除しようとした場合はエラーとする（購買データ）
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
            where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND TOKUISAKI_CD = @TORIHIKISAKI_CD");
            dtToriSk = new DataTable();
            dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("TOKUISAKI_CD",
                    "TB_HN_TORIHIKI_DENPYO", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                //Msg.Error("既に存在する仕入先コードと重複しています。");
                return false;
            }

            // 既に存在するコードを削除しようとした場合はエラーとする（購買データ）
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
            Sql = new StringBuilder();
            Sql.Append(" SELECT A.KANJO_KAMOKU_CD FROM TB_ZM_SHIWAKE_MEISAI A ");
            Sql.Append(" INNER JOIN TB_ZM_KANJO_KAMOKU B ");
            Sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            Sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append(" B.HOJO_SHIYO_KUBUN = 2 AND ");
            Sql.Append(" A.HOJO_KAMOKU_CD = @TORIHIKISAKI_CD ");
            DataTable dtShiwakeMei = this.Dba.GetDataTableFromSqlWithParams(Sql.ToString(), dpc);
            if (dtShiwakeMei.Rows.Count > 0)
            {
                //Msg.Error("既に存在する仕入先コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 正式船主名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtFunanushiNm.Text, this.txtFunanushiNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 船主カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtFunanushiKanaNm.Text, this.txtFunanushiKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 略称船主名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidRyFunanushiNm()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtRyFunanushiNm.Text, this.txtRyFunanushiNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(上3桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango1()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(下4桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango2()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所１の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho1()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho1.Text, this.txtJusho1.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所２の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho2()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho2.Text, this.txtJusho2.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 電話番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTel()
        {
            //// 15バイトを超えていたらエラー
            //if (!ValChk.IsWithinLength(this.txtTel.Text, this.txtTel.MaxLength))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}
            if (!string.IsNullOrEmpty(this.txtTel.Text))
            {
                if (!IsPhoneNumber(this.txtTel.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// FAX番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFax()
        {
            //// 15バイトを超えていたらエラー
            //if (!ValChk.IsWithinLength(this.txtFax.Text, this.txtFax.MaxLength))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}
            if (!string.IsNullOrEmpty(this.txtFax.Text))
            {
                if (!IsPhoneNumber(this.txtFax.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsPhoneNumber(string number)
        {
            return Regex.IsMatch(number, @"^0\d{1,4}-\d{1,4}-\d{4}$");
            //return Regex.IsMatch(number, @"\A0\d{1,4}-\d{1,4}-\d{4}\z");
        }

        /// <summary>
        /// 担当者コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTantoshaCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                this.txtTantoshaCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTantoshaCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtTantoshaCd.Text))
            {
                this.lblTantoshaNm.Text = string.Empty;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "", this.txtTantoshaCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblTantoshaNm.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 単価取得方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTankaShutokuHoho()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtTankaShutokuHoho.Text))
            {
                this.txtTankaShutokuHoho.Text = "0";
            }

            // 0,1,2のみ入力を許可
            if (!ValChk.IsNumber(this.txtTankaShutokuHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtTankaShutokuHoho.Text);
            if (intval < 0 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 金額端数処理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKingakuHasuShori()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtKingakuHasuShori.Text))
            {
                this.txtKingakuHasuShori.Text = "1";
            }

            // 1,2,3のみ入力を許可
            if (!ValChk.IsNumber(this.txtKingakuHasuShori.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtKingakuHasuShori.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税入力方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiNyuryokuHoho()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShohizeiNyuryokuHoho.Text))
            {
                this.txtShohizeiNyuryokuHoho.Text = "0";
            }

            // 0,1,2,3のみ入力を許可
            if (!ValChk.IsNumber(this.txtShohizeiNyuryokuHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 登録済みの値のみ許可
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            this.lblShzNrkHohoNm.Text = name;
            return true;
        }

        /// <summary>
        /// 締日の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShimebi()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShimebi.Text))
            {
                this.txtShimebi.Text = "0";
            }

            // 1～28,99のみ許可
            if (!ValChk.IsNumber(this.txtShimebi.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShimebi.Text);
            if (!((intval >= 1 && intval <= 28) || intval == 99))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 請求書形式の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeikyushoKeishiki()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtSeikyushoKeishiki.Text))
            {
                this.txtSeikyushoKeishiki.Text = "1";
            }

            // 1,2のみ入力許可
            if (!ValChk.IsNumber(this.txtSeikyushoKeishiki.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtSeikyushoKeishiki.Text);
            if (intval < 1 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税端数処理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiHasuShori()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtShohizeiHasuShori.Text))
            {
                this.txtShohizeiHasuShori.Text = "1";
            }

            // 1,2,3のみ入力許可
            if (!ValChk.IsNumber(this.txtShohizeiHasuShori.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShohizeiHasuShori.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税転嫁方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiTenkaHoho()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtShohizeiTenkaHoho.Text))
            {
                this.txtShohizeiTenkaHoho.Text = "1";
            }

            // 1,2,3のみ入力許可
            if (!ValChk.IsNumber(this.txtShohizeiTenkaHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShohizeiTenkaHoho.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 回収月の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKaishuTsuki()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKaishuTsuki.Text))
            {
                this.txtKaishuTsuki.Text = "0";
            }

            // 数字のみ入力許可
            if (!ValChk.IsNumber(this.txtKaishuTsuki.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 回収日の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKaishuBi()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKaishuBi.Text))
            {
                this.txtKaishuBi.Text = "0";
            }

            // 1～28,99のみ許可
            if (!ValChk.IsNumber(this.txtKaishuBi.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtKaishuBi.Text);
            if (!((intval >= 1 && intval <= 28) || intval == 99))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblJpGengo.Text = arrJpDate[0];
            this.txtJpYear.Text = arrJpDate[2];
            this.txtJpMonth.Text = arrJpDate[3];
            this.txtJpDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            this.txtJpYear.Text = Util.ToString(IsValid.SetYear(this.txtJpYear.Text));
            this.txtJpMonth.Text = Util.ToString(IsValid.SetMonth(this.txtJpMonth.Text));
            this.txtJpDay.Text = Util.ToString(IsValid.SetDay(this.txtJpDay.Text));

            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblJpGengo.Text, this.txtJpYear.Text,
                this.txtJpMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtJpDay.Text) > lastDayInMonth)
            {
                this.txtJpDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblJpGengo.Text, this.txtJpYear.Text,
                this.txtJpMonth.Text, this.txtJpDay.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckKanyu()
        {
            this.txtKanyuYear.Text = Util.ToString(IsValid.SetYear(this.txtKanyuYear.Text));
            this.txtKanyuMonth.Text = Util.ToString(IsValid.SetMonth(this.txtKanyuMonth.Text));
            this.txtKanyuDay.Text = Util.ToString(IsValid.SetDay(this.txtKanyuDay.Text));

            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblKanyuGengo.Text, this.txtKanyuYear.Text,
                this.txtKanyuMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtKanyuDay.Text) > lastDayInMonth)
            {
                this.txtKanyuDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetKanyu()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetKanyu(Util.FixJpDate(this.lblKanyuGengo.Text, this.txtKanyuYear.Text,
                this.txtKanyuMonth.Text, this.txtKanyuDay.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDattai()
        {
            this.txtDattaiYear.Text = Util.ToString(IsValid.SetYear(this.txtDattaiYear.Text));
            this.txtDattaiMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDattaiMonth.Text));
            this.txtDattaiDay.Text = Util.ToString(IsValid.SetDay(this.txtDattaiDay.Text));

            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDattaiGengo.Text, this.txtDattaiYear.Text,
                this.txtDattaiMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDattaiDay.Text) > lastDayInMonth)
            {
                this.txtDattaiDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDattai()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetDattai(Util.FixJpDate(this.lblDattaiGengo.Text, this.txtDattaiYear.Text,
                this.txtDattaiMonth.Text, this.txtDattaiDay.Text, this.Dba));
        }

        /// <summary>
        /// 脱退理由の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDattaiRiyu()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtDattaiRiyu.Text, this.txtDattaiRiyu.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 一覧表示の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidhyoji()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txthyoji.Text))
            {
                this.txthyoji.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txthyoji.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txthyoji.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 船名コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFuneNmCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtFuneNmCd.Text))
            {
                this.txtFuneNmCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtFuneNmCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            string name = this.Dba.GetName(this.UInfo, "TB_HN_FUNE_NM_MST", "", this.txtFuneNmCd.Text);
            // TODO:船名マスタに存在しなくてもOKとする(現行通り)
            //if (ValChk.IsEmpty(name))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}

            this.lblFuneNmCdSub.Text = name;

            return true;
        }

        /// <summary>
        /// 漁法コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyohoCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtGyohoCd.Text))
            {
                this.txtGyohoCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyohoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtGyohoCd.Text))
            {
                this.lblGyohoCdSub.Text = string.Empty;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "TB_HN_GYOHO_MST", "", this.txtGyohoCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblGyohoCdSub.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 地区コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidChikuCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtChikuCd.Text))
            {
                this.txtChikuCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtChikuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtChikuCd.Text))
            {
                this.lblChikuCdSub.Text = string.Empty;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "TB_HN_CHIKU_MST", "", this.txtChikuCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblChikuCdSub.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 組合手数料率の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKumiaiTesuryoRitsu()
        {
            // 未入力は0.00とする
            if (ValChk.IsEmpty(this.txtKumiaiTesuryoRitsu.Text))
            {
                this.txtKumiaiTesuryoRitsu.Text = "0.00";
            }
            // 数値チェック
            if (!ValChk.IsDecNumWithinLength(this.txtKumiaiTesuryoRitsu.Text, 2, 2))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値のフォーマット
            this.txtKumiaiTesuryoRitsu.Text = Util.FormatNum(this.txtKumiaiTesuryoRitsu.Text, 2);

            return true;
        }

        /// <summary>
        /// 支払区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiharaiKubun()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShiharaiKubun.Text))
            {
                this.txtShiharaiKubun.Text = "0";
            }

            // 0,1,2のみ入力許可
            if (!ValChk.IsNumber(this.txtShiharaiKubun.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShiharaiKubun.Text);
            if (intval < 0 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 支所区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShishoKubun()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShishoKubun.Text))
            {
                this.txtShishoKubun.Text = "0";
            }

            // 0,1,2のみ入力許可
            if (!ValChk.IsNumber(this.txtShishoKubun.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShishoKubun.Text);
            if (intval < 0 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 正準区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeijunKubun()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtSeijunKubun.Text))
            {
                this.txtShishoKubun.Text = "0";
            }

            // 0,1,2,3のみ入力許可
            if (!ValChk.IsNumber(this.txtSeijunKubun.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtSeijunKubun.Text);
            if (intval < 0 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 振込先割合(普通)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFutsuRitsu()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtFutsuRitsu.Text))
            {
                this.txtFutsuRitsu.Text = "0";
            }

            // 0,1,2のみ入力許可
            if (!ValChk.IsNumber(this.txtFutsuRitsu.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtFutsuRitsu.Text);
            if (intval < 0 || intval > 100)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 振込先割合(その他)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSonotaRitsu()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtSonotaRitsu.Text))
            {
                this.txtSonotaRitsu.Text = "0";
            }

            // 0,1,2のみ入力許可
            if (!ValChk.IsNumber(this.txtSonotaRitsu.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtSonotaRitsu.Text);
            if (intval < 0 || intval > 100)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 振込先割合(パヤオ)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidPayaoRitsu()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtPayaoRitsu.Text))
            {
                this.txtPayaoRitsu.Text = "0";
            }

            // 0,1,2のみ入力許可
            if (!ValChk.IsNumber(this.txtPayaoRitsu.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtPayaoRitsu.Text);
            if (intval < 0 || intval > 100)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 積立率の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTsumitateRitsu(string tumitateRitsu, int index)
        {
            // 未入力は0.00とする
            if (ValChk.IsEmpty(tumitateRitsu))
            {
                if (index == 2)
                {
                    tumitateRitsu = "0.00";
                }
                else if (index == 3)
                {
                    tumitateRitsu = "0.00";
                }
                else
                {
                    tumitateRitsu = "0.00";
                }
            }
            // 数値チェック
            else if (!ValChk.IsDecNumWithinLength(tumitateRitsu, 2, 2))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値のフォーマット
            if (index == 2)
            {
                this.txtTsumitateRitsu.Text = Util.FormatNum(tumitateRitsu, 2);
            }
            else if (index == 3)
            {
                this.txtTsumitateRitsuAzukari.Text = Util.FormatNum(tumitateRitsu, 2);
            }
            else
            {
                this.txtTsumitateRitsuHonnin.Text = Util.FormatNum(tumitateRitsu, 2);
            }

            return true;
        }

        /// <summary>
        /// 口座番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKozaBango(string kozaBango)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(kozaBango))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 口座区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKozaKubun(string kozaKubunStr, int index)
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(kozaKubunStr))
            {
                if (index == 1)
                {
                    this.txtFutsuKozaKubun.Text = "0";
                }
                else if (index == 2)
                {
                    this.txtTsumitateKozaKubun.Text = "0";
                }
                else if (index == 3)
                {
                    this.txtKozaKubunAzukari.Text = "0";
                }
                else
                {
                    this.txtKozaKubunHonnin.Text = "0";
                }
            }

            // 1,2のみ入力許可
            if (!ValChk.IsNumber(kozaKubunStr))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            int kozaKubun = Util.ToInt(kozaKubunStr);
            if (kozaKubun < 0 || kozaKubun > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// マイナンバーの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMyNumber()
        {
            // 入力サイズ
            if (!ValChk.IsWithinLength(txtMyNumber.Text, txtMyNumber.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 船主コードのチェック
                if (!IsValidFunanushiCd())
                {
                    this.txtFunanushiCd.Focus();
                    return false;
                }
            }

            // 正式船主名のチェック
            if (!IsValidFunanushiNm())
            {
                this.txtFunanushiNm.Focus();
                return false;
            }

            // 船主カナ名のチェック
            if (!IsValidFunanushiKanaNm())
            {
                this.txtFunanushiKanaNm.Focus();
                return false;
            }

            // 略称船主名のチェック
            if (!IsValidRyFunanushiNm())
            {
                this.txtRyFunanushiNm.Focus();
                return false;
            }

            // 郵便番号１のチェック
            if (!IsValidYubinBango1())
            {
                this.txtYubinBango1.Focus();
                return false;
            }

            // 郵便番号２のチェック
            if (!IsValidYubinBango2())
            {
                this.txtYubinBango2.Focus();
                return false;
            }

            // 郵便番号２だけの入力はエラー
            if (ValChk.IsEmpty(this.txtYubinBango1.Text) &&
                !ValChk.IsEmpty(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtYubinBango1.Focus();
                return false;
            }

            // 住所１のチェック
            if (!IsValidJusho1())
            {
                this.txtJusho1.Focus();
                return false;
            }

            // 住所２のチェック
            if (!IsValidJusho2())
            {
                this.txtJusho2.Focus();
                return false;
            }

            // 電話番号のチェック
            if (!IsValidTel())
            {
                this.txtTel.Focus();
                return false;
            }

            // FAX番号のチェック
            if (!IsValidFax())
            {
                this.txtFax.Focus();
                return false;
            }

            // 担当者コードのチェック
            if (!IsValidTantoshaCd())
            {
                this.txtTantoshaCd.Focus();
                return false;
            }

            // 単価取得方法のチェック
            if (!IsValidTankaShutokuHoho())
            {
                this.txtTankaShutokuHoho.Focus();
                return false;
            }

            // 金額端数処理のチェック
            if (!IsValidKingakuHasuShori())
            {
                this.txtKingakuHasuShori.Focus();
                return false;
            }

            // 消費税入力方法のチェック
            if (!IsValidShohizeiNyuryokuHoho())
            {
                this.txtShohizeiNyuryokuHoho.Focus();
                return false;
            }

            // 締日のチェック
            if (!IsValidShimebi())
            {
                this.txtShimebi.Focus();
                return false;
            }

            // 請求書形式のチェック
            if (!IsValidSeikyushoKeishiki())
            {
                this.txtSeikyushoKeishiki.Focus();
                return false;
            }

            // 消費税端数処理のチェック
            if (!IsValidShohizeiHasuShori())
            {
                this.txtShohizeiHasuShori.Focus();
                return false;
            }

            // 消費税転嫁方法のチェック
            if (!IsValidShohizeiTenkaHoho())
            {
                this.txtShohizeiTenkaHoho.Focus();
                return false;
            }

            // 回収月のチェック
            if (!IsValidKaishuTsuki())
            {
                this.txtKaishuTsuki.Focus();
                return false;
            }

            // 回収日のチェック
            if (!IsValidKaishuBi())
            {
                this.txtKaishuBi.Focus();
                return false;
            }

            if (!string.IsNullOrEmpty(this.txtJpYear.Text) &&
                !string.IsNullOrEmpty(this.txtJpMonth.Text) &&
                !string.IsNullOrEmpty(this.txtJpDay.Text))
            {

                // 生年月日(年)の入力チェック
                if (!IsValid.IsYear(this.txtJpYear.Text, this.txtJpYear.MaxLength))
                {
                    this.txtJpYear.Focus();
                    this.txtJpYear.SelectAll();
                    return false;
                }
                // 生年月日(月)の入力チェック
                if (!IsValid.IsMonth(this.txtJpMonth.Text, this.txtJpMonth.MaxLength))
                {
                    this.txtJpMonth.Focus();
                    this.txtJpMonth.SelectAll();
                    return false;
                }
                // 生年月日(日)の入力チェック
                if (!IsValid.IsDay(this.txtJpDay.Text, this.txtJpDay.MaxLength))
                {
                    this.txtJpDay.Focus();
                    this.txtJpDay.SelectAll();
                    return false;
                }
                // 年月日の月末入力チェック処理
                CheckJp();
                // 年月日の正しい和暦への変換処理
                SetJp();
            }
            if (!string.IsNullOrEmpty(this.txtKanyuYear.Text) &&
                !string.IsNullOrEmpty(this.txtKanyuMonth.Text) &&
                !string.IsNullOrEmpty(this.txtKanyuDay.Text))
            {
                // 加入日(年)の入力チェック
                if (!IsValid.IsYear(this.txtKanyuYear.Text, this.txtKanyuYear.MaxLength))
                {
                    this.txtKanyuYear.Focus();
                    this.txtKanyuYear.SelectAll();
                    return false;
                }
                // 加入日(月)の入力チェック
                if (!IsValid.IsMonth(this.txtKanyuMonth.Text, this.txtKanyuMonth.MaxLength))
                {
                    this.txtKanyuMonth.Focus();
                    this.txtKanyuMonth.SelectAll();
                    return false;
                }
                // 加入日(日)の入力チェック
                if (!IsValid.IsDay(this.txtKanyuDay.Text, this.txtKanyuDay.MaxLength))
                {
                    this.txtKanyuDay.Focus();
                    this.txtKanyuDay.SelectAll();
                    return false;
                }
                // 加入日の月末入力チェック処理
                CheckKanyu();
                // 加入日の正しい和暦への変換処理
                SetKanyu();
            }

            if (!string.IsNullOrEmpty(this.txtDattaiYear.Text) &&
                !string.IsNullOrEmpty(this.txtDattaiMonth.Text) &&
                !string.IsNullOrEmpty(this.txtDattaiDay.Text))
            {
                // 脱退日(年)の入力チェック
                if (!IsValid.IsYear(this.txtDattaiYear.Text, this.txtDattaiYear.MaxLength))
                {
                    this.txtDattaiYear.Focus();
                    this.txtDattaiYear.SelectAll();
                    return false;
                }
                // 脱退日(月)の入力チェック
                if (!IsValid.IsMonth(this.txtDattaiMonth.Text, this.txtDattaiMonth.MaxLength))
                {
                    this.txtDattaiMonth.Focus();
                    this.txtDattaiMonth.SelectAll();
                    return false;
                }
                // 脱退日(日)の入力チェック
                if (!IsValid.IsDay(this.txtDattaiDay.Text, this.txtDattaiDay.MaxLength))
                {
                    this.txtDattaiDay.Focus();
                    this.txtDattaiDay.SelectAll();
                    return false;
                }
                // 脱退日の月末入力チェック処理
                CheckDattai();
                // 脱退日の正しい和暦への変換処理
                SetDattai();
            }
            // 脱退理由のチェック
            if (!IsValidDattaiRiyu())
            {
                this.txtDattaiRiyu.Focus();
                return false;
            }
            // 一覧表示のチェック
            if (!IsValidhyoji())
            {
                this.txthyoji.Focus();
                return false;
            }

            // 船名コードのチェック
            if (!IsValidFuneNmCd())
            {
                this.txtFuneNmCd.Focus();
                return false;
            }

            // 漁法コードのチェック
            if (!IsValidGyohoCd())
            {
                this.txtGyohoCd.Focus();
                return false;
            }

            // 地区コードのチェック
            if (!IsValidChikuCd())
            {
                this.txtChikuCd.Focus();
                return false;
            }

            // 組合手数料率のチェック
            if (!IsValidKumiaiTesuryoRitsu())
            {
                this.txtKumiaiTesuryoRitsu.Focus();
                return false;
            }

            // 支払区分のチェック
            if (!IsValidShiharaiKubun())
            {
                this.txtShiharaiKubun.Focus();
                return false;
            }

            // 支所区分のチェック
            if (!IsValidShishoKubun())
            {
                this.txtShishoKubun.Focus();
                return false;
            }

            // 正準区分のチェック
            if (!IsValidSeijunKubun())
            {
                this.txtSeijunKubun.Focus();
                return false;
            }

            // 振込先割合(普通)のチェック
            if (!IsValidFutsuRitsu())
            {
                this.txtFutsuRitsu.Focus();
                return false;
            }

            // 振込先割合(その他)のチェック
            if (!IsValidSonotaRitsu())
            {
                this.txtSonotaRitsu.Focus();
                return false;
            }

            // 振込先割合(パヤオ)のチェック
            if (!IsValidPayaoRitsu())
            {
                this.txtPayaoRitsu.Focus();
                return false;
            }

            // 普通口座番号のチェック
            if (!IsValidKozaBango(this.txtFutsuKozaBango.Text))
            {
                this.txtFutsuKozaBango.Focus();
                return false;
            }

            // 普通口座区分のチェック
            if (!IsValidKozaKubun(this.txtFutsuKozaKubun.Text, 1))
            {
                this.txtFutsuKozaKubun.Focus();
                return false;
            }

            // 積立率のチェック
            if (!IsValidTsumitateRitsu(this.txtTsumitateRitsu.Text, 2))
            {
                this.txtTsumitateRitsu.Focus();
                return false;
            }

            // 積立口座番号のチェック
            if (!IsValidKozaBango(this.txtTsumitateKozaBango.Text))
            {
                this.txtTsumitateKozaBango.Focus();
                return false;
            }

            // 積立口座区分のチェック
            if (!IsValidKozaKubun(this.txtTsumitateKozaKubun.Text, 2))
            {
                this.txtTsumitateKozaKubun.Focus();
                return false;
            }

            // マイナンバーのチェック
            if (!IsValidMyNumber())
            {
                this.txtMyNumber.Focus();
                return false;
            }

            // 販売から起動された場合のみ実行
            if (MODE_HANBAI.Equals(this.Par2))
            {
                // 積立率(預り金)のチェック
                if (!IsValidTsumitateRitsu(this.txtTsumitateRitsuAzukari.Text, 3))
                {
                    this.txtTsumitateRitsuAzukari.Focus();
                    return false;
                }

                // 口座番号(預り金)のチェック
                if (!IsValidKozaBango(this.txtKozaBangoAzukari.Text))
                {
                    this.txtKozaBangoAzukari.Focus();
                    return false;
                }

                // 口座区分(預り金)のチェック
                if (!IsValidKozaKubun(this.txtKozaKubunAzukari.Text, 3))
                {
                    this.txtKozaKubunAzukari.Focus();
                    return false;
                }

                // 積立率(本人)のチェック
                if (!IsValidTsumitateRitsu(this.txtTsumitateRitsuHonnin.Text, 4))
                {
                    this.txtTsumitateRitsuHonnin.Focus();
                    return false;
                }

                // 口座番号(本人)のチェック
                if (!IsValidKozaBango(this.txtKozaBangoHonnin.Text))
                {
                    this.txtKozaBangoHonnin.Focus();
                    return false;
                }

                // 口座区分(本人)のチェック
                if (!IsValidKozaKubun(this.txtKozaKubunHonnin.Text, 4))
                {
                    this.txtKozaKubunHonnin.Focus();
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblJpGengo.Text = arrJpDate[0];
            this.txtJpYear.Text = arrJpDate[2];
            this.txtJpMonth.Text = arrJpDate[3];
            this.txtJpDay.Text = arrJpDate[4];
        }
        private void SetKanyu(string[] arrJpDate)
        {
            this.lblKanyuGengo.Text = arrJpDate[0];
            this.txtKanyuYear.Text = arrJpDate[2];
            this.txtKanyuMonth.Text = arrJpDate[3];
            this.txtKanyuDay.Text = arrJpDate[4];
        }
        private void SetDattai(string[] arrJpDate)
        {
            this.lblDattaiGengo.Text = arrJpDate[0];
            this.txtDattaiYear.Text = arrJpDate[2];
            this.txtDattaiMonth.Text = arrJpDate[3];
            this.txtDattaiDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 今ログインしている人のデータを取得
        /// </summary>
        /// <returns>roginUser</returns>
        private DataRow GetroginUserdetail()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.UInfo.UserCd);
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT ");
            sql.Append(" * ");
            sql.Append(" FROM ");
            sql.Append(" TB_CM_TANTOSHA ");
            sql.Append(" WHERE ");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            sql.Append(" TANTOSHA_CD = @TANTOSHA_CD ");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if (dt.Rows.Count == 0)
            {
                Msg.Info("ユーザ情報取得に失敗しました。");
            }

            return dt.Rows[0];
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装
            // 初期設定値の退避変数
            string ret = "";

            // 船主コードの初期値を取得
            // 船主の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            //StringBuilder where = new StringBuilder("TORIHIKISAKI_KUBUN1 = 1");
            //where.Append(" AND KAISHA_CD = @KAISHA_CD");
            //DataTable dtMaxFnns =
            //    this.Dba.GetDataTableByConditionWithParams("MAX(TORIHIKISAKI_CD) AS MAX_CD",
            //        "TB_HN_TORIHIKISAKI_JOHO", Util.ToString(where), dpc);
            // 取引先コードで登録されていない最小の番号を初期表示
            DataTable dtMaxFnns =
                this.Dba.GetDataTableByConditionWithParams("MIN(TORIHIKISAKI_CD + 1) AS MAX_CD",
                    "VI_HN_TORIHIKISAKI_JOHO",
                    "KAISHA_CD = @KAISHA_CD AND (TORIHIKISAKI_CD + 1) NOT IN (SELECT TORIHIKISAKI_CD FROM VI_HN_TORIHIKISAKI_JOHO WHERE KAISHA_CD = @KAISHA_CD)",
                    dpc);
            if (dtMaxFnns.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxFnns.Rows[0]["MAX_CD"]))
            {
                //this.txtFunanushiCd.Text = Util.ToString(Util.ToInt(dtMaxFnns.Rows[0]["MAX_CD"]) + 1);
                this.txtFunanushiCd.Text = Util.ToString(Util.ToInt(dtMaxFnns.Rows[0]["MAX_CD"]));
            }
            else
            {
                this.txtFunanushiCd.Text = "1001";
            }
            // 船主コードの値を請求先コードにコピー
            this.txtSeikyusakiCd.Text = this.txtFunanushiCd.Text;

            // 各項目の初期値を設定
            // 担当者コード0
            this.txtTantoshaCd.Text = "0";
            // 単価取得方法0
            //this.txtTankaShutokuHoho.Text = "0";
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "TankaShutokuHoho"));
            this.txtTankaShutokuHoho.Text = ret;

            // 金額端数処理2
            //this.txtKingakuHasuShori.Text = "2";
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "KingakuHasuShori"));
            this.txtKingakuHasuShori.Text = ret;

            // 消費税入力方法2
            //this.txtShohizeiNyuryokuHoho.Text = "2";
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiNyuryokuHoho"));
            this.txtShohizeiNyuryokuHoho.Text = ret;
            this.lblShzNrkHohoNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text);
            // 締日99
            this.txtShimebi.Text = "99";
            // 請求書発行1
            this.txtSeikyushoHakko.Text = "1";
            // 請求書形式2
            this.txtSeikyushoKeishiki.Text = "2";
            // 消費税端数処理2
            //this.txtShohizeiHasuShori.Text = "2";
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiHasuShori"));
            this.txtShohizeiHasuShori.Text = ret;
            // 消費税転嫁方法2
            //this.txtShohizeiTenkaHoho.Text = "2";
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiTenkaHoho"));
            this.txtShohizeiTenkaHoho.Text = ret;
            // 回収月0
            this.txtKaishuTsuki.Text = "0";
            // 回収日99
            this.txtKaishuBi.Text = "99";
            //// 生年月日(年)0
            //this.txtJpYear.Text = "0";
            //// 生年月日(月)0
            //this.txtJpMonth.Text = "0";
            //// 生年月日(日)0
            //this.txtJpDay.Text = "0";
            //// 加入日(年)0
            //this.txtKanyuYear.Text = "0";
            //// 加入日(月)0
            //this.txtKanyuMonth.Text = "0";
            //// 加入日(日)0
            //this.txtKanyuDay.Text = "0";
            //// 脱退日(年)0
            //this.txtDattaiYear.Text = "0";
            //// 脱退日(月)0
            //this.txtDattaiMonth.Text = "0";
            //// 脱退日(日)0
            //this.txtDattaiDay.Text = "0";

            // 生年月日(年)0
            this.txtJpYear.Text = "";
            // 生年月日(月)0
            this.txtJpMonth.Text = "";
            // 生年月日(日)0
            this.txtJpDay.Text = "";
            // 加入日(年)0
            this.txtKanyuYear.Text = "";
            // 加入日(月)0
            this.txtKanyuMonth.Text = "";
            // 加入日(日)0
            this.txtKanyuDay.Text = "";
            // 脱退日(年)0
            this.txtDattaiYear.Text = "";
            // 脱退日(月)0
            this.txtDattaiMonth.Text = "";
            // 脱退日(日)0
            this.txtDattaiDay.Text = "";

            // 一覧表示
            this.txthyoji.Text = "0";
            // 船主コード0
            this.txtFuneNmCd.Text = "0";
            // 漁法コード0
            this.txtGyohoCd.Text = "0";
            // 地区コード0
            this.txtChikuCd.Text = "0";
            // 組合手数料率0.00
            this.txtKumiaiTesuryoRitsu.Text = "0.00";
            // 支払区分0
            this.txtShiharaiKubun.Text = "0";
            // 支所区分0
            this.txtShishoKubun.Text = "0";
            // 正準区分0
            this.txtSeijunKubun.Text = "0";
            // 振込先割合(普通)0
            this.txtFutsuRitsu.Text = "0";
            // 振込先割合(その他)0
            this.txtSonotaRitsu.Text = "0";
            // 振込先割合(パヤオ)0
            this.txtPayaoRitsu.Text = "0";
            // 普通口座区分0
            this.txtFutsuKozaKubun.Text = "0";
            // 積立率0.00
            this.txtTsumitateRitsu.Text = "0.00";
            // 積立口座区分0
            this.txtTsumitateKozaKubun.Text = "0";
            // 積立率(預り金)0.00
            this.txtTsumitateRitsuAzukari.Text = "0.00";
            // 口座区分(預り金)0
            this.txtKozaKubunAzukari.Text = "0";
            // 積立率(本人)0.00
            this.txtTsumitateRitsuHonnin.Text = "0.00";
            // 口座区分(本人)0
            this.txtKozaKubunHonnin.Text = "0";

            // 請求先コード・請求書発行は入力不可
            this.lblSeikyusakiCd.Enabled = false;
            this.txtSeikyusakiCd.Enabled = false;
            this.lblSeikyushoHakko.Enabled = false;
            this.txtSeikyushoHakko.Enabled = false;

            // 正式船主名に初期フォーカス
            this.ActiveControl = this.txtFunanushiNm;
            this.txtFunanushiNm.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            //マイナンバー取得
            txtMyNumber.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 3, 0, this.Dba);
            if (!ValChk.IsEmpty(Util.ToString(txtMyNumber.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtMyNumber.ReadOnly = true;
                this.txtMyNumberHide.Visible = true;
            }

            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.TORIHIKISAKI_CD"); // 船主コード
            cols.Append(" ,A.SEIKYUSAKI_CD"); // 請求先コード
            cols.Append(" ,A.TANKA_SHUTOKU_HOHO"); // 単価取得方法
            cols.Append(" ,A.KINGAKU_HASU_SHORI"); //　金額端数処理
            cols.Append(" ,A.SHOHIZEI_NYURYOKU_HOHO"); // 消費税入力方法
            cols.Append(" ,B.TORIHIKISAKI_NM"); // 正式船主名
            cols.Append(" ,B.TORIHIKISAKI_KANA_NM"); // 船主カナ名
            cols.Append(" ,B.RYAKUSHO"); // 略称船主名
            cols.Append(" ,B.YUBIN_BANGO1"); // 郵便番号1
            cols.Append(" ,B.YUBIN_BANGO2"); // 郵便番号2
            cols.Append(" ,B.JUSHO1"); // 住所1
            cols.Append(" ,B.JUSHO2"); // 住所2
            cols.Append(" ,B.DENWA_BANGO"); // 電話番号
            cols.Append(" ,B.FAX_BANGO"); // FAX番号
            cols.Append(" ,B.TANTOSHA_CD"); // 担当者コード
            cols.Append(" ,B.SIMEBI"); // 締日
            cols.Append(" ,B.SEIKYUSHO_HAKKO"); // 請求書発行
            cols.Append(" ,B.SEIKYUSHO_KEISHIKI"); // 請求書形式
            cols.Append(" ,B.SHOHIZEI_HASU_SHORI"); // 消費税端数処理
            cols.Append(" ,B.SHOHIZEI_TENKA_HOHO"); // 消費税転嫁方法
            cols.Append(" ,B.KAISHU_TSUKI"); // 回収月
            cols.Append(" ,B.KAISHU_BI"); // 回収日
            cols.Append(" ,B.SEINENGAPPI"); // 生年月日
            cols.Append(" ,B.KANYUBI"); // 加入日
            cols.Append(" ,B.DATTAIBI"); // 脱退日
            cols.Append(" ,B.DATTAI_RIYU"); // 脱退理由
            cols.Append(" ,B.FUNE_NM_CD"); // 船主コード
            cols.Append(" ,B.GYOHO_CD"); // 漁法コード
            cols.Append(" ,B.CHIKU_CD"); // 地区コード
            cols.Append(" ,B.KUMIAI_TESURYO_RITSU"); // 組合手数料率
            cols.Append(" ,B.SHIHARAI_KUBUN"); // 支払区分
            cols.Append(" ,B.SHISHO_KUBUN"); // 支所区分
            cols.Append(" ,B.SEIJUN_KUBUN"); // 正準区分
            cols.Append(" ,B.FUTSU_WARIAI"); // 普通率
            cols.Append(" ,B.SONOTA_WARIAI"); // その他率
            cols.Append(" ,B.PAYAO_WARIAI"); // パヤオ率
            cols.Append(" ,B.FUTSU_KOZA_BANGO"); // 普通口座番号
            cols.Append(" ,B.FUTSU_KOZA_KUBUN"); // 普通口座区分
            cols.Append(" ,B.TSUMITATE_RITSU"); // 積立率
            cols.Append(" ,B.TSUMITATE_KOZA_BANGO"); // 積立口座番号
            cols.Append(" ,B.TSUMITATE_KOZA_KUBUN"); // 積立口座区分
            cols.Append(" ,B.AZUKARIKIN_TSUMITATE_RITSU"); // 積立率(預り金)
            cols.Append(" ,B.AZUKARIKIN_KOZA_BANGO"); // 口座番号(預り金)
            cols.Append(" ,B.AZUKARIKIN_KOZA_KUBUN"); // 口座区分(預り金)
            cols.Append(" ,B.HONNIN_TSUMITATE_RITSU"); // 積立率(本人)
            cols.Append(" ,B.HONNIN_KOZA_BANGO"); // 口座番号(本人)
            cols.Append(" ,B.HONNIN_KOZA_KUBUN"); // 口座区分(本人)
            cols.Append(" ,B.HYOJI_FLG"); // 表示フラグ

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_TORIHIKISAKI_JOHO AS A");
            from.Append(" INNER JOIN");
            from.Append(" TB_CM_TORIHIKISAKI AS B");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            from.Append(" AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtFunanushiCd.Text = Util.ToString(drDispData["TORIHIKISAKI_CD"]); // 船主コード
            this.txtFunanushiNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]); // 正式船主名
            this.txtFunanushiKanaNm.Text = Util.ToString(drDispData["TORIHIKISAKI_KANA_NM"]); // 船主カナ名
            this.txtRyFunanushiNm.Text = Util.ToString(drDispData["RYAKUSHO"]); // 略称船主名
            this.txtYubinBango1.Text = Util.ToString(drDispData["YUBIN_BANGO1"]); // 郵便番号1
            this.txtYubinBango2.Text = Util.ToString(drDispData["YUBIN_BANGO2"]); // 郵便番号2
            this.txtJusho1.Text = Util.ToString(drDispData["JUSHO1"]); // 住所1
            this.txtJusho2.Text = Util.ToString(drDispData["JUSHO2"]); // 住所2
            this.txtTel.Text = Util.ToString(drDispData["DENWA_BANGO"]); // 電話番号
            this.txtFax.Text = Util.ToString(drDispData["FAX_BANGO"]); // FAX番号
            this.txtTantoshaCd.Text = Util.ToString(drDispData["TANTOSHA_CD"]); // 担当者コード
            if (this.txtTantoshaCd.Text != "")
            {
                this.lblTantoshaNm.Text =
                this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "", this.txtTantoshaCd.Text); // 担当者コード
            }
            this.txtTankaShutokuHoho.Text = Util.ToString(drDispData["TANKA_SHUTOKU_HOHO"]); // 単価取得方法
            this.txtKingakuHasuShori.Text = Util.ToString(drDispData["KINGAKU_HASU_SHORI"]); // 金額端数処理
            this.txtShohizeiNyuryokuHoho.Text = Util.ToString(drDispData["SHOHIZEI_NYURYOKU_HOHO"]); // 消費税入力方法
            if (this.txtShohizeiNyuryokuHoho.Text != "")
            {
                this.lblShzNrkHohoNm.Text =
                this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text); // 消費税入力方法
            }
            this.txtSeikyusakiCd.Text = Util.ToString(drDispData["SEIKYUSAKI_CD"]); // 請求先コード
            this.lblSeikyusakiNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]); // 請求先名
            this.txtShimebi.Text = Util.ToString(drDispData["SIMEBI"]); // 締日
            this.txtSeikyushoHakko.Text = Util.ToString(drDispData["SEIKYUSHO_HAKKO"]); // 請求書発行
            this.txtSeikyushoKeishiki.Text = Util.ToString(drDispData["SEIKYUSHO_KEISHIKI"]); // 請求書形式
            this.txtShohizeiHasuShori.Text = Util.ToString(drDispData["SHOHIZEI_HASU_SHORI"]); // 消費税端数処理
            this.txtShohizeiTenkaHoho.Text = Util.ToString(drDispData["SHOHIZEI_TENKA_HOHO"]); // 消費税転嫁方法
            this.txtKaishuTsuki.Text = Util.ToString(drDispData["KAISHU_TSUKI"]); // 回収月
            this.txtKaishuBi.Text = Util.ToString(drDispData["KAISHU_BI"]); // 回収日
            // 生年月日
            if (Util.ToString(drDispData["SEINENGAPPI"]) != "")
            {
                string[] tmpSeinengappi = Util.ConvJpDate(Util.ToString(drDispData["SEINENGAPPI"]), this.Dba); // 日付範囲を和暦で保持
                this.lblJpGengo.Text = tmpSeinengappi[0]; // 生年月日(元号)
                //this.txtJpYear.Text = tmpSeinengappi[1]; // 生年月日(年)
                //this.txtJpMonth.Text = tmpSeinengappi[2]; // 生年月日(月)
                //this.txtJpDay.Text = tmpSeinengappi[3]; // 生年月日(日)
                this.txtJpYear.Text = tmpSeinengappi[2]; // 生年月日(年)
                this.txtJpMonth.Text = tmpSeinengappi[3]; // 生年月日(月)
                this.txtJpDay.Text = tmpSeinengappi[4]; // 生年月日(日)

            }
            else
            {
                this.lblJpGengo.Text = ""; // 生年月日(元号)
                //this.txtJpYear.Text = "0"; // 生年月日(年)
                //this.txtJpMonth.Text = "0"; // 生年月日(月)
                //this.txtJpDay.Text = "0"; // 生年月日(日)
                this.txtJpYear.Text = ""; // 生年月日(年)
                this.txtJpMonth.Text = ""; // 生年月日(月)
                this.txtJpDay.Text = ""; // 生年月日(日)
            }
            // 加入日
            if (Util.ToString(drDispData["KANYUBI"]) != "")
            {
                string[] tmpKanyubi = Util.ConvJpDate(Util.ToString(drDispData["KANYUBI"]), this.Dba); // 日付範囲を和暦で保持
                this.lblKanyuGengo.Text = tmpKanyubi[0]; // 加入日(元号)
                //this.txtKanyuYear.Text = tmpKanyubi[1]; // 加入日(年)
                //this.txtKanyuMonth.Text = tmpKanyubi[2]; // 加入日(月)
                //this.txtKanyuDay.Text = tmpKanyubi[3]; // 加入日(日)
                this.txtKanyuYear.Text = tmpKanyubi[2]; // 加入日(年)
                this.txtKanyuMonth.Text = tmpKanyubi[3]; // 加入日(月)
                this.txtKanyuDay.Text = tmpKanyubi[4]; // 加入日(日)
            }
            else
            {
                this.lblKanyuGengo.Text = ""; // 加入日(元号)
                //this.txtKanyuYear.Text = "0"; // 加入日(年)
                //this.txtKanyuMonth.Text = "0"; // 加入日(月)
                //this.txtKanyuDay.Text = "0"; // 加入日(日)
                this.txtKanyuYear.Text = ""; // 加入日(年)
                this.txtKanyuMonth.Text = ""; // 加入日(月)
                this.txtKanyuDay.Text = ""; // 加入日(日)
            }
            // 脱退日
            if (Util.ToString(drDispData["DATTAIBI"]) != "")
            {
                string[] tmpDattaibi = Util.ConvJpDate(Util.ToString(drDispData["DATTAIBI"]), this.Dba); // 日付範囲を和暦で保持
                this.lblDattaiGengo.Text = tmpDattaibi[0]; // 脱退日(元号)
                //this.txtDattaiYear.Text = tmpDattaibi[1]; // 脱退日(年)
                //this.txtDattaiMonth.Text = tmpDattaibi[2]; // 脱退日(月)
                //this.txtDattaiDay.Text = tmpDattaibi[3]; // 脱退日(日)
                this.txtDattaiYear.Text = tmpDattaibi[2]; // 脱退日(年)
                this.txtDattaiMonth.Text = tmpDattaibi[3]; // 脱退日(月)
                this.txtDattaiDay.Text = tmpDattaibi[4]; // 脱退日(日)
            }
            else
            {
                this.lblDattaiGengo.Text = ""; // 脱退日(元号)
                //this.txtDattaiYear.Text = "0"; // 脱退日(年)
                //this.txtDattaiMonth.Text = "0"; // 脱退日(月)
                //this.txtDattaiDay.Text = "0"; // 脱退日(日)
                this.txtDattaiYear.Text = ""; // 脱退日(年)
                this.txtDattaiMonth.Text = ""; // 脱退日(月)
                this.txtDattaiDay.Text = ""; // 脱退日(日)
            }
            this.txtDattaiRiyu.Text = Util.ToString(drDispData["DATTAI_RIYU"]); // 脱退理由
            this.txthyoji.Text = Util.ToString(drDispData["HYOJI_FLG"]); // 表示区分
            // 個人情報グループ
            this.txtFuneNmCd.Text = Util.ToString(drDispData["FUNE_NM_CD"]); // 船主コード
            // 船主名
            if (this.txtFuneNmCd.Text != "")
            {
                string name = this.Dba.GetName(this.UInfo, "TB_HN_FUNE_NM_MST", "", this.txtFuneNmCd.Text);
                this.lblFuneNmCdSub.Text = name;
            }
            this.txtGyohoCd.Text = Util.ToString(drDispData["GYOHO_CD"]); // 漁法コード
            // 漁法名
            if (this.txtGyohoCd.Text != "")
            {
                string name = this.Dba.GetName(this.UInfo, "TB_HN_GYOHO_MST", "", this.txtGyohoCd.Text);
                this.lblGyohoCdSub.Text = name;
            }
            this.txtChikuCd.Text = Util.ToString(drDispData["CHIKU_CD"]); // 地区コード
            // 地区名
            if (this.txtChikuCd.Text != "")
            {
                string name = this.Dba.GetName(this.UInfo, "TB_HN_CHIKU_MST", "", this.txtChikuCd.Text);
                this.lblChikuCdSub.Text = name;
            }
            this.txtKumiaiTesuryoRitsu.Text = Util.ToString(drDispData["KUMIAI_TESURYO_RITSU"]); // 組合手数料
            this.txtShiharaiKubun.Text = Util.ToString(drDispData["SHIHARAI_KUBUN"]); // 支払区分
            this.txtShishoKubun.Text = Util.ToString(drDispData["SHISHO_KUBUN"]); // 支所区分
            this.txtSeijunKubun.Text = Util.ToString(drDispData["SEIJUN_KUBUN"]); // 正準区分
            this.txtFutsuRitsu.Text = Util.ToString(drDispData["FUTSU_WARIAI"]); // 普通率
            this.txtSonotaRitsu.Text = Util.ToString(drDispData["SONOTA_WARIAI"]); // その他率
            this.txtPayaoRitsu.Text = Util.ToString(drDispData["PAYAO_WARIAI"]); // パヤオ率
            this.txtFutsuKozaBango.Text = Util.ToString(drDispData["FUTSU_KOZA_BANGO"]); // 普通口座番号
            this.txtFutsuKozaKubun.Text = Util.ToString(drDispData["FUTSU_KOZA_KUBUN"]); // 普通口座区分
            this.txtTsumitateRitsu.Text = Util.ToString(drDispData["TSUMITATE_RITSU"]); // 積立率
            this.txtTsumitateKozaBango.Text = Util.ToString(drDispData["TSUMITATE_KOZA_BANGO"]); // 積立口座番号
            this.txtTsumitateKozaKubun.Text = Util.ToString(drDispData["TSUMITATE_KOZA_KUBUN"]); // 積立口座区分
            // 販売から起動された場合のみ実行
            if (MODE_HANBAI.Equals(this.Par2))
            {
                this.txtTsumitateRitsuAzukari.Text = Util.ToString(drDispData["AZUKARIKIN_TSUMITATE_RITSU"] + ".00"); // 積立率(預り金)
                this.txtKozaBangoAzukari.Text = Util.ToString(drDispData["AZUKARIKIN_KOZA_BANGO"]); // 口座番号(預り金)
                this.txtKozaKubunAzukari.Text = Util.ToString(drDispData["AZUKARIKIN_KOZA_KUBUN"]); // 口座区分(預り金)
                this.txtTsumitateRitsuHonnin.Text = Util.ToString(drDispData["HONNIN_TSUMITATE_RITSU"] + ".00"); // 積立率(本人)
                this.txtKozaBangoHonnin.Text = Util.ToString(drDispData["HONNIN_KOZA_BANGO"]); // 口座番号(本人)
                this.txtKozaKubunHonnin.Text = Util.ToString(drDispData["HONNIN_KOZA_KUBUN"]); // 口座区分(本人)
            }

            // 船主コード・請求先コード・請求書発行は入力不可
            this.lblFunanushiCd.Enabled = false;
            this.txtFunanushiCd.Enabled = false;
            this.lblSeikyusakiCd.Enabled = false;
            this.txtSeikyusakiCd.Enabled = false;
            this.lblSeikyushoHakko.Enabled = false;
            this.txtSeikyushoHakko.Enabled = false;

            // 削除ボタン制御を実装
            this.btnF3.Enabled = IsValidFunanushiCd(this.txtFunanushiCd.Text);

            //// TB_ZM_SHIWAKE_MEISAIのデータを取得
            //DbParamCollection dpcDenpyo = new DbParamCollection();
            //StringBuilder Sql = new StringBuilder();
            //Sql.Append(" SELECT");
            //Sql.Append(" DENPYO_BANGO");
            //Sql.Append(" FROM");
            //Sql.Append(" TB_HN_TORIHIKI_DENPYO");
            //Sql.Append(" WHERE");
            //Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            //Sql.Append(" (TOKUISAKI_CD = @TORIHIKISAKI_CD OR SEIKYUSAKI_CD = @TORIHIKISAKI_CD)");
            //dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            //dpcDenpyo.SetParam("@TORIHIKISAKI_CD", SqlDbType.VarChar, 6, this.txtFunanushiCd.Text);

            //DataTable dtDenpyoUmu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpcDenpyo);
            //if (dtDenpyoUmu.Rows.Count == 0)
            //{
            //    this.btnF3.Enabled = true;
            //}
            //else
            //{
            //    this.btnF3.Enabled = false;
            //}
        }

        /// <summary>
        /// TB_CM_TORIHIKISAKIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと取引先コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと取引先コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
                alParams.Add(whereParam);
            }

            // 取引先名
            updParam.SetParam("@TORIHIKISAKI_NM", SqlDbType.VarChar, 40, this.txtFunanushiNm.Text);
            // 取引先カナ名
            updParam.SetParam("@TORIHIKISAKI_KANA_NM", SqlDbType.VarChar, 30, this.txtFunanushiKanaNm.Text);
            // 郵便番号1
            updParam.SetParam("@YUBIN_BANGO1", SqlDbType.VarChar, 3, this.txtYubinBango1.Text);
            // 郵便番号2
            updParam.SetParam("@YUBIN_BANGO2", SqlDbType.VarChar, 4, this.txtYubinBango2.Text);
            // 住所1
            updParam.SetParam("@JUSHO1", SqlDbType.VarChar, 30, this.txtJusho1.Text);
            // 住所2
            updParam.SetParam("@JUSHO2", SqlDbType.VarChar, 30, this.txtJusho2.Text);
            // 住所3
            updParam.SetParam("@JUSHO3", SqlDbType.VarChar, 30, "");
            // 電話番号
            updParam.SetParam("@DENWA_BANGO", SqlDbType.VarChar, 15, this.txtTel.Text);
            // FAX番号
            updParam.SetParam("@FAX_BANGO", SqlDbType.VarChar, 15, this.txtFax.Text);
            // 担当者コード
            updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.txtTantoshaCd.Text);
            // 締日
            updParam.SetParam("@SIMEBI", SqlDbType.Decimal, 2, this.txtShimebi.Text);
            // 回収月
            updParam.SetParam("@KAISHU_TSUKI", SqlDbType.Decimal, 2, this.txtKaishuTsuki.Text);
            // 回収日
            updParam.SetParam("@KAISHU_BI", SqlDbType.Decimal, 2, this.txtKaishuBi.Text);
            //請求準備月
            // TODO:画面上のどの項目なのか確認
            //請求締月
            // TODO:画面上のどの項目なのか確認
            // 請求書発行
            updParam.SetParam("@SEIKYUSHO_HAKKO", SqlDbType.Decimal, 1, this.txtSeikyushoHakko.Text);
            // 請求書形式
            updParam.SetParam("@SEIKYUSHO_KEISHIKI", SqlDbType.Decimal, 1, this.txtSeikyushoKeishiki.Text);
            // 消費税転嫁方法
            updParam.SetParam("@SHOHIZEI_TENKA_HOHO", SqlDbType.Decimal, 1, this.txtShohizeiTenkaHoho.Text);
            // 消費税端数処理
            updParam.SetParam("@SHOHIZEI_HASU_SHORI", SqlDbType.Decimal, 1, this.txtShohizeiHasuShori.Text);
            // 略称
            updParam.SetParam("@RYAKUSHO", SqlDbType.VarChar, 20, this.txtRyFunanushiNm.Text);
            // 船名CD
            updParam.SetParam("@FUNE_NM_CD", SqlDbType.VarChar, 5, this.txtFuneNmCd.Text);
            // 漁法CD
            updParam.SetParam("@GYOHO_CD", SqlDbType.VarChar, 5, this.txtGyohoCd.Text);
            // 地区CD
            updParam.SetParam("@CHIKU_CD", SqlDbType.VarChar, 5, this.txtChikuCd.Text);
            // 組合手数料率
            updParam.SetParam("@KUMIAI_TESURYO_RITSU", SqlDbType.VarChar, 6, this.txtKumiaiTesuryoRitsu.Text);
            // 普通口座番号
            updParam.SetParam("@FUTSU_KOZA_BANGO", SqlDbType.VarChar, 7, this.txtFutsuKozaBango.Text);
            // 普通口座区分
            updParam.SetParam("@FUTSU_KOZA_KUBUN", SqlDbType.VarChar, 1, this.txtFutsuKozaKubun.Text);
            // 積立区分
            // TODO:画面上のどの項目なのか確認
            // 積立率
            updParam.SetParam("@TSUMITATE_RITSU", SqlDbType.VarChar, 6, this.txtTsumitateRitsu.Text);
            // 積立口座番号
            updParam.SetParam("@TSUMITATE_KOZA_BANGO", SqlDbType.VarChar, 7, this.txtTsumitateKozaBango.Text);
            // 積立口座区分
            updParam.SetParam("@TSUMITATE_KOZA_KUBUN", SqlDbType.VarChar, 1, this.txtTsumitateKozaKubun.Text);
            // 支所区分
            updParam.SetParam("@SHISHO_KUBUN", SqlDbType.VarChar, 1, this.txtShishoKubun.Text);
            // 正準区分
            updParam.SetParam("@SEIJUN_KUBUN", SqlDbType.VarChar, 1, this.txtSeijunKubun.Text);
            // 振込先区分
            // TODO:必要なのか確認
            // 普通割合
            updParam.SetParam("@FUTSU_WARIAI", SqlDbType.VarChar, 3, this.txtFutsuRitsu.Text);
            // その他割合
            updParam.SetParam("@SONOTA_WARIAI", SqlDbType.VarChar, 3, this.txtSonotaRitsu.Text);
            // パヤオ割合
            updParam.SetParam("@PAYAO_WARIAI", SqlDbType.VarChar, 3, this.txtPayaoRitsu.Text);
            // 更新日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            // TODO:処理FLGには0を更新だはず
            updParam.SetParam("@SHORI_FLG", SqlDbType.VarChar, 1, "0");
            // 生年月日
            string tmpJpDate;
            //if (this.lblJpGengo.Text != null && this.txtJpYear.Text != "0" && this.txtJpMonth.Text != "0" && this.txtJpDay.Text != "0")
            if (!string.IsNullOrEmpty(this.lblJpGengo.Text) && !string.IsNullOrEmpty(this.txtJpYear.Text) && !string.IsNullOrEmpty(this.txtJpMonth.Text) && !string.IsNullOrEmpty(this.txtJpDay.Text))
            {
                // 和暦→西暦に変換
                tmpJpDate = Util.ToString(Util.ConvAdDate(this.lblJpGengo.Text, this.txtJpYear.Text, this.txtJpMonth.Text, this.txtJpDay.Text, this.Dba));
                updParam.SetParam("@SEINENGAPPI", SqlDbType.DateTime, tmpJpDate);
            }
            else
            {
                updParam.SetParam("@SEINENGAPPI", SqlDbType.DateTime, null);
            }
            // 加入日
            string tmpKanyuDate;
            //if (this.lblKanyuGengo.Text != null && this.txtKanyuYear.Text != "0" && this.txtKanyuMonth.Text != "0" && this.txtKanyuDay.Text != "0")
            if (!string.IsNullOrEmpty(this.lblKanyuGengo.Text) && !string.IsNullOrEmpty(this.txtKanyuYear.Text) && !string.IsNullOrEmpty(this.txtKanyuMonth.Text) && !string.IsNullOrEmpty(this.txtKanyuDay.Text))
            {
                // 和暦→西暦に変換
                tmpKanyuDate = Util.ToString(Util.ConvAdDate(this.lblKanyuGengo.Text, this.txtKanyuYear.Text, this.txtKanyuMonth.Text, this.txtKanyuDay.Text, this.Dba));
                updParam.SetParam("@KANYUBI", SqlDbType.DateTime, tmpKanyuDate);
            }
            else
            {
                updParam.SetParam("@KANYUBI", SqlDbType.DateTime, null);
            }
            // 脱退日
            string tmpDattaiDate;
            //if (this.lblDattaiGengo.Text != null && this.txtDattaiYear.Text != "0" && this.txtDattaiMonth.Text != "0" && this.txtDattaiDay.Text != "0")
            if (!string.IsNullOrEmpty(this.lblDattaiGengo.Text) && !string.IsNullOrEmpty(this.txtDattaiYear.Text) && !string.IsNullOrEmpty(this.txtDattaiMonth.Text) && !string.IsNullOrEmpty(this.txtDattaiDay.Text))
            {
                // 和暦→西暦に変換
                tmpDattaiDate = Util.ToString(Util.ConvAdDate(this.lblDattaiGengo.Text, this.txtDattaiYear.Text, this.txtDattaiMonth.Text, this.txtDattaiDay.Text, this.Dba));
                updParam.SetParam("@DATTAIBI", SqlDbType.DateTime, tmpDattaiDate);
            }
            else
            {
                updParam.SetParam("@DATTAIBI", SqlDbType.DateTime, null);
            }
            // 脱退理由
            updParam.SetParam("@DATTAI_RIYU", SqlDbType.VarChar, 40, this.txtDattaiRiyu.Text);
            // 表示区分
            updParam.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, this.txthyoji.Text);
            // 支払区分
            updParam.SetParam("@SHIHARAI_KUBUN", SqlDbType.Decimal, 1, this.txtShiharaiKubun.Text);

            // 販売から起動された場合のみ実行
            if (MODE_HANBAI.Equals(this.Par2))
            {
                // 預り金積立率
                updParam.SetParam("@AZUKARIKIN_TSUMITATE_RITSU", SqlDbType.Decimal, 5, this.txtTsumitateRitsuAzukari.Text);
                // 預り金口座番号
                updParam.SetParam("@AZUKARIKIN_KOZA_BANGO", SqlDbType.VarChar, 7, this.txtKozaBangoAzukari.Text);
                // 預り金口座区分
                updParam.SetParam("@AZUKARIKIN_KOZA_KUBUN", SqlDbType.Decimal, 5, this.txtKozaKubunAzukari.Text);
                // 本人積立率
                updParam.SetParam("@HONNIN_TSUMITATE_RITSU", SqlDbType.Decimal, 7, this.txtTsumitateRitsuHonnin.Text);
                // 本人口座番号
                updParam.SetParam("@HONNIN_KOZA_BANGO", SqlDbType.VarChar, 7, this.txtKozaBangoHonnin.Text);
                // 本人口座区分
                updParam.SetParam("@HONNIN_KOZA_KUBUN", SqlDbType.Decimal, 5, this.txtKozaKubunHonnin.Text);
            }

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_TORIHIKISAKI_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetHnToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと取引先コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと取引先コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
                alParams.Add(whereParam);
            }

            // 請求先コード
            updParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
            // TODO:事業区分には3を更新だはず
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, 3);
            // 単価取得方法
            updParam.SetParam("@TANKA_SHUTOKU_HOHO", SqlDbType.Decimal, 1, this.txtTankaShutokuHoho.Text);
            // 消費税入力方法
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, this.txtShohizeiNyuryokuHoho.Text);
            // 金額端数処理
            updParam.SetParam("@KINGAKU_HASU_SHORI", SqlDbType.Decimal, 1, this.txtKingakuHasuShori.Text);
            // 取引先区分1
            updParam.SetParam("@TORIHIKISAKI_KUBUN1", SqlDbType.Decimal, 4, 1);
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO", SqlDbType.Decimal, 9, 0);
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO_ZBN", SqlDbType.Decimal, 3, 0);
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO_KIS", SqlDbType.Decimal, 9, 0);
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO_SR", SqlDbType.Decimal, 9, 0);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_TMP_TORIHIKISAKIからデータを削除
        /// TB_HN_TORIHIKISAKI_JOHOからデータを削除
        /// TB_HN_NOHINSAKIからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 会社コードと船主コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcDenpyo = new DbParamCollection();
            dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcDenpyo.SetParam("@TORIHIKISAKI_CD", SqlDbType.VarChar, 6, this.txtFunanushiCd.Text);

            alParams.Add(dpcDenpyo);

            return alParams;
        }
		#endregion

		private void fsiPanel12_Paint(object sender, PaintEventArgs e)
		{

		}

		private void fsiTableLayoutPanel2_Paint(object sender, PaintEventArgs e)
		{

		}
	}

}
