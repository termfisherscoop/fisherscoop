﻿namespace jp.co.fsi.cm.cmcm2011
{
    partial class CMCM2011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblKanaNm = new System.Windows.Forms.Label();
			this.txtKanaName = new jp.co.fsi.common.controls.FsiTextBox();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.ckboxAllHyoji = new System.Windows.Forms.CheckBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(5, 5);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(5, 64);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF2
			// 
			this.btnF2.Location = new System.Drawing.Point(91, 64);
			this.btnF2.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF3
			// 
			this.btnF3.Location = new System.Drawing.Point(177, 64);
			this.btnF3.Margin = new System.Windows.Forms.Padding(5);
			this.btnF3.Click += new System.EventHandler(this.btnF3_Click);
			// 
			// btnF4
			// 
			this.btnF4.Location = new System.Drawing.Point(263, 64);
			this.btnF4.Margin = new System.Windows.Forms.Padding(5);
			this.btnF4.Click += new System.EventHandler(this.btnF4_Click);
			// 
			// btnF5
			// 
			this.btnF5.Location = new System.Drawing.Point(349, 64);
			this.btnF5.Margin = new System.Windows.Forms.Padding(5);
			this.btnF5.Click += new System.EventHandler(this.btnF5_Click);
			// 
			// btnF7
			// 
			this.btnF7.Location = new System.Drawing.Point(521, 64);
			this.btnF7.Margin = new System.Windows.Forms.Padding(5);
			this.btnF7.Click += new System.EventHandler(this.btnF7_Click);
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(435, 64);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			this.btnF6.Click += new System.EventHandler(this.btnF6_Click);
			// 
			// btnF8
			// 
			this.btnF8.Location = new System.Drawing.Point(607, 64);
			this.btnF8.Margin = new System.Windows.Forms.Padding(5);
			this.btnF8.Click += new System.EventHandler(this.btnF8_Click);
			// 
			// btnF9
			// 
			this.btnF9.Location = new System.Drawing.Point(693, 64);
			this.btnF9.Margin = new System.Windows.Forms.Padding(5);
			this.btnF9.Click += new System.EventHandler(this.btnF9_Click);
			// 
			// btnF12
			// 
			this.btnF12.Location = new System.Drawing.Point(951, 64);
			this.btnF12.Margin = new System.Windows.Forms.Padding(5);
			this.btnF12.Click += new System.EventHandler(this.btnF12_Click);
			// 
			// btnF11
			// 
			this.btnF11.Location = new System.Drawing.Point(865, 64);
			this.btnF11.Margin = new System.Windows.Forms.Padding(5);
			this.btnF11.Click += new System.EventHandler(this.btnF11_Click);
			// 
			// btnF10
			// 
			this.btnF10.Location = new System.Drawing.Point(779, 64);
			this.btnF10.Margin = new System.Windows.Forms.Padding(5);
			this.btnF10.Click += new System.EventHandler(this.btnF10_Click);
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(4, 329);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(707, 130);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(707, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblKanaNm
			// 
			this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKanaNm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblKanaNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblKanaNm.Name = "lblKanaNm";
			this.lblKanaNm.Size = new System.Drawing.Size(672, 35);
			this.lblKanaNm.TabIndex = 0;
			this.lblKanaNm.Tag = "CHANGE";
			this.lblKanaNm.Text = "カナ名";
			this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKanaName
			// 
			this.txtKanaName.AllowDrop = true;
			this.txtKanaName.AutoSizeFromLength = false;
			this.txtKanaName.DisplayLength = null;
			this.txtKanaName.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtKanaName.Location = new System.Drawing.Point(104, 6);
			this.txtKanaName.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanaName.MaxLength = 30;
			this.txtKanaName.Name = "txtKanaName";
			this.txtKanaName.Size = new System.Drawing.Size(343, 23);
			this.txtKanaName.TabIndex = 1;
			this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(16, 96);
			this.dgvList.Margin = new System.Windows.Forms.Padding(5);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(682, 277);
			this.dgvList.TabIndex = 6;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// ckboxAllHyoji
			// 
			this.ckboxAllHyoji.AutoSize = true;
			this.ckboxAllHyoji.BackColor = System.Drawing.Color.Silver;
			this.ckboxAllHyoji.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.ckboxAllHyoji.Location = new System.Drawing.Point(539, 8);
			this.ckboxAllHyoji.Margin = new System.Windows.Forms.Padding(4);
			this.ckboxAllHyoji.Name = "ckboxAllHyoji";
			this.ckboxAllHyoji.Size = new System.Drawing.Size(91, 20);
			this.ckboxAllHyoji.TabIndex = 5;
			this.ckboxAllHyoji.Tag = "CHANGE";
			this.ckboxAllHyoji.Text = "全て表示";
			this.ckboxAllHyoji.UseVisualStyleBackColor = false;
			this.ckboxAllHyoji.CheckedChanged += new System.EventHandler(this.rdoBashoNm01_CheckedChanged);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(16, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 1;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(682, 45);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtKanaName);
			this.fsiPanel1.Controls.Add(this.ckboxAllHyoji);
			this.fsiPanel1.Controls.Add(this.lblKanaNm);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(672, 35);
			this.fsiPanel1.TabIndex = 0;
			// 
			// CMCM2011
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(707, 465);
			this.Controls.Add(this.dgvList);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "CMCM2011";
			this.Text = "ReportSample";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.CheckBox ckboxAllHyoji;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
    }
}