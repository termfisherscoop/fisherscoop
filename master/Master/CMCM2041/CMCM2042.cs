﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm2041
{
    /// <summary>
    /// 部門のの登録(CMCM2042)
    /// </summary>
    public partial class CMCM2042 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2042()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public CMCM2042(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：部門コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtJigyoKbn":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            //アセンブリのロード
            System.Reflection.Assembly asm;
            // フォーム作成
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtJigyoKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_JIGYO_KUBUN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtJigyoKbn.Text = result[0];
                                this.lblJigyoKubun.Text = result[1];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;
            // 削除する前に再度コードの存在チェック
            //if (IsCodeUsing())
            if (!IsCodeUsing())
            {
                Msg.Error("部門コードが使用されているため、削除できません。");
                return;
            }
            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }
            // 削除処理
            try
            {
                // データ削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.InData);
                this.Dba.Delete("TB_CM_BUMON", "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD", dpc);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 存在しない事業者コードはエラー
            if (!this.txtJigyoKbn.Text.Equals("0") && this.lblJigyoKubun.Text.Equals(""))
            {
                Msg.Error("入力に誤りがあります。");
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsZmbumon = SetZmbumonParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    if (IsCodeExists())
                    {
                        Msg.Error("部門情報が登録済みです。");
                        return;
                    }

                    // データ登録
                    // 部門
                    this.Dba.Insert("TB_CM_BUMON", (DbParamCollection)alParamsZmbumon[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    
                    // データ更新
                    // 部門
                    this.Dba.Update("TB_CM_BUMON",
                        (DbParamCollection)alParamsZmbumon[1],
                        "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD",
                        (DbParamCollection)alParamsZmbumon[0]);
                }
                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();

        }
        #endregion

        #region イベント


        /// <summary>
        /// 事業区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJigyoKbn_Validating(object sender, CancelEventArgs e)
        {
            this.lblJigyoKubun.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_JIGYO_KUBUN", "", this.txtJigyoKbn.Text);
        }

        private void txtJigyoKbn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 部門コードの初期値を取得
            // 部門コードの中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            DataTable dtMaxbumon =
                this.Dba.GetDataTableByConditionWithParams("MAX(BUMON_CD) AS MAX_CD",
                    "TB_CM_BUMON", Util.ToString(where), dpc);
            if (dtMaxbumon.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxbumon.Rows[0]["MAX_CD"]))
            {
                this.txtBumonCd.Text = Util.ToString(Util.ToInt(dtMaxbumon.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtBumonCd.Text = "1";
            }
            // 事業区分0
            this.txtJigyoKbn.Text = "0";
            this.lblJigyoKubun.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_JIGYO_KUBUN", "", this.txtJigyoKbn.Text);
            this.txtBumonCd.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.BUMON_CD");
            cols.Append(" ,A.BUMON_NM");
            cols.Append(" ,A.BUMON_KANA_NM");
            cols.Append(" ,A.JIGYO_KUBUN");
            cols.Append(" ,A.HAIFU_KUBUN");

            StringBuilder from = new StringBuilder();
            from.Append("TB_CM_BUMON AS A");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.BUMON_CD = @BUMON_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtBumonCd.Text = Util.ToString(drDispData["BUMON_CD"]);
            this.txtBumonNm.Text = Util.ToString(drDispData["BUMON_NM"]);
            this.txtBumonKanaNm.Text = Util.ToString(drDispData["BUMON_KANA_NM"]);
            this.txtJigyoKbn.Text = Util.ToString(drDispData["JIGYO_KUBUN"]);
            this.lblJigyoKubun.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_JIGYO_KUBUN", "", this.txtJigyoKbn.Text);
            //　部門コード入力不可　
            this.txtBumonCd.Enabled = false;
            //if (IsCodeUsing())
            //{
            //    // 使用されているので削除不可
            //    this.btnF3.Enabled = false;
            //}
            //else
            //{
            //    // 使用されていないので削除OK
            //    this.btnF3.Enabled = true;
            //}
            this.btnF3.Enabled = IsCodeUsing();
        }
        /// <summary>
        /// 部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonCd()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtBumonCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBumonCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, this.txtBumonCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND BUMON_CD = @BUMON_CD");
            DataTable dtbumon =
                this.Dba.GetDataTableByConditionWithParams("BUMON_CD",
                    "TB_CM_BUMON", Util.ToString(where), dpc);
            if (dtbumon.Rows.Count > 0)
            {
                Msg.Error("既に存在する部門コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 部門の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtBumonNm.Text, this.txtBumonNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 部門カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtBumonKanaNm.Text, this.txtBumonKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }
        /// <summary>
        /// 事業区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJigyoKubn()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtJigyoKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtJigyoKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }
        
        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 部門コードのチェック
                if (!IsValidBumonCd())
                {
                    this.txtBumonCd.Focus();
                    return false;
                }
            }

            // 部門名のチェック
            if (!IsValidBumonNm())
            {
                this.txtBumonNm.Focus();
                return false;
            }

            // 部門カナ名のチェック
            if (!IsValidBumonKanaNm())
            {
                this.txtBumonKanaNm.Focus();
                return false;
            }

            // 事業区分のチェック
            if (!IsValidJigyoKubn())
            {
                this.txtJigyoKbn.Focus();
                return false;
            }
            return true;
        }
        /// <summary>
        /// TB_CM_BUMONに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmbumonParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと部門コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, this.txtBumonCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと部門コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, this.txtBumonCd.Text);
                alParams.Add(whereParam);
            }
            // 部門名
            updParam.SetParam("@BUMON_NM", SqlDbType.VarChar, 40, this.txtBumonNm.Text);
            // 部門カナ名
            updParam.SetParam("@BUMON_KANA_NM", SqlDbType.VarChar, 30, this.txtBumonKanaNm.Text);
            // 事業区分
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 3, this.txtJigyoKbn.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(updParam);

            return alParams;
        }
        /// <summary>
        /// 対象の部門コードがマスタ登録されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeExists()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.txtBumonCd.Text);
            DataTable dtBuMon = this.Dba.GetDataTableByConditionWithParams("*", "VI_ZM_BUMON", "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD", dpc);

            if (dtBuMon.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 対象の部門コードがトランテーブルで使用されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeUsing()
        {
            // 削除可否チェック
            // 指定した部門コードが使用されているかどうかをチェック
            // 仕訳明細
            StringBuilder from = new StringBuilder();
            from.Append(" TB_ZM_SHIWAKE_MEISAI ");

            StringBuilder where = new StringBuilder();
            where.Append(" KAISHA_CD = @KAISHA_CD");
            where.Append(" AND BUMON_CD = @BUMON_CD");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtBumon = this.Dba.GetDataTableByConditionWithParams("BUMON_CD", from.ToString(), where.ToString(), dpc);

            if (dtBumon.Rows.Count > 0)
            {
                return false;
            }

            from = new StringBuilder();
            from.Append(" TB_HN_TORIHIKI_MEISAI ");
            where = new StringBuilder();
            where.Append(" KAISHA_CD = @KAISHA_CD");
            where.Append(" AND BUMON_CD = @BUMON_CD");

            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            dtBumon = this.Dba.GetDataTableByConditionWithParams("BUMON_CD", from.ToString(), where.ToString(), dpc);
            if (dtBumon.Rows.Count > 0)
            {
                return false;
            }

            return true;
        }

        #endregion

    }
}
        