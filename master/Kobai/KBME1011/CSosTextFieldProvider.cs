﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using systembase.table;

namespace jp.co.fsi.kb.kbme1011
{
    class CSosTextFieldProvider: CFieldProvider
    {

        private ImeMode _imeMode;

        public CSosTextFieldProvider() : this(null)
        {
        }

        public CSosTextFieldProvider(string caption) : this(caption, System.Windows.Forms.ImeMode.NoControl)
        {
        }

        public CSosTextFieldProvider(string caption, ImeMode imeMode): base(caption)
        {
	        this._imeMode = imeMode;
        }

        public override IEditor CreateEditor()
        {
	        return new CFsiTextBoxEditor();
        }

        public override System.Windows.Forms.ImeMode ImeMode()
        {
	        return this._imeMode;
        }

    }
}
