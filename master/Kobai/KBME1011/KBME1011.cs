﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;
using jp.co.fsi.common.constants;
using systembase.table;

namespace jp.co.fsi.kb.kbme1011
{
    #region 棚卸設定構造体
    // 棚卸設定構造体
    public struct Settei
    {
        public int kbn;
        public decimal tanka;
        public decimal gokei;
        public int sort;
        public void Clear()
        {
            kbn = 0;
            tanka = 0;
            gokei = 0;
            sort = 0;
        }
    }
    #endregion

    /// <summary>
    /// 棚卸入力(KBME1011)
    /// </summary>
    public partial class KBME1011 : BasePgForm
    {

        #region 定数
        // 入力モード設定
        private int MODE_EDIT = 1; // 1:登録,2:修正
        // 仕入単価（単価設定がゼロなら仕入単価、違ったら最終仕入単価）
        private int TANKA_SETTEI_SHIIRE = 0;
        // ソート順
        public int Sort1 = 1;  // 商品コードでソート
        public int Sort2 = 2;  // 棚番、商品コード
        public int Sort3 = 3;  // 棚番、商品区分１、商品コード
        public int Sort4 = 4;  // 棚番、商品区分１、商品区分２、商品コード
        #endregion

        #region 変数
        /// <summary>
        /// 棚卸設定退避用変数
        /// </summary>
        public Settei gSettei;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBME1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 新規モードの初期表示
            InitDispOnNew();
            // 明細部の初期化
            this.InitDetailArea();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1～F12を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtGengoYear":
                case "txtShohinKbn1":
                case "txtTanabanCdFr":
                case "txtTanabanCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            if (!this.btnF1.Enabled)
                return;

            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。
            //System.Reflection.Assembly asm;
            Assembly asm;
            Type t;
            String[] result;

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtShohinKbn1":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.txtShohinKbn1.Text;
                            switch (gSettei.kbn)
                            {
                                case 1:
                                    frm.Par1 = "VI_HN_SHOHIN_KBN1";
                                    break;
                                case 2:
                                    frm.Par1 = "VI_HN_SHOHIN_KBN2";
                                    break;
                                case 3:
                                    frm.Par1 = "VI_HN_SHOHIN_KBN3";
                                    break;
                                default:
                                    break;
                            }
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShohinKbn1.Text = outData[0];
                                this.lblShohinKbn1Nm.Text = outData[1];

                            }
                        }
                    }
                    break;
                case "txtTanabanCdFr":
                case "txtTanabanCdTo":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm2011.KBCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.ActiveCtlNm == "txtTanabanCdFr")
                            {
                                frm.InData = this.txtTanabanCdFr.Text;
                            }
                            else if (this.ActiveCtlNm == "txtTanabanCdTo")
                            {
                                frm.InData = this.txtTanabanCdTo.Text;
                            }
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtTanabanCdFr")
                                {
                                    this.txtTanabanCdFr.Text = result[0];
                                    this.lblTanabanNmFr.Text = result[0];
                                }
                                else if (this.ActiveCtlNm == "txtTanabanCdTo")
                                {
                                    this.txtTanabanCdTo.Text = result[0];
                                    this.lblTanabanNmTo.Text = result[0];
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                this.SetJp(Util.FixJpDate(this.lblGengo.Text, this.txtGengoYear.Text,
                                        this.txtMonth.Text, this.txtDay.Text, this.Dba));
                            }
                        }

                    }
                    break;
                case "txtGengoYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                this.SetJp(Util.FixJpDate(this.lblGengo.Text, this.txtGengoYear.Text,
                                        this.txtMonth.Text, this.txtDay.Text, this.Dba));
                            }
                        }
                        
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            DbParamCollection dpc;
            DataTable dtCheck;

            // 登録モードは処理しない
            if (this.MODE_EDIT == 1)
            {
                return;
            }

            DateTime TANAOROSHI_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);

            // 棚卸確認処理
            // Han.TB_棚卸(TB_HN_TANAOROSHI)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, TANAOROSHI_DATE);
            //dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dtCheck = this.Dba.GetDataTableByConditionWithParams(
                "COUNT(*) AS CNT",
                "TB_HN_TANAOROSHI",
                "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND TANAOROSHI_DATE = @TANAOROSHI_DATE ",
                dpc);
            if (Util.ToLong(dtCheck.Rows[0]["CNT"]) == 0)
            {
                return;
            }

            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            this.pnlWait.Visible = true;
            this.pnlWait.Refresh();

            // 削除処理
            this.DeleteData();

            this.pnlWait.Visible = false;
            Cursor.Current = Cursors.Default;

            // 合計エリアのクリア
            this.mtbList.FooterContent.Records[0].Fields["TOTAL_VALUE2"].Value = "";
            // 登録初期表示処理へ
            this.InitDispOnNew();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // データチェック
            if (!this.ValidateAll())
            {
                return;
            }
            string msg = (this.MODE_EDIT == 1 ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            this.pnlWait.Visible = true;
            this.pnlWait.Refresh();

            // 登録処理
            this.UpdateData();

            this.pnlWait.Visible = false;
            Cursor.Current = Cursors.Default;

            // 合計エリアのクリア
            this.mtbList.FooterContent.Records[0].Fields["TOTAL_VALUE2"].Value = "";
            // 登録初期表示処理へ
            this.InitDispOnNew();
        }

        public override void PressF9()
        {
            Assembly asm;
            Type t;
            // アセンブリのロード
            asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBMR1021.exe");
            // フォーム作成
            t = asm.GetType("jp.co.fsi.kb.kbmr1021.KBMR1022");
            if (t != null)
            {
                Object obj = System.Activator.CreateInstance(t);
                if (obj != null)
                {
                    // タブの一部として埋め込む
                    BasePgForm frm = (BasePgForm)obj;
                    frm.ShowDialog(this);
                    if (frm.DialogResult == DialogResult.OK)
                    {
                        // 設定内容の取得
                        GetSettei();
                        // 設定を反映させる
                        ControlItemsBySettings();
                    }
                }

            }
        }

        /// <summary>
        /// F10キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 棚卸日付・年の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtGengoYear.Text, this.txtGengoYear.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYear.SelectAll();
            }
            else
            {
                this.txtGengoYear.Text = Util.ToString(IsValid.SetYear(this.txtGengoYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 棚卸日付・月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtMonth.SelectAll();
            }
            else
            {
                this.txtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtMonth.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 棚卸日付・日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDay.SelectAll();
            }
            else
            {
                this.txtDay.Text = Util.ToString(IsValid.SetDay(this.txtDay.Text));
                CheckJp();
                SetJp();

                // 入力データ表示
                this.LoadData();
            }
        }

        /// <summary>
        /// 商品区分１の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKbn1_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinKbn1.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtShohinKbn1.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtShohinKbn1.Text) || this.txtShohinKbn1.Text == "0")
            {
                this.txtShohinKbn1.Text = "0";
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtShohinKbn1.Text) && this.txtShohinKbn1.Text != "0")
            {
                switch (gSettei.kbn)
                {
                    case 1:
                        this.lblShohinKbn1Nm.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN1", this.txtMizuageShishoCd.Text, this.txtShohinKbn1.Text);
                        break;
                    case 2:
                        this.lblShohinKbn1Nm.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN2", this.txtMizuageShishoCd.Text, this.txtShohinKbn1.Text);
                        break;
                    case 3:
                        this.lblShohinKbn1Nm.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN3", this.txtMizuageShishoCd.Text, this.txtShohinKbn1.Text);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                this.lblShohinKbn1Nm.Text = "全　て";
            }
        }

        /// <summary>
        /// 棚番Frの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTanabanCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            //if (!ValChk.IsNumber(this.txtTanabanCdFr.Text))
            if (!ValChk.IsHalfChar(this.txtTanabanCdFr.Text))
            {
                //Msg.Error("数値のみで入力してください。");
                Msg.Notice("半角英数字のみで入力してください。");
                this.txtTanabanCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtTanabanCdFr.Text))
            {
                this.lblTanabanNmFr.Text = this.Dba.GetTanabanNm(this.UInfo, this.txtTanabanCdFr.Text);
            }
            else
            {
                this.lblTanabanNmFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 棚番Toの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTanabanCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            //if (!ValChk.IsNumber(this.txtTanabanCdTo.Text))
            if (!ValChk.IsHalfChar(this.txtTanabanCdTo.Text))
            {
                //Msg.Error("数値のみで入力してください。");
                Msg.Notice("半角英数字のみで入力してください。");
                this.txtTanabanCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtTanabanCdTo.Text))
            {
                this.lblTanabanNmTo.Text = this.Dba.GetTanabanNm(this.UInfo, this.txtTanabanCdTo.Text);
            }
            else
            {
                this.lblTanabanNmTo.Text = "最　後";
            }

            // 入力データ表示
            this.LoadData();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 初期値、入力制御を実装
            DbParamCollection dpc;
            DataTable dtCheck;

            // 登録に変更
            this.MODE_EDIT = 1;

            // Han.TB_棚卸(TB_HN_TANAOROSHI)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 6, this.txtMizuageShishoCd.Text);
            //dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            dtCheck = this.Dba.GetDataTableByConditionWithParams(
                "MAX(TANAOROSHI_DATE) AS 最終棚卸日付",
                "TB_HN_TANAOROSHI AS A LEFT OUTER JOIN TB_HN_SHOHIN AS B ON A.KAISHA_CD = B.KAISHA_CD AND A.SHISHO_CD = B.SHISHO_CD AND A.SHOHIN_CD = B.SHOHIN_CD",
                "A.KAISHA_CD = @KAISHA_CD AND A.SHISHO_CD = @SHISHO_CD AND B.SHOHIN_KUBUN5 <> 1",
                dpc);
            // 日付範囲の和暦設定
            if (Util.ToString(dtCheck.Rows[0]["最終棚卸日付"]).Length > 0)
            {
                this.SetJp(Util.ConvJpDate(Util.ToString(dtCheck.Rows[0]["最終棚卸日付"]), this.Dba));
            }
            else
            {
                this.SetJp(Util.ConvJpDate(DateTime.Now.Date, this.Dba));
            }
            
            // 商品区分1
            this.txtShohinKbn1.Text = "0"; // 掛仕入
            this.lblShohinKbn1Nm.Text = "全　て";

            // 棚番Fr
            this.txtTanabanCdFr.Text = "";
            this.lblTanabanNmFr.Text = "先　頭";
            // 棚番To
            this.txtTanabanCdTo.Text = "";
            this.lblTanabanNmTo.Text = "最　後";

            // 削除ボタン使用不可
            this.btnF3.Enabled = false;

            // 明細情報クリア
            this.mtbList.Content.ClearRecord();

            // 設定取得
            GetSettei();
            // 対象商品区分設定
            ControlItemsBySettings();

            this.txtGengoYear.Focus();

        }

        /// <summary>
        /// 明細部の初期化
        /// </summary>
        private void InitDetailArea()
        {
            UTable.CRecordProvider rp = new UTable.CRecordProvider();
            CLayoutBuilder lb = new CLayoutBuilder(CLayoutBuilder.EOrientation.ROW);
            UTable.CFieldDesc fd;

            // 明細情報クリア
            this.mtbList.Content.ClearRecord();

#region UTableフィールド設定・配置
            //****************************************************************************************************
            // 次の行・列位置へ各フィールドを配置
            //
            //0 | 棚番| 商品ｺｰﾄﾞ |  　 　　　 |      | 現ｹｰｽ数 | 実ｹｰｽ数 | 差ｹｰｽ数 |  　      |
            //  +-----+-----------------------+------+---------+---------+---------+----------| 
            //1 |  　 |       商品名   | 規格 | 入数 | 現ﾊﾞﾗ数 | 実ﾊﾞﾗ数 | 差ﾊﾞﾗ数 | 在庫金額 |  仕入単価
            //     0      1         2      3     4        5         6         7          8          9
            //
            //(サイズ)
            //  60,    120,   120,    120,      40,   80,    80,    80,80, 1
            //****************************************************************************************************
            // １行目の設定
            // 棚番フィールド
            fd = rp.AddField("TANABAN", new CSosTextFieldProvider("棚番"), lb.Set(0, 0).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 商品ｺｰﾄﾞフィールド
            fd = rp.AddField("SHOHIN_CD", new CSosTextFieldProvider("商品ｺｰﾄﾞ"), lb.Set(0, 1).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // ブランク(商品名半分と規格)フィールド
            fd = rp.AddField("BLANK1", new CSosTextFieldProvider(), lb.Set(0, 2).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // ブランク(入数)フィールド
            fd = rp.AddField("BLANK2", new CSosTextFieldProvider(), lb.Set(0, 4).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 現ｹｰｽ数フィールド
            fd = rp.AddField("GEN_SURYO1", new CSosTextFieldProvider("現ｹｰｽ数"), lb.Set(0, 5).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 実ｹｰｽ数フィールド
            fd = rp.AddField("JITSU_SURYO1", new CSosTextFieldProvider("実ｹｰｽ数"), lb.Set(0, 6).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 差ｹｰｽ数フィールド
            fd = rp.AddField("SA_SURYO1", new CSosTextFieldProvider("差ｹｰｽ数"), lb.Set(0, 7).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // ブランク(在庫金額)フィールド
            fd = rp.AddField("BLANK3", new CSosTextFieldProvider(), lb.Set(0, 8).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // ２行目の設定
            // ブランク(棚番)フィールド
            fd = rp.AddField("BLANK4", new CSosTextFieldProvider(), lb.Set(1, 0).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 商品名フィールド
            fd = rp.AddField("SHOHIN_NM", new CSosTextFieldProvider("商品名"), lb.Set(1, 1).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 規格フィールド
            fd = rp.AddField("KIKAKU", new CSosTextFieldProvider("規格"), lb.Set(1, 3).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 入数フィールド
            fd = rp.AddField("IRISU", new CSosTextFieldProvider("入数"), lb.Set(1, 4).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 現ﾊﾞﾗ数フィールド
            fd = rp.AddField("GEN_SURYO2", new CSosTextFieldProvider("現ﾊﾞﾗ数"), lb.Set(1, 5).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 実ﾊﾞﾗ数フィールド
            fd = rp.AddField("JITSU_SURYO2", new CSosTextFieldProvider("実ﾊﾞﾗ数"), lb.Set(1, 6).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 差ﾊﾞﾗ数フィールド
            fd = rp.AddField("SA_SURYO2", new CSosTextFieldProvider("差ﾊﾞﾗ数"), lb.Set(1, 7).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 在庫金額フィールド
            fd = rp.AddField("ZAIKO_KINGAKU", new CSosTextFieldProvider("在庫金額"), lb.Set(1, 8).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // (非表示)仕入単価フィールド
            fd = rp.AddField("SHIIRE_TANKA", new CSosTextFieldProvider(), lb.Set(0, 9).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // フッダー設定
            UTable.CRecord footer = this.mtbList.FooterContent.AddRecord();
            UTable.CField ffd;
            ffd = footer.AddField("TOTAL_CAPTION", new CCaptionFieldProvider("[　合　計　]"), lb.Set(0, 0).Next(2, 8));
            ffd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            ffd.Setting.Editable = UTable.EAllow.DISABLE;
            ffd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            ffd.Setting.CaptionFont = new Font("ＭＳ 明朝", 14F, FontStyle.Regular);
            ffd = footer.AddField("TOTAL_VALUE1", new CSosTextFieldProvider(), lb.Set(0, 8).Next());
            ffd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            ffd.Setting.Editable = UTable.EAllow.DISABLE;
            ffd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            ffd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            ffd = footer.AddField("TOTAL_VALUE2", new CSosTextFieldProvider(), lb.Set(1, 8).Next());
            ffd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            ffd.Setting.Editable = UTable.EAllow.DISABLE;
            ffd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            ffd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            #endregion

            this.mtbList.Content.SetRecordProvider(rp);
            this.mtbList.Setting.CaptionBackColor = Color.LightSkyBlue;
            this.mtbList.Setting.CaptionForeColor = Color.Navy;
            this.mtbList.Setting.BackColor = SystemColors.HighlightText;

            // 列幅の変更不可
            this.mtbList.Setting.UserColResizable = UTable.EAllow.DISABLE;
            this.mtbList.CreateCaption(UTable.EHAlign.MIDDLE);
            this.mtbList.Cols.SetSize(60, 120, 120, 120, 40, 100, 100, 100, 110, 1);        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                this.txtMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDay.Text) > lastDayInMonth)
            {
                this.txtDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblGengo.Text, this.txtGengoYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtGengoYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// データ取得処理
        /// </summary>
        private void LoadData()
        {
            // 在庫金額計算用
            decimal ZaikoKingaku;
            // 在庫変動メッセージ初期化
            this.lblDifferent.Text = "";

            DbParamCollection dpc;
            DataTable dtCheck;

            Cursor.Current = Cursors.WaitCursor;
            this.pnlWait.Visible = true;
            this.pnlWait.Refresh();

            DateTime TANAOROSHI_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);

            // 棚卸確認処理
            // Han.TB_棚卸(TB_HN_TANAOROSHI)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, TANAOROSHI_DATE);
            dtCheck = this.Dba.GetDataTableByConditionWithParams(
                "COUNT(*) AS CNT",
                "TB_HN_TANAOROSHI AS A LEFT OUTER JOIN TB_HN_SHOHIN AS B ON A.KAISHA_CD = B.KAISHA_CD AND A.SHISHO_CD = B.SHISHO_CD AND A.SHOHIN_CD = B.SHOHIN_CD",
                "A.KAISHA_CD = @KAISHA_CD AND A.SHISHO_CD = @SHISHO_CD AND A.TANAOROSHI_DATE = @TANAOROSHI_DATE AND B.SHOHIN_KUBUN5 <> 1", 
                dpc);
            if (Util.ToLong(dtCheck.Rows[0]["CNT"]) == 0)
            {
                // 登録モード
                this.MODE_EDIT = 1;
                this.btnF3.Enabled = false;
            }
            else
            {
                // 修正モード
                this.MODE_EDIT = 2;
                this.btnF3.Enabled = true;
            }

            // 商品在庫から取得したデータテーブル
            DataTable dtHN_SHOHIN_ZAIKO = this.GetShohinZaiko();
            if (dtHN_SHOHIN_ZAIKO.Rows.Count == 0)
            {
                // 登録モード
                this.MODE_EDIT = 1;
                this.btnF3.Enabled = false;
                // 入力UTableレコードクリア
                this.mtbList.Content.ClearRecord();
                // 合計エリアのクリア
                this.mtbList.FooterContent.Records[0].Fields["TOTAL_VALUE2"].Value = "";
                Msg.Info("該当データがありません。");
                return;
            }

            // 取引明細から移動数量データ取得処理
            DataTable dtHN_TORIHIKI_MEISAI = this.GetIdoSuryoFromTorihikiMeisai();

            // 入出庫明細から移動数量データ取得
            DataTable dtHN_IDO_MEISAI = this.GetIdoSuryoFromIdoMeisai(TANAOROSHI_DATE);

            // 棚卸からデータ取得処理
            DataTable dtHN_TANAOROSHI = this.GetTanaoroshi();

            /// **************************************************
            /// UTableレンダリングブロック開始
            /// **************************************************
            using (this.mtbList.RenderBlock())
            {
                this.mtbList.Content.ClearRecord();         // UTableレコードクリア
                DataRow[] foundRows;
                DataRow ret = null;

                DataRow[] IdoSuryoRows;
                Decimal IdoSuryo;

                DataRow[] ZaikoSuryoRows;
                Decimal ZaikoSuryo;

                foreach (DataRow dr in dtHN_SHOHIN_ZAIKO.Rows)
                {
                    // 商品在庫をUTableレコードへセット
                    UTable.CRecord rec;
                    rec = this.mtbList.Content.AddRecord();

                    // 棚卸の商品コードを条件に取得
                    foundRows = dtHN_TANAOROSHI.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                    if (foundRows.Length == 0)
                    {
                        // 登録モード(商品在庫から主に設定)

                        // 在庫数量
                        ZaikoSuryoRows = dtHN_SHOHIN_ZAIKO.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                        if (ZaikoSuryoRows.Length > 0)
                        {
                            if (Util.ToDecimal(ZaikoSuryoRows[0]["IRISU"]) == 0 || Util.ToDecimal(ZaikoSuryoRows[0]["IRISU"]) == 1)
                            {
                                ZaikoSuryo = Util.ToDecimal(ZaikoSuryoRows[0]["SURYO2"]);
                            }
                            else
                            {
                                ZaikoSuryo = GetBaraSu(Util.ToDecimal(ZaikoSuryoRows[0]["SURYO1"]),
                                                        Util.ToDecimal(ZaikoSuryoRows[0]["SURYO2"]),
                                                        Util.ToDecimal(ZaikoSuryoRows[0]["IRISU"]));
                            }

                        }
                        else
                        {
                            ZaikoSuryo = 0;
                        }

                        // 移動数量
                        IdoSuryoRows = dtHN_TORIHIKI_MEISAI.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                        if (IdoSuryoRows.Length > 0)
                        {
                            IdoSuryo = Util.ToDecimal(IdoSuryoRows[0]["IDO_SURYO"]);
                        }
                        else
                        {
                            IdoSuryo = 0;
                        }
                        // 移動数量
                        IdoSuryoRows = dtHN_IDO_MEISAI.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                        if (IdoSuryoRows.Length > 0)
                        {
                            IdoSuryo += Util.ToDecimal(IdoSuryoRows[0]["IDO_SURYO"]);
                        }
                        else
                        {
                            IdoSuryo += 0;
                        }

                        // １行目の設定
                        // 棚番
                        rec.Fields["TANABAN"].Value = Util.ToString(dr["TANABAN"]);
                        // 商品コード
                        rec.Fields["SHOHIN_CD"].Value = Util.ToString(dr["SHOHIN_CD"]);
                        // ２行目の設定
                        // 商品名
                        rec.Fields["SHOHIN_NM"].Value = Util.ToString(dr["SHOHIN_NM"]);
                        // 規格
                        rec.Fields["KIKAKU"].Value = Util.ToString(dr["KIKAKU"]);
                        // 入数
                        SetValue(rec, "IRISU", Util.ToDecimal(dr["IRISU"]), 0);

                        decimal CsSu = 0;
                        decimal BrSu = ZaikoSuryo + IdoSuryo;
                        SetSuryo(Util.ToDecimal(ZaikoSuryoRows[0]["IRISU"]), ref CsSu, ref BrSu);
                        // 入数が２以上の場合は、ケース数量を入力可にする。
                        if (Util.ToDecimal(ZaikoSuryoRows[0]["IRISU"]) > 1)
                        {
                            UTable.CField fd;
                            fd = rec.Fields["JITSU_SURYO1"];
                            fd.Setting.Editable = UTable.EAllow.ALLOW;
                            fd.Setting.TabStop = UTable.ETabStop.STOP;
                        }

                        // 現ｹｰｽ数と実ｹｰｽ数
                        SetValue(rec, "GEN_SURYO1", CsSu, 0);
                        SetValue(rec, "JITSU_SURYO1", CsSu, 0);

                        // 差ｹｰｽ数
                        SetValue(rec, "SA_SURYO1", 0, 0);

                        // 現ﾊﾞﾗ数と実ｹｰｽ数
                        SetValue(rec, "GEN_SURYO2", BrSu, 2);
                        SetValue(rec, "JITSU_SURYO2", BrSu, 2);
                        // 差ﾊﾞﾗ数
                        SetValue(rec, "SA_SURYO2", 0, 0);

                        // 仕入単価
                        decimal tanka = 0;
                        // 最終仕入単価の取得
                        DataRow drSaishuSiireTanka = getSaishuSiireTanka(TANAOROSHI_DATE, dr);
                        tanka = Util.ToDecimal(drSaishuSiireTanka["SAISHU_SHIIRE_TANKA"]);
                        // 単価設定が仕入単価の場合、又は最終仕入単価がゼロの場合は仕入単価を設定する。
                        if (!TANKA_SETTEI_SHIIRE.Equals(gSettei.tanka) || tanka == 0)
                        {
                            tanka = Util.ToDecimal(dr["SHIIRE_TANKA"]);
                        }
                        rec.Fields["SHIIRE_TANKA"].Value = tanka;
                        // 在庫金額
                        BrSu = ZaikoSuryo + IdoSuryo;
                        ZaikoKingaku = Util.ToDecimal(rec.Fields["SHIIRE_TANKA"].Value) * BrSu;
                        SetValue(rec, "ZAIKO_KINGAKU", ZaikoKingaku, 0);

                    }
                    else
                    {
                        // 修正モード(棚卸から主に設定)
                        ret = foundRows[0];

                        decimal Irisu = 0;
                        decimal CsSu = 0;
                        decimal BrSu = 0;

                        // 在庫数量
                        ZaikoSuryoRows = dtHN_SHOHIN_ZAIKO.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                        if (ZaikoSuryoRows.Length > 0)
                        {
                            Irisu = Util.ToDecimal(ZaikoSuryoRows[0]["IRISU"]);
                            CsSu = Util.ToDecimal(ZaikoSuryoRows[0]["SURYO1"]);
                            BrSu = Util.ToDecimal(ZaikoSuryoRows[0]["SURYO2"]);
                            ZaikoSuryo = GetBaraSu(CsSu, BrSu, Irisu);
                        }
                        else
                        {
                            ZaikoSuryo = 0;
                        }

                        // 移動数量
                        IdoSuryoRows = dtHN_TORIHIKI_MEISAI.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                        if (IdoSuryoRows.Length > 0)
                        {
                            IdoSuryo = Util.ToDecimal(IdoSuryoRows[0]["IDO_SURYO"]);
                        }
                        else
                        {
                            IdoSuryo = 0;
                        }
                        // 移動数量
                        IdoSuryoRows = dtHN_IDO_MEISAI.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                        if (IdoSuryoRows.Length > 0)
                        {
                            IdoSuryo += Util.ToDecimal(IdoSuryoRows[0]["IDO_SURYO"]);
                        }
                        else
                        {
                            IdoSuryo += 0;
                        }
                        CsSu = 0;
                        BrSu = ZaikoSuryo + IdoSuryo;
                        // 在庫テーブルの現在庫数量＋移動数量と、棚卸の現在庫数量との比較
                        if (BrSu != GetBaraSu(Util.ToDecimal(ret["GEN_SURYO1"]), Util.ToDecimal(ret["GEN_SURYO2"]), Irisu))
                        {
                            this.lblDifferent.Text = "☆現在庫数量が変動しています。";
                            UTable.CField fd;
                            fd = rec.Fields["GEN_SURYO1"];
                            fd.Setting.ForeColor = Color.Red;
                            fd = rec.Fields["GEN_SURYO2"];
                            fd.Setting.ForeColor = Color.Red;
                        }
                        SetSuryo(Irisu, ref CsSu, ref BrSu);

                        // １行目の設定
                        // 棚番
                        rec.Fields["TANABAN"].Value = Util.ToString(ret["TANABAN"]);
                        // 商品コード
                        rec.Fields["SHOHIN_CD"].Value = Util.ToString(dr["SHOHIN_CD"]);
                        // ２行目の設定
                        // 商品名
                        rec.Fields["SHOHIN_NM"].Value = Util.ToString(dr["SHOHIN_NM"]);
                        // 規格
                        rec.Fields["KIKAKU"].Value = Util.ToString(dr["KIKAKU"]);
                        // 入数
                        SetValue(rec, "IRISU", Util.ToDecimal(dr["IRISU"]), 0);

                        // 入数が設定されているばあいは実ケース数量も入力可にする（但し、２以上）
                        if (Util.ToDecimal(dr["IRISU"]) > 1)
                        {
                            UTable.CField fd;
                            fd = rec.Fields["JITSU_SURYO1"];
                            fd.Setting.Editable = UTable.EAllow.ALLOW;
                            fd.Setting.TabStop = UTable.ETabStop.STOP;
                        }

                        // 数量計算
                        // 現ｹｰｽ数
                        SetValue(rec, "GEN_SURYO1", CsSu, 0);
                        SetValue(rec, "GEN_SURYO2", BrSu, 2);

                        // 実ｹｰｽ数
                        CsSu = Util.ToDecimal(ret["JITSU_SURYO1"]);
                        BrSu = Util.ToDecimal(ret["JITSU_SURYO2"]);
                        SetSuryo(Irisu, ref CsSu, ref BrSu);
                        SetValue(rec, "JITSU_SURYO1", CsSu, 0);
                        SetValue(rec, "JITSU_SURYO2", BrSu, 2);

                        // 差ｹｰｽ数
                        CsSu = Util.ToDecimal(ret["SA_SURYO1"]);
                        BrSu = Util.ToDecimal(ret["SA_SURYO2"]);
                        SetSuryo(Irisu, ref CsSu, ref BrSu);
                        SetValue(rec, "SA_SURYO1", CsSu, 0);
                        SetValue(rec, "SA_SURYO2", BrSu, 2);

                        // 仕入単価
                        decimal tanka = 0;
                        // 最終仕入単価の取得
                        DataRow drSaishuSiireTanka = getSaishuSiireTanka(TANAOROSHI_DATE, dr);
                        tanka = Util.ToDecimal(drSaishuSiireTanka["SAISHU_SHIIRE_TANKA"]);
                        // 単価設定が仕入単価の場合、又は最終仕入単価がゼロの場合は仕入単価を設定する。
                        if (!TANKA_SETTEI_SHIIRE.Equals(gSettei.tanka) || tanka == 0)
                        {
                            tanka = Util.ToDecimal(dr["SHIIRE_TANKA"]);
                        }
                        rec.Fields["SHIIRE_TANKA"].Value = tanka;

                        // 在庫金額
                        CsSu = Util.ToDecimal(ret["JITSU_SURYO1"]);
                        BrSu = Util.ToDecimal(ret["JITSU_SURYO2"]);
                        BrSu = GetBaraSu(CsSu, BrSu, Irisu);
                        ZaikoKingaku = Util.ToDecimal(rec.Fields["SHIIRE_TANKA"].Value) * BrSu;
                        SetValue(rec, "ZAIKO_KINGAKU", ZaikoKingaku, 0);
                    }

                }
            }
            /// **************************************************
            /// UTableレンダリングブロック終了
            /// **************************************************
            this.mtbList.Render();

            // 合計エリアの計算
            this.CalcTotal();

            this.pnlWait.Visible = false;
            this.Cursor = Cursors.Default;

            return;
        }

        /// <summary>
        /// 商品在庫からデータ取得処理
        /// </summary>
        /// <returns>商品在庫から取得したデータテーブル</returns>
        private DataTable GetShohinZaiko()
        {
            DbParamCollection dpc;
            StringBuilder whereSql;

            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@ZAIKO_KANRI_KUBUN", SqlDbType.Decimal, 3, 1);

            whereSql = new StringBuilder();
            whereSql.Append("KAISHA_CD = @KAISHA_CD");
            whereSql.Append(" AND SHISHO_CD = @SHISHO_CD");
            whereSql.Append(" AND SHOHIN_KUBUN5 <> 1");
            whereSql.Append(" AND ZAIKO_KANRI_KUBUN = @ZAIKO_KANRI_KUBUN");
            // 商品区分
            if (!ValChk.IsEmpty(this.txtShohinKbn1.Text) && this.txtShohinKbn1.Text != "0")
            {
                switch (gSettei.kbn)
                {
                    case 1:
                        whereSql.Append(" AND SHOHIN_KUBUN1 = @SHOHIN_KUBUN1");
                        break;
                    case 2:
                        whereSql.Append(" AND SHOHIN_KUBUN2 = @SHOHIN_KUBUN1");
                        break;
                    case 3:
                        whereSql.Append(" AND SHOHIN_KUBUN3 = @SHOHIN_KUBUN1");
                        break;
                    default:
                        break;
                }
                dpc.SetParam("@SHOHIN_KUBUN1", SqlDbType.Decimal, 6, this.txtShohinKbn1.Text);
            }

            // 棚番
            if (!ValChk.IsEmpty(this.txtTanabanCdFr.Text))
            {
                whereSql.Append(" AND TANABAN >= @TANABAN_FR");
                dpc.SetParam("@TANABAN_FR", SqlDbType.VarChar, 6, this.txtTanabanCdFr.Text);
            }
            if (!ValChk.IsEmpty(this.txtTanabanCdTo.Text))
            {
                whereSql.Append(" AND TANABAN <= @TANABAN_TO");
                dpc.SetParam("@TANABAN_TO", SqlDbType.VarChar, 6, this.txtTanabanCdTo.Text);
            }
            // 中止区分
            whereSql.Append(" AND CHUSHI_KUBUN = 1");
            string groupsort = "";
            switch (gSettei.sort)
            {
                case 1:
                    groupsort = " SHOHIN_CD";
                    break;
                case 2:
                    groupsort = " TANABAN, SHOHIN_CD";
                    break;
                case 3:
                    groupsort = " TANABAN, SHOHIN_KUBUN1, SHOHIN_CD";
                    break;
                case 4:
                    groupsort = " TANABAN, SHOHIN_KUBUN1, SHOHIN_KUBUN2, SHOHIN_CD ";
                    break;
            }

            DataTable dtHN_SHOHIN_ZAIKO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_HN_SHOHIN_ZAIKO",
                Util.ToString(whereSql), groupsort,
                dpc);

            return dtHN_SHOHIN_ZAIKO;

        }

        /// <summary>
        /// 取引明細から移動数量データ取得処理
        /// </summary>
        /// <returns>取引明細から移動数量取得したデータテーブル</returns>
        private DataTable GetIdoSuryoFromTorihikiMeisai()
        {
            DbParamCollection dpc;
            StringBuilder sql;

            DateTime TANAOROSHI_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);

            sql = new StringBuilder();
            dpc = new DbParamCollection();
            sql.Append("SELECT");
            switch (gSettei.sort)
            {
                case 1:
                    sql.Append("  A.SHOHIN_CD AS SHOHIN_CD,");
                    sql.Append("  MAX(C.TANABAN) AS TANABAN,");
                    break;
                case 2:
                    sql.Append("  C.TANABAN AS TANABAN,");
                    sql.Append("  A.SHOHIN_CD AS SHOHIN_CD,");
                    break;
                case 3:
                    sql.Append("  C.TANABAN AS TANABAN,");
                    sql.Append("  A.SHOHIN_CD AS SHOHIN_CD,");
                    sql.Append("  C.SHOHIN_KUBUN1,");
                    break;
                case 4:
                    sql.Append("  C.TANABAN AS TANABAN,");
                    sql.Append("  A.SHOHIN_CD AS SHOHIN_CD,");
                    sql.Append("  C.SHOHIN_KUBUN1,");
                    sql.Append("  C.SHOHIN_KUBUN2,");
                    break;
            }
            sql.Append("  SUM( CASE WHEN B.DENPYO_KUBUN = 1 THEN ");
            sql.Append("        (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("          (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) ");
            sql.Append("         ELSE ((A.SURYO1 * A.IRISU) + A.SURYO2) END");
            sql.Append("        ) ");
            sql.Append("       ELSE ");
            sql.Append("         (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("          ((A.SURYO1 * A.IRISU) + A.SURYO2) ");
            sql.Append("          ELSE (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) END");
            sql.Append("         ) ");
            sql.Append("       END ");
            sql.Append("  ) AS IDO_SURYO ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_TORIHIKI_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("    TB_HN_TORIHIKI_DENPYO AS B ");
            sql.Append("ON     A.KAISHA_CD     = B.KAISHA_CD ");
            sql.Append("   AND A.SHISHO_CD     = B.SHISHO_CD ");
            sql.Append("   AND A.DENPYO_KUBUN  = B.DENPYO_KUBUN ");
            sql.Append("   AND A.DENPYO_BANGO  = B.DENPYO_BANGO ");
            sql.Append("   AND A.KAIKEI_NENDO  = B.KAIKEI_NENDO ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("    TB_HN_SHOHIN AS C ");
            sql.Append("ON     A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("   AND A.SHISHO_CD = C.SHISHO_CD ");
            sql.Append("   AND A.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append("WHERE ");
            sql.Append("       A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("   AND A.SHISHO_CD = @SHISHO_CD ");
            sql.Append("   AND C.SHOHIN_KUBUN5 <> 1 ");
            // 商品区分
            if (!ValChk.IsEmpty(this.txtShohinKbn1.Text) && this.txtShohinKbn1.Text != "0")
            {
                switch (gSettei.kbn)
                {
                    case 1:
                        sql.Append(" AND C.SHOHIN_KUBUN1 = @SHOHIN_KUBUN1");
                        break;
                    case 2:
                        sql.Append(" AND C.SHOHIN_KUBUN2 = @SHOHIN_KUBUN1");
                        break;
                    case 3:
                        sql.Append(" AND C.SHOHIN_KUBUN3 = @SHOHIN_KUBUN1");
                        break;
                    default:
                        break;
                }
                dpc.SetParam("@SHOHIN_KUBUN1", SqlDbType.Decimal, 6, this.txtShohinKbn1.Text);
            }

            sql.Append("   AND B.DENPYO_DATE  > @DENPYO_DATE ");
            sql.Append("   AND C.ZAIKO_KANRI_KUBUN = @ZAIKO_KANRI_KUBUN ");
            string groupsort = "";
            switch (gSettei.sort)
            {
                case 1:
                    groupsort = " A.SHOHIN_CD ";
                    break;
                case 2:
                    groupsort = " C.TANABAN, A.SHOHIN_CD ";
                    break;
                case 3:
                    groupsort = " C.TANABAN, C.SHOHIN_KUBUN1, A.SHOHIN_CD ";
                    break;
                case 4:
                    groupsort = " C.TANABAN, C.SHOHIN_KUBUN1, C.SHOHIN_KUBUN2, A.SHOHIN_CD ";
                    break;
            }
            sql.Append("GROUP BY ");
            sql.Append(groupsort);
            sql.Append("ORDER BY ");
            sql.Append(groupsort);

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, TANAOROSHI_DATE);
            dpc.SetParam("@ZAIKO_KANRI_KUBUN", SqlDbType.Decimal, 3, 1);
            DataTable dtHN_TORIHIKI_MEISAI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtHN_TORIHIKI_MEISAI;
        }

        /// <summary>
        /// 取引明細から移動数量データ取得処理
        /// </summary>
        /// <returns>取引明細から移動数量取得したデータテーブル</returns>
        private DataTable GetIdoSuryoFromIdoMeisai(DateTime TANAOROSHI_DATE)
        {
            StringBuilder sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            sql.Append("SELECT");
            switch (gSettei.sort)
            {
                case 1:
                    sql.Append("  A.SHOHIN_CD AS SHOHIN_CD,");
                    sql.Append("  MAX(C.TANABAN) AS TANABAN,");
                    break;
                case 2:
                    sql.Append("  C.TANABAN AS TANABAN,");
                    sql.Append("  A.SHOHIN_CD AS SHOHIN_CD,");
                    break;
                case 3:
                    sql.Append("  C.TANABAN AS TANABAN,");
                    sql.Append("  A.SHOHIN_CD AS SHOHIN_CD,");
                    sql.Append("  C.SHOHIN_KUBUN1,");
                    break;
                case 4:
                    sql.Append("  C.TANABAN AS TANABAN,");
                    sql.Append("  A.SHOHIN_CD AS SHOHIN_CD,");
                    sql.Append("  C.SHOHIN_KUBUN1,");
                    sql.Append("  C.SHOHIN_KUBUN2,");
                    break;
            }
            sql.Append("  SUM( CASE WHEN B.DENPYO_KUBUN = 1 THEN ");
            sql.Append("        (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("          (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) ");
            sql.Append("         ELSE ((A.SURYO1 * A.IRISU) + A.SURYO2) END");
            sql.Append("        ) ");
            sql.Append("       ELSE ");
            sql.Append("         (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("          ((A.SURYO1 * A.IRISU) + A.SURYO2) ");
            sql.Append("          ELSE (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) END");
            sql.Append("         ) ");
            sql.Append("       END ");
            sql.Append("  ) AS IDO_SURYO ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_NYUSHUKKO_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("    TB_HN_NYUSHUKKO_DENPYO AS B ");
            sql.Append("ON     A.KAISHA_CD     = B.KAISHA_CD ");
            sql.Append("   AND A.SHISHO_CD     = B.SHISHO_CD ");
            sql.Append("   AND A.DENPYO_KUBUN  = B.DENPYO_KUBUN ");
            sql.Append("   AND A.DENPYO_BANGO  = B.DENPYO_BANGO ");
            sql.Append("   AND A.KAIKEI_NENDO  = B.KAIKEI_NENDO ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("    TB_HN_SHOHIN AS C ");
            sql.Append("ON     A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("   AND A.SHISHO_CD = C.SHISHO_CD ");
            sql.Append("   AND A.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append("WHERE ");
            sql.Append("       A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("   AND A.SHISHO_CD = @SHISHO_CD ");
            sql.Append("   AND C.SHOHIN_KUBUN5 <> 1 ");
            // 商品区分
            if (!ValChk.IsEmpty(this.txtShohinKbn1.Text) && this.txtShohinKbn1.Text != "0")
            {
                switch (gSettei.kbn)
                {
                    case 1:
                        sql.Append(" AND C.SHOHIN_KUBUN1 = @SHOHIN_KUBUN1");
                        break;
                    case 2:
                        sql.Append(" AND C.SHOHIN_KUBUN2 = @SHOHIN_KUBUN1");
                        break;
                    case 3:
                        sql.Append(" AND C.SHOHIN_KUBUN3 = @SHOHIN_KUBUN1");
                        break;
                    default:
                        break;
                }
                dpc.SetParam("@SHOHIN_KUBUN1", SqlDbType.Decimal, 6, this.txtShohinKbn1.Text);
            }

            sql.Append("   AND B.DENPYO_DATE  > @DENPYO_DATE ");
            sql.Append("   AND C.ZAIKO_KANRI_KUBUN = @ZAIKO_KANRI_KUBUN ");
            string groupsort = "";
            switch (gSettei.sort)
            {
                case 1:
                    groupsort = " A.SHOHIN_CD ";
                    break;
                case 2:
                    groupsort = " C.TANABAN, A.SHOHIN_CD ";
                    break;
                case 3:
                    groupsort = " C.TANABAN, C.SHOHIN_KUBUN1, A.SHOHIN_CD ";
                    break;
                case 4:
                    groupsort = " C.TANABAN, C.SHOHIN_KUBUN1, C.SHOHIN_KUBUN2, A.SHOHIN_CD ";
                    break;
            }
            sql.Append("GROUP BY ");
            sql.Append(groupsort);
            sql.Append("ORDER BY ");
            sql.Append(groupsort);

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, TANAOROSHI_DATE);
            dpc.SetParam("@ZAIKO_KANRI_KUBUN", SqlDbType.Decimal, 3, 1);
            DataTable dtHN_IDO_MEISAI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtHN_IDO_MEISAI;
        }

        /// <summary>
        /// 棚卸からデータ取得処理
        /// </summary>
        /// <returns>棚卸から取得したデータテーブル</returns>
        private DataTable GetTanaoroshi()
        {
            DbParamCollection dpc;
            StringBuilder sql;

            DateTime TANAOROSHI_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);

            sql = new StringBuilder();
            dpc = new DbParamCollection();
            sql.Append("SELECT ");
            sql.Append("    A.KAISHA_CD       AS KAISHA_CD,");
            sql.Append("    A.TANAOROSHI_DATE AS TANAOROSHI_DATE,");
            sql.Append("    A.SHOHIN_CD       AS SHOHIN_CD,");
            sql.Append("    A.GEN_SURYO1      AS GEN_SURYO1,");
            sql.Append("    A.GEN_SURYO2      AS GEN_SURYO2,");
            sql.Append("    A.JITSU_SURYO1    AS JITSU_SURYO1,");
            sql.Append("    A.JITSU_SURYO2    AS JITSU_SURYO2,");
            sql.Append("    A.SA_SURYO1       AS SA_SURYO1,");
            sql.Append("    A.SA_SURYO2       AS SA_SURYO2,");
            sql.Append("    B.TANABAN         AS TANABAN ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_TANAOROSHI  AS A ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("    TB_HN_SHOHIN  AS B ");
            sql.Append("ON      A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("    AND A.SHISHO_CD = B.SHISHO_CD ");
            sql.Append("    AND A.SHOHIN_CD = B.SHOHIN_CD ");
            sql.Append("WHERE ");
            sql.Append("        A.KAISHA_CD       = @KAISHA_CD ");
            sql.Append("    AND A.SHISHO_CD       = @SHISHO_CD ");
            sql.Append("    AND B.SHOHIN_KUBUN5 <> 1");
            // 商品区分
            if (!ValChk.IsEmpty(this.txtShohinKbn1.Text) && this.txtShohinKbn1.Text != "0")
            {
                switch (gSettei.kbn)
                {
                    case 1:
                        sql.Append(" AND B.SHOHIN_KUBUN1 = @SHOHIN_KUBUN1");
                        break;
                    case 2:
                        sql.Append(" AND B.SHOHIN_KUBUN2 = @SHOHIN_KUBUN1");
                        break;
                    case 3:
                        sql.Append(" AND B.SHOHIN_KUBUN3 = @SHOHIN_KUBUN1");
                        break;
                    default:
                        break;
                }
                dpc.SetParam("@SHOHIN_KUBUN1", SqlDbType.Decimal, 6, this.txtShohinKbn1.Text);
            }

            sql.Append("    AND A.TANAOROSHI_DATE = @TANAOROSHI_DATE ");
            //sql.Append("ORDER BY ");
            //sql.Append("    B.TANABAN,");
            //sql.Append("    A.SHOHIN_CD");
            string groupsort = "";
            switch (gSettei.sort)
            {
                case 1:
                    groupsort = " A.SHOHIN_CD ";
                    break;
                case 2:
                    groupsort = " B.TANABAN, A.SHOHIN_CD ";
                    break;
                case 3:
                    groupsort = " B.TANABAN, B.SHOHIN_KUBUN1, A.SHOHIN_CD ";
                    break;
                case 4:
                    groupsort = " B.TANABAN, B.SHOHIN_KUBUN1, B.SHOHIN_KUBUN2, A.SHOHIN_CD ";
                    break;
            }
            sql.Append("ORDER BY ");
            sql.Append(groupsort);

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, TANAOROSHI_DATE);
            DataTable dtHN_TANAOROSHI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtHN_TANAOROSHI;
        }

        /// <summary>
        /// 入力チェック処理
        /// </summary>
        private bool ValidateAll()
        {
            return true;
        }

        /// <summary>
        /// 登録処理
        /// </summary>
        private void UpdateData()
        {
            DbParamCollection dpc;
            DataTable dtCheck;
            DbParamCollection updParam;
            DbParamCollection whereParam;

            DateTime TANAOROSHI_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            
            int i;
            
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 棚卸明細(UTable[name=mtbList]を取得)
                i = 0;
                while (this.mtbList.Content.Records.Count > i)
                {
                    UTable.CRecord rec = mtbList.Content.Records[i];
                    // Han.TB_棚卸(TB_HN_TANAOROSHI)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                    dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(rec.Fields["SHOHIN_CD"].Value));
                    dpc.SetParam("@SHOHIN_KUBUN5", SqlDbType.Decimal, 4, 1);

                    dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, TANAOROSHI_DATE);
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "COUNT(*) AS CNT",
                        "TB_HN_TANAOROSHI A LEFT OUTER JOIN TB_HN_SHOHIN B ON A.KAISHA_CD = B.KAISHA_CD AND A.SHISHO_CD = B.SHISHO_CD AND A.SHOHIN_CD = B.SHOHIN_CD",
                        "A.KAISHA_CD = @KAISHA_CD AND A.SHISHO_CD = @SHISHO_CD AND A.TANAOROSHI_DATE = @TANAOROSHI_DATE AND A.SHOHIN_CD = @SHOHIN_CD AND B.SHOHIN_KUBUN5 <> @SHOHIN_KUBUN5",
                        dpc);
                    if ((int)dtCheck.Rows[0]["CNT"] == 0)
                    {
                        // 棚卸登録
                        updParam = new DbParamCollection();

                        updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                        updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                        updParam.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, TANAOROSHI_DATE);
                        updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(rec.Fields["SHOHIN_CD"].Value));
                        updParam.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, Util.ToDecimal(0));
                        updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
                        updParam.SetParam("@GEN_SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["GEN_SURYO1"].Value));
                        updParam.SetParam("@GEN_SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["GEN_SURYO2"].Value));
                        updParam.SetParam("@JITSU_SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["JITSU_SURYO1"].Value));
                        updParam.SetParam("@JITSU_SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["JITSU_SURYO2"].Value));
                        updParam.SetParam("@SA_SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["SA_SURYO1"].Value));
                        updParam.SetParam("@SA_SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["SA_SURYO2"].Value));
                        updParam.SetParam("@TANKA", SqlDbType.Decimal, 15, Util.ToDecimal(rec.Fields["SHIIRE_TANKA"].Value));
                        updParam.SetParam("@IRISU", SqlDbType.Decimal, 4, Util.ToDecimal(rec.Fields["IRISU"].Value));
                        updParam.SetParam("@KINGAKU", SqlDbType.Decimal, 15, Util.ToDecimal(rec.Fields["ZAIKO_KINGAKU"].Value));
                        updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

                        this.Dba.Insert("TB_HN_TANAOROSHI", updParam);
                    }
                    else
                    {
                        // 棚卸更新
                        updParam = new DbParamCollection();
                        whereParam = new DbParamCollection();

                        updParam.SetParam("@GEN_SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["GEN_SURYO1"].Value));
                        updParam.SetParam("@GEN_SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["GEN_SURYO2"].Value));
                        updParam.SetParam("@JITSU_SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["JITSU_SURYO1"].Value));
                        updParam.SetParam("@JITSU_SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["JITSU_SURYO2"].Value));
                        updParam.SetParam("@SA_SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["SA_SURYO1"].Value));
                        updParam.SetParam("@SA_SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(rec.Fields["SA_SURYO2"].Value));
                        updParam.SetParam("@TANKA", SqlDbType.Decimal, 15, Util.ToDecimal(rec.Fields["SHIIRE_TANKA"].Value));
                        updParam.SetParam("@IRISU", SqlDbType.Decimal, 4, Util.ToDecimal(rec.Fields["IRISU"].Value));
                        updParam.SetParam("@KINGAKU", SqlDbType.Decimal, 15, Util.ToDecimal(rec.Fields["ZAIKO_KINGAKU"].Value));
                        updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

                        whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                        whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(rec.Fields["SHOHIN_CD"].Value));
                        whereParam.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, TANAOROSHI_DATE);
                        whereParam.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, Util.ToDecimal(0));

                        this.Dba.Update("TB_HN_TANAOROSHI",
                                        updParam,
                                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND TANAOROSHI_DATE = @TANAOROSHI_DATE AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD",
                                        whereParam
                                        );
                    }

                    i++;
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 削除処理
        /// </summary>
        private void DeleteData()
        {
            DbParamCollection whereParam;

            DateTime TANAOROSHI_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 棚卸削除
                // Han.TB_棚卸(TB_HN_TANAOROSHI)
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                whereParam.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, TANAOROSHI_DATE);
                this.Dba.Delete("TB_HN_TANAOROSHI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND TANAOROSHI_DATE = @TANAOROSHI_DATE",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

        }

        /// <summary>
        /// 合計エリアの計算
        /// </summary>
        private void CalcTotal()
        {
            decimal Total = 0;
            
            for (int i = 0; i < this.mtbList.Content.Records.Count; i++)
            {
                UTable.CRecord rec = this.mtbList.Content.Records[i];
                Total += Util.ToDecimal(Util.ToString(rec.Fields["ZAIKO_KINGAKU"].Value));
                
            }
            // 合計情報
            if (Total == 0)
            {
                this.mtbList.FooterContent.Records[0].Fields["TOTAL_VALUE2"].Value = "";
            }
            else
            {
                this.mtbList.FooterContent.Records[0].Fields["TOTAL_VALUE2"].Value = Util.FormatNum(Total);
            }
        }

        private void SetValue(UTable.CRecord rec, string FieldNm, decimal Suryo, int pos)
        {
            UTable.CField fd;
            fd = rec.Fields[FieldNm];
            if (Suryo < 0)
            {
                fd.Setting.ForeColor = Color.Red;
            }
            else
            {
                fd.Setting.ForeColor = Color.Black;
            }
            rec.Fields[FieldNm].Value = SetDisp(Suryo, pos);
        }

        #endregion

        #region UTableイベント
        /// <summary>
        /// フォーカス移動時
        /// </summary>
        /// <param name="field"></param>
        private void mtbList_FieldEnter(UTable.CField field)
        {
            // 編集モード開始のためF2キー送信
            this.mtbList.StartEdit();

            // グリッドに来た場合はF1不可
            this.btnF1.Enabled = false;
            /*
            if (this.MenuFrm != null)
                ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = false;
            */
        }

        /// <summary>
        /// カスタムエディタの初期化 
        /// </summary>
        private void mtbList_InitializeEditor(UTable.CField field, IEditor editor)
        {
            // 入力制限・IMEモード
            FsiTextBox ed = (FsiTextBox)editor;
            switch (Util.ToString(field.Key))
            {
                case "JITSU_SURYO1":
                case "JITSU_SURYO2":
                    ed.MaxLength = 15;
                    ed.ImeMode = ImeMode.Off;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        ///  編集モードの開始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mtbList_EditStart(UTable.CField field, IEditor editor)
        {
            // 編集値フォーマット
            FsiTextBox ed = (FsiTextBox)editor;
            switch (Util.ToString(field.Key))
            {
                case "JITSU_SURYO1":
                    if (Util.ToString(field.Value).Length == 0)
                    {
                        ed.Text = "";
                    }
                    else
                    {
                        ed.Text = Util.ToString(Util.ToDecimal(field.Value));
                    }
                    break;
                case "JITSU_SURYO2":
                    if (Util.ToString(field.Value).Length == 0)
                    {
                        ed.Text = "";
                    }
                    else
                    {
                        ed.Text = Util.ToString(Util.ToDecimal(field.Value));
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// フィールド値変更時
        /// </summary>
        /// <param name="field"></param>
        /// <param name="e"></param>
        private void mtbList_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!ValChk.IsDecNumWithinLength(field.Value, 15, 2, true))
            {
                field.Value = Regex.Replace(Util.ToString(field.Value), "^[a-zA-Z]+$", "");
                e.Cancel = true;
            }
            decimal wCsSu = 0;
            decimal wBrSu = 0;

            switch (Util.ToString(field.Key))
            {
                case "JITSU_SURYO1":
                    wCsSu = Util.ToDecimal(field.Value);
                    wBrSu = Util.ToDecimal(field.Record.Fields["JITSU_SURYO2"].Value);
                    break;
                case "JITSU_SURYO2":
                    wCsSu = Util.ToDecimal(field.Record.Fields["JITSU_SURYO1"].Value);
                    wBrSu = Util.ToDecimal(field.Value); 
                    break;
                default:
                    break;
            }
            SetSuryo(Util.ToDecimal(field.Record.Fields["IRISU"].Value), ref wCsSu, ref wBrSu);
            SetValue(field.Record, "JITSU_SURYO1", wCsSu, 0);
            SetValue(field.Record, "JITSU_SURYO2", wBrSu, 2);

            SetSuryo(field);
        }

        /// <summary>
        /// フォーカス喪失時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mtbList_Leave(object sender, EventArgs e)
        {
            mtbList.FocusField = null;      // フォーカス位置をクリア
        }
#endregion

        /// <summary>
        /// UTableイベント内、数量設定メソッド
        /// </summary>
        /// <param name="field"></param>
        private void SetSuryo(UTable.CField field)
        {
            decimal jCsSu = Util.ToDecimal(field.Record.Fields["JITSU_SURYO1"].Value);
            decimal jBrSu = Util.ToDecimal(field.Record.Fields["JITSU_SURYO2"].Value);
            decimal gCsSu = Util.ToDecimal(field.Record.Fields["GEN_SURYO1"].Value);
            decimal gBrSu = Util.ToDecimal(field.Record.Fields["GEN_SURYO2"].Value);
            decimal Irisu = Util.ToDecimal(field.Record.Fields["IRISU"].Value);
            decimal Tanka = Util.ToDecimal(field.Record.Fields["SHIIRE_TANKA"].Value);
            
            //差数量
            decimal wDbl = GetBaraSu(jCsSu, jBrSu, Irisu) - GetBaraSu(gCsSu, gBrSu, Irisu);
            //
            if(Irisu == 0 || Irisu == 1)
            {
                field.Record.Fields["SA_SURYO1"].Value = "";
                field.Record.Fields["SA_SURYO2"].Value = SetDisp(wDbl, 2);
            }
            else
            {
                decimal wCsSu = TaxUtil.CalcFraction(wDbl / Irisu, 0, TaxUtil.ROUND_CATEGORY.DOWN);
                field.Record.Fields["SA_SURYO1"].Value = SetDisp(wCsSu,　0);
                field.Record.Fields["SA_SURYO2"].Value = SetDisp(wDbl - (wCsSu * Irisu), 2);

            }
            wDbl = GetBaraSu(jCsSu, jBrSu, Irisu);
            decimal ZaikoKingaku = TaxUtil.CalcFraction(wDbl * Tanka, 0, TaxUtil.ROUND_CATEGORY.ADJUST);
            field.Record.Fields["ZAIKO_KINGAKU"].Value = SetDisp(ZaikoKingaku, 0);
            // 合計エリアの計算
            this.CalcTotal();
        }

        /// <summary>
        /// 【共通】入数、ケース数、バラ数の再計算
        /// </summary>
        /// <param name="pIriSu"></param>
        /// <param name="pCsSu"></param>
        /// <param name="pBrSu"></param>
        private void SetSuryo(decimal pIriSu, ref decimal pCsSu, ref decimal pBrSu)
        {
            decimal wkCsSu;
            decimal wkSu;
            if (pIriSu > 1)
            {
                wkSu = pCsSu * pIriSu + pBrSu;
                wkCsSu = (wkSu / pIriSu);
                pCsSu = TaxUtil.CalcFraction(wkCsSu, 0, TaxUtil.ROUND_CATEGORY.DOWN);
                pBrSu = wkSu - (pCsSu * pIriSu);
            }
            else
            {
                wkSu = pCsSu + pBrSu;
                pCsSu = 0;
                pBrSu = wkSu;
            }
        }

        /// <summary>
        /// 【共通】入数、ケース数量、バラ数量から総バラ数を返す
        /// </summary>
        /// <param name="CsSu"></param>
        /// <param name="BrSu"></param>
        /// <param name="Irisu"></param>
        /// <returns></returns>
        private decimal GetBaraSu(decimal CsSu, decimal BrSu, decimal Irisu)
        {
            decimal wSu;

            wSu = CsSu * Irisu + BrSu;
            return wSu;
        }

        private string SetDisp(decimal Su, int Places)
        {
            string ret;
            if (Su == 0)
            {
                ret = "";
            }
            else
            {
                ret = Util.FormatNum(Su, Places);
            }
            return ret;
        }

        /// <summary>
        /// 棚卸日付と商品コード（データレコードセット）より、棚卸日付以前の
        /// 最も最新の仕入単価レコードを返す
        /// </summary>
        /// <param name="tanaoroshiDate"></param>
        /// <param name="drShohinZaiko"></param>
        /// <returns></returns>
        private DataRow getSaishuSiireTanka(DateTime tanaoroshiDate, DataRow drShohinZaiko)
        {
            #region 最終仕入単価選択時 取得用SQL
            // の検索日付に発生しているデータを取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            Sql.Append(" EXEC SP_SAISHU_SHIIRE_TANKA @KAISHA_CD, @SHISHO_CD, @SHOHIN_CD, @TANAOROSHI_DATE");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            // 検索する棚卸日付をセット
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.VarChar, 10, tanaoroshiDate);
            // 検索する商品コードをセット
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 6, drShohinZaiko["SHOHIN_CD"]);
            DataTable dtSaishushiireTanka = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 最終仕入単価を取得
            DataRow drSaishushiireTanka;
            if (dtSaishushiireTanka.Rows.Count != 0)
            {
                drSaishushiireTanka = dtSaishushiireTanka.Rows[0];
            }
            else
            {
                drSaishushiireTanka = drShohinZaiko;
            }
            #endregion
            return drSaishushiireTanka;
        }

        /// <summary>
        /// Config.xmlより棚卸関連の設定を取得
        /// </summary>
        private void GetSettei()
        {
            gSettei.Clear();
            // 使用する商品区分取得
            gSettei.kbn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "ShohinKbn"));
            // 在庫金額計算用単価
            gSettei.tanka = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "UseTanka"));
            // 棚卸表・記入票の合計印字設定
            gSettei.gokei = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "PrintGokei"));
            // 棚卸入力ソート順
            gSettei.sort = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "Sort"));
        }
        /// <summary>
        /// 設定内容によって項目の使用可否を再度制御します。
        /// </summary>
        private void ControlItemsBySettings()
        {
            if (gSettei.kbn == 1)
            {
                this.lblShohinKbn1.Text = "商品区分1";
            }
            else if (gSettei.kbn == 2)
            {
                this.lblShohinKbn1.Text = "商品区分2";
            }
            else
            {
                this.lblShohinKbn1.Text = "商品分類";
            }
        }
    }

}
