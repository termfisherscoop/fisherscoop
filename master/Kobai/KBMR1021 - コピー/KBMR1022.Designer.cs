﻿namespace jp.co.fsi.kb.kbmr1021
{
    partial class KBMR1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdoShohinBunrui3 = new System.Windows.Forms.RadioButton();
            this.rdoShohinKubun2 = new System.Windows.Forms.RadioButton();
            this.rdoShohinKubun1 = new System.Windows.Forms.RadioButton();
            this.rdoInjiAri = new System.Windows.Forms.RadioButton();
            this.rdoInjiNashi = new System.Windows.Forms.RadioButton();
            this.rdoSaishuSireTanka = new System.Windows.Forms.RadioButton();
            this.rdoShohinMasutaSireTanka = new System.Windows.Forms.RadioButton();
            this.rdoSort4 = new System.Windows.Forms.RadioButton();
            this.rdoSort3 = new System.Windows.Forms.RadioButton();
            this.rdoSort2 = new System.Windows.Forms.RadioButton();
            this.rdoSort1 = new System.Windows.Forms.RadioButton();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 512);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1042, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1050, 31);
            this.lblTitle.Visible = false;
            // 
            // rdoShohinBunrui3
            // 
            this.rdoShohinBunrui3.AutoSize = true;
            this.rdoShohinBunrui3.BackColor = System.Drawing.Color.Silver;
            this.rdoShohinBunrui3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShohinBunrui3.Location = new System.Drawing.Point(366, 4);
            this.rdoShohinBunrui3.Margin = new System.Windows.Forms.Padding(4);
            this.rdoShohinBunrui3.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoShohinBunrui3.Name = "rdoShohinBunrui3";
            this.rdoShohinBunrui3.Size = new System.Drawing.Size(90, 24);
            this.rdoShohinBunrui3.TabIndex = 2;
            this.rdoShohinBunrui3.Tag = "CHANGE";
            this.rdoShohinBunrui3.Text = "商品分類";
            this.rdoShohinBunrui3.UseVisualStyleBackColor = false;
            // 
            // rdoShohinKubun2
            // 
            this.rdoShohinKubun2.AutoSize = true;
            this.rdoShohinKubun2.BackColor = System.Drawing.Color.Silver;
            this.rdoShohinKubun2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShohinKubun2.Location = new System.Drawing.Point(260, 4);
            this.rdoShohinKubun2.Margin = new System.Windows.Forms.Padding(4);
            this.rdoShohinKubun2.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoShohinKubun2.Name = "rdoShohinKubun2";
            this.rdoShohinKubun2.Size = new System.Drawing.Size(98, 24);
            this.rdoShohinKubun2.TabIndex = 1;
            this.rdoShohinKubun2.Tag = "CHANGE";
            this.rdoShohinKubun2.Text = "商品区分2";
            this.rdoShohinKubun2.UseVisualStyleBackColor = false;
            // 
            // rdoShohinKubun1
            // 
            this.rdoShohinKubun1.AutoSize = true;
            this.rdoShohinKubun1.BackColor = System.Drawing.Color.Silver;
            this.rdoShohinKubun1.Checked = true;
            this.rdoShohinKubun1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShohinKubun1.Location = new System.Drawing.Point(154, 4);
            this.rdoShohinKubun1.Margin = new System.Windows.Forms.Padding(4);
            this.rdoShohinKubun1.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoShohinKubun1.Name = "rdoShohinKubun1";
            this.rdoShohinKubun1.Size = new System.Drawing.Size(98, 24);
            this.rdoShohinKubun1.TabIndex = 0;
            this.rdoShohinKubun1.TabStop = true;
            this.rdoShohinKubun1.Tag = "CHANGE";
            this.rdoShohinKubun1.Text = "商品区分1";
            this.rdoShohinKubun1.UseVisualStyleBackColor = false;
            // 
            // rdoInjiAri
            // 
            this.rdoInjiAri.AutoSize = true;
            this.rdoInjiAri.BackColor = System.Drawing.Color.Silver;
            this.rdoInjiAri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoInjiAri.Location = new System.Drawing.Point(220, 4);
            this.rdoInjiAri.Margin = new System.Windows.Forms.Padding(4);
            this.rdoInjiAri.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoInjiAri.Name = "rdoInjiAri";
            this.rdoInjiAri.Size = new System.Drawing.Size(58, 24);
            this.rdoInjiAri.TabIndex = 1;
            this.rdoInjiAri.Tag = "CHANGE";
            this.rdoInjiAri.Text = "あり";
            this.rdoInjiAri.UseVisualStyleBackColor = false;
            // 
            // rdoInjiNashi
            // 
            this.rdoInjiNashi.AutoSize = true;
            this.rdoInjiNashi.BackColor = System.Drawing.Color.Silver;
            this.rdoInjiNashi.Checked = true;
            this.rdoInjiNashi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoInjiNashi.Location = new System.Drawing.Point(154, 4);
            this.rdoInjiNashi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoInjiNashi.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoInjiNashi.Name = "rdoInjiNashi";
            this.rdoInjiNashi.Size = new System.Drawing.Size(58, 24);
            this.rdoInjiNashi.TabIndex = 0;
            this.rdoInjiNashi.TabStop = true;
            this.rdoInjiNashi.Tag = "CHANGE";
            this.rdoInjiNashi.Text = "なし";
            this.rdoInjiNashi.UseVisualStyleBackColor = false;
            // 
            // rdoSaishuSireTanka
            // 
            this.rdoSaishuSireTanka.AutoSize = true;
            this.rdoSaishuSireTanka.BackColor = System.Drawing.Color.Silver;
            this.rdoSaishuSireTanka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSaishuSireTanka.Location = new System.Drawing.Point(344, 5);
            this.rdoSaishuSireTanka.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSaishuSireTanka.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoSaishuSireTanka.Name = "rdoSaishuSireTanka";
            this.rdoSaishuSireTanka.Size = new System.Drawing.Size(122, 24);
            this.rdoSaishuSireTanka.TabIndex = 1;
            this.rdoSaishuSireTanka.Tag = "CHANGE";
            this.rdoSaishuSireTanka.Text = "最終仕入単価";
            this.rdoSaishuSireTanka.UseVisualStyleBackColor = false;
            // 
            // rdoShohinMasutaSireTanka
            // 
            this.rdoShohinMasutaSireTanka.AutoSize = true;
            this.rdoShohinMasutaSireTanka.BackColor = System.Drawing.Color.Silver;
            this.rdoShohinMasutaSireTanka.Checked = true;
            this.rdoShohinMasutaSireTanka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShohinMasutaSireTanka.Location = new System.Drawing.Point(154, 5);
            this.rdoShohinMasutaSireTanka.Margin = new System.Windows.Forms.Padding(4);
            this.rdoShohinMasutaSireTanka.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoShohinMasutaSireTanka.Name = "rdoShohinMasutaSireTanka";
            this.rdoShohinMasutaSireTanka.Size = new System.Drawing.Size(186, 24);
            this.rdoShohinMasutaSireTanka.TabIndex = 0;
            this.rdoShohinMasutaSireTanka.TabStop = true;
            this.rdoShohinMasutaSireTanka.Tag = "CHANGE";
            this.rdoShohinMasutaSireTanka.Text = "商品マスター仕入単価";
            this.rdoShohinMasutaSireTanka.UseVisualStyleBackColor = false;
            // 
            // rdoSort4
            // 
            this.rdoSort4.AutoSize = true;
            this.rdoSort4.BackColor = System.Drawing.Color.Silver;
            this.rdoSort4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSort4.Location = new System.Drawing.Point(5, 4);
            this.rdoSort4.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort4.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoSort4.Name = "rdoSort4";
            this.rdoSort4.Size = new System.Drawing.Size(346, 24);
            this.rdoSort4.TabIndex = 3;
            this.rdoSort4.Tag = "CHANGE";
            this.rdoSort4.Text = "棚番、商品区分１、商品区分２、商品コード";
            this.rdoSort4.UseVisualStyleBackColor = false;
            // 
            // rdoSort3
            // 
            this.rdoSort3.AutoSize = true;
            this.rdoSort3.BackColor = System.Drawing.Color.Silver;
            this.rdoSort3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSort3.Location = new System.Drawing.Point(5, 3);
            this.rdoSort3.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort3.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoSort3.Name = "rdoSort3";
            this.rdoSort3.Size = new System.Drawing.Size(250, 24);
            this.rdoSort3.TabIndex = 2;
            this.rdoSort3.Tag = "CHANGE";
            this.rdoSort3.Text = "棚番、商品区分１、商品コード";
            this.rdoSort3.UseVisualStyleBackColor = false;
            // 
            // rdoSort2
            // 
            this.rdoSort2.AutoSize = true;
            this.rdoSort2.BackColor = System.Drawing.Color.Silver;
            this.rdoSort2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSort2.Location = new System.Drawing.Point(5, 3);
            this.rdoSort2.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort2.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoSort2.Name = "rdoSort2";
            this.rdoSort2.Size = new System.Drawing.Size(154, 24);
            this.rdoSort2.TabIndex = 1;
            this.rdoSort2.Tag = "CHANGE";
            this.rdoSort2.Text = "棚番、商品コード";
            this.rdoSort2.UseVisualStyleBackColor = false;
            // 
            // rdoSort1
            // 
            this.rdoSort1.AutoSize = true;
            this.rdoSort1.BackColor = System.Drawing.Color.Silver;
            this.rdoSort1.Checked = true;
            this.rdoSort1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSort1.Location = new System.Drawing.Point(5, 3);
            this.rdoSort1.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort1.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoSort1.Name = "rdoSort1";
            this.rdoSort1.Size = new System.Drawing.Size(106, 24);
            this.rdoSort1.TabIndex = 0;
            this.rdoSort1.TabStop = true;
            this.rdoSort1.Tag = "CHANGE";
            this.rdoSort1.Text = "商品コード";
            this.rdoSort1.UseVisualStyleBackColor = false;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 7);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 8;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(490, 306);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 118);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(482, 31);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(482, 31);
            this.label4.TabIndex = 904;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "棚卸入力ソート順";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.rdoInjiNashi);
            this.fsiPanel3.Controls.Add(this.rdoInjiAri);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 80);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(482, 31);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(482, 31);
            this.label3.TabIndex = 904;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "棚卸表数量合計印字";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.rdoShohinMasutaSireTanka);
            this.fsiPanel2.Controls.Add(this.rdoSaishuSireTanka);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 42);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(482, 31);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(482, 31);
            this.label2.TabIndex = 904;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "棚卸金額計算単価";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.rdoShohinKubun1);
            this.fsiPanel1.Controls.Add(this.rdoShohinKubun2);
            this.fsiPanel1.Controls.Add(this.rdoShohinBunrui3);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(482, 31);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.MinimumSize = new System.Drawing.Size(0, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(482, 31);
            this.label1.TabIndex = 904;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "商品区分絞込み条件";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.rdoSort1);
            this.fsiPanel5.Controls.Add(this.label5);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 156);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(482, 31);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.MinimumSize = new System.Drawing.Size(0, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(482, 31);
            this.label5.TabIndex = 904;
            this.label5.Tag = "CHANGE";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.rdoSort2);
            this.fsiPanel6.Controls.Add(this.label6);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 194);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(482, 31);
            this.fsiPanel6.TabIndex = 4;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.MinimumSize = new System.Drawing.Size(0, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(482, 31);
            this.label6.TabIndex = 904;
            this.label6.Tag = "CHANGE";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.rdoSort3);
            this.fsiPanel7.Controls.Add(this.label7);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(4, 232);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(482, 31);
            this.fsiPanel7.TabIndex = 4;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.MinimumSize = new System.Drawing.Size(0, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(482, 31);
            this.label7.TabIndex = 904;
            this.label7.Tag = "CHANGE";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.rdoSort4);
            this.fsiPanel8.Controls.Add(this.label8);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(4, 270);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(482, 32);
            this.fsiPanel8.TabIndex = 4;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.MinimumSize = new System.Drawing.Size(0, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(482, 32);
            this.label8.TabIndex = 904;
            this.label8.Tag = "CHANGE";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBMR1022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 651);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBMR1022";
            this.Text = "棚卸初期設定";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton rdoShohinBunrui3;
        private System.Windows.Forms.RadioButton rdoShohinKubun2;
        private System.Windows.Forms.RadioButton rdoShohinKubun1;
        private System.Windows.Forms.RadioButton rdoInjiAri;
        private System.Windows.Forms.RadioButton rdoInjiNashi;
        private System.Windows.Forms.RadioButton rdoSaishuSireTanka;
        private System.Windows.Forms.RadioButton rdoShohinMasutaSireTanka;
        private System.Windows.Forms.RadioButton rdoSort4;
        private System.Windows.Forms.RadioButton rdoSort3;
        private System.Windows.Forms.RadioButton rdoSort2;
        private System.Windows.Forms.RadioButton rdoSort1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private common.FsiPanel fsiPanel5;
        private System.Windows.Forms.Label label5;
        private common.FsiPanel fsiPanel6;
        private System.Windows.Forms.Label label6;
        private common.FsiPanel fsiPanel7;
        private System.Windows.Forms.Label label7;
        private common.FsiPanel fsiPanel8;
        private System.Windows.Forms.Label label8;
    }
}