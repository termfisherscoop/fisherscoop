﻿using System.Data;
using System;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1061
{
    /// <summary>
    /// KBMR1061R の帳票
    /// </summary>
    public partial class KBMR1061R : BaseReport
    {

        public KBMR1061R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            InitializeComponent();
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            ////和暦でDataTimeを文字列に変換する
            //System.Globalization.CultureInfo ci =
            //    new System.Globalization.CultureInfo("ja-JP", false);
            //ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            //this.txtToday.Text = DateTime.Now.ToString("gy年MM月dd日", ci);
        }

    }
}
