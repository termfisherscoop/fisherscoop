﻿namespace jp.co.fsi.kb.kbmr1011
{
    partial class KBMR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.lblDateYear = new System.Windows.Forms.Label();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.lblShohinKubunBet = new System.Windows.Forms.Label();
            this.lblShohinKubunFr = new System.Windows.Forms.Label();
            this.lblShohinKubunTo = new System.Windows.Forms.Label();
            this.txtShohinKubunTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohinKubunFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 708);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1124, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1113, 41);
            this.lblTitle.Text = "";
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonth.Location = new System.Drawing.Point(305, 4);
            this.lblDateMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateMonth.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(20, 24);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Tag = "CHANGE";
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYear
            // 
            this.lblDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYear.Location = new System.Drawing.Point(231, 4);
            this.lblDateYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateYear.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDateYear.Name = "lblDateYear";
            this.lblDateYear.Size = new System.Drawing.Size(23, 24);
            this.lblDateYear.TabIndex = 3;
            this.lblDateYear.Tag = "CHANGE";
            this.lblDateYear.Text = "年";
            this.lblDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.Location = new System.Drawing.Point(188, 5);
            this.txtDateYear.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.MinimumSize = new System.Drawing.Size(4, 24);
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(39, 23);
            this.txtDateYear.TabIndex = 2;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = false;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonth.Location = new System.Drawing.Point(258, 5);
            this.txtDateMonth.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.MinimumSize = new System.Drawing.Size(4, 24);
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(39, 23);
            this.txtDateMonth.TabIndex = 4;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.LightCyan;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.Location = new System.Drawing.Point(131, 4);
            this.lblDateGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateGengo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(51, 24);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKubunBet
            // 
            this.lblShohinKubunBet.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKubunBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunBet.Location = new System.Drawing.Point(418, 5);
            this.lblShohinKubunBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKubunBet.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblShohinKubunBet.Name = "lblShohinKubunBet";
            this.lblShohinKubunBet.Size = new System.Drawing.Size(23, 24);
            this.lblShohinKubunBet.TabIndex = 2;
            this.lblShohinKubunBet.Tag = "CHANGE";
            this.lblShohinKubunBet.Text = "～";
            this.lblShohinKubunBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShohinKubunFr
            // 
            this.lblShohinKubunFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinKubunFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKubunFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunFr.Location = new System.Drawing.Point(206, 2);
            this.lblShohinKubunFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKubunFr.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblShohinKubunFr.Name = "lblShohinKubunFr";
            this.lblShohinKubunFr.Size = new System.Drawing.Size(204, 27);
            this.lblShohinKubunFr.TabIndex = 1;
            this.lblShohinKubunFr.Text = "先　頭";
            this.lblShohinKubunFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKubunTo
            // 
            this.lblShohinKubunTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinKubunTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKubunTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunTo.Location = new System.Drawing.Point(523, 2);
            this.lblShohinKubunTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKubunTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblShohinKubunTo.Name = "lblShohinKubunTo";
            this.lblShohinKubunTo.Size = new System.Drawing.Size(204, 27);
            this.lblShohinKubunTo.TabIndex = 4;
            this.lblShohinKubunTo.Text = "最　後";
            this.lblShohinKubunTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKubunTo
            // 
            this.txtShohinKubunTo.AutoSizeFromLength = false;
            this.txtShohinKubunTo.DisplayLength = null;
            this.txtShohinKubunTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKubunTo.Location = new System.Drawing.Point(449, 6);
            this.txtShohinKubunTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohinKubunTo.MaxLength = 4;
            this.txtShohinKubunTo.MinimumSize = new System.Drawing.Size(4, 24);
            this.txtShohinKubunTo.Name = "txtShohinKubunTo";
            this.txtShohinKubunTo.Size = new System.Drawing.Size(65, 24);
            this.txtShohinKubunTo.TabIndex = 3;
            this.txtShohinKubunTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKubunTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohinKubunTo_KeyDown);
            this.txtShohinKubunTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKubunTo_Validating);
            // 
            // txtShohinKubunFr
            // 
            this.txtShohinKubunFr.AutoSizeFromLength = false;
            this.txtShohinKubunFr.DisplayLength = null;
            this.txtShohinKubunFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKubunFr.Location = new System.Drawing.Point(131, 6);
            this.txtShohinKubunFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohinKubunFr.MaxLength = 4;
            this.txtShohinKubunFr.MinimumSize = new System.Drawing.Size(4, 24);
            this.txtShohinKubunFr.Name = "txtShohinKubunFr";
            this.txtShohinKubunFr.Size = new System.Drawing.Size(65, 23);
            this.txtShohinKubunFr.TabIndex = 0;
            this.txtShohinKubunFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKubunFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKubunFr_Validating);
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.BackColor = System.Drawing.Color.Silver;
            this.rdoZeikomi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeikomi.Location = new System.Drawing.Point(75, 3);
            this.rdoZeikomi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeikomi.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(58, 24);
            this.rdoZeikomi.TabIndex = 1;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Tag = "CHANGE";
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = false;
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.BackColor = System.Drawing.Color.Silver;
            this.rdoZeinuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeinuki.Location = new System.Drawing.Point(4, 3);
            this.rdoZeinuki.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeinuki.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(58, 24);
            this.rdoZeinuki.TabIndex = 0;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Tag = "CHANGE";
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(131, 7);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.MinimumSize = new System.Drawing.Size(4, 24);
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 908;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(179, 6);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 910;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0: 全ての商品",
            "1: 取引有りの商品のみ",
            "2: 取引中止の商品のみ"});
            this.comboBox1.Location = new System.Drawing.Point(131, 8);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(295, 24);
            this.comboBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.MinimumSize = new System.Drawing.Size(0, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(769, 36);
            this.label1.TabIndex = 911;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "支所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(769, 36);
            this.label2.TabIndex = 911;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "出力月";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(769, 36);
            this.label3.TabIndex = 911;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "商品区分１の検索";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(769, 38);
            this.label4.TabIndex = 911;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "中止区分";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 44);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 4;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(777, 175);
            this.fsiTableLayoutPanel1.TabIndex = 912;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.comboBox1);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 133);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(769, 38);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtShohinKubunFr);
            this.fsiPanel3.Controls.Add(this.txtShohinKubunTo);
            this.fsiPanel3.Controls.Add(this.lblShohinKubunTo);
            this.fsiPanel3.Controls.Add(this.lblShohinKubunBet);
            this.fsiPanel3.Controls.Add(this.lblShohinKubunFr);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 90);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(769, 36);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.fsiPanel5);
            this.fsiPanel2.Controls.Add(this.lblDateGengo);
            this.fsiPanel2.Controls.Add(this.txtDateMonth);
            this.fsiPanel2.Controls.Add(this.txtDateYear);
            this.fsiPanel2.Controls.Add(this.lblDateYear);
            this.fsiPanel2.Controls.Add(this.lblDateMonth);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 47);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(769, 36);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.BackColor = System.Drawing.Color.Silver;
            this.fsiPanel5.Controls.Add(this.rdoZeinuki);
            this.fsiPanel5.Controls.Add(this.rdoZeikomi);
            this.fsiPanel5.Location = new System.Drawing.Point(344, 5);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(188, 31);
            this.fsiPanel5.TabIndex = 912;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(769, 36);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // KBMR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 745);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBMR1011";
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label lblDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonth;
        private System.Windows.Forms.Label lblShohinKubunFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKubunFr;
        private System.Windows.Forms.Label lblShohinKubunBet;
        private System.Windows.Forms.Label lblShohinKubunTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKubunTo;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel5;
    }
}