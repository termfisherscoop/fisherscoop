﻿namespace jp.co.fsi.kb.kbdr1041
{
    /// <summary>
    /// KBDR1041R の概要の説明です。
    /// </summary>
    partial class KBDR1041R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR1041R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト69 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ITEM01,
            this.ラベル44,
            this.ラベル45,
            this.ラベル46,
            this.ラベル47,
            this.ラベル48,
            this.ラベル49,
            this.ラベル51,
            this.ラベル52,
            this.直線59,
            this.テキスト6,
            this.ラベル9,
            this.ラベル10,
            this.直線13,
            this.ラベル20,
            this.textBox1,
            this.reportInfo1,
            this.label1});
            this.pageHeader.Height = 1.34398F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.15625F;
            this.ITEM01.Left = 0.3858268F;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM01";
            this.ITEM01.Top = 0.314813F;
            this.ITEM01.Width = 3.677166F;
            // 
            // ラベル44
            // 
            this.ラベル44.Height = 0.1972222F;
            this.ラベル44.HyperLink = null;
            this.ラベル44.Left = 0.26063F;
            this.ラベル44.Name = "ラベル44";
            this.ラベル44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.ラベル44.Tag = "";
            this.ラベル44.Text = "ｺｰﾄﾞ";
            this.ラベル44.Top = 1.146063F;
            this.ラベル44.Width = 0.42896F;
            // 
            // ラベル45
            // 
            this.ラベル45.Height = 0.1979167F;
            this.ラベル45.HyperLink = null;
            this.ラベル45.Left = 0.7361177F;
            this.ラベル45.Name = "ラベル45";
            this.ラベル45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.ラベル45.Tag = "";
            this.ラベル45.Text = "仲　買　人　氏　名";
            this.ラベル45.Top = 1.146063F;
            this.ラベル45.Width = 2.290261F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.1979167F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 3.893701F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "前月繰越金額";
            this.ラベル46.Top = 1.135039F;
            this.ラベル46.Width = 1.147917F;
            // 
            // ラベル47
            // 
            this.ラベル47.Height = 0.1979167F;
            this.ラベル47.HyperLink = null;
            this.ラベル47.Left = 5.38189F;
            this.ラベル47.Name = "ラベル47";
            this.ラベル47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.ラベル47.Tag = "";
            this.ラベル47.Text = "売上金額";
            this.ラベル47.Top = 1.135039F;
            this.ラベル47.Width = 0.8833494F;
            // 
            // ラベル48
            // 
            this.ラベル48.Height = 0.1979167F;
            this.ラベル48.HyperLink = null;
            this.ラベル48.Left = 6.418504F;
            this.ラベル48.Name = "ラベル48";
            this.ラベル48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.ラベル48.Tag = "";
            this.ラベル48.Text = "消費税額";
            this.ラベル48.Top = 1.134646F;
            this.ラベル48.Width = 0.8172297F;
            // 
            // ラベル49
            // 
            this.ラベル49.Height = 0.1979167F;
            this.ラベル49.HyperLink = null;
            this.ラベル49.Left = 7.379528F;
            this.ラベル49.Name = "ラベル49";
            this.ラベル49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.ラベル49.Tag = "";
            this.ラベル49.Text = "税込金額";
            this.ラベル49.Top = 1.146063F;
            this.ラベル49.Width = 0.8772802F;
            // 
            // ラベル51
            // 
            this.ラベル51.Height = 0.1979167F;
            this.ラベル51.HyperLink = null;
            this.ラベル51.Left = 9.177166F;
            this.ラベル51.Name = "ラベル51";
            this.ラベル51.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.ラベル51.Tag = "";
            this.ラベル51.Text = "入金金額";
            this.ラベル51.Top = 1.146063F;
            this.ラベル51.Width = 0.8724413F;
            // 
            // ラベル52
            // 
            this.ラベル52.Height = 0.1979167F;
            this.ラベル52.HyperLink = null;
            this.ラベル52.Left = 10.15354F;
            this.ラベル52.Name = "ラベル52";
            this.ラベル52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.ラベル52.Tag = "";
            this.ラベル52.Text = "繰越残高";
            this.ラベル52.Top = 1.146063F;
            this.ラベル52.Width = 0.9639845F;
            // 
            // 直線59
            // 
            this.直線59.Height = 0.0001920462F;
            this.直線59.Left = 0.26063F;
            this.直線59.LineWeight = 2F;
            this.直線59.Name = "直線59";
            this.直線59.Tag = "";
            this.直線59.Top = 1.332677F;
            this.直線59.Width = 10.96966F;
            this.直線59.X1 = 0.26063F;
            this.直線59.X2 = 11.23029F;
            this.直線59.Y1 = 1.332677F;
            this.直線59.Y2 = 1.332869F;
            // 
            // テキスト6
            // 
            this.テキスト6.DataField = "ITEM05";
            this.テキスト6.Height = 0.1666667F;
            this.テキスト6.Left = 4.354091F;
            this.テキスト6.Name = "テキスト6";
            this.テキスト6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト6.Tag = "";
            this.テキスト6.Text = "ITEM05";
            this.テキスト6.Top = 0.6689797F;
            this.テキスト6.Width = 2.718898F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.1548611F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 9.405561F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "【税込み】";
            this.ラベル9.Top = 0.6356464F;
            this.ラベル9.Visible = false;
            this.ラベル9.Width = 0.7479166F;
            // 
            // ラベル10
            // 
            this.ラベル10.Height = 0.1979167F;
            this.ラベル10.HyperLink = null;
            this.ラベル10.Left = 8.309055F;
            this.ラベル10.Name = "ラベル10";
            this.ラベル10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.ラベル10.Tag = "";
            this.ラベル10.Text = "売上返品";
            this.ラベル10.Top = 1.145276F;
            this.ラベル10.Width = 0.7523619F;
            // 
            // 直線13
            // 
            this.直線13.Height = 0F;
            this.直線13.Left = 4.532638F;
            this.直線13.LineWeight = 2F;
            this.直線13.Name = "直線13";
            this.直線13.Tag = "";
            this.直線13.Top = 0.5905512F;
            this.直線13.Width = 2.361805F;
            this.直線13.X1 = 4.532638F;
            this.直線13.X2 = 6.894443F;
            this.直線13.Y1 = 0.5905512F;
            this.直線13.Y2 = 0.5905512F;
            // 
            // ラベル20
            // 
            this.ラベル20.DataField = "ITEM02";
            this.ラベル20.Height = 0.28125F;
            this.ラベル20.Left = 4.723956F;
            this.ラベル20.Name = "ラベル20";
            this.ラベル20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15.75pt; fo" +
    "nt-weight: bold; text-align: center; ddo-char-set: 1";
            this.ラベル20.Tag = "";
            this.ラベル20.Text = "ITEM02";
            this.ラベル20.Top = 0.2752297F;
            this.ラベル20.Width = 1.979167F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.1560203F;
            this.textBox1.Left = 9.726772F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox1.Text = null;
            this.textBox1.Top = 0.406693F;
            this.textBox1.Width = 0.322835F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.166437F;
            this.reportInfo1.Left = 8.770866F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.reportInfo1.Top = 0.406693F;
            this.reportInfo1.Width = 0.8645835F;
            // 
            // label1
            // 
            this.label1.Height = 0.1559055F;
            this.label1.HyperLink = null;
            this.label1.Left = 10.05748F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label1.Text = "頁";
            this.label1.Top = 0.406693F;
            this.label1.Width = 0.2631226F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト60,
            this.テキスト61,
            this.テキスト64,
            this.テキスト65,
            this.テキスト66,
            this.テキスト67,
            this.テキスト68,
            this.テキスト69,
            this.テキスト70,
            this.テキスト71,
            this.テキスト72,
            this.テキスト73,
            this.テキスト74,
            this.テキスト75,
            this.テキスト76,
            this.テキスト77,
            this.テキスト11,
            this.テキスト12});
            this.detail.Height = 0.4808946F;
            this.detail.Name = "detail";
            // 
            // テキスト60
            // 
            this.テキスト60.DataField = "ITEM06";
            this.テキスト60.Height = 0.2006944F;
            this.テキスト60.Left = 0.1405512F;
            this.テキスト60.Name = "テキスト60";
            this.テキスト60.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト60.Tag = "";
            this.テキスト60.Text = "ITEM06";
            this.テキスト60.Top = 0.08346457F;
            this.テキスト60.Width = 0.5492072F;
            // 
            // テキスト61
            // 
            this.テキスト61.DataField = "ITEM07";
            this.テキスト61.Height = 0.4064851F;
            this.テキスト61.Left = 0.7362205F;
            this.テキスト61.Name = "テキスト61";
            this.テキスト61.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.テキスト61.Tag = "";
            this.テキスト61.Text = "ITEM07";
            this.テキスト61.Top = 0.07440946F;
            this.テキスト61.Width = 2.290278F;
            // 
            // テキスト64
            // 
            this.テキスト64.DataField = "ITEM09";
            this.テキスト64.Height = 0.2006944F;
            this.テキスト64.Left = 3.775197F;
            this.テキスト64.Name = "テキスト64";
            this.テキスト64.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 1";
            this.テキスト64.Tag = "";
            this.テキスト64.Text = "ITEM09";
            this.テキスト64.Top = 0.08346457F;
            this.テキスト64.Width = 1.266612F;
            // 
            // テキスト65
            // 
            this.テキスト65.DataField = "ITEM10";
            this.テキスト65.Height = 0.2006944F;
            this.テキスト65.Left = 5.118111F;
            this.テキスト65.Name = "テキスト65";
            this.テキスト65.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 1";
            this.テキスト65.Tag = "";
            this.テキスト65.Text = "ITEM10";
            this.テキスト65.Top = 0.07433835F;
            this.テキスト65.Width = 1.146298F;
            // 
            // テキスト66
            // 
            this.テキスト66.DataField = "ITEM11";
            this.テキスト66.Height = 0.2006944F;
            this.テキスト66.Left = 6.322835F;
            this.テキスト66.Name = "テキスト66";
            this.テキスト66.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 1";
            this.テキスト66.Tag = "";
            this.テキスト66.Text = "ITEM11";
            this.テキスト66.Top = 0.07440946F;
            this.テキスト66.Width = 0.8696413F;
            // 
            // テキスト67
            // 
            this.テキスト67.DataField = "ITEM12";
            this.テキスト67.Height = 0.2006944F;
            this.テキスト67.Left = 7.245276F;
            this.テキスト67.Name = "テキスト67";
            this.テキスト67.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト67.Tag = "";
            this.テキスト67.Text = "ITEM12";
            this.テキスト67.Top = 0.08346457F;
            this.テキスト67.Width = 1.011674F;
            // 
            // テキスト68
            // 
            this.テキスト68.DataField = "ITEM14";
            this.テキスト68.Height = 0.2006944F;
            this.テキスト68.Left = 9.114568F;
            this.テキスト68.Name = "テキスト68";
            this.テキスト68.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト68.Tag = "";
            this.テキスト68.Text = "ITEM14";
            this.テキスト68.Top = 0.07440946F;
            this.テキスト68.Width = 0.8763889F;
            // 
            // テキスト69
            // 
            this.テキスト69.DataField = "ITEM15";
            this.テキスト69.Height = 0.2006944F;
            this.テキスト69.Left = 10.04961F;
            this.テキスト69.Name = "テキスト69";
            this.テキスト69.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト69.Tag = "";
            this.テキスト69.Text = "ITEM15";
            this.テキスト69.Top = 0.07874016F;
            this.テキスト69.Width = 1.140944F;
            // 
            // テキスト70
            // 
            this.テキスト70.DataField = "ITEM17";
            this.テキスト70.Height = 0.2006944F;
            this.テキスト70.Left = 3.775197F;
            this.テキスト70.Name = "テキスト70";
            this.テキスト70.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 1";
            this.テキスト70.Tag = "";
            this.テキスト70.Text = "ITEM17";
            this.テキスト70.Top = 0.2799924F;
            this.テキスト70.Width = 1.266612F;
            // 
            // テキスト71
            // 
            this.テキスト71.DataField = "ITEM18";
            this.テキスト71.Height = 0.2006944F;
            this.テキスト71.Left = 5.118111F;
            this.テキスト71.Name = "テキスト71";
            this.テキスト71.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 1";
            this.テキスト71.Tag = "";
            this.テキスト71.Text = "ITEM18";
            this.テキスト71.Top = 0.2708662F;
            this.テキスト71.Width = 1.146993F;
            // 
            // テキスト72
            // 
            this.テキスト72.DataField = "ITEM19";
            this.テキスト72.Height = 0.2006944F;
            this.テキスト72.Left = 6.322835F;
            this.テキスト72.Name = "テキスト72";
            this.テキスト72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 1";
            this.テキスト72.Tag = "";
            this.テキスト72.Text = "ITEM19";
            this.テキスト72.Top = 0.2709373F;
            this.テキスト72.Width = 0.8696413F;
            // 
            // テキスト73
            // 
            this.テキスト73.DataField = "ITEM20";
            this.テキスト73.Height = 0.2006944F;
            this.テキスト73.Left = 7.245276F;
            this.テキスト73.Name = "テキスト73";
            this.テキスト73.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト73.Tag = "";
            this.テキスト73.Text = "ITEM20";
            this.テキスト73.Top = 0.2799924F;
            this.テキスト73.Width = 1.011674F;
            // 
            // テキスト74
            // 
            this.テキスト74.DataField = "ITEM22";
            this.テキスト74.Height = 0.2006944F;
            this.テキスト74.Left = 9.114568F;
            this.テキスト74.Name = "テキスト74";
            this.テキスト74.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト74.Tag = "";
            this.テキスト74.Text = "ITEM22";
            this.テキスト74.Top = 0.2709373F;
            this.テキスト74.Width = 0.8763889F;
            // 
            // テキスト75
            // 
            this.テキスト75.DataField = "ITEM23";
            this.テキスト75.Height = 0.2006944F;
            this.テキスト75.Left = 10.04961F;
            this.テキスト75.Name = "テキスト75";
            this.テキスト75.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト75.Tag = "";
            this.テキスト75.Text = "ITEM23";
            this.テキスト75.Top = 0.2711013F;
            this.テキスト75.Width = 1.140944F;
            // 
            // テキスト76
            // 
            this.テキスト76.DataField = "ITEM08";
            this.テキスト76.Height = 0.2006944F;
            this.テキスト76.Left = 3.072906F;
            this.テキスト76.Name = "テキスト76";
            this.テキスト76.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト76.Tag = "";
            this.テキスト76.Text = "ITEM08";
            this.テキスト76.Top = 0.08360128F;
            this.テキスト76.Width = 0.6027778F;
            // 
            // テキスト77
            // 
            this.テキスト77.DataField = "ITEM16";
            this.テキスト77.Height = 0.2006944F;
            this.テキスト77.Left = 3.072906F;
            this.テキスト77.Name = "テキスト77";
            this.テキスト77.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト77.Tag = "";
            this.テキスト77.Text = "ITEM16";
            this.テキスト77.Top = 0.2759624F;
            this.テキスト77.Width = 0.6027778F;
            // 
            // テキスト11
            // 
            this.テキスト11.DataField = "ITEM13";
            this.テキスト11.Height = 0.2006944F;
            this.テキスト11.Left = 8.330709F;
            this.テキスト11.Name = "テキスト11";
            this.テキスト11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト11.Tag = "";
            this.テキスト11.Text = "ITEM13";
            this.テキスト11.Top = 0.07874016F;
            this.テキスト11.Width = 0.7305555F;
            // 
            // テキスト12
            // 
            this.テキスト12.DataField = "ITEM21";
            this.テキスト12.Height = 0.2006944F;
            this.テキスト12.Left = 8.330709F;
            this.テキスト12.Name = "テキスト12";
            this.テキスト12.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト12.Tag = "";
            this.テキスト12.Text = "ITEM21";
            this.テキスト12.Top = 0.275268F;
            this.テキスト12.Width = 0.7305555F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0.1570866F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KBDR1041R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.42708F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル44;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル47;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル48;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル49;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル51;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル52;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト6;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル10;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト69;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト70;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト72;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
    }
}
