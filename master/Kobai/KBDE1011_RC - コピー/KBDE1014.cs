﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x02000006 RID: 6
	public partial class KBDE1014 : BasePgForm
	{
		// Token: 0x06000034 RID: 52 RVA: 0x00002269 File Offset: 0x00000469
		public KBDE1014(decimal TOKUISAKI_CD, decimal SHOHIN_CD)
		{
			this._pTOKUISAKI_CD = TOKUISAKI_CD;
			this._pSHOHIN_CD = SHOHIN_CD;
			this.InitializeComponent();
			base.BindGotFocusEvent();
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00021A0C File Offset: 0x0001FC0C
		protected override void InitForm()
		{
			this.ShishoCode = Util.ToInt(base.UInfo.ShishoCd);
			this.lblTitle.Visible = false;
			this.btnF2.Visible = false;
			this.btnF3.Enabled = true;
			this.btnF3.Visible = true;
			this.btnF3.Location = this.btnF2.Location;
			this.btnF3.Text = this.btnF2.Text;
			this.btnF3.Text = this.btnF3.Text.Replace("F2", "F3");
			this.btnF3.Enabled = false;
			this.lblShohinNm.Text = base.Dba.GetName(base.UInfo, "VI_HN_SHOHIN_SHIIRE", this.ShishoCode.ToString(), this._pSHOHIN_CD.ToString());
			DataTable dtData = new DataTable();
			try
			{
				dtData = this.GetTB_HN_URIAGE_TANKA_RIREKI();
			}
			catch (Exception ex)
			{
				Msg.Error(ex.Message);
				return;
			}
			DataTable dataSource = this.EditDataList(dtData);
			this.dgvList.DataSource = dataSource;
			foreach (object obj in this.dgvList.Columns)
			{
				((DataGridViewColumn)obj).SortMode = DataGridViewColumnSortMode.NotSortable;
			}
			this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9f);
			this.dgvList.Columns[0].Visible = false;
			this.dgvList.Columns[1].Width = 100;
			this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvList.Columns[2].Width = 148;
			this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
		}

		// Token: 0x06000036 RID: 54 RVA: 0x0000228B File Offset: 0x0000048B
		protected override void OnMoveFocus()
		{
			string activeCtlNm = base.ActiveCtlNm;
		}

		// Token: 0x06000037 RID: 55 RVA: 0x000020A4 File Offset: 0x000002A4
		public override void PressEsc()
		{
			base.DialogResult = DialogResult.Cancel;
			base.PressEsc();
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002267 File Offset: 0x00000467
		public override void PressF1()
		{
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002294 File Offset: 0x00000494
		public override void PressF3()
		{
			if (!this.btnF3.Enabled)
			{
				return;
			}
			this.DeleteData();
			this.InitForm();
		}

		// Token: 0x0600003A RID: 58 RVA: 0x000022B0 File Offset: 0x000004B0
		private void dgvList_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				this.ReturnVal();
			}
		}

		// Token: 0x0600003B RID: 59 RVA: 0x000022C2 File Offset: 0x000004C2
		private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			this.ReturnVal();
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00021C58 File Offset: 0x0001FE58
		private void dgvList_RowEnter(object sender, DataGridViewCellEventArgs e)
		{
			if (Util.ToDecimal(this.dgvList[0, e.RowIndex].Value) > 2m)
			{
				this.btnF3.Enabled = true;
				return;
			}
			this.btnF3.Enabled = false;
		}

		// Token: 0x0600003D RID: 61 RVA: 0x000022C2 File Offset: 0x000004C2
		private void btnEnter_Click(object sender, EventArgs e)
		{
			this.ReturnVal();
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00021CA8 File Offset: 0x0001FEA8
		private DataTable GetTB_HN_URIAGE_TANKA_RIREKI()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append(" CAST((YEAR(A.SAISHU_TORIHIKI_DATE) + 10000) + (MONTH(A.SAISHU_TORIHIKI_DATE) + 100) + (DAY(A.SAISHU_TORIHIKI_DATE)) AS DECIMAL(8)) AS 順位,");
			stringBuilder.Append(" CONVERT(VARCHAR(8), A.SAISHU_TORIHIKI_DATE, 11) AS 取引日付,");
			stringBuilder.Append(" A.TANKA AS 単価 ");
			stringBuilder.Append("FROM");
			stringBuilder.Append(" TB_HN_URIAGE_TANKA_RIREKI AS A ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append(" A.KAISHA_CD = @KAISHA_CD AND");
			stringBuilder.Append(" A.SHISHO_CD = @SHISHO_CD AND ");
			stringBuilder.Append(" A.SEIKYUSAKI_CD = 0 AND");
			stringBuilder.Append(" A.TOKUISAKI_CD = @TOKUISAKI_CD AND");
			stringBuilder.Append(" A.SHOHIN_CD = @SHOHIN_CD ");
			stringBuilder.Append("UNION ALL ");
			stringBuilder.Append("SELECT");
			stringBuilder.Append(" CAST(2 AS DECIMAL(8)) AS 順位,");
			stringBuilder.Append(" CAST('卸単価' AS VARCHAR(8)) AS 取引日付,");
			stringBuilder.Append(" B.OROSHI_TANKA  AS 単価 ");
			stringBuilder.Append("FROM");
			stringBuilder.Append(" TB_HN_SHOHIN  AS B ");
			stringBuilder.Append("WHERE");
			stringBuilder.Append(" B.KAISHA_CD = @KAISHA_CD AND");
			stringBuilder.Append(" B.SHISHO_CD = @SHISHO_CD AND ");
			stringBuilder.Append(" B.SHOHIN_CD = @SHOHIN_CD ");
			stringBuilder.Append("UNION ALL ");
			stringBuilder.Append("SELECT");
			stringBuilder.Append(" CAST(1 AS DECIMAL(8)) AS 順位,");
			stringBuilder.Append(" CAST('小売単価' AS VARCHAR(8)) AS 取引日付,");
			stringBuilder.Append(" C.KORI_TANKA AS 単価 ");
			stringBuilder.Append("FROM");
			stringBuilder.Append(" TB_HN_SHOHIN AS C ");
			stringBuilder.Append("WHERE");
			stringBuilder.Append(" C.KAISHA_CD = @KAISHA_CD AND");
			stringBuilder.Append(" C.SHISHO_CD = @SHISHO_CD AND ");
			stringBuilder.Append(" C.SHOHIN_CD = @SHOHIN_CD ");
			stringBuilder.Append("ORDER BY");
			stringBuilder.Append(" 1 DESC");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, this._pTOKUISAKI_CD);
			dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 8, this._pSHOHIN_CD);
			return base.Dba.GetDataTableFromSqlWithParams(Util.ToString(stringBuilder), dbParamCollection);
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00021EE4 File Offset: 0x000200E4
		private DataTable EditDataList(DataTable dtData)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("順位", typeof(string));
			dataTable.Columns.Add("取引日付", typeof(string));
			dataTable.Columns.Add("単価", typeof(string));
			for (int i = 0; i < dtData.Rows.Count; i++)
			{
				DataRow dataRow = dataTable.NewRow();
				dataRow["順位"] = dtData.Rows[i]["順位"];
				dataRow["取引日付"] = dtData.Rows[i]["取引日付"];
				dataRow["単価"] = Util.FormatNum(dtData.Rows[i]["単価"], 2);
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00021FE4 File Offset: 0x000201E4
		private void DeleteData()
		{
			try
			{
				base.Dba.BeginTransaction();
				DbParamCollection dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
				dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
				dbParamCollection.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, 0);
				dbParamCollection.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, this._pTOKUISAKI_CD);
				dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 8, this._pSHOHIN_CD);
				dbParamCollection.SetParam("@TANKA", SqlDbType.Decimal, 13, this.dgvList[2, this.dgvList.CurrentCell.RowIndex].Value);
				base.Dba.Delete("TB_HN_URIAGE_TANKA_RIREKI", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD AND TANKA = @TANKA", dbParamCollection);
				base.Dba.Commit();
			}
			finally
			{
				base.Dba.Rollback();
			}
		}

		// Token: 0x06000041 RID: 65 RVA: 0x000220E4 File Offset: 0x000202E4
		private void ReturnVal()
		{
			base.OutData = new string[]
			{
				Util.ToString(this.dgvList.SelectedRows[0].Cells["単価"].Value)
			};
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		// Token: 0x04000276 RID: 630
		private decimal _pTOKUISAKI_CD;

		// Token: 0x04000277 RID: 631
		private decimal _pSHOHIN_CD;

		// Token: 0x04000278 RID: 632
		private int ShishoCode;
	}
}
