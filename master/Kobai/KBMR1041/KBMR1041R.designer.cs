﻿namespace jp.co.fsi.kb.kbmr1041
{
    /// <summary>
    /// KBDR1061R の概要の説明です。
    /// </summary>
    partial class KBMR1041R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBMR1041R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtKaishaNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateHani = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblUriagejuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSyohinNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTanka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShohizei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingakukei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDetailAllCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHani)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUriagejuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSyohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingakukei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetailAllCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtKaishaNm,
            this.txtTitleName,
            this.txtDateHani,
            this.line1,
            this.textBox1,
            this.reportInfo1,
            this.lblPage,
            this.txtPageCount,
            this.label1,
            this.lblUriagejuni,
            this.lblCd,
            this.lblSyohinNm,
            this.lblSuryo,
            this.lblTanka,
            this.lblKingaku,
            this.lblShohizei,
            this.lblKingakukei,
            this.line2,
            this.line3,
            this.line4,
            this.line7,
            this.line10,
            this.line11,
            this.line12,
            this.line5,
            this.txtCount,
            this.txtDetailAllCount,
            this.line9,
            this.line8,
            this.line6});
            this.pageHeader.Height = 1.08937F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtKaishaNm
            // 
            this.txtKaishaNm.DataField = "ITEM01";
            this.txtKaishaNm.Height = 0.2F;
            this.txtKaishaNm.Left = 0.007874017F;
            this.txtKaishaNm.MultiLine = false;
            this.txtKaishaNm.Name = "txtKaishaNm";
            this.txtKaishaNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal";
            this.txtKaishaNm.Text = "01";
            this.txtKaishaNm.Top = 0.5602363F;
            this.txtKaishaNm.Width = 2.783859F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 2.325393F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle";
            this.txtTitleName.Text = "商品売上順位表";
            this.txtTitleName.Top = 0.003149607F;
            this.txtTitleName.Width = 2.833465F;
            // 
            // txtDateHani
            // 
            this.txtDateHani.DataField = "ITEM02";
            this.txtDateHani.Height = 0.2F;
            this.txtDateHani.Left = 3.153543F;
            this.txtDateHani.Name = "txtDateHani";
            this.txtDateHani.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center";
            this.txtDateHani.Text = "02";
            this.txtDateHani.Top = 0.3602363F;
            this.txtDateHani.Width = 1.177165F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.783857F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2874016F;
            this.line1.Width = 1.916538F;
            this.line1.X1 = 2.783857F;
            this.line1.X2 = 4.700395F;
            this.line1.Y1 = 0.2874016F;
            this.line1.Y2 = 0.2874016F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM03";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 5.658268F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left";
            this.textBox1.Text = "03";
            this.textBox1.Top = 0.003149599F;
            this.textBox1.Visible = false;
            this.textBox1.Width = 1.177166F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1574803F;
            this.reportInfo1.Left = 5.807087F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.reportInfo1.Top = 0.3330709F;
            this.reportInfo1.Width = 1.083071F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1574803F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.205894F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.3330709F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1574803F;
            this.txtPageCount.Left = 6.890158F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtPageCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.3330709F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // label1
            // 
            this.label1.Height = 0.130315F;
            this.label1.HyperLink = null;
            this.label1.Left = 5.301969F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9pt";
            this.label1.Text = "～";
            this.label1.Top = 0.003149599F;
            this.label1.Visible = false;
            this.label1.Width = 0.1716537F;
            // 
            // lblUriagejuni
            // 
            this.lblUriagejuni.Height = 0.2562992F;
            this.lblUriagejuni.HyperLink = null;
            this.lblUriagejuni.Left = 0.007874016F;
            this.lblUriagejuni.Name = "lblUriagejuni";
            this.lblUriagejuni.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-font-vertica" +
    "l: none";
            this.lblUriagejuni.Text = "売上順位";
            this.lblUriagejuni.Top = 0.8228347F;
            this.lblUriagejuni.Width = 0.582126F;
            // 
            // lblCd
            // 
            this.lblCd.Height = 0.2562992F;
            this.lblCd.HyperLink = null;
            this.lblCd.Left = 0.59F;
            this.lblCd.Name = "lblCd";
            this.lblCd.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.lblCd.Text = "コード";
            this.lblCd.Top = 0.823F;
            this.lblCd.Width = 0.475F;
            // 
            // lblSyohinNm
            // 
            this.lblSyohinNm.Height = 0.2562992F;
            this.lblSyohinNm.HyperLink = null;
            this.lblSyohinNm.Left = 1.076F;
            this.lblSyohinNm.Name = "lblSyohinNm";
            this.lblSyohinNm.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.lblSyohinNm.Text = "商  品  名";
            this.lblSyohinNm.Top = 0.823F;
            this.lblSyohinNm.Width = 1.984F;
            // 
            // lblSuryo
            // 
            this.lblSuryo.Height = 0.2562992F;
            this.lblSuryo.HyperLink = null;
            this.lblSuryo.Left = 3.05F;
            this.lblSuryo.Name = "lblSuryo";
            this.lblSuryo.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.lblSuryo.Text = "数  量";
            this.lblSuryo.Top = 0.823F;
            this.lblSuryo.Width = 0.8572993F;
            // 
            // lblTanka
            // 
            this.lblTanka.Height = 0.2562992F;
            this.lblTanka.HyperLink = null;
            this.lblTanka.Left = 3.907F;
            this.lblTanka.Name = "lblTanka";
            this.lblTanka.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; text-justify: auto; vertical-align: middle; ddo-char-set:" +
    " 1; ddo-shrink-to-fit: none";
            this.lblTanka.Text = "単  価";
            this.lblTanka.Top = 0.823F;
            this.lblTanka.Width = 0.8436999F;
            // 
            // lblKingaku
            // 
            this.lblKingaku.Height = 0.2562992F;
            this.lblKingaku.HyperLink = null;
            this.lblKingaku.Left = 4.751F;
            this.lblKingaku.Name = "lblKingaku";
            this.lblKingaku.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.lblKingaku.Text = "金  額";
            this.lblKingaku.Top = 0.8228347F;
            this.lblKingaku.Width = 0.9421258F;
            // 
            // lblShohizei
            // 
            this.lblShohizei.Height = 0.2562992F;
            this.lblShohizei.HyperLink = null;
            this.lblShohizei.Left = 5.693F;
            this.lblShohizei.Name = "lblShohizei";
            this.lblShohizei.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.lblShohizei.Text = "消費税";
            this.lblShohizei.Top = 0.8228347F;
            this.lblShohizei.Width = 0.8306222F;
            // 
            // lblKingakukei
            // 
            this.lblKingakukei.Height = 0.2562992F;
            this.lblKingakukei.HyperLink = null;
            this.lblKingakukei.Left = 6.523623F;
            this.lblKingakukei.Name = "lblKingakukei";
            this.lblKingakukei.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.lblKingakukei.Text = "金額計";
            this.lblKingakukei.Top = 0.8228347F;
            this.lblKingakukei.Width = 0.9566932F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.007874016F;
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Top = 1.079134F;
            this.line2.Width = 7.472441F;
            this.line2.X1 = 0.007874016F;
            this.line2.X2 = 7.480315F;
            this.line2.Y1 = 1.079134F;
            this.line2.Y2 = 1.079134F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0.007874016F;
            this.line3.LineWeight = 2F;
            this.line3.Name = "line3";
            this.line3.Top = 0.8228347F;
            this.line3.Width = 7.472441F;
            this.line3.X1 = 0.007874016F;
            this.line3.X2 = 7.480315F;
            this.line3.Y1 = 0.8228347F;
            this.line3.Y2 = 0.8228347F;
            // 
            // line4
            // 
            this.line4.Height = 0.2562993F;
            this.line4.Left = 0.007874016F;
            this.line4.LineWeight = 2F;
            this.line4.Name = "line4";
            this.line4.Top = 0.8228347F;
            this.line4.Width = 0F;
            this.line4.X1 = 0.007874016F;
            this.line4.X2 = 0.007874016F;
            this.line4.Y1 = 0.8228347F;
            this.line4.Y2 = 1.079134F;
            // 
            // line7
            // 
            this.line7.Height = 0.2562993F;
            this.line7.Left = 1.076F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.823F;
            this.line7.Width = 0F;
            this.line7.X1 = 1.076F;
            this.line7.X2 = 1.076F;
            this.line7.Y1 = 0.823F;
            this.line7.Y2 = 1.079299F;
            // 
            // line10
            // 
            this.line10.Height = 0.2562993F;
            this.line10.Left = 4.751F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0.823F;
            this.line10.Width = 0F;
            this.line10.X1 = 4.751F;
            this.line10.X2 = 4.751F;
            this.line10.Y1 = 0.823F;
            this.line10.Y2 = 1.079299F;
            // 
            // line11
            // 
            this.line11.Height = 0.2562993F;
            this.line11.Left = 5.693F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0.823F;
            this.line11.Width = 0F;
            this.line11.X1 = 5.693F;
            this.line11.X2 = 5.693F;
            this.line11.Y1 = 0.823F;
            this.line11.Y2 = 1.079299F;
            // 
            // line12
            // 
            this.line12.Height = 0.2562993F;
            this.line12.Left = 6.523623F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.8228347F;
            this.line12.Width = 0F;
            this.line12.X1 = 6.523623F;
            this.line12.X2 = 6.523623F;
            this.line12.Y1 = 0.8228347F;
            this.line12.Y2 = 1.079134F;
            // 
            // line5
            // 
            this.line5.Height = 0.2562993F;
            this.line5.Left = 7.480315F;
            this.line5.LineWeight = 2F;
            this.line5.Name = "line5";
            this.line5.Top = 0.8228347F;
            this.line5.Width = 0F;
            this.line5.X1 = 7.480315F;
            this.line5.X2 = 7.480315F;
            this.line5.Y1 = 0.8228347F;
            this.line5.Y2 = 1.079134F;
            // 
            // txtCount
            // 
            this.txtCount.CountNullValues = true;
            this.txtCount.Height = 0.2F;
            this.txtCount.Left = 6.523623F;
            this.txtCount.MultiLine = false;
            this.txtCount.Name = "txtCount";
            this.txtCount.Text = "0";
            this.txtCount.Top = 0.5602363F;
            this.txtCount.Visible = false;
            this.txtCount.Width = 0.2500005F;
            // 
            // txtDetailAllCount
            // 
            this.txtDetailAllCount.DataField = "ITEM12";
            this.txtDetailAllCount.Height = 0.2F;
            this.txtDetailAllCount.Left = 6.773623F;
            this.txtDetailAllCount.MultiLine = false;
            this.txtDetailAllCount.Name = "txtDetailAllCount";
            this.txtDetailAllCount.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle; ddo-font-vertical: none";
            this.txtDetailAllCount.Text = null;
            this.txtDetailAllCount.Top = 0.5602363F;
            this.txtDetailAllCount.Visible = false;
            this.txtDetailAllCount.Width = 0.2952757F;
            // 
            // line9
            // 
            this.line9.Height = 0.2562993F;
            this.line9.Left = 3.907F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0.823F;
            this.line9.Width = 0F;
            this.line9.X1 = 3.907F;
            this.line9.X2 = 3.907F;
            this.line9.Y1 = 0.823F;
            this.line9.Y2 = 1.079299F;
            // 
            // line8
            // 
            this.line8.Height = 0.2562993F;
            this.line8.Left = 3.05F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.823F;
            this.line8.Width = 0F;
            this.line8.X1 = 3.05F;
            this.line8.X2 = 3.05F;
            this.line8.Y1 = 0.823F;
            this.line8.Y2 = 1.079299F;
            // 
            // line6
            // 
            this.line6.Height = 0.2562993F;
            this.line6.Left = 0.59F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.823F;
            this.line6.Width = 0F;
            this.line6.X1 = 0.59F;
            this.line6.X2 = 0.59F;
            this.line6.Y1 = 0.823F;
            this.line6.Y2 = 1.079299F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox6,
            this.textBox9,
            this.textBox2,
            this.textBox7,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox3,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line22,
            this.line24,
            this.line21});
            this.detail.Height = 0.2048504F;
            this.detail.Name = "detail";
            this.detail.RepeatToFill = true;
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM04";
            this.textBox6.Height = 0.1968504F;
            this.textBox6.Left = 0.027F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle";
            this.textBox6.Text = "04";
            this.textBox6.Top = 0F;
            this.textBox6.Width = 0.543F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM07";
            this.textBox9.Height = 0.1968504F;
            this.textBox9.Left = 3.06F;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.textBox9.Text = "9,999,999";
            this.textBox9.Top = 0F;
            this.textBox9.Width = 0.826F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM06";
            this.textBox2.Height = 0.1968504F;
            this.textBox2.Left = 1.097F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.textBox2.Text = "06";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 1.937F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM05";
            this.textBox7.Height = 0.1968504F;
            this.textBox7.Left = 0.615F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.textBox7.Text = "23";
            this.textBox7.Top = 0F;
            this.textBox7.Width = 0.439F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM08";
            this.textBox10.Height = 0.1968504F;
            this.textBox10.Left = 3.927F;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-shrink-to-fit: none";
            this.textBox10.Text = "123456789";
            this.textBox10.Top = 0F;
            this.textBox10.Width = 0.8025982F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM09";
            this.textBox11.Height = 0.1968504F;
            this.textBox11.Left = 4.771F;
            this.textBox11.Name = "textBox11";
            this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.textBox11.Text = "12345678901";
            this.textBox11.Top = 0F;
            this.textBox11.Width = 0.9109998F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM10";
            this.textBox12.Height = 0.1968504F;
            this.textBox12.Left = 5.723F;
            this.textBox12.Name = "textBox12";
            this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.textBox12.Text = "10";
            this.textBox12.Top = 0F;
            this.textBox12.Width = 0.7800002F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM11";
            this.textBox3.Height = 0.1968504F;
            this.textBox3.Left = 6.545F;
            this.textBox3.Name = "textBox3";
            this.textBox3.OutputFormat = resources.GetString("textBox3.OutputFormat");
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.textBox3.Text = "12345678901";
            this.textBox3.Top = 0F;
            this.textBox3.Width = 0.9160004F;
            // 
            // line13
            // 
            this.line13.Height = 0.1968504F;
            this.line13.Left = 0.007874016F;
            this.line13.LineWeight = 2F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 0.007874016F;
            this.line13.X2 = 0.007874016F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.1968504F;
            // 
            // line14
            // 
            this.line14.Height = 0.1968504F;
            this.line14.Left = 7.480315F;
            this.line14.LineWeight = 2F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 7.480315F;
            this.line14.X2 = 7.480315F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.1968504F;
            // 
            // line15
            // 
            this.line15.Height = 0.1968504F;
            this.line15.Left = 6.523623F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 6.523623F;
            this.line15.X2 = 6.523623F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.1968504F;
            // 
            // line16
            // 
            this.line16.Height = 0.1968504F;
            this.line16.Left = 5.693F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 5.693F;
            this.line16.X2 = 5.693F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.1968504F;
            // 
            // line17
            // 
            this.line17.Height = 0.1968504F;
            this.line17.Left = 4.751F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0F;
            this.line17.Width = 0F;
            this.line17.X1 = 4.751F;
            this.line17.X2 = 4.751F;
            this.line17.Y1 = 0F;
            this.line17.Y2 = 0.1968504F;
            // 
            // line18
            // 
            this.line18.Height = 0.1968504F;
            this.line18.Left = 3.907F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 3.907F;
            this.line18.X2 = 3.907F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.1968504F;
            // 
            // line19
            // 
            this.line19.Height = 0.1968504F;
            this.line19.Left = 3.05F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0F;
            this.line19.Width = 0F;
            this.line19.X1 = 3.05F;
            this.line19.X2 = 3.05F;
            this.line19.Y1 = 0F;
            this.line19.Y2 = 0.1968504F;
            // 
            // line20
            // 
            this.line20.Height = 0.1968504F;
            this.line20.Left = 1.076F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0F;
            this.line20.Width = 0F;
            this.line20.X1 = 1.076F;
            this.line20.X2 = 1.076F;
            this.line20.Y1 = 0F;
            this.line20.Y2 = 0.1968504F;
            // 
            // line22
            // 
            this.line22.Height = 0F;
            this.line22.Left = 0.007874016F;
            this.line22.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.1968504F;
            this.line22.Width = 7.472441F;
            this.line22.X1 = 7.480315F;
            this.line22.X2 = 0.007874016F;
            this.line22.Y1 = 0.1968504F;
            this.line22.Y2 = 0.1968504F;
            // 
            // line24
            // 
            this.line24.Height = 0F;
            this.line24.Left = 0.007874017F;
            this.line24.LineWeight = 2F;
            this.line24.Name = "line24";
            this.line24.Top = 0.1968504F;
            this.line24.Visible = false;
            this.line24.Width = 7.472441F;
            this.line24.X1 = 0.007874017F;
            this.line24.X2 = 7.480315F;
            this.line24.Y1 = 0.1968504F;
            this.line24.Y2 = 0.1968504F;
            // 
            // line21
            // 
            this.line21.Height = 0.1968504F;
            this.line21.Left = 0.59F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0F;
            this.line21.Width = 0F;
            this.line21.X1 = 0.59F;
            this.line21.X2 = 0.59F;
            this.line21.Y1 = 0F;
            this.line21.Y2 = 0.1968504F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // KBMR1041R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7086614F;
            this.PageSettings.Margins.Left = 0.3937007F;
            this.PageSettings.Margins.Right = 0.3937007F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.484252F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHani)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUriagejuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSyohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingakukei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetailAllCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKaishaNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateHani;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblUriagejuni;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSyohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShohizei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingakukei;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDetailAllCount;
    }
}
