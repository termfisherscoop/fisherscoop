﻿namespace jp.co.fsi.kb.kbcm1011
{
    partial class KBCM1013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNakagaininCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtShiiresakiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblNakagaininCdFr = new System.Windows.Forms.Label();
            this.txtShiiresakiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 37);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(973, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(963, 31);
            this.lblTitle.Text = "仕入先の登録";
            // 
            // lblNakagaininCdTo
            // 
            this.lblNakagaininCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaininCdTo.Location = new System.Drawing.Point(542, 1);
            this.lblNakagaininCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
            this.lblNakagaininCdTo.Size = new System.Drawing.Size(289, 24);
            this.lblNakagaininCdTo.TabIndex = 4;
            this.lblNakagaininCdTo.Tag = "DISPNAME";
            this.lblNakagaininCdTo.Text = "最　後";
            this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(462, 3);
            this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(24, 24);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Tag = "CHANGE";
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCdFr
            // 
            this.txtShiiresakiCdFr.AutoSizeFromLength = false;
            this.txtShiiresakiCdFr.DisplayLength = null;
            this.txtShiiresakiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCdFr.Location = new System.Drawing.Point(111, 3);
            this.txtShiiresakiCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiiresakiCdFr.MaxLength = 4;
            this.txtShiiresakiCdFr.Name = "txtShiiresakiCdFr";
            this.txtShiiresakiCdFr.Size = new System.Drawing.Size(52, 23);
            this.txtShiiresakiCdFr.TabIndex = 2;
            this.txtShiiresakiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCdFr_Validating);
            // 
            // lblNakagaininCdFr
            // 
            this.lblNakagaininCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaininCdFr.Location = new System.Drawing.Point(167, 2);
            this.lblNakagaininCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
            this.lblNakagaininCdFr.Size = new System.Drawing.Size(289, 24);
            this.lblNakagaininCdFr.TabIndex = 1;
            this.lblNakagaininCdFr.Tag = "DISPNAME";
            this.lblNakagaininCdFr.Text = "先　頭";
            this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCdTo
            // 
            this.txtShiiresakiCdTo.AutoSizeFromLength = false;
            this.txtShiiresakiCdTo.DisplayLength = null;
            this.txtShiiresakiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCdTo.Location = new System.Drawing.Point(486, 2);
            this.txtShiiresakiCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiiresakiCdTo.MaxLength = 4;
            this.txtShiiresakiCdTo.Name = "txtShiiresakiCdTo";
            this.txtShiiresakiCdTo.Size = new System.Drawing.Size(52, 23);
            this.txtShiiresakiCdTo.TabIndex = 3;
            this.txtShiiresakiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShiiresakiCdTo_KeyDown);
            this.txtShiiresakiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCdTo_Validating);
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(6, 36);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 1;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(850, 36);
            this.fsiTableLayoutPanel1.TabIndex = 905;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.lblNakagaininCdTo);
            this.fsiPanel1.Controls.Add(this.txtShiiresakiCdFr);
            this.fsiPanel1.Controls.Add(this.txtShiiresakiCdTo);
            this.fsiPanel1.Controls.Add(this.lblCodeBet);
            this.fsiPanel1.Controls.Add(this.lblNakagaininCdFr);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(842, 28);
            this.fsiPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(842, 28);
            this.label1.TabIndex = 3;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "仕入先CD範囲";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBCM1013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 175);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBCM1013";
            this.ShowFButton = true;
            this.Text = "仕入先の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtShiiresakiCdFr;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.FsiTextBox txtShiiresakiCdTo;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel1;
		private System.Windows.Forms.Label label1;
	}
}