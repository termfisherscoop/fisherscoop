﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbcm1011
{
    /// <summary>
    /// 仕入先の登録(KBCM1012)
    /// </summary>
    public partial class KBCM1012 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBCM1012()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KBCM1012(string par1) : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：仕入先コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
                this.btnF3.Enabled = true;
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // ボタンの表示非表示を設定
            //this.btnF6.Location = this.btnF3.Location;
            //this.btnF3.Location = this.btnF2.Location;
            //this.btnEsc.Location = this.btnF1.Location;
            //this.btnF2.Visible = false;
            //this.btnF4.Visible = false;
            //this.btnF5.Visible = false;
            //this.btnF7.Visible = false;
            //this.btnF8.Visible = false;
            //this.btnF9.Visible = false;
            //this.btnF10.Visible = false;
            //this.btnF11.Visible = false;
            //this.btnF12.Visible = false;

            // ボタンの配置を調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;

            // ボタンを調整
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 郵便番号１・２、住所１・２、担当者コード、消費税入力方法に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtTantoshaCd":
                case "txtShohizeiNyuryokuHoho":
                case "txtYubinBango1":
                case "txtYubinBango2":
                case "txtJusho1":
                case "txtJusho2":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtTantoshaCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoshaCd.Text = outData[0];
                                this.lblTantoshaNm.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShohizeiNyuryokuHoho":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            // TODO:取得元違う。共通側の修正が必要。
                            frm.Par1 = "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO";
                            frm.InData = this.txtShohizeiNyuryokuHoho.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtShohizeiNyuryokuHoho.Text = result[0];
                                this.lblShzNrkHohoNm.Text = result[1];

                                // 次の項目(締日)にフォーカス
                                this.txtShimebi.Focus();
                            }
                        }
                    }
                    break;

                case "txtYubinBango1":
                case "txtYubinBango2":
                case "txtJusho1":
                case "txtJusho2":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1031.CMCM1031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.ShowDialog();

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtYubinBango1.Text = outData[0];
                                this.txtYubinBango2.Text = outData[1];
                                this.txtJusho1.Text = outData[2];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 削除する前に再度コードの存在チェック
            if (IsCodeUsing())
            {
                Msg.Error("仕入先コードが使用されているため、削除できません。");
                return;
            }

            // 削除処理
            try
            {
                // データ削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.InData);
                this.Dba.Delete("TB_CM_TORIHIKISAKI", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dpc);
                this.Dba.Delete("TB_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dpc);
                this.Dba.Delete("TB_HN_NOHINSAKI", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dpc);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTori = SetCmToriParams();
            ArrayList alParamsHnTori = SetHnToriParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    if (IsCodeExists())
                    {
                        Msg.Error("仕入先情報が登録済みです。");
                        return;
                    }

                    // データ登録
                    // 共通.取引先マスタ
                    this.Dba.Insert("TB_CM_TORIHIKISAKI", (DbParamCollection)alParamsCmTori[0]);
                    // 販売.取引先情報
                    // memo:表示フラグが登録できていない
                    this.Dba.Insert("TB_HN_TORIHIKISAKI_JOHO", (DbParamCollection)alParamsHnTori[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    if (!IsCodeExists())
                    {
                        Msg.Error("仕入先情報が登録されていません。");
                        return;
                    }

                    // データ更新
                    // 共通.取引先マスタ
                    this.Dba.Update("TB_CM_TORIHIKISAKI",
                        (DbParamCollection)alParamsCmTori[1],
                        "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                        (DbParamCollection)alParamsCmTori[0]);
                    // 販売.取引先情報
                    this.Dba.Update("TB_HN_TORIHIKISAKI_JOHO",
                        (DbParamCollection)alParamsHnTori[1],
                        "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                        (DbParamCollection)alParamsHnTori[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 仕入先コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireCd())
            {
                e.Cancel = true;
                this.txtShiireCd.SelectAll();
            }
        }

        /// <summary>
        /// 仕入先名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireNm())
            {
                e.Cancel = true;
                this.txtShiireNm.SelectAll();
            }
        }

        /// <summary>
        /// 仕入先カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireKanaNm())
            {
                e.Cancel = true;
                this.txtShiireKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 郵便番号(上3桁)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango1())
            {
                e.Cancel = true;
                this.txtYubinBango1.SelectAll();
            }
        }

        /// <summary>
        /// 郵便番号(下4桁)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango2())
            {
                e.Cancel = true;
                this.txtYubinBango2.SelectAll();
            }
        }

        /// <summary>
        /// 住所１の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho1())
            {
                e.Cancel = true;
                this.txtJusho1.SelectAll();
            }
        }

        /// <summary>
        /// 住所２の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho2())
            {
                e.Cancel = true;
                this.txtJusho2.SelectAll();
            }
        }

        /// <summary>
        /// 電話番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDenwaBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTel())
            {
                e.Cancel = true;
                this.txtDenwaBango.SelectAll();
            }
        }

        /// <summary>
        /// FAX番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFaxBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFax())
            {
                e.Cancel = true;
                this.txtFaxBango.SelectAll();
            }
        }

        /// <summary>
        /// 担当者コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoshaCd())
            {
                e.Cancel = true;
                this.txtTantoshaCd.SelectAll();
            }
        }

        /// <summary>
        /// 単価取得方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTankaShutokuHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTankaShutokuHoho())
            {
                e.Cancel = true;
                this.txtTankaShutokuHoho.SelectAll();
            }
        }

        /// <summary>
        /// 金額端数処理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKingakuHasuShori_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKingakuHasuShori())
            {
                e.Cancel = true;
                this.txtKingakuHasuShori.SelectAll();
            }
        }

        /// <summary>
        /// 消費税入力方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiNyuryokuHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiNyuryokuHoho())
            {
                e.Cancel = true;
                this.txtShohizeiNyuryokuHoho.SelectAll();
            }
        }

        /// <summary>
        /// 締日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShimebi_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShimebi())
            {
                e.Cancel = true;
                this.txtShimebi.SelectAll();
            }
        }

        /// <summary>
        /// 消費税端数処理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiHasuShori_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiHasuShori())
            {
                e.Cancel = true;
                this.txtShohizeiHasuShori.SelectAll();
            }
        }

        /// <summary>
        /// 消費税転嫁方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiTenkaHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiTenkaHoho())
            {
                e.Cancel = true;
                this.txtShohizeiTenkaHoho.SelectAll();
            }
        }

        /// <summary>
        /// 回収月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKaishuTsuki_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKaishuTsuki())
            {
                e.Cancel = true;
                this.txtKaishuTsuki.SelectAll();
            }
        }

        /// <summary>
        /// 回収日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKaishuBi_Validating(object sender, CancelEventArgs e)
        {
            //if (!IsValidKaishuBi())
            //{
            //    e.Cancel = true;
            //    this.txtKaishuBi.SelectAll();
            //}
        }

        /// <summary>
        /// 一覧表示の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txthyoji_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidhyoji())
            {
                e.Cancel = true;
                this.txthyoji.SelectAll();
            }
        }

        private void txthyoji_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装
            
            // 仕入先コードの初期値を取得
            // 仕入先の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 取引先コードで登録されていない最小の番号を初期表示
            //StringBuilder where = new StringBuilder("TORIHIKISAKI_KUBUN3 = 3");
            //where.Append(" AND KAISHA_CD = @KAISHA_CD");
            //DataTable dtMaxShrSk =
            //    this.Dba.GetDataTableByConditionWithParams("MAX(TORIHIKISAKI_CD) AS MAX_CD",
            //        "TB_HN_TORIHIKISAKI_JOHO", Util.ToString(where), dpc);
            //DataTable dtMaxShrSk =
            //    this.Dba.GetDataTableByConditionWithParams("MIN(TORIHIKISAKI_CD + 1) AS MAX_CD",
            //        "VI_HN_TORIHIKISAKI_JOHO",
            //        "KAISHA_CD = @KAISHA_CD AND (TORIHIKISAKI_CD + 1) NOT IN (SELECT TORIHIKISAKI_CD FROM VI_HN_TORIHIKISAKI_JOHO WHERE KAISHA_CD = @KAISHA_CD)",
            //        dpc);
            DataTable dtMaxShrSk =
                this.Dba.GetDataTableByCondition("MAX(TORIHIKISAKI_CD + 1) AS MAX_CD",
                    "TB_CM_TORIHIKISAKI");
            if (dtMaxShrSk.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxShrSk.Rows[0]["MAX_CD"]))
            {
                //this.txtShiireCd.Text = Util.ToString(Util.ToInt(dtMaxShrSk.Rows[0]["MAX_CD"]) + 1);
                this.txtShiireCd.Text = Util.ToString(Util.ToInt(dtMaxShrSk.Rows[0]["MAX_CD"]));
            }
            else
            {
                this.txtShiireCd.Text = "701";
            }
            // 仕入先コードの値を請求先コードにコピー
            this.txtSeikyusakiCd.Text = this.txtShiireCd.Text;

            // 各項目の初期値を設定
            string ret = "";
            // 担当者コード0
            this.txtTantoshaCd.Text = "0";
            // 単価取得方法0
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "TankaShutokuHoho"));
            this.txtTankaShutokuHoho.Text = ret;
            // 金額端数処理2
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "KingakuHasuShori"));
            this.txtKingakuHasuShori.Text = ret;
            // 消費税入力方法2
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShohizeiNyuryokuHoho"));
            this.txtShohizeiNyuryokuHoho.Text = ret;
            this.lblShzNrkHohoNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text);
            // 締日99
            this.txtShimebi.Text = "99";
            // 請求書発行1
            this.txtSeikyushoHakko.Text = "1";
            // 請求書形式1
            this.txtSeikyushoKeishiki.Text = "1";
            // 消費税端数処理2
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShohizeiHasuShori"));
            this.txtShohizeiHasuShori.Text = ret;
            // 消費税転嫁方法2
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShohizeiTenkaHoho"));
            this.txtShohizeiTenkaHoho.Text = ret;
            // 回収月0
            this.txtKaishuTsuki.Text = "0";
            // 回収日99
            this.txtKaishuBi.Text = "99";
            // 一覧表示
            this.txthyoji.Text = "0";


            // 請求先コード・請求書発行・請求書形式は入力不可
            this.lblSeikyusakiCd.Enabled = false;
            this.txtSeikyusakiCd.Enabled = false;
            this.lblSeikyushoHakko.Enabled = false;
            this.txtSeikyushoHakko.Enabled = false;
            this.lblSeikyushoKeishiki.Enabled = false;
            this.txtSeikyushoKeishiki.Enabled = false;

            // 仕入先名に初期フォーカス
            this.ActiveControl = this.txtShiireNm;
            this.txtShiireNm.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.TORIHIKISAKI_CD");
            cols.Append(" ,A.SEIKYUSAKI_CD");
            cols.Append(" ,A.TANKA_SHUTOKU_HOHO");
            cols.Append(" ,A.SHOHIZEI_NYURYOKU_HOHO");
            cols.Append(" ,A.KINGAKU_HASU_SHORI");
            cols.Append(" ,B.TORIHIKISAKI_NM");
            cols.Append(" ,B.TORIHIKISAKI_KANA_NM");
            cols.Append(" ,B.YUBIN_BANGO1");
            cols.Append(" ,B.YUBIN_BANGO2");
            cols.Append(" ,B.JUSHO1");
            cols.Append(" ,B.JUSHO2");
            cols.Append(" ,B.DENWA_BANGO");
            cols.Append(" ,B.FAX_BANGO");
            cols.Append(" ,B.TANTOSHA_CD");
           cols.Append(" ,B.SIMEBI");
            cols.Append(" ,B.KAISHU_TSUKI");
            cols.Append(" ,B.KAISHU_BI");
            cols.Append(" ,B.HYOJI_FLG");
            cols.Append(" ,B.SEIKYUSHO_HAKKO");
            cols.Append(" ,B.SEIKYUSHO_KEISHIKI");
            cols.Append(" ,B.SHOHIZEI_TENKA_HOHO");
            cols.Append(" ,B.SHOHIZEI_HASU_SHORI");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_TORIHIKISAKI_JOHO AS A");
            from.Append(" INNER JOIN");
            from.Append(" TB_CM_TORIHIKISAKI AS B");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            from.Append(" AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD AND A.SHUBETU_KUBUN=1");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtShiireCd.Text = Util.ToString(drDispData["TORIHIKISAKI_CD"]);
            this.txtShiireNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);
            this.txtShiireKanaNm.Text = Util.ToString(drDispData["TORIHIKISAKI_KANA_NM"]);
            this.txtYubinBango1.Text = Util.ToString(drDispData["YUBIN_BANGO1"]);
            this.txtYubinBango2.Text = Util.ToString(drDispData["YUBIN_BANGO2"]);
            this.txtDenwaBango.Text = Util.ToString(drDispData["DENWA_BANGO"]);
            this.txtFaxBango.Text = Util.ToString(drDispData["FAX_BANGO"]);
            this.txtTantoshaCd.Text = Util.ToString(drDispData["TANTOSHA_CD"]);
            this.lblTantoshaNm.Text =
                this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "", this.txtTantoshaCd.Text);
            this.txtTankaShutokuHoho.Text = Util.ToString(drDispData["TANKA_SHUTOKU_HOHO"]);
            this.txtKingakuHasuShori.Text = Util.ToString(drDispData["KINGAKU_HASU_SHORI"]);
            this.txtShohizeiNyuryokuHoho.Text = Util.ToString(drDispData["SHOHIZEI_NYURYOKU_HOHO"]);
            this.lblShzNrkHohoNm.Text =
                this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text);
            this.txtSeikyusakiCd.Text = Util.ToString(drDispData["SEIKYUSAKI_CD"]);
            this.lblSeikyusakiNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);
            this.txtShimebi.Text = Util.ToString(drDispData["SIMEBI"]);
            this.txtSeikyushoHakko.Text = Util.ToString(drDispData["SEIKYUSHO_HAKKO"]);
            this.txtSeikyushoKeishiki.Text = Util.ToString(drDispData["SEIKYUSHO_KEISHIKI"]);
            this.txtShohizeiHasuShori.Text = Util.ToString(drDispData["SHOHIZEI_HASU_SHORI"]);
            this.txtShohizeiTenkaHoho.Text = Util.ToString(drDispData["SHOHIZEI_TENKA_HOHO"]);
            this.txtKaishuTsuki.Text = Util.ToString(drDispData["KAISHU_TSUKI"]);
            this.txtKaishuBi.Text = Util.ToString(drDispData["KAISHU_BI"]);
            this.txthyoji.Text = Util.ToString(drDispData["HYOJI_FLG"]);

            // 仕入先コード・請求先コード・請求書発行・請求書形式は入力不可
            this.lblShiireCd.Enabled = false;
            this.txtShiireCd.Enabled = false;
            this.lblSeikyusakiCd.Enabled = false;
            this.txtSeikyusakiCd.Enabled = false;
            this.lblSeikyushoHakko.Enabled = false;
            this.txtSeikyushoHakko.Enabled = false;
            this.lblSeikyushoKeishiki.Enabled = false;
            this.txtSeikyushoKeishiki.Enabled = false;

            if (IsCodeUsing())
            {
                // 使用されているので削除不可
                this.btnF3.Enabled = false;
            }
            else
            {
                // 使用されていないので削除OK
                this.btnF3.Enabled = true;
            }
        }

        /// <summary>
        /// 仕入先コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiireCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtShiireCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD");
            DataTable dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("TORIHIKISAKI_CD",
                    "TB_HN_TORIHIKISAKI_JOHO", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                Msg.Error("既に存在する仕入先コードと重複しています。");
                return false;
            }

            // OKの場合、請求先コードに入力値をコピーする
            this.txtSeikyusakiCd.Text = this.txtShiireCd.Text;

            return true;
        }

        /// <summary>
        /// 仕入先名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShiireNm.Text, this.txtShiireNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仕入先カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShiireKanaNm.Text, this.txtShiireKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(上3桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango1()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(下4桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango2()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所１の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho1()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho1.Text, this.txtJusho1.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所２の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho2()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho2.Text, this.txtJusho2.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 電話番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTel()
        {
            //// 15バイトを超えていたらエラー
            //if (!ValChk.IsWithinLength(this.txtTel.Text, this.txtTel.MaxLength))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}
            if (!string.IsNullOrEmpty(this.txtDenwaBango.Text))
            {
                if (!IsPhoneNumber(this.txtDenwaBango.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// FAX番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFax()
        {
            //// 15バイトを超えていたらエラー
            //if (!ValChk.IsWithinLength(this.txtFax.Text, this.txtFax.MaxLength))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}
            if (!string.IsNullOrEmpty(this.txtFaxBango.Text))
            {
                if (!IsPhoneNumber(this.txtFaxBango.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsPhoneNumber(string number)
        {
            return Regex.IsMatch(number, @"^0\d{1,4}-\d{1,4}-\d{4}$");
        }

        /// <summary>
        /// 担当者コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTantoshaCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                this.txtTantoshaCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTantoshaCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtTantoshaCd.Text))
            {
                this.lblTantoshaNm.Text = string.Empty;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "", this.txtTantoshaCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblTantoshaNm.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 単価取得方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTankaShutokuHoho()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtTankaShutokuHoho.Text))
            {
                this.txtTankaShutokuHoho.Text = "0";
            }

            // 0,1,2のみ入力を許可
            if (!ValChk.IsNumber(this.txtTankaShutokuHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtTankaShutokuHoho.Text);
            if (intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 金額端数処理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKingakuHasuShori()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtKingakuHasuShori.Text))
            {
                this.txtKingakuHasuShori.Text = "1";
            }

            // 1,2,3のみ入力を許可
            if (!ValChk.IsNumber(this.txtKingakuHasuShori.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtKingakuHasuShori.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税入力方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiNyuryokuHoho()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShohizeiNyuryokuHoho.Text))
            {
                this.txtShohizeiNyuryokuHoho.Text = "0";
            }
            // 1,2,3のみ入力を許可
            if (!ValChk.IsNumber(this.txtShohizeiNyuryokuHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 登録済みの値のみ許可
            // TODO:取得元の値が違う。共通側も修正する必要あり。
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            this.lblShzNrkHohoNm.Text = name;
            return true;
        }

        /// <summary>
        /// 締日の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShimebi()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShimebi.Text))
            {
                this.txtShimebi.Text = "0";
            }

            // 1～28,99のみ許可
            if (!ValChk.IsNumber(this.txtShimebi.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShimebi.Text);
            if (!((intval >= 1 && intval <= 28) || intval == 99))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税端数処理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiHasuShori()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtShohizeiHasuShori.Text))
            {
                this.txtShohizeiHasuShori.Text = "1";
            }

            // 1,2,3のみ入力許可
            if (!ValChk.IsNumber(this.txtShohizeiHasuShori.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShohizeiHasuShori.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税転嫁方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiTenkaHoho()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtShohizeiTenkaHoho.Text))
            {
                this.txtShohizeiTenkaHoho.Text = "1";
            }

            // 1,2,3のみ入力許可
            if (!ValChk.IsNumber(this.txtShohizeiTenkaHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShohizeiTenkaHoho.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 回収月の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKaishuTsuki()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKaishuTsuki.Text))
            {
                this.txtKaishuTsuki.Text = "0";
            }

            // 数字のみ入力許可
            if (!ValChk.IsNumber(this.txtKaishuTsuki.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 回収日の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKaishuBi()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKaishuBi.Text))
            {
                this.txtKaishuBi.Text = "0";
            }

            // 1～28,99のみ許可
            if (!ValChk.IsNumber(this.txtKaishuBi.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtKaishuBi.Text);
            if (!((intval >= 1 && intval <= 28) || intval == 99))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 一覧表示の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidhyoji()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txthyoji.Text))
            {
                this.txthyoji.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txthyoji.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txthyoji.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 仕入先コードのチェック
                if (!IsValidShiireCd())
                {
                    this.txtShiireCd.Focus();
                    return false;
                }
            }

            // 仕入先名のチェック
            if (!IsValidShiireNm())
            {
                this.txtShiireNm.Focus();
                return false;
            }

            // 仕入先カナ名のチェック
            if (!IsValidShiireNm())
            {
                this.txtShiireNm.Focus();
                return false;
            }

            // 郵便番号１のチェック
            if (!IsValidYubinBango1())
            {
                this.txtYubinBango1.Focus();
                return false;
            }

            // 郵便番号２のチェック
            if (!IsValidYubinBango2())
            {
                this.txtYubinBango2.Focus();
                return false;
            }

            // 郵便番号２だけの入力はエラー
            if (ValChk.IsEmpty(this.txtYubinBango1.Text) &&
                !ValChk.IsEmpty(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtYubinBango1.Focus();
                return false;
            }

            // 住所１のチェック
            if (!IsValidJusho1())
            {
                this.txtJusho1.Focus();
                return false;
            }

            // 住所２のチェック
            if (!IsValidJusho2())
            {
                this.txtJusho2.Focus();
                return false;
            }

            // 電話番号のチェック
            if (!IsValidTel())
            {
                this.txtDenwaBango.Focus();
                return false;
            }

            // FAX番号のチェック
            if (!IsValidFax())
            {
                this.txtFaxBango.Focus();
                return false;
            }


            // 担当者コードのチェック
            if (!IsValidTantoshaCd())
            {
                this.txtTantoshaCd.Focus();
                return false;
            }

            // 単価取得方法のチェック
            if (!IsValidTankaShutokuHoho())
            {
                this.txtTankaShutokuHoho.Focus();
                return false;
            }

            // 金額端数処理のチェック
            if (!IsValidKingakuHasuShori())
            {
                this.txtKingakuHasuShori.Focus();
                return false;
            }

            // 消費税入力方法のチェック
            if (!IsValidShohizeiNyuryokuHoho())
            {
                this.txtShohizeiNyuryokuHoho.Focus();
                return false;
            }

            // 締日のチェック
            if (!IsValidShimebi())
            {
                this.txtShimebi.Focus();
                return false;
            }

            // 消費税端数処理のチェック
            if (!IsValidShohizeiHasuShori())
            {
                this.txtShohizeiHasuShori.Focus();
                return false;
            }

            // 消費税転嫁方法のチェック
            if (!IsValidShohizeiTenkaHoho())
            {
                this.txtShohizeiTenkaHoho.Focus();
                return false;
            }

            // 回収月のチェック
            if (!IsValidKaishuTsuki())
            {
                this.txtKaishuTsuki.Focus();
                return false;
            }

            // 回収日のチェック
            if (!IsValidKaishuBi())
            {
                this.txtKaishuBi.Focus();
                return false;
            }

            // 一覧表示のチェック
            if (!IsValidhyoji())
            {
                this.txthyoji.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// TB_CM_TORIHIKISAKIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと取引先コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtShiireCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと取引先コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtShiireCd.Text);
                alParams.Add(whereParam);
            }

            // 仕入先名
            updParam.SetParam("@TORIHIKISAKI_NM", SqlDbType.VarChar, 40, this.txtShiireNm.Text);
            // 仕入先カナ名
            updParam.SetParam("@TORIHIKISAKI_KANA_NM", SqlDbType.VarChar, 30, this.txtShiireKanaNm.Text);
            // 郵便番号１
            updParam.SetParam("@YUBIN_BANGO1", SqlDbType.VarChar, 3, this.txtYubinBango1.Text);
            // 郵便番号２
            updParam.SetParam("@YUBIN_BANGO2", SqlDbType.VarChar, 4, this.txtYubinBango2.Text);
            // 住所１
            updParam.SetParam("@JUSHO1", SqlDbType.VarChar, 30, this.txtJusho1.Text);
            // 住所２
            updParam.SetParam("@JUSHO2", SqlDbType.VarChar, 30, this.txtJusho2.Text);
            // 電話番号
            updParam.SetParam("@DENWA_BANGO", SqlDbType.VarChar, 15, this.txtDenwaBango.Text);
            // FAX番号
            updParam.SetParam("@FAX_BANGO", SqlDbType.VarChar, 15, this.txtFaxBango.Text);
            // 担当者コード
            updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.txtTantoshaCd.Text);
            // 締日
            updParam.SetParam("@SIMEBI", SqlDbType.Decimal, 2, this.txtShimebi.Text);
            // 回収月
            updParam.SetParam("@KAISHU_TSUKI", SqlDbType.Decimal, 2, this.txtKaishuTsuki.Text);
            // 回収日
            updParam.SetParam("@KAISHU_BI", SqlDbType.Decimal, 2, this.txtKaishuBi.Text);
            // 請求書発行
            updParam.SetParam("@SEIKYUSHO_HAKKO", SqlDbType.Decimal, 1, this.txtSeikyushoHakko.Text);
            // 請求書形式
            updParam.SetParam("@SEIKYUSHO_KEISHIKI", SqlDbType.Decimal, 1, this.txtSeikyushoKeishiki.Text);
            // 消費税転嫁方法
            updParam.SetParam("@SHOHIZEI_TENKA_HOHO", SqlDbType.Decimal, 1, this.txtShohizeiTenkaHoho.Text);
            // 消費税端数処理
            updParam.SetParam("@SHOHIZEI_HASU_SHORI", SqlDbType.Decimal, 1, this.txtShohizeiHasuShori.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            // 表示区分
            updParam.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, this.txthyoji.Text);


            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_TORIHIKISAKI_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetHnToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと取引先コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtShiireCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと取引先コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtShiireCd.Text);
                alParams.Add(whereParam);
            }

            // 請求先コード
            updParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 4, this.txtShiireCd.Text);
            // TODO:事業区分には3を更新だはず
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, 3);
            // 単価取得方法
            updParam.SetParam("@TANKA_SHUTOKU_HOHO", SqlDbType.Decimal, 1, this.txtTankaShutokuHoho.Text);
            // 消費税入力方法
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, this.txtShohizeiNyuryokuHoho.Text);
            // 金額端数処理
            updParam.SetParam("@KINGAKU_HASU_SHORI", SqlDbType.Decimal, 1, this.txtKingakuHasuShori.Text);
            // 取引先区分3
            updParam.SetParam("@SHUBETU_KUBUN", SqlDbType.Decimal, 4, 1);

            updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, txtTantoshaCd.Text);


            updParam.SetParam("@TORIHIKISAKI_KUBUN3", SqlDbType.Decimal, 4, 3);
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO", SqlDbType.Decimal, 9, 0);
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO_ZBN", SqlDbType.Decimal, 3, 0);
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO_KIS", SqlDbType.Decimal, 9, 0);
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO_SR", SqlDbType.Decimal, 9, 0);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 対象の取引先コードがマスタ登録されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeExists()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtShiireCd.Text);
            DataTable dtCmTorihikisaki = this.Dba.GetDataTableByConditionWithParams("*", "TB_CM_TORIHIKISAKI", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dpc);
            DataTable dtHnTorihikisaki = this.Dba.GetDataTableByConditionWithParams("*", "TB_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dpc);

            if (dtCmTorihikisaki.Rows.Count > 0 || dtHnTorihikisaki.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 対象の取引先コードがトランテーブルで使用されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeUsing()
        {
            // 削除可否チェック
            // 指定した取引先コードが使用されているかどうかをチェック
            // 仕訳明細
            StringBuilder from = new StringBuilder();
            from.Append("TB_ZM_SHIWAKE_MEISAI AS A");
            from.Append(" LEFT OUTER JOIN");
            from.Append(" TB_ZM_KANJO_KAMOKU AS B");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            from.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            from.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");

            StringBuilder where = new StringBuilder();
            where.Append("A.KAISHA_CD = @KAISHA_CD");
            where.Append(" AND A.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
            where.Append(" AND B.HOJO_SHIYO_KUBUN = 2");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtSwkMsi = this.Dba.GetDataTableByConditionWithParams("A.HOJO_KAMOKU_CD", from.ToString(), where.ToString(), dpc);

            // 取引伝票
            from = new StringBuilder();
            from.Append("TB_HN_TORIHIKI_DENPYO");

            where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND (TOKUISAKI_CD = @TORIHIKISAKI_CD OR SEIKYUSAKI_CD = @TORIHIKISAKI_CD)");

            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtToriDpy = this.Dba.GetDataTableByConditionWithParams("*", from.ToString(), where.ToString(), dpc);

            if (dtSwkMsi.Rows.Count > 0 || dtToriDpy.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

    }
}
