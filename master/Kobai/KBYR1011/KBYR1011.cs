﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbyr1011
{
    /// <summary>
    /// 売上高証明書(KBYR1011)
    /// </summary>
    public partial class KBYR1011 : BasePgForm
    {
        #region 定数
        private const string KOBAI_NYUKIN_KAMOKU = "141";
        private const string SEIHYO_NYUKIN_KAMOKU = "143";
        private const string KOBAI_URIAGE_KAMOKU = "611";
        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        //private const int prtCols = 36;
        private const int prtCols = 48;
        #endregion

        #region 変数
        /// <summary>
        /// 入金区分（入金相殺を行うか）
        /// </summary>
        private string _nyukinkbn = "0";

        /// <summary>
        /// レポート対応（１：名護、２：石川、沿岸）
        /// </summary>
        private string _report = "2";

        /// <summary>
        /// データ取得ストアド名
        /// </summary>
        private string _stpName = "KOBAI_URIAGE_SHOMEISHO";

        /// <summary>
        /// 条件パターン（1:年（石川パターン）、2:年月範囲＋実績無しも対象するしない（沿岸パターン））
        /// </summary>
        private int _conditionPtn = 1;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBYR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 設定読み込み
            try
            {
                _nyukinkbn = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBYR1011", "Setting", "nyukinkbn");
                _report = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBYR1011", "Setting", "report");
                _stpName = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBYR1011", "Setting", "stpName");
                _conditionPtn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBYR1011", "Setting", "conditionPtn"));
            }
            catch (Exception)
            {
                _nyukinkbn = "0";
                _report = "2";
                _stpName = "KOBAI_URIAGE_SHOMEISHO";
                _conditionPtn = 1;
            }

            // 現在の年号を取得する
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 取得された場合、年号をラベルに反映する
            this.lblEra.Text = Util.ToString(jpDate[0]);
            this.txtYear.Text = jpDate[2];

            // 日付範囲前
            jpDate = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = "1";
            // 日付範囲後
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = "12";

            // 初期フォーカス
            if (this._conditionPtn == 1)
            {
                this.label3.Visible = false;
                this.cbxZeroOutput.Visible = false;
                this.label3.Visible = true;
                this.txtYear.Focus();
            }
            else
            {
                this.label3.Visible = false;
                this.label3.Visible = true;
                this.cbxZeroOutput.Visible = true;
                this.txtDateYearFr.Focus();
            }
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、担当者コード１・２、船主コード１・２に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYear":
                case "txtCodeFr":
                case "txtCodeTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveControl.Name)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtCodeFr":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtCodeFr.Text = outData[0];
                                this.lblCodeFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtCodeTo":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtCodeTo.Text = outData[0];
                                this.lblCodeTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYear":
                    #region 元号(年指定)
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblEra.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblEra.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblEra.Text,
                                        this.txtYear.Text,
                                        "12",
                                        "1",
                                        this.Dba);
                                this.lblEra.Text = arrJpDate[0];
                                this.txtYear.Text = arrJpDate[2];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearFr":
                    #region 元号(日付範囲)
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        "12",
                                        "1",
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearTo":
                    #region 元号(日付範囲 締め)
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        "12",
                                        "1",
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBYR1011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYear())
            {
                e.Cancel = true;
                this.txtYear.SelectAll();
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblEra.Text, this.txtYear.Text,
                "12", "1", this.Dba));
        }

        /// <summary>
        /// 船主CD(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtCodeFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主CD(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtCodeFr.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtCodeTo.Focus();
                }
            }
        }

        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;

                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年の入力チェック
        /// </summary>
        private bool IsValidYear()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtYear.Text))
            {
                Msg.Notice("年は数値のみで入力してください。");
                return false;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtYear.Text))
            {
                this.txtYear.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtCodeFr.Text))
            {
                this.lblCodeFr.Text = "先　頭";
            }
            else
            {
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtCodeFr.Text))
                {
                    Msg.Notice("船主CD(自)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                    string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "",this.txtCodeFr.Text);

                    this.lblCodeFr.Text = name;
                }
            }
            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtCodeTo.Text))
            {
                this.lblCodeTo.Text = "最　後";
            }
            else
            {
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtCodeTo.Text))
                {
                    Msg.Notice("船主CD(至)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                    string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "",this.txtCodeTo.Text);


                    this.lblCodeTo.Text = name;
                }
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba));
        }


        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年のチェック
            if (!IsValidYear())
            {
                this.txtYear.Focus();
                this.txtYear.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtCodeFr.Focus();
                this.txtCodeFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtCodeTo.Focus();
                this.txtCodeTo.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();


            // 期間範囲のチェック
            if (this._conditionPtn == 2)
            {
                DateTime denpyoDateFr = Util.ConvAdDate(this.lblEra.Text, this.txtYear.Text, "1", "1", this.Dba);
                DateTime denpyoDateTo = Util.ConvAdDate(this.lblEra.Text, this.txtYear.Text, "12", "31", this.Dba);
                denpyoDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, "1", this.Dba);
                int lastDay = DateTime.DaysInMonth(denpyoDateTo.Year, denpyoDateTo.Month);
                denpyoDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, "1", this.Dba);
                denpyoDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, lastDay.ToString(), this.Dba);
                int maxMon = (denpyoDateTo.Month + (denpyoDateTo.Year - denpyoDateFr.Year) * 12) - denpyoDateFr.Month;
                if (maxMon > 12)
                {
                    Msg.Notice("入力に誤りがあります（最大12ヶ月）。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblEra.Text = arrJpDate[0];
            this.txtYear.Text = arrJpDate[2];
        }
        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
        }
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                //this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                //dataFlag = MakeWkData();
                dataFlag = MakeWkData01();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    //// 帳票オブジェクトをインスタンス化
                    jp.co.fsi.common.report.BaseReport rpt;
                    if (this._report == "1")
                    {
                        // 帳票オブジェクトをインスタンス化
                        rpt = new KBYR10111R(dtOutput);
                    }
                    else
                    {
                        // 帳票オブジェクトをインスタンス化
                        rpt = new KBYR10112R(dtOutput);
                    }
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;
                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                //this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        #region コメント
        ///// <summary>
        ///// 抽出条件を元にワークテーブルのデータを作成します。
        ///// </summary>
        //private bool MakeWkData()
        //{
        //    // 現在の日付を和暦で取得
        //    string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
        //    // 月と日を0詰め
        //    string today = jpDate[5].Replace(" ", "0");

        //    // 期間を設定
        //    string kikanFr = "平成" + this.txtYear.Text + "年01月01日";
        //    string kikanTo = "平成" + this.txtYear.Text + "年12月31日";
        //    // 検索条件用期間を設定
        //    DateTime denpyoDateFr = Util.ConvAdDate(this.lblEra.Text, this.txtYear.Text, "1", "1", this.Dba);
        //    DateTime denpyoDateTo = Util.ConvAdDate(this.lblEra.Text, this.txtYear.Text, "12", "31", this.Dba);
        //    // 船主コード設定
        //    string funanusiCdFr;
        //    string funanusiCdTo;
        //    if (Util.ToDecimal(txtCodeFr.Text) > 0)
        //    {
        //        funanusiCdFr = txtCodeFr.Text;
        //    }
        //    else
        //    {
        //        funanusiCdFr = "0";
        //    }
        //    if (Util.ToDecimal(txtCodeTo.Text) > 0)
        //    {
        //        funanusiCdTo = txtCodeTo.Text;
        //    }
        //    else
        //    {
        //        funanusiCdTo = "9999";
        //    }

        //    // 小計
        //    Decimal kobaiSyokei;
        //    Decimal seihyoSyokei;

        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder Sql = new StringBuilder();

        //    #region 会社情報を取得(Com.TB_会社情報)
        //    dpc = new DbParamCollection();
        //    Sql = new StringBuilder();
        //    Sql.Append("SELECT");
        //    Sql.Append(" KAISHA_NM, ");
        //    Sql.Append(" JUSHO1, ");
        //    Sql.Append(" DAIHYOSHA_NM ");
        //    Sql.Append("FROM");
        //    Sql.Append(" TB_CM_KAISHA_JOHO");

        //    DataTable dtKaishaJoho = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
        //    #endregion

        //    #region 取引明細情報を取得(Han.VI_取引明細)
        //    dpc = new DbParamCollection();
        //    Sql = new StringBuilder();
        //    Sql.Append("SELECT");
        //    Sql.Append("    T.TORIHIKISAKI_CD,");
        //    Sql.Append("    T.TORIHIKISAKI_NM,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU1, 0) + ISNULL(N.KOBAI_NYUKIN1, 0) AS KOBAI_KINGAKU1,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU1, 0) + ISNULL(N.SEIHYO_NYUKIN1, 0) AS SEIHYO_KINGAKU1,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU2, 0) + ISNULL(N.KOBAI_NYUKIN2, 0) AS KOBAI_KINGAKU2,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU2, 0) + ISNULL(N.SEIHYO_NYUKIN2, 0) AS SEIHYO_KINGAKU2,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU3, 0) + ISNULL(N.KOBAI_NYUKIN3, 0) AS KOBAI_KINGAKU3,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU3, 0) + ISNULL(N.SEIHYO_NYUKIN3, 0) AS SEIHYO_KINGAKU3,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU4, 0) + ISNULL(N.KOBAI_NYUKIN4, 0) AS KOBAI_KINGAKU4,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU4, 0) + ISNULL(N.SEIHYO_NYUKIN4, 0) AS SEIHYO_KINGAKU4,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU5, 0) + ISNULL(N.KOBAI_NYUKIN5, 0) AS KOBAI_KINGAKU5,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU5, 0) + ISNULL(N.SEIHYO_NYUKIN5, 0) AS SEIHYO_KINGAKU5,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU6, 0) + ISNULL(N.KOBAI_NYUKIN6, 0) AS KOBAI_KINGAKU6,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU6, 0) + ISNULL(N.SEIHYO_NYUKIN6, 0) AS SEIHYO_KINGAKU6,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU7, 0) + ISNULL(N.KOBAI_NYUKIN7, 0) AS KOBAI_KINGAKU7,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU7, 0) + ISNULL(N.SEIHYO_NYUKIN7, 0) AS SEIHYO_KINGAKU7,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU8, 0) + ISNULL(N.KOBAI_NYUKIN8, 0) AS KOBAI_KINGAKU8,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU8, 0) + ISNULL(N.SEIHYO_NYUKIN8, 0) AS SEIHYO_KINGAKU8,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU9, 0) + ISNULL(N.KOBAI_NYUKIN9, 0) AS KOBAI_KINGAKU9,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU9, 0) + ISNULL(N.SEIHYO_NYUKIN9, 0) AS SEIHYO_KINGAKU9,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU10, 0) + ISNULL(N.KOBAI_NYUKIN10, 0) AS KOBAI_KINGAKU10,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU10, 0) + ISNULL(N.SEIHYO_NYUKIN10, 0) AS SEIHYO_KINGAKU10,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU11, 0) + ISNULL(N.KOBAI_NYUKIN11, 0) AS KOBAI_KINGAKU11,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU11, 0) + ISNULL(N.SEIHYO_NYUKIN11, 0) AS SEIHYO_KINGAKU11,");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU12, 0) + ISNULL(N.KOBAI_NYUKIN12, 0) AS KOBAI_KINGAKU12,");
        //    Sql.Append("    ISNULL(G.SEIHYO_KINGAKU12, 0) + ISNULL(N.SEIHYO_NYUKIN12, 0) AS SEIHYO_KINGAKU12 ");
        //    Sql.Append("FROM");
        //    Sql.Append("    VI_HN_TORIHIKISAKI_JOHO AS T ");
        //    Sql.Append("LEFT JOIN ");
        //    Sql.Append("    (SELECT");
        //    Sql.Append("        CONVERT(int, A.KAIIN_BANGO) AS TORIHIKISAKI_CD,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  1 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU1,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  2 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU2,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  3 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU3,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  4 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU4,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  5 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU5,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  6 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU6,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  7 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU7,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  8 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU8,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  9 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU9,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) = 10 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU10,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) = 11 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU11,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) = 12 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD = 1      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS KOBAI_KINGAKU12,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  1 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU1,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  2 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU2,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  3 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU3,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  4 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU4,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  5 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU5,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  6 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU6,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  7 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU7,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  8 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU8,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) =  9 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU9,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) = 10 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU10,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) = 11 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU11,");
        //    Sql.Append("        SUM(CASE WHEN MONTH(A.DENPYO_DATE) = 12 AND A.TORIHIKI_KUBUN1 = 2 AND C.URIAGE_SHIWAKE_CD IN(2,3)      THEN CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0)) * -1 ELSE A.ZEINUKI_KINGAKU + ISNULL(A.SHOHIZEI, 0) END ELSE 0 END) AS SEIHYO_KINGAKU12");
        //    Sql.Append("    ");
        //    Sql.Append("    FROM");
        //    Sql.Append("        VI_HN_TORIHIKI_MEISAI AS A");
        //    Sql.Append("    LEFT OUTER JOIN");
        //    Sql.Append("        TB_HN_SHOHIN AS C");
        //    Sql.Append("    ON");
        //    Sql.Append("        A.KAISHA_CD = C.KAISHA_CD AND");
        //    Sql.Append("        A.SHOHIN_CD = C.SHOHIN_CD AND");
        //    Sql.Append("        C.BARCODE1 <> @BARCODE1");
        //    Sql.Append("    WHERE");
        //    Sql.Append("        A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO");
        //    Sql.Append("    GROUP BY");
        //    Sql.Append("        A.KAIIN_BANGO) AS G ");
        //    Sql.Append("ON");
        //    Sql.Append("    T.TORIHIKISAKI_CD = G.TORIHIKISAKI_CD ");
        //    Sql.Append("LEFT JOIN ");
        //    Sql.Append("    (SELECT");
        //    Sql.Append("        A.TORIHIKISAKI_CD,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN1) AS KOBAI_NYUKIN1,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN1) AS SEIHYO_NYUKIN1,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN2) AS KOBAI_NYUKIN2,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN2) AS SEIHYO_NYUKIN2,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN3) AS KOBAI_NYUKIN3,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN3) AS SEIHYO_NYUKIN3,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN4) AS KOBAI_NYUKIN4,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN4) AS SEIHYO_NYUKIN4,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN5) AS KOBAI_NYUKIN5,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN5) AS SEIHYO_NYUKIN5,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN6) AS KOBAI_NYUKIN6,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN6) AS SEIHYO_NYUKIN6,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN7) AS KOBAI_NYUKIN7,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN7) AS SEIHYO_NYUKIN7,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN8) AS KOBAI_NYUKIN8,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN8) AS SEIHYO_NYUKIN8,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN9) AS KOBAI_NYUKIN9,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN9) AS SEIHYO_NYUKIN9,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN10) AS KOBAI_NYUKIN10,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN10) AS SEIHYO_NYUKIN10,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN11) AS KOBAI_NYUKIN11,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN11) AS SEIHYO_NYUKIN11,");
        //    Sql.Append("        SUM(KOBAI_NYUKIN12) AS KOBAI_NYUKIN12,");
        //    Sql.Append("        SUM(SEIHYO_NYUKIN12) AS SEIHYO_NYUKIN12");
        //    Sql.Append("    FROM");
        //    Sql.Append("        (SELECT");
        //    Sql.Append("             KASHIKATA_HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 1 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN1,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 1 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN1,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 2 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN2,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 2 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN2,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 3 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN3,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 3 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN3,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 4 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN4,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 4 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN4,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 5 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN5,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 5 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN5,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 6 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN6,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 6 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN6,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 7 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN7,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 7 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN7,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 8 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN8,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 8 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN8,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 9 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN9,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 9 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN9,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 10 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN10,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 10 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN10,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 11 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN11,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 11 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN11,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 12 AND KASHIKATA_KANJO_KAMOKU_CD = 141 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS KOBAI_NYUKIN12,");
        //    Sql.Append("             SUM(CASE WHEN MONTH(DENPYO_DATE) = 12 AND KASHIKATA_KANJO_KAMOKU_CD = 143 THEN KASHIKATA_ZEIKOMI_KINGAKU ELSE 0 END) AS SEIHYO_NYUKIN12");
        //    Sql.Append("        FROM");
        //    Sql.Append("            VI_ZM_SHIWAKE_DENPYO");
        //    Sql.Append("        WHERE");

        //    // 「KARIKATA_KANJO_KAMOKU_CD <> 611」ではなく、VI_ZM_KANJO_KAMOKUとJOINして、「KAMOKU_KUBUN <> 3」かもしれない。
        //    Sql.Append("            DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND KARIKATA_KANJO_KAMOKU_CD <> 611");

        //    Sql.Append("        GROUP BY");
        //    Sql.Append("            KASHIKATA_HOJO_KAMOKU_CD");
        //    Sql.Append("        ) AS A");
        //    Sql.Append("    LEFT OUTER JOIN");
        //    Sql.Append("        VI_HN_TORIHIKISAKI_JOHO AS B");
        //    Sql.Append("    ON");
        //    Sql.Append("        A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD");
        //    Sql.Append("    WHERE");
        //    Sql.Append("        B.KAISHA_CD = @KAISHA_CD AND");
        //    Sql.Append("        B.TORIHIKISAKI_KUBUN1 = @TORIHIKISAKI_KUBUN1");
        //    Sql.Append("    GROUP BY");
        //    Sql.Append("        A.TORIHIKISAKI_CD) AS N ");
        //    Sql.Append("ON");
        //    Sql.Append("    T.TORIHIKISAKI_CD = N.TORIHIKISAKI_CD ");
        //    Sql.Append("WHERE");
        //    Sql.Append("    T.KAISHA_CD = @KAISHA_CD AND");
        //    Sql.Append("    T.TORIHIKISAKI_KUBUN1 = @TORIHIKISAKI_KUBUN1 AND");
        //    Sql.Append("    T.TORIHIKISAKI_CD BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO AND");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU1, 0) + ISNULL(N.KOBAI_NYUKIN1, 0) + ISNULL(G.SEIHYO_KINGAKU1, 0) + ISNULL(N.SEIHYO_NYUKIN1, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU2, 0) + ISNULL(N.KOBAI_NYUKIN2, 0) + ISNULL(G.SEIHYO_KINGAKU2, 0) + ISNULL(N.SEIHYO_NYUKIN2, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU3, 0) + ISNULL(N.KOBAI_NYUKIN3, 0) + ISNULL(G.SEIHYO_KINGAKU3, 0) + ISNULL(N.SEIHYO_NYUKIN3, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU4, 0) + ISNULL(N.KOBAI_NYUKIN4, 0) + ISNULL(G.SEIHYO_KINGAKU4, 0) + ISNULL(N.SEIHYO_NYUKIN4, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU5, 0) + ISNULL(N.KOBAI_NYUKIN5, 0) + ISNULL(G.SEIHYO_KINGAKU5, 0) + ISNULL(N.SEIHYO_NYUKIN5, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU6, 0) + ISNULL(N.KOBAI_NYUKIN6, 0) + ISNULL(G.SEIHYO_KINGAKU6, 0) + ISNULL(N.SEIHYO_NYUKIN6, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU7, 0) + ISNULL(N.KOBAI_NYUKIN7, 0) + ISNULL(G.SEIHYO_KINGAKU7, 0) + ISNULL(N.SEIHYO_NYUKIN7, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU8, 0) + ISNULL(N.KOBAI_NYUKIN8, 0) + ISNULL(G.SEIHYO_KINGAKU8, 0) + ISNULL(N.SEIHYO_NYUKIN8, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU9, 0) + ISNULL(N.KOBAI_NYUKIN9, 0) + ISNULL(G.SEIHYO_KINGAKU9, 0) + ISNULL(N.SEIHYO_NYUKIN9, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU10, 0) + ISNULL(N.KOBAI_NYUKIN10, 0) + ISNULL(G.SEIHYO_KINGAKU10, 0) + ISNULL(N.SEIHYO_NYUKIN10, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU11, 0) + ISNULL(N.KOBAI_NYUKIN11, 0) + ISNULL(G.SEIHYO_KINGAKU11, 0) + ISNULL(N.SEIHYO_NYUKIN11, 0) +");
        //    Sql.Append("    ISNULL(G.KOBAI_KINGAKU12, 0) + ISNULL(N.KOBAI_NYUKIN12, 0) + ISNULL(G.SEIHYO_KINGAKU12, 0) + ISNULL(N.SEIHYO_NYUKIN12, 0) > 0 ");
        //    Sql.Append("ORDER BY");
        //    Sql.Append("    T.TORIHIKISAKI_CD");

        //    dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
        //    dpc.SetParam("@TORIHIKISAKI_KUBUN1", SqlDbType.Decimal, 1, 1);
        //    dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.Decimal, 4, funanusiCdFr);
        //    dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.Decimal, 4, funanusiCdTo);
        //    dpc.SetParam("@BARCODE1", SqlDbType.VarChar, 3, "999");
        //    dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, 10, denpyoDateFr.Date);
        //    dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, 10, denpyoDateTo.Date);

        //    DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
        //    #endregion

        //    if (dtMainLoop.Rows.Count == 0)
        //    {
        //        Msg.Info("該当データがありません。");
        //        return false;
        //    }
        //    else
        //    {
        //        int i = 0; // ループ用カウント変数

        //        if (dtMainLoop.Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in dtMainLoop.Rows)
        //            {
        //                #region 印刷ワークテーブルに登録
        //                // 入力された情報を元にワークテーブルに更新をする
        //                Sql = new StringBuilder();
        //                dpc = new DbParamCollection();
        //                Sql.Append("INSERT INTO PR_HN_TBL(");
        //                Sql.Append("  GUID");
        //                Sql.Append(" ,SORT");
        //                Sql.Append(" ,ITEM01");
        //                Sql.Append(" ,ITEM02");
        //                Sql.Append(" ,ITEM03");
        //                Sql.Append(" ,ITEM04");
        //                Sql.Append(" ,ITEM05");
        //                Sql.Append(" ,ITEM06");
        //                Sql.Append(" ,ITEM07");
        //                Sql.Append(" ,ITEM08");
        //                Sql.Append(" ,ITEM09");
        //                Sql.Append(" ,ITEM10");
        //                Sql.Append(" ,ITEM11");
        //                Sql.Append(" ,ITEM12");
        //                Sql.Append(" ,ITEM13");
        //                Sql.Append(" ,ITEM14");
        //                Sql.Append(" ,ITEM15");
        //                Sql.Append(" ,ITEM16");
        //                Sql.Append(" ,ITEM17");
        //                Sql.Append(" ,ITEM18");
        //                Sql.Append(" ,ITEM19");
        //                Sql.Append(" ,ITEM20");
        //                Sql.Append(" ,ITEM21");
        //                Sql.Append(" ,ITEM22");
        //                Sql.Append(" ,ITEM23");
        //                Sql.Append(" ,ITEM24");
        //                Sql.Append(" ,ITEM25");
        //                Sql.Append(" ,ITEM26");
        //                Sql.Append(" ,ITEM27");
        //                Sql.Append(" ,ITEM28");
        //                Sql.Append(" ,ITEM29");
        //                Sql.Append(" ,ITEM30");
        //                Sql.Append(" ,ITEM31");
        //                Sql.Append(" ,ITEM32");
        //                Sql.Append(" ,ITEM33");
        //                Sql.Append(" ,ITEM34");
        //                Sql.Append(" ,ITEM35");
        //                Sql.Append(" ,ITEM36");
        //                Sql.Append(") ");
        //                Sql.Append("VALUES(");
        //                Sql.Append("  @GUID");
        //                Sql.Append(" ,@SORT");
        //                Sql.Append(" ,@ITEM01");
        //                Sql.Append(" ,@ITEM02");
        //                Sql.Append(" ,@ITEM03");
        //                Sql.Append(" ,@ITEM04");
        //                Sql.Append(" ,@ITEM05");
        //                Sql.Append(" ,@ITEM06");
        //                Sql.Append(" ,@ITEM07");
        //                Sql.Append(" ,@ITEM08");
        //                Sql.Append(" ,@ITEM09");
        //                Sql.Append(" ,@ITEM10");
        //                Sql.Append(" ,@ITEM11");
        //                Sql.Append(" ,@ITEM12");
        //                Sql.Append(" ,@ITEM13");
        //                Sql.Append(" ,@ITEM14");
        //                Sql.Append(" ,@ITEM15");
        //                Sql.Append(" ,@ITEM16");
        //                Sql.Append(" ,@ITEM17");
        //                Sql.Append(" ,@ITEM18");
        //                Sql.Append(" ,@ITEM19");
        //                Sql.Append(" ,@ITEM20");
        //                Sql.Append(" ,@ITEM21");
        //                Sql.Append(" ,@ITEM22");
        //                Sql.Append(" ,@ITEM23");
        //                Sql.Append(" ,@ITEM24");
        //                Sql.Append(" ,@ITEM25");
        //                Sql.Append(" ,@ITEM26");
        //                Sql.Append(" ,@ITEM27");
        //                Sql.Append(" ,@ITEM28");
        //                Sql.Append(" ,@ITEM29");
        //                Sql.Append(" ,@ITEM30");
        //                Sql.Append(" ,@ITEM31");
        //                Sql.Append(" ,@ITEM32");
        //                Sql.Append(" ,@ITEM33");
        //                Sql.Append(" ,@ITEM34");
        //                Sql.Append(" ,@ITEM35");
        //                Sql.Append(" ,@ITEM36");
        //                Sql.Append(") ");

        //                // 小計の計算
        //                kobaiSyokei = Util.ToDecimal(dr["KOBAI_KINGAKU1"]) + Util.ToDecimal(dr["KOBAI_KINGAKU2"]) + Util.ToDecimal(dr["KOBAI_KINGAKU3"])
        //                            + Util.ToDecimal(dr["KOBAI_KINGAKU4"]) + Util.ToDecimal(dr["KOBAI_KINGAKU5"]) + Util.ToDecimal(dr["KOBAI_KINGAKU6"])
        //                            + Util.ToDecimal(dr["KOBAI_KINGAKU7"]) + Util.ToDecimal(dr["KOBAI_KINGAKU8"]) + Util.ToDecimal(dr["KOBAI_KINGAKU9"])
        //                            + Util.ToDecimal(dr["KOBAI_KINGAKU10"]) + Util.ToDecimal(dr["KOBAI_KINGAKU11"]) + Util.ToDecimal(dr["KOBAI_KINGAKU12"]);

        //                seihyoSyokei = Util.ToDecimal(dr["SEIHYO_KINGAKU1"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU2"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU3"])
        //                             + Util.ToDecimal(dr["SEIHYO_KINGAKU4"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU5"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU6"])
        //                             + Util.ToDecimal(dr["SEIHYO_KINGAKU7"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU8"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU9"])
        //                             + Util.ToDecimal(dr["SEIHYO_KINGAKU10"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU11"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU12"]);

        //                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
        //                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
        //                // ページヘッダーデータを設定
        //                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, ""); // 証 番号(現状、空で表示)
        //                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, today); // 日付
        //                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["TORIHIKISAKI_NM"]); // 船主名
        //                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKaishaJoho.Rows[0]["JUSHO1"]); // 企業情報01
        //                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtKaishaJoho.Rows[0]["KAISHA_NM"]); // 企業情報02
        //                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtKaishaJoho.Rows[0]["DAIHYOSHA_NM"]); // 企業情報03
        //                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, kikanFr); // 期間(自)
        //                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, kikanTo); // 期間(至)
        //                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "\\" + Util.FormatNum(kobaiSyokei + seihyoSyokei)); // 合計値
        //                // データをセット
        //                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU1"])); // 購買01
        //                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU2"])); // 購買02
        //                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU3"])); // 購買03
        //                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU4"])); // 購買04
        //                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU5"])); // 購買05
        //                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU6"])); // 購買06
        //                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU7"])); // 購買07
        //                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU8"])); // 購買08
        //                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU9"])); // 購買09
        //                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU10"])); // 購買10
        //                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU11"])); // 購買11
        //                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU12"])); // 購買12
        //                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(kobaiSyokei)); // 購買小計
        //                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU1"])); // 製氷01
        //                dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU2"])); // 製氷02
        //                dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU3"])); // 製氷03
        //                dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU4"])); // 製氷04
        //                dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU5"])); // 製氷05
        //                dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU6"])); // 製氷06
        //                dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU7"])); // 製氷07
        //                dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU8"])); // 製氷08
        //                dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU9"])); // 製氷09
        //                dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU10"])); // 製氷10
        //                dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU11"])); // 製氷11
        //                dpc.SetParam("@ITEM34", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU12"])); // 製氷12
        //                dpc.SetParam("@ITEM35", SqlDbType.VarChar, 200, Util.FormatNum(seihyoSyokei)); // 製氷小計
        //                dpc.SetParam("@ITEM36", SqlDbType.VarChar, 200, dr["TORIHIKISAKI_CD"]); // 船主CD
        //                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
        //                i++;
        //            }
        //            #endregion
        //        }
        //    }

        //    // 印刷ワークテーブルのデータ件数を取得
        //    dpc = new DbParamCollection();
        //    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
        //    DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
        //        "SORT",
        //        "PR_HN_TBL",
        //        "GUID = @GUID",
        //        dpc);

        //    bool dataFlag;
        //    if (tmpdtPR_HN_TBL.Rows.Count > 0)
        //    {
        //        dataFlag = true;
        //    }
        //    else
        //    {
        //        dataFlag = false;
        //    }

        //    return dataFlag;
        //}
        #endregion


        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData01()
        {
            #region データ取得準備
            // 現在の日付を和暦で取得
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 月と日を0詰め
            string today = jpDate[5].Replace(" ", "0");

            // 期間を設定
            string kikanFr = "平成" + this.txtYear.Text + "年01月01日";
            string kikanTo = "平成" + this.txtYear.Text + "年12月31日";
            // 検索条件用期間を設定
            DateTime denpyoDateFr = Util.ConvAdDate(this.lblEra.Text, this.txtYear.Text, "1", "1", this.Dba);
            DateTime denpyoDateTo = Util.ConvAdDate(this.lblEra.Text, this.txtYear.Text, "12", "31", this.Dba);
            if (this._conditionPtn == 1)
            {
                kikanFr = this.lblEra.Text + this.txtYear.Text + "年01月01日";
                kikanTo = this.lblEra.Text + this.txtYear.Text + "年12月31日";
            }
            else if (this._conditionPtn == 2)
            {
                denpyoDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, "1", this.Dba);
                int lastDay = DateTime.DaysInMonth(denpyoDateTo.Year, denpyoDateTo.Month);
                denpyoDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, "1", this.Dba);
                denpyoDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, lastDay.ToString(), this.Dba);
                jpDate = Util.ConvJpDate(denpyoDateFr, this.Dba);
                kikanFr = jpDate[0] + jpDate[2] + "年" + Util.ToInt(jpDate[3]).ToString("00") + "月" + Util.ToInt(jpDate[4]).ToString("00") + "日";
                jpDate = Util.ConvJpDate(denpyoDateTo, this.Dba);
                kikanTo = jpDate[0] + jpDate[2] + "年" + Util.ToInt(jpDate[3]).ToString("00") + "月" + Util.ToInt(jpDate[4]).ToString("00") + "日";
            }

            // 月情報の設定
            int maxMon = (denpyoDateTo.Month + (denpyoDateTo.Year - denpyoDateFr.Year) * 12) - denpyoDateFr.Month;
            string[] mon = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
            for (int i = 0; i < 12; i++)
            {
                if (i <= maxMon)
                    mon[i] = denpyoDateFr.AddMonths(i).Month.ToString();
                else
                    mon[i] = "";
            }

            // 船主コード設定
            string funanusiCdFr;
            string funanusiCdTo;
            if (Util.ToDecimal(txtCodeFr.Text) > 0)
            {
                funanusiCdFr = txtCodeFr.Text;
            }
            else
            {
                funanusiCdFr = "0";
            }
            if (Util.ToDecimal(txtCodeTo.Text) > 0)
            {
                funanusiCdTo = txtCodeTo.Text;
            }
            else
            {
                funanusiCdTo = "9999";
            }
            string shishoCd = this.txtMizuageShishoCd.Text;

            // 小計
            Decimal kobaiSyokei;
            Decimal seihyoSyokei;

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
#endregion

            #region 会社情報を取得(Com.TB_会社情報)
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" KAISHA_NM, ");
            Sql.Append(" JUSHO1, ");
            Sql.Append(" DAIHYOSHA_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_CM_KAISHA_JOHO");
            DataTable dtKaishaJoho = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region メインデータ取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            //Sql.Append("EXEC KOBAI_URIAGE_SHOMEISHO");
            Sql.Append("EXEC " + this._stpName);
            Sql.Append(" @KAISHA_CD,");
            Sql.Append(" @SHISHO_CD,");
            Sql.Append(" @TORIHIKISAKI_CD_FR,");
            Sql.Append(" @TORIHIKISAKI_CD_TO,");
            Sql.Append(" @DENPYO_DATE_FR,");
            Sql.Append(" @DENPYO_DATE_TO,");
            Sql.Append(" @TORIHIKISAKI_KUBUN1,");
            //Sql.Append(" 1"); //入金（売掛回収の取得）を指定（名護仕様）
            //Sql.Append(" " + this._nyukinkbn); //入金（売掛回収の取得）を指定（名護仕様）
            if (this._conditionPtn == 2)
            {
                Sql.Append(" @KAISHU_DATA_FLG,");
                Sql.Append(" @ZEROPUT_FLG");
            }
            else
                Sql.Append(" @KAISHU_DATA_FLG ");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@TORIHIKISAKI_KUBUN1", SqlDbType.Decimal, 1, 1);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.Decimal, 4, funanusiCdFr);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.Decimal, 4, funanusiCdTo);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, 10, denpyoDateFr.Date);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, 10, denpyoDateTo.Date);

            dpc.SetParam("@KAISHU_DATA_FLG", SqlDbType.Decimal, 1, this._nyukinkbn);
            if (this._conditionPtn == 2)
                dpc.SetParam("@ZEROPUT_FLG", SqlDbType.Decimal, 1, (this.cbxZeroOutput.Checked ? 1 : 0));

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                int i = 0; // ループ用カウント変数

                if (dtMainLoop.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtMainLoop.Rows)
                    {
                        #region 印刷ワークテーブルに登録
                        // 入力された情報を元にワークテーブルに更新をする
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        //Sql.Append(" ,ITEM01");
                        //Sql.Append(" ,ITEM02");
                        //Sql.Append(" ,ITEM03");
                        //Sql.Append(" ,ITEM04");
                        //Sql.Append(" ,ITEM05");
                        //Sql.Append(" ,ITEM06");
                        //Sql.Append(" ,ITEM07");
                        //Sql.Append(" ,ITEM08");
                        //Sql.Append(" ,ITEM09");
                        //Sql.Append(" ,ITEM10");
                        //Sql.Append(" ,ITEM11");
                        //Sql.Append(" ,ITEM12");
                        //Sql.Append(" ,ITEM13");
                        //Sql.Append(" ,ITEM14");
                        //Sql.Append(" ,ITEM15");
                        //Sql.Append(" ,ITEM16");
                        //Sql.Append(" ,ITEM17");
                        //Sql.Append(" ,ITEM18");
                        //Sql.Append(" ,ITEM19");
                        //Sql.Append(" ,ITEM20");
                        //Sql.Append(" ,ITEM21");
                        //Sql.Append(" ,ITEM22");
                        //Sql.Append(" ,ITEM23");
                        //Sql.Append(" ,ITEM24");
                        //Sql.Append(" ,ITEM25");
                        //Sql.Append(" ,ITEM26");
                        //Sql.Append(" ,ITEM27");
                        //Sql.Append(" ,ITEM28");
                        //Sql.Append(" ,ITEM29");
                        //Sql.Append(" ,ITEM30");
                        //Sql.Append(" ,ITEM31");
                        //Sql.Append(" ,ITEM32");
                        //Sql.Append(" ,ITEM33");
                        //Sql.Append(" ,ITEM34");
                        //Sql.Append(" ,ITEM35");
                        //Sql.Append(" ,ITEM36");
                        //Sql.Append(") ");
                        //Sql.Append("VALUES(");
                        //Sql.Append("  @GUID");
                        //Sql.Append(" ,@SORT");
                        //Sql.Append(" ,@ITEM01");
                        //Sql.Append(" ,@ITEM02");
                        //Sql.Append(" ,@ITEM03");
                        //Sql.Append(" ,@ITEM04");
                        //Sql.Append(" ,@ITEM05");
                        //Sql.Append(" ,@ITEM06");
                        //Sql.Append(" ,@ITEM07");
                        //Sql.Append(" ,@ITEM08");
                        //Sql.Append(" ,@ITEM09");
                        //Sql.Append(" ,@ITEM10");
                        //Sql.Append(" ,@ITEM11");
                        //Sql.Append(" ,@ITEM12");
                        //Sql.Append(" ,@ITEM13");
                        //Sql.Append(" ,@ITEM14");
                        //Sql.Append(" ,@ITEM15");
                        //Sql.Append(" ,@ITEM16");
                        //Sql.Append(" ,@ITEM17");
                        //Sql.Append(" ,@ITEM18");
                        //Sql.Append(" ,@ITEM19");
                        //Sql.Append(" ,@ITEM20");
                        //Sql.Append(" ,@ITEM21");
                        //Sql.Append(" ,@ITEM22");
                        //Sql.Append(" ,@ITEM23");
                        //Sql.Append(" ,@ITEM24");
                        //Sql.Append(" ,@ITEM25");
                        //Sql.Append(" ,@ITEM26");
                        //Sql.Append(" ,@ITEM27");
                        //Sql.Append(" ,@ITEM28");
                        //Sql.Append(" ,@ITEM29");
                        //Sql.Append(" ,@ITEM30");
                        //Sql.Append(" ,@ITEM31");
                        //Sql.Append(" ,@ITEM32");
                        //Sql.Append(" ,@ITEM33");
                        //Sql.Append(" ,@ITEM34");
                        //Sql.Append(" ,@ITEM35");
                        //Sql.Append(" ,@ITEM36");
                        //Sql.Append(") ");
                        Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                        Sql.Append(") ");

                        // 小計の計算
                        kobaiSyokei = Util.ToDecimal(dr["KOBAI_KINGAKU1"]) + Util.ToDecimal(dr["KOBAI_KINGAKU2"]) + Util.ToDecimal(dr["KOBAI_KINGAKU3"])
                                    + Util.ToDecimal(dr["KOBAI_KINGAKU4"]) + Util.ToDecimal(dr["KOBAI_KINGAKU5"]) + Util.ToDecimal(dr["KOBAI_KINGAKU6"])
                                    + Util.ToDecimal(dr["KOBAI_KINGAKU7"]) + Util.ToDecimal(dr["KOBAI_KINGAKU8"]) + Util.ToDecimal(dr["KOBAI_KINGAKU9"])
                                    + Util.ToDecimal(dr["KOBAI_KINGAKU10"]) + Util.ToDecimal(dr["KOBAI_KINGAKU11"]) + Util.ToDecimal(dr["KOBAI_KINGAKU12"]);

                        seihyoSyokei = Util.ToDecimal(dr["SEIHYO_KINGAKU1"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU2"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU3"])
                                     + Util.ToDecimal(dr["SEIHYO_KINGAKU4"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU5"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU6"])
                                     + Util.ToDecimal(dr["SEIHYO_KINGAKU7"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU8"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU9"])
                                     + Util.ToDecimal(dr["SEIHYO_KINGAKU10"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU11"]) + Util.ToDecimal(dr["SEIHYO_KINGAKU12"]);

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, ""); // 証 番号(現状、空で表示)
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, today); // 日付
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["TORIHIKISAKI_NM"]); // 船主名
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKaishaJoho.Rows[0]["JUSHO1"]); // 企業情報01
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtKaishaJoho.Rows[0]["KAISHA_NM"]); // 企業情報02
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtKaishaJoho.Rows[0]["DAIHYOSHA_NM"]); // 企業情報03
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, kikanFr); // 期間(自)
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, kikanTo); // 期間(至)
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "\\" + Util.FormatNum(kobaiSyokei + seihyoSyokei)); // 合計値
                        // データをセット
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU1"])); // 購買01
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU2"])); // 購買02
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU3"])); // 購買03
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU4"])); // 購買04
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU5"])); // 購買05
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU6"])); // 購買06
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU7"])); // 購買07
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU8"])); // 購買08
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU9"])); // 購買09
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU10"])); // 購買10
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU11"])); // 購買11
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dr["KOBAI_KINGAKU12"])); // 購買12
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(kobaiSyokei)); // 購買小計
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU1"])); // 製氷01
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU2"])); // 製氷02
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU3"])); // 製氷03
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU4"])); // 製氷04
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU5"])); // 製氷05
                        dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU6"])); // 製氷06
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU7"])); // 製氷07
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU8"])); // 製氷08
                        dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU9"])); // 製氷09
                        dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU10"])); // 製氷10
                        dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU11"])); // 製氷11
                        dpc.SetParam("@ITEM34", SqlDbType.VarChar, 200, Util.FormatNum(dr["SEIHYO_KINGAKU12"])); // 製氷12
                        dpc.SetParam("@ITEM35", SqlDbType.VarChar, 200, Util.FormatNum(seihyoSyokei)); // 製氷小計
                        dpc.SetParam("@ITEM36", SqlDbType.VarChar, 200, dr["TORIHIKISAKI_CD"]); // 船主CD

                        dpc.SetParam("@ITEM37", SqlDbType.VarChar, 200, mon[0]); // 月
                        dpc.SetParam("@ITEM38", SqlDbType.VarChar, 200, mon[1]); // 月
                        dpc.SetParam("@ITEM39", SqlDbType.VarChar, 200, mon[2]); // 月
                        dpc.SetParam("@ITEM40", SqlDbType.VarChar, 200, mon[3]); // 月
                        dpc.SetParam("@ITEM41", SqlDbType.VarChar, 200, mon[4]); // 月
                        dpc.SetParam("@ITEM42", SqlDbType.VarChar, 200, mon[5]); // 月
                        dpc.SetParam("@ITEM43", SqlDbType.VarChar, 200, mon[6]); // 月
                        dpc.SetParam("@ITEM44", SqlDbType.VarChar, 200, mon[7]); // 月
                        dpc.SetParam("@ITEM45", SqlDbType.VarChar, 200, mon[8]); // 月
                        dpc.SetParam("@ITEM46", SqlDbType.VarChar, 200, mon[9]); // 月
                        dpc.SetParam("@ITEM47", SqlDbType.VarChar, 200, mon[10]); // 月
                        dpc.SetParam("@ITEM48", SqlDbType.VarChar, 200, mon[11]); // 月

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        i++;
                    }
                    #endregion
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        #endregion

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
