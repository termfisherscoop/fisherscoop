﻿namespace jp.co.fsi.kb.kbyr1011
{
    partial class KBYR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCodeFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeFr = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblCodeTo = new System.Windows.Forms.Label();
            this.txtCodeTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.lblEra = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateYearTo = new System.Windows.Forms.Label();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateYearFr = new System.Windows.Forms.Label();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.cbxZeroOutput = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "";
            // 
            // txtCodeFr
            // 
            this.txtCodeFr.AutoSizeFromLength = false;
            this.txtCodeFr.DisplayLength = null;
            this.txtCodeFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtCodeFr.Location = new System.Drawing.Point(139, 6);
            this.txtCodeFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtCodeFr.MaxLength = 4;
            this.txtCodeFr.Name = "txtCodeFr";
            this.txtCodeFr.Size = new System.Drawing.Size(65, 23);
            this.txtCodeFr.TabIndex = 0;
            this.txtCodeFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCodeFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblCodeFr
            // 
            this.lblCodeFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblCodeFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCodeFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeFr.Location = new System.Drawing.Point(213, 5);
            this.lblCodeFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeFr.Name = "lblCodeFr";
            this.lblCodeFr.Size = new System.Drawing.Size(272, 24);
            this.lblCodeFr.TabIndex = 1;
            this.lblCodeFr.Tag = "DISPNAME";
            this.lblCodeFr.Text = "先　頭";
            this.lblCodeFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.AutoSize = true;
            this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(497, 9);
            this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(24, 16);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Tag = "CHANGE";
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeTo
            // 
            this.lblCodeTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblCodeTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCodeTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeTo.Location = new System.Drawing.Point(607, 5);
            this.lblCodeTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeTo.Name = "lblCodeTo";
            this.lblCodeTo.Size = new System.Drawing.Size(272, 24);
            this.lblCodeTo.TabIndex = 4;
            this.lblCodeTo.Tag = "DISPNAME";
            this.lblCodeTo.Text = "最　後";
            this.lblCodeTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCodeTo
            // 
            this.txtCodeTo.AutoSizeFromLength = false;
            this.txtCodeTo.DisplayLength = null;
            this.txtCodeTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtCodeTo.Location = new System.Drawing.Point(532, 6);
            this.txtCodeTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtCodeTo.MaxLength = 4;
            this.txtCodeTo.Name = "txtCodeTo";
            this.txtCodeTo.Size = new System.Drawing.Size(65, 23);
            this.txtCodeTo.TabIndex = 3;
            this.txtCodeTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCodeTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodeTo_KeyDown);
            this.txtCodeTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // txtYear
            // 
            this.txtYear.AutoSizeFromLength = false;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYear.Location = new System.Drawing.Point(205, 6);
            this.txtYear.Margin = new System.Windows.Forms.Padding(5);
            this.txtYear.MaxLength = 2;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(39, 23);
            this.txtYear.TabIndex = 2;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYear.Visible = false;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // lblYearFr
            // 
            this.lblYearFr.AutoSize = true;
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.Location = new System.Drawing.Point(249, 9);
            this.lblYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(24, 16);
            this.lblYearFr.TabIndex = 3;
            this.lblYearFr.Tag = "CHANGE";
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblYearFr.Visible = false;
            // 
            // lblEra
            // 
            this.lblEra.BackColor = System.Drawing.Color.LightCyan;
            this.lblEra.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEra.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEra.Location = new System.Drawing.Point(145, 5);
            this.lblEra.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEra.Name = "lblEra";
            this.lblEra.Size = new System.Drawing.Size(55, 24);
            this.lblEra.TabIndex = 1;
            this.lblEra.Tag = "DISPNAME";
            this.lblEra.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblEra.Visible = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(139, 6);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(192, 5);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.AutoSize = true;
            this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetDate.Location = new System.Drawing.Point(368, 9);
            this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(24, 16);
            this.lblCodeBetDate.TabIndex = 6;
            this.lblCodeBetDate.Tag = "CHANGE";
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.AutoSize = true;
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthTo.Location = new System.Drawing.Point(605, 9);
            this.lblDateMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(24, 16);
            this.lblDateMonthTo.TabIndex = 0;
            this.lblDateMonthTo.Tag = "CHANGE";
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearTo
            // 
            this.lblDateYearTo.AutoSize = true;
            this.lblDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearTo.Location = new System.Drawing.Point(527, 9);
            this.lblDateYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateYearTo.Name = "lblDateYearTo";
            this.lblDateYearTo.Size = new System.Drawing.Size(24, 16);
            this.lblDateYearTo.TabIndex = 11;
            this.lblDateYearTo.Tag = "CHANGE";
            this.lblDateYearTo.Text = "年";
            this.lblDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearTo.Location = new System.Drawing.Point(486, 6);
            this.txtDateYearTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(39, 23);
            this.txtDateYearTo.TabIndex = 10;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthTo.Location = new System.Drawing.Point(562, 6);
            this.txtDateMonthTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(39, 23);
            this.txtDateMonthTo.TabIndex = 12;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoTo.Location = new System.Drawing.Point(426, 5);
            this.lblDateGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(55, 24);
            this.lblDateGengoTo.TabIndex = 9;
            this.lblDateGengoTo.Tag = "DISPNAME";
            this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.AutoSize = true;
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthFr.Location = new System.Drawing.Point(318, 9);
            this.lblDateMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(24, 16);
            this.lblDateMonthFr.TabIndex = 5;
            this.lblDateMonthFr.Tag = "CHANGE";
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearFr
            // 
            this.lblDateYearFr.AutoSize = true;
            this.lblDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearFr.Location = new System.Drawing.Point(243, 9);
            this.lblDateYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateYearFr.Name = "lblDateYearFr";
            this.lblDateYearFr.Size = new System.Drawing.Size(24, 16);
            this.lblDateYearFr.TabIndex = 3;
            this.lblDateYearFr.Tag = "CHANGE";
            this.lblDateYearFr.Text = "年";
            this.lblDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearFr.Location = new System.Drawing.Point(199, 6);
            this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(39, 23);
            this.txtDateYearFr.TabIndex = 2;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthFr.Location = new System.Drawing.Point(274, 6);
            this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(39, 23);
            this.txtDateMonthFr.TabIndex = 4;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoFr.Location = new System.Drawing.Point(139, 5);
            this.lblDateGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(55, 24);
            this.lblDateGengoFr.TabIndex = 1;
            this.lblDateGengoFr.Tag = "DISPNAME";
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxZeroOutput
            // 
            this.cbxZeroOutput.AutoSize = true;
            this.cbxZeroOutput.BackColor = System.Drawing.Color.Silver;
            this.cbxZeroOutput.Checked = true;
            this.cbxZeroOutput.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxZeroOutput.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cbxZeroOutput.Location = new System.Drawing.Point(117, 51);
            this.cbxZeroOutput.Margin = new System.Windows.Forms.Padding(4);
            this.cbxZeroOutput.MinimumSize = new System.Drawing.Size(0, 32);
            this.cbxZeroOutput.Name = "cbxZeroOutput";
            this.cbxZeroOutput.Size = new System.Drawing.Size(187, 32);
            this.cbxZeroOutput.TabIndex = 5;
            this.cbxZeroOutput.Tag = "CHANGE";
            this.cbxZeroOutput.Text = "実績無しも表示を行う";
            this.cbxZeroOutput.UseVisualStyleBackColor = false;
            this.cbxZeroOutput.Visible = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.MinimumSize = new System.Drawing.Size(0, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(894, 34);
            this.label1.TabIndex = 0;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "支所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(894, 34);
            this.label2.TabIndex = 0;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "年指定";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Visible = false;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(894, 34);
            this.label3.TabIndex = 0;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "日付範囲";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(894, 35);
            this.label4.TabIndex = 0;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "船主CD範囲";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(904, 131);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtCodeFr);
            this.fsiPanel4.Controls.Add(this.txtCodeTo);
            this.fsiPanel4.Controls.Add(this.lblCodeTo);
            this.fsiPanel4.Controls.Add(this.cbxZeroOutput);
            this.fsiPanel4.Controls.Add(this.lblCodeFr);
            this.fsiPanel4.Controls.Add(this.lblCodeBet);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 91);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(894, 35);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.lblDateGengoFr);
            this.fsiPanel3.Controls.Add(this.txtDateMonthFr);
            this.fsiPanel3.Controls.Add(this.txtDateYearFr);
            this.fsiPanel3.Controls.Add(this.lblCodeBetDate);
            this.fsiPanel3.Controls.Add(this.lblDateYearFr);
            this.fsiPanel3.Controls.Add(this.lblDateMonthFr);
            this.fsiPanel3.Controls.Add(this.lblDateGengoTo);
            this.fsiPanel3.Controls.Add(this.txtDateMonthTo);
            this.fsiPanel3.Controls.Add(this.lblDateMonthTo);
            this.fsiPanel3.Controls.Add(this.txtDateYearTo);
            this.fsiPanel3.Controls.Add(this.lblDateYearTo);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 48);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(894, 34);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblEra);
            this.fsiPanel2.Controls.Add(this.lblYearFr);
            this.fsiPanel2.Controls.Add(this.txtYear);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Location = new System.Drawing.Point(9, 187);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(894, 34);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            this.fsiPanel2.Visible = false;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(894, 34);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // KBYR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.fsiPanel2);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBYR1011";
            this.Text = "ReportSample";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.fsiPanel2, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtCodeFr;
        private System.Windows.Forms.Label lblCodeFr;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblCodeTo;
        private jp.co.fsi.common.controls.FsiTextBox txtCodeTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYear;
        private System.Windows.Forms.Label lblEra;
        private System.Windows.Forms.Label lblYearFr;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label lblDateYearTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateYearFr;
        private common.controls.FsiTextBox txtDateYearFr;
        private common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.CheckBox cbxZeroOutput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}