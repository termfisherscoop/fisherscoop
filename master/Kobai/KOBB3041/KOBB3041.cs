﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kob.kobb3041
{
    /// <summary>
    /// 棚卸更新(KOBB3041)
    /// </summary>
    public partial class KOBB3041 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KOBB3041()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 棚卸テーブルから最新の棚卸日付を取得
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            DataTable dtTnors = this.Dba.GetDataTableByConditionWithParams(
                "MAX(TANAOROSHI_DATE) AS 最終棚卸日付", "TB_HN_TANAOROSHI", "KAISHA_CD = @KAISHA_CD", dpc);

            DateTime initialDate = DateTime.Now;

            if (dtTnors.Rows.Count > 0 && !ValChk.IsEmpty(dtTnors.Rows[0]["最終棚卸日付"]))
            {
                // 取得してきた日付を設定
                initialDate = Util.ToDate(dtTnors.Rows[0]["最終棚卸日付"]);
            }

            // 和暦変換して画面に表示
            string[] arrJpDate = Util.ConvJpDate(initialDate, this.Dba);
            this.lblTnorsDtGengo.Text = arrJpDate[0];
            this.txtTnorsDtJpYear.Text = arrJpDate[2];
            this.txtTnorsDtMonth.Text = arrJpDate[3];
            this.txtTnorsDtDay.Text = arrJpDate[4];

            // 初期フォーカスを設定
            this.txtTnorsDtJpYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtTnorsDtJpYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            System.Reflection.Assembly asm = null;
            Type t = null;

            switch (this.ActiveCtlNm)
            {
                case "txtTnorsDtJpYear":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblTnorsDtGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblTnorsDtGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblTnorsDtGengo.Text,
                                        this.txtTnorsDtJpYear.Text,
                                        this.txtTnorsDtMonth.Text,
                                        this.txtTnorsDtDay.Text,
                                        this.Dba);
                                this.lblTnorsDtGengo.Text = arrJpDate[0];
                                this.txtTnorsDtJpYear.Text = arrJpDate[2];
                                this.txtTnorsDtMonth.Text = arrJpDate[3];
                                this.txtTnorsDtDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 登録処理
            // 全項目の入力チェック
            if (!ValidateAll())
            {
                return;
            }

            if (Msg.ConfYesNo("更新しますか？") == DialogResult.No)
            {
                // 「いいえ」が押されたら処理終了
                return;
            }

            // 更新中メッセージ表示
            KOBB3042 msgFrm = new KOBB3042();
            msgFrm.Show();
            msgFrm.Refresh();

            try
            {
                this.Dba.BeginTransaction();

                DbParamCollection dpc;
                DbParamCollection updDpc;
                DateTime tanaoroshiDate = Util.ConvAdDate(this.lblTnorsDtGengo.Text, this.txtTnorsDtJpYear.Text,
                    this.txtTnorsDtMonth.Text, this.txtTnorsDtDay.Text, this.Dba);

                // 店舗データを取得
                // TODO:目的が不明
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@TENPO_CD", SqlDbType.Decimal, 4, 0);
                DataTable dtTenpo = this.Dba.GetDataTableByConditionWithParams("*", "TB_HN_TENPO",
                    "KAISHA_CD = @KAISHA_CD AND TENPO_CD = @TENPO_CD", dpc);

                // 棚卸データを取得
                DataTable dtTanaoroshi = GetTanaoroshiData(tanaoroshiDate);
                // 取引明細から数量を取得
                DataTable dtToriSuryo = GetTorihikiMeisaiSuryoData(tanaoroshiDate);
                DataRow[] drToriSuryo;
                decimal idoSuryo;

                // 現在庫数と棚卸在庫数に差異があるデータについては在庫テーブルに棚卸数量を反映する
                DataTable dtZaikoExChk;
                for (int i = 0; i < dtTanaoroshi.Rows.Count; i++)
                {
                    // TB_HN_ZAIKOの存在チェック
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, dtTanaoroshi.Rows[i]["商品コード"]);
                    dpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, dtTanaoroshi.Rows[i]["倉庫コード"]);
                    dtZaikoExChk = this.Dba.GetDataTableByConditionWithParams("*", "TB_HN_ZAIKO",
                        "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD", dpc);

                    if (dtZaikoExChk.Rows.Count == 0)
                    {
                        // 在庫データの新規登録
                        updDpc = new DbParamCollection();
                        updDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        updDpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, dtTanaoroshi.Rows[i]["商品コード"]);
                        updDpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, dtTanaoroshi.Rows[i]["倉庫コード"]);
                        updDpc.SetParam("@SURYO1", SqlDbType.Decimal, 12, 3, dtTanaoroshi.Rows[i]["実数量１"]);
                        updDpc.SetParam("@SURYO2", SqlDbType.Decimal, 12, 3, dtTanaoroshi.Rows[i]["実数量２"]);
                        updDpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
                        updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

                        this.Dba.Insert("TB_HN_ZAIKO", updDpc);
                    }
                    else
                    {
                        if ((Util.ToDecimal(dtTanaoroshi.Rows[i]["現数量１"]) != Util.ToDecimal(dtTanaoroshi.Rows[i]["実数量１"]))
                            || (Util.ToDecimal(dtTanaoroshi.Rows[i]["現数量２"]) != Util.ToDecimal(dtTanaoroshi.Rows[i]["実数量２"])))
                        {
                            // 取引明細　移動数量を実数量に足す
                            drToriSuryo = dtToriSuryo.Select("SHOHIN_CD =" + dtTanaoroshi.Rows[i]["商品コード"]);
                            if (drToriSuryo.Length > 0)
                            {
                                idoSuryo = Util.ToDecimal(dtTanaoroshi.Rows[i]["実数量２"]) + Util.ToDecimal(drToriSuryo[0]["IDO_SURYO2"]);
                            }
                            else
                            {
                                idoSuryo = Util.ToDecimal(dtTanaoroshi.Rows[i]["実数量２"]);
                            }

                            // 実在庫と棚卸在庫が合わない場合、在庫数の更新
                            // SET句
                            updDpc = new DbParamCollection();
                            updDpc.SetParam("@SURYO1", SqlDbType.Decimal, 12, 3, dtTanaoroshi.Rows[i]["実数量１"]);
                            updDpc.SetParam("@SURYO2", SqlDbType.Decimal, 12, 3, idoSuryo);
                            updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

                            // WHERE句
                            dpc = new DbParamCollection();
                            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, dtTanaoroshi.Rows[i]["商品コード"]);
                            dpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, dtTanaoroshi.Rows[i]["倉庫コード"]);

                            this.Dba.Update("TB_HN_ZAIKO", updDpc,
                                "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD", dpc);
                        }
                    }
                }


                //// 棚卸バックアップ１の全件削除
                //this.Dba.Delete("TB_HN_TANAOROSHI_BACKUP1", null, null);
                
                // 棚卸バックアップ１(購買)全件削除
                this.Dba.Delete("TB_HN_TANAOROSHI_BACKUP1 FROM TB_HN_TANAOROSHI_BACKUP1 AS A LEFT OUTER JOIN TB_HN_SHOHIN AS B ON A.KAISHA_CD = B.KAISHA_CD AND A.SHOHIN_CD = B.SHOHIN_CD",
                    "B.SHOHIN_KUBUN5 <> 1 AND B.BARCODE1 <> 999",
                    null);

                for (int i = 0; i < dtTanaoroshi.Rows.Count; i++)
                {
                    // 棚卸バックアップ１の登録
                    InsertTnorsBkup1(tanaoroshiDate, dtTanaoroshi.Rows[i]);
                }

                // 商品在庫のデータを取得
                DataTable dtZaiko = GetZaikoData();

                //// 棚卸バックアップ２の全件削除
                //this.Dba.Delete("TB_HN_TANAOROSHI_BACKUP2", null, null);

                // 棚卸バックアップ２（購買）全件削除
                this.Dba.Delete("TB_HN_TANAOROSHI_BACKUP2 FROM TB_HN_TANAOROSHI_BACKUP2 AS A LEFT OUTER JOIN TB_HN_SHOHIN AS B ON A.KAISHA_CD = B.KAISHA_CD AND A.SHOHIN_CD = B.SHOHIN_CD",
                    "B.SHOHIN_KUBUN5 <> 1 AND B.BARCODE1 <> 999",
                    null);

                for (int i = 0; i < dtZaiko.Rows.Count; i++)
                {
                    // 棚卸バックアップ２の登録
                    InsertTnorsBkup2(tanaoroshiDate, dtZaiko.Rows[i]);
                }

                //// 棚卸テーブルの削除
                //this.Dba.Delete("TB_HN_TANAOROSHI", null, null);

                // 棚卸テーブルの削除
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
                this.Dba.Delete("TB_HN_TANAOROSHI",
                    "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD IN (SELECT SHOHIN_CD FROM TB_HN_SHOHIN WHERE SHOHIN_KUBUN5 <> 1) AND TANAOROSHI_DATE = @TANAOROSHI_DATE",
                    whereParam);

                this.Dba.Commit();

                // メッセージを表示する
                Msg.InfoNm("棚卸更新", "終了しました。");

                // メッセージ画面を閉じる
                msgFrm.Close();
                // 画面を閉じる
                this.DialogResult = DialogResult.Cancel;
                base.PressEsc();
            }

            catch (Exception)
            {
                Msg.Error("更新に失敗しました。更新処理をやり直して下さい。");
                // メッセージ画面を閉じる
                msgFrm.Close();
                this.Dba.Rollback();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 棚卸日付・年の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSwkDpyDtJpYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSwkDpyDtJpYear())
            {
                e.Cancel = true;
                this.txtTnorsDtJpYear.SelectAll();
            }
        }

        /// <summary>
        /// 棚卸日付・月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSwkDpyDtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSwkDpyDtMonth())
            {
                e.Cancel = true;
                this.txtTnorsDtMonth.SelectAll();
            }
        }

        /// <summary>
        /// 棚卸日付・日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSwkDpyDtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSwkDpyDtDay())
            {
                e.Cancel = true;
                this.txtTnorsDtDay.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 日のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTnorsDtDay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // 登録処理
                // 全項目の入力チェック
                if (!ValidateAll())
                {
                    return;
                }

                if (Msg.ConfYesNo("更新しますか？") == DialogResult.No)
                {
                    // 「いいえ」が押されたら処理終了
                    return;
                }

                // 更新中メッセージ表示
                KOBB3042 msgFrm = new KOBB3042();
                msgFrm.Show();
                msgFrm.Refresh();

                try
                {
                    this.Dba.BeginTransaction();

                    DbParamCollection dpc;
                    DbParamCollection updDpc;
                    DateTime tanaoroshiDate = Util.ConvAdDate(this.lblTnorsDtGengo.Text, this.txtTnorsDtJpYear.Text,
                        this.txtTnorsDtMonth.Text, this.txtTnorsDtDay.Text, this.Dba);

                    // 店舗データを取得
                    // TODO:目的が不明
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@TENPO_CD", SqlDbType.Decimal, 4, 0);
                    DataTable dtTenpo = this.Dba.GetDataTableByConditionWithParams("*", "TB_HN_TENPO",
                        "KAISHA_CD = @KAISHA_CD AND TENPO_CD = @TENPO_CD", dpc);

                    // 棚卸データを取得
                    DataTable dtTanaoroshi = GetTanaoroshiData(tanaoroshiDate);
                    // 取引明細から数量を取得
                    DataTable dtToriSuryo = GetTorihikiMeisaiSuryoData(tanaoroshiDate);
                    DataRow[] drToriSuryo;
                    decimal idoSuryo;

                    // 現在庫数と棚卸在庫数に差異があるデータについては在庫テーブルに棚卸数量を反映する
                    DataTable dtZaikoExChk;
                    for (int i = 0; i < dtTanaoroshi.Rows.Count; i++)
                    {
                        // TB_HN_ZAIKOの存在チェック
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, dtTanaoroshi.Rows[i]["商品コード"]);
                        dpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, dtTanaoroshi.Rows[i]["倉庫コード"]);
                        dtZaikoExChk = this.Dba.GetDataTableByConditionWithParams("*", "TB_HN_ZAIKO",
                            "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD", dpc);

                        if (dtZaikoExChk.Rows.Count == 0)
                        {
                            // 在庫データの新規登録
                            updDpc = new DbParamCollection();
                            updDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                            updDpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, dtTanaoroshi.Rows[i]["商品コード"]);
                            updDpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, dtTanaoroshi.Rows[i]["倉庫コード"]);
                            updDpc.SetParam("@SURYO1", SqlDbType.Decimal, 12, 3, dtTanaoroshi.Rows[i]["実数量１"]);
                            updDpc.SetParam("@SURYO2", SqlDbType.Decimal, 12, 3, dtTanaoroshi.Rows[i]["実数量２"]);
                            updDpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
                            updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

                            this.Dba.Insert("TB_HN_ZAIKO", updDpc);
                        }
                        else
                        {
                            if ((Util.ToDecimal(dtTanaoroshi.Rows[i]["現数量１"]) != Util.ToDecimal(dtTanaoroshi.Rows[i]["実数量１"]))
                                || (Util.ToDecimal(dtTanaoroshi.Rows[i]["現数量２"]) != Util.ToDecimal(dtTanaoroshi.Rows[i]["実数量２"])))
                            {
                                // 取引明細　移動数量を実数量に足す
                                drToriSuryo = dtToriSuryo.Select("SHOHIN_CD =" + dtTanaoroshi.Rows[i]["商品コード"]);
                                if (drToriSuryo.Length > 0)
                                {
                                    idoSuryo = Util.ToDecimal(dtTanaoroshi.Rows[i]["実数量２"]) + Util.ToDecimal(drToriSuryo[0]["IDO_SURYO2"]);
                                }
                                else
                                {
                                    idoSuryo = Util.ToDecimal(dtTanaoroshi.Rows[i]["実数量２"]);
                                }

                                // 実在庫と棚卸在庫が合わない場合、在庫数の更新
                                // SET句
                                updDpc = new DbParamCollection();
                                updDpc.SetParam("@SURYO1", SqlDbType.Decimal, 12, 3, dtTanaoroshi.Rows[i]["実数量１"]);
                                updDpc.SetParam("@SURYO2", SqlDbType.Decimal, 12, 3, idoSuryo);
                                updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

                                // WHERE句
                                dpc = new DbParamCollection();
                                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                                dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, dtTanaoroshi.Rows[i]["商品コード"]);
                                dpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, dtTanaoroshi.Rows[i]["倉庫コード"]);

                                this.Dba.Update("TB_HN_ZAIKO", updDpc,
                                    "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD", dpc);
                            }
                        }
                    }


                    //// 棚卸バックアップ１の全件削除
                    //this.Dba.Delete("TB_HN_TANAOROSHI_BACKUP1", null, null);

                    // 棚卸バックアップ１(購買)全件削除
                    this.Dba.Delete("TB_HN_TANAOROSHI_BACKUP1 FROM TB_HN_TANAOROSHI_BACKUP1 AS A LEFT OUTER JOIN TB_HN_SHOHIN AS B ON A.KAISHA_CD = B.KAISHA_CD AND A.SHOHIN_CD = B.SHOHIN_CD",
                        "B.SHOHIN_KUBUN5 <> 1 AND B.BARCODE1 <> 999",
                        null);

                    for (int i = 0; i < dtTanaoroshi.Rows.Count; i++)
                    {
                        // 棚卸バックアップ１の登録
                        InsertTnorsBkup1(tanaoroshiDate, dtTanaoroshi.Rows[i]);
                    }

                    // 商品在庫のデータを取得
                    DataTable dtZaiko = GetZaikoData();

                    //// 棚卸バックアップ２の全件削除
                    //this.Dba.Delete("TB_HN_TANAOROSHI_BACKUP2", null, null);

                    // 棚卸バックアップ２（購買）全件削除
                    this.Dba.Delete("TB_HN_TANAOROSHI_BACKUP2 FROM TB_HN_TANAOROSHI_BACKUP2 AS A LEFT OUTER JOIN TB_HN_SHOHIN AS B ON A.KAISHA_CD = B.KAISHA_CD AND A.SHOHIN_CD = B.SHOHIN_CD",
                        "B.SHOHIN_KUBUN5 <> 1 AND B.BARCODE1 <> 999",
                        null);

                    for (int i = 0; i < dtZaiko.Rows.Count; i++)
                    {
                        // 棚卸バックアップ２の登録
                        InsertTnorsBkup2(tanaoroshiDate, dtZaiko.Rows[i]);
                    }

                    //// 棚卸テーブルの削除
                    //this.Dba.Delete("TB_HN_TANAOROSHI", null, null);

                    // 棚卸テーブルの削除
                    DbParamCollection whereParam = new DbParamCollection();
                    whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    whereParam.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
                    this.Dba.Delete("TB_HN_TANAOROSHI",
                        "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD IN (SELECT SHOHIN_CD FROM TB_HN_SHOHIN WHERE SHOHIN_KUBUN5 <> 1) AND TANAOROSHI_DATE = @TANAOROSHI_DATE",
                        whereParam);

                    this.Dba.Commit();

                    // メッセージを表示する
                    Msg.InfoNm("棚卸更新", "終了しました。");

                    // メッセージ画面を閉じる
                    msgFrm.Close();
                    // 画面を閉じる
                    this.DialogResult = DialogResult.Cancel;
                    base.PressEsc();
                }
                catch (Exception)
                {
                    Msg.Error("更新に失敗しました。更新処理をやり直して下さい。");
                    // メッセージ画面を閉じる
                    msgFrm.Close();
                    this.Dba.Rollback();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 仕訳伝票日付
            if (!IsValidSwkDpyDtJpYear())
            {
                this.txtTnorsDtJpYear.Focus();
                return false;
            }
            if (!IsValidSwkDpyDtMonth())
            {
                this.txtTnorsDtMonth.Focus();
                return false;
            }
            if (!IsValidSwkDpyDtDay())
            {
                this.txtTnorsDtDay.Focus();
                return false;
            }

            DateTime tanaoroshiDate = Util.ConvAdDate(this.lblTnorsDtGengo.Text, this.txtTnorsDtJpYear.Text,
                this.txtTnorsDtMonth.Text, this.txtTnorsDtDay.Text, this.Dba);

            // 入力した日付が棚卸テーブルに無ければエラー
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
            DataTable dtDateChk = this.Dba.GetDataTableByConditionWithParams("COUNT(*) AS 件数", "TB_HN_TANAOROSHI",
                "KAISHA_CD = @KAISHA_CD AND TANAOROSHI_DATE = @TANAOROSHI_DATE", dpc);

            if (dtDateChk.Rows.Count == 0 || Util.ToInt(dtDateChk.Rows[0]["件数"]) == 0)
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 棚卸日付・年の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidSwkDpyDtJpYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtTnorsDtJpYear.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtTnorsDtJpYear.Text))
            {
                this.txtTnorsDtJpYear.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateToSwkDpyDt(Util.FixJpDate(this.lblTnorsDtGengo.Text,
                this.txtTnorsDtJpYear.Text,
                this.txtTnorsDtMonth.Text,
                this.txtTnorsDtDay.Text,
                this.Dba));

            return true;
        }

        /// <summary>
        /// 棚卸日付・月の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidSwkDpyDtMonth()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtTnorsDtMonth.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtTnorsDtMonth.Text))
            {
                // 空の場合、1月として処理
                this.txtTnorsDtMonth.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtTnorsDtMonth.Text) > 12)
                {
                    this.txtTnorsDtMonth.Text = "12";
                }
            }

            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblTnorsDtGengo.Text, this.txtTnorsDtJpYear.Text,
                this.txtTnorsDtMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtTnorsDtDay.Text) > lastDayInMonth)
            {
                this.txtTnorsDtDay.Text = Util.ToString(lastDayInMonth);
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateToSwkDpyDt(Util.FixJpDate(this.lblTnorsDtGengo.Text,
                this.txtTnorsDtJpYear.Text,
                this.txtTnorsDtMonth.Text,
                this.txtTnorsDtDay.Text,
                this.Dba));

            return true;
        }

        /// <summary>
        /// 棚卸日付・日の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidSwkDpyDtDay()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtTnorsDtDay.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtTnorsDtDay.Text))
            {
                // 空の場合、1日として処理
                this.txtTnorsDtDay.Text = "1";
            }
            else
            {
                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(this.lblTnorsDtGengo.Text,
                    this.txtTnorsDtJpYear.Text,
                    this.txtTnorsDtMonth.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                if (Util.ToInt(this.txtTnorsDtDay.Text) > lastDayInMonth)
                {
                    this.txtTnorsDtDay.Text = Util.ToString(lastDayInMonth);
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateToSwkDpyDt(Util.FixJpDate(this.lblTnorsDtGengo.Text,
                this.txtTnorsDtJpYear.Text,
                this.txtTnorsDtMonth.Text,
                this.txtTnorsDtDay.Text,
                this.Dba));

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を棚卸日付にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateToSwkDpyDt(string[] arrJpDate)
        {
            this.lblTnorsDtGengo.Text = arrJpDate[0];
            this.txtTnorsDtJpYear.Text = arrJpDate[2];
            this.txtTnorsDtMonth.Text = arrJpDate[3];
            this.txtTnorsDtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 棚卸データを取得
        /// </summary>
        /// <param name="tanaoroshiDate">棚卸日付</param>
        /// <returns>棚卸テーブルから取得したデータ</returns>
        private DataTable GetTanaoroshiData(DateTime tanaoroshiDate)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  A.KAIKEI_NENDO    AS 会計年度 ");
            sql.Append(" ,A.SHOHIN_CD       AS 商品コード ");
            sql.Append(" ,A.SOKO_CD         AS 倉庫コード ");
            sql.Append(" ,B.SHOHIN_NM       AS 商品名 ");
            sql.Append(" ,B.IRISU           AS 入数 ");
            sql.Append(" ,A.GEN_SURYO1      AS 現数量１ ");
            sql.Append(" ,A.GEN_SURYO2      AS 現数量２ ");
            sql.Append(" ,A.JITSU_SURYO1    AS 実数量１ ");
            sql.Append(" ,A.JITSU_SURYO2    AS 実数量２ ");
            sql.Append(" ,A.SA_SURYO1       AS 差数量１ ");
            sql.Append(" ,A.SA_SURYO2       AS 差数量２ ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_TANAOROSHI AS A ");
            sql.Append("LEFT OUTER JOIN TB_HN_SHOHIN AS B ");
            sql.Append("ON  A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("AND A.SHOHIN_CD = B.SHOHIN_CD ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND A.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND B.SHOHIN_KUBUN5 <> 1 ");
            sql.Append("AND B.BARCODE1 != '999' ");
            sql.Append("AND A.TANAOROSHI_DATE = @TANAOROSHI_DATE ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        ///// <summary>
        ///// 取引明細　移動数量データを取得
        ///// </summary>
        ///// <param name="tanaoroshiDate">棚卸日付</param>
        ///// <returns>更新日付以降の移動数量</returns>
        //private DataTable GetTorihikiMeisaiSuryoData(DateTime tanaoroshiDate)
        //{
        //    StringBuilder Sql = new StringBuilder();
        //    Sql.Append("SELECT");
        //    Sql.Append("    SHOHIN_CD,");
        //    Sql.Append("    SUM(CASE WHEN BD.DENPYO_KUBUN = 2 AND BD.TORIHIKI_KUBUN2 <> 2 THEN SURYO2 ");
        //    Sql.Append("     ELSE 0 END)");
        //    Sql.Append("  - SUM(CASE WHEN BD.DENPYO_KUBUN = 2 AND BD.TORIHIKI_KUBUN2 = 2 THEN SURYO2 ");
        //    Sql.Append("     ELSE 0 END)");
        //    Sql.Append("  - SUM(CASE WHEN BD.DENPYO_KUBUN = 1 AND BD.TORIHIKI_KUBUN2 <> 2 THEN SURYO2 ");
        //    Sql.Append("     ELSE 0 END)");
        //    Sql.Append("  + SUM(CASE WHEN BD.DENPYO_KUBUN = 1 AND BD.TORIHIKI_KUBUN2 = 2 THEN SURYO2 ");
        //    Sql.Append("     ELSE 0 END) AS IDO_SURYO2");
        //    Sql.Append(" FROM ");
        //    Sql.Append("    VI_HN_TORIHIKI_MEISAI AS BD");
        //    Sql.Append(" WHERE ");
        //    Sql.Append("    BD.DENPYO_DATE > @TANAOROSHI_DATE AND");
        //    Sql.Append("    BD.SHOHIN_KUBUN5 <> 1 AND");
        //    Sql.Append("    BD.ZAIKO_KANRI_KUBUN = 1");
        //    Sql.Append(" GROUP BY ");
        //    Sql.Append("    BD.SHOHIN_CD,");
        //    Sql.Append("    BD.TORIHIKI_KUBUN2");
        //    DbParamCollection dpc = new DbParamCollection();
        //    dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);

        //    DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Sql.ToString(), dpc);
        //    return dtResult;
        //}

        /// <summary>
        /// 取引明細　移動数量データを取得
        /// </summary>
        /// <param name="tanaoroshiDate">棚卸日付</param>
        /// <returns>更新日付以降の移動数量</returns>
        private DataTable GetTorihikiMeisaiSuryoData(DateTime tanaoroshiDate)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append("  C.TANABAN AS TANABAN,");
            sql.Append("  A.SHOHIN_CD AS SHOHIN_CD,");
            sql.Append("  SUM( CASE WHEN B.DENPYO_KUBUN = 1 THEN ");
            sql.Append("        (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("          ((A.SURYO1 * A.IRISU) + A.SURYO2) ");
            sql.Append("         ELSE (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) END");
            sql.Append("        ) ");
            sql.Append("       ELSE ");
            sql.Append("         (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("          (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) ");
            sql.Append("          ELSE ((A.SURYO1 * A.IRISU) + A.SURYO2) END");
            sql.Append("         ) ");
            sql.Append("       END ");
            sql.Append("  ) AS IDO_SURYO2 ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_TORIHIKI_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("    TB_HN_TORIHIKI_DENPYO AS B ");
            sql.Append("ON     A.KAISHA_CD   = B.KAISHA_CD ");
            sql.Append("   AND A.DENPYO_KUBUN     = B.DENPYO_KUBUN ");
            sql.Append("   AND A.DENPYO_BANGO     = B.DENPYO_BANGO ");
            sql.Append("   AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("    TB_HN_SHOHIN AS C ");
            sql.Append("ON     A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("   AND A.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append("WHERE ");
            sql.Append("       A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("   AND C.SHOHIN_KUBUN5 <> 1 ");
            sql.Append("   AND B.DENPYO_DATE  > @TANAOROSHI_DATE ");
            sql.Append("   AND C.ZAIKO_KANRI_KUBUN = @ZAIKO_KANRI_KUBUN ");
            sql.Append("GROUP BY ");
            sql.Append("    C.TANABAN,");
            sql.Append("    A.SHOHIN_CD ");
            sql.Append("ORDER BY ");
            sql.Append("    C.TANABAN,");
            sql.Append("    A.SHOHIN_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
            dpc.SetParam("@ZAIKO_KANRI_KUBUN", SqlDbType.Decimal, 3, 1);
            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            return dtResult;
        }

        /// <summary>
        /// 棚卸バックアップ１の登録
        /// </summary>
        /// <param name="tanaoroshiDate">棚卸日付</param>
        /// <param name="drTnorsData">棚卸データ</param>
        private void InsertTnorsBkup1(DateTime tanaoroshiDate, DataRow drTnorsData)
        {
            // 更新パラメータのセット
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, drTnorsData["商品コード"]);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@GEN_SURYO1", SqlDbType.Decimal, 12, 3, drTnorsData["現数量１"]);
            dpc.SetParam("@GEN_SURYO2", SqlDbType.Decimal, 12, 3, drTnorsData["現数量２"]);
            dpc.SetParam("@JITSU_SURYO1", SqlDbType.Decimal, 12, 3, drTnorsData["実数量１"]);
            dpc.SetParam("@JITSU_SURYO2", SqlDbType.Decimal, 12, 3, drTnorsData["実数量２"]);
            dpc.SetParam("@SA_SURYO1", SqlDbType.Decimal, 12, 3, drTnorsData["差数量１"]);
            dpc.SetParam("@SA_SURYO2", SqlDbType.Decimal, 12, 3, drTnorsData["差数量２"]);
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");

            // データの登録
            this.Dba.Insert("TB_HN_TANAOROSHI_BACKUP1", dpc);
        }

        /// <summary>
        /// 在庫データを取得
        /// </summary>
        /// <returns>商品在庫VIEWから取得したデータ</returns>
        private DataTable GetZaikoData()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  SHOHIN_CD ");
            sql.Append(" ,SURYO1 ");
            sql.Append(" ,SURYO2 ");
            sql.Append(" ,SHIIRE_TANKA ");
            sql.Append("FROM ");
            sql.Append("  VI_HN_SHOHIN_ZAIKO ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND SHOHIN_KUBUN5  <> 1 ");
            sql.Append("AND BARCODE1 <> 999 ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 棚卸バックアップ２の登録
        /// </summary>
        /// <param name="tanaoroshiDate">棚卸日付</param>
        /// <param name="drZaikoData">在庫データ</param>
        private void InsertTnorsBkup2(DateTime tanaoroshiDate, DataRow drZaikoData)
        {
            // 更新パラメータのセット
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, drZaikoData["SHOHIN_CD"]);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@SURYO1", SqlDbType.Decimal, 12, 3, drZaikoData["SURYO1"]);
            dpc.SetParam("@SURYO2", SqlDbType.Decimal, 12, 3, drZaikoData["SURYO2"]);
            dpc.SetParam("@SHIIRE_TANKA", SqlDbType.Decimal, 12, 3, drZaikoData["SHIIRE_TANKA"]);
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");

            // データの登録
            this.Dba.Insert("TB_HN_TANAOROSHI_BACKUP2", dpc);
        }
        #endregion
    }
}
