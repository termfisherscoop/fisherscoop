﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using DDCssLib;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.SectionReportModel;
using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbde1011
{
    // Token: 0x02000005 RID: 5
    public class KBDE10132R : BaseReport
    {
        // Token: 0x06000030 RID: 48 RVA: 0x00002258 File Offset: 0x00000458
        public KBDE10132R(DataTable tgtData) : base(tgtData)
        {
            this.InitializeComponent();
        }

        // Token: 0x06000031 RID: 49 RVA: 0x00002267 File Offset: 0x00000467
        private void pageHeader_Format(object sender, EventArgs e)
        {
        }

        // Token: 0x06000032 RID: 50 RVA: 0x0000224D File Offset: 0x0000044D
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        // Token: 0x06000033 RID: 51 RVA: 0x00013DEC File Offset: 0x00011FEC
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDE10132R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル203 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル196 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル535 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox84 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox85 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox69 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox81 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox83 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox86 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox87 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox88 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox89 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox90 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox91 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox92 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox93 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox94 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox96 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル340 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.BackClrCg = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト332 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト351 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト370 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト231 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト342 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト361 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト258 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル201 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト204 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト205 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線206 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル207 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル208 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト209 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル210 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト211 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト212 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト213 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト215 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト217 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト218 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル219 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト220 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル221 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル222 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル223 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル224 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル225 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル226 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル227 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル228 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル229 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト230 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト232 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト233 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト234 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト235 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト236 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト237 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト238 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト331 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト333 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト334 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト335 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト336 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト337 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト338 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト339 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト341 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト343 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト344 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト345 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト346 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト347 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト348 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト349 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト350 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト352 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト353 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト354 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト355 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト356 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト357 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト358 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト360 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト362 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト363 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト364 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト365 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト366 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト367 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト368 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト369 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト371 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト372 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト373 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス309 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス310 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.テキスト312 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト313 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト314 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト315 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト318 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線407 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト642 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト643 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox95 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル203)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル196)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル535)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル340)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClrCg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト332)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト351)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト370)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト231)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト342)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト361)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト258)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル201)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト204)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト205)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル207)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル208)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト209)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル210)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト211)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト212)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト213)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト215)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト217)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト218)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル219)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト220)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル221)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル222)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル223)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル224)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル225)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル226)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル227)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル228)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル229)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト230)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト232)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト233)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト234)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト235)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト236)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト237)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト238)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト331)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト333)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト334)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト335)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト336)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト337)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト338)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト339)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト341)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト343)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト344)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト345)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト346)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト347)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト348)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト349)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト350)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト352)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト353)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト354)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト355)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト356)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト357)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト358)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト360)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト362)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト363)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト364)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト365)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト366)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト367)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト368)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト369)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト371)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト372)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト373)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト312)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト313)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト314)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト315)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト318)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト642)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト643)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape9,
            this.ラベル203,
            this.shape4,
            this.shape5,
            this.label7,
            this.ラベル196,
            this.ラベル535,
            this.shape1,
            this.line2,
            this.textBox84,
            this.line27,
            this.textBox85,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.label8,
            this.label9,
            this.textBox10,
            this.textBox11,
            this.line1,
            this.label10,
            this.label11,
            this.textBox12,
            this.label12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.label14,
            this.textBox19,
            this.label17,
            this.label18,
            this.label19,
            this.label20,
            this.label21,
            this.label25,
            this.label26,
            this.label33,
            this.label34,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox51,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox73,
            this.shape6,
            this.shape2,
            this.textBox74,
            this.textBox75,
            this.textBox76,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox80,
            this.line15,
            this.textBox81,
            this.textBox82,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line31,
            this.textBox83,
            this.label35,
            this.label36,
            this.label37,
            this.label38,
            this.label39,
            this.label40,
            this.label41,
            this.line28,
            this.label42,
            this.shape3,
            this.textBox86,
            this.textBox87,
            this.textBox88,
            this.textBox89,
            this.shape11,
            this.textBox90,
            this.textBox91,
            this.textBox92,
            this.textBox93,
            this.shape12,
            this.textBox94,
            this.textBox96,
            this.line29,
            this.line30,
            this.line36,
            this.label32,
            this.ラベル340,
            this.label31,
            this.BackClrCg,
            this.label30,
            this.label28,
            this.テキスト332,
            this.テキスト351,
            this.テキスト370,
            this.テキスト231,
            this.テキスト342,
            this.テキスト361,
            this.テキスト258,
            this.ITEM02,
            this.ITEM01,
            this.ラベル71,
            this.ラベル201,
            this.テキスト204,
            this.テキスト205,
            this.直線206,
            this.ラベル207,
            this.ラベル208,
            this.テキスト209,
            this.ラベル210,
            this.テキスト211,
            this.テキスト212,
            this.テキスト213,
            this.テキスト215,
            this.テキスト217,
            this.テキスト218,
            this.ラベル219,
            this.テキスト220,
            this.ラベル221,
            this.ラベル222,
            this.ラベル223,
            this.ラベル224,
            this.ラベル225,
            this.ラベル226,
            this.ラベル227,
            this.ラベル228,
            this.ラベル229,
            this.テキスト230,
            this.テキスト232,
            this.テキスト233,
            this.テキスト234,
            this.テキスト235,
            this.テキスト236,
            this.テキスト237,
            this.テキスト238,
            this.テキスト331,
            this.テキスト333,
            this.テキスト334,
            this.テキスト335,
            this.テキスト336,
            this.テキスト337,
            this.テキスト338,
            this.テキスト339,
            this.テキスト341,
            this.テキスト343,
            this.テキスト344,
            this.テキスト345,
            this.テキスト346,
            this.テキスト347,
            this.テキスト348,
            this.テキスト349,
            this.テキスト350,
            this.テキスト352,
            this.テキスト353,
            this.テキスト354,
            this.テキスト355,
            this.テキスト356,
            this.テキスト357,
            this.テキスト358,
            this.テキスト360,
            this.テキスト362,
            this.テキスト363,
            this.テキスト364,
            this.テキスト365,
            this.テキスト366,
            this.テキスト367,
            this.テキスト368,
            this.テキスト369,
            this.テキスト371,
            this.テキスト372,
            this.テキスト373,
            this.ボックス309,
            this.ボックス310,
            this.テキスト312,
            this.テキスト313,
            this.テキスト314,
            this.テキスト315,
            this.テキスト316,
            this.テキスト317,
            this.テキスト318,
            this.直線407,
            this.テキスト642,
            this.テキスト643,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.textBox59,
            this.label13,
            this.label16,
            this.label23,
            this.label15,
            this.line32,
            this.label29,
            this.shape7,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.shape8,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.shape10,
            this.textBox68,
            this.textBox95,
            this.line33,
            this.line34,
            this.line35});
            this.detail.Height = 11.263F;
            this.detail.Name = "detail";
            // 
            // shape9
            // 
            this.shape9.Height = 5.414F;
            this.shape9.Left = 0F;
            this.shape9.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape9.Name = "shape9";
            this.shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(10F, null, null, null, null);
            this.shape9.Top = 0F;
            this.shape9.Width = 7.895F;
            // 
            // ラベル203
            // 
            this.ラベル203.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Height = 0.3951389F;
            this.ラベル203.HyperLink = null;
            this.ラベル203.Left = 3.099F;
            this.ラベル203.Name = "ラベル203";
            this.ラベル203.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-weight: bold; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ラベル203.Tag = "";
            this.ラベル203.Text = "　";
            this.ラベル203.Top = 0.309F;
            this.ラベル203.Width = 1.811111F;
            // 
            // shape4
            // 
            this.shape4.Height = 5.83F;
            this.shape4.Left = 9.313229E-10F;
            this.shape4.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape4.Name = "shape4";
            this.shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(10F, null, null, null, null);
            this.shape4.Top = 5.433F;
            this.shape4.Width = 7.895F;
            // 
            // shape5
            // 
            this.shape5.Height = 5.83F;
            this.shape5.Left = 0F;
            this.shape5.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape5.Name = "shape5";
            this.shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(10F, null, null, null, null);
            this.shape5.Top = 5.433F;
            this.shape5.Width = 7.895F;
            // 
            // label7
            // 
            this.label7.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label7.Height = 0.3951389F;
            this.label7.HyperLink = null;
            this.label7.Left = 3.099F;
            this.label7.Name = "label7";
            this.label7.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-weight: bold; t" +
    "ext-align: center; ddo-char-set: 1";
            this.label7.Tag = "";
            this.label7.Text = "　";
            this.label7.Top = 5.903F;
            this.label7.Width = 1.811111F;
            // 
            // ラベル196
            // 
            this.ラベル196.Height = 0.2333333F;
            this.ラベル196.HyperLink = null;
            this.ラベル196.Left = 3.295F;
            this.ラベル196.Name = "ラベル196";
            this.ラベル196.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt" +
    "; font-weight: normal; text-align: center; ddo-char-set: 128";
            this.ラベル196.Tag = "";
            this.ラベル196.Text = "売上票(控え)";
            this.ラベル196.Top = 0.391F;
            this.ラベル196.Width = 1.415278F;
            // 
            // ラベル535
            // 
            this.ラベル535.Height = 0.2291667F;
            this.ラベル535.HyperLink = null;
            this.ラベル535.Left = 3.194F;
            this.ラベル535.Name = "ラベル535";
            this.ラベル535.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt" +
    "; font-weight: normal; text-align: center; ddo-char-set: 128";
            this.ラベル535.Tag = "";
            this.ラベル535.Text = "納品・請求書";
            this.ラベル535.Top = 5.986F;
            this.ラベル535.Width = 1.614567F;
            // 
            // shape1
            // 
            this.shape1.Height = 0.5416667F;
            this.shape1.Left = 0.108F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(30F, null, null, null, null);
            this.shape1.Top = 4.673F;
            this.shape1.Width = 1.375F;
            // 
            // line2
            // 
            this.line2.Height = 0.5436673F;
            this.line2.Left = 0.462F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 4.673F;
            this.line2.Width = 0F;
            this.line2.X1 = 0.462F;
            this.line2.X2 = 0.462F;
            this.line2.Y1 = 4.673F;
            this.line2.Y2 = 5.216667F;
            // 
            // textBox84
            // 
            this.textBox84.Height = 0.4267716F;
            this.textBox84.Left = 0.17F;
            this.textBox84.Name = "textBox84";
            this.textBox84.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center; vertical-align: midd" +
    "le; ddo-font-vertical: true";
            this.textBox84.Text = "受領印";
            this.textBox84.Top = 4.736F;
            this.textBox84.Width = 0.2291667F;
            // 
            // line27
            // 
            this.line27.Height = 0F;
            this.line27.Left = 0F;
            this.line27.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 5.433071F;
            this.line27.Width = 7.874016F;
            this.line27.X1 = 0F;
            this.line27.X2 = 7.874016F;
            this.line27.Y1 = 5.433071F;
            this.line27.Y2 = 5.433071F;
            // 
            // textBox85
            // 
            this.textBox85.DataField = "ITEM99";
            this.textBox85.Height = 0.5417323F;
            this.textBox85.Left = 1.528F;
            this.textBox85.Name = "textBox85";
            this.textBox85.Padding = new GrapeCity.ActiveReports.PaddingEx(4, 0, 0, 0);
            this.textBox85.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox85.Tag = "";
            this.textBox85.Text = "ITEM99";
            this.textBox85.Top = 4.673F;
            this.textBox85.Visible = false;
            this.textBox85.Width = 1.91F;
            // 
            // label1
            // 
            this.label1.Height = 0.3354585F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.105F;
            this.label1.Name = "label1";
            this.label1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.label1.Tag = "";
            this.label1.Text = "　";
            this.label1.Top = 8.856022F;
            this.label1.Width = 7.654F;
            // 
            // label2
            // 
            this.label2.Height = 0.3354167F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.108F;
            this.label2.Name = "label2";
            this.label2.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label2.Tag = "";
            this.label2.Text = "　";
            this.label2.Top = 8.521F;
            this.label2.Width = 7.654F;
            // 
            // label3
            // 
            this.label3.Height = 0.3354167F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.103F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "　";
            this.label3.Top = 8.186001F;
            this.label3.Width = 7.654F;
            // 
            // label4
            // 
            this.label4.Height = 0.3354167F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.108F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label4.Tag = "";
            this.label4.Text = "　";
            this.label4.Top = 7.846F;
            this.label4.Width = 7.653736F;
            // 
            // label5
            // 
            this.label5.Height = 0.3354167F;
            this.label5.HyperLink = null;
            this.label5.Left = 0.103F;
            this.label5.Name = "label5";
            this.label5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.label5.Tag = "";
            this.label5.Text = "　";
            this.label5.Top = 7.497F;
            this.label5.Width = 7.654F;
            // 
            // label6
            // 
            this.label6.Height = 0.194F;
            this.label6.HyperLink = null;
            this.label6.Left = 3.887F;
            this.label6.Name = "label6";
            this.label6.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label6.Tag = "";
            this.label6.Text = "区分";
            this.label6.Top = 7.307001F;
            this.label6.Width = 0.3480006F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM25";
            this.textBox1.Height = 0.188F;
            this.textBox1.Left = 0.9046944F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM25";
            this.textBox1.Top = 7.919751F;
            this.textBox1.Width = 2F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM45";
            this.textBox2.Height = 0.1875F;
            this.textBox2.Left = 0.9046944F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM45";
            this.textBox2.Top = 8.595001F;
            this.textBox2.Width = 2.000305F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM27";
            this.textBox3.Height = 0.188F;
            this.textBox3.Left = 3.913F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM27";
            this.textBox3.Top = 7.919751F;
            this.textBox3.Width = 0.287F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM15";
            this.textBox4.Height = 0.1875F;
            this.textBox4.Left = 0.905F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM15";
            this.textBox4.Top = 7.571001F;
            this.textBox4.Width = 2.000305F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM35";
            this.textBox5.Height = 0.1875F;
            this.textBox5.Left = 0.9046944F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM35";
            this.textBox5.Top = 8.26F;
            this.textBox5.Width = 2.000305F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM55";
            this.textBox6.Height = 0.1875F;
            this.textBox6.Left = 0.905F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM55";
            this.textBox6.Top = 8.93F;
            this.textBox6.Width = 2F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM13";
            this.textBox7.Height = 0.197F;
            this.textBox7.Left = 7.253F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM13";
            this.textBox7.Top = 6.091001F;
            this.textBox7.Width = 0.3229165F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM02";
            this.textBox8.Height = 0.1979167F;
            this.textBox8.Left = 6.525F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox8.Tag = "";
            this.textBox8.Text = "ITEM02";
            this.textBox8.Top = 6.091001F;
            this.textBox8.Width = 0.728F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM01";
            this.textBox9.Height = 0.1875F;
            this.textBox9.Left = 6.391F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox9.Tag = "";
            this.textBox9.Text = "ITEM01";
            this.textBox9.Top = 5.899F;
            this.textBox9.Width = 1.34375F;
            // 
            // label8
            // 
            this.label8.Height = 0.1972222F;
            this.label8.HyperLink = null;
            this.label8.Left = 7.576F;
            this.label8.Name = "label8";
            this.label8.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.label8.Tag = "";
            this.label8.Text = "頁";
            this.label8.Top = 6.091001F;
            this.label8.Width = 0.1715278F;
            // 
            // label9
            // 
            this.label9.Height = 0.1979167F;
            this.label9.HyperLink = null;
            this.label9.Left = 5.691972F;
            this.label9.Name = "label9";
            this.label9.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.label9.Tag = "";
            this.label9.Text = "伝票番号：";
            this.label9.Top = 6.091001F;
            this.label9.Width = 0.8333333F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM03";
            this.textBox10.Height = 0.1875F;
            this.textBox10.Left = 0.108F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox10.Tag = "";
            this.textBox10.Text = "ITEM03";
            this.textBox10.Top = 5.837F;
            this.textBox10.Visible = false;
            this.textBox10.Width = 1.34375F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM04";
            this.textBox11.Height = 0.1875F;
            this.textBox11.Left = 0.108F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "color: Black; font-family: MS UI Gothic; font-size: 14.25pt; font-weight: normal;" +
    " text-align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox11.Tag = "";
            this.textBox11.Text = "ITEM04";
            this.textBox11.Top = 6.587F;
            this.textBox11.Width = 2.279F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.09722208F;
            this.line1.LineWeight = 0F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 6.798536F;
            this.line1.Width = 2.710417F;
            this.line1.X1 = 0.09722208F;
            this.line1.X2 = 2.807639F;
            this.line1.Y1 = 6.798536F;
            this.line1.Y2 = 6.798536F;
            // 
            // label10
            // 
            this.label10.Height = 0.187693F;
            this.label10.HyperLink = null;
            this.label10.Left = 2.387F;
            this.label10.Name = "label10";
            this.label10.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text" +
    "-align: center; vertical-align: middle; ddo-char-set: 128";
            this.label10.Tag = "";
            this.label10.Text = "御中";
            this.label10.Top = 6.587306F;
            this.label10.Width = 0.441F;
            // 
            // label11
            // 
            this.label11.Height = 0.15625F;
            this.label11.HyperLink = null;
            this.label11.Left = 0.108F;
            this.label11.Name = "label11";
            this.label11.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.label11.Tag = "";
            this.label11.Text = "TEL";
            this.label11.Top = 6.835F;
            this.label11.Visible = false;
            this.label11.Width = 0.2395833F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM05";
            this.textBox12.Height = 0.15625F;
            this.textBox12.Left = 0.348F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox12.Tag = "";
            this.textBox12.Text = "ITEM05";
            this.textBox12.Top = 6.835F;
            this.textBox12.Visible = false;
            this.textBox12.Width = 0.944F;
            // 
            // label12
            // 
            this.label12.Height = 0.15625F;
            this.label12.HyperLink = null;
            this.label12.Left = 1.292F;
            this.label12.Name = "label12";
            this.label12.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; vertical-align: middle; ddo-char-set: 128";
            this.label12.Tag = "";
            this.label12.Text = "FAX";
            this.label12.Top = 6.835F;
            this.label12.Visible = false;
            this.label12.Width = 0.2395833F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM06";
            this.textBox13.Height = 0.15625F;
            this.textBox13.Left = 1.528F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox13.Tag = "";
            this.textBox13.Text = "ITEM06";
            this.textBox13.Top = 6.836F;
            this.textBox13.Visible = false;
            this.textBox13.Width = 0.9791667F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM07";
            this.textBox14.Height = 0.1875F;
            this.textBox14.Left = 5.516F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox14.Tag = "";
            this.textBox14.Text = "ITEM07";
            this.textBox14.Top = 6.320205F;
            this.textBox14.Width = 2.21875F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM08";
            this.textBox15.Height = 0.1875F;
            this.textBox15.Left = 5.516F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox15.Tag = "";
            this.textBox15.Text = "ITEM08";
            this.textBox15.Top = 6.508001F;
            this.textBox15.Width = 2.21875F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM09";
            this.textBox16.Height = 0.15625F;
            this.textBox16.Left = 5.535F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox16.Tag = "";
            this.textBox16.Text = "ITEM09";
            this.textBox16.Top = 6.724751F;
            this.textBox16.Width = 2.21875F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM10";
            this.textBox17.Height = 0.15625F;
            this.textBox17.Left = 5.535F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox17.Tag = "";
            this.textBox17.Text = "ITEM10";
            this.textBox17.Top = 6.881001F;
            this.textBox17.Width = 2.21875F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM11";
            this.textBox18.Height = 0.1875F;
            this.textBox18.Left = 3.097222F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox18.Tag = "";
            this.textBox18.Text = "ITEM11";
            this.textBox18.Top = 7.042284F;
            this.textBox18.Width = 1.811111F;
            // 
            // label14
            // 
            this.label14.Height = 0.15625F;
            this.label14.HyperLink = null;
            this.label14.Left = 5.993333F;
            this.label14.Name = "label14";
            this.label14.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.label14.Tag = "";
            this.label14.Text = "担当：";
            this.label14.Top = 7.074F;
            this.label14.Width = 0.4791667F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM12";
            this.textBox19.Height = 0.15625F;
            this.textBox19.Left = 6.460001F;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox19.Tag = "";
            this.textBox19.Text = "ITEM12";
            this.textBox19.Top = 7.074F;
            this.textBox19.Width = 1.302083F;
            // 
            // label17
            // 
            this.label17.Height = 0.1979167F;
            this.label17.HyperLink = null;
            this.label17.Left = 0.1083779F;
            this.label17.Name = "label17";
            this.label17.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label17.Tag = "";
            this.label17.Text = "品名コード";
            this.label17.Top = 7.303056F;
            this.label17.Width = 0.7806222F;
            // 
            // label18
            // 
            this.label18.Height = 0.194F;
            this.label18.HyperLink = null;
            this.label18.Left = 0.879F;
            this.label18.Name = "label18";
            this.label18.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label18.Tag = "";
            this.label18.Text = "商　品　名";
            this.label18.Top = 7.303056F;
            this.label18.Width = 2.066F;
            // 
            // label19
            // 
            this.label19.Height = 0.194F;
            this.label19.HyperLink = null;
            this.label19.Left = 2.935F;
            this.label19.Name = "label19";
            this.label19.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label19.Tag = "";
            this.label19.Text = "規　格";
            this.label19.Top = 7.303001F;
            this.label19.Width = 0.963F;
            // 
            // label20
            // 
            this.label20.Height = 0.194F;
            this.label20.HyperLink = null;
            this.label20.Left = 4.225F;
            this.label20.Name = "label20";
            this.label20.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label20.Tag = "";
            this.label20.Text = "単位";
            this.label20.Top = 7.303001F;
            this.label20.Width = 0.415F;
            // 
            // label21
            // 
            this.label21.Height = 0.194F;
            this.label21.HyperLink = null;
            this.label21.Left = 4.640001F;
            this.label21.Name = "label21";
            this.label21.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label21.Tag = "";
            this.label21.Text = "入数";
            this.label21.Top = 7.303001F;
            this.label21.Width = 0.5789995F;
            // 
            // label25
            // 
            this.label25.Height = 0.194F;
            this.label25.HyperLink = null;
            this.label25.Left = 5.212F;
            this.label25.Name = "label25";
            this.label25.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label25.Tag = "";
            this.label25.Text = "ケース";
            this.label25.Top = 7.303001F;
            this.label25.Width = 0.4319997F;
            // 
            // label26
            // 
            this.label26.Height = 0.194F;
            this.label26.HyperLink = null;
            this.label26.Left = 5.644F;
            this.label26.Name = "label26";
            this.label26.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label26.Tag = "";
            this.label26.Text = "バラ";
            this.label26.Top = 7.303001F;
            this.label26.Width = 0.6329998F;
            // 
            // label33
            // 
            this.label33.Height = 0.194F;
            this.label33.HyperLink = null;
            this.label33.Left = 6.267F;
            this.label33.Name = "label33";
            this.label33.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label33.Tag = "";
            this.label33.Text = "単　価";
            this.label33.Top = 7.303056F;
            this.label33.Width = 0.6350002F;
            // 
            // label34
            // 
            this.label34.Height = 0.194F;
            this.label34.HyperLink = null;
            this.label34.Left = 6.892F;
            this.label34.Name = "label34";
            this.label34.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label34.Tag = "";
            this.label34.Text = "金　額";
            this.label34.Top = 7.303056F;
            this.label34.Width = 0.8641646F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM14";
            this.textBox20.Height = 0.1875F;
            this.textBox20.Left = 0.132F;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox20.Tag = "";
            this.textBox20.Text = "ITEM14";
            this.textBox20.Top = 7.570959F;
            this.textBox20.Width = 0.713F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM16";
            this.textBox21.Height = 0.188F;
            this.textBox21.Left = 2.967F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox21.Tag = "";
            this.textBox21.Text = "ITEM16";
            this.textBox21.Top = 7.571001F;
            this.textBox21.Width = 0.9F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM18";
            this.textBox22.Height = 0.188F;
            this.textBox22.Left = 4.265001F;
            this.textBox22.Name = "textBox22";
            this.textBox22.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox22.Tag = "";
            this.textBox22.Text = "ITEM18";
            this.textBox22.Top = 7.570959F;
            this.textBox22.Width = 0.344F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM19";
            this.textBox23.Height = 0.188F;
            this.textBox23.Left = 4.67F;
            this.textBox23.Name = "textBox23";
            this.textBox23.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox23.Tag = "";
            this.textBox23.Text = "ITEM19";
            this.textBox23.Top = 7.57F;
            this.textBox23.Width = 0.511F;
            // 
            // textBox24
            // 
            this.textBox24.DataField = "ITEM20";
            this.textBox24.Height = 0.1875F;
            this.textBox24.Left = 5.236669F;
            this.textBox24.Name = "textBox24";
            this.textBox24.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox24.Tag = "";
            this.textBox24.Text = "ITEM20";
            this.textBox24.Top = 7.570959F;
            this.textBox24.Width = 0.3663311F;
            // 
            // textBox25
            // 
            this.textBox25.DataField = "ITEM21";
            this.textBox25.Height = 0.1875F;
            this.textBox25.Left = 5.67F;
            this.textBox25.Name = "textBox25";
            this.textBox25.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox25.Tag = "";
            this.textBox25.Text = "ITEM21";
            this.textBox25.Top = 7.570959F;
            this.textBox25.Width = 0.5720001F;
            // 
            // textBox26
            // 
            this.textBox26.DataField = "ITEM22";
            this.textBox26.Height = 0.1875F;
            this.textBox26.Left = 6.297F;
            this.textBox26.Name = "textBox26";
            this.textBox26.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox26.Tag = "";
            this.textBox26.Text = "ITEM22";
            this.textBox26.Top = 7.571001F;
            this.textBox26.Width = 0.5525824F;
            // 
            // textBox27
            // 
            this.textBox27.DataField = "ITEM23";
            this.textBox27.Height = 0.1875F;
            this.textBox27.Left = 6.923F;
            this.textBox27.Name = "textBox27";
            this.textBox27.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox27.Tag = "";
            this.textBox27.Text = "ITEM23";
            this.textBox27.Top = 7.571001F;
            this.textBox27.Width = 0.8060007F;
            // 
            // textBox28
            // 
            this.textBox28.DataField = "ITEM24";
            this.textBox28.Height = 0.188F;
            this.textBox28.Left = 0.132F;
            this.textBox28.Name = "textBox28";
            this.textBox28.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox28.Tag = "";
            this.textBox28.Text = "ITEM24";
            this.textBox28.Top = 7.919709F;
            this.textBox28.Width = 0.713F;
            // 
            // textBox29
            // 
            this.textBox29.DataField = "ITEM26";
            this.textBox29.Height = 0.188F;
            this.textBox29.Left = 2.967F;
            this.textBox29.Name = "textBox29";
            this.textBox29.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox29.Tag = "";
            this.textBox29.Text = "ITEM26";
            this.textBox29.Top = 7.919751F;
            this.textBox29.Width = 0.9F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM28";
            this.textBox30.Height = 0.188F;
            this.textBox30.Left = 4.265001F;
            this.textBox30.Name = "textBox30";
            this.textBox30.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox30.Tag = "";
            this.textBox30.Text = "ITEM28";
            this.textBox30.Top = 7.919709F;
            this.textBox30.Width = 0.344F;
            // 
            // textBox31
            // 
            this.textBox31.DataField = "ITEM29";
            this.textBox31.Height = 0.188F;
            this.textBox31.Left = 4.67F;
            this.textBox31.Name = "textBox31";
            this.textBox31.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox31.Tag = "";
            this.textBox31.Text = "ITEM29";
            this.textBox31.Top = 7.920001F;
            this.textBox31.Width = 0.511F;
            // 
            // textBox32
            // 
            this.textBox32.DataField = "ITEM30";
            this.textBox32.Height = 0.188F;
            this.textBox32.Left = 5.237F;
            this.textBox32.Name = "textBox32";
            this.textBox32.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox32.Tag = "";
            this.textBox32.Text = "ITEM30";
            this.textBox32.Top = 7.920001F;
            this.textBox32.Width = 0.366F;
            // 
            // textBox33
            // 
            this.textBox33.DataField = "ITEM31";
            this.textBox33.Height = 0.188F;
            this.textBox33.Left = 5.67F;
            this.textBox33.Name = "textBox33";
            this.textBox33.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox33.Tag = "";
            this.textBox33.Text = "ITEM31";
            this.textBox33.Top = 7.919709F;
            this.textBox33.Width = 0.5720001F;
            // 
            // textBox34
            // 
            this.textBox34.DataField = "ITEM32";
            this.textBox34.Height = 0.188F;
            this.textBox34.Left = 6.297F;
            this.textBox34.Name = "textBox34";
            this.textBox34.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox34.Tag = "";
            this.textBox34.Text = "ITEM32";
            this.textBox34.Top = 7.919751F;
            this.textBox34.Width = 0.548F;
            // 
            // textBox35
            // 
            this.textBox35.DataField = "ITEM33";
            this.textBox35.Height = 0.188F;
            this.textBox35.Left = 6.923F;
            this.textBox35.Name = "textBox35";
            this.textBox35.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox35.Tag = "";
            this.textBox35.Text = "ITEM33";
            this.textBox35.Top = 7.920001F;
            this.textBox35.Width = 0.806F;
            // 
            // textBox36
            // 
            this.textBox36.DataField = "ITEM34";
            this.textBox36.Height = 0.1875F;
            this.textBox36.Left = 0.132F;
            this.textBox36.Name = "textBox36";
            this.textBox36.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox36.Tag = "";
            this.textBox36.Text = "ITEM34";
            this.textBox36.Top = 8.259958F;
            this.textBox36.Width = 0.713F;
            // 
            // textBox37
            // 
            this.textBox37.DataField = "ITEM36";
            this.textBox37.Height = 0.188F;
            this.textBox37.Left = 2.972001F;
            this.textBox37.Name = "textBox37";
            this.textBox37.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox37.Tag = "";
            this.textBox37.Text = "ITEM36";
            this.textBox37.Top = 8.26F;
            this.textBox37.Width = 0.9F;
            // 
            // textBox38
            // 
            this.textBox38.DataField = "ITEM38";
            this.textBox38.Height = 0.188F;
            this.textBox38.Left = 4.265001F;
            this.textBox38.Name = "textBox38";
            this.textBox38.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox38.Tag = "";
            this.textBox38.Text = "ITEM38";
            this.textBox38.Top = 8.259958F;
            this.textBox38.Width = 0.344F;
            // 
            // textBox39
            // 
            this.textBox39.DataField = "ITEM39";
            this.textBox39.Height = 0.188F;
            this.textBox39.Left = 4.67F;
            this.textBox39.Name = "textBox39";
            this.textBox39.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox39.Tag = "";
            this.textBox39.Text = "ITEM39";
            this.textBox39.Top = 8.259958F;
            this.textBox39.Width = 0.511F;
            // 
            // textBox40
            // 
            this.textBox40.DataField = "ITEM40";
            this.textBox40.Height = 0.188F;
            this.textBox40.Left = 5.236669F;
            this.textBox40.Name = "textBox40";
            this.textBox40.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox40.Tag = "";
            this.textBox40.Text = "ITEM40";
            this.textBox40.Top = 8.259958F;
            this.textBox40.Width = 0.366F;
            // 
            // textBox41
            // 
            this.textBox41.DataField = "ITEM41";
            this.textBox41.Height = 0.1875F;
            this.textBox41.Left = 5.67F;
            this.textBox41.Name = "textBox41";
            this.textBox41.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox41.Tag = "";
            this.textBox41.Text = "ITEM41";
            this.textBox41.Top = 8.259958F;
            this.textBox41.Width = 0.5767716F;
            // 
            // textBox42
            // 
            this.textBox42.DataField = "ITEM42";
            this.textBox42.Height = 0.1875F;
            this.textBox42.Left = 6.297F;
            this.textBox42.Name = "textBox42";
            this.textBox42.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox42.Tag = "";
            this.textBox42.Text = "ITEM42";
            this.textBox42.Top = 8.26F;
            this.textBox42.Width = 0.5620003F;
            // 
            // textBox43
            // 
            this.textBox43.DataField = "ITEM43";
            this.textBox43.Height = 0.188F;
            this.textBox43.Left = 6.923F;
            this.textBox43.Name = "textBox43";
            this.textBox43.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox43.Tag = "";
            this.textBox43.Text = "ITEM43";
            this.textBox43.Top = 8.26F;
            this.textBox43.Width = 0.806F;
            // 
            // textBox44
            // 
            this.textBox44.DataField = "ITEM44";
            this.textBox44.Height = 0.1875F;
            this.textBox44.Left = 0.132F;
            this.textBox44.Name = "textBox44";
            this.textBox44.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox44.Tag = "";
            this.textBox44.Text = "ITEM44";
            this.textBox44.Top = 8.594959F;
            this.textBox44.Width = 0.713F;
            // 
            // textBox45
            // 
            this.textBox45.DataField = "ITEM46";
            this.textBox45.Height = 0.188F;
            this.textBox45.Left = 2.967F;
            this.textBox45.Name = "textBox45";
            this.textBox45.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox45.Tag = "";
            this.textBox45.Text = "ITEM46";
            this.textBox45.Top = 8.595001F;
            this.textBox45.Width = 0.9F;
            // 
            // textBox46
            // 
            this.textBox46.DataField = "ITEM48";
            this.textBox46.Height = 0.188F;
            this.textBox46.Left = 4.265001F;
            this.textBox46.Name = "textBox46";
            this.textBox46.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox46.Tag = "";
            this.textBox46.Text = "ITEM48";
            this.textBox46.Top = 8.594959F;
            this.textBox46.Width = 0.344F;
            // 
            // textBox47
            // 
            this.textBox47.DataField = "ITEM49";
            this.textBox47.Height = 0.188F;
            this.textBox47.Left = 4.67F;
            this.textBox47.Name = "textBox47";
            this.textBox47.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox47.Tag = "";
            this.textBox47.Text = "ITEM49";
            this.textBox47.Top = 8.594959F;
            this.textBox47.Width = 0.511F;
            // 
            // textBox48
            // 
            this.textBox48.DataField = "ITEM50";
            this.textBox48.Height = 0.188F;
            this.textBox48.Left = 5.236669F;
            this.textBox48.Name = "textBox48";
            this.textBox48.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox48.Tag = "";
            this.textBox48.Text = "ITEM50";
            this.textBox48.Top = 8.594709F;
            this.textBox48.Width = 0.366F;
            // 
            // textBox49
            // 
            this.textBox49.DataField = "ITEM51";
            this.textBox49.Height = 0.1875F;
            this.textBox49.Left = 5.67F;
            this.textBox49.Name = "textBox49";
            this.textBox49.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox49.Tag = "";
            this.textBox49.Text = "ITEM51";
            this.textBox49.Top = 8.594959F;
            this.textBox49.Width = 0.5719998F;
            // 
            // textBox50
            // 
            this.textBox50.DataField = "ITEM52";
            this.textBox50.Height = 0.1875F;
            this.textBox50.Left = 6.297F;
            this.textBox50.Name = "textBox50";
            this.textBox50.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox50.Tag = "";
            this.textBox50.Text = "ITEM52";
            this.textBox50.Top = 8.595001F;
            this.textBox50.Width = 0.5620003F;
            // 
            // textBox51
            // 
            this.textBox51.DataField = "ITEM53";
            this.textBox51.Height = 0.188F;
            this.textBox51.Left = 6.923F;
            this.textBox51.Name = "textBox51";
            this.textBox51.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox51.Tag = "";
            this.textBox51.Text = "ITEM53";
            this.textBox51.Top = 8.595001F;
            this.textBox51.Width = 0.806F;
            // 
            // textBox52
            // 
            this.textBox52.DataField = "ITEM54";
            this.textBox52.Height = 0.1875F;
            this.textBox52.Left = 0.132F;
            this.textBox52.Name = "textBox52";
            this.textBox52.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox52.Tag = "";
            this.textBox52.Text = "ITEM54";
            this.textBox52.Top = 8.93F;
            this.textBox52.Width = 0.713F;
            // 
            // textBox53
            // 
            this.textBox53.DataField = "ITEM56";
            this.textBox53.Height = 0.188F;
            this.textBox53.Left = 2.967F;
            this.textBox53.Name = "textBox53";
            this.textBox53.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox53.Tag = "";
            this.textBox53.Text = "ITEM56";
            this.textBox53.Top = 8.92975F;
            this.textBox53.Width = 0.9F;
            // 
            // textBox54
            // 
            this.textBox54.DataField = "ITEM58";
            this.textBox54.Height = 0.188F;
            this.textBox54.Left = 4.265001F;
            this.textBox54.Name = "textBox54";
            this.textBox54.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox54.Tag = "";
            this.textBox54.Text = "ITEM58";
            this.textBox54.Top = 8.92975F;
            this.textBox54.Width = 0.344F;
            // 
            // textBox55
            // 
            this.textBox55.DataField = "ITEM59";
            this.textBox55.Height = 0.188F;
            this.textBox55.Left = 4.67F;
            this.textBox55.Name = "textBox55";
            this.textBox55.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox55.Tag = "";
            this.textBox55.Text = "ITEM59";
            this.textBox55.Top = 8.92975F;
            this.textBox55.Width = 0.511F;
            // 
            // textBox56
            // 
            this.textBox56.DataField = "ITEM60";
            this.textBox56.Height = 0.188F;
            this.textBox56.Left = 5.236669F;
            this.textBox56.Name = "textBox56";
            this.textBox56.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox56.Tag = "";
            this.textBox56.Text = "ITEM60";
            this.textBox56.Top = 8.92975F;
            this.textBox56.Width = 0.366F;
            // 
            // textBox57
            // 
            this.textBox57.DataField = "ITEM61";
            this.textBox57.Height = 0.1875F;
            this.textBox57.Left = 5.67F;
            this.textBox57.Name = "textBox57";
            this.textBox57.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox57.Tag = "";
            this.textBox57.Text = "ITEM61";
            this.textBox57.Top = 8.93F;
            this.textBox57.Width = 0.5767716F;
            // 
            // textBox58
            // 
            this.textBox58.DataField = "ITEM62";
            this.textBox58.Height = 0.1875F;
            this.textBox58.Left = 6.297F;
            this.textBox58.Name = "textBox58";
            this.textBox58.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox58.Tag = "";
            this.textBox58.Text = "ITEM62";
            this.textBox58.Top = 8.93F;
            this.textBox58.Width = 0.5620003F;
            // 
            // textBox69
            // 
            this.textBox69.DataField = "ITEM63";
            this.textBox69.Height = 0.188F;
            this.textBox69.Left = 6.923F;
            this.textBox69.Name = "textBox69";
            this.textBox69.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.textBox69.Tag = "";
            this.textBox69.Text = "ITEM63";
            this.textBox69.Top = 8.92975F;
            this.textBox69.Width = 0.806F;
            // 
            // textBox70
            // 
            this.textBox70.DataField = "ITEM17";
            this.textBox70.Height = 0.188F;
            this.textBox70.Left = 3.913F;
            this.textBox70.Name = "textBox70";
            this.textBox70.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox70.Tag = "";
            this.textBox70.Text = "ITEM17";
            this.textBox70.Top = 7.571001F;
            this.textBox70.Width = 0.287F;
            // 
            // textBox71
            // 
            this.textBox71.DataField = "ITEM37";
            this.textBox71.Height = 0.188F;
            this.textBox71.Left = 3.913F;
            this.textBox71.Name = "textBox71";
            this.textBox71.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox71.Tag = "";
            this.textBox71.Text = "ITEM37";
            this.textBox71.Top = 8.26F;
            this.textBox71.Width = 0.287F;
            // 
            // textBox72
            // 
            this.textBox72.DataField = "ITEM47";
            this.textBox72.Height = 0.188F;
            this.textBox72.Left = 3.913F;
            this.textBox72.Name = "textBox72";
            this.textBox72.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox72.Tag = "";
            this.textBox72.Text = "ITEM47";
            this.textBox72.Top = 8.594751F;
            this.textBox72.Width = 0.287F;
            // 
            // textBox73
            // 
            this.textBox73.DataField = "ITEM57";
            this.textBox73.Height = 0.188F;
            this.textBox73.Left = 3.913F;
            this.textBox73.Name = "textBox73";
            this.textBox73.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox73.Tag = "";
            this.textBox73.Text = "ITEM57";
            this.textBox73.Top = 8.92975F;
            this.textBox73.Width = 0.287F;
            // 
            // shape6
            // 
            this.shape6.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape6.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape6.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape6.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape6.Height = 0.2706432F;
            this.shape6.Left = 3.675F;
            this.shape6.Name = "shape6";
            this.shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape6.Tag = "";
            this.shape6.Top = 10.267F;
            this.shape6.Width = 4.080749F;
            // 
            // shape2
            // 
            this.shape2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape2.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape2.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape2.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape2.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape2.Height = 0.2395833F;
            this.shape2.Left = 3.675F;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape2.Tag = "";
            this.shape2.Top = 9.485569F;
            this.shape2.Width = 4.08075F;
            // 
            // textBox74
            // 
            this.textBox74.DataField = "ITEM64";
            this.textBox74.Height = 0.155333F;
            this.textBox74.Left = 4.844F;
            this.textBox74.Name = "textBox74";
            this.textBox74.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox74.Tag = "";
            this.textBox74.Text = "ITEM64";
            this.textBox74.Top = 9.527695F;
            this.textBox74.Width = 0.9364729F;
            // 
            // textBox75
            // 
            this.textBox75.DataField = "ITEM65";
            this.textBox75.Height = 0.155F;
            this.textBox75.Left = 5.866F;
            this.textBox75.Name = "textBox75";
            this.textBox75.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox75.Tag = "";
            this.textBox75.Text = "ITEM65";
            this.textBox75.Top = 9.528F;
            this.textBox75.Width = 0.878F;
            // 
            // textBox76
            // 
            this.textBox76.DataField = "ITEM66";
            this.textBox76.Height = 0.155F;
            this.textBox76.Left = 6.827001F;
            this.textBox76.Name = "textBox76";
            this.textBox76.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox76.Tag = "";
            this.textBox76.Text = "ITEM66";
            this.textBox76.Top = 9.528F;
            this.textBox76.Width = 0.891F;
            // 
            // textBox77
            // 
            this.textBox77.DataField = "ITEM69";
            this.textBox77.Height = 0.1875F;
            this.textBox77.Left = 3.72212F;
            this.textBox77.Name = "textBox77";
            this.textBox77.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox77.Tag = "";
            this.textBox77.Text = "ITEM69";
            this.textBox77.Top = 10.30957F;
            this.textBox77.Width = 1.056001F;
            // 
            // textBox78
            // 
            this.textBox78.DataField = "ITEM76";
            this.textBox78.Height = 0.188F;
            this.textBox78.Left = 4.844F;
            this.textBox78.Name = "textBox78";
            this.textBox78.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox78.Tag = "";
            this.textBox78.Text = "ITEM76";
            this.textBox78.Top = 10.308F;
            this.textBox78.Width = 0.936F;
            // 
            // textBox79
            // 
            this.textBox79.DataField = "ITEM77";
            this.textBox79.Height = 0.188F;
            this.textBox79.Left = 5.866F;
            this.textBox79.Name = "textBox79";
            this.textBox79.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox79.Tag = "";
            this.textBox79.Text = "ITEM77";
            this.textBox79.Top = 10.309F;
            this.textBox79.Width = 0.878F;
            // 
            // textBox80
            // 
            this.textBox80.DataField = "ITEM78";
            this.textBox80.Height = 0.188F;
            this.textBox80.Left = 6.827033F;
            this.textBox80.Name = "textBox80";
            this.textBox80.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox80.Tag = "";
            this.textBox80.Text = "ITEM78";
            this.textBox80.Top = 10.30785F;
            this.textBox80.Width = 0.891F;
            // 
            // line15
            // 
            this.line15.Height = 0F;
            this.line15.Left = 0.105F;
            this.line15.LineWeight = 0F;
            this.line15.Name = "line15";
            this.line15.Tag = "";
            this.line15.Top = 7.500695F;
            this.line15.Width = 7.653F;
            this.line15.X1 = 0.105F;
            this.line15.X2 = 7.758F;
            this.line15.Y1 = 7.500695F;
            this.line15.Y2 = 7.500695F;
            // 
            // textBox81
            // 
            this.textBox81.DataField = "ITEM94";
            this.textBox81.Height = 0.1875F;
            this.textBox81.Left = 0.108F;
            this.textBox81.Name = "textBox81";
            this.textBox81.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox81.Tag = "";
            this.textBox81.Text = "ITEM94";
            this.textBox81.Top = 6.024F;
            this.textBox81.Width = 2.375F;
            // 
            // textBox82
            // 
            this.textBox82.DataField = "ITEM95";
            this.textBox82.Height = 0.1875F;
            this.textBox82.Left = 0.108F;
            this.textBox82.Name = "textBox82";
            this.textBox82.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox82.Tag = "";
            this.textBox82.Text = "ITEM95";
            this.textBox82.Top = 6.21F;
            this.textBox82.Width = 2.375F;
            // 
            // line16
            // 
            this.line16.Height = 1.889F;
            this.line16.Left = 0.879F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 7.303001F;
            this.line16.Width = 0F;
            this.line16.X1 = 0.879F;
            this.line16.X2 = 0.879F;
            this.line16.Y1 = 7.303001F;
            this.line16.Y2 = 9.192001F;
            // 
            // line17
            // 
            this.line17.Height = 1.885F;
            this.line17.Left = 2.935F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 7.307001F;
            this.line17.Width = 0F;
            this.line17.X1 = 2.935F;
            this.line17.X2 = 2.935F;
            this.line17.Y1 = 7.307001F;
            this.line17.Y2 = 9.192001F;
            // 
            // line18
            // 
            this.line18.Height = 1.889F;
            this.line18.Left = 3.898F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 7.303001F;
            this.line18.Width = 0F;
            this.line18.X1 = 3.898F;
            this.line18.X2 = 3.898F;
            this.line18.Y1 = 7.303001F;
            this.line18.Y2 = 9.192001F;
            // 
            // line19
            // 
            this.line19.Height = 1.889F;
            this.line19.Left = 4.64F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 7.303001F;
            this.line19.Width = 9.536743E-07F;
            this.line19.X1 = 4.640001F;
            this.line19.X2 = 4.64F;
            this.line19.Y1 = 7.303001F;
            this.line19.Y2 = 9.192001F;
            // 
            // line20
            // 
            this.line20.Height = 1.889F;
            this.line20.Left = 5.207001F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 7.303001F;
            this.line20.Width = 6.198883E-05F;
            this.line20.X1 = 5.207001F;
            this.line20.X2 = 5.207063F;
            this.line20.Y1 = 7.303001F;
            this.line20.Y2 = 9.192001F;
            // 
            // line21
            // 
            this.line21.Height = 1.889F;
            this.line21.Left = 5.633507F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 7.303001F;
            this.line21.Width = 0.0004930496F;
            this.line21.X1 = 5.634F;
            this.line21.X2 = 5.633507F;
            this.line21.Y1 = 7.303001F;
            this.line21.Y2 = 9.192001F;
            // 
            // line22
            // 
            this.line22.Height = 1.889F;
            this.line22.Left = 6.267F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 7.303001F;
            this.line22.Width = 0.0004258156F;
            this.line22.X1 = 6.267F;
            this.line22.X2 = 6.267426F;
            this.line22.Y1 = 7.303001F;
            this.line22.Y2 = 9.192001F;
            // 
            // line23
            // 
            this.line23.Height = 1.889F;
            this.line23.Left = 6.892F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 7.303001F;
            this.line23.Width = 0.0006127357F;
            this.line23.X1 = 6.892F;
            this.line23.X2 = 6.892613F;
            this.line23.Y1 = 7.303001F;
            this.line23.Y2 = 9.192001F;
            // 
            // line24
            // 
            this.line24.Height = 1.888001F;
            this.line24.Left = 7.757F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 7.304F;
            this.line24.Width = 0.002840042F;
            this.line24.X1 = 7.75984F;
            this.line24.X2 = 7.757F;
            this.line24.Y1 = 7.304F;
            this.line24.Y2 = 9.192001F;
            // 
            // line25
            // 
            this.line25.Height = 1.888001F;
            this.line25.Left = 0.105F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 7.304F;
            this.line25.Width = 0F;
            this.line25.X1 = 0.105F;
            this.line25.X2 = 0.105F;
            this.line25.Y1 = 7.304F;
            this.line25.Y2 = 9.192001F;
            // 
            // line26
            // 
            this.line26.Height = 0F;
            this.line26.Left = 0.1048345F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 7.303001F;
            this.line26.Width = 7.652165F;
            this.line26.X1 = 0.1048345F;
            this.line26.X2 = 7.757F;
            this.line26.Y1 = 7.303001F;
            this.line26.Y2 = 7.303001F;
            // 
            // line31
            // 
            this.line31.Height = 0F;
            this.line31.Left = 0.105F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 9.192001F;
            this.line31.Width = 7.653F;
            this.line31.X1 = 0.105F;
            this.line31.X2 = 7.758F;
            this.line31.Y1 = 9.192001F;
            this.line31.Y2 = 9.192001F;
            // 
            // textBox83
            // 
            this.textBox83.DataField = "ITEM96";
            this.textBox83.Height = 0.1875F;
            this.textBox83.Left = 0.108F;
            this.textBox83.Name = "textBox83";
            this.textBox83.Style = "color: Black; font-family: MS UI Gothic; font-size: 14.25pt; font-weight: normal;" +
    " text-align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox83.Tag = "";
            this.textBox83.Text = "ITEM14";
            this.textBox83.Top = 6.392F;
            this.textBox83.Width = 1.184252F;
            // 
            // label35
            // 
            this.label35.Height = 0.16125F;
            this.label35.HyperLink = null;
            this.label35.Left = 5.964F;
            this.label35.Name = "label35";
            this.label35.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.label35.Tag = "";
            this.label35.Text = "FAX\r\n";
            this.label35.Top = 6.881001F;
            this.label35.Width = 0.8216542F;
            // 
            // label36
            // 
            this.label36.Height = 0.1412151F;
            this.label36.HyperLink = null;
            this.label36.Left = 6.481F;
            this.label36.Name = "label36";
            this.label36.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.label36.Tag = "";
            this.label36.Text = "(098)861-2707";
            this.label36.Top = 6.730001F;
            this.label36.Width = 1.270078F;
            // 
            // label37
            // 
            this.label37.Height = 0.14125F;
            this.label37.HyperLink = null;
            this.label37.Left = 5.964F;
            this.label37.Name = "label37";
            this.label37.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 1";
            this.label37.Tag = "";
            this.label37.Text = "事務所";
            this.label37.Top = 6.730001F;
            this.label37.Width = 0.8216542F;
            // 
            // label38
            // 
            this.label38.Height = 0.15625F;
            this.label38.HyperLink = null;
            this.label38.Left = 6.474F;
            this.label38.Name = "label38";
            this.label38.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.label38.Tag = "";
            this.label38.Text = "(098)861-0819";
            this.label38.Top = 6.881001F;
            this.label38.Width = 1.270078F;
            // 
            // label39
            // 
            this.label39.Height = 0.1972222F;
            this.label39.HyperLink = null;
            this.label39.Left = 0.108F;
            this.label39.Name = "label39";
            this.label39.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 128";
            this.label39.Tag = "";
            this.label39.Text = "振込先：";
            this.label39.Top = 10.154F;
            this.label39.Width = 0.9122048F;
            // 
            // label40
            // 
            this.label40.Height = 0.1972222F;
            this.label40.HyperLink = null;
            this.label40.Left = 0.108F;
            this.label40.Name = "label40";
            this.label40.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 128";
            this.label40.Tag = "";
            this.label40.Text = "沖縄銀行曙支店(普)No.0910018";
            this.label40.Top = 10.382F;
            this.label40.Width = 3.329529F;
            // 
            // label41
            // 
            this.label41.Height = 0.1972222F;
            this.label41.HyperLink = null;
            this.label41.Left = 0.108F;
            this.label41.Name = "label41";
            this.label41.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 128";
            this.label41.Tag = "";
            this.label41.Text = "沖縄県信漁連本所(普)No.0000639";
            this.label41.Top = 10.612F;
            this.label41.Width = 3.329529F;
            // 
            // line28
            // 
            this.line28.Height = 1.889F;
            this.line28.Left = 4.225F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 7.303001F;
            this.line28.Width = 0F;
            this.line28.X1 = 4.225F;
            this.line28.X2 = 4.225F;
            this.line28.Y1 = 7.303001F;
            this.line28.Y2 = 9.192001F;
            // 
            // label42
            // 
            this.label42.Height = 0.1972222F;
            this.label42.HyperLink = null;
            this.label42.Left = 0.108F;
            this.label42.Name = "label42";
            this.label42.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 1";
            this.label42.Tag = "";
            this.label42.Text = "「*」は軽減税率であることを示します。";
            this.label42.Top = 9.288F;
            this.label42.Width = 3.33F;
            // 
            // shape3
            // 
            this.shape3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape3.Height = 0.2706432F;
            this.shape3.Left = 3.675F;
            this.shape3.Name = "shape3";
            this.shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape3.Tag = "";
            this.shape3.Top = 9.996F;
            this.shape3.Width = 4.081001F;
            // 
            // textBox86
            // 
            this.textBox86.DataField = "ITEM68";
            this.textBox86.Height = 0.1875F;
            this.textBox86.Left = 3.722F;
            this.textBox86.Name = "textBox86";
            this.textBox86.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox86.Tag = "";
            this.textBox86.Text = "ITEM68";
            this.textBox86.Top = 10.037F;
            this.textBox86.Width = 1.056001F;
            // 
            // textBox87
            // 
            this.textBox87.DataField = "ITEM73";
            this.textBox87.Height = 0.188F;
            this.textBox87.Left = 4.844F;
            this.textBox87.Name = "textBox87";
            this.textBox87.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox87.Tag = "";
            this.textBox87.Text = "ITEM73";
            this.textBox87.Top = 10.036F;
            this.textBox87.Width = 0.936F;
            // 
            // textBox88
            // 
            this.textBox88.DataField = "ITEM74";
            this.textBox88.Height = 0.188F;
            this.textBox88.Left = 5.866F;
            this.textBox88.Name = "textBox88";
            this.textBox88.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox88.Tag = "";
            this.textBox88.Text = "ITEM74";
            this.textBox88.Top = 10.037F;
            this.textBox88.Width = 0.878F;
            // 
            // textBox89
            // 
            this.textBox89.DataField = "ITEM75";
            this.textBox89.Height = 0.188F;
            this.textBox89.Left = 6.824883F;
            this.textBox89.Name = "textBox89";
            this.textBox89.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox89.Tag = "";
            this.textBox89.Text = "ITEM75";
            this.textBox89.Top = 10.03686F;
            this.textBox89.Width = 0.891F;
            // 
            // shape11
            // 
            this.shape11.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Height = 0.2706432F;
            this.shape11.Left = 3.675F;
            this.shape11.Name = "shape11";
            this.shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape11.Tag = "";
            this.shape11.Top = 9.725571F;
            this.shape11.Width = 4.081002F;
            // 
            // textBox90
            // 
            this.textBox90.DataField = "ITEM67";
            this.textBox90.Height = 0.1875F;
            this.textBox90.Left = 3.72212F;
            this.textBox90.Name = "textBox90";
            this.textBox90.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox90.Tag = "";
            this.textBox90.Text = "ITEM67";
            this.textBox90.Top = 9.766577F;
            this.textBox90.Width = 1.056001F;
            // 
            // textBox91
            // 
            this.textBox91.DataField = "ITEM70";
            this.textBox91.Height = 0.188F;
            this.textBox91.Left = 4.844F;
            this.textBox91.Name = "textBox91";
            this.textBox91.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox91.Tag = "";
            this.textBox91.Text = "ITEM70";
            this.textBox91.Top = 9.766001F;
            this.textBox91.Width = 0.936F;
            // 
            // textBox92
            // 
            this.textBox92.DataField = "ITEM71";
            this.textBox92.Height = 0.188F;
            this.textBox92.Left = 5.866F;
            this.textBox92.Name = "textBox92";
            this.textBox92.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox92.Tag = "";
            this.textBox92.Text = "ITEM71";
            this.textBox92.Top = 9.767F;
            this.textBox92.Width = 0.878F;
            // 
            // textBox93
            // 
            this.textBox93.DataField = "ITEM72";
            this.textBox93.Height = 0.188F;
            this.textBox93.Left = 6.825001F;
            this.textBox93.Name = "textBox93";
            this.textBox93.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox93.Tag = "";
            this.textBox93.Text = "ITEM72";
            this.textBox93.Top = 9.766432F;
            this.textBox93.Width = 0.891F;
            // 
            // shape12
            // 
            this.shape12.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape12.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape12.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape12.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape12.Height = 0.2706433F;
            this.shape12.Left = 3.674999F;
            this.shape12.Name = "shape12";
            this.shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape12.Tag = "";
            this.shape12.Top = 10.538F;
            this.shape12.Width = 4.080749F;
            // 
            // textBox94
            // 
            this.textBox94.DataField = "ITEM79";
            this.textBox94.Height = 0.1875F;
            this.textBox94.Left = 3.722F;
            this.textBox94.Name = "textBox94";
            this.textBox94.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox94.Tag = "";
            this.textBox94.Text = "ITEM79";
            this.textBox94.Top = 10.58F;
            this.textBox94.Width = 3.022001F;
            // 
            // textBox96
            // 
            this.textBox96.DataField = "ITEM80";
            this.textBox96.Height = 0.188F;
            this.textBox96.Left = 6.827F;
            this.textBox96.Name = "textBox96";
            this.textBox96.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox96.Tag = "";
            this.textBox96.Text = "ITEM80";
            this.textBox96.Top = 10.579F;
            this.textBox96.Width = 0.891F;
            // 
            // line29
            // 
            this.line29.Height = 1.323999F;
            this.line29.Left = 6.786001F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 9.485001F;
            this.line29.Width = 0F;
            this.line29.X1 = 6.786001F;
            this.line29.X2 = 6.786001F;
            this.line29.Y1 = 9.485001F;
            this.line29.Y2 = 10.809F;
            // 
            // line30
            // 
            this.line30.Height = 1.052999F;
            this.line30.Left = 4.814F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 9.485001F;
            this.line30.Width = 0F;
            this.line30.X1 = 4.814F;
            this.line30.X2 = 4.814F;
            this.line30.Y1 = 9.485001F;
            this.line30.Y2 = 10.538F;
            // 
            // line36
            // 
            this.line36.Height = 1.052999F;
            this.line36.Left = 5.822001F;
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Top = 9.485001F;
            this.line36.Width = 0F;
            this.line36.X1 = 5.822001F;
            this.line36.X2 = 5.822001F;
            this.line36.Y1 = 9.485001F;
            this.line36.Y2 = 10.538F;
            // 
            // label32
            // 
            this.label32.Height = 0.3354585F;
            this.label32.HyperLink = null;
            this.label32.Left = 0.105F;
            this.label32.Name = "label32";
            this.label32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.label32.Tag = "";
            this.label32.Text = "　";
            this.label32.Top = 3.262021F;
            this.label32.Width = 7.654F;
            // 
            // ラベル340
            // 
            this.ラベル340.Height = 0.3354167F;
            this.ラベル340.HyperLink = null;
            this.ラベル340.Left = 0.108F;
            this.ラベル340.Name = "ラベル340";
            this.ラベル340.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.ラベル340.Tag = "";
            this.ラベル340.Text = "　";
            this.ラベル340.Top = 2.927F;
            this.ラベル340.Width = 7.654F;
            // 
            // label31
            // 
            this.label31.Height = 0.3354167F;
            this.label31.HyperLink = null;
            this.label31.Left = 0.103F;
            this.label31.Name = "label31";
            this.label31.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.label31.Tag = "";
            this.label31.Text = "　";
            this.label31.Top = 2.592F;
            this.label31.Width = 7.654F;
            // 
            // BackClrCg
            // 
            this.BackClrCg.Height = 0.3354167F;
            this.BackClrCg.HyperLink = null;
            this.BackClrCg.Left = 0.108F;
            this.BackClrCg.Name = "BackClrCg";
            this.BackClrCg.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.BackClrCg.Tag = "";
            this.BackClrCg.Text = "　";
            this.BackClrCg.Top = 2.252F;
            this.BackClrCg.Width = 7.653736F;
            // 
            // label30
            // 
            this.label30.Height = 0.3354167F;
            this.label30.HyperLink = null;
            this.label30.Left = 0.103F;
            this.label30.Name = "label30";
            this.label30.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.label30.Tag = "";
            this.label30.Text = "　";
            this.label30.Top = 1.903F;
            this.label30.Width = 7.654F;
            // 
            // label28
            // 
            this.label28.Height = 0.194F;
            this.label28.HyperLink = null;
            this.label28.Left = 3.887F;
            this.label28.Name = "label28";
            this.label28.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label28.Tag = "";
            this.label28.Text = "区分";
            this.label28.Top = 1.713F;
            this.label28.Width = 0.3480006F;
            // 
            // テキスト332
            // 
            this.テキスト332.DataField = "ITEM25";
            this.テキスト332.Height = 0.188F;
            this.テキスト332.Left = 0.9046944F;
            this.テキスト332.Name = "テキスト332";
            this.テキスト332.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト332.Tag = "";
            this.テキスト332.Text = "ITEM25";
            this.テキスト332.Top = 2.325748F;
            this.テキスト332.Width = 2F;
            // 
            // テキスト351
            // 
            this.テキスト351.DataField = "ITEM45";
            this.テキスト351.Height = 0.1875F;
            this.テキスト351.Left = 0.9046944F;
            this.テキスト351.Name = "テキスト351";
            this.テキスト351.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト351.Tag = "";
            this.テキスト351.Text = "ITEM45";
            this.テキスト351.Top = 3.000998F;
            this.テキスト351.Width = 2.000305F;
            // 
            // テキスト370
            // 
            this.テキスト370.DataField = "ITEM27";
            this.テキスト370.Height = 0.188F;
            this.テキスト370.Left = 3.913F;
            this.テキスト370.Name = "テキスト370";
            this.テキスト370.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト370.Tag = "";
            this.テキスト370.Text = "ITEM27";
            this.テキスト370.Top = 2.325751F;
            this.テキスト370.Width = 0.287F;
            // 
            // テキスト231
            // 
            this.テキスト231.DataField = "ITEM15";
            this.テキスト231.Height = 0.1875F;
            this.テキスト231.Left = 0.905F;
            this.テキスト231.Name = "テキスト231";
            this.テキスト231.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト231.Tag = "";
            this.テキスト231.Text = "ITEM15";
            this.テキスト231.Top = 1.977F;
            this.テキスト231.Width = 2.000305F;
            // 
            // テキスト342
            // 
            this.テキスト342.DataField = "ITEM35";
            this.テキスト342.Height = 0.1875F;
            this.テキスト342.Left = 0.9046944F;
            this.テキスト342.Name = "テキスト342";
            this.テキスト342.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト342.Tag = "";
            this.テキスト342.Text = "ITEM35";
            this.テキスト342.Top = 2.665999F;
            this.テキスト342.Width = 2.000305F;
            // 
            // テキスト361
            // 
            this.テキスト361.DataField = "ITEM55";
            this.テキスト361.Height = 0.1875F;
            this.テキスト361.Left = 0.905F;
            this.テキスト361.Name = "テキスト361";
            this.テキスト361.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト361.Tag = "";
            this.テキスト361.Text = "ITEM55";
            this.テキスト361.Top = 3.336F;
            this.テキスト361.Width = 2F;
            // 
            // テキスト258
            // 
            this.テキスト258.DataField = "ITEM13";
            this.テキスト258.Height = 0.197F;
            this.テキスト258.Left = 7.253F;
            this.テキスト258.Name = "テキスト258";
            this.テキスト258.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト258.Tag = "";
            this.テキスト258.Text = "ITEM13";
            this.テキスト258.Top = 0.497F;
            this.テキスト258.Width = 0.3229165F;
            // 
            // ITEM02
            // 
            this.ITEM02.DataField = "ITEM02";
            this.ITEM02.Height = 0.1979167F;
            this.ITEM02.Left = 6.525F;
            this.ITEM02.Name = "ITEM02";
            this.ITEM02.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM02.Tag = "";
            this.ITEM02.Text = "ITEM02";
            this.ITEM02.Top = 0.497F;
            this.ITEM02.Width = 0.728F;
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.1875F;
            this.ITEM01.Left = 6.391F;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: right; vertical-align: middle; ddo-char-set: 128";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM01";
            this.ITEM01.Top = 0.3050001F;
            this.ITEM01.Width = 1.34375F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.1972222F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 7.576F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "頁";
            this.ラベル71.Top = 0.497F;
            this.ラベル71.Width = 0.1715278F;
            // 
            // ラベル201
            // 
            this.ラベル201.Height = 0.1979167F;
            this.ラベル201.HyperLink = null;
            this.ラベル201.Left = 5.691972F;
            this.ラベル201.Name = "ラベル201";
            this.ラベル201.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.ラベル201.Tag = "";
            this.ラベル201.Text = "伝票番号：";
            this.ラベル201.Top = 0.497F;
            this.ラベル201.Width = 0.8333333F;
            // 
            // テキスト204
            // 
            this.テキスト204.DataField = "ITEM03";
            this.テキスト204.Height = 0.1875F;
            this.テキスト204.Left = 0.108F;
            this.テキスト204.Name = "テキスト204";
            this.テキスト204.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト204.Tag = "";
            this.テキスト204.Text = "ITEM03";
            this.テキスト204.Top = 0.243F;
            this.テキスト204.Visible = false;
            this.テキスト204.Width = 1.34375F;
            // 
            // テキスト205
            // 
            this.テキスト205.DataField = "ITEM04";
            this.テキスト205.Height = 0.1875F;
            this.テキスト205.Left = 0.108F;
            this.テキスト205.Name = "テキスト205";
            this.テキスト205.Style = "color: Black; font-family: MS UI Gothic; font-size: 14.25pt; font-weight: normal;" +
    " text-align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト205.Tag = "";
            this.テキスト205.Text = "ITEM04";
            this.テキスト205.Top = 0.993F;
            this.テキスト205.Width = 2.279F;
            // 
            // 直線206
            // 
            this.直線206.Height = 0F;
            this.直線206.Left = 0.09722208F;
            this.直線206.LineWeight = 0F;
            this.直線206.Name = "直線206";
            this.直線206.Tag = "";
            this.直線206.Top = 1.204536F;
            this.直線206.Width = 2.710417F;
            this.直線206.X1 = 0.09722208F;
            this.直線206.X2 = 2.807639F;
            this.直線206.Y1 = 1.204536F;
            this.直線206.Y2 = 1.204536F;
            // 
            // ラベル207
            // 
            this.ラベル207.Height = 0.187693F;
            this.ラベル207.HyperLink = null;
            this.ラベル207.Left = 2.387F;
            this.ラベル207.Name = "ラベル207";
            this.ラベル207.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text" +
    "-align: center; vertical-align: middle; ddo-char-set: 128";
            this.ラベル207.Tag = "";
            this.ラベル207.Text = "御中";
            this.ラベル207.Top = 0.9933069F;
            this.ラベル207.Width = 0.441F;
            // 
            // ラベル208
            // 
            this.ラベル208.Height = 0.15625F;
            this.ラベル208.HyperLink = null;
            this.ラベル208.Left = 0.108F;
            this.ラベル208.Name = "ラベル208";
            this.ラベル208.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル208.Tag = "";
            this.ラベル208.Text = "TEL";
            this.ラベル208.Top = 1.241F;
            this.ラベル208.Visible = false;
            this.ラベル208.Width = 0.2395833F;
            // 
            // テキスト209
            // 
            this.テキスト209.DataField = "ITEM05";
            this.テキスト209.Height = 0.15625F;
            this.テキスト209.Left = 0.348F;
            this.テキスト209.Name = "テキスト209";
            this.テキスト209.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト209.Tag = "";
            this.テキスト209.Text = "ITEM05";
            this.テキスト209.Top = 1.241F;
            this.テキスト209.Visible = false;
            this.テキスト209.Width = 0.944F;
            // 
            // ラベル210
            // 
            this.ラベル210.Height = 0.15625F;
            this.ラベル210.HyperLink = null;
            this.ラベル210.Left = 1.292F;
            this.ラベル210.Name = "ラベル210";
            this.ラベル210.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; vertical-align: middle; ddo-char-set: 128";
            this.ラベル210.Tag = "";
            this.ラベル210.Text = "FAX";
            this.ラベル210.Top = 1.241F;
            this.ラベル210.Visible = false;
            this.ラベル210.Width = 0.2395833F;
            // 
            // テキスト211
            // 
            this.テキスト211.DataField = "ITEM06";
            this.テキスト211.Height = 0.15625F;
            this.テキスト211.Left = 1.528F;
            this.テキスト211.Name = "テキスト211";
            this.テキスト211.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト211.Tag = "";
            this.テキスト211.Text = "ITEM06";
            this.テキスト211.Top = 1.242F;
            this.テキスト211.Visible = false;
            this.テキスト211.Width = 0.9791667F;
            // 
            // テキスト212
            // 
            this.テキスト212.DataField = "ITEM07";
            this.テキスト212.Height = 0.1875F;
            this.テキスト212.Left = 5.516F;
            this.テキスト212.Name = "テキスト212";
            this.テキスト212.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト212.Tag = "";
            this.テキスト212.Text = "ITEM07";
            this.テキスト212.Top = 0.7262049F;
            this.テキスト212.Width = 2.21875F;
            // 
            // テキスト213
            // 
            this.テキスト213.DataField = "ITEM08";
            this.テキスト213.Height = 0.1875F;
            this.テキスト213.Left = 5.516F;
            this.テキスト213.Name = "テキスト213";
            this.テキスト213.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト213.Tag = "";
            this.テキスト213.Text = "ITEM08";
            this.テキスト213.Top = 0.914F;
            this.テキスト213.Width = 2.21875F;
            // 
            // テキスト215
            // 
            this.テキスト215.DataField = "ITEM09";
            this.テキスト215.Height = 0.15625F;
            this.テキスト215.Left = 5.535F;
            this.テキスト215.Name = "テキスト215";
            this.テキスト215.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 1";
            this.テキスト215.Tag = "";
            this.テキスト215.Text = "ITEM09";
            this.テキスト215.Top = 1.13075F;
            this.テキスト215.Width = 2.21875F;
            // 
            // テキスト217
            // 
            this.テキスト217.DataField = "ITEM10";
            this.テキスト217.Height = 0.15625F;
            this.テキスト217.Left = 5.535F;
            this.テキスト217.Name = "テキスト217";
            this.テキスト217.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 1";
            this.テキスト217.Tag = "";
            this.テキスト217.Text = "ITEM10";
            this.テキスト217.Top = 1.287F;
            this.テキスト217.Width = 2.21875F;
            // 
            // テキスト218
            // 
            this.テキスト218.DataField = "ITEM11";
            this.テキスト218.Height = 0.1875F;
            this.テキスト218.Left = 3.097222F;
            this.テキスト218.Name = "テキスト218";
            this.テキスト218.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト218.Tag = "";
            this.テキスト218.Text = "ITEM11";
            this.テキスト218.Top = 1.448286F;
            this.テキスト218.Width = 1.811111F;
            // 
            // ラベル219
            // 
            this.ラベル219.Height = 0.15625F;
            this.ラベル219.HyperLink = null;
            this.ラベル219.Left = 5.993333F;
            this.ラベル219.Name = "ラベル219";
            this.ラベル219.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.ラベル219.Tag = "";
            this.ラベル219.Text = "担当：";
            this.ラベル219.Top = 1.48F;
            this.ラベル219.Width = 0.4791667F;
            // 
            // テキスト220
            // 
            this.テキスト220.DataField = "ITEM12";
            this.テキスト220.Height = 0.15625F;
            this.テキスト220.Left = 6.460001F;
            this.テキスト220.Name = "テキスト220";
            this.テキスト220.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト220.Tag = "";
            this.テキスト220.Text = "ITEM12";
            this.テキスト220.Top = 1.48F;
            this.テキスト220.Width = 1.302083F;
            // 
            // ラベル221
            // 
            this.ラベル221.Height = 0.1979167F;
            this.ラベル221.HyperLink = null;
            this.ラベル221.Left = 0.1083779F;
            this.ラベル221.Name = "ラベル221";
            this.ラベル221.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル221.Tag = "";
            this.ラベル221.Text = "品名コード";
            this.ラベル221.Top = 1.709055F;
            this.ラベル221.Width = 0.7806222F;
            // 
            // ラベル222
            // 
            this.ラベル222.Height = 0.194F;
            this.ラベル222.HyperLink = null;
            this.ラベル222.Left = 0.879F;
            this.ラベル222.Name = "ラベル222";
            this.ラベル222.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル222.Tag = "";
            this.ラベル222.Text = "商　品　名";
            this.ラベル222.Top = 1.709055F;
            this.ラベル222.Width = 2.066F;
            // 
            // ラベル223
            // 
            this.ラベル223.Height = 0.194F;
            this.ラベル223.HyperLink = null;
            this.ラベル223.Left = 2.935F;
            this.ラベル223.Name = "ラベル223";
            this.ラベル223.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル223.Tag = "";
            this.ラベル223.Text = "規　格";
            this.ラベル223.Top = 1.709F;
            this.ラベル223.Width = 0.963F;
            // 
            // ラベル224
            // 
            this.ラベル224.Height = 0.194F;
            this.ラベル224.HyperLink = null;
            this.ラベル224.Left = 4.225F;
            this.ラベル224.Name = "ラベル224";
            this.ラベル224.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル224.Tag = "";
            this.ラベル224.Text = "単位";
            this.ラベル224.Top = 1.709F;
            this.ラベル224.Width = 0.415F;
            // 
            // ラベル225
            // 
            this.ラベル225.Height = 0.194F;
            this.ラベル225.HyperLink = null;
            this.ラベル225.Left = 4.640001F;
            this.ラベル225.Name = "ラベル225";
            this.ラベル225.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル225.Tag = "";
            this.ラベル225.Text = "入数";
            this.ラベル225.Top = 1.709F;
            this.ラベル225.Width = 0.5789995F;
            // 
            // ラベル226
            // 
            this.ラベル226.Height = 0.194F;
            this.ラベル226.HyperLink = null;
            this.ラベル226.Left = 5.212F;
            this.ラベル226.Name = "ラベル226";
            this.ラベル226.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル226.Tag = "";
            this.ラベル226.Text = "ケース";
            this.ラベル226.Top = 1.709F;
            this.ラベル226.Width = 0.4319997F;
            // 
            // ラベル227
            // 
            this.ラベル227.Height = 0.194F;
            this.ラベル227.HyperLink = null;
            this.ラベル227.Left = 5.644F;
            this.ラベル227.Name = "ラベル227";
            this.ラベル227.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル227.Tag = "";
            this.ラベル227.Text = "バラ";
            this.ラベル227.Top = 1.709F;
            this.ラベル227.Width = 0.6329998F;
            // 
            // ラベル228
            // 
            this.ラベル228.Height = 0.194F;
            this.ラベル228.HyperLink = null;
            this.ラベル228.Left = 6.267F;
            this.ラベル228.Name = "ラベル228";
            this.ラベル228.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル228.Tag = "";
            this.ラベル228.Text = "単　価";
            this.ラベル228.Top = 1.709055F;
            this.ラベル228.Width = 0.6350002F;
            // 
            // ラベル229
            // 
            this.ラベル229.Height = 0.194F;
            this.ラベル229.HyperLink = null;
            this.ラベル229.Left = 6.892F;
            this.ラベル229.Name = "ラベル229";
            this.ラベル229.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル229.Tag = "";
            this.ラベル229.Text = "金　額";
            this.ラベル229.Top = 1.709055F;
            this.ラベル229.Width = 0.8641646F;
            // 
            // テキスト230
            // 
            this.テキスト230.DataField = "ITEM14";
            this.テキスト230.Height = 0.1875F;
            this.テキスト230.Left = 0.132F;
            this.テキスト230.Name = "テキスト230";
            this.テキスト230.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト230.Tag = "";
            this.テキスト230.Text = "ITEM14";
            this.テキスト230.Top = 1.976959F;
            this.テキスト230.Width = 0.713F;
            // 
            // テキスト232
            // 
            this.テキスト232.DataField = "ITEM16";
            this.テキスト232.Height = 0.188F;
            this.テキスト232.Left = 2.967F;
            this.テキスト232.Name = "テキスト232";
            this.テキスト232.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト232.Tag = "";
            this.テキスト232.Text = "ITEM16";
            this.テキスト232.Top = 1.977F;
            this.テキスト232.Width = 0.9F;
            // 
            // テキスト233
            // 
            this.テキスト233.DataField = "ITEM18";
            this.テキスト233.Height = 0.188F;
            this.テキスト233.Left = 4.265001F;
            this.テキスト233.Name = "テキスト233";
            this.テキスト233.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト233.Tag = "";
            this.テキスト233.Text = "ITEM18";
            this.テキスト233.Top = 1.976959F;
            this.テキスト233.Width = 0.344F;
            // 
            // テキスト234
            // 
            this.テキスト234.DataField = "ITEM19";
            this.テキスト234.Height = 0.188F;
            this.テキスト234.Left = 4.67F;
            this.テキスト234.Name = "テキスト234";
            this.テキスト234.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト234.Tag = "";
            this.テキスト234.Text = "ITEM19";
            this.テキスト234.Top = 1.976F;
            this.テキスト234.Width = 0.511F;
            // 
            // テキスト235
            // 
            this.テキスト235.DataField = "ITEM20";
            this.テキスト235.Height = 0.1875F;
            this.テキスト235.Left = 5.236669F;
            this.テキスト235.Name = "テキスト235";
            this.テキスト235.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト235.Tag = "";
            this.テキスト235.Text = "ITEM20";
            this.テキスト235.Top = 1.976959F;
            this.テキスト235.Width = 0.3663311F;
            // 
            // テキスト236
            // 
            this.テキスト236.DataField = "ITEM21";
            this.テキスト236.Height = 0.1875F;
            this.テキスト236.Left = 5.67F;
            this.テキスト236.Name = "テキスト236";
            this.テキスト236.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト236.Tag = "";
            this.テキスト236.Text = "ITEM21";
            this.テキスト236.Top = 1.976959F;
            this.テキスト236.Width = 0.5720001F;
            // 
            // テキスト237
            // 
            this.テキスト237.DataField = "ITEM22";
            this.テキスト237.Height = 0.1875F;
            this.テキスト237.Left = 6.297F;
            this.テキスト237.Name = "テキスト237";
            this.テキスト237.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト237.Tag = "";
            this.テキスト237.Text = "ITEM22";
            this.テキスト237.Top = 1.977F;
            this.テキスト237.Width = 0.5525824F;
            // 
            // テキスト238
            // 
            this.テキスト238.DataField = "ITEM23";
            this.テキスト238.Height = 0.1875F;
            this.テキスト238.Left = 6.923F;
            this.テキスト238.Name = "テキスト238";
            this.テキスト238.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト238.Tag = "";
            this.テキスト238.Text = "ITEM23";
            this.テキスト238.Top = 1.977F;
            this.テキスト238.Width = 0.8060007F;
            // 
            // テキスト331
            // 
            this.テキスト331.DataField = "ITEM24";
            this.テキスト331.Height = 0.188F;
            this.テキスト331.Left = 0.132F;
            this.テキスト331.Name = "テキスト331";
            this.テキスト331.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト331.Tag = "";
            this.テキスト331.Text = "ITEM24";
            this.テキスト331.Top = 2.325708F;
            this.テキスト331.Width = 0.713F;
            // 
            // テキスト333
            // 
            this.テキスト333.DataField = "ITEM26";
            this.テキスト333.Height = 0.188F;
            this.テキスト333.Left = 2.967F;
            this.テキスト333.Name = "テキスト333";
            this.テキスト333.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト333.Tag = "";
            this.テキスト333.Text = "ITEM26";
            this.テキスト333.Top = 2.325751F;
            this.テキスト333.Width = 0.9F;
            // 
            // テキスト334
            // 
            this.テキスト334.DataField = "ITEM28";
            this.テキスト334.Height = 0.188F;
            this.テキスト334.Left = 4.265001F;
            this.テキスト334.Name = "テキスト334";
            this.テキスト334.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト334.Tag = "";
            this.テキスト334.Text = "ITEM28";
            this.テキスト334.Top = 2.325709F;
            this.テキスト334.Width = 0.344F;
            // 
            // テキスト335
            // 
            this.テキスト335.DataField = "ITEM29";
            this.テキスト335.Height = 0.188F;
            this.テキスト335.Left = 4.67F;
            this.テキスト335.Name = "テキスト335";
            this.テキスト335.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト335.Tag = "";
            this.テキスト335.Text = "ITEM29";
            this.テキスト335.Top = 2.326F;
            this.テキスト335.Width = 0.511F;
            // 
            // テキスト336
            // 
            this.テキスト336.DataField = "ITEM30";
            this.テキスト336.Height = 0.188F;
            this.テキスト336.Left = 5.237F;
            this.テキスト336.Name = "テキスト336";
            this.テキスト336.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト336.Tag = "";
            this.テキスト336.Text = "ITEM30";
            this.テキスト336.Top = 2.326F;
            this.テキスト336.Width = 0.366F;
            // 
            // テキスト337
            // 
            this.テキスト337.DataField = "ITEM31";
            this.テキスト337.Height = 0.188F;
            this.テキスト337.Left = 5.67F;
            this.テキスト337.Name = "テキスト337";
            this.テキスト337.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト337.Tag = "";
            this.テキスト337.Text = "ITEM31";
            this.テキスト337.Top = 2.325708F;
            this.テキスト337.Width = 0.5720001F;
            // 
            // テキスト338
            // 
            this.テキスト338.DataField = "ITEM32";
            this.テキスト338.Height = 0.188F;
            this.テキスト338.Left = 6.297F;
            this.テキスト338.Name = "テキスト338";
            this.テキスト338.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト338.Tag = "";
            this.テキスト338.Text = "ITEM32";
            this.テキスト338.Top = 2.325748F;
            this.テキスト338.Width = 0.548F;
            // 
            // テキスト339
            // 
            this.テキスト339.DataField = "ITEM33";
            this.テキスト339.Height = 0.188F;
            this.テキスト339.Left = 6.923F;
            this.テキスト339.Name = "テキスト339";
            this.テキスト339.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト339.Tag = "";
            this.テキスト339.Text = "ITEM33";
            this.テキスト339.Top = 2.326F;
            this.テキスト339.Width = 0.806F;
            // 
            // テキスト341
            // 
            this.テキスト341.DataField = "ITEM34";
            this.テキスト341.Height = 0.1875F;
            this.テキスト341.Left = 0.132F;
            this.テキスト341.Name = "テキスト341";
            this.テキスト341.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト341.Tag = "";
            this.テキスト341.Text = "ITEM34";
            this.テキスト341.Top = 2.665958F;
            this.テキスト341.Width = 0.713F;
            // 
            // テキスト343
            // 
            this.テキスト343.DataField = "ITEM36";
            this.テキスト343.Height = 0.188F;
            this.テキスト343.Left = 2.972001F;
            this.テキスト343.Name = "テキスト343";
            this.テキスト343.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト343.Tag = "";
            this.テキスト343.Text = "ITEM36";
            this.テキスト343.Top = 2.666F;
            this.テキスト343.Width = 0.9F;
            // 
            // テキスト344
            // 
            this.テキスト344.DataField = "ITEM38";
            this.テキスト344.Height = 0.188F;
            this.テキスト344.Left = 4.265001F;
            this.テキスト344.Name = "テキスト344";
            this.テキスト344.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト344.Tag = "";
            this.テキスト344.Text = "ITEM38";
            this.テキスト344.Top = 2.665958F;
            this.テキスト344.Width = 0.344F;
            // 
            // テキスト345
            // 
            this.テキスト345.DataField = "ITEM39";
            this.テキスト345.Height = 0.188F;
            this.テキスト345.Left = 4.67F;
            this.テキスト345.Name = "テキスト345";
            this.テキスト345.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト345.Tag = "";
            this.テキスト345.Text = "ITEM39";
            this.テキスト345.Top = 2.665958F;
            this.テキスト345.Width = 0.511F;
            // 
            // テキスト346
            // 
            this.テキスト346.DataField = "ITEM40";
            this.テキスト346.Height = 0.188F;
            this.テキスト346.Left = 5.236669F;
            this.テキスト346.Name = "テキスト346";
            this.テキスト346.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト346.Tag = "";
            this.テキスト346.Text = "ITEM40";
            this.テキスト346.Top = 2.665958F;
            this.テキスト346.Width = 0.366F;
            // 
            // テキスト347
            // 
            this.テキスト347.DataField = "ITEM41";
            this.テキスト347.Height = 0.1875F;
            this.テキスト347.Left = 5.67F;
            this.テキスト347.Name = "テキスト347";
            this.テキスト347.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト347.Tag = "";
            this.テキスト347.Text = "ITEM41";
            this.テキスト347.Top = 2.665958F;
            this.テキスト347.Width = 0.5767716F;
            // 
            // テキスト348
            // 
            this.テキスト348.DataField = "ITEM42";
            this.テキスト348.Height = 0.1875F;
            this.テキスト348.Left = 6.297F;
            this.テキスト348.Name = "テキスト348";
            this.テキスト348.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト348.Tag = "";
            this.テキスト348.Text = "ITEM42";
            this.テキスト348.Top = 2.665999F;
            this.テキスト348.Width = 0.5620003F;
            // 
            // テキスト349
            // 
            this.テキスト349.DataField = "ITEM43";
            this.テキスト349.Height = 0.188F;
            this.テキスト349.Left = 6.923F;
            this.テキスト349.Name = "テキスト349";
            this.テキスト349.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト349.Tag = "";
            this.テキスト349.Text = "ITEM43";
            this.テキスト349.Top = 2.666F;
            this.テキスト349.Width = 0.806F;
            // 
            // テキスト350
            // 
            this.テキスト350.DataField = "ITEM44";
            this.テキスト350.Height = 0.1875F;
            this.テキスト350.Left = 0.132F;
            this.テキスト350.Name = "テキスト350";
            this.テキスト350.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト350.Tag = "";
            this.テキスト350.Text = "ITEM44";
            this.テキスト350.Top = 3.000957F;
            this.テキスト350.Width = 0.713F;
            // 
            // テキスト352
            // 
            this.テキスト352.DataField = "ITEM46";
            this.テキスト352.Height = 0.188F;
            this.テキスト352.Left = 2.967F;
            this.テキスト352.Name = "テキスト352";
            this.テキスト352.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト352.Tag = "";
            this.テキスト352.Text = "ITEM46";
            this.テキスト352.Top = 3.001F;
            this.テキスト352.Width = 0.9F;
            // 
            // テキスト353
            // 
            this.テキスト353.DataField = "ITEM48";
            this.テキスト353.Height = 0.188F;
            this.テキスト353.Left = 4.265001F;
            this.テキスト353.Name = "テキスト353";
            this.テキスト353.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト353.Tag = "";
            this.テキスト353.Text = "ITEM48";
            this.テキスト353.Top = 3.000958F;
            this.テキスト353.Width = 0.344F;
            // 
            // テキスト354
            // 
            this.テキスト354.DataField = "ITEM49";
            this.テキスト354.Height = 0.188F;
            this.テキスト354.Left = 4.67F;
            this.テキスト354.Name = "テキスト354";
            this.テキスト354.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト354.Tag = "";
            this.テキスト354.Text = "ITEM49";
            this.テキスト354.Top = 3.000958F;
            this.テキスト354.Width = 0.511F;
            // 
            // テキスト355
            // 
            this.テキスト355.DataField = "ITEM50";
            this.テキスト355.Height = 0.188F;
            this.テキスト355.Left = 5.236669F;
            this.テキスト355.Name = "テキスト355";
            this.テキスト355.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト355.Tag = "";
            this.テキスト355.Text = "ITEM50";
            this.テキスト355.Top = 3.000708F;
            this.テキスト355.Width = 0.366F;
            // 
            // テキスト356
            // 
            this.テキスト356.DataField = "ITEM51";
            this.テキスト356.Height = 0.1875F;
            this.テキスト356.Left = 5.67F;
            this.テキスト356.Name = "テキスト356";
            this.テキスト356.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト356.Tag = "";
            this.テキスト356.Text = "ITEM51";
            this.テキスト356.Top = 3.000958F;
            this.テキスト356.Width = 0.5719998F;
            // 
            // テキスト357
            // 
            this.テキスト357.DataField = "ITEM52";
            this.テキスト357.Height = 0.1875F;
            this.テキスト357.Left = 6.297F;
            this.テキスト357.Name = "テキスト357";
            this.テキスト357.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト357.Tag = "";
            this.テキスト357.Text = "ITEM52";
            this.テキスト357.Top = 3.000998F;
            this.テキスト357.Width = 0.5620003F;
            // 
            // テキスト358
            // 
            this.テキスト358.DataField = "ITEM53";
            this.テキスト358.Height = 0.188F;
            this.テキスト358.Left = 6.923F;
            this.テキスト358.Name = "テキスト358";
            this.テキスト358.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト358.Tag = "";
            this.テキスト358.Text = "ITEM53";
            this.テキスト358.Top = 3.001F;
            this.テキスト358.Width = 0.806F;
            // 
            // テキスト360
            // 
            this.テキスト360.DataField = "ITEM54";
            this.テキスト360.Height = 0.1875F;
            this.テキスト360.Left = 0.132F;
            this.テキスト360.Name = "テキスト360";
            this.テキスト360.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト360.Tag = "";
            this.テキスト360.Text = "ITEM54";
            this.テキスト360.Top = 3.336F;
            this.テキスト360.Width = 0.713F;
            // 
            // テキスト362
            // 
            this.テキスト362.DataField = "ITEM56";
            this.テキスト362.Height = 0.188F;
            this.テキスト362.Left = 2.967F;
            this.テキスト362.Name = "テキスト362";
            this.テキスト362.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト362.Tag = "";
            this.テキスト362.Text = "ITEM56";
            this.テキスト362.Top = 3.33575F;
            this.テキスト362.Width = 0.9F;
            // 
            // テキスト363
            // 
            this.テキスト363.DataField = "ITEM58";
            this.テキスト363.Height = 0.188F;
            this.テキスト363.Left = 4.265001F;
            this.テキスト363.Name = "テキスト363";
            this.テキスト363.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト363.Tag = "";
            this.テキスト363.Text = "ITEM58";
            this.テキスト363.Top = 3.33575F;
            this.テキスト363.Width = 0.344F;
            // 
            // テキスト364
            // 
            this.テキスト364.DataField = "ITEM59";
            this.テキスト364.Height = 0.188F;
            this.テキスト364.Left = 4.67F;
            this.テキスト364.Name = "テキスト364";
            this.テキスト364.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト364.Tag = "";
            this.テキスト364.Text = "ITEM59";
            this.テキスト364.Top = 3.33575F;
            this.テキスト364.Width = 0.511F;
            // 
            // テキスト365
            // 
            this.テキスト365.DataField = "ITEM60";
            this.テキスト365.Height = 0.188F;
            this.テキスト365.Left = 5.236669F;
            this.テキスト365.Name = "テキスト365";
            this.テキスト365.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト365.Tag = "";
            this.テキスト365.Text = "ITEM60";
            this.テキスト365.Top = 3.33575F;
            this.テキスト365.Width = 0.366F;
            // 
            // テキスト366
            // 
            this.テキスト366.DataField = "ITEM61";
            this.テキスト366.Height = 0.1875F;
            this.テキスト366.Left = 5.67F;
            this.テキスト366.Name = "テキスト366";
            this.テキスト366.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト366.Tag = "";
            this.テキスト366.Text = "ITEM61";
            this.テキスト366.Top = 3.336F;
            this.テキスト366.Width = 0.5767716F;
            // 
            // テキスト367
            // 
            this.テキスト367.DataField = "ITEM62";
            this.テキスト367.Height = 0.1875F;
            this.テキスト367.Left = 6.297F;
            this.テキスト367.Name = "テキスト367";
            this.テキスト367.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト367.Tag = "";
            this.テキスト367.Text = "ITEM62";
            this.テキスト367.Top = 3.336F;
            this.テキスト367.Width = 0.5620003F;
            // 
            // テキスト368
            // 
            this.テキスト368.DataField = "ITEM63";
            this.テキスト368.Height = 0.188F;
            this.テキスト368.Left = 6.923F;
            this.テキスト368.Name = "テキスト368";
            this.テキスト368.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト368.Tag = "";
            this.テキスト368.Text = "ITEM63";
            this.テキスト368.Top = 3.33575F;
            this.テキスト368.Width = 0.806F;
            // 
            // テキスト369
            // 
            this.テキスト369.DataField = "ITEM17";
            this.テキスト369.Height = 0.188F;
            this.テキスト369.Left = 3.913F;
            this.テキスト369.Name = "テキスト369";
            this.テキスト369.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト369.Tag = "";
            this.テキスト369.Text = "ITEM17";
            this.テキスト369.Top = 1.977F;
            this.テキスト369.Width = 0.287F;
            // 
            // テキスト371
            // 
            this.テキスト371.DataField = "ITEM37";
            this.テキスト371.Height = 0.188F;
            this.テキスト371.Left = 3.913F;
            this.テキスト371.Name = "テキスト371";
            this.テキスト371.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト371.Tag = "";
            this.テキスト371.Text = "ITEM37";
            this.テキスト371.Top = 2.666F;
            this.テキスト371.Width = 0.287F;
            // 
            // テキスト372
            // 
            this.テキスト372.DataField = "ITEM47";
            this.テキスト372.Height = 0.188F;
            this.テキスト372.Left = 3.913F;
            this.テキスト372.Name = "テキスト372";
            this.テキスト372.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト372.Tag = "";
            this.テキスト372.Text = "ITEM47";
            this.テキスト372.Top = 3.00075F;
            this.テキスト372.Width = 0.287F;
            // 
            // テキスト373
            // 
            this.テキスト373.DataField = "ITEM57";
            this.テキスト373.Height = 0.188F;
            this.テキスト373.Left = 3.913F;
            this.テキスト373.Name = "テキスト373";
            this.テキスト373.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト373.Tag = "";
            this.テキスト373.Text = "ITEM57";
            this.テキスト373.Top = 3.33575F;
            this.テキスト373.Width = 0.287F;
            // 
            // ボックス309
            // 
            this.ボックス309.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Height = 0.2706432F;
            this.ボックス309.Left = 3.675F;
            this.ボックス309.Name = "ボックス309";
            this.ボックス309.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス309.Tag = "";
            this.ボックス309.Top = 4.673F;
            this.ボックス309.Width = 4.080749F;
            // 
            // ボックス310
            // 
            this.ボックス310.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス310.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Height = 0.2395833F;
            this.ボックス310.Left = 3.675F;
            this.ボックス310.Name = "ボックス310";
            this.ボックス310.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス310.Tag = "";
            this.ボックス310.Top = 3.891572F;
            this.ボックス310.Width = 4.08075F;
            // 
            // テキスト312
            // 
            this.テキスト312.DataField = "ITEM64";
            this.テキスト312.Height = 0.155333F;
            this.テキスト312.Left = 4.844F;
            this.テキスト312.Name = "テキスト312";
            this.テキスト312.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト312.Tag = "";
            this.テキスト312.Text = "ITEM64";
            this.テキスト312.Top = 3.933697F;
            this.テキスト312.Width = 0.9364729F;
            // 
            // テキスト313
            // 
            this.テキスト313.DataField = "ITEM65";
            this.テキスト313.Height = 0.155F;
            this.テキスト313.Left = 5.866F;
            this.テキスト313.Name = "テキスト313";
            this.テキスト313.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト313.Tag = "";
            this.テキスト313.Text = "ITEM65";
            this.テキスト313.Top = 3.934F;
            this.テキスト313.Width = 0.878F;
            // 
            // テキスト314
            // 
            this.テキスト314.DataField = "ITEM66";
            this.テキスト314.Height = 0.155F;
            this.テキスト314.Left = 6.827001F;
            this.テキスト314.Name = "テキスト314";
            this.テキスト314.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト314.Tag = "";
            this.テキスト314.Text = "ITEM66";
            this.テキスト314.Top = 3.934F;
            this.テキスト314.Width = 0.891F;
            // 
            // テキスト315
            // 
            this.テキスト315.DataField = "ITEM69";
            this.テキスト315.Height = 0.1875F;
            this.テキスト315.Left = 3.72212F;
            this.テキスト315.Name = "テキスト315";
            this.テキスト315.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 1";
            this.テキスト315.Tag = "";
            this.テキスト315.Text = "ITEM69";
            this.テキスト315.Top = 4.715571F;
            this.テキスト315.Width = 1.056001F;
            // 
            // テキスト316
            // 
            this.テキスト316.DataField = "ITEM76";
            this.テキスト316.Height = 0.188F;
            this.テキスト316.Left = 4.844F;
            this.テキスト316.Name = "テキスト316";
            this.テキスト316.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト316.Tag = "";
            this.テキスト316.Text = "ITEM76";
            this.テキスト316.Top = 4.714002F;
            this.テキスト316.Width = 0.936F;
            // 
            // テキスト317
            // 
            this.テキスト317.DataField = "ITEM77";
            this.テキスト317.Height = 0.188F;
            this.テキスト317.Left = 5.866F;
            this.テキスト317.Name = "テキスト317";
            this.テキスト317.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト317.Tag = "";
            this.テキスト317.Text = "ITEM77";
            this.テキスト317.Top = 4.715F;
            this.テキスト317.Width = 0.878F;
            // 
            // テキスト318
            // 
            this.テキスト318.DataField = "ITEM78";
            this.テキスト318.Height = 0.188F;
            this.テキスト318.Left = 6.827033F;
            this.テキスト318.Name = "テキスト318";
            this.テキスト318.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト318.Tag = "";
            this.テキスト318.Text = "ITEM78";
            this.テキスト318.Top = 4.713858F;
            this.テキスト318.Width = 0.891F;
            // 
            // 直線407
            // 
            this.直線407.Height = 0F;
            this.直線407.Left = 0.105F;
            this.直線407.LineWeight = 0F;
            this.直線407.Name = "直線407";
            this.直線407.Tag = "";
            this.直線407.Top = 1.906693F;
            this.直線407.Width = 7.653F;
            this.直線407.X1 = 0.105F;
            this.直線407.X2 = 7.758F;
            this.直線407.Y1 = 1.906693F;
            this.直線407.Y2 = 1.906693F;
            // 
            // テキスト642
            // 
            this.テキスト642.DataField = "ITEM94";
            this.テキスト642.Height = 0.1875F;
            this.テキスト642.Left = 0.108F;
            this.テキスト642.Name = "テキスト642";
            this.テキスト642.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト642.Tag = "";
            this.テキスト642.Text = "ITEM94";
            this.テキスト642.Top = 0.4300001F;
            this.テキスト642.Width = 2.375F;
            // 
            // テキスト643
            // 
            this.テキスト643.DataField = "ITEM95";
            this.テキスト643.Height = 0.1875F;
            this.テキスト643.Left = 0.108F;
            this.テキスト643.Name = "テキスト643";
            this.テキスト643.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト643.Tag = "";
            this.テキスト643.Text = "ITEM95";
            this.テキスト643.Top = 0.6159999F;
            this.テキスト643.Width = 2.375F;
            // 
            // line3
            // 
            this.line3.Height = 1.889F;
            this.line3.Left = 0.879F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 1.709F;
            this.line3.Width = 0F;
            this.line3.X1 = 0.879F;
            this.line3.X2 = 0.879F;
            this.line3.Y1 = 1.709F;
            this.line3.Y2 = 3.598F;
            // 
            // line4
            // 
            this.line4.Height = 1.885F;
            this.line4.Left = 2.935F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 1.713F;
            this.line4.Width = 0F;
            this.line4.X1 = 2.935F;
            this.line4.X2 = 2.935F;
            this.line4.Y1 = 1.713F;
            this.line4.Y2 = 3.598F;
            // 
            // line5
            // 
            this.line5.Height = 1.889F;
            this.line5.Left = 3.898F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 1.709F;
            this.line5.Width = 0F;
            this.line5.X1 = 3.898F;
            this.line5.X2 = 3.898F;
            this.line5.Y1 = 1.709F;
            this.line5.Y2 = 3.598F;
            // 
            // line6
            // 
            this.line6.Height = 1.889F;
            this.line6.Left = 4.64F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 1.709F;
            this.line6.Width = 9.536743E-07F;
            this.line6.X1 = 4.640001F;
            this.line6.X2 = 4.64F;
            this.line6.Y1 = 1.709F;
            this.line6.Y2 = 3.598F;
            // 
            // line7
            // 
            this.line7.Height = 1.889F;
            this.line7.Left = 5.207001F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 1.709F;
            this.line7.Width = 6.198883E-05F;
            this.line7.X1 = 5.207001F;
            this.line7.X2 = 5.207063F;
            this.line7.Y1 = 1.709F;
            this.line7.Y2 = 3.598F;
            // 
            // line8
            // 
            this.line8.Height = 1.889F;
            this.line8.Left = 5.633507F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 1.709F;
            this.line8.Width = 0.0004930496F;
            this.line8.X1 = 5.634F;
            this.line8.X2 = 5.633507F;
            this.line8.Y1 = 1.709F;
            this.line8.Y2 = 3.598F;
            // 
            // line9
            // 
            this.line9.Height = 1.889F;
            this.line9.Left = 6.267F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 1.709F;
            this.line9.Width = 0.0004258156F;
            this.line9.X1 = 6.267F;
            this.line9.X2 = 6.267426F;
            this.line9.Y1 = 1.709F;
            this.line9.Y2 = 3.598F;
            // 
            // line10
            // 
            this.line10.Height = 1.889F;
            this.line10.Left = 6.892F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 1.709F;
            this.line10.Width = 0.0006127357F;
            this.line10.X1 = 6.892F;
            this.line10.X2 = 6.892613F;
            this.line10.Y1 = 1.709F;
            this.line10.Y2 = 3.598F;
            // 
            // line11
            // 
            this.line11.Height = 1.888F;
            this.line11.Left = 7.757F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 1.71F;
            this.line11.Width = 0.002840042F;
            this.line11.X1 = 7.75984F;
            this.line11.X2 = 7.757F;
            this.line11.Y1 = 1.71F;
            this.line11.Y2 = 3.598F;
            // 
            // line12
            // 
            this.line12.Height = 1.888F;
            this.line12.Left = 0.105F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 1.71F;
            this.line12.Width = 0F;
            this.line12.X1 = 0.105F;
            this.line12.X2 = 0.105F;
            this.line12.Y1 = 1.71F;
            this.line12.Y2 = 3.598F;
            // 
            // line13
            // 
            this.line13.Height = 2.026558E-06F;
            this.line13.Left = 0.1048345F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 1.708998F;
            this.line13.Width = 7.652165F;
            this.line13.X1 = 0.1048345F;
            this.line13.X2 = 7.757F;
            this.line13.Y1 = 1.708998F;
            this.line13.Y2 = 1.709F;
            // 
            // line14
            // 
            this.line14.Height = 0F;
            this.line14.Left = 0.105F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 3.598001F;
            this.line14.Width = 7.653F;
            this.line14.X1 = 0.105F;
            this.line14.X2 = 7.758F;
            this.line14.Y1 = 3.598001F;
            this.line14.Y2 = 3.598001F;
            // 
            // textBox59
            // 
            this.textBox59.DataField = "ITEM96";
            this.textBox59.Height = 0.1875F;
            this.textBox59.Left = 0.108F;
            this.textBox59.Name = "textBox59";
            this.textBox59.Style = "color: Black; font-family: MS UI Gothic; font-size: 14.25pt; font-weight: normal;" +
    " text-align: left; vertical-align: middle; ddo-char-set: 128";
            this.textBox59.Tag = "";
            this.textBox59.Text = "ITEM14";
            this.textBox59.Top = 0.7980001F;
            this.textBox59.Width = 1.184252F;
            // 
            // label13
            // 
            this.label13.Height = 0.16125F;
            this.label13.HyperLink = null;
            this.label13.Left = 5.964F;
            this.label13.Name = "label13";
            this.label13.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.label13.Tag = "";
            this.label13.Text = "FAX\r\n";
            this.label13.Top = 1.287F;
            this.label13.Width = 0.8216542F;
            // 
            // label16
            // 
            this.label16.Height = 0.1412151F;
            this.label16.HyperLink = null;
            this.label16.Left = 6.481F;
            this.label16.Name = "label16";
            this.label16.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.label16.Tag = "";
            this.label16.Text = "(098)861-2707";
            this.label16.Top = 1.136F;
            this.label16.Width = 1.270078F;
            // 
            // label23
            // 
            this.label23.Height = 0.14125F;
            this.label23.HyperLink = null;
            this.label23.Left = 5.964F;
            this.label23.Name = "label23";
            this.label23.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 1";
            this.label23.Tag = "";
            this.label23.Text = "事務所";
            this.label23.Top = 1.136F;
            this.label23.Width = 0.8216542F;
            // 
            // label15
            // 
            this.label15.Height = 0.15625F;
            this.label15.HyperLink = null;
            this.label15.Left = 6.474F;
            this.label15.Name = "label15";
            this.label15.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 128";
            this.label15.Tag = "";
            this.label15.Text = "(098)861-0819";
            this.label15.Top = 1.287F;
            this.label15.Width = 1.270078F;
            // 
            // line32
            // 
            this.line32.Height = 1.889F;
            this.line32.Left = 4.225F;
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Top = 1.709F;
            this.line32.Width = 0F;
            this.line32.X1 = 4.225F;
            this.line32.X2 = 4.225F;
            this.line32.Y1 = 1.709F;
            this.line32.Y2 = 3.598F;
            // 
            // label29
            // 
            this.label29.Height = 0.1972222F;
            this.label29.HyperLink = null;
            this.label29.Left = 0.108F;
            this.label29.Name = "label29";
            this.label29.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 1";
            this.label29.Tag = "";
            this.label29.Text = "「*」は軽減税率であることを示します。";
            this.label29.Top = 3.694F;
            this.label29.Width = 3.33F;
            // 
            // shape7
            // 
            this.shape7.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape7.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape7.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape7.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape7.Height = 0.2706432F;
            this.shape7.Left = 3.675F;
            this.shape7.Name = "shape7";
            this.shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape7.Tag = "";
            this.shape7.Top = 4.402F;
            this.shape7.Width = 4.081001F;
            // 
            // textBox60
            // 
            this.textBox60.DataField = "ITEM68";
            this.textBox60.Height = 0.1875F;
            this.textBox60.Left = 3.722F;
            this.textBox60.Name = "textBox60";
            this.textBox60.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox60.Tag = "";
            this.textBox60.Text = "ITEM68";
            this.textBox60.Top = 4.443F;
            this.textBox60.Width = 1.056001F;
            // 
            // textBox61
            // 
            this.textBox61.DataField = "ITEM73";
            this.textBox61.Height = 0.188F;
            this.textBox61.Left = 4.844F;
            this.textBox61.Name = "textBox61";
            this.textBox61.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox61.Tag = "";
            this.textBox61.Text = "ITEM73";
            this.textBox61.Top = 4.442001F;
            this.textBox61.Width = 0.936F;
            // 
            // textBox62
            // 
            this.textBox62.DataField = "ITEM74";
            this.textBox62.Height = 0.188F;
            this.textBox62.Left = 5.866F;
            this.textBox62.Name = "textBox62";
            this.textBox62.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox62.Tag = "";
            this.textBox62.Text = "ITEM74";
            this.textBox62.Top = 4.443F;
            this.textBox62.Width = 0.878F;
            // 
            // textBox63
            // 
            this.textBox63.DataField = "ITEM75";
            this.textBox63.Height = 0.188F;
            this.textBox63.Left = 6.824883F;
            this.textBox63.Name = "textBox63";
            this.textBox63.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox63.Tag = "";
            this.textBox63.Text = "ITEM75";
            this.textBox63.Top = 4.442857F;
            this.textBox63.Width = 0.891F;
            // 
            // shape8
            // 
            this.shape8.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape8.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape8.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape8.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape8.Height = 0.2706432F;
            this.shape8.Left = 3.675F;
            this.shape8.Name = "shape8";
            this.shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape8.Tag = "";
            this.shape8.Top = 4.131572F;
            this.shape8.Width = 4.081002F;
            // 
            // textBox64
            // 
            this.textBox64.DataField = "ITEM67";
            this.textBox64.Height = 0.1875F;
            this.textBox64.Left = 3.72212F;
            this.textBox64.Name = "textBox64";
            this.textBox64.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox64.Tag = "";
            this.textBox64.Text = "ITEM67";
            this.textBox64.Top = 4.172572F;
            this.textBox64.Width = 1.056001F;
            // 
            // textBox65
            // 
            this.textBox65.DataField = "ITEM70";
            this.textBox65.Height = 0.188F;
            this.textBox65.Left = 4.844F;
            this.textBox65.Name = "textBox65";
            this.textBox65.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox65.Tag = "";
            this.textBox65.Text = "ITEM70";
            this.textBox65.Top = 4.172001F;
            this.textBox65.Width = 0.936F;
            // 
            // textBox66
            // 
            this.textBox66.DataField = "ITEM71";
            this.textBox66.Height = 0.188F;
            this.textBox66.Left = 5.866F;
            this.textBox66.Name = "textBox66";
            this.textBox66.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox66.Tag = "";
            this.textBox66.Text = "ITEM71";
            this.textBox66.Top = 4.173F;
            this.textBox66.Width = 0.878F;
            // 
            // textBox67
            // 
            this.textBox67.DataField = "ITEM72";
            this.textBox67.Height = 0.188F;
            this.textBox67.Left = 6.825001F;
            this.textBox67.Name = "textBox67";
            this.textBox67.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox67.Tag = "";
            this.textBox67.Text = "ITEM72";
            this.textBox67.Top = 4.172429F;
            this.textBox67.Width = 0.891F;
            // 
            // shape10
            // 
            this.shape10.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Height = 0.2706433F;
            this.shape10.Left = 3.674999F;
            this.shape10.Name = "shape10";
            this.shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape10.Tag = "";
            this.shape10.Top = 4.944F;
            this.shape10.Width = 4.080749F;
            // 
            // textBox68
            // 
            this.textBox68.DataField = "ITEM79";
            this.textBox68.Height = 0.1875F;
            this.textBox68.Left = 3.722F;
            this.textBox68.Name = "textBox68";
            this.textBox68.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox68.Tag = "";
            this.textBox68.Text = "ITEM79";
            this.textBox68.Top = 4.986F;
            this.textBox68.Width = 3.022001F;
            // 
            // textBox95
            // 
            this.textBox95.DataField = "ITEM80";
            this.textBox95.Height = 0.188F;
            this.textBox95.Left = 6.827F;
            this.textBox95.Name = "textBox95";
            this.textBox95.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox95.Tag = "";
            this.textBox95.Text = "ITEM80";
            this.textBox95.Top = 4.985001F;
            this.textBox95.Width = 0.891F;
            // 
            // line33
            // 
            this.line33.Height = 1.323999F;
            this.line33.Left = 6.786001F;
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Top = 3.891001F;
            this.line33.Width = 0F;
            this.line33.X1 = 6.786001F;
            this.line33.X2 = 6.786001F;
            this.line33.Y1 = 3.891001F;
            this.line33.Y2 = 5.215F;
            // 
            // line34
            // 
            this.line34.Height = 1.052998F;
            this.line34.Left = 4.814F;
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Top = 3.891001F;
            this.line34.Width = 0F;
            this.line34.X1 = 4.814F;
            this.line34.X2 = 4.814F;
            this.line34.Y1 = 3.891001F;
            this.line34.Y2 = 4.943999F;
            // 
            // line35
            // 
            this.line35.Height = 1.052998F;
            this.line35.Left = 5.822001F;
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Top = 3.891001F;
            this.line35.Width = 0F;
            this.line35.X1 = 5.822001F;
            this.line35.X2 = 5.822001F;
            this.line35.Y1 = 3.891001F;
            this.line35.Y2 = 4.943999F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KBDE10132R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.874166F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル203)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル196)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル535)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル340)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClrCg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト332)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト351)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト370)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト231)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト342)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト361)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト258)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル201)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト204)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト205)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル207)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル208)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト209)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル210)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト211)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト212)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト213)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト215)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト217)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト218)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル219)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト220)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル221)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル222)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル223)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル224)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル225)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル226)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル227)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル228)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル229)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト230)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト232)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト233)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト234)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト235)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト236)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト237)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト238)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト331)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト333)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト334)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト335)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト336)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト337)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト338)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト339)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト341)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト343)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト344)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト345)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト346)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト347)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト348)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト349)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト350)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト352)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト353)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト354)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト355)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト356)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト357)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト358)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト360)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト362)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト363)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト364)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト365)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト366)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト367)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト368)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト369)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト371)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト372)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト373)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト312)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト313)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト314)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト315)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト318)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト642)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト643)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        // Token: 0x0400014B RID: 331
        private PageHeader pageHeader;

        // Token: 0x0400014C RID: 332
        private Detail detail;

        // Token: 0x0400014D RID: 333
        private PageFooter pageFooter;

        // Token: 0x0400015F RID: 351
        private Label ラベル196;

        // Token: 0x040001D3 RID: 467
        private Label ラベル535;

        // Token: 0x04000251 RID: 593
        private Shape shape1;

        // Token: 0x04000252 RID: 594
        private Line line2;

        // Token: 0x04000253 RID: 595
        private TextBox textBox84;

        // Token: 0x04000254 RID: 596
        private Line line27;
        private Shape shape4;
        private Shape shape5;
        private Label label7;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private TextBox textBox1;
        private TextBox textBox2;
        private TextBox textBox3;
        private TextBox textBox4;
        private TextBox textBox5;
        private TextBox textBox6;
        private TextBox textBox7;
        private TextBox textBox8;
        private TextBox textBox9;
        private Label label8;
        private Label label9;
        private TextBox textBox10;
        private TextBox textBox11;
        private Line line1;
        private Label label10;
        private Label label11;
        private TextBox textBox12;
        private Label label12;
        private TextBox textBox13;
        private TextBox textBox14;
        private TextBox textBox15;
        private TextBox textBox16;
        private TextBox textBox17;
        private TextBox textBox18;
        private Label label14;
        private TextBox textBox19;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label20;
        private Label label21;
        private Label label25;
        private Label label26;
        private Label label33;
        private Label label34;
        private TextBox textBox20;
        private TextBox textBox21;
        private TextBox textBox22;
        private TextBox textBox23;
        private TextBox textBox24;
        private TextBox textBox25;
        private TextBox textBox26;
        private TextBox textBox27;
        private TextBox textBox28;
        private TextBox textBox29;
        private TextBox textBox30;
        private TextBox textBox31;
        private TextBox textBox32;
        private TextBox textBox33;
        private TextBox textBox34;
        private TextBox textBox35;
        private TextBox textBox36;
        private TextBox textBox37;
        private TextBox textBox38;
        private TextBox textBox39;
        private TextBox textBox40;
        private TextBox textBox41;
        private TextBox textBox42;
        private TextBox textBox43;
        private TextBox textBox44;
        private TextBox textBox45;
        private TextBox textBox46;
        private TextBox textBox47;
        private TextBox textBox48;
        private TextBox textBox49;
        private TextBox textBox50;
        private TextBox textBox51;
        private TextBox textBox52;
        private TextBox textBox53;
        private TextBox textBox54;
        private TextBox textBox55;
        private TextBox textBox56;
        private TextBox textBox57;
        private TextBox textBox58;
        private TextBox textBox69;
        private TextBox textBox70;
        private TextBox textBox71;
        private TextBox textBox72;
        private TextBox textBox73;
        private Shape shape6;
        private Shape shape2;
        private TextBox textBox74;
        private TextBox textBox75;
        private TextBox textBox76;
        private TextBox textBox77;
        private TextBox textBox78;
        private TextBox textBox79;
        private TextBox textBox80;
        private Line line15;
        private TextBox textBox81;
        private TextBox textBox82;
        private Line line16;
        private Line line17;
        private Line line18;
        private Line line19;
        private Line line20;
        private Line line21;
        private Line line22;
        private Line line23;
        private Line line24;
        private Line line25;
        private Line line26;
        private Line line31;
        private TextBox textBox83;
        private Label label35;
        private Label label36;
        private Label label37;
        private Label label38;
        private Label label39;
        private Label label40;
        private Label label41;
        private Line line28;
        private Label label42;
        private Shape shape3;
        private TextBox textBox86;
        private TextBox textBox87;
        private TextBox textBox88;
        private TextBox textBox89;
        private Shape shape11;
        private TextBox textBox90;
        private TextBox textBox91;
        private TextBox textBox92;
        private TextBox textBox93;
        private Shape shape12;
        private TextBox textBox94;
        private TextBox textBox96;
        private Line line29;
        private Line line30;
        private Line line36;
        private Shape shape9;
        private Label ラベル203;
        private Label label32;
        private Label ラベル340;
        private Label label31;
        private Label BackClrCg;
        private Label label30;
        private Label label28;
        private TextBox テキスト332;
        private TextBox テキスト351;
        private TextBox テキスト370;
        private TextBox テキスト231;
        private TextBox テキスト342;
        private TextBox テキスト361;
        private TextBox テキスト258;
        private TextBox ITEM02;
        private TextBox ITEM01;
        private Label ラベル71;
        private Label ラベル201;
        private TextBox テキスト204;
        private TextBox テキスト205;
        private Line 直線206;
        private Label ラベル207;
        private Label ラベル208;
        private TextBox テキスト209;
        private Label ラベル210;
        private TextBox テキスト211;
        private TextBox テキスト212;
        private TextBox テキスト213;
        private TextBox テキスト215;
        private TextBox テキスト217;
        private TextBox テキスト218;
        private Label ラベル219;
        private TextBox テキスト220;
        private Label ラベル221;
        private Label ラベル222;
        private Label ラベル223;
        private Label ラベル224;
        private Label ラベル225;
        private Label ラベル226;
        private Label ラベル227;
        private Label ラベル228;
        private Label ラベル229;
        private TextBox テキスト230;
        private TextBox テキスト232;
        private TextBox テキスト233;
        private TextBox テキスト234;
        private TextBox テキスト235;
        private TextBox テキスト236;
        private TextBox テキスト237;
        private TextBox テキスト238;
        private TextBox テキスト331;
        private TextBox テキスト333;
        private TextBox テキスト334;
        private TextBox テキスト335;
        private TextBox テキスト336;
        private TextBox テキスト337;
        private TextBox テキスト338;
        private TextBox テキスト339;
        private TextBox テキスト341;
        private TextBox テキスト343;
        private TextBox テキスト344;
        private TextBox テキスト345;
        private TextBox テキスト346;
        private TextBox テキスト347;
        private TextBox テキスト348;
        private TextBox テキスト349;
        private TextBox テキスト350;
        private TextBox テキスト352;
        private TextBox テキスト353;
        private TextBox テキスト354;
        private TextBox テキスト355;
        private TextBox テキスト356;
        private TextBox テキスト357;
        private TextBox テキスト358;
        private TextBox テキスト360;
        private TextBox テキスト362;
        private TextBox テキスト363;
        private TextBox テキスト364;
        private TextBox テキスト365;
        private TextBox テキスト366;
        private TextBox テキスト367;
        private TextBox テキスト368;
        private TextBox テキスト369;
        private TextBox テキスト371;
        private TextBox テキスト372;
        private TextBox テキスト373;
        private Shape ボックス309;
        private Shape ボックス310;
        private TextBox テキスト312;
        private TextBox テキスト313;
        private TextBox テキスト314;
        private TextBox テキスト315;
        private TextBox テキスト316;
        private TextBox テキスト317;
        private TextBox テキスト318;
        private Line 直線407;
        private TextBox テキスト642;
        private TextBox テキスト643;
        private Line line3;
        private Line line4;
        private Line line5;
        private Line line6;
        private Line line7;
        private Line line8;
        private Line line9;
        private Line line10;
        private Line line11;
        private Line line12;
        private Line line13;
        private Line line14;
        private TextBox textBox59;
        private Label label13;
        private Label label16;
        private Label label23;
        private Label label15;
        private Line line32;
        private Label label29;
        private Shape shape7;
        private TextBox textBox60;
        private TextBox textBox61;
        private TextBox textBox62;
        private TextBox textBox63;
        private Shape shape8;
        private TextBox textBox64;
        private TextBox textBox65;
        private TextBox textBox66;
        private TextBox textBox67;
        private Shape shape10;
        private TextBox textBox68;
        private TextBox textBox95;
        private Line line33;
        private Line line34;
        private Line line35;

        // Token: 0x04000255 RID: 597
        private TextBox textBox85;
    }
}
