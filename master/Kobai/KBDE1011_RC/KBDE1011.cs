﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.controls;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x0200000A RID: 10
	public partial class KBDE1011 : BasePgForm
	{


		private bool isChanged = false;

		// Token: 0x0600004E RID: 78 RVA: 0x00022AB4 File Offset: 0x00020CB4
		public KBDE1011()
		{
			this.InitializeComponent();
			base.BindGotFocusEvent();
			new MessageForwarder(this.txtGridEdit, 522);
			new MessageForwarder(this.dgvInputList, 522);
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00022B1C File Offset: 0x00020D1C
		protected override void InitForm()
		{
			this.InitDispOnNew();
			try
			{
				this.HIN_CHUSHI_KUBUN = Util.ToInt(Util.ToString(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1011", "Setting", "CHUSHI_KUBUN")));
			}
			catch (Exception)
			{
				this.HIN_CHUSHI_KUBUN = 1;
			}
			try
			{
				this.SHIFT_CATEGORY_SLIP_TOTAL = Util.ToInt(Util.ToString(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1011", "Setting", "SHIFT_CATEGORY_SLIP_TOTAL")));
				this.ROUND_CATEGORY_DOWN = Util.ToInt(Util.ToString(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1011", "Setting", "ROUND_CATEGORY_DOWN")));
			}
			catch (Exception)
			{
				this.SHIFT_CATEGORY_SLIP_TOTAL = 0;
				this.ROUND_CATEGORY_DOWN = 0;
			}
			try
			{
				this._zaikoDisplay = Util.ToInt(Util.ToString(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1011", "Setting", "ZaikoDisplay")));
			}
			catch (Exception)
			{
				this._zaikoDisplay = 0;
			}
			if (this._zaikoDisplay != 0)
			{
				this.lblShohinNm.Visible = true;
				this.lblZaikoSu.Visible = true;
			}
			DataRow dataRow = base.Dba.GetKaikeiSettingsByKessanKi(base.UInfo.KaishaCd, base.UInfo.KessanKi).Rows[0];
			this.CalcInfo.JIGYO_KUBUN = Util.ToInt(dataRow["JIGYO_KUBUN"]);
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00022C98 File Offset: 0x00020E98
		protected override void OnMoveFocus()
		{
			string activeCtlNm = base.ActiveCtlNm;
			uint num = PrivateImplementationDetails.ComputeStringHash(activeCtlNm);
			if (num <= 2240602464u)
			{
				if (num != 545334842u)
				{
					if (num != 1408784631u)
					{
						if (num != 2240602464u)
						{
							goto IL_15D;
						}
						if (!(activeCtlNm == "txtTantoshaCd"))
						{
							goto IL_15D;
						}
					}
					else if (!(activeCtlNm == "txtFunanushiCd"))
					{
						goto IL_15D;
					}
				}
				else if (!(activeCtlNm == "txtMizuageShishoCd"))
				{
					goto IL_15D;
				}
			}
			else if (num <= 3419771198u)
			{
				if (num != 3386514175u)
				{
					if (num != 3419771198u)
					{
						goto IL_15D;
					}
					if (!(activeCtlNm == "txtTorihikiKubunCd"))
					{
						goto IL_15D;
					}
				}
				else
				{
					if (!(activeCtlNm == "txtGridEdit"))
					{
						goto IL_15D;
					}
					int columnIndex = this.dgvInputList.CurrentCell.ColumnIndex;
					if (columnIndex - 1 > 1)
					{
						if (columnIndex != 8)
						{
							if (columnIndex != 9)
							{
								this.btnF1.Enabled = false;
								goto IL_156;
							}
						}
						else
						{
							if (!ValChk.IsEmpty(this.lblFunanushiNm.Text))
							{
								this.btnF1.Enabled = true;
								goto IL_156;
							}
							goto IL_156;
						}
					}
					this.btnF1.Enabled = true;
					IL_156:
					this.SetZaiko();
					return;
				}
			}
			else if (num != 3660399968u)
			{
				if (num != 4163360521u)
				{
					goto IL_15D;
				}
				if (!(activeCtlNm == "txtDenpyoNo"))
				{
					goto IL_15D;
				}
			}
			else if (!(activeCtlNm == "txtGengoYear"))
			{
				goto IL_15D;
			}
			this.btnF1.Enabled = true;
			this.zaikoClear();
			return;
			IL_15D:
			this.btnF1.Enabled = false;
			this.zaikoClear();
		}

		// Token: 0x06000051 RID: 81 RVA: 0x000020A4 File Offset: 0x000002A4
		public override void PressEsc()
		{

			if (this.IS_INPUT == 2)
			{
				if (Msg.ConfYesNo("未更新データが存在します！\u3000処理を終了しますか？") == DialogResult.No)
				{
					return;
				}
				else
				{
					this.OrgnDialogResult = false;
				}
			}
			base.PressEsc();
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00022E14 File Offset: 0x00021014
		public override void PressF1()
		{
			string activeCtlNm = base.ActiveCtlNm;
			uint num = PrivateImplementationDetails.ComputeStringHash(activeCtlNm);
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            if (num <= 2240602464u)
			{
				if (num != 545334842u)
				{
					if (num != 1408784631u)
					{
						if (num != 2240602464u)
						{
							return;
						}
						if (!(activeCtlNm == "txtTantoshaCd"))
						{
							return;
						}
						Type type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe").GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
						if (type != null)
						{
							object obj = Activator.CreateInstance(type);
							if (obj != null)
							{
								BasePgForm basePgForm = (BasePgForm)obj;
								basePgForm.Par1 = "1";
								basePgForm.ShowDialog(this);
								if (basePgForm.DialogResult == DialogResult.OK)
								{
									string[] array = (string[])basePgForm.OutData;
									this.txtTantoshaCd.Text = array[0];
									this.lblTantoshaNm.Text = array[1];
									return;
								}
							}
						}
					}
					else
					{
						if (!(activeCtlNm == "txtFunanushiCd"))
						{
							return;
						}
						Type type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe").GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
						if (type != null)
						{
							object obj2 = Activator.CreateInstance(type);
							if (obj2 != null)
							{
								BasePgForm basePgForm2 = (BasePgForm)obj2;
								basePgForm2.Par1 = "1";
								basePgForm2.ShowDialog(this);
								if (basePgForm2.DialogResult == DialogResult.OK)
								{
									string[] array2 = (string[])basePgForm2.OutData;
									this.txtFunanushiCd.Text = array2[0];
									this.SetFunanushiInfo();
									return;
								}
							}
						}
					}
				}
				else
				{
					if (!(activeCtlNm == "txtMizuageShishoCd"))
					{
						return;
					}
					Type type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe").GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
					if (type != null)
					{
						object obj3 = Activator.CreateInstance(type);
						if (obj3 != null)
						{
							BasePgForm basePgForm3 = (BasePgForm)obj3;
							basePgForm3.Par1 = "1";
							basePgForm3.ShowDialog(this);
							if (basePgForm3.DialogResult == DialogResult.OK)
							{
								string[] array3 = (string[])basePgForm3.OutData;
								this.txtMizuageShishoCd.Text = array3[0];
								this.lblMizuageShishoNm.Text = array3[1];
								this.txtFunanushiCd.Focus();
								return;
							}
						}
					}
				}
			}
			else
			{
				Type type;
				if (num <= 3419771198u)
				{
					if (num != 3386514175u)
					{
						if (num != 3419771198u)
						{
							return;
						}
						if (!(activeCtlNm == "txtTorihikiKubunCd"))
						{
							return;
						}
					}
					else
					{
						if (!(activeCtlNm == "txtGridEdit"))
						{
							return;
						}
						int columnIndex = this.dgvInputList.CurrentCell.ColumnIndex;
						if (columnIndex <= 2)
						{
							if (columnIndex == 1)
							{
								type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe").GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
								if (type != null)
								{
									object obj4 = Activator.CreateInstance(type);
									if (obj4 != null)
									{
										BasePgForm basePgForm4 = (BasePgForm)obj4;
										basePgForm4.Par1 = "VI_HN_HANBAI_KUBUN_NM";
										basePgForm4.ShowDialog(this);
										if (basePgForm4.DialogResult == DialogResult.OK)
										{
											string[] array4 = (string[])basePgForm4.OutData;
											this.txtGridEdit.Text = array4[0];
										}
									}
								}
								this.txtGridEdit.Focus();
								return;
							}
							if (columnIndex != 2)
							{
								return;
							}
							type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe").GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
							if (type != null)
							{
								object obj5 = Activator.CreateInstance(type);
								if (obj5 != null)
								{
									BasePgForm basePgForm5 = (BasePgForm)obj5;
									basePgForm5.Par1 = "1";
									if (this.HIN_CHUSHI_KUBUN == 1)
									{
										basePgForm5.Par3 = this.HIN_CHUSHI_KUBUN.ToString();
									}
									basePgForm5.ShowDialog(this);
									if (basePgForm5.DialogResult == DialogResult.OK)
									{
										string[] array5 = (string[])basePgForm5.OutData;
										this.txtGridEdit.Text = array5[0];
										this.GetShohinInfo(2);
									}
								}
							}
							this.txtGridEdit.Focus();
							return;
						}
						else
						{
							if (columnIndex == 8)
							{
								if (!ValChk.IsEmpty(this.lblFunanushiNm.Text) && !ValChk.IsEmpty(this.dgvInputList[2, this.dgvInputList.CurrentCell.RowIndex].Value))
								{
									using (KBDE1014 kbde = new KBDE1014(Util.ToDecimal(this.txtFunanushiCd.Text), Util.ToDecimal(this.dgvInputList[2, this.dgvInputList.CurrentCell.RowIndex].Value)))
									{
										kbde.ShowDialog(this);
										if (kbde.DialogResult == DialogResult.OK)
										{
											string[] array6 = (string[])kbde.OutData;
											this.txtGridEdit.Text = Util.FormatNum(array6[0], this.CalcInfo.KETA_URI_TANKA);
											this.dgvInputList[8, this.dgvInputList.CurrentCell.RowIndex].Value = Util.FormatNum(array6[0], this.CalcInfo.KETA_URI_TANKA);
											if (!this.IsCalc(this.dgvInputList.CurrentCell.RowIndex))
											{
												this.dgvInputList[9, this.dgvInputList.CurrentCell.RowIndex].Value = "";
												this.dgvInputList[10, this.dgvInputList.CurrentCell.RowIndex].Value = "";
											}
											else
											{
												this.CellKingkSet(this.dgvInputList.CurrentCell.RowIndex);
											}
											this.DataGridSum();
										}
									}
								}
								this.txtGridEdit.Focus();
								return;
							}
							if (columnIndex != 9)
							{
								return;
							}
							type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1061.exe").GetType("jp.co.fsi.zm.zmcm1061.ZMCM1065");
							if (type != null)
							{
								object obj6 = Activator.CreateInstance(type);
								if (obj6 != null)
								{
									bool flag = false;
									BasePgForm basePgForm6 = (BasePgForm)obj6;
									basePgForm6.InData = Util.ToString(this.dgvInputList[14, this.dgvInputList.CurrentCell.RowIndex].Value);
									while (!flag)
									{
										basePgForm6.ShowDialog(this);
										if (basePgForm6.DialogResult == DialogResult.OK)
										{
											string[] array7 = (string[])basePgForm6.OutData;
											// TODO 2020/02/12 Upd Mutu Asato
											// DateTime baseDate = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text, this.txtMonth.Text, this.txtDay.Text, base.Dba);
											DateTime baseDate = Util.ConvAdDate(this.fsiDateDay.Gengo, this.fsiDateDay.WYear, this.fsiDateDay.Month, this.fsiDateDay.Day, base.Dba);
											decimal num2 = 0m;
											if (this.ConfPastZeiKbn(baseDate, array7[0], ref num2, Util.ToDecimal(Util.ToString(array7[2]))) == DialogResult.OK)
											{
												this.dgvInputList[14, this.dgvInputList.CurrentCell.RowIndex].Value = array7[0];
												this.dgvInputList[11, this.dgvInputList.CurrentCell.RowIndex].Value = Util.FormatNum(Util.ToDecimal(array7[2]), 1);
												this.dgvInputList[17, this.dgvInputList.CurrentCell.RowIndex].Value = array7[3];

												// TODO 2020/02/13 Mutu Asato
												this.dgvInputList[19, this.dgvInputList.CurrentCell.RowIndex].Value = "";
												if (array7[0] == "50")
												{
													this.dgvInputList[19, this.dgvInputList.CurrentCell.RowIndex].Value = "*";
												}

												this.CellTaxSet(this.dgvInputList.CurrentCell.RowIndex);
												flag = true;
											}
										}
										else
										{
											flag = true;
										}
									}
								}
							}
							this.txtGridEdit.Focus();
							return;
						}
					}
				}
				else if (num != 3660399968u)
				{
					if (num != 4163360521u)
					{
						return;
					}
					if (!(activeCtlNm == "txtDenpyoNo"))
					{
						return;
					}
					using (KBDE1012 kbde2 = new KBDE1012())
					{
						kbde2.InData = this.DenpyoCondition;
						kbde2.ShowDialog(this);
						if (kbde2.DialogResult == DialogResult.OK)
						{
							string[] array8 = (string[])kbde2.OutData;
							this.DenpyoCondition = array8[1];
							this.txtDenpyoNo.Text = array8[0];
							this.GetDenpyoData();
							return;
						}
						this.DenpyoCondition = string.Empty;
						return;
					}
				}
				else
				{
					// TODO 2020/02/12 Upd Mutu Asato
					//if (!(activeCtlNm == "txtGengoYear"))
					//{
					//	return;
					//}
					//type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe").GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");

					//if (!(type != null))
					//{
					//	return;
					//}
					//object obj7 = Activator.CreateInstance(type);
					//if (obj7 == null)
					//{
					//	return;
					//}
					//BasePgForm basePgForm7 = (BasePgForm)obj7;
					//basePgForm7.InData = this.lblGengo.Text;
					//basePgForm7.ShowDialog(this);
					//if (basePgForm7.DialogResult == DialogResult.OK)
					//{
					//	string[] array9 = (string[])basePgForm7.OutData;
					//	this.lblGengo.Text = array9[1];
					//	this.SetJp();
					//	return;
					//}
					//return;
				}
				type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe").GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
				if (type != null)
				{
					object obj8 = Activator.CreateInstance(type);
					if (obj8 != null)
					{
						BasePgForm basePgForm8 = (BasePgForm)obj8;
						basePgForm8.InData = this.txtTorihikiKubunCd.Text;
						basePgForm8.Par1 = "TB_HN_F_TORIHIKI_KUBUN1";
						basePgForm8.ShowDialog(this);
						if (basePgForm8.DialogResult == DialogResult.OK)
						{
							string[] array10 = (string[])basePgForm8.OutData;
							this.txtTorihikiKubunCd.Text = array10[0];
							this.lblTorihikiKubunNm.Text = array10[1];
							return;
						}
					}
				}
			}
		}

		// Token: 0x06000053 RID: 83 RVA: 0x000236CC File Offset: 0x000218CC
		public override void PressF3()
		{
			if (!this.btnF3.Enabled)
			{
				return;
			}
			if (this.MODE_EDIT == 1)
			{
				return;
			}
			if (!this.isValidDenpyoNo())
			{
				this.txtDenpyoNo.SelectAll();
				this.txtDenpyoNo.Focus();
				return;
			}
			if (ValChk.IsEmpty(this.txtDenpyoNo.Text))
			{
				return;
			}
			if (Util.GetKaikeiNendoFixedFlg(base.UInfo.KaikeiNendo, base.Dba))
			{
				Msg.Error("この会計年度は凍結されています。");
				return;
			}
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(this.txtDenpyoNo.Text));
			if (Util.ToLong(base.Dba.GetDataTableByConditionWithParams("COUNT(*) AS CNT", "TB_HN_TORIHIKI_DENPYO", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO", dbParamCollection).Rows[0]["CNT"]) == 0L)
			{
				return;
			}
			if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
			{
				return;
			}
			this.DeleteData();
			this.InitDispOnNew();
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00023838 File Offset: 0x00021A38
		public override void PressF6()
		{
			if (!this.btnF6.Enabled)
			{
				return;
			}
			if (Util.GetKaikeiNendoFixedFlg(base.UInfo.KaikeiNendo, base.Dba))
			{
				Msg.Error("この会計年度は凍結されています。");
				return;
			}
			this._lastUpdCheck = true;
			if (!this.ValidateAll())
			{
				return;
			}
			this.DataGridSum();
			this._lastUpdCheck = false;
			// TODO 2020/02/12 Mutu Asato
			DateTime baseDate2 =
				Util.ConvAdDate(this.fsiDateDay.Gengo, this.fsiDateDay.WYear, this.fsiDateDay.Month, this.fsiDateDay.Day, base.Dba);

			if (Util.GetKaikeiNendo(baseDate2, base.Dba) != base.UInfo.KaikeiNendo)
			{
				Msg.Error("入力日付が選択会計年度の範囲外です。");
				return;
			}
			if (Msg.ConfYesNo(((this.MODE_EDIT == 1) ? "登録" : "更新") + "しますか？") == DialogResult.No)
			{
				return;
			}
			decimal denpyo_BANGO = this.UpdateData();
			if (Msg.ConfYesNo("プレビューしますか？") == DialogResult.Yes)
			{
				this.DoPrint(true, denpyo_BANGO, 2);
			}
			this.InitDispOnNew();
		}

		// Token: 0x06000055 RID: 85 RVA: 0x0002394C File Offset: 0x00021B4C
		public override void PressF8()
		{
			if (this.dgvInputList.CurrentCell == null)
			{
				return;
			}
			if (this.dgvInputList.RowCount > 1 && this.dgvInputList.RowCount > this.dgvInputList.CurrentCell.RowIndex + 1)
			{
				this.dgvInputList.Rows.RemoveAt(this.dgvInputList.CurrentCell.RowIndex);
				int num = 0;
				while (this.dgvInputList.RowCount > num)
				{
					this.dgvInputList[0, num].Value = num + 1;
					num++;
				}
				this.DataGridSum();
			}
			if (base.ActiveControl != this.dgvInputList)
			{
				this.dgvInputList.Focus();
			}
			try
			{
				DataGridViewCell currentCell = this.dgvInputList.CurrentCell;
				if (currentCell != null)
				{
					this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex];
				}
			}
			catch (Exception)
			{
			}
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00023A48 File Offset: 0x00021C48
		public override void PressF9()
		{
			if (this.dgvInputList.CurrentCell == null)
			{
				return;
			}
			if (this.dgvInputList.CurrentCell.RowIndex > 0)
			{
				int num = 1;
				int rowIndex = this.dgvInputList.CurrentCell.RowIndex;
				DataGridViewRow dataGridViewRow = this.dgvInputList.Rows[rowIndex - 1];
				while (dataGridViewRow.Cells.Count > num)
				{
					this.dgvInputList[num, rowIndex].Value = this.dgvInputList[num, rowIndex - 1].Value;
					num++;
				}
				if (this.txtGridEdit.Visible)
				{
					this.txtGridEdit.Text = Util.ToString(this.dgvInputList[this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex].Value);
				}
				if (this.dgvInputList.Rows.Count == rowIndex + 1)
				{
					this.dgvInputList.RowCount = this.dgvInputList.RowCount + 1;
					this.dgvInputList[0, this.dgvInputList.RowCount - 1].Value = this.dgvInputList.RowCount;
				}
				this.DataGridSum();
				if (base.ActiveControl != this.dgvInputList)
				{
					this.dgvInputList.Focus();
				}
			}
		}

		// Token: 0x06000057 RID: 87 RVA: 0x000020A4 File Offset: 0x000002A4
		public override void PressF10()
		{
			base.DialogResult = DialogResult.Cancel;
			base.PressEsc();
		}

		// Token: 0x06000058 RID: 88 RVA: 0x00023BA4 File Offset: 0x00021DA4
		public override void PressF11()
		{
			if (!this.btnF11.Enabled)
			{
				return;
			}
			if (Util.GetKaikeiNendoFixedFlg(base.UInfo.KaikeiNendo, base.Dba))
			{
				Msg.Error("この会計年度は凍結されています。");
				return;
			}
			if (!this.ValidateAll())
			{
				return;
			}
			this.DataGridSum();

			DateTime baseTime1 = Util.ConvAdDate(this.fsiDateDay.Gengo, this.fsiDateDay.WYear, this.fsiDateDay.Month, this.fsiDateDay.Day, base.Dba);

			if (Util.GetKaikeiNendo(baseTime1, base.Dba) != base.UInfo.KaikeiNendo)
			{
				Msg.Error("入力日付が選択会計年度の範囲外です。");
				return;
			}
			decimal denpyo_BANGO = this.UpdateData();
			this.DoPrint(true, denpyo_BANGO, 1);
			this.InitDispOnNew();
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00002398 File Offset: 0x00000598
		public override void PressF12()
		{
			new PrintSettingForm(new string[]
			{
				"KBDE10131R",
				"KBDE10132R"
			}).ShowDialog();
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00023C74 File Offset: 0x00021E74
		private void txtDenpyoNo_Validating(object sender, CancelEventArgs e)
		{
			if (!this.isValidDenpyoNo())
			{
				this.txtDenpyoNo.SelectAll();
				this.txtDenpyoNo.Focus();
				e.Cancel = true;
				return;
			}
			if (ValChk.IsEmpty(this.txtDenpyoNo.Text))
			{
				e.Cancel = false;
				return;
			}
			this.IS_INPUT = 2;
			this.OrgnDialogResult = true;
			e.Cancel = this.GetDenpyoData();
		}

		// Token: 0x0600005B RID: 91 RVA: 0x00023CD8 File Offset: 0x00021ED8
		private void txtGengoYear_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsYear(this.txtGengoYear.Text, this.txtGengoYear.MaxLength))
			{
				e.Cancel = true;
				this.txtGengoYear.SelectAll();
				return;
			}
			this.txtGengoYear.Text = Util.ToString(IsValid.SetYear(this.txtGengoYear.Text));
			//this.CheckJp();
			//this.SetJp();
			this.IS_INPUT = 2;
			this.OrgnDialogResult = true;

		}

		// Token: 0x0600005C RID: 92 RVA: 0x00023D50 File Offset: 0x00021F50
		private void txtMonth_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
			{
				e.Cancel = true;
				this.txtMonth.SelectAll();
				return;
			}
			this.txtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtMonth.Text));
			//this.CheckJp();
			//this.SetJp();
			this.IS_INPUT = 2;
			this.OrgnDialogResult = true;

		}

		// Token: 0x0600005D RID: 93 RVA: 0x00023DC8 File Offset: 0x00021FC8
		private void txtDay_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
			{
				e.Cancel = true;
				this.txtDay.SelectAll();
				return;
			}
			this.txtDay.Text = Util.ToString(IsValid.SetDay(this.txtDay.Text));
			//this.CheckJp();
			//this.SetJp();
			this.IS_INPUT = 2;
			this.OrgnDialogResult = true;

		}

		// Token: 0x0600005E RID: 94 RVA: 0x000023BB File Offset: 0x000005BB
		private void txtTorihikiKubunCd_Validating(object sender, CancelEventArgs e)
		{
			this.lblTorihikiKubunNm.Text = base.Dba.GetName(base.UInfo, "TB_HN_F_TORIHIKI_KUBUN1", this.txtMizuageShishoCd.Text, this.txtTorihikiKubunCd.Text);
			this.IS_INPUT = 2;
			this.OrgnDialogResult = true;

		}

		// Token: 0x0600005F RID: 95 RVA: 0x00023E40 File Offset: 0x00022040
		private void txtFunanushiCd_Validating(object sender, CancelEventArgs e)
		{

			this.lblShohizeiInputHoho2.Text = "";
			this.lblShohizeiTenka2.Text = "";
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("TORIHIKISAKI_NM", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN1 = 1", dbParamCollection);
			if (dataTableByConditionWithParams != null && dataTableByConditionWithParams.Rows.Count > 0)
			{
				this.lblFunanushiNm.Text = Util.ToString(dataTableByConditionWithParams.Rows[0]["TORIHIKISAKI_NM"]);
			}

			if (!ValChk.IsEmpty(this.lblFunanushiNm.Text))
			{
				int tokuisaki_CD = this.CalcInfo.TOKUISAKI_CD;
				this.SetFunanushiInfo();
				if (tokuisaki_CD != 0 && !ValChk.IsEmpty(this.txtFunanushiCd.Text) && Util.ToInt(this.txtFunanushiCd.Text) != tokuisaki_CD)
				{
					this.CellTaxRateSet();
				}
				this.IS_INPUT = 2;
				this.OrgnDialogResult = true;
			}
		}

		// Token: 0x06000060 RID: 96 RVA: 0x000023EB File Offset: 0x000005EB
		private void txtTantoshaCd_Validating(object sender, CancelEventArgs e)
		{
			this._personCheck = false;
			this.lblTantoshaNm.Text = base.Dba.GetName(base.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);
			if (!ValChk.IsEmpty(this.lblTantoshaNm.Text))
			{
				this._personCheck = true;
				this.IS_INPUT = 2;
				this.OrgnDialogResult = true;
			}
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00023EDC File Offset: 0x000220DC
		private void txtTantoshaCd_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Up)
			{
				this.txtFunanushiCd.Focus();
			}
			if (this._personCheck && e.KeyCode == Keys.Return)
			{
				this._personCheck = false;
				this.dgvInputList.Focus();
				this.dgvInputList.CurrentCell = this.dgvInputList[2, 0];
			}
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00023F3C File Offset: 0x0002213C
		private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
		{
			this.lblMizuageShishoNm.Text = base.Dba.GetName(base.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

			if (!ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
			{
				this.IS_INPUT = 2;
				this.OrgnDialogResult = true;
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00023FAC File Offset: 0x000221AC
		private void dgvInputList_Enter(object sender, EventArgs e)
		{
			try
			{
				DataGridViewCell currentCell = this.dgvInputList.CurrentCell;
				if (currentCell != null && currentCell.ColumnIndex == 0)
				{
					this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex];
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		// Token: 0x06000064 RID: 100 RVA: 0x0002400C File Offset: 0x0002220C
		private void dgvInputList_CellEnter(object sender, DataGridViewCellEventArgs e)
		{
			this.txtGridEdit.TextAlign = HorizontalAlignment.Right;
			this.txtGridEdit.MaxLength = 10;
			this.txtGridEdit.ImeMode = ImeMode.Disable;
			int columnIndex = e.ColumnIndex;
			if (columnIndex - 1 <= 2 || columnIndex - 6 <= 4)
			{
				if ((Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 1 || e.ColumnIndex != 10) && ((e.ColumnIndex != 4 && e.ColumnIndex != 6) || Util.ToInt(Util.ToString(this.dgvInputList[4, e.RowIndex].Value)) != 0) && (e.ColumnIndex != 10 || !ValChk.IsEmpty(this.dgvInputList[11, e.RowIndex].Value) || !ValChk.IsEmpty(this.dgvInputList[14, e.RowIndex].Value)) && (e.ColumnIndex != 3 || Util.ToInt(Util.ToString(this.dgvInputList[18, e.RowIndex].Value)) != 1) && (e.ColumnIndex != 3 || !ValChk.IsEmpty(this.dgvInputList[2, e.RowIndex].Value)))
				{
					if (e.ColumnIndex == 3)
					{
						this.txtGridEdit.TextAlign = HorizontalAlignment.Left;
						this.txtGridEdit.MaxLength = 40;
						this.txtGridEdit.ImeMode = ImeMode.Hiragana;
					}
					this.txtGridEdit.BackColor = Color.White;
					if (e.RowIndex % 2 == 1)
					{
						this.txtGridEdit.BackColor = ColorTranslator.FromHtml("#BBFFFF");
					}
					this.txtGridEdit.Visible = true;
					Rectangle cellDisplayRectangle = ((DataGridView)sender).GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);
					this.txtGridEdit.Size = cellDisplayRectangle.Size;
					this.txtGridEdit.Top = cellDisplayRectangle.Top + this.dgvInputList.Top;
					this.txtGridEdit.Left = cellDisplayRectangle.Left + this.dgvInputList.Left;
					this.txtGridEdit.Text = Util.ToString(this.dgvInputList[e.ColumnIndex, e.RowIndex].Value).Replace(",", "");
					this.txtGridEdit.Focus();
					return;
				}
			}
			else
			{
				this.txtGridEdit.Visible = false;
			}
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00024288 File Offset: 0x00022488
		private void dgvInputList_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
		{
			switch (e.ColumnIndex)
			{
			case 1:
			case 2:
			case 8:
			case 9:
				this.btnF1.Enabled = true;
				return;
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 10:
			case 11:
				this.btnF1.Enabled = false;
				return;
			default:
				return;
			}
		}

		// Token: 0x06000066 RID: 102 RVA: 0x000242EC File Offset: 0x000224EC
		private void dgvInputList_Scroll(object sender, ScrollEventArgs e)
		{
			Rectangle cellDisplayRectangle = ((DataGridView)sender).GetCellDisplayRectangle(this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex, false);
			this.txtGridEdit.Size = cellDisplayRectangle.Size;
			this.txtGridEdit.Top = cellDisplayRectangle.Top + this.dgvInputList.Top;
			this.txtGridEdit.Left = cellDisplayRectangle.Left + this.dgvInputList.Left;
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00024374 File Offset: 0x00022574
		private void dgvInputList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			int columnIndex = e.ColumnIndex;
			if (columnIndex - 6 <= 1 || columnIndex - 9 <= 1)
			{
				if (Util.ToDecimal(Util.ToString(e.Value)) < 0m)
				{
					e.CellStyle.ForeColor = Color.Red;
					return;
				}
				e.CellStyle.ForeColor = Color.Black;
			}
		}

		// Token: 0x06000068 RID: 104 RVA: 0x000243D4 File Offset: 0x000225D4
		private void txtGridEdit_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.Right)
			{
				DataGridViewCell currentCell = this.dgvInputList.CurrentCell;
				decimal num = 0m;
				int columnIndex = currentCell.ColumnIndex;
				if ((columnIndex == 1 || columnIndex - 5 <= 5) && e.KeyCode == Keys.Down)
				{
					int num2 = this.dgvInputList.Rows.Count;
					if (currentCell.RowIndex + 1 == num2)
					{
						this.txtGridEdit.Focus();
						return;
					}
				}
				switch (currentCell.ColumnIndex)
				{
					case 1:
						if (ValChk.IsEmpty(this.txtGridEdit.Text))
						{
							this.dgvInputList[1, currentCell.RowIndex].Value = "";
						}
						else
						{
							if (!ValChk.IsNumber(this.txtGridEdit.Text))
							{
								this.dgvInputList[1, currentCell.RowIndex].Value = Regex.Replace(this.txtGridEdit.Text, "\\D", "");
								this.txtGridEdit.Focus();
								return;
							}

							if (base.Dba.GetName(base.UInfo, "VI_HN_HANBAI_KUBUN_NM", this.txtMizuageShishoCd.Text, this.txtGridEdit.Text) == null)
							{
								this.dgvInputList[1, currentCell.RowIndex].Value = "";
								this.txtGridEdit.Text = "";
								Msg.Error("入力に誤りがあります。");
								this.txtGridEdit.Focus();
								return;
							}
							this.dgvInputList[1, currentCell.RowIndex].Value = this.txtGridEdit.Text;
						}

						if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex];
						}
						else
						{
							if (currentCell.RowIndex > 0)
							{
								//this.dgvInputList.CurrentCell = this.dgvInputList[1, currentCell.RowIndex - 1];
							}
							else
							{
								if (e.KeyCode == Keys.Up)
								{
									this.txtGridEdit.Visible = false;
									this.txtTantoshaCd.Focus();
									return;
								}
							}
							this.txtGridEdit.Focus();
						}
						if (e.KeyCode == Keys.Right)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
							return;
						}
						break;
					case 2:
						if (ValChk.IsEmpty(this.txtGridEdit.Text) || !ValChk.IsNumber(this.txtGridEdit.Text))
						{
							this.dgvInputList[2, currentCell.RowIndex].Value = "";
							this.dgvInputList[3, currentCell.RowIndex].Value = "";
							this.dgvInputList[12, currentCell.RowIndex].Value = "";
							this.dgvInputList[13, currentCell.RowIndex].Value = "";
							this.dgvInputList[14, currentCell.RowIndex].Value = "";
							this.dgvInputList[15, currentCell.RowIndex].Value = "";
							this.dgvInputList[11, currentCell.RowIndex].Value = "";
							this.dgvInputList[16, currentCell.RowIndex].Value = "";
							this.dgvInputList[17, currentCell.RowIndex].Value = "";
							this.dgvInputList[18, currentCell.RowIndex].Value = "";
							this.dgvInputList[19, currentCell.RowIndex].Value = "";
							this.dgvInputList[4, currentCell.RowIndex].Value = "";
							this.dgvInputList[5, currentCell.RowIndex].Value = "";

							if (e.KeyCode == Keys.Up && currentCell.RowIndex == 0)
							{
								this.txtGridEdit.Visible = false;
								this.txtDummy.Visible = false;
								this.txtTantoshaCd.Focus();
								return;
							}
							Msg.Error("商品コードを入力してください。");

							this.txtGridEdit.Focus();
							return;
						}
						// エラーあり
						if (!this.GetShohinInfo(1))
						{
							return;
						}

						if (!this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[2, this.dgvInputList.CurrentCell.RowIndex];
						}
						if (e.KeyCode == Keys.Right)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[7, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
							return;
						}
						if (e.KeyCode == Keys.Enter)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[7, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
							return;
						}
						break;
					case 3:

						if (!ValChk.IsWithinLength(this.txtGridEdit.Text, this.txtGridEdit.MaxLength))
						{
							Msg.Error("入力に誤りがあります。");
							this.txtGridEdit.Focus();
							return;
						}
						this.dgvInputList[3, currentCell.RowIndex].Value = Util.ToString(this.txtGridEdit.Text);
						if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
						{
							if (Util.ToInt(Util.ToString(this.dgvInputList[4, currentCell.RowIndex].Value)) == 0)
							{
								this.dgvInputList.CurrentCell = this.dgvInputList[7, currentCell.RowIndex];
							}
							else
							{
								this.dgvInputList.CurrentCell = this.dgvInputList[6, currentCell.RowIndex];
							}
						}
						if (e.KeyCode == Keys.Right)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[5, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
							return;
						}
						break;
					case 4:
						break;
					case 5:

						if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[7, currentCell.RowIndex];
						}
						if (e.KeyCode == Keys.Right)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[6, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
							return;
						}
						break;
					case 6:
						// 2020/02/13 Mutu Asato

						if (!ValChk.IsDecNumWithinLength(this.txtGridEdit.Text, 19, 2, true))
						{
							this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
							this.txtGridEdit.Focus();
							return;
						}
						if (Util.ToDecimal(Util.ToString(this.dgvInputList[6, currentCell.RowIndex].Value)) != Util.ToDecimal(Util.ToString(this.txtGridEdit.Text)))
						{
							this.dgvInputList[6, currentCell.RowIndex].Value = Util.FormatNum(Util.ToDecimal(Util.ToString(this.txtGridEdit.Text)));
							if (!this.CellKingkSet(currentCell.RowIndex))
							{
								Msg.Error("金額が大きすぎる為、複数明細へ分けて入力を行って下さい。");
								this.txtGridEdit.Focus();
								return;
							}
							this.DataGridSum();
						}
						if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[7, currentCell.RowIndex];
						}
						if (e.KeyCode == Keys.Right)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[7, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
							return;
						}
						break;
					case 7:
						if (!ValChk.IsDecNumWithinLength(this.txtGridEdit.Text, 19, 3, true))
						{
							this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
							this.txtGridEdit.Focus();
							return;
						}
						num = Util.ToDecimal(Util.ToString(this.txtGridEdit.Text)) * 100m;
						num = TaxUtil.CalcFraction(num, 0, 1) / 100m;
						decimal num3 = Util.ToDecimal(Util.FormatNum(num, this.CalcInfo.KETA_SURYO2));
						if (Util.ToInt(Util.ToString(this.dgvInputList[4, currentCell.RowIndex].Value)) != 0 && Util.ToDecimal(Util.ToString(this.dgvInputList[4, currentCell.RowIndex].Value)) <= num3)
						{
							decimal d = Util.ToDecimal(Util.ToString(this.dgvInputList[4, currentCell.RowIndex].Value)) * Util.ToDecimal(Util.ToString(this.dgvInputList[6, currentCell.RowIndex].Value)) + num3;
							num3 = d / Util.ToDecimal(Util.ToString(this.dgvInputList[4, currentCell.RowIndex].Value));
							this.dgvInputList[6, currentCell.RowIndex].Value = TaxUtil.CalcFraction(num3, 0, 1);
							num3 = d - Util.ToDecimal(Util.ToString(this.dgvInputList[4, currentCell.RowIndex].Value)) * Util.ToDecimal(Util.ToString(this.dgvInputList[6, currentCell.RowIndex].Value));
						}
						if (Util.ToDecimal(Util.ToString(this.dgvInputList[7, currentCell.RowIndex].Value)) != num3)
						{
							this.dgvInputList[7, currentCell.RowIndex].Value = Util.FormatNum(num3.ToString(), this.CalcInfo.KETA_SURYO2);
							if (!this.IsCalc(currentCell.RowIndex))
							{
								this.dgvInputList[9, currentCell.RowIndex].Value = "";
								this.dgvInputList[10, currentCell.RowIndex].Value = "";
							}
							else if (!this.CellKingkSet(currentCell.RowIndex))
							{
								Msg.Error("金額が大きすぎる為、複数明細へ分けて入力を行って下さい。");
								this.txtGridEdit.Focus();
								return;
							}
							this.DataGridSum();
						}
						if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[8, currentCell.RowIndex];
						}
						if (e.KeyCode == Keys.Right)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[8, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
							return;
						}
						break;
					case 8:
						if (!ValChk.IsDecNumWithinLength(this.txtGridEdit.Text, 12, 3, true))
						{
							this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
							this.txtGridEdit.Focus();
							return;
						}
						if (Util.ToDecimal(this.txtGridEdit.Text) < 0m)
						{
							this.txtGridEdit.Text = string.Empty;
							this.txtGridEdit.Focus();
							return;
						}
						num = Util.ToDecimal(Util.ToString(this.txtGridEdit.Text)) * 100m;
						num = TaxUtil.CalcFraction(num, 0, 1) / 100m;
						if (Util.ToDecimal(Util.ToString(this.dgvInputList[8, currentCell.RowIndex].Value)) != num)
						{
							this.dgvInputList[8, currentCell.RowIndex].Value = Util.FormatNum(num, this.CalcInfo.KETA_URI_TANKA);
							if (!this.IsCalc(currentCell.RowIndex))
							{
								this.dgvInputList[9, currentCell.RowIndex].Value = "";
								this.dgvInputList[10, currentCell.RowIndex].Value = "";
							}
							else if (!this.CellKingkSet(currentCell.RowIndex))
							{
								Msg.Error("金額が大きすぎる為、複数明細へ分けて入力を行って下さい。");
								this.txtGridEdit.Focus();
								return;
							}
							this.DataGridSum();
						}
						if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[9, currentCell.RowIndex];
						}
						if (e.KeyCode == Keys.Right)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[9, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
							return;
						}
						break;
					case 9:
					{
						if (!ValChk.IsDecNumWithinLength(this.txtGridEdit.Text, 15, 0, true))
						{
							this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
							this.txtGridEdit.Focus();
							return;
						}
						bool flag = false;
						if (Util.ToDecimal(Util.ToString(this.dgvInputList[9, currentCell.RowIndex].Value)) != Util.ToDecimal(this.txtGridEdit.Text))
						{
							flag = true;
						}
						this.dgvInputList[9, currentCell.RowIndex].Value = Util.FormatNum(this.txtGridEdit.Text);
						if (!ValChk.IsEmpty(this.dgvInputList[9, currentCell.RowIndex].Value))
						{
							if (!ValChk.IsEmpty(this.dgvInputList[11, currentCell.RowIndex].Value) && Util.ToDecimal(this.dgvInputList[17, currentCell.RowIndex].Value).Equals(1m))
							{
								if (flag)
								{
									this.CellTaxSet(currentCell.RowIndex);
								}
							}
							else
							{
								this.DataGridSum();
								if (this.dgvInputList.RowCount <= currentCell.RowIndex + 1 || !this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
								{
									break;
								}
								if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 1)
								{
									this.dgvInputList.CurrentCell = this.dgvInputList[10, currentCell.RowIndex];
									return;
								}
								this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex + 1];
								return;
							}
						}
						else
						{
							this.dgvInputList[10, currentCell.RowIndex].Value = "";
						}
						this.DataGridSum();
						if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
						{
							if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 1)
							{
								this.dgvInputList.CurrentCell = this.dgvInputList[10, currentCell.RowIndex];
							}
							else
							{
								this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex + 1];
							}
						}
						if (e.KeyCode == Keys.Right)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
							return;
						}
						break;
					}
					case 10:
						if (!ValChk.IsDecNumWithinLength(this.txtGridEdit.Text, 15, 0, true))
						{
							this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
							this.txtGridEdit.Focus();
							return;
						}
						if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 1)
						{
							this.dgvInputList[10, currentCell.RowIndex].Value = Util.FormatNum(this.txtGridEdit.Text);
						}
						if (this.dgvInputList.RowCount > currentCell.RowIndex + 1 && this.GetDenpyoMeisaiKeyEnter(e.KeyCode, currentCell))
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex + 1];
						}
						if (e.KeyCode == Keys.Right)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex];
							this.txtDummy.Focus();
							this.txtGridEdit.Focus();
						}
						this.DataGridSum();
						return;
					default:
						return;
				}
			}
			else if (e.KeyCode == Keys.Left)
			{
				DataGridViewCell currentCell = this.dgvInputList.CurrentCell;
				//				DataGridViewCell currentCell2 = this.dgvInputList.CurrentCell;
				switch (currentCell.ColumnIndex)
				{

					case 2:

						this.dgvInputList.CurrentCell = this.dgvInputList[1, currentCell.RowIndex];

						break;

					case 6:

						if (Util.ToInt(this.dgvInputList[18, currentCell.RowIndex].Value) == 0)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[3, currentCell.RowIndex];
						}
						else
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex];
						}
						break;
					case 7:
						if (Util.ToInt(Util.ToString(this.dgvInputList[4, currentCell.RowIndex].Value)) != 0)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[6, currentCell.RowIndex];
						}
						else if (Util.ToInt(this.dgvInputList[18, currentCell.RowIndex].Value) == 0)
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[3, currentCell.RowIndex];
						}
						else
						{
							this.dgvInputList.CurrentCell = this.dgvInputList[2, currentCell.RowIndex];
						}
						break;
					case 8:
						this.dgvInputList.CurrentCell = this.dgvInputList[7, currentCell.RowIndex];
						break;
					case 9:
						this.dgvInputList.CurrentCell = this.dgvInputList[8, currentCell.RowIndex];
						break;
					case 10:
						this.dgvInputList.CurrentCell = this.dgvInputList[9, currentCell.RowIndex];
						break;
				}

				this.txtDummy.Focus();

				this.txtGridEdit.Focus();

			}
		}

		// Token: 0x06000069 RID: 105 RVA: 0x00002429 File Offset: 0x00000629
		private void KBDE1011_FormClosing(object sender, FormClosingEventArgs e)
		{
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00002450 File Offset: 0x00000650
		private void txtGridEdit_Enter(object sender, EventArgs e)
		{
			base.BeginInvoke(new EventHandler(this.TextBoxSelectAll), new object[]
			{
				sender,
				e
			});
		}

		// Token: 0x0600006E RID: 110 RVA: 0x0002575C File Offset: 0x0002395C
		private bool SetFunanushiInfo()
		{
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("TORIHIKISAKI_NM, TANKA_SHUTOKU_HOHO, KINGAKU_HASU_SHORI, SHOHIZEI_NYURYOKU_HOHO, SHOHIZEI_HASU_SHORI, SHOHIZEI_TENKA_HOHO, SHOHIZEI_TENKA_HOHO_NM, SHOHIZEI_NYURYOKU_HOHO_NM ", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count == 0)
			{
				return true;
			}
			this.lblShohizeiInputHoho2.Text = dataTableByConditionWithParams.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"].ToString() + ":" + dataTableByConditionWithParams.Rows[0]["SHOHIZEI_NYURYOKU_HOHO_NM"].ToString();
			this.lblShohizeiTenka2.Text = dataTableByConditionWithParams.Rows[0]["SHOHIZEI_TENKA_HOHO"].ToString() + ":" + dataTableByConditionWithParams.Rows[0]["SHOHIZEI_TENKA_HOHO_NM"].ToString();
			if (this.CalcInfo.TOKUISAKI_CD != Util.ToInt(this.txtFunanushiCd.Text))
			{
				this.lblFunanushiNm.Text = Util.ToString(dataTableByConditionWithParams.Rows[0]["TORIHIKISAKI_NM"]);
				this.CalcInfo.TANKA_SHUTOKU_HOHO = Util.ToDecimal(dataTableByConditionWithParams.Rows[0]["TANKA_SHUTOKU_HOHO"]);
				this.CalcInfo.KINGAKU_HASU_SHORI = Util.ToDecimal(dataTableByConditionWithParams.Rows[0]["KINGAKU_HASU_SHORI"]);
				this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO = Util.ToDecimal(dataTableByConditionWithParams.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"]);
				this.CalcInfo.SHOHIZEI_HASU_SHORI = Util.ToDecimal(dataTableByConditionWithParams.Rows[0]["SHOHIZEI_HASU_SHORI"]);
				this.CalcInfo.SHOHIZEI_TENKA_HOHO = Util.ToDecimal(dataTableByConditionWithParams.Rows[0]["SHOHIZEI_TENKA_HOHO"]);
				this.CalcInfo.SHOHIN_SHOHIZEI_KUBUN = "1";
				if (Util.ToDecimal(dataTableByConditionWithParams.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"]) ==3)
				{
					this.CalcInfo.SHOHIN_SHOHIZEI_KUBUN = "0";
				}
				else
				{

				}
				if (this.SHIFT_CATEGORY_SLIP_TOTAL != 0 && this.SHIFT_CATEGORY_SLIP_TOTAL >= 1 && this.SHIFT_CATEGORY_SLIP_TOTAL <= 2)
				{
					this.CalcInfo.SHOHIZEI_TENKA_HOHO = this.SHIFT_CATEGORY_SLIP_TOTAL;
					this.SetTaxShiftCategory((int)this.CalcInfo.SHOHIZEI_TENKA_HOHO);
				}
				if (this.ROUND_CATEGORY_DOWN != 0 && this.ROUND_CATEGORY_DOWN >= 1 && this.ROUND_CATEGORY_DOWN <= 3)
				{
					this.CalcInfo.SHOHIZEI_HASU_SHORI = this.ROUND_CATEGORY_DOWN;
				}
				this.CalcInfo.TOKUISAKI_CD = Util.ToInt(this.txtFunanushiCd.Text);
				this.DataGridSum();
			}
			return false;
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00025A44 File Offset: 0x00023C44
		private void InitDispOnNew()
		{

			this.isChanged = false;

			this.MODE_EDIT = 1;
			this.lblMode.Text = "【登録】";
			this.MEISAI_DENPYO_BANGO = 0m;
			this.CalcInfo.Clear();
			this.CalcInfo.DENPYO_DATE = DateTime.Now.ToString("yyyy/MM/dd");
			this.txtDenpyoNo.Text = "";
			this.txtDenpyoNo.Focus();
			this.txtMizuageShishoCd.Text = base.UInfo.ShishoCd;
			this.lblMizuageShishoNm.Text = base.UInfo.ShishoNm;

			fsiDateDay.ExecBtn = this.btnF1;
			fsiDateDay.OpenDialogExe = "CMCM1021";
			fsiDateDay.DbConnect = this.Dba;

			if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
			{
				DataRow personInfo = this.GetPersonInfo(base.UInfo.UserCd);
				if (personInfo != null)
				{
					base.UInfo.ShishoCd = personInfo["SHISHO_CD"].ToString();
					base.UInfo.ShishoNm = base.Dba.GetName(base.UInfo, "TB_CM_SHISHO", base.UInfo.ShishoCd, base.UInfo.ShishoCd);
					this.txtMizuageShishoCd.Text = base.UInfo.ShishoCd;
					this.lblMizuageShishoNm.Text = base.UInfo.ShishoNm;
				}
			}
			this.txtMizuageShishoCd.Enabled = false;
			//if (ValChk.IsEmpty(this.txtGengoYear.Text) && ValChk.IsEmpty(this.txtMonth.Text) && ValChk.IsEmpty(this.txtDay.Text))
			//{
			string[] array = Util.ConvJpDate(DateTime.Now, base.Dba);
			this.fsiDateDay.JpDate = array;
			//	//this.lblGengo.Text = array[0];
			//	//this.txtGengoYear.Text = array[2];
			//	//this.txtMonth.Text = array[3];
			//	//this.txtDay.Text = array[4];
			//	//this.CalcInfo.DENPYO_DATE = DateTime.Now.ToString("yyyy/MM/dd");
			//}
			if (ValChk.IsEmpty(this.txtTorihikiKubunCd.Text))
			{
				this.txtTorihikiKubunCd.Text = "11";
				this.lblTorihikiKubunNm.Text = base.Dba.GetName(base.UInfo, "TB_HN_F_TORIHIKI_KUBUN1", this.txtMizuageShishoCd.Text, this.txtTorihikiKubunCd.Text);
			}
			this.txtFunanushiCd.Text = "";
			this.lblFunanushiNm.Text = "";
			this.txtTantoshaCd.Text = base.UInfo.UserCd;
			this.lblTantoshaNm.Text = base.Dba.GetName(base.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);
			if (!ValChk.IsEmpty(this.txtTantoshaCd.Text))
			{
				this.SetBumonInfo(Util.ToInt(this.txtTantoshaCd.Text));
			}
			this.btnF3.Enabled = false;
			this.dgvInputList.AlternatingRowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#BBFFFF");
			this.dgvInputList.DefaultCellStyle.SelectionBackColor = Color.Transparent;
			foreach (object obj in this.dgvInputList.Columns)
			{
				((DataGridViewColumn)obj).SortMode = DataGridViewColumnSortMode.NotSortable;
			}
			this.dgvInputList.ColumnHeadersDefaultCellStyle.Font = this.txtGridEdit.Font;
			this.dgvInputList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvInputList.DefaultCellStyle.Font = this.txtGridEdit.Font;
			this.dgvInputList.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvInputList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
			this.dgvInputList.AllowUserToAddRows = false;
			this.dgvInputList.EditMode = DataGridViewEditMode.EditProgrammatically;
			this.dgvInputList.Rows.Clear();
			this.dgvInputList.RowCount = 1;
			this.dgvInputList[0, 0].Value = "1";
			this.dgvInputList.CurrentCell = this.dgvInputList[0, 0];
			this.dgvInputList.CurrentCell = null;
			this.lblTaxInfo.Text = string.Empty;
			this.lblShohizeiInputHoho2.Text = "";
			this.lblShohizeiTenka2.Text = "";
			this.txtSyokei.Text = "0";
			this.txtSyohiZei.Text = "0";
			this.txtTotalGaku.Text = "0";
			this.txtGridEdit.Visible = false;
			this._itemStock = new Dictionary<string, decimal>();
			this._torihikiKubun2 = 1;
			this.IS_INPUT = 1;
			this.OrgnDialogResult = false;

			this._lastUpdCheck = false;
		}
		//消費税額取得用の配列
		decimal[] hikazei_zeigk = new decimal[0];
		decimal[] keigen_zeigk = new decimal[0];
		decimal[] kazei_zeigk = new decimal[0];

		// Token: 0x06000070 RID: 112 RVA: 0x00025F24 File Offset: 0x00024124
		private void InitDispOnEdit(DataTable dtDENPYO)
		{
			this.MODE_EDIT = 2;
			this.lblMode.Text = "【修正】";
			this.MEISAI_DENPYO_BANGO = Util.ToDecimal(dtDENPYO.Rows[0]["DENPYO_BANGO"]);
			this.CalcInfo.DENPYO_DATE = Util.ToDate(dtDENPYO.Rows[0]["DENPYO_DATE"]).ToString("yyyy/MM/dd");
			// TODO 2020/02/12 Upd 日付フィールド
			string[] array = Util.ConvJpDate(DateTime.Parse(dtDENPYO.Rows[0]["DENPYO_DATE"].ToString()), base.Dba);
			this.fsiDateDay.JpDate = array;
			
			//this.lblGengo.Text = array[0];
			//this.txtGengoYear.Text = array[2];
			//this.txtMonth.Text = array[3];
			//this.txtDay.Text = array[4];
			this.txtTorihikiKubunCd.Text = dtDENPYO.Rows[0]["TORIHIKI_KUBUN1"].ToString() + dtDENPYO.Rows[0]["TORIHIKI_KUBUN2"].ToString();
			this.lblTorihikiKubunNm.Text = base.Dba.GetName(base.UInfo, "TB_HN_F_TORIHIKI_KUBUN1", this.txtMizuageShishoCd.Text, this.txtTorihikiKubunCd.Text);
			this.txtFunanushiCd.Text = dtDENPYO.Rows[0]["TOKUISAKI_CD"].ToString();
			this.SetFunanushiInfo();
			this.lblFunanushiNm.Text = dtDENPYO.Rows[0]["TOKUISAKI_NM"].ToString();
			this.txtMizuageShishoCd.Text = dtDENPYO.Rows[0]["SHISHO_CD"].ToString();
			this.lblMizuageShishoNm.Text = base.Dba.GetName(base.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
			this.txtTantoshaCd.Text = dtDENPYO.Rows[0]["TANTOSHA_CD"].ToString();
			this.lblTantoshaNm.Text = base.Dba.GetName(base.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);
			this.SetBumonInfo(Util.ToInt(this.txtTantoshaCd.Text));
			this.btnF3.Enabled = true;
			this.txtSyokei.Text = Util.FormatNum(dtDENPYO.Rows[0]["URIAGE_KINGAKU"]);
			this.txtSyohiZei.Text = Util.FormatNum(dtDENPYO.Rows[0]["SHOHIZEIGAKU"]);
			this.txtTotalGaku.Text = Util.FormatNum(dtDENPYO.Rows[0]["NYUKIN_GOKEIGAKU"]);
			this.txtGridEdit.Visible = false;
			this._itemStock = new Dictionary<string, decimal>();
			this._torihikiKubun2 = Util.ToInt(dtDENPYO.Rows[0]["TORIHIKI_KUBUN2"].ToString());
			this.dgvInputList.Rows.Clear();
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(dtDENPYO.Rows[0]["DENPYO_BANGO"]));
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams(
				@"M.GYO_BANGO ,
                  H.NM AS KUBUN_NM ,
                  M.SHOHIN_CD ,
                  M.SHOHIN_NM ,
                  M.IRISU ,
                  M.TANI ,
                  M.SURYO1 ,
                  M.SURYO2 ,
                  M.GEN_TANKA ,
                  M.URI_TANKA ,
                  M.BAIKA_KINGAKU ,
                  M.SHOHIZEI ,
                  M.SHIWAKE_CD ,
                  M.BUMON_CD ,
                  M.ZEI_KUBUN ,
                  M.JIGYO_KUBUN ,
                  Z.KAZEI_KUBUN ,
                  M.ZEI_RITSU ,
                  M.SHOHIZEI_NYURYOKU_HOHO ,
                  M.ZAIKO_KANRI_KUBUN ,
                  M.KUBUN,
                  (CASE WHEN M.ZEI_KUBUN = 50 THEN '*' ELSE '' END) KEI_FLG ", 
				@"VI_HN_TORIHIKI_MEISAI AS M 
                  LEFT JOIN TB_ZM_F_ZEI_KUBUN AS Z 
                  ON M.ZEI_KUBUN = Z.ZEI_KUBUN LEFT 
                  JOIN VI_HN_HANBAI_KUBUN_NM AS H 
                  ON M.KAISHA_CD = H.KAISHA_CD 
                  AND H.SHUBETSU_CD = 2 
                  AND H.KUBUN_SHUBETSU = 1 
                  AND H.KUBUN = M.KUBUN ", 
				@"M.KAISHA_CD = @KAISHA_CD 
                  AND M.SHISHO_CD = @SHISHO_CD 
                  AND M.KAIKEI_NENDO = @KAIKEI_NENDO 
                  AND M.DENPYO_KUBUN = @DENPYO_KUBUN 
                  AND M.DENPYO_BANGO = @DENPYO_BANGO", 
				dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count > 0)
			{
				this.dgvInputList.RowCount = dataTableByConditionWithParams.Rows.Count + 1;
				int num = 0;
				while (dataTableByConditionWithParams.Rows.Count > num)
				{
					this.dgvInputList[0, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["GYO_BANGO"]);
					this.dgvInputList[1, num].Value = ((Util.ToDecimal(dataTableByConditionWithParams.Rows[num]["KUBUN"]) == 0m) ? "" : Util.ToString(dataTableByConditionWithParams.Rows[num]["KUBUN"]));
					this.dgvInputList[2, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["SHOHIN_CD"]);
					this.dgvInputList[3, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["SHOHIN_NM"]);
					this.dgvInputList[4, num].Value = ((Util.ToDecimal(dataTableByConditionWithParams.Rows[num]["IRISU"]) == 0m) ? "" : dataTableByConditionWithParams.Rows[num]["IRISU"]);
					this.dgvInputList[5, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["TANI"]);
					this.dgvInputList[6, num].Value = Util.FormatNum(dataTableByConditionWithParams.Rows[num]["SURYO1"]);
					this.dgvInputList[7, num].Value = Util.FormatNum(dataTableByConditionWithParams.Rows[num]["SURYO2"], this.CalcInfo.KETA_SURYO2);
					this.dgvInputList[8, num].Value = Util.FormatNum(dataTableByConditionWithParams.Rows[num]["URI_TANKA"], this.CalcInfo.KETA_URI_TANKA);
					this.dgvInputList[9, num].Value = Util.FormatNum(dataTableByConditionWithParams.Rows[num]["BAIKA_KINGAKU"]);
					this.dgvInputList[10, num].Value = Util.FormatNum(dataTableByConditionWithParams.Rows[num]["SHOHIZEI"]);
					this.dgvInputList[12, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["SHIWAKE_CD"]);
					this.dgvInputList[13, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["BUMON_CD"]);
					this.dgvInputList[14, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["ZEI_KUBUN"]);
					this.dgvInputList[15, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["JIGYO_KUBUN"]);
					this.dgvInputList[11, num].Value = Util.FormatNum(Util.ToDecimal(Util.ToString(dataTableByConditionWithParams.Rows[num]["ZEI_RITSU"])), 1);
					this.dgvInputList[16, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["GEN_TANKA"]);
					this.dgvInputList[17, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["KAZEI_KUBUN"]);
					this.dgvInputList[19, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["KEI_FLG"]);

					//消費税の取得
					switch (Util.ToInt(dataTableByConditionWithParams.Rows[num]["ZEI_KUBUN"])) //税区分の値で判断
					{
						case 0: //0の場合、非課税
							Array.Resize(ref hikazei_zeigk, num + 1);
							hikazei_zeigk[num] = Util.ToDecimal(Util.FormatNum(dataTableByConditionWithParams.Rows[num]["SHOHIZEI"])); //消費税額の合算
							break;

						case 50: //50の場合、軽減税率対象
							Array.Resize(ref keigen_zeigk, num + 1);
							keigen_zeigk[num] = Util.ToDecimal(Util.FormatNum(dataTableByConditionWithParams.Rows[num]["SHOHIZEI"])); //消費税額の合算
							break;

						default: //上記以外の場合
							Array.Resize(ref kazei_zeigk, num + 1);
							kazei_zeigk[num] = Util.ToDecimal(Util.FormatNum(dataTableByConditionWithParams.Rows[num]["SHOHIZEI"])); //消費税額の合算
							break;
					}

					if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) != 1)
					{
						this.dgvInputList[10, num].Value = 0;
					}
					if (Util.ToInt(this.dgvInputList[14, num].Value) == 0)
					{
						this.dgvInputList[10, num].Value = 0;
					}

					if (num == 0)
					{
						if (this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO != Util.ToDecimal(dataTableByConditionWithParams.Rows[num]["SHOHIZEI_NYURYOKU_HOHO"]))
						{
							this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO = Util.ToDecimal(dataTableByConditionWithParams.Rows[num]["SHOHIZEI_NYURYOKU_HOHO"]);
							this.SetTaxInputCategory(Util.ToInt(Util.ToString(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO)));
						}
						// TODO 新規登録の場合と呼び出しの場合にセットしている値が違うのでは？？？？？
						this.CalcInfo.SHOHIN_SHOHIZEI_KUBUN = ((Util.ToDecimal(dataTableByConditionWithParams.Rows[num]["SHOHIZEI_NYURYOKU_HOHO"]) == 3m) ? "0" : "1");
					}
					this.dgvInputList[18, num].Value = Util.ToString(dataTableByConditionWithParams.Rows[num]["ZAIKO_KANRI_KUBUN"]);
					if (Util.ToInt(this.dgvInputList[17, num].Value) != 1)
					{
						this.dgvInputList[10, num].Value = 0;
						this.dgvInputList[11, num].Value = string.Empty;
					}
					if (Util.ToString(this.dgvInputList[18, num].Value) == "1")
					{
						string text = Util.ToString(this.dgvInputList[2, num].Value);
						decimal num2 = Util.ToDecimal(Util.ToString(this.dgvInputList[6, num].Value)) * Util.ToDecimal(Util.ToString(this.dgvInputList[4, num].Value)) + Util.ToDecimal(Util.ToString(this.dgvInputList[7, num].Value));
						if (!this._itemStock.ContainsKey(text))
						{
							this._itemStock.Add(text, num2);
						}
						else
						{
							Dictionary<string, decimal> itemStock = this._itemStock;
							string key = text;
							itemStock[key] += num2;
						}
					}
					num++;
				}
				this.dgvInputList[0, this.dgvInputList.Rows.Count - 1].Value = this.dgvInputList.Rows.Count;

				this.SetTaxInfo();

				// TODO 2020/02/13 Add Mutu 
				// カレント設定
				this.dgvInputList.CurrentCell = this.dgvInputList[2, this.dgvInputList.Rows.Count - 1];


			}
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00026A14 File Offset: 0x00024C14
		private void DataGridSum()
		{
			decimal[] array = new decimal[3];
			decimal num = 0m;
			int num2 = 0;
			List<KBDE1011.MeisaiTbl> list = new List<KBDE1011.MeisaiTbl>();
			int num3 = 0;
			while (this.dgvInputList.RowCount > num3)
			{
				if (!ValChk.IsEmpty(Util.ToString(this.dgvInputList[14, num3].Value)))
				{
					if (Util.ToInt(Util.ToString(this.dgvInputList[14, num3].Value)) == 0)
					{
						array[0] += Util.ToDecimal(Util.ToString(this.dgvInputList[9, num3].Value));
					}
					else
					{
						switch (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO))
						{
						case 1:
						case 3:
						{
							int num4 = Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO);
							if (num4 != 2)
							{
								if (num4 != 3)
								{
									array[1] += Util.ToDecimal(Util.ToString(this.dgvInputList[9, num3].Value));
									array[2] += Util.ToDecimal(Util.ToString(this.dgvInputList[10, num3].Value));
								}
								else
								{
									array[1] += Util.ToDecimal(Util.ToString(this.dgvInputList[9, num3].Value)) - Util.ToDecimal(Util.ToString(this.dgvInputList[10, num3].Value));
									array[2] += Util.ToDecimal(Util.ToString(this.dgvInputList[10, num3].Value));
								}
							}
							else
							{
								array[1] += Util.ToDecimal(Util.ToString(this.dgvInputList[9, num3].Value));
								array[2] += Util.ToDecimal(Util.ToString(this.dgvInputList[10, num3].Value));
							}
							break;
						}
						case 2:
						{
							bool flag = false;
							if (num2 != 0)
							{
								for (int i = 0; i < list.Count; i++)
								{
									if (list[i].ZeiRt == Util.ToDecimal(Util.ToString(this.dgvInputList[11, num3].Value)))
									{
										list[i].Kingk += Util.ToDecimal(Util.ToString(this.dgvInputList[9, num3].Value));
										flag = true;
										break;
									}
								}
							}
							if (!flag)
							{
								// TODO 2020/02/13 Add Mutu Asato
								list.Add(new KBDE1011.MeisaiTbl
								{
									ZeiRt = Util.ToDecimal(Util.ToString(this.dgvInputList[11, num3].Value)),
									Kingk = Util.ToDecimal(Util.ToString(this.dgvInputList[9, num3].Value)),
									Zeikbn = Util.ToDecimal(Util.ToString(this.dgvInputList[14, num3].Value))
								});
								num2++;
							}
							break;
						}
						}
					}
				}
				num3++;
			}
			if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 2 && num2 > 0)
			{
				for (int j = 0; j < list.Count; j++)
				{
					array[1] += list[j].Kingk;
					if (list[j].ZeiRt != 0m && list[j].Kingk != 0m)
					{
						int num4 = Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO);
						if (num4 != 2)
						{
							if (num4 == 3)
							{
								num = list[j].Kingk / (100m + list[j].ZeiRt) * list[j].ZeiRt;
								array[1] = array[1] - TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.SHOHIZEI_HASU_SHORI));
								array[2] += TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.SHOHIZEI_HASU_SHORI));
							}
						}
						else
						{
							num = list[j].Kingk * list[j].ZeiRt / 100m;
							array[2] += TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.SHOHIZEI_HASU_SHORI));
						}
					}
				}
			}
			this.txtSyokei.Text = Util.FormatNum(array[0] + array[1]);
			this.txtSyohiZei.Text = Util.FormatNum(array[2]);
			this.txtTotalGaku.Text = Util.FormatNum(array[0] + array[1] + array[2]);
			this.SetTaxInfo();
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00026FD8 File Offset: 0x000251D8
		private void SetZaiko()
		{
			if (this._zaikoDisplay != 0)
			{
				try
				{
					this.zaikoClear();
					if (this.dgvInputList.CurrentCell != null)
					{
						DataGridViewCell currentCell = this.dgvInputList.CurrentCell;
						if (this.dgvInputList[2, currentCell.RowIndex].Value != null && Util.ToString(this.dgvInputList[18, currentCell.RowIndex].Value) == "1")
						{
							string text = Util.ToString(this.dgvInputList[2, currentCell.RowIndex].Value);
							DbParamCollection dbParamCollection = new DbParamCollection();
							dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
							dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
							dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(text));
							DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "VI_HN_SHOHIN_ZAIKO", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD", dbParamCollection);
							if (dataTableByConditionWithParams.Rows.Count == 0)
							{
								if (Util.ToDecimal(this.dgvInputList[2, currentCell.RowIndex].Value) != 0m)
								{
									this.lblShohinNm.Text = Util.ToString(this.dgvInputList[3, currentCell.RowIndex].Value);
									this.lblZaikoSu.Text = "";
								}
							}
							else if (Util.ToDecimal(this.dgvInputList[2, currentCell.RowIndex].Value) != 0m)
							{
								decimal suryo = this.getSuryo(currentCell.RowIndex, text);
								this.lblShohinNm.Text = "在庫数：" + Util.ToString(this.dgvInputList[3, currentCell.RowIndex].Value);
								decimal d = Util.ToDecimal(Util.ToString(dataTableByConditionWithParams.Rows[0]["IRISU"])) * Util.ToDecimal(Util.ToString(dataTableByConditionWithParams.Rows[0]["SURYO1"])) + Util.ToDecimal(Util.ToString(dataTableByConditionWithParams.Rows[0]["SURYO2"]));
								if (this._itemStock.ContainsKey(text))
								{
									if (this._torihikiKubun2 == 2)
									{
										d -= this._itemStock[text];
									}
									else
									{
										d += this._itemStock[text];
									}
								}
								decimal num = 0m;
								if (this.txtTorihikiKubunCd.Text.EndsWith("2"))
								{
									num = d + suryo;
								}
								else
								{
									num = d - suryo;
								}
								this.lblZaikoSu.Text = Util.FormatNum(num, 2);
								if (num < 0m)
								{
									this.lblZaikoSu.ForeColor = Color.Red;
								}
								else
								{
									this.lblZaikoSu.ForeColor = Color.Black;
								}
							}
						}
					}
				}
				catch (Exception)
				{
				}
			}
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00027320 File Offset: 0x00025520
		private decimal getSuryo(int Row, string hinCode)
		{
			decimal d = 0m;
			decimal num = 0m;
			int num2 = 0;
			try
			{
				while (this.dgvInputList.RowCount > num2)
				{
					if (Util.ToString(this.dgvInputList[2, num2].Value) == hinCode)
					{
						d = Util.ToDecimal(Util.ToString(this.dgvInputList[6, num2].Value)) * Util.ToDecimal(Util.ToString(this.dgvInputList[4, num2].Value));
						num += d + Util.ToDecimal(Util.ToString(this.dgvInputList[7, num2].Value));
					}
					num2++;
				}
			}
			catch (Exception)
			{
			}
			return num;
		}

		// Token: 0x06000074 RID: 116 RVA: 0x000024B2 File Offset: 0x000006B2
		private void zaikoClear()
		{
			if (this._zaikoDisplay != 0)
			{
				this.lblShohinNm.Text = "";
				this.lblZaikoSu.Text = "";
			}
		}

		// Token: 0x06000075 RID: 117 RVA: 0x000273F8 File Offset: 0x000255F8
		private bool ValidateAll()
		{
			if (this.dgvInputList.CurrentCell != null)
			{
				this.dgvInputList.CurrentCell.Selected = false;
			}
			if (!this.isValidDenpyoNo())
			{
				this.txtDenpyoNo.SelectAll();
				this.txtDenpyoNo.Focus();
				return false;
			}
			if (!IsValid.IsYear(this.txtGengoYear.Text, this.txtGengoYear.MaxLength))
			{
				this.txtGengoYear.Focus();
				this.txtGengoYear.SelectAll();
				return false;
			}
			if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
			{
				this.txtMonth.Focus();
				this.txtMonth.SelectAll();
				return false;
			}
			if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
			{
				this.txtDay.Focus();
				this.txtDay.SelectAll();
				return false;
			}
			//this.CheckJp();
			//this.SetJp();
			if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !this.IsValidMizuageShishoCd())
			{
				this.txtMizuageShishoCd.Focus();
				this.txtMizuageShishoCd.SelectAll();
				return false;
			}
			if (!this.isValidTorihikiKubunCd())
			{
				this.txtTorihikiKubunCd.SelectAll();
				this.txtTorihikiKubunCd.Focus();
				return false;
			}
			if (!this.isValidFunanushiCd())
			{
				this.txtFunanushiCd.SelectAll();
				this.txtFunanushiCd.Focus();
				return false;
			}
			if (!this.isValidTantoshaCd())
			{
				this.txtTantoshaCd.SelectAll();
				this.txtTantoshaCd.Focus();
				return false;
			}
			if (this.dgvInputList.Rows.Count == 0)
			{
				Msg.Notice("明細が入力されていません。");
				this.txtFunanushiCd.Focus();
				this.txtFunanushiCd.SelectAll();
				return false;
			}
			if (!ValChk.IsDecNumWithinLength(this.txtSyokei.Text, 9, 0, true))
			{
				Msg.Error("金額が大きすぎる為、伝票を分けて入力を行って下さい。");
				this.txtGridEdit.Focus();
				return false;
			}
			if (!ValChk.IsDecNumWithinLength(this.txtSyohiZei.Text, 7, 0, true))
			{
				Msg.Error("金額が大きすぎる為、伝票を分けて入力を行って下さい。");
				this.txtGridEdit.Focus();
				return false;
			}
			int num = 0;
			while (this.dgvInputList.RowCount > num)
			{
				if (!ValChk.IsEmpty(this.dgvInputList[1, num].Value))
				{
					DbParamCollection dbParamCollection = new DbParamCollection();
					dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
					dbParamCollection.SetParam("@KUBUN", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[1, num].Value));
					if (base.Dba.GetDataTableByConditionWithParams("*", "VI_HN_HANBAI_KUBUN_NM", "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 1 AND KUBUN = @KUBUN", dbParamCollection).Rows.Count == 0 && this.dgvInputList.RowCount > num + 1)
					{
						Msg.Error("入力に誤りがあります。");
						this.dgvInputList.CurrentCell = this.dgvInputList[1, num];
						this.txtGridEdit.SelectAll();
						return false;
					}
				}
				if (ValChk.IsEmpty(this.dgvInputList[2, num].Value))
				{
					if (this.dgvInputList.RowCount > num + 1)
					{
						Msg.Error("入力に誤りがあります。");
						this.dgvInputList.CurrentCell = this.dgvInputList[2, num];
						this.txtGridEdit.SelectAll();
						return false;
					}
				}
				else
				{
					DbParamCollection dbParamCollection = new DbParamCollection();
					dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
					dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
					dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(this.dgvInputList[2, num].Value));
					if (base.Dba.GetDataTableByConditionWithParams("*", "VI_HN_SHOHIN", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_KUBUN5 <> 1 AND SHOHIN_CD = @SHOHIN_CD", dbParamCollection).Rows.Count == 0)
					{
						if (this.dgvInputList.RowCount > num + 1)
						{
							Msg.Error("入力に誤りがあります。");
							this.dgvInputList.CurrentCell = this.dgvInputList[2, num];
							this.txtGridEdit.SelectAll();
							return false;
						}
						dbParamCollection = new DbParamCollection();
						dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
						dbParamCollection.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[13, num].Value));
						if (base.Dba.GetDataTableByConditionWithParams("*", "VI_ZM_BUMON", "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD", dbParamCollection).Rows.Count == 0 && this.dgvInputList.RowCount > num + 1)
						{
							Msg.Error("入力に誤りがあります。");
							this.dgvInputList.CurrentCell = this.dgvInputList[2, num];
							this.txtGridEdit.SelectAll();
							return false;
						}
					}
				}
				num++;
			}
			if (Util.ToDecimal(this.txtTotalGaku.Text) == 0m)
			{
				Msg.Error("精算できません。金額が0です。");
				this.txtGridEdit.Focus();
				return false;
			}
			return true;
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00027910 File Offset: 0x00025B10
		private decimal UpdateData()
		{
			//DateTime dateTime = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text, this.txtMonth.Text, this.txtDay.Text, base.Dba);
			DateTime dateTime = Util.ConvAdDate(this.fsiDateDay.Gengo, this.fsiDateDay.WYear, this.fsiDateDay.Month, this.fsiDateDay.Day, base.Dba);
			decimal num = Util.ToDecimal(this.txtDenpyoNo.Text);
			List<KBDE1011.MeisaiTbl> list = new List<KBDE1011.MeisaiTbl>();
			this.TaxSorting(ref list);
			this.SetTaxInfo();
			try
			{
				base.Dba.BeginTransaction();
				DbParamCollection dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
				dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
				dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
				dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
				dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, num);
				DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("COUNT(*) AS CNT", "TB_HN_TORIHIKI_DENPYO", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO", dbParamCollection);
				if (Util.ToLong(dataTableByConditionWithParams.Rows[0]["CNT"]) == 0L)
				{
					num = Util.ToDecimal(base.Dba.GetHNDenpyoNo(base.UInfo, Util.ToDecimal(Util.ToString(this.txtMizuageShishoCd.Text)), 1, 0));
					if (base.Dba.UpdateHNDenpyoNo(base.UInfo, Util.ToDecimal(Util.ToString(this.txtMizuageShishoCd.Text)), 1, 0, Util.ToInt(num)) != 1)
					{
						Msg.Error("伝票番号の発番に失敗しました。");
						this.txtDenpyoNo.SelectAll();
						return 0m;
					}
				}
				this.ZaikoKousin(num, 3);
				DbParamCollection dbParamCollection2 = new DbParamCollection();
				dbParamCollection2.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
				dbParamCollection2.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
				dbParamCollection2.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, base.UInfo.KaikeiNendo);
				dbParamCollection2.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
				dbParamCollection2.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, num);
				base.Dba.Delete("TB_HN_TORIHIKI_MEISAI", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO", dbParamCollection2);
				dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
				dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
				dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, base.UInfo.KaikeiNendo);
				dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
				dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, num);
				dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("COUNT(*) AS CNT", "TB_HN_TORIHIKI_DENPYO", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO", dbParamCollection);
				if ((int)dataTableByConditionWithParams.Rows[0]["CNT"] == 0)
				{
					DbParamCollection dbParamCollection3 = new DbParamCollection();
					dbParamCollection3.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
					dbParamCollection3.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
					dbParamCollection3.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, base.UInfo.KaikeiNendo);
					dbParamCollection3.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
					dbParamCollection3.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, num);
					dbParamCollection3.SetParam("@DENPYO_DATE", SqlDbType.DateTime, dateTime);
					dbParamCollection3.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 3, Util.ToDecimal(this.txtTorihikiKubunCd.Text.Substring(0, 1)));
					dbParamCollection3.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 3, Util.ToDecimal(this.txtTorihikiKubunCd.Text.Substring(1, 1)));
					dbParamCollection3.SetParam("@TOKUISAKI_CD", SqlDbType.VarChar, 4, this.txtFunanushiCd.Text);
					dbParamCollection3.SetParam("@TOKUISAKI_NM", SqlDbType.VarChar, 40, this.lblFunanushiNm.Text);
					dbParamCollection3.SetParam("@SEIKYUSAKI_CD", SqlDbType.VarChar, 4, this.txtFunanushiCd.Text);
					dbParamCollection3.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
					dbParamCollection3.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 8, 0);
					dbParamCollection3.SetParam("@URIAGE_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyokei.Text));
					dbParamCollection3.SetParam("@SHOHIZEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyohiZei.Text));
					dbParamCollection3.SetParam("@NYUKIN_GOKEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalGaku.Text));
					dbParamCollection3.SetParam("@TOROKU_JIKAN", SqlDbType.DateTime, DateTime.Now);
					dbParamCollection3.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
					base.Dba.Insert("TB_HN_TORIHIKI_DENPYO", dbParamCollection3);
				}
				else
				{
					DbParamCollection dbParamCollection3 = new DbParamCollection();
					dbParamCollection2 = new DbParamCollection();
					dbParamCollection3.SetParam("@DENPYO_DATE", SqlDbType.DateTime, dateTime);
					dbParamCollection3.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 3, Util.ToDecimal(this.txtTorihikiKubunCd.Text.Substring(0, 1)));
					dbParamCollection3.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 3, Util.ToDecimal(this.txtTorihikiKubunCd.Text.Substring(1, 1)));
					dbParamCollection3.SetParam("@TOKUISAKI_CD", SqlDbType.VarChar, 4, this.txtFunanushiCd.Text);
					dbParamCollection3.SetParam("@TOKUISAKI_NM", SqlDbType.VarChar, 40, this.lblFunanushiNm.Text);
					dbParamCollection3.SetParam("@SEIKYUSAKI_CD", SqlDbType.VarChar, 4, this.txtFunanushiCd.Text);
					dbParamCollection3.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
					dbParamCollection3.SetParam("@URIAGE_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyokei.Text));
					dbParamCollection3.SetParam("@SHOHIZEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyohiZei.Text));
					dbParamCollection3.SetParam("@NYUKIN_GOKEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalGaku.Text));
					dbParamCollection3.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
					dbParamCollection2.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
					dbParamCollection2.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
					dbParamCollection2.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, base.UInfo.KaikeiNendo);
					dbParamCollection2.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
					dbParamCollection2.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, num);
					base.Dba.Update("TB_HN_TORIHIKI_DENPYO", dbParamCollection3, "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO", dbParamCollection2);
				}
				int num2 = 0;
				while (this.dgvInputList.RowCount - 1 > num2)
				{
					dbParamCollection = new DbParamCollection();
					dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
					dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
					dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, base.UInfo.KaikeiNendo);
					dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
					dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, num);
					dbParamCollection.SetParam("@GYO_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[0, num2].Value));
					dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("COUNT(*) AS CNT", "TB_HN_TORIHIKI_MEISAI", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO AND GYO_BANGO = @GYO_BANGO", dbParamCollection);
					if ((int)dataTableByConditionWithParams.Rows[0]["CNT"] == 0)
					{
						DbParamCollection dbParamCollection3 = new DbParamCollection();
						dbParamCollection3.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
						dbParamCollection3.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
						dbParamCollection3.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, base.UInfo.KaikeiNendo);
						dbParamCollection3.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
						dbParamCollection3.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, num);
						dbParamCollection3.SetParam("@GYO_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[0, num2].Value));
						dbParamCollection3.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(this.dgvInputList[2, num2].Value));
						dbParamCollection3.SetParam("@SHOHIN_NM", SqlDbType.VarChar, 40, Util.ToString(this.dgvInputList[3, num2].Value));
						dbParamCollection3.SetParam("@IRISU", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[4, num2].Value)));
						dbParamCollection3.SetParam("@TANI", SqlDbType.VarChar, 4, Util.ToString(this.dgvInputList[5, num2].Value));
						dbParamCollection3.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[6, num2].Value)));
						dbParamCollection3.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[7, num2].Value)));
						dbParamCollection3.SetParam("@KUBUN", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[1, num2].Value)));
						dbParamCollection3.SetParam("@GEN_TANKA", SqlDbType.Decimal, 13, list[num2].Gentan);
						dbParamCollection3.SetParam("@GENKA_KINGAKU", SqlDbType.Decimal, 14, list[num2].Genka);
						dbParamCollection3.SetParam("@URI_TANKA", SqlDbType.Decimal, 13, Util.ToDecimal(Util.ToString(this.dgvInputList[8, num2].Value)));
						dbParamCollection3.SetParam("@BAIKA_KINGAKU", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[9, num2].Value)));
						dbParamCollection3.SetParam("@HANBAI_TANKA", SqlDbType.Decimal, 13, 0);
						dbParamCollection3.SetParam("@HANBAI_KINGAKU", SqlDbType.Decimal, 14, 0);
						dbParamCollection3.SetParam("@SHOHIZEI", SqlDbType.Decimal, 14, list[num2].Zeigk);
						dbParamCollection3.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 3, this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO);
						dbParamCollection3.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[12, num2].Value)));
						dbParamCollection3.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[13, num2].Value)));
						dbParamCollection3.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, Util.ToDecimal(Util.ToString(this.dgvInputList[14, num2].Value)));
						dbParamCollection3.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 3, Util.ToDecimal(Util.ToString(this.dgvInputList[15, num2].Value)));
						dbParamCollection3.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[11, num2].Value)));
						dbParamCollection3.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
						base.Dba.Insert("TB_HN_TORIHIKI_MEISAI", dbParamCollection3);
					}
					dbParamCollection = new DbParamCollection();
					dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
					dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
					dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
					dbParamCollection.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, 0);
					dbParamCollection.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
					dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToDecimal(this.dgvInputList[2, num2].Value));
					dbParamCollection.SetParam("@TANKA", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(this.dgvInputList[8, num2].Value)));
					dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "TB_HN_URIAGE_TANKA_RIREKI", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND TANKA = @TANKA", dbParamCollection);
					if (dataTableByConditionWithParams.Rows.Count == 0)
					{
						DbParamCollection dbParamCollection3 = new DbParamCollection();
						dbParamCollection3.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
						dbParamCollection3.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
						dbParamCollection3.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
						dbParamCollection3.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, 0);
						dbParamCollection3.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
						dbParamCollection3.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToDecimal(this.dgvInputList[2, num2].Value));
						dbParamCollection3.SetParam("@TANKA", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(this.dgvInputList[8, num2].Value)));
						dbParamCollection3.SetParam("@SAISHU_TORIHIKI_DATE", SqlDbType.DateTime, dateTime);
						dbParamCollection3.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
						base.Dba.Insert("TB_HN_URIAGE_TANKA_RIREKI", dbParamCollection3);
					}
					else
					{
						DbParamCollection dbParamCollection3 = new DbParamCollection();
						dbParamCollection2 = new DbParamCollection();
						dbParamCollection3.SetParam("@SAISHU_TORIHIKI_DATE", SqlDbType.DateTime, dateTime);
						dbParamCollection3.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
						dbParamCollection2.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
						dbParamCollection2.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
						dbParamCollection2.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
						dbParamCollection2.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, 0);
						dbParamCollection2.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
						dbParamCollection2.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToDecimal(this.dgvInputList[2, num2].Value));
						dbParamCollection2.SetParam("@TANKA", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(this.dgvInputList[8, num2].Value)));
						if (Util.ToDecimal(Util.ToString(this.dgvInputList[8, num2].Value)) > 0m && dateTime > Util.ToDate(dataTableByConditionWithParams.Rows[0]["SAISHU_TORIHIKI_DATE"]))
						{
							base.Dba.Update("TB_HN_URIAGE_TANKA_RIREKI", dbParamCollection3, "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND TANKA = @TANKA", dbParamCollection2);
						}
					}
					num2++;
				}
				this.ZaikoKousin(num, 1);
				base.Dba.Commit();
			}
			finally
			{
				base.Dba.Rollback();
			}
			this.lblBeforeDenpyoNo.Text = string.Concat(new string[]
			{
				"【前回",
				(this.MODE_EDIT == 1) ? "登録" : "更新",
				"伝票番号：",
				num.ToString().PadLeft(6),
				"】"
			});
			this.lblBeforeDenpyoNo.Visible = true;
			return num;
		}

		// Token: 0x06000077 RID: 119 RVA: 0x000289DC File Offset: 0x00026BDC
		private void DeleteData()
		{
			decimal num = Util.ToDecimal(this.txtDenpyoNo.Text);
			try
			{
				base.Dba.BeginTransaction();
				this.ZaikoKousin(num, 3);
				DbParamCollection dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
				dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
				dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, base.UInfo.KaikeiNendo);
				dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
				dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, num);
				base.Dba.Delete("TB_HN_TORIHIKI_DENPYO", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO", dbParamCollection);
				dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
				dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
				dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, base.UInfo.KaikeiNendo);
				dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
				dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, num);
				base.Dba.Delete("TB_HN_TORIHIKI_MEISAI", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO", dbParamCollection);
				base.Dba.Commit();
			}
			finally
			{
				base.Dba.Rollback();
			}
			this.lblBeforeDenpyoNo.Text = "【前回削除伝票番号：" + num.ToString().PadLeft(6) + "】";
			this.lblBeforeDenpyoNo.Visible = true;
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00028BB4 File Offset: 0x00026DB4
		private bool GetShohinInfo(int input_type)
		{
			DataGridViewCell currentCell = this.dgvInputList.CurrentCell;
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Int, 6, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 15, this.txtGridEdit.Text);
			dbParamCollection.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 4, this.HIN_CHUSHI_KUBUN);
			string where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SHOHIN_KUBUN5 <> 1";
			if (this.HIN_CHUSHI_KUBUN == 1)
			{
				where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND ISNULL(CHUSHI_KUBUN, 0) = @CHUSHI_KUBUN AND SHOHIN_KUBUN5 <> 1";
			}
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "VI_HN_SHOHIN", where, dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count == 0)
			{
				this.dgvInputList[2, currentCell.RowIndex].Value = "";
				this.dgvInputList[3, currentCell.RowIndex].Value = "";
				this.dgvInputList[12, currentCell.RowIndex].Value = "";
				this.dgvInputList[13, currentCell.RowIndex].Value = "";
				this.dgvInputList[14, currentCell.RowIndex].Value = "";
				this.dgvInputList[15, currentCell.RowIndex].Value = "";
				this.dgvInputList[11, currentCell.RowIndex].Value = "";
				this.dgvInputList[16, currentCell.RowIndex].Value = "";
				this.dgvInputList[17, currentCell.RowIndex].Value = "";
				this.dgvInputList[18, currentCell.RowIndex].Value = "";
				this.dgvInputList[19, currentCell.RowIndex].Value = "";
				this.dgvInputList[4, currentCell.RowIndex].Value = "";
				this.dgvInputList[5, currentCell.RowIndex].Value = "";
				Msg.Error("入力に誤りがあります。");
				this.txtGridEdit.Focus();
				return false;
			}
			if (Util.ToString(this.dgvInputList[2, currentCell.RowIndex].Value) != this.txtGridEdit.Text)
			{
				//if (currentCell.RowIndex == 0)
				//{
				//	this.CalcInfo.SHOHIN_SHOHIZEI_KUBUN = Util.ToString(dataTableByConditionWithParams.Rows[0]["SHOHIZEI_KUBUN"]);
				//	if (this.CalcInfo.SHOHIN_SHOHIZEI_KUBUN == "0")
				//	{
				//		this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO = 3m;
				//		this.CellTaxRateSet();
				//		this.SetTaxInputCategory(Util.ToInt(Util.ToString(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO)));
				//	}
				//}
				//else if (this.CalcInfo.SHOHIN_SHOHIZEI_KUBUN != Util.ToString(dataTableByConditionWithParams.Rows[0]["SHOHIZEI_KUBUN"]))
				if (this.CalcInfo.SHOHIN_SHOHIZEI_KUBUN != Util.ToString(dataTableByConditionWithParams.Rows[0]["SHOHIZEI_KUBUN"]))
				{
					this.dgvInputList[2, currentCell.RowIndex].Value = "";
					this.dgvInputList[3, currentCell.RowIndex].Value = "";
					this.dgvInputList[12, currentCell.RowIndex].Value = "";
					this.dgvInputList[13, currentCell.RowIndex].Value = "";
					this.dgvInputList[14, currentCell.RowIndex].Value = "";
					this.dgvInputList[15, currentCell.RowIndex].Value = "";
					this.dgvInputList[11, currentCell.RowIndex].Value = "";
					this.dgvInputList[16, currentCell.RowIndex].Value = "";
					this.dgvInputList[17, currentCell.RowIndex].Value = "";
					this.dgvInputList[18, currentCell.RowIndex].Value = "";
					this.dgvInputList[19, currentCell.RowIndex].Value = "";
					this.dgvInputList[4, currentCell.RowIndex].Value = "";
					this.dgvInputList[5, currentCell.RowIndex].Value = "";
					Msg.Error("入力に誤りがあります。");
					this.txtGridEdit.Focus();
					return false;
				}
				this.dgvInputList[2, currentCell.RowIndex].Value = this.txtGridEdit.Text;
				this.dgvInputList[3, currentCell.RowIndex].Value = dataTableByConditionWithParams.Rows[0]["SHOHIN_NM"];
				decimal num = Util.ToDecimal(Util.FormatNum(dataTableByConditionWithParams.Rows[0]["OROSHI_TANKA"], this.CalcInfo.KETA_URI_TANKA));
				int num2 = Util.ToInt(this.CalcInfo.TANKA_SHUTOKU_HOHO);
				if (num2 != 1)
				{
					if (num2 == 2)
					{
						if (!this.GetTanka(Util.ToInt(this.txtFunanushiCd.Text), Util.ToDecimal(this.txtGridEdit.Text), ref num))
						{
							num = Util.ToDecimal(Util.FormatNum(dataTableByConditionWithParams.Rows[0]["KORI_TANKA"], this.CalcInfo.KETA_URI_TANKA));
						}
					}
				}
				else
				{
					num = Util.ToDecimal(Util.FormatNum(dataTableByConditionWithParams.Rows[0]["KORI_TANKA"], this.CalcInfo.KETA_URI_TANKA));
				}
				this.dgvInputList[8, currentCell.RowIndex].Value = Util.FormatNum(num, this.CalcInfo.KETA_URI_TANKA);
				this.dgvInputList[16, currentCell.RowIndex].Value = Util.FormatNum(dataTableByConditionWithParams.Rows[0]["SHIIRE_TANKA"], 1);
				this.dgvInputList[12, currentCell.RowIndex].Value = Util.ToString(dataTableByConditionWithParams.Rows[0]["URIAGE_SHIWAKE_CD"]);
				this.dgvInputList[13, currentCell.RowIndex].Value = Util.ToString((this.CalcInfo.BUMON_CD != 0) ? this.CalcInfo.BUMON_CD : dataTableByConditionWithParams.Rows[0]["URIAGE_BUMON_CD"]);
				this.dgvInputList[14, currentCell.RowIndex].Value = Util.ToString(dataTableByConditionWithParams.Rows[0]["URIAGE_ZEI_KUBUN"]);
				this.dgvInputList[15, currentCell.RowIndex].Value = Util.ToString((this.CalcInfo.JIGYO_KUBUN != 0) ? this.CalcInfo.JIGYO_KUBUN : dataTableByConditionWithParams.Rows[0]["URIAGE_JIGYO_KUBUN"]);
				this.dgvInputList[4, currentCell.RowIndex].Value = ((Util.ToDecimal(dataTableByConditionWithParams.Rows[0]["IRISU"]) == 0m) ? "" : dataTableByConditionWithParams.Rows[0]["IRISU"]);
				this.dgvInputList[5, currentCell.RowIndex].Value = Util.ToString(dataTableByConditionWithParams.Rows[0]["TANI"]);
				this.dgvInputList[18, currentCell.RowIndex].Value = Util.ToInt(dataTableByConditionWithParams.Rows[0]["ZAIKO_KANRI_KUBUN"]);

				// TODO 2020/02/12 Add Mutu Asato
				this.dgvInputList[19, currentCell.RowIndex].Value = "";
				if (Util.ToString(dataTableByConditionWithParams.Rows[0]["URIAGE_ZEI_KUBUN"])=="50")
				{
					this.dgvInputList[19, currentCell.RowIndex].Value = "*";
				}

				if (!ValChk.IsEmpty(dataTableByConditionWithParams.Rows[0]["URIAGE_ZEI_KUBUN"]))
				{
					dbParamCollection = new DbParamCollection();
					dbParamCollection.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, dataTableByConditionWithParams.Rows[0]["URIAGE_ZEI_KUBUN"]);
					DataTable dataTableByConditionWithParams2 = base.Dba.GetDataTableByConditionWithParams("*", "TB_ZM_F_ZEI_KUBUN", "ZEI_KUBUN = @ZEI_KUBUN", dbParamCollection);

					// TODO 2020/02/12 Upd Mutu Asato
					//DateTime date = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text, this.txtMonth.Text, this.txtDay.Text, base.Dba);
					DateTime date = Util.ConvAdDate(this.fsiDateDay.Gengo, this.fsiDateDay.WYear, this.fsiDateDay.Month, this.fsiDateDay.Day, base.Dba);


					this.dgvInputList[11, currentCell.RowIndex].Value = Util.FormatNum(TaxUtil.GetTaxRate(date, Util.ToInt(Util.ToString(this.dgvInputList[14, currentCell.RowIndex].Value)), base.Dba), 1);
					this.dgvInputList[17, currentCell.RowIndex].Value = Util.ToString(dataTableByConditionWithParams2.Rows[0]["KAZEI_KUBUN"]);
				}
				else
				{
					this.dgvInputList[11, currentCell.RowIndex].Value = "";
					this.dgvInputList[17, currentCell.RowIndex].Value = 0;
				}
				if (this.dgvInputList.RowCount == currentCell.RowIndex + 1)
				{
					this.dgvInputList.RowCount = this.dgvInputList.RowCount + 1;
					this.dgvInputList[0, this.dgvInputList.RowCount - 1].Value = this.dgvInputList.RowCount;
				}
			}
			if (input_type == 1)
			{
				if (Util.ToInt(this.dgvInputList[18, currentCell.RowIndex].Value) == 0)
				{
					this.dgvInputList.CurrentCell = this.dgvInputList[3, currentCell.RowIndex];
				}
				else if (Util.ToInt(Util.ToString(this.dgvInputList[4, currentCell.RowIndex].Value)) == 0)
				{
					this.dgvInputList.CurrentCell = this.dgvInputList[7, currentCell.RowIndex];
				}
				else
				{
					this.dgvInputList.CurrentCell = this.dgvInputList[6, currentCell.RowIndex];
				}
			}
			this.IS_INPUT = 2;
			this.OrgnDialogResult = true;

			return true;
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00029610 File Offset: 0x00027810
		private bool GetDenpyoData()
		{
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(this.txtDenpyoNo.Text));
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "TB_HN_TORIHIKI_DENPYO", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO", dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count == 0)
			{
				Msg.Info("該当データがありません。");
				this.txtDenpyoNo.SelectAll();
				return true;
			}
			if (this.MEISAI_DENPYO_BANGO != Util.ToDecimal(this.txtDenpyoNo.Text))
			{
				this.MEISAI_DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoNo.Text);
				this.InitDispOnEdit(dataTableByConditionWithParams);
			}
			return false;
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00029730 File Offset: 0x00027930
		private bool GetDenpyoMeisaiKeyEnter(Keys KeyCode, DataGridViewCell dgvCell)
		{
			if (KeyCode == Keys.Return || KeyCode == Keys.Right)
			{
				return true;
			}
			if (KeyCode == Keys.Down)
			{
				if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
				{
					this.dgvInputList.CurrentCell = this.dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex + 1];
				}
			}
			else if (KeyCode == Keys.Up)
			{
				if (dgvCell.RowIndex > 0)
				{
					this.dgvInputList.CurrentCell = this.dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex - 1];
				}
				else if (dgvCell.ColumnIndex == 1 && dgvCell.RowIndex == 0)
				{
					this.txtTantoshaCd.Focus();
				}
			}
			return false;
		}

		// Token: 0x0600007B RID: 123 RVA: 0x000297DC File Offset: 0x000279DC
		private void DoPrint(bool isPreview, decimal DENPYO_BANGO, int isLayOut)
		{
			try
			{
				base.Dba.BeginTransaction();
				if (this.MakeWkData(DENPYO_BANGO))
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append("  ITEM01");
					stringBuilder.Append(" ,ITEM02");
					stringBuilder.Append(" ,ITEM03");
					stringBuilder.Append(" ,ITEM04");
					stringBuilder.Append(" ,ITEM05");
					stringBuilder.Append(" ,ITEM06");
					stringBuilder.Append(" ,ITEM07");
					stringBuilder.Append(" ,ITEM08");
					stringBuilder.Append(" ,ITEM09");
					stringBuilder.Append(" ,ITEM10");
					stringBuilder.Append(" ,ITEM11");
					stringBuilder.Append(" ,ITEM12");
					stringBuilder.Append(" ,ITEM13");
					stringBuilder.Append(" ,ITEM14");
					stringBuilder.Append(" ,ITEM15");
					stringBuilder.Append(" ,ITEM16");
					stringBuilder.Append(" ,ITEM17");
					stringBuilder.Append(" ,ITEM18");
					stringBuilder.Append(" ,ITEM19");
					stringBuilder.Append(" ,ITEM20");
					stringBuilder.Append(" ,ITEM21");
					stringBuilder.Append(" ,ITEM22");
					stringBuilder.Append(" ,ITEM23");
					stringBuilder.Append(" ,ITEM24");
					stringBuilder.Append(" ,ITEM25");
					stringBuilder.Append(" ,ITEM26");
					stringBuilder.Append(" ,ITEM27");
					stringBuilder.Append(" ,ITEM28");
					stringBuilder.Append(" ,ITEM29");
					stringBuilder.Append(" ,ITEM30");
					stringBuilder.Append(" ,ITEM31");
					stringBuilder.Append(" ,ITEM32");
					stringBuilder.Append(" ,ITEM33");
					stringBuilder.Append(" ,ITEM34");
					stringBuilder.Append(" ,ITEM35");
					stringBuilder.Append(" ,ITEM36");
					stringBuilder.Append(" ,ITEM37");
					stringBuilder.Append(" ,ITEM38");
					stringBuilder.Append(" ,ITEM39");
					stringBuilder.Append(" ,ITEM40");
					stringBuilder.Append(" ,ITEM41");
					stringBuilder.Append(" ,ITEM42");
					stringBuilder.Append(" ,ITEM43");
					stringBuilder.Append(" ,ITEM44");
					stringBuilder.Append(" ,ITEM45");
					stringBuilder.Append(" ,ITEM46");
					stringBuilder.Append(" ,ITEM47");
					stringBuilder.Append(" ,ITEM48");
					stringBuilder.Append(" ,ITEM49");
					stringBuilder.Append(" ,ITEM50");
					stringBuilder.Append(" ,ITEM51");
					stringBuilder.Append(" ,ITEM52");
					stringBuilder.Append(" ,ITEM53");
					stringBuilder.Append(" ,ITEM54");
					stringBuilder.Append(" ,ITEM55");
					stringBuilder.Append(" ,ITEM56");
					stringBuilder.Append(" ,ITEM57");
					stringBuilder.Append(" ,ITEM58");
					stringBuilder.Append(" ,ITEM59");
					stringBuilder.Append(" ,ITEM60");
					stringBuilder.Append(" ,ITEM61");
					stringBuilder.Append(" ,ITEM62");
					stringBuilder.Append(" ,ITEM63");
					stringBuilder.Append(" ,ITEM64");
					stringBuilder.Append(" ,ITEM65");
					stringBuilder.Append(" ,ITEM66");
					stringBuilder.Append(" ,ITEM67");
					stringBuilder.Append(" ,ITEM68");
					stringBuilder.Append(" ,ITEM69");
					stringBuilder.Append(" ,ITEM70");
					stringBuilder.Append(" ,ITEM71");
					stringBuilder.Append(" ,ITEM72");
					stringBuilder.Append(" ,ITEM73");
					stringBuilder.Append(" ,ITEM74");
					stringBuilder.Append(" ,ITEM75");
					stringBuilder.Append(" ,ITEM76");
					stringBuilder.Append(" ,ITEM77");
					stringBuilder.Append(" ,ITEM78");
					stringBuilder.Append(" ,ITEM79");
					stringBuilder.Append(" ,ITEM80");
					stringBuilder.Append(" ,ITEM81");
					stringBuilder.Append(" ,ITEM82");
					stringBuilder.Append(" ,ITEM83");
					stringBuilder.Append(" ,ITEM84");
					stringBuilder.Append(" ,ITEM85");
					stringBuilder.Append(" ,ITEM86");
					stringBuilder.Append(" ,ITEM87");
					stringBuilder.Append(" ,ITEM88");
					stringBuilder.Append(" ,ITEM89");
					stringBuilder.Append(" ,ITEM90");
					stringBuilder.Append(" ,ITEM91");
					stringBuilder.Append(" ,ITEM92");
					stringBuilder.Append(" ,ITEM93");
					stringBuilder.Append(" ,ITEM94");
					stringBuilder.Append(" ,ITEM95");
					stringBuilder.Append(" ,ITEM96");
					stringBuilder.Append(" ,ITEM99");
					stringBuilder.Append(" ,ITEM100");
					stringBuilder.Append(" ,ITEM101");
					stringBuilder.Append(" ,ITEM102");
					stringBuilder.Append(" ,ITEM103");
					stringBuilder.Append(" ,ITEM104");
					stringBuilder.Append(" ,ITEM105");
					stringBuilder.Append(" ,ITEM106");
					stringBuilder.Append(" ,ITEM107");
					stringBuilder.Append(" ,REGIST_DATE");
					DbParamCollection dbParamCollection = new DbParamCollection();
					dbParamCollection.SetParam("@GUID", SqlDbType.VarChar, 36, base.UnqId);
					DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams(Util.ToString(stringBuilder), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dbParamCollection);
					KBDE10131R rpt = new KBDE10131R(dataTableByConditionWithParams);
					KBDE10132R rpt2 = new KBDE10132R(dataTableByConditionWithParams);
					new PreviewForm(rpt, rpt2, base.UnqId)
					{
						WindowState = FormWindowState.Maximized,
						TopMost = true
					}.ShowDialog();
				}
				else
				{
					Msg.Info("該当データがありません。");
				}
			}
			finally
			{
				base.Dba.Rollback();
			}
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00029DA0 File Offset: 0x00027FA0
		private bool MakeWkData(decimal DENPYO_BANGO)
		{
			decimal d = 10m; //表の列数。9m→10mへ変更
			decimal num = 5m; //表の行数。8m→5mへ変更
			decimal num2 = 0m;
			decimal d2 = 0m;
			string val = "";

			//軽減税率のフラグと値
			int keigen_flg = 0;
			int keigen_zeiritsu = 0;

			//金額と消費税額
			decimal hi_kin = 0m;
			decimal hi_zei = 0m;
			decimal kei_kin = 0m;
			decimal kei_zei = 0m;
			decimal ka_kin = 0m;
			decimal ka_zei = 0m;

			// TODO 2020/02/12 Upd Mutu Asato
			//			DateTime dateTime = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text, this.txtMonth.Text, this.txtDay.Text, base.Dba);
			DateTime dateTime = Util.ConvAdDate(this.fsiDateDay.Gengo, this.fsiDateDay.WYear, this.fsiDateDay.Month, this.fsiDateDay.Day, base.Dba);


			Util.ConvJpDate(DateTime.Now, base.Dba);
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "TB_CM_KAISHA_JOHO", "KAISHA_CD = @KAISHA_CD", "KAISHA_CD", dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count == 0)
			{
				return false;
			}
			dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
			DataTable dataTableByConditionWithParams2 = base.Dba.GetDataTableByConditionWithParams("*", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dbParamCollection);
			if (dataTableByConditionWithParams2.Rows.Count == 0)
			{
				return false;
			}
			dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
			DataTable dataTableByConditionWithParams3 = base.Dba.GetDataTableByConditionWithParams("*", "TB_HN_TORIHIKI_DENPYO", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO", dbParamCollection);
			if (dataTableByConditionWithParams3.Rows.Count == 0)
			{
				return true;
			}
			dbParamCollection = new DbParamCollection();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT");
			stringBuilder.Append("    (CASE WHEN A.NOHINSAKI_CD = 0 THEN A.SEIKYUSAKI_NM ELSE A.TOKUISAKI_NM END) AS 社名,");
			stringBuilder.Append("    (CASE WHEN A.NOHINSAKI_CD = 0 THEN CASE WHEN A.TOKUISAKI_CD = A.SEIKYUSAKI_CD THEN '' ELSE A.TOKUISAKI_NM END ELSE A.NOHINSAKI_NM END) AS 店名,");
			stringBuilder.Append("    ''                       AS 社店コード,");
			stringBuilder.Append("    ''                       AS 分類コード,");
			stringBuilder.Append("    ''                       AS 伝票区分,");
			stringBuilder.Append("    A.DENPYO_BANGO           AS 伝票番号,");
			stringBuilder.Append("    0                        AS 取引先コード,");
			stringBuilder.Append("    ''                       AS 発注日,");
			stringBuilder.Append("    A.DENPYO_DATE            AS 納品日,");
			stringBuilder.Append("    0                        AS 便,");
			stringBuilder.Append("    A.SHOHIN_CD              AS 商品コード,");
			stringBuilder.Append("    A.SHOHIN_NM              AS 商品名,");
			stringBuilder.Append("    A.KIKAKU                 AS 規格,");
			stringBuilder.Append("    ''                       AS 色,");
			stringBuilder.Append("    ''                       AS サイズ,");
			stringBuilder.Append("    A.IRISU                  AS 入数,");
			stringBuilder.Append("    A.SURYO1                 AS ケース,");
			stringBuilder.Append("    A.SURYO2                 AS バラ,");
			stringBuilder.Append("    A.TANI                   AS 単位,");
			stringBuilder.Append("    ((A.SURYO1 * A.IRISU) + A.SURYO2) AS 数量,");
			stringBuilder.Append("    0                        AS 訂正数量,");
			stringBuilder.Append("    ''                       AS 引合,");
			stringBuilder.Append("    A.URI_TANKA              AS 原単価,");
			stringBuilder.Append("    A.BAIKA_KINGAKU          AS 原価金額,");
			stringBuilder.Append("    A.HANBAI_TANKA           AS 売単価,");
			stringBuilder.Append("    A.HANBAI_KINGAKU         AS 売価金額,");
			stringBuilder.Append("    A.ZEI_KUBUN              AS 税区分,");
			stringBuilder.Append("    A.SHOHIZEI_NYURYOKU_HOHO AS 消費税入力方法,");
			stringBuilder.Append("    A.TEKIYO_NM              AS 摘要名,");
			stringBuilder.Append("    A.TORIHIKI_KUBUN_NM      AS 取引区分名称,");
			stringBuilder.Append("    A.TORIHIKI_KUBUN         AS 取引区分,");
			stringBuilder.Append("    A.TANTOSHA_NM            AS 担当者名,");
			stringBuilder.Append("    A.SEIKYUSAKI_NM          AS 集計名称,");
			stringBuilder.Append("    A.TOKUISAKI_CD           AS 得意先コード,");
			stringBuilder.Append("    A.SEIKYUSAKI_CD          AS 請求先コード,");
			stringBuilder.Append("    A.NOHINSAKI_CD           AS 納品先コード,");
			stringBuilder.Append("    A.TOKUISAKI_NM           AS 得意先名,");
			stringBuilder.Append("    A.SEIKYUSAKI_NM          AS 請求先名,");
			stringBuilder.Append("    A.NOHINSAKI_NM           AS 納品先名,");
			stringBuilder.Append("    A.ZEI_RITSU              AS 税率,");
			stringBuilder.Append("    CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 THEN A.SHOHIZEI ELSE 0 END AS 内消費税,");
			stringBuilder.Append("    CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 THEN 0 ELSE A.SHOHIZEI END AS 外消費税 ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("    VI_HN_TORIHIKI_MEISAI AS A ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    A.KAISHA_CD      = @KAISHA_CD AND");
			stringBuilder.Append("    A.SHISHO_CD      = @SHISHO_CD AND");
			stringBuilder.Append("    A.DENPYO_KUBUN   = @DENPYO_KUBUN AND");
			stringBuilder.Append("    A.KAIKEI_NENDO   = @KAIKEI_NENDO AND");
			stringBuilder.Append("    (A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_FR) AND");
			stringBuilder.Append("    (A.DENPYO_BANGO BETWEEN @DENPYO_BANGO_FR AND @DENPYO_BANGO_TO) AND");
			stringBuilder.Append("    (A.TANTOSHA_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO) AND");
			stringBuilder.Append("    A.TOKUISAKI_CD   = @TOKUISAKI_CD ");
			stringBuilder.Append("ORDER BY ");
			stringBuilder.Append("    A.KAISHA_CD ASC,");
			stringBuilder.Append("    A.DENPYO_DATE ASC,");
			stringBuilder.Append("    A.DENPYO_BANGO ASC,");
			stringBuilder.Append("    A.GYO_BANGO ASC");
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, base.UInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, dateTime);
			dbParamCollection.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, dateTime);
			dbParamCollection.SetParam("@DENPYO_BANGO_FR", SqlDbType.Decimal, 8, DENPYO_BANGO);
			dbParamCollection.SetParam("@DENPYO_BANGO_TO", SqlDbType.Decimal, 8, DENPYO_BANGO);
			dbParamCollection.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
			dbParamCollection.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
			dbParamCollection.SetParam("@TOKUISAKI_CD", SqlDbType.VarChar, 4, this.txtFunanushiCd.Text);
			DataTable dataTableFromSqlWithParams = base.Dba.GetDataTableFromSqlWithParams(Util.ToString(stringBuilder), dbParamCollection);
			if (dataTableFromSqlWithParams.Rows.Count == 0)
			{
				Msg.Info("該当データがありません。");
				return false;
			}
			d2 = Math.Ceiling(dataTableFromSqlWithParams.Rows.Count / num);
			stringBuilder = new StringBuilder();
			stringBuilder.Append("INSERT INTO PR_HN_TBL(");
			stringBuilder.Append("  GUID");
			stringBuilder.Append(" ,SORT");
			stringBuilder.Append(" ,ITEM01");
			stringBuilder.Append(" ,ITEM02");
			stringBuilder.Append(" ,ITEM03");
			stringBuilder.Append(" ,ITEM04");
			stringBuilder.Append(" ,ITEM05");
			stringBuilder.Append(" ,ITEM06");
			stringBuilder.Append(" ,ITEM07");
			stringBuilder.Append(" ,ITEM08");
			stringBuilder.Append(" ,ITEM09");
			stringBuilder.Append(" ,ITEM10");
			stringBuilder.Append(" ,ITEM11");
			stringBuilder.Append(" ,ITEM12");
			stringBuilder.Append(" ,ITEM13");
			stringBuilder.Append(" ,ITEM14");
			stringBuilder.Append(" ,ITEM15");
			stringBuilder.Append(" ,ITEM16");
			stringBuilder.Append(" ,ITEM17");
			stringBuilder.Append(" ,ITEM18");
			stringBuilder.Append(" ,ITEM19");
			stringBuilder.Append(" ,ITEM20");
			stringBuilder.Append(" ,ITEM21");
			stringBuilder.Append(" ,ITEM22");
			stringBuilder.Append(" ,ITEM23");
			stringBuilder.Append(" ,ITEM24");
			stringBuilder.Append(" ,ITEM25");
			stringBuilder.Append(" ,ITEM26");
			stringBuilder.Append(" ,ITEM27");
			stringBuilder.Append(" ,ITEM28");
			stringBuilder.Append(" ,ITEM29");
			stringBuilder.Append(" ,ITEM30");
			stringBuilder.Append(" ,ITEM31");
			stringBuilder.Append(" ,ITEM32");
			stringBuilder.Append(" ,ITEM33");
			stringBuilder.Append(" ,ITEM34");
			stringBuilder.Append(" ,ITEM35");
			stringBuilder.Append(" ,ITEM36");
			stringBuilder.Append(" ,ITEM37");
			stringBuilder.Append(" ,ITEM38");
			stringBuilder.Append(" ,ITEM39");
			stringBuilder.Append(" ,ITEM40");
			stringBuilder.Append(" ,ITEM41");
			stringBuilder.Append(" ,ITEM42");
			stringBuilder.Append(" ,ITEM43");
			stringBuilder.Append(" ,ITEM44");
			stringBuilder.Append(" ,ITEM45");
			stringBuilder.Append(" ,ITEM46");
			stringBuilder.Append(" ,ITEM47");
			stringBuilder.Append(" ,ITEM48");
			stringBuilder.Append(" ,ITEM49");
			stringBuilder.Append(" ,ITEM50");
			stringBuilder.Append(" ,ITEM51");
			stringBuilder.Append(" ,ITEM52");
			stringBuilder.Append(" ,ITEM53");
			stringBuilder.Append(" ,ITEM54");
			stringBuilder.Append(" ,ITEM55");
			stringBuilder.Append(" ,ITEM56");
			stringBuilder.Append(" ,ITEM57");
			stringBuilder.Append(" ,ITEM58");
			stringBuilder.Append(" ,ITEM59");
			stringBuilder.Append(" ,ITEM60");
			stringBuilder.Append(" ,ITEM61");
			stringBuilder.Append(" ,ITEM62");
			stringBuilder.Append(" ,ITEM63");
			stringBuilder.Append(" ,ITEM64");
			stringBuilder.Append(" ,ITEM65");
			stringBuilder.Append(" ,ITEM66");
			stringBuilder.Append(" ,ITEM67");
			stringBuilder.Append(" ,ITEM68");
			stringBuilder.Append(" ,ITEM69");
			stringBuilder.Append(" ,ITEM70");
			stringBuilder.Append(" ,ITEM71");
			stringBuilder.Append(" ,ITEM72");
			stringBuilder.Append(" ,ITEM73");
			stringBuilder.Append(" ,ITEM74");
			stringBuilder.Append(" ,ITEM75");
			stringBuilder.Append(" ,ITEM76");
			stringBuilder.Append(" ,ITEM77");
			stringBuilder.Append(" ,ITEM78");
			stringBuilder.Append(" ,ITEM79");
			stringBuilder.Append(" ,ITEM80");
			//stringBuilder.Append(" ,ITEM81");
			//stringBuilder.Append(" ,ITEM82");
			//stringBuilder.Append(" ,ITEM83");
			//stringBuilder.Append(" ,ITEM84");
			//stringBuilder.Append(" ,ITEM85");
			//stringBuilder.Append(" ,ITEM86");
			//stringBuilder.Append(" ,ITEM87");
			//stringBuilder.Append(" ,ITEM88");
			//stringBuilder.Append(" ,ITEM89");
			//stringBuilder.Append(" ,ITEM90");
			//stringBuilder.Append(" ,ITEM91");
			//stringBuilder.Append(" ,ITEM92");
			//stringBuilder.Append(" ,ITEM93");
			stringBuilder.Append(" ,ITEM94");
			stringBuilder.Append(" ,ITEM95");
			stringBuilder.Append(" ,ITEM96");
			stringBuilder.Append(" ,ITEM99");
			stringBuilder.Append(" ,ITEM100");
			stringBuilder.Append(" ,ITEM101");
			stringBuilder.Append(" ,ITEM102");
			stringBuilder.Append(" ,ITEM103");
			stringBuilder.Append(" ,ITEM104");
			//stringBuilder.Append(" ,ITEM105");
			//stringBuilder.Append(" ,ITEM106");
			//stringBuilder.Append(" ,ITEM107");
			stringBuilder.Append(" ,REGIST_DATE");
			stringBuilder.Append(") ");
			stringBuilder.Append("VALUES(");
			stringBuilder.Append("  @GUID");
			stringBuilder.Append(" ,@SORT");
			stringBuilder.Append(" ,@ITEM01");
			stringBuilder.Append(" ,@ITEM02");
			stringBuilder.Append(" ,@ITEM03");
			stringBuilder.Append(" ,@ITEM04");
			stringBuilder.Append(" ,@ITEM05");
			stringBuilder.Append(" ,@ITEM06");
			stringBuilder.Append(" ,@ITEM07");
			stringBuilder.Append(" ,@ITEM08");
			stringBuilder.Append(" ,@ITEM09");
			stringBuilder.Append(" ,@ITEM10");
			stringBuilder.Append(" ,@ITEM11");
			stringBuilder.Append(" ,@ITEM12");
			stringBuilder.Append(" ,@ITEM13");
			stringBuilder.Append(" ,@ITEM14");
			stringBuilder.Append(" ,@ITEM15");
			stringBuilder.Append(" ,@ITEM16");
			stringBuilder.Append(" ,@ITEM17");
			stringBuilder.Append(" ,@ITEM18");
			stringBuilder.Append(" ,@ITEM19");
			stringBuilder.Append(" ,@ITEM20");
			stringBuilder.Append(" ,@ITEM21");
			stringBuilder.Append(" ,@ITEM22");
			stringBuilder.Append(" ,@ITEM23");
			stringBuilder.Append(" ,@ITEM24");
			stringBuilder.Append(" ,@ITEM25");
			stringBuilder.Append(" ,@ITEM26");
			stringBuilder.Append(" ,@ITEM27");
			stringBuilder.Append(" ,@ITEM28");
			stringBuilder.Append(" ,@ITEM29");
			stringBuilder.Append(" ,@ITEM30");
			stringBuilder.Append(" ,@ITEM31");
			stringBuilder.Append(" ,@ITEM32");
			stringBuilder.Append(" ,@ITEM33");
			stringBuilder.Append(" ,@ITEM34");
			stringBuilder.Append(" ,@ITEM35");
			stringBuilder.Append(" ,@ITEM36");
			stringBuilder.Append(" ,@ITEM37");
			stringBuilder.Append(" ,@ITEM38");
			stringBuilder.Append(" ,@ITEM39");
			stringBuilder.Append(" ,@ITEM40");
			stringBuilder.Append(" ,@ITEM41");
			stringBuilder.Append(" ,@ITEM42");
			stringBuilder.Append(" ,@ITEM43");
			stringBuilder.Append(" ,@ITEM44");
			stringBuilder.Append(" ,@ITEM45");
			stringBuilder.Append(" ,@ITEM46");
			stringBuilder.Append(" ,@ITEM47");
			stringBuilder.Append(" ,@ITEM48");
			stringBuilder.Append(" ,@ITEM49");
			stringBuilder.Append(" ,@ITEM50");
			stringBuilder.Append(" ,@ITEM51");
			stringBuilder.Append(" ,@ITEM52");
			stringBuilder.Append(" ,@ITEM53");
			stringBuilder.Append(" ,@ITEM54");
			stringBuilder.Append(" ,@ITEM55");
			stringBuilder.Append(" ,@ITEM56");
			stringBuilder.Append(" ,@ITEM57");
			stringBuilder.Append(" ,@ITEM58");
			stringBuilder.Append(" ,@ITEM59");
			stringBuilder.Append(" ,@ITEM60");
			stringBuilder.Append(" ,@ITEM61");
			stringBuilder.Append(" ,@ITEM62");
			stringBuilder.Append(" ,@ITEM63");
			stringBuilder.Append(" ,@ITEM64");
			stringBuilder.Append(" ,@ITEM65");
			stringBuilder.Append(" ,@ITEM66");
			stringBuilder.Append(" ,@ITEM67");
			stringBuilder.Append(" ,@ITEM68");
			stringBuilder.Append(" ,@ITEM69");
			stringBuilder.Append(" ,@ITEM70");
			stringBuilder.Append(" ,@ITEM71");
			stringBuilder.Append(" ,@ITEM72");
			stringBuilder.Append(" ,@ITEM73");
			stringBuilder.Append(" ,@ITEM74");
			stringBuilder.Append(" ,@ITEM75");
			stringBuilder.Append(" ,@ITEM76");
			stringBuilder.Append(" ,@ITEM77");
			stringBuilder.Append(" ,@ITEM78");
			stringBuilder.Append(" ,@ITEM79");
			stringBuilder.Append(" ,@ITEM80");
			//stringBuilder.Append(" ,@ITEM81");
			//stringBuilder.Append(" ,@ITEM82");
			//stringBuilder.Append(" ,@ITEM83");
			//stringBuilder.Append(" ,@ITEM84");
			//stringBuilder.Append(" ,@ITEM85");
			//stringBuilder.Append(" ,@ITEM86");
			//stringBuilder.Append(" ,@ITEM87");
			//stringBuilder.Append(" ,@ITEM88");
			//stringBuilder.Append(" ,@ITEM89");
			//stringBuilder.Append(" ,@ITEM90");
			//stringBuilder.Append(" ,@ITEM91");
			//stringBuilder.Append(" ,@ITEM92");
			//stringBuilder.Append(" ,@ITEM93");
			stringBuilder.Append(" ,@ITEM94");
			stringBuilder.Append(" ,@ITEM95");
			stringBuilder.Append(" ,@ITEM96");
			stringBuilder.Append(" ,@ITEM99");
			stringBuilder.Append(" ,@ITEM100");
			stringBuilder.Append(" ,@ITEM101");
			stringBuilder.Append(" ,@ITEM102");
			stringBuilder.Append(" ,@ITEM103");
			stringBuilder.Append(" ,@ITEM104");
			//stringBuilder.Append(" ,@ITEM105");
			//stringBuilder.Append(" ,@ITEM106");
			//stringBuilder.Append(" ,@ITEM107");
			stringBuilder.Append(" ,@REGIST_DATE");
			stringBuilder.Append(") ");
			int num3 = 0;
			int num4 = 1;
			while (dataTableFromSqlWithParams.Rows.Count > num3)
			{
				num2 = Math.Ceiling(num3 / num);
				if (num2 == 0m)
				{
					num2 = 1m;
				}
				else
				{
					num2 = ++num2;
				}
				dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@GUID", SqlDbType.VarChar, 36, base.UnqId);
				dbParamCollection.SetParam("@SORT", SqlDbType.VarChar, 4, num4);
				dbParamCollection.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.ToString(Util.ConvJpDate(dateTime, base.Dba)[5]));
				dbParamCollection.SetParam("@ITEM02", SqlDbType.VarChar, 200, Util.ToString(dataTableFromSqlWithParams.Rows[num3]["伝票番号"]));
				dbParamCollection.SetParam("@ITEM03", SqlDbType.VarChar, 200, "〒 " + Util.ToString(dataTableByConditionWithParams2.Rows[0]["YUBIN_BANGO1"]) + " － " + Util.ToString(dataTableByConditionWithParams2.Rows[0]["YUBIN_BANGO2"]));
				dbParamCollection.SetParam("@ITEM04", SqlDbType.VarChar, 200, Util.ToString(dataTableByConditionWithParams2.Rows[0]["TORIHIKISAKI_NM"]));

				dbParamCollection.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.ToString(dataTableByConditionWithParams2.Rows[0]["DENWA_BANGO"]));
				dbParamCollection.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.ToString(dataTableByConditionWithParams2.Rows[0]["FAX_BANGO"]));
				dbParamCollection.SetParam("@ITEM94", SqlDbType.VarChar, 200, Util.ToString(dataTableByConditionWithParams2.Rows[0]["JUSHO1"]));
				dbParamCollection.SetParam("@ITEM95", SqlDbType.VarChar, 200, Util.ToString(dataTableByConditionWithParams2.Rows[0]["JUSHO2"]));
				dbParamCollection.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.ToString(dataTableByConditionWithParams.Rows[0]["KAISHA_NM"]));
				dbParamCollection.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.ToString(dataTableByConditionWithParams.Rows[0]["JUSHO1"]));
				dbParamCollection.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.ToString(dataTableByConditionWithParams.Rows[0]["JUSHO2"]));
				dbParamCollection.SetParam("@ITEM10", SqlDbType.VarChar, 200, "");
				dbParamCollection.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.ToString("《 " + dataTableFromSqlWithParams.Rows[num3]["取引区分名称"] + " 》"));
				dbParamCollection.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.ToString(dataTableFromSqlWithParams.Rows[num3]["担当者名"]));
				dbParamCollection.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.ToString(num2));
				dbParamCollection.SetParam("@ITEM96", SqlDbType.VarChar, 200, Util.ToString(dataTableByConditionWithParams2.Rows[0]["TORIHIKISAKI_CD"]));
				
				int num5 = 0;
				while (num > num5)
				{
					if (dataTableFromSqlWithParams.Rows.Count > num3 + num5)
					{
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 14m + num5 * d), SqlDbType.VarChar, 200, Util.ToString(dataTableFromSqlWithParams.Rows[num3 + num5]["商品コード"]));
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 15m + num5 * d), SqlDbType.VarChar, 200, Util.ToString(dataTableFromSqlWithParams.Rows[num3 + num5]["商品名"]));
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 16m + num5 * d), SqlDbType.VarChar, 200, Util.ToString(dataTableFromSqlWithParams.Rows[num3 + num5]["規格"]));

						//区分の表示
						switch (Util.ToInt(dataTableFromSqlWithParams.Rows[num3 + num5]["税区分"])) //税区分の値で判断
						{
							case 0: //0の場合、非課税
								dbParamCollection.SetParam(string.Format("@ITEM{0}", 17m + num5 * d), SqlDbType.VarChar, 200, Util.ToString("")); //空欄を表示

								//金額と税額取得
								hi_kin += Util.ToDecimal(dataTableFromSqlWithParams.Rows[num3 + num5]["原価金額"]); 
								hi_zei += hikazei_zeigk[num3 + num5];
								break;

							case 50: //50の場合、軽減税率対象
								dbParamCollection.SetParam(string.Format("@ITEM{0}", 17m + num5 * d), SqlDbType.VarChar, 200, Util.ToString("*")); //「*」を表示

								//金額と税額取得
								kei_kin += Util.ToDecimal(dataTableFromSqlWithParams.Rows[num3 + num5]["原価金額"]);
								kei_zei += keigen_zeigk[num3 + num5];

								//フラグ立て・軽減税率の値取得
								keigen_flg = 1;
								keigen_zeiritsu = (Util.ToInt(dataTableFromSqlWithParams.Rows[num3 + num5]["税率"]));
								break;

							default:
								dbParamCollection.SetParam(string.Format("@ITEM{0}", 17m + num5 * d), SqlDbType.VarChar, 200, Util.ToString("")); //空欄を表示

								//金額と税額取得
								ka_kin += Util.ToDecimal(dataTableFromSqlWithParams.Rows[num3 + num5]["原価金額"]);
								ka_zei += kazei_zeigk[num3 + num5];
								break;
						}

						dbParamCollection.SetParam(string.Format("@ITEM{0}", 18m + num5 * d), SqlDbType.VarChar, 200, Util.ToString(dataTableFromSqlWithParams.Rows[num3 + num5]["単位"]));
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 19m + num5 * d), SqlDbType.VarChar, 200, (Util.ToDecimal(dataTableFromSqlWithParams.Rows[num3 + num5]["入数"]) > 0m) ? Util.FormatNum(dataTableFromSqlWithParams.Rows[num3 + num5]["入数"], 1) : "");
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 20m + num5 * d), SqlDbType.VarChar, 200, (Util.ToDecimal(dataTableFromSqlWithParams.Rows[num3 + num5]["ケース"]) > 0m) ? Util.FormatNum(dataTableFromSqlWithParams.Rows[num3 + num5]["ケース"], 1) : "");
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 21m + num5 * d), SqlDbType.VarChar, 200, (Util.ToDecimal(dataTableFromSqlWithParams.Rows[num3 + num5]["バラ"]) > 0m) ? Util.FormatNum(dataTableFromSqlWithParams.Rows[num3 + num5]["バラ"], 2) : "");
						if (KBDE1011.IsDecimal(Util.ToDecimal(dataTableFromSqlWithParams.Rows[num3 + num5]["原単価"])))
						{
							dbParamCollection.SetParam(string.Format("@ITEM{0}", 22m + num5 * d), SqlDbType.VarChar, 200, Util.ToDecimal(Util.FormatNum(dataTableFromSqlWithParams.Rows[num3 + num5]["原単価"], 2)));
						}
						else
						{
							dbParamCollection.SetParam(string.Format("@ITEM{0}", 22m + num5 * d), SqlDbType.VarChar, 200, Util.FormatNum(dataTableFromSqlWithParams.Rows[num3 + num5]["原単価"]));
						}
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 23m + num5 * d), SqlDbType.VarChar, 200, Util.FormatNum(dataTableFromSqlWithParams.Rows[num3 + num5]["原価金額"]));
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 100 + num5), SqlDbType.VarChar, 200, ((Util.ToInt(dataTableFromSqlWithParams.Rows[num3 + num5]["税率"]) < 10) ? " " : "") + Util.FormatNum(dataTableFromSqlWithParams.Rows[num3 + num5]["税率"]));
					}
					else
					{
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 14m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 15m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 16m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 17m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 18m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 19m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 20m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 21m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 22m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 23m + num5 * d), SqlDbType.VarChar, 200, val);
						dbParamCollection.SetParam(string.Format("@ITEM{0}", 100 + num5), SqlDbType.VarChar, 200, val);
					}
					num5++;
				}

				if (num2 == d2)
				{
					dbParamCollection.SetParam("@ITEM64", SqlDbType.VarChar, 200, Util.ToString("金額"));
					dbParamCollection.SetParam("@ITEM65", SqlDbType.VarChar, 200, Util.ToString("内消費税"));
					dbParamCollection.SetParam("@ITEM66", SqlDbType.VarChar, 200, Util.ToString("合計"));

					//金額の表示
					switch (keigen_flg)
					{
						case 1:
							//非課税の表示
							dbParamCollection.SetParam("@ITEM67", SqlDbType.VarChar, 200, Util.ToString("非課税売上"));
							dbParamCollection.SetParam("@ITEM70", SqlDbType.VarChar, 200, Util.FormatNum(hi_kin));
							dbParamCollection.SetParam("@ITEM71", SqlDbType.VarChar, 200, Util.FormatNum(hi_zei));
							dbParamCollection.SetParam("@ITEM72", SqlDbType.VarChar, 200, Util.FormatNum(hi_kin + hi_zei));

							//課税の表示
							dbParamCollection.SetParam("@ITEM68", SqlDbType.VarChar, 200, Util.ToString("課税売上(10%)"));
							dbParamCollection.SetParam("@ITEM73", SqlDbType.VarChar, 200, Util.FormatNum(ka_kin));
							dbParamCollection.SetParam("@ITEM74", SqlDbType.VarChar, 200, Util.FormatNum(ka_zei));
							dbParamCollection.SetParam("@ITEM75", SqlDbType.VarChar, 200, Util.FormatNum(ka_kin + ka_zei));

							//軽減税率対象の表示
							dbParamCollection.SetParam("@ITEM69", SqlDbType.VarChar, 200, Util.ToString("課税売上(" + Util.FormatNum(keigen_zeiritsu) + "%)"));
							dbParamCollection.SetParam("@ITEM76", SqlDbType.VarChar, 200, Util.FormatNum(kei_kin));
							dbParamCollection.SetParam("@ITEM77", SqlDbType.VarChar, 200, Util.FormatNum(kei_zei));
							dbParamCollection.SetParam("@ITEM78", SqlDbType.VarChar, 200, Util.FormatNum(kei_kin + kei_zei));

							break;

						case 0:
							//非課税
							dbParamCollection.SetParam("@ITEM67", SqlDbType.VarChar, 200, Util.ToString("非課税売上"));
							dbParamCollection.SetParam("@ITEM70", SqlDbType.VarChar, 200, Util.FormatNum(hi_kin));
							dbParamCollection.SetParam("@ITEM71", SqlDbType.VarChar, 200, Util.FormatNum(hi_zei));
							dbParamCollection.SetParam("@ITEM72", SqlDbType.VarChar, 200, Util.FormatNum(hi_kin + hi_zei));

							//軽減税率対象外
							dbParamCollection.SetParam("@ITEM68", SqlDbType.VarChar, 200, Util.ToString("課税売上(10%)"));
							dbParamCollection.SetParam("@ITEM73", SqlDbType.VarChar, 200, Util.FormatNum(ka_kin));
							dbParamCollection.SetParam("@ITEM74", SqlDbType.VarChar, 200, Util.FormatNum(ka_zei));
							dbParamCollection.SetParam("@ITEM75", SqlDbType.VarChar, 200, Util.FormatNum(ka_kin + ka_zei));

							//軽減税率対象 ※対象がないので非表示
							dbParamCollection.SetParam("@ITEM69", SqlDbType.VarChar, 200, Util.ToString(""));
							dbParamCollection.SetParam("@ITEM76", SqlDbType.VarChar, 200, Util.ToString(""));
							dbParamCollection.SetParam("@ITEM77", SqlDbType.VarChar, 200, Util.ToString(""));
							dbParamCollection.SetParam("@ITEM78", SqlDbType.VarChar, 200, Util.ToString(""));
							break;
					}
					//軽減税率表記説明。軽減税率の値を表示
					//dbParamCollection.SetParam("@ITEM81", SqlDbType.VarChar, 200, Util.ToString("「*」は軽減税率であることを示します。"));

					//合計表示
					dbParamCollection.SetParam("@ITEM79", SqlDbType.VarChar, 200, Util.ToString("合計"));
					dbParamCollection.SetParam("@ITEM80", SqlDbType.VarChar, 200, Util.FormatNum(dataTableByConditionWithParams3.Rows[0]["NYUKIN_GOKEIGAKU"]));

				}
				else
				{
					dbParamCollection.SetParam("@ITEM64", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM65", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM66", SqlDbType.VarChar, 200, val);

					dbParamCollection.SetParam("@ITEM67", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM68", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM69", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM70", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM71", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM72", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM73", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM74", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM75", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM76", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM77", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM78", SqlDbType.VarChar, 200, val);

					dbParamCollection.SetParam("@ITEM80", SqlDbType.VarChar, 200, val);
					dbParamCollection.SetParam("@ITEM79", SqlDbType.VarChar, 200, val);
					//dbParamCollection.SetParam("@ITEM81", SqlDbType.VarChar, 200, val);
				}

				num3 += num5;
				dbParamCollection.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now);
				dbParamCollection.SetParam("@ITEM99", SqlDbType.VarChar, 200, this.lblTaxInfo.Text.Replace("消費税内訳", ""));
				base.Dba.ModifyBySql(Util.ToString(stringBuilder), dbParamCollection);
				num4++;
			}
			dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@GUID", SqlDbType.VarChar, 36, base.UnqId);
			return Util.ToDecimal(base.Dba.GetDataTableByConditionWithParams("COUNT(*) AS CNT", "PR_HN_TBL", "GUID = @GUID", dbParamCollection).Rows[0]["CNT"]) > 0m;
		}

		// Token: 0x0600007D RID: 125 RVA: 0x0002BC18 File Offset: 0x00029E18
		private bool isValidDenpyoNo()
		{
			if (!ValChk.IsNumber(this.txtDenpyoNo.Text))
			{
				Msg.Notice("数値のみで入力してください。");
				return false;
			}
			if (!ValChk.IsWithinLength(this.txtDenpyoNo.Text, this.txtDenpyoNo.MaxLength))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			if (this.MEISAI_DENPYO_BANGO != 0m && ValChk.IsEmpty(this.txtDenpyoNo.Text))
			{
				this.InitDispOnNew();
				return true;
			}
			if (ValChk.IsEmpty(this.txtDenpyoNo.Text) && (this.MEISAI_DENPYO_BANGO == 0m))
			{
				return true;
			}
			return true;
		}

		// Token: 0x0600007E RID: 126 RVA: 0x0002BCC0 File Offset: 0x00029EC0
		private bool IsValidMizuageShishoCd()
		{
			if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || object.Equals(this.txtMizuageShishoCd.Text, "0"))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			this.lblMizuageShishoNm.Text = base.Dba.GetName(base.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
			if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			if (Util.ToInt(base.UInfo.ShishoCd) != Util.ToInt(this.txtMizuageShishoCd.Text))
			{
				base.UInfo.ShishoCd = this.txtMizuageShishoCd.Text;
				base.UInfo.ShishoNm = this.lblMizuageShishoNm.Text;
			}
			return true;
		}

		// Token: 0x0600007F RID: 127 RVA: 0x0002BDA8 File Offset: 0x00029FA8
		private bool isValidTorihikiKubunCd()
		{
			if (!ValChk.IsNumber(this.txtTorihikiKubunCd.Text))
			{
				Msg.Notice("数値のみで入力してください。");
				return false;
			}
			if (!ValChk.IsWithinLength(this.txtTorihikiKubunCd.Text, this.txtTorihikiKubunCd.MaxLength))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			if (ValChk.IsEmpty(this.txtTorihikiKubunCd.Text))
			{
				this.txtTorihikiKubunCd.Text = "0";
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			if (this.lblTorihikiKubunNm.Text == "")
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			return true;
		}

		// Token: 0x06000080 RID: 128 RVA: 0x0002BE8C File Offset: 0x0002A08C
		private bool isValidFunanushiCd()
		{
			if (!ValChk.IsNumber(this.txtFunanushiCd.Text))
			{
				Msg.Notice("数値のみで入力してください。");
				return false;
			}
			if (!ValChk.IsWithinLength(this.txtFunanushiCd.Text, this.txtFunanushiCd.MaxLength))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			if (ValChk.IsEmpty(this.txtFunanushiCd.Text))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("TORIHIKISAKI_NM", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN1 = 1", dbParamCollection);
			if (dataTableByConditionWithParams == null || dataTableByConditionWithParams.Rows.Count == 0)
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			this.lblFunanushiNm.Text = Util.ToString(dataTableByConditionWithParams.Rows[0]["TORIHIKISAKI_NM"]);
			return true;
		}

		// Token: 0x06000081 RID: 129 RVA: 0x0002BF94 File Offset: 0x0002A194
		private bool isValidTantoshaCd()
		{
			if (!ValChk.IsNumber(this.txtTantoshaCd.Text))
			{
				Msg.Notice("数値のみで入力してください。");
				return false;
			}
			if (!ValChk.IsWithinLength(this.txtTantoshaCd.Text, this.txtTantoshaCd.MaxLength))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			if (ValChk.IsEmpty(this.txtTantoshaCd.Text))
			{
				this.txtTantoshaCd.Text = "0";
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			this.lblTantoshaNm.Text = base.Dba.GetName(base.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);
			if (this.lblTantoshaNm.Text == "")
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			this.SetBumonInfo(Util.ToInt(this.txtTantoshaCd.Text));
			return true;
		}

		// Token: 0x06000082 RID: 130 RVA: 0x000024DC File Offset: 0x000006DC
		private static bool IsDecimal(decimal dValue)
		{
			return dValue - Math.Floor(dValue) != 0m;
		}

		// Token: 0x06000083 RID: 131 RVA: 0x0002C08C File Offset: 0x0002A28C
		private bool CellKingkSet(int Row)
		{
			decimal d = Util.ToDecimal(Util.ToString(this.dgvInputList[6, Row].Value)) * Util.ToDecimal(Util.ToString(this.dgvInputList[4, Row].Value)) + Util.ToDecimal(Util.ToString(this.dgvInputList[7, Row].Value));
			decimal d2 = Util.ToDecimal(Util.ToString(this.dgvInputList[8, Row].Value));
			decimal num = d * d2;
			num = TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.KINGAKU_HASU_SHORI));
			if (!ValChk.IsDecNumWithinLength(num, 12, 0, true))
			{
				return false;
			}
			this.dgvInputList[9, Row].Value = Util.FormatNum(num);
			this.CellTaxSet(Row);
			this.DataGridSum();
			return true;
		}

		// Token: 0x06000084 RID: 132 RVA: 0x0002C178 File Offset: 0x0002A378
		private void CellTaxSet(int Row)
		{
			if (this._lastUpdCheck && this.CalcInfo.SHOHIZEI_TENKA_HOHO == 1m)
			{
				return;
			}
			int num = 0;
			decimal num2 = 0m;
			decimal num3 = 0m;
			decimal num4 = 0m;
			if (this.dgvInputList[14, Row].Value != null)
			{
				int num5 = Util.ToInt(this.dgvInputList[14, Row].Value);
				DbParamCollection dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, num5);
				DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "TB_ZM_F_ZEI_KUBUN", "ZEI_KUBUN = @ZEI_KUBUN", dbParamCollection);
				if (dataTableByConditionWithParams.Rows.Count > 0)
				{
					num = Util.ToInt(Util.ToString(dataTableByConditionWithParams.Rows[0]["KAZEI_KUBUN"]));
				}

				// TODO 2020/02/12 Add Mutu Asato
				DateTime numdateTime = Util.ConvAdDate(this.fsiDateDay.Gengo, this.fsiDateDay.WYear, this.fsiDateDay.Month, this.fsiDateDay.Day, base.Dba);

				num2 = TaxUtil.GetTaxRate(numdateTime, num5, base.Dba);

				if (Util.ToString(this.dgvInputList[11, Row].Value) != "")
				{
					num2 = Util.ToDecimal(Util.ToString(this.dgvInputList[11, Row].Value));
				}
			}
			if (num == 1)
			{
				num3 = Util.ToDecimal(Util.ToString(this.dgvInputList[9, Row].Value));
				if (this.dgvInputList[10, Row].Value != null)
				{
					num4 = Util.ToDecimal(Util.ToString(this.dgvInputList[10, Row].Value));
				}
				if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) != 1)
				{
					num4 = 0m;
				}
				int num6 = Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO);
				if (num6 == 1)
				{
					num6 = Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO);
					if (num6 != 2)
					{
						if (num6 == 3)
						{
							num4 = num3 / (100m + num2) * num2;
						}
					}
					else
					{
						num4 = num3 * num2 / 100m;
					}
				}
				this.dgvInputList[9, Row].Value = Util.FormatNum(num3);
				this.dgvInputList[10, Row].Value = Util.FormatNum(TaxUtil.CalcFraction(num4, 0, Util.ToInt(this.CalcInfo.SHOHIZEI_HASU_SHORI)));
				this.dgvInputList[11, Row].Value = num2;
				this.dgvInputList[17, Row].Value = num;
			}
			else
			{
				this.dgvInputList[10, Row].Value = null;
				this.dgvInputList[11, Row].Value = string.Empty;
				this.dgvInputList[17, Row].Value = string.Empty;
			}
			this.DataGridSum();
		}

		// Token: 0x06000085 RID: 133 RVA: 0x0002C4A8 File Offset: 0x0002A6A8
		private void CellTaxRateSet()
		{
			int num = 0;
			while (this.dgvInputList.RowCount > num)
			{
				if (this.dgvInputList[14, num].Value != null && Util.ToInt(this.dgvInputList[14, num].Value) != 0)
				{
					this.CellTaxSet(num);
				}
				num++;
			}
		}

		// Token: 0x06000086 RID: 134 RVA: 0x0002C504 File Offset: 0x0002A704
		private void TaxSorting(ref List<KBDE1011.MeisaiTbl> tb)
		{
			decimal num = 0m;
			decimal genka = 0m;
			int num2 = 0;
			List<KBDE1011.MeisaiTbl> list = new List<KBDE1011.MeisaiTbl>();
			int num3 = 0;
			tb = new List<KBDE1011.MeisaiTbl>();
			while (this.dgvInputList.RowCount > num3)
			{
				//if (!ValChk.IsEmpty(Util.ToString(this.dgvInputList[14, num3].Value)))
				//{
					KBDE1011.MeisaiTbl meisaiTbl = new KBDE1011.MeisaiTbl();
					meisaiTbl.Kingk = Util.ToDecimal(Util.ToString(this.dgvInputList[9, num3].Value));
					meisaiTbl.Zeigk = Util.ToDecimal(Util.ToString(this.dgvInputList[10, num3].Value));
					meisaiTbl.ZeiRt = Util.ToDecimal(Util.ToString(this.dgvInputList[11, num3].Value));
					meisaiTbl.Gentan = Util.ToDecimal(Util.ToString(this.dgvInputList[16, num3].Value));
					meisaiTbl.Genka = 0m;
					num = Util.ToDecimal(Util.ToString(this.dgvInputList[6, num3].Value)) * Util.ToDecimal(Util.ToString(this.dgvInputList[4, num3].Value));
					num += Util.ToDecimal(Util.ToString(this.dgvInputList[7, num3].Value));
					if (num != 0m && meisaiTbl.Gentan != 0m)
					{
						num += meisaiTbl.Gentan;
						num = TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.KINGAKU_HASU_SHORI));
						if (Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO) == 3)
						{
							num = meisaiTbl.Genka / (100m + meisaiTbl.ZeiRt) * meisaiTbl.ZeiRt;
							genka = TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.SHOHIZEI_HASU_SHORI));
							meisaiTbl.Genka = genka;
						}
					}
					if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 2 && Util.ToInt(this.dgvInputList[17, num3].Value) == 1 && meisaiTbl.Kingk != 0m && meisaiTbl.ZeiRt != 0m)
					{
						int num4 = Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO);
						if (num4 != 2)
						{
							if (num4 == 3)
							{
								num = meisaiTbl.Kingk / (100m + meisaiTbl.ZeiRt) * meisaiTbl.ZeiRt;
								meisaiTbl.Zeigk = TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.SHOHIZEI_HASU_SHORI));
							}
						}
						else
						{
							num = meisaiTbl.Kingk * meisaiTbl.ZeiRt / 100m;
							meisaiTbl.Zeigk = TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.SHOHIZEI_HASU_SHORI));
						}
						bool flag = false;
						if (num2 > 0)
						{
							for (int i = 0; i < list.Count; i++)
							{
								if (list[i].ZeiRt == Util.ToDecimal(Util.ToString(this.dgvInputList[11, num3].Value)))
								{
									list[i].Kingk += meisaiTbl.Kingk;
									list[i].Zeigk += meisaiTbl.Zeigk;
									list[i].Row = num3;
									flag = true;
									break;
								}
							}
						}
						if (!flag)
						{
							list.Add(new KBDE1011.MeisaiTbl
							{
								Kingk = meisaiTbl.Kingk,
								Zeigk = meisaiTbl.Zeigk,
								ZeiRt = meisaiTbl.ZeiRt,
								Row = num3
							});
							num2++;
						}
					}
					tb.Add(meisaiTbl);
				//}
				num3++;
			}
			if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 2 && num2 > 0)
			{
				for (int j = 0; j < list.Count; j++)
				{
					num = 0m;
					int num4 = Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO);
					if (num4 != 2)
					{
						if (num4 == 3)
						{
							num = list[j].Kingk / (100m + list[j].ZeiRt) * list[j].ZeiRt;
						}
					}
					else
					{
						num = list[j].Kingk * list[j].ZeiRt / 100m;
					}
					if (list[j].Zeigk != TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.SHOHIZEI_HASU_SHORI)))
					{
						num2 = list[j].Row;
						tb[num2].Zeigk = tb[num2].Zeigk - (list[j].Zeigk - TaxUtil.CalcFraction(num, 0, Util.ToInt(this.CalcInfo.SHOHIZEI_HASU_SHORI)));
					}
				}
			}
		}

		// Token: 0x06000087 RID: 135 RVA: 0x0002CAC4 File Offset: 0x0002ACC4
		private void SetTaxInfo()
		{
			List<KBDE1011.MeisaiTbl> list = new List<KBDE1011.MeisaiTbl>();
			this.TaxSorting(ref list);
			if (list == null)
			{
				return;
			}
			try
			{
                // 明細のリストより税率毎に集計
                List<MeisaiTbl> sumTaxTbl = (from a in list
                                             //orderby a.ZeiRt
                                             group a by new { a.ZeiRt } into g
                                             select new MeisaiTbl
                                             {
                                                 ZeiRt = g.Max(s => s.ZeiRt),
                                                 Kingk = g.Sum(s => s.Kingk),
                                                 Zeigk = g.Sum(s => s.Zeigk)
                                             }
                    ).ToList();

                string inf = string.Empty;
                for (int i = 0; i < sumTaxTbl.Count; i++)
                {
					string strMark = string.Empty;

					// TODO 2020/02/13 Add Mutu Asato つぎはぎロジックしかかけない、、、、、、
					if (null != dgvInputList[14, i])
					{
						if (dgvInputList[14, i].Value.ToString() == "50")
						{
							strMark = "*";
						}
					}

					var l = strMark + sumTaxTbl[i].ZeiRt.ToString("#0") + "%:\\" + Util.FormatNum(sumTaxTbl[i].Kingk) + "(税 \\" + Util.FormatNum(sumTaxTbl[i].Zeigk) + ")";

					inf += l + Environment.NewLine;
                }
                lblTaxInfo.Text = string.Empty;
                if (inf.Length != 0)
                    lblTaxInfo.Text = "消費税内訳" + Environment.NewLine + inf;			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		// Token: 0x06000088 RID: 136 RVA: 0x0002CC54 File Offset: 0x0002AE54
		private void TextBoxSelectAll(object sender, EventArgs e)
		{

			this.isChanged = false;

			TextBox textBox = sender as TextBox;
			if (textBox == null)
			{
				return;
			}
			textBox.SelectAll();
		}

		// Token: 0x06000089 RID: 137 RVA: 0x0002CC74 File Offset: 0x0002AE74
		private void SetBumonInfo(int TantoshaCd)
		{
			this.CalcInfo.BUMON_CD = 0;
			if (!ValChk.IsEmpty(this.txtTantoshaCd.Text))
			{
				DbParamCollection dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, base.UInfo.KaishaCd);
				dbParamCollection.SetParam("@TANTOSHA_CD", SqlDbType.Int, 4, TantoshaCd);
				DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "TB_CM_TANTOSHA", "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD", dbParamCollection);
				if (dataTableByConditionWithParams.Rows.Count != 0)
				{
					this.CalcInfo.BUMON_CD = Util.ToInt(dataTableByConditionWithParams.Rows[0]["BUMON_CD"]);
				}
			}
		}

		// Token: 0x0600008A RID: 138 RVA: 0x0002CD24 File Offset: 0x0002AF24
		private bool GetTanka(int TokuisakiCd, decimal shohinCd, ref decimal Tanka)
		{
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@SEIKYUSAKI_CD", SqlDbType.VarChar, 4, 0);
			dbParamCollection.SetParam("@TOKUISAKI_CD", SqlDbType.VarChar, 4, TokuisakiCd);
			dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, shohinCd);
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "TB_HN_URIAGE_TANKA_RIREKI", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD", "SAISHU_TORIHIKI_DATE desc", dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count != 0)
			{
				Tanka = Util.ToDecimal(dataTableByConditionWithParams.Rows[0]["TANKA"]);
				return true;
			}
			return false;
		}

		// Token: 0x0600008B RID: 139 RVA: 0x0002CE08 File Offset: 0x0002B008
		private void SetTaxInputCategory(int cat)
		{
			string name = base.Dba.GetName(base.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", this.txtMizuageShishoCd.Text, cat.ToString());
			this.lblShohizeiInputHoho2.Text = cat.ToString() + ":" + name;
		}

		// Token: 0x0600008C RID: 140 RVA: 0x0002CE5C File Offset: 0x0002B05C
		private void SetTaxShiftCategory(int cat)
		{
			string str = "";
			switch (cat)
			{
			case 1:
				str = "明細転嫁";
				break;
			case 2:
				str = "伝票転嫁";
				break;
			case 3:
				str = "請求転嫁";
				break;
			}
			this.lblShohizeiTenka2.Text = cat.ToString() + ":" + str;
		}

		// Token: 0x0600008D RID: 141 RVA: 0x0002CEB8 File Offset: 0x0002B0B8
		private bool IsCalc(int row)
		{
			return (!ValChk.IsEmpty(this.dgvInputList[6, row].Value) || !ValChk.IsEmpty(this.dgvInputList[7, row].Value)) && !ValChk.IsEmpty(this.dgvInputList[8, row].Value);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x0002CF18 File Offset: 0x0002B118
		private bool ZaikoKousin(decimal DENPYO_BANGO, int updMode)
		{
			DataTable zaikoUpdateData = this.GetZaikoUpdateData(DENPYO_BANGO);
			if (zaikoUpdateData == null || zaikoUpdateData.Rows.Count == 0)
			{
				return false;
			}
			int num = 1;
			if (updMode != 3)
			{
				num = -1;
			}
			if (Util.ToInt(zaikoUpdateData.Rows[0]["TORIHIKI_KUBUN2"]) == 2)
			{
				num = ((num == 1) ? -1 : 1);
			}
			decimal num2 = 0m;
			decimal num3 = 0m;
			int num4 = 0;
			while (zaikoUpdateData.Rows.Count > num4)
			{
				if (Util.ToInt(zaikoUpdateData.Rows[num4]["ZAIKO_KANRI_KUBUN"]) == 1)
				{
					DataTable zaiko = this.GetZaiko(Util.ToDecimal(zaikoUpdateData.Rows[num4]["SHOHIN_CD"]));
					num2 = Util.ToDecimal(Util.ToString(zaikoUpdateData.Rows[num4]["SURYO1"])) * num;
					num3 = Util.ToDecimal(Util.ToString(zaikoUpdateData.Rows[num4]["SURYO2"])) * num;
					if (zaiko.Rows.Count == 0)
					{
						this.ZaikoSu(Util.ToInt(Util.ToString(zaikoUpdateData.Rows[num4]["IRISU"])), ref num2, ref num3);
						DbParamCollection dbParamCollection = new DbParamCollection();
						dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
						dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
						dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToDecimal(zaikoUpdateData.Rows[num4]["SHOHIN_CD"]));
						dbParamCollection.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, 0);
						dbParamCollection.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(zaikoUpdateData.Rows[num4]["SURYO1"])) * num);
						dbParamCollection.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(zaikoUpdateData.Rows[num4]["SURYO2"])) * num);
						dbParamCollection.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
						base.Dba.Insert("TB_HN_ZAIKO", dbParamCollection);
					}
					else
					{
						num2 += Util.ToDecimal(Util.ToString(zaiko.Rows[0]["SURYO1"]));
						num3 += Util.ToDecimal(Util.ToString(zaiko.Rows[0]["SURYO2"]));
						this.ZaikoSu(Util.ToInt(Util.ToString(zaikoUpdateData.Rows[num4]["IRISU"])), ref num2, ref num3);
						DbParamCollection dbParamCollection = new DbParamCollection();
						DbParamCollection dbParamCollection2 = new DbParamCollection();
						dbParamCollection.SetParam("@SURYO1", SqlDbType.Decimal, 14, num2);
						dbParamCollection.SetParam("@SURYO2", SqlDbType.Decimal, 14, num3);
						dbParamCollection.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
						dbParamCollection2.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
						dbParamCollection2.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
						dbParamCollection2.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(zaikoUpdateData.Rows[num4]["SHOHIN_CD"]));
						dbParamCollection2.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, 0);
						base.Dba.Update("TB_HN_ZAIKO", dbParamCollection, "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD", dbParamCollection2);
					}
				}
				num4++;
			}
			return true;
		}

		// Token: 0x0600008F RID: 143 RVA: 0x0002D30C File Offset: 0x0002B50C
		private void ZaikoSu(int Irisu, ref decimal Suryo1, ref decimal Suryo2)
		{
			decimal num = 0m;
			if (Irisu == 0)
			{
				num = Suryo1 + Suryo2;
				Suryo1 = 0m;
				Suryo2 = num;
				return;
			}
			num = Suryo2 + Suryo1 * Irisu;
			decimal num2 = num / Irisu;
			Suryo1 = TaxUtil.CalcFraction(num2, 0, 1);
			Suryo2 = num - Suryo1 * Irisu;
		}

		// Token: 0x06000090 RID: 144 RVA: 0x0002D39C File Offset: 0x0002B59C
		private DataTable GetZaikoUpdateData(decimal DENPYO_BANGO)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT");
			stringBuilder.Append(" A.KAISHA_CD,");
			stringBuilder.Append(" A.SHISHO_CD,");
			stringBuilder.Append(" A.DENPYO_KUBUN,");
			stringBuilder.Append(" A.KAIKEI_NENDO,");
			stringBuilder.Append(" A.DENPYO_BANGO,");
			stringBuilder.Append(" A.SHOHIN_CD,");
			stringBuilder.Append(" A.TORIHIKI_KUBUN2,");
			stringBuilder.Append(" 0 AS SURYO1,");
			stringBuilder.Append(" SUM( ((A.SURYO1 * A.IRISU) + A.SURYO2) ) AS SURYO2,");
			stringBuilder.Append(" MIN( A.DENPYO_DATE ) AS DENPYO_DATE,");
			stringBuilder.Append(" MIN( B.IRISU ) AS IRISU,");
			stringBuilder.Append(" MIN( B.ZAIKO_KANRI_KUBUN ) AS ZAIKO_KANRI_KUBUN,");
			stringBuilder.Append(" ( SELECT X.URI_TANKA ");
			stringBuilder.Append("   FROM TB_HN_TORIHIKI_MEISAI AS X");
			stringBuilder.Append("   WHERE X.KAISHA_CD    = A.KAISHA_CD ");
			stringBuilder.Append("     AND X.SHISHO_CD    = A.SHISHO_CD ");
			stringBuilder.Append("     AND X.DENPYO_KUBUN = A.DENPYO_KUBUN ");
			stringBuilder.Append("     AND X.KAIKEI_NENDO = A.KAIKEI_NENDO ");
			stringBuilder.Append("     AND X.DENPYO_BANGO = A.DENPYO_BANGO ");
			stringBuilder.Append("     AND X.GYO_BANGO = ( SELECT MAX( Y.GYO_BANGO ) ");
			stringBuilder.Append("                         FROM TB_HN_TORIHIKI_MEISAI AS Y ");
			stringBuilder.Append("                         WHERE Y.KAISHA_CD = A.KAISHA_CD ");
			stringBuilder.Append("                           AND Y.SHISHO_CD = A.SHISHO_CD ");
			stringBuilder.Append("                           AND Y.DENPYO_KUBUN = A.DENPYO_KUBUN ");
			stringBuilder.Append("                           AND Y.KAIKEI_NENDO = A.KAIKEI_NENDO ");
			stringBuilder.Append("                           AND Y.DENPYO_BANGO = A.DENPYO_BANGO ");
			stringBuilder.Append("                           AND Y.SHOHIN_CD = A.SHOHIN_CD ");
			stringBuilder.Append("                           AND Y.URI_TANKA > 0 ");
			stringBuilder.Append("                         GROUP BY ");
			stringBuilder.Append("                           Y.KAISHA_CD,");
			stringBuilder.Append("                           Y.SHISHO_CD,");
			stringBuilder.Append("                           Y.DENPYO_KUBUN,");
			stringBuilder.Append("                           Y.KAIKEI_NENDO,");
			stringBuilder.Append("                           Y.DENPYO_BANGO,");
			stringBuilder.Append("                           Y.SHOHIN_CD)");
			stringBuilder.Append(" ) AS URI_TANKA ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("    VI_HN_TORIHIKI_MEISAI AS A ");
			stringBuilder.Append("LEFT OUTER JOIN ");
			stringBuilder.Append("    TB_HN_SHOHIN  AS B ");
			stringBuilder.Append("     ON A.KAISHA_CD = B.KAISHA_CD ");
			stringBuilder.Append("    AND A.SHISHO_CD = B.SHISHO_CD ");
			stringBuilder.Append("    AND A.SHOHIN_CD = B.SHOHIN_CD ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    A.KAISHA_CD      = @KAISHA_CD AND");
			stringBuilder.Append("    A.SHISHO_CD      = @SHISHO_CD AND");
			stringBuilder.Append("    A.DENPYO_KUBUN   = @DENPYO_KUBUN AND");
			stringBuilder.Append("    A.KAIKEI_NENDO   = @KAIKEI_NENDO AND");
			stringBuilder.Append("    A.DENPYO_BANGO   = @DENPYO_BANGO AND");
			stringBuilder.Append("GROUP BY ");
			stringBuilder.Append(" A.KAISHA_CD,");
			stringBuilder.Append(" A.SHISHO_CD,");
			stringBuilder.Append(" A.DENPYO_KUBUN,");
			stringBuilder.Append(" A.KAIKEI_NENDO,");
			stringBuilder.Append(" A.DENPYO_BANGO,");
			stringBuilder.Append(" A.SHOHIN_CD,");
			stringBuilder.Append(" A.TORIHIKI_KUBUN2 ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);



			return base.Dba.GetDataTableFromSqlWithParams(Util.ToString(stringBuilder), dbParamCollection);
		}

		// Token: 0x06000091 RID: 145 RVA: 0x0002D704 File Offset: 0x0002B904
		private DataTable GetZaiko(decimal shohinCd)
		{
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, shohinCd);
			dbParamCollection.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, 0);
			return base.Dba.GetDataTableByConditionWithParams("*", "TB_HN_ZAIKO", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD", dbParamCollection);
		}

		// Token: 0x06000092 RID: 146 RVA: 0x0002D798 File Offset: 0x0002B998
		private DialogResult ConfPastZeiKbn(DateTime baseDate, string zeiKbn, ref decimal zeiritsu, decimal taxRate)
		{
			zeiritsu = 0m;
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, zeiKbn);
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "TB_ZM_F_ZEI_KUBUN", "ZEI_KUBUN = @ZEI_KUBUN", dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count == 0)
			{
				return DialogResult.No;
			}
			if (Util.ToInt(dataTableByConditionWithParams.Rows[0]["KAZEI_KUBUN"]) != 1)
			{
				return DialogResult.OK;
			}
			decimal zeiritsu2 = base.GetZeiritsu(baseDate);
			dbParamCollection = new DbParamCollection();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT dbo.FNC_GetTaxRate( @ZEI_KUBUN, @DENPYO_DATE )");
			dbParamCollection.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, zeiKbn);
			dbParamCollection.SetParam("@DENPYO_DATE", SqlDbType.DateTime, baseDate);
			DataTable dataTableFromSqlWithParams = base.Dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
			decimal num = 0m;
			if (dataTableFromSqlWithParams.Rows.Count == 0)
			{
				num = 0m;
			}
			else
			{
				num = Util.ToDecimal(dataTableFromSqlWithParams.Rows[0][0]);
			}
			if (taxRate != -1m)
			{
				num = taxRate;
			}
			if (zeiritsu2 != num)
			{
				DialogResult dialogResult = Msg.ConfOKCancel("基準日時点の税率と異なる税率を持つ税区分が選択されています。" + Environment.NewLine + "処理を続行してよろしいですか？");
				if (dialogResult == DialogResult.OK)
				{
					zeiritsu = num;
				}
				return dialogResult;
			}
			zeiritsu = num;
			return DialogResult.OK;
		}

		// Token: 0x06000093 RID: 147 RVA: 0x000024F9 File Offset: 0x000006F9
		private new DialogResult ConfPastZeiKbn(DateTime baseDate, string zeiKbn, ref decimal zeiritsu)
		{
			return this.ConfPastZeiKbn(baseDate, zeiKbn, ref zeiritsu, -1m);
		}

		// Token: 0x06000094 RID: 148 RVA: 0x0002D8E0 File Offset: 0x0002BAE0
		private DataRow GetPersonInfo(string code)
		{
			DataRow result = null;
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "TB_CM_TANTOSHA", "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ", dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count != 0)
			{
				result = dataTableByConditionWithParams.Rows[0];
			}
			return result;
		}

		// Token: 0x0400028E RID: 654
		private int MODE_EDIT = 1;

		// Token: 0x0400028F RID: 655
		private int IS_INPUT = 1;

		// Token: 0x04000290 RID: 656
		private MeisaiCalcInfo CalcInfo;

		// Token: 0x04000291 RID: 657
		private string DenpyoCondition = string.Empty;

		// Token: 0x04000292 RID: 658
		private const int COL_GYO_NO = 0;

		// Token: 0x04000293 RID: 659
		private const int COL_KUBUN = 1;

		// Token: 0x04000294 RID: 660
		private const int COL_SHOHIN_CD = 2;

		// Token: 0x04000295 RID: 661
		private const int COL_SHOHIN_NM = 3;

		// Token: 0x04000296 RID: 662
		private const int COL_IRISU = 4;

		// Token: 0x04000297 RID: 663
		private const int COL_TANI = 5;

		// Token: 0x04000298 RID: 664
		private const int COL_SURYO1 = 6;

		// Token: 0x04000299 RID: 665
		private const int COL_SURYO2 = 7;

		// Token: 0x0400029A RID: 666
		private const int COL_URI_TANKA = 8;

		// Token: 0x0400029B RID: 667
		private const int COL_BAIKA_KINGAKU = 9;

		// Token: 0x0400029C RID: 668
		private const int COL_SHOHIZEI = 10;

		// Token: 0x0400029D RID: 669
		private const int COL_ZEI_RITSU = 11;

		// Token: 0x0400029E RID: 670
		private const int COL_SHIWAKE_CD = 12;

		// Token: 0x0400029F RID: 671
		private const int COL_BUMON_CD = 13;

		// Token: 0x040002A0 RID: 672
		private const int COL_ZEI_KUBUN = 14;

		// Token: 0x040002A1 RID: 673
		private const int COL_JIGYO_KUBUN = 15;

		// Token: 0x040002A2 RID: 674
		private const int COL_GEN_TANKA = 16;

		// Token: 0x040002A3 RID: 675
		private const int COL_KAZEI_KUBUN = 17;

		// Token: 0x040002A4 RID: 676
		private const int COL_ZAIKO_KANRI_KUBUN = 18;

		// Token: 0x040002A5 RID: 677
		private bool _personCheck;

		// Token: 0x040002A6 RID: 678
		private bool _lastUpdCheck;

		// Token: 0x040002A7 RID: 679
		private int HIN_CHUSHI_KUBUN;

		// Token: 0x040002A8 RID: 680
		private int SHIFT_CATEGORY_SLIP_TOTAL;

		// Token: 0x040002A9 RID: 681
		private int ROUND_CATEGORY_DOWN;

		// Token: 0x040002AA RID: 682
		private int _zaikoDisplay = 1;

		// Token: 0x040002AB RID: 683
		private Dictionary<string, decimal> _itemStock;

		// Token: 0x040002AC RID: 684
		private int _torihikiKubun2 = 1;

		// Token: 0x040002AD RID: 685
		private decimal MEISAI_DENPYO_BANGO;

		// Token: 0x0200000B RID: 11
		private class MeisaiTbl
		{
			// Token: 0x17000002 RID: 2
			// (get) Token: 0x06000097 RID: 151 RVA: 0x00002528 File Offset: 0x00000728
			// (set) Token: 0x06000098 RID: 152 RVA: 0x00002530 File Offset: 0x00000730
			public int Row { get; set; }

			// Token: 0x17000003 RID: 3
			// (get) Token: 0x06000099 RID: 153 RVA: 0x00002539 File Offset: 0x00000739
			// (set) Token: 0x0600009A RID: 154 RVA: 0x00002541 File Offset: 0x00000741
			public decimal Kingk { get; set; }

			// Token: 0x17000004 RID: 4
			// (get) Token: 0x0600009B RID: 155 RVA: 0x0000254A File Offset: 0x0000074A
			// (set) Token: 0x0600009C RID: 156 RVA: 0x00002552 File Offset: 0x00000752
			public decimal Zeigk { get; set; }

			// Token: 0x17000005 RID: 5
			// (get) Token: 0x0600009D RID: 157 RVA: 0x0000255B File Offset: 0x0000075B
			// (set) Token: 0x0600009E RID: 158 RVA: 0x00002563 File Offset: 0x00000763
			public decimal ZeiRt { get; set; }

			// Token: 0x17000006 RID: 6
			// (get) Token: 0x0600009F RID: 159 RVA: 0x0000256C File Offset: 0x0000076C
			// (set) Token: 0x060000A0 RID: 160 RVA: 0x00002574 File Offset: 0x00000774
			public decimal Gentan { get; set; }

			// Token: 0x17000007 RID: 7
			// (get) Token: 0x060000A1 RID: 161 RVA: 0x0000257D File Offset: 0x0000077D
			// (set) Token: 0x060000A2 RID: 162 RVA: 0x00002585 File Offset: 0x00000785
			public decimal Genka { get; set; }

			public decimal Zeikbn { get; set; }

		}

		private void txt_Enter(object sender, EventArgs e)
		{
			// 入力チェック
			this.isChanged = false;
		}

		private void txt_TextChanged(object sender, EventArgs e)
		{
			// 入力チェック
			this.isChanged = true;
		}

		private void txtGridEdit_TextChanged(object sender, EventArgs e)
		{
			this.isChanged = true;
		}
	}
}
