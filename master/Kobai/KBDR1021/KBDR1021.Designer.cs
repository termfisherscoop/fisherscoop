﻿namespace jp.co.fsi.kb.kbdr1021
{
    partial class KBDR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTantoCdBet = new System.Windows.Forms.Label();
            this.lblTantoCdFr = new System.Windows.Forms.Label();
            this.lblTantoCdTo = new System.Windows.Forms.Label();
            this.txtTantoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTantoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.lblSenshuCdBet = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.cmbToriKbn = new System.Windows.Forms.ComboBox();
            this.rdoMeisai = new System.Windows.Forms.RadioButton();
            this.rdoGokei = new System.Windows.Forms.RadioButton();
            this.lblJpDayFrTo = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblJpDayFrFr = new System.Windows.Forms.Label();
            this.lblJpMonthTo = new System.Windows.Forms.Label();
            this.lblJpMonthFr = new System.Windows.Forms.Label();
            this.lblJpearTo = new System.Windows.Forms.Label();
            this.txtJpDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpearFr = new System.Windows.Forms.Label();
            this.txtJpMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpTo = new System.Windows.Forms.Label();
            this.lblJpFr = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDenpyoNoFr = new System.Windows.Forms.Label();
            this.lblDenpyoNoTo = new System.Windows.Forms.Label();
            this.sjTxtDenpyoNoTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.sjTxtDenpyoNoFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "";
            // 
            // lblTantoCdBet
            // 
            this.lblTantoCdBet.BackColor = System.Drawing.Color.Silver;
            this.lblTantoCdBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoCdBet.Location = new System.Drawing.Point(504, 2);
            this.lblTantoCdBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoCdBet.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblTantoCdBet.Name = "lblTantoCdBet";
            this.lblTantoCdBet.Size = new System.Drawing.Size(23, 32);
            this.lblTantoCdBet.TabIndex = 2;
            this.lblTantoCdBet.Tag = "CHANGE";
            this.lblTantoCdBet.Text = "～";
            this.lblTantoCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTantoCdFr
            // 
            this.lblTantoCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblTantoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoCdFr.Location = new System.Drawing.Point(224, 2);
            this.lblTantoCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoCdFr.Name = "lblTantoCdFr";
            this.lblTantoCdFr.Size = new System.Drawing.Size(272, 24);
            this.lblTantoCdFr.TabIndex = 1;
            this.lblTantoCdFr.Tag = "DISPNAME";
            this.lblTantoCdFr.Text = "先　頭";
            this.lblTantoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoCdTo
            // 
            this.lblTantoCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblTantoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoCdTo.Location = new System.Drawing.Point(609, 2);
            this.lblTantoCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoCdTo.Name = "lblTantoCdTo";
            this.lblTantoCdTo.Size = new System.Drawing.Size(272, 24);
            this.lblTantoCdTo.TabIndex = 4;
            this.lblTantoCdTo.Tag = "DISPNAME";
            this.lblTantoCdTo.Text = "最　後";
            this.lblTantoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoCdTo
            // 
            this.txtTantoCdTo.AutoSizeFromLength = false;
            this.txtTantoCdTo.DisplayLength = null;
            this.txtTantoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoCdTo.Location = new System.Drawing.Point(533, 3);
            this.txtTantoCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTantoCdTo.MaxLength = 4;
            this.txtTantoCdTo.Name = "txtTantoCdTo";
            this.txtTantoCdTo.Size = new System.Drawing.Size(65, 23);
            this.txtTantoCdTo.TabIndex = 3;
            this.txtTantoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCdTo_Validating);
            // 
            // txtTantoCdFr
            // 
            this.txtTantoCdFr.AutoSizeFromLength = false;
            this.txtTantoCdFr.DisplayLength = null;
            this.txtTantoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoCdFr.Location = new System.Drawing.Point(148, 3);
            this.txtTantoCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtTantoCdFr.MaxLength = 4;
            this.txtTantoCdFr.Name = "txtTantoCdFr";
            this.txtTantoCdFr.Size = new System.Drawing.Size(65, 23);
            this.txtTantoCdFr.TabIndex = 0;
            this.txtTantoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCdFr_Validating);
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(148, 4);
            this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(65, 23);
            this.txtFunanushiCdFr.TabIndex = 0;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(224, 3);
            this.lblFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(272, 24);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Tag = "DISPNAME";
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSenshuCdBet
            // 
            this.lblSenshuCdBet.BackColor = System.Drawing.Color.Silver;
            this.lblSenshuCdBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSenshuCdBet.Location = new System.Drawing.Point(505, 3);
            this.lblSenshuCdBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSenshuCdBet.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblSenshuCdBet.Name = "lblSenshuCdBet";
            this.lblSenshuCdBet.Size = new System.Drawing.Size(23, 32);
            this.lblSenshuCdBet.TabIndex = 2;
            this.lblSenshuCdBet.Tag = "CHANGE";
            this.lblSenshuCdBet.Text = "～";
            this.lblSenshuCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(535, 4);
            this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(65, 23);
            this.txtFunanushiCdTo.TabIndex = 3;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(611, 3);
            this.lblFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(272, 24);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Tag = "DISPNAME";
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(148, 4);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(195, 3);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 907;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbToriKbn
            // 
            this.cmbToriKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToriKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbToriKbn.FormattingEnabled = true;
            this.cmbToriKbn.Location = new System.Drawing.Point(148, 3);
            this.cmbToriKbn.Margin = new System.Windows.Forms.Padding(4);
            this.cmbToriKbn.Name = "cmbToriKbn";
            this.cmbToriKbn.Size = new System.Drawing.Size(161, 24);
            this.cmbToriKbn.TabIndex = 1;
            // 
            // rdoMeisai
            // 
            this.rdoMeisai.AutoSize = true;
            this.rdoMeisai.BackColor = System.Drawing.Color.Silver;
            this.rdoMeisai.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoMeisai.Location = new System.Drawing.Point(252, -1);
            this.rdoMeisai.Margin = new System.Windows.Forms.Padding(4);
            this.rdoMeisai.MinimumSize = new System.Drawing.Size(0, 32);
            this.rdoMeisai.Name = "rdoMeisai";
            this.rdoMeisai.Size = new System.Drawing.Size(58, 32);
            this.rdoMeisai.TabIndex = 2;
            this.rdoMeisai.Tag = "CHANGE";
            this.rdoMeisai.Text = "明細";
            this.rdoMeisai.UseVisualStyleBackColor = false;
            // 
            // rdoGokei
            // 
            this.rdoGokei.AutoSize = true;
            this.rdoGokei.BackColor = System.Drawing.Color.Silver;
            this.rdoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoGokei.Location = new System.Drawing.Point(148, -1);
            this.rdoGokei.Margin = new System.Windows.Forms.Padding(4);
            this.rdoGokei.MinimumSize = new System.Drawing.Size(0, 32);
            this.rdoGokei.Name = "rdoGokei";
            this.rdoGokei.Size = new System.Drawing.Size(58, 32);
            this.rdoGokei.TabIndex = 1;
            this.rdoGokei.Tag = "CHANGE";
            this.rdoGokei.Text = "合計";
            this.rdoGokei.UseVisualStyleBackColor = false;
            // 
            // lblJpDayFrTo
            // 
            this.lblJpDayFrTo.AutoSize = true;
            this.lblJpDayFrTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayFrTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpDayFrTo.Location = new System.Drawing.Point(833, 0);
            this.lblJpDayFrTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpDayFrTo.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpDayFrTo.Name = "lblJpDayFrTo";
            this.lblJpDayFrTo.Size = new System.Drawing.Size(24, 32);
            this.lblJpDayFrTo.TabIndex = 14;
            this.lblJpDayFrTo.Tag = "CHANGE";
            this.lblJpDayFrTo.Text = "日";
            this.lblJpDayFrTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(496, 0);
            this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBetDate.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(24, 32);
            this.lblCodeBetDate.TabIndex = 7;
            this.lblCodeBetDate.Tag = "CHANGE";
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpDayFrFr
            // 
            this.lblJpDayFrFr.AutoSize = true;
            this.lblJpDayFrFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayFrFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpDayFrFr.Location = new System.Drawing.Point(448, -1);
            this.lblJpDayFrFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpDayFrFr.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpDayFrFr.Name = "lblJpDayFrFr";
            this.lblJpDayFrFr.Size = new System.Drawing.Size(24, 32);
            this.lblJpDayFrFr.TabIndex = 6;
            this.lblJpDayFrFr.Tag = "CHANGE";
            this.lblJpDayFrFr.Text = "日";
            this.lblJpDayFrFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpMonthTo
            // 
            this.lblJpMonthTo.AutoSize = true;
            this.lblJpMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpMonthTo.Location = new System.Drawing.Point(743, 0);
            this.lblJpMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpMonthTo.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpMonthTo.Name = "lblJpMonthTo";
            this.lblJpMonthTo.Size = new System.Drawing.Size(24, 32);
            this.lblJpMonthTo.TabIndex = 12;
            this.lblJpMonthTo.Tag = "CHANGE";
            this.lblJpMonthTo.Text = "月";
            this.lblJpMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpMonthFr
            // 
            this.lblJpMonthFr.AutoSize = true;
            this.lblJpMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpMonthFr.Location = new System.Drawing.Point(356, -1);
            this.lblJpMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpMonthFr.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpMonthFr.Name = "lblJpMonthFr";
            this.lblJpMonthFr.Size = new System.Drawing.Size(24, 32);
            this.lblJpMonthFr.TabIndex = 4;
            this.lblJpMonthFr.Tag = "CHANGE";
            this.lblJpMonthFr.Text = "月";
            this.lblJpMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpearTo
            // 
            this.lblJpearTo.AutoSize = true;
            this.lblJpearTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpearTo.Location = new System.Drawing.Point(651, 0);
            this.lblJpearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpearTo.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpearTo.Name = "lblJpearTo";
            this.lblJpearTo.Size = new System.Drawing.Size(24, 32);
            this.lblJpearTo.TabIndex = 10;
            this.lblJpearTo.Tag = "CHANGE";
            this.lblJpearTo.Text = "年";
            this.lblJpearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJpDayTo
            // 
            this.txtJpDayTo.AutoSizeFromLength = false;
            this.txtJpDayTo.DisplayLength = null;
            this.txtJpDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpDayTo.Location = new System.Drawing.Point(777, 4);
            this.txtJpDayTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpDayTo.MaxLength = 2;
            this.txtJpDayTo.Name = "txtJpDayTo";
            this.txtJpDayTo.Size = new System.Drawing.Size(52, 23);
            this.txtJpDayTo.TabIndex = 13;
            this.txtJpDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtJpDayFr
            // 
            this.txtJpDayFr.AutoSizeFromLength = false;
            this.txtJpDayFr.DisplayLength = null;
            this.txtJpDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpDayFr.Location = new System.Drawing.Point(391, 4);
            this.txtJpDayFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpDayFr.MaxLength = 2;
            this.txtJpDayFr.Name = "txtJpDayFr";
            this.txtJpDayFr.Size = new System.Drawing.Size(52, 23);
            this.txtJpDayFr.TabIndex = 5;
            this.txtJpDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblJpearFr
            // 
            this.lblJpearFr.AutoSize = true;
            this.lblJpearFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpearFr.Location = new System.Drawing.Point(265, -1);
            this.lblJpearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpearFr.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpearFr.Name = "lblJpearFr";
            this.lblJpearFr.Size = new System.Drawing.Size(24, 32);
            this.lblJpearFr.TabIndex = 2;
            this.lblJpearFr.Tag = "CHANGE";
            this.lblJpearFr.Text = "年";
            this.lblJpearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJpMonthTo
            // 
            this.txtJpMonthTo.AutoSizeFromLength = false;
            this.txtJpMonthTo.DisplayLength = null;
            this.txtJpMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthTo.Location = new System.Drawing.Point(685, 4);
            this.txtJpMonthTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpMonthTo.MaxLength = 2;
            this.txtJpMonthTo.Name = "txtJpMonthTo";
            this.txtJpMonthTo.Size = new System.Drawing.Size(52, 23);
            this.txtJpMonthTo.TabIndex = 11;
            this.txtJpMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // txtJpMonthFr
            // 
            this.txtJpMonthFr.AutoSizeFromLength = false;
            this.txtJpMonthFr.DisplayLength = null;
            this.txtJpMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthFr.Location = new System.Drawing.Point(299, 4);
            this.txtJpMonthFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpMonthFr.MaxLength = 2;
            this.txtJpMonthFr.Name = "txtJpMonthFr";
            this.txtJpMonthFr.Size = new System.Drawing.Size(52, 23);
            this.txtJpMonthFr.TabIndex = 3;
            this.txtJpMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtJpYearTo
            // 
            this.txtJpYearTo.AutoSizeFromLength = false;
            this.txtJpYearTo.DisplayLength = null;
            this.txtJpYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearTo.Location = new System.Drawing.Point(593, 4);
            this.txtJpYearTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpYearTo.MaxLength = 2;
            this.txtJpYearTo.Name = "txtJpYearTo";
            this.txtJpYearTo.Size = new System.Drawing.Size(52, 23);
            this.txtJpYearTo.TabIndex = 9;
            this.txtJpYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtJpYearFr
            // 
            this.txtJpYearFr.AutoSizeFromLength = false;
            this.txtJpYearFr.DisplayLength = null;
            this.txtJpYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearFr.Location = new System.Drawing.Point(208, 4);
            this.txtJpYearFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpYearFr.MaxLength = 2;
            this.txtJpYearFr.Name = "txtJpYearFr";
            this.txtJpYearFr.Size = new System.Drawing.Size(52, 23);
            this.txtJpYearFr.TabIndex = 1;
            this.txtJpYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // lblJpTo
            // 
            this.lblJpTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblJpTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpTo.Location = new System.Drawing.Point(533, 3);
            this.lblJpTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpTo.Name = "lblJpTo";
            this.lblJpTo.Size = new System.Drawing.Size(53, 24);
            this.lblJpTo.TabIndex = 8;
            this.lblJpTo.Tag = "DISPNAME";
            this.lblJpTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJpFr
            // 
            this.lblJpFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblJpFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpFr.Location = new System.Drawing.Point(148, 3);
            this.lblJpFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpFr.Name = "lblJpFr";
            this.lblJpFr.Size = new System.Drawing.Size(53, 24);
            this.lblJpFr.TabIndex = 0;
            this.lblJpFr.Tag = "DISPNAME";
            this.lblJpFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(504, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.MinimumSize = new System.Drawing.Size(0, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 32);
            this.label1.TabIndex = 2;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "～";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDenpyoNoFr
            // 
            this.lblDenpyoNoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblDenpyoNoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoNoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoNoFr.Location = new System.Drawing.Point(224, 2);
            this.lblDenpyoNoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDenpyoNoFr.Name = "lblDenpyoNoFr";
            this.lblDenpyoNoFr.Size = new System.Drawing.Size(272, 24);
            this.lblDenpyoNoFr.TabIndex = 1;
            this.lblDenpyoNoFr.Tag = "DISPNAME";
            this.lblDenpyoNoFr.Text = "先　頭";
            this.lblDenpyoNoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenpyoNoTo
            // 
            this.lblDenpyoNoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblDenpyoNoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoNoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoNoTo.Location = new System.Drawing.Point(609, 2);
            this.lblDenpyoNoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDenpyoNoTo.Name = "lblDenpyoNoTo";
            this.lblDenpyoNoTo.Size = new System.Drawing.Size(272, 24);
            this.lblDenpyoNoTo.TabIndex = 4;
            this.lblDenpyoNoTo.Tag = "DISPNAME";
            this.lblDenpyoNoTo.Text = "最　後";
            this.lblDenpyoNoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sjTxtDenpyoNoTo
            // 
            this.sjTxtDenpyoNoTo.AutoSizeFromLength = false;
            this.sjTxtDenpyoNoTo.DisplayLength = null;
            this.sjTxtDenpyoNoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.sjTxtDenpyoNoTo.Location = new System.Drawing.Point(533, 4);
            this.sjTxtDenpyoNoTo.Margin = new System.Windows.Forms.Padding(4);
            this.sjTxtDenpyoNoTo.MaxLength = 6;
            this.sjTxtDenpyoNoTo.Name = "sjTxtDenpyoNoTo";
            this.sjTxtDenpyoNoTo.Size = new System.Drawing.Size(65, 23);
            this.sjTxtDenpyoNoTo.TabIndex = 3;
            this.sjTxtDenpyoNoTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.sjTxtDenpyoNoTo.Validating += new System.ComponentModel.CancelEventHandler(this.TxtDenpyoNoTo_Validating);
            // 
            // sjTxtDenpyoNoFr
            // 
            this.sjTxtDenpyoNoFr.AutoSizeFromLength = false;
            this.sjTxtDenpyoNoFr.DisplayLength = null;
            this.sjTxtDenpyoNoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.sjTxtDenpyoNoFr.Location = new System.Drawing.Point(148, 4);
            this.sjTxtDenpyoNoFr.Margin = new System.Windows.Forms.Padding(4);
            this.sjTxtDenpyoNoFr.MaxLength = 6;
            this.sjTxtDenpyoNoFr.Name = "sjTxtDenpyoNoFr";
            this.sjTxtDenpyoNoFr.Size = new System.Drawing.Size(65, 23);
            this.sjTxtDenpyoNoFr.TabIndex = 0;
            this.sjTxtDenpyoNoFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.sjTxtDenpyoNoFr.Validating += new System.ComponentModel.CancelEventHandler(this.TxtDenpyoNoFr_Validating);
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 7;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.8F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(921, 275);
            this.fsiTableLayoutPanel1.TabIndex = 908;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.txtFunanushiCdFr);
            this.fsiPanel7.Controls.Add(this.lblFunanushiCdTo);
            this.fsiPanel7.Controls.Add(this.txtFunanushiCdTo);
            this.fsiPanel7.Controls.Add(this.lblFunanushiCdFr);
            this.fsiPanel7.Controls.Add(this.lblSenshuCdBet);
            this.fsiPanel7.Controls.Add(this.label8);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(5, 233);
            this.fsiPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(911, 37);
            this.fsiPanel7.TabIndex = 6;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.MinimumSize = new System.Drawing.Size(0, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(911, 37);
            this.label8.TabIndex = 907;
            this.label8.Tag = "CHANGE";
            this.label8.Text = "船主CD範囲";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtTantoCdFr);
            this.fsiPanel6.Controls.Add(this.txtTantoCdTo);
            this.fsiPanel6.Controls.Add(this.lblTantoCdTo);
            this.fsiPanel6.Controls.Add(this.lblTantoCdFr);
            this.fsiPanel6.Controls.Add(this.lblTantoCdBet);
            this.fsiPanel6.Controls.Add(this.label7);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(5, 195);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(911, 29);
            this.fsiPanel6.TabIndex = 5;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.MinimumSize = new System.Drawing.Size(0, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(911, 32);
            this.label7.TabIndex = 907;
            this.label7.Tag = "CHANGE";
            this.label7.Text = "担当者CD範囲";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.sjTxtDenpyoNoFr);
            this.fsiPanel5.Controls.Add(this.sjTxtDenpyoNoTo);
            this.fsiPanel5.Controls.Add(this.lblDenpyoNoTo);
            this.fsiPanel5.Controls.Add(this.lblDenpyoNoFr);
            this.fsiPanel5.Controls.Add(this.label1);
            this.fsiPanel5.Controls.Add(this.label6);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(5, 157);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(911, 29);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.MinimumSize = new System.Drawing.Size(0, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(911, 32);
            this.label6.TabIndex = 907;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "伝票番号範囲";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.cmbToriKbn);
            this.fsiPanel4.Controls.Add(this.label5);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 119);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(911, 29);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.MinimumSize = new System.Drawing.Size(0, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(911, 32);
            this.label5.TabIndex = 907;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "表示方法";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.rdoGokei);
            this.fsiPanel3.Controls.Add(this.rdoMeisai);
            this.fsiPanel3.Controls.Add(this.label4);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 81);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(911, 29);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(911, 32);
            this.label4.TabIndex = 907;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "取引区分";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblJpFr);
            this.fsiPanel2.Controls.Add(this.lblJpTo);
            this.fsiPanel2.Controls.Add(this.txtJpYearFr);
            this.fsiPanel2.Controls.Add(this.lblJpDayFrTo);
            this.fsiPanel2.Controls.Add(this.txtJpYearTo);
            this.fsiPanel2.Controls.Add(this.lblCodeBetDate);
            this.fsiPanel2.Controls.Add(this.txtJpMonthFr);
            this.fsiPanel2.Controls.Add(this.lblJpDayFrFr);
            this.fsiPanel2.Controls.Add(this.txtJpMonthTo);
            this.fsiPanel2.Controls.Add(this.lblJpMonthTo);
            this.fsiPanel2.Controls.Add(this.lblJpearFr);
            this.fsiPanel2.Controls.Add(this.lblJpMonthFr);
            this.fsiPanel2.Controls.Add(this.txtJpDayFr);
            this.fsiPanel2.Controls.Add(this.lblJpearTo);
            this.fsiPanel2.Controls.Add(this.txtJpDayTo);
            this.fsiPanel2.Controls.Add(this.label3);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 43);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(911, 29);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(911, 32);
            this.label3.TabIndex = 907;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "日付範囲";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label2);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(911, 29);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(911, 32);
            this.label2.TabIndex = 907;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "支所";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDR1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 745);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBDR1021";
            this.Par1 = "1";
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblTantoCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoCdFr;
        private System.Windows.Forms.Label lblTantoCdBet;
        private System.Windows.Forms.Label lblTantoCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private System.Windows.Forms.Label lblSenshuCdBet;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.ComboBox cmbToriKbn;
        private System.Windows.Forms.RadioButton rdoMeisai;
        private System.Windows.Forms.RadioButton rdoGokei;
        private System.Windows.Forms.Label lblJpDayFrTo;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblJpDayFrFr;
        private System.Windows.Forms.Label lblJpMonthTo;
        private System.Windows.Forms.Label lblJpMonthFr;
        private System.Windows.Forms.Label lblJpearTo;
        private common.controls.FsiTextBox txtJpDayTo;
        private common.controls.FsiTextBox txtJpDayFr;
        private System.Windows.Forms.Label lblJpearFr;
        private common.controls.FsiTextBox txtJpMonthTo;
        private common.controls.FsiTextBox txtJpMonthFr;
        private common.controls.FsiTextBox txtJpYearTo;
        private common.controls.FsiTextBox txtJpYearFr;
        private System.Windows.Forms.Label lblJpTo;
        private System.Windows.Forms.Label lblJpFr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDenpyoNoFr;
        private System.Windows.Forms.Label lblDenpyoNoTo;
        private common.controls.FsiTextBox sjTxtDenpyoNoTo;
        private common.controls.FsiTextBox sjTxtDenpyoNoFr;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel7;
        private System.Windows.Forms.Label label8;
        private common.FsiPanel fsiPanel6;
        private System.Windows.Forms.Label label7;
        private common.FsiPanel fsiPanel5;
        private System.Windows.Forms.Label label6;
        private common.FsiPanel fsiPanel4;
        private System.Windows.Forms.Label label5;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label4;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label3;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label2;
    }
}