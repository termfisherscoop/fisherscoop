﻿using System.Data;
using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr1021
{
    /// <summary>
    /// KBDR1021R の帳票
    /// </summary>
    public partial class KBDR1021R : BaseReport
    {

        public KBDR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {
            //if (Util.ToInt(this.nowRouNo.Text) % 48 == 0 || this.nowRouNo.Text == this.totalRowNo.Text)
            //{
            //    this.detailLine.Visible = false;
            //    this.lastLine.Visible = true;
            //}
            //else
            //{
            //    this.detailLine.Visible = true;
            //    this.lastLine.Visible = false;
            //}
        }

        private void pageFooter_Format(object sender, System.EventArgs e)
        {

        }
    }
}
