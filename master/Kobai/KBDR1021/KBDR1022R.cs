﻿using System.Data;
using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr1021
{
    /// <summary>
    /// KBDR1022R の帳票
    /// </summary>
    public partial class KBDR1022R : BaseReport
    {

        public KBDR1022R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {

            if (Util.ToInt(this.judgeNo.Text) == 0)
            {
                this.detailLine.Visible = true;
                this.lastLine.Visible = false;
            }
            else
            {
                this.detailLine.Visible = false;
                this.lastLine.Visible = true;
            }
        }
        private void KBDR1022R_ReportStart(object sender, System.EventArgs e)
        {

        }

        private void pageFooter_Format(object sender, System.EventArgs e)
        {

        }

        private void groupFooter1_Format(object sender, System.EventArgs e)
        {

        }
    }
}
