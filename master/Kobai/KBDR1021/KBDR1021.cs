﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr1021
{
    /// <summary>
    /// 売上一覧(KBDR1021)
    /// </summary>
    public partial class KBDR1021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 燃料売上
        /// </summary>
        private const string NENRYO_URIAGE = "11";

        /// <summary>
        /// 現金売上
        /// </summary>
        private const string GENKIN_URIAGE = "21";

        /// <summary>
        /// 資材売上
        /// </summary>
        private const string SHIZAI_URIAGE = "31";

        /// <summary>
        /// 資材返品
        /// </summary>
        private const string SHIZAI_HENPIN = "32";

        /// <summary>
        /// 製氷売上
        /// </summary>
        private const string SEIHYO_URIAGE = "41";
       
        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols1 = 11;
        private const int prtCols2 = 33; //19→29に変更
        #endregion

        #region 変数
        /// <summary>
        /// 掛け
        /// </summary>
        private string KAKE = "1";
        /// <summary>
        /// 掛け名称
        /// </summary>
        private string KAKENM = "掛";

        /// <summary>
        /// 現金
        /// </summary>
        private string GENKIN = "2";
        /// <summary>
        /// 現金名称
        /// </summary>
        private string GENKINNM = "現金";

        ///// <summary>
        ///// 資材
        ///// </summary>
        //private string SHIZAI = "3";
        ///// <summary>
        ///// 資材名称
        ///// </summary>
        //private string SHIZAINM = "燃料";

        /// <summary>
        /// 返品
        /// </summary>
        private string HENPIN = "2";

        private int DENPYO_KUBUN = 1;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        /// <summary>
        /// 消費税額取得用
        /// </summary>
        private Decimal[] _dtDenpyoGokeiGaku = new Decimal[3];
        public Decimal[] DenpyoGokei
        {
            get
            {
                return this._dtDenpyoGokeiGaku;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDR1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 設定読み込み
            try
            {
                KAKE = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1021", "Setting", "KAKE");
                KAKENM = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1021", "Setting", "KAKENM");
                GENKIN = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1021", "Setting", "GENKIN");
                GENKINNM = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1021", "Setting", "GENKINNM");
                //SHIZAI = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1021", "Setting", "GENKIN");
                //SHIZAINM = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1021", "Setting", "GENKINNM");
                HENPIN = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1021", "Setting", "HENPIN");
            }
            catch (Exception)
            {
                KAKE = "1";
                KAKENM = "掛";
                GENKIN = "2";
                GENKINNM = "現金";
                //SHIZAI = "3";
                //SHIZAINM = "燃料";
                HENPIN = "2";
            }

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblJpFr.Text = jpDate[0];
            txtJpYearFr.Text = jpDate[2];
            txtJpMonthFr.Text = jpDate[3];
            txtJpDayFr.Text = jpDate[4];
            lblJpTo.Text = jpDate[0];
            txtJpYearTo.Text = jpDate[2];
            txtJpMonthTo.Text = jpDate[3];
            txtJpDayTo.Text = jpDate[4];

            // 取引区分設定
            cmbToriKbn.Items.Add("全て");
            cmbToriKbn.Items.Add(KAKENM + "取引");
            cmbToriKbn.Items.Add(GENKINNM + "取引");
            //cmbToriKbn.Items.Add(SHIZAINM + "取引");
            cmbToriKbn.SelectedIndex = 0;

            //伝票区分退避
            DENPYO_KUBUN = Util.ToInt(this.Par1);

            if (DENPYO_KUBUN == 1)
            {
                this.label8.Text = "船主CD範囲";

            }
            else
            {
                this.label8.Text = "仕入先CD範囲";
            }

            // 初期フォーカス
            txtJpYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、担当者コード１・２、船主コード１・２に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtJpYearFr":
                case "txtJpYearTo":
                case "txtTantoCdFr":
                case "txtTantoCdTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTantoCdFr":
                    #region 担当者CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoCdFr.Text = outData[0];
                                this.lblTantoCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTantoCdTo":
                    #region 担当者CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoCdTo.Text = outData[0];
                                this.lblTantoCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdFr":
                    #region 船主CD
                    if (DENPYO_KUBUN == 1)
                    {
                        // アセンブリのロード
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+ "CMCM2011.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    }
                    else
                    {
                        // アセンブリのロード
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"KBCM1011.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    }
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdTo":
                    #region 船主CD
                    if (DENPYO_KUBUN == 1)
                    {
                        // アセンブリのロード
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    }
                    else
                    {
                        // アセンブリのロード
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"KBCM1011.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    }
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtJpYearFr":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblJpFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                                    this.txtJpMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                if (Util.ToInt(this.txtJpDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtJpDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblJpFr.Text,
                                        this.txtJpYearFr.Text,
                                        this.txtJpMonthFr.Text,
                                        this.txtJpDayFr.Text,
                                        this.Dba);
                                this.lblJpFr.Text = arrJpDate[0];
                                this.txtJpYearFr.Text = arrJpDate[2];
                                this.txtJpMonthFr.Text = arrJpDate[3];
                                this.txtJpDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtJpYearTo":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblJpTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                                    this.txtJpMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                if (Util.ToInt(this.txtJpDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtJpDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblJpTo.Text,
                                        this.txtJpYearTo.Text,
                                        this.txtJpMonthTo.Text,
                                        this.txtJpDayTo.Text,
                                        this.Dba);
                                this.lblJpTo.Text = arrJpDate[0];
                                this.txtJpYearTo.Text = arrJpDate[2];
                                this.txtJpMonthTo.Text = arrJpDate[3];
                                this.txtJpDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBDR1021R" });
            psForm.ShowDialog();
        }

        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        private void TxtDenpyoNoFr_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidDenpyoNoFr())
            {
                e.Cancel = true;
                //this.lblDenpyoNoFr.Text = "";
                this.sjTxtDenpyoNoFr.SelectAll();
                this.sjTxtDenpyoNoFr.Focus();
            }
        }
        private void TxtDenpyoNoTo_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidDenpyoNoTo())
            {
                e.Cancel = true;
                //this.lblDenpyoNoTo.Text = "";
                this.sjTxtDenpyoNoTo.SelectAll();
                this.sjTxtDenpyoNoTo.Focus();
            }
        }

        /// <summary>
        /// 担当者コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoCodeFr())
            {
                e.Cancel = true;
                this.txtTantoCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 担当者コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoCodeTo())
            {
                e.Cancel = true;
                this.txtTantoCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCodeFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCodeTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 年(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtJpYearFr.Text, this.txtJpYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtJpYearFr.SelectAll();
            }
            else
            {
                this.txtJpYearFr.Text = Util.ToString(IsValid.SetYear(this.txtJpYearFr.Text));
                CheckJpDateFr();
                SetJpDateFr();
            }
        }
        /// <summary>
        /// 月(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtJpMonthFr.Text, this.txtJpMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtJpMonthFr.SelectAll();
            }
            else
            {
                this.txtJpMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtJpMonthFr.Text));
                CheckJpDateFr();
                SetJpDateFr();
            }
        }
        /// <summary>
        /// 日(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtJpDayFr.Text, this.txtJpDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtJpDayFr.SelectAll();
            }
            else
            {
                this.txtJpDayFr.Text = Util.ToString(IsValid.SetDay(this.txtJpDayFr.Text));
                CheckJpDateFr();
                SetJpDateFr();
            }
        }

        /// <summary>
        /// 年(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtJpYearTo.Text, this.txtJpYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtJpYearTo.SelectAll();
            }
            else
            {
                this.txtJpYearTo.Text = Util.ToString(IsValid.SetYear(this.txtJpYearTo.Text));
                CheckJpDateTo();
                SetJpDateTo();
            }
        }
        /// <summary>
        /// 月(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtJpMonthTo.Text, this.txtJpMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtJpMonthTo.SelectAll();
            }
            else
            {
                this.txtJpMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtJpMonthTo.Text));
                CheckJpDateTo();
                SetJpDateTo();
            }
        }
        /// <summary>
        /// 日(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtJpDayTo.Text, this.txtJpDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtJpDayTo.SelectAll();
            }
            else
            {
                this.txtJpDayTo.Text = Util.ToString(IsValid.SetDay(this.txtJpDayTo.Text));
                CheckJpDateTo();
                SetJpDateTo();
            }
        }

        /// <summary>
        /// 船主CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                this.txtJpMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtJpDayFr.Text) > lastDayInMonth)
            {
                this.txtJpDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                this.txtJpMonthFr.Text, this.txtJpDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                this.txtJpMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtJpDayTo.Text) > lastDayInMonth)
            {
                this.txtJpDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                this.txtJpMonthTo.Text, this.txtJpDayTo.Text, this.Dba));
        }

        private bool IsValidDenpyoNoFr()
        {
            if (ValChk.IsEmpty(this.sjTxtDenpyoNoFr.Text))
            {
                this.lblDenpyoNoFr.Text = "先　頭";
            }
            else if (!ValChk.IsNumber(this.sjTxtDenpyoNoFr.Text))
            {
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                Msg.Error("伝票番号は数値のみで入力してください。");
                return false;
            }
            else
            {
                this.lblDenpyoNoFr.Text = "";
            }
            return true;
        }
        private bool IsValidDenpyoNoTo()
        {
            if (ValChk.IsEmpty(this.sjTxtDenpyoNoTo.Text))
            {
                this.lblDenpyoNoTo.Text = "最　後";
            }
            else if (!ValChk.IsNumber(this.sjTxtDenpyoNoTo.Text))
            {
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                Msg.Error("伝票番号は数値のみで入力してください。");
                return false;
            }
            else
            {
                this.lblDenpyoNoTo.Text = "";
            }
            return true;
        }


        /// <summary>
        /// 担当者コード(自)の入力チェック
        /// </summary>
        /// 
        private bool IsValidTantoCodeFr()
        {
            if (ValChk.IsEmpty(this.txtTantoCdFr.Text))
            {
                this.lblTantoCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoCdFr.Text))
            {
                Msg.Error("担当者コードは数値のみで入力してください。");
                return false;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "",this.txtTantoCdFr.Text);

                this.lblTantoCdFr.Text = name;
            }
            return true;
        }

        /// <summary>
        /// 担当者コード(至)の入力チェック
        /// </summary>
        /// 
        private bool IsValidTantoCodeTo()
        {
            if (ValChk.IsEmpty(this.txtTantoCdTo.Text))
            {
                this.lblTantoCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtTantoCdTo.Text))
                {
                    Msg.Error("担当者コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "", this.txtTantoCdTo.Text);

                    this.lblTantoCdTo.Text = name;
                }
            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        /// 
        private bool IsValidFunanushiCodeFr()
        {
            string title = (DENPYO_KUBUN == 1) ? "船主" : "仕入先";

            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error(title + "コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                string name = "";
                if (DENPYO_KUBUN == 1)
                {
                    name = this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", "", this.txtFunanushiCdFr.Text);
                }
                else
                {
                    name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtFunanushiCdFr.Text);
                }
                this.lblFunanushiCdFr.Text = name;
            }
            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        /// 
        private bool IsValidFunanushiCodeTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error("船主コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                string name = "";
                if (DENPYO_KUBUN == 1)
                {
                    name = this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", "", this.txtFunanushiCdTo.Text);
                }
                else
                {
                    name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtFunanushiCdTo.Text);
                }
                this.lblFunanushiCdTo.Text = name;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtJpYearFr.Text, this.txtJpYearFr.MaxLength))
            {
                this.txtJpYearFr.Focus();
                this.txtJpYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtJpMonthFr.Text, this.txtJpMonthFr.MaxLength))
            {
                this.txtJpMonthFr.Focus();
                this.txtJpMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtJpDayFr.Text, this.txtJpDayFr.MaxLength))
            {
                this.txtJpDayFr.Focus();
                this.txtJpDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtJpYearTo.Text, this.txtJpYearTo.MaxLength))
            {
                this.txtJpYearTo.Focus();
                this.txtJpYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtJpMonthTo.Text, this.txtJpMonthTo.MaxLength))
            {
                this.txtJpMonthTo.Focus();
                this.txtJpMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtJpDayTo.Text, this.txtJpDayTo.MaxLength))
            {
                this.txtJpDayTo.Focus();
                this.txtJpDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpDateTo();

            // 担当者コード(自)の入力チェック
            if (!IsValidTantoCodeFr())
            {
                this.txtTantoCdFr.Focus();
                this.txtTantoCdFr.SelectAll();
                return false;
            }
            // 担当者コード(至)の入力チェック
            if (!IsValidTantoCodeTo())
            {
                this.txtTantoCdTo.Focus();
                this.txtTantoCdTo.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCodeFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCodeTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblJpFr.Text = arrJpDate[0];
            this.txtJpYearFr.Text = arrJpDate[2];
            this.txtJpMonthFr.Text = arrJpDate[3];
            this.txtJpDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblJpTo.Text = arrJpDate[0];
            this.txtJpYearTo.Text = arrJpDate[2];
            this.txtJpMonthTo.Text = arrJpDate[3];
            this.txtJpDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                int Cols;
                // 帳票出力用にワークテーブルにデータを作成
                if (rdoGokei.Checked)
                {
                    dataFlag = MakeWkData01();
                    Cols = prtCols1;
                }
                else
                {
                    dataFlag = MakeWkData();
                    Cols = prtCols2;
                }
                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(Cols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    jp.co.fsi.common.report.BaseReport rpt;

                    // 帳票オブジェクトをインスタンス化
                    if (rdoGokei.Checked)
                    {
                        rpt = new KBDR1021R(dtOutput);
                    }
                    else
                    {
                        rpt = new KBDR1022R(dtOutput);
                    }
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        ///// <summary>
        ///// 取引伝票の合計金額取得
        ///// </summary>
        ///// <param name="DenpyoNo">伝票No</param>
        ///// <returns>消費税</returns>
        //private void GetDenpyoGokei(string Denpyo_denpyoNo)
        //{
        //    StringBuilder Sql = new StringBuilder();
        //    DbParamCollection dpc = new DbParamCollection();
        //    // 担当者コード設定
        //    string tantoshaCdFr;
        //    string tantoshaCdTo;
        //    if (Util.ToString(txtTantoCdFr.Text) != "")
        //    {
        //        tantoshaCdFr = txtTantoCdFr.Text;
        //    }
        //    else
        //    {
        //        tantoshaCdFr = "0";
        //    }
        //    if (Util.ToString(txtTantoCdTo.Text) != "")
        //    {
        //        tantoshaCdTo = txtTantoCdTo.Text;
        //    }
        //    else
        //    {
        //        tantoshaCdTo = "9999";
        //    }
        //    // 船主コード設定
        //    string funanushiCdFr;
        //    string funanushiCdTo;
        //    if (Util.ToString(txtFunanushiCdFr.Text) != "")
        //    {
        //        funanushiCdFr = txtFunanushiCdFr.Text;
        //    }
        //    else
        //    {
        //        funanushiCdFr = "0";
        //    }
        //    if (Util.ToString(txtFunanushiCdTo.Text) != "")
        //    {
        //        funanushiCdTo = txtFunanushiCdTo.Text;
        //    }
        //    else
        //    {
        //        funanushiCdTo = "9999";
        //    }
        //    string shishoCd = this.txtMizuageShishoCd.Text;
        //    int DenpyoKbn = Util.ToInt(this.Par1);
        //    string rptTitle = (DenpyoKbn == 1) ? "売上" : "仕入";
        //    rptTitle += "一覧（合計）";

        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
        //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
        //    // 検索する伝票番号をセット
        //    dpc.SetParam("@DENPYO_BANGO", SqlDbType.VarChar, 10, Denpyo_denpyoNo);
        //    // 検索する担当者コードをセット
        //    dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, tantoshaCdFr);
        //    dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, tantoshaCdTo);
        //    // 検索する船主コードをセット
        //    dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 4, funanushiCdFr);
        //    dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 4, funanushiCdTo);
        //    // 検索する支所コードをセット
        //    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
        //    // 伝票区分をセット
        //    dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, DenpyoKbn);

        //    // han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
        //    // の検索日付に発生しているデータを取得
        //    Sql.Append("SELECT ");
        //    Sql.Append("  *");
        //    Sql.Append(" FROM ");
        //    Sql.Append("  TB_HN_TORIHIKI_DENPYO");
        //    Sql.Append(" WHERE ");
        //    Sql.Append("      KAISHA_CD    = @KAISHA_CD ");
        //    Sql.Append("  AND SHISHO_CD    = @SHISHO_CD ");
        //    Sql.Append("  AND KAIKEI_NENDO  = @KAIKEI_NENDO ");
        //    Sql.Append("  AND DENPYO_KUBUN = 1 ");
        //    Sql.Append("  AND DENPYO_BANGO  = @DENPYO_BANGO ");
        //    //Sql.Append("  AND KAIIN_BANGO  BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
        //    Sql.Append("  AND TOKUISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
        //    Sql.Append("  AND TANTOSHA_CD  BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO ");
        //    DataTable dtDenpyoGokeiGaku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            
        //    DenpyoGokei[0] = Util.ToDecimal(dtDenpyoGokeiGaku.Rows[0]["URIAGE_KINGAKU"]);
        //    DenpyoGokei[1] = Util.ToDecimal(dtDenpyoGokeiGaku.Rows[0]["SHOHIZEIGAKU"]);
        //    DenpyoGokei[2] = Util.ToDecimal(dtDenpyoGokeiGaku.Rows[0]["NYUKIN_GOKEIGAKU"]);

        //}

        //struct NumVals
        //{
        //    public decimal kingaku;
        //    public decimal shohizei;
        //    public decimal gokei;

        //    public void Clear()
        //    {
        //        kingaku = 0;
        //        shohizei = 0;
        //        gokei = 0;
        //    }
        //}

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region 前準備
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                    this.txtJpMonthFr.Text, this.txtJpDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, this.txtJpDayTo.Text, this.Dba);
            //今月の最後の日
            DateTime tmpLastDay = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, Util.ToString(DateTime.DaysInMonth(Util.ToInt(this.txtJpYearTo.Text), Util.ToInt(this.txtJpMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 日付表示形式を判断
            int dataHyojiFlag = 0; // 表示日付用変数
            // 入力開始日付が1日でない場合
            if (tmpDateFr.Day != 1)
            {
                dataHyojiFlag = 1;
            }
            // 入力終了日付が、その月の最終日でない場合
            else if (tmpDateTo != tmpLastDay)
            {
                dataHyojiFlag = 1;
            }
            // 入力開始日付と入力終了日付の年又は月が一致でない場合
            if (tmpDateFr.Year != tmpDateTo.Year || tmpDateFr.Month != tmpDateTo.Month)
            {
                dataHyojiFlag = 1;
            }

            string hyojiDate; // 表示用日付            
            if (dataHyojiFlag == 0)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }
            // 伝票番号設定
            string denpyoFr;
            string denpyoTo;
            if (Util.ToString(this.sjTxtDenpyoNoFr.Text) != "")
            {
                denpyoFr = this.sjTxtDenpyoNoFr.Text;
            }
            else
            {
                denpyoFr = "0";
            }
            if (Util.ToString(this.sjTxtDenpyoNoTo.Text) != "")
            {
                denpyoTo = this.sjTxtDenpyoNoTo.Text;
            }
            else
            {
                denpyoTo = "999999";
            }

            // 担当者コード設定
            string tantoshaCdFr;
            string tantoshaCdTo;
            if (Util.ToString(txtTantoCdFr.Text) != "")
            {
                tantoshaCdFr = txtTantoCdFr.Text;
            }
            else
            {
                tantoshaCdFr = "0";
            }
            if (Util.ToString(txtTantoCdTo.Text) != "")
            {
                tantoshaCdTo = txtTantoCdTo.Text;
            }
            else
            {
                tantoshaCdTo = "9999";
            }
            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToString(txtFunanushiCdFr.Text) != "")
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToString(txtFunanushiCdTo.Text) != "")
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "9999";
            }
            string shishoCd = this.txtMizuageShishoCd.Text;
            int DenpyoKbn = Util.ToInt(this.Par1);
            string rptTitle = (DenpyoKbn == 1) ? "売上" : "仕入";
            // TODO 2019-01-08 (明細）を除去  
            //            rptTitle += "一覧（明細）";
            rptTitle += "一覧";

            // 取引区分
            int torihikikbn = this.cmbToriKbn.SelectedIndex;
            string torihikiKbnNm = "";
            switch (torihikikbn)
            {
                case 1:
                    torihikiKbnNm = "掛取引";
                    break;
                case 2:
                    torihikiKbnNm = "現金取引";
                    break;
                default:
                    torihikiKbnNm = "全て";
                    break;
            }
            int dbSORT = 1; // ループ用カウント変数

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 検索する日付をセット
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            // 検索する伝票番号
            dpc.SetParam("@DENPYO_BANGO_FR", SqlDbType.Decimal, 6, denpyoFr);
            dpc.SetParam("@DENPYO_BANGO_TO", SqlDbType.Decimal, 6, denpyoTo);
            // 検索する担当者コードをセット
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, tantoshaCdFr);
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, tantoshaCdTo);
            // 検索する船主コードをセット
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 4, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 4, funanushiCdTo);
            // 検索する支所コードをセット
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            // 伝票区分をセット
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, DenpyoKbn);

            // han.VI_取引明細(VI_HN_TORIHIKI_MEISAI)
            // の検索日付に発生しているデータを取得
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT ");
            Sql.Append("  *");
            Sql.Append("  ,CASE WHEN TORIHIKI_KUBUN2 = 2 THEN ZEINUKI_KINGAKU * -1 ELSE ZEINUKI_KINGAKU END AS WK_ZEINUKI_KINGAKU ");
            Sql.Append("  ,CASE WHEN TORIHIKI_KUBUN2 = 2 THEN SHOHIZEI * -1 ELSE SHOHIZEI END AS WK_SHOHIZEI ");
            Sql.Append(" FROM ");
            Sql.Append("  VI_HN_TORIHIKI_MEISAI");
            Sql.Append(" WHERE ");
            Sql.Append("      KAISHA_CD    = @KAISHA_CD ");
            if (shishoCd != "0")
            {
                Sql.Append("  AND SHISHO_CD    = @SHISHO_CD ");
            }

            // TODO STA 2019-01-08 Append By Ganeko 取引区分：すべて場合の対処
            if (DenpyoKbn != 0)
            {

                Sql.Append("  AND DENPYO_KUBUN = @DENPYO_KUBUN ");

            }
            // TODO END 2019-01-08 Append By Ganeko 取引区分：すべて場合の対処

            Sql.Append("  AND DENPYO_DATE  BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO ");
            Sql.Append("  AND DENPYO_BANGO BETWEEN @DENPYO_BANGO_FR AND @DENPYO_BANGO_TO ");
            Sql.Append("  AND TOKUISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append("  AND TANTOSHA_CD  BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO ");

            if(2 == Util.ToInt(this.Par1))
                Sql.Append("  AND SHUBETU_KUBUN = 2 "); //種別区分：2（仕入）のみ取得

            switch (torihikikbn)
            {
                case 1:
                    Sql.Append(" AND TORIHIKI_KUBUN NOT IN (" + GENKIN + "1, " + GENKIN + "2) ");
                    break;
                case 2:
                    Sql.Append(" AND TORIHIKI_KUBUN IN (" + GENKIN + "1, " + GENKIN + "2) ");
                    break;
                //case 3:
                //    Sql.Append(" AND TORIHIKI_KUBUN IN (31, 32) ");
                //    break;
                default:
                    break;
            }
            Sql.Append("ORDER BY ");
            Sql.Append("  KAISHA_CD ASC");
            Sql.Append(" ,SHISHO_CD ASC");
            Sql.Append(" ,DENPYO_DATE ASC");
            //Sql.Append(" ,TOKUISAKI_CD ASC");
            Sql.Append(" ,DENPYO_BANGO ASC");
            Sql.Append(" ,GYO_BANGO ASC");
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion
            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols2, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols2, "@"));
                    Sql.Append(") ");
                    #endregion

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;

                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);   // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, torihikiKbnNm);         // 取引区分名（条件）
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, hyojiDate);             // 表示用対象期間
                                                                                            // 伝票日付を和暦に変換
                    DateTime d = Util.ToDate(dr["DENPYO_DATE"]);
                    string[] denpyo_date = Util.ConvJpDate(d, this.Dba);
                    string wareki_denpyo_date = denpyoDate(denpyo_date);

                    //0317
                    //税区分情報取得(from ZMCM1062.cs)
                    string hikazei = "0"; //非課税の税率は0で固定にしています…
                    string kazei = "";
                    string keigen = "";
                    string[] zeiKbn = { "10", "20", "50", "60"};
                    //税率の値の取得
                    foreach (string zeiKubun in zeiKbn)
                    {
                        StringBuilder SqlZk = new StringBuilder();
                        DbParamCollection dpcZk = new DbParamCollection();
                        // 税区分ビューからデータを取得して表示
                        SqlZk.Append("SELECT");
                        SqlZk.Append(" TEKIYOU_KAISHI, ");
                        SqlZk.Append(" SHIN_ZEI_RITSU  ");
                        SqlZk.Append("FROM");
                        SqlZk.Append(" TB_ZM_F_ZEI_KBN_SHIN_ZEI_RT ");
                        SqlZk.Append("WHERE");
                        SqlZk.Append(" ZEI_KUBUN = @ZEI_KUBUN ");

                        dpcZk.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, zeiKubun);

                        DataTable dtShinZeiRitsu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(SqlZk), dpcZk);
                        
                        foreach (DataRow drZk in dtShinZeiRitsu.Rows)
                        {
                            DateTime dZkbn = Util.ToDate(drZk["TEKIYOU_KAISHI"]);
                            if ("10" == zeiKubun && ("" == kazei || d >= dZkbn)) //課税
                            {
                                kazei = "" + Decimal.ToInt32(Util.ToDecimal(drZk["SHIN_ZEI_RITSU"])).ToString();
                            }
                            else if ("20" == zeiKubun && ("" == kazei || d >= dZkbn)) //課税
                            {
                                kazei = "" + Decimal.ToInt32(Util.ToDecimal(drZk["SHIN_ZEI_RITSU"])).ToString();
                            }
                            else if ("50" == zeiKubun && ("" == keigen || d >= dZkbn)) //軽減税率
                            {
                                keigen = "*" + Decimal.ToInt32(Util.ToDecimal(drZk["SHIN_ZEI_RITSU"])).ToString();
                            }
                            else if ("60" == zeiKubun && ("" == keigen || d >= dZkbn)) //軽減税率
                            {
                                keigen = "*" + Decimal.ToInt32(Util.ToDecimal(drZk["SHIN_ZEI_RITSU"])).ToString();
                            }
                        }
                        //return dtShinZeiRitsu;
                    }
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, wareki_denpyo_date);    // 伝票日付
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["DENPYO_BANGO"]);    // 伝票番号
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["TOKUISAKI_CD"]);    // 取引先コード
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["TOKUISAKI_NM"]);    // 取引先名
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["TORIHIKI_KUBUN_NM"]); // 取引区分名
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);       // 商品コード
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["SHOHIN_NM"]);       // 商品名
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["IRISU"]);           // 入数
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["SURYO1"]);          // 数量(ケース)
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dr["SURYO2"]);          // 数量(ﾊﾞﾗ)
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dr["URI_TANKA"]);       // 単価

                    //dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dr["BAIKA_KINGAKU"]);   // 金額
                    //dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dr["SHOHIZEI"]);        // 消費税
                    //dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.ToDecimal(dr["ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["SHOHIZEI"]));        // 合計
                    //dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, dr["SHISHO_CD"]); //船主CD

                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dr["WK_ZEINUKI_KINGAKU"]); // 金額
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dr["WK_SHOHIZEI"]); // 消費税　
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.ToDecimal(dr["WK_ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["WK_SHOHIZEI"])); // 合計
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.ToString(dr["TANTOSHA_CD"]) + " " + dr["TANTOSHA_NM"]); // 担当者コード
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, rptTitle); //帳票タイトル

                    //税率毎の処理
                    switch (Util.ToInt(dr["ZEI_KUBUN"]))
                    {
                        case 0: //税区分で判断。0は非課税
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToString(dr["ZEI_RITSU"]));
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, dr["WK_ZEINUKI_KINGAKU"]); // 金額
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, dr["WK_SHOHIZEI"]); // 消費税　
                            dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Util.ToDecimal(dr["WK_ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["WK_SHOHIZEI"])); //合計

                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, 0m); //消費税　
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, 0m); //合計
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, 0m); //消費税　
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, 0m); //合計
                            break;

                        case 10: //税区分で判断。10は課税(売上)
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToString(dr["ZEI_RITSU"]));
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dr["WK_ZEINUKI_KINGAKU"]); // 金額
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dr["WK_SHOHIZEI"]); // 消費税　
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.ToDecimal(dr["WK_ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["WK_SHOHIZEI"])); //合計

                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, 0m); //消費税　
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, 0m); //合計
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, 0m); //消費税
                            dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, 0m); //合計
                            break;
                        
                        case 20: //税区分で判断。20は課税
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToString(dr["ZEI_RITSU"]));
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dr["WK_ZEINUKI_KINGAKU"]); // 金額
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dr["WK_SHOHIZEI"]); // 消費税　
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.ToDecimal(dr["WK_ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["WK_SHOHIZEI"])); //合計

                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, 0m); //消費税　
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, 0m); //合計
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, 0m); //消費税
                            dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, 0m); //合計
                            break;
                        
                        case 50: //税区分で判断。50は軽減税率対象(売上)
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToString("*" + dr["ZEI_RITSU"])); //軽減税率の場合「*」表示
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dr["WK_ZEINUKI_KINGAKU"]); // 金額
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, dr["WK_SHOHIZEI"]); // 消費税　
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.ToDecimal(dr["WK_ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["WK_SHOHIZEI"])); //合計

                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, 0m); //消費税
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, 0m); //合計
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, 0m); //消費税
                            dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, 0m); //合計
                            break;
                        
                        case 60: //税区分で判断。60は軽減税率対象
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToString("*" + dr["ZEI_RITSU"])); //軽減税率の場合「*」表示
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dr["WK_ZEINUKI_KINGAKU"]); // 金額
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, dr["WK_SHOHIZEI"]); // 消費税　
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.ToDecimal(dr["WK_ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["WK_SHOHIZEI"])); //合計

                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, 0m); //消費税
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, 0m); //合計
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, 0m); //消費税
                            dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, 0m); //合計
                            break;
                        
                        case 26: //税区分で判断。26は非課税
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToString(dr["ZEI_RITSU"]));
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, dr["WK_ZEINUKI_KINGAKU"]); // 金額
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, dr["WK_SHOHIZEI"]); // 消費税　
                            dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Util.ToDecimal(dr["WK_ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["WK_SHOHIZEI"])); //合計

                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, 0m); //消費税　
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, 0m); //合計
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, 0m); //消費税　
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, 0m); //合計
                            break;

                        default:
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToString(dr["ZEI_RITSU"]));
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, dr["WK_ZEINUKI_KINGAKU"]); // 金額
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, dr["WK_SHOHIZEI"]); // 消費税　
                            dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Util.ToDecimal(dr["WK_ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["WK_SHOHIZEI"])); //合計

                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, 0m); //消費税　
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, 0m); //合計
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 0m); //金額
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, 0m); //消費税　
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, 0m); //合計
                            break;
                    }
                    //消費税内訳での税率表示
                    dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Util.ToString(kazei));
                    dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, Util.ToString(keigen));
                    dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, Util.ToString(hikazei));

                    //返品の時、「伝票合計」の表示を変える
                    switch (Util.ToInt(dr["TORIHIKI_KUBUN"]))
                    {
                        //取引区分が「12」「22」の場合は返品
                        case 12:
                        case 22:
                            dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, "【返品伝票 合計】");
                            break;

                        default:
                            dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, "【伝票合計】");
                        break;
                    }

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                }
                #endregion
            }
            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {

                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// 合計データ作成
        /// </summary>
        private bool MakeWkData01()
        {
            #region 前準備
            DbParamCollection dpc = new DbParamCollection();
            // 日付を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                    this.txtJpMonthFr.Text, this.txtJpDayFr.Text, this.Dba);
            // 日付を西暦にして取得
            DateTime tmpDateTo = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, this.txtJpDayTo.Text, this.Dba);
            //今月の最後の日
            DateTime tmpLastDay = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, Util.ToString(DateTime.DaysInMonth(Util.ToInt(this.txtJpYearTo.Text), Util.ToInt(this.txtJpMonthTo.Text))), this.Dba);

            // 日付を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            // 日付表示形式を判断
            int dataHyojiFlag = 0; // 表示日付用変数
            // 入力開始日付が1日でない場合
            if (tmpDateFr.Day != 1)
            {
                dataHyojiFlag = 1;
            }
            // 入力終了日付が、その月の最終日でない場合
            else if (tmpDateTo != tmpLastDay)
            {
                dataHyojiFlag = 1;
            }
            // 入力開始日付と入力終了日付の年又は月が一致でない場合
            if (tmpDateFr.Year != tmpDateTo.Year || tmpDateFr.Month != tmpDateTo.Month)
            {
                dataHyojiFlag = 1;
            }

            string hyojiDate; // 表示用日付            
            if (dataHyojiFlag == 0)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }
            // 伝票番号設定
            string denpyoFr;
            string denpyoTo;
            if (Util.ToString(this.sjTxtDenpyoNoFr.Text) != "")
            {
                denpyoFr = this.sjTxtDenpyoNoFr.Text;
            }
            else
            {
                denpyoFr = "0";
            }
            if (Util.ToString(this.sjTxtDenpyoNoTo.Text) != "")
            {
                denpyoTo = this.sjTxtDenpyoNoTo.Text;
            }
            else
            {
                denpyoTo = "999999";
            }

            // 担当者コード設定
            string tantoshaCdFr;
            string tantoshaCdTo;
            if (Util.ToString(txtTantoCdFr.Text) != "")
            {
                tantoshaCdFr = txtTantoCdFr.Text;
            }
            else
            {
                tantoshaCdFr = "0";
            }
            if (Util.ToString(txtTantoCdTo.Text) != "")
            {
                tantoshaCdTo = txtTantoCdTo.Text;
            }
            else
            {
                tantoshaCdTo = "9999";
            }
            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToString(txtFunanushiCdFr.Text) != "")
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToString(txtFunanushiCdTo.Text) != "")
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "9999";
            }
            // 支所コード
            string shishoCd = this.txtMizuageShishoCd.Text;
            // 取引区分
            int torihikikbn = this.cmbToriKbn.SelectedIndex;
            string torihikiKbnNm = "";
            switch(torihikikbn)
            {
                case 1:
                    torihikiKbnNm = "掛取引";
                    break;
                case 2:
                    torihikiKbnNm = "現金取引";
                    break;
                default:
                    torihikiKbnNm = "全て";
                    break;
            }

            int i = 0; // ループ用カウント変数
            int DenpyoKbn = Util.ToInt(this.Par1);
            string rptTitle =  (DenpyoKbn == 1) ? "売上" : "仕入";
　　　　　　// TODO 2019-01-08 Upd STA 合計を除去
//            rptTitle += "一覧（合計）";
            rptTitle += "一覧";

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 検索する日付をセット
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            // 検索する伝票番号
            dpc.SetParam("@DENPYO_BANGO_FR", SqlDbType.Decimal, 6, denpyoFr);
            dpc.SetParam("@DENPYO_BANGO_TO", SqlDbType.Decimal, 6, denpyoTo);
            // 検索する担当者コードをセット
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, tantoshaCdFr);
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, tantoshaCdTo);
            // 検索する船主コードをセット
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 4, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 4, funanushiCdTo);
            // 検索する支所コードをセット
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            // 伝票区分をセット
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, DenpyoKbn);

            // han.VI_取引明細(VI_HN_TORIHIKI_MEISAI)
            // の検索日付に発生しているデータを取得
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT ");
            Sql.Append(" KAISHA_CD, ");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD, ");
            Sql.Append(" MAX(TORIHIKI_KUBUN1)	AS TORIHIKI_KUBUN1, ");
            Sql.Append(" MAX(TORIHIKI_KUBUN2)	AS TORIHIKI_KUBUN2, ");
            Sql.Append(" MAX(TORIHIKI_KUBUN_NM)	AS TORIHIKI_KUBUN_NM, ");
            Sql.Append(" TOKUISAKI_CD			AS TOKUISAKI_CD, ");
            Sql.Append(" MAX(TOKUISAKI_NM)		AS TOKUISAKI_NM, ");
            Sql.Append(" SUM(CASE WHEN TORIHIKI_KUBUN2 = 2 THEN ZEINUKI_KINGAKU * -1 ELSE ZEINUKI_KINGAKU END) AS ZEINUKI_KINGAKU, ");
            Sql.Append(" SUM(CASE WHEN TORIHIKI_KUBUN2 = 2 THEN SHOHIZEI * -1 ELSE SHOHIZEI END) AS SHOHIZEI ");
            Sql.Append("FROM VI_HN_TORIHIKI_MEISAI ");
            Sql.Append("WHERE ");
            Sql.Append("      KAISHA_CD    = @KAISHA_CD ");
            if (shishoCd != "0")
                Sql.Append("  AND SHISHO_CD    = @SHISHO_CD ");

            // TODO 2019-01-08 Append STA 伝票区分：すべての場合の対処
            if (DenpyoKbn != 0)
            {
                Sql.Append("  AND DENPYO_KUBUN = @DENPYO_KUBUN ");

            }
            // TODO 2019-01-08 Append END 伝票区分：すべての場合の対処

            Sql.Append("  AND DENPYO_BANGO BETWEEN @DENPYO_BANGO_FR AND @DENPYO_BANGO_TO ");
            Sql.Append("  AND DENPYO_DATE  BETWEEN @DATE_FR AND @DATE_TO ");
            Sql.Append("  AND TANTOSHA_CD  BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO ");
            Sql.Append("  AND TOKUISAKI_CD  BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            switch (torihikikbn)
            {
                case 1:
                    Sql.Append(" AND TORIHIKI_KUBUN NOT IN (" + GENKIN + "1, " + GENKIN + "2) ");
                    break;
                case 2:
                    Sql.Append(" AND TORIHIKI_KUBUN IN (" + GENKIN +"1, " + GENKIN + "2) ");
                    break;
                //case 3:
                //    Sql.Append(" AND TORIHIKI_KUBUN IN (31, 32) ");
                //    break;
                default:
                    break;
            }
            Sql.Append("GROUP BY ");
            Sql.Append(" KAISHA_CD, ");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD, ");
            Sql.Append(" TOKUISAKI_CD, ");
            Sql.Append(" TORIHIKI_KUBUN ");
            Sql.Append("ORDER BY ");
            Sql.Append(" KAISHA_CD, ");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD, ");
            Sql.Append(" TOKUISAKI_CD, ");
            Sql.Append(" TORIHIKI_KUBUN ");
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion
            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols1, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols1, "@"));
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, torihikiKbnNm);  //取引区分
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, hyojiDate);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["TOKUISAKI_CD"]);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["TOKUISAKI_NM"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["TORIHIKI_KUBUN_NM"]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["ZEINUKI_KINGAKU"]);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["SHOHIZEI"]);
                    decimal gokeiKingaku = Util.ToDecimal(dr["ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr["SHOHIZEI"]);
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, gokeiKingaku);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, rptTitle);
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, shishoCd); //帳票タイトル

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                // 印刷ワークテーブルのデータ件数を取得
                dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                    "SORT",
                    "PR_HN_TBL",
                    "GUID = @GUID",
                    dpc);

                bool dataFlag;
                if (tmpdtPR_HN_TBL.Rows.Count > 0)
                {

                    dataFlag = true;
                }
                else
                {
                    dataFlag = false;
                }

                return dataFlag;
                #endregion
            }

        }
        /// <summary>
        /// 伝票日付の退避
        /// </summary>
        /// <param name="denpyo_date">和暦日付の配列</param>
        /// <returns>和暦日付を返す</returns>
        private string denpyoDate(string[] denpyo_date)
        {
            if (denpyo_date[3].Length == 1)
            {
                denpyo_date[3] = " " + denpyo_date[3];
            }
            if (denpyo_date[4].Length == 1)
            {
                denpyo_date[4] = " " + denpyo_date[4];
            }
            return denpyo_date[2] + "/" + denpyo_date[3] + "/" + denpyo_date[4];
        }


        #endregion


    }
}
