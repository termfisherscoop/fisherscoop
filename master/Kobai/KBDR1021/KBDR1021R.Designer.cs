﻿namespace jp.co.fsi.kb.kbdr1021
{
    /// <summary>
    /// KBDR1021R の概要の説明です。
    /// </summary>
    partial class KBDR1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR1021R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.totalRowNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.nowRouNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ITEM03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalRowNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nowRouNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ITEM01,
            this.ITEM02,
            this.直線1,
            this.reportInfo1,
            this.textBox1,
            this.label1,
            this.shape1,
            this.ラベル9,
            this.ラベル10,
            this.ラベル11,
            this.ラベル14,
            this.ラベル15,
            this.ラベル16,
            this.直線34,
            this.直線35,
            this.直線37,
            this.直線38,
            this.直線39,
            this.totalRowNo,
            this.nowRouNo,
            this.textBox2,
            this.textBox4});
            this.pageHeader.Height = 1.39567F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.15625F;
            this.ITEM01.Left = 0.1893701F;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM01";
            this.ITEM01.Top = 0.1673228F;
            this.ITEM01.Width = 2.191825F;
            // 
            // ITEM02
            // 
            this.ITEM02.DataField = "ITEM03";
            this.ITEM02.Height = 0.15625F;
            this.ITEM02.Left = 2.594488F;
            this.ITEM02.Name = "ITEM02";
            this.ITEM02.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM02.Tag = "";
            this.ITEM02.Text = "ITEM03";
            this.ITEM02.Top = 0.7569555F;
            this.ITEM02.Width = 2.277166F;
            // 
            // 直線1
            // 
            this.直線1.Height = 3.838539E-05F;
            this.直線1.Left = 2.600788F;
            this.直線1.LineWeight = 2F;
            this.直線1.Name = "直線1";
            this.直線1.Tag = "";
            this.直線1.Top = 0.6409449F;
            this.直線1.Width = 2.185886F;
            this.直線1.X1 = 2.600788F;
            this.直線1.X2 = 4.786674F;
            this.直線1.Y1 = 0.6409449F;
            this.直線1.Y2 = 0.6409833F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1972441F;
            this.reportInfo1.Left = 5.827953F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.reportInfo1.Top = 0.5598426F;
            this.reportInfo1.Width = 1.072129F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.2005741F;
            this.textBox1.Left = 6.900001F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox1.Text = "textBox1";
            this.textBox1.Top = 0.5598426F;
            this.textBox1.Width = 0.2708659F;
            // 
            // label1
            // 
            this.label1.Height = 0.2005741F;
            this.label1.HyperLink = null;
            this.label1.Left = 7.168504F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label1.Text = "頁";
            this.label1.Top = 0.5598426F;
            this.label1.Width = 0.15625F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape1.Height = 0.3834646F;
            this.shape1.Left = 0.1893701F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape1.Top = 1.012205F;
            this.shape1.Width = 7.161024F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.1965278F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 0.1944882F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "船主コード";
            this.ラベル9.Top = 1.120079F;
            this.ラベル9.Width = 0.8090551F;
            // 
            // ラベル10
            // 
            this.ラベル10.Height = 0.1965278F;
            this.ラベル10.HyperLink = null;
            this.ラベル10.Left = 3.906693F;
            this.ラベル10.Name = "ラベル10";
            this.ラベル10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル10.Tag = "";
            this.ラベル10.Text = "取引区分";
            this.ラベル10.Top = 1.120866F;
            this.ラベル10.Width = 0.8803151F;
            // 
            // ラベル11
            // 
            this.ラベル11.Height = 0.1972222F;
            this.ラベル11.HyperLink = null;
            this.ラベル11.Left = 1.003543F;
            this.ラベル11.Name = "ラベル11";
            this.ラベル11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル11.Tag = "";
            this.ラベル11.Text = "船　主　名";
            this.ラベル11.Top = 1.120079F;
            this.ラベル11.Width = 2.902362F;
            // 
            // ラベル14
            // 
            this.ラベル14.Height = 0.1972222F;
            this.ラベル14.HyperLink = null;
            this.ラベル14.Left = 4.787008F;
            this.ラベル14.Name = "ラベル14";
            this.ラベル14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル14.Tag = "";
            this.ラベル14.Text = "金　額";
            this.ラベル14.Top = 1.120079F;
            this.ラベル14.Width = 0.8263779F;
            // 
            // ラベル15
            // 
            this.ラベル15.Height = 0.1965278F;
            this.ラベル15.HyperLink = null;
            this.ラベル15.Left = 5.613386F;
            this.ラベル15.Name = "ラベル15";
            this.ラベル15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル15.Tag = "";
            this.ラベル15.Text = "消費税";
            this.ラベル15.Top = 1.120079F;
            this.ラベル15.Width = 0.8267718F;
            // 
            // ラベル16
            // 
            this.ラベル16.Height = 0.1965278F;
            this.ラベル16.HyperLink = null;
            this.ラベル16.Left = 6.440158F;
            this.ラベル16.Name = "ラベル16";
            this.ラベル16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル16.Tag = "";
            this.ラベル16.Text = "金額計";
            this.ラベル16.Top = 1.120079F;
            this.ラベル16.Width = 0.8338586F;
            // 
            // 直線34
            // 
            this.直線34.Height = 0.3771321F;
            this.直線34.Left = 1.00344F;
            this.直線34.LineWeight = 1F;
            this.直線34.Name = "直線34";
            this.直線34.Tag = "";
            this.直線34.Top = 1.01378F;
            this.直線34.Width = 0.0001029968F;
            this.直線34.X1 = 1.003543F;
            this.直線34.X2 = 1.00344F;
            this.直線34.Y1 = 1.01378F;
            this.直線34.Y2 = 1.390912F;
            // 
            // 直線35
            // 
            this.直線35.Height = 0.381857F;
            this.直線35.Left = 3.90659F;
            this.直線35.LineWeight = 1F;
            this.直線35.Name = "直線35";
            this.直線35.Tag = "";
            this.直線35.Top = 1.01378F;
            this.直線35.Width = 0.0001029968F;
            this.直線35.X1 = 3.906693F;
            this.直線35.X2 = 3.90659F;
            this.直線35.Y1 = 1.01378F;
            this.直線35.Y2 = 1.395637F;
            // 
            // 直線37
            // 
            this.直線37.Height = 0.377077F;
            this.直線37.Left = 4.786829F;
            this.直線37.LineWeight = 1F;
            this.直線37.Name = "直線37";
            this.直線37.Tag = "";
            this.直線37.Top = 1.01378F;
            this.直線37.Width = 0.0001788139F;
            this.直線37.X1 = 4.787008F;
            this.直線37.X2 = 4.786829F;
            this.直線37.Y1 = 1.01378F;
            this.直線37.Y2 = 1.390857F;
            // 
            // 直線38
            // 
            this.直線38.Height = 0.377077F;
            this.直線38.Left = 5.613205F;
            this.直線38.LineWeight = 1F;
            this.直線38.Name = "直線38";
            this.直線38.Tag = "";
            this.直線38.Top = 1.01378F;
            this.直線38.Width = 0.0001811981F;
            this.直線38.X1 = 5.613386F;
            this.直線38.X2 = 5.613205F;
            this.直線38.Y1 = 1.01378F;
            this.直線38.Y2 = 1.390857F;
            // 
            // 直線39
            // 
            this.直線39.Height = 0.377077F;
            this.直線39.Left = 6.439977F;
            this.直線39.LineWeight = 1F;
            this.直線39.Name = "直線39";
            this.直線39.Tag = "";
            this.直線39.Top = 1.01378F;
            this.直線39.Width = 0.0001807213F;
            this.直線39.X1 = 6.440158F;
            this.直線39.X2 = 6.439977F;
            this.直線39.Y1 = 1.01378F;
            this.直線39.Y2 = 1.390857F;
            // 
            // totalRowNo
            // 
            this.totalRowNo.DataField = "ITEM11";
            this.totalRowNo.Height = 0.1976378F;
            this.totalRowNo.Left = 1.261024F;
            this.totalRowNo.Name = "totalRowNo";
            this.totalRowNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.totalRowNo.Tag = "";
            this.totalRowNo.Text = null;
            this.totalRowNo.Top = 0.3236221F;
            this.totalRowNo.Visible = false;
            this.totalRowNo.Width = 0.3826776F;
            // 
            // nowRouNo
            // 
            this.nowRouNo.CountNullValues = true;
            this.nowRouNo.DataField = "SORT";
            this.nowRouNo.Height = 0.15625F;
            this.nowRouNo.Left = 0.357874F;
            this.nowRouNo.Name = "nowRouNo";
            this.nowRouNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.nowRouNo.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.nowRouNo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.nowRouNo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.nowRouNo.Tag = "";
            this.nowRouNo.Text = "sort";
            this.nowRouNo.Top = 0.3649606F;
            this.nowRouNo.Visible = false;
            this.nowRouNo.Width = 0.3826773F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM02";
            this.textBox2.Height = 0.15625F;
            this.textBox2.Left = 0.1893702F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM02";
            this.textBox2.Top = 0.7602363F;
            this.textBox2.Width = 1.454331F;
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.DataField = "ITEM10";
            this.textBox4.Height = 0.3015748F;
            this.textBox4.Left = 2.594488F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: center; white-space: nowrap; ddo-char-set: 1; ddo-wrap" +
    "-mode: nowrap";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM10";
            this.textBox4.Top = 0.3236221F;
            this.textBox4.Width = 2.277165F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ITEM03,
            this.ITEM05,
            this.ITEM06,
            this.ITEM08,
            this.ITEM09,
            this.ITEM10,
            this.直線30,
            this.直線31,
            this.直線40,
            this.直線42,
            this.直線44,
            this.直線45,
            this.直線46,
            this.line1});
            this.detail.Height = 0.1992126F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // ITEM03
            // 
            this.ITEM03.DataField = "ITEM04";
            this.ITEM03.Height = 0.15625F;
            this.ITEM03.Left = 0.2129921F;
            this.ITEM03.Name = "ITEM03";
            this.ITEM03.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM03.Tag = "";
            this.ITEM03.Text = "ITEM04";
            this.ITEM03.Top = 0.03779528F;
            this.ITEM03.Width = 0.7070867F;
            // 
            // ITEM05
            // 
            this.ITEM05.DataField = "ITEM05";
            this.ITEM05.Height = 0.15625F;
            this.ITEM05.Left = 1.066142F;
            this.ITEM05.MultiLine = false;
            this.ITEM05.Name = "ITEM05";
            this.ITEM05.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM05.Tag = "";
            this.ITEM05.Text = "ITEM05";
            this.ITEM05.Top = 0.03779528F;
            this.ITEM05.Width = 2.839763F;
            // 
            // ITEM06
            // 
            this.ITEM06.DataField = "ITEM06";
            this.ITEM06.Height = 0.15625F;
            this.ITEM06.Left = 3.905906F;
            this.ITEM06.Name = "ITEM06";
            this.ITEM06.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM06.Tag = "";
            this.ITEM06.Text = "ITEM06";
            this.ITEM06.Top = 0.03779528F;
            this.ITEM06.Width = 0.8807087F;
            // 
            // ITEM08
            // 
            this.ITEM08.DataField = "ITEM07";
            this.ITEM08.Height = 0.15625F;
            this.ITEM08.Left = 4.786614F;
            this.ITEM08.Name = "ITEM08";
            this.ITEM08.OutputFormat = resources.GetString("ITEM08.OutputFormat");
            this.ITEM08.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM08.Tag = "";
            this.ITEM08.Text = "ITEM07";
            this.ITEM08.Top = 0.03779528F;
            this.ITEM08.Width = 0.7696856F;
            // 
            // ITEM09
            // 
            this.ITEM09.DataField = "ITEM08";
            this.ITEM09.Height = 0.15625F;
            this.ITEM09.Left = 5.613386F;
            this.ITEM09.Name = "ITEM09";
            this.ITEM09.OutputFormat = resources.GetString("ITEM09.OutputFormat");
            this.ITEM09.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM09.Tag = "";
            this.ITEM09.Text = "ITEM08";
            this.ITEM09.Top = 0.03779528F;
            this.ITEM09.Width = 0.7641734F;
            // 
            // ITEM10
            // 
            this.ITEM10.DataField = "ITEM09";
            this.ITEM10.Height = 0.15625F;
            this.ITEM10.Left = 6.440158F;
            this.ITEM10.Name = "ITEM10";
            this.ITEM10.OutputFormat = resources.GetString("ITEM10.OutputFormat");
            this.ITEM10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM10.Tag = "";
            this.ITEM10.Text = "ITEM09";
            this.ITEM10.Top = 0.03779528F;
            this.ITEM10.Width = 0.8267716F;
            // 
            // 直線30
            // 
            this.直線30.Height = 0.1979167F;
            this.直線30.Left = 0.1943085F;
            this.直線30.LineWeight = 1F;
            this.直線30.Name = "直線30";
            this.直線30.Tag = "";
            this.直線30.Top = 0F;
            this.直線30.Width = 0F;
            this.直線30.X1 = 0.1943085F;
            this.直線30.X2 = 0.1943085F;
            this.直線30.Y1 = 0F;
            this.直線30.Y2 = 0.1979167F;
            // 
            // 直線31
            // 
            this.直線31.Height = 0.1979167F;
            this.直線31.Left = 7.344095F;
            this.直線31.LineWeight = 1F;
            this.直線31.Name = "直線31";
            this.直線31.Tag = "";
            this.直線31.Top = 0F;
            this.直線31.Width = 0F;
            this.直線31.X1 = 7.344095F;
            this.直線31.X2 = 7.344095F;
            this.直線31.Y1 = 0F;
            this.直線31.Y2 = 0.1979167F;
            // 
            // 直線40
            // 
            this.直線40.Height = 0.1965278F;
            this.直線40.Left = 1.003543F;
            this.直線40.LineWeight = 1F;
            this.直線40.Name = "直線40";
            this.直線40.Tag = "";
            this.直線40.Top = 0F;
            this.直線40.Width = 0F;
            this.直線40.X1 = 1.003543F;
            this.直線40.X2 = 1.003543F;
            this.直線40.Y1 = 0F;
            this.直線40.Y2 = 0.1965278F;
            // 
            // 直線42
            // 
            this.直線42.Height = 0.1965278F;
            this.直線42.Left = 3.906693F;
            this.直線42.LineWeight = 1F;
            this.直線42.Name = "直線42";
            this.直線42.Tag = "";
            this.直線42.Top = 0F;
            this.直線42.Width = 0F;
            this.直線42.X1 = 3.906693F;
            this.直線42.X2 = 3.906693F;
            this.直線42.Y1 = 0F;
            this.直線42.Y2 = 0.1965278F;
            // 
            // 直線44
            // 
            this.直線44.Height = 0.1965278F;
            this.直線44.Left = 4.787008F;
            this.直線44.LineWeight = 1F;
            this.直線44.Name = "直線44";
            this.直線44.Tag = "";
            this.直線44.Top = 0F;
            this.直線44.Width = 0F;
            this.直線44.X1 = 4.787008F;
            this.直線44.X2 = 4.787008F;
            this.直線44.Y1 = 0F;
            this.直線44.Y2 = 0.1965278F;
            // 
            // 直線45
            // 
            this.直線45.Height = 0.1965278F;
            this.直線45.Left = 5.613386F;
            this.直線45.LineWeight = 1F;
            this.直線45.Name = "直線45";
            this.直線45.Tag = "";
            this.直線45.Top = 0F;
            this.直線45.Width = 0F;
            this.直線45.X1 = 5.613386F;
            this.直線45.X2 = 5.613386F;
            this.直線45.Y1 = 0F;
            this.直線45.Y2 = 0.1965278F;
            // 
            // 直線46
            // 
            this.直線46.Height = 0.1965278F;
            this.直線46.Left = 6.440158F;
            this.直線46.LineWeight = 1F;
            this.直線46.Name = "直線46";
            this.直線46.Tag = "";
            this.直線46.Top = 0F;
            this.直線46.Width = 0F;
            this.直線46.X1 = 6.440158F;
            this.直線46.X2 = 6.440158F;
            this.直線46.Y1 = 0F;
            this.直線46.Y2 = 0.1965278F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.1944882F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 0.1980315F;
            this.line1.Width = 7.149606F;
            this.line1.X1 = 0.1944882F;
            this.line1.X2 = 7.344094F;
            this.line1.Y1 = 0.1980315F;
            this.line1.Y2 = 0.1980315F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Format += new System.EventHandler(this.pageFooter_Format);
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox3,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9});
            this.reportFooter1.Height = 0.28125F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // textBox3
            // 
            this.textBox3.Height = 0.15625F;
            this.textBox3.Left = 0.998425F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 128";
            this.textBox3.Tag = "";
            this.textBox3.Text = "【　　合　　計　　】";
            this.textBox3.Top = 0.03779528F;
            this.textBox3.Width = 2.902362F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM07";
            this.textBox5.Height = 0.15625F;
            this.textBox5.Left = 4.781496F;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM07";
            this.textBox5.Top = 0.03779528F;
            this.textBox5.Width = 0.7748035F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM08";
            this.textBox6.Height = 0.15625F;
            this.textBox6.Left = 5.608268F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.textBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM08";
            this.textBox6.Top = 0.03779528F;
            this.textBox6.Width = 0.7692918F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM09";
            this.textBox7.Height = 0.15625F;
            this.textBox7.Left = 6.43504F;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.textBox7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM09";
            this.textBox7.Top = 0.03779528F;
            this.textBox7.Width = 0.8267716F;
            // 
            // line2
            // 
            this.line2.Height = 0.1979167F;
            this.line2.Left = 0.1944882F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Tag = "";
            this.line2.Top = 0F;
            this.line2.Width = 0F;
            this.line2.X1 = 0.1944882F;
            this.line2.X2 = 0.1944882F;
            this.line2.Y1 = 0F;
            this.line2.Y2 = 0.1979167F;
            // 
            // line3
            // 
            this.line3.Height = 0.1979167F;
            this.line3.Left = 7.344094F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Tag = "";
            this.line3.Top = 0F;
            this.line3.Width = 0F;
            this.line3.X1 = 7.344094F;
            this.line3.X2 = 7.344094F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0.1979167F;
            // 
            // line4
            // 
            this.line4.Height = 0.1965278F;
            this.line4.Left = 0.998425F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Tag = "";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 0.998425F;
            this.line4.X2 = 0.998425F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0.1965278F;
            // 
            // line5
            // 
            this.line5.Height = 0.1965278F;
            this.line5.Left = 3.901575F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Tag = "";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 3.901575F;
            this.line5.X2 = 3.901575F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0.1965278F;
            // 
            // line6
            // 
            this.line6.Height = 0.1965278F;
            this.line6.Left = 4.78189F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Tag = "";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 4.78189F;
            this.line6.X2 = 4.78189F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 0.1965278F;
            // 
            // line7
            // 
            this.line7.Height = 0.1965278F;
            this.line7.Left = 5.608268F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Tag = "";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 5.608268F;
            this.line7.X2 = 5.608268F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.1965278F;
            // 
            // line8
            // 
            this.line8.Height = 0.1965278F;
            this.line8.Left = 6.43504F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Tag = "";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 6.43504F;
            this.line8.X2 = 6.43504F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.1965278F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 0.1944882F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Tag = "";
            this.line9.Top = 0.1940945F;
            this.line9.Width = 7.149606F;
            this.line9.X1 = 0.1944882F;
            this.line9.X2 = 7.344094F;
            this.line9.Y1 = 0.1940945F;
            this.line9.Y2 = 0.1940945F;
            // 
            // KBDR1021R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5905512F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.447917F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalRowNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nowRouNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM02;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM10;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線30;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線31;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線40;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線42;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線44;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線45;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線46;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル10;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル11;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル14;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル15;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル16;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線34;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線35;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線37;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線38;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線39;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox nowRouNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox totalRowNo;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
    }
}
