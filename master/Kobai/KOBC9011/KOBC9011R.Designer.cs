﻿namespace jp.co.fsi.kob.kobc9011
{
    /// <summary>
    /// KOBC9011R の概要の説明です。
    /// </summary>
    partial class KOBC9011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KOBC9011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtCompanyName,
            this.lblTitle,
            this.line1,
            this.txtTitle01,
            this.txtTitle02,
            this.txtTitle03,
            this.txtTitle04,
            this.txtTitle05,
            this.txtTitle06,
            this.txtTitle07,
            this.txtTitle08,
            this.txtTitle09,
            this.txtTitle10,
            this.txtTitle11,
            this.txtTitle12,
            this.txtTitle13,
            this.txtTitle14,
            this.txtTitle15,
            this.txtTitle16,
            this.txtTitle17,
            this.txtTitle18,
            this.txtTitle19,
            this.txtTitle20,
            this.line2,
            this.line3,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25});
            this.pageHeader.Height = 1.374016F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.2070866F;
            this.txtToday.Left = 9.659843F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtToday.Text = "ggyy年M月d日";
            this.txtToday.Top = 0.4129921F;
            this.txtToday.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2070866F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 11.13623F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.4129921F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.2070866F;
            this.txtPageCount.Left = 10.84095F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.4129921F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0.07362206F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.txtCompanyName.Text = "111";
            this.txtCompanyName.Top = 0.372441F;
            this.txtCompanyName.Width = 4.650787F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.2464567F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 4.72441F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center";
            this.lblTitle.Text = "仕入先一覧表";
            this.lblTitle.Top = 0.06929134F;
            this.lblTitle.Width = 1.846457F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 4.72441F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.3157481F;
            this.line1.Width = 1.846457F;
            this.line1.X1 = 4.72441F;
            this.line1.X2 = 6.570867F;
            this.line1.Y1 = 0.3157481F;
            this.line1.Y2 = 0.3157481F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.2901574F;
            this.txtTitle01.Left = 0.01181102F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle01.Text = "コード";
            this.txtTitle01.Top = 0.9393702F;
            this.txtTitle01.Width = 0.456693F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.2791339F;
            this.txtTitle02.Left = 0.468504F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold" +
    "; text-align: center; vertical-align: middle";
            this.txtTitle02.Text = "仕入先名";
            this.txtTitle02.Top = 0.9503938F;
            this.txtTitle02.Width = 1.652362F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.2901576F;
            this.txtTitle03.Left = 2.120866F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold" +
    "; text-align: center; vertical-align: middle";
            this.txtTitle03.Text = "住所";
            this.txtTitle03.Top = 0.9393702F;
            this.txtTitle03.Width = 1.961418F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.1968504F;
            this.txtTitle04.Left = 4.082677F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle04.Text = "代表者名";
            this.txtTitle04.Top = 0.7830709F;
            this.txtTitle04.Width = 1.270473F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.Height = 0.1968504F;
            this.txtTitle05.Left = 4.082677F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle05.Text = "電話番号";
            this.txtTitle05.Top = 0.9692914F;
            this.txtTitle05.Width = 1.270473F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.Height = 0.1968504F;
            this.txtTitle06.Left = 4.082677F;
            this.txtTitle06.MultiLine = false;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle06.Text = "FAX番号";
            this.txtTitle06.Top = 1.166142F;
            this.txtTitle06.Width = 1.270473F;
            // 
            // txtTitle07
            // 
            this.txtTitle07.Height = 0.2944882F;
            this.txtTitle07.Left = 5.35315F;
            this.txtTitle07.MultiLine = false;
            this.txtTitle07.Name = "txtTitle07";
            this.txtTitle07.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle07.Text = "締日\r\n";
            this.txtTitle07.Top = 0.7830709F;
            this.txtTitle07.Width = 0.9779529F;
            // 
            // txtTitle08
            // 
            this.txtTitle08.Height = 0.2854331F;
            this.txtTitle08.Left = 5.35315F;
            this.txtTitle08.MultiLine = false;
            this.txtTitle08.Name = "txtTitle08";
            this.txtTitle08.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle08.Text = "回収日\r\n";
            this.txtTitle08.Top = 1.088583F;
            this.txtTitle08.Width = 0.977953F;
            // 
            // txtTitle09
            // 
            this.txtTitle09.Height = 0.1968504F;
            this.txtTitle09.Left = 6.331103F;
            this.txtTitle09.MultiLine = false;
            this.txtTitle09.Name = "txtTitle09";
            this.txtTitle09.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle09.Text = "消費税端数処理";
            this.txtTitle09.Top = 0.772441F;
            this.txtTitle09.Width = 1.876771F;
            // 
            // txtTitle10
            // 
            this.txtTitle10.Height = 0.1968504F;
            this.txtTitle10.Left = 6.331103F;
            this.txtTitle10.MultiLine = false;
            this.txtTitle10.Name = "txtTitle10";
            this.txtTitle10.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle10.Text = "消費税転嫁方法";
            this.txtTitle10.Top = 0.9692914F;
            this.txtTitle10.Width = 1.876771F;
            // 
            // txtTitle11
            // 
            this.txtTitle11.Height = 0.1968504F;
            this.txtTitle11.Left = 6.331103F;
            this.txtTitle11.MultiLine = false;
            this.txtTitle11.Name = "txtTitle11";
            this.txtTitle11.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle11.Text = "消費税入力方法";
            this.txtTitle11.Top = 1.166142F;
            this.txtTitle11.Width = 1.876771F;
            // 
            // txtTitle12
            // 
            this.txtTitle12.Height = 0.1968504F;
            this.txtTitle12.Left = 8.207874F;
            this.txtTitle12.MultiLine = false;
            this.txtTitle12.Name = "txtTitle12";
            this.txtTitle12.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle12.Text = "事業区分";
            this.txtTitle12.Top = 0.7830709F;
            this.txtTitle12.Width = 1.109056F;
            // 
            // txtTitle13
            // 
            this.txtTitle13.Height = 0.1968504F;
            this.txtTitle13.Left = 8.207874F;
            this.txtTitle13.MultiLine = false;
            this.txtTitle13.Name = "txtTitle13";
            this.txtTitle13.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle13.Text = "単価取得";
            this.txtTitle13.Top = 0.9799213F;
            this.txtTitle13.Width = 1.109056F;
            // 
            // txtTitle14
            // 
            this.txtTitle14.Height = 0.1968504F;
            this.txtTitle14.Left = 8.207874F;
            this.txtTitle14.MultiLine = false;
            this.txtTitle14.Name = "txtTitle14";
            this.txtTitle14.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle14.Text = "金額端数";
            this.txtTitle14.Top = 1.166142F;
            this.txtTitle14.Width = 1.109056F;
            // 
            // txtTitle15
            // 
            this.txtTitle15.Height = 0.1968504F;
            this.txtTitle15.Left = 9.31693F;
            this.txtTitle15.MultiLine = false;
            this.txtTitle15.Name = "txtTitle15";
            this.txtTitle15.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle15.Text = "担当者";
            this.txtTitle15.Top = 0.7834646F;
            this.txtTitle15.Width = 0.9311028F;
            // 
            // txtTitle16
            // 
            this.txtTitle16.Height = 0.1968504F;
            this.txtTitle16.Left = 9.31693F;
            this.txtTitle16.MultiLine = false;
            this.txtTitle16.Name = "txtTitle16";
            this.txtTitle16.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle16.Text = "店舗/取引先";
            this.txtTitle16.Top = 0.9692914F;
            this.txtTitle16.Width = 0.9181099F;
            // 
            // txtTitle17
            // 
            this.txtTitle17.Height = 0.1968504F;
            this.txtTitle17.Left = 9.31693F;
            this.txtTitle17.MultiLine = false;
            this.txtTitle17.Name = "txtTitle17";
            this.txtTitle17.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle17.Text = "分類/伝票区";
            this.txtTitle17.Top = 1.166142F;
            this.txtTitle17.Width = 0.9311028F;
            // 
            // txtTitle18
            // 
            this.txtTitle18.Height = 0.1968504F;
            this.txtTitle18.Left = 10.22717F;
            this.txtTitle18.MultiLine = false;
            this.txtTitle18.Name = "txtTitle18";
            this.txtTitle18.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle18.Text = "仕入先区分1";
            this.txtTitle18.Top = 0.7834646F;
            this.txtTitle18.Width = 1.060236F;
            // 
            // txtTitle19
            // 
            this.txtTitle19.Height = 0.1968504F;
            this.txtTitle19.Left = 10.24291F;
            this.txtTitle19.MultiLine = false;
            this.txtTitle19.Name = "txtTitle19";
            this.txtTitle19.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle19.Text = "仕入先区分2";
            this.txtTitle19.Top = 0.9692914F;
            this.txtTitle19.Width = 1.052362F;
            // 
            // txtTitle20
            // 
            this.txtTitle20.Height = 0.1968504F;
            this.txtTitle20.Left = 10.22717F;
            this.txtTitle20.MultiLine = false;
            this.txtTitle20.Name = "txtTitle20";
            this.txtTitle20.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle20.Text = "仕入先区分3";
            this.txtTitle20.Top = 1.166142F;
            this.txtTitle20.Width = 1.060236F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.01181102F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.772441F;
            this.line2.Width = 11.28347F;
            this.line2.X1 = 0.01181102F;
            this.line2.X2 = 11.29528F;
            this.line2.Y1 = 0.772441F;
            this.line2.Y2 = 0.772441F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0.01181102F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 1.362992F;
            this.line3.Width = 11.28346F;
            this.line3.X1 = 0.01181102F;
            this.line3.X2 = 11.29528F;
            this.line3.Y1 = 1.362992F;
            this.line3.Y2 = 1.362992F;
            // 
            // line16
            // 
            this.line16.Height = 0.590551F;
            this.line16.Left = 0.4685039F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.7724411F;
            this.line16.Width = 1.192093E-07F;
            this.line16.X1 = 0.468504F;
            this.line16.X2 = 0.4685039F;
            this.line16.Y1 = 1.362992F;
            this.line16.Y2 = 0.7724411F;
            // 
            // line17
            // 
            this.line17.Height = 0.5905513F;
            this.line17.Left = 2.120866F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0.7724408F;
            this.line17.Width = 0F;
            this.line17.X1 = 2.120866F;
            this.line17.X2 = 2.120866F;
            this.line17.Y1 = 1.362992F;
            this.line17.Y2 = 0.7724408F;
            // 
            // line18
            // 
            this.line18.Height = 0.5905513F;
            this.line18.Left = 4.082284F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0.7724408F;
            this.line18.Width = 0F;
            this.line18.X1 = 4.082284F;
            this.line18.X2 = 4.082284F;
            this.line18.Y1 = 1.362992F;
            this.line18.Y2 = 0.7724408F;
            // 
            // line19
            // 
            this.line19.Height = 0.5905517F;
            this.line19.Left = 5.35315F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.7834644F;
            this.line19.Width = 0F;
            this.line19.X1 = 5.35315F;
            this.line19.X2 = 5.35315F;
            this.line19.Y1 = 1.374016F;
            this.line19.Y2 = 0.7834644F;
            // 
            // line20
            // 
            this.line20.Height = 0.5905517F;
            this.line20.Left = 6.331103F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.7834644F;
            this.line20.Width = 0F;
            this.line20.X1 = 6.331103F;
            this.line20.X2 = 6.331103F;
            this.line20.Y1 = 1.374016F;
            this.line20.Y2 = 0.7834644F;
            // 
            // line21
            // 
            this.line21.Height = 0.5905517F;
            this.line21.Left = 8.207874F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.7834644F;
            this.line21.Width = 0F;
            this.line21.X1 = 8.207874F;
            this.line21.X2 = 8.207874F;
            this.line21.Y1 = 1.374016F;
            this.line21.Y2 = 0.7834644F;
            // 
            // line22
            // 
            this.line22.Height = 0.5905517F;
            this.line22.Left = 9.31693F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.7834644F;
            this.line22.Width = 0F;
            this.line22.X1 = 9.31693F;
            this.line22.X2 = 9.31693F;
            this.line22.Y1 = 1.374016F;
            this.line22.Y2 = 0.7834644F;
            // 
            // line23
            // 
            this.line23.Height = 0.5905511F;
            this.line23.Left = 10.23504F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.7830708F;
            this.line23.Width = 0F;
            this.line23.X1 = 10.23504F;
            this.line23.X2 = 10.23504F;
            this.line23.Y1 = 1.373622F;
            this.line23.Y2 = 0.7830708F;
            // 
            // line24
            // 
            this.line24.Height = 0.5905513F;
            this.line24.Left = 11.29528F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.7724408F;
            this.line24.Width = 0F;
            this.line24.X1 = 11.29528F;
            this.line24.X2 = 11.29528F;
            this.line24.Y1 = 1.362992F;
            this.line24.Y2 = 0.7724408F;
            // 
            // line25
            // 
            this.line25.Height = 0.5905514F;
            this.line25.Left = 0.01181102F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.7724407F;
            this.line25.Width = 0F;
            this.line25.X1 = 0.01181102F;
            this.line25.X2 = 0.01181102F;
            this.line25.Y1 = 1.362992F;
            this.line25.Y2 = 0.7724407F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label3,
            this.label4,
            this.txtValue01,
            this.txtValue02,
            this.txtValue03,
            this.txtValue10,
            this.txtValue11,
            this.label1,
            this.txtValue06,
            this.txtValue12,
            this.label2,
            this.txtValue05,
            this.txtValue04,
            this.txtValue07,
            this.txtValue08,
            this.txtValue09,
            this.txtValue13,
            this.txtValue14,
            this.txtValue15,
            this.txtValue16,
            this.txtValue17,
            this.txtValue18,
            this.txtValue19,
            this.txtValue20,
            this.txtValue21,
            this.txtValue22,
            this.txtValue23,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15});
            this.detail.Height = 0.6055118F;
            this.detail.Name = "detail";
            // 
            // label3
            // 
            this.label3.Height = 0.1968504F;
            this.label3.HyperLink = null;
            this.label3.Left = 4.12441F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.label3.Text = "TEL:";
            this.label3.Top = 0.2031496F;
            this.label3.Width = 0.3082677F;
            // 
            // label4
            // 
            this.label4.Height = 0.1968504F;
            this.label4.HyperLink = null;
            this.label4.Left = 4.12441F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.label4.Text = "FAX:";
            this.label4.Top = 0.3937008F;
            this.label4.Width = 0.3082677F;
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM02";
            this.txtValue01.Height = 0.1968504F;
            this.txtValue01.Left = 0F;
            this.txtValue01.LineSpacing = 1F;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: middle; ddo-char-set: 1";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0.1968504F;
            this.txtValue01.Width = 0.468504F;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM03";
            this.txtValue02.Height = 0.1574803F;
            this.txtValue02.Left = 0.5102363F;
            this.txtValue02.LineSpacing = 1F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0.1377953F;
            this.txtValue02.Width = 1.61063F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM04";
            this.txtValue03.Height = 0.1968504F;
            this.txtValue03.Left = 0.5102363F;
            this.txtValue03.LineSpacing = 1F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue03.Text = "\r\n";
            this.txtValue03.Top = 0.2952756F;
            this.txtValue03.Width = 1.61063F;
            // 
            // txtValue10
            // 
            this.txtValue10.DataField = "ITEM11";
            this.txtValue10.Height = 0.2952756F;
            this.txtValue10.Left = 5.436615F;
            this.txtValue10.LineSpacing = 1F;
            this.txtValue10.Name = "txtValue10";
            this.txtValue10.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue10.Text = null;
            this.txtValue10.Top = 0F;
            this.txtValue10.Width = 0.8944883F;
            // 
            // txtValue11
            // 
            this.txtValue11.DataField = "ITEM12";
            this.txtValue11.Height = 0.2952756F;
            this.txtValue11.Left = 5.436615F;
            this.txtValue11.LineSpacing = 1F;
            this.txtValue11.Name = "txtValue11";
            this.txtValue11.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue11.Text = "\r\n";
            this.txtValue11.Top = 0.2952756F;
            this.txtValue11.Width = 0.8944883F;
            // 
            // label1
            // 
            this.label1.Height = 0.1968504F;
            this.label1.HyperLink = null;
            this.label1.Left = 2.572047F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.label1.Text = "-";
            this.label1.Top = 0.03228347F;
            this.label1.Width = 0.1484252F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM07";
            this.txtValue06.Height = 0.1968504F;
            this.txtValue06.Left = 2.182678F;
            this.txtValue06.LineSpacing = 1F;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue06.Top = 0.2291338F;
            this.txtValue06.Width = 1.899606F;
            // 
            // txtValue12
            // 
            this.txtValue12.DataField = "ITEM13";
            this.txtValue12.Height = 0.1968504F;
            this.txtValue12.Left = 6.394095F;
            this.txtValue12.LineSpacing = 1F;
            this.txtValue12.Name = "txtValue12";
            this.txtValue12.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue12.Text = null;
            this.txtValue12.Top = 0F;
            this.txtValue12.Width = 1.761812F;
            // 
            // label2
            // 
            this.label2.Height = 0.1968504F;
            this.label2.HyperLink = null;
            this.label2.Left = 2.182678F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.label2.Text = "〒";
            this.label2.Top = 0.03228347F;
            this.label2.Width = 0.1484252F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM06";
            this.txtValue05.Height = 0.1968504F;
            this.txtValue05.Left = 2.720473F;
            this.txtValue05.LineSpacing = 1F;
            this.txtValue05.MultiLine = false;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0.03228343F;
            this.txtValue05.Width = 0.3811024F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM05";
            this.txtValue04.Height = 0.1968504F;
            this.txtValue04.Left = 2.331104F;
            this.txtValue04.LineSpacing = 1F;
            this.txtValue04.MultiLine = false;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0.03228347F;
            this.txtValue04.Width = 0.2948819F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM08";
            this.txtValue07.Height = 0.1968504F;
            this.txtValue07.Left = 4.12441F;
            this.txtValue07.LineSpacing = 1F;
            this.txtValue07.MultiLine = false;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0.03228347F;
            this.txtValue07.Width = 1.172441F;
            // 
            // txtValue08
            // 
            this.txtValue08.DataField = "ITEM09";
            this.txtValue08.Height = 0.1968504F;
            this.txtValue08.Left = 4.432677F;
            this.txtValue08.LineSpacing = 1F;
            this.txtValue08.Name = "txtValue08";
            this.txtValue08.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 0";
            this.txtValue08.Text = "090-1111-1234";
            this.txtValue08.Top = 0.2031496F;
            this.txtValue08.Width = 0.9204725F;
            // 
            // txtValue09
            // 
            this.txtValue09.DataField = "ITEM10";
            this.txtValue09.Height = 0.1968504F;
            this.txtValue09.Left = 4.432677F;
            this.txtValue09.LineSpacing = 1F;
            this.txtValue09.Name = "txtValue09";
            this.txtValue09.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 0";
            this.txtValue09.Text = null;
            this.txtValue09.Top = 0.3937008F;
            this.txtValue09.Width = 0.9204725F;
            // 
            // txtValue13
            // 
            this.txtValue13.DataField = "ITEM14";
            this.txtValue13.Height = 0.1968504F;
            this.txtValue13.Left = 6.394095F;
            this.txtValue13.LineSpacing = 1F;
            this.txtValue13.Name = "txtValue13";
            this.txtValue13.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue13.Text = null;
            this.txtValue13.Top = 0.1897638F;
            this.txtValue13.Width = 1.761812F;
            // 
            // txtValue14
            // 
            this.txtValue14.DataField = "ITEM15";
            this.txtValue14.Height = 0.1968504F;
            this.txtValue14.Left = 6.394095F;
            this.txtValue14.LineSpacing = 1F;
            this.txtValue14.Name = "txtValue14";
            this.txtValue14.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue14.Text = null;
            this.txtValue14.Top = 0.3866142F;
            this.txtValue14.Width = 1.761811F;
            // 
            // txtValue15
            // 
            this.txtValue15.DataField = "ITEM16";
            this.txtValue15.Height = 0.1968504F;
            this.txtValue15.Left = 8.303938F;
            this.txtValue15.LineSpacing = 1F;
            this.txtValue15.Name = "txtValue15";
            this.txtValue15.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue15.Text = null;
            this.txtValue15.Top = 0F;
            this.txtValue15.Width = 1.012992F;
            // 
            // txtValue16
            // 
            this.txtValue16.DataField = "ITEM17";
            this.txtValue16.Height = 0.1968504F;
            this.txtValue16.Left = 8.303938F;
            this.txtValue16.LineSpacing = 1F;
            this.txtValue16.Name = "txtValue16";
            this.txtValue16.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue16.Text = null;
            this.txtValue16.Top = 0.1897638F;
            this.txtValue16.Width = 1.012992F;
            // 
            // txtValue17
            // 
            this.txtValue17.DataField = "ITEM18";
            this.txtValue17.Height = 0.1968504F;
            this.txtValue17.Left = 8.303938F;
            this.txtValue17.LineSpacing = 1F;
            this.txtValue17.Name = "txtValue17";
            this.txtValue17.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue17.Text = null;
            this.txtValue17.Top = 0.4F;
            this.txtValue17.Width = 1.012992F;
            // 
            // txtValue18
            // 
            this.txtValue18.DataField = "ITEM19";
            this.txtValue18.Height = 0.1968504F;
            this.txtValue18.Left = 9.369292F;
            this.txtValue18.LineSpacing = 1F;
            this.txtValue18.Name = "txtValue18";
            this.txtValue18.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue18.Text = null;
            this.txtValue18.Top = 0F;
            this.txtValue18.Width = 0.8657484F;
            // 
            // txtValue19
            // 
            this.txtValue19.DataField = "ITEM20";
            this.txtValue19.Height = 0.1968504F;
            this.txtValue19.Left = 9.369292F;
            this.txtValue19.LineSpacing = 1F;
            this.txtValue19.Name = "txtValue19";
            this.txtValue19.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue19.Text = null;
            this.txtValue19.Top = 0.1897638F;
            this.txtValue19.Width = 0.8657484F;
            // 
            // txtValue20
            // 
            this.txtValue20.DataField = "ITEM21";
            this.txtValue20.Height = 0.1968504F;
            this.txtValue20.Left = 9.369292F;
            this.txtValue20.LineSpacing = 1F;
            this.txtValue20.Name = "txtValue20";
            this.txtValue20.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue20.Text = null;
            this.txtValue20.Top = 0.3866142F;
            this.txtValue20.Width = 0.8657484F;
            // 
            // txtValue21
            // 
            this.txtValue21.DataField = "ITEM22";
            this.txtValue21.Height = 0.1968504F;
            this.txtValue21.Left = 10.29488F;
            this.txtValue21.LineSpacing = 1F;
            this.txtValue21.Name = "txtValue21";
            this.txtValue21.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue21.Text = null;
            this.txtValue21.Top = 0F;
            this.txtValue21.Width = 1.000393F;
            // 
            // txtValue22
            // 
            this.txtValue22.DataField = "ITEM23";
            this.txtValue22.Height = 0.1968504F;
            this.txtValue22.Left = 10.29488F;
            this.txtValue22.LineSpacing = 1F;
            this.txtValue22.Name = "txtValue22";
            this.txtValue22.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue22.Text = null;
            this.txtValue22.Top = 0.1897638F;
            this.txtValue22.Width = 1.012205F;
            // 
            // txtValue23
            // 
            this.txtValue23.DataField = "ITEM24";
            this.txtValue23.Height = 0.1968504F;
            this.txtValue23.Left = 10.29488F;
            this.txtValue23.LineSpacing = 1F;
            this.txtValue23.Name = "txtValue23";
            this.txtValue23.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtValue23.Text = null;
            this.txtValue23.Top = 0.3866142F;
            this.txtValue23.Width = 1.000393F;
            // 
            // line5
            // 
            this.line5.Height = 0F;
            this.line5.Left = 0.01181102F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.5905512F;
            this.line5.Width = 11.28347F;
            this.line5.X1 = 0.01181102F;
            this.line5.X2 = 11.29528F;
            this.line5.Y1 = 0.5905512F;
            this.line5.Y2 = 0.5905512F;
            // 
            // line6
            // 
            this.line6.Height = 0.5905512F;
            this.line6.Left = 0.468504F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 0.468504F;
            this.line6.X2 = 0.468504F;
            this.line6.Y1 = 0.5905512F;
            this.line6.Y2 = 0F;
            // 
            // line7
            // 
            this.line7.Height = 0.5905512F;
            this.line7.Left = 2.120866F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 2.120866F;
            this.line7.X2 = 2.120866F;
            this.line7.Y1 = 0.5905512F;
            this.line7.Y2 = 0F;
            // 
            // line8
            // 
            this.line8.Height = 0.5905512F;
            this.line8.Left = 4.082284F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 4.082284F;
            this.line8.X2 = 4.082284F;
            this.line8.Y1 = 0.5905512F;
            this.line8.Y2 = 0F;
            // 
            // line9
            // 
            this.line9.Height = 0.5905512F;
            this.line9.Left = 5.35315F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 5.35315F;
            this.line9.X2 = 5.35315F;
            this.line9.Y1 = 0.5905512F;
            this.line9.Y2 = 0F;
            // 
            // line10
            // 
            this.line10.Height = 0.5905512F;
            this.line10.Left = 6.331103F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 6.331103F;
            this.line10.X2 = 6.331103F;
            this.line10.Y1 = 0.5905512F;
            this.line10.Y2 = 0F;
            // 
            // line11
            // 
            this.line11.Height = 0.5905512F;
            this.line11.Left = 8.207874F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 8.207874F;
            this.line11.X2 = 8.207874F;
            this.line11.Y1 = 0.5905512F;
            this.line11.Y2 = 0F;
            // 
            // line12
            // 
            this.line12.Height = 0.5905512F;
            this.line12.Left = 9.31693F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 9.31693F;
            this.line12.X2 = 9.31693F;
            this.line12.Y1 = 0.5905512F;
            this.line12.Y2 = 0F;
            // 
            // line13
            // 
            this.line13.Height = 0.5905512F;
            this.line13.Left = 10.23504F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 10.23504F;
            this.line13.X2 = 10.23504F;
            this.line13.Y1 = 0.5905512F;
            this.line13.Y2 = 0F;
            // 
            // line14
            // 
            this.line14.Height = 0.5905512F;
            this.line14.Left = 11.29528F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = -3.72529E-09F;
            this.line14.Width = 0F;
            this.line14.X1 = 11.29528F;
            this.line14.X2 = 11.29528F;
            this.line14.Y1 = 0.5905512F;
            this.line14.Y2 = -3.72529E-09F;
            // 
            // line15
            // 
            this.line15.Height = 0.5905512F;
            this.line15.Left = 0.01181102F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 0.01181102F;
            this.line15.X2 = 0.01181102F;
            this.line15.Y1 = 0.5905512F;
            this.line15.Y2 = 0F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.5795275F;
            this.textBox1.Left = 0.01181102F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: cent" +
    "er; vertical-align: middle";
            this.textBox1.Top = 0.7834646F;
            this.textBox1.Width = 11.28347F;
            // 
            // KOBC9011R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 11.29528F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
    }
}
