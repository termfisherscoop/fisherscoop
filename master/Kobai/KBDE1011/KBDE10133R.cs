﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbde1011
{
    /// <summary>
    /// KBDE1013_2R の概要の説明です。
    /// </summary>
    public partial class KBDE10133R : BaseReport
    {

        public KBDE10133R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
        public KBDE10133R(DataTable tgtData, string repID) : base(tgtData, repID)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
