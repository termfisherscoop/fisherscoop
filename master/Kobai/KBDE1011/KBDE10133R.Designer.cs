﻿namespace jp.co.fsi.kb.kbde1011
{
    /// <summary>
    /// KBDE1013_2R の概要の説明です。
    /// </summary>
    partial class KBDE10133R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDE10133R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ラベル378 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.BackClrCg = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル340 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル359 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト389 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト332 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト351 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト370 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト231 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト342 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト361 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト380 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル203 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト258 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル196 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル201 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト204 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト205 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線206 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル207 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル208 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト209 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル210 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト211 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト212 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト213 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト215 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト217 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト218 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル219 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト220 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト230 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト232 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト236 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト237 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト238 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト331 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト333 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト337 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト338 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト339 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト341 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト343 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト347 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト348 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト349 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト350 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト352 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト356 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト357 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト358 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト360 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト362 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト366 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト367 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト368 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト369 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト371 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト375 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト376 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト377 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト379 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト381 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト385 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト386 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト387 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト388 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト390 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト394 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト395 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト396 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス309 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス310 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.テキスト311 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト312 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト313 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト314 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト315 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト318 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線407 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル414 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト415 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト416 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト417 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル418 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル420 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト422 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線423 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル424 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル425 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト426 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル427 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト428 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト429 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト430 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト431 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト432 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト433 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル434 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト435 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル535 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト642 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト643 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox81 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox83 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox84 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox85 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox86 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox87 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox88 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox89 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox90 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox91 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox92 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox93 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox94 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox95 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox96 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox97 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox98 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox99 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox101 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル378)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClrCg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル340)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル359)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト389)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト332)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト351)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト370)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト231)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト342)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト361)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト380)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル203)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト258)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル196)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル201)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト204)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト205)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル207)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル208)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト209)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル210)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト211)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト212)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト213)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト215)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト217)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト218)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル219)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト220)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト230)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト232)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト236)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト237)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト238)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト331)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト333)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト337)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト338)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト339)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト341)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト343)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト347)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト348)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト349)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト350)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト352)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト356)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト357)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト358)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト360)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト362)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト366)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト367)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト368)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト369)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト371)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト375)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト376)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト377)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト379)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト381)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト385)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト386)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト387)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト388)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト390)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト394)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト395)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト396)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト311)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト312)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト313)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト314)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト315)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト318)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル414)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト415)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト416)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト417)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル418)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル420)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト422)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル424)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル425)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト426)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル427)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト428)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト429)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト430)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト431)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト432)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト433)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル434)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト435)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル535)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト642)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト643)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル378,
            this.BackClrCg,
            this.ラベル340,
            this.ラベル359,
            this.テキスト389,
            this.テキスト332,
            this.テキスト351,
            this.テキスト370,
            this.テキスト231,
            this.テキスト342,
            this.テキスト361,
            this.テキスト380,
            this.ラベル203,
            this.テキスト258,
            this.ITEM02,
            this.ITEM01,
            this.ラベル71,
            this.ラベル196,
            this.ラベル201,
            this.テキスト204,
            this.テキスト205,
            this.直線206,
            this.ラベル207,
            this.ラベル208,
            this.テキスト209,
            this.ラベル210,
            this.テキスト211,
            this.テキスト212,
            this.テキスト213,
            this.テキスト215,
            this.テキスト217,
            this.テキスト218,
            this.ラベル219,
            this.テキスト220,
            this.テキスト230,
            this.テキスト232,
            this.テキスト236,
            this.テキスト237,
            this.テキスト238,
            this.テキスト331,
            this.テキスト333,
            this.テキスト337,
            this.テキスト338,
            this.テキスト339,
            this.テキスト341,
            this.テキスト343,
            this.テキスト347,
            this.テキスト348,
            this.テキスト349,
            this.テキスト350,
            this.テキスト352,
            this.テキスト356,
            this.テキスト357,
            this.テキスト358,
            this.テキスト360,
            this.テキスト362,
            this.テキスト366,
            this.テキスト367,
            this.テキスト368,
            this.テキスト369,
            this.テキスト371,
            this.テキスト375,
            this.テキスト376,
            this.テキスト377,
            this.テキスト379,
            this.テキスト381,
            this.テキスト385,
            this.テキスト386,
            this.テキスト387,
            this.テキスト388,
            this.テキスト390,
            this.テキスト394,
            this.テキスト395,
            this.テキスト396,
            this.ボックス309,
            this.ボックス310,
            this.テキスト311,
            this.テキスト312,
            this.テキスト313,
            this.テキスト314,
            this.テキスト315,
            this.テキスト316,
            this.テキスト317,
            this.テキスト318,
            this.直線407,
            this.ラベル414,
            this.テキスト415,
            this.テキスト416,
            this.テキスト417,
            this.ラベル418,
            this.ラベル420,
            this.テキスト422,
            this.直線423,
            this.ラベル424,
            this.ラベル425,
            this.テキスト426,
            this.ラベル427,
            this.テキスト428,
            this.テキスト429,
            this.テキスト430,
            this.テキスト431,
            this.テキスト432,
            this.テキスト433,
            this.ラベル434,
            this.テキスト435,
            this.ラベル535,
            this.テキスト642,
            this.テキスト643,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.shape10,
            this.shape11,
            this.textBox76,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox80,
            this.textBox81,
            this.textBox82,
            this.textBox83,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.line1,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.shape1,
            this.line2,
            this.textBox84,
            this.line27,
            this.textBox85,
            this.textBox86,
            this.textBox87,
            this.textBox88,
            this.textBox89,
            this.textBox90,
            this.textBox91,
            this.textBox92,
            this.textBox93,
            this.textBox94,
            this.textBox95,
            this.textBox96,
            this.textBox97,
            this.textBox98,
            this.textBox99,
            this.textBox100,
            this.textBox101,
            this.textBox102});
            this.detail.Height = 11.04449F;
            this.detail.Name = "detail";
            // 
            // ラベル378
            // 
            this.ラベル378.Height = 0.3354167F;
            this.ラベル378.HyperLink = null;
            this.ラベル378.Left = 0.1388885F;
            this.ラベル378.Name = "ラベル378";
            this.ラベル378.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.ラベル378.Tag = "";
            this.ラベル378.Text = "　";
            this.ラベル378.Top = 4.185174F;
            this.ラベル378.Visible = false;
            this.ラベル378.Width = 7.55F;
            // 
            // BackClrCg
            // 
            this.BackClrCg.Height = 0.3354167F;
            this.BackClrCg.HyperLink = null;
            this.BackClrCg.Left = 0.1388885F;
            this.BackClrCg.Name = "BackClrCg";
            this.BackClrCg.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.BackClrCg.Tag = "";
            this.BackClrCg.Text = "　";
            this.BackClrCg.Top = 2.167813F;
            this.BackClrCg.Visible = false;
            this.BackClrCg.Width = 7.55F;
            // 
            // ラベル340
            // 
            this.ラベル340.Height = 0.3354167F;
            this.ラベル340.HyperLink = null;
            this.ラベル340.Left = 0.1388885F;
            this.ラベル340.Name = "ラベル340";
            this.ラベル340.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.ラベル340.Tag = "";
            this.ラベル340.Text = "　";
            this.ラベル340.Top = 2.85323F;
            this.ラベル340.Visible = false;
            this.ラベル340.Width = 7.55F;
            // 
            // ラベル359
            // 
            this.ラベル359.Height = 0.3361111F;
            this.ラベル359.HyperLink = null;
            this.ラベル359.Left = 0.1388885F;
            this.ラベル359.Name = "ラベル359";
            this.ラベル359.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.ラベル359.Tag = "";
            this.ラベル359.Text = "　";
            this.ラベル359.Top = 3.512258F;
            this.ラベル359.Visible = false;
            this.ラベル359.Width = 7.55F;
            // 
            // テキスト389
            // 
            this.テキスト389.DataField = "ITEM78";
            this.テキスト389.Height = 0.1875F;
            this.テキスト389.Left = 0.9527559F;
            this.テキスト389.Name = "テキスト389";
            this.テキスト389.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト389.Tag = "";
            this.テキスト389.Text = "ITEM78";
            this.テキスト389.Top = 4.343307F;
            this.テキスト389.Width = 2.385417F;
            // 
            // テキスト332
            // 
            this.テキスト332.DataField = "ITEM24";
            this.テキスト332.Height = 0.1805556F;
            this.テキスト332.Left = 0.9527559F;
            this.テキスト332.Name = "テキスト332";
            this.テキスト332.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト332.Tag = "";
            this.テキスト332.Text = "ITEM24";
            this.テキスト332.Top = 2.555118F;
            this.テキスト332.Width = 2.385417F;
            // 
            // テキスト351
            // 
            this.テキスト351.DataField = "ITEM42";
            this.テキスト351.Height = 0.1875F;
            this.テキスト351.Left = 0.9527559F;
            this.テキスト351.Name = "テキスト351";
            this.テキスト351.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト351.Tag = "";
            this.テキスト351.Text = "ITEM42";
            this.テキスト351.Top = 3.13937F;
            this.テキスト351.Width = 2.385417F;
            // 
            // テキスト370
            // 
            this.テキスト370.DataField = "ITEM60";
            this.テキスト370.Height = 0.1875F;
            this.テキスト370.Left = 0.9527559F;
            this.テキスト370.Name = "テキスト370";
            this.テキスト370.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト370.Tag = "";
            this.テキスト370.Text = "ITEM60";
            this.テキスト370.Top = 3.750394F;
            this.テキスト370.Width = 2.385417F;
            // 
            // テキスト231
            // 
            this.テキスト231.DataField = "ITEM15";
            this.テキスト231.Height = 0.1875F;
            this.テキスト231.Left = 0.9527559F;
            this.テキスト231.Name = "テキスト231";
            this.テキスト231.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト231.Tag = "";
            this.テキスト231.Text = "ITEM15";
            this.テキスト231.Top = 2.294095F;
            this.テキスト231.Width = 2.385417F;
            // 
            // テキスト342
            // 
            this.テキスト342.DataField = "ITEM33";
            this.テキスト342.Height = 0.1875F;
            this.テキスト342.Left = 0.9527559F;
            this.テキスト342.Name = "テキスト342";
            this.テキスト342.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト342.Tag = "";
            this.テキスト342.Text = "ITEM33";
            this.テキスト342.Top = 2.857874F;
            this.テキスト342.Width = 2.385417F;
            // 
            // テキスト361
            // 
            this.テキスト361.DataField = "ITEM51";
            this.テキスト361.Height = 0.1875F;
            this.テキスト361.Left = 0.9527559F;
            this.テキスト361.Name = "テキスト361";
            this.テキスト361.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト361.Tag = "";
            this.テキスト361.Text = "ITEM51";
            this.テキスト361.Top = 3.438583F;
            this.テキスト361.Width = 2.385417F;
            // 
            // テキスト380
            // 
            this.テキスト380.DataField = "ITEM69";
            this.テキスト380.Height = 0.1875F;
            this.テキスト380.Left = 0.9527559F;
            this.テキスト380.Name = "テキスト380";
            this.テキスト380.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト380.Tag = "";
            this.テキスト380.Text = "ITEM69";
            this.テキスト380.Top = 4.051575F;
            this.テキスト380.Width = 2.385417F;
            // 
            // ラベル203
            // 
            this.ラベル203.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Height = 0.3951389F;
            this.ラベル203.HyperLink = null;
            this.ラベル203.Left = 3.098611F;
            this.ラベル203.Name = "ラベル203";
            this.ラベル203.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-weight: bold; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ラベル203.Tag = "";
            this.ラベル203.Text = "　";
            this.ラベル203.Top = 0.3087857F;
            this.ラベル203.Visible = false;
            this.ラベル203.Width = 1.811111F;
            // 
            // テキスト258
            // 
            this.テキスト258.DataField = "ITEM13";
            this.テキスト258.Height = 0.1875F;
            this.テキスト258.Left = 7.161024F;
            this.テキスト258.Name = "テキスト258";
            this.テキスト258.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト258.Tag = "";
            this.テキスト258.Text = "ITEM13";
            this.テキスト258.Top = 0.9476379F;
            this.テキスト258.Width = 0.3229167F;
            // 
            // ITEM02
            // 
            this.ITEM02.DataField = "ITEM02";
            this.ITEM02.Height = 0.1979167F;
            this.ITEM02.Left = 6.416536F;
            this.ITEM02.Name = "ITEM02";
            this.ITEM02.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ITEM02.Tag = "";
            this.ITEM02.Text = "ITEM02";
            this.ITEM02.Top = 0.9476379F;
            this.ITEM02.Width = 0.59375F;
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.1875F;
            this.ITEM01.Left = 6.270079F;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM01";
            this.ITEM01.Top = 0.7511811F;
            this.ITEM01.Width = 1.34375F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.1972222F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 7.47126F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "頁";
            this.ラベル71.Top = 0.9476379F;
            this.ラベル71.Width = 0.1715278F;
            // 
            // ラベル196
            // 
            this.ラベル196.Height = 0.2333333F;
            this.ラベル196.HyperLink = null;
            this.ラベル196.Left = 3.295139F;
            this.ラベル196.Name = "ラベル196";
            this.ラベル196.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt" +
    "; font-weight: normal; text-align: center; ddo-char-set: 128";
            this.ラベル196.Tag = "";
            this.ラベル196.Text = "納品書 (控え)";
            this.ラベル196.Top = 0.3907302F;
            this.ラベル196.Visible = false;
            this.ラベル196.Width = 1.415278F;
            // 
            // ラベル201
            // 
            this.ラベル201.Height = 0.1979167F;
            this.ラベル201.HyperLink = null;
            this.ラベル201.Left = 5.722221F;
            this.ラベル201.Name = "ラベル201";
            this.ラベル201.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル201.Tag = "";
            this.ラベル201.Text = "伝票番号：";
            this.ラベル201.Top = 0.5011469F;
            this.ラベル201.Visible = false;
            this.ラベル201.Width = 0.8333333F;
            // 
            // テキスト204
            // 
            this.テキスト204.DataField = "ITEM03";
            this.テキスト204.Height = 0.1875F;
            this.テキスト204.Left = 0.3326772F;
            this.テキスト204.Name = "テキスト204";
            this.テキスト204.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト204.Tag = "";
            this.テキスト204.Text = "ITEM03";
            this.テキスト204.Top = 0.5937008F;
            this.テキスト204.Width = 1.34375F;
            // 
            // テキスト205
            // 
            this.テキスト205.DataField = "ITEM04";
            this.テキスト205.Height = 0.1875F;
            this.テキスト205.Left = 0.3326772F;
            this.テキスト205.Name = "テキスト205";
            this.テキスト205.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.テキスト205.Tag = "";
            this.テキスト205.Text = "ITEM04";
            this.テキスト205.Top = 1.385433F;
            this.テキスト205.Width = 2.375F;
            // 
            // 直線206
            // 
            this.直線206.Height = 0F;
            this.直線206.Left = 0.09722185F;
            this.直線206.LineWeight = 0F;
            this.直線206.Name = "直線206";
            this.直線206.Tag = "";
            this.直線206.Top = 1.204619F;
            this.直線206.Visible = false;
            this.直線206.Width = 2.710417F;
            this.直線206.X1 = 0.09722185F;
            this.直線206.X2 = 2.807639F;
            this.直線206.Y1 = 1.204619F;
            this.直線206.Y2 = 1.204619F;
            // 
            // ラベル207
            // 
            this.ラベル207.Height = 0.1972222F;
            this.ラベル207.HyperLink = null;
            this.ラベル207.Left = 2.534722F;
            this.ラベル207.Name = "ラベル207";
            this.ラベル207.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル207.Tag = "";
            this.ラベル207.Text = "様";
            this.ラベル207.Top = 1.006702F;
            this.ラベル207.Visible = false;
            this.ラベル207.Width = 0.2444444F;
            // 
            // ラベル208
            // 
            this.ラベル208.Height = 0.15625F;
            this.ラベル208.HyperLink = null;
            this.ラベル208.Left = 0.1319441F;
            this.ラベル208.Name = "ラベル208";
            this.ラベル208.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル208.Tag = "";
            this.ラベル208.Text = "TEL";
            this.ラベル208.Top = 1.245591F;
            this.ラベル208.Visible = false;
            this.ラベル208.Width = 0.2395833F;
            // 
            // テキスト209
            // 
            this.テキスト209.DataField = "ITEM05";
            this.テキスト209.Height = 0.15625F;
            this.テキスト209.Left = 0.6472441F;
            this.テキスト209.Name = "テキスト209";
            this.テキスト209.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 128";
            this.テキスト209.Tag = "";
            this.テキスト209.Text = "ITEM05";
            this.テキスト209.Top = 1.649606F;
            this.テキスト209.Width = 0.9479167F;
            // 
            // ラベル210
            // 
            this.ラベル210.Height = 0.15625F;
            this.ラベル210.HyperLink = null;
            this.ラベル210.Left = 1.315972F;
            this.ラベル210.Name = "ラベル210";
            this.ラベル210.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル210.Tag = "";
            this.ラベル210.Text = "FAX";
            this.ラベル210.Top = 1.241424F;
            this.ラベル210.Visible = false;
            this.ラベル210.Width = 0.2395833F;
            // 
            // テキスト211
            // 
            this.テキスト211.DataField = "ITEM06";
            this.テキスト211.Height = 0.15625F;
            this.テキスト211.Left = 2.032284F;
            this.テキスト211.Name = "テキスト211";
            this.テキスト211.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 128";
            this.テキスト211.Tag = "";
            this.テキスト211.Text = "ITEM06";
            this.テキスト211.Top = 1.649606F;
            this.テキスト211.Width = 0.9791667F;
            // 
            // テキスト212
            // 
            this.テキスト212.DataField = "ITEM07";
            this.テキスト212.Height = 0.1875F;
            this.テキスト212.Left = 3.697638F;
            this.テキスト212.Name = "テキスト212";
            this.テキスト212.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト212.Tag = "";
            this.テキスト212.Text = "ITEM07";
            this.テキスト212.Top = 0.8179677F;
            this.テキスト212.Visible = false;
            this.テキスト212.Width = 2.21875F;
            // 
            // テキスト213
            // 
            this.テキスト213.DataField = "ITEM08";
            this.テキスト213.Height = 0.1875F;
            this.テキスト213.Left = 3.697638F;
            this.テキスト213.Name = "テキスト213";
            this.テキスト213.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト213.Tag = "";
            this.テキスト213.Text = "ITEM08";
            this.テキスト213.Top = 1.017273F;
            this.テキスト213.Visible = false;
            this.テキスト213.Width = 2.21875F;
            // 
            // テキスト215
            // 
            this.テキスト215.DataField = "ITEM09";
            this.テキスト215.Height = 0.15625F;
            this.テキスト215.Left = 3.697638F;
            this.テキスト215.Name = "テキスト215";
            this.テキスト215.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト215.Tag = "";
            this.テキスト215.Text = "ITEM09";
            this.テキスト215.Top = 1.204774F;
            this.テキスト215.Visible = false;
            this.テキスト215.Width = 2.21875F;
            // 
            // テキスト217
            // 
            this.テキスト217.DataField = "ITEM10";
            this.テキスト217.Height = 0.15625F;
            this.テキスト217.Left = 3.697638F;
            this.テキスト217.Name = "テキスト217";
            this.テキスト217.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト217.Tag = "";
            this.テキスト217.Text = "ITEM10";
            this.テキスト217.Top = 1.361024F;
            this.テキスト217.Visible = false;
            this.テキスト217.Width = 2.21875F;
            // 
            // テキスト218
            // 
            this.テキスト218.DataField = "ITEM11";
            this.テキスト218.Height = 0.1875F;
            this.テキスト218.Left = 3.041339F;
            this.テキスト218.Name = "テキスト218";
            this.テキスト218.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 128";
            this.テキスト218.Tag = "";
            this.テキスト218.Text = "ITEM11";
            this.テキスト218.Top = 1.705906F;
            this.テキスト218.Width = 1.811111F;
            // 
            // ラベル219
            // 
            this.ラベル219.Height = 0.15625F;
            this.ラベル219.HyperLink = null;
            this.ラベル219.Left = 6.003471F;
            this.ラベル219.Name = "ラベル219";
            this.ラベル219.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル219.Tag = "";
            this.ラベル219.Text = "担当：";
            this.ラベル219.Top = 1.475452F;
            this.ラベル219.Visible = false;
            this.ラベル219.Width = 0.4791667F;
            // 
            // テキスト220
            // 
            this.テキスト220.DataField = "ITEM12";
            this.テキスト220.Height = 0.15625F;
            this.テキスト220.Left = 6.552756F;
            this.テキスト220.Name = "テキスト220";
            this.テキスト220.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト220.Tag = "";
            this.テキスト220.Text = "ITEM12";
            this.テキスト220.Top = 1.705906F;
            this.テキスト220.Width = 1.038189F;
            // 
            // テキスト230
            // 
            this.テキスト230.DataField = "ITEM14";
            this.テキスト230.Height = 0.1875F;
            this.テキスト230.Left = 0.3511811F;
            this.テキスト230.Name = "テキスト230";
            this.テキスト230.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト230.Tag = "";
            this.テキスト230.Text = "ITEM14";
            this.テキスト230.Top = 2.294095F;
            this.テキスト230.Width = 0.5744094F;
            // 
            // テキスト232
            // 
            this.テキスト232.DataField = "ITEM16";
            this.テキスト232.Height = 0.1875F;
            this.テキスト232.Left = 3.42126F;
            this.テキスト232.Name = "テキスト232";
            this.テキスト232.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト232.Tag = "";
            this.テキスト232.Text = "ITEM16";
            this.テキスト232.Top = 2.294095F;
            this.テキスト232.Width = 0.9479167F;
            // 
            // テキスト236
            // 
            this.テキスト236.DataField = "ITEM20";
            this.テキスト236.Height = 0.1875F;
            this.テキスト236.Left = 4.101575F;
            this.テキスト236.Name = "テキスト236";
            this.テキスト236.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト236.Tag = "";
            this.テキスト236.Text = "ITEM20";
            this.テキスト236.Top = 2.294095F;
            this.テキスト236.Width = 0.7874016F;
            // 
            // テキスト237
            // 
            this.テキスト237.DataField = "ITEM21";
            this.テキスト237.Height = 0.1875F;
            this.テキスト237.Left = 4.899213F;
            this.テキスト237.Name = "テキスト237";
            this.テキスト237.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト237.Tag = "";
            this.テキスト237.Text = "ITEM21";
            this.テキスト237.Top = 2.294095F;
            this.テキスト237.Width = 0.865748F;
            // 
            // テキスト238
            // 
            this.テキスト238.DataField = "ITEM22";
            this.テキスト238.Height = 0.1875F;
            this.テキスト238.Left = 5.748425F;
            this.テキスト238.Name = "テキスト238";
            this.テキスト238.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト238.Tag = "";
            this.テキスト238.Text = "ITEM22";
            this.テキスト238.Top = 2.294095F;
            this.テキスト238.Width = 0.9448819F;
            // 
            // テキスト331
            // 
            this.テキスト331.DataField = "ITEM23";
            this.テキスト331.Height = 0.1805556F;
            this.テキスト331.Left = 0.3511811F;
            this.テキスト331.Name = "テキスト331";
            this.テキスト331.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト331.Tag = "";
            this.テキスト331.Text = "ITEM23";
            this.テキスト331.Top = 2.555118F;
            this.テキスト331.Width = 0.5744094F;
            // 
            // テキスト333
            // 
            this.テキスト333.DataField = "ITEM25";
            this.テキスト333.Height = 0.1805556F;
            this.テキスト333.Left = 3.42126F;
            this.テキスト333.Name = "テキスト333";
            this.テキスト333.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト333.Tag = "";
            this.テキスト333.Text = "ITEM25";
            this.テキスト333.Top = 2.555118F;
            this.テキスト333.Width = 0.9479167F;
            // 
            // テキスト337
            // 
            this.テキスト337.DataField = "ITEM29";
            this.テキスト337.Height = 0.1874016F;
            this.テキスト337.Left = 4.101575F;
            this.テキスト337.Name = "テキスト337";
            this.テキスト337.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト337.Tag = "";
            this.テキスト337.Text = "ITEM29";
            this.テキスト337.Top = 2.555118F;
            this.テキスト337.Width = 0.7874016F;
            // 
            // テキスト338
            // 
            this.テキスト338.DataField = "ITEM30";
            this.テキスト338.Height = 0.1805556F;
            this.テキスト338.Left = 4.899213F;
            this.テキスト338.Name = "テキスト338";
            this.テキスト338.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト338.Tag = "";
            this.テキスト338.Text = "ITEM30";
            this.テキスト338.Top = 2.555118F;
            this.テキスト338.Width = 0.865748F;
            // 
            // テキスト339
            // 
            this.テキスト339.DataField = "ITEM31";
            this.テキスト339.Height = 0.1805556F;
            this.テキスト339.Left = 5.748425F;
            this.テキスト339.Name = "テキスト339";
            this.テキスト339.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト339.Tag = "";
            this.テキスト339.Text = "ITEM31";
            this.テキスト339.Top = 2.555118F;
            this.テキスト339.Width = 0.9448819F;
            // 
            // テキスト341
            // 
            this.テキスト341.DataField = "ITEM32";
            this.テキスト341.Height = 0.1875F;
            this.テキスト341.Left = 0.3511811F;
            this.テキスト341.Name = "テキスト341";
            this.テキスト341.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト341.Tag = "";
            this.テキスト341.Text = "ITEM32";
            this.テキスト341.Top = 2.857874F;
            this.テキスト341.Width = 0.5744094F;
            // 
            // テキスト343
            // 
            this.テキスト343.DataField = "ITEM34";
            this.テキスト343.Height = 0.1875F;
            this.テキスト343.Left = 3.42126F;
            this.テキスト343.Name = "テキスト343";
            this.テキスト343.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト343.Tag = "";
            this.テキスト343.Text = "ITEM34";
            this.テキスト343.Top = 2.857874F;
            this.テキスト343.Width = 0.9479167F;
            // 
            // テキスト347
            // 
            this.テキスト347.DataField = "ITEM38";
            this.テキスト347.Height = 0.1875F;
            this.テキスト347.Left = 4.101575F;
            this.テキスト347.Name = "テキスト347";
            this.テキスト347.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト347.Tag = "";
            this.テキスト347.Text = "ITEM38";
            this.テキスト347.Top = 2.857874F;
            this.テキスト347.Width = 0.7874016F;
            // 
            // テキスト348
            // 
            this.テキスト348.DataField = "ITEM39";
            this.テキスト348.Height = 0.1875F;
            this.テキスト348.Left = 4.899213F;
            this.テキスト348.Name = "テキスト348";
            this.テキスト348.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト348.Tag = "";
            this.テキスト348.Text = "ITEM39";
            this.テキスト348.Top = 2.857874F;
            this.テキスト348.Width = 0.865748F;
            // 
            // テキスト349
            // 
            this.テキスト349.DataField = "ITEM40";
            this.テキスト349.Height = 0.1875F;
            this.テキスト349.Left = 5.748425F;
            this.テキスト349.Name = "テキスト349";
            this.テキスト349.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト349.Tag = "";
            this.テキスト349.Text = "ITEM40";
            this.テキスト349.Top = 2.857874F;
            this.テキスト349.Width = 0.9448819F;
            // 
            // テキスト350
            // 
            this.テキスト350.DataField = "ITEM41";
            this.テキスト350.Height = 0.1875F;
            this.テキスト350.Left = 0.3511811F;
            this.テキスト350.Name = "テキスト350";
            this.テキスト350.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト350.Tag = "";
            this.テキスト350.Text = "ITEM41";
            this.テキスト350.Top = 3.13937F;
            this.テキスト350.Width = 0.5744094F;
            // 
            // テキスト352
            // 
            this.テキスト352.DataField = "ITEM43";
            this.テキスト352.Height = 0.1875F;
            this.テキスト352.Left = 3.42126F;
            this.テキスト352.Name = "テキスト352";
            this.テキスト352.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト352.Tag = "";
            this.テキスト352.Text = "ITEM43";
            this.テキスト352.Top = 3.13937F;
            this.テキスト352.Width = 0.9479167F;
            // 
            // テキスト356
            // 
            this.テキスト356.DataField = "ITEM47";
            this.テキスト356.Height = 0.1875F;
            this.テキスト356.Left = 4.101575F;
            this.テキスト356.Name = "テキスト356";
            this.テキスト356.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト356.Tag = "";
            this.テキスト356.Text = "ITEM47";
            this.テキスト356.Top = 3.13937F;
            this.テキスト356.Width = 0.7874016F;
            // 
            // テキスト357
            // 
            this.テキスト357.DataField = "ITEM48";
            this.テキスト357.Height = 0.1875F;
            this.テキスト357.Left = 4.899213F;
            this.テキスト357.Name = "テキスト357";
            this.テキスト357.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト357.Tag = "";
            this.テキスト357.Text = "ITEM48";
            this.テキスト357.Top = 3.13937F;
            this.テキスト357.Width = 0.865748F;
            // 
            // テキスト358
            // 
            this.テキスト358.DataField = "ITEM49";
            this.テキスト358.Height = 0.1875F;
            this.テキスト358.Left = 5.748425F;
            this.テキスト358.Name = "テキスト358";
            this.テキスト358.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト358.Tag = "";
            this.テキスト358.Text = "ITEM49";
            this.テキスト358.Top = 3.13937F;
            this.テキスト358.Width = 0.9448819F;
            // 
            // テキスト360
            // 
            this.テキスト360.DataField = "ITEM50";
            this.テキスト360.Height = 0.1875F;
            this.テキスト360.Left = 0.3511811F;
            this.テキスト360.Name = "テキスト360";
            this.テキスト360.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト360.Tag = "";
            this.テキスト360.Text = "ITEM50";
            this.テキスト360.Top = 3.438583F;
            this.テキスト360.Width = 0.5744094F;
            // 
            // テキスト362
            // 
            this.テキスト362.DataField = "ITEM52";
            this.テキスト362.Height = 0.1875F;
            this.テキスト362.Left = 3.42126F;
            this.テキスト362.Name = "テキスト362";
            this.テキスト362.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト362.Tag = "";
            this.テキスト362.Text = "ITEM52";
            this.テキスト362.Top = 3.438583F;
            this.テキスト362.Width = 0.9479167F;
            // 
            // テキスト366
            // 
            this.テキスト366.DataField = "ITEM56";
            this.テキスト366.Height = 0.1875F;
            this.テキスト366.Left = 4.101575F;
            this.テキスト366.Name = "テキスト366";
            this.テキスト366.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト366.Tag = "";
            this.テキスト366.Text = "ITEM56";
            this.テキスト366.Top = 3.438583F;
            this.テキスト366.Width = 0.7874016F;
            // 
            // テキスト367
            // 
            this.テキスト367.DataField = "ITEM57";
            this.テキスト367.Height = 0.1875F;
            this.テキスト367.Left = 4.899213F;
            this.テキスト367.Name = "テキスト367";
            this.テキスト367.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト367.Tag = "";
            this.テキスト367.Text = "ITEM57";
            this.テキスト367.Top = 3.438583F;
            this.テキスト367.Width = 0.865748F;
            // 
            // テキスト368
            // 
            this.テキスト368.DataField = "ITEM58";
            this.テキスト368.Height = 0.1875F;
            this.テキスト368.Left = 5.748425F;
            this.テキスト368.Name = "テキスト368";
            this.テキスト368.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト368.Tag = "";
            this.テキスト368.Text = "ITEM58";
            this.テキスト368.Top = 3.438583F;
            this.テキスト368.Width = 0.9448819F;
            // 
            // テキスト369
            // 
            this.テキスト369.DataField = "ITEM59";
            this.テキスト369.Height = 0.1875F;
            this.テキスト369.Left = 0.3511811F;
            this.テキスト369.Name = "テキスト369";
            this.テキスト369.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト369.Tag = "";
            this.テキスト369.Text = "ITEM59";
            this.テキスト369.Top = 3.750394F;
            this.テキスト369.Width = 0.5744094F;
            // 
            // テキスト371
            // 
            this.テキスト371.DataField = "ITEM61";
            this.テキスト371.Height = 0.1875F;
            this.テキスト371.Left = 3.42126F;
            this.テキスト371.Name = "テキスト371";
            this.テキスト371.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト371.Tag = "";
            this.テキスト371.Text = "ITEM61";
            this.テキスト371.Top = 3.750394F;
            this.テキスト371.Width = 0.9479167F;
            // 
            // テキスト375
            // 
            this.テキスト375.DataField = "ITEM65";
            this.テキスト375.Height = 0.1875F;
            this.テキスト375.Left = 4.101575F;
            this.テキスト375.Name = "テキスト375";
            this.テキスト375.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト375.Tag = "";
            this.テキスト375.Text = "ITEM65";
            this.テキスト375.Top = 3.750394F;
            this.テキスト375.Width = 0.7874016F;
            // 
            // テキスト376
            // 
            this.テキスト376.DataField = "ITEM66";
            this.テキスト376.Height = 0.1875F;
            this.テキスト376.Left = 4.899213F;
            this.テキスト376.Name = "テキスト376";
            this.テキスト376.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト376.Tag = "";
            this.テキスト376.Text = "ITEM66";
            this.テキスト376.Top = 3.750394F;
            this.テキスト376.Width = 0.865748F;
            // 
            // テキスト377
            // 
            this.テキスト377.DataField = "ITEM67";
            this.テキスト377.Height = 0.1875F;
            this.テキスト377.Left = 5.748425F;
            this.テキスト377.Name = "テキスト377";
            this.テキスト377.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト377.Tag = "";
            this.テキスト377.Text = "ITEM67";
            this.テキスト377.Top = 3.750394F;
            this.テキスト377.Width = 0.9448819F;
            // 
            // テキスト379
            // 
            this.テキスト379.DataField = "ITEM68";
            this.テキスト379.Height = 0.1875F;
            this.テキスト379.Left = 0.3511811F;
            this.テキスト379.Name = "テキスト379";
            this.テキスト379.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 0";
            this.テキスト379.Tag = "";
            this.テキスト379.Text = "ITEM68";
            this.テキスト379.Top = 4.051575F;
            this.テキスト379.Width = 0.5744094F;
            // 
            // テキスト381
            // 
            this.テキスト381.DataField = "ITEM70";
            this.テキスト381.Height = 0.1875F;
            this.テキスト381.Left = 3.42126F;
            this.テキスト381.Name = "テキスト381";
            this.テキスト381.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト381.Tag = "";
            this.テキスト381.Text = "ITEM70";
            this.テキスト381.Top = 4.051575F;
            this.テキスト381.Width = 0.9479167F;
            // 
            // テキスト385
            // 
            this.テキスト385.DataField = "ITEM74";
            this.テキスト385.Height = 0.1875F;
            this.テキスト385.Left = 4.101575F;
            this.テキスト385.Name = "テキスト385";
            this.テキスト385.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト385.Tag = "";
            this.テキスト385.Text = "ITEM74";
            this.テキスト385.Top = 4.051575F;
            this.テキスト385.Width = 0.7874016F;
            // 
            // テキスト386
            // 
            this.テキスト386.DataField = "ITEM75";
            this.テキスト386.Height = 0.1875F;
            this.テキスト386.Left = 4.899213F;
            this.テキスト386.Name = "テキスト386";
            this.テキスト386.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト386.Tag = "";
            this.テキスト386.Text = "ITEM75";
            this.テキスト386.Top = 4.051575F;
            this.テキスト386.Width = 0.865748F;
            // 
            // テキスト387
            // 
            this.テキスト387.DataField = "ITEM76";
            this.テキスト387.Height = 0.1875F;
            this.テキスト387.Left = 5.748425F;
            this.テキスト387.Name = "テキスト387";
            this.テキスト387.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト387.Tag = "";
            this.テキスト387.Text = "ITEM76";
            this.テキスト387.Top = 4.051575F;
            this.テキスト387.Width = 0.9448819F;
            // 
            // テキスト388
            // 
            this.テキスト388.DataField = "ITEM77";
            this.テキスト388.Height = 0.1875F;
            this.テキスト388.Left = 0.3511811F;
            this.テキスト388.Name = "テキスト388";
            this.テキスト388.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト388.Tag = "";
            this.テキスト388.Text = "ITEM77";
            this.テキスト388.Top = 4.343307F;
            this.テキスト388.Width = 0.5744095F;
            // 
            // テキスト390
            // 
            this.テキスト390.DataField = "ITEM79";
            this.テキスト390.Height = 0.1875F;
            this.テキスト390.Left = 3.42126F;
            this.テキスト390.Name = "テキスト390";
            this.テキスト390.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト390.Tag = "";
            this.テキスト390.Text = "ITEM79";
            this.テキスト390.Top = 4.343307F;
            this.テキスト390.Width = 0.9479167F;
            // 
            // テキスト394
            // 
            this.テキスト394.DataField = "ITEM83";
            this.テキスト394.Height = 0.1875F;
            this.テキスト394.Left = 4.101575F;
            this.テキスト394.Name = "テキスト394";
            this.テキスト394.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト394.Tag = "";
            this.テキスト394.Text = "ITEM83";
            this.テキスト394.Top = 4.343307F;
            this.テキスト394.Width = 0.7874016F;
            // 
            // テキスト395
            // 
            this.テキスト395.DataField = "ITEM84";
            this.テキスト395.Height = 0.1875F;
            this.テキスト395.Left = 4.899213F;
            this.テキスト395.Name = "テキスト395";
            this.テキスト395.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト395.Tag = "";
            this.テキスト395.Text = "ITEM84";
            this.テキスト395.Top = 4.343307F;
            this.テキスト395.Width = 0.865748F;
            // 
            // テキスト396
            // 
            this.テキスト396.DataField = "ITEM85";
            this.テキスト396.Height = 0.1875F;
            this.テキスト396.Left = 5.748425F;
            this.テキスト396.Name = "テキスト396";
            this.テキスト396.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト396.Tag = "";
            this.テキスト396.Text = "ITEM85";
            this.テキスト396.Top = 4.343307F;
            this.テキスト396.Width = 0.9448819F;
            // 
            // ボックス309
            // 
            this.ボックス309.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Height = 0.34375F;
            this.ボックス309.Left = 3.785433F;
            this.ボックス309.Name = "ボックス309";
            this.ボックス309.RoundingRadius = 9.999999F;
            this.ボックス309.Tag = "";
            this.ボックス309.Top = 4.881702F;
            this.ボックス309.Visible = false;
            this.ボックス309.Width = 3.915956F;
            // 
            // ボックス310
            // 
            this.ボックス310.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス310.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Height = 0.2395833F;
            this.ボックス310.Left = 3.785433F;
            this.ボックス310.Name = "ボックス310";
            this.ボックス310.RoundingRadius = 9.999999F;
            this.ボックス310.Tag = "";
            this.ボックス310.Top = 4.642119F;
            this.ボックス310.Visible = false;
            this.ボックス310.Width = 3.915261F;
            // 
            // テキスト311
            // 
            this.テキスト311.DataField = "ITEM86";
            this.テキスト311.Height = 0.1875F;
            this.テキスト311.Left = 3.840551F;
            this.テキスト311.Name = "テキスト311";
            this.テキスト311.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト311.Tag = "";
            this.テキスト311.Text = "ITEM86";
            this.テキスト311.Top = 4.683942F;
            this.テキスト311.Visible = false;
            this.テキスト311.Width = 1.000394F;
            // 
            // テキスト312
            // 
            this.テキスト312.DataField = "ITEM87";
            this.テキスト312.Height = 0.1875F;
            this.テキスト312.Left = 4.929528F;
            this.テキスト312.Name = "テキスト312";
            this.テキスト312.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト312.Tag = "";
            this.テキスト312.Text = "ITEM87";
            this.テキスト312.Top = 4.683942F;
            this.テキスト312.Visible = false;
            this.テキスト312.Width = 0.8763778F;
            // 
            // テキスト313
            // 
            this.テキスト313.DataField = "ITEM88";
            this.テキスト313.Height = 0.1875F;
            this.テキスト313.Left = 5.95486F;
            this.テキスト313.Name = "テキスト313";
            this.テキスト313.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト313.Tag = "";
            this.テキスト313.Text = "ITEM88";
            this.テキスト313.Top = 4.683785F;
            this.テキスト313.Visible = false;
            this.テキスト313.Width = 0.7916667F;
            // 
            // テキスト314
            // 
            this.テキスト314.DataField = "ITEM89";
            this.テキスト314.Height = 0.1875F;
            this.テキスト314.Left = 6.864583F;
            this.テキスト314.Name = "テキスト314";
            this.テキスト314.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト314.Tag = "";
            this.テキスト314.Text = "ITEM89";
            this.テキスト314.Top = 4.683785F;
            this.テキスト314.Visible = false;
            this.テキスト314.Width = 0.7916667F;
            // 
            // テキスト315
            // 
            this.テキスト315.DataField = "ITEM90";
            this.テキスト315.Height = 0.1875F;
            this.テキスト315.Left = 3.486221F;
            this.テキスト315.Name = "テキスト315";
            this.テキスト315.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト315.Tag = "";
            this.テキスト315.Text = "ITEM90";
            this.テキスト315.Top = 4.990551F;
            this.テキスト315.Visible = false;
            this.テキスト315.Width = 1.000394F;
            // 
            // テキスト316
            // 
            this.テキスト316.DataField = "ITEM91";
            this.テキスト316.Height = 0.1875F;
            this.テキスト316.Left = 4.800787F;
            this.テキスト316.Name = "テキスト316";
            this.テキスト316.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト316.Tag = "";
            this.テキスト316.Text = "ITEM91";
            this.テキスト316.Top = 4.953543F;
            this.テキスト316.Width = 0.7917323F;
            // 
            // テキスト317
            // 
            this.テキスト317.DataField = "ITEM92";
            this.テキスト317.Height = 0.1875F;
            this.テキスト317.Left = 5.702756F;
            this.テキスト317.Name = "テキスト317";
            this.テキスト317.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト317.Tag = "";
            this.テキスト317.Text = "ITEM92";
            this.テキスト317.Top = 4.953543F;
            this.テキスト317.Width = 0.7916667F;
            // 
            // テキスト318
            // 
            this.テキスト318.DataField = "ITEM93";
            this.テキスト318.Height = 0.1875F;
            this.テキスト318.Left = 6.670866F;
            this.テキスト318.Name = "テキスト318";
            this.テキスト318.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト318.Tag = "";
            this.テキスト318.Text = "ITEM93";
            this.テキスト318.Top = 4.953543F;
            this.テキスト318.Width = 0.7916667F;
            // 
            // 直線407
            // 
            this.直線407.Height = 0F;
            this.直線407.Left = 0.1388885F;
            this.直線407.LineWeight = 0F;
            this.直線407.Name = "直線407";
            this.直線407.Tag = "";
            this.直線407.Top = 1.881702F;
            this.直線407.Visible = false;
            this.直線407.Width = 7.559722F;
            this.直線407.X1 = 0.1388885F;
            this.直線407.X2 = 7.698611F;
            this.直線407.Y1 = 1.881702F;
            this.直線407.Y2 = 1.881702F;
            // 
            // ラベル414
            // 
            this.ラベル414.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル414.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル414.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル414.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル414.Height = 0.3951389F;
            this.ラベル414.HyperLink = null;
            this.ラベル414.Left = 3.098611F;
            this.ラベル414.Name = "ラベル414";
            this.ラベル414.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-weight: bold; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ラベル414.Tag = "";
            this.ラベル414.Text = "　";
            this.ラベル414.Top = 5.902536F;
            this.ラベル414.Visible = false;
            this.ラベル414.Width = 1.811111F;
            // 
            // テキスト415
            // 
            this.テキスト415.DataField = "ITEM13";
            this.テキスト415.Height = 0.1875F;
            this.テキスト415.Left = 7.165748F;
            this.テキスト415.Name = "テキスト415";
            this.テキスト415.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト415.Tag = "";
            this.テキスト415.Text = "ITEM13";
            this.テキスト415.Top = 6.446457F;
            this.テキスト415.Width = 0.3229167F;
            // 
            // テキスト416
            // 
            this.テキスト416.DataField = "ITEM02";
            this.テキスト416.Height = 0.1979167F;
            this.テキスト416.Left = 6.416536F;
            this.テキスト416.Name = "テキスト416";
            this.テキスト416.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト416.Tag = "";
            this.テキスト416.Text = "ITEM02";
            this.テキスト416.Top = 6.446457F;
            this.テキスト416.Width = 0.59375F;
            // 
            // テキスト417
            // 
            this.テキスト417.DataField = "ITEM01";
            this.テキスト417.Height = 0.1875F;
            this.テキスト417.Left = 6.270079F;
            this.テキスト417.Name = "テキスト417";
            this.テキスト417.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.テキスト417.Tag = "";
            this.テキスト417.Text = "ITEM01";
            this.テキスト417.Top = 6.249213F;
            this.テキスト417.Width = 1.34375F;
            // 
            // ラベル418
            // 
            this.ラベル418.Height = 0.1972222F;
            this.ラベル418.HyperLink = null;
            this.ラベル418.Left = 7.475985F;
            this.ラベル418.Name = "ラベル418";
            this.ラベル418.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル418.Tag = "";
            this.ラベル418.Text = "頁";
            this.ラベル418.Top = 6.446457F;
            this.ラベル418.Visible = false;
            this.ラベル418.Width = 0.1715278F;
            // 
            // ラベル420
            // 
            this.ラベル420.Height = 0.1979167F;
            this.ラベル420.HyperLink = null;
            this.ラベル420.Left = 5.722221F;
            this.ラベル420.Name = "ラベル420";
            this.ラベル420.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル420.Tag = "";
            this.ラベル420.Text = "伝票番号：";
            this.ラベル420.Top = 6.094897F;
            this.ラベル420.Visible = false;
            this.ラベル420.Width = 0.8333333F;
            // 
            // テキスト422
            // 
            this.テキスト422.DataField = "ITEM04";
            this.テキスト422.Height = 0.1875F;
            this.テキスト422.Left = 0.3326772F;
            this.テキスト422.Name = "テキスト422";
            this.テキスト422.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.テキスト422.Tag = "";
            this.テキスト422.Text = "ITEM04";
            this.テキスト422.Top = 6.858268F;
            this.テキスト422.Width = 2.375F;
            // 
            // 直線423
            // 
            this.直線423.Height = 0F;
            this.直線423.Left = 0.09722185F;
            this.直線423.LineWeight = 0F;
            this.直線423.Name = "直線423";
            this.直線423.Tag = "";
            this.直線423.Top = 6.798369F;
            this.直線423.Visible = false;
            this.直線423.Width = 2.710417F;
            this.直線423.X1 = 0.09722185F;
            this.直線423.X2 = 2.807639F;
            this.直線423.Y1 = 6.798369F;
            this.直線423.Y2 = 6.798369F;
            // 
            // ラベル424
            // 
            this.ラベル424.Height = 0.1972222F;
            this.ラベル424.HyperLink = null;
            this.ラベル424.Left = 2.534722F;
            this.ラベル424.Name = "ラベル424";
            this.ラベル424.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル424.Tag = "";
            this.ラベル424.Text = "様";
            this.ラベル424.Top = 6.600452F;
            this.ラベル424.Visible = false;
            this.ラベル424.Width = 0.2444444F;
            // 
            // ラベル425
            // 
            this.ラベル425.Height = 0.15625F;
            this.ラベル425.HyperLink = null;
            this.ラベル425.Left = 0.1319441F;
            this.ラベル425.Name = "ラベル425";
            this.ラベル425.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル425.Tag = "";
            this.ラベル425.Text = "TEL";
            this.ラベル425.Top = 6.835175F;
            this.ラベル425.Visible = false;
            this.ラベル425.Width = 0.2395833F;
            // 
            // テキスト426
            // 
            this.テキスト426.DataField = "ITEM05";
            this.テキスト426.Height = 0.15625F;
            this.テキスト426.Left = 0.6472441F;
            this.テキスト426.Name = "テキスト426";
            this.テキスト426.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 128";
            this.テキスト426.Tag = "";
            this.テキスト426.Text = "ITEM05";
            this.テキスト426.Top = 7.13189F;
            this.テキスト426.Width = 0.9479167F;
            // 
            // ラベル427
            // 
            this.ラベル427.Height = 0.15625F;
            this.ラベル427.HyperLink = null;
            this.ラベル427.Left = 1.315972F;
            this.ラベル427.Name = "ラベル427";
            this.ラベル427.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル427.Tag = "";
            this.ラベル427.Text = "FAX";
            this.ラベル427.Top = 6.835175F;
            this.ラベル427.Visible = false;
            this.ラベル427.Width = 0.2395833F;
            // 
            // テキスト428
            // 
            this.テキスト428.DataField = "ITEM06";
            this.テキスト428.Height = 0.15625F;
            this.テキスト428.Left = 2.032284F;
            this.テキスト428.Name = "テキスト428";
            this.テキスト428.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 128";
            this.テキスト428.Tag = "";
            this.テキスト428.Text = "ITEM06";
            this.テキスト428.Top = 7.13189F;
            this.テキスト428.Width = 0.9791667F;
            // 
            // テキスト429
            // 
            this.テキスト429.DataField = "ITEM07";
            this.テキスト429.Height = 0.1875F;
            this.テキスト429.Left = 3.42126F;
            this.テキスト429.Name = "テキスト429";
            this.テキスト429.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト429.Tag = "";
            this.テキスト429.Text = "ITEM07";
            this.テキスト429.Top = 6.312505F;
            this.テキスト429.Visible = false;
            this.テキスト429.Width = 2.21875F;
            // 
            // テキスト430
            // 
            this.テキスト430.DataField = "ITEM08";
            this.テキスト430.Height = 0.1875F;
            this.テキスト430.Left = 3.42126F;
            this.テキスト430.Name = "テキスト430";
            this.テキスト430.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト430.Tag = "";
            this.テキスト430.Text = "ITEM08";
            this.テキスト430.Top = 6.511811F;
            this.テキスト430.Visible = false;
            this.テキスト430.Width = 2.21875F;
            // 
            // テキスト431
            // 
            this.テキスト431.DataField = "ITEM09";
            this.テキスト431.Height = 0.15625F;
            this.テキスト431.Left = 3.42126F;
            this.テキスト431.Name = "テキスト431";
            this.テキスト431.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト431.Tag = "";
            this.テキスト431.Text = "ITEM09";
            this.テキスト431.Top = 6.699311F;
            this.テキスト431.Visible = false;
            this.テキスト431.Width = 2.21875F;
            // 
            // テキスト432
            // 
            this.テキスト432.DataField = "ITEM10";
            this.テキスト432.Height = 0.15625F;
            this.テキスト432.Left = 3.42126F;
            this.テキスト432.Name = "テキスト432";
            this.テキスト432.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト432.Tag = "";
            this.テキスト432.Text = "ITEM10";
            this.テキスト432.Top = 6.855561F;
            this.テキスト432.Visible = false;
            this.テキスト432.Width = 2.21875F;
            // 
            // テキスト433
            // 
            this.テキスト433.DataField = "ITEM11";
            this.テキスト433.Height = 0.1875F;
            this.テキスト433.Left = 3.041339F;
            this.テキスト433.Name = "テキスト433";
            this.テキスト433.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 128";
            this.テキスト433.Tag = "";
            this.テキスト433.Text = "ITEM11";
            this.テキスト433.Top = 7.182283F;
            this.テキスト433.Width = 1.811111F;
            // 
            // ラベル434
            // 
            this.ラベル434.Height = 0.15625F;
            this.ラベル434.HyperLink = null;
            this.ラベル434.Left = 6.003471F;
            this.ラベル434.Name = "ラベル434";
            this.ラベル434.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル434.Tag = "";
            this.ラベル434.Text = "担当：";
            this.ラベル434.Top = 7.069201F;
            this.ラベル434.Visible = false;
            this.ラベル434.Width = 0.4791667F;
            // 
            // テキスト435
            // 
            this.テキスト435.DataField = "ITEM12";
            this.テキスト435.Height = 0.15625F;
            this.テキスト435.Left = 6.552756F;
            this.テキスト435.Name = "テキスト435";
            this.テキスト435.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト435.Tag = "";
            this.テキスト435.Text = "ITEM12";
            this.テキスト435.Top = 7.186221F;
            this.テキスト435.Width = 1.038189F;
            // 
            // ラベル535
            // 
            this.ラベル535.Height = 0.2291667F;
            this.ラベル535.HyperLink = null;
            this.ラベル535.Left = 3.194488F;
            this.ラベル535.Name = "ラベル535";
            this.ラベル535.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt" +
    "; font-weight: normal; text-align: center; ddo-char-set: 128";
            this.ラベル535.Tag = "";
            this.ラベル535.Text = "請 求 書";
            this.ラベル535.Top = 5.981008F;
            this.ラベル535.Visible = false;
            this.ラベル535.Width = 1.614567F;
            // 
            // テキスト642
            // 
            this.テキスト642.DataField = "ITEM94";
            this.テキスト642.Height = 0.1875F;
            this.テキスト642.Left = 0.3326772F;
            this.テキスト642.Name = "テキスト642";
            this.テキスト642.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.テキスト642.Tag = "";
            this.テキスト642.Text = "ITEM94";
            this.テキスト642.Top = 0.7866142F;
            this.テキスト642.Width = 2.375F;
            // 
            // テキスト643
            // 
            this.テキスト643.DataField = "ITEM95";
            this.テキスト643.Height = 0.1875F;
            this.テキスト643.Left = 0.3326772F;
            this.テキスト643.Name = "テキスト643";
            this.テキスト643.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.テキスト643.Tag = "";
            this.テキスト643.Text = "ITEM95";
            this.テキスト643.Top = 0.9838583F;
            this.テキスト643.Width = 2.375F;
            // 
            // textBox73
            // 
            this.textBox73.DataField = "ITEM03";
            this.textBox73.Height = 0.1875F;
            this.textBox73.Left = 0.3326772F;
            this.textBox73.Name = "textBox73";
            this.textBox73.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox73.Tag = "";
            this.textBox73.Text = "ITEM03";
            this.textBox73.Top = 6.111811F;
            this.textBox73.Width = 1.34375F;
            // 
            // textBox74
            // 
            this.textBox74.DataField = "ITEM94";
            this.textBox74.Height = 0.1875F;
            this.textBox74.Left = 0.3326772F;
            this.textBox74.Name = "textBox74";
            this.textBox74.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.textBox74.Tag = "";
            this.textBox74.Text = "ITEM94";
            this.textBox74.Top = 6.301969F;
            this.textBox74.Width = 2.375F;
            // 
            // textBox75
            // 
            this.textBox75.DataField = "ITEM95";
            this.textBox75.Height = 0.1875F;
            this.textBox75.Left = 0.3326772F;
            this.textBox75.Name = "textBox75";
            this.textBox75.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.textBox75.Tag = "";
            this.textBox75.Text = "ITEM95";
            this.textBox75.Top = 6.5F;
            this.textBox75.Width = 2.375F;
            // 
            // shape10
            // 
            this.shape10.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Height = 0.34375F;
            this.shape10.Left = 3.785039F;
            this.shape10.Name = "shape10";
            this.shape10.RoundingRadius = 9.999999F;
            this.shape10.Tag = "";
            this.shape10.Top = 10.47529F;
            this.shape10.Visible = false;
            this.shape10.Width = 3.915956F;
            // 
            // shape11
            // 
            this.shape11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape11.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Height = 0.2395833F;
            this.shape11.Left = 3.785039F;
            this.shape11.Name = "shape11";
            this.shape11.RoundingRadius = 9.999999F;
            this.shape11.Tag = "";
            this.shape11.Top = 10.2357F;
            this.shape11.Visible = false;
            this.shape11.Width = 3.915261F;
            // 
            // textBox76
            // 
            this.textBox76.DataField = "ITEM86";
            this.textBox76.Height = 0.1875F;
            this.textBox76.Left = 3.840158F;
            this.textBox76.Name = "textBox76";
            this.textBox76.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.textBox76.Tag = "";
            this.textBox76.Text = "ITEM86";
            this.textBox76.Top = 10.27756F;
            this.textBox76.Visible = false;
            this.textBox76.Width = 1.000393F;
            // 
            // textBox77
            // 
            this.textBox77.DataField = "ITEM87";
            this.textBox77.Height = 0.1875F;
            this.textBox77.Left = 4.929528F;
            this.textBox77.Name = "textBox77";
            this.textBox77.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.textBox77.Tag = "";
            this.textBox77.Text = "ITEM87";
            this.textBox77.Top = 10.27725F;
            this.textBox77.Visible = false;
            this.textBox77.Width = 0.8763778F;
            // 
            // textBox78
            // 
            this.textBox78.DataField = "ITEM88";
            this.textBox78.Height = 0.1875F;
            this.textBox78.Left = 5.954471F;
            this.textBox78.Name = "textBox78";
            this.textBox78.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.textBox78.Tag = "";
            this.textBox78.Text = "ITEM88";
            this.textBox78.Top = 10.27737F;
            this.textBox78.Visible = false;
            this.textBox78.Width = 0.7916667F;
            // 
            // textBox79
            // 
            this.textBox79.DataField = "ITEM89";
            this.textBox79.Height = 0.1875F;
            this.textBox79.Left = 6.864193F;
            this.textBox79.Name = "textBox79";
            this.textBox79.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.textBox79.Tag = "";
            this.textBox79.Text = "ITEM89";
            this.textBox79.Top = 10.27737F;
            this.textBox79.Visible = false;
            this.textBox79.Width = 0.7916667F;
            // 
            // textBox80
            // 
            this.textBox80.DataField = "ITEM90";
            this.textBox80.Height = 0.1875F;
            this.textBox80.Left = 3.486221F;
            this.textBox80.Name = "textBox80";
            this.textBox80.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox80.Tag = "";
            this.textBox80.Text = "ITEM90";
            this.textBox80.Top = 10.54803F;
            this.textBox80.Visible = false;
            this.textBox80.Width = 1.000393F;
            // 
            // textBox81
            // 
            this.textBox81.DataField = "ITEM91";
            this.textBox81.Height = 0.1875F;
            this.textBox81.Left = 4.800787F;
            this.textBox81.Name = "textBox81";
            this.textBox81.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox81.Tag = "";
            this.textBox81.Text = "ITEM91";
            this.textBox81.Top = 10.45512F;
            this.textBox81.Width = 0.7917323F;
            // 
            // textBox82
            // 
            this.textBox82.DataField = "ITEM92";
            this.textBox82.Height = 0.1875F;
            this.textBox82.Left = 5.702756F;
            this.textBox82.Name = "textBox82";
            this.textBox82.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox82.Tag = "";
            this.textBox82.Text = "ITEM92";
            this.textBox82.Top = 10.45512F;
            this.textBox82.Width = 0.7916667F;
            // 
            // textBox83
            // 
            this.textBox83.DataField = "ITEM93";
            this.textBox83.Height = 0.1875F;
            this.textBox83.Left = 6.670866F;
            this.textBox83.Name = "textBox83";
            this.textBox83.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox83.Tag = "";
            this.textBox83.Text = "ITEM93";
            this.textBox83.Top = 10.45512F;
            this.textBox83.Width = 0.7916667F;
            // 
            // line3
            // 
            this.line3.Height = 2.847605F;
            this.line3.Left = 0.8854164F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 1.673312F;
            this.line3.Visible = false;
            this.line3.Width = 0.003559828F;
            this.line3.X1 = 0.8889762F;
            this.line3.X2 = 0.8854164F;
            this.line3.Y1 = 1.673312F;
            this.line3.Y2 = 4.520917F;
            // 
            // line4
            // 
            this.line4.Height = 2.83973F;
            this.line4.Left = 3.350771F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 1.677249F;
            this.line4.Visible = false;
            this.line4.Width = 0.003560066F;
            this.line4.X1 = 3.354331F;
            this.line4.X2 = 3.350771F;
            this.line4.Y1 = 1.677249F;
            this.line4.Y2 = 4.516979F;
            // 
            // line5
            // 
            this.line5.Height = 2.847605F;
            this.line5.Left = 4.381086F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 1.673312F;
            this.line5.Visible = false;
            this.line5.Width = 0.003560066F;
            this.line5.X1 = 4.384646F;
            this.line5.X2 = 4.381086F;
            this.line5.Y1 = 1.673312F;
            this.line5.Y2 = 4.520917F;
            // 
            // line6
            // 
            this.line6.Height = 2.847605F;
            this.line6.Left = 4.698016F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 1.673312F;
            this.line6.Visible = false;
            this.line6.Width = 0.003560066F;
            this.line6.X1 = 4.701576F;
            this.line6.X2 = 4.698016F;
            this.line6.Y1 = 1.673312F;
            this.line6.Y2 = 4.520917F;
            // 
            // line7
            // 
            this.line7.Height = 2.847605F;
            this.line7.Left = 5.016126F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 1.673312F;
            this.line7.Visible = false;
            this.line7.Width = 0.003560066F;
            this.line7.X1 = 5.019686F;
            this.line7.X2 = 5.016126F;
            this.line7.Y1 = 1.673312F;
            this.line7.Y2 = 4.520917F;
            // 
            // line8
            // 
            this.line8.Height = 2.847604F;
            this.line8.Left = 5.437385F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 1.673229F;
            this.line8.Visible = false;
            this.line8.Width = 0.003560066F;
            this.line8.X1 = 5.440945F;
            this.line8.X2 = 5.437385F;
            this.line8.Y1 = 1.673229F;
            this.line8.Y2 = 4.520833F;
            // 
            // line9
            // 
            this.line9.Height = 2.847605F;
            this.line9.Left = 6.036992F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 1.673312F;
            this.line9.Visible = false;
            this.line9.Width = 0.003560066F;
            this.line9.X1 = 6.040552F;
            this.line9.X2 = 6.036992F;
            this.line9.Y1 = 1.673312F;
            this.line9.Y2 = 4.520917F;
            // 
            // line10
            // 
            this.line10.Height = 2.847605F;
            this.line10.Left = 6.859433F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 1.673312F;
            this.line10.Visible = false;
            this.line10.Width = 0.003559589F;
            this.line10.X1 = 6.862993F;
            this.line10.X2 = 6.859433F;
            this.line10.Y1 = 1.673312F;
            this.line10.Y2 = 4.520917F;
            // 
            // line11
            // 
            this.line11.Height = 2.847605F;
            this.line11.Left = 7.693292F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 1.673312F;
            this.line11.Visible = false;
            this.line11.Width = 0.003554821F;
            this.line11.X1 = 7.696847F;
            this.line11.X2 = 7.693292F;
            this.line11.Y1 = 1.673312F;
            this.line11.Y2 = 4.520917F;
            // 
            // line12
            // 
            this.line12.Height = 2.847605F;
            this.line12.Left = 0.132267F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 1.673312F;
            this.line12.Visible = false;
            this.line12.Width = 0.003559604F;
            this.line12.X1 = 0.1358266F;
            this.line12.X2 = 0.132267F;
            this.line12.Y1 = 1.673312F;
            this.line12.Y2 = 4.520917F;
            // 
            // line13
            // 
            this.line13.Height = 2.026558E-06F;
            this.line13.Left = 0.1358266F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 1.673228F;
            this.line13.Visible = false;
            this.line13.Width = 7.552249F;
            this.line13.X1 = 0.1358266F;
            this.line13.X2 = 7.688076F;
            this.line13.Y1 = 1.673228F;
            this.line13.Y2 = 1.67323F;
            // 
            // line14
            // 
            this.line14.Height = 0F;
            this.line14.Left = 0.1417323F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 4.535433F;
            this.line14.Visible = false;
            this.line14.Width = 7.552247F;
            this.line14.X1 = 0.1417323F;
            this.line14.X2 = 7.693979F;
            this.line14.Y1 = 4.535433F;
            this.line14.Y2 = 4.535433F;
            // 
            // label1
            // 
            this.label1.Height = 0.3354167F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.1413384F;
            this.label1.Name = "label1";
            this.label1.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label1.Tag = "";
            this.label1.Text = "　";
            this.label1.Top = 9.790244F;
            this.label1.Visible = false;
            this.label1.Width = 7.55F;
            // 
            // label2
            // 
            this.label2.Height = 0.3354167F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.1413384F;
            this.label2.Name = "label2";
            this.label2.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label2.Tag = "";
            this.label2.Text = "　";
            this.label2.Top = 7.772882F;
            this.label2.Visible = false;
            this.label2.Width = 7.55F;
            // 
            // label3
            // 
            this.label3.Height = 0.3354167F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.1413384F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "　";
            this.label3.Top = 8.4583F;
            this.label3.Visible = false;
            this.label3.Width = 7.55F;
            // 
            // label4
            // 
            this.label4.Height = 0.3361111F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.1413384F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label4.Tag = "";
            this.label4.Text = "　";
            this.label4.Top = 9.117327F;
            this.label4.Visible = false;
            this.label4.Width = 7.55F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM78";
            this.textBox1.Height = 0.1875F;
            this.textBox1.Left = 0.9527559F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM78";
            this.textBox1.Top = 9.839764F;
            this.textBox1.Width = 2.385417F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM24";
            this.textBox2.Height = 0.1805556F;
            this.textBox2.Left = 0.9527559F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM24";
            this.textBox2.Top = 8.051575F;
            this.textBox2.Width = 2.385417F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM42";
            this.textBox3.Height = 0.1875F;
            this.textBox3.Left = 0.9527559F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM42";
            this.textBox3.Top = 8.635827F;
            this.textBox3.Width = 2.385417F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM60";
            this.textBox4.Height = 0.1875F;
            this.textBox4.Left = 0.9527559F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM60";
            this.textBox4.Top = 9.24685F;
            this.textBox4.Width = 2.385417F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM15";
            this.textBox5.Height = 0.1875F;
            this.textBox5.Left = 0.9527559F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM15";
            this.textBox5.Top = 7.790551F;
            this.textBox5.Width = 2.367132F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM33";
            this.textBox6.Height = 0.1875F;
            this.textBox6.Left = 0.9527559F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM33";
            this.textBox6.Top = 8.354331F;
            this.textBox6.Width = 2.385417F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM51";
            this.textBox7.Height = 0.1875F;
            this.textBox7.Left = 0.9527559F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM51";
            this.textBox7.Top = 8.93504F;
            this.textBox7.Width = 2.385417F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM69";
            this.textBox8.Height = 0.1875F;
            this.textBox8.Left = 0.9527559F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox8.Tag = "";
            this.textBox8.Text = "ITEM69";
            this.textBox8.Top = 9.548032F;
            this.textBox8.Width = 2.385417F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM14";
            this.textBox9.Height = 0.1875F;
            this.textBox9.Left = 0.3511811F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox9.Tag = "";
            this.textBox9.Text = "ITEM14";
            this.textBox9.Top = 7.790551F;
            this.textBox9.Width = 0.5744094F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM16";
            this.textBox10.Height = 0.1875F;
            this.textBox10.Left = 3.42126F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox10.Tag = "";
            this.textBox10.Text = "ITEM16";
            this.textBox10.Top = 7.790551F;
            this.textBox10.Width = 0.9479167F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM20";
            this.textBox14.Height = 0.1874016F;
            this.textBox14.Left = 4.101575F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox14.Tag = "";
            this.textBox14.Text = "ITEM20";
            this.textBox14.Top = 7.790551F;
            this.textBox14.Width = 0.7874016F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM21";
            this.textBox15.Height = 0.1875F;
            this.textBox15.Left = 4.899213F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox15.Tag = "";
            this.textBox15.Text = "ITEM21";
            this.textBox15.Top = 7.790551F;
            this.textBox15.Width = 0.865748F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM22";
            this.textBox16.Height = 0.1875F;
            this.textBox16.Left = 5.748425F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox16.Tag = "";
            this.textBox16.Text = "ITEM22";
            this.textBox16.Top = 7.790551F;
            this.textBox16.Width = 0.9448819F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM23";
            this.textBox17.Height = 0.1805556F;
            this.textBox17.Left = 0.3511811F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox17.Tag = "";
            this.textBox17.Text = "ITEM23";
            this.textBox17.Top = 8.051575F;
            this.textBox17.Width = 0.5744094F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM25";
            this.textBox18.Height = 0.1805556F;
            this.textBox18.Left = 3.42126F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox18.Tag = "";
            this.textBox18.Text = "ITEM25";
            this.textBox18.Top = 8.051575F;
            this.textBox18.Width = 0.9479167F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM29";
            this.textBox22.Height = 0.1805556F;
            this.textBox22.Left = 4.101575F;
            this.textBox22.Name = "textBox22";
            this.textBox22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox22.Tag = "";
            this.textBox22.Text = "ITEM29";
            this.textBox22.Top = 8.051575F;
            this.textBox22.Width = 0.7874016F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM30";
            this.textBox23.Height = 0.1805556F;
            this.textBox23.Left = 4.899213F;
            this.textBox23.Name = "textBox23";
            this.textBox23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox23.Tag = "";
            this.textBox23.Text = "ITEM30";
            this.textBox23.Top = 8.051575F;
            this.textBox23.Width = 0.865748F;
            // 
            // textBox24
            // 
            this.textBox24.DataField = "ITEM31";
            this.textBox24.Height = 0.1805556F;
            this.textBox24.Left = 5.748425F;
            this.textBox24.Name = "textBox24";
            this.textBox24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox24.Tag = "";
            this.textBox24.Text = "ITEM31";
            this.textBox24.Top = 8.051575F;
            this.textBox24.Width = 0.9448819F;
            // 
            // textBox25
            // 
            this.textBox25.DataField = "ITEM32";
            this.textBox25.Height = 0.1875F;
            this.textBox25.Left = 0.3511811F;
            this.textBox25.Name = "textBox25";
            this.textBox25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox25.Tag = "";
            this.textBox25.Text = "ITEM32";
            this.textBox25.Top = 8.354331F;
            this.textBox25.Width = 0.5744094F;
            // 
            // textBox26
            // 
            this.textBox26.DataField = "ITEM34";
            this.textBox26.Height = 0.1875F;
            this.textBox26.Left = 3.42126F;
            this.textBox26.Name = "textBox26";
            this.textBox26.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox26.Tag = "";
            this.textBox26.Text = "ITEM34";
            this.textBox26.Top = 8.354331F;
            this.textBox26.Width = 0.9479167F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM38";
            this.textBox30.Height = 0.1875F;
            this.textBox30.Left = 4.101575F;
            this.textBox30.Name = "textBox30";
            this.textBox30.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox30.Tag = "";
            this.textBox30.Text = "ITEM38";
            this.textBox30.Top = 8.354331F;
            this.textBox30.Width = 0.7874016F;
            // 
            // textBox31
            // 
            this.textBox31.DataField = "ITEM39";
            this.textBox31.Height = 0.1875F;
            this.textBox31.Left = 4.899213F;
            this.textBox31.Name = "textBox31";
            this.textBox31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox31.Tag = "";
            this.textBox31.Text = "ITEM39";
            this.textBox31.Top = 8.354331F;
            this.textBox31.Width = 0.865748F;
            // 
            // textBox32
            // 
            this.textBox32.DataField = "ITEM40";
            this.textBox32.Height = 0.1875F;
            this.textBox32.Left = 5.748425F;
            this.textBox32.Name = "textBox32";
            this.textBox32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox32.Tag = "";
            this.textBox32.Text = "ITEM40";
            this.textBox32.Top = 8.354331F;
            this.textBox32.Width = 0.9448819F;
            // 
            // textBox33
            // 
            this.textBox33.DataField = "ITEM41";
            this.textBox33.Height = 0.1875F;
            this.textBox33.Left = 0.3511811F;
            this.textBox33.Name = "textBox33";
            this.textBox33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox33.Tag = "";
            this.textBox33.Text = "ITEM41";
            this.textBox33.Top = 8.635827F;
            this.textBox33.Width = 0.5744094F;
            // 
            // textBox34
            // 
            this.textBox34.DataField = "ITEM43";
            this.textBox34.Height = 0.1875F;
            this.textBox34.Left = 3.42126F;
            this.textBox34.Name = "textBox34";
            this.textBox34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox34.Tag = "";
            this.textBox34.Text = "ITEM43";
            this.textBox34.Top = 8.635827F;
            this.textBox34.Width = 0.9479167F;
            // 
            // textBox38
            // 
            this.textBox38.DataField = "ITEM47";
            this.textBox38.Height = 0.1875F;
            this.textBox38.Left = 4.101575F;
            this.textBox38.Name = "textBox38";
            this.textBox38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox38.Tag = "";
            this.textBox38.Text = "ITEM47";
            this.textBox38.Top = 8.635827F;
            this.textBox38.Width = 0.7874016F;
            // 
            // textBox39
            // 
            this.textBox39.DataField = "ITEM48";
            this.textBox39.Height = 0.1875F;
            this.textBox39.Left = 4.899213F;
            this.textBox39.Name = "textBox39";
            this.textBox39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox39.Tag = "";
            this.textBox39.Text = "ITEM48";
            this.textBox39.Top = 8.635827F;
            this.textBox39.Width = 0.865748F;
            // 
            // textBox40
            // 
            this.textBox40.DataField = "ITEM49";
            this.textBox40.Height = 0.1875F;
            this.textBox40.Left = 5.748425F;
            this.textBox40.Name = "textBox40";
            this.textBox40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox40.Tag = "";
            this.textBox40.Text = "ITEM49";
            this.textBox40.Top = 8.635827F;
            this.textBox40.Width = 0.9448819F;
            // 
            // textBox41
            // 
            this.textBox41.DataField = "ITEM50";
            this.textBox41.Height = 0.1875F;
            this.textBox41.Left = 0.3511811F;
            this.textBox41.Name = "textBox41";
            this.textBox41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox41.Tag = "";
            this.textBox41.Text = "ITEM50";
            this.textBox41.Top = 8.93504F;
            this.textBox41.Width = 0.5744094F;
            // 
            // textBox42
            // 
            this.textBox42.DataField = "ITEM52";
            this.textBox42.Height = 0.1875F;
            this.textBox42.Left = 3.42126F;
            this.textBox42.Name = "textBox42";
            this.textBox42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox42.Tag = "";
            this.textBox42.Text = "ITEM52";
            this.textBox42.Top = 8.93504F;
            this.textBox42.Width = 0.9479167F;
            // 
            // textBox46
            // 
            this.textBox46.DataField = "ITEM56";
            this.textBox46.Height = 0.1875F;
            this.textBox46.Left = 4.101575F;
            this.textBox46.Name = "textBox46";
            this.textBox46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox46.Tag = "";
            this.textBox46.Text = "ITEM56";
            this.textBox46.Top = 8.93504F;
            this.textBox46.Width = 0.7874016F;
            // 
            // textBox47
            // 
            this.textBox47.DataField = "ITEM57";
            this.textBox47.Height = 0.1875F;
            this.textBox47.Left = 4.899213F;
            this.textBox47.Name = "textBox47";
            this.textBox47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox47.Tag = "";
            this.textBox47.Text = "ITEM57";
            this.textBox47.Top = 8.93504F;
            this.textBox47.Width = 0.865748F;
            // 
            // textBox48
            // 
            this.textBox48.DataField = "ITEM58";
            this.textBox48.Height = 0.1875F;
            this.textBox48.Left = 5.748425F;
            this.textBox48.Name = "textBox48";
            this.textBox48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox48.Tag = "";
            this.textBox48.Text = "ITEM58";
            this.textBox48.Top = 8.93504F;
            this.textBox48.Width = 0.9448819F;
            // 
            // textBox49
            // 
            this.textBox49.DataField = "ITEM59";
            this.textBox49.Height = 0.1875F;
            this.textBox49.Left = 0.3511811F;
            this.textBox49.Name = "textBox49";
            this.textBox49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox49.Tag = "";
            this.textBox49.Text = "ITEM59";
            this.textBox49.Top = 9.24685F;
            this.textBox49.Width = 0.5744094F;
            // 
            // textBox50
            // 
            this.textBox50.DataField = "ITEM61";
            this.textBox50.Height = 0.1875F;
            this.textBox50.Left = 3.42126F;
            this.textBox50.Name = "textBox50";
            this.textBox50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox50.Tag = "";
            this.textBox50.Text = "ITEM61";
            this.textBox50.Top = 9.24685F;
            this.textBox50.Width = 0.9479167F;
            // 
            // textBox54
            // 
            this.textBox54.DataField = "ITEM65";
            this.textBox54.Height = 0.1875F;
            this.textBox54.Left = 4.101575F;
            this.textBox54.Name = "textBox54";
            this.textBox54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox54.Tag = "";
            this.textBox54.Text = "ITEM65";
            this.textBox54.Top = 9.24685F;
            this.textBox54.Width = 0.7874016F;
            // 
            // textBox55
            // 
            this.textBox55.DataField = "ITEM66";
            this.textBox55.Height = 0.1875F;
            this.textBox55.Left = 4.899213F;
            this.textBox55.Name = "textBox55";
            this.textBox55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox55.Tag = "";
            this.textBox55.Text = "ITEM66";
            this.textBox55.Top = 9.24685F;
            this.textBox55.Width = 0.865748F;
            // 
            // textBox56
            // 
            this.textBox56.DataField = "ITEM67";
            this.textBox56.Height = 0.1875F;
            this.textBox56.Left = 5.748425F;
            this.textBox56.Name = "textBox56";
            this.textBox56.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox56.Tag = "";
            this.textBox56.Text = "ITEM67";
            this.textBox56.Top = 9.24685F;
            this.textBox56.Width = 0.9448819F;
            // 
            // textBox57
            // 
            this.textBox57.DataField = "ITEM68";
            this.textBox57.Height = 0.1875F;
            this.textBox57.Left = 0.3511811F;
            this.textBox57.Name = "textBox57";
            this.textBox57.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox57.Tag = "";
            this.textBox57.Text = "ITEM68";
            this.textBox57.Top = 9.548032F;
            this.textBox57.Width = 0.5744094F;
            // 
            // textBox58
            // 
            this.textBox58.DataField = "ITEM70";
            this.textBox58.Height = 0.1875F;
            this.textBox58.Left = 3.42126F;
            this.textBox58.Name = "textBox58";
            this.textBox58.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox58.Tag = "";
            this.textBox58.Text = "ITEM70";
            this.textBox58.Top = 9.548032F;
            this.textBox58.Width = 0.9479167F;
            // 
            // textBox62
            // 
            this.textBox62.DataField = "ITEM74";
            this.textBox62.Height = 0.1875F;
            this.textBox62.Left = 4.101575F;
            this.textBox62.Name = "textBox62";
            this.textBox62.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox62.Tag = "";
            this.textBox62.Text = "ITEM74";
            this.textBox62.Top = 9.548032F;
            this.textBox62.Width = 0.7874016F;
            // 
            // textBox63
            // 
            this.textBox63.DataField = "ITEM75";
            this.textBox63.Height = 0.1875F;
            this.textBox63.Left = 4.899213F;
            this.textBox63.Name = "textBox63";
            this.textBox63.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox63.Tag = "";
            this.textBox63.Text = "ITEM75";
            this.textBox63.Top = 9.548032F;
            this.textBox63.Width = 0.865748F;
            // 
            // textBox64
            // 
            this.textBox64.DataField = "ITEM76";
            this.textBox64.Height = 0.1875F;
            this.textBox64.Left = 5.748425F;
            this.textBox64.Name = "textBox64";
            this.textBox64.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox64.Tag = "";
            this.textBox64.Text = "ITEM76";
            this.textBox64.Top = 9.548032F;
            this.textBox64.Width = 0.9448819F;
            // 
            // textBox65
            // 
            this.textBox65.DataField = "ITEM77";
            this.textBox65.Height = 0.1875F;
            this.textBox65.Left = 0.3511811F;
            this.textBox65.Name = "textBox65";
            this.textBox65.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox65.Tag = "";
            this.textBox65.Text = "ITEM77";
            this.textBox65.Top = 9.839764F;
            this.textBox65.Width = 0.5744094F;
            // 
            // textBox66
            // 
            this.textBox66.DataField = "ITEM79";
            this.textBox66.Height = 0.1875F;
            this.textBox66.Left = 3.42126F;
            this.textBox66.Name = "textBox66";
            this.textBox66.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox66.Tag = "";
            this.textBox66.Text = "ITEM79";
            this.textBox66.Top = 9.839764F;
            this.textBox66.Width = 0.9479167F;
            // 
            // textBox70
            // 
            this.textBox70.DataField = "ITEM83";
            this.textBox70.Height = 0.1875F;
            this.textBox70.Left = 4.101575F;
            this.textBox70.Name = "textBox70";
            this.textBox70.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox70.Tag = "";
            this.textBox70.Text = "ITEM83";
            this.textBox70.Top = 9.839764F;
            this.textBox70.Width = 0.7874016F;
            // 
            // textBox71
            // 
            this.textBox71.DataField = "ITEM84";
            this.textBox71.Height = 0.1875F;
            this.textBox71.Left = 4.899213F;
            this.textBox71.Name = "textBox71";
            this.textBox71.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox71.Tag = "";
            this.textBox71.Text = "ITEM84";
            this.textBox71.Top = 9.839764F;
            this.textBox71.Width = 0.865748F;
            // 
            // textBox72
            // 
            this.textBox72.DataField = "ITEM85";
            this.textBox72.Height = 0.1875F;
            this.textBox72.Left = 5.748425F;
            this.textBox72.Name = "textBox72";
            this.textBox72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox72.Tag = "";
            this.textBox72.Text = "ITEM85";
            this.textBox72.Top = 9.839764F;
            this.textBox72.Width = 0.9448819F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.1413384F;
            this.line1.LineWeight = 0F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 7.486771F;
            this.line1.Visible = false;
            this.line1.Width = 7.559724F;
            this.line1.X1 = 0.1413384F;
            this.line1.X2 = 7.701062F;
            this.line1.Y1 = 7.486771F;
            this.line1.Y2 = 7.486771F;
            // 
            // line15
            // 
            this.line15.Height = 2.847608F;
            this.line15.Left = 0.8878664F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 7.278382F;
            this.line15.Visible = false;
            this.line15.Width = 0.003559828F;
            this.line15.X1 = 0.8914262F;
            this.line15.X2 = 0.8878664F;
            this.line15.Y1 = 7.278382F;
            this.line15.Y2 = 10.12599F;
            // 
            // line16
            // 
            this.line16.Height = 2.839737F;
            this.line16.Left = 3.353221F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 7.282313F;
            this.line16.Visible = false;
            this.line16.Width = 0.003560066F;
            this.line16.X1 = 3.356781F;
            this.line16.X2 = 3.353221F;
            this.line16.Y1 = 7.282313F;
            this.line16.Y2 = 10.12205F;
            // 
            // line17
            // 
            this.line17.Height = 2.847608F;
            this.line17.Left = 4.383536F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 7.278382F;
            this.line17.Visible = false;
            this.line17.Width = 0.003559113F;
            this.line17.X1 = 4.387095F;
            this.line17.X2 = 4.383536F;
            this.line17.Y1 = 7.278382F;
            this.line17.Y2 = 10.12599F;
            // 
            // line18
            // 
            this.line18.Height = 2.847608F;
            this.line18.Left = 4.700465F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 7.278382F;
            this.line18.Visible = false;
            this.line18.Width = 0.003559589F;
            this.line18.X1 = 4.704025F;
            this.line18.X2 = 4.700465F;
            this.line18.Y1 = 7.278382F;
            this.line18.Y2 = 10.12599F;
            // 
            // line19
            // 
            this.line19.Height = 2.847608F;
            this.line19.Left = 5.018575F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 7.278382F;
            this.line19.Visible = false;
            this.line19.Width = 0.003559589F;
            this.line19.X1 = 5.022135F;
            this.line19.X2 = 5.018575F;
            this.line19.Y1 = 7.278382F;
            this.line19.Y2 = 10.12599F;
            // 
            // line20
            // 
            this.line20.Height = 2.847613F;
            this.line20.Left = 5.446835F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 7.278347F;
            this.line20.Visible = false;
            this.line20.Width = 0.003559113F;
            this.line20.X1 = 5.450394F;
            this.line20.X2 = 5.446835F;
            this.line20.Y1 = 7.278347F;
            this.line20.Y2 = 10.12596F;
            // 
            // line21
            // 
            this.line21.Height = 2.847608F;
            this.line21.Left = 6.039442F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 7.278382F;
            this.line21.Visible = false;
            this.line21.Width = 0.003559113F;
            this.line21.X1 = 6.043001F;
            this.line21.X2 = 6.039442F;
            this.line21.Y1 = 7.278382F;
            this.line21.Y2 = 10.12599F;
            // 
            // line22
            // 
            this.line22.Height = 2.847608F;
            this.line22.Left = 6.861883F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 7.278382F;
            this.line22.Visible = false;
            this.line22.Width = 0.003560066F;
            this.line22.X1 = 6.865443F;
            this.line22.X2 = 6.861883F;
            this.line22.Y1 = 7.278382F;
            this.line22.Y2 = 10.12599F;
            // 
            // line23
            // 
            this.line23.Height = 2.847608F;
            this.line23.Left = 7.695743F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 7.278382F;
            this.line23.Visible = false;
            this.line23.Width = 0.003555775F;
            this.line23.X1 = 7.699299F;
            this.line23.X2 = 7.695743F;
            this.line23.Y1 = 7.278382F;
            this.line23.Y2 = 10.12599F;
            // 
            // line24
            // 
            this.line24.Height = 2.847608F;
            this.line24.Left = 0.1347168F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 7.278382F;
            this.line24.Visible = false;
            this.line24.Width = 0.003559604F;
            this.line24.X1 = 0.1382764F;
            this.line24.X2 = 0.1347168F;
            this.line24.Y1 = 7.278382F;
            this.line24.Y2 = 10.12599F;
            // 
            // line25
            // 
            this.line25.Height = 3.004074E-05F;
            this.line25.Left = 0.1382764F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 7.285039F;
            this.line25.Visible = false;
            this.line25.Width = 7.552251F;
            this.line25.X1 = 0.1382764F;
            this.line25.X2 = 7.690528F;
            this.line25.Y1 = 7.285069F;
            this.line25.Y2 = 7.285039F;
            // 
            // line26
            // 
            this.line26.Height = 0F;
            this.line26.Left = 0.144488F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 10.12165F;
            this.line26.Visible = false;
            this.line26.Width = 7.552247F;
            this.line26.X1 = 0.144488F;
            this.line26.X2 = 7.696735F;
            this.line26.Y1 = 10.12165F;
            this.line26.Y2 = 10.12165F;
            // 
            // shape1
            // 
            this.shape1.Height = 0.5416667F;
            this.shape1.Left = 0.2395833F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 30F;
            this.shape1.Top = 4.739583F;
            this.shape1.Visible = false;
            this.shape1.Width = 1.375F;
            // 
            // line2
            // 
            this.line2.Height = 0.541667F;
            this.line2.Left = 0.5937008F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 4.739764F;
            this.line2.Visible = false;
            this.line2.Width = 0F;
            this.line2.X1 = 0.5937008F;
            this.line2.X2 = 0.5937008F;
            this.line2.Y1 = 4.739764F;
            this.line2.Y2 = 5.281431F;
            // 
            // textBox84
            // 
            this.textBox84.Height = 0.4267716F;
            this.textBox84.Left = 0.3125984F;
            this.textBox84.Name = "textBox84";
            this.textBox84.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; ddo-font-vertical: true";
            this.textBox84.Text = "受領印";
            this.textBox84.Top = 4.798819F;
            this.textBox84.Visible = false;
            this.textBox84.Width = 0.2291667F;
            // 
            // line27
            // 
            this.line27.Height = 0F;
            this.line27.Left = 0F;
            this.line27.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 5.433071F;
            this.line27.Visible = false;
            this.line27.Width = 7.874016F;
            this.line27.X1 = 0F;
            this.line27.X2 = 7.874016F;
            this.line27.Y1 = 5.433071F;
            this.line27.Y2 = 5.433071F;
            // 
            // textBox85
            // 
            this.textBox85.DataField = "ITEM99";
            this.textBox85.Height = 0.5417323F;
            this.textBox85.Left = 1.75F;
            this.textBox85.Name = "textBox85";
            this.textBox85.Padding = new GrapeCity.ActiveReports.PaddingEx(4, 0, 0, 0);
            this.textBox85.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox85.Tag = "";
            this.textBox85.Text = "ITEM99";
            this.textBox85.Top = 4.739764F;
            this.textBox85.Width = 1.947638F;
            // 
            // textBox86
            // 
            this.textBox86.DataField = "ITEM99";
            this.textBox86.Height = 0.5417323F;
            this.textBox86.Left = 1.75F;
            this.textBox86.Name = "textBox86";
            this.textBox86.Padding = new GrapeCity.ActiveReports.PaddingEx(4, 0, 0, 0);
            this.textBox86.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox86.Tag = "";
            this.textBox86.Text = "ITEM99";
            this.textBox86.Top = 10.27756F;
            this.textBox86.Width = 1.947638F;
            // 
            // textBox87
            // 
            this.textBox87.DataField = "ITEM100";
            this.textBox87.Height = 0.1875F;
            this.textBox87.Left = 3.127559F;
            this.textBox87.Name = "textBox87";
            this.textBox87.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox87.Tag = "";
            this.textBox87.Text = "10";
            this.textBox87.Top = 7.790551F;
            this.textBox87.Width = 0.1858268F;
            // 
            // textBox88
            // 
            this.textBox88.DataField = "ITEM101";
            this.textBox88.Height = 0.1875F;
            this.textBox88.Left = 3.121654F;
            this.textBox88.Name = "textBox88";
            this.textBox88.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox88.Tag = "";
            this.textBox88.Text = "10";
            this.textBox88.Top = 8.051575F;
            this.textBox88.Width = 0.1917322F;
            // 
            // textBox89
            // 
            this.textBox89.DataField = "ITEM102";
            this.textBox89.Height = 0.1875F;
            this.textBox89.Left = 3.127559F;
            this.textBox89.Name = "textBox89";
            this.textBox89.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox89.Tag = "";
            this.textBox89.Text = "10";
            this.textBox89.Top = 8.354331F;
            this.textBox89.Width = 0.1917322F;
            // 
            // textBox90
            // 
            this.textBox90.DataField = "ITEM103";
            this.textBox90.Height = 0.1875F;
            this.textBox90.Left = 3.127559F;
            this.textBox90.Name = "textBox90";
            this.textBox90.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox90.Tag = "";
            this.textBox90.Text = "10";
            this.textBox90.Top = 8.635827F;
            this.textBox90.Width = 0.1917322F;
            // 
            // textBox91
            // 
            this.textBox91.DataField = "ITEM104";
            this.textBox91.Height = 0.1875F;
            this.textBox91.Left = 3.127559F;
            this.textBox91.Name = "textBox91";
            this.textBox91.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox91.Tag = "";
            this.textBox91.Text = "10";
            this.textBox91.Top = 8.93504F;
            this.textBox91.Width = 0.1917322F;
            // 
            // textBox92
            // 
            this.textBox92.DataField = "ITEM105";
            this.textBox92.Height = 0.1875F;
            this.textBox92.Left = 3.127559F;
            this.textBox92.Name = "textBox92";
            this.textBox92.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox92.Tag = "";
            this.textBox92.Text = "10";
            this.textBox92.Top = 9.24685F;
            this.textBox92.Width = 0.1917322F;
            // 
            // textBox93
            // 
            this.textBox93.DataField = "ITEM106";
            this.textBox93.Height = 0.1875F;
            this.textBox93.Left = 3.127559F;
            this.textBox93.Name = "textBox93";
            this.textBox93.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox93.Tag = "";
            this.textBox93.Text = "10";
            this.textBox93.Top = 9.548032F;
            this.textBox93.Width = 0.1917322F;
            // 
            // textBox94
            // 
            this.textBox94.DataField = "ITEM107";
            this.textBox94.Height = 0.1875F;
            this.textBox94.Left = 3.127559F;
            this.textBox94.Name = "textBox94";
            this.textBox94.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox94.Tag = "";
            this.textBox94.Text = "10";
            this.textBox94.Top = 9.839764F;
            this.textBox94.Width = 0.1917322F;
            // 
            // textBox95
            // 
            this.textBox95.DataField = "ITEM100";
            this.textBox95.Height = 0.1875F;
            this.textBox95.Left = 3.133465F;
            this.textBox95.Name = "textBox95";
            this.textBox95.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox95.Tag = "";
            this.textBox95.Text = "10";
            this.textBox95.Top = 2.294095F;
            this.textBox95.Width = 0.1858267F;
            // 
            // textBox96
            // 
            this.textBox96.DataField = "ITEM101";
            this.textBox96.Height = 0.1875F;
            this.textBox96.Left = 3.12756F;
            this.textBox96.Name = "textBox96";
            this.textBox96.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox96.Tag = "";
            this.textBox96.Text = "10";
            this.textBox96.Top = 2.555118F;
            this.textBox96.Width = 0.1917322F;
            // 
            // textBox97
            // 
            this.textBox97.DataField = "ITEM102";
            this.textBox97.Height = 0.1875F;
            this.textBox97.Left = 3.133465F;
            this.textBox97.Name = "textBox97";
            this.textBox97.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox97.Tag = "";
            this.textBox97.Text = "10";
            this.textBox97.Top = 2.857874F;
            this.textBox97.Width = 0.1917322F;
            // 
            // textBox98
            // 
            this.textBox98.DataField = "ITEM103";
            this.textBox98.Height = 0.1875F;
            this.textBox98.Left = 3.133465F;
            this.textBox98.Name = "textBox98";
            this.textBox98.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox98.Tag = "";
            this.textBox98.Text = "10";
            this.textBox98.Top = 3.13937F;
            this.textBox98.Width = 0.1917322F;
            // 
            // textBox99
            // 
            this.textBox99.DataField = "ITEM104";
            this.textBox99.Height = 0.1875F;
            this.textBox99.Left = 3.133465F;
            this.textBox99.Name = "textBox99";
            this.textBox99.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox99.Tag = "";
            this.textBox99.Text = "10";
            this.textBox99.Top = 3.438583F;
            this.textBox99.Width = 0.1917322F;
            // 
            // textBox100
            // 
            this.textBox100.DataField = "ITEM105";
            this.textBox100.Height = 0.1875F;
            this.textBox100.Left = 3.133465F;
            this.textBox100.Name = "textBox100";
            this.textBox100.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox100.Tag = "";
            this.textBox100.Text = "10";
            this.textBox100.Top = 3.750394F;
            this.textBox100.Width = 0.1917322F;
            // 
            // textBox101
            // 
            this.textBox101.DataField = "ITEM106";
            this.textBox101.Height = 0.1875F;
            this.textBox101.Left = 3.133465F;
            this.textBox101.Name = "textBox101";
            this.textBox101.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox101.Tag = "";
            this.textBox101.Text = "10";
            this.textBox101.Top = 4.051575F;
            this.textBox101.Width = 0.1917322F;
            // 
            // textBox102
            // 
            this.textBox102.DataField = "ITEM107";
            this.textBox102.Height = 0.1875F;
            this.textBox102.Left = 3.133465F;
            this.textBox102.Name = "textBox102";
            this.textBox102.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.textBox102.Tag = "";
            this.textBox102.Text = "10";
            this.textBox102.Top = 4.343307F;
            this.textBox102.Width = 0.1917322F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KBDE10133R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.767716F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル378)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClrCg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル340)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル359)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト389)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト332)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト351)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト370)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト231)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト342)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト361)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト380)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル203)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト258)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル196)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル201)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト204)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト205)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル207)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル208)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト209)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル210)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト211)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト212)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト213)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト215)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト217)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト218)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル219)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト220)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト230)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト232)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト236)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト237)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト238)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト331)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト333)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト337)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト338)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト339)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト341)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト343)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト347)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト348)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト349)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト350)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト352)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト356)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト357)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト358)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト360)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト362)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト366)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト367)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト368)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト369)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト371)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト375)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト376)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト377)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト379)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト381)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト385)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト386)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト387)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト388)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト390)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト394)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト395)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト396)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト311)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト312)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト313)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト314)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト315)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト318)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル414)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト415)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト416)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト417)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル418)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル420)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト422)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル424)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル425)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト426)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル427)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト428)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト429)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト430)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト431)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト432)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト433)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル434)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト435)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル535)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト642)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト643)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル378;
        private GrapeCity.ActiveReports.SectionReportModel.Label BackClrCg;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル340;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル359;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト389;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト332;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト351;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト370;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト231;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト342;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト361;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト380;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル203;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト258;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル196;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル201;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト204;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト205;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線206;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル207;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル208;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト209;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル210;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト211;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト212;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト213;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト215;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト217;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト218;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル219;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト220;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト230;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト232;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト236;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト237;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト238;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト331;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト333;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト337;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト338;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト339;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト341;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト343;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト347;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト348;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト349;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト350;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト352;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト356;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト357;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト358;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト360;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト362;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト366;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト367;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト368;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト369;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト371;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト375;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト376;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト377;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト379;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト381;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト385;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト386;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト387;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト388;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト390;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト394;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト395;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト396;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス309;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス310;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト311;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト312;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト313;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト314;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト315;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト316;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト317;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト318;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線407;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル414;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト415;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト416;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト417;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル418;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル420;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト422;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線423;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル424;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル425;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト426;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル427;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト428;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト429;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト430;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト431;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト432;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト433;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル434;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト435;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル535;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト642;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト643;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox75;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape10;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox78;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox80;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox81;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox82;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox83;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox70;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox72;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox84;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox85;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox86;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox87;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox88;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox89;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox90;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox91;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox92;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox93;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox94;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox95;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox96;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox97;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox98;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox99;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox100;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox101;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox102;
    }
}
