﻿namespace jp.co.fsi.kb.kbde1011
{
    partial class KBDE1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblTantoshaNmFr = new System.Windows.Forms.Label();
            this.txtTantoshaCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaCd = new System.Windows.Forms.Label();
            this.lblFunanushiNmFr = new System.Windows.Forms.Label();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCd = new System.Windows.Forms.Label();
            this.txtSearchCode = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSearchCode = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGengoYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblEraBackFr = new System.Windows.Forms.Label();
            this.lblDenpyoDate = new System.Windows.Forms.Label();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblDateBet1 = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGengoYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDateBet2 = new System.Windows.Forms.Label();
            this.lblFunanushiNmTo = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateBet3 = new System.Windows.Forms.Label();
            this.lblTantoshaNmTo = new System.Windows.Forms.Label();
            this.txtTantoshaCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.btnEnter = new System.Windows.Forms.Button();
            this.lblShohinNmTo = new System.Windows.Forms.Label();
            this.txtShohinCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblShohinNmFr = new System.Windows.Forms.Label();
            this.txtShohinCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinCd = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(754, 23);
            this.lblTitle.Text = "仕入伝票検索";
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(3, 49);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(132, 49);
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Location = new System.Drawing.Point(196, 49);
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Location = new System.Drawing.Point(196, 49);
            // 
            // btnF5
            // 
            this.btnF5.Location = new System.Drawing.Point(324, 49);
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(452, 49);
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(260, 49);
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(516, 49);
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Location = new System.Drawing.Point(580, 49);
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Location = new System.Drawing.Point(772, 49);
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Location = new System.Drawing.Point(708, 49);
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Location = new System.Drawing.Point(644, 49);
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.btnEnter);
            this.pnlDebug.Location = new System.Drawing.Point(5, 356);
            this.pnlDebug.Size = new System.Drawing.Size(762, 101);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
            // 
            // lblTantoshaNmFr
            // 
            this.lblTantoshaNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNmFr.Location = new System.Drawing.Point(119, 58);
            this.lblTantoshaNmFr.Name = "lblTantoshaNmFr";
            this.lblTantoshaNmFr.Size = new System.Drawing.Size(210, 20);
            this.lblTantoshaNmFr.TabIndex = 26;
            this.lblTantoshaNmFr.Text = "先　頭";
            this.lblTantoshaNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCdFr
            // 
            this.txtTantoshaCdFr.AutoSizeFromLength = true;
            this.txtTantoshaCdFr.DisplayLength = null;
            this.txtTantoshaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoshaCdFr.Location = new System.Drawing.Point(84, 58);
            this.txtTantoshaCdFr.MaxLength = 4;
            this.txtTantoshaCdFr.Name = "txtTantoshaCdFr";
            this.txtTantoshaCdFr.Size = new System.Drawing.Size(34, 20);
            this.txtTantoshaCdFr.TabIndex = 25;
            this.txtTantoshaCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCdFr_Validating);
            // 
            // lblTantoshaCd
            // 
            this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaCd.Location = new System.Drawing.Point(12, 58);
            this.lblTantoshaCd.Name = "lblTantoshaCd";
            this.lblTantoshaCd.Size = new System.Drawing.Size(72, 20);
            this.lblTantoshaCd.TabIndex = 24;
            this.lblTantoshaCd.Text = "担 当 者";
            this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiNmFr
            // 
            this.lblFunanushiNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiNmFr.Location = new System.Drawing.Point(119, 38);
            this.lblFunanushiNmFr.Name = "lblFunanushiNmFr";
            this.lblFunanushiNmFr.Size = new System.Drawing.Size(210, 20);
            this.lblFunanushiNmFr.TabIndex = 20;
            this.lblFunanushiNmFr.Text = "先　頭";
            this.lblFunanushiNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = true;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(84, 38);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(34, 20);
            this.txtFunanushiCdFr.TabIndex = 19;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
            // 
            // lblFunanushiCd
            // 
            this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCd.Location = new System.Drawing.Point(12, 38);
            this.lblFunanushiCd.Name = "lblFunanushiCd";
            this.lblFunanushiCd.Size = new System.Drawing.Size(72, 20);
            this.lblFunanushiCd.TabIndex = 18;
            this.lblFunanushiCd.Text = "船主CD";
            this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSearchCode
            // 
            this.txtSearchCode.AutoSizeFromLength = true;
            this.txtSearchCode.DisplayLength = null;
            this.txtSearchCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSearchCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSearchCode.Location = new System.Drawing.Point(699, 58);
            this.txtSearchCode.MaxLength = 10;
            this.txtSearchCode.Name = "txtSearchCode";
            this.txtSearchCode.Size = new System.Drawing.Size(76, 20);
            this.txtSearchCode.TabIndex = 37;
            this.txtSearchCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtSearchCode_Validating);
            // 
            // lblSearchCode
            // 
            this.lblSearchCode.BackColor = System.Drawing.Color.Silver;
            this.lblSearchCode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSearchCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSearchCode.Location = new System.Drawing.Point(611, 58);
            this.lblSearchCode.Name = "lblSearchCode";
            this.lblSearchCode.Size = new System.Drawing.Size(88, 20);
            this.lblSearchCode.TabIndex = 36;
            this.lblSearchCode.Text = "検索コード";
            this.lblSearchCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.AutoSize = true;
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.Location = new System.Drawing.Point(307, 19);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(21, 13);
            this.lblDayFr.TabIndex = 8;
            this.lblDayFr.Text = "日";
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.AutoSize = true;
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.Location = new System.Drawing.Point(240, 19);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(21, 13);
            this.lblMonthFr.TabIndex = 6;
            this.lblMonthFr.Text = "月";
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.Location = new System.Drawing.Point(263, 15);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(40, 20);
            this.txtDayFr.TabIndex = 7;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblYearFr
            // 
            this.lblYearFr.AutoSize = true;
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.Location = new System.Drawing.Point(173, 19);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(21, 13);
            this.lblYearFr.TabIndex = 4;
            this.lblYearFr.Text = "年";
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.Location = new System.Drawing.Point(196, 15);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(40, 20);
            this.txtMonthFr.TabIndex = 5;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtGengoYearFr
            // 
            this.txtGengoYearFr.AutoSizeFromLength = false;
            this.txtGengoYearFr.DisplayLength = null;
            this.txtGengoYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearFr.Location = new System.Drawing.Point(129, 15);
            this.txtGengoYearFr.MaxLength = 2;
            this.txtGengoYearFr.Name = "txtGengoYearFr";
            this.txtGengoYearFr.Size = new System.Drawing.Size(40, 20);
            this.txtGengoYearFr.TabIndex = 3;
            this.txtGengoYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearFr_Validating);
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.Location = new System.Drawing.Point(86, 15);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(40, 20);
            this.lblGengoFr.TabIndex = 2;
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblEraBackFr
            // 
            this.lblEraBackFr.BackColor = System.Drawing.Color.Silver;
            this.lblEraBackFr.Location = new System.Drawing.Point(83, 13);
            this.lblEraBackFr.Name = "lblEraBackFr";
            this.lblEraBackFr.Size = new System.Drawing.Size(245, 24);
            this.lblEraBackFr.TabIndex = 6;
            this.lblEraBackFr.Text = " ";
            // 
            // lblDenpyoDate
            // 
            this.lblDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDate.Location = new System.Drawing.Point(12, 14);
            this.lblDenpyoDate.Name = "lblDenpyoDate";
            this.lblDenpyoDate.Size = new System.Drawing.Size(72, 24);
            this.lblDenpyoDate.TabIndex = 0;
            this.lblDenpyoDate.Text = "伝票日付";
            this.lblDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(12, 103);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(760, 296);
            this.dgvList.TabIndex = 38;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvList_CellFormatting);
            this.dgvList.Enter += new System.EventHandler(this.dgvList_Enter);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // lblDateBet1
            // 
            this.lblDateBet1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateBet1.Location = new System.Drawing.Point(334, 15);
            this.lblDateBet1.Name = "lblDateBet1";
            this.lblDateBet1.Size = new System.Drawing.Size(18, 20);
            this.lblDateBet1.TabIndex = 9;
            this.lblDateBet1.Text = "～";
            this.lblDateBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.AutoSize = true;
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTo.Location = new System.Drawing.Point(584, 18);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(21, 13);
            this.lblDayTo.TabIndex = 17;
            this.lblDayTo.Text = "日";
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.AutoSize = true;
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.Location = new System.Drawing.Point(517, 18);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(21, 13);
            this.lblMonthTo.TabIndex = 15;
            this.lblMonthTo.Text = "月";
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTo.Location = new System.Drawing.Point(540, 14);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(40, 20);
            this.txtDayTo.TabIndex = 16;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // lblYearTo
            // 
            this.lblYearTo.AutoSize = true;
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.Location = new System.Drawing.Point(450, 18);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(21, 13);
            this.lblYearTo.TabIndex = 13;
            this.lblYearTo.Text = "年";
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.Location = new System.Drawing.Point(473, 14);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(40, 20);
            this.txtMonthTo.TabIndex = 14;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // txtGengoYearTo
            // 
            this.txtGengoYearTo.AutoSizeFromLength = false;
            this.txtGengoYearTo.DisplayLength = null;
            this.txtGengoYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearTo.Location = new System.Drawing.Point(406, 14);
            this.txtGengoYearTo.MaxLength = 2;
            this.txtGengoYearTo.Name = "txtGengoYearTo";
            this.txtGengoYearTo.Size = new System.Drawing.Size(40, 20);
            this.txtGengoYearTo.TabIndex = 12;
            this.txtGengoYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearTo_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.Location = new System.Drawing.Point(363, 14);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(40, 20);
            this.lblGengoTo.TabIndex = 11;
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.Location = new System.Drawing.Point(360, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(245, 24);
            this.label6.TabIndex = 908;
            this.label6.Text = " ";
            // 
            // lblDateBet2
            // 
            this.lblDateBet2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateBet2.Location = new System.Drawing.Point(334, 37);
            this.lblDateBet2.Name = "lblDateBet2";
            this.lblDateBet2.Size = new System.Drawing.Size(18, 20);
            this.lblDateBet2.TabIndex = 21;
            this.lblDateBet2.Text = "～";
            this.lblDateBet2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiNmTo
            // 
            this.lblFunanushiNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiNmTo.Location = new System.Drawing.Point(396, 38);
            this.lblFunanushiNmTo.Name = "lblFunanushiNmTo";
            this.lblFunanushiNmTo.Size = new System.Drawing.Size(211, 20);
            this.lblFunanushiNmTo.TabIndex = 23;
            this.lblFunanushiNmTo.Text = "最　後";
            this.lblFunanushiNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = true;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(361, 38);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(34, 20);
            this.txtFunanushiCdTo.TabIndex = 22;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
            // 
            // lblDateBet3
            // 
            this.lblDateBet3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateBet3.Location = new System.Drawing.Point(334, 57);
            this.lblDateBet3.Name = "lblDateBet3";
            this.lblDateBet3.Size = new System.Drawing.Size(18, 20);
            this.lblDateBet3.TabIndex = 27;
            this.lblDateBet3.Text = "～";
            this.lblDateBet3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoshaNmTo
            // 
            this.lblTantoshaNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNmTo.Location = new System.Drawing.Point(396, 58);
            this.lblTantoshaNmTo.Name = "lblTantoshaNmTo";
            this.lblTantoshaNmTo.Size = new System.Drawing.Size(211, 20);
            this.lblTantoshaNmTo.TabIndex = 29;
            this.lblTantoshaNmTo.Text = "最　後";
            this.lblTantoshaNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCdTo
            // 
            this.txtTantoshaCdTo.AutoSizeFromLength = true;
            this.txtTantoshaCdTo.DisplayLength = null;
            this.txtTantoshaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoshaCdTo.Location = new System.Drawing.Point(361, 58);
            this.txtTantoshaCdTo.MaxLength = 4;
            this.txtTantoshaCdTo.Name = "txtTantoshaCdTo";
            this.txtTantoshaCdTo.Size = new System.Drawing.Size(34, 20);
            this.txtTantoshaCdTo.TabIndex = 28;
            this.txtTantoshaCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCdTo_Validating);
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnter.Location = new System.Drawing.Point(67, 49);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(65, 45);
            this.btnEnter.TabIndex = 905;
            this.btnEnter.TabStop = false;
            this.btnEnter.Text = "Enter\r\n\r\n決定";
            this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // lblShohinNmTo
            // 
            this.lblShohinNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblShohinNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNmTo.Location = new System.Drawing.Point(512, 78);
            this.lblShohinNmTo.Name = "lblShohinNmTo";
            this.lblShohinNmTo.Size = new System.Drawing.Size(213, 20);
            this.lblShohinNmTo.TabIndex = 35;
            this.lblShohinNmTo.Text = "最　後";
            this.lblShohinNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdTo
            // 
            this.txtShohinCdTo.AutoSizeFromLength = true;
            this.txtShohinCdTo.DisplayLength = null;
            this.txtShohinCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohinCdTo.Location = new System.Drawing.Point(414, 78);
            this.txtShohinCdTo.MaxLength = 13;
            this.txtShohinCdTo.Name = "txtShohinCdTo";
            this.txtShohinCdTo.Size = new System.Drawing.Size(97, 20);
            this.txtShohinCdTo.TabIndex = 34;
            this.txtShohinCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdTo_Validating);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(389, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 33;
            this.label2.Text = "～";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinNmFr
            // 
            this.lblShohinNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblShohinNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNmFr.Location = new System.Drawing.Point(181, 78);
            this.lblShohinNmFr.Name = "lblShohinNmFr";
            this.lblShohinNmFr.Size = new System.Drawing.Size(206, 20);
            this.lblShohinNmFr.TabIndex = 32;
            this.lblShohinNmFr.Text = "先　頭";
            this.lblShohinNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdFr
            // 
            this.txtShohinCdFr.AutoSizeFromLength = true;
            this.txtShohinCdFr.DisplayLength = null;
            this.txtShohinCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohinCdFr.Location = new System.Drawing.Point(84, 78);
            this.txtShohinCdFr.MaxLength = 13;
            this.txtShohinCdFr.Name = "txtShohinCdFr";
            this.txtShohinCdFr.Size = new System.Drawing.Size(97, 20);
            this.txtShohinCdFr.TabIndex = 31;
            this.txtShohinCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdFr_Validating);
            // 
            // lblShohinCd
            // 
            this.lblShohinCd.BackColor = System.Drawing.Color.Silver;
            this.lblShohinCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinCd.Location = new System.Drawing.Point(12, 78);
            this.lblShohinCd.Name = "lblShohinCd";
            this.lblShohinCd.Size = new System.Drawing.Size(72, 20);
            this.lblShohinCd.TabIndex = 30;
            this.lblShohinCd.Text = "商品CD";
            this.lblShohinCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDE1012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 460);
            this.Controls.Add(this.lblShohinNmTo);
            this.Controls.Add(this.txtShohinCdTo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblShohinNmFr);
            this.Controls.Add(this.txtShohinCdFr);
            this.Controls.Add(this.lblShohinCd);
            this.Controls.Add(this.lblTantoshaNmTo);
            this.Controls.Add(this.txtTantoshaCdTo);
            this.Controls.Add(this.lblDateBet3);
            this.Controls.Add(this.lblFunanushiNmTo);
            this.Controls.Add(this.txtFunanushiCdTo);
            this.Controls.Add(this.lblDateBet2);
            this.Controls.Add(this.lblDayTo);
            this.Controls.Add(this.lblMonthTo);
            this.Controls.Add(this.txtDayTo);
            this.Controls.Add(this.lblYearTo);
            this.Controls.Add(this.txtMonthTo);
            this.Controls.Add(this.txtGengoYearTo);
            this.Controls.Add(this.lblGengoTo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblDateBet1);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.lblDenpyoDate);
            this.Controls.Add(this.lblDayFr);
            this.Controls.Add(this.lblMonthFr);
            this.Controls.Add(this.txtDayFr);
            this.Controls.Add(this.lblYearFr);
            this.Controls.Add(this.txtMonthFr);
            this.Controls.Add(this.txtGengoYearFr);
            this.Controls.Add(this.lblGengoFr);
            this.Controls.Add(this.lblEraBackFr);
            this.Controls.Add(this.txtSearchCode);
            this.Controls.Add(this.lblSearchCode);
            this.Controls.Add(this.lblFunanushiNmFr);
            this.Controls.Add(this.txtFunanushiCdFr);
            this.Controls.Add(this.lblFunanushiCd);
            this.Controls.Add(this.lblTantoshaNmFr);
            this.Controls.Add(this.txtTantoshaCdFr);
            this.Controls.Add(this.lblTantoshaCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBDE1012";
            this.ShowFButton = true;
            this.Text = "売上伝票検索";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTantoshaCd, 0);
            this.Controls.SetChildIndex(this.txtTantoshaCdFr, 0);
            this.Controls.SetChildIndex(this.lblTantoshaNmFr, 0);
            this.Controls.SetChildIndex(this.lblFunanushiCd, 0);
            this.Controls.SetChildIndex(this.txtFunanushiCdFr, 0);
            this.Controls.SetChildIndex(this.lblFunanushiNmFr, 0);
            this.Controls.SetChildIndex(this.lblSearchCode, 0);
            this.Controls.SetChildIndex(this.txtSearchCode, 0);
            this.Controls.SetChildIndex(this.lblEraBackFr, 0);
            this.Controls.SetChildIndex(this.lblGengoFr, 0);
            this.Controls.SetChildIndex(this.txtGengoYearFr, 0);
            this.Controls.SetChildIndex(this.txtMonthFr, 0);
            this.Controls.SetChildIndex(this.lblYearFr, 0);
            this.Controls.SetChildIndex(this.txtDayFr, 0);
            this.Controls.SetChildIndex(this.lblMonthFr, 0);
            this.Controls.SetChildIndex(this.lblDayFr, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDate, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblDateBet1, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.lblGengoTo, 0);
            this.Controls.SetChildIndex(this.txtGengoYearTo, 0);
            this.Controls.SetChildIndex(this.txtMonthTo, 0);
            this.Controls.SetChildIndex(this.lblYearTo, 0);
            this.Controls.SetChildIndex(this.txtDayTo, 0);
            this.Controls.SetChildIndex(this.lblMonthTo, 0);
            this.Controls.SetChildIndex(this.lblDayTo, 0);
            this.Controls.SetChildIndex(this.lblDateBet2, 0);
            this.Controls.SetChildIndex(this.txtFunanushiCdTo, 0);
            this.Controls.SetChildIndex(this.lblFunanushiNmTo, 0);
            this.Controls.SetChildIndex(this.lblDateBet3, 0);
            this.Controls.SetChildIndex(this.txtTantoshaCdTo, 0);
            this.Controls.SetChildIndex(this.lblTantoshaNmTo, 0);
            this.Controls.SetChildIndex(this.lblShohinCd, 0);
            this.Controls.SetChildIndex(this.txtShohinCdFr, 0);
            this.Controls.SetChildIndex(this.lblShohinNmFr, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtShohinCdTo, 0);
            this.Controls.SetChildIndex(this.lblShohinNmTo, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTantoshaNmFr;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCdFr;
        private System.Windows.Forms.Label lblTantoshaCd;
        private System.Windows.Forms.Label lblFunanushiNmFr;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtSearchCode;
        private System.Windows.Forms.Label lblSearchCode;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private System.Windows.Forms.Label lblYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearFr;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblEraBackFr;
        private System.Windows.Forms.Label lblDenpyoDate;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblDateBet1;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblDateBet2;
        private System.Windows.Forms.Label lblFunanushiNmTo;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblDateBet3;
        private System.Windows.Forms.Label lblTantoshaNmTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCdTo;
        protected System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.Label lblShohinNmTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblShohinNmFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdFr;
        private System.Windows.Forms.Label lblShohinCd;

    }
}