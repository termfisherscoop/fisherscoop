﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.sosok.erp.common.dataaccess;
using jp.co.sosok.erp.common.forms;
using jp.co.sosok.erp.common.util;


namespace jp.co.sosok.erp.kob.kobe2011
{
    /// <summary>
    /// 売上伝票検索(KOBE2012)
    /// </summary>
    public partial class KOBE2012 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KOBE2012()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 開始
            lblGengoFr.Text = jpDate[0];
            txtGengoYearFr.Text = jpDate[2];
            txtMonthFr.Text = jpDate[3];
            txtDayFr.Text = jpDate[4];
            // 終了
            lblGengoTo.Text = jpDate[0];
            txtGengoYearTo.Text = jpDate[2];
            txtMonthTo.Text = jpDate[3];
            txtDayTo.Text = jpDate[4];

            // データ取得してグリッドに設定
            this.getListData(true);

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 81;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[1].Width = 68;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[2].Width = 74;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[3].Width = 190;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[4].Width = 115;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[5].Width = 115;
            this.dgvList.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[6].Width = 89;
            this.dgvList.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // データが無い時
            this.btnEnter.Enabled = false;
            this.dgvList.Enabled = false;

            this.txtGengoYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年と取引先コードに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                case "txtTantoshaCdFr":
                case "txtTantoshaCdTo":
                case "txtShohinCdFr":
                case "txtShohinCdTo":
                case "txtGengoYearFr":
                case "txtGengoYearTo":
                    this.btnF1.Enabled = true;
                    this.btnF4.Enabled = false;
                    this.btnF6.Enabled = true;
                    this.btnEnter.Enabled = false;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    this.btnF4.Enabled = false;
                    this.btnF6.Enabled = true;
                    this.btnEnter.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。
            System.Reflection.Assembly asm;
            Type t;

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                #region 元号
                case "txtGengoYearFr":
                case "txtGengoYearTo":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.sosok.erp.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.ActiveCtlNm == "txtGengoYearFr")
                            {
                                frm.InData = this.lblGengoFr.Text;
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearTo")
                            {
                                frm.InData = this.lblGengoTo.Text;
                            }
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtGengoYearFr")
                                {
                                    this.lblGengoFr.Text = result[1];

                                    this.SetJpDateFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text,
                                        this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba));
                                }
                                else if (this.ActiveCtlNm == "txtGengoYearTo")
                                {
                                    this.lblGengoTo.Text = result[1];

                                    this.SetJpDateTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                                        this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba));
                                }
                            }
                        }

                    }
                    break;
                #endregion

                #region 船主CD
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.sosok.erp.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtFunanushiCdFr")
                                {
                                    this.txtFunanushiCdFr.Text = outData[0];
                                    this.lblFunanushiNmFr.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtFunanushiCdTo")
                                {
                                    this.txtFunanushiCdTo.Text = outData[0];
                                    this.lblFunanushiNmTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region 担当者CD
                case "txtTantoshaCdFr":
                case "txtTantoshaCdTo":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.sosok.erp.kob.kobc9041.KOBC9041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtTantoshaCdFr")
                                {
                                    this.txtTantoshaCdFr.Text = outData[0];
                                    this.lblTantoshaNmFr.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtTantoshaCdTo")
                                {
                                    this.txtTantoshaCdTo.Text = outData[0];
                                    this.lblTantoshaNmTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region 商品CD
                case "txtShohinCdFr":
                case "txtShohinCdTo":
                    // 商品一覧選択KOBC9031
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KOBC9031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.sosok.erp.kob.kobc9031.KOBC9031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtShohinCdFr")
                                {
                                    this.txtShohinCdFr.Text = outData[0];
                                    this.lblShohinNmFr.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtShohinCdTo")
                                {
                                    this.txtShohinCdTo.Text = outData[0];
                                    this.lblShohinNmTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    break;
                #endregion
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 条件（検索結果をクリアして年(自)にフォーカスする）
            this.getListData(true);
            this.txtGengoYearFr.SelectAll();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 検索開始
            this.getListData(false);
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドのフォーカス時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Enter(object sender, EventArgs e)
        {
            this.btnF1.Enabled = false;
            this.btnF4.Enabled = true;
            this.btnF6.Enabled = false;
            this.btnEnter.Enabled = true;
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                this.ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルフォーマット設定処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 5:
                case 6:
                    // 伝票金額、消費税がゼロ以下は文字色を赤にする
                    if (Util.ToDecimal(e.Value) < 0)
                    {
                        e.CellStyle.ForeColor = Color.Red;
                    }
                    break;
            }
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            this.ReturnVal();
        }

        /// <summary>
        /// 年(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtGengoYearFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtGengoYearFr.Text))
            {
                this.txtGengoYearFr.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text,
                this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 月(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtMonthFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空又は1未満の場合、1月として処理
            if (ValChk.IsEmpty(this.txtMonthFr.Text) || Util.ToInt(this.txtMonthFr.Text) < 1)
            {
                this.txtMonthFr.Text = "1";
            }
            // 12を超える月が入力された場合、12月として処理
            else if (Util.ToInt(this.txtMonthFr.Text) > 12)
            {
                this.txtMonthFr.Text = "12";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 日(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDayFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtDayFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空又は1未満の場合、1日として処理
            if (ValChk.IsEmpty(this.txtDayFr.Text) || Util.ToInt(this.txtDayFr.Text) < 1)
            {
                this.txtDayFr.Text = "1";
            }
            else
            {
                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text,
                    this.txtMonthFr.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                if (Util.ToInt(this.txtDayFr.Text) > lastDayInMonth)
                {
                    this.txtDayFr.Text = Util.ToString(lastDayInMonth);
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text,
                this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtGengoYearTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtGengoYearTo.Text))
            {
                this.txtGengoYearTo.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 月(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtMonthTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空又は1未満の場合、1月として処理
            if (ValChk.IsEmpty(this.txtMonthTo.Text) || Util.ToInt(this.txtMonthTo.Text) < 1)
            {
                this.txtMonthTo.Text = "1";
            }
            // 12を超える月が入力された場合、12月として処理
            else if (Util.ToInt(this.txtMonthTo.Text) > 12)
            {
                this.txtMonthTo.Text = "12";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 日(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDayTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtDayTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空又は1未満の場合、1日として処理
            if (ValChk.IsEmpty(this.txtDayTo.Text) || Util.ToInt(this.txtDayTo.Text) < 1)
            {
                // 空の場合、1日として処理
                this.txtDayFr.Text = "1";
            }
            else
            {
                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                    this.txtMonthTo.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                if (Util.ToInt(this.txtDayTo.Text) > lastDayInMonth)
                {
                    this.txtDayTo.Text = Util.ToString(lastDayInMonth);
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 船主CD(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtFunanushiCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiNmFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdFr.Text);
            }
            else
            {
                this.lblFunanushiNmFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 船主CD(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtFunanushiCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiNmTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdTo.Text);
            }
            else
            {
                this.lblFunanushiNmTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 担当者CD(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoshaCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtTantoshaCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtTantoshaCdFr.Text))
            {
                this.lblTantoshaNmFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtTantoshaCdFr.Text);
            }
            else
            {
                this.lblTantoshaNmFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 担当者CD(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoshaCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtTantoshaCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtTantoshaCdTo.Text))
            {
                this.lblTantoshaNmTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtTantoshaCdTo.Text);
            }
            else
            {
                this.lblTantoshaNmTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 商品CD(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtShohinCdFr.Text))
            {
                this.lblShohinNmFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN", this.txtShohinCdFr.Text);
            }
            else
            {
                this.lblShohinNmFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 商品CD(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtShohinCdTo.Text))
            {
                this.lblShohinNmTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN", this.txtShohinCdTo.Text);
            }
            else
            {
                this.lblShohinNmTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 検索コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearchCode_Validating(object sender, CancelEventArgs e)
        {
            // データ取得してグリッドに設定
            this.getListData(false);
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtGengoYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtGengoYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 売上伝票履歴のデータを取得
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        /// <returns>売上伝票履歴の取得したデータ</returns>
        private DataTable GetTB_HN_ZIDO_SHIWAKE_RIREKI(bool isInitial)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" A.DENPYO_BANGO    AS 伝票番号,");
            sql.Append(" A.KAIKEI_NENDO    AS 会計年度,");
            sql.Append(" A.DENPYO_DATE     AS 伝票日付,");
            sql.Append(" A.KAIIN_BANGO     AS 会員番号,");
            sql.Append(" A.KAIIN_NM        AS 会員名称,");
            sql.Append(" A.TANTOSHA_CD     AS 担当者コード,");
            sql.Append(" D.TANTOSHA_NM     AS 担当者名,");
            sql.Append(" A.TORIHIKI_KUBUN2 AS 取引区分２ ");
            sql.Append("FROM");
            sql.Append(" TB_HN_TORIHIKI_DENPYO AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_CM_TANTOSHA AS D");
            sql.Append(" ON A.KAISHA_CD = D.KAISHA_CD");
            sql.Append(" AND A.TANTOSHA_CD = D.TANTOSHA_CD ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND A.DENPYO_KUBUN = 1");
            //sql.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" AND A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO");
            sql.Append(" AND A.KAIIN_BANGO BETWEEN @KAIIN_BANGO_FR AND @KAIIN_BANGO_TO");
            sql.Append(" AND A.TANTOSHA_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO");
            sql.Append(" AND A.DENPYO_BANGO IN (");
            sql.Append("SELECT");
            sql.Append(" DENPYO_BANGO ");
            sql.Append("FROM");
            sql.Append(" TB_HN_TORIHIKI_MEISAI ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = A.KAISHA_CD");
            sql.Append(" AND DENPYO_KUBUN = A.DENPYO_KUBUN");
            sql.Append(" AND KAIKEI_NENDO = A.KAIKEI_NENDO");
            sql.Append(" AND (SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO)");
            sql.Append(")");

            // 検索コード
            if (!ValChk.IsEmpty(this.txtSearchCode.Text))
            {
                sql.AppendFormat(" AND A.SHOHYO_BANGO = '{0}'", this.txtSearchCode.Text);
            }
            // 初期起動時
            if (isInitial)
            {
                // 表示しない条件を追加
                sql.Append(" AND 1 = 0");
            }
            sql.Append(" ORDER BY");
            sql.Append(" A.KAISHA_CD ASC,");
            sql.Append(" A.DENPYO_DATE ASC,");
            sql.Append(" A.DENPYO_BANGO ASC ");

            // 伝票日付
            DateTime DENPYO_DATE_FR = Util.ConvAdDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text,
                    this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
            DateTime DENPYO_DATE_TO = Util.ConvAdDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                    this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);
            
            // 船主CD
            string KAIIN_BANGO_FR = "";
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                KAIIN_BANGO_FR = "0";
            }
            else
            {
                KAIIN_BANGO_FR = this.txtFunanushiCdFr.Text;
            }
            string KAIIN_BANGO_TO = "";
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                KAIIN_BANGO_TO = "9999";
            }
            else
            {
                KAIIN_BANGO_TO = this.txtFunanushiCdTo.Text;
            }

            // 担当者CD
            Decimal TANTOSHA_CD_FR;
            if (ValChk.IsEmpty(this.txtTantoshaCdFr.Text))
            {
                TANTOSHA_CD_FR = 0;
            }
            else
            {
                TANTOSHA_CD_FR = Util.ToDecimal(this.txtTantoshaCdFr.Text);
            }
            Decimal TANTOSHA_CD_TO;
            if (ValChk.IsEmpty(this.txtTantoshaCdTo.Text))
            {
                TANTOSHA_CD_TO = 999999;
            }
            else
            {
                TANTOSHA_CD_TO = Util.ToDecimal(this.txtTantoshaCdTo.Text);
            }

            // 商品CD
            Decimal SHOHIN_CD_FR;
            if (ValChk.IsEmpty(this.txtShohinCdFr.Text))
            {
                SHOHIN_CD_FR = 0;
            }
            else
            {
                SHOHIN_CD_FR = Util.ToDecimal(this.txtShohinCdFr.Text);
            }
            Decimal SHOHIN_CD_TO;
            if (ValChk.IsEmpty(this.txtShohinCdTo.Text))
            {
                SHOHIN_CD_TO = 999999;
            }
            else
            {
                SHOHIN_CD_TO = Util.ToDecimal(this.txtShohinCdTo.Text);
            }

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            //dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, DENPYO_DATE_FR);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, DENPYO_DATE_TO);
            dpc.SetParam("@KAIIN_BANGO_FR", SqlDbType.VarChar, 4, KAIIN_BANGO_FR);
            dpc.SetParam("@KAIIN_BANGO_TO", SqlDbType.VarChar, 4, KAIIN_BANGO_TO);
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 6, TANTOSHA_CD_FR);
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 6, TANTOSHA_CD_TO);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 15, SHOHIN_CD_FR);
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 15, SHOHIN_CD_TO);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 伝票金額と消費税を取引明細から取得するクエリ
            DbParamCollection dpc;
            StringBuilder sql = new StringBuilder();
            DataTable dtMeisaiResult;
            // Han.TB_取引明細(TB_HN_TORIHIKI_DENPYO)
            sql.Append("SELECT");
            sql.Append(" SUM(CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3");
            sql.Append("  THEN (A.BAIKA_KINGAKU - A.SHOHIZEI)");
            sql.Append("  ELSE A.BAIKA_KINGAKU");
            sql.Append("  END) AS 金額合計,");
            sql.Append(" SUM(A.SHOHIZEI) AS 消費税合計 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_TORIHIKI_MEISAI AS A ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND A.DENPYO_KUBUN = 1");
            sql.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" AND A.DENPYO_BANGO = @DENPYO_BANGO");

            // 日付を編集する際に用いるワーク
            string[] aryDate;
            string tmpDate;

            // 列の定義を作成
            dtResult.Columns.Add("伝票日付", typeof(string));
            dtResult.Columns.Add("伝票番号", typeof(int));
            dtResult.Columns.Add("船主ｺｰﾄﾞ", typeof(int));
            dtResult.Columns.Add("船主名", typeof(string));
            dtResult.Columns.Add("担当者", typeof(string));
            dtResult.Columns.Add("伝票金額", typeof(string));
            dtResult.Columns.Add("（消費税）", typeof(string));

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();

                aryDate = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["伝票日付"]), this.Dba);
                tmpDate = Util.ToInt(aryDate[2]).ToString("00") + "/" + aryDate[3].PadLeft(2, ' ') + "/" + aryDate[4].PadLeft(2, ' ');
                drResult["伝票日付"] = tmpDate;
                drResult["伝票番号"] = dtData.Rows[i]["伝票番号"];
                drResult["船主ｺｰﾄﾞ"] = dtData.Rows[i]["会員番号"];
                drResult["船主名"] = dtData.Rows[i]["会員名称"];
                drResult["担当者"] = dtData.Rows[i]["担当者名"];

                // 伝票金額と消費税を取引明細から取得する
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, dtData.Rows[i]["会計年度"]);
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, dtData.Rows[i]["伝票番号"]);
                dtMeisaiResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

                // 取引伝票の取引区分２が『2』は返品
                if (Util.ToDecimal(dtData.Rows[i]["取引区分２"]) == 2)
                {
                    drResult["伝票金額"] = Util.FormatNum(Util.ToDecimal(dtMeisaiResult.Rows[0]["金額合計"]) * -1);
                    drResult["（消費税）"] = Util.FormatNum(Util.ToDecimal(dtMeisaiResult.Rows[0]["消費税合計"]) * -1);
                }
                else
                {
                    drResult["伝票金額"] = Util.FormatNum(dtMeisaiResult.Rows[0]["金額合計"]);
                    drResult["（消費税）"] = Util.FormatNum(dtMeisaiResult.Rows[0]["消費税合計"]);
                }
                
                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }

        /// <summary>
        /// データ取得してグリッドに設定
        /// <param name="isInitial">初期処理であるかどうか</param>
        /// </summary>
        private void getListData(bool isInitial)
        {
            // データ取得のSQLを発行してGridに反映
            DataTable dtList = new DataTable();
            try
            {
                dtList = this.GetTB_HN_ZIDO_SHIWAKE_RIREKI(isInitial);
                if (!isInitial && dtList.Rows.Count == 0)
                {
                    // データが無い時
                    this.btnEnter.Enabled = false;
                    this.dgvList.Enabled = false;
                    Msg.Error("該当データがありません。");
                    return;
                }
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                return;
            }

            // データが有る時
            this.btnEnter.Enabled = true;
            this.dgvList.Enabled = true;

            // 取得したデータを表示用に編集
            DataTable dtDsp = this.EditDataList(dtList);

            this.dgvList.DataSource = dtDsp;
            if (!isInitial)
            {
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[1] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["伝票番号"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
