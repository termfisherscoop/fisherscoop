﻿namespace jp.co.sosok.erp.kob.kobe2011
{
    /// <summary>
    /// KOBE2013R の概要の説明です。
    /// </summary>
    partial class KOBE2013R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KOBE2013R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ラベル378 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.BackClrCg = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル340 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル359 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト389 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト332 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト351 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト370 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト231 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト342 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト361 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト380 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル203 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト258 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル196 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル201 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト204 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト205 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線206 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル207 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル208 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト209 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル210 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト211 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト212 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト213 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト215 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト217 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト218 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル219 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト220 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル221 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル222 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル223 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル224 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル225 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル226 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル227 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル228 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル229 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト230 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト232 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト233 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト234 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト235 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト236 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト237 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト238 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト331 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト333 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト334 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト335 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト336 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト337 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト338 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト339 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト341 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト343 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト344 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト345 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト346 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト347 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト348 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト349 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト350 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト352 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト353 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト354 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト355 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト356 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト357 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト358 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト360 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト362 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト363 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト364 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト365 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト366 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト367 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト368 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト369 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト371 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト372 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト373 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト374 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト375 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト376 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト377 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト379 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト381 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト382 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト383 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト384 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト385 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト386 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト387 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト388 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト390 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト391 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト392 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト393 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト394 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト395 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト396 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス309 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス310 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.テキスト311 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト312 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト313 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト314 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト315 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト318 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線407 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル414 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト415 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト416 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト417 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル418 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル420 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト422 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線423 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル424 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル425 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト426 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル427 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト428 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト429 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト430 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト431 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト432 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト433 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル434 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト435 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル535 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト642 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト643 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox81 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox83 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox69 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル378)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClrCg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル340)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル359)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト389)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト332)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト351)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト370)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト231)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト342)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト361)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト380)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル203)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト258)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル196)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル201)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト204)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト205)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル207)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル208)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト209)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル210)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト211)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト212)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト213)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト215)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト217)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト218)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル219)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト220)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル221)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル222)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル223)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル224)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル225)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル226)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル227)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル228)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル229)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト230)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト232)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト233)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト234)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト235)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト236)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト237)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト238)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト331)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト333)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト334)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト335)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト336)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト337)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト338)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト339)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト341)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト343)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト344)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト345)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト346)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト347)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト348)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト349)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト350)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト352)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト353)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト354)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト355)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト356)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト357)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト358)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト360)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト362)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト363)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト364)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト365)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト366)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト367)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト368)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト369)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト371)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト372)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト373)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト374)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト375)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト376)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト377)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト379)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト381)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト382)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト383)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト384)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト385)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト386)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト387)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト388)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト390)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト391)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト392)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト393)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト394)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト395)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト396)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト311)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト312)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト313)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト314)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト315)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト318)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル414)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト415)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト416)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト417)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル418)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル420)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト422)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル424)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル425)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト426)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル427)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト428)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト429)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト430)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト431)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト432)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト433)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル434)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト435)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル535)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト642)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト643)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル378,
            this.BackClrCg,
            this.ラベル340,
            this.ラベル359,
            this.テキスト389,
            this.テキスト332,
            this.テキスト351,
            this.テキスト370,
            this.テキスト231,
            this.テキスト342,
            this.テキスト361,
            this.テキスト380,
            this.ラベル203,
            this.テキスト258,
            this.ITEM02,
            this.ITEM01,
            this.ラベル71,
            this.ラベル196,
            this.ラベル201,
            this.テキスト204,
            this.テキスト205,
            this.直線206,
            this.ラベル207,
            this.ラベル208,
            this.テキスト209,
            this.ラベル210,
            this.テキスト211,
            this.テキスト212,
            this.テキスト213,
            this.テキスト215,
            this.テキスト217,
            this.テキスト218,
            this.ラベル219,
            this.テキスト220,
            this.ラベル221,
            this.ラベル222,
            this.ラベル223,
            this.ラベル224,
            this.ラベル225,
            this.ラベル226,
            this.ラベル227,
            this.ラベル228,
            this.ラベル229,
            this.テキスト230,
            this.テキスト232,
            this.テキスト233,
            this.テキスト234,
            this.テキスト235,
            this.テキスト236,
            this.テキスト237,
            this.テキスト238,
            this.テキスト331,
            this.テキスト333,
            this.テキスト334,
            this.テキスト335,
            this.テキスト336,
            this.テキスト337,
            this.テキスト338,
            this.テキスト339,
            this.テキスト341,
            this.テキスト343,
            this.テキスト344,
            this.テキスト345,
            this.テキスト346,
            this.テキスト347,
            this.テキスト348,
            this.テキスト349,
            this.テキスト350,
            this.テキスト352,
            this.テキスト353,
            this.テキスト354,
            this.テキスト355,
            this.テキスト356,
            this.テキスト357,
            this.テキスト358,
            this.テキスト360,
            this.テキスト362,
            this.テキスト363,
            this.テキスト364,
            this.テキスト365,
            this.テキスト366,
            this.テキスト367,
            this.テキスト368,
            this.テキスト369,
            this.テキスト371,
            this.テキスト372,
            this.テキスト373,
            this.テキスト374,
            this.テキスト375,
            this.テキスト376,
            this.テキスト377,
            this.テキスト379,
            this.テキスト381,
            this.テキスト382,
            this.テキスト383,
            this.テキスト384,
            this.テキスト385,
            this.テキスト386,
            this.テキスト387,
            this.テキスト388,
            this.テキスト390,
            this.テキスト391,
            this.テキスト392,
            this.テキスト393,
            this.テキスト394,
            this.テキスト395,
            this.テキスト396,
            this.ボックス309,
            this.ボックス310,
            this.テキスト311,
            this.テキスト312,
            this.テキスト313,
            this.テキスト314,
            this.テキスト315,
            this.テキスト316,
            this.テキスト317,
            this.テキスト318,
            this.直線407,
            this.ラベル414,
            this.テキスト415,
            this.テキスト416,
            this.テキスト417,
            this.ラベル418,
            this.ラベル420,
            this.テキスト422,
            this.直線423,
            this.ラベル424,
            this.ラベル425,
            this.テキスト426,
            this.ラベル427,
            this.テキスト428,
            this.テキスト429,
            this.テキスト430,
            this.テキスト431,
            this.テキスト432,
            this.テキスト433,
            this.ラベル434,
            this.テキスト435,
            this.ラベル535,
            this.テキスト642,
            this.テキスト643,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.shape10,
            this.shape11,
            this.textBox76,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox80,
            this.textBox81,
            this.textBox82,
            this.textBox83,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.label9,
            this.label10,
            this.label11,
            this.label12,
            this.label13,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox51,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.line1,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line2});
            this.detail.Height = 11.04449F;
            this.detail.Name = "detail";
            // 
            // ラベル378
            // 
            this.ラベル378.Height = 0.3354167F;
            this.ラベル378.HyperLink = null;
            this.ラベル378.Left = 0.1388888F;
            this.ラベル378.Name = "ラベル378";
            this.ラベル378.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.ラベル378.Tag = "";
            this.ラベル378.Text = "　";
            this.ラベル378.Top = 4.185091F;
            this.ラベル378.Width = 7.55F;
            // 
            // BackClrCg
            // 
            this.BackClrCg.Height = 0.3354167F;
            this.BackClrCg.HyperLink = null;
            this.BackClrCg.Left = 0.1388888F;
            this.BackClrCg.Name = "BackClrCg";
            this.BackClrCg.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.BackClrCg.Tag = "";
            this.BackClrCg.Text = "　";
            this.BackClrCg.Top = 2.16773F;
            this.BackClrCg.Width = 7.55F;
            // 
            // ラベル340
            // 
            this.ラベル340.Height = 0.3354167F;
            this.ラベル340.HyperLink = null;
            this.ラベル340.Left = 0.1388888F;
            this.ラベル340.Name = "ラベル340";
            this.ラベル340.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.ラベル340.Tag = "";
            this.ラベル340.Text = "　";
            this.ラベル340.Top = 2.853147F;
            this.ラベル340.Width = 7.55F;
            // 
            // ラベル359
            // 
            this.ラベル359.Height = 0.3361111F;
            this.ラベル359.HyperLink = null;
            this.ラベル359.Left = 0.1388888F;
            this.ラベル359.Name = "ラベル359";
            this.ラベル359.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.ラベル359.Tag = "";
            this.ラベル359.Text = "　";
            this.ラベル359.Top = 3.512175F;
            this.ラベル359.Width = 7.55F;
            // 
            // テキスト389
            // 
            this.テキスト389.DataField = "ITEM78";
            this.テキスト389.Height = 0.1875F;
            this.テキスト389.Left = 0.9256943F;
            this.テキスト389.Name = "テキスト389";
            this.テキスト389.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト389.Tag = "";
            this.テキスト389.Text = "ITEM78";
            this.テキスト389.Top = 4.265646F;
            this.テキスト389.Width = 2.385417F;
            // 
            // テキスト332
            // 
            this.テキスト332.DataField = "ITEM24";
            this.テキスト332.Height = 0.1805556F;
            this.テキスト332.Left = 0.9256943F;
            this.テキスト332.Name = "テキスト332";
            this.テキスト332.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト332.Tag = "";
            this.テキスト332.Text = "ITEM24";
            this.テキスト332.Top = 2.262175F;
            this.テキスト332.Width = 2.385417F;
            // 
            // テキスト351
            // 
            this.テキスト351.DataField = "ITEM42";
            this.テキスト351.Height = 0.1875F;
            this.テキスト351.Left = 0.9256943F;
            this.テキスト351.Name = "テキスト351";
            this.テキスト351.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト351.Tag = "";
            this.テキスト351.Text = "ITEM42";
            this.テキスト351.Top = 2.944813F;
            this.テキスト351.Width = 2.385417F;
            // 
            // テキスト370
            // 
            this.テキスト370.DataField = "ITEM60";
            this.テキスト370.Height = 0.1875F;
            this.テキスト370.Left = 0.9256943F;
            this.テキスト370.Name = "テキスト370";
            this.テキスト370.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト370.Tag = "";
            this.テキスト370.Text = "ITEM60";
            this.テキスト370.Top = 3.610786F;
            this.テキスト370.Width = 2.385417F;
            // 
            // テキスト231
            // 
            this.テキスト231.DataField = "ITEM15";
            this.テキスト231.Height = 0.1875F;
            this.テキスト231.Left = 0.9256943F;
            this.テキスト231.Name = "テキスト231";
            this.テキスト231.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト231.Tag = "";
            this.テキスト231.Text = "ITEM15";
            this.テキスト231.Top = 1.946897F;
            this.テキスト231.Width = 2.385417F;
            // 
            // テキスト342
            // 
            this.テキスト342.DataField = "ITEM33";
            this.テキスト342.Height = 0.1875F;
            this.テキスト342.Left = 0.9256943F;
            this.テキスト342.Name = "テキスト342";
            this.テキスト342.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト342.Tag = "";
            this.テキスト342.Text = "ITEM33";
            this.テキスト342.Top = 2.609397F;
            this.テキスト342.Width = 2.385417F;
            // 
            // テキスト361
            // 
            this.テキスト361.DataField = "ITEM51";
            this.テキスト361.Height = 0.1875F;
            this.テキスト361.Left = 0.9256943F;
            this.テキスト361.Name = "テキスト361";
            this.テキスト361.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト361.Tag = "";
            this.テキスト361.Text = "ITEM51";
            this.テキスト361.Top = 3.272591F;
            this.テキスト361.Width = 2.385417F;
            // 
            // テキスト380
            // 
            this.テキスト380.DataField = "ITEM69";
            this.テキスト380.Height = 0.1875F;
            this.テキスト380.Left = 0.9256943F;
            this.テキスト380.Name = "テキスト380";
            this.テキスト380.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト380.Tag = "";
            this.テキスト380.Text = "ITEM69";
            this.テキスト380.Top = 3.923286F;
            this.テキスト380.Width = 2.385417F;
            // 
            // ラベル203
            // 
            this.ラベル203.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル203.Height = 0.3951389F;
            this.ラベル203.HyperLink = null;
            this.ラベル203.Left = 3.098611F;
            this.ラベル203.Name = "ラベル203";
            this.ラベル203.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-weight: bold; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ラベル203.Tag = "";
            this.ラベル203.Text = "　";
            this.ラベル203.Top = 0.3087023F;
            this.ラベル203.Width = 1.811111F;
            // 
            // テキスト258
            // 
            this.テキスト258.DataField = "ITEM13";
            this.テキスト258.Height = 0.1875F;
            this.テキスト258.Left = 7.295139F;
            this.テキスト258.Name = "テキスト258";
            this.テキスト258.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト258.Tag = "";
            this.テキスト258.Text = "ITEM13";
            this.テキスト258.Top = 0.4962023F;
            this.テキスト258.Width = 0.3229167F;
            // 
            // ITEM02
            // 
            this.ITEM02.DataField = "ITEM02";
            this.ITEM02.Height = 0.1979167F;
            this.ITEM02.Left = 6.590972F;
            this.ITEM02.Name = "ITEM02";
            this.ITEM02.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ITEM02.Tag = "";
            this.ITEM02.Text = "ITEM02";
            this.ITEM02.Top = 0.5010635F;
            this.ITEM02.Width = 0.59375F;
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.1875F;
            this.ITEM01.Left = 6.444444F;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM01";
            this.ITEM01.Top = 0.3045356F;
            this.ITEM01.Width = 1.34375F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.1972222F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 7.60625F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "頁";
            this.ラベル71.Top = 0.5010635F;
            this.ラベル71.Width = 0.1715278F;
            // 
            // ラベル196
            // 
            this.ラベル196.Height = 0.2333333F;
            this.ラベル196.HyperLink = null;
            this.ラベル196.Left = 3.295139F;
            this.ラベル196.Name = "ラベル196";
            this.ラベル196.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt" +
    "; font-weight: normal; text-align: center; ddo-char-set: 128";
            this.ラベル196.Tag = "";
            this.ラベル196.Text = "領 収 書";
            this.ラベル196.Top = 0.3906467F;
            this.ラベル196.Width = 1.415278F;
            // 
            // ラベル201
            // 
            this.ラベル201.Height = 0.1979167F;
            this.ラベル201.HyperLink = null;
            this.ラベル201.Left = 5.722222F;
            this.ラベル201.Name = "ラベル201";
            this.ラベル201.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル201.Tag = "";
            this.ラベル201.Text = "伝票番号：";
            this.ラベル201.Top = 0.5010635F;
            this.ラベル201.Width = 0.8333333F;
            // 
            // テキスト204
            // 
            this.テキスト204.DataField = "ITEM03";
            this.テキスト204.Height = 0.1875F;
            this.テキスト204.Left = 0.1319443F;
            this.テキスト204.Name = "テキスト204";
            this.テキスト204.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト204.Tag = "";
            this.テキスト204.Text = "ITEM03";
            this.テキスト204.Top = 0.225369F;
            this.テキスト204.Width = 1.34375F;
            // 
            // テキスト205
            // 
            this.テキスト205.DataField = "ITEM04";
            this.テキスト205.Height = 0.1875F;
            this.テキスト205.Left = 0.1319443F;
            this.テキスト205.Name = "テキスト205";
            this.テキスト205.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: bold; text-a" +
    "lign: left; ddo-char-set: 128";
            this.テキスト205.Tag = "";
            this.テキスト205.Text = "ITEM04";
            this.テキスト205.Top = 1.009397F;
            this.テキスト205.Width = 2.375F;
            // 
            // 直線206
            // 
            this.直線206.Height = 0F;
            this.直線206.Left = 0.09722209F;
            this.直線206.LineWeight = 0F;
            this.直線206.Name = "直線206";
            this.直線206.Tag = "";
            this.直線206.Top = 1.204536F;
            this.直線206.Width = 2.710417F;
            this.直線206.X1 = 0.09722209F;
            this.直線206.X2 = 2.807639F;
            this.直線206.Y1 = 1.204536F;
            this.直線206.Y2 = 1.204536F;
            // 
            // ラベル207
            // 
            this.ラベル207.Height = 0.1972222F;
            this.ラベル207.HyperLink = null;
            this.ラベル207.Left = 2.534722F;
            this.ラベル207.Name = "ラベル207";
            this.ラベル207.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル207.Tag = "";
            this.ラベル207.Text = "様";
            this.ラベル207.Top = 1.006619F;
            this.ラベル207.Width = 0.2444444F;
            // 
            // ラベル208
            // 
            this.ラベル208.Height = 0.15625F;
            this.ラベル208.HyperLink = null;
            this.ラベル208.Left = 0.1319443F;
            this.ラベル208.Name = "ラベル208";
            this.ラベル208.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル208.Tag = "";
            this.ラベル208.Text = "TEL";
            this.ラベル208.Top = 1.245508F;
            this.ラベル208.Width = 0.2395833F;
            // 
            // テキスト209
            // 
            this.テキスト209.DataField = "ITEM05";
            this.テキスト209.Height = 0.15625F;
            this.テキスト209.Left = 0.3680554F;
            this.テキスト209.Name = "テキスト209";
            this.テキスト209.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.テキスト209.Tag = "";
            this.テキスト209.Text = "ITEM05";
            this.テキスト209.Top = 1.246202F;
            this.テキスト209.Width = 0.9479167F;
            // 
            // ラベル210
            // 
            this.ラベル210.Height = 0.15625F;
            this.ラベル210.HyperLink = null;
            this.ラベル210.Left = 1.315972F;
            this.ラベル210.Name = "ラベル210";
            this.ラベル210.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル210.Tag = "";
            this.ラベル210.Text = "FAX";
            this.ラベル210.Top = 1.241341F;
            this.ラベル210.Width = 0.2395833F;
            // 
            // テキスト211
            // 
            this.テキスト211.DataField = "ITEM06";
            this.テキスト211.Height = 0.15625F;
            this.テキスト211.Left = 1.552083F;
            this.テキスト211.Name = "テキスト211";
            this.テキスト211.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.テキスト211.Tag = "";
            this.テキスト211.Text = "ITEM06";
            this.テキスト211.Top = 1.242036F;
            this.テキスト211.Width = 0.9791667F;
            // 
            // テキスト212
            // 
            this.テキスト212.DataField = "ITEM07";
            this.テキスト212.Height = 0.1875F;
            this.テキスト212.Left = 5.565972F;
            this.テキスト212.Name = "テキスト212";
            this.テキスト212.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト212.Tag = "";
            this.テキスト212.Text = "ITEM07";
            this.テキスト212.Top = 0.7343967F;
            this.テキスト212.Width = 2.21875F;
            // 
            // テキスト213
            // 
            this.テキスト213.DataField = "ITEM08";
            this.テキスト213.Height = 0.1875F;
            this.テキスト213.Left = 5.565972F;
            this.テキスト213.Name = "テキスト213";
            this.テキスト213.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト213.Tag = "";
            this.テキスト213.Text = "ITEM08";
            this.テキスト213.Top = 0.9337022F;
            this.テキスト213.Width = 2.21875F;
            // 
            // テキスト215
            // 
            this.テキスト215.DataField = "ITEM09";
            this.テキスト215.Height = 0.15625F;
            this.テキスト215.Left = 5.565972F;
            this.テキスト215.Name = "テキスト215";
            this.テキスト215.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト215.Tag = "";
            this.テキスト215.Text = "ITEM09";
            this.テキスト215.Top = 1.121202F;
            this.テキスト215.Width = 2.21875F;
            // 
            // テキスト217
            // 
            this.テキスト217.DataField = "ITEM10";
            this.テキスト217.Height = 0.15625F;
            this.テキスト217.Left = 5.565972F;
            this.テキスト217.Name = "テキスト217";
            this.テキスト217.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト217.Tag = "";
            this.テキスト217.Text = "ITEM10";
            this.テキスト217.Top = 1.277452F;
            this.テキスト217.Width = 2.21875F;
            // 
            // テキスト218
            // 
            this.テキスト218.DataField = "ITEM11";
            this.テキスト218.Height = 0.1875F;
            this.テキスト218.Left = 3.097222F;
            this.テキスト218.Name = "テキスト218";
            this.テキスト218.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: center; ddo-char-set: 128";
            this.テキスト218.Tag = "";
            this.テキスト218.Text = "ITEM11";
            this.テキスト218.Top = 1.448286F;
            this.テキスト218.Width = 1.811111F;
            // 
            // ラベル219
            // 
            this.ラベル219.Height = 0.15625F;
            this.ラベル219.HyperLink = null;
            this.ラベル219.Left = 6.003472F;
            this.ラベル219.Name = "ラベル219";
            this.ラベル219.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル219.Tag = "";
            this.ラベル219.Text = "担当：";
            this.ラベル219.Top = 1.475369F;
            this.ラベル219.Width = 0.4791667F;
            // 
            // テキスト220
            // 
            this.テキスト220.DataField = "ITEM12";
            this.テキスト220.Height = 0.15625F;
            this.テキスト220.Left = 6.470139F;
            this.テキスト220.Name = "テキスト220";
            this.テキスト220.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト220.Tag = "";
            this.テキスト220.Text = "ITEM12";
            this.テキスト220.Top = 1.475369F;
            this.テキスト220.Width = 1.302083F;
            // 
            // ラベル221
            // 
            this.ラベル221.Height = 0.1979167F;
            this.ラベル221.HyperLink = null;
            this.ラベル221.Left = 0.1388888F;
            this.ラベル221.Name = "ラベル221";
            this.ラベル221.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル221.Tag = "";
            this.ラベル221.Text = "コード";
            this.ラベル221.Top = 1.673286F;
            this.ラベル221.Width = 0.7916667F;
            // 
            // ラベル222
            // 
            this.ラベル222.Height = 0.1979167F;
            this.ラベル222.HyperLink = null;
            this.ラベル222.Left = 0.8888887F;
            this.ラベル222.Name = "ラベル222";
            this.ラベル222.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 128";
            this.ラベル222.Tag = "";
            this.ラベル222.Text = "　商品名";
            this.ラベル222.Top = 1.673286F;
            this.ラベル222.Width = 2.489583F;
            // 
            // ラベル223
            // 
            this.ラベル223.Height = 0.1979167F;
            this.ラベル223.HyperLink = null;
            this.ラベル223.Left = 3.368055F;
            this.ラベル223.Name = "ラベル223";
            this.ラベル223.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル223.Tag = "";
            this.ラベル223.Text = "規　格";
            this.ラベル223.Top = 1.673286F;
            this.ラベル223.Width = 1.03125F;
            // 
            // ラベル224
            // 
            this.ラベル224.Height = 0.1979167F;
            this.ラベル224.HyperLink = null;
            this.ラベル224.Left = 4.386111F;
            this.ラベル224.Name = "ラベル224";
            this.ラベル224.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル224.Tag = "";
            this.ラベル224.Text = "単位";
            this.ラベル224.Top = 1.673286F;
            this.ラベル224.Width = 0.3125F;
            // 
            // ラベル225
            // 
            this.ラベル225.Height = 0.1979167F;
            this.ラベル225.HyperLink = null;
            this.ラベル225.Left = 4.700694F;
            this.ラベル225.Name = "ラベル225";
            this.ラベル225.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル225.Tag = "";
            this.ラベル225.Text = "入数";
            this.ラベル225.Top = 1.673286F;
            this.ラベル225.Width = 0.3541667F;
            // 
            // ラベル226
            // 
            this.ラベル226.Height = 0.1979167F;
            this.ラベル226.HyperLink = null;
            this.ラベル226.Left = 5.024305F;
            this.ラベル226.Name = "ラベル226";
            this.ラベル226.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル226.Tag = "";
            this.ラベル226.Text = "ケース";
            this.ラベル226.Top = 1.673286F;
            this.ラベル226.Width = 0.4052229F;
            // 
            // ラベル227
            // 
            this.ラベル227.Height = 0.1979167F;
            this.ラベル227.HyperLink = null;
            this.ラベル227.Left = 5.436615F;
            this.ラベル227.Name = "ラベル227";
            this.ラベル227.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル227.Tag = "";
            this.ラベル227.Text = "バラ";
            this.ラベル227.Top = 1.673229F;
            this.ラベル227.Width = 0.6109028F;
            // 
            // ラベル228
            // 
            this.ラベル228.Height = 0.1979167F;
            this.ラベル228.HyperLink = null;
            this.ラベル228.Left = 6.045139F;
            this.ラベル228.Name = "ラベル228";
            this.ラベル228.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル228.Tag = "";
            this.ラベル228.Text = "単　価";
            this.ラベル228.Top = 1.673286F;
            this.ラベル228.Width = 0.8229167F;
            // 
            // ラベル229
            // 
            this.ラベル229.Height = 0.1979167F;
            this.ラベル229.HyperLink = null;
            this.ラベル229.Left = 6.868055F;
            this.ラベル229.Name = "ラベル229";
            this.ラベル229.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル229.Tag = "";
            this.ラベル229.Text = "金　額";
            this.ラベル229.Top = 1.673286F;
            this.ラベル229.Width = 0.8229167F;
            // 
            // テキスト230
            // 
            this.テキスト230.DataField = "ITEM14";
            this.テキスト230.Height = 0.1875F;
            this.テキスト230.Left = 0.1701388F;
            this.テキスト230.Name = "テキスト230";
            this.テキスト230.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト230.Tag = "";
            this.テキスト230.Text = "ITEM14";
            this.テキスト230.Top = 1.946897F;
            this.テキスト230.Width = 0.6770833F;
            // 
            // テキスト232
            // 
            this.テキスト232.DataField = "ITEM16";
            this.テキスト232.Height = 0.1875F;
            this.テキスト232.Left = 3.399305F;
            this.テキスト232.Name = "テキスト232";
            this.テキスト232.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト232.Tag = "";
            this.テキスト232.Text = "ITEM16";
            this.テキスト232.Top = 1.946897F;
            this.テキスト232.Width = 0.9479167F;
            // 
            // テキスト233
            // 
            this.テキスト233.DataField = "ITEM17";
            this.テキスト233.Height = 0.1875F;
            this.テキスト233.Left = 4.430555F;
            this.テキスト233.Name = "テキスト233";
            this.テキスト233.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト233.Tag = "";
            this.テキスト233.Text = "ITEM17";
            this.テキスト233.Top = 1.946897F;
            this.テキスト233.Width = 0.2291667F;
            // 
            // テキスト234
            // 
            this.テキスト234.DataField = "ITEM18";
            this.テキスト234.Height = 0.1875F;
            this.テキスト234.Left = 4.743055F;
            this.テキスト234.Name = "テキスト234";
            this.テキスト234.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト234.Tag = "";
            this.テキスト234.Text = "ITEM18";
            this.テキスト234.Top = 1.946897F;
            this.テキスト234.Width = 0.2395833F;
            // 
            // テキスト235
            // 
            this.テキスト235.DataField = "ITEM19";
            this.テキスト235.Height = 0.1875F;
            this.テキスト235.Left = 5.055555F;
            this.テキスト235.Name = "テキスト235";
            this.テキスト235.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト235.Tag = "";
            this.テキスト235.Text = "ITEM19";
            this.テキスト235.Top = 1.946897F;
            this.テキスト235.Width = 0.3676737F;
            // 
            // テキスト236
            // 
            this.テキスト236.DataField = "ITEM20";
            this.テキスト236.Height = 0.1875F;
            this.テキスト236.Left = 5.448819F;
            this.テキスト236.Name = "テキスト236";
            this.テキスト236.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト236.Tag = "";
            this.テキスト236.Text = "ITEM20";
            this.テキスト236.Top = 1.946851F;
            this.テキスト236.Width = 0.5767716F;
            // 
            // テキスト237
            // 
            this.テキスト237.DataField = "ITEM21";
            this.テキスト237.Height = 0.1875F;
            this.テキスト237.Left = 6.079166F;
            this.テキスト237.Name = "テキスト237";
            this.テキスト237.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト237.Tag = "";
            this.テキスト237.Text = "ITEM21";
            this.テキスト237.Top = 1.946897F;
            this.テキスト237.Width = 0.7604167F;
            // 
            // テキスト238
            // 
            this.テキスト238.DataField = "ITEM22";
            this.テキスト238.Height = 0.1875F;
            this.テキスト238.Left = 6.909722F;
            this.テキスト238.Name = "テキスト238";
            this.テキスト238.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト238.Tag = "";
            this.テキスト238.Text = "ITEM22";
            this.テキスト238.Top = 1.946897F;
            this.テキスト238.Width = 0.7604167F;
            // 
            // テキスト331
            // 
            this.テキスト331.DataField = "ITEM23";
            this.テキスト331.Height = 0.1805556F;
            this.テキスト331.Left = 0.1652776F;
            this.テキスト331.Name = "テキスト331";
            this.テキスト331.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト331.Tag = "";
            this.テキスト331.Text = "ITEM23";
            this.テキスト331.Top = 2.262175F;
            this.テキスト331.Width = 0.6770833F;
            // 
            // テキスト333
            // 
            this.テキスト333.DataField = "ITEM25";
            this.テキスト333.Height = 0.1805556F;
            this.テキスト333.Left = 3.394444F;
            this.テキスト333.Name = "テキスト333";
            this.テキスト333.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト333.Tag = "";
            this.テキスト333.Text = "ITEM25";
            this.テキスト333.Top = 2.262175F;
            this.テキスト333.Width = 0.9479167F;
            // 
            // テキスト334
            // 
            this.テキスト334.DataField = "ITEM26";
            this.テキスト334.Height = 0.1805556F;
            this.テキスト334.Left = 4.425694F;
            this.テキスト334.Name = "テキスト334";
            this.テキスト334.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト334.Tag = "";
            this.テキスト334.Text = "ITEM26";
            this.テキスト334.Top = 2.262175F;
            this.テキスト334.Width = 0.2291667F;
            // 
            // テキスト335
            // 
            this.テキスト335.DataField = "ITEM27";
            this.テキスト335.Height = 0.1805556F;
            this.テキスト335.Left = 4.738194F;
            this.テキスト335.Name = "テキスト335";
            this.テキスト335.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト335.Tag = "";
            this.テキスト335.Text = "ITEM27";
            this.テキスト335.Top = 2.262175F;
            this.テキスト335.Width = 0.2395833F;
            // 
            // テキスト336
            // 
            this.テキスト336.DataField = "ITEM28";
            this.テキスト336.Height = 0.1805556F;
            this.テキスト336.Left = 5.030709F;
            this.テキスト336.Name = "テキスト336";
            this.テキスト336.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト336.Tag = "";
            this.テキスト336.Text = "ITEM28";
            this.テキスト336.Top = 2.262205F;
            this.テキスト336.Width = 0.3877953F;
            // 
            // テキスト337
            // 
            this.テキスト337.DataField = "ITEM29";
            this.テキスト337.Height = 0.1805556F;
            this.テキスト337.Left = 5.5625F;
            this.テキスト337.Name = "テキスト337";
            this.テキスト337.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト337.Tag = "";
            this.テキスト337.Text = "ITEM29";
            this.テキスト337.Top = 2.262175F;
            this.テキスト337.Width = 0.4270833F;
            // 
            // テキスト338
            // 
            this.テキスト338.DataField = "ITEM30";
            this.テキスト338.Height = 0.1805556F;
            this.テキスト338.Left = 6.074306F;
            this.テキスト338.Name = "テキスト338";
            this.テキスト338.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト338.Tag = "";
            this.テキスト338.Text = "ITEM30";
            this.テキスト338.Top = 2.262175F;
            this.テキスト338.Width = 0.7604167F;
            // 
            // テキスト339
            // 
            this.テキスト339.DataField = "ITEM31";
            this.テキスト339.Height = 0.1805556F;
            this.テキスト339.Left = 6.904861F;
            this.テキスト339.Name = "テキスト339";
            this.テキスト339.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト339.Tag = "";
            this.テキスト339.Text = "ITEM31";
            this.テキスト339.Top = 2.262175F;
            this.テキスト339.Width = 0.7604167F;
            // 
            // テキスト341
            // 
            this.テキスト341.DataField = "ITEM32";
            this.テキスト341.Height = 0.1875F;
            this.テキスト341.Left = 0.1701388F;
            this.テキスト341.Name = "テキスト341";
            this.テキスト341.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト341.Tag = "";
            this.テキスト341.Text = "ITEM32";
            this.テキスト341.Top = 2.609397F;
            this.テキスト341.Width = 0.6770833F;
            // 
            // テキスト343
            // 
            this.テキスト343.DataField = "ITEM34";
            this.テキスト343.Height = 0.1875F;
            this.テキスト343.Left = 3.399305F;
            this.テキスト343.Name = "テキスト343";
            this.テキスト343.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト343.Tag = "";
            this.テキスト343.Text = "ITEM34";
            this.テキスト343.Top = 2.609397F;
            this.テキスト343.Width = 0.9479167F;
            // 
            // テキスト344
            // 
            this.テキスト344.DataField = "ITEM35";
            this.テキスト344.Height = 0.1875F;
            this.テキスト344.Left = 4.430555F;
            this.テキスト344.Name = "テキスト344";
            this.テキスト344.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト344.Tag = "";
            this.テキスト344.Text = "ITEM35";
            this.テキスト344.Top = 2.609397F;
            this.テキスト344.Width = 0.2291667F;
            // 
            // テキスト345
            // 
            this.テキスト345.DataField = "ITEM36";
            this.テキスト345.Height = 0.1875F;
            this.テキスト345.Left = 4.743055F;
            this.テキスト345.Name = "テキスト345";
            this.テキスト345.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト345.Tag = "";
            this.テキスト345.Text = "ITEM36";
            this.テキスト345.Top = 2.609397F;
            this.テキスト345.Width = 0.2395833F;
            // 
            // テキスト346
            // 
            this.テキスト346.DataField = "ITEM37";
            this.テキスト346.Height = 0.1875F;
            this.テキスト346.Left = 5.055555F;
            this.テキスト346.Name = "テキスト346";
            this.テキスト346.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト346.Tag = "";
            this.テキスト346.Text = "ITEM37";
            this.テキスト346.Top = 2.609397F;
            this.テキスト346.Width = 0.3676737F;
            // 
            // テキスト347
            // 
            this.テキスト347.DataField = "ITEM38";
            this.テキスト347.Height = 0.1875F;
            this.テキスト347.Left = 5.448819F;
            this.テキスト347.Name = "テキスト347";
            this.テキスト347.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト347.Tag = "";
            this.テキスト347.Text = "ITEM38";
            this.テキスト347.Top = 2.609397F;
            this.テキスト347.Width = 0.5767716F;
            // 
            // テキスト348
            // 
            this.テキスト348.DataField = "ITEM39";
            this.テキスト348.Height = 0.1875F;
            this.テキスト348.Left = 6.079166F;
            this.テキスト348.Name = "テキスト348";
            this.テキスト348.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト348.Tag = "";
            this.テキスト348.Text = "ITEM39";
            this.テキスト348.Top = 2.609397F;
            this.テキスト348.Width = 0.7604167F;
            // 
            // テキスト349
            // 
            this.テキスト349.DataField = "ITEM40";
            this.テキスト349.Height = 0.1875F;
            this.テキスト349.Left = 6.909722F;
            this.テキスト349.Name = "テキスト349";
            this.テキスト349.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト349.Tag = "";
            this.テキスト349.Text = "ITEM40";
            this.テキスト349.Top = 2.609397F;
            this.テキスト349.Width = 0.7604167F;
            // 
            // テキスト350
            // 
            this.テキスト350.DataField = "ITEM41";
            this.テキスト350.Height = 0.1875F;
            this.テキスト350.Left = 0.1701388F;
            this.テキスト350.Name = "テキスト350";
            this.テキスト350.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト350.Tag = "";
            this.テキスト350.Text = "ITEM41";
            this.テキスト350.Top = 2.944813F;
            this.テキスト350.Width = 0.6770833F;
            // 
            // テキスト352
            // 
            this.テキスト352.DataField = "ITEM43";
            this.テキスト352.Height = 0.1875F;
            this.テキスト352.Left = 3.399305F;
            this.テキスト352.Name = "テキスト352";
            this.テキスト352.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト352.Tag = "";
            this.テキスト352.Text = "ITEM43";
            this.テキスト352.Top = 2.944813F;
            this.テキスト352.Width = 0.9479167F;
            // 
            // テキスト353
            // 
            this.テキスト353.DataField = "ITEM44";
            this.テキスト353.Height = 0.1875F;
            this.テキスト353.Left = 4.430555F;
            this.テキスト353.Name = "テキスト353";
            this.テキスト353.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト353.Tag = "";
            this.テキスト353.Text = "ITEM44";
            this.テキスト353.Top = 2.944813F;
            this.テキスト353.Width = 0.2291667F;
            // 
            // テキスト354
            // 
            this.テキスト354.DataField = "ITEM45";
            this.テキスト354.Height = 0.1875F;
            this.テキスト354.Left = 4.743055F;
            this.テキスト354.Name = "テキスト354";
            this.テキスト354.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト354.Tag = "";
            this.テキスト354.Text = "ITEM45";
            this.テキスト354.Top = 2.944813F;
            this.テキスト354.Width = 0.2395833F;
            // 
            // テキスト355
            // 
            this.テキスト355.DataField = "ITEM46";
            this.テキスト355.Height = 0.1805556F;
            this.テキスト355.Left = 5.030709F;
            this.テキスト355.Name = "テキスト355";
            this.テキスト355.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト355.Tag = "";
            this.テキスト355.Text = "ITEM46";
            this.テキスト355.Top = 2.944813F;
            this.テキスト355.Width = 0.3877953F;
            // 
            // テキスト356
            // 
            this.テキスト356.DataField = "ITEM47";
            this.テキスト356.Height = 0.1875F;
            this.テキスト356.Left = 5.567361F;
            this.テキスト356.Name = "テキスト356";
            this.テキスト356.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト356.Tag = "";
            this.テキスト356.Text = "ITEM47";
            this.テキスト356.Top = 2.944813F;
            this.テキスト356.Width = 0.4270833F;
            // 
            // テキスト357
            // 
            this.テキスト357.DataField = "ITEM48";
            this.テキスト357.Height = 0.1875F;
            this.テキスト357.Left = 6.079166F;
            this.テキスト357.Name = "テキスト357";
            this.テキスト357.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト357.Tag = "";
            this.テキスト357.Text = "ITEM48";
            this.テキスト357.Top = 2.944813F;
            this.テキスト357.Width = 0.7604167F;
            // 
            // テキスト358
            // 
            this.テキスト358.DataField = "ITEM49";
            this.テキスト358.Height = 0.1875F;
            this.テキスト358.Left = 6.909722F;
            this.テキスト358.Name = "テキスト358";
            this.テキスト358.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト358.Tag = "";
            this.テキスト358.Text = "ITEM49";
            this.テキスト358.Top = 2.944813F;
            this.テキスト358.Width = 0.7604167F;
            // 
            // テキスト360
            // 
            this.テキスト360.DataField = "ITEM50";
            this.テキスト360.Height = 0.1875F;
            this.テキスト360.Left = 0.1701388F;
            this.テキスト360.Name = "テキスト360";
            this.テキスト360.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト360.Tag = "";
            this.テキスト360.Text = "ITEM50";
            this.テキスト360.Top = 3.272591F;
            this.テキスト360.Width = 0.6770833F;
            // 
            // テキスト362
            // 
            this.テキスト362.DataField = "ITEM52";
            this.テキスト362.Height = 0.1875F;
            this.テキスト362.Left = 3.399305F;
            this.テキスト362.Name = "テキスト362";
            this.テキスト362.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト362.Tag = "";
            this.テキスト362.Text = "ITEM52";
            this.テキスト362.Top = 3.272591F;
            this.テキスト362.Width = 0.9479167F;
            // 
            // テキスト363
            // 
            this.テキスト363.DataField = "ITEM53";
            this.テキスト363.Height = 0.1875F;
            this.テキスト363.Left = 4.430555F;
            this.テキスト363.Name = "テキスト363";
            this.テキスト363.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト363.Tag = "";
            this.テキスト363.Text = "ITEM53";
            this.テキスト363.Top = 3.272591F;
            this.テキスト363.Width = 0.2291667F;
            // 
            // テキスト364
            // 
            this.テキスト364.DataField = "ITEM54";
            this.テキスト364.Height = 0.1875F;
            this.テキスト364.Left = 4.743055F;
            this.テキスト364.Name = "テキスト364";
            this.テキスト364.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト364.Tag = "";
            this.テキスト364.Text = "ITEM54";
            this.テキスト364.Top = 3.272591F;
            this.テキスト364.Width = 0.2395833F;
            // 
            // テキスト365
            // 
            this.テキスト365.DataField = "ITEM55";
            this.テキスト365.Height = 0.1875F;
            this.テキスト365.Left = 5.055555F;
            this.テキスト365.Name = "テキスト365";
            this.テキスト365.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト365.Tag = "";
            this.テキスト365.Text = "ITEM55";
            this.テキスト365.Top = 3.272591F;
            this.テキスト365.Width = 0.3676737F;
            // 
            // テキスト366
            // 
            this.テキスト366.DataField = "ITEM56";
            this.テキスト366.Height = 0.1875F;
            this.テキスト366.Left = 5.448819F;
            this.テキスト366.Name = "テキスト366";
            this.テキスト366.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト366.Tag = "";
            this.テキスト366.Text = "ITEM56";
            this.テキスト366.Top = 3.272591F;
            this.テキスト366.Width = 0.5767716F;
            // 
            // テキスト367
            // 
            this.テキスト367.DataField = "ITEM57";
            this.テキスト367.Height = 0.1875F;
            this.テキスト367.Left = 6.079166F;
            this.テキスト367.Name = "テキスト367";
            this.テキスト367.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト367.Tag = "";
            this.テキスト367.Text = "ITEM57";
            this.テキスト367.Top = 3.272591F;
            this.テキスト367.Width = 0.7604167F;
            // 
            // テキスト368
            // 
            this.テキスト368.DataField = "ITEM58";
            this.テキスト368.Height = 0.1875F;
            this.テキスト368.Left = 6.909722F;
            this.テキスト368.Name = "テキスト368";
            this.テキスト368.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト368.Tag = "";
            this.テキスト368.Text = "ITEM58";
            this.テキスト368.Top = 3.272591F;
            this.テキスト368.Width = 0.7604167F;
            // 
            // テキスト369
            // 
            this.テキスト369.DataField = "ITEM59";
            this.テキスト369.Height = 0.1875F;
            this.テキスト369.Left = 0.1701388F;
            this.テキスト369.Name = "テキスト369";
            this.テキスト369.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト369.Tag = "";
            this.テキスト369.Text = "ITEM59";
            this.テキスト369.Top = 3.610786F;
            this.テキスト369.Width = 0.6770833F;
            // 
            // テキスト371
            // 
            this.テキスト371.DataField = "ITEM61";
            this.テキスト371.Height = 0.1875F;
            this.テキスト371.Left = 3.399305F;
            this.テキスト371.Name = "テキスト371";
            this.テキスト371.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト371.Tag = "";
            this.テキスト371.Text = "ITEM61";
            this.テキスト371.Top = 3.610786F;
            this.テキスト371.Width = 0.9479167F;
            // 
            // テキスト372
            // 
            this.テキスト372.DataField = "ITEM62";
            this.テキスト372.Height = 0.1875F;
            this.テキスト372.Left = 4.430555F;
            this.テキスト372.Name = "テキスト372";
            this.テキスト372.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト372.Tag = "";
            this.テキスト372.Text = "ITEM62";
            this.テキスト372.Top = 3.610786F;
            this.テキスト372.Width = 0.2291667F;
            // 
            // テキスト373
            // 
            this.テキスト373.DataField = "ITEM63";
            this.テキスト373.Height = 0.1875F;
            this.テキスト373.Left = 4.743055F;
            this.テキスト373.Name = "テキスト373";
            this.テキスト373.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト373.Tag = "";
            this.テキスト373.Text = "ITEM63";
            this.テキスト373.Top = 3.610786F;
            this.テキスト373.Width = 0.2395833F;
            // 
            // テキスト374
            // 
            this.テキスト374.DataField = "ITEM64";
            this.テキスト374.Height = 0.1805556F;
            this.テキスト374.Left = 5.030709F;
            this.テキスト374.Name = "テキスト374";
            this.テキスト374.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト374.Tag = "";
            this.テキスト374.Text = "ITEM64";
            this.テキスト374.Top = 3.610786F;
            this.テキスト374.Width = 0.3877953F;
            // 
            // テキスト375
            // 
            this.テキスト375.DataField = "ITEM65";
            this.テキスト375.Height = 0.1875F;
            this.テキスト375.Left = 5.567361F;
            this.テキスト375.Name = "テキスト375";
            this.テキスト375.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト375.Tag = "";
            this.テキスト375.Text = "ITEM65";
            this.テキスト375.Top = 3.610786F;
            this.テキスト375.Width = 0.4270833F;
            // 
            // テキスト376
            // 
            this.テキスト376.DataField = "ITEM66";
            this.テキスト376.Height = 0.1875F;
            this.テキスト376.Left = 6.079166F;
            this.テキスト376.Name = "テキスト376";
            this.テキスト376.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト376.Tag = "";
            this.テキスト376.Text = "ITEM66";
            this.テキスト376.Top = 3.610786F;
            this.テキスト376.Width = 0.7604167F;
            // 
            // テキスト377
            // 
            this.テキスト377.DataField = "ITEM67";
            this.テキスト377.Height = 0.1875F;
            this.テキスト377.Left = 6.909722F;
            this.テキスト377.Name = "テキスト377";
            this.テキスト377.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト377.Tag = "";
            this.テキスト377.Text = "ITEM67";
            this.テキスト377.Top = 3.610786F;
            this.テキスト377.Width = 0.7604167F;
            // 
            // テキスト379
            // 
            this.テキスト379.DataField = "ITEM68";
            this.テキスト379.Height = 0.1875F;
            this.テキスト379.Left = 0.1701388F;
            this.テキスト379.Name = "テキスト379";
            this.テキスト379.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト379.Tag = "";
            this.テキスト379.Text = "ITEM68";
            this.テキスト379.Top = 3.923286F;
            this.テキスト379.Width = 0.6770833F;
            // 
            // テキスト381
            // 
            this.テキスト381.DataField = "ITEM70";
            this.テキスト381.Height = 0.1875F;
            this.テキスト381.Left = 3.399305F;
            this.テキスト381.Name = "テキスト381";
            this.テキスト381.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト381.Tag = "";
            this.テキスト381.Text = "ITEM70";
            this.テキスト381.Top = 3.923286F;
            this.テキスト381.Width = 0.9479167F;
            // 
            // テキスト382
            // 
            this.テキスト382.DataField = "ITEM71";
            this.テキスト382.Height = 0.1875F;
            this.テキスト382.Left = 4.430555F;
            this.テキスト382.Name = "テキスト382";
            this.テキスト382.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト382.Tag = "";
            this.テキスト382.Text = "ITEM71";
            this.テキスト382.Top = 3.923286F;
            this.テキスト382.Width = 0.2291667F;
            // 
            // テキスト383
            // 
            this.テキスト383.DataField = "ITEM72";
            this.テキスト383.Height = 0.1875F;
            this.テキスト383.Left = 4.743055F;
            this.テキスト383.Name = "テキスト383";
            this.テキスト383.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト383.Tag = "";
            this.テキスト383.Text = "ITEM72";
            this.テキスト383.Top = 3.923286F;
            this.テキスト383.Width = 0.2395833F;
            // 
            // テキスト384
            // 
            this.テキスト384.DataField = "ITEM73";
            this.テキスト384.Height = 0.1875F;
            this.テキスト384.Left = 5.055555F;
            this.テキスト384.Name = "テキスト384";
            this.テキスト384.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト384.Tag = "";
            this.テキスト384.Text = "ITEM73";
            this.テキスト384.Top = 3.923286F;
            this.テキスト384.Width = 0.3676737F;
            // 
            // テキスト385
            // 
            this.テキスト385.DataField = "ITEM74";
            this.テキスト385.Height = 0.1875F;
            this.テキスト385.Left = 5.448819F;
            this.テキスト385.Name = "テキスト385";
            this.テキスト385.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト385.Tag = "";
            this.テキスト385.Text = "ITEM74";
            this.テキスト385.Top = 3.923286F;
            this.テキスト385.Width = 0.5767716F;
            // 
            // テキスト386
            // 
            this.テキスト386.DataField = "ITEM75";
            this.テキスト386.Height = 0.1875F;
            this.テキスト386.Left = 6.079166F;
            this.テキスト386.Name = "テキスト386";
            this.テキスト386.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト386.Tag = "";
            this.テキスト386.Text = "ITEM75";
            this.テキスト386.Top = 3.923286F;
            this.テキスト386.Width = 0.7604167F;
            // 
            // テキスト387
            // 
            this.テキスト387.DataField = "ITEM76";
            this.テキスト387.Height = 0.1875F;
            this.テキスト387.Left = 6.909722F;
            this.テキスト387.Name = "テキスト387";
            this.テキスト387.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト387.Tag = "";
            this.テキスト387.Text = "ITEM76";
            this.テキスト387.Top = 3.923286F;
            this.テキスト387.Width = 0.7604167F;
            // 
            // テキスト388
            // 
            this.テキスト388.DataField = "ITEM77";
            this.テキスト388.Height = 0.1875F;
            this.テキスト388.Left = 0.1701388F;
            this.テキスト388.Name = "テキスト388";
            this.テキスト388.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト388.Tag = "";
            this.テキスト388.Text = "ITEM77";
            this.テキスト388.Top = 4.265646F;
            this.テキスト388.Width = 0.6770833F;
            // 
            // テキスト390
            // 
            this.テキスト390.DataField = "ITEM79";
            this.テキスト390.Height = 0.1875F;
            this.テキスト390.Left = 3.399305F;
            this.テキスト390.Name = "テキスト390";
            this.テキスト390.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト390.Tag = "";
            this.テキスト390.Text = "ITEM79";
            this.テキスト390.Top = 4.265646F;
            this.テキスト390.Width = 0.9479167F;
            // 
            // テキスト391
            // 
            this.テキスト391.DataField = "ITEM80";
            this.テキスト391.Height = 0.1875F;
            this.テキスト391.Left = 4.430555F;
            this.テキスト391.Name = "テキスト391";
            this.テキスト391.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト391.Tag = "";
            this.テキスト391.Text = "ITEM80";
            this.テキスト391.Top = 4.265646F;
            this.テキスト391.Width = 0.2291667F;
            // 
            // テキスト392
            // 
            this.テキスト392.DataField = "ITEM81";
            this.テキスト392.Height = 0.1875F;
            this.テキスト392.Left = 4.743055F;
            this.テキスト392.Name = "テキスト392";
            this.テキスト392.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト392.Tag = "";
            this.テキスト392.Text = "ITEM81";
            this.テキスト392.Top = 4.265646F;
            this.テキスト392.Width = 0.2395833F;
            // 
            // テキスト393
            // 
            this.テキスト393.DataField = "ITEM82";
            this.テキスト393.Height = 0.1805556F;
            this.テキスト393.Left = 5.030709F;
            this.テキスト393.Name = "テキスト393";
            this.テキスト393.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト393.Tag = "";
            this.テキスト393.Text = "ITEM82";
            this.テキスト393.Top = 4.265646F;
            this.テキスト393.Width = 0.3877953F;
            // 
            // テキスト394
            // 
            this.テキスト394.DataField = "ITEM83";
            this.テキスト394.Height = 0.1875F;
            this.テキスト394.Left = 5.567361F;
            this.テキスト394.Name = "テキスト394";
            this.テキスト394.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト394.Tag = "";
            this.テキスト394.Text = "ITEM83";
            this.テキスト394.Top = 4.265646F;
            this.テキスト394.Width = 0.4270833F;
            // 
            // テキスト395
            // 
            this.テキスト395.DataField = "ITEM84";
            this.テキスト395.Height = 0.1875F;
            this.テキスト395.Left = 6.079166F;
            this.テキスト395.Name = "テキスト395";
            this.テキスト395.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト395.Tag = "";
            this.テキスト395.Text = "ITEM84";
            this.テキスト395.Top = 4.265646F;
            this.テキスト395.Width = 0.7604167F;
            // 
            // テキスト396
            // 
            this.テキスト396.DataField = "ITEM85";
            this.テキスト396.Height = 0.1875F;
            this.テキスト396.Left = 6.909722F;
            this.テキスト396.Name = "テキスト396";
            this.テキスト396.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト396.Tag = "";
            this.テキスト396.Text = "ITEM85";
            this.テキスト396.Top = 4.265646F;
            this.テキスト396.Width = 0.7604167F;
            // 
            // ボックス309
            // 
            this.ボックス309.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス309.Height = 0.34375F;
            this.ボックス309.Left = 3.785433F;
            this.ボックス309.Name = "ボックス309";
            this.ボックス309.RoundingRadius = 9.999999F;
            this.ボックス309.Tag = "";
            this.ボックス309.Top = 4.881619F;
            this.ボックス309.Width = 3.915956F;
            // 
            // ボックス310
            // 
            this.ボックス310.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス310.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Height = 0.2395833F;
            this.ボックス310.Left = 3.785433F;
            this.ボックス310.Name = "ボックス310";
            this.ボックス310.RoundingRadius = 9.999999F;
            this.ボックス310.Tag = "";
            this.ボックス310.Top = 4.642035F;
            this.ボックス310.Width = 3.915261F;
            // 
            // テキスト311
            // 
            this.テキスト311.DataField = "ITEM86";
            this.テキスト311.Height = 0.1875F;
            this.テキスト311.Left = 3.840551F;
            this.テキスト311.Name = "テキスト311";
            this.テキスト311.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト311.Tag = "";
            this.テキスト311.Text = "ITEM86";
            this.テキスト311.Top = 4.683858F;
            this.テキスト311.Width = 1.000394F;
            // 
            // テキスト312
            // 
            this.テキスト312.DataField = "ITEM87";
            this.テキスト312.Height = 0.1875F;
            this.テキスト312.Left = 4.929528F;
            this.テキスト312.Name = "テキスト312";
            this.テキスト312.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト312.Tag = "";
            this.テキスト312.Text = "ITEM87";
            this.テキスト312.Top = 4.683858F;
            this.テキスト312.Width = 0.8763781F;
            // 
            // テキスト313
            // 
            this.テキスト313.DataField = "ITEM88";
            this.テキスト313.Height = 0.1875F;
            this.テキスト313.Left = 5.954861F;
            this.テキスト313.Name = "テキスト313";
            this.テキスト313.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト313.Tag = "";
            this.テキスト313.Text = "ITEM88";
            this.テキスト313.Top = 4.683702F;
            this.テキスト313.Width = 0.7916667F;
            // 
            // テキスト314
            // 
            this.テキスト314.DataField = "ITEM89";
            this.テキスト314.Height = 0.1875F;
            this.テキスト314.Left = 6.864583F;
            this.テキスト314.Name = "テキスト314";
            this.テキスト314.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト314.Tag = "";
            this.テキスト314.Text = "ITEM89";
            this.テキスト314.Top = 4.683702F;
            this.テキスト314.Width = 0.7916667F;
            // 
            // テキスト315
            // 
            this.テキスト315.DataField = "ITEM90";
            this.テキスト315.Height = 0.1875F;
            this.テキスト315.Left = 3.840551F;
            this.テキスト315.Name = "テキスト315";
            this.テキスト315.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト315.Tag = "";
            this.テキスト315.Text = "ITEM90";
            this.テキスト315.Top = 4.954725F;
            this.テキスト315.Width = 1.000394F;
            // 
            // テキスト316
            // 
            this.テキスト316.DataField = "ITEM91";
            this.テキスト316.Height = 0.1875F;
            this.テキスト316.Left = 4.929528F;
            this.テキスト316.Name = "テキスト316";
            this.テキスト316.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト316.Tag = "";
            this.テキスト316.Text = "ITEM91";
            this.テキスト316.Top = 4.954725F;
            this.テキスト316.Width = 0.8763781F;
            // 
            // テキスト317
            // 
            this.テキスト317.DataField = "ITEM92";
            this.テキスト317.Height = 0.1875F;
            this.テキスト317.Left = 5.954861F;
            this.テキスト317.Name = "テキスト317";
            this.テキスト317.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト317.Tag = "";
            this.テキスト317.Text = "ITEM92";
            this.テキスト317.Top = 4.954535F;
            this.テキスト317.Width = 0.7916667F;
            // 
            // テキスト318
            // 
            this.テキスト318.DataField = "ITEM93";
            this.テキスト318.Height = 0.1875F;
            this.テキスト318.Left = 6.864583F;
            this.テキスト318.Name = "テキスト318";
            this.テキスト318.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト318.Tag = "";
            this.テキスト318.Text = "ITEM93";
            this.テキスト318.Top = 4.954535F;
            this.テキスト318.Width = 0.7916667F;
            // 
            // 直線407
            // 
            this.直線407.Height = 0F;
            this.直線407.Left = 0.1388888F;
            this.直線407.LineWeight = 0F;
            this.直線407.Name = "直線407";
            this.直線407.Tag = "";
            this.直線407.Top = 1.881619F;
            this.直線407.Width = 7.559722F;
            this.直線407.X1 = 0.1388888F;
            this.直線407.X2 = 7.698611F;
            this.直線407.Y1 = 1.881619F;
            this.直線407.Y2 = 1.881619F;
            // 
            // ラベル414
            // 
            this.ラベル414.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル414.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル414.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル414.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル414.Height = 0.3951389F;
            this.ラベル414.HyperLink = null;
            this.ラベル414.Left = 3.098611F;
            this.ラベル414.Name = "ラベル414";
            this.ラベル414.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-weight: bold; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ラベル414.Tag = "";
            this.ラベル414.Text = "　";
            this.ラベル414.Top = 5.902452F;
            this.ラベル414.Width = 1.811111F;
            // 
            // テキスト415
            // 
            this.テキスト415.DataField = "ITEM13";
            this.テキスト415.Height = 0.1875F;
            this.テキスト415.Left = 7.295139F;
            this.テキスト415.Name = "テキスト415";
            this.テキスト415.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト415.Tag = "";
            this.テキスト415.Text = "ITEM13";
            this.テキスト415.Top = 6.094813F;
            this.テキスト415.Width = 0.3229167F;
            // 
            // テキスト416
            // 
            this.テキスト416.DataField = "ITEM02";
            this.テキスト416.Height = 0.1979167F;
            this.テキスト416.Left = 6.590972F;
            this.テキスト416.Name = "テキスト416";
            this.テキスト416.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト416.Tag = "";
            this.テキスト416.Text = "ITEM02";
            this.テキスト416.Top = 6.094813F;
            this.テキスト416.Width = 0.59375F;
            // 
            // テキスト417
            // 
            this.テキスト417.DataField = "ITEM01";
            this.テキスト417.Height = 0.1875F;
            this.テキスト417.Left = 6.444444F;
            this.テキスト417.Name = "テキスト417";
            this.テキスト417.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト417.Tag = "";
            this.テキスト417.Text = "ITEM01";
            this.テキスト417.Top = 5.898285F;
            this.テキスト417.Width = 1.34375F;
            // 
            // ラベル418
            // 
            this.ラベル418.Height = 0.1972222F;
            this.ラベル418.HyperLink = null;
            this.ラベル418.Left = 7.60625F;
            this.ラベル418.Name = "ラベル418";
            this.ラベル418.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル418.Tag = "";
            this.ラベル418.Text = "頁";
            this.ラベル418.Top = 6.094813F;
            this.ラベル418.Width = 0.1715278F;
            // 
            // ラベル420
            // 
            this.ラベル420.Height = 0.1979167F;
            this.ラベル420.HyperLink = null;
            this.ラベル420.Left = 5.722222F;
            this.ラベル420.Name = "ラベル420";
            this.ラベル420.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル420.Tag = "";
            this.ラベル420.Text = "伝票番号：";
            this.ラベル420.Top = 6.094813F;
            this.ラベル420.Width = 0.8333333F;
            // 
            // テキスト422
            // 
            this.テキスト422.DataField = "ITEM04";
            this.テキスト422.Height = 0.1875F;
            this.テキスト422.Left = 0.1319443F;
            this.テキスト422.Name = "テキスト422";
            this.テキスト422.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: bold; text-a" +
    "lign: left; ddo-char-set: 128";
            this.テキスト422.Tag = "";
            this.テキスト422.Text = "ITEM04";
            this.テキスト422.Top = 6.603147F;
            this.テキスト422.Width = 2.375F;
            // 
            // 直線423
            // 
            this.直線423.Height = 0F;
            this.直線423.Left = 0.09722209F;
            this.直線423.LineWeight = 0F;
            this.直線423.Name = "直線423";
            this.直線423.Tag = "";
            this.直線423.Top = 6.798285F;
            this.直線423.Width = 2.710417F;
            this.直線423.X1 = 0.09722209F;
            this.直線423.X2 = 2.807639F;
            this.直線423.Y1 = 6.798285F;
            this.直線423.Y2 = 6.798285F;
            // 
            // ラベル424
            // 
            this.ラベル424.Height = 0.1972222F;
            this.ラベル424.HyperLink = null;
            this.ラベル424.Left = 2.534722F;
            this.ラベル424.Name = "ラベル424";
            this.ラベル424.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル424.Tag = "";
            this.ラベル424.Text = "様";
            this.ラベル424.Top = 6.600369F;
            this.ラベル424.Width = 0.2444444F;
            // 
            // ラベル425
            // 
            this.ラベル425.Height = 0.15625F;
            this.ラベル425.HyperLink = null;
            this.ラベル425.Left = 0.1319443F;
            this.ラベル425.Name = "ラベル425";
            this.ラベル425.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル425.Tag = "";
            this.ラベル425.Text = "TEL";
            this.ラベル425.Top = 6.835091F;
            this.ラベル425.Width = 0.2395833F;
            // 
            // テキスト426
            // 
            this.テキスト426.DataField = "ITEM05";
            this.テキスト426.Height = 0.15625F;
            this.テキスト426.Left = 0.3680554F;
            this.テキスト426.Name = "テキスト426";
            this.テキスト426.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.テキスト426.Tag = "";
            this.テキスト426.Text = "ITEM05";
            this.テキスト426.Top = 6.839952F;
            this.テキスト426.Width = 0.9479167F;
            // 
            // ラベル427
            // 
            this.ラベル427.Height = 0.15625F;
            this.ラベル427.HyperLink = null;
            this.ラベル427.Left = 1.315972F;
            this.ラベル427.Name = "ラベル427";
            this.ラベル427.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル427.Tag = "";
            this.ラベル427.Text = "FAX";
            this.ラベル427.Top = 6.835091F;
            this.ラベル427.Width = 0.2395833F;
            // 
            // テキスト428
            // 
            this.テキスト428.DataField = "ITEM06";
            this.テキスト428.Height = 0.15625F;
            this.テキスト428.Left = 1.552083F;
            this.テキスト428.Name = "テキスト428";
            this.テキスト428.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.テキスト428.Tag = "";
            this.テキスト428.Text = "ITEM06";
            this.テキスト428.Top = 6.835785F;
            this.テキスト428.Width = 0.9791667F;
            // 
            // テキスト429
            // 
            this.テキスト429.DataField = "ITEM07";
            this.テキスト429.Height = 0.1875F;
            this.テキスト429.Left = 5.565972F;
            this.テキスト429.Name = "テキスト429";
            this.テキスト429.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト429.Tag = "";
            this.テキスト429.Text = "ITEM07";
            this.テキスト429.Top = 6.328146F;
            this.テキスト429.Width = 2.21875F;
            // 
            // テキスト430
            // 
            this.テキスト430.DataField = "ITEM08";
            this.テキスト430.Height = 0.1875F;
            this.テキスト430.Left = 5.565972F;
            this.テキスト430.Name = "テキスト430";
            this.テキスト430.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト430.Tag = "";
            this.テキスト430.Text = "ITEM08";
            this.テキスト430.Top = 6.527452F;
            this.テキスト430.Width = 2.21875F;
            // 
            // テキスト431
            // 
            this.テキスト431.DataField = "ITEM09";
            this.テキスト431.Height = 0.15625F;
            this.テキスト431.Left = 5.565972F;
            this.テキスト431.Name = "テキスト431";
            this.テキスト431.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト431.Tag = "";
            this.テキスト431.Text = "ITEM09";
            this.テキスト431.Top = 6.714952F;
            this.テキスト431.Width = 2.21875F;
            // 
            // テキスト432
            // 
            this.テキスト432.DataField = "ITEM10";
            this.テキスト432.Height = 0.15625F;
            this.テキスト432.Left = 5.565972F;
            this.テキスト432.Name = "テキスト432";
            this.テキスト432.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト432.Tag = "";
            this.テキスト432.Text = "ITEM10";
            this.テキスト432.Top = 6.871202F;
            this.テキスト432.Width = 2.21875F;
            // 
            // テキスト433
            // 
            this.テキスト433.DataField = "ITEM11";
            this.テキスト433.Height = 0.1875F;
            this.テキスト433.Left = 3.097222F;
            this.テキスト433.Name = "テキスト433";
            this.テキスト433.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: center; ddo-char-set: 128";
            this.テキスト433.Tag = "";
            this.テキスト433.Text = "ITEM11";
            this.テキスト433.Top = 7.042036F;
            this.テキスト433.Width = 1.811111F;
            // 
            // ラベル434
            // 
            this.ラベル434.Height = 0.15625F;
            this.ラベル434.HyperLink = null;
            this.ラベル434.Left = 6.003472F;
            this.ラベル434.Name = "ラベル434";
            this.ラベル434.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.ラベル434.Tag = "";
            this.ラベル434.Text = "担当：";
            this.ラベル434.Top = 7.069119F;
            this.ラベル434.Width = 0.4791667F;
            // 
            // テキスト435
            // 
            this.テキスト435.DataField = "ITEM12";
            this.テキスト435.Height = 0.15625F;
            this.テキスト435.Left = 6.470139F;
            this.テキスト435.Name = "テキスト435";
            this.テキスト435.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト435.Tag = "";
            this.テキスト435.Text = "ITEM12";
            this.テキスト435.Top = 7.069119F;
            this.テキスト435.Width = 1.302083F;
            // 
            // ラベル535
            // 
            this.ラベル535.Height = 0.2291667F;
            this.ラベル535.HyperLink = null;
            this.ラベル535.Left = 3.194489F;
            this.ラベル535.Name = "ラベル535";
            this.ラベル535.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt" +
    "; font-weight: normal; text-align: center; ddo-char-set: 128";
            this.ラベル535.Tag = "";
            this.ラベル535.Text = "領収書 (控え)";
            this.ラベル535.Top = 5.980925F;
            this.ラベル535.Width = 1.614567F;
            // 
            // テキスト642
            // 
            this.テキスト642.DataField = "ITEM94";
            this.テキスト642.Height = 0.1875F;
            this.テキスト642.Left = 0.1319443F;
            this.テキスト642.Name = "テキスト642";
            this.テキスト642.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: left; ddo-char-set: 128";
            this.テキスト642.Tag = "";
            this.テキスト642.Text = "ITEM94";
            this.テキスト642.Top = 0.4198134F;
            this.テキスト642.Width = 2.375F;
            // 
            // テキスト643
            // 
            this.テキスト643.DataField = "ITEM95";
            this.テキスト643.Height = 0.1875F;
            this.テキスト643.Left = 0.1319443F;
            this.テキスト643.Name = "テキスト643";
            this.テキスト643.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: left; ddo-char-set: 128";
            this.テキスト643.Tag = "";
            this.テキスト643.Text = "ITEM95";
            this.テキスト643.Top = 0.6163412F;
            this.テキスト643.Width = 2.375F;
            // 
            // textBox73
            // 
            this.textBox73.DataField = "ITEM03";
            this.textBox73.Height = 0.1875F;
            this.textBox73.Left = 0.1318898F;
            this.textBox73.Name = "textBox73";
            this.textBox73.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox73.Tag = "";
            this.textBox73.Text = "ITEM03";
            this.textBox73.Top = 5.805512F;
            this.textBox73.Width = 1.34375F;
            // 
            // textBox74
            // 
            this.textBox74.DataField = "ITEM94";
            this.textBox74.Height = 0.1875F;
            this.textBox74.Left = 0.1318898F;
            this.textBox74.Name = "textBox74";
            this.textBox74.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: left; ddo-char-set: 128";
            this.textBox74.Tag = "";
            this.textBox74.Text = "ITEM94";
            this.textBox74.Top = 5.999957F;
            this.textBox74.Width = 2.375F;
            // 
            // textBox75
            // 
            this.textBox75.DataField = "ITEM95";
            this.textBox75.Height = 0.1875F;
            this.textBox75.Left = 0.1318898F;
            this.textBox75.Name = "textBox75";
            this.textBox75.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: left; ddo-char-set: 128";
            this.textBox75.Tag = "";
            this.textBox75.Text = "ITEM95";
            this.textBox75.Top = 6.196485F;
            this.textBox75.Width = 2.375F;
            // 
            // shape10
            // 
            this.shape10.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape10.Height = 0.34375F;
            this.shape10.Left = 3.78504F;
            this.shape10.Name = "shape10";
            this.shape10.RoundingRadius = 9.999999F;
            this.shape10.Tag = "";
            this.shape10.Top = 10.4752F;
            this.shape10.Width = 3.915956F;
            // 
            // shape11
            // 
            this.shape11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape11.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape11.Height = 0.2395833F;
            this.shape11.Left = 3.78504F;
            this.shape11.Name = "shape11";
            this.shape11.RoundingRadius = 9.999999F;
            this.shape11.Tag = "";
            this.shape11.Top = 10.23561F;
            this.shape11.Width = 3.915261F;
            // 
            // textBox76
            // 
            this.textBox76.DataField = "ITEM86";
            this.textBox76.Height = 0.1875F;
            this.textBox76.Left = 3.840158F;
            this.textBox76.Name = "textBox76";
            this.textBox76.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.textBox76.Tag = "";
            this.textBox76.Text = "ITEM86";
            this.textBox76.Top = 10.27744F;
            this.textBox76.Width = 1.000393F;
            // 
            // textBox77
            // 
            this.textBox77.DataField = "ITEM87";
            this.textBox77.Height = 0.1875F;
            this.textBox77.Left = 4.929528F;
            this.textBox77.Name = "textBox77";
            this.textBox77.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.textBox77.Tag = "";
            this.textBox77.Text = "ITEM87";
            this.textBox77.Top = 10.27717F;
            this.textBox77.Width = 0.8763781F;
            // 
            // textBox78
            // 
            this.textBox78.DataField = "ITEM88";
            this.textBox78.Height = 0.1875F;
            this.textBox78.Left = 5.95447F;
            this.textBox78.Name = "textBox78";
            this.textBox78.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.textBox78.Tag = "";
            this.textBox78.Text = "ITEM88";
            this.textBox78.Top = 10.27728F;
            this.textBox78.Width = 0.7916667F;
            // 
            // textBox79
            // 
            this.textBox79.DataField = "ITEM89";
            this.textBox79.Height = 0.1875F;
            this.textBox79.Left = 6.864193F;
            this.textBox79.Name = "textBox79";
            this.textBox79.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.textBox79.Tag = "";
            this.textBox79.Text = "ITEM89";
            this.textBox79.Top = 10.27728F;
            this.textBox79.Width = 0.7916667F;
            // 
            // textBox80
            // 
            this.textBox80.DataField = "ITEM90";
            this.textBox80.Height = 0.1875F;
            this.textBox80.Left = 3.840158F;
            this.textBox80.Name = "textBox80";
            this.textBox80.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox80.Tag = "";
            this.textBox80.Text = "ITEM90";
            this.textBox80.Top = 10.5483F;
            this.textBox80.Width = 1.000393F;
            // 
            // textBox81
            // 
            this.textBox81.DataField = "ITEM91";
            this.textBox81.Height = 0.1875F;
            this.textBox81.Left = 4.929528F;
            this.textBox81.Name = "textBox81";
            this.textBox81.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox81.Tag = "";
            this.textBox81.Text = "ITEM91";
            this.textBox81.Top = 10.54803F;
            this.textBox81.Width = 0.8763779F;
            // 
            // textBox82
            // 
            this.textBox82.DataField = "ITEM92";
            this.textBox82.Height = 0.1875F;
            this.textBox82.Left = 5.95447F;
            this.textBox82.Name = "textBox82";
            this.textBox82.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox82.Tag = "";
            this.textBox82.Text = "ITEM92";
            this.textBox82.Top = 10.54811F;
            this.textBox82.Width = 0.7916667F;
            // 
            // textBox83
            // 
            this.textBox83.DataField = "ITEM93";
            this.textBox83.Height = 0.1875F;
            this.textBox83.Left = 6.864193F;
            this.textBox83.Name = "textBox83";
            this.textBox83.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox83.Tag = "";
            this.textBox83.Text = "ITEM93";
            this.textBox83.Top = 10.54811F;
            this.textBox83.Width = 0.7916667F;
            // 
            // line3
            // 
            this.line3.Height = 2.847604F;
            this.line3.Left = 0.8854167F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 1.673229F;
            this.line3.Width = 0.003559828F;
            this.line3.X1 = 0.8889765F;
            this.line3.X2 = 0.8854167F;
            this.line3.Y1 = 1.673229F;
            this.line3.Y2 = 4.520833F;
            // 
            // line4
            // 
            this.line4.Height = 2.839729F;
            this.line4.Left = 3.350771F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 1.677166F;
            this.line4.Width = 0.003560066F;
            this.line4.X1 = 3.354331F;
            this.line4.X2 = 3.350771F;
            this.line4.Y1 = 1.677166F;
            this.line4.Y2 = 4.516895F;
            // 
            // line5
            // 
            this.line5.Height = 2.847604F;
            this.line5.Left = 4.381086F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 1.673229F;
            this.line5.Width = 0.003560066F;
            this.line5.X1 = 4.384646F;
            this.line5.X2 = 4.381086F;
            this.line5.Y1 = 1.673229F;
            this.line5.Y2 = 4.520833F;
            // 
            // line6
            // 
            this.line6.Height = 2.847604F;
            this.line6.Left = 4.698016F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 1.673229F;
            this.line6.Width = 0.003558636F;
            this.line6.X1 = 4.701575F;
            this.line6.X2 = 4.698016F;
            this.line6.Y1 = 1.673229F;
            this.line6.Y2 = 4.520833F;
            // 
            // line7
            // 
            this.line7.Height = 2.847604F;
            this.line7.Left = 5.016126F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 1.673229F;
            this.line7.Width = 0.003558636F;
            this.line7.X1 = 5.019685F;
            this.line7.X2 = 5.016126F;
            this.line7.Y1 = 1.673229F;
            this.line7.Y2 = 4.520833F;
            // 
            // line8
            // 
            this.line8.Height = 2.847604F;
            this.line8.Left = 5.429511F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 1.673229F;
            this.line8.Width = 0.003560066F;
            this.line8.X1 = 5.433071F;
            this.line8.X2 = 5.429511F;
            this.line8.Y1 = 1.673229F;
            this.line8.Y2 = 4.520833F;
            // 
            // line9
            // 
            this.line9.Height = 2.847604F;
            this.line9.Left = 6.036992F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 1.673229F;
            this.line9.Width = 0.003560066F;
            this.line9.X1 = 6.040552F;
            this.line9.X2 = 6.036992F;
            this.line9.Y1 = 1.673229F;
            this.line9.Y2 = 4.520833F;
            // 
            // line10
            // 
            this.line10.Height = 2.847604F;
            this.line10.Left = 6.859433F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 1.673229F;
            this.line10.Width = 0.003559589F;
            this.line10.X1 = 6.862993F;
            this.line10.X2 = 6.859433F;
            this.line10.Y1 = 1.673229F;
            this.line10.Y2 = 4.520833F;
            // 
            // line11
            // 
            this.line11.Height = 2.847604F;
            this.line11.Left = 7.693291F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 1.673229F;
            this.line11.Width = 0.003559589F;
            this.line11.X1 = 7.696851F;
            this.line11.X2 = 7.693291F;
            this.line11.Y1 = 1.673229F;
            this.line11.Y2 = 4.520833F;
            // 
            // line12
            // 
            this.line12.Height = 2.847604F;
            this.line12.Left = 0.1322672F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 1.673229F;
            this.line12.Width = 0.003559589F;
            this.line12.X1 = 0.1358268F;
            this.line12.X2 = 0.1322672F;
            this.line12.Y1 = 1.673229F;
            this.line12.Y2 = 4.520833F;
            // 
            // line13
            // 
            this.line13.Height = 8.237362E-05F;
            this.line13.Left = 0.1358268F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 1.673146F;
            this.line13.Width = 7.552248F;
            this.line13.X1 = 0.1358268F;
            this.line13.X2 = 7.688074F;
            this.line13.Y1 = 1.673228F;
            this.line13.Y2 = 1.673146F;
            // 
            // line14
            // 
            this.line14.Height = 0.006774902F;
            this.line14.Left = 0.1417323F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 4.528658F;
            this.line14.Width = 7.55225F;
            this.line14.X1 = 0.1417323F;
            this.line14.X2 = 7.693982F;
            this.line14.Y1 = 4.535433F;
            this.line14.Y2 = 4.528658F;
            // 
            // label1
            // 
            this.label1.Height = 0.3354167F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.1413386F;
            this.label1.Name = "label1";
            this.label1.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label1.Tag = "";
            this.label1.Text = "　";
            this.label1.Top = 9.790158F;
            this.label1.Width = 7.55F;
            // 
            // label2
            // 
            this.label2.Height = 0.3354167F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.1413386F;
            this.label2.Name = "label2";
            this.label2.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label2.Tag = "";
            this.label2.Text = "　";
            this.label2.Top = 7.772797F;
            this.label2.Width = 7.55F;
            // 
            // label3
            // 
            this.label3.Height = 0.3354167F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.1413386F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "　";
            this.label3.Top = 8.458213F;
            this.label3.Width = 7.55F;
            // 
            // label4
            // 
            this.label4.Height = 0.3361111F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.1413386F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: bold; text-align: left; ddo-char-set: 1";
            this.label4.Tag = "";
            this.label4.Text = "　";
            this.label4.Top = 9.117241F;
            this.label4.Width = 7.55F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM78";
            this.textBox1.Height = 0.1875F;
            this.textBox1.Left = 0.9281443F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM78";
            this.textBox1.Top = 9.870713F;
            this.textBox1.Width = 2.385417F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM24";
            this.textBox2.Height = 0.1805556F;
            this.textBox2.Left = 0.9281443F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM24";
            this.textBox2.Top = 7.867241F;
            this.textBox2.Width = 2.385417F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM42";
            this.textBox3.Height = 0.1875F;
            this.textBox3.Left = 0.9281443F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM42";
            this.textBox3.Top = 8.54988F;
            this.textBox3.Width = 2.385417F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM60";
            this.textBox4.Height = 0.1875F;
            this.textBox4.Left = 0.9281443F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM60";
            this.textBox4.Top = 9.215852F;
            this.textBox4.Width = 2.385417F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM15";
            this.textBox5.Height = 0.1875F;
            this.textBox5.Left = 0.9281443F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM15";
            this.textBox5.Top = 7.551963F;
            this.textBox5.Width = 2.385417F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM33";
            this.textBox6.Height = 0.1875F;
            this.textBox6.Left = 0.9281443F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM33";
            this.textBox6.Top = 8.214463F;
            this.textBox6.Width = 2.385417F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM51";
            this.textBox7.Height = 0.1875F;
            this.textBox7.Left = 0.9281443F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM51";
            this.textBox7.Top = 8.877659F;
            this.textBox7.Width = 2.385417F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM69";
            this.textBox8.Height = 0.1875F;
            this.textBox8.Left = 0.9281443F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox8.Tag = "";
            this.textBox8.Text = "ITEM69";
            this.textBox8.Top = 9.528352F;
            this.textBox8.Width = 2.385417F;
            // 
            // label5
            // 
            this.label5.Height = 0.1979167F;
            this.label5.HyperLink = null;
            this.label5.Left = 0.1413386F;
            this.label5.Name = "label5";
            this.label5.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label5.Tag = "";
            this.label5.Text = "コード";
            this.label5.Top = 7.278352F;
            this.label5.Width = 0.7916667F;
            // 
            // label6
            // 
            this.label6.Height = 0.1979167F;
            this.label6.HyperLink = null;
            this.label6.Left = 0.8913387F;
            this.label6.Name = "label6";
            this.label6.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 128";
            this.label6.Tag = "";
            this.label6.Text = "　商品名";
            this.label6.Top = 7.278352F;
            this.label6.Width = 2.489583F;
            // 
            // label7
            // 
            this.label7.Height = 0.1979167F;
            this.label7.HyperLink = null;
            this.label7.Left = 3.370506F;
            this.label7.Name = "label7";
            this.label7.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label7.Tag = "";
            this.label7.Text = "規　格";
            this.label7.Top = 7.278352F;
            this.label7.Width = 1.03125F;
            // 
            // label8
            // 
            this.label8.Height = 0.1979167F;
            this.label8.HyperLink = null;
            this.label8.Left = 4.388561F;
            this.label8.Name = "label8";
            this.label8.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label8.Tag = "";
            this.label8.Text = "単位";
            this.label8.Top = 7.278352F;
            this.label8.Width = 0.3125F;
            // 
            // label9
            // 
            this.label9.Height = 0.1979167F;
            this.label9.HyperLink = null;
            this.label9.Left = 4.703144F;
            this.label9.Name = "label9";
            this.label9.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label9.Tag = "";
            this.label9.Text = "入数";
            this.label9.Top = 7.278352F;
            this.label9.Width = 0.3541667F;
            // 
            // label10
            // 
            this.label10.Height = 0.1979167F;
            this.label10.HyperLink = null;
            this.label10.Left = 5.026755F;
            this.label10.Name = "label10";
            this.label10.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label10.Tag = "";
            this.label10.Text = "ケース";
            this.label10.Top = 7.278352F;
            this.label10.Width = 0.4052229F;
            // 
            // label11
            // 
            this.label11.Height = 0.1979167F;
            this.label11.HyperLink = null;
            this.label11.Left = 5.418504F;
            this.label11.Name = "label11";
            this.label11.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label11.Tag = "";
            this.label11.Text = "バラ";
            this.label11.Top = 7.278352F;
            this.label11.Width = 0.6436677F;
            // 
            // label12
            // 
            this.label12.Height = 0.1979167F;
            this.label12.HyperLink = null;
            this.label12.Left = 6.047588F;
            this.label12.Name = "label12";
            this.label12.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label12.Tag = "";
            this.label12.Text = "単　価";
            this.label12.Top = 7.278352F;
            this.label12.Width = 0.8229167F;
            // 
            // label13
            // 
            this.label13.Height = 0.1979167F;
            this.label13.HyperLink = null;
            this.label13.Left = 6.870505F;
            this.label13.Name = "label13";
            this.label13.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label13.Tag = "";
            this.label13.Text = "金　額";
            this.label13.Top = 7.278352F;
            this.label13.Width = 0.8229167F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM14";
            this.textBox9.Height = 0.1875F;
            this.textBox9.Left = 0.1725886F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox9.Tag = "";
            this.textBox9.Text = "ITEM14";
            this.textBox9.Top = 7.551963F;
            this.textBox9.Width = 0.6770833F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM16";
            this.textBox10.Height = 0.1875F;
            this.textBox10.Left = 3.401756F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox10.Tag = "";
            this.textBox10.Text = "ITEM16";
            this.textBox10.Top = 7.551963F;
            this.textBox10.Width = 0.9479167F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM17";
            this.textBox11.Height = 0.1875F;
            this.textBox11.Left = 4.433005F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox11.Tag = "";
            this.textBox11.Text = "ITEM17";
            this.textBox11.Top = 7.551963F;
            this.textBox11.Width = 0.2291667F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM18";
            this.textBox12.Height = 0.1875F;
            this.textBox12.Left = 4.745505F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox12.Tag = "";
            this.textBox12.Text = "ITEM18";
            this.textBox12.Top = 7.551963F;
            this.textBox12.Width = 0.2395833F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM19";
            this.textBox13.Height = 0.1875F;
            this.textBox13.Left = 5.055555F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox13.Tag = "";
            this.textBox13.Text = "ITEM19";
            this.textBox13.Top = 7.551963F;
            this.textBox13.Width = 0.3676737F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM20";
            this.textBox14.Height = 0.1875F;
            this.textBox14.Left = 5.448819F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox14.Tag = "";
            this.textBox14.Text = "ITEM20";
            this.textBox14.Top = 7.551963F;
            this.textBox14.Width = 0.5767716F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM21";
            this.textBox15.Height = 0.1875F;
            this.textBox15.Left = 6.081616F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox15.Tag = "";
            this.textBox15.Text = "ITEM21";
            this.textBox15.Top = 7.551963F;
            this.textBox15.Width = 0.7604167F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM22";
            this.textBox16.Height = 0.1875F;
            this.textBox16.Left = 6.912171F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox16.Tag = "";
            this.textBox16.Text = "ITEM22";
            this.textBox16.Top = 7.551963F;
            this.textBox16.Width = 0.7604167F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM23";
            this.textBox17.Height = 0.1805556F;
            this.textBox17.Left = 0.1677275F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox17.Tag = "";
            this.textBox17.Text = "ITEM23";
            this.textBox17.Top = 7.867241F;
            this.textBox17.Width = 0.6770833F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM25";
            this.textBox18.Height = 0.1805556F;
            this.textBox18.Left = 3.396894F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox18.Tag = "";
            this.textBox18.Text = "ITEM25";
            this.textBox18.Top = 7.867241F;
            this.textBox18.Width = 0.9479167F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM26";
            this.textBox19.Height = 0.1805556F;
            this.textBox19.Left = 4.428144F;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox19.Tag = "";
            this.textBox19.Text = "ITEM26";
            this.textBox19.Top = 7.867241F;
            this.textBox19.Width = 0.2291667F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM27";
            this.textBox20.Height = 0.1805556F;
            this.textBox20.Left = 4.740644F;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox20.Tag = "";
            this.textBox20.Text = "ITEM27";
            this.textBox20.Top = 7.867241F;
            this.textBox20.Width = 0.2395833F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM28";
            this.textBox21.Height = 0.1805556F;
            this.textBox21.Left = 5.030709F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox21.Tag = "";
            this.textBox21.Text = "ITEM28";
            this.textBox21.Top = 7.867241F;
            this.textBox21.Width = 0.3877953F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM29";
            this.textBox22.Height = 0.1805556F;
            this.textBox22.Left = 5.56495F;
            this.textBox22.Name = "textBox22";
            this.textBox22.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox22.Tag = "";
            this.textBox22.Text = "ITEM29";
            this.textBox22.Top = 7.867241F;
            this.textBox22.Width = 0.4270833F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM30";
            this.textBox23.Height = 0.1805556F;
            this.textBox23.Left = 6.076755F;
            this.textBox23.Name = "textBox23";
            this.textBox23.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox23.Tag = "";
            this.textBox23.Text = "ITEM30";
            this.textBox23.Top = 7.867241F;
            this.textBox23.Width = 0.7604167F;
            // 
            // textBox24
            // 
            this.textBox24.DataField = "ITEM31";
            this.textBox24.Height = 0.1805556F;
            this.textBox24.Left = 6.90731F;
            this.textBox24.Name = "textBox24";
            this.textBox24.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox24.Tag = "";
            this.textBox24.Text = "ITEM31";
            this.textBox24.Top = 7.867241F;
            this.textBox24.Width = 0.7604167F;
            // 
            // textBox25
            // 
            this.textBox25.DataField = "ITEM32";
            this.textBox25.Height = 0.1875F;
            this.textBox25.Left = 0.1725886F;
            this.textBox25.Name = "textBox25";
            this.textBox25.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox25.Tag = "";
            this.textBox25.Text = "ITEM32";
            this.textBox25.Top = 8.214463F;
            this.textBox25.Width = 0.6770833F;
            // 
            // textBox26
            // 
            this.textBox26.DataField = "ITEM34";
            this.textBox26.Height = 0.1875F;
            this.textBox26.Left = 3.401756F;
            this.textBox26.Name = "textBox26";
            this.textBox26.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox26.Tag = "";
            this.textBox26.Text = "ITEM34";
            this.textBox26.Top = 8.214463F;
            this.textBox26.Width = 0.9479167F;
            // 
            // textBox27
            // 
            this.textBox27.DataField = "ITEM35";
            this.textBox27.Height = 0.1875F;
            this.textBox27.Left = 4.433005F;
            this.textBox27.Name = "textBox27";
            this.textBox27.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox27.Tag = "";
            this.textBox27.Text = "ITEM35";
            this.textBox27.Top = 8.214463F;
            this.textBox27.Width = 0.2291667F;
            // 
            // textBox28
            // 
            this.textBox28.DataField = "ITEM36";
            this.textBox28.Height = 0.1875F;
            this.textBox28.Left = 4.745505F;
            this.textBox28.Name = "textBox28";
            this.textBox28.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox28.Tag = "";
            this.textBox28.Text = "ITEM36";
            this.textBox28.Top = 8.214463F;
            this.textBox28.Width = 0.2395833F;
            // 
            // textBox29
            // 
            this.textBox29.DataField = "ITEM37";
            this.textBox29.Height = 0.1875F;
            this.textBox29.Left = 5.055555F;
            this.textBox29.Name = "textBox29";
            this.textBox29.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox29.Tag = "";
            this.textBox29.Text = "ITEM37";
            this.textBox29.Top = 8.214463F;
            this.textBox29.Width = 0.3676737F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM38";
            this.textBox30.Height = 0.1875F;
            this.textBox30.Left = 5.448819F;
            this.textBox30.Name = "textBox30";
            this.textBox30.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox30.Tag = "";
            this.textBox30.Text = "ITEM38";
            this.textBox30.Top = 8.214463F;
            this.textBox30.Width = 0.5767716F;
            // 
            // textBox31
            // 
            this.textBox31.DataField = "ITEM39";
            this.textBox31.Height = 0.1875F;
            this.textBox31.Left = 6.081616F;
            this.textBox31.Name = "textBox31";
            this.textBox31.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox31.Tag = "";
            this.textBox31.Text = "ITEM39";
            this.textBox31.Top = 8.214463F;
            this.textBox31.Width = 0.7604167F;
            // 
            // textBox32
            // 
            this.textBox32.DataField = "ITEM40";
            this.textBox32.Height = 0.1875F;
            this.textBox32.Left = 6.912171F;
            this.textBox32.Name = "textBox32";
            this.textBox32.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox32.Tag = "";
            this.textBox32.Text = "ITEM40";
            this.textBox32.Top = 8.214463F;
            this.textBox32.Width = 0.7604167F;
            // 
            // textBox33
            // 
            this.textBox33.DataField = "ITEM41";
            this.textBox33.Height = 0.1875F;
            this.textBox33.Left = 0.1725886F;
            this.textBox33.Name = "textBox33";
            this.textBox33.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox33.Tag = "";
            this.textBox33.Text = "ITEM41";
            this.textBox33.Top = 8.54988F;
            this.textBox33.Width = 0.6770833F;
            // 
            // textBox34
            // 
            this.textBox34.DataField = "ITEM43";
            this.textBox34.Height = 0.1875F;
            this.textBox34.Left = 3.401756F;
            this.textBox34.Name = "textBox34";
            this.textBox34.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox34.Tag = "";
            this.textBox34.Text = "ITEM43";
            this.textBox34.Top = 8.54988F;
            this.textBox34.Width = 0.9479167F;
            // 
            // textBox35
            // 
            this.textBox35.DataField = "ITEM44";
            this.textBox35.Height = 0.1875F;
            this.textBox35.Left = 4.433005F;
            this.textBox35.Name = "textBox35";
            this.textBox35.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox35.Tag = "";
            this.textBox35.Text = "ITEM44";
            this.textBox35.Top = 8.54988F;
            this.textBox35.Width = 0.2291667F;
            // 
            // textBox36
            // 
            this.textBox36.DataField = "ITEM45";
            this.textBox36.Height = 0.1875F;
            this.textBox36.Left = 4.745505F;
            this.textBox36.Name = "textBox36";
            this.textBox36.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox36.Tag = "";
            this.textBox36.Text = "ITEM45";
            this.textBox36.Top = 8.54988F;
            this.textBox36.Width = 0.2395833F;
            // 
            // textBox37
            // 
            this.textBox37.DataField = "ITEM46";
            this.textBox37.Height = 0.1805556F;
            this.textBox37.Left = 5.030709F;
            this.textBox37.Name = "textBox37";
            this.textBox37.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox37.Tag = "";
            this.textBox37.Text = "ITEM46";
            this.textBox37.Top = 8.54988F;
            this.textBox37.Width = 0.3877953F;
            // 
            // textBox38
            // 
            this.textBox38.DataField = "ITEM47";
            this.textBox38.Height = 0.1875F;
            this.textBox38.Left = 5.56981F;
            this.textBox38.Name = "textBox38";
            this.textBox38.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox38.Tag = "";
            this.textBox38.Text = "ITEM47";
            this.textBox38.Top = 8.54988F;
            this.textBox38.Width = 0.4270833F;
            // 
            // textBox39
            // 
            this.textBox39.DataField = "ITEM48";
            this.textBox39.Height = 0.1875F;
            this.textBox39.Left = 6.081616F;
            this.textBox39.Name = "textBox39";
            this.textBox39.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox39.Tag = "";
            this.textBox39.Text = "ITEM48";
            this.textBox39.Top = 8.54988F;
            this.textBox39.Width = 0.7604167F;
            // 
            // textBox40
            // 
            this.textBox40.DataField = "ITEM49";
            this.textBox40.Height = 0.1875F;
            this.textBox40.Left = 6.912171F;
            this.textBox40.Name = "textBox40";
            this.textBox40.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox40.Tag = "";
            this.textBox40.Text = "ITEM49";
            this.textBox40.Top = 8.54988F;
            this.textBox40.Width = 0.7604167F;
            // 
            // textBox41
            // 
            this.textBox41.DataField = "ITEM50";
            this.textBox41.Height = 0.1875F;
            this.textBox41.Left = 0.1725886F;
            this.textBox41.Name = "textBox41";
            this.textBox41.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox41.Tag = "";
            this.textBox41.Text = "ITEM50";
            this.textBox41.Top = 8.877659F;
            this.textBox41.Width = 0.6770833F;
            // 
            // textBox42
            // 
            this.textBox42.DataField = "ITEM52";
            this.textBox42.Height = 0.1875F;
            this.textBox42.Left = 3.401756F;
            this.textBox42.Name = "textBox42";
            this.textBox42.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox42.Tag = "";
            this.textBox42.Text = "ITEM52";
            this.textBox42.Top = 8.877659F;
            this.textBox42.Width = 0.9479167F;
            // 
            // textBox43
            // 
            this.textBox43.DataField = "ITEM53";
            this.textBox43.Height = 0.1875F;
            this.textBox43.Left = 4.433005F;
            this.textBox43.Name = "textBox43";
            this.textBox43.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox43.Tag = "";
            this.textBox43.Text = "ITEM53";
            this.textBox43.Top = 8.877659F;
            this.textBox43.Width = 0.2291667F;
            // 
            // textBox44
            // 
            this.textBox44.DataField = "ITEM54";
            this.textBox44.Height = 0.1875F;
            this.textBox44.Left = 4.745505F;
            this.textBox44.Name = "textBox44";
            this.textBox44.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox44.Tag = "";
            this.textBox44.Text = "ITEM54";
            this.textBox44.Top = 8.877659F;
            this.textBox44.Width = 0.2395833F;
            // 
            // textBox45
            // 
            this.textBox45.DataField = "ITEM55";
            this.textBox45.Height = 0.1875F;
            this.textBox45.Left = 5.055555F;
            this.textBox45.Name = "textBox45";
            this.textBox45.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox45.Tag = "";
            this.textBox45.Text = "ITEM55";
            this.textBox45.Top = 8.877659F;
            this.textBox45.Width = 0.3676737F;
            // 
            // textBox46
            // 
            this.textBox46.DataField = "ITEM56";
            this.textBox46.Height = 0.1875F;
            this.textBox46.Left = 5.448819F;
            this.textBox46.Name = "textBox46";
            this.textBox46.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox46.Tag = "";
            this.textBox46.Text = "ITEM56";
            this.textBox46.Top = 8.877659F;
            this.textBox46.Width = 0.5767716F;
            // 
            // textBox47
            // 
            this.textBox47.DataField = "ITEM57";
            this.textBox47.Height = 0.1875F;
            this.textBox47.Left = 6.081616F;
            this.textBox47.Name = "textBox47";
            this.textBox47.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox47.Tag = "";
            this.textBox47.Text = "ITEM57";
            this.textBox47.Top = 8.877659F;
            this.textBox47.Width = 0.7604167F;
            // 
            // textBox48
            // 
            this.textBox48.DataField = "ITEM58";
            this.textBox48.Height = 0.1875F;
            this.textBox48.Left = 6.912171F;
            this.textBox48.Name = "textBox48";
            this.textBox48.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox48.Tag = "";
            this.textBox48.Text = "ITEM58";
            this.textBox48.Top = 8.877659F;
            this.textBox48.Width = 0.7604167F;
            // 
            // textBox49
            // 
            this.textBox49.DataField = "ITEM59";
            this.textBox49.Height = 0.1875F;
            this.textBox49.Left = 0.1725886F;
            this.textBox49.Name = "textBox49";
            this.textBox49.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox49.Tag = "";
            this.textBox49.Text = "ITEM59";
            this.textBox49.Top = 9.215852F;
            this.textBox49.Width = 0.6770833F;
            // 
            // textBox50
            // 
            this.textBox50.DataField = "ITEM61";
            this.textBox50.Height = 0.1875F;
            this.textBox50.Left = 3.401756F;
            this.textBox50.Name = "textBox50";
            this.textBox50.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox50.Tag = "";
            this.textBox50.Text = "ITEM61";
            this.textBox50.Top = 9.215852F;
            this.textBox50.Width = 0.9479167F;
            // 
            // textBox51
            // 
            this.textBox51.DataField = "ITEM62";
            this.textBox51.Height = 0.1875F;
            this.textBox51.Left = 4.433005F;
            this.textBox51.Name = "textBox51";
            this.textBox51.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox51.Tag = "";
            this.textBox51.Text = "ITEM62";
            this.textBox51.Top = 9.215852F;
            this.textBox51.Width = 0.2291667F;
            // 
            // textBox52
            // 
            this.textBox52.DataField = "ITEM63";
            this.textBox52.Height = 0.1875F;
            this.textBox52.Left = 4.745505F;
            this.textBox52.Name = "textBox52";
            this.textBox52.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox52.Tag = "";
            this.textBox52.Text = "ITEM63";
            this.textBox52.Top = 9.215852F;
            this.textBox52.Width = 0.2395833F;
            // 
            // textBox53
            // 
            this.textBox53.DataField = "ITEM64";
            this.textBox53.Height = 0.1805556F;
            this.textBox53.Left = 5.030709F;
            this.textBox53.Name = "textBox53";
            this.textBox53.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox53.Tag = "";
            this.textBox53.Text = "ITEM64";
            this.textBox53.Top = 9.215852F;
            this.textBox53.Width = 0.3877953F;
            // 
            // textBox54
            // 
            this.textBox54.DataField = "ITEM65";
            this.textBox54.Height = 0.1875F;
            this.textBox54.Left = 5.56981F;
            this.textBox54.Name = "textBox54";
            this.textBox54.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox54.Tag = "";
            this.textBox54.Text = "ITEM65";
            this.textBox54.Top = 9.215852F;
            this.textBox54.Width = 0.4270833F;
            // 
            // textBox55
            // 
            this.textBox55.DataField = "ITEM66";
            this.textBox55.Height = 0.1875F;
            this.textBox55.Left = 6.081616F;
            this.textBox55.Name = "textBox55";
            this.textBox55.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox55.Tag = "";
            this.textBox55.Text = "ITEM66";
            this.textBox55.Top = 9.215852F;
            this.textBox55.Width = 0.7604167F;
            // 
            // textBox56
            // 
            this.textBox56.DataField = "ITEM67";
            this.textBox56.Height = 0.1875F;
            this.textBox56.Left = 6.912171F;
            this.textBox56.Name = "textBox56";
            this.textBox56.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox56.Tag = "";
            this.textBox56.Text = "ITEM67";
            this.textBox56.Top = 9.215852F;
            this.textBox56.Width = 0.7604167F;
            // 
            // textBox57
            // 
            this.textBox57.DataField = "ITEM68";
            this.textBox57.Height = 0.1875F;
            this.textBox57.Left = 0.1725886F;
            this.textBox57.Name = "textBox57";
            this.textBox57.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox57.Tag = "";
            this.textBox57.Text = "ITEM68";
            this.textBox57.Top = 9.528352F;
            this.textBox57.Width = 0.6770833F;
            // 
            // textBox58
            // 
            this.textBox58.DataField = "ITEM70";
            this.textBox58.Height = 0.1875F;
            this.textBox58.Left = 3.401756F;
            this.textBox58.Name = "textBox58";
            this.textBox58.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox58.Tag = "";
            this.textBox58.Text = "ITEM70";
            this.textBox58.Top = 9.528352F;
            this.textBox58.Width = 0.9479167F;
            // 
            // textBox59
            // 
            this.textBox59.DataField = "ITEM71";
            this.textBox59.Height = 0.1875F;
            this.textBox59.Left = 4.433005F;
            this.textBox59.Name = "textBox59";
            this.textBox59.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox59.Tag = "";
            this.textBox59.Text = "ITEM71";
            this.textBox59.Top = 9.528352F;
            this.textBox59.Width = 0.2291667F;
            // 
            // textBox60
            // 
            this.textBox60.DataField = "ITEM72";
            this.textBox60.Height = 0.1875F;
            this.textBox60.Left = 4.745505F;
            this.textBox60.Name = "textBox60";
            this.textBox60.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox60.Tag = "";
            this.textBox60.Text = "ITEM72";
            this.textBox60.Top = 9.528352F;
            this.textBox60.Width = 0.2395833F;
            // 
            // textBox61
            // 
            this.textBox61.DataField = "ITEM73";
            this.textBox61.Height = 0.1875F;
            this.textBox61.Left = 5.055555F;
            this.textBox61.Name = "textBox61";
            this.textBox61.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox61.Tag = "";
            this.textBox61.Text = "ITEM73";
            this.textBox61.Top = 9.528352F;
            this.textBox61.Width = 0.3676737F;
            // 
            // textBox62
            // 
            this.textBox62.DataField = "ITEM74";
            this.textBox62.Height = 0.1875F;
            this.textBox62.Left = 5.448819F;
            this.textBox62.Name = "textBox62";
            this.textBox62.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox62.Tag = "";
            this.textBox62.Text = "ITEM74";
            this.textBox62.Top = 9.528352F;
            this.textBox62.Width = 0.5767716F;
            // 
            // textBox63
            // 
            this.textBox63.DataField = "ITEM75";
            this.textBox63.Height = 0.1875F;
            this.textBox63.Left = 6.081616F;
            this.textBox63.Name = "textBox63";
            this.textBox63.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox63.Tag = "";
            this.textBox63.Text = "ITEM75";
            this.textBox63.Top = 9.528352F;
            this.textBox63.Width = 0.7604167F;
            // 
            // textBox64
            // 
            this.textBox64.DataField = "ITEM76";
            this.textBox64.Height = 0.1875F;
            this.textBox64.Left = 6.912171F;
            this.textBox64.Name = "textBox64";
            this.textBox64.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox64.Tag = "";
            this.textBox64.Text = "ITEM76";
            this.textBox64.Top = 9.528352F;
            this.textBox64.Width = 0.7604167F;
            // 
            // textBox65
            // 
            this.textBox65.DataField = "ITEM77";
            this.textBox65.Height = 0.1875F;
            this.textBox65.Left = 0.1725886F;
            this.textBox65.Name = "textBox65";
            this.textBox65.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox65.Tag = "";
            this.textBox65.Text = "ITEM77";
            this.textBox65.Top = 9.870713F;
            this.textBox65.Width = 0.6770833F;
            // 
            // textBox66
            // 
            this.textBox66.DataField = "ITEM79";
            this.textBox66.Height = 0.1875F;
            this.textBox66.Left = 3.401756F;
            this.textBox66.Name = "textBox66";
            this.textBox66.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox66.Tag = "";
            this.textBox66.Text = "ITEM79";
            this.textBox66.Top = 9.870713F;
            this.textBox66.Width = 0.9479167F;
            // 
            // textBox67
            // 
            this.textBox67.DataField = "ITEM80";
            this.textBox67.Height = 0.1875F;
            this.textBox67.Left = 4.433005F;
            this.textBox67.Name = "textBox67";
            this.textBox67.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox67.Tag = "";
            this.textBox67.Text = "ITEM80";
            this.textBox67.Top = 9.870713F;
            this.textBox67.Width = 0.2291667F;
            // 
            // textBox68
            // 
            this.textBox68.DataField = "ITEM81";
            this.textBox68.Height = 0.1875F;
            this.textBox68.Left = 4.745505F;
            this.textBox68.Name = "textBox68";
            this.textBox68.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.textBox68.Tag = "";
            this.textBox68.Text = "ITEM81";
            this.textBox68.Top = 9.870713F;
            this.textBox68.Width = 0.2395833F;
            // 
            // textBox69
            // 
            this.textBox69.DataField = "ITEM82";
            this.textBox69.Height = 0.1805556F;
            this.textBox69.Left = 5.030709F;
            this.textBox69.Name = "textBox69";
            this.textBox69.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox69.Tag = "";
            this.textBox69.Text = "ITEM82";
            this.textBox69.Top = 9.870713F;
            this.textBox69.Width = 0.3877953F;
            // 
            // textBox70
            // 
            this.textBox70.DataField = "ITEM83";
            this.textBox70.Height = 0.1875F;
            this.textBox70.Left = 5.56981F;
            this.textBox70.Name = "textBox70";
            this.textBox70.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox70.Tag = "";
            this.textBox70.Text = "ITEM83";
            this.textBox70.Top = 9.870713F;
            this.textBox70.Width = 0.4270833F;
            // 
            // textBox71
            // 
            this.textBox71.DataField = "ITEM84";
            this.textBox71.Height = 0.1875F;
            this.textBox71.Left = 6.081616F;
            this.textBox71.Name = "textBox71";
            this.textBox71.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox71.Tag = "";
            this.textBox71.Text = "ITEM84";
            this.textBox71.Top = 9.870713F;
            this.textBox71.Width = 0.7604167F;
            // 
            // textBox72
            // 
            this.textBox72.DataField = "ITEM85";
            this.textBox72.Height = 0.1875F;
            this.textBox72.Left = 6.912171F;
            this.textBox72.Name = "textBox72";
            this.textBox72.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.textBox72.Tag = "";
            this.textBox72.Text = "ITEM85";
            this.textBox72.Top = 9.870713F;
            this.textBox72.Width = 0.7604167F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.1413386F;
            this.line1.LineWeight = 0F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 7.486686F;
            this.line1.Width = 7.559722F;
            this.line1.X1 = 0.1413386F;
            this.line1.X2 = 7.701061F;
            this.line1.Y1 = 7.486686F;
            this.line1.Y2 = 7.486686F;
            // 
            // line15
            // 
            this.line15.Height = 2.847604F;
            this.line15.Left = 0.8878665F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 7.278296F;
            this.line15.Width = 0.003559709F;
            this.line15.X1 = 0.8914262F;
            this.line15.X2 = 0.8878665F;
            this.line15.Y1 = 7.278296F;
            this.line15.Y2 = 10.1259F;
            // 
            // line16
            // 
            this.line16.Height = 2.839728F;
            this.line16.Left = 3.353221F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 7.282232F;
            this.line16.Width = 0.003560066F;
            this.line16.X1 = 3.356781F;
            this.line16.X2 = 3.353221F;
            this.line16.Y1 = 7.282232F;
            this.line16.Y2 = 10.12196F;
            // 
            // line17
            // 
            this.line17.Height = 2.847604F;
            this.line17.Left = 4.383536F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 7.278296F;
            this.line17.Width = 0.003559113F;
            this.line17.X1 = 4.387095F;
            this.line17.X2 = 4.383536F;
            this.line17.Y1 = 7.278296F;
            this.line17.Y2 = 10.1259F;
            // 
            // line18
            // 
            this.line18.Height = 2.847604F;
            this.line18.Left = 4.700466F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 7.278296F;
            this.line18.Width = 0.003558636F;
            this.line18.X1 = 4.704025F;
            this.line18.X2 = 4.700466F;
            this.line18.Y1 = 7.278296F;
            this.line18.Y2 = 10.1259F;
            // 
            // line19
            // 
            this.line19.Height = 2.847604F;
            this.line19.Left = 5.018575F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 7.278296F;
            this.line19.Width = 0.003559589F;
            this.line19.X1 = 5.022135F;
            this.line19.X2 = 5.018575F;
            this.line19.Y1 = 7.278296F;
            this.line19.Y2 = 10.1259F;
            // 
            // line20
            // 
            this.line20.Height = 2.847603F;
            this.line20.Left = 5.448802F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 7.278347F;
            this.line20.Width = 0.00356102F;
            this.line20.X1 = 5.452363F;
            this.line20.X2 = 5.448802F;
            this.line20.Y1 = 7.278347F;
            this.line20.Y2 = 10.12595F;
            // 
            // line21
            // 
            this.line21.Height = 2.847604F;
            this.line21.Left = 6.039442F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 7.278296F;
            this.line21.Width = 0.003559113F;
            this.line21.X1 = 6.043001F;
            this.line21.X2 = 6.039442F;
            this.line21.Y1 = 7.278296F;
            this.line21.Y2 = 10.1259F;
            // 
            // line22
            // 
            this.line22.Height = 2.847604F;
            this.line22.Left = 6.861883F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 7.278296F;
            this.line22.Width = 0.003560066F;
            this.line22.X1 = 6.865443F;
            this.line22.X2 = 6.861883F;
            this.line22.Y1 = 7.278296F;
            this.line22.Y2 = 10.1259F;
            // 
            // line23
            // 
            this.line23.Height = 2.847604F;
            this.line23.Left = 7.695741F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 7.278296F;
            this.line23.Width = 0.003554821F;
            this.line23.X1 = 7.699296F;
            this.line23.X2 = 7.695741F;
            this.line23.Y1 = 7.278296F;
            this.line23.Y2 = 10.1259F;
            // 
            // line24
            // 
            this.line24.Height = 2.847604F;
            this.line24.Left = 0.1347171F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 7.278296F;
            this.line24.Width = 0.003559589F;
            this.line24.X1 = 0.1382767F;
            this.line24.X2 = 0.1347171F;
            this.line24.Y1 = 7.278296F;
            this.line24.Y2 = 10.1259F;
            // 
            // line25
            // 
            this.line25.Height = 5.149841E-05F;
            this.line25.Left = 0.1382767F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 7.284988F;
            this.line25.Width = 7.552249F;
            this.line25.X1 = 0.1382767F;
            this.line25.X2 = 7.690526F;
            this.line25.Y1 = 7.284988F;
            this.line25.Y2 = 7.285039F;
            // 
            // line26
            // 
            this.line26.Height = 0.006780624F;
            this.line26.Left = 0.1444882F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 10.12157F;
            this.line26.Width = 7.552247F;
            this.line26.X1 = 0.1444882F;
            this.line26.X2 = 7.696735F;
            this.line26.Y1 = 10.12835F;
            this.line26.Y2 = 10.12157F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 5.433071F;
            this.line2.Width = 7.874016F;
            this.line2.X1 = 0F;
            this.line2.X2 = 7.874016F;
            this.line2.Y1 = 5.433071F;
            this.line2.Y2 = 5.433071F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KOBE2013R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.885417F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル378)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClrCg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル340)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル359)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト389)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト332)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト351)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト370)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト231)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト342)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト361)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト380)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル203)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト258)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル196)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル201)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト204)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト205)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル207)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル208)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト209)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル210)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト211)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト212)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト213)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト215)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト217)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト218)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル219)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト220)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル221)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル222)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル223)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル224)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル225)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル226)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル227)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル228)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル229)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト230)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト232)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト233)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト234)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト235)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト236)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト237)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト238)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト331)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト333)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト334)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト335)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト336)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト337)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト338)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト339)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト341)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト343)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト344)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト345)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト346)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト347)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト348)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト349)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト350)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト352)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト353)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト354)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト355)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト356)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト357)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト358)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト360)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト362)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト363)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト364)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト365)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト366)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト367)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト368)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト369)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト371)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト372)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト373)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト374)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト375)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト376)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト377)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト379)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト381)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト382)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト383)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト384)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト385)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト386)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト387)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト388)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト390)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト391)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト392)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト393)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト394)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト395)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト396)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト311)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト312)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト313)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト314)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト315)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト318)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル414)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト415)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト416)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト417)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル418)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル420)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト422)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル424)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル425)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト426)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル427)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト428)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト429)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト430)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト431)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト432)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト433)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル434)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト435)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル535)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト642)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト643)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル378;
        private GrapeCity.ActiveReports.SectionReportModel.Label BackClrCg;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル340;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル359;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト389;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト332;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト351;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト370;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト231;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト342;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト361;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト380;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル203;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト258;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル196;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル201;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト204;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト205;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線206;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル207;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル208;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト209;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル210;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト211;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト212;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト213;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト215;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト217;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト218;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル219;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト220;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル221;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル222;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル223;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル224;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル225;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル226;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル227;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル228;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル229;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト230;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト232;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト233;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト234;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト235;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト236;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト237;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト238;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト331;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト333;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト334;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト335;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト336;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト337;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト338;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト339;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト341;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト343;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト344;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト345;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト346;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト347;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト348;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト349;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト350;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト352;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト353;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト354;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト355;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト356;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト357;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト358;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト360;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト362;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト363;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト364;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト365;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト366;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト367;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト368;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト369;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト371;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト372;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト373;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト374;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト375;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト376;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト377;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト379;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト381;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト382;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト383;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト384;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト385;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト386;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト387;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト388;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト390;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト391;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト392;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト393;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト394;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト395;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト396;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス309;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス310;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト311;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト312;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト313;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト314;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト315;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト316;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト317;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト318;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線407;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル414;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト415;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト416;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト417;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル418;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル420;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト422;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線423;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル424;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル425;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト426;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル427;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト428;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト429;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト430;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト431;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト432;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト433;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル434;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト435;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル535;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト642;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト643;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox75;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape10;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox78;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox80;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox81;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox82;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox83;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox69;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox70;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox72;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
    }
}
