﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;


using jp.co.sosok.erp.common.dataaccess;
using jp.co.sosok.erp.common.forms;
using jp.co.sosok.erp.common.util;

namespace jp.co.sosok.erp.kob.kobe2011
{
    #region 構造体
    /// <summary>
    /// 伝票明細計算情報のデータ構造体
    /// </summary>
    struct MeisaiCalcInfo
    {
        public decimal TANKA_SHUTOKU_HOHO;　// 単価取得方法
        public decimal KINGAKU_HASU_SHORI; // 金額端数処理
        public decimal SHOHIZEI_NYURYOKU_HOHO; // 消費税入力方法
        public decimal SHOHIZEI_HASU_SHORI; // 消費税端数処理
        public decimal SHOHIZEI_TENKA_HOHO; // 消費税転嫁方法

        public void Clear()
        {
            TANKA_SHUTOKU_HOHO = 0;
            KINGAKU_HASU_SHORI = 0;
            SHOHIZEI_NYURYOKU_HOHO = 0;
            SHOHIZEI_HASU_SHORI = 0;
            SHOHIZEI_TENKA_HOHO = 0;
        }
    }

    #endregion

    /// <summary>
    /// 売上伝票入力(KOBE2011)
    /// </summary>
    public partial class KOBE2011 : BasePgForm
    {
        // 入力モード設定
        private int MODE_EDIT = 1; // 1:登録,2:修正
        // データ入力フラグ(フォームを閉じる時に確認画面を出力時使用)
        private int IS_INPUT = 1;  // 1:未入力,2:入力済
        // 伝票明細計算情報を設定
        MeisaiCalcInfo UriageInfo = new MeisaiCalcInfo();

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KOBE2011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();

            //MessageForwarder(マウスホール用) のインスタンス生成．対象はtxtGridEditとdgvInputList
            new MessageForwarder(this.txtGridEdit, 0x20A);
            new MessageForwarder(this.dgvInputList, 0x20A);
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 新規モードの初期表示
            InitDispOnNew();           
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1～F12を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDenpyoNo":
                case "txtGengoYear":
                case "txtTorihikiKubunCd":
                case "txtFunanushiCd":
                case "txtTantoshaCd":
                    this.btnF1.Enabled = true;
                    break;
                case "txtGridEdit":
                    switch (this.dgvInputList.CurrentCell.ColumnIndex)
                    {
                        case 1:
                        case 2:
                            this.btnF1.Enabled = true;
                            break;
                        case 8:
                            if (!ValChk.IsEmpty(this.lblFunanushiNm.Text))
                            {
                                this.btnF1.Enabled = true;
                            }
                            break;
                        default:
                            this.btnF1.Enabled = false;
                            break;
                    }
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。
            System.Reflection.Assembly asm;
            Type t;

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                #region 元号
                case "txtGengoYear":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.sosok.erp.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                this.SetJpDate(Util.FixJpDate(this.lblGengo.Text, this.txtGengoYear.Text,
                                        this.txtMonth.Text, this.txtDay.Text, this.Dba));
                            }
                        }
                    }
                    break;
                #endregion

                #region 伝票番号
                case "txtDenpyoNo":
                    // 仕入伝票検索選択画面
                    KOBE2012 frm1012 = new KOBE2012();
                    frm1012.ShowDialog(this);

                    if (frm1012.DialogResult == DialogResult.OK)
                    {
                        string[] result = (string[])frm1012.OutData;
                        
                        this.txtDenpyoNo.Text = result[0];
                        // 伝票データチェック後、伝票修正処理へ
                        this.GetDenpyoData();
                    }
                    frm1012.Dispose();
                    break;
                #endregion

                #region 取引先CD
                case "txtTorihikiKubunCd":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.sosok.erp.com.comc8011.COMC8011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.txtTorihikiKubunCd.Text;
                            frm.Par1 = "TB_HN_F_TORIHIKI_KUBUN1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTorihikiKubunCd.Text = outData[0];
                                this.lblTorihikiKubunNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion

                #region 船主CD
                case "txtFunanushiCd":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.sosok.erp.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCd.Text = outData[0];
                                
                                this.SetFunanushiInfo();
                            }
                        }
                    }
                    break;
                #endregion

                #region 担当者CD
                case "txtTantoshaCd":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.sosok.erp.kob.kobc9041.KOBC9041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoshaCd.Text = outData[0];
                                this.lblTantoshaNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion

                #region 明細
                case "txtGridEdit":
                    switch (this.dgvInputList.CurrentCell.ColumnIndex)
                    {
                        case 1:
                            // アセンブリのロード
                            asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                            // フォーム作成
                            t = asm.GetType("jp.co.sosok.erp.com.comc8011.COMC8011");
                            if (t != null)
                            {
                                Object obj = System.Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.Par1 = "VI_HN_HANBAI_KUBUN_NM";
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        string[] outData = (string[])frm.OutData;
                                        this.txtGridEdit.Text = outData[0];
                                    }
                                }
                            }
                            break;
                        case 2:
                            // 商品一覧選択KOBC9031
                            // アセンブリのロード
                            asm = System.Reflection.Assembly.LoadFrom("KOBC9031.exe");
                            // フォーム作成
                            t = asm.GetType("jp.co.sosok.erp.kob.kobc9031.KOBC9031");
                            if (t != null)
                            {
                                Object obj = System.Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.Par1 = "1";
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        string[] outData = (string[])frm.OutData;
                                        this.txtGridEdit.Text = outData[0];
                                        // 商品情報取得
                                        this.GetShohinInfo(2);
                                    }
                                }
                            }
                            break;
                        case 8:
                            if (!ValChk.IsEmpty(this.lblFunanushiNm.Text) && !ValChk.IsEmpty(this.dgvInputList[2, this.dgvInputList.CurrentCell.RowIndex].Value))
                            {
                                // 売上単価参照選択画面
                                KOBE2014 frm2014 = new KOBE2014(Util.ToDecimal(this.txtFunanushiCd.Text), Util.ToDecimal(this.dgvInputList[2, this.dgvInputList.CurrentCell.RowIndex].Value));
                                frm2014.ShowDialog(this);

                                if (frm2014.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm2014.OutData;

                                    this.txtGridEdit.Text = Util.FormatNum(result[0], 1);
                                    this.dgvInputList[8, this.dgvInputList.CurrentCell.RowIndex].Value = Util.FormatNum(result[0], 1);

                                    // 金額と消費税計算
                                    if (ValChk.IsEmpty(this.dgvInputList[7, this.dgvInputList.CurrentCell.RowIndex].Value) || ValChk.IsEmpty(dgvInputList[8, this.dgvInputList.CurrentCell.RowIndex].Value))
                                    {
                                        this.dgvInputList[9, this.dgvInputList.CurrentCell.RowIndex].Value = "";
                                        this.dgvInputList[10, this.dgvInputList.CurrentCell.RowIndex].Value = "";
                                    }
                                    else
                                    {
                                        this.dgvInputList[9, this.dgvInputList.CurrentCell.RowIndex].Value = Util.FormatNum(Util.ToDecimal(this.dgvInputList[8, this.dgvInputList.CurrentCell.RowIndex].Value) * Util.ToDecimal(this.dgvInputList[7, this.dgvInputList.CurrentCell.RowIndex].Value));
                                        if (!ValChk.IsEmpty(this.dgvInputList[15, this.dgvInputList.CurrentCell.RowIndex].Value))
                                        {
                                            this.dgvInputList[10, this.dgvInputList.CurrentCell.RowIndex].Value = Util.ToDecimal(dgvInputList[9, this.dgvInputList.CurrentCell.RowIndex].Value) * (Util.ToDecimal(this.dgvInputList[15, this.dgvInputList.CurrentCell.RowIndex].Value) / 100);
                                        }
                                    }
                                    this.DataGridSum();
                                }
                                frm2014.Dispose();
                            }
                            break;
                    }
                    break;
                #endregion
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            DbParamCollection dpc;
            DataTable dtCheck;

            // 登録モードは処理しない
            if (this.MODE_EDIT == 1)
            {
                return;
            }

            // 伝票の入力チェック
            if (!this.isValidDenpyoNo())
            {
                this.txtDenpyoNo.SelectAll();
                this.txtDenpyoNo.Focus();
                return;
            }
            // 空の場合、特に何もしない
            else if (ValChk.IsEmpty(this.txtDenpyoNo.Text))
            {
                return;
            }

            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 伝票番号確認処理
            // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(this.txtDenpyoNo.Text));
            dtCheck = this.Dba.GetDataTableByConditionWithParams(
                "COUNT(*) AS CNT",
                "TB_HN_TORIHIKI_DENPYO",
                "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                dpc);
            if (Util.ToLong(dtCheck.Rows[0]["CNT"]) == 0)
            {
                return;
            }

            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 削除処理
            this.DeleteData();
            // 登録初期表示処理へ
            this.InitDispOnNew();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 入力チェック
            if (!this.ValidateAll())
            {
                return;
            }

            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return;
            }

            string msg = (this.MODE_EDIT == 1 ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }
            // 登録処理
            Decimal DENPYO_BANGO = this.UpdateData();

            msg = "プレビューしますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.Yes)
            {
                // 「はい」を押されたらプレビュー処理
                DoPrint(true, DENPYO_BANGO, 2);
            }

            // 登録初期表示処理へ
            this.InitDispOnNew();
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            // 行削除処理
            if (this.dgvInputList.RowCount >　1  && this.dgvInputList.RowCount > this.dgvInputList.CurrentCell.RowIndex + 1)
            {
                this.dgvInputList.Rows.RemoveAt(this.dgvInputList.CurrentCell.RowIndex);
                int i = 0;
                while (this.dgvInputList.RowCount > i)
                {
                    this.dgvInputList[0, i].Value = i + 1;
                    i++;
                }
                this.DataGridSum();
            }
        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            // 複写処理(上の行データを選択行データにコピー)
            if (this.dgvInputList.CurrentCell.RowIndex > 0)
            {
                int col = 1;
                int rowCurPos = this.dgvInputList.CurrentCell.RowIndex;
                DataGridViewRow getRow = this.dgvInputList.Rows[rowCurPos - 1];
                while (getRow.Cells.Count > col)
                {
                    this.dgvInputList[col, rowCurPos].Value = this.dgvInputList[col, rowCurPos - 1].Value;
                    col++;
                }
                if (this.txtGridEdit.Visible == true)
                {
                    this.txtGridEdit.Text = Util.ToString(this.dgvInputList[this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex].Value);
                }
                if (this.dgvInputList.Rows.Count == rowCurPos + 1)
                {
                    this.dgvInputList.RowCount = this.dgvInputList.RowCount + 1;
                    this.dgvInputList[0, this.dgvInputList.RowCount - 1].Value = this.dgvInputList.RowCount;
                }
                this.DataGridSum();
            }
        }

        /// <summary>
        /// F10キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 領収書印刷
            // 入力チェック
            if (!this.ValidateAll())
            {
                return;
            }

            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return;
            }

            // 登録処理
            Decimal DENPYO_BANGO = this.UpdateData();
            
            // プレビュー処理
            DoPrint(true, DENPYO_BANGO, 1);

            // 登録初期表示処理へ
            this.InitDispOnNew();
        }
        #endregion

        #region イベント
        private decimal MEISAI_DENPYO_BANGO = 0;
        /// <summary>
        /// 伝票番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDenpyoNo_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidDenpyoNo())
            {
                this.txtDenpyoNo.SelectAll();
                this.txtDenpyoNo.Focus();
                e.Cancel = true;
                return;
            }
            // 空の場合、特に何もしない
            else if (ValChk.IsEmpty(this.txtDenpyoNo.Text))
            {
                e.Cancel = false;
                return;
            }

            this.IS_INPUT = 2;

            // 伝票データチェック後、伝票修正処理へ
            e.Cancel = this.GetDenpyoData();
        }

        /// <summary>
        /// 年の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYear_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidGengoYear())
            {
                this.txtGengoYear.SelectAll();
                this.txtGengoYear.Focus();
                e.Cancel = true;
                return;
            }

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidMonth())
            {
                this.txtMonth.SelectAll();
                this.txtMonth.Focus();
                e.Cancel = true;
                return;
            }

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidDay())
            {
                this.txtDay.SelectAll();
                this.txtDay.Focus();
                e.Cancel = true;
                return;
            }

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 取引区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTorihikiKubunCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidTorihikiKubunCd())
            {
                this.txtTorihikiKubunCd.SelectAll();
                this.txtTorihikiKubunCd.Focus();
                e.Cancel = true;
                return;
            }

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 船主CDの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidFunanushiCd())
            {
                this.txtFunanushiCd.SelectAll();
                this.txtFunanushiCd.Focus();
                e.Cancel = true;
                return;
            }

            this.SetFunanushiInfo();

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 担当者CDの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidTantoshaCd())
            {
                this.txtTantoshaCd.SelectAll();
                this.txtTantoshaCd.Focus();
                e.Cancel = true;
                return;
            }
            
            this.IS_INPUT = 2;
        }

        /// <summary>
        /// データグリッドにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Enter(object sender, EventArgs e)
        {
            DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
            if (dgvCell.ColumnIndex == 0)
            {
                this.dgvInputList.CurrentCell = this.dgvInputList[2, dgvCell.RowIndex];
            }
        }

        /// <summary>
        /// データグリッドのセルにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 1:
                case 2:
                case 4:
                case 5:
                case 7:
                case 8:
                case 9:
                //case 10: // 消費税の部分を入力不可に
                    if (e.ColumnIndex == 10 && ValChk.IsEmpty(this.dgvInputList[15, e.RowIndex].Value))
                    {
                        if (ValChk.IsEmpty(this.dgvInputList[13, e.RowIndex].Value))
                        {
                            break;
                        }
                    }
                    this.txtGridEdit.BackColor = Color.White;
                    if (e.RowIndex % 2 == 1)
                    {
                        this.txtGridEdit.BackColor = ColorTranslator.FromHtml("#BBFFFF");
                    }
                    this.txtGridEdit.Visible = true;
                    DataGridView dgv = (DataGridView)sender;
                    Rectangle rctCell = dgv.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);

                    this.txtGridEdit.Size = rctCell.Size;
                    this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
                    this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;
                    this.txtGridEdit.Text = Util.ToString(dgvInputList[e.ColumnIndex, e.RowIndex].Value).Replace(",", "");

                    this.txtGridEdit.Focus();
                    break;
                default:
                    this.txtGridEdit.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// データグリッドがスクロールした時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            Rectangle rctCell = dgv.GetCellDisplayRectangle(this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex, false);

            this.txtGridEdit.Size = rctCell.Size;
            this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
            this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;

        }

        /// <summary>
        /// データグリッド用のテキストにキーダウン時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGridEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
            {
                DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;

                switch (dgvCell.ColumnIndex)
                {
                    // 区（販売区分名称）
                    case 1:
                        if (ValChk.IsEmpty(txtGridEdit.Text))
                        {
                            this.dgvInputList[1, dgvCell.RowIndex].Value = "";
                        }
                        else if (!ValChk.IsNumber(txtGridEdit.Text))
                        {
                            this.dgvInputList[1, dgvCell.RowIndex].Value = Regex.Replace(this.txtGridEdit.Text, "\\D", "");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        else
                        {
                            // 販売区分名称情報取得
                            string get_name = this.Dba.GetName(this.UInfo, "VI_HN_HANBAI_KUBUN_NM2", this.txtGridEdit.Text);
                            if (get_name == null)
                            {
                                this.dgvInputList[1, dgvCell.RowIndex].Value = "";
                                this.txtGridEdit.Text = "";
                                Msg.Error("入力に誤りがあります。");
                                this.txtGridEdit.Focus();
                                return;
                            }
                            else
                            {
                                this.dgvInputList[1, dgvCell.RowIndex].Value = this.txtGridEdit.Text;
                            }
                        }
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            this.dgvInputList.CurrentCell = dgvInputList[2, dgvCell.RowIndex];
                        }
                        break;
                    // 商品コード
                    case 2:
                        if (ValChk.IsEmpty(txtGridEdit.Text))
                        {
                            this.dgvInputList[2, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[3, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[11, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[12, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[13, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[14, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[15, dgvCell.RowIndex].Value = "";
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        if (!ValChk.IsNumber(txtGridEdit.Text))
                        {
                            this.dgvInputList[2, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[3, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[11, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[12, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[13, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[14, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[15, dgvCell.RowIndex].Value = "";
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        
                        // 商品情報取得
                        this.GetShohinInfo(1);
                        if (!this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            this.dgvInputList.CurrentCell = this.dgvInputList[2, this.dgvInputList.CurrentCell.RowIndex];
                        }
                        
                        break;
                    // 単位
                    case 5:
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            this.dgvInputList.CurrentCell = this.dgvInputList[7, dgvCell.RowIndex];
                        }
                        break;
                    // バラ数
                    case 7:
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 19, 2, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        dgvInputList[7, dgvCell.RowIndex].Value = Util.FormatNum(txtGridEdit.Text, 2);
                        // 金額と消費税計算
                        if (ValChk.IsEmpty(dgvInputList[7, dgvCell.RowIndex].Value) || ValChk.IsEmpty(dgvInputList[8, dgvCell.RowIndex].Value))
                        {
                            dgvInputList[9, dgvCell.RowIndex].Value = "";
                            dgvInputList[10, dgvCell.RowIndex].Value = "";
                        }
                        else
                        {
                            dgvInputList[9, dgvCell.RowIndex].Value = Util.FormatNum(Util.ToDecimal(dgvInputList[8, dgvCell.RowIndex].Value) * Util.ToDecimal(dgvInputList[7, dgvCell.RowIndex].Value));
                            if (!ValChk.IsEmpty(dgvInputList[15, dgvCell.RowIndex].Value) && Util.ToDecimal(this.dgvInputList[17, dgvCell.RowIndex].Value).Equals(1))
                            {
                                // 消費税計算
                                Decimal TaxValue = this.GetTaxCalc(Util.ToDecimal(dgvInputList[9, dgvCell.RowIndex].Value), Util.ToDecimal(dgvInputList[15, dgvCell.RowIndex].Value));
                                dgvInputList[10, dgvCell.RowIndex].Value = TaxValue;
                            }
                        }
                        this.DataGridSum();
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            dgvInputList.CurrentCell = dgvInputList[8, dgvCell.RowIndex];
                        }
                        break;
                    // 単価
                    case 8:
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 12, 2, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        dgvInputList[8, dgvCell.RowIndex].Value = Util.FormatNum(txtGridEdit.Text, 2);
                        // 金額と消費税計算
                        if (ValChk.IsEmpty(dgvInputList[7, dgvCell.RowIndex].Value) || ValChk.IsEmpty(dgvInputList[8, dgvCell.RowIndex].Value))
                        {
                            dgvInputList[9, dgvCell.RowIndex].Value = "";
                            dgvInputList[10, dgvCell.RowIndex].Value = "";
                        }
                        else
                        {
                            dgvInputList[9, dgvCell.RowIndex].Value = Util.FormatNum(Util.ToDecimal(dgvInputList[8, dgvCell.RowIndex].Value) * Util.ToDecimal(dgvInputList[7, dgvCell.RowIndex].Value));
                            if (!ValChk.IsEmpty(dgvInputList[15, dgvCell.RowIndex].Value) && Util.ToDecimal(this.dgvInputList[17, dgvCell.RowIndex].Value).Equals(1))
                            {
                                // 消費税計算
                                Decimal TaxValue = this.GetTaxCalc(Util.ToDecimal(dgvInputList[9, dgvCell.RowIndex].Value), Util.ToDecimal(dgvInputList[15, dgvCell.RowIndex].Value));
                                dgvInputList[10, dgvCell.RowIndex].Value = TaxValue;
                            }
                        }
                        this.DataGridSum();
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            dgvInputList.CurrentCell = dgvInputList[9, dgvCell.RowIndex];
                        }
                        break;
                    // 金額
                    case 9:
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 15, 0, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        dgvInputList[9, dgvCell.RowIndex].Value = Util.FormatNum(txtGridEdit.Text);
                        // 金額と消費税計算
                        if (!ValChk.IsEmpty(dgvInputList[9, dgvCell.RowIndex].Value))
                        {
                            if (!ValChk.IsEmpty(dgvInputList[15, dgvCell.RowIndex].Value) && Util.ToDecimal(this.dgvInputList[17, dgvCell.RowIndex].Value).Equals(1))
                            {
                                // 消費税計算
                                Decimal TaxValue = this.GetTaxCalc(Util.ToDecimal(dgvInputList[9, dgvCell.RowIndex].Value), Util.ToDecimal(dgvInputList[15, dgvCell.RowIndex].Value));
                                dgvInputList[10, dgvCell.RowIndex].Value = TaxValue;
                            }
                            else
                            {
                                this.DataGridSum();
                                if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
                                {
                                    if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                                    {
                                        dgvInputList.CurrentCell = dgvInputList[2, dgvCell.RowIndex + 1];
                                    }
                                }
                                break;
                            }
                        }
                        else
                        {
                            dgvInputList[10, dgvCell.RowIndex].Value = "";
                        }
                        this.DataGridSum();
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            dgvInputList.CurrentCell = dgvInputList[2, dgvCell.RowIndex + 1];
                        }
                        break;
                    // 消費税
                    case 10:
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 15, 0, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        dgvInputList[10, dgvCell.RowIndex].Value = txtGridEdit.Text;
                        if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
                        {
                            if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                            {
                                dgvInputList.CurrentCell = dgvInputList[2, dgvCell.RowIndex + 1];
                            }
                        }
                        this.DataGridSum();
                        break;
                }
                
            }
        }

        /// <summary>
        /// フォーム閉じる前に発生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KOBE2011_FormClosing(object sender, FormClosingEventArgs e)
        {
            // データ入力済時に確認します
            if (this.IS_INPUT == 2)
            {
                string msg = "未更新データが存在します！　処理を終了しますか？";
                if (Msg.ConfYesNo(msg) == DialogResult.No)
                {
                    // 「いいえ」を押されたらフォームを閉じない
                    // コントロールの検証処理(Validata)を有効にする
                    base.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    e.Cancel = true;
                    return;
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 船主CD設定情報
        /// </summary>
        private bool SetFunanushiInfo()
        {
            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtFunanushiCd.Text))
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);
                DataTable dtVI_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                    "TORIHIKISAKI_NM, TANKA_SHUTOKU_HOHO, KINGAKU_HASU_SHORI, SHOHIZEI_NYURYOKU_HOHO, SHOHIZEI_HASU_SHORI, SHOHIZEI_TENKA_HOHO",
                    "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD", dpc);

                if (dtVI_HN_TORIHIKISAKI_JOHO.Rows.Count == 0)
                {
                    Msg.Error("存在しない船主CDです。開発者へご連絡ください。");
                    return true;
                }

                this.lblFunanushiNm.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"]);
                this.UriageInfo.TANKA_SHUTOKU_HOHO = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TANKA_SHUTOKU_HOHO"]);
                this.UriageInfo.KINGAKU_HASU_SHORI = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["KINGAKU_HASU_SHORI"]);
                this.UriageInfo.SHOHIZEI_NYURYOKU_HOHO = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"]);
                this.UriageInfo.SHOHIZEI_HASU_SHORI = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_HASU_SHORI"]);
                this.UriageInfo.SHOHIZEI_TENKA_HOHO = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_TENKA_HOHO"]);
                this.DataGridSum();
            }
            else
            {
                Msg.Error("入力に誤りがあります。");
                this.txtFunanushiCd.SelectAll();
                this.txtFunanushiCd.Focus();
                return true;
                //this.lblFunanushiNm.Text = "";
                //this.UriageInfo.Clear();
            }

            return false;
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 登録に変更
            this.MODE_EDIT = 1;
            this.lblMode.Text = "【登録】";
            this.MEISAI_DENPYO_BANGO = 0;

            // 伝票番号
            this.txtDenpyoNo.Text = "";
            this.txtDenpyoNo.Focus();

            // 日付範囲の和暦設定
            if (ValChk.IsEmpty(this.txtGengoYear.Text) && ValChk.IsEmpty(this.txtMonth.Text) && ValChk.IsEmpty(this.txtDay.Text))
            {
                this.SetJpDate(Util.ConvJpDate(DateTime.Now, this.Dba));
            }
            
            // 取引区分
            if (ValChk.IsEmpty(this.txtTorihikiKubunCd.Text))
            {
                this.txtTorihikiKubunCd.Text = "11"; // 掛仕入
                this.lblTorihikiKubunNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_F_TORIHIKI_KUBUN1", this.txtTorihikiKubunCd.Text); ;
            }

            // 船主CD
            this.txtFunanushiCd.Text = "";
            this.lblFunanushiNm.Text = "";
            this.UriageInfo.Clear();

            // 担当者
            this.txtTantoshaCd.Text = this.UInfo.UserCd;
            this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtTantoshaCd.Text);

            // 削除ボタン使用不可
            this.btnF3.Enabled = false;

            //奇数行を淡い水色にする
            this.dgvInputList.AlternatingRowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#BBFFFF");
            this.dgvInputList.DefaultCellStyle.SelectionBackColor = Color.Transparent;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvInputList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Font = this.txtGridEdit.Font;
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvInputList.DefaultCellStyle.Font = this.txtGridEdit.Font;
            this.dgvInputList.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInputList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            // ユーザ操作による行追加を無効(禁止)
            this.dgvInputList.AllowUserToAddRows = false;
            // 入力モード設定
            this.dgvInputList.EditMode = DataGridViewEditMode.EditProgrammatically;

            // 行追加(1行目)
            this.dgvInputList.Rows.Clear();
            this.dgvInputList.RowCount = 1;
            //this.dgvInputList.Rows.Add();
            this.dgvInputList[0, 0].Value = "1";

            // 小計
            this.txtSyokei.Text = "0";
            // 消費税（外）
            this.txtSyohiZei.Text = "0";
            // 合計額
            this.txtTotalGaku.Text = "0";

            // DataGridViewの編集用テキストコントロールを非表示
            this.txtGridEdit.Visible = false;

            this.IS_INPUT = 1;
        }

        /// <summary>
        /// 修正モードの初期表示
        /// </summary>
        private void InitDispOnEdit(DataTable dtDENPYO)
        {
            // 初期値、入力制御を実装
            DbParamCollection dpc;

            // 修正に変更
            this.MODE_EDIT = 2;
            this.lblMode.Text = "【修正】";

            // 日付範囲の和暦設定
            this.SetJpDate(Util.ConvJpDate(dtDENPYO.Rows[0]["DENPYO_DATE"].ToString(), this.Dba));

            // 取引区分
            this.txtTorihikiKubunCd.Text = dtDENPYO.Rows[0]["TORIHIKI_KUBUN1"].ToString() + dtDENPYO.Rows[0]["TORIHIKI_KUBUN2"].ToString();
            this.lblTorihikiKubunNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_F_TORIHIKI_KUBUN1", this.txtTorihikiKubunCd.Text);

            // 船主
            this.txtFunanushiCd.Text = dtDENPYO.Rows[0]["KAIIN_BANGO"].ToString();
            this.SetFunanushiInfo();

            // 担当者
            this.txtTantoshaCd.Text = dtDENPYO.Rows[0]["TANTOSHA_CD"].ToString();
            this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtTantoshaCd.Text);

            // 削除ボタン使用可能
            this.btnF3.Enabled = true;
            
            // 小計
            this.txtSyokei.Text = Util.FormatNum(dtDENPYO.Rows[0]["URIAGE_KINGAKU"]);
            // 消費税（外）
            this.txtSyohiZei.Text = Util.FormatNum(dtDENPYO.Rows[0]["SHOHIZEIGAKU"]);
            // 合計額
            this.txtTotalGaku.Text = Util.FormatNum(dtDENPYO.Rows[0]["NYUKIN_GOKEIGAKU"]);

            // DataGridViewの編集用テキストコントロールを非表示
            this.txtGridEdit.Visible = false;

            // グリッドをクリア
            this.dgvInputList.Rows.Clear();
            // 伝票明細取得してグリッドに設定
            // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(dtDENPYO.Rows[0]["DENPYO_BANGO"]));
            DataTable dtTB_HN_TORIHIKI_MEISAI = this.Dba.GetDataTableByConditionWithParams(
                "M.GYO_BANGO " +
                ",H.NM AS KUBUN_NM " +
                ",M.SHOHIN_CD " +
                ",M.SHOHIN_NM " +
                ",M.IRISU " +
                ",M.TANI " +
                ",M.SURYO1 " +
                ",M.SURYO2 " +
                ",M.GEN_TANKA " +
                ",M.URI_TANKA " +
                ",M.BAIKA_KINGAKU " +
                ",M.SHOHIZEI " +
                ",M.SHIWAKE_CD " +
                ",M.BUMON_CD " +
                ",M.ZEI_KUBUN " +
                ",M.JIGYO_KUBUN " +
                ",Z.KAZEI_KUBUN " +
                ",M.KUBUN ",
                "TB_HN_TORIHIKI_MEISAI AS M " +
                "LEFT JOIN TB_ZM_F_ZEI_KUBUN AS Z " +
                    "ON M.ZEI_KUBUN = Z.ZEI_KUBUN " +
                "LEFT JOIN VI_HN_HANBAI_KUBUN_NM AS H " +
                    "ON M.KAISHA_CD = H.KAISHA_CD " +
                    "AND H.SHUBETSU_CD = 2 " +
                    "AND H.KUBUN_SHUBETSU = 1 " +
                    "AND H.KUBUN = M.KUBUN ",
                "M.KAISHA_CD = @KAISHA_CD AND M.KAIKEI_NENDO = @KAIKEI_NENDO AND M.DENPYO_KUBUN = @DENPYO_KUBUN AND M.DENPYO_BANGO = @DENPYO_BANGO",
                dpc);
            if (dtTB_HN_TORIHIKI_MEISAI.Rows.Count > 0)
            {
                
                this.dgvInputList.RowCount = dtTB_HN_TORIHIKI_MEISAI.Rows.Count + 1;
                int i = 0;
                while (dtTB_HN_TORIHIKI_MEISAI.Rows.Count > i)
                {
                    this.dgvInputList[0, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["GYO_BANGO"]);
                    this.dgvInputList[1, i].Value = (Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["KUBUN"]) == 0 ? "" : Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["KUBUN"]));
                    this.dgvInputList[2, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_CD"]);
                    this.dgvInputList[3, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_NM"]);
                    this.dgvInputList[4, i].Value = (Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["IRISU"]) == 0 ? "" : dtTB_HN_TORIHIKI_MEISAI.Rows[i]["IRISU"]);
                    this.dgvInputList[5, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["TANI"]);
                    this.dgvInputList[6, i].Value = Util.FormatNum(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"], 1);
                    this.dgvInputList[7, i].Value = Util.FormatNum(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"], 2);
                    this.dgvInputList[8, i].Value = Util.FormatNum(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["URI_TANKA"], 2);
                    this.dgvInputList[9, i].Value = Util.FormatNum(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["BAIKA_KINGAKU"]);
                    this.dgvInputList[10, i].Value = dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIZEI"];
                    this.dgvInputList[11, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHIWAKE_CD"]);
                    this.dgvInputList[12, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["BUMON_CD"]);
                    this.dgvInputList[13, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["ZEI_KUBUN"]);
                    this.dgvInputList[14, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["JIGYO_KUBUN"]);
                    this.dgvInputList[15, i].Value = Util.ToString(this.GetZeiritsu(Util.ToDate(dtDENPYO.Rows[0]["DENPYO_DATE"])));
                    this.dgvInputList[16, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["GEN_TANKA"]);
                    this.dgvInputList[17, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[0]["KAZEI_KUBUN"]);
                    
                    i++;
                }
                // 行追加(最後の行目)
                this.dgvInputList[0, this.dgvInputList.Rows.Count - 1].Value = this.dgvInputList.Rows.Count;
            }
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtGengoYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// データグリッドを小計、消費税（外）、合計額に集計
        /// </summary>
        private void DataGridSum()
        {
            int i = 0;
            decimal SyohiZei = 0, TotalGaku = 0;

            while (this.dgvInputList.RowCount > i)
            {
                if (!ValChk.IsEmpty(this.dgvInputList[13, i].Value))
                {
                    if (!ValChk.IsEmpty(this.dgvInputList[9, i].Value))
                    {
                        TotalGaku += Util.ToDecimal(this.dgvInputList[9, i].Value);
                    }
                    if (!ValChk.IsEmpty(this.dgvInputList[10, i].Value))
                    {

                        SyohiZei += Util.ToDecimal(this.dgvInputList[10, i].Value);
                    }
                }
                i++;
            }
            // 消費税入力方法で計算
            if (this.UriageInfo.SHOHIZEI_NYURYOKU_HOHO.Equals(3))
            {
                this.txtSyokei.Text = Util.FormatNum(TotalGaku - SyohiZei);
                this.txtSyohiZei.Text = Util.FormatNum(SyohiZei);
                this.txtTotalGaku.Text = Util.FormatNum(TotalGaku);
            }
            else
            {
                this.txtSyokei.Text = Util.FormatNum(TotalGaku);
                this.txtSyohiZei.Text = Util.FormatNum(SyohiZei);
                this.txtTotalGaku.Text = Util.FormatNum(TotalGaku + SyohiZei);
            }
        }

        /// <summary>
        /// 入力チェック処理
        /// </summary>
        private bool ValidateAll()
        {
            DbParamCollection dpc;
            DataTable dtCheck;

            // 伝票の入力チェック
            if (!this.isValidDenpyoNo())
            {
                this.txtDenpyoNo.SelectAll();
                this.txtDenpyoNo.Focus();
                return false;
            }
            // 年の入力チェック
            if (!this.isValidGengoYear())
            {
                this.txtGengoYear.Focus();
                this.txtGengoYear.SelectAll();
                return false;
            }
            // 月の入力チェック
            if (!this.isValidMonth())
            {
                this.txtMonth.Focus();
                this.txtMonth.SelectAll();
                return false;
            }
            // 日の入力チェック
            if (!this.isValidDay())
            {
                this.txtDay.Focus();
                this.txtDay.SelectAll();
                return false;
            }

            // 伝票日付が会計期間内かチェック
            //DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
            //                                                   this.txtMonth.Text, this.txtDay.Text, this.Dba);
            //DataRow settings = this.Dba.GetKaikeiSettingsByDate(this.UInfo.KaishaCd, DENPYO_DATE).Rows[0];
            //dpc = new DbParamCollection();
            //dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            //dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 6, this.UInfo.KessanKi);
            //dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DENPYO_DATE);
            //dtCheck = this.Dba.GetDataTableByConditionWithParams(
            //    "*",
            //    "VI_ZM_KAISHA_JOHO",
            //    "KAISHA_CD = @KAISHA_CD AND KESSANKI = @KESSANKI AND (@DENPYO_DATE BETWEEN KAIKEI_KIKAN_KAISHIBI AND KAIKEI_KIKAN_SHURYOBI)",
            //    dpc);
            //if (dtCheck.Rows.Count == 0)
            //{
            //    Msg.Error("伝票日付が会計期間外になってます。");
            //    this.txtGengoYear.Focus();
            //    this.txtGengoYear.SelectAll();
            //    return false;
            //}

            // 取引区分チェック
            if (!this.isValidTorihikiKubunCd())
            {
                this.txtTorihikiKubunCd.SelectAll();
                this.txtTorihikiKubunCd.Focus();
                return false;
            }

            // 船主チェック
            if (!this.isValidFunanushiCd())
            {
                this.txtFunanushiCd.SelectAll();
                this.txtFunanushiCd.Focus();
                return false;
            }

            // 担当者チェック
            if (!this.isValidTantoshaCd())
            {
                this.txtTantoshaCd.SelectAll();
                this.txtTantoshaCd.Focus();
                return false;
            }

            // 明細チェック
            int i = 0;
            while (this.dgvInputList.RowCount > i)
            {
                // 区(販売区分)
                if (!ValChk.IsEmpty(this.dgvInputList[1, i].Value))
                {
                    // Han.VI_販売区分名称(VI_HN_HANBAI_KUBUN_NM)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[1, i].Value));
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "*",
                        "VI_HN_HANBAI_KUBUN_NM",
                        "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 1 AND KUBUN = @KUBUN",
                        dpc);
                    if (dtCheck.Rows.Count == 0)
                    {
                        if (this.dgvInputList.RowCount > i + 1)
                        {
                            Msg.Error("入力に誤りがあります。");
                            this.dgvInputList.CurrentCell = this.dgvInputList[1, i];
                            this.txtGridEdit.SelectAll();
                            return false;
                        }
                    }
                }

                // 商品コード
                if (ValChk.IsEmpty(this.dgvInputList[2, i].Value))
                {
                    if (this.dgvInputList.RowCount > i + 1)
                    {
                        Msg.Error("入力に誤りがあります。");
                        this.dgvInputList.CurrentCell = this.dgvInputList[2, i];
                        this.txtGridEdit.SelectAll();
                        return false;
                    }
                }
                else
                {
                    // Han.VI_商品(VI_HN_SHOHIN)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(this.dgvInputList[2, i].Value));
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "*",
                        "VI_HN_SHOHIN",
                        "KAISHA_CD = @KAISHA_CD AND BARCODE1 != 999 AND SHOHIN_KUBUN5 <> 1 AND SHOHIN_CD = @SHOHIN_CD",
                        dpc);
                    if (dtCheck.Rows.Count == 0)
                    {
                        if (this.dgvInputList.RowCount > i + 1)
                        {
                            Msg.Error("入力に誤りがあります。");
                            this.dgvInputList.CurrentCell = this.dgvInputList[2, i];
                            this.txtGridEdit.SelectAll();
                            return false;
                        }

                        // ZAM01025.VI_部門(VI_ZM_BUMON)
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                        dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[12, i].Value));
                        dtCheck = this.Dba.GetDataTableByConditionWithParams(
                            "*",
                            "VI_ZM_BUMON",
                            "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD",
                            dpc);
                        if (dtCheck.Rows.Count == 0)
                        {
                            if (this.dgvInputList.RowCount > i + 1)
                            {
                                Msg.Error("入力に誤りがあります。");
                                this.dgvInputList.CurrentCell = this.dgvInputList[2, i];
                                this.txtGridEdit.SelectAll();
                                return false;
                            }
                        }
                    }
                }

                i++;
            }

            return true;
        }

        /// <summary>
        /// 登録処理
        /// </summary>
        /// <returns>
        /// 登録した伝票番号を返す
        /// </returns>
        private Decimal UpdateData()
        {
            DbParamCollection dpc;
            DataTable dtCheck;
            DbParamCollection updParam;
            DbParamCollection whereParam;

            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            Decimal DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoNo.Text);
            int i;

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 伝票番号確定処理
                
                // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                dtCheck = this.Dba.GetDataTableByConditionWithParams(
                    "COUNT(*) AS CNT",
                    "TB_HN_TORIHIKI_DENPYO",
                    "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    dpc);
                if (Util.ToLong(dtCheck.Rows[0]["CNT"]) == 0)
                {
                    // 仕入伝票新規作成
                    DENPYO_BANGO = Util.ToDecimal(this.Dba.GetHNDenpyoNo(this.UInfo, 1, 0));

                    this.Dba.UpdateHNDenpyoNo(this.UInfo, 1, 0, Util.ToInt(DENPYO_BANGO));
                }
                else
                {
                    // 仕入伝票修正時の在庫マスタ調整

                    // 伝票明細取得
                    // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                    DataTable dtTB_HN_TORIHIKI_MEISAI = this.Dba.GetDataTableByConditionWithParams(
                        "M.SHOHIN_CD,SUM(M.SURYO1) AS SURYO1,SUM(M.SURYO2) AS SURYO2, D.TORIHIKI_KUBUN2",
                        "TB_HN_TORIHIKI_MEISAI AS M LEFT OUTER JOIN TB_HN_TORIHIKI_DENPYO AS D ON M.KAISHA_CD = D.KAISHA_CD AND M.DENPYO_KUBUN = D.DENPYO_KUBUN AND  M.DENPYO_BANGO = D.DENPYO_BANGO AND M.KAIKEI_NENDO = D.KAIKEI_NENDO",
                        "M.KAISHA_CD = @KAISHA_CD AND M.KAIKEI_NENDO = @KAIKEI_NENDO AND M.DENPYO_KUBUN = @DENPYO_KUBUN AND M.DENPYO_BANGO = @DENPYO_BANGO",
                        "M.SHOHIN_CD ASC",
                        "M.SHOHIN_CD,D.TORIHIKI_KUBUN2",
                        dpc);
                    if (dtTB_HN_TORIHIKI_MEISAI.Rows.Count > 0)
                    {
                        i = 0;
                        while (dtTB_HN_TORIHIKI_MEISAI.Rows.Count > i)
                        {
                            // 在庫マスタ登録
                            // HAN.TB_在庫(TB_HN_ZAIKO)
                            dpc = new DbParamCollection();
                            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_CD"]));
                            dpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, 0);
                            dtCheck = this.Dba.GetDataTableByConditionWithParams(
                                "*",
                                "TB_HN_ZAIKO",
                                "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD",
                                dpc);
                            if (dtCheck.Rows.Count == 0)
                            {
                                updParam = new DbParamCollection();

                                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                                updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_CD"]));
                                updParam.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, 0);
                                // 伝票の取引区分２が『2』は返品処理
                                if (Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["TORIHIKI_KUBUN2"]) == 2)
                                {
                                    updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"]) * -1);
                                    updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"]) * -1);
                                }
                                else
                                {
                                    updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"]));
                                    updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"]));
                                }
                                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                                this.Dba.Insert("TB_HN_ZAIKO", updParam);
                            }
                            else
                            {
                                updParam = new DbParamCollection();
                                whereParam = new DbParamCollection();

                                // 伝票の取引区分２が『2』は返品処理
                                if (Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["TORIHIKI_KUBUN2"]) == 2)
                                {
                                    updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO1"]) - Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"]));
                                    updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO2"]) - Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"]));
                                }
                                else
                                {
                                    updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO1"]) + Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"]));
                                    updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO2"]) + Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"]));
                                }
                                updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                                whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_CD"]));
                                whereParam.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, 0);

                                this.Dba.Update("TB_HN_ZAIKO",
                                                updParam,
                                                "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD",
                                                whereParam
                                                );
                            }

                            i++;
                        }
                    }

                }

                // 伝票明細削除
                // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                this.Dba.Delete("TB_HN_TORIHIKI_MEISAI",
                    "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    whereParam);

                // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                dtCheck = this.Dba.GetDataTableByConditionWithParams(
                    "COUNT(*) AS CNT",
                    "TB_HN_TORIHIKI_DENPYO",
                    "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    dpc);
                if ((int)dtCheck.Rows[0]["CNT"] == 0)
                {
                    // 伝票登録
                    updParam = new DbParamCollection();

                    updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                    updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                    updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                    updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DENPYO_DATE);
                    updParam.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 3, Util.ToDecimal(this.txtTorihikiKubunCd.Text.Substring(0, 1)));
                    updParam.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 3, Util.ToDecimal(this.txtTorihikiKubunCd.Text.Substring(1, 1)));
                    updParam.SetParam("@KAIIN_BANGO", SqlDbType.VarChar, 4, this.txtFunanushiCd.Text);
                    updParam.SetParam("@KAIIN_NM", SqlDbType.VarChar, 30, this.lblFunanushiNm.Text);
                    updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
                    updParam.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 8, 0);
                    updParam.SetParam("@TENPO_CD", SqlDbType.VarChar, 10, "0");
                    updParam.SetParam("@URIAGE_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyokei.Text));
                    updParam.SetParam("@SHOHIZEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyohiZei.Text));
                    updParam.SetParam("@GENKIN_NYUKINGAKU", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@NEBIKIGAKU", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@NYUKIN_GOKEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalGaku.Text));
                    updParam.SetParam("@OTSURI", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@TOROKU_JIKAN", SqlDbType.DateTime, DateTime.Now);
                    updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                    this.Dba.Insert("TB_HN_TORIHIKI_DENPYO", updParam);
                }
                else
                {
                    // 伝票更新
                    updParam = new DbParamCollection();
                    whereParam = new DbParamCollection();

                    updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DENPYO_DATE);
                    updParam.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 3, Util.ToDecimal(txtTorihikiKubunCd.Text.Substring(0, 1)));
                    updParam.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 3, Util.ToDecimal(txtTorihikiKubunCd.Text.Substring(1, 1)));
                    updParam.SetParam("@KAIIN_BANGO", SqlDbType.VarChar, 4, this.txtFunanushiCd.Text);
                    updParam.SetParam("@KAIIN_NM", SqlDbType.VarChar, 30, this.lblFunanushiNm.Text);
                    updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
                    updParam.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 8, 0);
                    updParam.SetParam("@TENPO_CD", SqlDbType.VarChar, 10, "0");
                    updParam.SetParam("@URIAGE_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyokei.Text));
                    updParam.SetParam("@SHOHIZEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyohiZei.Text));
                    updParam.SetParam("@GENKIN_NYUKINGAKU", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@NEBIKIGAKU", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@NYUKIN_GOKEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalGaku.Text));
                    updParam.SetParam("@OTSURI", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                    whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                    whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                    whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);

                    this.Dba.Update("TB_HN_TORIHIKI_DENPYO",
                                    updParam,
                                    "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                                    whereParam
                                    );
                }

                // 伝票明細(DataGridView[name=dgvInputList]を取得)
                i = 0;
                while (this.dgvInputList.RowCount - 1 > i)
                {
                    // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                    dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[0, i].Value));
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "COUNT(*) AS CNT",
                        "TB_HN_TORIHIKI_MEISAI",
                        "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = 1 AND DENPYO_BANGO = @DENPYO_BANGO AND GYO_BANGO = @GYO_BANGO",
                        dpc);
                    if ((int)dtCheck.Rows[0]["CNT"] == 0)
                    {
                        // 伝票明細登録
                        updParam = new DbParamCollection();

                        updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                        updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                        updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                        updParam.SetParam("@GYO_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[0, i].Value));
                        updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(this.dgvInputList[2, i].Value));
                        updParam.SetParam("@SHOHIN_NM", SqlDbType.VarChar, 40, Util.ToString(this.dgvInputList[3, i].Value));
                        updParam.SetParam("@TENPO_CD", SqlDbType.VarChar, 10, "0");
                        updParam.SetParam("@IRISU", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[4, i].Value)));
                        updParam.SetParam("@TANI", SqlDbType.VarChar, 4, Util.ToString(this.dgvInputList[5, i].Value));
                        updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[6, i].Value)));
                        updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[7, i].Value)));
                        updParam.SetParam("@KUBUN", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[1, i].Value)));
                        updParam.SetParam("@GEN_TANKA", SqlDbType.Decimal, 13, Util.ToDecimal(Util.ToString(this.dgvInputList[16, i].Value)));
                        updParam.SetParam("@GENKA_KINGAKU", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[16, i].Value)) * Util.ToDecimal(Util.ToString(this.dgvInputList[7, i].Value)));
                        updParam.SetParam("@URI_TANKA", SqlDbType.Decimal, 13, Util.ToDecimal(Util.ToString(this.dgvInputList[8, i].Value)));
                        updParam.SetParam("@BAIKA_KINGAKU", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[9, i].Value)));
                        updParam.SetParam("@HANBAI_TANKA", SqlDbType.Decimal, 13, 0);
                        updParam.SetParam("@HANBAI_KINGAKU", SqlDbType.Decimal, 14, 0);
                        updParam.SetParam("@SHOHIZEI", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[10, i].Value)));
                        updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 3, this.UriageInfo.SHOHIZEI_NYURYOKU_HOHO);
                        updParam.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[11, i].Value)));
                        updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[12, i].Value)));
                        updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, Util.ToDecimal(Util.ToString(this.dgvInputList[13, i].Value)));
                        updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 3, Util.ToDecimal(Util.ToString(this.dgvInputList[14, i].Value)));
                        updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                        this.Dba.Insert("TB_HN_TORIHIKI_MEISAI", updParam);
                    }

                    // 在庫マスタ登録
                    // HAN.TB_在庫(TB_HN_ZAIKO)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, this.dgvInputList[2, i].Value);
                    dpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, 0);
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "*",
                        "TB_HN_ZAIKO",
                        "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD",
                        dpc);
                    if (dtCheck.Rows.Count == 0)
                    {
                        updParam = new DbParamCollection();

                        updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(this.dgvInputList[2, i].Value));
                        updParam.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, 0);
                        // 伝票の取引区分２が『2』は返品処理
                        if (Util.ToDecimal(txtTorihikiKubunCd.Text.Substring(1, 1)) == 2)
                        {
                            updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[6, i].Value)));
                            updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[7, i].Value)));
                        }
                        else
                        {
                            updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[6, i].Value)) * -1);
                            updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[7, i].Value)) * -1);
                        }
                        // TODO:ここで最終仕入日付と最終仕入単価の更新はいらない気がする
                        updParam.SetParam("@SAISHU_SHIIRE_DATE", SqlDbType.DateTime, DENPYO_DATE);
                        updParam.SetParam("@SAISHU_SHIIRE_TANKA", SqlDbType.Decimal, 13, Util.ToDecimal(Util.ToString(this.dgvInputList[8, i].Value)));
                        updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                        this.Dba.Insert("TB_HN_ZAIKO", updParam);
                    }
                    else
                    {
                        updParam = new DbParamCollection();
                        whereParam = new DbParamCollection();

                        // 伝票の取引区分２が『2』は返品処理
                        if (Util.ToDecimal(txtTorihikiKubunCd.Text.Substring(1, 1)) == 2)
                        {
                            updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO1"]) + Util.ToDecimal(Util.ToString(this.dgvInputList[6, i].Value)));
                            updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO2"]) + Util.ToDecimal(Util.ToString(this.dgvInputList[7, i].Value)));
                        }
                        else
                        {
                            updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO1"]) - Util.ToDecimal(Util.ToString(this.dgvInputList[6, i].Value)));
                            updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO2"]) - Util.ToDecimal(Util.ToString(this.dgvInputList[7, i].Value)));
                        }
                        // TODO:ここで最終仕入日付と最終仕入単価の更新はいらない気がする
                        updParam.SetParam("@SAISHU_SHIIRE_DATE", SqlDbType.DateTime, DENPYO_DATE);
                        updParam.SetParam("@SAISHU_SHIIRE_TANKA", SqlDbType.Decimal, 13, Util.ToDecimal(Util.ToString(this.dgvInputList[8, i].Value)));
                        updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                        whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, this.dgvInputList[2, i].Value);

                        this.Dba.Update("TB_HN_ZAIKO",
                                        updParam,
                                        "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD",
                                        whereParam
                                        );
                    }

                    // 売上単価履歴マスタ登録
                    // HAN.TB_売上単価履歴(TB_HN_URIAGE_TANKA_RIREKI)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, 0);
                    dpc.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
                    dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 8, Util.ToDecimal(this.dgvInputList[2, i].Value));
                    dpc.SetParam("@TANKA", SqlDbType.Decimal, 8, Util.ToDecimal(Util.ToString(this.dgvInputList[8, i].Value)));
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "*",
                        "TB_HN_URIAGE_TANKA_RIREKI",
                        "KAISHA_CD = @KAISHA_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND TANKA = @TANKA",
                        dpc);
                    if (dtCheck.Rows.Count == 0)
                    {
                        updParam = new DbParamCollection();

                        updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        updParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, 0);
                        updParam.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
                        updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 8, Util.ToDecimal(Util.ToString(this.dgvInputList[2, i].Value)));
                        updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        updParam.SetParam("@TANKA", SqlDbType.Decimal, 8, Util.ToDecimal(Util.ToString(this.dgvInputList[8, i].Value)));
                        updParam.SetParam("@SAISHU_TORIHIKI_DATE", SqlDbType.DateTime, DENPYO_DATE);
                        updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                        this.Dba.Insert("TB_HN_URIAGE_TANKA_RIREKI", updParam);
                    }
                    else
                    {
                        updParam = new DbParamCollection();
                        whereParam = new DbParamCollection();

                        updParam.SetParam("@SAISHU_TORIHIKI_DATE", SqlDbType.DateTime, DENPYO_DATE);
                        updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                        whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        whereParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, 0);
                        whereParam.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
                        whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 8, Util.ToDecimal(this.dgvInputList[2, i].Value));
                        whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        whereParam.SetParam("@TANKA", SqlDbType.Decimal, 8, Util.ToDecimal(Util.ToString(this.dgvInputList[8, i].Value)));

                        this.Dba.Update("TB_HN_URIAGE_TANKA_RIREKI",
                                        updParam,
                                        "KAISHA_CD = @KAISHA_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD AND TANKA = @TANKA",
                                        whereParam
                                        );
                    }
                    
                    i++;
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            this.lblBeforeDenpyoNo.Text = "【前回" + (this.MODE_EDIT == 1 ? "登録" : "更新") + "伝票番号：" + DENPYO_BANGO.ToString().PadLeft(6) + "】";
            this.lblBeforeDenpyoNo.Visible = true;

            return DENPYO_BANGO;
        }

        /// <summary>
        /// 削除処理
        /// </summary>
        private void DeleteData()
        {
            DbParamCollection dpc;
            DataTable dtCheck;
            DbParamCollection updParam;
            DbParamCollection whereParam;

            Decimal DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoNo.Text);
            int i;

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 仕入伝票削除時の在庫マスタ調整

                // 伝票明細取得
                // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                DataTable dtTB_HN_TORIHIKI_MEISAI = this.Dba.GetDataTableByConditionWithParams(
                    "M.SHOHIN_CD,SUM(M.SURYO1) AS SURYO1,SUM(M.SURYO2) AS SURYO2, D.TORIHIKI_KUBUN2",
                    "TB_HN_TORIHIKI_MEISAI AS M LEFT OUTER JOIN TB_HN_TORIHIKI_DENPYO AS D ON M.KAISHA_CD = D.KAISHA_CD AND M.DENPYO_KUBUN = D.DENPYO_KUBUN AND  M.DENPYO_BANGO = D.DENPYO_BANGO AND M.KAIKEI_NENDO = D.KAIKEI_NENDO",
                    "M.KAISHA_CD = @KAISHA_CD AND M.KAIKEI_NENDO = @KAIKEI_NENDO AND M.DENPYO_KUBUN = @DENPYO_KUBUN AND M.DENPYO_BANGO = @DENPYO_BANGO",
                    "M.SHOHIN_CD ASC",
                    "M.SHOHIN_CD,D.TORIHIKI_KUBUN2",
                    dpc);
                if (dtTB_HN_TORIHIKI_MEISAI.Rows.Count > 0)
                {
                    i = 0;
                    while (dtTB_HN_TORIHIKI_MEISAI.Rows.Count > i)
                    {
                        // 在庫マスタ登録
                        // HAN.TB_在庫(TB_HN_ZAIKO)
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_CD"]));
                        dpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, 0);
                        dtCheck = this.Dba.GetDataTableByConditionWithParams(
                            "*",
                            "TB_HN_ZAIKO",
                            "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD",
                            dpc);
                        if (dtCheck.Rows.Count == 0)
                        {
                            updParam = new DbParamCollection();

                            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                            updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_CD"]));
                            updParam.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, 0);
                            // 伝票の取引区分２が『2』は返品処理
                            if (Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["TORIHIKI_KUBUN2"]) == 2)
                            {
                                updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"]));
                                updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"]));
                            }
                            else
                            {
                                updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"]) * -1);
                                updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"]) * -1);
                            }
                            updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                            this.Dba.Insert("TB_HN_ZAIKO", updParam);
                        }
                        else
                        {
                            updParam = new DbParamCollection();
                            whereParam = new DbParamCollection();

                            // 伝票の取引区分２が『2』は返品処理
                            if (Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["TORIHIKI_KUBUN2"]) == 2)
                            {
                                updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO1"]) - Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"]));
                                updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO2"]) - Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"]));
                            }
                            else
                            {
                                updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO1"]) + Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"]));
                                updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(dtCheck.Rows[0]["SURYO2"]) + Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"]));
                            }
                            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                            whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_CD"]));
                            whereParam.SetParam("@SOKO_CD", SqlDbType.Decimal, 6, 0);

                            this.Dba.Update("TB_HN_ZAIKO",
                                            updParam,
                                            "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD",
                                            whereParam
                                            );
                        }

                        i++;
                    }
                }

                // 伝票削除
                // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                this.Dba.Delete("TB_HN_TORIHIKI_DENPYO",
                    "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    whereParam);

                // 伝票明細削除
                // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                this.Dba.Delete("TB_HN_TORIHIKI_MEISAI",
                    "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            this.lblBeforeDenpyoNo.Text = "【前回削除伝票伝票番号：" + DENPYO_BANGO.ToString().PadLeft(6) + "】";
            this.lblBeforeDenpyoNo.Visible = true;
        }

        /// <summary>
        /// 商品コードから商品情報取得
        /// </summary>
        /// <param name="input_type">1:手入力,2:一覧画面から取得</param>
        private void GetShohinInfo(int input_type)
        {
            DbParamCollection dpc;
            DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;

            // 商品情報取得
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 15, this.txtGridEdit.Text);
            DataTable dtVI_HN_SHOHIN = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_HN_SHOHIN",
                "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND BARCODE1 != 999 AND SHOHIN_KUBUN5 <> 1",
                dpc);
            if (dtVI_HN_SHOHIN.Rows.Count == 0)
            {
                this.dgvInputList[2, dgvCell.RowIndex].Value = "";
                this.dgvInputList[3, dgvCell.RowIndex].Value = "";
                this.dgvInputList[11, dgvCell.RowIndex].Value = "";
                this.dgvInputList[12, dgvCell.RowIndex].Value = "";
                this.dgvInputList[13, dgvCell.RowIndex].Value = "";
                this.dgvInputList[14, dgvCell.RowIndex].Value = "";
                this.dgvInputList[15, dgvCell.RowIndex].Value = "";
                Msg.Error("入力に誤りがあります。");
                this.txtGridEdit.Focus();
                return;
            }
            else
            {
                if (Util.ToString(this.dgvInputList[2, dgvCell.RowIndex].Value) != this.txtGridEdit.Text)
                {
                    // 商品コード設定
                    this.dgvInputList[2, dgvCell.RowIndex].Value = txtGridEdit.Text;
                    // 商品名設定
                    this.dgvInputList[3, dgvCell.RowIndex].Value = dtVI_HN_SHOHIN.Rows[0]["SHOHIN_NM"];
                    // 単価設定
                    if (ValChk.IsEmpty(dgvInputList[8, dgvCell.RowIndex].Value))
                    {
                        // 売単価
                        this.dgvInputList[8, dgvCell.RowIndex].Value = Util.FormatNum(dtVI_HN_SHOHIN.Rows[0]["KORI_TANKA"], 1);
                    }
                    // 原単価
                    this.dgvInputList[16, dgvCell.RowIndex].Value = Util.FormatNum(dtVI_HN_SHOHIN.Rows[0]["SHIIRE_TANKA"], 1);
                    // 仕訳コード
                    this.dgvInputList[11, dgvCell.RowIndex].Value = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["URIAGE_SHIWAKE_CD"]);
                    // 部門コード
                    this.dgvInputList[12, dgvCell.RowIndex].Value = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["URIAGE_BUMON_CD"]);
                    // 税区分
                    this.dgvInputList[13, dgvCell.RowIndex].Value = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["URIAGE_ZEI_KUBUN"]);
                    // 事業区分
                    this.dgvInputList[14, dgvCell.RowIndex].Value = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["URIAGE_JIGYO_KUBUN"]);

                    // 売上税区分チェック
                    if (!ValChk.IsEmpty(dtVI_HN_SHOHIN.Rows[0]["URIAGE_ZEI_KUBUN"]))
                    {
                        // zam.TB_Ｆ税区分(TB_ZM_F_ZEI_KUBUN)
                        // 消費税率を取得
                        dpc = new DbParamCollection();
                        dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, dtVI_HN_SHOHIN.Rows[0]["URIAGE_ZEI_KUBUN"]);
                        DataTable dtTB_ZM_F_ZEI_KUBUN = this.Dba.GetDataTableByConditionWithParams(
                            "*",
                            "TB_ZM_F_ZEI_KUBUN",
                            "ZEI_KUBUN = @ZEI_KUBUN",
                            dpc);
                        // 税率
                        DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                                                               this.txtMonth.Text, this.txtDay.Text, this.Dba);
                        this.dgvInputList[15, dgvCell.RowIndex].Value = this.GetZeiritsu(DENPYO_DATE);
                        // 課税区分
                        this.dgvInputList[17, dgvCell.RowIndex].Value = Util.ToString(dtTB_ZM_F_ZEI_KUBUN.Rows[0]["KAZEI_KUBUN"]);
                    }
                    else
                    {
                        this.dgvInputList[15, dgvCell.RowIndex].Value = "";
                        this.dgvInputList[17, dgvCell.RowIndex].Value = 0;
                    }

                    if (this.dgvInputList.RowCount == dgvCell.RowIndex + 1)
                    {
                        // 行追加
                        //this.dgvInputList.Rows.Add();
                        this.dgvInputList.RowCount = this.dgvInputList.RowCount + 1;
                        this.dgvInputList[0, this.dgvInputList.RowCount - 1].Value = this.dgvInputList.RowCount;
                    }
                }
                if (input_type == 1)
                {
                    this.dgvInputList.CurrentCell = this.dgvInputList[7, dgvCell.RowIndex];
                }
            }

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 伝票データチェック後、伝票修正処理へ
        /// </summary>
        /// <returns>イベントをキャンセルする真偽値(真:キャンセル,偽:続行)</param>
        private bool GetDenpyoData()
        {
            DbParamCollection dpc;
            DataTable dtTB_HN_TORIHIKI_DENPYO;
            dpc = new DbParamCollection();
            // 仕入伝票情報確認
            // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(this.txtDenpyoNo.Text));
            dtTB_HN_TORIHIKI_DENPYO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_TORIHIKI_DENPYO",
                "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                dpc);

            if (dtTB_HN_TORIHIKI_DENPYO.Rows.Count == 0)
            {
                Msg.Error("入力に誤りがあります。");
                this.txtDenpyoNo.SelectAll();
                return true;
            }
            // 既に修正モードへ切り替え時には処理しない
            if (this.MEISAI_DENPYO_BANGO != Util.ToDecimal(this.txtDenpyoNo.Text))
            {
                this.MEISAI_DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoNo.Text);
                // 修正モードへ切り替え
                this.InitDispOnEdit(dtTB_HN_TORIHIKI_DENPYO);
            }
            return false;
        }

        /// <summary>
        /// 伝票明細エンターキー以外の処理
        /// </summary>
        /// <returns>エンターキーを押したかの真偽値(真:押した,偽:押してない)</param>
        private bool GetDenpyoMeisaiKeyEnter(Keys KeyCode, DataGridViewCell dgvCell)
        {
            if (KeyCode == Keys.Enter)
            {
                return true;
            }
            if (KeyCode == Keys.Down)
            {
                if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
                {
                    this.dgvInputList.CurrentCell = dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex + 1];
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (dgvCell.RowIndex > 0)
                {
                    this.dgvInputList.CurrentCell = dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex - 1];
                }
                else if (dgvCell.RowIndex == 0)
                {
                    this.txtTantoshaCd.Focus();
                }
            }
            return false;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        /// <param name="DENPYO_BANGO">伝票番号</param>
        /// <param name="isLayOut">印刷レイアウト(1:通常、2:位置合わせ用)</param>
        private void DoPrint(bool isPreview, Decimal DENPYO_BANGO, int isLayOut)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData(DENPYO_BANGO);

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");
                    cols.Append(" ,ITEM28");
                    cols.Append(" ,ITEM29");
                    cols.Append(" ,ITEM30");
                    cols.Append(" ,ITEM31");
                    cols.Append(" ,ITEM32");
                    cols.Append(" ,ITEM33");
                    cols.Append(" ,ITEM34");
                    cols.Append(" ,ITEM35");
                    cols.Append(" ,ITEM36");
                    cols.Append(" ,ITEM37");
                    cols.Append(" ,ITEM38");
                    cols.Append(" ,ITEM39");
                    cols.Append(" ,ITEM40");
                    cols.Append(" ,ITEM41");
                    cols.Append(" ,ITEM42");
                    cols.Append(" ,ITEM43");
                    cols.Append(" ,ITEM44");
                    cols.Append(" ,ITEM45");
                    cols.Append(" ,ITEM46");
                    cols.Append(" ,ITEM47");
                    cols.Append(" ,ITEM48");
                    cols.Append(" ,ITEM49");
                    cols.Append(" ,ITEM50");
                    cols.Append(" ,ITEM51");
                    cols.Append(" ,ITEM52");
                    cols.Append(" ,ITEM53");
                    cols.Append(" ,ITEM54");
                    cols.Append(" ,ITEM55");
                    cols.Append(" ,ITEM56");
                    cols.Append(" ,ITEM57");
                    cols.Append(" ,ITEM58");
                    cols.Append(" ,ITEM59");
                    cols.Append(" ,ITEM60");
                    cols.Append(" ,ITEM61");
                    cols.Append(" ,ITEM62");
                    cols.Append(" ,ITEM63");
                    cols.Append(" ,ITEM64");
                    cols.Append(" ,ITEM65");
                    cols.Append(" ,ITEM66");
                    cols.Append(" ,ITEM67");
                    cols.Append(" ,ITEM68");
                    cols.Append(" ,ITEM69");
                    cols.Append(" ,ITEM70");
                    cols.Append(" ,ITEM71");
                    cols.Append(" ,ITEM72");
                    cols.Append(" ,ITEM73");
                    cols.Append(" ,ITEM74");
                    cols.Append(" ,ITEM75");
                    cols.Append(" ,ITEM76");
                    cols.Append(" ,ITEM77");
                    cols.Append(" ,ITEM78");
                    cols.Append(" ,ITEM79");
                    cols.Append(" ,ITEM80");
                    cols.Append(" ,ITEM81");
                    cols.Append(" ,ITEM82");
                    cols.Append(" ,ITEM83");
                    cols.Append(" ,ITEM84");
                    cols.Append(" ,ITEM85");
                    cols.Append(" ,ITEM86");
                    cols.Append(" ,ITEM87");
                    cols.Append(" ,ITEM88");
                    cols.Append(" ,ITEM89");
                    cols.Append(" ,ITEM90");
                    cols.Append(" ,ITEM91");
                    cols.Append(" ,ITEM92");
                    cols.Append(" ,ITEM93");
                    cols.Append(" ,ITEM94");
                    cols.Append(" ,ITEM95");
                    cols.Append(" ,REGIST_DATE");


                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 印刷レイアウト分岐
                    switch (isLayOut)
                    {
                        case 1:
                            // 帳票オブジェクトをインスタンス化
                            KOBE2013R rpt = new KOBE2013R(dtOutput);
                            
                            // プレビュー画面表示
                            PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                            pFrm.WindowState = FormWindowState.Maximized;
                            pFrm.TopMost = true;
                            pFrm.Show();
                            break;
                        case 2:
                            // 帳票オブジェクトをインスタンス化
                            KOBE2013_2R rpt2 = new KOBE2013_2R(dtOutput);

                            // プレビュー画面表示
                            PreviewForm pFrm2 = new PreviewForm(rpt2, this.UnqId);
                            pFrm2.WindowState = FormWindowState.Maximized;
                            pFrm2.TopMost = true;
                            pFrm2.Show();
                            break;
                    }

                }
                else
                {
                    Msg.Info("該当データがありません。");
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData(Decimal DENPYO_BANGO)
        {
            DbParamCollection dpc;

            // 明細項目ステップ
            Decimal Item_Step = 9;
            // 頁明細件数
            Decimal Page_Line = 8;
            // 現在頁
            Decimal Now_Page = 0;
            // 最終頁
            Decimal End_Page = 0;
            // 空文字
            string SPACE = "";
            // 伝票日付設定
            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            // 印刷日を和暦変換
            string[] jpPrintDate = Util.ConvJpDate(DateTime.Now ,this.Dba);

            StringBuilder sql;
            int i,j; // ループ用カウント変数

            #region メインループデータ取得準備
            // Han.TB_会社情報(TB_HN_KAISHA_JOHO)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            DataTable dtHN_KAISHA_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_KAISHA_JOHO",
                "KAISHA_CD = @KAISHA_CD",
                "KAISHA_CD",
                dpc);
            if (dtHN_KAISHA_JOHO.Rows.Count == 0)
            {
                return false;
            }

            // Han.VI_取引先情報(VI_HN_TORIHIKISAKI_JOHO)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtFunanushiCd.Text));
            DataTable dtHN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_HN_TORIHIKISAKI_JOHO",
                "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                dpc);
            if (dtHN_TORIHIKISAKI_JOHO.Rows.Count == 0)
            {
                return false;
            }

            // 伝票合計金額
            // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
            DataTable dtHN_TORIHIKI_DENPYO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_TORIHIKI_DENPYO",
                "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                dpc);

            if (dtHN_TORIHIKI_DENPYO.Rows.Count == 0)
            {
                return true;
            }

            // Han.VI_取引明細(VI_HN_TORIHIKI_MEISAI)からデータ取得
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append("    (CASE WHEN A.NOHINSAKI_CD = 0 THEN A.SEIKYUSAKI_NM ELSE A.TOKUISAKI_NM END) AS 社名,");
            sql.Append("    (CASE WHEN A.NOHINSAKI_CD = 0 THEN CASE WHEN A.KAIIN_BANGO = A.SEIKYUSAKI_CD THEN '' ELSE A.TOKUISAKI_NM END ELSE A.NOHINSAKI_NM END) AS 店名,");
            sql.Append("    ''                       AS 社店コード,");
            sql.Append("    ''                       AS 分類コード,");
            sql.Append("    ''                       AS 伝票区分,");
            sql.Append("    A.DENPYO_BANGO           AS 伝票番号,");
            sql.Append("    0                        AS 取引先コード,");
            sql.Append("    ''                       AS 発注日,");
            sql.Append("    A.DENPYO_DATE            AS 納品日,");
            sql.Append("    0                        AS 便,");
            sql.Append("    A.SHOHIN_CD              AS 商品コード,");
            sql.Append("    A.SHOHIN_NM              AS 商品名,");
            sql.Append("    A.KIKAKU                 AS 規格,");
            sql.Append("    ''                       AS 色,");
            sql.Append("    ''                       AS サイズ,");
            sql.Append("    A.IRISU                  AS 入数,");
            sql.Append("    A.SURYO1                 AS ケース,");
            sql.Append("    A.SURYO2                 AS バラ,");
            sql.Append("    A.TANI                   AS 単位,");
            sql.Append("    ((A.SURYO1 * A.IRISU) + A.SURYO2) AS 数量,");
            sql.Append("    0                        AS 訂正数量,");
            sql.Append("    ''                       AS 引合,");
            sql.Append("    A.URI_TANKA              AS 原単価,");
            sql.Append("    A.BAIKA_KINGAKU          AS 原価金額,");
            sql.Append("    A.HANBAI_TANKA           AS 売単価,");
            sql.Append("    A.HANBAI_KINGAKU         AS 売価金額,");
            sql.Append("    A.ZEI_KUBUN              AS 税区分,");
            sql.Append("    A.SHOHIZEI_NYURYOKU_HOHO AS 消費税入力方法,");
            sql.Append("    A.TEKIYO_NM              AS 摘要名,");
            sql.Append("    A.TORIHIKI_KUBUN_NM      AS 取引区分名称,");
            sql.Append("    A.TORIHIKI_KUBUN         AS 取引区分,");
            sql.Append("    A.TANTOSHA_NM            AS 担当者名,");
            sql.Append("    A.SEIKYUSAKI_NM          AS 集計名称,");
            sql.Append("    A.KAIIN_BANGO            AS 得意先コード,");
            sql.Append("    A.SEIKYUSAKI_CD          AS 請求先コード,");
            sql.Append("    A.NOHINSAKI_CD           AS 納品先コード,");
            sql.Append("    A.TOKUISAKI_NM           AS 得意先名,");
            sql.Append("    A.SEIKYUSAKI_NM          AS 請求先名,");
            sql.Append("    A.NOHINSAKI_NM           AS 納品先名,");
            sql.Append("    CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 THEN A.SHOHIZEI ELSE 0 END AS 内消費税,");
            sql.Append("    CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 THEN 0 ELSE A.SHOHIZEI END AS 外消費税 ");
            sql.Append("FROM ");
            sql.Append("    VI_HN_TORIHIKI_MEISAI AS A ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD      = @KAISHA_CD AND");
            sql.Append("    A.DENPYO_KUBUN   = @DENPYO_KUBUN AND");
            sql.Append("    A.KAIKEI_NENDO   = @KAIKEI_NENDO AND");
            sql.Append("    (A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_FR) AND");
            sql.Append("    (A.DENPYO_BANGO BETWEEN @DENPYO_BANGO_FR AND @DENPYO_BANGO_TO) AND");
            sql.Append("    (A.TANTOSHA_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO) AND");
            sql.Append("    A.KAIIN_BANGO    = @KAIIN_BANGO ");
            sql.Append("ORDER BY ");
            sql.Append("    A.KAISHA_CD ASC,");
            sql.Append("    A.DENPYO_DATE ASC,");
            sql.Append("    A.DENPYO_BANGO ASC,");
            sql.Append("    A.GYO_BANGO ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 1);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, DENPYO_DATE);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, DENPYO_DATE);
            dpc.SetParam("@DENPYO_BANGO_FR", SqlDbType.Decimal, 8, DENPYO_BANGO);
            dpc.SetParam("@DENPYO_BANGO_TO", SqlDbType.Decimal, 8, DENPYO_BANGO);
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
            dpc.SetParam("@KAIIN_BANGO", SqlDbType.VarChar, 4, this.txtFunanushiCd.Text);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                // 最終頁を格納
                End_Page = Math.Ceiling((Decimal)dtMainLoop.Rows.Count / Page_Line);

                #region 登録SQL
                sql = new StringBuilder();
                sql.Append("INSERT INTO PR_HN_TBL(");
                sql.Append("  GUID");
                sql.Append(" ,SORT");
                sql.Append(" ,ITEM01");
                sql.Append(" ,ITEM02");
                sql.Append(" ,ITEM03");
                sql.Append(" ,ITEM04");
                sql.Append(" ,ITEM05");
                sql.Append(" ,ITEM06");
                sql.Append(" ,ITEM07");
                sql.Append(" ,ITEM08");
                sql.Append(" ,ITEM09");
                sql.Append(" ,ITEM10");
                sql.Append(" ,ITEM11");
                sql.Append(" ,ITEM12");
                sql.Append(" ,ITEM13");
                sql.Append(" ,ITEM14");
                sql.Append(" ,ITEM15");
                sql.Append(" ,ITEM16");
                sql.Append(" ,ITEM17");
                sql.Append(" ,ITEM18");
                sql.Append(" ,ITEM19");
                sql.Append(" ,ITEM20");
                sql.Append(" ,ITEM21");
                sql.Append(" ,ITEM22");
                sql.Append(" ,ITEM23");
                sql.Append(" ,ITEM24");
                sql.Append(" ,ITEM25");
                sql.Append(" ,ITEM26");
                sql.Append(" ,ITEM27");
                sql.Append(" ,ITEM28");
                sql.Append(" ,ITEM29");
                sql.Append(" ,ITEM30");
                sql.Append(" ,ITEM31");
                sql.Append(" ,ITEM32");
                sql.Append(" ,ITEM33");
                sql.Append(" ,ITEM34");
                sql.Append(" ,ITEM35");
                sql.Append(" ,ITEM36");
                sql.Append(" ,ITEM37");
                sql.Append(" ,ITEM38");
                sql.Append(" ,ITEM39");
                sql.Append(" ,ITEM40");
                sql.Append(" ,ITEM41");
                sql.Append(" ,ITEM42");
                sql.Append(" ,ITEM43");
                sql.Append(" ,ITEM44");
                sql.Append(" ,ITEM45");
                sql.Append(" ,ITEM46");
                sql.Append(" ,ITEM47");
                sql.Append(" ,ITEM48");
                sql.Append(" ,ITEM49");
                sql.Append(" ,ITEM50");
                sql.Append(" ,ITEM51");
                sql.Append(" ,ITEM52");
                sql.Append(" ,ITEM53");
                sql.Append(" ,ITEM54");
                sql.Append(" ,ITEM55");
                sql.Append(" ,ITEM56");
                sql.Append(" ,ITEM57");
                sql.Append(" ,ITEM58");
                sql.Append(" ,ITEM59");
                sql.Append(" ,ITEM60");
                sql.Append(" ,ITEM61");
                sql.Append(" ,ITEM62");
                sql.Append(" ,ITEM63");
                sql.Append(" ,ITEM64");
                sql.Append(" ,ITEM65");
                sql.Append(" ,ITEM66");
                sql.Append(" ,ITEM67");
                sql.Append(" ,ITEM68");
                sql.Append(" ,ITEM69");
                sql.Append(" ,ITEM70");
                sql.Append(" ,ITEM71");
                sql.Append(" ,ITEM72");
                sql.Append(" ,ITEM73");
                sql.Append(" ,ITEM74");
                sql.Append(" ,ITEM75");
                sql.Append(" ,ITEM76");
                sql.Append(" ,ITEM77");
                sql.Append(" ,ITEM78");
                sql.Append(" ,ITEM79");
                sql.Append(" ,ITEM80");
                sql.Append(" ,ITEM81");
                sql.Append(" ,ITEM82");
                sql.Append(" ,ITEM83");
                sql.Append(" ,ITEM84");
                sql.Append(" ,ITEM85");
                sql.Append(" ,ITEM86");
                sql.Append(" ,ITEM87");
                sql.Append(" ,ITEM88");
                sql.Append(" ,ITEM89");
                sql.Append(" ,ITEM90");
                sql.Append(" ,ITEM91");
                sql.Append(" ,ITEM92");
                sql.Append(" ,ITEM93");
                sql.Append(" ,ITEM94");
                sql.Append(" ,ITEM95");
                sql.Append(" ,REGIST_DATE");
                sql.Append(") ");
                sql.Append("VALUES(");
                sql.Append("  @GUID");
                sql.Append(" ,@SORT");
                sql.Append(" ,@ITEM01");
                sql.Append(" ,@ITEM02");
                sql.Append(" ,@ITEM03");
                sql.Append(" ,@ITEM04");
                sql.Append(" ,@ITEM05");
                sql.Append(" ,@ITEM06");
                sql.Append(" ,@ITEM07");
                sql.Append(" ,@ITEM08");
                sql.Append(" ,@ITEM09");
                sql.Append(" ,@ITEM10");
                sql.Append(" ,@ITEM11");
                sql.Append(" ,@ITEM12");
                sql.Append(" ,@ITEM13");
                sql.Append(" ,@ITEM14");
                sql.Append(" ,@ITEM15");
                sql.Append(" ,@ITEM16");
                sql.Append(" ,@ITEM17");
                sql.Append(" ,@ITEM18");
                sql.Append(" ,@ITEM19");
                sql.Append(" ,@ITEM20");
                sql.Append(" ,@ITEM21");
                sql.Append(" ,@ITEM22");
                sql.Append(" ,@ITEM23");
                sql.Append(" ,@ITEM24");
                sql.Append(" ,@ITEM25");
                sql.Append(" ,@ITEM26");
                sql.Append(" ,@ITEM27");
                sql.Append(" ,@ITEM28");
                sql.Append(" ,@ITEM29");
                sql.Append(" ,@ITEM30");
                sql.Append(" ,@ITEM31");
                sql.Append(" ,@ITEM32");
                sql.Append(" ,@ITEM33");
                sql.Append(" ,@ITEM34");
                sql.Append(" ,@ITEM35");
                sql.Append(" ,@ITEM36");
                sql.Append(" ,@ITEM37");
                sql.Append(" ,@ITEM38");
                sql.Append(" ,@ITEM39");
                sql.Append(" ,@ITEM40");
                sql.Append(" ,@ITEM41");
                sql.Append(" ,@ITEM42");
                sql.Append(" ,@ITEM43");
                sql.Append(" ,@ITEM44");
                sql.Append(" ,@ITEM45");
                sql.Append(" ,@ITEM46");
                sql.Append(" ,@ITEM47");
                sql.Append(" ,@ITEM48");
                sql.Append(" ,@ITEM49");
                sql.Append(" ,@ITEM50");
                sql.Append(" ,@ITEM51");
                sql.Append(" ,@ITEM52");
                sql.Append(" ,@ITEM53");
                sql.Append(" ,@ITEM54");
                sql.Append(" ,@ITEM55");
                sql.Append(" ,@ITEM56");
                sql.Append(" ,@ITEM57");
                sql.Append(" ,@ITEM58");
                sql.Append(" ,@ITEM59");
                sql.Append(" ,@ITEM60");
                sql.Append(" ,@ITEM61");
                sql.Append(" ,@ITEM62");
                sql.Append(" ,@ITEM63");
                sql.Append(" ,@ITEM64");
                sql.Append(" ,@ITEM65");
                sql.Append(" ,@ITEM66");
                sql.Append(" ,@ITEM67");
                sql.Append(" ,@ITEM68");
                sql.Append(" ,@ITEM69");
                sql.Append(" ,@ITEM70");
                sql.Append(" ,@ITEM71");
                sql.Append(" ,@ITEM72");
                sql.Append(" ,@ITEM73");
                sql.Append(" ,@ITEM74");
                sql.Append(" ,@ITEM75");
                sql.Append(" ,@ITEM76");
                sql.Append(" ,@ITEM77");
                sql.Append(" ,@ITEM78");
                sql.Append(" ,@ITEM79");
                sql.Append(" ,@ITEM80");
                sql.Append(" ,@ITEM81");
                sql.Append(" ,@ITEM82");
                sql.Append(" ,@ITEM83");
                sql.Append(" ,@ITEM84");
                sql.Append(" ,@ITEM85");
                sql.Append(" ,@ITEM86");
                sql.Append(" ,@ITEM87");
                sql.Append(" ,@ITEM88");
                sql.Append(" ,@ITEM89");
                sql.Append(" ,@ITEM90");
                sql.Append(" ,@ITEM91");
                sql.Append(" ,@ITEM92");
                sql.Append(" ,@ITEM93");
                sql.Append(" ,@ITEM94");
                sql.Append(" ,@ITEM95");
                sql.Append(" ,@REGIST_DATE");
                sql.Append(") ");
                #endregion

                i = 0;
                int dbSORT = 1;

                while (dtMainLoop.Rows.Count > i)
                {
                    // 現在頁を格納
                    Now_Page = Math.Ceiling((Decimal)i / Page_Line);
                    if (Now_Page == 0)
                    {
                        Now_Page = 1;
                    }
                    else
                    {
                        Now_Page++;
                    }

                    #region 印刷ワークテーブルに登録

                    #region データ登録
                    dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.ToString(Util.ConvJpDate(DENPYO_DATE, this.Dba)[5])); // 伝票日付(和暦年月日)
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, Util.ToString(dtMainLoop.Rows[i]["伝票番号"])); // 伝票番号
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200,
                        "〒 " + Util.ToString(dtHN_TORIHIKISAKI_JOHO.Rows[0]["YUBIN_BANGO1"]) + " － " + Util.ToString(dtHN_TORIHIKISAKI_JOHO.Rows[0]["YUBIN_BANGO2"])); // 会員郵便番号
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, Util.ToString(dtHN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"])); // 会員名称
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.ToString(dtHN_TORIHIKISAKI_JOHO.Rows[0]["DENWA_BANGO"])); // 会員TEL
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.ToString(dtHN_TORIHIKISAKI_JOHO.Rows[0]["FAX_BANGO"])); // 会員FAX
                    dpc.SetParam("@ITEM94", SqlDbType.VarChar, 200, Util.ToString(dtHN_TORIHIKISAKI_JOHO.Rows[0]["JUSHO1"])); // 会員住所1
                    dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, Util.ToString(dtHN_TORIHIKISAKI_JOHO.Rows[0]["JUSHO2"])); // 会員住所2
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.ToString(dtHN_KAISHA_JOHO.Rows[0]["KAISHA_NM1"])); // 組合住所
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.ToString(dtHN_KAISHA_JOHO.Rows[0]["KAISHA_NM2"])); // 組合名称
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.ToString(dtHN_KAISHA_JOHO.Rows[0]["KAISHA_NM3"])); // 組合TEL
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.ToString(dtHN_KAISHA_JOHO.Rows[0]["JISHA_PR"])); // 組合FAX
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.ToString("《 " + dtMainLoop.Rows[i]["取引区分名称"] + " 》")); // 取引区分名称
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.ToString(dtMainLoop.Rows[i]["担当者名"])); // 担当者名
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.ToString(Now_Page)); // ページ
                    
                    // 合計金額を設定
                    if (Now_Page == End_Page)
                    {
                        dpc.SetParam("@ITEM86", SqlDbType.VarChar, 200, Util.ToString("（非課税売上）")); // （非課税売上）*TITLE
                        dpc.SetParam("@ITEM87", SqlDbType.VarChar, 200, Util.ToString("（課税売上）")); // （課税売上）  *TITLE
                        dpc.SetParam("@ITEM88", SqlDbType.VarChar, 200, Util.ToString("内消費税")); // 内消費税      *TITLE
                        dpc.SetParam("@ITEM89", SqlDbType.VarChar, 200, Util.ToString("合計")); // 合計          *TITLE
                        dpc.SetParam("@ITEM90", SqlDbType.VarChar, 200, Util.ToString("0")); // （非課税売上）*VALUE
                        dpc.SetParam("@ITEM91", SqlDbType.VarChar, 200, Util.FormatNum(dtHN_TORIHIKI_DENPYO.Rows[0]["URIAGE_KINGAKU"])); // （課税売上）  *VALUE
                        dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, Util.FormatNum(dtHN_TORIHIKI_DENPYO.Rows[0]["SHOHIZEIGAKU"])); // 内消費税      *VALUE
                        dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, Util.FormatNum(dtHN_TORIHIKI_DENPYO.Rows[0]["NYUKIN_GOKEIGAKU"])); // 合計          *VALUE
                    }
                    else
                    {
                        dpc.SetParam("@ITEM86", SqlDbType.VarChar, 200, SPACE); // （非課税売上）*TITLE
                        dpc.SetParam("@ITEM87", SqlDbType.VarChar, 200, SPACE); // （課税売上）  *TITLE
                        dpc.SetParam("@ITEM88", SqlDbType.VarChar, 200, SPACE); // 内消費税      *TITLE
                        dpc.SetParam("@ITEM89", SqlDbType.VarChar, 200, SPACE); // 合計          *TITLE
                        dpc.SetParam("@ITEM90", SqlDbType.VarChar, 200, SPACE); // （非課税売上）*VALUE
                        dpc.SetParam("@ITEM91", SqlDbType.VarChar, 200, SPACE); // （課税売上）  *VALUE
                        dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, SPACE); // 内消費税      *VALUE
                        dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, SPACE); // 合計          *VALUE
                    }
                    // 明細を設定
                    j = 0;
                    while (Page_Line > j)
                    {
                        if (dtMainLoop.Rows.Count > i + j)
                        {
                            dpc.SetParam(string.Format("@ITEM{0}", (14 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.ToString(dtMainLoop.Rows[i + j]["商品コード"])); // コード
                            dpc.SetParam(string.Format("@ITEM{0}", (15 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.ToString(dtMainLoop.Rows[i + j]["商品名"])); // 商品名
                            dpc.SetParam(string.Format("@ITEM{0}", (16 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.ToString(dtMainLoop.Rows[i + j]["規格"])); // 規格
                            dpc.SetParam(string.Format("@ITEM{0}", (17 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.ToString(dtMainLoop.Rows[i + j]["単位"])); // 単位
                            dpc.SetParam(string.Format("@ITEM{0}", (18 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.ToDecimal(dtMainLoop.Rows[i + j]["入数"]) > 0 ? Util.FormatNum(dtMainLoop.Rows[i + j]["入数"], 1) : ""); // 入数
                            dpc.SetParam(string.Format("@ITEM{0}", (19 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.ToDecimal(dtMainLoop.Rows[i + j]["ケース"]) > 0 ? Util.FormatNum(dtMainLoop.Rows[i + j]["ケース"], 1) : ""); // ケース
                            dpc.SetParam(string.Format("@ITEM{0}", (20 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.ToDecimal(dtMainLoop.Rows[i + j]["バラ"]) > 0 ? Util.FormatNum(dtMainLoop.Rows[i + j]["バラ"], 2) : ""); // バラ
                            if (IsDecimal(Util.ToDecimal(dtMainLoop.Rows[i + j]["原単価"]))) //原単価が小数を含むかどうか
                            {

                                dpc.SetParam(string.Format("@ITEM{0}", (21 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.ToDecimal(Util.FormatNum(dtMainLoop.Rows[i + j]["原単価"], 2))); // 第２位まで表示
                            }
                            else
                            {
                                dpc.SetParam(string.Format("@ITEM{0}", (21 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i + j]["原単価"])); // 整数で表示
                            }
                            dpc.SetParam(string.Format("@ITEM{0}", (22 + (j * Item_Step))), SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i + j]["原価金額"])); // 原価金額
                        }
                        else
                        {
                            dpc.SetParam(string.Format("@ITEM{0}", (14 + (j * Item_Step))), SqlDbType.VarChar, 200, SPACE); // コード
                            dpc.SetParam(string.Format("@ITEM{0}", (15 + (j * Item_Step))), SqlDbType.VarChar, 200, SPACE); // 商品名
                            dpc.SetParam(string.Format("@ITEM{0}", (16 + (j * Item_Step))), SqlDbType.VarChar, 200, SPACE); // 規格
                            dpc.SetParam(string.Format("@ITEM{0}", (17 + (j * Item_Step))), SqlDbType.VarChar, 200, SPACE); // 単位
                            dpc.SetParam(string.Format("@ITEM{0}", (18 + (j * Item_Step))), SqlDbType.VarChar, 200, SPACE); // 入数
                            dpc.SetParam(string.Format("@ITEM{0}", (19 + (j * Item_Step))), SqlDbType.VarChar, 200, SPACE); // ケース
                            dpc.SetParam(string.Format("@ITEM{0}", (20 + (j * Item_Step))), SqlDbType.VarChar, 200, SPACE); // バラ
                            dpc.SetParam(string.Format("@ITEM{0}", (21 + (j * Item_Step))), SqlDbType.VarChar, 200, SPACE); // 原単価
                            dpc.SetParam(string.Format("@ITEM{0}", (22 + (j * Item_Step))), SqlDbType.VarChar, 200, SPACE); // 原価金額
                        }
                        j++;
                    }
                    i = i + j;
                    dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now); // 登録日付

                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion

                    dbSORT++;

                    #endregion
                    
                }

            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "COUNT(*) AS CNT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (Util.ToDecimal(tmpdtPR_HN_TBL.Rows[0]["CNT"]) > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 消費税計算処理
        /// </summary>
        /// <param name="CalcValue">消費税を求めたい金額</param>
        /// <param name="TaxValue">消費税率</param>
        /// <returns>消費税計算結果</returns>
        private Decimal GetTaxCalc(Decimal CalcValue, Decimal TaxValue)
        {
            Decimal RetTaxValue = 0;
            Decimal TaxPercent = (TaxValue / 100);
            // 消費税抜き入り計算
            switch (Util.ToInt(this.UriageInfo.SHOHIZEI_NYURYOKU_HOHO))
            {
                // 税抜き入力(自動計算なし)
                case 1:
                    RetTaxValue = 0;
                    break;
                // 税抜き入力(自動計算あり)
                case 2:
                    RetTaxValue = CalcValue * TaxPercent;
                    RetTaxValue = Math.Round(RetTaxValue, 2);
                    break;
                // 税込み入力(自動計算あり)
                case 3:
                    // RetTaxValue = (CalcValue * (1 - TaxPercent)) * TaxPercent;
                    decimal ShoKei = (CalcValue / (TaxValue + 100)) * 100;
                    RetTaxValue = CalcValue - ShoKei;
                    RetTaxValue = Math.Round(RetTaxValue, 2);
                    break;
            }
            // 消費税端数処理
            // 2016-10-24　四捨五入処理の為、コメントアウトを解除
            // 島袋
            switch (Util.ToInt(this.UriageInfo.SHOHIZEI_HASU_SHORI))
            {
                // 切り捨て
                case 1:
                    RetTaxValue = Math.Floor(RetTaxValue);
                    break;
                // 四捨五入
                case 2:
                    RetTaxValue = Util.Round(RetTaxValue, 0);
                    break;
                // 切り上げ
                case 3:
                    RetTaxValue = Math.Ceiling(RetTaxValue);
                    break;
            }/**/

            return RetTaxValue;
        }

        /// <summary>
        /// 伝票番号の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidDenpyoNo()
        {
            // 数値チェック
            if (!ValChk.IsNumber(this.txtDenpyoNo.Text))
            {
                //this.txtDenpyoNo.Text = Regex.Replace(this.txtDenpyoNo.Text, "\\D", "");
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtDenpyoNo.Text, this.txtDenpyoNo.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidGengoYear()
        {
            // 数値チェック
            if (!ValChk.IsNumber(this.txtGengoYear.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtGengoYear.Text, this.txtGengoYear.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 空の場合、0年として処理
            else if (ValChk.IsEmpty(this.txtGengoYear.Text))
            {
                this.txtGengoYear.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDate(Util.FixJpDate(this.lblGengo.Text, this.txtGengoYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));

            return true;
        }

        /// <summary>
        /// 月の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidMonth()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonth.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 空又は1未満の場合、1月として処理
            else if (ValChk.IsEmpty(this.txtMonth.Text) || Util.ToInt(this.txtMonth.Text) < 1)
            {
                this.txtMonth.Text = "1";
            }
            // 12を超える月が入力された場合、12月として処理
            else if (Util.ToInt(this.txtMonth.Text) > 12)
            {
                this.txtMonth.Text = "12";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDate(Util.FixJpDate(this.lblGengo.Text, this.txtGengoYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));

            return true;
        }

        /// <summary>
        /// 日の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidDay()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDay.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtDay.Text, this.txtDay.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 空又は1未満の場合、1日として処理
            else if (ValChk.IsEmpty(this.txtDay.Text) || Util.ToInt(this.txtDay.Text) < 1)
            {
                this.txtDay.Text = "1";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDate(Util.FixJpDate(this.lblGengo.Text, this.txtGengoYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));

            return true;
        }

        /// <summary>
        /// 取引区分の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidTorihikiKubunCd()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTorihikiKubunCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtTorihikiKubunCd.Text, this.txtTorihikiKubunCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 空の場合、0として処理
            else if (ValChk.IsEmpty(this.txtTorihikiKubunCd.Text))
            {
                this.txtDay.Text = "0";
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // コードを元に名称を取得する
            else
            {
                this.lblTorihikiKubunNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_F_TORIHIKI_KUBUN1", this.txtTorihikiKubunCd.Text);
                // 存在しない区分が入力された場合(名称が空の場合)
                if (this.lblTorihikiKubunNm.Text == "")
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 船主CDの値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidFunanushiCd()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtFunanushiCd.Text, this.txtFunanushiCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 空チェック
            else if (ValChk.IsEmpty(this.txtFunanushiCd.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // コードを元に名称を取得する
            else
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);
                DataTable dtVI_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                    "TORIHIKISAKI_NM", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TORIHIKISAKI_KUBUN1 = 1", dpc);

                // 存在しないコードが入力された場合
                if (dtVI_HN_TORIHIKISAKI_JOHO.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblFunanushiNm.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"]);
                }
            }

            return true;
        }

        /// <summary>
        /// 担当者CDの値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidTantoshaCd()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoshaCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtTantoshaCd.Text, this.txtTantoshaCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 空の場合、0として処理
            else if (ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                this.txtDay.Text = "0";
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // コードを元に名称を取得する
            else
            {
                this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtTantoshaCd.Text);
                // 存在しないコードが入力された場合(名称が空の場合)
                if (this.lblTantoshaNm.Text == "")
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     指定した値が小数を含むかどうかを返します。</summary>
        /// <param name="dValue">
        ///     検査対象となる値。</param>
        /// <returns>
        ///     小数を含む場合は true。それ以外は false。</returns>
        private static bool IsDecimal(decimal dValue)
        {
            if (dValue - System.Math.Floor(dValue) != 0)
            {
                return true;
            }

            return false;
        }
        #endregion
    }
}
