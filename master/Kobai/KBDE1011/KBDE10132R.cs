﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbde1011
{
    /// <summary>
    /// KBDE1013_2R の概要の説明です。
    /// </summary>
    public partial class KBDE10132R : BaseReport
    {

        public KBDE10132R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
