﻿using System.Data;
using System;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1051
{
    /// <summary>
    /// KBMR1051R の帳票
    /// </summary>
    public partial class KBMR1051R : BaseReport
    {

        public KBMR1051R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            InitializeComponent();
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            ////和暦でDataTimeを文字列に変換する
            //System.Globalization.CultureInfo ci =
            //    new System.Globalization.CultureInfo("ja-JP", false);
            //ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            //this.txtToday.Text = DateTime.Now.ToString("gy年MM月dd日", ci);
        }

        private void detail_Format(object sender, EventArgs e)
        {
            // 供給原価数量
            this.txtGenkaSuryo.Text = Util.FormatNum(Util.ToDecimal(txtKurikoshiSuryo.Text) + Util.ToDecimal(txtUkeireSuryo.Text) - Util.ToDecimal(txtTanaSuryo.Text),2);
            // 供給原価金額
            this.txtGenkaKingaku.Text = Util.FormatNum(Util.ToDecimal(this.txtKurikoshiKingaku.Text) + Util.ToDecimal(txtUkeireKingaku.Text) - Util.ToDecimal(txtTanaKingaku.Text));
        }

        private void groupFooter1_Format(object sender, EventArgs e)
        {
            // 供給原価数量
            this.txtGrpGenkSu.Text = Util.FormatNum(Util.ToDecimal(txtGrpKriSu.Text) + Util.ToDecimal(txtGrpUkeSu.Text) - Util.ToDecimal(txtGrpTanaSu.Text), 2);
            // 供給原価金額
            this.txtGrpGenkKin.Text = Util.FormatNum(Util.ToDecimal(txtGrpKriKin.Text) + Util.ToDecimal(txtGrpUkeKin.Text) - Util.ToDecimal(txtGrpTanaKin.Text));
        }

        private void reportFooter1_Format(object sender, EventArgs e)
        {
            // 供給原価数量
            this.txtRptGenkSu.Text = Util.FormatNum(Util.ToDecimal(txtRptKriSu.Text) + Util.ToDecimal(txtRptUkeSu.Text) - Util.ToDecimal(txtRptTanaSu.Text), 2);
            // 供給原価金額
            this.txtRptGenkKin.Text = Util.FormatNum(Util.ToDecimal(txtRptKriKin.Text) + Util.ToDecimal(txtRptUkeKin.Text) - Util.ToDecimal(txtRptTanaKin.Text));
        }
    }
}
