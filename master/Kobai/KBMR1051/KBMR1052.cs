﻿using System;
using System.Reflection;
using System.ComponentModel;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1051
{
    /// <summary>
    /// 県漁連コード設定(KBMR1052)
    /// </summary>
    public partial class KBMR1052 : BasePgForm
    {
        //#region private変数
        ///// <summary>
        ///// KBMR1052(条件画面)のオブジェクト(設定内容の取得のため)
        ///// </summary>
        //KBMR1052 _pForm;
        //#endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBMR1052()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            this.pnlDebug.Visible = true;
            this.lblTitle.Visible = false;
            // タイトルは非表示
            this.lblTitle.Visible = false;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF3.Location;
            this.btnEsc.Visible = true;
            this.btnF1.Visible = true;
            this.btnF6.Visible = true;
            this.btnEsc.Enabled = true;
            this.btnF1.Enabled = true;
            this.btnF6.Enabled = true;

            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            // 画面の初期表示
            InitDisp();

            // 初期フォーカス
            this.txtShiiresakiCd1.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、仕入先コード１・２に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShiiresakiCd1":
                case "txtShiiresakiCd2":
                case "txtShiiresakiCd3":
                case "txtShiiresakiCd4":
                case "txtShiiresakiCd5":
                case "txtShiiresakiCd6":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            switch (this.ActiveCtlNm)
            {
                case "txtShiiresakiCd1":
                    #region 仕入先CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShiiresakiCd1.Text = result[0];
                                this.lblShiiresakiNm1.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtShiiresakiCd2":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShiiresakiCd2.Text = result[0];
                                this.lblShiiresakiNm2.Text = result[1];
                            }
                        }
                    }
                    break;
                case "txtShiiresakiCd3":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);
                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShiiresakiCd3.Text = result[0];
                                this.lblShiiresakiNm3.Text = result[1];
                            }
                        }
                    }
                    break;
                case "txtShiiresakiCd4":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);
                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShiiresakiCd4.Text = result[0];
                                this.lblShiiresakiNm4.Text = result[1];
                            }
                        }
                    }
                    break;
                case "txtShiiresakiCd5":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);
                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShiiresakiCd5.Text = result[0];
                                this.lblShiiresakiNm5.Text = result[1];
                            }
                        }
                    }
                    break;
                case "txtShiiresakiCd6":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);
                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShiiresakiCd6.Text = result[0];
                                this.lblShiiresakiNm6.Text = result[1];
                            }
                        }
                    }
                    break;
            }

        }

        /// <summary>
        /// F6押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            if (Msg.ConfYesNo("更新しますか？") == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            // 更新処理
            SaveSettings();

            // 画面を閉じる
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 仕入先コード1の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCd1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireCd1())
            {
                e.Cancel = true;
                this.txtShiiresakiCd1.SelectAll();
            }
        }
        /// <summary>
        /// 仕入先コード2の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCd2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireCd2())
            {
                e.Cancel = true;
                this.txtShiiresakiCd2.SelectAll();
            }
        }
        /// <summary>
        /// 仕入先コード3の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCd3_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireCd3())
            {
                e.Cancel = true;
                this.txtShiiresakiCd3.SelectAll();
            }
        }
        /// <summary>
        /// 仕入先コード4の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCd4_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireCd4())
            {
                e.Cancel = true;
                this.txtShiiresakiCd4.SelectAll();
            }
        }
        /// <summary>
        /// 仕入先コード5の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCd5_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireCd5())
            {
                e.Cancel = true;
                this.txtShiiresakiCd5.SelectAll();
            }
        }
        /// <summary>
        /// 仕入先コード6の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCd6_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireCd6())
            {
                e.Cancel = true;
                this.txtShiiresakiCd6.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 仕入先コード1の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireCd1()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiiresakiCd1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 入力されたら名称を取得
            if (!ValChk.IsEmpty(this.txtShiiresakiCd1.Text))
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiiresakiCd1.Text);
                this.lblShiiresakiNm1.Text = name;
            }
            return true;
        }
        /// <summary>
        /// 仕入先コード2の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireCd2()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiiresakiCd2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 入力されたら名称を取得
            if (!ValChk.IsEmpty(this.txtShiiresakiCd2.Text))
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiiresakiCd2.Text);
                this.lblShiiresakiNm2.Text = name;
            }
            return true;
        }
        /// <summary>
        /// 仕入先コード3の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireCd3()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiiresakiCd3.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 入力されたら名称を取得
            if (!ValChk.IsEmpty(this.txtShiiresakiCd3.Text))
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiiresakiCd3.Text);
                this.lblShiiresakiNm3.Text = name;
            }
            return true;
        }
        /// <summary>
        /// 仕入先コード4の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireCd4()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiiresakiCd4.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 入力されたら名称を取得
            if (!ValChk.IsEmpty(this.txtShiiresakiCd4.Text))
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiiresakiCd4.Text);
                this.lblShiiresakiNm4.Text = name;
            }
            return true;
        }
        /// <summary>
        /// 仕入先コード5の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireCd5()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiiresakiCd5.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 入力されたら名称を取得
            if (!ValChk.IsEmpty(this.txtShiiresakiCd5.Text))
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiiresakiCd5.Text);
                this.lblShiiresakiNm5.Text = name;
            }
            return true;
        }
        /// <summary>
        /// 仕入先コード6の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireCd6()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiiresakiCd6.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 入力されたら名称を取得
            if (!ValChk.IsEmpty(this.txtShiiresakiCd6.Text))
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiiresakiCd6.Text);
                this.lblShiiresakiNm6.Text = name;
            }
            return true;
        }

        /// <summary>
        /// 初期情報の表示
        /// </summary>
        private void InitDisp()
        {
            // 仕入先コード1
            string shiiresakiCd = this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI1");
            this.txtShiiresakiCd1.Text = shiiresakiCd;
            IsValidShiireCd1();
            // 仕入先コード2
            shiiresakiCd = this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI2");
            this.txtShiiresakiCd2.Text = shiiresakiCd;
            IsValidShiireCd2();
            // 仕入先コード3
            shiiresakiCd = this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI3");
            this.txtShiiresakiCd3.Text = shiiresakiCd;
            IsValidShiireCd3();
            // 仕入先コード4
            shiiresakiCd = this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI4");
            this.txtShiiresakiCd4.Text = shiiresakiCd;
            IsValidShiireCd4();
            // 仕入先コード5
            shiiresakiCd = this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI5");
            this.txtShiiresakiCd5.Text = shiiresakiCd;
            IsValidShiireCd5();
            // 仕入先コード6
            shiiresakiCd = this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI6");
            this.txtShiiresakiCd6.Text = shiiresakiCd;
            IsValidShiireCd6();

        }

        /// <summary>
        /// 設定を保存する
        /// </summary>
        private void SaveSettings()
        {
            // 仕入先コード1
            string shiireSaki = this.txtShiiresakiCd1.Text;
            this.Config.SetPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI1", shiireSaki);
            // 仕入先コード1
            shiireSaki = this.txtShiiresakiCd2.Text;
            this.Config.SetPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI2", shiireSaki);
            // 仕入先コード1
            shiireSaki = this.txtShiiresakiCd3.Text;
            this.Config.SetPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI3", shiireSaki);
            // 仕入先コード1
            shiireSaki = this.txtShiiresakiCd4.Text;
            this.Config.SetPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI4", shiireSaki);
            // 仕入先コード1
            shiireSaki = this.txtShiiresakiCd5.Text;
            this.Config.SetPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI5", shiireSaki);
            // 仕入先コード1
            shiireSaki = this.txtShiiresakiCd6.Text;
            this.Config.SetPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI6", shiireSaki);


            // 設定を保存する
            this.Config.SaveConfig();
        }
        #endregion
    }
}
