﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1021
{
    /// <summary>
    /// 作成済仕訳データ検索(KBDB1022)
    /// </summary>
    public partial class KBDB1022 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDB1022()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // データ取得のSQLを発行してGridに反映
            DataTable dtList = new DataTable();
            try
            {
                dtList = GetTB_HN_ZIDO_SHIWAKE_RIREKI();
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                return;
            }

            // 取得したデータを表示用に編集
            DataTable dtDsp = EditDataList(dtList);

            this.dgvList.DataSource = dtDsp;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 78;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 58;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[2].Width = 64;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[3].Width = 40;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[4].Width = 120;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[5].Width = 260;
            this.dgvList.Columns[6].Width = 80;
            this.dgvList.Columns[7].Width = 0;
            this.dgvList.Columns[8].Width = 0;
            this.dgvList.Columns[9].Width = 0;
            this.dgvList.Columns[10].Width = 0;
            this.dgvList.Columns[11].Width = 0;
            this.dgvList.Columns[12].Width = 0;
            this.dgvList.Columns[13].Width = 0;
            this.dgvList.Columns[14].Width = 0;
            this.dgvList.Columns[15].Width = 0;
            this.dgvList.Columns[16].Width = 0;
            this.dgvList.Columns[17].Width = 0;
            this.dgvList.Columns[18].Width = 0;
            for (int i = 7; i < 19; i++)
            {
                this.dgvList.Columns[i].Visible = false;
            }
            this.dgvList.Columns[7].HeaderText = string.Empty;
            this.dgvList.Columns[8].HeaderText = string.Empty;
            this.dgvList.Columns[9].HeaderText = string.Empty;
            this.dgvList.Columns[10].HeaderText = string.Empty;
            this.dgvList.Columns[11].HeaderText = string.Empty;
            this.dgvList.Columns[12].HeaderText = string.Empty;
            this.dgvList.Columns[13].HeaderText = string.Empty;
            this.dgvList.Columns[14].HeaderText = string.Empty;
            this.dgvList.Columns[15].HeaderText = string.Empty;
            this.dgvList.Columns[16].HeaderText = string.Empty;
            this.dgvList.Columns[17].HeaderText = string.Empty;
            this.dgvList.Columns[18].HeaderText = string.Empty;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            ReturnVal();
        }

        /// <summary>
        /// フォーム表示後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KBDB1022_Shown(object sender, EventArgs e)
        {
            if (this.dgvList.RowCount == 0)
            {
                Msg.Info("該当データがありません。");
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 自動仕訳履歴のデータを取得
        /// </summary>
        /// <returns>自動仕訳履歴から取得したデータ</returns>
        private DataTable GetTB_HN_ZIDO_SHIWAKE_RIREKI()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  A.DENPYO_BANGO              AS 伝票番号 ");
            sql.Append(" ,A.DENPYO_DATE               AS 伝票日付 ");
            sql.Append(" ,MIN(A.SHIWAKE_DENPYO_BANGO) AS 仕訳伝票番号 ");
            sql.Append(" ,MAX(A.SHORI_KUBUN)          AS 処理区分 ");
            sql.Append(" ,MAX(CASE WHEN A.SHORI_KUBUN = 1 THEN '現金' ");
            sql.Append("           ELSE CASE WHEN A.SHORI_KUBUN = 2 THEN '現金、掛' ");
            sql.Append("                     ELSE CASE WHEN A.SHORI_KUBUN = 3 THEN '締日基準' ");
            sql.Append("                               ELSE '' ");
            sql.Append("                          END ");
            sql.Append("                END ");
            sql.Append("      END) AS 処理区分名称 ");
            sql.Append(" ,MAX(A.SHIMEBI)              AS 締日 ");
            sql.Append(" ,MAX(A.KAISHI_DENPYO_DATE)   AS 開始伝票日付 ");
            sql.Append(" ,MAX(A.SHURYO_DENPYO_DATE)   AS 終了伝票日付 ");
            sql.Append(" ,MAX(A.KAISHI_SEIKYUSAKI_CD) AS 開始請求先コード ");
            sql.Append(" ,MAX(CASE WHEN ISNULL(A.KAISHI_SEIKYUSAKI_CD, '') = '' THEN '先　頭' ELSE B.TORIHIKISAKI_NM END) AS 開始請求先名 ");
            sql.Append(" ,MAX(A.SHURYO_SEIKYUSAKI_CD) AS 終了請求先コード ");
            sql.Append(" ,MAX(CASE WHEN ISNULL(A.SHURYO_SEIKYUSAKI_CD, '') = '' THEN '最　後' ELSE C.TORIHIKISAKI_NM END) AS 終了請求先名 ");
            sql.Append(" ,MAX(A.TEKIYO_CD)            AS 摘要コード ");
            sql.Append(" ,MAX(A.TEKIYO)               AS 摘要名 ");
            sql.Append(" ,MAX(A.TANTOSHA_CD)          AS 担当者コード ");
            sql.Append(" ,MAX(D.TANTOSHA_NM)          AS 担当者名 ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_ZIDO_SHIWAKE_RIREKI AS A ");
            sql.Append("LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B ");
            sql.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("AND A.KAISHI_SEIKYUSAKI_CD = B.TORIHIKISAKI_CD ");
            sql.Append("LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS C ");
            sql.Append("ON A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("AND A.SHURYO_SEIKYUSAKI_CD = C.TORIHIKISAKI_CD ");
            sql.Append("LEFT OUTER JOIN TB_CM_TANTOSHA AS D ");
            sql.Append("ON A.KAISHA_CD = D.KAISHA_CD ");
            sql.Append("AND A.TANTOSHA_CD = D.TANTOSHA_CD ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND A.SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND A.DENPYO_KUBUN = 2 ");
            sql.Append("AND A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO ");
            sql.Append("GROUP BY ");
            sql.Append("  A.DENPYO_DATE ");
            sql.Append(" ,A.DENPYO_BANGO ");
            sql.Append(" ,A.SHORI_KUBUN ");
            sql.Append("ORDER BY ");
            sql.Append("  A.DENPYO_DATE DESC ");
            sql.Append(" ,A.DENPYO_BANGO DESC ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(this.UInfo.ShishoCd));
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 日付を編集する際に用いるワーク
            string[] aryDate;
            string tmpDate;
            // 支払先範囲を編集する際に用いるワーク
            string tmpStr;

            // 列の定義を作成
            dtResult.Columns.Add("仕訳伝票№", typeof(int));
            dtResult.Columns.Add("日 付", typeof(string));
            dtResult.Columns.Add("作成区分", typeof(string));
            dtResult.Columns.Add("締日", typeof(string));
            dtResult.Columns.Add("取引伝票日付範囲", typeof(string));
            dtResult.Columns.Add("支払先範囲", typeof(string));
            dtResult.Columns.Add("ﾃﾞｰﾀ担当者", typeof(string));
            dtResult.Columns.Add("返却用_伝票番号", typeof(string));
            dtResult.Columns.Add("返却用_伝票日付", typeof(string));
            dtResult.Columns.Add("返却用_処理区分", typeof(string));
            dtResult.Columns.Add("返却用_締日", typeof(string));
            dtResult.Columns.Add("返却用_開始伝票日付", typeof(string));
            dtResult.Columns.Add("返却用_終了伝票日付", typeof(string));
            dtResult.Columns.Add("返却用_開始請求先コード", typeof(string));
            dtResult.Columns.Add("返却用_終了請求先コード", typeof(string));
            dtResult.Columns.Add("返却用_摘要コード", typeof(string));
            dtResult.Columns.Add("返却用_摘要", typeof(string));
            dtResult.Columns.Add("返却用_担当者コード", typeof(string));
            dtResult.Columns.Add("返却用_担当者名", typeof(string));

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();
                drResult["仕訳伝票№"] = dtData.Rows[i]["仕訳伝票番号"];
                aryDate = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["伝票日付"]), this.Dba);
                tmpDate = Util.ToInt(aryDate[2]).ToString("00") + "/" + aryDate[3].PadLeft(2, ' ') + "/" + aryDate[4].PadLeft(2, ' ');
                drResult["日 付"] = tmpDate;
                drResult["作成区分"] = dtData.Rows[i]["処理区分名称"];
                drResult["締日"] = dtData.Rows[i]["締日"];
                aryDate = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["開始伝票日付"]), this.Dba);
                tmpDate = Util.ToInt(aryDate[2]).ToString("00") + "/" + aryDate[3].PadLeft(2, ' ') + "/" + aryDate[4].PadLeft(2, ' ');
                aryDate = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["終了伝票日付"]), this.Dba);
                tmpDate = tmpDate + "～" + Util.ToInt(aryDate[2]).ToString("00") + "/" + aryDate[3].PadLeft(2, ' ') + "/" + aryDate[4].PadLeft(2, ' ');
                drResult["取引伝票日付範囲"] = tmpDate;
                if (ValChk.IsEmpty(dtData.Rows[i]["開始請求先コード"]))
                {
                    tmpStr = "    " + Util.ToString(dtData.Rows[i]["開始請求先名"]) + "          ";
                }
                else
                {
                    tmpStr = Util.ToString(dtData.Rows[i]["開始請求先名"]) + new String(' ', 20 - Util.GetByteLength(dtData.Rows[i]["開始請求先名"].ToString()));
                }
                tmpStr = tmpStr + "～" + Util.ToString(dtData.Rows[i]["終了請求先名"]);
                drResult["支払先範囲"] = tmpStr;
                tmpStr = Util.ToString(dtData.Rows[i]["担当者コード"]) + " " + Util.ToString(dtData.Rows[i]["担当者名"]);
                drResult["ﾃﾞｰﾀ担当者"] = tmpStr;
                drResult["返却用_伝票番号"] = Util.ToString(dtData.Rows[i]["伝票番号"]);
                drResult["返却用_伝票日付"] = Util.ToDateStr(dtData.Rows[i]["伝票日付"]);
                drResult["返却用_処理区分"] = Util.ToString(dtData.Rows[i]["処理区分"]);
                drResult["返却用_締日"] = Util.ToString(dtData.Rows[i]["締日"]);
                drResult["返却用_開始伝票日付"] = Util.ToDateStr(dtData.Rows[i]["開始伝票日付"]);
                drResult["返却用_終了伝票日付"] = Util.ToDateStr(dtData.Rows[i]["終了伝票日付"]);
                drResult["返却用_開始請求先コード"] = Util.ToString(dtData.Rows[i]["開始請求先コード"]);
                drResult["返却用_終了請求先コード"] = Util.ToString(dtData.Rows[i]["終了請求先コード"]);
                drResult["返却用_摘要コード"] = Util.ToInt(dtData.Rows[i]["摘要コード"]) == 0 ? string.Empty : Util.ToString(dtData.Rows[i]["摘要コード"]);
                drResult["返却用_摘要"] = Util.ToString(dtData.Rows[i]["摘要名"]);
                drResult["返却用_担当者コード"] = Util.ToInt(dtData.Rows[i]["担当者コード"]) == 0 ? string.Empty : Util.ToString(dtData.Rows[i]["担当者コード"]);
                drResult["返却用_担当者名"] = Util.ToString(dtData.Rows[i]["担当者名"]);
                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[12] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_伝票番号"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_伝票日付"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_処理区分"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_締日"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_開始伝票日付"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_終了伝票日付"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_開始請求先コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_終了請求先コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_摘要コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_摘要"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_担当者コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_担当者名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
