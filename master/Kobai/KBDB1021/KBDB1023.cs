﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1021
{
    /// <summary>
    /// 仕訳データ参照(KBDB1023)
    /// </summary>
    public partial class KBDB1023 : BasePgForm
    {
        #region 構造体
        /// <summary>
        /// 合計情報
        /// </summary>
        private struct Summary
        {
            public decimal genkinShiire;
            public decimal genkinZei;
            public decimal kakeShiire;
            public decimal kakeZei;
            public decimal keiShiire;

            /// <summary>
            /// 金額をクリア
            /// </summary>
            public void Clear()
            {
                genkinShiire = 0;
                genkinZei = 0;
                kakeShiire = 0;
                kakeZei = 0;
                keiShiire = 0;
            }
        }

        // 支払先単位の合計を保持する変数
        Summary _sumShiharaiInfo = new Summary();

        // 全レコードの合計を保持する変数
        Summary _sumTotalInfo = new Summary();
        #endregion

        #region private変数
        /// <summary>
        /// KBDB1021(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        KBDB1021 _pForm;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDB1023(KBDB1021 frm)
        {
            this._pForm = frm;

            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 期間の初期表示
            string tmpTerm = " ";
            string[] aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DpyDtFr"]), this.Dba);
            tmpTerm += aryJpDate[5];
            aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DpyDtTo"]), this.Dba);
            tmpTerm += " ～ " + aryJpDate[5];
            this.lblTerm.Text = tmpTerm;

            // 表示用にデータを編集
            DataTable dtDisp = EditDataList(this._pForm.SwkTgtData);

            this.dgvList.DataSource = dtDisp;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 190;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[1].Width = 80;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[2].Width = 80;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[3].Width = 80;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[4].Width = 80;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[5].Width = 80;
            this.dgvList.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // 合計値の表示
            this.lblGenkinShiire.Text = Util.FormatNum(this._sumTotalInfo.genkinShiire);
            this.lblGenkinZei.Text = Util.FormatNum(this._sumTotalInfo.genkinZei);
            this.lblKakeShiire.Text = Util.FormatNum(this._sumTotalInfo.kakeShiire);
            this.lblKakeZei.Text = Util.FormatNum(this._sumTotalInfo.kakeZei);
            this.lblKeiShiire.Text = Util.FormatNum(this._sumTotalInfo.keiShiire);

            // InData在りの場合は更新モード起動、その場合は更新処理は不可
            if (this.InData != null && !ValChk.IsEmpty(this.InData))
            {
                string inData = (string)this.InData;
                if (inData.Length != 0)
                    this.btnF6.Enabled = false;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            // 仕訳形式画面を起動
            using (KBDB1024 frm1024 = new KBDB1024(this._pForm, this))
            {
                frm1024.ShowDialog(this);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled) return;

            // 登録処理
            string modeNm = this._pForm.Mode == 2 ? "更新" : "登録";
            if (Msg.ConfYesNo(modeNm + "しますか？") == DialogResult.No)
            {
                // 「いいえ」が押されたら処理終了
                return;
            }

            // 更新中メッセージ表示
            KBDB1026 msgFrm = new KBDB1026();
            msgFrm.Show();
            msgFrm.Refresh();

            bool result = false;

            try
            {
                this.Dba.BeginTransaction();

                // 登録処理を実行
                KBDB1021DA da = new KBDB1021DA(this.UInfo, this.Dba, this.Config);
                result = da.MakeSwkData(this._pForm.Mode, this._pForm.PackDpyNo,
                    this._pForm.Condition, this._pForm.TaishakuData);

                // 更新終了後、メッセージを閉じる
                msgFrm.Close();

                // 更新に失敗していればその旨表示する
                if (result)
                {
                    this.Dba.Commit();
                }
                else
                {
                    this.Dba.Rollback();
                    Msg.Error("更新に失敗しました。" + Environment.NewLine + "もう一度やり直して下さい。");
                }
            }
            finally
            {
                this.Dba.Rollback();
            }

            if (result)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// フォーム表示後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KBDB1023_Shown(object sender, EventArgs e)
        {
            if (this.dgvList.RowCount == 0)
            {
                Msg.Info("該当データがありません。");
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }

        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData">DBから取得したデータ</param>
        /// <returns>表示用のデータ</returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 列の定義を作成
            dtResult.Columns.Add("支払先名", typeof(string));
            dtResult.Columns.Add("現金仕入額", typeof(string));
            dtResult.Columns.Add("現金消費税", typeof(string));
            dtResult.Columns.Add("掛仕入", typeof(string));
            dtResult.Columns.Add("掛消費税", typeof(string));
            dtResult.Columns.Add("合計仕入", typeof(string));

            // 会員番号ブレイク判断用ワーク
            string prevKaiinNo = string.Empty;
            string prevKaiinNm = string.Empty;

            int bias = 1;   // 仕入か返品かの掛数(仕入：1、返品：-1)

            // 合計を格納する構造体をクリア
            this._sumShiharaiInfo.Clear();
            this._sumTotalInfo.Clear();

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                if (!ValChk.IsEmpty(prevKaiinNo) && !Util.ToString(dtData.Rows[i]["会員番号"]).Equals(prevKaiinNo))
                {
                    // 会員番号ブレイク時、表示用データをセットする
                    drResult = dtResult.NewRow();

                    drResult["支払先名"] = prevKaiinNm;
                    drResult["現金仕入額"] = Util.FormatNum(this._sumShiharaiInfo.genkinShiire);
                    drResult["現金消費税"] = Util.FormatNum(this._sumShiharaiInfo.genkinZei);
                    drResult["掛仕入"] = Util.FormatNum(this._sumShiharaiInfo.kakeShiire);
                    drResult["掛消費税"] = Util.FormatNum(this._sumShiharaiInfo.kakeZei);
                    drResult["合計仕入"] = Util.FormatNum(this._sumShiharaiInfo.keiShiire);

                    dtResult.Rows.Add(drResult);
                    this._sumShiharaiInfo.Clear();
                }

                // 取引区分２=2の場合返品として金額からマイナスする
                bias = Util.ToInt(dtData.Rows[i]["取引区分２"]) == 2 ? -1 : 1;

                if (Util.ToInt(dtData.Rows[i]["取引区分１"]) == 2)
                {
                    // 取引区分１=2の場合現金
                    this._sumShiharaiInfo.genkinShiire += (Util.ToDecimal(dtData.Rows[i]["売上金額"]) * bias);
                    this._sumShiharaiInfo.genkinZei += (Util.ToDecimal(dtData.Rows[i]["消費税"]) * bias);
                    this._sumTotalInfo.genkinShiire += (Util.ToDecimal(dtData.Rows[i]["売上金額"]) * bias);
                    this._sumTotalInfo.genkinZei += (Util.ToDecimal(dtData.Rows[i]["消費税"]) * bias);
                }
                else
                {
                    // 取引区分１≠2の場合掛
                    this._sumShiharaiInfo.kakeShiire += (Util.ToDecimal(dtData.Rows[i]["売上金額"]) * bias);
                    this._sumShiharaiInfo.kakeZei += (Util.ToDecimal(dtData.Rows[i]["消費税"]) * bias);
                    this._sumTotalInfo.kakeShiire += (Util.ToDecimal(dtData.Rows[i]["売上金額"]) * bias);
                    this._sumTotalInfo.kakeZei += (Util.ToDecimal(dtData.Rows[i]["消費税"]) * bias);
                }
                this._sumShiharaiInfo.keiShiire += (Util.ToDecimal(dtData.Rows[i]["売上金額"]) + Util.ToDecimal(dtData.Rows[i]["消費税"])) * bias;
                this._sumTotalInfo.keiShiire += (Util.ToDecimal(dtData.Rows[i]["売上金額"]) + Util.ToDecimal(dtData.Rows[i]["消費税"])) * bias;

                prevKaiinNo = Util.ToString(dtData.Rows[i]["会員番号"]);
                prevKaiinNm = Util.ToString(dtData.Rows[i]["会員名称"]);
            }

            // 最終行の追加
            if (dtData.Rows.Count > 0)
            {
                drResult = dtResult.NewRow();

                drResult["支払先名"] = prevKaiinNm;
                drResult["現金仕入額"] = Util.FormatNum(this._sumShiharaiInfo.genkinShiire);
                drResult["現金消費税"] = Util.FormatNum(this._sumShiharaiInfo.genkinZei);
                drResult["掛仕入"] = Util.FormatNum(this._sumShiharaiInfo.kakeShiire);
                drResult["掛消費税"] = Util.FormatNum(this._sumShiharaiInfo.kakeZei);
                drResult["合計仕入"] = Util.FormatNum(this._sumShiharaiInfo.keiShiire);

                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }
        #endregion
    }
}
