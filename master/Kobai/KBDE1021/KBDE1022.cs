﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.kb.kbde1021
{
    /// <summary>
    /// 仕入伝票検索(KBDE1022)
    /// </summary>
    public partial class KBDE1022 : BasePgForm
    {
        // 支所コード
        private int ShishoCode;

        // 商品中止区分
        private int HIN_CHUSHI_KUBUN = 0;     // 1:取引有,その他は中止区分を見ない

        private string activeContorolName = "";

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDE1022()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();

        }

        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            // 中止区分対応
            try
            {
                this.HIN_CHUSHI_KUBUN = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1021", "Setting", "CHUSHI_KUBUN")));
            }
            catch (Exception)
            {
                this.HIN_CHUSHI_KUBUN = 1;
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 開始
            lblGengoFr.Text = jpDate[0];
            txtGengoYearFr.Text = jpDate[2];
            txtMonthFr.Text = jpDate[3];
            txtDayFr.Text = jpDate[4];
            // 終了
            lblGengoTo.Text = jpDate[0];
            txtGengoYearTo.Text = jpDate[2];
            txtMonthTo.Text = jpDate[3];
            txtDayTo.Text = jpDate[4];

            // データ取得してグリッドに設定
            this.getListData(true);

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);
            this.dgvList.ColumnHeadersDefaultCellStyle.ForeColor = Color.Navy;

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 81;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[1].Width = 80;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[2].Width = 100;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[3].Width = 190;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[4].Width = 115;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[5].Width = 115;
            this.dgvList.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[6].Width = 110;
            this.dgvList.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // データが無い時
            this.btnEnter.Enabled = false;
            this.dgvList.Enabled = false;

            //this.txtGengoYearFr.Focus();
            if (this.InData != null && !ValChk.IsEmpty(this.InData))
            {
                string inData = (string)this.InData;
                if (inData.Length == 0)
                {
                    // 伝票日付（開始）年にフォーカスを移す
                    this.txtGengoYearFr.Focus();
                }
                else
                {
                    // 検索条件保持の場合は抽出
                    this.setDenpyoCondition(inData);
                    this.btnF6.PerformClick();
                    if (this.dgvList.Rows.Count > 0)
                    {
                        ActiveControl = this.dgvList;
                        this.dgvList.Rows[0].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, 0];
                    }
                }
            }
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年と取引先コードに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtSiiresakiCdFr":
                case "txtSiiresakiCdTo":
                case "txtTantoshaCdFr":
                case "txtTantoshaCdTo":
                case "txtShohinCdFr":
                case "txtShohinCdTo":
                case "txtGengoYearFr":
                case "txtGengoYearTo":
                    this.btnF1.Enabled = true;
                    this.btnF4.Enabled = false;
                    this.btnF6.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    this.btnF4.Enabled = false;
                    this.btnF6.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。
            System.Reflection.Assembly asm;
            Type t;
            string ActiveContorolName = this.ActiveCtlNm;
            if (activeContorolName != "" && activeContorolName != ActiveContorolName && ActiveContorolName == "txtGengoYearFr")
                ActiveContorolName = activeContorolName;
            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            // switch (this.ActiveCtlNm)
            switch (ActiveContorolName)
            {
                #region 元号
                case "txtGengoYearFr":
                case "txtGengoYearTo":
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            // if (this.ActiveCtlNm == "txtGengoYearFr")
                            if (ActiveContorolName == "txtGengoYearFr")
                            {
                                frm.InData = this.lblGengoFr.Text;
                            }
                            // else if (this.ActiveCtlNm == "txtGengoYearTo")
                            else if (ActiveContorolName == "txtGengoYearTo")
                            {
                                frm.InData = this.lblGengoTo.Text;
                            }
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                // if (this.ActiveCtlNm == "txtGengoYearFr")
                                if (ActiveContorolName == "txtGengoYearFr")
                                {
                                    this.lblGengoFr.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    SetJpFr();
                                }
                                // else if (this.ActiveCtlNm == "txtGengoYearTo")
                                else if (ActiveContorolName == "txtGengoYearTo")
                                {
                                    this.lblGengoTo.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    SetJpTo();
                                }
                            }
                        }

                    }
                    break;
                #endregion

                #region 仕入先CD
                case "txtSiiresakiCdFr":
                case "txtSiiresakiCdTo":
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9011.exe");
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9011.KOBC9011");
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                // if (this.ActiveCtlNm == "txtSiiresakiCdFr")
                                if (ActiveContorolName == "txtSiiresakiCdFr")
                                {
                                    this.txtSiiresakiCdFr.Text = outData[0];
                                    this.lblSiiresakiNmFr.Text = outData[1];
                                }
                                // else if (this.ActiveCtlNm == "txtSiiresakiCdTo")
                                else if (ActiveContorolName == "txtSiiresakiCdTo")
                                {
                                    this.txtSiiresakiCdTo.Text = outData[0];
                                    this.lblSiiresakiNmTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region 担当者CD
                case "txtTantoshaCdFr":
                case "txtTantoshaCdTo":
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9041.KOBC9041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                // if (this.ActiveCtlNm == "txtTantoshaCdFr")
                                if (ActiveContorolName == "txtTantoshaCdFr")
                                {
                                    this.txtTantoshaCdFr.Text = outData[0];
                                    this.lblTantoshaNmFr.Text = outData[1];
                                }
                                // else if (this.ActiveCtlNm == "txtTantoshaCdTo")
                                else if (ActiveContorolName == "txtTantoshaCdTo")
                                {
                                    this.txtTantoshaCdTo.Text = outData[0];
                                    this.lblTantoshaNmTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region 商品CD
                case "txtShohinCdFr":
                case "txtShohinCdTo":
                    // 商品一覧選択KOBC9031
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9031.exe");
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9031.KOBC9031");
                    t = asm.GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            // 中止区分対応
                            if (this.HIN_CHUSHI_KUBUN == 1)
                            {
                                frm.Par3 = this.HIN_CHUSHI_KUBUN.ToString();
                            }
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                // if (this.ActiveCtlNm == "txtShohinCdFr")
                                if (ActiveContorolName == "txtShohinCdFr")
                                {
                                    this.txtShohinCdFr.Text = outData[0];
                                    this.lblShohinNmFr.Text = outData[1];
                                }
                                // else if (this.ActiveCtlNm == "txtShohinCdTo")
                                else if (ActiveContorolName == "txtShohinCdTo")
                                {
                                    this.txtShohinCdTo.Text = outData[0];
                                    this.lblShohinNmTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    break;
                    #endregion
            }
            try
            {
                Controls[ActiveContorolName].Focus();
            }
            catch (Exception) { }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (!this.ValidateAll())
            {
                return;
            }
            // 条件（検索結果をクリアして伝票日付Frの年におフォーカスする）
            this.getListData(true);
            this.txtGengoYearFr.SelectAll();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (!this.ValidateAll())
            {
                return;
            }
            // 検索開始
            this.getListData(false);
        }

        #endregion

        #region イベント
        /// <summary>
        /// グリッドのフォーカス時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Enter(object sender, EventArgs e)
        {
            this.btnF1.Enabled = false;
            this.btnF4.Enabled = true;
            this.btnF6.Enabled = false;
            this.btnEnter.Enabled = true;
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                this.ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルフォーマット設定処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 5:
                case 6:
                    // 伝票金額、消費税がゼロ以下は文字色を赤にする
                    if (Util.ToDecimal(e.Value) < 0)
                    {
                        e.CellStyle.ForeColor = Color.Red;
                    }
                    break;
            }
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            this.ReturnVal();
        }

        /// <summary>
        /// コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!IsValid.IsYear(this.txtGengoYearFr.Text, this.txtGengoYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYearFr.SelectAll();
            }
            else
            {
                this.txtGengoYearFr.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthFr.SelectAll();
            }
            else
            {
                this.txtMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayFr.SelectAll();
            }
            else
            {
                this.txtDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDayFr.Text));
                CheckJpFr();
                SetJpFr();
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!IsValid.IsYear(this.txtGengoYearTo.Text, this.txtGengoYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYearTo.SelectAll();
            }
            else
            {
                this.txtGengoYearTo.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthTo.SelectAll();
            }
            else
            {
                this.txtMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayTo.SelectAll();
            }
            else
            {
                this.txtDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDayTo.Text));
                CheckJpTo();
                SetJpTo();
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// 仕入先コードFromの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSiiresakiCdFr_Validating(object sender, CancelEventArgs e)
        {
            this.lblSiiresakiNmFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", this.ShishoCode.ToString(), this.txtSiiresakiCdFr.Text);

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (ValChk.IsEmpty(this.lblSiiresakiNmFr.Text))
            {
                this.lblSiiresakiNmFr.Text = "先　頭";
            }
            else
            {
                activeContorolName = this.ActiveCtlNm;
            }
        }

        /// <summary>
        /// 仕入先コードFromの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSiiresakiCdTo_Validating(object sender, CancelEventArgs e)
        {
            this.lblSiiresakiNmTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", this.ShishoCode.ToString(), this.txtSiiresakiCdTo.Text);

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (ValChk.IsEmpty(this.lblSiiresakiNmTo.Text))
            {
                this.lblSiiresakiNmTo.Text = "最　後";
            }
            else
            {
                activeContorolName = this.ActiveCtlNm;
            }
        }

        /// <summary>
        /// 担当者コードFromの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCdFr_Validating(object sender, CancelEventArgs e)
        {
            this.lblTantoshaNmFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.ShishoCode.ToString(), this.txtTantoshaCdFr.Text);

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (ValChk.IsEmpty(this.lblTantoshaNmFr.Text))
            {
                this.lblTantoshaNmFr.Text = "先　頭";
            }
            else
            {
                activeContorolName = this.ActiveCtlNm;
            }
        }

        /// <summary>
        /// 担当者コードToの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCdTo_Validating(object sender, CancelEventArgs e)
        {
            this.lblTantoshaNmTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.ShishoCode.ToString(), this.txtTantoshaCdTo.Text);

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (ValChk.IsEmpty(this.lblTantoshaNmTo.Text))
            {
                this.lblTantoshaNmTo.Text = "最　後";
            }
            else
            {
                activeContorolName = this.ActiveCtlNm;
            }
        }

        /// <summary>
        /// 商品CD(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtShohinCdFr.Text))
            {
                //this.lblShohinNmFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN", this.ShishoCode.ToString(), this.txtShohinCdFr.Text);
                DbParamCollection dpc;
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 6, this.ShishoCode);
                dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 15, this.txtShohinCdFr.Text);
                dpc.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 4, this.HIN_CHUSHI_KUBUN);
                // 中止区分対応
                string condition = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SHOHIN_KUBUN5 <> 1";
                if (this.HIN_CHUSHI_KUBUN == 1)
                {
                    condition = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND ISNULL(CHUSHI_KUBUN, 0) = @CHUSHI_KUBUN AND SHOHIN_KUBUN5 <> 1";
                }
                DataTable dtVI_HN_SHOHIN = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "VI_HN_SHOHIN",
                    condition,
                    dpc);
                if (dtVI_HN_SHOHIN != null && dtVI_HN_SHOHIN.Rows.Count > 0)
                {
                    this.lblShohinNmFr.Text = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["SHOHIN_NM"]);
                }
                else
                {
                    this.lblShohinNmFr.Text = "先　頭";
                }
            }
            else
            {
                this.lblShohinNmFr.Text = "先　頭";
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// 商品CD(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtShohinCdTo.Text))
            {
                //this.lblShohinNmTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN", this.ShishoCode.ToString(), this.txtShohinCdTo.Text);
                DbParamCollection dpc;
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 6, this.ShishoCode);
                dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 15, this.txtShohinCdTo.Text);
                dpc.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 4, this.HIN_CHUSHI_KUBUN);
                // 中止区分対応
                string condition = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SHOHIN_KUBUN5 <> 1";
                if (this.HIN_CHUSHI_KUBUN == 1)
                {
                    condition = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND ISNULL(CHUSHI_KUBUN, 0) = @CHUSHI_KUBUN AND SHOHIN_KUBUN5 <> 1";
                }
                DataTable dtVI_HN_SHOHIN = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "VI_HN_SHOHIN",
                    condition,
                    dpc);
                if (dtVI_HN_SHOHIN != null && dtVI_HN_SHOHIN.Rows.Count > 0)
                {
                    this.lblShohinNmTo.Text = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["SHOHIN_NM"]);
                }
                else
                {
                    this.lblShohinNmTo.Text = "最　後";
                }
            }
            else
            {
                this.lblShohinNmTo.Text = "最　後";
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// 検索コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearchCode_Validating(object sender, CancelEventArgs e)
        {
            // データ取得してグリッドに設定
            //this.getListData(false);
        }

        #endregion

        #region privateメソッド
        private bool isValidSiiresakiCdFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSiiresakiCdFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtSiiresakiCdFr.Focus();
                this.txtSiiresakiCdFr.SelectAll();
                return false;
            }
            return true;
        }

        private bool isValidSiiresakiCdTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSiiresakiCdTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                this.txtSiiresakiCdTo.Focus();
                this.txtSiiresakiCdTo.SelectAll();
                return false;
            }
            return true;
        }

        private bool isValidTantoshaCdFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoshaCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtTantoshaCdFr.Focus();
                this.txtTantoshaCdFr.SelectAll();
                return false;
            }
            return true;
        }

        private bool isValidTantoshaCdTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoshaCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtTantoshaCdTo.Focus();
                this.txtTantoshaCdTo.SelectAll();
                return false;
            }
            return true;
        }

        private bool isValidShohinCdFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinCdFr.Focus();
                this.txtShohinCdFr.SelectAll();
                return false;
            }
            return true;
        }

        private bool isValidShohinCdTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinCdTo.Focus();
                this.txtShohinCdTo.SelectAll();
                return false;
            }
            return true;
        }

        private bool ValidateAll()
        {
            if (!isValidSiiresakiCdFr())
            {
                return false;
            }

            if (!isValidSiiresakiCdTo())
            {
                return false;
            }

            if (!isValidTantoshaCdFr())
            {
                return false;
            }

            if (!isValidTantoshaCdTo())
            {
                return false;
            }

            if (!isValidShohinCdFr())
            {
                return false;
            }

            if (!isValidShohinCdTo())
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text,
                this.txtMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayFr.Text) > lastDayInMonth)
            {
                this.txtDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text,
                this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                this.txtMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayTo.Text) > lastDayInMonth)
            {
                this.txtDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtGengoYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtGengoYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 仕入伝票履歴のデータを取得
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        /// <returns>仕入伝票履歴の取得したデータ</returns>
        private DataTable GetTB_HN_ZIDO_SHIWAKE_RIREKI(bool isInitial)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("    A.DENPYO_BANGO    AS 伝票番号, ");
            sql.Append("    A.DENPYO_DATE     AS 伝票日付, ");
            //sql.Append("    A.KAIIN_BANGO     AS 会員番号, ");
            //sql.Append("    A.KAIIN_NM        AS 会員名称, ");
            sql.Append("    A.TOKUISAKI_CD    AS 会員番号,");
            sql.Append("    A.TOKUISAKI_NM    AS 会員名称,");
            sql.Append("    A.TANTOSHA_CD     AS 担当者コード, ");
            sql.Append("    D.TANTOSHA_NM     AS 担当者名, ");
            sql.Append("    A.TORIHIKI_KUBUN2 AS 取引区分２ ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_TORIHIKI_DENPYO AS A ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("    TB_CM_TANTOSHA AS D ");
            sql.Append("    ON A.KAISHA_CD   = D.KAISHA_CD ");
            sql.Append("    AND A.TANTOSHA_CD = D.TANTOSHA_CD ");
            sql.Append("WHERE ");
            sql.Append("        A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("    AND A.SHISHO_CD = @SHISHO_CD");
            sql.Append("    AND A.DENPYO_KUBUN   = 2 ");
            sql.Append("    AND A.KAIKEI_NENDO   = @KAIKEI_NENDO ");
            sql.Append("    AND (A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO) ");
            //sql.Append("    AND (A.KAIIN_BANGO BETWEEN @KAIIN_BANGO_FR AND @KAIIN_BANGO_TO) ");
            sql.Append("    AND (A.TOKUISAKI_CD BETWEEN @KAIIN_BANGO_FR AND @KAIIN_BANGO_TO) ");
            sql.Append("    AND (A.TANTOSHA_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO) ");
            sql.Append(" AND A.DENPYO_BANGO IN (");
            sql.Append("SELECT");
            sql.Append(" DENPYO_BANGO ");
            sql.Append("FROM");
            sql.Append(" TB_HN_TORIHIKI_MEISAI ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = A.KAISHA_CD");
            sql.Append(" AND SHISHO_CD = A.SHISHO_CD");
            sql.Append(" AND DENPYO_KUBUN = A.DENPYO_KUBUN");
            sql.Append(" AND KAIKEI_NENDO = A.KAIKEI_NENDO");
            sql.Append(" AND (SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO)");
            sql.Append(")");

            // 検索コード
            if (!ValChk.IsEmpty(this.txtSearchCode.Text))
            {
                sql.AppendFormat("    AND A.SHOHYO_BANGO = '{0}'", this.txtSearchCode.Text);
            }
            // 初期起動時
            if (isInitial)
            {
                // 表示しない条件を追加
                sql.Append("    AND 1 = 0 ");
            }
            sql.Append("ORDER BY ");
            sql.Append("    A.KAISHA_CD    ASC, ");
            sql.Append("    A.DENPYO_DATE  ASC, ");
            sql.Append("    A.DENPYO_BANGO ASC ");

            // 伝票日付
            DateTime DENPYO_DATE_FR = Util.ConvAdDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text,
                    this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
            DateTime DENPYO_DATE_TO = Util.ConvAdDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text,
                    this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);
            
            // 仕入先コード
            string KAIIN_BANGO_FR = "";
            if (ValChk.IsEmpty(this.txtSiiresakiCdFr.Text))
            {
                KAIIN_BANGO_FR = "0";
            }
            else
            {
                KAIIN_BANGO_FR = this.txtSiiresakiCdFr.Text;
            }
            string KAIIN_BANGO_TO = "";
            if (ValChk.IsEmpty(this.txtSiiresakiCdTo.Text))
            {
                KAIIN_BANGO_TO = "9999";
            }
            else
            {
                KAIIN_BANGO_TO = this.txtSiiresakiCdTo.Text;
            }

            // 担当者コード
            Decimal TANTOSHA_CD_FR;
            if (ValChk.IsEmpty(this.txtTantoshaCdFr.Text))
            {
                TANTOSHA_CD_FR = 0;
            }
            else
            {
                TANTOSHA_CD_FR = Util.ToDecimal(this.txtTantoshaCdFr.Text);
            }
            Decimal TANTOSHA_CD_TO;
            if (ValChk.IsEmpty(this.txtTantoshaCdTo.Text))
            {
                TANTOSHA_CD_TO = 999999;
            }
            else
            {
                TANTOSHA_CD_TO = Util.ToDecimal(this.txtTantoshaCdTo.Text);
            }

            // 商品CD
            Decimal SHOHIN_CD_FR;
            if (ValChk.IsEmpty(this.txtShohinCdFr.Text))
            {
                SHOHIN_CD_FR = 0;
            }
            else
            {
                SHOHIN_CD_FR = Util.ToDecimal(this.txtShohinCdFr.Text);
            }
            Decimal SHOHIN_CD_TO;
            if (ValChk.IsEmpty(this.txtShohinCdTo.Text))
            {
                //SHOHIN_CD_TO = 999999;
                SHOHIN_CD_TO = 999999999999;
            }
            else
            {
                SHOHIN_CD_TO = Util.ToDecimal(this.txtShohinCdTo.Text);
            }

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, DENPYO_DATE_FR);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, DENPYO_DATE_TO);
            dpc.SetParam("@KAIIN_BANGO_FR", SqlDbType.VarChar, 4, KAIIN_BANGO_FR);
            dpc.SetParam("@KAIIN_BANGO_TO", SqlDbType.VarChar, 4, KAIIN_BANGO_TO);
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 6, TANTOSHA_CD_FR);
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 6, TANTOSHA_CD_TO);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 15, SHOHIN_CD_FR);
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 15, SHOHIN_CD_TO);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 伝票金額と消費税を取引明細から取得するクエリ
            DbParamCollection dpc;
            StringBuilder sql = new StringBuilder();
            DataTable dtMeisaiResult;
            // Han.TB_取引明細(TB_HN_TORIHIKI_DENPYO)
            sql.Append("SELECT ");
            sql.Append("    SUM(CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append("             THEN (A.BAIKA_KINGAKU - A.SHOHIZEI) ");
            sql.Append("             ELSE A.BAIKA_KINGAKU ");
            sql.Append("        END) AS 金額合計,");
            sql.Append("    SUM(A.SHOHIZEI) AS 消費税合計 ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_TORIHIKI_MEISAI AS A ");
            sql.Append("WHERE ");
            sql.Append("        A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("    AND A.SHISHO_CD = @SHISHO_CD");
            sql.Append("    AND A.DENPYO_KUBUN = 2 ");
            sql.Append("    AND A.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("    AND A.DENPYO_BANGO = @DENPYO_BANGO");

            // 日付を編集する際に用いるワーク
            string[] aryDate;
            string tmpDate;

            // 列の定義を作成
            dtResult.Columns.Add("伝票日付", typeof(string));
            dtResult.Columns.Add("伝票番号", typeof(int));
            dtResult.Columns.Add("仕入先ｺｰﾄﾞ", typeof(int));
            dtResult.Columns.Add("仕入先名称", typeof(string));
            dtResult.Columns.Add("担当者", typeof(string));
            dtResult.Columns.Add("伝票金額", typeof(string));
            dtResult.Columns.Add("（消費税）", typeof(string));

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();
                
                aryDate = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["伝票日付"]), this.Dba);
                tmpDate = Util.ToInt(aryDate[2]).ToString("00") + "/" + aryDate[3].PadLeft(2, ' ') + "/" + aryDate[4].PadLeft(2, ' ');
                drResult["伝票日付"] = tmpDate;
                drResult["伝票番号"] = dtData.Rows[i]["伝票番号"];
                drResult["仕入先ｺｰﾄﾞ"] = dtData.Rows[i]["会員番号"];
                drResult["仕入先名称"] = dtData.Rows[i]["会員名称"];
                drResult["担当者"] = dtData.Rows[i]["担当者名"];

                // 伝票金額と消費税を取引明細から取得する
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, dtData.Rows[i]["伝票番号"]);
                dtMeisaiResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

                // 取引伝票の取引区分２が『2』は返品
                if (Util.ToDecimal(dtData.Rows[i]["取引区分２"]) == 2)
                {
                    //drResult["伝票金額"] = Util.FormatNum(Util.ToDecimal(dtMeisaiResult.Rows[0]["金額合計"]) * -1);
                    drResult["伝票金額"] = Util.FormatNum((Util.ToDecimal(dtMeisaiResult.Rows[0]["金額合計"]) + Util.ToDecimal(dtMeisaiResult.Rows[0]["消費税合計"])) * -1);
                    drResult["（消費税）"] = Util.FormatNum(Util.ToDecimal(dtMeisaiResult.Rows[0]["消費税合計"]) * -1);
                }
                else
                {
                    //drResult["伝票金額"] = Util.FormatNum(dtMeisaiResult.Rows[0]["金額合計"]);
                    drResult["伝票金額"] = Util.FormatNum((Util.ToDecimal(dtMeisaiResult.Rows[0]["金額合計"]) + Util.ToDecimal(dtMeisaiResult.Rows[0]["消費税合計"])));
                    drResult["（消費税）"] = Util.FormatNum(dtMeisaiResult.Rows[0]["消費税合計"]);
                }
                
                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }

        /// <summary>
        /// データ取得してグリッドに設定
        /// <param name="isInitial">初期処理であるかどうか</param>
        /// </summary>
        private void getListData(bool isInitial)
        {
            // データ取得のSQLを発行してGridに反映
            DataTable dtList = new DataTable();
            try
            {
                dtList = this.GetTB_HN_ZIDO_SHIWAKE_RIREKI(isInitial);
                if (!isInitial && dtList.Rows.Count == 0)
                {
                    // データが無い時
                    this.btnEnter.Enabled = false;
                    this.dgvList.Enabled = false;
                    //Msg.Info("該当データがありません。");
                    Msg.Info("該当データはありません。");
                    return;
                }
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                return;
            }

            // データが有る時
            this.btnEnter.Enabled = true;
            this.dgvList.Enabled = true;

            // 取得したデータを表示用に編集
            DataTable dtDsp = this.EditDataList(dtList);

            this.dgvList.DataSource = dtDsp;
            if (!isInitial)
            {
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[2] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["伝票番号"].Value),
                this.getDenpyoCondition(),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 検索条件設定
        /// </summary>
        /// <param name="denpyoCondition">検索条件</param>
        private void setDenpyoCondition(string denpyoCondition)
        {
            string[] cnd = denpyoCondition.Split(',');
            if (cnd.Length != 0)
            {
                try
                {
                    string[] d = Util.ConvJpDate(Util.ToDate(cnd[0]), this.Dba);
                    SetJpFr(d);
                    d = Util.ConvJpDate(Util.ToDate(cnd[1]), this.Dba);
                    SetJpTo(d);

                    this.txtSiiresakiCdFr.Text = cnd[2];
                    this.lblSiiresakiNmFr.Text = cnd[3];
                    this.txtSiiresakiCdTo.Text = cnd[4];
                    this.lblSiiresakiNmTo.Text = cnd[5];

                    this.txtTantoshaCdFr.Text = cnd[6];
                    this.lblTantoshaNmFr.Text = cnd[7];
                    this.txtTantoshaCdTo.Text = cnd[8];
                    this.lblTantoshaNmTo.Text = cnd[9];

                    this.txtShohinCdFr.Text = cnd[10];
                    this.lblShohinNmFr.Text = cnd[11];
                    this.txtShohinCdTo.Text = cnd[12];
                    this.lblShohinNmTo.Text = cnd[13];

                    this.txtSearchCode.Text = cnd[14];
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Print(ex.Message);
                }
            }
        }

        /// <summary>
        /// 検索条件取得
        /// </summary>
        /// <returns>検索条件配列</returns>
        private string getDenpyoCondition()
        {
            string[] cnd = {
                            Util.ConvAdDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text, this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba).ToString("yyyy/MM/dd"),
                            Util.ConvAdDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text, this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba).ToString("yyyy/MM/dd"),
                            this.txtSiiresakiCdFr.Text,
                            this.lblSiiresakiNmFr.Text,
                            this.txtSiiresakiCdTo.Text,
                            this.lblSiiresakiNmTo.Text,
                            this.txtTantoshaCdFr.Text,
                            this.lblTantoshaNmFr.Text,
                            this.txtTantoshaCdTo.Text,
                            this.lblTantoshaNmTo.Text,
                            this.txtShohinCdFr.Text,
                            this.lblShohinNmFr.Text,
                            this.txtShohinCdTo.Text,
                            this.lblShohinNmTo.Text,
                            this.txtSearchCode.Text
            };

            return string.Join(",", cnd);
        }
        #endregion
    }

}
