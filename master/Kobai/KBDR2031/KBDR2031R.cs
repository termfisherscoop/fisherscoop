﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr2031
{
    /// <summary>
    /// KBDR2031R の概要の説明です。
    /// </summary>
    public partial class KBDR2031R : BaseReport
    {

        public KBDR2031R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
