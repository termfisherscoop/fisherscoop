﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbdr2031
{
    /// <summary>
    /// KOBR1032R の概要の説明です。
    /// </summary>
    public partial class KBDR2032R : BaseReport
    {

        public KBDR2032R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
        int ct;
        private void KOBR1032R_FetchData(object sender, FetchEventArgs eArgs)
        {
            ct++;
            if (ct == 49)
            {
                this.直線16.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Solid;
                ct = 0;
            }
            else
            {
                this.直線16.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            }
        }

        private void KOBR1032R_PageEnd(object sender, System.EventArgs e)
        {
            this.直線16.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            ct = 0;
        }
    }
}
