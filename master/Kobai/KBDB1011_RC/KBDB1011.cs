﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.controls;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000005 RID: 5
	public partial class KBDB1011 : BasePgForm
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600001D RID: 29 RVA: 0x00008172 File Offset: 0x00006372
		public int Mode
		{
			get
			{
				return this._mode;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600001E RID: 30 RVA: 0x0000817A File Offset: 0x0000637A
		public int PackDpyNo
		{
			get
			{
				return this._packDpyNo;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600001F RID: 31 RVA: 0x00008182 File Offset: 0x00006382
		public DataTable ZeiSetting
		{
			get
			{
				return this._zeiSetting;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000020 RID: 32 RVA: 0x0000818A File Offset: 0x0000638A
		public DataTable JdSwkSettingA
		{
			get
			{
				return this._jdSwkSettingA;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000021 RID: 33 RVA: 0x00008192 File Offset: 0x00006392
		public DataTable JdSwkSettingB
		{
			get
			{
				return this._jdSwkSettingB;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000022 RID: 34 RVA: 0x0000819A File Offset: 0x0000639A
		public DataTable SwkTgtData
		{
			get
			{
				return this._dtSwkTgtData;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000023 RID: 35 RVA: 0x000081A2 File Offset: 0x000063A2
		public DataSet TaishakuData
		{
			get
			{
				return this._dsTaishakuData;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000024 RID: 36 RVA: 0x000081AA File Offset: 0x000063AA
		public Hashtable Condition
		{
			get
			{
				return this.GetCondition();
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000025 RID: 37 RVA: 0x000081B2 File Offset: 0x000063B2
		public bool Flg
		{
			get
			{
				return this._dtFlg;
			}
		}

		// Token: 0x06000026 RID: 38 RVA: 0x000081BA File Offset: 0x000063BA
		public KBDB1011()
		{
			this.InitializeComponent();
			base.BindGotFocusEvent();
			this._mode = 1;
		}

		// Token: 0x06000027 RID: 39 RVA: 0x000081EC File Offset: 0x000063EC
		protected override void InitForm()
		{
			this.lblMessage.Text = "『会計期間第" + Util.ToString(base.UInfo.KessanKi) + "期が選択されています』";
			this.GetZeiSetting();
			if (!this.IsZeiSetting())
			{
				base.Close();
			}
			this.txtMizuageShishoCd.Text = base.UInfo.ShishoCd;
			this.lblMizuageShishoNm.Text = base.UInfo.ShishoNm;
			if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
			{
				DataRow personInfo = this.GetPersonInfo(base.UInfo.UserCd);
				if (personInfo != null)
				{
					base.UInfo.ShishoCd = personInfo["SHISHO_CD"].ToString();
					base.UInfo.ShishoNm = base.Dba.GetName(base.UInfo, "TB_CM_SHISHO", base.UInfo.ShishoCd, base.UInfo.ShishoCd);
					this.txtMizuageShishoCd.Text = base.UInfo.ShishoCd;
					this.lblMizuageShishoNm.Text = base.UInfo.ShishoNm;
				}
			}
			this.txtMizuageShishoCd.Enabled = false;
			this.GetTB_HN_ZIDO_SHIWAKE_SETTEI_A();
			if (this._jdSwkSettingA.Rows.Count == 0)
			{
				Msg.Notice("仕訳情報が設定されていません。");
				base.Close();
			}
			this.GetTB_HN_ZIDO_SHIWAKE_SETTEI_B();
			if (this._jdSwkSettingB.Rows.Count == 0)
			{
				Msg.Notice("仕訳情報が設定されていません。");
				base.Close();
			}
			if (this.rdbGenkinKake.Enabled)
			{
				this.rdbGenkinKake.Checked = true;
				this.rdbGenkinKake.Focus();
			}
			else if (this.rdbGenkin.Enabled)
			{
				this.rdbGenkin.Checked = true;
				this.rdbGenkin.Focus();
			}
			else if (this.rdbShimebi.Enabled)
			{
				this.rdbShimebi.Checked = true;
				this.rdbShimebi.Focus();
			}
			this.txtShimebi.Enabled = this.rdbShimebi.Checked;
			this.ClearForm();
			this.ControlItemsBySettings();
			this._dtFlg = false;
			this.btnEsc.Text = "Esc\n\r終了"; //"\n\r"を一つ削除
			//if (base.MenuFrm != null)
			//{
			//	((Button)base.MenuFrm.Controls["btnEsc"]).Text = "Esc\n\r\n\r終了";
			//}
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00008448 File Offset: 0x00006648
		protected override void OnMoveFocus()
		{
			string activeCtlNm = base.ActiveCtlNm;
			uint num = PrivateImplementationDetails.ComputeStringHash(activeCtlNm);
			if (num <= 2975426115u)
			{
				if (num <= 545334842u)
				{
					if (num != 190880578u)
					{
						if (num != 545334842u)
						{
							goto IL_166;
						}
						if (!(activeCtlNm == "txtMizuageShishoCd"))
						{
							goto IL_166;
						}
					}
					else if (!(activeCtlNm == "txtFunanushiCdTo"))
					{
						goto IL_166;
					}
				}
				else if (num != 2651571839u)
				{
					if (num != 2768747093u)
					{
						if (num != 2975426115u)
						{
							goto IL_166;
						}
						if (!(activeCtlNm == "rdbGenkin"))
						{
							goto IL_166;
						}
					}
					else if (!(activeCtlNm == "txtDpyDtFrJpYear"))
					{
						goto IL_166;
					}
				}
				else if (!(activeCtlNm == "txtTekiyoCd"))
				{
					goto IL_166;
				}
			}
			else if (num <= 4035117827u)
			{
				if (num != 3749619808u)
				{
					if (num != 3888954959u)
					{
						if (num != 4035117827u)
						{
							goto IL_166;
						}
						if (!(activeCtlNm == "txtFunanushiCdFr"))
						{
							goto IL_166;
						}
					}
					else if (!(activeCtlNm == "rdbGenkinKake"))
					{
						goto IL_166;
					}
				}
				else if (!(activeCtlNm == "txtDpyDtToJpYear"))
				{
					goto IL_166;
				}
			}
			else if (num != 4054953420u)
			{
				if (num != 4254132416u)
				{
					if (num != 4263780086u)
					{
						goto IL_166;
					}
					if (!(activeCtlNm == "txtSwkDpyDtJpYear"))
					{
						goto IL_166;
					}
				}
				else if (!(activeCtlNm == "txtTantoCd"))
				{
					goto IL_166;
				}
			}
			else if (!(activeCtlNm == "rdbShimebi"))
			{
				goto IL_166;
			}
			this.btnF1.Enabled = true;
			return;
			IL_166:
			this.btnF1.Enabled = false;
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000085C7 File Offset: 0x000067C7
		public override void PressEsc()
		{
			if (this._mode == 2)
			{
				this.InitForm();
				return;
			}
			base.DialogResult = DialogResult.Cancel;
			base.PressEsc();
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000085E8 File Offset: 0x000067E8
		public override void PressF1()
		{
			string activeCtlNm = base.ActiveCtlNm;
			uint num = PrivateImplementationDetails.ComputeStringHash(activeCtlNm);
			Type type;
			if (num <= 2975426115u)
			{
				if (num <= 545334842u)
				{
					if (num != 190880578u)
					{
						if (num != 545334842u)
						{
							return;
						}
						if (!(activeCtlNm == "txtMizuageShishoCd"))
						{
							return;
						}
						// アセンブリのロード
						type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe").GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
						if (!(type != null))
						{
							return;
						}
						object obj = Activator.CreateInstance(type);
						if (obj == null)
						{
							return;
						}
						BasePgForm basePgForm = (BasePgForm)obj;
						basePgForm.Par1 = "1";
						basePgForm.ShowDialog(this);
						if (basePgForm.DialogResult == DialogResult.OK)
						{
							string[] array = (string[])basePgForm.OutData;
							this.txtMizuageShishoCd.Text = array[0];
							this.lblMizuageShishoNm.Text = array[1];
							return;
						}
						return;
					}
					else
					{
						if (!(activeCtlNm == "txtFunanushiCdTo"))
						{
							return;
						}
						// アセンブリのロード
						type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe").GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
						if (!(type != null))
						{
							return;
						}
						object obj2 = Activator.CreateInstance(type);
						if (obj2 == null)
						{
							return;
						}
						BasePgForm basePgForm2 = (BasePgForm)obj2;
						basePgForm2.Par1 = "1";
						basePgForm2.Par2 = "1";
						basePgForm2.ShowDialog(this);
						if (basePgForm2.DialogResult == DialogResult.OK)
						{
							string[] array2 = (string[])basePgForm2.OutData;
							this.txtFunanushiCdTo.Text = array2[0];
							this.lblFunanushiNmTo.Text = array2[1];
							return;
						}
						return;
					}
				}
				else if (num != 2651571839u)
				{
					if (num != 2768747093u)
					{
						if (num != 2975426115u)
						{
							return;
						}
						if (!(activeCtlNm == "rdbGenkin"))
						{
							return;
						}
					}
					else
					{
						if (!(activeCtlNm == "txtDpyDtFrJpYear"))
						{
							return;
						}
						goto IL_281;
					}
				}
				else
				{
					if (!(activeCtlNm == "txtTekiyoCd"))
					{
						return;
					}
					// アセンブリのロード
					type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2061.exe").GetType("jp.co.fsi.cm.cmcm2061.CMCM2061");
					if (!(type != null))
					{
						return;
					}
					object obj3 = Activator.CreateInstance(type);
					if (obj3 == null)
					{
						return;
					}
					BasePgForm basePgForm3 = (BasePgForm)obj3;
					basePgForm3.Par1 = "1";
					basePgForm3.InData = Util.ToString(this.txtMizuageShishoCd.Text);
					basePgForm3.ShowDialog(this);
					if (basePgForm3.DialogResult == DialogResult.OK)
					{
						string[] array3 = (string[])basePgForm3.OutData;
						this.txtTekiyoCd.Text = array3[0];
						this.txtTekiyo.Text = array3[1];
						return;
					}
					return;
				}
			}
			else if (num <= 4035117827u)
			{
				if (num != 3749619808u)
				{
					if (num != 3888954959u)
					{
						if (num != 4035117827u)
						{
							return;
						}
						if (!(activeCtlNm == "txtFunanushiCdFr"))
						{
							return;
						}
						// アセンブリのロード
						type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe").GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
						if (!(type != null))
						{
							return;
						}
						object obj4 = Activator.CreateInstance(type);
						if (obj4 == null)
						{
							return;
						}
						BasePgForm basePgForm4 = (BasePgForm)obj4;
						basePgForm4.Par1 = "1";
						basePgForm4.Par2 = "1";
						basePgForm4.ShowDialog(this);
						if (basePgForm4.DialogResult == DialogResult.OK)
						{
							string[] array4 = (string[])basePgForm4.OutData;
							this.txtFunanushiCdFr.Text = array4[0];
							this.lblFunanushiNmFr.Text = array4[1];
							return;
						}
						return;
					}
					else if (!(activeCtlNm == "rdbGenkinKake"))
					{
						return;
					}
				}
				else
				{
					if (!(activeCtlNm == "txtDpyDtToJpYear"))
					{
						return;
					}
					// アセンブリのロード
					type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe").GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
					if (!(type != null))
					{
						return;
					}
					object obj5 = Activator.CreateInstance(type);
					if (obj5 == null)
					{
						return;
					}
					BasePgForm basePgForm5 = (BasePgForm)obj5;
					basePgForm5.InData = this.lblDpyDtToGengo.Text;
					basePgForm5.ShowDialog(this);
					if (basePgForm5.DialogResult == DialogResult.OK)
					{
						string[] array5 = (string[])basePgForm5.OutData;
						this.lblDpyDtToGengo.Text = array5[1];
						this.SetJpTo();
						return;
					}
					return;
				}
			}
			else if (num != 4054953420u)
			{
				if (num != 4254132416u)
				{
					if (num != 4263780086u)
					{
						return;
					}
					if (!(activeCtlNm == "txtSwkDpyDtJpYear"))
					{
						return;
					}
					// アセンブリのロード
					type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe").GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
					if (!(type != null))
					{
						return;
					}
					object obj6 = Activator.CreateInstance(type);
					if (obj6 == null)
					{
						return;
					}
					BasePgForm basePgForm6 = (BasePgForm)obj6;
					basePgForm6.InData = this.lblSwkDpyDtGengo.Text;
					basePgForm6.ShowDialog(this);
					if (basePgForm6.DialogResult == DialogResult.OK)
					{
						string[] array6 = (string[])basePgForm6.OutData;
						this.lblSwkDpyDtGengo.Text = array6[1];
						this.SetJp();
						return;
					}
					return;
				}
				else
				{
					if (!(activeCtlNm == "txtTantoCd"))
					{
						return;
					}
					// アセンブリのロード
					type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe").GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
					if (!(type != null))
					{
						return;
					}
					object obj7 = Activator.CreateInstance(type);
					if (obj7 == null)
					{
						return;
					}
					BasePgForm basePgForm7 = (BasePgForm)obj7;
					basePgForm7.Par1 = "1";
					basePgForm7.ShowDialog(this);
					if (basePgForm7.DialogResult == DialogResult.OK)
					{
						string[] array7 = (string[])basePgForm7.OutData;
						this.txtTantoCd.Text = array7[0];
						this.lblTantoNm.Text = array7[1];
						return;
					}
					return;
				}
			}
			else if (!(activeCtlNm == "rdbShimebi"))
			{
				return;
			}
			using (KBDB1012 kbdb = new KBDB1012())
			{
				kbdb.ShowDialog(this);
				if (kbdb.DialogResult == DialogResult.OK)
				{
					string[] updateItems = (string[])kbdb.OutData;
					this.SetUpdateItems(updateItems);
					this._mode = 2;
					this.lblUpdMode.Visible = true;
					this.btnF3.Enabled = true;
					this.btnF6.Enabled = false;
					this.btnEsc.Text = "Esc\n\r\n\r取消";
					//if (base.MenuFrm != null)
					//{
					//	((Button)base.MenuFrm.Controls["btnEsc"]).Text = "Esc\n\r\n\r取消";
					//}
				}
				return;
			}
			IL_281:
			// アセンブリのロード
			type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe").GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
			if (type != null)
			{
				object obj8 = Activator.CreateInstance(type);
				if (obj8 != null)
				{
					BasePgForm basePgForm8 = (BasePgForm)obj8;
					basePgForm8.InData = this.lblDpyDtFrGengo.Text;
					basePgForm8.ShowDialog(this);
					if (basePgForm8.DialogResult == DialogResult.OK)
					{
						string[] array8 = (string[])basePgForm8.OutData;
						this.lblDpyDtFrGengo.Text = array8[1];
						this.SetJpFr();
						return;
					}
				}
			}
		}

		// Token: 0x0600002B RID: 43 RVA: 0x00008C5C File Offset: 0x00006E5C
		public override void PressF3()
		{
			if (!this.btnF3.Enabled)
			{
				return;
			}
			if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
			{
				return;
			}
			KBDB1016 kbdb = new KBDB1016();
			kbdb.Show();
			kbdb.Refresh();
			try
			{
				base.Dba.BeginTransaction();
				bool flag = new KBDB1011DA(base.UInfo, base.Dba, base.Config).MakeSwkData(3, this._packDpyNo, null, null);
				kbdb.Close();
				if (flag)
				{
					base.Dba.Commit();
					this.InitForm();
				}
				else
				{
					base.Dba.Rollback();
					Msg.Error("更新に失敗しました。" + Environment.NewLine + "もう一度やり直して下さい。");
				}
			}
			finally
			{
				base.Dba.Rollback();
			}
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00008D28 File Offset: 0x00006F28
		public override void PressF4()
		{
			if (!this.btnF4.Enabled)
			{
				return;
			}
			if (!this.ValidateAll())
			{
				return;
			}
			this.GetTB_HN_ZIDO_SHIWAKE_SETTEI_A(Util.ToDate(this.Condition["SwkDpyDt"]));
			this.GetTB_HN_ZIDO_SHIWAKE_SETTEI_B(Util.ToDate(this.Condition["SwkDpyDt"]));
			KBDB1011DA kbdb1011DA = new KBDB1011DA(base.UInfo, base.Dba, base.Config);
			this._dtSwkTgtData = kbdb1011DA.GetSwkTgtData(this._mode, this.Condition);
			this._dsTaishakuData = kbdb1011DA.GetTaishakuData(this.Condition, this._zeiSetting, this._jdSwkSettingA, this._jdSwkSettingB, this._dtSwkTgtData);
			using (KBDB1013 kbdb = new KBDB1013(this, base.Config))
			{
				if (this._mode == 2)
				{
					kbdb.InData = "2";
				}
				if (kbdb.ShowDialog(this) == DialogResult.OK)
				{
					this.InitForm();
				}
			}
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00008E2C File Offset: 0x0000702C
		public override void PressF6()
		{
			if (!this.btnF6.Enabled)
			{
				return;
			}
			this.UpdateData();
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00008E44 File Offset: 0x00007044
		public override void PressF12()
		{
			using (KBDB1015 kbdb = new KBDB1015())
			{
				if (kbdb.ShowDialog(this) == DialogResult.OK)
				{
					base.Config.ReloadConfig();
					this.ControlItemsBySettings();
				}
			}
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00008E90 File Offset: 0x00007090
		private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidMizuageShishoCd())
			{
				e.Cancel = true;
				this.lblMizuageShishoNm.Text = "";
				this.txtMizuageShishoCd.SelectAll();
				this.txtMizuageShishoCd.Focus();
			}
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00008EC8 File Offset: 0x000070C8
		private void rdbShimebi_CheckedChanged(object sender, EventArgs e)
		{
			this.txtShimebi.Enabled = this.rdbShimebi.Checked;
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00008EE0 File Offset: 0x000070E0
		private void txtShimebi_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidShimebi())
			{
				e.Cancel = true;
				this.txtShimebi.SelectAll();
			}
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00008EFC File Offset: 0x000070FC
		private void txtDpyDtFrJpYear_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsYear(this.txtDpyDtFrJpYear.Text, this.txtDpyDtFrJpYear.MaxLength))
			{
				e.Cancel = true;
				this.txtDpyDtFrJpYear.SelectAll();
				return;
			}
			this.txtDpyDtFrJpYear.Text = Util.ToString(IsValid.SetYear(this.txtDpyDtFrJpYear.Text));
			this.CheckJpFr();
			this.SetJpFr();
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00008F6C File Offset: 0x0000716C
		private void txtDpyDtFrMonth_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsMonth(this.txtDpyDtFrMonth.Text, this.txtDpyDtFrMonth.MaxLength))
			{
				e.Cancel = true;
				this.txtDpyDtFrMonth.SelectAll();
				return;
			}
			this.txtDpyDtFrMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDpyDtFrMonth.Text));
			this.CheckJpFr();
			this.SetJpFr();
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00008FDC File Offset: 0x000071DC
		private void txtDpyDtFrDay_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsDay(this.txtDpyDtFrDay.Text, this.txtDpyDtFrDay.MaxLength))
			{
				e.Cancel = true;
				this.txtDpyDtFrDay.SelectAll();
				return;
			}
			this.txtDpyDtFrDay.Text = Util.ToString(IsValid.SetDay(this.txtDpyDtFrDay.Text));
			this.CheckJpFr();
			this.SetJpFr();
		}

		// Token: 0x06000035 RID: 53 RVA: 0x0000904C File Offset: 0x0000724C
		private void txtDpyDtToJpYear_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsYear(this.txtDpyDtToJpYear.Text, this.txtDpyDtToJpYear.MaxLength))
			{
				e.Cancel = true;
				this.txtDpyDtToJpYear.SelectAll();
				return;
			}
			this.txtDpyDtToJpYear.Text = Util.ToString(IsValid.SetYear(this.txtDpyDtToJpYear.Text));
			this.CheckJpTo();
			this.SetJpTo();
		}

		// Token: 0x06000036 RID: 54 RVA: 0x000090BC File Offset: 0x000072BC
		private void txtDpyDtToMonth_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsMonth(this.txtDpyDtToMonth.Text, this.txtDpyDtToMonth.MaxLength))
			{
				e.Cancel = true;
				this.txtDpyDtToMonth.SelectAll();
				return;
			}
			this.txtDpyDtToMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDpyDtToMonth.Text));
			this.CheckJpTo();
			this.SetJpTo();
		}

		// Token: 0x06000037 RID: 55 RVA: 0x0000912C File Offset: 0x0000732C
		private void txtDpyDtToDay_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsDay(this.txtDpyDtToDay.Text, this.txtDpyDtToDay.MaxLength))
			{
				e.Cancel = true;
				this.txtDpyDtToDay.SelectAll();
				return;
			}
			this.txtDpyDtToDay.Text = Util.ToString(IsValid.SetDay(this.txtDpyDtToDay.Text));
			this.CheckJpTo();
			this.SetJpTo();
			this.lblSwkDpyDtGengo.Text = this.lblDpyDtToGengo.Text;
			this.txtSwkDpyDtJpYear.Text = this.txtDpyDtToJpYear.Text;
			this.txtSwkDpyDtMonth.Text = this.txtDpyDtToMonth.Text;
			this.txtSwkDpyDtDay.Text = this.txtDpyDtToDay.Text;
		}

		// Token: 0x06000038 RID: 56 RVA: 0x000091F2 File Offset: 0x000073F2
		private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidFunanushiCdFr())
			{
				e.Cancel = true;
				this.txtFunanushiCdFr.SelectAll();
			}
		}

		// Token: 0x06000039 RID: 57 RVA: 0x0000920E File Offset: 0x0000740E
		private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidFunanushiCdTo())
			{
				e.Cancel = true;
				this.txtFunanushiCdTo.SelectAll();
			}
		}

		// Token: 0x0600003A RID: 58 RVA: 0x0000922A File Offset: 0x0000742A
		private void txtTantoCd_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidTantoCd())
			{
				e.Cancel = true;
				this.txtTantoCd.SelectAll();
			}
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00009248 File Offset: 0x00007448
		private void txtSwkDpyDtJpYear_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsYear(this.txtSwkDpyDtJpYear.Text, this.txtSwkDpyDtJpYear.MaxLength))
			{
				e.Cancel = true;
				this.txtSwkDpyDtJpYear.SelectAll();
				return;
			}
			this.txtSwkDpyDtJpYear.Text = Util.ToString(IsValid.SetYear(this.txtSwkDpyDtJpYear.Text));
			this.CheckJp();
			this.SetJp();
		}

		// Token: 0x0600003C RID: 60 RVA: 0x000092B8 File Offset: 0x000074B8
		private void txtSwkDpyDtMonth_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsMonth(this.txtSwkDpyDtMonth.Text, this.txtSwkDpyDtMonth.MaxLength))
			{
				e.Cancel = true;
				this.txtSwkDpyDtMonth.SelectAll();
				return;
			}
			this.txtSwkDpyDtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtSwkDpyDtMonth.Text));
			this.CheckJp();
			this.SetJp();
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00009328 File Offset: 0x00007528
		private void txtSwkDpyDtDay_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsDay(this.txtSwkDpyDtDay.Text, this.txtSwkDpyDtDay.MaxLength))
			{
				e.Cancel = true;
				this.txtSwkDpyDtDay.SelectAll();
				return;
			}
			this.txtSwkDpyDtDay.Text = Util.ToString(IsValid.SetDay(this.txtSwkDpyDtDay.Text));
			this.CheckJp();
			this.SetJp();
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00009396 File Offset: 0x00007596
		private void txtTekiyoCd_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidTekiyoCd())
			{
				e.Cancel = true;
				this.txtTekiyoCd.SelectAll();
			}
		}

		// Token: 0x0600003F RID: 63 RVA: 0x000093B2 File Offset: 0x000075B2
		private void txtTekiyo_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidTekiyo())
			{
				e.Cancel = true;
				this.txtTekiyo.SelectAll();
				this._dtFlg = false;
				return;
			}
			this._dtFlg = true;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000093DD File Offset: 0x000075DD
		private void txtTekiyo_KeyDown(object sender, KeyEventArgs e)
		{
			if (!this.btnF6.Enabled)
			{
				return;
			}
			if (e.KeyCode == Keys.Return && this.Flg)
			{
				this._dtFlg = false;
				this.UpdateData();
			}
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00009410 File Offset: 0x00007610
		private bool IsValidMizuageShishoCd()
		{
			if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
			{
				Msg.Notice("数値のみで入力してください。");
				return false;
			}
			if (object.Equals(this.txtMizuageShishoCd.Text, "0"))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			this.lblMizuageShishoNm.Text = base.Dba.GetName(base.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
			if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
			{
				Msg.Notice("入力に誤りがあります。");
				return false;
			}
			if (Util.ToInt(base.UInfo.ShishoCd) != Util.ToInt(this.txtMizuageShishoCd.Text))
			{
				base.UInfo.ShishoCd = this.txtMizuageShishoCd.Text;
				base.UInfo.ShishoNm = this.lblMizuageShishoNm.Text;
			}
			return true;
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00009550 File Offset: 0x00007750
		private bool IsValidFunanushiCdFr()
		{
			if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
			{
				this.lblFunanushiNmFr.Text = "先\u3000頭";
				return true;
			}
			if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			string name = base.Dba.GetName(base.UInfo, "VI_HN_TORIHIKISAKI_JOHO", this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text, 1m);
			this.lblFunanushiNmFr.Text = name;
			return true;
		}

		// Token: 0x06000043 RID: 67 RVA: 0x000095E0 File Offset: 0x000077E0
		private bool IsValidFunanushiCdTo()
		{
			if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
			{
				this.lblFunanushiNmTo.Text = "最\u3000後";
				return true;
			}
			if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			string name = base.Dba.GetName(base.UInfo, "VI_HN_TORIHIKISAKI_JOHO", this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text, 1m);
			this.lblFunanushiNmTo.Text = name;
			return true;
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00009670 File Offset: 0x00007870
		private bool IsValidTantoCd()
		{
			if (ValChk.IsEmpty(this.txtTantoCd.Text))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			if (!ValChk.IsNumber(this.txtTantoCd.Text))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			string name = base.Dba.GetName(base.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoCd.Text);
			if (ValChk.IsEmpty(name))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			this.lblTantoNm.Text = name;
			return true;
		}

		// Token: 0x06000045 RID: 69 RVA: 0x0000970C File Offset: 0x0000790C
		private bool IsValidTekiyoCd()
		{
			if (ValChk.IsEmpty(this.txtTekiyoCd.Text))
			{
				return true;
			}
			if (!ValChk.IsNumber(this.txtTekiyoCd.Text))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			string name = base.Dba.GetName(base.UInfo, "TB_HN_TEKIYO", this.txtMizuageShishoCd.Text, this.txtTekiyoCd.Text);
			if (ValChk.IsEmpty(name))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			this.txtTekiyo.Text = name;
			return true;
		}

		// Token: 0x06000046 RID: 70 RVA: 0x0000979B File Offset: 0x0000799B
		private bool IsValidTekiyo()
		{
			if (!ValChk.IsWithinLength(this.txtTekiyo.Text, this.txtTekiyo.MaxLength))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			return true;
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000097C8 File Offset: 0x000079C8
		private bool ValidateAll()
		{
			if (!this.IsValidMizuageShishoCd())
			{
				this.txtMizuageShishoCd.Focus();
				this.txtMizuageShishoCd.SelectAll();
				return false;
			}
			if (!this.IsValidShimebi())
			{
				this.txtShimebi.Focus();
				this.txtShimebi.SelectAll();
				return false;
			}
			if (this.rdbShimebi.Checked && ValChk.IsEmpty(this.txtShimebi.Text))
			{
				Msg.Notice("入力に誤りがあります。");
				this.txtShimebi.Focus();
				return false;
			}
			if (!IsValid.IsYear(this.txtDpyDtFrJpYear.Text, this.txtDpyDtFrJpYear.MaxLength))
			{
				this.txtDpyDtFrJpYear.Focus();
				this.txtDpyDtFrJpYear.SelectAll();
				return false;
			}
			if (!IsValid.IsMonth(this.txtDpyDtFrMonth.Text, this.txtDpyDtFrMonth.MaxLength))
			{
				this.txtDpyDtFrMonth.Focus();
				this.txtDpyDtFrMonth.SelectAll();
				return false;
			}
			if (!IsValid.IsDay(this.txtDpyDtFrDay.Text, this.txtDpyDtFrDay.MaxLength))
			{
				this.txtDpyDtFrDay.Focus();
				this.txtDpyDtFrDay.SelectAll();
				return false;
			}
			DateTime dateTime = Util.ConvAdDate(this.lblDpyDtFrGengo.Text, Util.ToInt(this.txtDpyDtFrJpYear.Text), Util.ToInt(this.txtDpyDtFrMonth.Text), Util.ToInt(this.txtDpyDtFrDay.Text), base.Dba);
			if (dateTime.CompareTo(Util.ToDate(base.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"])) < 0 || dateTime.CompareTo(Util.ToDate(base.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"])) > 0)
			{
				Msg.Error("会計期間内ではありません！");
				this.txtDpyDtFrJpYear.Focus();
				return false;
			}
			this.CheckJpFr();
			this.SetJpFr();
			if (!IsValid.IsYear(this.txtDpyDtToJpYear.Text, this.txtDpyDtToJpYear.MaxLength))
			{
				this.txtDpyDtToJpYear.Focus();
				this.txtDpyDtToJpYear.SelectAll();
				return false;
			}
			if (!IsValid.IsMonth(this.txtDpyDtToMonth.Text, this.txtDpyDtToMonth.MaxLength))
			{
				this.txtDpyDtToMonth.Focus();
				this.txtDpyDtToMonth.SelectAll();
				return false;
			}
			if (!IsValid.IsDay(this.txtDpyDtToDay.Text, this.txtDpyDtToDay.MaxLength))
			{
				this.txtDpyDtToDay.Focus();
				this.txtDpyDtToDay.SelectAll();
				return false;
			}
			DateTime value = Util.ConvAdDate(this.lblDpyDtToGengo.Text, Util.ToInt(this.txtDpyDtToJpYear.Text), Util.ToInt(this.txtDpyDtToMonth.Text), Util.ToInt(this.txtDpyDtToDay.Text), base.Dba);
			if (value.CompareTo(Util.ToDate(base.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"])) < 0 || value.CompareTo(Util.ToDate(base.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"])) > 0)
			{
				Msg.Error("会計期間内ではありません！");
				this.txtDpyDtToJpYear.Focus();
				return false;
			}
			this.CheckJpTo();
			this.SetJpTo();
			if (dateTime.CompareTo(value) > 0)
			{
				Msg.Error("入力に誤りがあります。");
				this.txtDpyDtFrJpYear.Focus();
				return false;
			}
			if (!this.IsValidFunanushiCdFr())
			{
				this.txtFunanushiCdFr.Focus();
				return false;
			}
			if (!this.IsValidFunanushiCdTo())
			{
				this.txtFunanushiCdTo.Focus();
				return false;
			}
			if (!this.IsValidTantoCd())
			{
				this.txtTantoCd.Focus();
				return false;
			}
			if (!IsValid.IsYear(this.txtSwkDpyDtJpYear.Text, this.txtSwkDpyDtJpYear.MaxLength))
			{
				this.txtSwkDpyDtJpYear.Focus();
				this.txtSwkDpyDtJpYear.SelectAll();
				return false;
			}
			if (!IsValid.IsMonth(this.txtSwkDpyDtMonth.Text, this.txtSwkDpyDtMonth.MaxLength))
			{
				this.txtSwkDpyDtMonth.Focus();
				this.txtSwkDpyDtMonth.SelectAll();
				return false;
			}
			if (!IsValid.IsDay(this.txtSwkDpyDtDay.Text, this.txtSwkDpyDtDay.MaxLength))
			{
				this.txtSwkDpyDtDay.Focus();
				this.txtSwkDpyDtDay.SelectAll();
				return false;
			}
			this.CheckJp();
			this.SetJp();
			if (!this.IsValidTekiyoCd())
			{
				this.txtTekiyoCd.Focus();
				return false;
			}
			if (!this.IsValidTekiyo())
			{
				this.txtTekiyo.Focus();
				return false;
			}
			DateTime dateTime2 = Util.ConvAdDate(this.lblSwkDpyDtGengo.Text, Util.ToInt(this.txtSwkDpyDtJpYear.Text), Util.ToInt(this.txtSwkDpyDtMonth.Text), Util.ToInt(this.txtSwkDpyDtDay.Text), base.Dba);
			if (dateTime2.CompareTo(Util.ToDate(base.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"])) < 0 || dateTime2.CompareTo(Util.ToDate(base.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"])) > 0)
			{
				string[] array = Util.ConvJpDate(Util.ToDate(base.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]), base.Dba);
				this.lblSwkDpyDtGengo.Text = array[0];
				this.txtSwkDpyDtJpYear.Text = array[2];
				this.txtSwkDpyDtMonth.Text = array[3];
				this.txtSwkDpyDtDay.Text = array[4];
			}
			return true;
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00009D29 File Offset: 0x00007F29
		private void SetJpFr()
		{
			this.SetJpFr(Util.FixJpDate(this.lblDpyDtFrGengo.Text, this.txtDpyDtFrJpYear.Text, this.txtDpyDtFrMonth.Text, this.txtDpyDtFrDay.Text, base.Dba));
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00009D68 File Offset: 0x00007F68
		private void SetJpTo()
		{
			this.SetJpTo(Util.FixJpDate(this.lblDpyDtToGengo.Text, this.txtDpyDtToJpYear.Text, this.txtDpyDtToMonth.Text, this.txtDpyDtToDay.Text, base.Dba));
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00009DA8 File Offset: 0x00007FA8
		private void SetJp()
		{
			this.SetJp(Util.ConvJpDate(this.FixNendoDate(Util.ConvAdDate(this.lblSwkDpyDtGengo.Text, this.txtSwkDpyDtJpYear.Text, this.txtSwkDpyDtMonth.Text, this.txtSwkDpyDtDay.Text, base.Dba)), base.Dba));
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00009E04 File Offset: 0x00008004
		private void CheckJpFr()
		{
			DateTime dateTime = Util.ConvAdDate(this.lblDpyDtFrGengo.Text, this.txtDpyDtFrJpYear.Text, this.txtDpyDtFrMonth.Text, "1", base.Dba);
			int num = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
			if (Util.ToInt(this.txtDpyDtFrDay.Text) > num)
			{
				this.txtDpyDtFrDay.Text = Util.ToString(num);
			}
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00009E80 File Offset: 0x00008080
		private void CheckJpTo()
		{
			DateTime dateTime = Util.ConvAdDate(this.lblDpyDtToGengo.Text, this.txtSwkDpyDtJpYear.Text, this.txtDpyDtToMonth.Text, "1", base.Dba);
			int num = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
			if (Util.ToInt(this.txtDpyDtToDay.Text) > num)
			{
				this.txtDpyDtToDay.Text = Util.ToString(num);
			}
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00009EFC File Offset: 0x000080FC
		private void CheckJp()
		{
			DateTime dateTime = Util.ConvAdDate(this.lblSwkDpyDtGengo.Text, this.txtSwkDpyDtJpYear.Text, this.txtSwkDpyDtMonth.Text, "1", base.Dba);
			int num = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
			if (Util.ToInt(this.txtSwkDpyDtDay.Text) > num)
			{
				this.txtSwkDpyDtDay.Text = Util.ToString(num);
			}
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00009F78 File Offset: 0x00008178
		private void SetJpFr(string[] arrJpDate)
		{
			this.lblDpyDtFrGengo.Text = arrJpDate[0];
			this.txtDpyDtFrJpYear.Text = arrJpDate[2];
			this.txtDpyDtFrMonth.Text = arrJpDate[3];
			this.txtDpyDtFrDay.Text = arrJpDate[4];
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00009FB2 File Offset: 0x000081B2
		private void SetJpTo(string[] arrJpDate)
		{
			this.lblDpyDtToGengo.Text = arrJpDate[0];
			this.txtDpyDtToJpYear.Text = arrJpDate[2];
			this.txtDpyDtToMonth.Text = arrJpDate[3];
			this.txtDpyDtToDay.Text = arrJpDate[4];
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00009FEC File Offset: 0x000081EC
		private void SetJp(string[] arrJpDate)
		{
			this.lblSwkDpyDtGengo.Text = arrJpDate[0];
			this.txtSwkDpyDtJpYear.Text = arrJpDate[2];
			this.txtSwkDpyDtMonth.Text = arrJpDate[3];
			this.txtSwkDpyDtDay.Text = arrJpDate[4];
		}

		// Token: 0x06000051 RID: 81 RVA: 0x0000A028 File Offset: 0x00008228
		private void GetZeiSetting()
		{
			bool flag = false;
			bool flag2 = false;
			DataTable vi_ZM_KANJO_KAMOKU = this.GetVI_ZM_KANJO_KAMOKU(Util.ToString(base.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));
			DataTable vi_ZM_KANJO_KAMOKU2 = this.GetVI_ZM_KANJO_KAMOKU(Util.ToString(base.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
			DataTable dataTable = vi_ZM_KANJO_KAMOKU.Clone();
			if (vi_ZM_KANJO_KAMOKU.Rows.Count > 0)
			{
				dataTable.ImportRow(vi_ZM_KANJO_KAMOKU.Rows[0]);
				flag = true;
			}
			if (vi_ZM_KANJO_KAMOKU2.Rows.Count > 0)
			{
				dataTable.ImportRow(vi_ZM_KANJO_KAMOKU2.Rows[0]);
				flag2 = true;
			}
			dataTable.Columns.Add("KBN", typeof(int));
			if (flag)
			{
				dataTable.Rows[0]["KBN"] = 1;
			}
			if (flag2)
			{
				if (flag)
				{
					dataTable.Rows[1]["KBN"] = 2;
				}
				else
				{
					dataTable.Rows[0]["KBN"] = 2;
				}
			}
			this._zeiSetting = dataTable;
		}

		// Token: 0x06000052 RID: 82 RVA: 0x0000A150 File Offset: 0x00008350
		private DataTable GetVI_ZM_KANJO_KAMOKU(string kanjoKmkCd)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  * ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("  VI_ZM_KANJO_KAMOKU ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    KAISHA_CD     = @KAISHA_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO  = @KAIKEI_NENDO ");
			stringBuilder.Append("AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			dbParamCollection.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKmkCd);
			return base.Dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000053 RID: 83 RVA: 0x0000A220 File Offset: 0x00008420
		private void GetTB_HN_ZIDO_SHIWAKE_SETTEI_A(DateTime DenpyoDay)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  A.SHIWAKE_CD            AS 仕訳コード ");
			stringBuilder.Append(" ,A.KANJO_KAMOKU_CD       AS 勘定科目コード ");
			stringBuilder.Append(" ,B.KANJO_KAMOKU_NM       AS 勘定科目名 ");
			stringBuilder.Append(" ,(CASE WHEN B.HOJO_KAMOKU_UMU = 0 THEN 0 ");
			stringBuilder.Append("        ELSE CASE WHEN A.HOJO_KAMOKU_CD = 0 THEN -1 ");
			stringBuilder.Append("                  ELSE A.HOJO_KAMOKU_CD ");
			stringBuilder.Append("             END ");
			stringBuilder.Append("   END) AS 補助科目コード ");
			stringBuilder.Append(" ,'                                        ' AS 補助科目名 ");
			stringBuilder.Append(" ,(CASE WHEN B.BUMON_UMU = 0 THEN 0 ");
			stringBuilder.Append("        ELSE CASE WHEN A.BUMON_CD = 0 THEN -1 ");
			stringBuilder.Append("                  ELSE A.BUMON_CD ");
			stringBuilder.Append("             END ");
			stringBuilder.Append("   END) AS 部門コード ");
			stringBuilder.Append(" ,B.HOJO_SHIYO_KUBUN      AS 補助使用区分 ");
			stringBuilder.Append(" ,B.TAISHAKU_KUBUN        AS 貸借区分 ");
			stringBuilder.Append(" ,A.ZEI_KUBUN             AS 税区分 ");
			stringBuilder.Append(" ,C.KAZEI_KUBUN           AS 課税区分 ");
			stringBuilder.Append(" ,C.TORIHIKI_KUBUN        AS 取引区分 ");
			stringBuilder.Append(",dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS 税率 ");
			stringBuilder.Append(" ,A.JIGYO_KUBUN           AS 事業区分 ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("    TB_HN_ZIDO_SHIWAKE_SETTEI_A AS A ");
			stringBuilder.Append("LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ");
			stringBuilder.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
			stringBuilder.Append("AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
			stringBuilder.Append("AND B.KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("LEFT OUTER JOIN TB_ZM_F_ZEI_KUBUN AS C ");
			stringBuilder.Append("ON A.ZEI_KUBUN = C.ZEI_KUBUN ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    A.KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND A.SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND A.DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND ISNULL(B.KANJO_KAMOKU_CD, 0) <> 0 ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);
			DataTable dataTableFromSqlWithParams = base.Dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
			DataTable dataTable = dataTableFromSqlWithParams.Clone();
			for (int i = 0; i < dataTableFromSqlWithParams.Rows.Count; i++)
			{
				DataRow dataRow = dataTable.NewRow();
				for (int j = 0; j < dataTableFromSqlWithParams.Columns.Count; j++)
				{
					if (dataTableFromSqlWithParams.Columns[j].ColumnName.Equals("補助科目名"))
					{
						DataTable tb_ZM_HOJO_KAMOKU = this.GetTB_ZM_HOJO_KAMOKU(Util.ToString(dataTableFromSqlWithParams.Rows[i]["勘定科目コード"]), Util.ToString(dataTableFromSqlWithParams.Rows[i]["補助科目コード"]));
						if (tb_ZM_HOJO_KAMOKU.Rows.Count > 0)
						{
							dataRow[j] = tb_ZM_HOJO_KAMOKU.Rows[0]["HOJO_KAMOKU_NM"];
						}
						else
						{
							dataRow[j] = null;
						}
					}
					else
					{
						dataRow[j] = dataTableFromSqlWithParams.Rows[i][j];
					}
				}
				dataTable.Rows.Add(dataRow);
			}
			this._jdSwkSettingA = dataTable;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x0000A579 File Offset: 0x00008779
		private void GetTB_HN_ZIDO_SHIWAKE_SETTEI_A()
		{
			this.GetTB_HN_ZIDO_SHIWAKE_SETTEI_A(DateTime.Today);
		}

		// Token: 0x06000055 RID: 85 RVA: 0x0000A588 File Offset: 0x00008788
		private void GetTB_HN_ZIDO_SHIWAKE_SETTEI_B(DateTime DenpyoDay)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  A.SHIWAKE_CD            AS 仕訳コード ");
			stringBuilder.Append(" ,A.KANJO_KAMOKU_CD       AS 勘定科目コード ");
			stringBuilder.Append(" ,B.KANJO_KAMOKU_NM       AS 勘定科目名 ");
			stringBuilder.Append(" ,(CASE WHEN B.HOJO_KAMOKU_UMU = 0 THEN 0 ");
			stringBuilder.Append("        ELSE CASE WHEN A.HOJO_KAMOKU_CD = 0 THEN -1 ");
			stringBuilder.Append("                  ELSE A.HOJO_KAMOKU_CD ");
			stringBuilder.Append("             END ");
			stringBuilder.Append("   END) AS 補助科目コード ");
			stringBuilder.Append(" ,'                                        ' AS 補助科目名 ");
			stringBuilder.Append(" ,(CASE WHEN B.BUMON_UMU = 0 THEN 0 ");
			stringBuilder.Append("        ELSE CASE WHEN A.BUMON_CD = 0 THEN -1 ");
			stringBuilder.Append("                  ELSE A.BUMON_CD ");
			stringBuilder.Append("             END ");
			stringBuilder.Append("   END) AS 部門コード ");
			stringBuilder.Append(" ,B.HOJO_SHIYO_KUBUN      AS 補助使用区分 ");
			stringBuilder.Append(" ,A.TAISHAKU_KUBUN        AS 貸借区分 ");
			stringBuilder.Append(" ,A.ZEI_KUBUN             AS 税区分 ");
			stringBuilder.Append(" ,C.KAZEI_KUBUN           AS 課税区分 ");
			stringBuilder.Append(" ,C.TORIHIKI_KUBUN        AS 取引区分 ");
			stringBuilder.Append(" ,dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS 税率 ");
			stringBuilder.Append(" ,A.JIGYO_KUBUN           AS 事業区分 ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("    TB_HN_ZIDO_SHIWAKE_SETTEI_B  AS A ");
			stringBuilder.Append("LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU  AS B ");
			stringBuilder.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
			stringBuilder.Append("AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
			stringBuilder.Append("AND B.KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("LEFT OUTER JOIN TB_ZM_F_ZEI_KUBUN AS C ");
			stringBuilder.Append("ON A.ZEI_KUBUN = C.ZEI_KUBUN ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    A.KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND A.SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND A.DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND ISNULL(B.KANJO_KAMOKU_CD, 0) <> 0 ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);
			DataTable dataTableFromSqlWithParams = base.Dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
			DataTable dataTable = dataTableFromSqlWithParams.Clone();
			for (int i = 0; i < dataTableFromSqlWithParams.Rows.Count; i++)
			{
				DataRow dataRow = dataTable.NewRow();
				for (int j = 0; j < dataTableFromSqlWithParams.Columns.Count; j++)
				{
					if (dataTableFromSqlWithParams.Columns[j].ColumnName.Equals("補助科目名"))
					{
						DataTable tb_ZM_HOJO_KAMOKU = this.GetTB_ZM_HOJO_KAMOKU(Util.ToString(dataTableFromSqlWithParams.Rows[i]["勘定科目コード"]), Util.ToString(dataTableFromSqlWithParams.Rows[i]["補助科目コード"]));
						if (tb_ZM_HOJO_KAMOKU.Rows.Count > 0)
						{
							dataRow[j] = tb_ZM_HOJO_KAMOKU.Rows[0]["HOJO_KAMOKU_NM"];
						}
						else
						{
							dataRow[j] = null;
						}
					}
					else
					{
						dataRow[j] = dataTableFromSqlWithParams.Rows[i][j];
					}
				}
				dataTable.Rows.Add(dataRow);
			}
			this._jdSwkSettingB = dataTable;
		}

		// Token: 0x06000056 RID: 86 RVA: 0x0000A8E1 File Offset: 0x00008AE1
		private void GetTB_HN_ZIDO_SHIWAKE_SETTEI_B()
		{
			this.GetTB_HN_ZIDO_SHIWAKE_SETTEI_B(DateTime.Today);
		}

		// Token: 0x06000057 RID: 87 RVA: 0x0000A8F0 File Offset: 0x00008AF0
		private DataTable GetTB_ZM_HOJO_KAMOKU(string kanjoKmkCd, string hojoKmkCd)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  HOJO_KAMOKU_NM ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("    TB_ZM_HOJO_KAMOKU ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD ");
			stringBuilder.Append("AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(this.txtMizuageShishoCd.Text)));
			dbParamCollection.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKmkCd);
			dbParamCollection.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, hojoKmkCd);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			return base.Dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000058 RID: 88 RVA: 0x0000AA0C File Offset: 0x00008C0C
		private void ClearForm()
		{
			this._mode = 1;
			this._packDpyNo = 0;
			this.lblUpdMode.Visible = false;
			this.btnF6.Enabled = true;
			this.btnF3.Enabled = false;
			this.txtShimebi.Text = string.Empty;
			string[] array = Util.ConvJpDate(DateTime.Now, base.Dba);
			this.lblDpyDtFrGengo.Text = array[0];
			this.txtDpyDtFrJpYear.Text = array[2];
			this.txtDpyDtFrMonth.Text = array[3];
			this.txtDpyDtFrDay.Text = array[4];
			this.lblDpyDtToGengo.Text = array[0];
			this.txtDpyDtToJpYear.Text = array[2];
			this.txtDpyDtToMonth.Text = array[3];
			this.txtDpyDtToDay.Text = array[4];
			this.lblSwkDpyDtGengo.Text = array[0];
			this.txtSwkDpyDtJpYear.Text = array[2];
			this.txtSwkDpyDtMonth.Text = array[3];
			this.txtSwkDpyDtDay.Text = array[4];
			this.txtFunanushiCdFr.Text = string.Empty;
			this.lblFunanushiNmFr.Text = "先\u3000頭";
			this.txtFunanushiCdTo.Text = string.Empty;
			this.lblFunanushiNmTo.Text = "最\u3000後";
			this.txtTantoCd.Text = base.UInfo.UserCd;
			string name = base.Dba.GetName(base.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoCd.Text);
			if (ValChk.IsEmpty(name))
			{
				Msg.Error("入力に誤りがあります。");
			}
			this.lblTantoNm.Text = name;
			this.txtTekiyoCd.Text = base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "TekiyoCd");
			this.txtTekiyo.Text = base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "Tekiyo");
		}

		// Token: 0x06000059 RID: 89 RVA: 0x0000AC04 File Offset: 0x00008E04
		private void SetUpdateItems(string[] outData)
		{
			this._packDpyNo = Util.ToInt(outData[0]);
			string[] array = Util.ConvJpDate(Util.ToDate(outData[1]), base.Dba);
			this.lblSwkDpyDtGengo.Text = array[0];
			this.txtSwkDpyDtJpYear.Text = array[2];
			this.txtSwkDpyDtMonth.Text = array[3];
			this.txtSwkDpyDtDay.Text = array[4];
			string a = outData[2];
			if (!(a == "1"))
			{
				if (!(a == "2"))
				{
					if (a == "3")
					{
						this.rdbShimebi.Checked = true;
					}
				}
				else
				{
					this.rdbGenkinKake.Checked = true;
				}
			}
			else
			{
				this.rdbGenkin.Checked = true;
			}
			this.txtShimebi.Text = outData[3];
			array = Util.ConvJpDate(Util.ToDate(outData[4]), base.Dba);
			this.lblDpyDtFrGengo.Text = array[0];
			this.txtDpyDtFrJpYear.Text = array[2];
			this.txtDpyDtFrMonth.Text = array[3];
			this.txtDpyDtFrDay.Text = array[4];
			array = Util.ConvJpDate(Util.ToDate(outData[5]), base.Dba);
			this.lblDpyDtToGengo.Text = array[0];
			this.txtDpyDtToJpYear.Text = array[2];
			this.txtDpyDtToMonth.Text = array[3];
			this.txtDpyDtToDay.Text = array[4];
			this.txtFunanushiCdFr.Text = outData[6];
			if (ValChk.IsEmpty(outData[6]))
			{
				this.lblFunanushiNmFr.Text = "先\u3000頭";
			}
			else
			{
				this.lblFunanushiNmFr.Text = base.Dba.GetName(base.UInfo, "VI_HN_TORIHIKISAKI_JOHO", this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text, 1m);
			}
			this.txtFunanushiCdTo.Text = outData[7];
			if (ValChk.IsEmpty(outData[7]))
			{
				this.lblFunanushiNmTo.Text = "最\u3000後";
			}
			else
			{
				this.lblFunanushiNmTo.Text = base.Dba.GetName(base.UInfo, "VI_HN_TORIHIKISAKI_JOHO", this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text, 1m);
			}
			this.txtTekiyoCd.Text = outData[8];
			this.txtTekiyo.Text = outData[9];
			this.txtTantoCd.Text = outData[10];
			this.lblTantoNm.Text = outData[11];
		}

		// Token: 0x0600005A RID: 90 RVA: 0x0000AE70 File Offset: 0x00009070
		private bool IsValidShimebi()
		{
			if (ValChk.IsEmpty(this.txtShimebi.Text))
			{
				return true;
			}
			if (!ValChk.IsNumber(this.txtShimebi.Text))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			int num = Util.ToInt(this.txtShimebi.Text);
			if ((num < 1 || num > 28) && num != 99)
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			return true;
		}

		// Token: 0x0600005B RID: 91 RVA: 0x0000AEE0 File Offset: 0x000090E0
		private void ControlItemsBySettings()
		{
			if (Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenGenToriDp")) == 1 || Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenGenHnDp")) == 1)
			{
				this.rdbGenkin.Enabled = true;
			}
			else
			{
				this.rdbGenkin.Enabled = false;
				this.rdbGenkin.Checked = false;
			}
			if (Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeKakeToriDp")) == 1 || Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeKakeHnDp")) == 1 || Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeGenToriDp")) == 1 || Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeGenHnDp")) == 1)
			{
				this.rdbGenkinKake.Enabled = true;
			}
			else
			{
				this.rdbGenkinKake.Enabled = false;
				this.rdbGenkinKake.Checked = false;
			}
			if (Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbKakeToriDp")) == 1 || Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbKakeHnDp")) == 1 || Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbGenToriDp")) == 1 || Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbGenHnDp")) == 1)
			{
				this.rdbShimebi.Enabled = true;
				return;
			}
			this.rdbShimebi.Enabled = false;
			this.rdbShimebi.Checked = false;
		}

		// Token: 0x0600005C RID: 92 RVA: 0x0000B0BC File Offset: 0x000092BC
		private Hashtable GetCondition()
		{
			Hashtable hashtable = new Hashtable();
			hashtable["ShishoCode"] = Util.ToString(this.txtMizuageShishoCd.Text);
			KBDB1011.SKbn skbn;
			if (this.rdbGenkin.Checked)
			{
				skbn = KBDB1011.SKbn.Genkin;
			}
			else if (this.rdbGenkinKake.Checked)
			{
				skbn = KBDB1011.SKbn.GenkinKake;
			}
			else if (this.rdbShimebi.Checked)
			{
				skbn = KBDB1011.SKbn.Shimebi;
			}
			else
			{
				skbn = KBDB1011.SKbn.None;
			}
			hashtable["SakuseiKbn"] = skbn;
			if (skbn == KBDB1011.SKbn.Shimebi)
			{
				hashtable["Shimebi"] = Util.ToInt(this.txtShimebi.Text);
			}
			else
			{
				hashtable["Shimebi"] = null;
			}
			hashtable["DpyDtFr"] = Util.ConvAdDate(this.lblDpyDtFrGengo.Text, Util.ToInt(this.txtDpyDtFrJpYear.Text), Util.ToInt(this.txtDpyDtFrMonth.Text), Util.ToInt(this.txtDpyDtFrDay.Text), base.Dba);
			hashtable["DpyDtTo"] = Util.ConvAdDate(this.lblDpyDtToGengo.Text, Util.ToInt(this.txtDpyDtToJpYear.Text), Util.ToInt(this.txtDpyDtToMonth.Text), Util.ToInt(this.txtDpyDtToDay.Text), base.Dba);
			if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
			{
				hashtable["FunanushiCdFr"] = "0";
			}
			else
			{
				hashtable["FunanushiCdFr"] = this.txtFunanushiCdFr.Text;
			}
			if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
			{
				hashtable["FunanushiCdTo"] = "9999";
			}
			else
			{
				hashtable["FunanushiCdTo"] = this.txtFunanushiCdTo.Text;
			}
			hashtable["TantoCd"] = this.txtTantoCd.Text;
			hashtable["SwkDpyDt"] = Util.ConvAdDate(this.lblSwkDpyDtGengo.Text, Util.ToInt(this.txtSwkDpyDtJpYear.Text), Util.ToInt(this.txtSwkDpyDtMonth.Text), Util.ToInt(this.txtSwkDpyDtDay.Text), base.Dba);
			hashtable["TekiyoCd"] = this.txtTekiyoCd.Text;
			hashtable["Tekiyo"] = this.txtTekiyo.Text;
			return hashtable;
		}

		// Token: 0x0600005D RID: 93 RVA: 0x0000B31C File Offset: 0x0000951C
		private DataRow GetPersonInfo(string code)
		{
			DataRow result = null;
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
			DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "TB_CM_TANTOSHA", "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ", dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count != 0)
			{
				result = dataTableByConditionWithParams.Rows[0];
			}
			return result;
		}

		// Token: 0x0600005E RID: 94 RVA: 0x0000B39C File Offset: 0x0000959C
		private DateTime FixNendoDate(DateTime date)
		{
			DateTime dateTime = Util.ToDate(base.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
			DateTime dateTime2 = Util.ToDate(base.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
			if (date < dateTime)
			{
				return dateTime;
			}
			if (date > dateTime2)
			{
				return dateTime2;
			}
			return date;
		}

		// Token: 0x0600005F RID: 95 RVA: 0x0000B3F8 File Offset: 0x000095F8
		private bool IsZeiSetting()
		{
			DataTable vi_ZM_KANJO_KAMOKU = this.GetVI_ZM_KANJO_KAMOKU(Util.ToString(base.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));
			DataTable vi_ZM_KANJO_KAMOKU2 = this.GetVI_ZM_KANJO_KAMOKU(Util.ToString(base.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
			if (vi_ZM_KANJO_KAMOKU.Rows.Count == 0 || vi_ZM_KANJO_KAMOKU2.Rows.Count == 0)
			{
				Msg.Error(((vi_ZM_KANJO_KAMOKU.Rows.Count == 0) ? "仮受" : "仮払") + "消費税情報が読込みできません。");
				return false;
			}
			return true;
		}

		// Token: 0x06000060 RID: 96 RVA: 0x0000B490 File Offset: 0x00009690
		private bool UpdateData()
		{
			if (!this.ValidateAll())
			{
				return false;
			}
			this.GetTB_HN_ZIDO_SHIWAKE_SETTEI_A(Util.ToDate(this.Condition["SwkDpyDt"]));
			this.GetTB_HN_ZIDO_SHIWAKE_SETTEI_B(Util.ToDate(this.Condition["SwkDpyDt"]));
			if (Msg.ConfYesNo(((this._mode == 2) ? "更新" : "登録") + "しますか？") == DialogResult.No)
			{
				return false;
			}
			KBDB1016 kbdb = new KBDB1016();
			kbdb.Show();
			kbdb.Refresh();
			try
			{
				base.Dba.BeginTransaction();
				KBDB1011DA kbdb1011DA = new KBDB1011DA(base.UInfo, base.Dba, base.Config);
				this._dtSwkTgtData = kbdb1011DA.GetSwkTgtData(this._mode, this.Condition);
				if (this._dtSwkTgtData.Rows.Count == 0)
				{
					Msg.Info("該当データがありません。");
					kbdb.Close();
					return false;
				}
				this._dsTaishakuData = kbdb1011DA.GetTaishakuData(this.Condition, this._zeiSetting, this._jdSwkSettingA, this._jdSwkSettingB, this._dtSwkTgtData);
				bool flag = kbdb1011DA.MakeSwkData(this._mode, this._packDpyNo, this.Condition, this._dsTaishakuData);
				kbdb.Close();
				if (flag)
				{
					base.Dba.Commit();
					this.InitForm();
					return true;
				}
				base.Dba.Rollback();
				Msg.Error("更新に失敗しました。" + Environment.NewLine + "もう一度やり直して下さい。");
			}
			finally
			{
				base.Dba.Rollback();
			}
			return false;
		}

		// Token: 0x04000007 RID: 7
		private int _mode;

		// Token: 0x04000008 RID: 8
		private int _packDpyNo;

		// Token: 0x04000009 RID: 9
		private DataTable _zeiSetting;

		// Token: 0x0400000A RID: 10
		private DataTable _jdSwkSettingA;

		// Token: 0x0400000B RID: 11
		private DataTable _jdSwkSettingB;

		// Token: 0x0400000C RID: 12
		private DataTable _dtSwkTgtData = new DataTable();

		// Token: 0x0400000D RID: 13
		private DataSet _dsTaishakuData = new DataSet();

		// Token: 0x0400000E RID: 14
		private bool _dtFlg;

		// Token: 0x0200000B RID: 11
		public enum SKbn
		{
			// Token: 0x04000086 RID: 134
			Genkin = 1,
			// Token: 0x04000087 RID: 135
			GenkinKake,
			// Token: 0x04000088 RID: 136
			Shimebi,
			// Token: 0x04000089 RID: 137
			None = 0
		}
	}
}
