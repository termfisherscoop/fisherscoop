﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.controls;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000008 RID: 8
	public partial class KBDB1015 : BasePgForm
	{
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000077 RID: 119 RVA: 0x00010C29 File Offset: 0x0000EE29
		public bool Flg
		{
			get
			{
				return this._dtFlg;
			}
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00010C31 File Offset: 0x0000EE31
		public KBDB1015()
		{
			this.InitializeComponent();
			base.BindGotFocusEvent();
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00010C48 File Offset: 0x0000EE48
		protected override void InitForm()
		{
			this.ShishoCode = Util.ToInt(base.UInfo.ShishoCd);
			this.InitDisp();
			if (Util.ToInt(this.GetTB_HN_ZIDO_SHIWAKE_SETTEI_A().Rows[0]["件数"]) > 0)
			{
				DataTable vi_ZM_BUMON = this.GetVI_ZM_BUMON();
				if (vi_ZM_BUMON.Rows.Count > 0)
				{
					this.txtBumonCd.Text = Util.ToString(vi_ZM_BUMON.Rows[0]["BUMON_CD"]);
					this.lblBumonNm.Text = Util.ToString(vi_ZM_BUMON.Rows[0]["BUMON_NM"]);
				}
			}
			this.chkGenKakeToriDp.Enabled = false;
			this.chkGenKakeHnDp.Enabled = false;
			this.chkGenGenToriDp.Focus();
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00010D1C File Offset: 0x0000EF1C
		protected override void OnMoveFocus()
		{
			string activeCtlNm = base.ActiveCtlNm;
			if (activeCtlNm == "txtBumonCd" || activeCtlNm == "txtTekiyoCd")
			{
				this.btnF1.Enabled = true;
				return;
			}
			this.btnF1.Enabled = false;
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00010D64 File Offset: 0x0000EF64
		public override void PressF1()
		{
			string activeCtlNm = base.ActiveCtlNm;
			if (!(activeCtlNm == "txtBumonCd"))
			{
				if (!(activeCtlNm == "txtTekiyoCd"))
				{
					return;
				}
				Type type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2061.exe").GetType("jp.co.fsi.cm.cmcm2061.CMCM2061");
				if (type != null)
				{
					object obj = Activator.CreateInstance(type);
					if (obj != null)
					{
						BasePgForm basePgForm = (BasePgForm)obj;
						basePgForm.Par1 = "1";
						basePgForm.InData = Util.ToString(this.ShishoCode);
						basePgForm.ShowDialog(this);
						if (basePgForm.DialogResult == DialogResult.OK)
						{
							string[] array = (string[])basePgForm.OutData;
							this.txtTekiyoCd.Text = array[0];
							this.txtTekiyo.Text = array[1];
						}
					}
				}
			}
			else
			{
				Type type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe").GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
				if (type != null)
				{
					object obj2 = Activator.CreateInstance(type);
					if (obj2 != null)
					{
						BasePgForm basePgForm2 = (BasePgForm)obj2;
						basePgForm2.Par1 = "TB_CM_BUMON";
						basePgForm2.InData = this.txtBumonCd.Text;
						basePgForm2.ShowDialog(this);
						if (basePgForm2.DialogResult == DialogResult.OK)
						{
							string[] array2 = (string[])basePgForm2.OutData;
							this.txtBumonCd.Text = array2[0];
							this.lblBumonNm.Text = array2[1];
							this.txtTekiyoCd.Focus();
							return;
						}
					}
				}
			}
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00010EC8 File Offset: 0x0000F0C8
		public override void PressF6()
		{
			if (!this.ValidateAll())
			{
				return;
			}
			if (Msg.ConfYesNo("更新しますか？") == DialogResult.No)
			{
				return;
			}
			this.SaveSettings();
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00010EF4 File Offset: 0x0000F0F4
		private void txtBumonCd_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidBumonCd())
			{
				e.Cancel = true;
				this.txtBumonCd.SelectAll();
			}
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00010F10 File Offset: 0x0000F110
		private void txtTekiyoCd_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidTekiyoCd())
			{
				e.Cancel = true;
				this.txtTekiyoCd.SelectAll();
			}
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00010F2C File Offset: 0x0000F12C
		private void txtTekiyo_Validating(object sender, CancelEventArgs e)
		{
			if (!this.IsValidTekiyo())
			{
				e.Cancel = true;
				this.txtTekiyo.SelectAll();
				this._dtFlg = false;
				return;
			}
			this._dtFlg = true;
		}

		// Token: 0x06000080 RID: 128 RVA: 0x00010F58 File Offset: 0x0000F158
		private void txtTekiyo_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return && this.Flg)
			{
				this._dtFlg = false;
				if (!this.ValidateAll())
				{
					return;
				}
				if (Msg.ConfYesNo("更新しますか？") == DialogResult.No)
				{
					base.ActiveControl = this.txtTekiyo;
					return;
				}
				this.SaveSettings();
				base.DialogResult = DialogResult.OK;
				base.Close();
			}
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00010FB4 File Offset: 0x0000F1B4
		private void InitDisp()
		{
			this.chkGenGenToriDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenGenToriDp"));
			this.chkGenGenHnDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenGenHnDp"));
			this.chkGenKakeKakeToriDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeKakeToriDp"));
			this.chkGenKakeKakeHnDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeKakeHnDp"));
			this.chkGenKakeGenToriDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeGenToriDp"));
			this.chkGenKakeGenHnDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeGenHnDp"));
			this.chkSmbKakeToriDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbKakeToriDp"));
			this.chkSmbKakeHnDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbKakeHnDp"));
			this.chkSmbGenToriDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbGenToriDp"));
			this.chkSmbGenHnDp.Checked = "1".Equals(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbGenHnDp"));
			switch (Util.ToInt(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SwkDpMk")))
			{
			case 1:
				this.rdbFukugoDp.Checked = true;
				break;
			case 2:
				this.rdbTanitsuDp.Checked = true;
				break;
			case 3:
				this.rdbSeikyuDp.Checked = true;
				break;
			}
			this.txtTekiyoCd.Text = base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "TekiyoCd");
			this.txtTekiyo.Text = base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "Tekiyo");
			this.txtBumonCd.Text = base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "BumonCd");
			string text = "";
			try
			{
				text = base.Dba.GetName(base.UInfo, "TB_CM_BUMON", this.ShishoCode.ToString(), this.txtBumonCd.Text);
			}
			catch (Exception)
			{
			}
			this.lblBumonNm.Text = text;
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00011304 File Offset: 0x0000F504
		private DataTable GetTB_HN_ZIDO_SHIWAKE_SETTEI_A()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  COUNT(*) AS 件数 ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("    TB_HN_ZIDO_SHIWAKE_SETTEI_A AS A ");
			stringBuilder.Append("LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ");
			stringBuilder.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
			stringBuilder.Append("AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
			stringBuilder.Append("AND B.KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    A.KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND A.SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND A.DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND B.BUMON_UMU = 1 ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, base.UInfo.KaikeiNendo);
			return base.Dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00011418 File Offset: 0x0000F618
		private DataTable GetVI_ZM_BUMON()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  * ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("    VI_ZM_BUMON ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND BUMON_CD = @BUMON_CD ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			int num = Util.ToInt(Util.ToString(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "BumonCd")));
			dbParamCollection.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, num);
			return base.Dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000084 RID: 132 RVA: 0x000114E8 File Offset: 0x0000F6E8
		private bool ValidateAll()
		{
			if (!this.IsValidBumonCd())
			{
				this.txtBumonCd.SelectAll();
				this.txtBumonCd.Focus();
				return false;
			}
			if (!this.IsValidTekiyoCd())
			{
				this.txtTekiyoCd.SelectAll();
				this.txtTekiyoCd.Focus();
				return false;
			}
			if (!this.IsValidTekiyo())
			{
				this.txtTekiyo.SelectAll();
				this.txtTekiyo.Focus();
				return false;
			}
			return true;
		}

		// Token: 0x06000085 RID: 133 RVA: 0x0001155C File Offset: 0x0000F75C
		private bool IsValidBumonCd()
		{
			if (ValChk.IsEmpty(this.txtBumonCd.Text) || this.txtBumonCd.Text == "0")
			{
				this.lblBumonNm.Text = "";
				return true;
			}
			if (!ValChk.IsNumber(this.txtBumonCd.Text))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			string name = base.Dba.GetName(base.UInfo, "TB_CM_BUMON", this.ShishoCode.ToString(), this.txtBumonCd.Text);
			if (ValChk.IsEmpty(name))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			this.lblBumonNm.Text = name;
			return true;
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00011614 File Offset: 0x0000F814
		private bool IsValidTekiyoCd()
		{
			if (ValChk.IsEmpty(this.txtTekiyoCd.Text))
			{
				return true;
			}
			if (!ValChk.IsNumber(this.txtTekiyoCd.Text))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			string name = base.Dba.GetName(base.UInfo, "TB_HN_TEKIYO", this.ShishoCode.ToString(), this.txtTekiyoCd.Text);
			if (ValChk.IsEmpty(name))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			this.txtTekiyo.Text = name;
			return true;
		}

		// Token: 0x06000087 RID: 135 RVA: 0x000116A3 File Offset: 0x0000F8A3
		private bool IsValidTekiyo()
		{
			if (!ValChk.IsWithinLength(this.txtTekiyo.Text, this.txtTekiyo.MaxLength))
			{
				Msg.Error("入力に誤りがあります。");
				return false;
			}
			return true;
		}

		// Token: 0x06000088 RID: 136 RVA: 0x000116D0 File Offset: 0x0000F8D0
		private void SaveSettings()
		{
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenGenToriDp", this.chkGenGenToriDp.Checked ? "1" : "0");
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenGenHnDp", this.chkGenGenHnDp.Checked ? "1" : "0");
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeKakeToriDp", this.chkGenKakeKakeToriDp.Checked ? "1" : "0");
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeKakeHnDp", this.chkGenKakeKakeHnDp.Checked ? "1" : "0");
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeGenToriDp", this.chkGenKakeGenToriDp.Checked ? "1" : "0");
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeGenHnDp", this.chkGenKakeGenHnDp.Checked ? "1" : "0");
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbKakeToriDp", this.chkSmbKakeToriDp.Checked ? "1" : "0");
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbKakeHnDp", this.chkSmbKakeHnDp.Checked ? "1" : "0");
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbGenToriDp", this.chkSmbGenToriDp.Checked ? "1" : "0");
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbGenHnDp", this.chkSmbGenHnDp.Checked ? "1" : "0");
			string value = "";
			if (this.rdbFukugoDp.Checked)
			{
				value = "1";
			}
			else if (this.rdbTanitsuDp.Checked)
			{
				value = "2";
			}
			else if (this.rdbSeikyuDp.Checked)
			{
				value = "3";
			}
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SwkDpMk", value);
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "BumonCd", this.txtBumonCd.Text);
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "TekiyoCd", this.txtTekiyoCd.Text);
			base.Config.SetPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "Tekiyo", this.txtTekiyo.Text);
			base.Config.SaveConfig();
		}

		// Token: 0x04000055 RID: 85
		private int ShishoCode;

		// Token: 0x04000056 RID: 86
		private bool _dtFlg;
	}
}
