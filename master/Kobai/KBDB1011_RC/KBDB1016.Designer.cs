﻿namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000003 RID: 3
	public partial class KBDB1016 : global::System.Windows.Forms.Form
	{
		// Token: 0x06000019 RID: 25 RVA: 0x00007FD0 File Offset: 0x000061D0
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00007FF0 File Offset: 0x000061F0
		private void InitializeComponent()
		{
			this.lblNotice = new global::System.Windows.Forms.Label();
			base.SuspendLayout();
			this.lblNotice.AutoSize = true;
			this.lblNotice.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 15.75f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblNotice.Location = new global::System.Drawing.Point(40, 10);
			this.lblNotice.Name = "lblNotice";
			this.lblNotice.Size = new global::System.Drawing.Size(207, 21);
			this.lblNotice.TabIndex = 0;
			this.lblNotice.Text = "集計しています...";
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(280, 40);
			base.Controls.Add(this.lblNotice);
			base.FormBorderStyle = global::System.Windows.Forms.FormBorderStyle.None;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "KBDB1016";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = global::System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "KBDB1016";
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x04000005 RID: 5
		private global::System.ComponentModel.IContainer components = null;

		// Token: 0x04000006 RID: 6
		private global::System.Windows.Forms.Label lblNotice;
	}
}
