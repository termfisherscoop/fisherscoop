﻿namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000007 RID: 7
	public partial class KBDB1013 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x06000075 RID: 117 RVA: 0x00010304 File Offset: 0x0000E504
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00010324 File Offset: 0x0000E524
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblTerm = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblGenkinUriage = new System.Windows.Forms.Label();
            this.lblGenkinZei = new System.Windows.Forms.Label();
            this.lblKakeUriage = new System.Windows.Forms.Label();
            this.lblKakeZei = new System.Windows.Forms.Label();
            this.lblKeiUriage = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(4, 65);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(89, 65);
            this.btnF1.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(175, 65);
            this.btnF6.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.lblGenkinZei);
            this.pnlDebug.Controls.Add(this.lblGenkinUriage);
            this.pnlDebug.Location = new System.Drawing.Point(14, 428);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(822, 133);
            this.pnlDebug.Controls.SetChildIndex(this.lblGenkinUriage, 0);
            this.pnlDebug.Controls.SetChildIndex(this.lblGenkinZei, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(840, 31);
            this.lblTitle.Text = "仕訳データ参照";
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.EnableHeadersVisualStyles = false;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(17, 39);
            this.dgvList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(807, 421);
            this.dgvList.TabIndex = 22;
            // 
            // lblTerm
            // 
            this.lblTerm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTerm.ForeColor = System.Drawing.Color.Blue;
            this.lblTerm.Location = new System.Drawing.Point(19, 17);
            this.lblTerm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTerm.Name = "lblTerm";
            this.lblTerm.Size = new System.Drawing.Size(320, 16);
            this.lblTerm.TabIndex = 903;
            this.lblTerm.Text = " 平成 21年 4月 1日 ～ 平成 22年 3月31日";
            this.lblTerm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(17, 459);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 26);
            this.label1.TabIndex = 904;
            this.label1.Text = "[ 合 計 ] ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGenkinUriage
            // 
            this.lblGenkinUriage.BackColor = System.Drawing.Color.White;
            this.lblGenkinUriage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGenkinUriage.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGenkinUriage.ForeColor = System.Drawing.Color.Black;
            this.lblGenkinUriage.Location = new System.Drawing.Point(192, 31);
            this.lblGenkinUriage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGenkinUriage.Name = "lblGenkinUriage";
            this.lblGenkinUriage.Size = new System.Drawing.Size(120, 26);
            this.lblGenkinUriage.TabIndex = 905;
            this.lblGenkinUriage.Text = "-999,999,999";
            this.lblGenkinUriage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGenkinZei
            // 
            this.lblGenkinZei.BackColor = System.Drawing.Color.White;
            this.lblGenkinZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGenkinZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGenkinZei.ForeColor = System.Drawing.Color.Black;
            this.lblGenkinZei.Location = new System.Drawing.Point(312, 31);
            this.lblGenkinZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGenkinZei.Name = "lblGenkinZei";
            this.lblGenkinZei.Size = new System.Drawing.Size(120, 26);
            this.lblGenkinZei.TabIndex = 906;
            this.lblGenkinZei.Text = "-999,999,999";
            this.lblGenkinZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKakeUriage
            // 
            this.lblKakeUriage.BackColor = System.Drawing.Color.White;
            this.lblKakeUriage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKakeUriage.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKakeUriage.ForeColor = System.Drawing.Color.Black;
            this.lblKakeUriage.Location = new System.Drawing.Point(446, 459);
            this.lblKakeUriage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKakeUriage.Name = "lblKakeUriage";
            this.lblKakeUriage.Size = new System.Drawing.Size(120, 26);
            this.lblKakeUriage.TabIndex = 907;
            this.lblKakeUriage.Text = "-999,999,999";
            this.lblKakeUriage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKakeZei
            // 
            this.lblKakeZei.BackColor = System.Drawing.Color.White;
            this.lblKakeZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKakeZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKakeZei.ForeColor = System.Drawing.Color.Black;
            this.lblKakeZei.Location = new System.Drawing.Point(566, 459);
            this.lblKakeZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKakeZei.Name = "lblKakeZei";
            this.lblKakeZei.Size = new System.Drawing.Size(120, 26);
            this.lblKakeZei.TabIndex = 908;
            this.lblKakeZei.Text = "-999,999,999";
            this.lblKakeZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKeiUriage
            // 
            this.lblKeiUriage.BackColor = System.Drawing.Color.White;
            this.lblKeiUriage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKeiUriage.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKeiUriage.ForeColor = System.Drawing.Color.Black;
            this.lblKeiUriage.Location = new System.Drawing.Point(686, 459);
            this.lblKeiUriage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKeiUriage.Name = "lblKeiUriage";
            this.lblKeiUriage.Size = new System.Drawing.Size(138, 26);
            this.lblKeiUriage.TabIndex = 909;
            this.lblKeiUriage.Text = "-999,999,999";
            this.lblKeiUriage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // KBDB1013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 565);
            this.Controls.Add(this.lblKeiUriage);
            this.Controls.Add(this.lblKakeZei);
            this.Controls.Add(this.lblKakeUriage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTerm);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBDB1013";
            this.ShowFButton = true;
            this.ShowTitle = false;
            this.Text = "仕訳データ参照";
            this.Shown += new System.EventHandler(this.KBDB1013_Shown);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblTerm, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblKakeUriage, 0);
            this.Controls.SetChildIndex(this.lblKakeZei, 0);
            this.Controls.SetChildIndex(this.lblKeiUriage, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);

		}

		// Token: 0x0400004C RID: 76
		private global::System.ComponentModel.IContainer components = null;

		// Token: 0x0400004D RID: 77
		private global::System.Windows.Forms.DataGridView dgvList;

		// Token: 0x0400004E RID: 78
		private global::System.Windows.Forms.Label lblTerm;

		// Token: 0x0400004F RID: 79
		private global::System.Windows.Forms.Label label1;

		// Token: 0x04000050 RID: 80
		private global::System.Windows.Forms.Label lblGenkinUriage;

		// Token: 0x04000051 RID: 81
		private global::System.Windows.Forms.Label lblGenkinZei;

		// Token: 0x04000052 RID: 82
		private global::System.Windows.Forms.Label lblKakeUriage;

		// Token: 0x04000053 RID: 83
		private global::System.Windows.Forms.Label lblKakeZei;

		// Token: 0x04000054 RID: 84
		private global::System.Windows.Forms.Label lblKeiUriage;
	}
}
