﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1031
{
    /// <summary>
    /// 商品照会一覧表(KBMR1031)
    /// </summary>
    public partial class KBMR1031 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols = 21;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBMR1031()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 開始
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = "1";
            // 終了
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年と商品コードに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtShohinCd":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。
            Assembly asm;
            Type t;
            String[] result;

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinCd":
                    #region 商品CD
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtShohinCd")
                                {
                                    this.txtShohinCd.Text = outData[0];
                                    this.lblShohinNm.Text = outData[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearFr":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearTo":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBMR1031R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 商品コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCd())
            {
                e.Cancel = true;
                this.txtShohinCd.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 商品CDのEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtShohinCd.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 商品コードの入力チェック
        /// </summary>
        /// 
        private bool IsValidShohinCd()
        {
            if (ValChk.IsEmpty(this.txtShohinCd.Text))
            {
                this.lblShohinNm.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinCd.Text))
            {
                Msg.Error("商品コードは数値のみで入力してください。");
                return false;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN", this.txtMizuageShishoCd.Text, this.txtShohinCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("商品コードは正しく入力してください。");
                    return false;
                }
                this.lblShohinNm.Text = name;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            // 商品コードの入力チェック
            if (!ValChk.IsEmpty(this.txtShohinCd.Text))
            {
                if (!IsValidShohinCd())
                {
                    this.txtShohinCd.Focus();
                    this.txtShohinCd.SelectAll();
                    return false;
                }
            }
            else
            {
                Msg.Error("商品コードを入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBMR1031R rpt = new KBMR1031R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;


                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Error("データがありません。");
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpjpDateTo2 = Util.ConvJpDate(tmpDateTo2, this.Dba);
            // 支所コード
            string shishoCd = this.txtMizuageShishoCd.Text;


            StringBuilder Sql;
            Decimal zaiko = 0; // 在庫数
            string denpyoKbn; // 伝票区分
            string[] denpyoDate; // 伝票日付

            // メインループデータ取得準備
            // Zam.TB_会社情報(TB_ZM_KAISHA_JOHO)
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            DataTable dtZM_KAISHA_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_ZM_KAISHA_JOHO",
                "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                "KAISHA_CD,KESSANKI",
                dpc);
            if (dtZM_KAISHA_JOHO.Rows.Count == 0)
            {
                return false;
            }

            #region ループ用データ取得
            // 在庫管理区分
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     A.*,");
            Sql.Append("     B.ZAIKO_KANRI_KUBUN, ");
            Sql.Append("     B.SAISHU_SHIIRE_TANKA ");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_SHOHIN_BETSU_ZAIKO AS A ");
            Sql.Append(" LEFT OUTER JOIN");
            Sql.Append("     VI_HN_SHOHIN_ZAIKO AS B");
            Sql.Append("  ON ");
            Sql.Append("     A.KAISHA_CD = B.KAISHA_CD AND");
            Sql.Append("     A.SHISHO_CD = B.SHISHO_CD AND");
            Sql.Append("     A.SHOHIN_CD = B.SHOHIN_CD ");
            Sql.Append(" WHERE ");
            Sql.Append("     B.SHOHIN_KUBUN5 <> 1 AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append("     A.SHOHIN_CD = @SHOHIN_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 13, this.txtShohinCd.Text);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);

            DataTable dtZaikoKanriKbn = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 在庫数
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     *");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_SHOHIN_ZAIKO_ZEI_KUBUN ");
            Sql.Append(" WHERE");
            Sql.Append("     KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     SHISHO_CD = @SHISHO_CD AND");
            Sql.Append("     SHOHIN_CD = @SHOHIN_CD ");
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 13, this.txtShohinCd.Text);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);

            DataTable dtZaiko = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 出庫数量-入庫数量
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append("    SOKO_CD AS SOKO_CD,");
            Sql.Append("    MAX(SHOHIN_CD) AS SHOHIN_CD,");
            Sql.Append("    MAX(SHOHIN_NM) AS SHOHIN_NM,");
            Sql.Append("    SUM(BARA_SOSU) AS BARA_SOSU,");
            Sql.Append("    SUM(CASE_SU) AS CASE_SU,");
            Sql.Append("    SUM(BARASU) AS BARASU ");
            Sql.Append("FROM");
            Sql.Append("    (SELECT ");
            Sql.Append("        0 AS SOKO_CD,");
            Sql.Append("        MAX(SHOHIN_CD) AS SHOHIN_CD,");
            Sql.Append("        MAX(SHOHIN_NM) AS SHOHIN_NM,");
            Sql.Append("        SUM(CASE WHEN DENPYO_KUBUN = 1 THEN");
            Sql.Append("                CASE WHEN TORIHIKI_KUBUN2 = 2 THEN");
            Sql.Append("                    -((IRISU * SURYO1) + SURYO2)");
            Sql.Append("                ELSE");
            Sql.Append("                    ((IRISU * SURYO1) + SURYO2) END");
            Sql.Append("            ELSE CASE WHEN TORIHIKI_KUBUN2 = 2 THEN");
            Sql.Append("                ((IRISU * SURYO1) + SURYO2)");
            Sql.Append("            ELSE");
            Sql.Append("                -((IRISU * SURYO1) + SURYO2) END END) AS BARA_SOSU,");
            Sql.Append("        SUM(CASE WHEN DENPYO_KUBUN = 1 THEN");
            Sql.Append("                CASE WHEN TORIHIKI_KUBUN2 = 2 THEN");
            Sql.Append("                    -SURYO1");
            Sql.Append("                ELSE");
            Sql.Append("                    SURYO1 END");
            Sql.Append("            ELSE CASE WHEN TORIHIKI_KUBUN2 = 2 THEN");
            Sql.Append("                SURYO1");
            Sql.Append("            ELSE");
            Sql.Append("                -SURYO1 END END) AS CASE_SU,");
            Sql.Append("        SUM(CASE WHEN DENPYO_KUBUN = 1 THEN");
            Sql.Append("                CASE WHEN TORIHIKI_KUBUN2 = 2 THEN");
            Sql.Append("                    -SURYO2");
            Sql.Append("                ELSE");
            Sql.Append("                    SURYO2 END");
            Sql.Append("            ELSE CASE WHEN TORIHIKI_KUBUN2 = 2 THEN");
            Sql.Append("                SURYO2");
            Sql.Append("            ELSE");
            Sql.Append("                -SURYO2 END END) AS BARASU");
            Sql.Append("    FROM");
            Sql.Append("        VI_HN_TORIHIKI_MEISAI");
            Sql.Append("    WHERE");
            Sql.Append("        KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("        SHISHO_CD = @SHISHO_CD AND");
            Sql.Append("        DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            Sql.Append("        SHOHIN_CD = @SHOHIN_CD");
            Sql.Append("        ) AS A ");
            Sql.Append("GROUP BY");
            Sql.Append("    SOKO_CD ");
            Sql.Append("ORDER BY");
            Sql.Append("    SOKO_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 13, this.txtShohinCd.Text);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);


            DataTable dtTsukiIdoSuryo = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 選択月以降の移動数量
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append("    C.TANABAN AS TANABAN,");
            Sql.Append("    A.SHOHIN_CD AS SHOHIN_CD,");
            Sql.Append("    0 AS TENPO_CD,");
            Sql.Append("    SUM(CASE WHEN B.DENPYO_KUBUN = 1 THEN");
            Sql.Append("        (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN");
            Sql.Append("            (CASE WHEN B.TORIHIKI_KUBUN1 = 2 THEN");
            Sql.Append("                (((A.SURYO1 * A.IRISU) + A.SURYO2) * 1)");
            Sql.Append("            ELSE");
            Sql.Append("                (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) END)");
            Sql.Append("        ELSE");
            Sql.Append("            ((A.SURYO1 * A.IRISU) + A.SURYO2) END)");
            Sql.Append("    ELSE");
            Sql.Append("        (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN");
            Sql.Append("            ((A.SURYO1 * A.IRISU) + A.SURYO2)");
            Sql.Append("        ELSE");
            Sql.Append("            (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) END) END) AS IDO_SURYO ");
            Sql.Append("FROM");
            Sql.Append("    TB_HN_TORIHIKI_MEISAI AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append("    TB_HN_TORIHIKI_DENPYO AS B ");
            Sql.Append("ON A.KAISHA_CD = B.KAISHA_CD AND");
            Sql.Append("    A.SHISHO_CD = B.SHISHO_CD AND");
            Sql.Append("    A.DENPYO_KUBUN = B.DENPYO_KUBUN AND");
            Sql.Append("    A.DENPYO_BANGO = B.DENPYO_BANGO AND");
            Sql.Append("    A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append("    TB_HN_SHOHIN AS C ");
            Sql.Append("ON A.KAISHA_CD = C.KAISHA_CD AND");
            Sql.Append("    A.SHISHO_CD = C.SHISHO_CD AND ");
            Sql.Append("    A.SHOHIN_CD = C.SHOHIN_CD ");
            Sql.Append("WHERE");
            Sql.Append("    A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("    A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append("    B.DENPYO_DATE > @DATE_TO AND");
            Sql.Append("    A.SHOHIN_CD = @SHOHIN_CD AND");
            Sql.Append("    C.ZAIKO_KANRI_KUBUN = 1 ");
            Sql.Append("GROUP BY");
            Sql.Append("    C.TANABAN, A.SHOHIN_CD ");
            Sql.Append("ORDER BY");
            Sql.Append("    C.TANABAN, A.SHOHIN_CD ");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 13, this.txtShohinCd.Text);
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);

            DataTable dtIdoSuryo = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     A.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append("     A.TOKUISAKI_CD AS TOKUISAKI_CD_OR_IDO_MOTO,");
            Sql.Append("     A.TOKUISAKI_NM AS TOKUISAKI_NM,");
            Sql.Append("     A.DENPYO_KUBUN AS DENPYO_KUBUN,");
            Sql.Append("     A.TORIHIKI_KUBUN AS TORIHIKI_KUBUN,");
            Sql.Append("     A.TORIHIKI_KUBUN2 AS TORIHIKI_KUBUN2,");
            Sql.Append("     A.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append("     A.GYO_BANGO AS GYO_BANGO,");
            Sql.Append("     A.SHOHIN_CD AS SHOHIN_CD,");
            Sql.Append("     A.SHOHIN_NM AS SHOHIN_NM,");
            Sql.Append("     0 AS SOKO_CD_OR_IDO_SAKI,");
            Sql.Append("     (A.IRISU * A.SURYO1) + (A.SURYO2) AS SO_BARASU,");
            Sql.Append("     A.SURYO1 AS CASE_SU,");
            Sql.Append("     A.SURYO2 AS BARASU,");
            Sql.Append("     A.IRISU AS IRISU,");
            Sql.Append("     A.BAIKA_KINGAKU AS BAIKA_KINGAKU,");
            Sql.Append("     A.SHOHIZEI AS SHOHIZEI,");
            Sql.Append(" 	B.URIAGE_ZEI_KUBUN,");
            Sql.Append(" 	B.SHIIRE_ZEI_KUBUN");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_TORIHIKI_MEISAI A ");
            Sql.Append(" 	LEFT JOIN VI_HN_SHOHIN B");
            Sql.Append(" 	ON	A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD");
            Sql.Append(" 	AND A.SHOHIN_CD = B.SHOHIN_CD");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append("     A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            Sql.Append("     A.SHOHIN_CD = @SHOHIN_CD");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.DENPYO_DATE, ");
            Sql.Append(" 	A.DENPYO_KUBUN DESC, ");
            Sql.Append(" 	A.DENPYO_BANGO");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 13, this.txtShohinCd.Text);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtZaikoKanriKbn.Rows.Count > 0 && Util.ToDecimal(dtZaikoKanriKbn.Rows[0]["ZAIKO_KANRI_KUBUN"]) == 0)
            {
                Msg.Info("商品コード" + this.txtShohinCd.Text + "は、在庫管理区分なしです。");
            }

            // 月初在庫数の計算
            if (dtMainLoop.Rows.Count == 0)
            {
                if (dtIdoSuryo.Rows.Count > 0)
                {
                    zaiko = Util.ToDecimal(dtZaiko.Rows[0]["SURYO2"]) + Util.ToDecimal(dtIdoSuryo.Rows[0]["IDO_SURYO"]);
                }
                else if (dtTsukiIdoSuryo.Rows.Count > 0)
                {
                    zaiko = Util.ToDecimal(dtZaiko.Rows[0]["SURYO2"]) + Util.ToDecimal(dtTsukiIdoSuryo.Rows[0]["BARASU"]);
                }
                else
                {
                    zaiko = Util.ToDecimal(dtZaiko.Rows[0]["SURYO2"]);
                }
            }
            else
            {
                if (dtZaiko.Rows.Count != 0)
                {
                    zaiko += Util.ToDecimal(dtZaiko.Rows[0]["SURYO2"]);
                }
                if (dtIdoSuryo.Rows.Count != 0)
                {
                    zaiko += Util.ToDecimal(dtIdoSuryo.Rows[0]["IDO_SURYO"]);
                }
                if (dtTsukiIdoSuryo.Rows.Count != 0)
                {
                    zaiko += Util.ToDecimal(dtTsukiIdoSuryo.Rows[0]["BARASU"]);
                }
            }
            int dbSORT = 1;
            decimal zaikoZeinukiKingaku = 0;//税抜き金額
            decimal zaikoZeikomiKingaku = 0;//税込み金額
            decimal Zaikoshohizei;//消費税
            decimal consumptionTax = 1; // 消費税率初期値（バグがわかりやすいように1%を設定している
            decimal nyukoZeinukiKingaku = 0;//税抜き金額
            decimal nyukoZeikomiKingaku = 0;//税込み金額
            decimal nyukoshohizei;//消費税
            decimal syukoZeinukiKingaku = 0;//税抜き金額
            decimal syukoZeikomiKingaku = 0;//税込み金額
            decimal syukoshohizei;//消費税

            if (dtMainLoop.Rows.Count > 0)
            {
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    // 伝票日付を和暦で保持
                    denpyoDate = Util.ConvJpDate(Util.ToString(dr["DENPYO_DATE"]), this.Dba);

                    // 在庫数
                    if (Util.ToDecimal(dr["DENPYO_KUBUN"]) == 1)
                    {
                        zaiko = zaiko - Util.ToDecimal(dr["BARASU"]);
                    }
                    else if (Util.ToDecimal(dr["DENPYO_KUBUN"]) == 2)
                    {
                        zaiko = zaiko + Util.ToDecimal(dr["BARASU"]);
                    }

                    // 伝票区分
                    if (Util.ToDecimal(dr["DENPYO_KUBUN"]) == 1)
                    {
                        denpyoKbn = "売上";
                    }
                    else
                    {
                        denpyoKbn = "仕入";
                    }

                    #region 印刷ワークテーブルに登録
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                    Sql.Append(") ");
                    // 月計登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                    if (tmpDateFr.Year == tmpDateTo.Year && tmpDateFr.Month == tmpDateTo.Month && tmpDateFr.Day == 1 && Util.ToDecimal(tmpjpDateTo2[4]) == tmpDateTo.Day)
                    {
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                            string.Format("{0}{1}年{2}月度", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]));
                    }
                    else
                    {
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                            string.Format(tmpjpDateFr[5])
                            + " ～ " +
                            tmpjpDateTo[5]);
                    }

                    // データを設定
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["SHOHIN_NM"]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, denpyoDate[6].Substring(1));
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["DENPYO_BANGO"]);
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, denpyoKbn);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["TORIHIKI_KUBUN"]);
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["TOKUISAKI_NM"]);
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["SOKO_CD_OR_IDO_SAKI"]);
                    if (Util.ToDecimal(dr["DENPYO_KUBUN"]) == 1)
                    {
                        //dpc.SetParam("@ITEM13", SqlDbType.VarChar, 12, "0.000");
                        //dpc.SetParam("@ITEM14", SqlDbType.VarChar, 12, "0.000");
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 12, "0");
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 12, "0");
                        dpc.SetParam("@ITEM15", SqlDbType.Decimal, 12, dr["CASE_SU"]);
                        dpc.SetParam("@ITEM16", SqlDbType.Decimal, 12, dr["BARASU"]);
                    }
                    else
                    {
                        dpc.SetParam("@ITEM13", SqlDbType.Decimal, 12, dr["CASE_SU"]);
                        dpc.SetParam("@ITEM14", SqlDbType.Decimal, 12, dr["BARASU"]);
                        //dpc.SetParam("@ITEM15", SqlDbType.VarChar, 12, "0.000");
                        //dpc.SetParam("@ITEM16", SqlDbType.VarChar, 12, "0.000");
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 12, "0");
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 12, "0");
                    }
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 12, "0.000");
                    if (dtZaikoKanriKbn.Rows.Count > 0 && Util.ToDecimal(dtZaikoKanriKbn.Rows[0]["ZAIKO_KANRI_KUBUN"]) == 0)
                    {
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 12, "0.000");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM18", SqlDbType.Decimal, 12, zaiko);
                    }
                    // ※一個あたりの税抜金額計算（在庫）
                    zaikoZeinukiKingaku = Util.ToDecimal(dtZaikoKanriKbn.Rows[0]["KORI_TANKA"]);
                    /// 最終仕入単価も用意しておく
                    //zaikoZeinukiKingaku = Util.ToDecimal(dtZaikoKanriKbn.Rows[0]["SAISHU_SHIIRE_TANKA"]);

                    // 消費税計算
                    consumptionTax = getShouhizeiRitsu(Util.ToDecimal( dr["SHIIRE_ZEI_KUBUN"]), Util.ToDate(dr["DENPYO_DATE"]));
                    Zaikoshohizei = GetTaxCalc(zaikoZeinukiKingaku, consumptionTax);
                    // 税込金額計算
                    zaikoZeikomiKingaku = zaikoZeinukiKingaku + Zaikoshohizei;
                    dpc.SetParam("@ITEM19", SqlDbType.Decimal, 12, zaikoZeikomiKingaku);
                    if (Util.ToDecimal(dr["DENPYO_KUBUN"]) == 1)
                    {
                        dpc.SetParam("@ITEM20", SqlDbType.Decimal, 12, "0.000");

                        // （出庫）税抜金額計算
                        syukoZeinukiKingaku = Util.ToDecimal(dr["BAIKA_KINGAKU"]);
                        // 消費税計算
                        syukoshohizei = Util.ToDecimal(dr["SHOHIZEI"]);
                        syukoZeikomiKingaku = syukoZeinukiKingaku + syukoshohizei;
                        dpc.SetParam("@ITEM21", SqlDbType.Decimal, 12, syukoZeikomiKingaku);
                    }
                    else
                    {
                        // （入庫）税抜金額計算
                        nyukoZeinukiKingaku = Util.ToDecimal(dr["BAIKA_KINGAKU"]);
                        nyukoshohizei = Util.ToDecimal(dr["SHOHIZEI"]);
                        // 税込金額計算
                        nyukoZeikomiKingaku = nyukoZeinukiKingaku + nyukoshohizei;
                        dpc.SetParam("@ITEM20", SqlDbType.Decimal, 12, nyukoZeikomiKingaku);

                        dpc.SetParam("@ITEM21", SqlDbType.Decimal, 12, "0.000");
                    }

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                    dbSORT++;
                    #endregion
                }
            }
            else if (Util.ToDecimal(dtZaiko.Rows.Count) > 0)
            {
                #region 印刷ワークテーブルに登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                //Sql.Append(" ,ITEM01");
                //Sql.Append(" ,ITEM02");
                //Sql.Append(" ,ITEM03");
                //Sql.Append(" ,ITEM04");
                //Sql.Append(" ,ITEM05");
                //Sql.Append(" ,ITEM06");
                //Sql.Append(" ,ITEM07");
                //Sql.Append(" ,ITEM08");
                //Sql.Append(" ,ITEM09");
                //Sql.Append(" ,ITEM10");
                //Sql.Append(" ,ITEM11");
                //Sql.Append(" ,ITEM12");
                //Sql.Append(" ,ITEM13");
                //Sql.Append(" ,ITEM14");
                //Sql.Append(" ,ITEM15");
                //Sql.Append(" ,ITEM16");
                //Sql.Append(" ,ITEM17");
                //Sql.Append(" ,ITEM18");
                //Sql.Append(" ,ITEM19");
                //Sql.Append(" ,ITEM20");
                //Sql.Append(" ,ITEM21");
                //Sql.Append(") ");
                //Sql.Append("VALUES(");
                //Sql.Append("  @GUID");
                //Sql.Append(" ,@SORT");
                //Sql.Append(" ,@ITEM01");
                //Sql.Append(" ,@ITEM02");
                //Sql.Append(" ,@ITEM03");
                //Sql.Append(" ,@ITEM04");
                //Sql.Append(" ,@ITEM05");
                //Sql.Append(" ,@ITEM06");
                //Sql.Append(" ,@ITEM07");
                //Sql.Append(" ,@ITEM08");
                //Sql.Append(" ,@ITEM09");
                //Sql.Append(" ,@ITEM10");
                //Sql.Append(" ,@ITEM11");
                //Sql.Append(" ,@ITEM12");
                //Sql.Append(" ,@ITEM13");
                //Sql.Append(" ,@ITEM14");
                //Sql.Append(" ,@ITEM15");
                //Sql.Append(" ,@ITEM16");
                //Sql.Append(" ,@ITEM17");
                //Sql.Append(" ,@ITEM18");
                //Sql.Append(" ,@ITEM19");
                //Sql.Append(" ,@ITEM20");
                //Sql.Append(" ,@ITEM21");
                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                Sql.Append(") ");
                // 月計登録
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                if (tmpDateFr.Year == tmpDateTo.Year && tmpDateFr.Month == tmpDateTo.Month && tmpDateFr.Day == 1 && Util.ToDecimal(tmpjpDateTo2[4]) == tmpDateTo.Day)
                {
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                        string.Format("{0}{1}年{2}月度", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]));
                }
                else
                {
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                        string.Format(tmpjpDateFr[5])
                        + " ～ " +
                        tmpjpDateTo[5]);
                }
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, txtShohinCd.Text);
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, lblShohinNm.Text);
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, "注意：入数は０です。");
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "");
                //dpc.SetParam("@ITEM13", SqlDbType.VarChar, 12, "0.000");
                //dpc.SetParam("@ITEM14", SqlDbType.VarChar, 12, "0.000");
                //dpc.SetParam("@ITEM15", SqlDbType.VarChar, 12, "0.000");
                //dpc.SetParam("@ITEM16", SqlDbType.VarChar, 12, "0.000");
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 12, "0");
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 12, "0");
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 12, "0");
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 12, "0");
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 12, "");
                dpc.SetParam("@ITEM18", SqlDbType.Decimal, 12, zaiko);

                // 在庫税抜金額計算
                zaikoZeinukiKingaku = Util.ToDecimal(dtZaikoKanriKbn.Rows[0]["KORI_TANKA"]);
                /// 最終仕入単価も用意しておく
                //zaikoZeinukiKingaku = Util.ToDecimal(dtZaikoKanriKbn.Rows[0]["SAISHU_SHIIRE_TANKA"]);

                // 消費税計算
                //consumptionTax = getShouhizeiRitsu();
                consumptionTax = TaxUtil.GetTaxRate(Util.ToDate(dtZaiko.Rows[0]["SAISHU_SHIIRE_DATE"]), Util.ToDecimal(dtZaiko.Rows[0]["SHIIRE_ZEI_KUBUN"]), this.Dba);
                Zaikoshohizei = GetTaxCalc(zaikoZeinukiKingaku, consumptionTax);
                // 税込金額計算
                zaikoZeikomiKingaku = zaikoZeinukiKingaku + Zaikoshohizei;
                dpc.SetParam("@ITEM19", SqlDbType.Decimal, 12, zaikoZeikomiKingaku);
                //dpc.SetParam("@ITEM20", SqlDbType.Decimal, 12, "0.000");
                //dpc.SetParam("@ITEM21", SqlDbType.Decimal, 12, "0.000");
                dpc.SetParam("@ITEM20", SqlDbType.Decimal, 12, "0");
                dpc.SetParam("@ITEM21", SqlDbType.Decimal, 12, "0");

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 消費税計算処理
        /// </summary>
        /// <param name="CalcValue">消費税を求めたい金額</param>
        /// <param name="TaxValue">消費税率</param>
        /// <returns>消費税計算結果</returns>
        private Decimal GetTaxCalc(Decimal CalcValue, Decimal TaxValue)
        {
            Decimal RetTaxValue = 0;
            Decimal TaxPercent = (TaxValue / 100);
            RetTaxValue = CalcValue * TaxPercent;
            //RetTaxValue = Util.Round(RetTaxValue, 0);
            RetTaxValue = TaxUtil.CalcFraction(RetTaxValue, 0, TaxUtil.ROUND_CATEGORY.ADJUST);

            return RetTaxValue;
        }
        /// <summary>
        /// 消費税率取得
        ///   指定した日付の税率を取得
        /// </summary>
        /// <returns></returns>
        private decimal getShouhizeiRitsu()
        {
            // 指定した日付の税率を返す
            DateTime shiteibi = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
            this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            decimal wk_zeiritu = this.GetZeiritsu(shiteibi);
            return this.GetZeiritsu(shiteibi);
        }
        /// <summary>
        /// 消費税率取得
        ///   指定した日付、税区分より税率を取得
        /// </summary>
        /// <returns></returns>
        private decimal getShouhizeiRitsu(Decimal ZeiKbn, DateTime Hiduke)
        {
            // 指定した日付の税率を返す
            decimal wk_zeiritu = TaxUtil.GetTaxRate(Hiduke, ZeiKbn, this.Dba);
            return wk_zeiritu;
        }

        #endregion
    }
}
