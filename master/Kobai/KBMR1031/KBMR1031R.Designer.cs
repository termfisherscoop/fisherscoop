﻿namespace jp.co.fsi.kb.kbmr1031
{
    /// <summary>
    /// KBMR1031R の概要の説明です。
    /// </summary>
    partial class KBMR1031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBMR1031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.LinTitle = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.rptDate = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblDenpyoDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDenpyoNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTorihikiKbn = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTorihikisakiNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSokoCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNyukoSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCaseSu1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBaraSu1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShukkoSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCaseSu2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBaraSu2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblZaikoSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCaseSu3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBaraSu3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDenpyoDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDenpyoNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDenpyoKbn = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTorihikiKbn = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTorihikisakiNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokoCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNyukoCase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNyukoBara = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSyukkoCase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSyukkoBara = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZaikoCase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZaikoBara = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblGokei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtNyukoGokei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSyukkoGokei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDenpyoDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDenpyoNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTorihikiKbn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTorihikisakiNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSokoCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNyukoSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCaseSu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBaraSu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShukkoSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCaseSu2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBaraSu2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZaikoSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCaseSu3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBaraSu3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenpyoDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenpyoNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenpyoKbn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTorihikiKbn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTorihikisakiNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokoCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNyukoCase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNyukoBara)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyukkoCase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyukkoBara)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoCase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoBara)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNyukoGokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyukkoGokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle,
            this.LinTitle,
            this.txtDate,
            this.ITEM01,
            this.txtShohinCd,
            this.txtShohinNm,
            this.shape1,
            this.txtPage,
            this.lblPage,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.rptDate,
            this.line9,
            this.line10,
            this.line11,
            this.lblDenpyoDate,
            this.lblDenpyoNo,
            this.lblTorihikiKbn,
            this.lblTorihikisakiNm,
            this.lblSokoCd,
            this.lblNyukoSuryo,
            this.lblCaseSu1,
            this.lblBaraSu1,
            this.lblShukkoSuryo,
            this.lblCaseSu2,
            this.lblBaraSu2,
            this.lblZaikoSuryo,
            this.lblCaseSu3,
            this.lblBaraSu3});
            this.pageHeader.Height = 1.2F;
            this.pageHeader.Name = "pageHeader";
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.3228511F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 2.472195F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align: center; verti" +
    "cal-align: middle; ddo-char-set: 128";
            this.lblTitle.Text = "商 品 出 納 帳";
            this.lblTitle.Top = 0F;
            this.lblTitle.Width = 2.541618F;
            // 
            // LinTitle
            // 
            this.LinTitle.Height = 0F;
            this.LinTitle.Left = 2.472195F;
            this.LinTitle.LineWeight = 1F;
            this.LinTitle.Name = "LinTitle";
            this.LinTitle.Top = 0.3228511F;
            this.LinTitle.Width = 2.541618F;
            this.LinTitle.X1 = 2.472195F;
            this.LinTitle.X2 = 5.013813F;
            this.LinTitle.Y1 = 0.3228511F;
            this.LinTitle.Y2 = 0.3228511F;
            // 
            // txtDate
            // 
            this.txtDate.DataField = "ITEM04";
            this.txtDate.Height = 0.1979167F;
            this.txtDate.Left = 2.060918F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; ddo-char-set: 1";
            this.txtDate.Text = "DATE";
            this.txtDate.Top = 0.40625F;
            this.txtDate.Width = 3.364173F;
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.1978183F;
            this.ITEM01.Left = 4.13504F;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "font-family: ＭＳ 明朝; text-align: right; ddo-char-set: 1";
            this.ITEM01.Text = "KAISHA_NM";
            this.ITEM01.Top = 0.6062993F;
            this.ITEM01.Width = 3.28143F;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.DataField = "ITEM05";
            this.txtShohinCd.Height = 0.2291667F;
            this.txtShohinCd.Left = 0.1307087F;
            this.txtShohinCd.MultiLine = false;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Style = "font-family: ＭＳ 明朝; text-align: left; ddo-char-set: 1";
            this.txtShohinCd.Text = "SHOHIN_CD";
            this.txtShohinCd.Top = 0.6062993F;
            this.txtShohinCd.Width = 0.7354331F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.DataField = "ITEM06";
            this.txtShohinNm.Height = 0.2293306F;
            this.txtShohinNm.Left = 0.8858268F;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.txtShohinNm.Text = "SHOHIN_NM";
            this.txtShohinNm.Top = 0.6062993F;
            this.txtShohinNm.Width = 2.626173F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape1.Height = 0.3645833F;
            this.shape1.Left = 0.01023622F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape1.Top = 0.8314961F;
            this.shape1.Width = 7.458333F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1874836F;
            this.txtPage.Left = 7.018505F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; text-align: right; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.1354331F;
            this.txtPage.Width = 0.2834644F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1874836F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.302083F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.1354331F;
            this.lblPage.Width = 0.1666665F;
            // 
            // line1
            // 
            this.line1.Height = 0.3645669F;
            this.line1.Left = 0.6043308F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.8354331F;
            this.line1.Width = 0F;
            this.line1.X1 = 0.6043308F;
            this.line1.X2 = 0.6043308F;
            this.line1.Y1 = 0.8354331F;
            this.line1.Y2 = 1.2F;
            // 
            // line2
            // 
            this.line2.Height = 0.3645669F;
            this.line2.Left = 1.172047F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.8354331F;
            this.line2.Width = 0F;
            this.line2.X1 = 1.172047F;
            this.line2.X2 = 1.172047F;
            this.line2.Y1 = 0.8354331F;
            this.line2.Y2 = 1.2F;
            // 
            // line3
            // 
            this.line3.Height = 0.3645669F;
            this.line3.Left = 1.906299F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.8354331F;
            this.line3.Width = 0F;
            this.line3.X1 = 1.906299F;
            this.line3.X2 = 1.906299F;
            this.line3.Y1 = 0.8354331F;
            this.line3.Y2 = 1.2F;
            // 
            // line4
            // 
            this.line4.Height = 0.3645669F;
            this.line4.Left = 3.630315F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.8354331F;
            this.line4.Width = 0F;
            this.line4.X1 = 3.630315F;
            this.line4.X2 = 3.630315F;
            this.line4.Y1 = 0.8354331F;
            this.line4.Y2 = 1.2F;
            // 
            // line5
            // 
            this.line5.Height = 0.3645669F;
            this.line5.Left = 3.932284F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.8354331F;
            this.line5.Width = 0F;
            this.line5.X1 = 3.932284F;
            this.line5.X2 = 3.932284F;
            this.line5.Y1 = 0.8354331F;
            this.line5.Y2 = 1.2F;
            // 
            // line6
            // 
            this.line6.Height = 0.3645669F;
            this.line6.Left = 5.129921F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.8354331F;
            this.line6.Width = 0F;
            this.line6.X1 = 5.129921F;
            this.line6.X2 = 5.129921F;
            this.line6.Y1 = 0.8354331F;
            this.line6.Y2 = 1.2F;
            // 
            // line7
            // 
            this.line7.Height = 0.3645669F;
            this.line7.Left = 6.307481F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.8354331F;
            this.line7.Width = 0F;
            this.line7.X1 = 6.307481F;
            this.line7.X2 = 6.307481F;
            this.line7.Y1 = 0.8354331F;
            this.line7.Y2 = 1.2F;
            // 
            // line8
            // 
            this.line8.Height = 0F;
            this.line8.Left = 3.936221F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 1.031496F;
            this.line8.Width = 3.531496F;
            this.line8.X1 = 3.936221F;
            this.line8.X2 = 7.467717F;
            this.line8.Y1 = 1.031496F;
            this.line8.Y2 = 1.031496F;
            // 
            // rptDate
            // 
            this.rptDate.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.rptDate.Height = 0.1874016F;
            this.rptDate.Left = 6.182678F;
            this.rptDate.Name = "rptDate";
            this.rptDate.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.rptDate.Top = 0.1354331F;
            this.rptDate.Width = 0.7811022F;
            // 
            // line9
            // 
            this.line9.Height = 0.168504F;
            this.line9.Left = 4.33189F;
            this.line9.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 1.031496F;
            this.line9.Width = 0F;
            this.line9.X1 = 4.33189F;
            this.line9.X2 = 4.33189F;
            this.line9.Y1 = 1.031496F;
            this.line9.Y2 = 1.2F;
            // 
            // line10
            // 
            this.line10.Height = 0.168504F;
            this.line10.Left = 5.517717F;
            this.line10.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 1.031496F;
            this.line10.Width = 0F;
            this.line10.X1 = 5.517717F;
            this.line10.X2 = 5.517717F;
            this.line10.Y1 = 1.031496F;
            this.line10.Y2 = 1.2F;
            // 
            // line11
            // 
            this.line11.Height = 0.168504F;
            this.line11.Left = 6.694882F;
            this.line11.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 1.031496F;
            this.line11.Width = 0F;
            this.line11.X1 = 6.694882F;
            this.line11.X2 = 6.694882F;
            this.line11.Y1 = 1.031496F;
            this.line11.Y2 = 1.2F;
            // 
            // lblDenpyoDate
            // 
            this.lblDenpyoDate.Height = 0.3110236F;
            this.lblDenpyoDate.HyperLink = null;
            this.lblDenpyoDate.Left = 0.01968504F;
            this.lblDenpyoDate.Name = "lblDenpyoDate";
            this.lblDenpyoDate.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle";
            this.lblDenpyoDate.Text = "伝票日付";
            this.lblDenpyoDate.Top = 0.8641733F;
            this.lblDenpyoDate.Width = 0.5637795F;
            // 
            // lblDenpyoNo
            // 
            this.lblDenpyoNo.Height = 0.3110236F;
            this.lblDenpyoNo.HyperLink = null;
            this.lblDenpyoNo.Left = 0.6318898F;
            this.lblDenpyoNo.Name = "lblDenpyoNo";
            this.lblDenpyoNo.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle";
            this.lblDenpyoNo.Text = "伝票No.";
            this.lblDenpyoNo.Top = 0.8641733F;
            this.lblDenpyoNo.Width = 0.5283464F;
            // 
            // lblTorihikiKbn
            // 
            this.lblTorihikiKbn.Height = 0.3110235F;
            this.lblTorihikiKbn.HyperLink = null;
            this.lblTorihikiKbn.Left = 1.253937F;
            this.lblTorihikiKbn.Name = "lblTorihikiKbn";
            this.lblTorihikiKbn.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle";
            this.lblTorihikiKbn.Text = "伝票      取引区分";
            this.lblTorihikiKbn.Top = 0.8641733F;
            this.lblTorihikiKbn.Width = 0.5992124F;
            // 
            // lblTorihikisakiNm
            // 
            this.lblTorihikisakiNm.Height = 0.3110236F;
            this.lblTorihikisakiNm.HyperLink = null;
            this.lblTorihikisakiNm.Left = 2.031496F;
            this.lblTorihikisakiNm.Name = "lblTorihikisakiNm";
            this.lblTorihikisakiNm.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle";
            this.lblTorihikisakiNm.Text = "取 引 先 名";
            this.lblTorihikisakiNm.Top = 0.8641733F;
            this.lblTorihikisakiNm.Width = 1.480315F;
            // 
            // lblSokoCd
            // 
            this.lblSokoCd.Height = 0.2322834F;
            this.lblSokoCd.HyperLink = null;
            this.lblSokoCd.Left = 3.630315F;
            this.lblSokoCd.Name = "lblSokoCd";
            this.lblSokoCd.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblSokoCd.Text = "倉庫ｺｰﾄﾞ";
            this.lblSokoCd.Top = 0.9204726F;
            this.lblSokoCd.Width = 0.3019686F;
            // 
            // lblNyukoSuryo
            // 
            this.lblNyukoSuryo.Height = 0.1250656F;
            this.lblNyukoSuryo.HyperLink = null;
            this.lblNyukoSuryo.Left = 4.018504F;
            this.lblNyukoSuryo.Name = "lblNyukoSuryo";
            this.lblNyukoSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblNyukoSuryo.Text = "入 庫 数 量";
            this.lblNyukoSuryo.Top = 0.8889765F;
            this.lblNyukoSuryo.Width = 1.014961F;
            // 
            // lblCaseSu1
            // 
            this.lblCaseSu1.Height = 0.1250656F;
            this.lblCaseSu1.HyperLink = null;
            this.lblCaseSu1.Left = 3.936F;
            this.lblCaseSu1.Name = "lblCaseSu1";
            this.lblCaseSu1.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblCaseSu1.Text = "ｹｰｽ数";
            this.lblCaseSu1.Top = 1.05F;
            this.lblCaseSu1.Width = 0.3676064F;
            // 
            // lblBaraSu1
            // 
            this.lblBaraSu1.Height = 0.1250656F;
            this.lblBaraSu1.HyperLink = null;
            this.lblBaraSu1.Left = 4.404F;
            this.lblBaraSu1.Name = "lblBaraSu1";
            this.lblBaraSu1.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblBaraSu1.Text = "バラ数";
            this.lblBaraSu1.Top = 1.05F;
            this.lblBaraSu1.Width = 0.6295277F;
            // 
            // lblShukkoSuryo
            // 
            this.lblShukkoSuryo.Height = 0.1250656F;
            this.lblShukkoSuryo.HyperLink = null;
            this.lblShukkoSuryo.Left = 5.220079F;
            this.lblShukkoSuryo.Name = "lblShukkoSuryo";
            this.lblShukkoSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblShukkoSuryo.Text = "出 庫 数 量";
            this.lblShukkoSuryo.Top = 0.8889765F;
            this.lblShukkoSuryo.Width = 1.014961F;
            // 
            // lblCaseSu2
            // 
            this.lblCaseSu2.Height = 0.1250656F;
            this.lblCaseSu2.HyperLink = null;
            this.lblCaseSu2.Left = 5.13F;
            this.lblCaseSu2.Name = "lblCaseSu2";
            this.lblCaseSu2.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblCaseSu2.Text = "ｹｰｽ数";
            this.lblCaseSu2.Top = 1.05F;
            this.lblCaseSu2.Width = 0.3996062F;
            // 
            // lblBaraSu2
            // 
            this.lblBaraSu2.Height = 0.1250656F;
            this.lblBaraSu2.HyperLink = null;
            this.lblBaraSu2.Left = 5.605F;
            this.lblBaraSu2.Name = "lblBaraSu2";
            this.lblBaraSu2.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblBaraSu2.Text = "バラ数";
            this.lblBaraSu2.Top = 1.05F;
            this.lblBaraSu2.Width = 0.6295278F;
            // 
            // lblZaikoSuryo
            // 
            this.lblZaikoSuryo.Height = 0.1250656F;
            this.lblZaikoSuryo.HyperLink = null;
            this.lblZaikoSuryo.Left = 6.401575F;
            this.lblZaikoSuryo.Name = "lblZaikoSuryo";
            this.lblZaikoSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblZaikoSuryo.Text = "在 庫 数 量";
            this.lblZaikoSuryo.Top = 0.8889765F;
            this.lblZaikoSuryo.Width = 1.014961F;
            // 
            // lblCaseSu3
            // 
            this.lblCaseSu3.Height = 0.1250656F;
            this.lblCaseSu3.HyperLink = null;
            this.lblCaseSu3.Left = 6.307F;
            this.lblCaseSu3.Name = "lblCaseSu3";
            this.lblCaseSu3.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblCaseSu3.Text = "ｹｰｽ数";
            this.lblCaseSu3.Top = 1.05F;
            this.lblCaseSu3.Width = 0.3995748F;
            // 
            // lblBaraSu3
            // 
            this.lblBaraSu3.Height = 0.1250656F;
            this.lblBaraSu3.HyperLink = null;
            this.lblBaraSu3.Left = 6.786496F;
            this.lblBaraSu3.Name = "lblBaraSu3";
            this.lblBaraSu3.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center";
            this.lblBaraSu3.Text = "バラ数";
            this.lblBaraSu3.Top = 1.05F;
            this.lblBaraSu3.Width = 0.6295278F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.txtDenpyoDate,
            this.txtDenpyoNo,
            this.txtDenpyoKbn,
            this.txtTorihikiKbn,
            this.txtTorihikisakiNm,
            this.txtSokoCd,
            this.txtNyukoCase,
            this.txtNyukoBara,
            this.txtSyukkoCase,
            this.txtSyukkoBara,
            this.txtZaikoCase,
            this.txtZaikoBara,
            this.label1,
            this.textBox1,
            this.label2,
            this.textBox2,
            this.label3,
            this.textBox3});
            this.detail.Height = 0.3937008F;
            this.detail.Name = "detail";
            // 
            // line12
            // 
            this.line12.Height = 0F;
            this.line12.Left = 0.003937008F;
            this.line12.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.3937008F;
            this.line12.Width = 7.459843F;
            this.line12.X1 = 0.003937008F;
            this.line12.X2 = 7.46378F;
            this.line12.Y1 = 0.3937008F;
            this.line12.Y2 = 0.3937008F;
            // 
            // line13
            // 
            this.line13.Height = 0.3937008F;
            this.line13.Left = 0.01181102F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 0.01181102F;
            this.line13.X2 = 0.01181102F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.3937008F;
            // 
            // line14
            // 
            this.line14.Height = 0.3937008F;
            this.line14.Left = 0.6043308F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 0.6043308F;
            this.line14.X2 = 0.6043308F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.3937008F;
            // 
            // line15
            // 
            this.line15.Height = 0.3937008F;
            this.line15.Left = 1.172047F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 1.172047F;
            this.line15.X2 = 1.172047F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.3937008F;
            // 
            // line16
            // 
            this.line16.Height = 0.3937008F;
            this.line16.Left = 1.906299F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 1.906299F;
            this.line16.X2 = 1.906299F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.3937008F;
            // 
            // line17
            // 
            this.line17.Height = 0.3937008F;
            this.line17.Left = 3.630315F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = -9.313226E-10F;
            this.line17.Width = 0F;
            this.line17.X1 = 3.630315F;
            this.line17.X2 = 3.630315F;
            this.line17.Y1 = -9.313226E-10F;
            this.line17.Y2 = 0.3937008F;
            // 
            // line18
            // 
            this.line18.Height = 0.3937008F;
            this.line18.Left = 3.932284F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 3.932284F;
            this.line18.X2 = 3.932284F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.3937008F;
            // 
            // line19
            // 
            this.line19.Height = 0.3937008F;
            this.line19.Left = 5.129921F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0F;
            this.line19.Width = 0F;
            this.line19.X1 = 5.129921F;
            this.line19.X2 = 5.129921F;
            this.line19.Y1 = 0F;
            this.line19.Y2 = 0.3937008F;
            // 
            // line20
            // 
            this.line20.Height = 0.3937008F;
            this.line20.Left = 6.307481F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0F;
            this.line20.Width = 0F;
            this.line20.X1 = 6.307481F;
            this.line20.X2 = 6.307481F;
            this.line20.Y1 = 0F;
            this.line20.Y2 = 0.3937008F;
            // 
            // line21
            // 
            this.line21.Height = 0.3937008F;
            this.line21.Left = 7.46378F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0F;
            this.line21.Width = 0F;
            this.line21.X1 = 7.46378F;
            this.line21.X2 = 7.46378F;
            this.line21.Y1 = 0F;
            this.line21.Y2 = 0.3937008F;
            // 
            // line22
            // 
            this.line22.Height = 0.3937008F;
            this.line22.Left = 4.33189F;
            this.line22.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0F;
            this.line22.Width = 0F;
            this.line22.X1 = 4.33189F;
            this.line22.X2 = 4.33189F;
            this.line22.Y1 = 0F;
            this.line22.Y2 = 0.3937008F;
            // 
            // line23
            // 
            this.line23.Height = 0.3937008F;
            this.line23.Left = 5.517717F;
            this.line23.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0F;
            this.line23.Width = 0F;
            this.line23.X1 = 5.517717F;
            this.line23.X2 = 5.517717F;
            this.line23.Y1 = 0F;
            this.line23.Y2 = 0.3937008F;
            // 
            // line24
            // 
            this.line24.Height = 0.3937008F;
            this.line24.Left = 6.694882F;
            this.line24.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0F;
            this.line24.Width = 0F;
            this.line24.X1 = 6.694882F;
            this.line24.X2 = 6.694882F;
            this.line24.Y1 = 0F;
            this.line24.Y2 = 0.3937008F;
            // 
            // line25
            // 
            this.line25.Height = 0F;
            this.line25.Left = 0.007874017F;
            this.line25.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.1968504F;
            this.line25.Width = 7.455906F;
            this.line25.X1 = 0.007874017F;
            this.line25.X2 = 7.46378F;
            this.line25.Y1 = 0.1968504F;
            this.line25.Y2 = 0.1968504F;
            // 
            // txtDenpyoDate
            // 
            this.txtDenpyoDate.DataField = "ITEM07";
            this.txtDenpyoDate.Height = 0.1376476F;
            this.txtDenpyoDate.Left = 0.01968504F;
            this.txtDenpyoDate.MultiLine = false;
            this.txtDenpyoDate.Name = "txtDenpyoDate";
            this.txtDenpyoDate.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-justify: auto; ddo-char-set: 1";
            this.txtDenpyoDate.Text = "DENPYO_DATE";
            this.txtDenpyoDate.Top = 0.02401575F;
            this.txtDenpyoDate.Width = 0.5637796F;
            // 
            // txtDenpyoNo
            // 
            this.txtDenpyoNo.DataField = "ITEM08";
            this.txtDenpyoNo.Height = 0.125F;
            this.txtDenpyoNo.Left = 0.6318898F;
            this.txtDenpyoNo.MultiLine = false;
            this.txtDenpyoNo.Name = "txtDenpyoNo";
            this.txtDenpyoNo.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtDenpyoNo.Text = "DENPYO_BANGO";
            this.txtDenpyoNo.Top = 0.03188977F;
            this.txtDenpyoNo.Width = 0.5283466F;
            // 
            // txtDenpyoKbn
            // 
            this.txtDenpyoKbn.DataField = "ITEM09";
            this.txtDenpyoKbn.Height = 0.125F;
            this.txtDenpyoKbn.Left = 1.202756F;
            this.txtDenpyoKbn.MultiLine = false;
            this.txtDenpyoKbn.Name = "txtDenpyoKbn";
            this.txtDenpyoKbn.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.txtDenpyoKbn.Text = "DENPYO_KBN";
            this.txtDenpyoKbn.Top = 0.03188977F;
            this.txtDenpyoKbn.Width = 0.3503936F;
            // 
            // txtTorihikiKbn
            // 
            this.txtTorihikiKbn.DataField = "ITEM10";
            this.txtTorihikiKbn.Height = 0.125F;
            this.txtTorihikiKbn.Left = 1.575591F;
            this.txtTorihikiKbn.MultiLine = false;
            this.txtTorihikiKbn.Name = "txtTorihikiKbn";
            this.txtTorihikiKbn.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtTorihikiKbn.Text = "TORIHIKI_KBN";
            this.txtTorihikiKbn.Top = 0.03188977F;
            this.txtTorihikiKbn.Width = 0.3086611F;
            // 
            // txtTorihikisakiNm
            // 
            this.txtTorihikisakiNm.DataField = "ITEM11";
            this.txtTorihikisakiNm.Height = 0.125F;
            this.txtTorihikisakiNm.Left = 2.002362F;
            this.txtTorihikisakiNm.MultiLine = false;
            this.txtTorihikisakiNm.Name = "txtTorihikisakiNm";
            this.txtTorihikisakiNm.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: left; ddo-char-set: 1";
            this.txtTorihikisakiNm.Text = "TORIHIKISAKI_NM";
            this.txtTorihikisakiNm.Top = 0.03110237F;
            this.txtTorihikisakiNm.Width = 1.627953F;
            // 
            // txtSokoCd
            // 
            this.txtSokoCd.DataField = "ITEM12";
            this.txtSokoCd.Height = 0.125F;
            this.txtSokoCd.Left = 3.691339F;
            this.txtSokoCd.MultiLine = false;
            this.txtSokoCd.Name = "txtSokoCd";
            this.txtSokoCd.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtSokoCd.Text = "SOKO_CD";
            this.txtSokoCd.Top = 0.03188977F;
            this.txtSokoCd.Width = 0.2047246F;
            // 
            // txtNyukoCase
            // 
            this.txtNyukoCase.DataField = "ITEM13";
            this.txtNyukoCase.Height = 0.125F;
            this.txtNyukoCase.Left = 4.018504F;
            this.txtNyukoCase.Name = "txtNyukoCase";
            this.txtNyukoCase.OutputFormat = resources.GetString("txtNyukoCase.OutputFormat");
            this.txtNyukoCase.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtNyukoCase.Text = "NYUKO_CASE";
            this.txtNyukoCase.Top = 0.03188977F;
            this.txtNyukoCase.Width = 0.277559F;
            // 
            // txtNyukoBara
            // 
            this.txtNyukoBara.DataField = "ITEM14";
            this.txtNyukoBara.Height = 0.125F;
            this.txtNyukoBara.Left = 4.403937F;
            this.txtNyukoBara.Name = "txtNyukoBara";
            this.txtNyukoBara.OutputFormat = resources.GetString("txtNyukoBara.OutputFormat");
            this.txtNyukoBara.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtNyukoBara.Text = "NYUKO_BARA";
            this.txtNyukoBara.Top = 0.03188977F;
            this.txtNyukoBara.Width = 0.6452754F;
            // 
            // txtSyukkoCase
            // 
            this.txtSyukkoCase.DataField = "ITEM15";
            this.txtSyukkoCase.Height = 0.125F;
            this.txtSyukkoCase.Left = 5.220079F;
            this.txtSyukkoCase.Name = "txtSyukkoCase";
            this.txtSyukkoCase.OutputFormat = resources.GetString("txtSyukkoCase.OutputFormat");
            this.txtSyukkoCase.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtSyukkoCase.Text = "SYUKKO_CASE";
            this.txtSyukkoCase.Top = 0.03188977F;
            this.txtSyukkoCase.Width = 0.277559F;
            // 
            // txtSyukkoBara
            // 
            this.txtSyukkoBara.DataField = "ITEM16";
            this.txtSyukkoBara.Height = 0.125F;
            this.txtSyukkoBara.Left = 5.605512F;
            this.txtSyukkoBara.Name = "txtSyukkoBara";
            this.txtSyukkoBara.OutputFormat = resources.GetString("txtSyukkoBara.OutputFormat");
            this.txtSyukkoBara.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtSyukkoBara.Text = "SYUKKO_BARA";
            this.txtSyukkoBara.Top = 0.03188977F;
            this.txtSyukkoBara.Width = 0.6452754F;
            // 
            // txtZaikoCase
            // 
            this.txtZaikoCase.DataField = "ITEM17";
            this.txtZaikoCase.Height = 0.125F;
            this.txtZaikoCase.Left = 6.401575F;
            this.txtZaikoCase.Name = "txtZaikoCase";
            this.txtZaikoCase.OutputFormat = resources.GetString("txtZaikoCase.OutputFormat");
            this.txtZaikoCase.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtZaikoCase.Text = "ZAIKO_CASE";
            this.txtZaikoCase.Top = 0.03188977F;
            this.txtZaikoCase.Width = 0.277559F;
            // 
            // txtZaikoBara
            // 
            this.txtZaikoBara.DataField = "ITEM18";
            this.txtZaikoBara.Height = 0.125F;
            this.txtZaikoBara.Left = 6.707087F;
            this.txtZaikoBara.Name = "txtZaikoBara";
            this.txtZaikoBara.OutputFormat = resources.GetString("txtZaikoBara.OutputFormat");
            this.txtZaikoBara.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtZaikoBara.Text = "ZAIKO_BARA";
            this.txtZaikoBara.Top = 0.03188977F;
            this.txtZaikoBara.Width = 0.7094483F;
            // 
            // label1
            // 
            this.label1.Height = 0.1250656F;
            this.label1.HyperLink = null;
            this.label1.Left = 6.330709F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label1.Text = "税込";
            this.label1.Top = 0.2370079F;
            this.label1.Visible = false;
            this.label1.Width = 0.3405514F;
            // 
            // textBox1
            // 
            this.textBox1.CanGrow = false;
            this.textBox1.CurrencyCulture = new System.Globalization.CultureInfo("ja-JP");
            this.textBox1.DataField = "ITEM19";
            this.textBox1.Height = 0.125F;
            this.textBox1.Left = 6.714961F;
            this.textBox1.Name = "textBox1";
            this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.textBox1.Text = "ZEIKOMI_KINGAKU";
            this.textBox1.Top = 0.2370079F;
            this.textBox1.Visible = false;
            this.textBox1.Width = 0.7094488F;
            // 
            // label2
            // 
            this.label2.Height = 0.1250656F;
            this.label2.HyperLink = null;
            this.label2.Left = 3.963386F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label2.Text = "税込";
            this.label2.Top = 0.2370079F;
            this.label2.Visible = false;
            this.label2.Width = 0.3405514F;
            // 
            // textBox2
            // 
            this.textBox2.CanGrow = false;
            this.textBox2.DataField = "ITEM20";
            this.textBox2.Height = 0.125F;
            this.textBox2.Left = 4.339764F;
            this.textBox2.Name = "textBox2";
            this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.textBox2.Text = "ZEIKOMI_KINGAKU";
            this.textBox2.Top = 0.2370079F;
            this.textBox2.Visible = false;
            this.textBox2.Width = 0.7094486F;
            // 
            // label3
            // 
            this.label3.Height = 0.1250656F;
            this.label3.HyperLink = null;
            this.label3.Left = 5.164961F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label3.Text = "税込";
            this.label3.Top = 0.2370079F;
            this.label3.Visible = false;
            this.label3.Width = 0.3405514F;
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = false;
            this.textBox3.DataField = "ITEM21";
            this.textBox3.Height = 0.125F;
            this.textBox3.Left = 5.541339F;
            this.textBox3.Name = "textBox3";
            this.textBox3.OutputFormat = resources.GetString("textBox3.OutputFormat");
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.textBox3.Text = "ZEIKOMI_KINGAKU";
            this.textBox3.Top = 0.2370079F;
            this.textBox3.Visible = false;
            this.textBox3.Width = 0.7094486F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line26,
            this.line27,
            this.line28,
            this.line29,
            this.line30,
            this.line31,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line36,
            this.line37,
            this.line38,
            this.line39,
            this.lblGokei,
            this.txtNyukoGokei,
            this.txtSyukkoGokei,
            this.label4,
            this.textBox4,
            this.label5});
            this.reportFooter1.Height = 0.4006453F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // line26
            // 
            this.line26.Height = 0F;
            this.line26.Left = 0F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 0.3937008F;
            this.line26.Width = 7.46378F;
            this.line26.X1 = 0F;
            this.line26.X2 = 7.46378F;
            this.line26.Y1 = 0.3937008F;
            this.line26.Y2 = 0.3937008F;
            // 
            // line27
            // 
            this.line27.Height = 0.3901575F;
            this.line27.Left = 0.01181102F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 0.003543307F;
            this.line27.Width = 0F;
            this.line27.X1 = 0.01181102F;
            this.line27.X2 = 0.01181102F;
            this.line27.Y1 = 0.003543307F;
            this.line27.Y2 = 0.3937008F;
            // 
            // line28
            // 
            this.line28.Height = 0.3901575F;
            this.line28.Left = 0.6043308F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 0.003543307F;
            this.line28.Width = 0F;
            this.line28.X1 = 0.6043308F;
            this.line28.X2 = 0.6043308F;
            this.line28.Y1 = 0.003543307F;
            this.line28.Y2 = 0.3937008F;
            // 
            // line29
            // 
            this.line29.Height = 0.3901575F;
            this.line29.Left = 1.172047F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 0.003543307F;
            this.line29.Width = 0F;
            this.line29.X1 = 1.172047F;
            this.line29.X2 = 1.172047F;
            this.line29.Y1 = 0.003543307F;
            this.line29.Y2 = 0.3937008F;
            // 
            // line30
            // 
            this.line30.Height = 0.3901575F;
            this.line30.Left = 1.906299F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 0.003543307F;
            this.line30.Width = 0F;
            this.line30.X1 = 1.906299F;
            this.line30.X2 = 1.906299F;
            this.line30.Y1 = 0.003543307F;
            this.line30.Y2 = 0.3937008F;
            // 
            // line31
            // 
            this.line31.Height = 0.3901575F;
            this.line31.Left = 3.630315F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 0.003543307F;
            this.line31.Width = 0F;
            this.line31.X1 = 3.630315F;
            this.line31.X2 = 3.630315F;
            this.line31.Y1 = 0.003543307F;
            this.line31.Y2 = 0.3937008F;
            // 
            // line32
            // 
            this.line32.Height = 0.3937008F;
            this.line32.Left = 3.932284F;
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Top = 0F;
            this.line32.Width = 0F;
            this.line32.X1 = 3.932284F;
            this.line32.X2 = 3.932284F;
            this.line32.Y1 = 0F;
            this.line32.Y2 = 0.3937008F;
            // 
            // line33
            // 
            this.line33.Height = 0.3901575F;
            this.line33.Left = 5.129921F;
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Top = 0.003543307F;
            this.line33.Width = 0F;
            this.line33.X1 = 5.129921F;
            this.line33.X2 = 5.129921F;
            this.line33.Y1 = 0.003543307F;
            this.line33.Y2 = 0.3937008F;
            // 
            // line34
            // 
            this.line34.Height = 0.3901575F;
            this.line34.Left = 6.307481F;
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Top = 0.003543307F;
            this.line34.Width = 0F;
            this.line34.X1 = 6.307481F;
            this.line34.X2 = 6.307481F;
            this.line34.Y1 = 0.003543307F;
            this.line34.Y2 = 0.3937008F;
            // 
            // line35
            // 
            this.line35.Height = 0.3937008F;
            this.line35.Left = 7.46378F;
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Top = 0F;
            this.line35.Width = 0F;
            this.line35.X1 = 7.46378F;
            this.line35.X2 = 7.46378F;
            this.line35.Y1 = 0F;
            this.line35.Y2 = 0.3937008F;
            // 
            // line36
            // 
            this.line36.Height = 0.3937008F;
            this.line36.Left = 4.33189F;
            this.line36.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Top = 0F;
            this.line36.Width = 0F;
            this.line36.X1 = 4.33189F;
            this.line36.X2 = 4.33189F;
            this.line36.Y1 = 0F;
            this.line36.Y2 = 0.3937008F;
            // 
            // line37
            // 
            this.line37.Height = 0.3901575F;
            this.line37.Left = 5.517717F;
            this.line37.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line37.LineWeight = 1F;
            this.line37.Name = "line37";
            this.line37.Top = 0.003543307F;
            this.line37.Width = 0F;
            this.line37.X1 = 5.517717F;
            this.line37.X2 = 5.517717F;
            this.line37.Y1 = 0.003543307F;
            this.line37.Y2 = 0.3937008F;
            // 
            // line38
            // 
            this.line38.Height = 0.3901575F;
            this.line38.Left = 6.694882F;
            this.line38.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line38.LineWeight = 1F;
            this.line38.Name = "line38";
            this.line38.Top = 0.003543307F;
            this.line38.Width = 0F;
            this.line38.X1 = 6.694882F;
            this.line38.X2 = 6.694882F;
            this.line38.Y1 = 0.003543307F;
            this.line38.Y2 = 0.3937008F;
            // 
            // line39
            // 
            this.line39.Height = 0F;
            this.line39.Left = 0.003937006F;
            this.line39.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line39.LineWeight = 1F;
            this.line39.Name = "line39";
            this.line39.Top = 0.1968504F;
            this.line39.Width = 7.459843F;
            this.line39.X1 = 0.003937006F;
            this.line39.X2 = 7.46378F;
            this.line39.Y1 = 0.1968504F;
            this.line39.Y2 = 0.1968504F;
            // 
            // lblGokei
            // 
            this.lblGokei.Height = 0.1354167F;
            this.lblGokei.HyperLink = null;
            this.lblGokei.Left = 2.34375F;
            this.lblGokei.Name = "lblGokei";
            this.lblGokei.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.lblGokei.Text = "【　合　計　】";
            this.lblGokei.Top = 0.02083333F;
            this.lblGokei.Width = 1F;
            // 
            // txtNyukoGokei
            // 
            this.txtNyukoGokei.DataField = "ITEM14";
            this.txtNyukoGokei.Height = 0.125F;
            this.txtNyukoGokei.Left = 4.403937F;
            this.txtNyukoGokei.Name = "txtNyukoGokei";
            this.txtNyukoGokei.OutputFormat = resources.GetString("txtNyukoGokei.OutputFormat");
            this.txtNyukoGokei.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtNyukoGokei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtNyukoGokei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtNyukoGokei.Text = "NYUKO_BARA_GOKEI";
            this.txtNyukoGokei.Top = 0.02086614F;
            this.txtNyukoGokei.Width = 0.6452754F;
            // 
            // txtSyukkoGokei
            // 
            this.txtSyukkoGokei.DataField = "ITEM16";
            this.txtSyukkoGokei.Height = 0.125F;
            this.txtSyukkoGokei.Left = 5.589764F;
            this.txtSyukkoGokei.Name = "txtSyukkoGokei";
            this.txtSyukkoGokei.OutputFormat = resources.GetString("txtSyukkoGokei.OutputFormat");
            this.txtSyukkoGokei.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtSyukkoGokei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSyukkoGokei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSyukkoGokei.Text = "SYUKKO_BARA_GOKEI";
            this.txtSyukkoGokei.Top = 0.02086614F;
            this.txtSyukkoGokei.Width = 0.6452754F;
            // 
            // label4
            // 
            this.label4.Height = 0.1250656F;
            this.label4.HyperLink = null;
            this.label4.Left = 6.338583F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label4.Text = "税込";
            this.label4.Top = 0.2366142F;
            this.label4.Width = 0.3405514F;
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.CurrencyCulture = new System.Globalization.CultureInfo("ja-JP");
            this.textBox4.DataField = "ITEM19";
            this.textBox4.Height = 0.125F;
            this.textBox4.Left = 6.714961F;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.textBox4.Text = "ZEIKOMI_KINGAKU";
            this.textBox4.Top = 0.2366142F;
            this.textBox4.Width = 0.7094486F;
            // 
            // label5
            // 
            this.label5.Height = 0.1354167F;
            this.label5.HyperLink = null;
            this.label5.Left = 1.948032F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.label5.Text = "【一個当たりの税込み金額】";
            this.label5.Top = 0.226378F;
            this.label5.Width = 1.682284F;
            // 
            // KBMR1031R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.3937007F;
            this.PageSettings.Margins.Right = 0.3937007F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.486008F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDenpyoDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDenpyoNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTorihikiKbn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTorihikisakiNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSokoCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNyukoSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCaseSu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBaraSu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShukkoSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCaseSu2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBaraSu2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZaikoSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCaseSu3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBaraSu3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenpyoDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenpyoNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDenpyoKbn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTorihikiKbn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTorihikisakiNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokoCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNyukoCase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNyukoBara)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyukkoCase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyukkoBara)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoCase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoBara)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNyukoGokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyukkoGokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Line LinTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo rptDate;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDenpyoDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDenpyoNo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTorihikiKbn;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTorihikisakiNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSokoCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNyukoSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCaseSu1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBaraSu1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShukkoSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCaseSu2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBaraSu2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblZaikoSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCaseSu3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBaraSu3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDenpyoDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDenpyoNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDenpyoKbn;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTorihikiKbn;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTorihikisakiNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokoCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNyukoCase;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNyukoBara;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSyukkoCase;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSyukkoBara;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZaikoCase;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZaikoBara;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGokei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNyukoGokei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSyukkoGokei;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
    }
}
