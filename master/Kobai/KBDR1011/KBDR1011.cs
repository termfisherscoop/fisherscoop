﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr1011
{
    /// <summary>
    /// 売上日報(KBDR1011)
    /// </summary>
    public partial class KBDR1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 燃料売上
        /// </summary>
        private const string NENRYO_URIAGE = "11";

        /// <summary>
        /// 現金売上
        /// </summary>
        private const string GENKIN_URIAGE = "21";

        /// <summary>
        /// 資材売上
        /// </summary>
        private const string SHIZAI_URIAGE = "31";

        /// <summary>
        /// 資材返品
        /// </summary>
        private const string SHIZAI_HENPIN = "32";

        /// <summary>
        /// 製氷売上
        /// </summary>
        private const string SEIHYO_URIAGE = "41";
       
        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols = 11;
        #endregion

        #region 変数
        /// <summary>
        /// 掛け
        /// </summary>
        private string KAKE = "1";

        /// <summary>
        /// 現金
        /// </summary>
        private string GENKIN = "2";

        /// <summary>
        /// 返品
        /// </summary>
        private string HENPIN = "2";
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        /// <summary>
        /// 消費税額取得用
        /// </summary>
        private Decimal[] _dtDenpyoGokeiGaku = new Decimal[3];
        public Decimal[] DenpyoGokei
        {
            get
            {
                return this._dtDenpyoGokeiGaku;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 設定読み込み
            try
            {
                KAKE = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1011", "Setting", "KAKE");
                GENKIN = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1011", "Setting", "GENKIN");
                HENPIN = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR1011", "Setting", "HENPIN");
            }
            catch (Exception)
            {
                KAKE = "1";
                GENKIN = "2";
                HENPIN = "2";
            }

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            txtDateMonth.Text = jpDate[3];
            txtDateDay.Text = jpDate[4];

            // 初期フォーカス
            txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、担当者コード１・２、船主コード１・２に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtTantoCdFr":
                case "txtTantoCdTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTantoCdFr":
                    #region 担当者CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoCdFr.Text = outData[0];
                                this.lblTantoCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTantoCdTo":
                    #region 担当者CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoCdTo.Text = outData[0];
                                this.lblTantoCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdFr":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdTo":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYear":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                                    this.txtDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengo.Text,
                                        this.txtDateYear.Text,
                                        this.txtDateMonth.Text,
                                        this.txtDateDay.Text,
                                        this.Dba);
                                this.lblDateGengo.Text = arrJpDate[0];
                                this.txtDateYear.Text = arrJpDate[2];
                                this.txtDateMonth.Text = arrJpDate[3];
                                this.txtDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBDR1011R" });
            psForm.ShowDialog();
        }

        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 担当者コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoCodeFr())
            {
                e.Cancel = true;
                this.txtTantoCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 担当者コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoCodeTo())
            {
                e.Cancel = true;
                this.txtTantoCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCodeFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCodeTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                this.txtDateYear.Text = Util.ToString(IsValid.SetYear(this.txtDateYear.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonth.SelectAll();
            }
            else
            {
                this.txtDateMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonth.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDay.SelectAll();
            }
            else
            {
                this.txtDateDay.Text = Util.ToString(IsValid.SetDay(this.txtDateDay.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 船主CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
            {
                this.txtDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(請求書発行)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 担当者コード(自)の入力チェック
        /// </summary>
        /// 
        private bool IsValidTantoCodeFr()
        {
            if (ValChk.IsEmpty(this.txtTantoCdFr.Text))
            {
                this.lblTantoCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtTantoCdFr.Text))
                {
                    Msg.Error("担当者コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "",this.txtTantoCdFr.Text);

                    this.lblTantoCdFr.Text = name;
                }
            return true;
        }

        /// <summary>
        /// 担当者コード(至)の入力チェック
        /// </summary>
        /// 
        private bool IsValidTantoCodeTo()
        {
            if (ValChk.IsEmpty(this.txtTantoCdTo.Text))
            {
                this.lblTantoCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtTantoCdTo.Text))
                {
                    Msg.Error("担当者コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "", this.txtTantoCdTo.Text);

                    this.lblTantoCdTo.Text = name;
                }
            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        /// 
        private bool IsValidFunanushiCodeFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Error("船主コード(自)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                    string name = this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", "", this.txtFunanushiCdFr.Text);

                    this.lblFunanushiCdFr.Text = name;
                }
            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        /// 
        private bool IsValidFunanushiCodeTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Error("船主コード(至)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                string name = this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", "", this.txtFunanushiCdTo.Text);

                this.lblFunanushiCdTo.Text = name;
                }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                this.txtDateDay.Focus();
                this.txtDateDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpDate();
            // 年月日(自)の正しい和暦への変換処理
            SetJpDate();

            // 担当者コード(自)の入力チェック
            if (!IsValidTantoCodeFr())
            {
                this.txtTantoCdFr.Focus();
                this.txtTantoCdFr.SelectAll();
                return false;
            }
            // 担当者コード(至)の入力チェック
            if (!IsValidTantoCodeTo())
            {
                this.txtTantoCdTo.Focus();
                this.txtTantoCdTo.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCodeFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCodeTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
            this.txtDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBDR1011R rpt = new KBDR1011R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();

                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 取引伝票の合計金額取得
        /// </summary>
        /// <param name="DenpyoNo">伝票No</param>
        /// <returns>消費税</returns>
        private void GetDenpyoGokei(string Denpyo_denpyoNo)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            // 担当者コード設定
            string tantoshaCdFr;
            string tantoshaCdTo;
            if (Util.ToString(txtTantoCdFr.Text) != "")
            {
                tantoshaCdFr = txtTantoCdFr.Text;
            }
            else
            {
                tantoshaCdFr = "0";
            }
            if (Util.ToString(txtTantoCdTo.Text) != "")
            {
                tantoshaCdTo = txtTantoCdTo.Text;
            }
            else
            {
                tantoshaCdTo = "9999";
            }
            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToString(txtFunanushiCdFr.Text) != "")
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToString(txtFunanushiCdTo.Text) != "")
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "9999";
            }
            string shishoCd = this.txtMizuageShishoCd.Text;

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            // 検索する伝票番号をセット
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.VarChar, 10, Denpyo_denpyoNo);
            // 検索する担当者コードをセット
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, tantoshaCdFr);
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, tantoshaCdTo);
            // 検索する船主コードをセット
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 4, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 4, funanushiCdTo);
            // 検索する支所コードをセット
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            // han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
            // の検索日付に発生しているデータを取得
            Sql.Append("SELECT ");
            Sql.Append("  *");
            Sql.Append(" FROM ");
            Sql.Append("  TB_HN_TORIHIKI_DENPYO");
            Sql.Append(" WHERE ");
            Sql.Append("      KAISHA_CD    = @KAISHA_CD ");
            if (shishoCd != "0")
                Sql.Append("  AND SHISHO_CD    = @SHISHO_CD ");
            Sql.Append("  AND KAIKEI_NENDO  = @KAIKEI_NENDO ");
            Sql.Append("  AND DENPYO_KUBUN = 1 ");
            Sql.Append("  AND DENPYO_BANGO  = @DENPYO_BANGO ");
            //Sql.Append("  AND KAIIN_BANGO  BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append("  AND TOKUISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append("  AND TANTOSHA_CD  BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO ");
            DataTable dtDenpyoGokeiGaku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            
            DenpyoGokei[0] = Util.ToDecimal(dtDenpyoGokeiGaku.Rows[0]["URIAGE_KINGAKU"]);
            DenpyoGokei[1] = Util.ToDecimal(dtDenpyoGokeiGaku.Rows[0]["SHOHIZEIGAKU"]);
            DenpyoGokei[2] = Util.ToDecimal(dtDenpyoGokeiGaku.Rows[0]["NYUKIN_GOKEIGAKU"]);

        }

        struct NumVals
        {
            public decimal kingaku;
            public decimal shohizei;
            public decimal gokei;

            public void Clear()
            {
                kingaku = 0;
                shohizei = 0;
                gokei = 0;
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region 前準備
            DbParamCollection dpc = new DbParamCollection();
            // 日付を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);
            // 日付を和暦で保持
            string[] tmpjpDate = Util.ConvJpDate(tmpDate, this.Dba);
            int i; // ループ用カウント変数
            // 担当者コード設定
            string tantoshaCdFr;
            string tantoshaCdTo;
            if (Util.ToString(txtTantoCdFr.Text) != "")
            {
                tantoshaCdFr = txtTantoCdFr.Text;
            }
            else
            {
                tantoshaCdFr = "0";
            }
            if (Util.ToString(txtTantoCdTo.Text) != "")
            {
                tantoshaCdTo = txtTantoCdTo.Text;
            }
            else
            {
                tantoshaCdTo = "9999";
            }
            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToString(txtFunanushiCdFr.Text) != "")
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToString(txtFunanushiCdTo.Text) != "")
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "9999";
            }
            string shishoCd = this.txtMizuageShishoCd.Text;

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 検索する日付をセット
            dpc.SetParam("@DATE", SqlDbType.VarChar, 10, tmpDate.Date.ToString("yyyy/MM/dd"));
            // 検索する担当者コードをセット
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, tantoshaCdFr);
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, tantoshaCdTo);
            // 検索する船主コードをセット
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 4, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 4, funanushiCdTo);
            // 検索する支所コードをセット
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            // han.VI_取引明細(VI_HN_TORIHIKI_MEISAI)
            // の検索日付に発生しているデータを取得
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT ");
            Sql.Append("  *");
            Sql.Append(" FROM ");
            Sql.Append("  VI_HN_TORIHIKI_MEISAI");
            Sql.Append(" WHERE ");
            Sql.Append("      KAISHA_CD    = @KAISHA_CD ");
            if (shishoCd != "0")
                Sql.Append("  AND SHISHO_CD    = @SHISHO_CD ");
            Sql.Append("  AND DENPYO_KUBUN = 1 ");
            Sql.Append("  AND DENPYO_DATE  = @DATE ");
            //Sql.Append("  AND KAIIN_BANGO  BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append("  AND TOKUISAKI_CD  BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append("  AND TANTOSHA_CD  BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO ");
            Sql.Append("ORDER BY ");
            Sql.Append("  KAISHA_CD ASC");
            Sql.Append(" ,SHISHO_CD ASC");
            Sql.Append(" ,DENPYO_BANGO ASC");
            Sql.Append(" ,GYO_BANGO ASC");
            Sql.Append(" ,DENPYO_DATE ASC");
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion
            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                dpc = new DbParamCollection();

                NumVals shokei = new NumVals(); ;
                NumVals genkinShiireGokei = new NumVals(); ;
                NumVals kakeShiireGokei = new NumVals(); ;
                NumVals sokei = new NumVals(); ;
                i = 1;
                string denpyoNo = "";
                string torihikiKubun = "";

                int sgn = 1;

                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    // 空じゃない AND 前回の伝票Noと同じじゃない場合
                    if (!ValChk.IsEmpty(denpyoNo) && !Util.ToString(dr["DENPYO_BANGO"]).Equals(denpyoNo))
                    {
                        GetDenpyoGokei(denpyoNo);
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                        // データを設定
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, torihikiKubun);
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, (DenpyoGokei[0] * sgn).ToString("N0"));
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, (DenpyoGokei[1] * sgn).ToString("N0"));
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, (DenpyoGokei[2] * sgn).ToString("N0"));

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        i++;
                    }

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                    // 会員番号、得意先名、商品名を連結
                    //string kaiinBango = dr["KAIIN_BANGO"].ToString();
                    string kaiinBango = dr["TOKUISAKI_CD"].ToString();
                    string tokuisakiNm = dr["TOKUISAKI_NM"].ToString();
                    string shohinNm = dr["SHOHIN_NM"].ToString();
                    // 得意先名が５文字以上なら５文字にする。(バイト数に変更予定)
                    int tokuisakiLen = tokuisakiNm.Length;
                    if (tokuisakiLen >= 5)
                    {
                        tokuisakiNm = tokuisakiNm.Substring(0, 5);
                    }
                    string sumShohinNm = kaiinBango + tokuisakiNm + " " + shohinNm;
                    // 小数点以下が存在するデータの四捨五入
                    decimal baraSosu = Util.Round((decimal)dr["BARA_SOSU"], 1);
                    decimal uriTanka = Util.Round((decimal)dr["URI_TANKA"], 1);
                    decimal baikaKingaku = (decimal)dr["ZEINUKI_KINGAKU"];
                    decimal shohizei = (decimal)dr["SHOHIZEI"];
                    //if (Util.ToString(dr["TORIHIKI_KUBUN"]) == SHIZAI_HENPIN)
                    if (Util.ToString(dr["TORIHIKI_KUBUN2"]) == HENPIN)
                    {
                        baraSosu = baraSosu * -1;
                        baikaKingaku = baikaKingaku * -1;
                        shohizei = shohizei * -1;
                    }
                    // 数値にカンマをセット
                    string baraSosu1 = baraSosu.ToString("N1");
                    string uriTanka1 = uriTanka.ToString("N0");
                    string baikaKingaku1 = baikaKingaku.ToString("N0");
                    string shohizei1;
                    //if (Util.ToString(dr["TORIHIKI_KUBUN"]) == GENKIN_URIAGE)
                    if (Util.ToString(dr["SHOHIZEI_TENKA_HOHO"]) == "1")
                    {
                        shohizei1 = Util.FormatNum(shohizei.ToString("N0"));
                    }
                    else
                    {
                        shohizei1 = "";
                    }
                    //データを設定
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["DENPYO_BANGO"]);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["TORIHIKI_KUBUN_NM"]);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, sumShohinNm);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, baraSosu1);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, uriTanka1);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, baikaKingaku1);
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, shohizei1);

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;

                    sgn = Util.ToString(dr["TORIHIKI_KUBUN2"]) == HENPIN ? -1 : 1;

                    // 明細データから合計の値を足しこむ
                    //if (Util.ToString(dr["TORIHIKI_KUBUN"]) == SHIZAI_HENPIN)
                    //if (Util.ToString(dr["TORIHIKI_KUBUN2"]) == HENPIN)
                    //{
                    //    shokei.kingaku += Util.ToDecimal(dr["ZEINUKI_KINGAKU"]) * -1;
                    //    shokei.shohizei += Util.ToDecimal(dr["SHOHIZEI"]) * -1;
                    //    shokei.gokei = (shokei.kingaku + shokei.shohizei);
                    //    kakeShiireGokei.kingaku += Util.ToDecimal(DenpyoGokei[0]) * -1;
                    //    kakeShiireGokei.shohizei += Util.ToDecimal(DenpyoGokei[1]) * -1;
                    //    kakeShiireGokei.gokei = (kakeShiireGokei.kingaku + kakeShiireGokei.shohizei);
                    //    sokei.kingaku += Util.ToDecimal(dr["ZEINUKI_KINGAKU"]) * -1;
                    //    sokei.shohizei += Util.ToDecimal(dr["SHOHIZEI"]) * -1;
                    //    sokei.gokei = (sokei.kingaku + sokei.shohizei);
                    //}
                    //else
                    //{
                    //    //if (Util.ToString(dr["TORIHIKI_KUBUN"]) == GENKIN_URIAGE)
                        if (Util.ToString(dr["TORIHIKI_KUBUN1"]) == GENKIN)
                        {
                            // 前回の伝票Noと同じじゃない場合
                            if (!Util.ToString(dr["DENPYO_BANGO"]).Equals(denpyoNo))
                            {
                                GetDenpyoGokei(Util.ToString(dr["DENPYO_BANGO"]));
                                genkinShiireGokei.kingaku += Util.ToDecimal(DenpyoGokei[0]) * sgn;
                                genkinShiireGokei.shohizei += Util.ToDecimal(DenpyoGokei[1]) * sgn;
                                genkinShiireGokei.gokei = (genkinShiireGokei.kingaku + genkinShiireGokei.shohizei);
                            }
                        }
                        else
                        {
                            if (!Util.ToString(dr["DENPYO_BANGO"]).Equals(denpyoNo))
                            {
                                GetDenpyoGokei(Util.ToString(dr["DENPYO_BANGO"]));
                                kakeShiireGokei.kingaku += Util.ToDecimal(DenpyoGokei[0]) * sgn;
                                kakeShiireGokei.shohizei += Util.ToDecimal(DenpyoGokei[1]) * sgn;
                                kakeShiireGokei.gokei = (kakeShiireGokei.kingaku + kakeShiireGokei.shohizei);
                            }

                        }
                        // 小計金額・消費税・合計額
                        shokei.kingaku += DenpyoGokei[0] * sgn;
                        shokei.shohizei += DenpyoGokei[1] * sgn;
                        shokei.gokei = (shokei.kingaku + shokei.shohizei);
                        if (!Util.ToString(dr["DENPYO_BANGO"]).Equals(denpyoNo))
                        {
                            GetDenpyoGokei(Util.ToString(dr["DENPYO_BANGO"]));
                            // 総計金額・消費税・合計額
                            sokei.kingaku += DenpyoGokei[0] * sgn;
                            sokei.shohizei += DenpyoGokei[1] * sgn;
                            sokei.gokei = (sokei.kingaku + sokei.shohizei);/**/
                        }
                    //}

                    // 伝票番号の保持
                    denpyoNo = dr["DENPYO_BANGO"].ToString();
                    // 取引区分名称から、表示する文字列を設定
                    //if (Util.ToString(dr["TORIHIKI_KUBUN"]) == SHIZAI_HENPIN)
                    if (Util.ToString(dr["TORIHIKI_KUBUN2"]) == HENPIN)
                    {
                        torihikiKubun = "　　　　　　　【  返品伝票 合 計  】";
                    }
                    else
                    {
                        torihikiKubun = "　　　　　　　【  伝 票 合 計  】";
                    }
                }
                GetDenpyoGokei(denpyoNo);
                // ループ終了後に小計を登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                // 数値にカンマをセット
                string shokeiKin2 = shokei.kingaku.ToString("N0");
                string shokeiSho2 = shokei.shohizei.ToString("N0");
                string shokeiGo2 = shokei.gokei.ToString("N0");
                // データを設定
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, torihikiKubun);
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, (DenpyoGokei[0] * sgn).ToString("N0"));
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, (DenpyoGokei[1] * sgn).ToString("N0"));
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, (DenpyoGokei[2] * sgn).ToString("N0"));

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                // 小計をリセット
                shokei.Clear();
                i++;

                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(") ");

                // 空行登録 １つ
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;

                // 現金売上合計を登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                // 数値にカンマをセット
                string genkinShiireKingaku = genkinShiireGokei.kingaku.ToString("N0");
                string genkinShiireShohizei = genkinShiireGokei.shohizei.ToString("N0");
                string genkinShiireSokei = genkinShiireGokei.gokei.ToString("N0");
                // データを設定
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "【 総合計 】　　①現金売上合計");
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, genkinShiireKingaku);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, genkinShiireShohizei);
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, genkinShiireSokei);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;

                // 掛売上合計を登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                // 数値にカンマをセット
                string kakeShiireKingaku = kakeShiireGokei.kingaku.ToString("N0");
                string kakeShiireShohizei = kakeShiireGokei.shohizei.ToString("N0");
                string kakeShiireSokei = kakeShiireGokei.gokei.ToString("N0");
                // データを設定
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "　　　　　　　　①掛売上合計");
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, kakeShiireKingaku);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, kakeShiireShohizei);
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, kakeShiireSokei);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;

                // 売上合計を登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                // 数値にカンマをセット
                string soShiireKingaku = sokei.kingaku.ToString("N0");
                string soShiireShohizei = sokei.shohizei.ToString("N0");
                string soShiireSokei = sokei.gokei.ToString("N0");
                // データをセット
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "　　　　　　　　①売上合計　(①＋②)");
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, soShiireKingaku);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, soShiireShohizei);
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, soShiireSokei);
                
                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                // Item11に総件数をセット
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                DbParamCollection updParam = new DbParamCollection();
                updParam.SetParam("@ITEM11", SqlDbType.VarChar, 200, i-1);
                this.Dba.Update("PR_HN_TBL", updParam, "GUID = @GUID", whereParam);

                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion

        private void PreviewClose_KeyDown(object sender, KeyEventArgs e)
        {
            // 帳票オブジェクトをインスタンス化
            KBDR1011R rpt = null;

            if (e.KeyCode.ToString() != "C")
            {
                return;
            }
            else
            {
                PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                pFrm.WindowState = FormWindowState.Maximized;
                pFrm.Close();
            }
        }

        private void PreviewClose_KeyDown(object sender, KeyPressEventArgs e)
        {
            // 帳票オブジェクトをインスタンス化
            KBDR1011R rpt = null;

            if (e.KeyChar.ToString() != "C")
            {
                return;
            }
            else
            {
                PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                pFrm.WindowState = FormWindowState.Maximized;

                pFrm.Close();
            }

        }

    }
}
