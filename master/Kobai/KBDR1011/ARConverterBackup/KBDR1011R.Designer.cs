﻿namespace jp.co.fsi.kb.kbdr1011
{
    /// <summary>
    /// KBDR1011R の概要の説明です。
    /// </summary>
    partial class KBDR1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR1011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.totalRowNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ITEM03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detailLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lastLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.nowRouNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalRowNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nowRouNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル0,
            this.ITEM01,
            this.ITEM02,
            this.直線1,
            this.ラベル2,
            this.reportInfo1,
            this.textBox1,
            this.label1,
            this.shape1,
            this.ラベル9,
            this.ラベル10,
            this.ラベル11,
            this.ラベル12,
            this.ラベル13,
            this.ラベル14,
            this.ラベル15,
            this.ラベル16,
            this.直線32,
            this.直線34,
            this.直線35,
            this.直線36,
            this.直線37,
            this.直線38,
            this.直線39,
            this.totalRowNo});
            this.pageHeader.Height = 1.392246F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.28125F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 2.600711F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "売上日報";
            this.ラベル0.Top = 0.3236221F;
            this.ラベル0.Width = 1.78125F;
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.15625F;
            this.ITEM01.Left = 0.2361279F;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM01";
            this.ITEM01.Top = 0.4902888F;
            this.ITEM01.Width = 2.191825F;
            // 
            // ITEM02
            // 
            this.ITEM02.DataField = "ITEM02";
            this.ITEM02.Height = 0.15625F;
            this.ITEM02.Left = 2.882656F;
            this.ITEM02.Name = "ITEM02";
            this.ITEM02.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM02.Tag = "";
            this.ITEM02.Text = "ITEM02";
            this.ITEM02.Top = 0.7569555F;
            this.ITEM02.Width = 1.18125F;
            // 
            // 直線1
            // 
            this.直線1.Height = 0F;
            this.直線1.Left = 2.594461F;
            this.直線1.LineWeight = 2F;
            this.直線1.Name = "直線1";
            this.直線1.Tag = "";
            this.直線1.Top = 0.6409833F;
            this.直線1.Width = 1.811111F;
            this.直線1.X1 = 2.594461F;
            this.直線1.X2 = 4.405572F;
            this.直線1.Y1 = 0.6409833F;
            this.直線1.Y2 = 0.6409833F;
            // 
            // ラベル2
            // 
            this.ラベル2.Height = 0.1569444F;
            this.ラベル2.HyperLink = null;
            this.ラベル2.Left = 0.2326556F;
            this.ラベル2.Name = "ラベル2";
            this.ラベル2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル2.Tag = "";
            this.ラベル2.Text = "0 : 先　頭 ～ 9999 : 最　後";
            this.ラベル2.Top = 0.7590388F;
            this.ラベル2.Width = 1.929167F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1972441F;
            this.reportInfo1.Left = 5.827953F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.reportInfo1.Top = 0.5598426F;
            this.reportInfo1.Width = 1.072129F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.2005741F;
            this.textBox1.Left = 6.900001F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox1.Text = "textBox1";
            this.textBox1.Top = 0.5598426F;
            this.textBox1.Width = 0.2708659F;
            // 
            // label1
            // 
            this.label1.Height = 0.2005741F;
            this.label1.HyperLink = null;
            this.label1.Left = 7.168504F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label1.Text = "頁";
            this.label1.Top = 0.5598426F;
            this.label1.Width = 0.15625F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape1.Height = 0.3834646F;
            this.shape1.Left = 0.1893701F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 1.012205F;
            this.shape1.Width = 7.161024F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.1965278F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 0.2130148F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "伝票No.";
            this.ラベル9.Top = 1.120079F;
            this.ラベル9.Width = 0.5506945F;
            // 
            // ラベル10
            // 
            this.ラベル10.Height = 0.1965278F;
            this.ラベル10.HyperLink = null;
            this.ラベル10.Left = 0.7637796F;
            this.ラベル10.Name = "ラベル10";
            this.ラベル10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル10.Tag = "";
            this.ラベル10.Text = "取引区分";
            this.ラベル10.Top = 1.120079F;
            this.ラベル10.Width = 0.6625984F;
            // 
            // ラベル11
            // 
            this.ラベル11.Height = 0.1972222F;
            this.ラベル11.HyperLink = null;
            this.ラベル11.Left = 1.472737F;
            this.ラベル11.Name = "ラベル11";
            this.ラベル11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル11.Tag = "";
            this.ラベル11.Text = "商　　品　　名";
            this.ラベル11.Top = 1.120079F;
            this.ラベル11.Width = 2.433333F;
            // 
            // ラベル12
            // 
            this.ラベル12.Height = 0.1972222F;
            this.ラベル12.HyperLink = null;
            this.ラベル12.Left = 3.953293F;
            this.ラベル12.Name = "ラベル12";
            this.ラベル12.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル12.Tag = "";
            this.ラベル12.Text = "数　量";
            this.ラベル12.Top = 1.120079F;
            this.ラベル12.Width = 0.5534722F;
            // 
            // ラベル13
            // 
            this.ラベル13.Height = 0.1965278F;
            this.ラベル13.HyperLink = null;
            this.ラベル13.Left = 4.54357F;
            this.ラベル13.Name = "ラベル13";
            this.ラベル13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル13.Tag = "";
            this.ラベル13.Text = "単　価";
            this.ラベル13.Top = 1.120079F;
            this.ラベル13.Width = 0.5555556F;
            // 
            // ラベル14
            // 
            this.ラベル14.Height = 0.1972222F;
            this.ラベル14.HyperLink = null;
            this.ラベル14.Left = 5.134542F;
            this.ラベル14.Name = "ラベル14";
            this.ラベル14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル14.Tag = "";
            this.ラベル14.Text = "金　額";
            this.ラベル14.Top = 1.120079F;
            this.ラベル14.Width = 0.7013889F;
            // 
            // ラベル15
            // 
            this.ラベル15.Height = 0.1965278F;
            this.ラベル15.HyperLink = null;
            this.ラベル15.Left = 5.924126F;
            this.ラベル15.Name = "ラベル15";
            this.ラベル15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル15.Tag = "";
            this.ラベル15.Text = "消費税";
            this.ラベル15.Top = 1.120079F;
            this.ラベル15.Width = 0.5861111F;
            // 
            // ラベル16
            // 
            this.ラベル16.Height = 0.1965278F;
            this.ラベル16.HyperLink = null;
            this.ラベル16.Left = 6.560236F;
            this.ラベル16.Name = "ラベル16";
            this.ラベル16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル16.Tag = "";
            this.ラベル16.Text = "金額計";
            this.ラベル16.Top = 1.120079F;
            this.ラベル16.Width = 0.7838594F;
            // 
            // 直線32
            // 
            this.直線32.Height = 0.3819391F;
            this.直線32.Left = 0.7570867F;
            this.直線32.LineWeight = 1F;
            this.直線32.Name = "直線32";
            this.直線32.Tag = "";
            this.直線32.Top = 1.01378F;
            this.直線32.Width = 0F;
            this.直線32.X1 = 0.7570867F;
            this.直線32.X2 = 0.7570867F;
            this.直線32.Y1 = 1.01378F;
            this.直線32.Y2 = 1.395719F;
            // 
            // 直線34
            // 
            this.直線34.Height = 0.3771321F;
            this.直線34.Left = 1.426669F;
            this.直線34.LineWeight = 1F;
            this.直線34.Name = "直線34";
            this.直線34.Tag = "";
            this.直線34.Top = 1.01378F;
            this.直線34.Width = 0.0001029968F;
            this.直線34.X1 = 1.426772F;
            this.直線34.X2 = 1.426669F;
            this.直線34.Y1 = 1.01378F;
            this.直線34.Y2 = 1.390912F;
            // 
            // 直線35
            // 
            this.直線35.Height = 0.381857F;
            this.直線35.Left = 3.90659F;
            this.直線35.LineWeight = 1F;
            this.直線35.Name = "直線35";
            this.直線35.Tag = "";
            this.直線35.Top = 1.01378F;
            this.直線35.Width = 0.0001029968F;
            this.直線35.X1 = 3.906693F;
            this.直線35.X2 = 3.90659F;
            this.直線35.Y1 = 1.01378F;
            this.直線35.Y2 = 1.395637F;
            // 
            // 直線36
            // 
            this.直線36.Height = 0.3819391F;
            this.直線36.Left = 4.49667F;
            this.直線36.LineWeight = 1F;
            this.直線36.Name = "直線36";
            this.直線36.Tag = "";
            this.直線36.Top = 1.01378F;
            this.直線36.Width = 0.0001802444F;
            this.直線36.X1 = 4.49685F;
            this.直線36.X2 = 4.49667F;
            this.直線36.Y1 = 1.01378F;
            this.直線36.Y2 = 1.395719F;
            // 
            // 直線37
            // 
            this.直線37.Height = 0.377077F;
            this.直線37.Left = 5.087643F;
            this.直線37.LineWeight = 1F;
            this.直線37.Name = "直線37";
            this.直線37.Tag = "";
            this.直線37.Top = 1.01378F;
            this.直線37.Width = 0.0001797676F;
            this.直線37.X1 = 5.087823F;
            this.直線37.X2 = 5.087643F;
            this.直線37.Y1 = 1.01378F;
            this.直線37.Y2 = 1.390857F;
            // 
            // 直線38
            // 
            this.直線38.Height = 0.377077F;
            this.直線38.Left = 5.875143F;
            this.直線38.LineWeight = 1F;
            this.直線38.Name = "直線38";
            this.直線38.Tag = "";
            this.直線38.Top = 1.01378F;
            this.直線38.Width = 0.0001807213F;
            this.直線38.X1 = 5.875324F;
            this.直線38.X2 = 5.875143F;
            this.直線38.Y1 = 1.01378F;
            this.直線38.Y2 = 1.390857F;
            // 
            // 直線39
            // 
            this.直線39.Height = 0.377077F;
            this.直線39.Left = 6.505003F;
            this.直線39.LineWeight = 1F;
            this.直線39.Name = "直線39";
            this.直線39.Tag = "";
            this.直線39.Top = 1.01378F;
            this.直線39.Width = 0.0001811981F;
            this.直線39.X1 = 6.505184F;
            this.直線39.X2 = 6.505003F;
            this.直線39.Y1 = 1.01378F;
            this.直線39.Y2 = 1.390857F;
            // 
            // totalRowNo
            // 
            this.totalRowNo.DataField = "ITEM11";
            this.totalRowNo.Height = 0.3330709F;
            this.totalRowNo.Left = 1.211811F;
            this.totalRowNo.Name = "totalRowNo";
            this.totalRowNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.totalRowNo.Tag = "";
            this.totalRowNo.Text = null;
            this.totalRowNo.Top = 0F;
            this.totalRowNo.Visible = false;
            this.totalRowNo.Width = 0.3826776F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ITEM03,
            this.ITEM04,
            this.ITEM05,
            this.ITEM06,
            this.ITEM07,
            this.ITEM08,
            this.ITEM09,
            this.ITEM10,
            this.detailLine,
            this.直線30,
            this.直線31,
            this.直線40,
            this.直線41,
            this.直線42,
            this.直線43,
            this.直線44,
            this.直線45,
            this.直線46,
            this.lastLine,
            this.nowRouNo});
            this.detail.Height = 0.1980315F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // ITEM03
            // 
            this.ITEM03.DataField = "ITEM03";
            this.ITEM03.Height = 0.15625F;
            this.ITEM03.Left = 0.2129921F;
            this.ITEM03.Name = "ITEM03";
            this.ITEM03.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM03.Tag = "";
            this.ITEM03.Text = "ITEM03";
            this.ITEM03.Top = 0.03779528F;
            this.ITEM03.Width = 0.5275591F;
            // 
            // ITEM04
            // 
            this.ITEM04.DataField = "ITEM04";
            this.ITEM04.Height = 0.15625F;
            this.ITEM04.Left = 0.7637796F;
            this.ITEM04.MultiLine = false;
            this.ITEM04.Name = "ITEM04";
            this.ITEM04.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM04.Tag = "";
            this.ITEM04.Text = "ITEM04";
            this.ITEM04.Top = 0.03779528F;
            this.ITEM04.Width = 0.6627625F;
            // 
            // ITEM05
            // 
            this.ITEM05.DataField = "ITEM05";
            this.ITEM05.Height = 0.15625F;
            this.ITEM05.Left = 1.472835F;
            this.ITEM05.MultiLine = false;
            this.ITEM05.Name = "ITEM05";
            this.ITEM05.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM05.Tag = "";
            this.ITEM05.Text = "ITEM05";
            this.ITEM05.Top = 0.03779528F;
            this.ITEM05.Width = 2.433071F;
            // 
            // ITEM06
            // 
            this.ITEM06.DataField = "ITEM06";
            this.ITEM06.Height = 0.15625F;
            this.ITEM06.Left = 3.905906F;
            this.ITEM06.Name = "ITEM06";
            this.ITEM06.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM06.Tag = "";
            this.ITEM06.Text = "ITEM06";
            this.ITEM06.Top = 0.03779528F;
            this.ITEM06.Width = 0.5629921F;
            // 
            // ITEM07
            // 
            this.ITEM07.DataField = "ITEM07";
            this.ITEM07.Height = 0.15625F;
            this.ITEM07.Left = 4.506693F;
            this.ITEM07.Name = "ITEM07";
            this.ITEM07.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM07.Tag = "";
            this.ITEM07.Text = "ITEM07";
            this.ITEM07.Top = 0.03779528F;
            this.ITEM07.Width = 0.552362F;
            // 
            // ITEM08
            // 
            this.ITEM08.DataField = "ITEM08";
            this.ITEM08.Height = 0.15625F;
            this.ITEM08.Left = 5.099213F;
            this.ITEM08.Name = "ITEM08";
            this.ITEM08.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM08.Tag = "";
            this.ITEM08.Text = "ITEM08";
            this.ITEM08.Top = 0.03779528F;
            this.ITEM08.Width = 0.7381558F;
            // 
            // ITEM09
            // 
            this.ITEM09.DataField = "ITEM09";
            this.ITEM09.Height = 0.15625F;
            this.ITEM09.Left = 5.894095F;
            this.ITEM09.Name = "ITEM09";
            this.ITEM09.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM09.Tag = "";
            this.ITEM09.Text = "ITEM09";
            this.ITEM09.Top = 0.03779528F;
            this.ITEM09.Width = 0.587008F;
            // 
            // ITEM10
            // 
            this.ITEM10.DataField = "ITEM10";
            this.ITEM10.Height = 0.15625F;
            this.ITEM10.Left = 6.510237F;
            this.ITEM10.Name = "ITEM10";
            this.ITEM10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
            this.ITEM10.Tag = "";
            this.ITEM10.Text = "ITEM10";
            this.ITEM10.Top = 0.03779528F;
            this.ITEM10.Width = 0.8145671F;
            // 
            // detailLine
            // 
            this.detailLine.Height = 0F;
            this.detailLine.Left = 0.218504F;
            this.detailLine.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.detailLine.LineWeight = 1F;
            this.detailLine.Name = "detailLine";
            this.detailLine.Tag = "";
            this.detailLine.Top = 0.1948819F;
            this.detailLine.Width = 7.125694F;
            this.detailLine.X1 = 0.218504F;
            this.detailLine.X2 = 7.344198F;
            this.detailLine.Y1 = 0.1948819F;
            this.detailLine.Y2 = 0.1948819F;
            // 
            // 直線30
            // 
            this.直線30.Height = 0.1979167F;
            this.直線30.Left = 0.1943085F;
            this.直線30.LineWeight = 1F;
            this.直線30.Name = "直線30";
            this.直線30.Tag = "";
            this.直線30.Top = 0F;
            this.直線30.Width = 0F;
            this.直線30.X1 = 0.1943085F;
            this.直線30.X2 = 0.1943085F;
            this.直線30.Y1 = 0F;
            this.直線30.Y2 = 0.1979167F;
            // 
            // 直線31
            // 
            this.直線31.Height = 0.1979167F;
            this.直線31.Left = 7.344095F;
            this.直線31.LineWeight = 1F;
            this.直線31.Name = "直線31";
            this.直線31.Tag = "";
            this.直線31.Top = 0F;
            this.直線31.Width = 0F;
            this.直線31.X1 = 7.344095F;
            this.直線31.X2 = 7.344095F;
            this.直線31.Y1 = 0F;
            this.直線31.Y2 = 0.1979167F;
            // 
            // 直線40
            // 
            this.直線40.Height = 0.1965278F;
            this.直線40.Left = 0.7568085F;
            this.直線40.LineWeight = 1F;
            this.直線40.Name = "直線40";
            this.直線40.Tag = "";
            this.直線40.Top = 0F;
            this.直線40.Width = 0F;
            this.直線40.X1 = 0.7568085F;
            this.直線40.X2 = 0.7568085F;
            this.直線40.Y1 = 0F;
            this.直線40.Y2 = 0.1965278F;
            // 
            // 直線41
            // 
            this.直線41.Height = 0.1965278F;
            this.直線41.Left = 1.426772F;
            this.直線41.LineWeight = 1F;
            this.直線41.Name = "直線41";
            this.直線41.Tag = "";
            this.直線41.Top = 0F;
            this.直線41.Width = 0F;
            this.直線41.X1 = 1.426772F;
            this.直線41.X2 = 1.426772F;
            this.直線41.Y1 = 0F;
            this.直線41.Y2 = 0.1965278F;
            // 
            // 直線42
            // 
            this.直線42.Height = 0.1965278F;
            this.直線42.Left = 3.906693F;
            this.直線42.LineWeight = 1F;
            this.直線42.Name = "直線42";
            this.直線42.Tag = "";
            this.直線42.Top = 0F;
            this.直線42.Width = 0F;
            this.直線42.X1 = 3.906693F;
            this.直線42.X2 = 3.906693F;
            this.直線42.Y1 = 0F;
            this.直線42.Y2 = 0.1965278F;
            // 
            // 直線43
            // 
            this.直線43.Height = 0.1965278F;
            this.直線43.Left = 4.49685F;
            this.直線43.LineWeight = 1F;
            this.直線43.Name = "直線43";
            this.直線43.Tag = "";
            this.直線43.Top = 0F;
            this.直線43.Width = 0F;
            this.直線43.X1 = 4.49685F;
            this.直線43.X2 = 4.49685F;
            this.直線43.Y1 = 0F;
            this.直線43.Y2 = 0.1965278F;
            // 
            // 直線44
            // 
            this.直線44.Height = 0.1965278F;
            this.直線44.Left = 5.087795F;
            this.直線44.LineWeight = 1F;
            this.直線44.Name = "直線44";
            this.直線44.Tag = "";
            this.直線44.Top = 0F;
            this.直線44.Width = 0F;
            this.直線44.X1 = 5.087795F;
            this.直線44.X2 = 5.087795F;
            this.直線44.Y1 = 0F;
            this.直線44.Y2 = 0.1965278F;
            // 
            // 直線45
            // 
            this.直線45.Height = 0.1965278F;
            this.直線45.Left = 5.875197F;
            this.直線45.LineWeight = 1F;
            this.直線45.Name = "直線45";
            this.直線45.Tag = "";
            this.直線45.Top = 0F;
            this.直線45.Width = 0F;
            this.直線45.X1 = 5.875197F;
            this.直線45.X2 = 5.875197F;
            this.直線45.Y1 = 0F;
            this.直線45.Y2 = 0.1965278F;
            // 
            // 直線46
            // 
            this.直線46.Height = 0.1965278F;
            this.直線46.Left = 6.505118F;
            this.直線46.LineWeight = 1F;
            this.直線46.Name = "直線46";
            this.直線46.Tag = "";
            this.直線46.Top = 0F;
            this.直線46.Width = 0F;
            this.直線46.X1 = 6.505118F;
            this.直線46.X2 = 6.505118F;
            this.直線46.Y1 = 0F;
            this.直線46.Y2 = 0.1965278F;
            // 
            // lastLine
            // 
            this.lastLine.Height = 0F;
            this.lastLine.Left = 0.1893701F;
            this.lastLine.LineWeight = 1F;
            this.lastLine.Name = "lastLine";
            this.lastLine.Tag = "";
            this.lastLine.Top = 0.1980315F;
            this.lastLine.Width = 7.154832F;
            this.lastLine.X1 = 0.1893701F;
            this.lastLine.X2 = 7.344202F;
            this.lastLine.Y1 = 0.1980315F;
            this.lastLine.Y2 = 0.1980315F;
            // 
            // nowRouNo
            // 
            this.nowRouNo.CountNullValues = true;
            this.nowRouNo.DataField = "SORT";
            this.nowRouNo.Height = 0.15625F;
            this.nowRouNo.Left = 0.06259844F;
            this.nowRouNo.Name = "nowRouNo";
            this.nowRouNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.nowRouNo.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.nowRouNo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.nowRouNo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.nowRouNo.Tag = "";
            this.nowRouNo.Text = "sort";
            this.nowRouNo.Top = 0F;
            this.nowRouNo.Visible = false;
            this.nowRouNo.Width = 0.3826773F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Height = 0F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // KBDR1011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5905512F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.447917F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            this.ReportEnd += new System.EventHandler(this.detail_Format);
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalRowNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nowRouNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM02;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM10;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線30;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線31;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線40;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線41;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線42;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線43;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線44;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線45;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線46;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル10;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル11;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル12;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル13;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル14;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル15;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル16;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線32;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線34;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線35;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線36;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線37;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線38;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線39;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line detailLine;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox nowRouNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox totalRowNo;
        private GrapeCity.ActiveReports.SectionReportModel.Line lastLine;

    }
}
