﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbyr1021
{
    /// <summary>
    /// 購買売上一覧表(KBYR1021)
    /// </summary>
    public partial class KBYR1021 : BasePgForm
    {
        #region 定数
        // 帳票用列数
        public const int prtCols = 26;
        // タイトル列数
        public const int fldCols = 8;
        #endregion

        #region 変数
        // 伝票区分（メニューより）
        private int denpyou_kubun = 1;
        // 商品区分（設定より）
        private int shohin_kubun = 2;
        // 印刷データソート
        int _dbSORT;
        // レポートID
        private string _repID = "KBYR10211R";
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBYR1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = UInfo.ShishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 現在の年号を取得する
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            //今月の最初の日を取得する
            DateTime dtMonthFr = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            string[] jpMontFr = Util.ConvJpDate(dtMonthFr, this.Dba);
            // 日付範囲前
            this.lblGengoFr.Text = jpMontFr[0];
            this.txtYearFr.Text = jpMontFr[2];
            this.txtMonthFr.Text = jpMontFr[3];
            this.txtDayFr.Text = jpMontFr[4];
            // 日付範囲後
            dtMonthFr = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            jpDate = Util.ConvJpDate(dtMonthFr, this.Dba);
            lblGengoTo.Text = jpDate[0];
            txtYearTo.Text = jpDate[2];
            txtMonthTo.Text = jpDate[3];
            txtDayTo.Text = jpDate[4];

            denpyou_kubun = Util.ToInt(Util.ToString(this.Par1));
            if (denpyou_kubun == 0) denpyou_kubun = 1;

            _repID += denpyou_kubun.ToString();

#if DEBUG
            //denpyou_kubun = 2;
#endif

            // 選択項目の商品区分
            try
            {
                this.shohin_kubun = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBYR1021", "Setting", "SHOHIN_KUBUN")));
            }
            catch (Exception)
            {
                this.shohin_kubun = 2;
            }

            if (denpyou_kubun == 2)
            {
                label3.Text = "仕入先CD範囲";
            }

            this.rdoZeikomi.Checked = true;
            // 初期フォーカス
            txtYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年),船主CDにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYearFr":
                case "txtYearTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];

                                // 次の項目(元号)にフォーカス
                                this.txtYearFr.Focus();
                            }
                        }
                    }
                    break;
                #endregion

                #region 元号
                case "txtYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpFr();
                            }
                        }
                    }
                    break;
                #endregion

                #region 元号
                case "txtYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblGengoTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpTo();
                            }
                        }
                    }
                    break;
                #endregion

                #region 船主CD
                case "txtFunanushiCdFr":
                    if (denpyou_kubun == 2)
                    {
                        // アセンブリのロード
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    }
                    else
                    {
                        // アセンブリのロード
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    }
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion

                #region 船主CD 締め
                case "txtFunanushiCdTo":
                    if (denpyou_kubun == 2)
                    {
                        // アセンブリのロード
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1011.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    }
                    else
                    {
                        // アセンブリのロード
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    }
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
                    #endregion
            }
        }

        //ラジオボタン "数量"選択で税込・税抜非活性
        private void rdoSu_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rdoSu.Checked)
            {
                rdoZeikomi.Enabled = false;
                rdoZeikomi.Checked = false;
                rdoZeinuki.Enabled = false;
                rdoZeinuki.Checked = false;
            }
            else
            {
                rdoZeikomi.Enabled = true;
                rdoZeikomi.Checked = true;
                rdoZeinuki.Enabled = true;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F11キー押下処理
        /// 商品区分設定
        /// </summary>
        public override void PressF11()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return;
            }

            // 支所がゼロの場合は設定不可
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                Msg.Notice("支所を指定して下さい。");
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return;
            }

            int kinsuu_kubun = 0;
            if (this.rdoKin.Checked)
            {
                kinsuu_kubun = 1;
                this.rdoKin.Select();
            }
            else
            {
                kinsuu_kubun = 2;
            }

            // 項目設定画面
            using (KBYR1022 frm = new KBYR1022(this.denpyou_kubun.ToString(), kinsuu_kubun.ToString()))
            {
                frm.ShishoCode = Util.ToInt(this.txtMizuageShishoCd.Text);
                frm.ShohinKbn = this.shohin_kubun;
                frm.ShowDialog(this);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { _repID });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtYearFr.SelectAll();
            }
            else
            {
                this.txtYearFr.Text = Util.ToString(IsValid.SetYear(this.txtYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }
        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthFr.SelectAll();
            }
            else
            {
                this.txtMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayFr.SelectAll();
            }
            else
            {
                this.txtDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDayFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtYearTo.SelectAll();
            }
            else
            {
                this.txtYearTo.Text = Util.ToString(IsValid.SetYear(this.txtYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthTo.SelectAll();
            }
            else
            {
                this.txtMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayTo.SelectAll();
            }
            else
            {
                this.txtDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDayTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayFr.Text) > lastDayInMonth)
            {
                this.txtDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayTo.Text) > lastDayInMonth)
            {
                this.txtDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba));
        }
        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            string cdName = "船主";
            string targetView = "VI_HN_FUNANUSHI";
            if (denpyou_kubun == 2)
            {
                cdName = "仕入先";
                targetView = "TB_CM_TORIHIKISAKI";
            }

            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error(cdName + "コードは数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, targetView, this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            string cdName = "船主";
            string targetView = "VI_HN_FUNANUSHI";
            if (denpyou_kubun == 2)
            {
                cdName = "仕入先";
                targetView = "TB_CM_TORIHIKISAKI";
            }

            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error(cdName + "コードは数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, targetView, this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                this.txtYearFr.Focus();
                this.txtYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                this.txtMonthFr.Focus();
                this.txtMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                this.txtDayFr.Focus();
                this.txtDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                this.txtYearTo.Focus();
                this.txtYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                this.txtMonthTo.Focus();
                this.txtMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                this.txtDayTo.Focus();
                this.txtDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            // 設定チェック
            if (!IsSetting())
            {
                Msg.Notice("先に設定を行って下さい。");
                return false;
            }

            return true;
        }

        private bool IsSetting()
        {
            int shishoCd = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));
            int kinsuu_kubun = 0;
            if (this.rdoKin.Checked)
                kinsuu_kubun = 1;
            else
                kinsuu_kubun = 2;

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, denpyou_kubun);
            dpc.SetParam("@KINSUU_KUBUN", SqlDbType.Decimal, 4, kinsuu_kubun);

            // タイトルの設定
            Sql = new StringBuilder();
            Sql.Append(" SELECT DISTINCT ");
            Sql.Append(" GRP_CD,");
            Sql.Append(" GRP_NM ");
            Sql.Append(" FROM TB_HN_KOBAI_LIST_SETTEI ");
            Sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            if (shishoCd != 0)
                Sql.Append("   AND SHISHO_CD = @SHISHO_CD");
            Sql.Append("   AND DENPYO_KUBUN = @DENPYO_KUBUN");
            Sql.Append("   AND KINSUU_KUBUN = @KINSUU_KUBUN");
            Sql.Append(" ORDER BY");
            Sql.Append(" GRP_CD ");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (dt.Rows.Count > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBYR10211R rpt = new KBYR10211R(dtOutput, _repID);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                // デバッグ時はデータの確認をしたいので、データが残るようにする。
                this.Dba.Commit();
#else
                // 印刷後なので、帳票出力用ワークテーブルをロールバックで元に戻す
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 全て選択時は支所毎に抽出
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            // 支所コード
            int shishoCd = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));
            if (shishoCd != 0)
                Sql.Append(" AND SHISHO_CD = @SHISHO_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_SHISHO",
                "KAISHA_CD = @KAISHA_CD " + Sql.ToString(),
                "SHISHO_CD",
                dpc);

            foreach (DataRow dr in dt.Rows)
            {
                MakeWkDataMain(Util.ToInt(Util.ToString(dr["SHISHO_CD"])));
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkDataMain(int shishoCd)
        {
            #region 準備
            // 会計期間開始日を取得
            DateTime kaikeiNendoKaishiBi = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);

            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                    this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                    this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                    this.txtMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtMonthTo.Text))), this.Dba);

            // 会計年度の設定
            string kaikeiNendo;
            if (kaikeiNendoKaishiBi.Month <= tmpDateFr.Month)
            {
                kaikeiNendo = Util.ToString(tmpDateFr.Year);
            }
            else
            {
                kaikeiNendo = Util.ToString(tmpDateFr.Year - 1);
            }
            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpjpDateTo2 = Util.ConvJpDate(tmpDateTo2, this.Dba);
            string[] tmpjpDateOut = Util.ConvJpDate(DateTime.Now, this.Dba);

            // 表示する日付を設定する
            string hyojiDate; // 表示用日付
            string dateFrChk = string.Format("{0}{1}{2}{3}", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4]);
            string dateToChk = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            string dateFrChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], "1");
            string dateToChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo2[4]);
            if (dateFrChk == dateFrChk2 && dateToChk == dateToChk2)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }
            dateFrChk = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4]);
            dateToChk = string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            string dateOut = string.Format("{0}{1}年{2}月{3}日", tmpjpDateOut[0], tmpjpDateOut[2], tmpjpDateOut[3], tmpjpDateOut[4]);

            int kinsuu_kubun = 0;
            if (this.rdoKin.Checked)
                kinsuu_kubun = 1;
            else
                kinsuu_kubun = 2;

            // ループ用カウント変数
            this._dbSORT = (shishoCd * 1000000);

            // 帳票表示用変数
            Decimal kingaku = 0;
            Decimal kingakuSum = 0;
            string tmpkingaku = "";

            // コード設定
            string FunanushiCdFr;
            string FunanushiCdTo;
            if (Util.ToString(txtFunanushiCdFr.Text) != "")
            {
                FunanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                FunanushiCdFr = "0";
            }
            if (Util.ToString(txtFunanushiCdTo.Text) != "")
            {
                FunanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                FunanushiCdTo = "9999";
            }
            // 税抜,税込設定
            string zeiKbn;
            if (rdoZeinuki.Checked)
            {
                //zeiKbn = "税抜き";
                zeiKbn = "【" + this.rdoZeinuki.Text + "】";
            }
            else
            {
                //zeiKbn = "税込み";
                zeiKbn = "【" + this.rdoZeikomi.Text + "】";
            }
            // タイトル設定
            List<string> title = new List<string>();
            title.Add("取　引　先"); // タイトル1
            title.Add(""); // タイトル2
            title.Add(""); // タイトル3
            title.Add(""); // タイトル4
            title.Add(""); // タイトル5
            title.Add(""); // タイトル6
            title.Add(""); // タイトル7
            title.Add(""); // タイトル8
            title.Add("計"); // タイトル9
            // 合計用
            List<decimal> kingakuTtl = new List<decimal>();
            kingakuTtl.Add(0m);
            kingakuTtl.Add(0m);
            kingakuTtl.Add(0m);
            kingakuTtl.Add(0m);
            kingakuTtl.Add(0m);
            kingakuTtl.Add(0m);
            kingakuTtl.Add(0m);
            kingakuTtl.Add(0m);
            kingakuTtl.Add(0m);

            string shisho_cd = "";
            string tokuisaki_cd = "";
            string tokuisaki_nm = "";
            #endregion

            #region メインデータの取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 2, this.UInfo.KessanKi);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 4, FunanushiCdFr);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 4, FunanushiCdTo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, denpyou_kubun);
            dpc.SetParam("@KINSUU_KUBUN", SqlDbType.Decimal, 4, kinsuu_kubun);

            // タイトルの設定
            Sql = new StringBuilder();
            Sql.Append(" SELECT DISTINCT ");
            Sql.Append(" GRP_CD,");
            Sql.Append(" GRP_NM ");
            Sql.Append(" FROM TB_HN_KOBAI_LIST_SETTEI ");
            Sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            Sql.Append("   AND SHISHO_CD = @SHISHO_CD");
            Sql.Append("   AND DENPYO_KUBUN = @DENPYO_KUBUN");
            Sql.Append("   AND KINSUU_KUBUN = @KINSUU_KUBUN");
            Sql.Append(" ORDER BY");
            Sql.Append(" GRP_CD ");
            DataTable dtTitle = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            foreach (DataRow dr in dtTitle.Rows)
            {
                title[Util.ToInt(Util.ToString(dr["GRP_CD"]))] = Util.ToString(dr["GRP_NM"]);
            }

            // メインデータの抽出
            Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.TOKUISAKI_CD,");
            Sql.Append(" MAX(A.TOKUISAKI_NM) AS TOKUISAKI_NM,");
            for (int col = 1; col < fldCols; col++)
            {
                if (kinsuu_kubun == 1)
                    Sql.Append(" SUM(CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN A.KINGAKU" + col.ToString() + " * -1 ELSE A.KINGAKU" + col.ToString() + " END) AS KINGAKU" + col.ToString() + ",");
                else
                    Sql.Append(" SUM(CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN A.KINGAKU" + col.ToString() + " * -1 ELSE A.KINGAKU" + col.ToString() + " END) AS KINGAKU" + col.ToString() + ",");

            }
            Sql.Append(" NULL AS dummy_field ");
            Sql.Append(" FROM (");

            #region サブクエリ
            string taxField = "";
            if (rdoZeikomi.Checked)
            {
                taxField = " + S.SHOHIZEI";
            }
            Sql.Append(" SELECT ");
            Sql.Append(" S.SHISHO_CD,");
            Sql.Append(" S.TOKUISAKI_CD,");
            Sql.Append(" S.TOKUISAKI_NM,");
            Sql.Append(" S.TORIHIKI_KUBUN2,");
            for (int col = 1; col < fldCols; col++)
            {
                if (kinsuu_kubun == 1)
                {
                    Sql.Append(" CASE WHEN H.SHOHIN_KUBUN" + shohin_kubun.ToString() + " IN(");
                    Sql.Append("          SELECT SHOHIN_KUBUN FROM TB_HN_KOBAI_LIST_SETTEI");
                    Sql.Append("          WHERE KAISHA_CD = @KAISHA_CD");
                    Sql.Append("            AND SHISHO_CD = @SHISHO_CD");
                    Sql.Append("            AND DENPYO_KUBUN = @DENPYO_KUBUN");
                    Sql.Append("            AND KINSUU_KUBUN = @KINSUU_KUBUN");
                    Sql.Append("            AND GRP_CD = " + col.ToString() + ") ");
                    Sql.Append("      THEN S.ZEINUKI_KINGAKU" + taxField + " ELSE 0 END KINGAKU" + col.ToString() + ",");
                }
                else
                {
                    Sql.Append(" CASE WHEN H.SHOHIN_KUBUN" + shohin_kubun.ToString() + " IN(");
                    Sql.Append("          SELECT SHOHIN_KUBUN FROM TB_HN_KOBAI_LIST_SETTEI");
                    Sql.Append("          WHERE KAISHA_CD = @KAISHA_CD");
                    Sql.Append("            AND SHISHO_CD = @SHISHO_CD");
                    Sql.Append("            AND DENPYO_KUBUN = @DENPYO_KUBUN");
                    Sql.Append("            AND KINSUU_KUBUN = @KINSUU_KUBUN");
                    Sql.Append("            AND GRP_CD = " + col.ToString() + ") ");
                    Sql.Append("      THEN S.BARA_SOSU ELSE 0 END KINGAKU" + col.ToString() + ",");
                }
            }
            Sql.Append(" NULL AS dummy_field ");

            Sql.Append(" FROM VI_HN_TORIHIKI_MEISAI AS S ");
            Sql.Append(" LEFT OUTER JOIN VI_HN_SHOHIN AS H ");
            Sql.Append("     ON S.KAISHA_CD = H.KAISHA_CD ");
            Sql.Append("    AND S.SHISHO_CD = H.SHISHO_CD ");
            Sql.Append("    AND S.SHOHIN_CD = H.SHOHIN_CD ");
            Sql.Append(" WHERE");
            Sql.Append("    S.KAISHA_CD = @KAISHA_CD AND ");

            if (shishoCd != 0)
                Sql.Append("    S.SHISHO_CD = @SHISHO_CD AND");

            Sql.Append("    S.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append("    S.DENPYO_KUBUN = @DENPYO_KUBUN AND ");

            Sql.Append("    S.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND ");
            Sql.Append("    S.TOKUISAKI_CD BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO ");
            #endregion

            Sql.Append(" ) AS A ");
            Sql.Append(" WHERE");
            for (int col = 1; col < fldCols; col++)
            {
                if (kinsuu_kubun == 1)
                    Sql.Append(" A.KINGAKU" + col.ToString() + " <> 0 OR ");
                else
                    Sql.Append(" A.KINGAKU" + col.ToString() + " <> 0 OR ");
            }
            Sql.Append(" A.TOKUISAKI_CD IS NULL ");

            Sql.Append(" GROUP BY");
            Sql.Append("     A.SHISHO_CD,");
            Sql.Append("     A.TOKUISAKI_CD ");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.SHISHO_CD ASC,");
            Sql.Append("     A.TOKUISAKI_CD ASC");

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                //Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                #region 明細の登録
                int fldColNo;
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    // 取得したデータをワークテーブルに更新をする
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                    Sql.Append(") ");

                    shisho_cd = Util.ToString(dr["SHISHO_CD"]);
                    tokuisaki_cd = Util.ToString(dr["TOKUISAKI_CD"]);
                    tokuisaki_nm = Util.ToString(dr["TOKUISAKI_NM"]);

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 9, this._dbSORT);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                    //                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, this.lblTitle.Text + (kinsuu_kubun == 1 ? "(金額)" : "(数量)"));
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, this.lblTitle.Text);

                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dateFrChk);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dateToChk);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, (kinsuu_kubun == 1 ? zeiKbn + "（単位：円）" : ""));
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, title[0]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, title[1]);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, title[2]);
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, title[3]);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, title[4]);
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, title[5]);
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, title[6]);
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, title[7]);
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, title[8]);

                    // 明細データを設定
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, tokuisaki_cd);
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, tokuisaki_nm);

                    kingakuSum = 0m;
                    fldColNo = 17;
                    for (int col = 1; col < fldCols; col++)
                    {
                        kingaku = Util.ToDecimal(Util.ToString(dr["KINGAKU" + col.ToString()]));
                        kingakuSum += kingaku;
                        if (kinsuu_kubun == 1)
                            tmpkingaku = Util.FormatNum(kingaku);
                        else
                            tmpkingaku = Util.FormatNum(kingaku, 1);
                        if (tmpkingaku == "0" || tmpkingaku == "0.0") tmpkingaku = "";
                        dpc.SetParam("@ITEM" + fldColNo.ToString(), SqlDbType.VarChar, 200, tmpkingaku);
                        kingakuTtl[col] += kingaku;
                        fldColNo++;
                    }
                    kingakuTtl[fldCols] += kingakuSum;

                    if (kinsuu_kubun == 1)
                        tmpkingaku = Util.FormatNum(kingakuSum);
                    else
                        tmpkingaku = Util.FormatNum(kingakuSum, 1);
                    if (tmpkingaku == "0" || tmpkingaku == "0.0") tmpkingaku = "";
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpkingaku);
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, shisho_cd);
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, dateOut);

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                    this._dbSORT++;
                }
                #endregion

                #region 最終合計出力
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 9, this._dbSORT);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, this.lblTitle.Text + (kinsuu_kubun == 1 ? "(金額)" : "(数量)"));
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dateFrChk);
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dateToChk);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, (kinsuu_kubun == 1 ? zeiKbn + "（単位：円）" : ""));
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, title[0]);
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, title[1]);
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, title[2]);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, title[3]);
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, title[4]);
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, title[5]);
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, title[6]);
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, title[7]);
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, title[8]);

                // データを設定
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "　　　合　　計");

                fldColNo = 17;
                for (int col = 1; col < fldCols; col++)
                {
                    if (kinsuu_kubun == 1)
                        tmpkingaku = Util.FormatNum(kingakuTtl[col]);
                    else
                        tmpkingaku = Util.FormatNum(kingakuTtl[col], 1);
                    if (tmpkingaku == "0" || tmpkingaku == "0.0") tmpkingaku = "";
                    dpc.SetParam("@ITEM" + fldColNo.ToString(), SqlDbType.VarChar, 200, tmpkingaku);
                    fldColNo++;
                }

                if (kinsuu_kubun == 1)
                    tmpkingaku = Util.FormatNum(kingakuTtl[fldCols]);
                else
                    tmpkingaku = Util.FormatNum(kingakuTtl[fldCols], 1);
                if (tmpkingaku == "0" || tmpkingaku == "0.0") tmpkingaku = "";
                dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tmpkingaku);
                dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, shisho_cd);
                dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, dateOut);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                #endregion
            }

            //// 印刷ワークテーブルのデータ件数を取得
            //dpc = new DbParamCollection();
            //dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            //DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
            //    "SORT",
            //    "PR_HN_TBL",
            //    "GUID = @GUID",
            //    dpc);

            bool dataFlag = true;
            //if (tmpdtPR_HN_TBL.Rows.Count > 0)
            //{
            //    dataFlag = true;
            //}
            //else
            //{
            //    dataFlag = false;
            //}

            return dataFlag;
        }
        #endregion

    }
}
