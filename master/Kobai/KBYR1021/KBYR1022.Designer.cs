﻿namespace jp.co.fsi.kb.kbyr1021
{
    partial class KBYR1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdoKojun = new System.Windows.Forms.RadioButton();
            this.rdoShojun = new System.Windows.Forms.RadioButton();
            this.rdoNashi = new System.Windows.Forms.RadioButton();
            this.lblTitleCd = new System.Windows.Forms.Label();
            this.txtTitleNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.nudTitleCd = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.btnLMove = new System.Windows.Forms.Button();
            this.btnRMove = new System.Windows.Forms.Button();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.lbxSentakuKomoku = new System.Windows.Forms.ListBox();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.lbxShukeiKubun = new System.Windows.Forms.ListBox();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTitleCd)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF1
            // 
            this.btnF1.Text = "F1";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4";
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6";
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 527);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(743, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(732, 31);
            this.lblTitle.Text = "";
            // 
            // rdoKojun
            // 
            this.rdoKojun.AutoSize = true;
            this.rdoKojun.BackColor = System.Drawing.Color.Silver;
            this.rdoKojun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKojun.ForeColor = System.Drawing.Color.Black;
            this.rdoKojun.Location = new System.Drawing.Point(219, 5);
            this.rdoKojun.Margin = new System.Windows.Forms.Padding(4);
            this.rdoKojun.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoKojun.Name = "rdoKojun";
            this.rdoKojun.Size = new System.Drawing.Size(58, 24);
            this.rdoKojun.TabIndex = 2;
            this.rdoKojun.TabStop = true;
            this.rdoKojun.Tag = "CHANGE";
            this.rdoKojun.Text = "降順";
            this.rdoKojun.UseVisualStyleBackColor = false;
            // 
            // rdoShojun
            // 
            this.rdoShojun.AutoSize = true;
            this.rdoShojun.BackColor = System.Drawing.Color.Silver;
            this.rdoShojun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShojun.ForeColor = System.Drawing.Color.Black;
            this.rdoShojun.Location = new System.Drawing.Point(141, 5);
            this.rdoShojun.Margin = new System.Windows.Forms.Padding(4);
            this.rdoShojun.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoShojun.Name = "rdoShojun";
            this.rdoShojun.Size = new System.Drawing.Size(58, 24);
            this.rdoShojun.TabIndex = 1;
            this.rdoShojun.TabStop = true;
            this.rdoShojun.Tag = "CHANGE";
            this.rdoShojun.Text = "昇順";
            this.rdoShojun.UseVisualStyleBackColor = false;
            // 
            // rdoNashi
            // 
            this.rdoNashi.AutoSize = true;
            this.rdoNashi.BackColor = System.Drawing.Color.Silver;
            this.rdoNashi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoNashi.ForeColor = System.Drawing.Color.Black;
            this.rdoNashi.Location = new System.Drawing.Point(81, 5);
            this.rdoNashi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoNashi.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoNashi.Name = "rdoNashi";
            this.rdoNashi.Size = new System.Drawing.Size(42, 24);
            this.rdoNashi.TabIndex = 0;
            this.rdoNashi.TabStop = true;
            this.rdoNashi.Tag = "CHANGE";
            this.rdoNashi.Text = "無";
            this.rdoNashi.UseVisualStyleBackColor = false;
            // 
            // lblTitleCd
            // 
            this.lblTitleCd.BackColor = System.Drawing.Color.Silver;
            this.lblTitleCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTitleCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitleCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitleCd.ForeColor = System.Drawing.Color.Black;
            this.lblTitleCd.Location = new System.Drawing.Point(0, 0);
            this.lblTitleCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitleCd.Name = "lblTitleCd";
            this.lblTitleCd.Size = new System.Drawing.Size(708, 42);
            this.lblTitleCd.TabIndex = 3;
            this.lblTitleCd.Tag = "CHANGE";
            this.lblTitleCd.Text = "グループ";
            this.lblTitleCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTitleNm
            // 
            this.txtTitleNm.AutoSizeFromLength = false;
            this.txtTitleNm.DisplayLength = null;
            this.txtTitleNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTitleNm.ForeColor = System.Drawing.Color.Black;
            this.txtTitleNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTitleNm.Location = new System.Drawing.Point(143, 8);
            this.txtTitleNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtTitleNm.MaxLength = 20;
            this.txtTitleNm.Name = "txtTitleNm";
            this.txtTitleNm.Size = new System.Drawing.Size(268, 23);
            this.txtTitleNm.TabIndex = 1;
            this.txtTitleNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitleNm_Validating);
            // 
            // nudTitleCd
            // 
            this.nudTitleCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.nudTitleCd.Location = new System.Drawing.Point(81, 8);
            this.nudTitleCd.Margin = new System.Windows.Forms.Padding(4);
            this.nudTitleCd.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nudTitleCd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTitleCd.Name = "nudTitleCd";
            this.nudTitleCd.Size = new System.Drawing.Size(60, 23);
            this.nudTitleCd.TabIndex = 2;
            this.nudTitleCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudTitleCd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTitleCd.ValueChanged += new System.EventHandler(this.nudTitleCd_ValueChanged);
            this.nudTitleCd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudTitleCd_KeyUp);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(708, 42);
            this.label1.TabIndex = 3;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "順位";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(289, 32);
            this.label2.TabIndex = 3;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "集計区分";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(278, 32);
            this.label3.TabIndex = 3;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "選択済み";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(716, 485);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.btnLMove);
            this.fsiPanel3.Controls.Add(this.btnRMove);
            this.fsiPanel3.Controls.Add(this.fsiPanel5);
            this.fsiPanel3.Controls.Add(this.fsiPanel4);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 102);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(708, 379);
            this.fsiPanel3.TabIndex = 2;
            // 
            // btnLMove
            // 
            this.btnLMove.BackColor = System.Drawing.Color.Silver;
            this.btnLMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLMove.Location = new System.Drawing.Point(344, 175);
            this.btnLMove.Margin = new System.Windows.Forms.Padding(4);
            this.btnLMove.Name = "btnLMove";
            this.btnLMove.Size = new System.Drawing.Size(59, 52);
            this.btnLMove.TabIndex = 5;
            this.btnLMove.Text = "＜";
            this.btnLMove.UseVisualStyleBackColor = false;
            this.btnLMove.Click += new System.EventHandler(this.btnLMove_Click);
            // 
            // btnRMove
            // 
            this.btnRMove.BackColor = System.Drawing.Color.Silver;
            this.btnRMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRMove.Location = new System.Drawing.Point(344, 103);
            this.btnRMove.Margin = new System.Windows.Forms.Padding(4);
            this.btnRMove.Name = "btnRMove";
            this.btnRMove.Size = new System.Drawing.Size(59, 52);
            this.btnRMove.TabIndex = 4;
            this.btnRMove.Text = "＞";
            this.btnRMove.UseVisualStyleBackColor = false;
            this.btnRMove.Click += new System.EventHandler(this.btnRMove_Click);
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lbxSentakuKomoku);
            this.fsiPanel5.Controls.Add(this.label3);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel5.Location = new System.Drawing.Point(430, 0);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(278, 379);
            this.fsiPanel5.TabIndex = 3;
            // 
            // lbxSentakuKomoku
            // 
            this.lbxSentakuKomoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxSentakuKomoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbxSentakuKomoku.FormattingEnabled = true;
            this.lbxSentakuKomoku.ItemHeight = 16;
            this.lbxSentakuKomoku.Location = new System.Drawing.Point(0, 32);
            this.lbxSentakuKomoku.Margin = new System.Windows.Forms.Padding(4);
            this.lbxSentakuKomoku.Name = "lbxSentakuKomoku";
            this.lbxSentakuKomoku.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxSentakuKomoku.Size = new System.Drawing.Size(278, 347);
            this.lbxSentakuKomoku.TabIndex = 4;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lbxShukeiKubun);
            this.fsiPanel4.Controls.Add(this.label2);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel4.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(289, 379);
            this.fsiPanel4.TabIndex = 3;
            // 
            // lbxShukeiKubun
            // 
            this.lbxShukeiKubun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxShukeiKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbxShukeiKubun.FormattingEnabled = true;
            this.lbxShukeiKubun.ItemHeight = 16;
            this.lbxShukeiKubun.Location = new System.Drawing.Point(0, 32);
            this.lbxShukeiKubun.Margin = new System.Windows.Forms.Padding(4);
            this.lbxShukeiKubun.Name = "lbxShukeiKubun";
            this.lbxShukeiKubun.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxShukeiKubun.Size = new System.Drawing.Size(289, 347);
            this.lbxShukeiKubun.TabIndex = 4;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.rdoNashi);
            this.fsiPanel2.Controls.Add(this.rdoShojun);
            this.fsiPanel2.Controls.Add(this.rdoKojun);
            this.fsiPanel2.Controls.Add(this.label1);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 53);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(708, 42);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.nudTitleCd);
            this.fsiPanel1.Controls.Add(this.txtTitleNm);
            this.fsiPanel1.Controls.Add(this.lblTitleCd);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(708, 42);
            this.fsiPanel1.TabIndex = 0;
            // 
            // KBYR1022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 664);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBYR1022";
            this.ShowFButton = true;
            this.Text = "グループ設定";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudTitleCd)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton rdoKojun;
        private System.Windows.Forms.RadioButton rdoShojun;
        private System.Windows.Forms.RadioButton rdoNashi;
        private System.Windows.Forms.Label lblTitleCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTitleNm;
        private System.Windows.Forms.NumericUpDown nudTitleCd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel5;
        private System.Windows.Forms.ListBox lbxSentakuKomoku;
        private common.FsiPanel fsiPanel4;
        private System.Windows.Forms.ListBox lbxShukeiKubun;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Button btnLMove;
        private System.Windows.Forms.Button btnRMove;
    }
}