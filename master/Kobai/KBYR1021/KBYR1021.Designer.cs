﻿namespace jp.co.fsi.kb.kbyr1021
{
    partial class KBYR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblFunanushiCdBet = new System.Windows.Forms.Label();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.rdoKin = new System.Windows.Forms.RadioButton();
            this.rdoSu = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "購買売上一覧表";
            // 
            // lblDayFr
            // 
            this.lblDayFr.AutoSize = true;
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblDayFr.Location = new System.Drawing.Point(391, 9);
            this.lblDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(24, 16);
            this.lblDayFr.TabIndex = 6;
            this.lblDayFr.Tag = "CHANGE";
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.AutoSize = true;
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblDayTo.Location = new System.Drawing.Point(699, 9);
            this.lblDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(24, 16);
            this.lblDayTo.TabIndex = 14;
            this.lblDayTo.Tag = "CHANGE";
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDayFr.Location = new System.Drawing.Point(349, 6);
            this.txtDayFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(39, 23);
            this.txtDayFr.TabIndex = 5;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.AutoSize = true;
            this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetDate.Location = new System.Drawing.Point(419, 9);
            this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(24, 16);
            this.lblCodeBetDate.TabIndex = 7;
            this.lblCodeBetDate.Tag = "CHANGE";
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtDayTo.Location = new System.Drawing.Point(657, 6);
            this.txtDayTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(39, 23);
            this.txtDayTo.TabIndex = 13;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.AutoSize = true;
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblMonthTo.Location = new System.Drawing.Point(624, 9);
            this.lblMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(24, 16);
            this.lblMonthTo.TabIndex = 12;
            this.lblMonthTo.Tag = "CHANGE";
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearTo
            // 
            this.lblYearTo.AutoSize = true;
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblYearTo.Location = new System.Drawing.Point(549, 9);
            this.lblYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(24, 16);
            this.lblYearTo.TabIndex = 10;
            this.lblYearTo.Tag = "CHANGE";
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtYearTo.Location = new System.Drawing.Point(507, 6);
            this.txtYearTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(39, 23);
            this.txtYearTo.TabIndex = 9;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtMonthTo.Location = new System.Drawing.Point(582, 6);
            this.txtMonthTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(39, 23);
            this.txtMonthTo.TabIndex = 11;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblGengoTo.Location = new System.Drawing.Point(447, 5);
            this.lblGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(55, 24);
            this.lblGengoTo.TabIndex = 8;
            this.lblGengoTo.Tag = "DISPNAME";
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.AutoSize = true;
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblMonthFr.Location = new System.Drawing.Point(316, 9);
            this.lblMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(24, 16);
            this.lblMonthFr.TabIndex = 4;
            this.lblMonthFr.Tag = "CHANGE";
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearFr
            // 
            this.lblYearFr.AutoSize = true;
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblYearFr.Location = new System.Drawing.Point(241, 9);
            this.lblYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(24, 16);
            this.lblYearFr.TabIndex = 2;
            this.lblYearFr.Tag = "CHANGE";
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtYearFr.Location = new System.Drawing.Point(199, 6);
            this.txtYearFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(39, 23);
            this.txtYearFr.TabIndex = 1;
            this.txtYearFr.Tag = "";
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtMonthFr.Location = new System.Drawing.Point(274, 6);
            this.txtMonthFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(39, 23);
            this.txtMonthFr.TabIndex = 3;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblGengoFr.Location = new System.Drawing.Point(139, 5);
            this.lblGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(55, 24);
            this.lblGengoFr.TabIndex = 0;
            this.lblGengoFr.Tag = "DISPNAME";
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiCdBet
            // 
            this.lblFunanushiCdBet.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdBet.ForeColor = System.Drawing.Color.Black;
            this.lblFunanushiCdBet.Location = new System.Drawing.Point(499, 1);
            this.lblFunanushiCdBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanushiCdBet.Name = "lblFunanushiCdBet";
            this.lblFunanushiCdBet.Size = new System.Drawing.Size(24, 32);
            this.lblFunanushiCdBet.TabIndex = 2;
            this.lblFunanushiCdBet.Tag = "CHANGE";
            this.lblFunanushiCdBet.Text = "～";
            this.lblFunanushiCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(607, 5);
            this.lblFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(272, 24);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Tag = "DISPNAME";
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(213, 5);
            this.lblFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(272, 24);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Tag = "DISPNAME";
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(532, 6);
            this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(64, 23);
            this.txtFunanushiCdTo.TabIndex = 3;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(139, 6);
            this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(64, 23);
            this.txtFunanushiCdFr.TabIndex = 0;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(139, 5);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(192, 4);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(287, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.BackColor = System.Drawing.Color.Silver;
            this.rdoZeikomi.Checked = true;
            this.rdoZeikomi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeikomi.Location = new System.Drawing.Point(8, 7);
            this.rdoZeikomi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(58, 20);
            this.rdoZeikomi.TabIndex = 0;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Tag = "CHANGE";
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = false;
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.BackColor = System.Drawing.Color.Silver;
            this.rdoZeinuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeinuki.Location = new System.Drawing.Point(93, 7);
            this.rdoZeinuki.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(58, 20);
            this.rdoZeinuki.TabIndex = 1;
            this.rdoZeinuki.Tag = "CHANGE";
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = false;
            // 
            // rdoKin
            // 
            this.rdoKin.AutoSize = true;
            this.rdoKin.BackColor = System.Drawing.Color.Silver;
            this.rdoKin.Checked = true;
            this.rdoKin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKin.Location = new System.Drawing.Point(4, 7);
            this.rdoKin.Margin = new System.Windows.Forms.Padding(4);
            this.rdoKin.Name = "rdoKin";
            this.rdoKin.Size = new System.Drawing.Size(58, 20);
            this.rdoKin.TabIndex = 0;
            this.rdoKin.TabStop = true;
            this.rdoKin.Tag = "CHANGE";
            this.rdoKin.Text = "金額";
            this.rdoKin.UseVisualStyleBackColor = false;
            // 
            // rdoSu
            // 
            this.rdoSu.AutoSize = true;
            this.rdoSu.BackColor = System.Drawing.Color.Silver;
            this.rdoSu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSu.Location = new System.Drawing.Point(89, 7);
            this.rdoSu.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSu.Name = "rdoSu";
            this.rdoSu.Size = new System.Drawing.Size(58, 20);
            this.rdoSu.TabIndex = 1;
            this.rdoSu.Tag = "CHANGE";
            this.rdoSu.Text = "数量";
            this.rdoSu.UseVisualStyleBackColor = false;
            this.rdoSu.CheckedChanged += new System.EventHandler(this.rdoSu_CheckedChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1074, 33);
            this.label1.TabIndex = 902;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "支所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1074, 34);
            this.label2.TabIndex = 902;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "日付範囲";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1074, 35);
            this.label3.TabIndex = 902;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "船主CD範囲";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.4F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1084, 130);
            this.fsiTableLayoutPanel1.TabIndex = 903;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtFunanushiCdFr);
            this.fsiPanel3.Controls.Add(this.txtFunanushiCdTo);
            this.fsiPanel3.Controls.Add(this.lblFunanushiCdBet);
            this.fsiPanel3.Controls.Add(this.lblFunanushiCdFr);
            this.fsiPanel3.Controls.Add(this.lblFunanushiCdTo);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 90);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(1074, 35);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblDayFr);
            this.fsiPanel2.Controls.Add(this.lblDayTo);
            this.fsiPanel2.Controls.Add(this.fsiPanel4);
            this.fsiPanel2.Controls.Add(this.fsiPanel5);
            this.fsiPanel2.Controls.Add(this.lblGengoFr);
            this.fsiPanel2.Controls.Add(this.txtDayFr);
            this.fsiPanel2.Controls.Add(this.txtMonthFr);
            this.fsiPanel2.Controls.Add(this.lblCodeBetDate);
            this.fsiPanel2.Controls.Add(this.txtYearFr);
            this.fsiPanel2.Controls.Add(this.txtDayTo);
            this.fsiPanel2.Controls.Add(this.lblYearFr);
            this.fsiPanel2.Controls.Add(this.lblMonthTo);
            this.fsiPanel2.Controls.Add(this.lblMonthFr);
            this.fsiPanel2.Controls.Add(this.lblYearTo);
            this.fsiPanel2.Controls.Add(this.lblGengoTo);
            this.fsiPanel2.Controls.Add(this.txtYearTo);
            this.fsiPanel2.Controls.Add(this.txtMonthTo);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 47);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(1074, 34);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.BackColor = System.Drawing.Color.Silver;
            this.fsiPanel4.Controls.Add(this.rdoKin);
            this.fsiPanel4.Controls.Add(this.rdoSu);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel4.Location = new System.Drawing.Point(746, 0);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(164, 34);
            this.fsiPanel4.TabIndex = 15;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.BackColor = System.Drawing.Color.Silver;
            this.fsiPanel5.Controls.Add(this.rdoZeikomi);
            this.fsiPanel5.Controls.Add(this.rdoZeinuki);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel5.Location = new System.Drawing.Point(910, 0);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(164, 34);
            this.fsiPanel5.TabIndex = 16;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(1074, 33);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // KBYR1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBYR1021";
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private common.controls.FsiTextBox txtYearTo;
        private common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblDayTo;
        private common.controls.FsiTextBox txtDayFr;
        private common.controls.FsiTextBox txtDayTo;
        private System.Windows.Forms.Label lblFunanushiCdBet;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private System.Windows.Forms.RadioButton rdoKin;
        private System.Windows.Forms.RadioButton rdoSu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel1;
    }
}
