﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbyr1021
{
    /// <summary>
    /// 項目設定(KBYR1022)
    /// </summary>
    public partial class KBYR1022 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// KBYR1022(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        String _pDENPYO_KUBUN;
        String _pKINSUU_KUBUN;

        private int _shishoCode = 0;                    // 支所コード

        private int _shohin_kubun = 2;                  // 商品区分（設定より）
        #endregion

        #region 定数
        /// <summary>
        /// データ取得用
        /// </summary>
        private const int POSITION_START_NO = 1;
        private const int POSITION_END_NO = 7;
        #endregion

        #region プロパティ
        /// <summary>
        /// タイトルを格納する用データテーブル
        /// </summary>
        private DataTable _dtTitle = new DataTable();
        public DataTable Title
        {
            get
            {
                return this._dtTitle;
            }
        }

        /// <summary>
        /// 商品区分を格納する用データテーブル
        /// </summary>
        private DataTable _dtShohinKubun = new DataTable();
        public DataTable ShohinKubun
        {
            get
            {
                return this._dtShohinKubun;
            }
        }

        /// <summary>
        /// 選択項目を格納する用データテーブル
        /// </summary>
        private DataTable _dtSentakuKomoku = new DataTable();
        public DataTable SentakuKomoku
        {
            get
            {
                return this._dtSentakuKomoku;
            }
        }

        /// <summary>
        /// 選択候補を格納する用データテーブル
        /// </summary>
        private DataTable _dtSentakuKoho = new DataTable();
        public DataTable SentakuKoho
        {
            get
            {
                return this._dtSentakuKoho;
            }
        }

        /// <summary>
        /// グループNoを格納する用変数
        /// </summary>
        private Decimal _dtPositionNo = new Decimal();
        public Decimal PositionNo
        {
            get
            {
                return this._dtPositionNo;
            }
        }

        public int ShishoCode
        {
            set
            {
                this._shishoCode = value;
            }
        }
        public int ShohinKbn
        {
            set
            {
                this._shohin_kubun = value;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBYR1022(String DENPYO_KUBUN, String KINSUU_KUBUN)
        {
            this._pDENPYO_KUBUN = DENPYO_KUBUN;
            this._pKINSUU_KUBUN = KINSUU_KUBUN;

            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルの表示非表示を設定
            this.lblTitle.Visible = false;

            // Escapeのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 画面の初期表示時
            DispDataSetTitle();
            DispDataSetSentakuKomoku();
            DispDataSetShohinKubun();
            DispDataSetSentakuKoho();

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KUBUN_SHUBETSU", SqlDbType.Decimal, 6, this._shohin_kubun);
            dpc.SetParam("@SHOHIN_KUBUN", SqlDbType.Decimal, 6, 0);
            DataTable dtCheck = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_F_SHOHIN_KUBUN",
                "KUBUN_SHUBETSU = @KUBUN_SHUBETSU AND SHOHIN_KUBUN = @SHOHIN_KUBUN ",
                dpc);
            if (dtCheck.Rows.Count == 0)
                label2.Text = "商品区分" + this._shohin_kubun.ToString() + "-未選択";
            else
                label2.Text = Util.ToString(dtCheck.Rows[0]["SHOHIN_KUBUN_NM"]) + "-未選択";
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // 選択項目を登録する
            this.InsertTB_HN_KOBAI_LIST_SETTEI();

            // 不要なデータの削除
            Delete_Trush();

            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// タイトル名の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTitleNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTitleNm())
            {
                e.Cancel = true;
                this.txtTitleNm.SelectAll();
            }
        }

        /// <summary>
        /// タイトルが変更された場合の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nudTitleCd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // 選択項目をセットする
                this.SetSentakuKomoku();
                this.SetTitle();
                // 商品区分をセットする
                this.SetShohinKubun();

                // タイトルNoを保持
                this._dtPositionNo = (Decimal)this.nudTitleCd.Value;
            }
        }

        /// <summary>
        /// タイトルが変更された場合の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nudTitleCd_ValueChanged(object sender, EventArgs e)
        {
            // 選択項目をセットする
            this.SetSentakuKomoku();
            this.SetTitle();
            // 商品区分をセットする
            this.SetShohinKubun();

            // タイトルNoを保持
            this._dtPositionNo = (Decimal)this.nudTitleCd.Value;
        }

        /// <summary>
        /// 選択した項目の移動処理(左→右)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRMove_Click(object sender, System.EventArgs e)
        {
            int[] selectNum = new int[this.lbxShukeiKubun.SelectedIndices.Count];

            for (int i = 0; i < this.lbxShukeiKubun.SelectedIndices.Count; i++)
            {
                selectNum[i] = this.lbxShukeiKubun.SelectedIndices[i];
            }

            // 選択した項目をセット
            string ShukeiKubunNm;
            string filter;
            DataRow[] dataRows;
            DataTable kubunList = new DataTable();
            kubunList.Columns.Add("num", Type.GetType("System.Int32"));
            kubunList.Columns.Add("cd", Type.GetType("System.Int32"));
            kubunList.Columns.Add("nm", Type.GetType("System.String"));
            DataRow row;
            for (int i = 0; i < selectNum.Length; i++)
            {
                // 商品区分名称を取得
                ShukeiKubunNm = this.lbxShukeiKubun.Items[selectNum[i]].ToString();
                // 商品区分情報を取得
                filter = " nm = \'" + ShukeiKubunNm + "\'";
                dataRows = this.ShohinKubun.Select(filter);
                row = kubunList.NewRow();
                row["num"] = i;
                row["cd"] = Util.ToInt(dataRows[0]["cd"]);
                row["nm"] = Util.ToString(dataRows[0]["nm"]);
                kubunList.Rows.Add(row);
            }

            // 選択項目の情報を取得
            DataRow[] dataRowsSentaku = this.SentakuKomoku.Select();

            // 選択した項目の追加
            dataRows = kubunList.Select();
            foreach (DataRow dr in dataRows)
            {
                this.lbxSentakuKomoku.Items.Add(this.lbxShukeiKubun.Items[selectNum[Util.ToInt(dr["num"])]]);

                // 選択項目に情報をセット
                row = SentakuKomoku.NewRow();
                row["GRP_CD"] = this.PositionNo;
                row["cd"] = Util.ToInt(dr["cd"]);
                row["nm"] = Util.ToString(dr["nm"]);
                SentakuKomoku.Rows.Add(row);

                // 商品区分の情報を取得
                filter = " nm = \'" + Util.ToString(dr["nm"]) + "\' and cd = \'" + Util.ToString(dr["cd"]) + "\'";
                DataRow[] dataRowsShukei = this.ShohinKubun.Select(filter);

                // 商品区分から削除する
                foreach (DataRow drSK in dataRowsShukei)
                {
                    drSK.Delete();
                }
            }

            int l = 1;
            // 選択した項目の削除
            for (int i = 0; i < selectNum.Length; i++)
            {
                if (i == 0)
                {
                    this.lbxShukeiKubun.Items.Remove(this.lbxShukeiKubun.Items[selectNum[i]]);
                }
                else
                {
                    this.lbxShukeiKubun.Items.Remove(this.lbxShukeiKubun.Items[selectNum[i] - l]);
                    l++;
                }
            }
        }

        /// <summary>
        /// 選択した項目の移動処理(右→左)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLMove_Click(object sender, System.EventArgs e)
        {
            {
                int[] selectNum = new int[this.lbxSentakuKomoku.SelectedIndices.Count];
                for (int i = 0; i < this.lbxSentakuKomoku.SelectedIndices.Count; i++)
                {
                    selectNum[i] = this.lbxSentakuKomoku.SelectedIndices[i];
                }

                // 選択した項目の追加
                string shohinKubunNm;
                string filter;
                DataRow[] dataRows;
                DataRow row;
                for (int i = 0; i < selectNum.Length; i++)
                {
                    this.lbxShukeiKubun.Items.Add(this.lbxSentakuKomoku.Items[selectNum[i]]);

                    if (this.PositionNo != 1)
                    {
                        // 商品区分名称を取得
                        shohinKubunNm = this.lbxSentakuKomoku.Items[selectNum[i]].ToString();
                        // 選択項目の情報を取得
                        filter = "GRP_CD = " + this.PositionNo + " and nm = \'" + shohinKubunNm + "\'";
                        dataRows = this.SentakuKomoku.Select(filter);

                        // 商品区分に情報をセット
                        row = ShohinKubun.NewRow();
                        row["GRP_CD"] = this.PositionNo;
                        row["cd"] = Util.ToInt(dataRows[0]["cd"]);
                        row["nm"] = shohinKubunNm;
                        this.ShohinKubun.Rows.Add(row);

                        // 選択項目から削除する
                        foreach (DataRow dr in dataRows)
                        {
                            dr.Delete();
                        }
                    }
                }

                int l = 1;
                // 選択した項目の削除
                for (int i = 0; i < selectNum.Length; i++)
                {
                    if (i == 0)
                    {
                        this.lbxSentakuKomoku.Items.Remove(this.lbxSentakuKomoku.Items[selectNum[i]]);
                    }
                    else
                    {
                        this.lbxSentakuKomoku.Items.Remove(this.lbxSentakuKomoku.Items[selectNum[i] - l]);
                        l++;
                    }
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// タイトル名の入力チェック
        /// </summary>
        private bool IsValidTitleNm()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtTitleNm.Text, this.txtTitleNm.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 画面から取得したデータを格納(タイトル)
        /// </summary>
        private void SetTitle()
        {
            // タイトルをDataTableに登録する
            int num = (int)this.PositionNo - 1;
            this.Title.Rows[num]["GRP_NM"] = this.txtTitleNm.Text;

            // タイトルを表示する
            String filter = "GRP_CD = " + this.nudTitleCd.Value;
            DataRow[] dataRows = this.Title.Select(filter);
            String titleNm = Util.ToString(dataRows[0]["GRP_NM"]);
            this.txtTitleNm.Text = titleNm;
        }

        /// <summary>
        /// 画面から取得したデータを格納(商品区分)
        /// </summary>
        private void SetShohinKubun()
        {
            // 商品区分を表示する
            this.lbxShukeiKubun.Items.Clear();
            string filter = "";
            DataRow[] dataRows = this.ShohinKubun.Select(filter);
            foreach (DataRow dr in dataRows)
            {
                // 選択項目にセットする
                this.lbxShukeiKubun.Items.Add(Util.ToString(dr["nm"]));
            }
        }

        /// <summary>
        /// 画面から取得したデータを格納(選択項目)
        /// </summary>
        private void SetSentakuKomoku()
        {
            // 選択項目をDBに反映する
            this.InsertTB_HN_KOBAI_LIST_SETTEI();

            // 選択項目を表示する
            this.lbxSentakuKomoku.Items.Clear();
            string filter = "GRP_CD = " + (Decimal)this.nudTitleCd.Value;
            DataRow[] dataRows = this.SentakuKomoku.Select(filter);
            foreach (DataRow dr in dataRows)
            {
                // 選択項目にセットする
                if (Util.ToInt(dr["cd"]) != 0)
                {
                    this.lbxSentakuKomoku.Items.Add(Util.ToString(dr["nm"]));
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(タイトル)
        /// 画面に反映(タイトル)
        /// </summary>
        private void DispDataSetTitle()
        {
            // 対象データテーブルにカラムを2列ずつ定義
            this.Title.Columns.Add("GRP_CD", Type.GetType("System.Int32"));
            this.Title.Columns.Add("GRP_NM", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;
            string title = "";
            string titleNm = "";

            // タイトルをデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= POSITION_END_NO; cnt++)
            {
                dtResult = this.GetTB_HN_KOBAI_LIST_SETTEI(cnt);
                title = "";
                if (dtResult.Rows.Count > 0)
                {
                    title = dtResult.Rows[0]["GRP_NM"].ToString();
                    // 1件目のタイトルをセット
                    if (cnt == POSITION_START_NO)
                    {
                        titleNm = dtResult.Rows[0]["GRP_NM"].ToString();
                    }
                }

                row = this.Title.NewRow();
                row["GRP_CD"] = cnt;
                row["GRP_NM"] = title;
                this.Title.Rows.Add(row);
            }

            // タイトルをセット
            this.txtTitleNm.Text = titleNm;
            _dtPositionNo = (Decimal)POSITION_START_NO;
        }

        /// <summary>
        /// DBから取得したデータを格納(商品区分)
        /// 画面に反映(商品区分)
        /// </summary>
        private void DispDataSetShohinKubun()
        {
            // 対象データテーブルにカラムを定義
            this.ShohinKubun.Columns.Add("GRP_CD", Type.GetType("System.Int32"));
            this.ShohinKubun.Columns.Add("cd", Type.GetType("System.Int32"));
            this.ShohinKubun.Columns.Add("nm", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;

            int cnt = 1;
            dtResult = this.GetTB_HN_F_SHOHIN_KUBUN(cnt, true);
            foreach (DataRow dr in dtResult.Rows)
            {
                row = this.ShohinKubun.NewRow();
                row["GRP_CD"] = cnt;
                row["cd"] = Util.ToInt(dr["SHOHIN_KUBUN"]);
                row["nm"] = Util.ToString(dr["SHOHIN_KUBUN_NM"]);
                this.ShohinKubun.Rows.Add(row);

                // 1件目のデータをセット
                if (cnt == POSITION_START_NO)
                {
                    // 商品区分にセットする
                    this.lbxShukeiKubun.Items.Add(Util.ToString(dr["SHOHIN_KUBUN_NM"]));
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(選択項目)
        /// 画面に反映(選択項目)
        /// </summary>
        private void DispDataSetSentakuKomoku()
        {
            // 対象データテーブルにカラムを定義
            this.SentakuKomoku.Columns.Add("GRP_CD", Type.GetType("System.Int32"));
            this.SentakuKomoku.Columns.Add("cd", Type.GetType("System.Int32"));
            this.SentakuKomoku.Columns.Add("nm", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;

            // 項目名をデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= POSITION_END_NO; cnt++)
            {
                dtResult = this.GetTB_HN_KOBAI_LIST_SETTEI_MEISAI(cnt);
                foreach (DataRow dr in dtResult.Rows)
                {
                    row = this.SentakuKomoku.NewRow();
                    row["GRP_CD"] = cnt;
                    row["cd"] = Util.ToInt(dr["SHOHIN_KUBUN"]);
                    row["nm"] = Util.ToString(dr["SHOHIN_KUBUN_NM"]);
                    this.SentakuKomoku.Rows.Add(row);

                    // 1件目のデータをセット
                    if (cnt == POSITION_START_NO)
                    {
                        // 選択項目にセットする
                        if (Util.ToInt(dr["SHOHIN_KUBUN"]) != 0)
                        {
                            this.lbxSentakuKomoku.Items.Add(Util.ToString(dr["SHOHIN_KUBUN_NM"]));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(選択項目候補)
        /// </summary>
        private void DispDataSetSentakuKoho()
        {
            // 対象データテーブルにカラムを定義
            this.SentakuKoho.Columns.Add("GRP_CD", Type.GetType("System.Int32"));
            this.SentakuKoho.Columns.Add("cd", Type.GetType("System.Int32"));
            this.SentakuKoho.Columns.Add("nm", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;

            // 項目名をデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= POSITION_END_NO; cnt++)
            {
                dtResult = this.GetTB_HN_F_SHOHIN_KUBUN(cnt, false);
                foreach (DataRow dr in dtResult.Rows)
                {
                    row = this.SentakuKoho.NewRow();
                    row["GRP_CD"] = cnt;
                    row["cd"] = Util.ToInt(dr["SHOHIN_KUBUN"]);
                    row["nm"] = Util.ToString(dr["SHOHIN_KUBUN_NM"]);
                    this.SentakuKoho.Rows.Add(row);
                }
            }
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetTB_HN_KOBAI_LIST_SETTEI(int cnt)
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 6, this._shishoCode);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(this._pDENPYO_KUBUN));
            dpc.SetParam("@KINSUU_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(this._pKINSUU_KUBUN));
            dpc.SetParam("@GRP_CD", SqlDbType.Decimal, 2, cnt);

            Sql.Append("SELECT DISTINCT ");
            Sql.Append(" GRP_CD,");
            Sql.Append(" GRP_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_KOBAI_LIST_SETTEI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND ");
            Sql.Append(" KINSUU_KUBUN = @KINSUU_KUBUN AND ");
            Sql.Append(" GRP_CD = @GRP_CD ");
            Sql.Append("ORDER BY");
            Sql.Append(" GRP_CD");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetTB_HN_KOBAI_LIST_SETTEI_MEISAI(int cnt)
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(this._pDENPYO_KUBUN));
            dpc.SetParam("@KINSUU_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(this._pKINSUU_KUBUN));
            dpc.SetParam("@GRP_CD", SqlDbType.Decimal, 2, cnt);
            dpc.SetParam("@KUBUN_SHUBETSU", SqlDbType.Decimal, 2, this._shohin_kubun);

            Sql.Append("SELECT");
            Sql.Append(" A.GRP_CD,");
            Sql.Append(" A.GRP_NM,");
            Sql.Append(" A.SHOHIN_KUBUN,");
            Sql.Append(" B.SHOHIN_KUBUN_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_KOBAI_LIST_SETTEI AS A ");

            Sql.Append(" LEFT OUTER JOIN TB_HN_F_SHOHIN_KUBUN AS B ");
            Sql.Append("     ON A.SHOHIN_KUBUN = B.SHOHIN_KUBUN ");
            Sql.Append("    AND B.KUBUN_SHUBETSU = @KUBUN_SHUBETSU ");

            Sql.Append("WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" A.DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" A.KINSUU_KUBUN = @KINSUU_KUBUN AND");
            Sql.Append(" A.GRP_CD = @GRP_CD ");
            Sql.Append("ORDER BY");
            Sql.Append(" A.SHOHIN_KUBUN");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetTB_HN_F_SHOHIN_KUBUN(int cnt, bool flg)
        {
            String notCd = "";

            // 商品区分表示用のデータを取得する場合
            if (flg)
            {
                // 全て対象
                String filter = "";
                DataRow[] dataRowsSentakuKomoku = this.SentakuKomoku.Select(filter);
                ArrayList cdList = new ArrayList();
                foreach (DataRow dr in dataRowsSentakuKomoku)
                {
                    // 選択項目にセットする
                    cdList.Add(Util.ToString(dr.ItemArray[1].ToString()));
                }
                string[] cds = (string[])cdList.ToArray(typeof(string));
                notCd = String.Join(",", cds);
            }

            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KUBUN_SHUBETSU", SqlDbType.Decimal, 4, this._shohin_kubun);

            Sql.Append("SELECT");
            Sql.Append(" SHOHIN_KUBUN,");
            Sql.Append(" SHOHIN_KUBUN_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_F_SHOHIN_KUBUN ");
            Sql.Append("WHERE");
            Sql.Append(" KUBUN_SHUBETSU = @KUBUN_SHUBETSU ");
            if (notCd != "")
            {
                Sql.Append(" AND SHOHIN_KUBUN NOT IN (" + notCd + ") ");
            }
            Sql.Append("ORDER BY");
            Sql.Append(" SHOHIN_KUBUN");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを登録(選択項目)
        /// </summary>
        private void InsertTB_HN_KOBAI_LIST_SETTEI()
        {
            // 削除
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // TB_集計表設定明細削除
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(this._pDENPYO_KUBUN));
                whereParam.SetParam("@KINSUU_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(this._pKINSUU_KUBUN));
                whereParam.SetParam("@GRP_CD", SqlDbType.Decimal, 2, this.PositionNo);
                this.Dba.Delete("TB_HN_KOBAI_LIST_SETTEI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND DENPYO_KUBUN = @DENPYO_KUBUN AND KINSUU_KUBUN = @KINSUU_KUBUN AND GRP_CD = @GRP_CD",
                    whereParam);

                // TB_集計表設定明細登録
                DbParamCollection updParam;
                String SentakuKomokuNm;
                int titleCd = 0;

                for (int cnt = 0; cnt < this.lbxSentakuKomoku.Items.Count; cnt++)
                {
                    SentakuKomokuNm = this.lbxSentakuKomoku.Items[cnt].ToString();

                    String filter = "nm = \'" + SentakuKomokuNm + "\'";
                    DataRow[] dataRowsTitle = SentakuKoho.Select(filter);
                    if (dataRowsTitle.Length > 0)
                    {
                        titleCd = Util.ToInt(dataRowsTitle[0]["cd"]);
                    }

                    updParam = new DbParamCollection();

                    updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
                    updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(this._pDENPYO_KUBUN));
                    updParam.SetParam("@KINSUU_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(this._pKINSUU_KUBUN));
                    updParam.SetParam("@GRP_CD", SqlDbType.Decimal, 2, this.PositionNo);
                    updParam.SetParam("@GRP_NM", SqlDbType.VarChar, 20, this.txtTitleNm.Text);
                    updParam.SetParam("@SHOHIN_KUBUN", SqlDbType.Decimal, 2, titleCd);
                    updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

                    this.Dba.Insert("TB_HN_KOBAI_LIST_SETTEI", updParam);
                    try
                    {
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// ゴミの削除
        /// </summary>
        private void Delete_Trush()
        {
        }
        #endregion
    }
}
