﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.kb.kbcm1021
{
    /// <summary>
    /// KBCM1021R の帳票
    /// </summary>
    public partial class KBCM1021R : BaseReport
    {

        public KBCM1021R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            //西暦から和暦に変換
            CultureInfo culture = new CultureInfo("ja-JP", true);
            culture.DateTimeFormat.Calendar = new JapaneseCalendar();
            txtToday.Text = DateTime.Now.ToString("ggyy年M月d日", culture);

        }
    }
}
