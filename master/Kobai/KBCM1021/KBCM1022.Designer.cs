﻿namespace jp.co.fsi.kb.kbcm1021
{
    partial class KBCM1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblShohinCd = new System.Windows.Forms.Label();
			this.txtShohinCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinNm = new System.Windows.Forms.Label();
			this.txtShohinNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinKanaNm = new System.Windows.Forms.Label();
			this.txtShohinKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinKikaku = new System.Windows.Forms.Label();
			this.txtShohinKikaku = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTanaban = new System.Windows.Forms.Label();
			this.txtTanaban = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblZaikoKanriKbn = new System.Windows.Forms.Label();
			this.txtZaikoKanriKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblUriageShiwakeCd = new System.Windows.Forms.Label();
			this.txtUriageShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiireShiwakeCd = new System.Windows.Forms.Label();
			this.txtShiireShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblIrisu = new System.Windows.Forms.Label();
			this.txtIrisu = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTani = new System.Windows.Forms.Label();
			this.txtTani = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblZaikoKanriKbnResults = new System.Windows.Forms.Label();
			this.lblUriageShiwakeCdResults = new System.Windows.Forms.Label();
			this.lblShiireShiwakeCdResults = new System.Windows.Forms.Label();
			this.txtShiireTanka = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiireTanka = new System.Windows.Forms.Label();
			this.txtOroshiTanka = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblOroshiTanka = new System.Windows.Forms.Label();
			this.txtKouriTanka = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKouriTanka = new System.Windows.Forms.Label();
			this.lblShohinKbn1Results = new System.Windows.Forms.Label();
			this.txtShohinKbn1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinKbn1 = new System.Windows.Forms.Label();
			this.lblShohinKbn2Results = new System.Windows.Forms.Label();
			this.txtShohinKbn2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinKbn2 = new System.Windows.Forms.Label();
			this.lblShohinKbn3Results = new System.Windows.Forms.Label();
			this.txtShohinKbn3 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinKbn3 = new System.Windows.Forms.Label();
			this.lblShohinKbn4Results = new System.Windows.Forms.Label();
			this.txtShohinKbn4 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinKbn4 = new System.Windows.Forms.Label();
			this.txtBarcodeCase = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblBarcodeCase = new System.Windows.Forms.Label();
			this.txtBarcodeBara = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblBarcodeBara = new System.Windows.Forms.Label();
			this.txtTekiseisu = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTekiseisu = new System.Windows.Forms.Label();
			this.txtShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShishoNm = new System.Windows.Forms.Label();
			this.lblShisho = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.txtShohizeiKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohiZeiKbn = new System.Windows.Forms.Label();
			this.lblChushiKbnNm = new System.Windows.Forms.Label();
			this.txtChushiKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblChuchiKbn = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel12.SuspendLayout();
			this.fsiPanel11.SuspendLayout();
			this.fsiPanel10.SuspendLayout();
			this.fsiPanel9.SuspendLayout();
			this.fsiPanel8.SuspendLayout();
			this.fsiPanel7.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 477);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1094, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1084, 41);
			this.lblTitle.Text = "";
			// 
			// lblShohinCd
			// 
			this.lblShohinCd.BackColor = System.Drawing.Color.Silver;
			this.lblShohinCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohinCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinCd.Location = new System.Drawing.Point(0, 0);
			this.lblShohinCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinCd.Name = "lblShohinCd";
			this.lblShohinCd.Size = new System.Drawing.Size(1063, 31);
			this.lblShohinCd.TabIndex = 4;
			this.lblShohinCd.Tag = "CHANGE";
			this.lblShohinCd.Text = "商 品 コ ー ド";
			this.lblShohinCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinCd
			// 
			this.txtShohinCd.AllowDrop = true;
			this.txtShohinCd.AutoSizeFromLength = false;
			this.txtShohinCd.DisplayLength = null;
			this.txtShohinCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinCd.Location = new System.Drawing.Point(138, 4);
			this.txtShohinCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinCd.MaxLength = 13;
			this.txtShohinCd.Name = "txtShohinCd";
			this.txtShohinCd.Size = new System.Drawing.Size(172, 23);
			this.txtShohinCd.TabIndex = 2;
			this.txtShohinCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCd_Validating);
			// 
			// lblShohinNm
			// 
			this.lblShohinNm.BackColor = System.Drawing.Color.Silver;
			this.lblShohinNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohinNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinNm.Location = new System.Drawing.Point(0, 0);
			this.lblShohinNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinNm.Name = "lblShohinNm";
			this.lblShohinNm.Size = new System.Drawing.Size(1063, 31);
			this.lblShohinNm.TabIndex = 6;
			this.lblShohinNm.Tag = "CHANGE";
			this.lblShohinNm.Text = "商    品    名";
			this.lblShohinNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinNm
			// 
			this.txtShohinNm.AutoSizeFromLength = false;
			this.txtShohinNm.DisplayLength = null;
			this.txtShohinNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtShohinNm.Location = new System.Drawing.Point(138, 4);
			this.txtShohinNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinNm.MaxLength = 40;
			this.txtShohinNm.Name = "txtShohinNm";
			this.txtShohinNm.Size = new System.Drawing.Size(367, 23);
			this.txtShohinNm.TabIndex = 3;
			this.txtShohinNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinNm_Validating);
			// 
			// lblShohinKanaNm
			// 
			this.lblShohinKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblShohinKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohinKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblShohinKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKanaNm.Name = "lblShohinKanaNm";
			this.lblShohinKanaNm.Size = new System.Drawing.Size(1063, 31);
			this.lblShohinKanaNm.TabIndex = 8;
			this.lblShohinKanaNm.Tag = "CHANGE";
			this.lblShohinKanaNm.Text = "商 品 カ ナ 名";
			this.lblShohinKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinKanaNm
			// 
			this.txtShohinKanaNm.AllowDrop = true;
			this.txtShohinKanaNm.AutoSizeFromLength = false;
			this.txtShohinKanaNm.DisplayLength = null;
			this.txtShohinKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtShohinKanaNm.Location = new System.Drawing.Point(138, 4);
			this.txtShohinKanaNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinKanaNm.MaxLength = 15;
			this.txtShohinKanaNm.Name = "txtShohinKanaNm";
			this.txtShohinKanaNm.Size = new System.Drawing.Size(211, 23);
			this.txtShohinKanaNm.TabIndex = 4;
			this.txtShohinKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKanaNm_Validating);
			// 
			// lblShohinKikaku
			// 
			this.lblShohinKikaku.BackColor = System.Drawing.Color.Silver;
			this.lblShohinKikaku.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohinKikaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKikaku.Location = new System.Drawing.Point(0, 0);
			this.lblShohinKikaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKikaku.Name = "lblShohinKikaku";
			this.lblShohinKikaku.Size = new System.Drawing.Size(1063, 31);
			this.lblShohinKikaku.TabIndex = 10;
			this.lblShohinKikaku.Tag = "CHANGE";
			this.lblShohinKikaku.Text = "商  品  規  格";
			this.lblShohinKikaku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinKikaku
			// 
			this.txtShohinKikaku.AllowDrop = true;
			this.txtShohinKikaku.AutoSizeFromLength = false;
			this.txtShohinKikaku.DisplayLength = null;
			this.txtShohinKikaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinKikaku.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtShohinKikaku.Location = new System.Drawing.Point(138, 4);
			this.txtShohinKikaku.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinKikaku.MaxLength = 30;
			this.txtShohinKikaku.Name = "txtShohinKikaku";
			this.txtShohinKikaku.Size = new System.Drawing.Size(367, 23);
			this.txtShohinKikaku.TabIndex = 5;
			this.txtShohinKikaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKikaku_Validating);
			// 
			// lblTanaban
			// 
			this.lblTanaban.BackColor = System.Drawing.Color.Silver;
			this.lblTanaban.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTanaban.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTanaban.Location = new System.Drawing.Point(0, 0);
			this.lblTanaban.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTanaban.Name = "lblTanaban";
			this.lblTanaban.Size = new System.Drawing.Size(1063, 31);
			this.lblTanaban.TabIndex = 12;
			this.lblTanaban.Tag = "CHANGE";
			this.lblTanaban.Text = "棚          番";
			this.lblTanaban.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTanaban
			// 
			this.txtTanaban.AllowDrop = true;
			this.txtTanaban.AutoSizeFromLength = false;
			this.txtTanaban.DisplayLength = null;
			this.txtTanaban.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTanaban.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTanaban.Location = new System.Drawing.Point(138, 4);
			this.txtTanaban.Margin = new System.Windows.Forms.Padding(4);
			this.txtTanaban.MaxLength = 6;
			this.txtTanaban.Name = "txtTanaban";
			this.txtTanaban.Size = new System.Drawing.Size(77, 23);
			this.txtTanaban.TabIndex = 6;
			this.txtTanaban.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTanaban.Validating += new System.ComponentModel.CancelEventHandler(this.txtTanaban_Validating);
			// 
			// lblZaikoKanriKbn
			// 
			this.lblZaikoKanriKbn.BackColor = System.Drawing.Color.Silver;
			this.lblZaikoKanriKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblZaikoKanriKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblZaikoKanriKbn.Location = new System.Drawing.Point(0, 0);
			this.lblZaikoKanriKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZaikoKanriKbn.Name = "lblZaikoKanriKbn";
			this.lblZaikoKanriKbn.Size = new System.Drawing.Size(1063, 31);
			this.lblZaikoKanriKbn.TabIndex = 14;
			this.lblZaikoKanriKbn.Tag = "CHANGE";
			this.lblZaikoKanriKbn.Text = "在庫管理区分";
			this.lblZaikoKanriKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtZaikoKanriKbn
			// 
			this.txtZaikoKanriKbn.AllowDrop = true;
			this.txtZaikoKanriKbn.AutoSizeFromLength = false;
			this.txtZaikoKanriKbn.DisplayLength = null;
			this.txtZaikoKanriKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtZaikoKanriKbn.Location = new System.Drawing.Point(131, 4);
			this.txtZaikoKanriKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtZaikoKanriKbn.MaxLength = 1;
			this.txtZaikoKanriKbn.Name = "txtZaikoKanriKbn";
			this.txtZaikoKanriKbn.Size = new System.Drawing.Size(51, 23);
			this.txtZaikoKanriKbn.TabIndex = 7;
			this.txtZaikoKanriKbn.Text = "0";
			this.txtZaikoKanriKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtZaikoKanriKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtZaikoKanriKbn_Validating);
			// 
			// lblUriageShiwakeCd
			// 
			this.lblUriageShiwakeCd.BackColor = System.Drawing.Color.Silver;
			this.lblUriageShiwakeCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblUriageShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblUriageShiwakeCd.Location = new System.Drawing.Point(0, 0);
			this.lblUriageShiwakeCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblUriageShiwakeCd.Name = "lblUriageShiwakeCd";
			this.lblUriageShiwakeCd.Size = new System.Drawing.Size(1063, 31);
			this.lblUriageShiwakeCd.TabIndex = 17;
			this.lblUriageShiwakeCd.Tag = "CHANGE";
			this.lblUriageShiwakeCd.Text = "売上仕訳コード";
			this.lblUriageShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtUriageShiwakeCd
			// 
			this.txtUriageShiwakeCd.AllowDrop = true;
			this.txtUriageShiwakeCd.AutoSizeFromLength = false;
			this.txtUriageShiwakeCd.DisplayLength = null;
			this.txtUriageShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtUriageShiwakeCd.Location = new System.Drawing.Point(131, 4);
			this.txtUriageShiwakeCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtUriageShiwakeCd.MaxLength = 4;
			this.txtUriageShiwakeCd.Name = "txtUriageShiwakeCd";
			this.txtUriageShiwakeCd.Size = new System.Drawing.Size(51, 23);
			this.txtUriageShiwakeCd.TabIndex = 8;
			this.txtUriageShiwakeCd.Text = "0";
			this.txtUriageShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtUriageShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtUriageShiwakeCd_Validating);
			// 
			// lblShiireShiwakeCd
			// 
			this.lblShiireShiwakeCd.BackColor = System.Drawing.Color.Silver;
			this.lblShiireShiwakeCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShiireShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiireShiwakeCd.Location = new System.Drawing.Point(0, 0);
			this.lblShiireShiwakeCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiireShiwakeCd.Name = "lblShiireShiwakeCd";
			this.lblShiireShiwakeCd.Size = new System.Drawing.Size(1063, 31);
			this.lblShiireShiwakeCd.TabIndex = 20;
			this.lblShiireShiwakeCd.Tag = "CHANGE";
			this.lblShiireShiwakeCd.Text = "仕入仕訳コード";
			this.lblShiireShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiireShiwakeCd
			// 
			this.txtShiireShiwakeCd.AllowDrop = true;
			this.txtShiireShiwakeCd.AutoSizeFromLength = false;
			this.txtShiireShiwakeCd.DisplayLength = null;
			this.txtShiireShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiireShiwakeCd.Location = new System.Drawing.Point(131, 4);
			this.txtShiireShiwakeCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiireShiwakeCd.MaxLength = 4;
			this.txtShiireShiwakeCd.Name = "txtShiireShiwakeCd";
			this.txtShiireShiwakeCd.Size = new System.Drawing.Size(51, 23);
			this.txtShiireShiwakeCd.TabIndex = 9;
			this.txtShiireShiwakeCd.Text = "0";
			this.txtShiireShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiireShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireShiwakeCd_Validating);
			// 
			// lblIrisu
			// 
			this.lblIrisu.BackColor = System.Drawing.Color.Silver;
			this.lblIrisu.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblIrisu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblIrisu.Location = new System.Drawing.Point(0, 0);
			this.lblIrisu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblIrisu.Name = "lblIrisu";
			this.lblIrisu.Size = new System.Drawing.Size(1063, 31);
			this.lblIrisu.TabIndex = 26;
			this.lblIrisu.Tag = "CHANGE";
			this.lblIrisu.Text = "入          数";
			this.lblIrisu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtIrisu
			// 
			this.txtIrisu.AllowDrop = true;
			this.txtIrisu.AutoSizeFromLength = false;
			this.txtIrisu.DisplayLength = null;
			this.txtIrisu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtIrisu.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtIrisu.Location = new System.Drawing.Point(131, 3);
			this.txtIrisu.Margin = new System.Windows.Forms.Padding(4);
			this.txtIrisu.MaxLength = 4;
			this.txtIrisu.Name = "txtIrisu";
			this.txtIrisu.Size = new System.Drawing.Size(51, 23);
			this.txtIrisu.TabIndex = 11;
			this.txtIrisu.Text = "0";
			this.txtIrisu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtIrisu.Validating += new System.ComponentModel.CancelEventHandler(this.txtIrisu_Validating);
			// 
			// lblTani
			// 
			this.lblTani.BackColor = System.Drawing.Color.Silver;
			this.lblTani.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTani.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTani.Location = new System.Drawing.Point(0, 0);
			this.lblTani.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTani.Name = "lblTani";
			this.lblTani.Size = new System.Drawing.Size(1063, 34);
			this.lblTani.TabIndex = 28;
			this.lblTani.Tag = "CHANGE";
			this.lblTani.Text = "単          位";
			this.lblTani.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTani
			// 
			this.txtTani.AllowDrop = true;
			this.txtTani.AutoSizeFromLength = false;
			this.txtTani.DisplayLength = null;
			this.txtTani.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTani.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtTani.Location = new System.Drawing.Point(131, 5);
			this.txtTani.Margin = new System.Windows.Forms.Padding(4);
			this.txtTani.MaxLength = 4;
			this.txtTani.Name = "txtTani";
			this.txtTani.Size = new System.Drawing.Size(51, 23);
			this.txtTani.TabIndex = 12;
			this.txtTani.Validating += new System.ComponentModel.CancelEventHandler(this.txtTani_Validating);
			// 
			// lblZaikoKanriKbnResults
			// 
			this.lblZaikoKanriKbnResults.BackColor = System.Drawing.Color.LightCyan;
			this.lblZaikoKanriKbnResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblZaikoKanriKbnResults.Location = new System.Drawing.Point(188, 4);
			this.lblZaikoKanriKbnResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZaikoKanriKbnResults.Name = "lblZaikoKanriKbnResults";
			this.lblZaikoKanriKbnResults.Size = new System.Drawing.Size(312, 24);
			this.lblZaikoKanriKbnResults.TabIndex = 16;
			this.lblZaikoKanriKbnResults.Tag = "CHANGE";
			this.lblZaikoKanriKbnResults.Text = " 0:管理しない  1:管理する";
			this.lblZaikoKanriKbnResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblUriageShiwakeCdResults
			// 
			this.lblUriageShiwakeCdResults.BackColor = System.Drawing.Color.LightCyan;
			this.lblUriageShiwakeCdResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblUriageShiwakeCdResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblUriageShiwakeCdResults.Location = new System.Drawing.Point(188, 4);
			this.lblUriageShiwakeCdResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblUriageShiwakeCdResults.Name = "lblUriageShiwakeCdResults";
			this.lblUriageShiwakeCdResults.Size = new System.Drawing.Size(312, 24);
			this.lblUriageShiwakeCdResults.TabIndex = 19;
			this.lblUriageShiwakeCdResults.Tag = "DISPNAME";
			this.lblUriageShiwakeCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShiireShiwakeCdResults
			// 
			this.lblShiireShiwakeCdResults.BackColor = System.Drawing.Color.LightCyan;
			this.lblShiireShiwakeCdResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShiireShiwakeCdResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiireShiwakeCdResults.Location = new System.Drawing.Point(188, 3);
			this.lblShiireShiwakeCdResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiireShiwakeCdResults.Name = "lblShiireShiwakeCdResults";
			this.lblShiireShiwakeCdResults.Size = new System.Drawing.Size(312, 24);
			this.lblShiireShiwakeCdResults.TabIndex = 22;
			this.lblShiireShiwakeCdResults.Tag = "DISPNAME";
			this.lblShiireShiwakeCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiireTanka
			// 
			this.txtShiireTanka.AllowDrop = true;
			this.txtShiireTanka.AutoSizeFromLength = false;
			this.txtShiireTanka.DisplayLength = null;
			this.txtShiireTanka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiireTanka.Location = new System.Drawing.Point(655, 3);
			this.txtShiireTanka.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiireTanka.MaxLength = 12;
			this.txtShiireTanka.Name = "txtShiireTanka";
			this.txtShiireTanka.Size = new System.Drawing.Size(172, 23);
			this.txtShiireTanka.TabIndex = 13;
			this.txtShiireTanka.Text = "0.00";
			this.txtShiireTanka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiireTanka.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireTanka_Validating);
			// 
			// lblShiireTanka
			// 
			this.lblShiireTanka.BackColor = System.Drawing.Color.Silver;
			this.lblShiireTanka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiireTanka.Location = new System.Drawing.Point(526, 4);
			this.lblShiireTanka.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiireTanka.Name = "lblShiireTanka";
			this.lblShiireTanka.Size = new System.Drawing.Size(131, 24);
			this.lblShiireTanka.TabIndex = 30;
			this.lblShiireTanka.Tag = "CHANGE";
			this.lblShiireTanka.Text = "仕  入  単  価";
			this.lblShiireTanka.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtOroshiTanka
			// 
			this.txtOroshiTanka.AllowDrop = true;
			this.txtOroshiTanka.AutoSizeFromLength = false;
			this.txtOroshiTanka.DisplayLength = null;
			this.txtOroshiTanka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtOroshiTanka.Location = new System.Drawing.Point(655, 3);
			this.txtOroshiTanka.Margin = new System.Windows.Forms.Padding(4);
			this.txtOroshiTanka.MaxLength = 12;
			this.txtOroshiTanka.Name = "txtOroshiTanka";
			this.txtOroshiTanka.Size = new System.Drawing.Size(172, 23);
			this.txtOroshiTanka.TabIndex = 14;
			this.txtOroshiTanka.Text = "0.00";
			this.txtOroshiTanka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtOroshiTanka.Validating += new System.ComponentModel.CancelEventHandler(this.txtOroshiTanka_Validating);
			// 
			// lblOroshiTanka
			// 
			this.lblOroshiTanka.BackColor = System.Drawing.Color.Silver;
			this.lblOroshiTanka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblOroshiTanka.Location = new System.Drawing.Point(526, 3);
			this.lblOroshiTanka.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblOroshiTanka.Name = "lblOroshiTanka";
			this.lblOroshiTanka.Size = new System.Drawing.Size(131, 24);
			this.lblOroshiTanka.TabIndex = 32;
			this.lblOroshiTanka.Tag = "CHANGE";
			this.lblOroshiTanka.Text = "卸    単    価";
			this.lblOroshiTanka.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKouriTanka
			// 
			this.txtKouriTanka.AllowDrop = true;
			this.txtKouriTanka.AutoSizeFromLength = false;
			this.txtKouriTanka.DisplayLength = null;
			this.txtKouriTanka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKouriTanka.Location = new System.Drawing.Point(655, 4);
			this.txtKouriTanka.Margin = new System.Windows.Forms.Padding(4);
			this.txtKouriTanka.MaxLength = 12;
			this.txtKouriTanka.Name = "txtKouriTanka";
			this.txtKouriTanka.Size = new System.Drawing.Size(172, 23);
			this.txtKouriTanka.TabIndex = 15;
			this.txtKouriTanka.Text = "0.00";
			this.txtKouriTanka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKouriTanka.Validating += new System.ComponentModel.CancelEventHandler(this.txtKouriTanka_Validating);
			// 
			// lblKouriTanka
			// 
			this.lblKouriTanka.BackColor = System.Drawing.Color.Silver;
			this.lblKouriTanka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKouriTanka.Location = new System.Drawing.Point(526, 4);
			this.lblKouriTanka.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKouriTanka.Name = "lblKouriTanka";
			this.lblKouriTanka.Size = new System.Drawing.Size(131, 24);
			this.lblKouriTanka.TabIndex = 34;
			this.lblKouriTanka.Tag = "CHANGE";
			this.lblKouriTanka.Text = "小  売  単  価";
			this.lblKouriTanka.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohinKbn1Results
			// 
			this.lblShohinKbn1Results.BackColor = System.Drawing.Color.LightCyan;
			this.lblShohinKbn1Results.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinKbn1Results.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKbn1Results.Location = new System.Drawing.Point(712, 4);
			this.lblShohinKbn1Results.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKbn1Results.Name = "lblShohinKbn1Results";
			this.lblShohinKbn1Results.Size = new System.Drawing.Size(285, 24);
			this.lblShohinKbn1Results.TabIndex = 38;
			this.lblShohinKbn1Results.Tag = "DISPNAME";
			this.lblShohinKbn1Results.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinKbn1
			// 
			this.txtShohinKbn1.AllowDrop = true;
			this.txtShohinKbn1.AutoSizeFromLength = false;
			this.txtShohinKbn1.DisplayLength = null;
			this.txtShohinKbn1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinKbn1.Location = new System.Drawing.Point(655, 4);
			this.txtShohinKbn1.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinKbn1.MaxLength = 4;
			this.txtShohinKbn1.Name = "txtShohinKbn1";
			this.txtShohinKbn1.Size = new System.Drawing.Size(51, 23);
			this.txtShohinKbn1.TabIndex = 16;
			this.txtShohinKbn1.Text = "0";
			this.txtShohinKbn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinKbn1.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbn1_Validating);
			// 
			// lblShohinKbn1
			// 
			this.lblShohinKbn1.BackColor = System.Drawing.Color.Silver;
			this.lblShohinKbn1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKbn1.Location = new System.Drawing.Point(526, 3);
			this.lblShohinKbn1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKbn1.Name = "lblShohinKbn1";
			this.lblShohinKbn1.Size = new System.Drawing.Size(116, 24);
			this.lblShohinKbn1.TabIndex = 36;
			this.lblShohinKbn1.Tag = "CHANGE";
			this.lblShohinKbn1.Text = "商 品 区 分 1";
			this.lblShohinKbn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohinKbn2Results
			// 
			this.lblShohinKbn2Results.BackColor = System.Drawing.Color.LightCyan;
			this.lblShohinKbn2Results.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinKbn2Results.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKbn2Results.Location = new System.Drawing.Point(712, 4);
			this.lblShohinKbn2Results.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKbn2Results.Name = "lblShohinKbn2Results";
			this.lblShohinKbn2Results.Size = new System.Drawing.Size(285, 24);
			this.lblShohinKbn2Results.TabIndex = 41;
			this.lblShohinKbn2Results.Tag = "DISPNAME";
			this.lblShohinKbn2Results.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinKbn2
			// 
			this.txtShohinKbn2.AllowDrop = true;
			this.txtShohinKbn2.AutoSizeFromLength = false;
			this.txtShohinKbn2.DisplayLength = null;
			this.txtShohinKbn2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinKbn2.Location = new System.Drawing.Point(655, 3);
			this.txtShohinKbn2.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinKbn2.MaxLength = 4;
			this.txtShohinKbn2.Name = "txtShohinKbn2";
			this.txtShohinKbn2.Size = new System.Drawing.Size(51, 23);
			this.txtShohinKbn2.TabIndex = 17;
			this.txtShohinKbn2.Text = "0";
			this.txtShohinKbn2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinKbn2.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbn2_Validating);
			// 
			// lblShohinKbn2
			// 
			this.lblShohinKbn2.BackColor = System.Drawing.Color.Silver;
			this.lblShohinKbn2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKbn2.Location = new System.Drawing.Point(526, 3);
			this.lblShohinKbn2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKbn2.Name = "lblShohinKbn2";
			this.lblShohinKbn2.Size = new System.Drawing.Size(116, 24);
			this.lblShohinKbn2.TabIndex = 39;
			this.lblShohinKbn2.Tag = "CHANGE";
			this.lblShohinKbn2.Text = "商 品 区 分 2";
			this.lblShohinKbn2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohinKbn3Results
			// 
			this.lblShohinKbn3Results.BackColor = System.Drawing.Color.LightCyan;
			this.lblShohinKbn3Results.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinKbn3Results.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKbn3Results.Location = new System.Drawing.Point(712, 4);
			this.lblShohinKbn3Results.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKbn3Results.Name = "lblShohinKbn3Results";
			this.lblShohinKbn3Results.Size = new System.Drawing.Size(285, 24);
			this.lblShohinKbn3Results.TabIndex = 44;
			this.lblShohinKbn3Results.Tag = "DISPNAME";
			this.lblShohinKbn3Results.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinKbn3
			// 
			this.txtShohinKbn3.AllowDrop = true;
			this.txtShohinKbn3.AutoSizeFromLength = false;
			this.txtShohinKbn3.DisplayLength = null;
			this.txtShohinKbn3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinKbn3.Location = new System.Drawing.Point(655, 4);
			this.txtShohinKbn3.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinKbn3.MaxLength = 4;
			this.txtShohinKbn3.Name = "txtShohinKbn3";
			this.txtShohinKbn3.Size = new System.Drawing.Size(51, 23);
			this.txtShohinKbn3.TabIndex = 18;
			this.txtShohinKbn3.Text = "0";
			this.txtShohinKbn3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinKbn3.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbn3_Validating);
			// 
			// lblShohinKbn3
			// 
			this.lblShohinKbn3.BackColor = System.Drawing.Color.Silver;
			this.lblShohinKbn3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKbn3.Location = new System.Drawing.Point(526, 3);
			this.lblShohinKbn3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKbn3.Name = "lblShohinKbn3";
			this.lblShohinKbn3.Size = new System.Drawing.Size(116, 24);
			this.lblShohinKbn3.TabIndex = 42;
			this.lblShohinKbn3.Tag = "CHANGE";
			this.lblShohinKbn3.Text = "商 品 分 類";
			this.lblShohinKbn3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohinKbn4Results
			// 
			this.lblShohinKbn4Results.BackColor = System.Drawing.Color.LightCyan;
			this.lblShohinKbn4Results.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinKbn4Results.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKbn4Results.Location = new System.Drawing.Point(712, 3);
			this.lblShohinKbn4Results.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKbn4Results.Name = "lblShohinKbn4Results";
			this.lblShohinKbn4Results.Size = new System.Drawing.Size(285, 24);
			this.lblShohinKbn4Results.TabIndex = 47;
			this.lblShohinKbn4Results.Tag = "DISPNAME";
			this.lblShohinKbn4Results.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinKbn4
			// 
			this.txtShohinKbn4.AllowDrop = true;
			this.txtShohinKbn4.AutoSizeFromLength = false;
			this.txtShohinKbn4.DisplayLength = null;
			this.txtShohinKbn4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinKbn4.Location = new System.Drawing.Point(655, 3);
			this.txtShohinKbn4.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinKbn4.MaxLength = 4;
			this.txtShohinKbn4.Name = "txtShohinKbn4";
			this.txtShohinKbn4.Size = new System.Drawing.Size(51, 23);
			this.txtShohinKbn4.TabIndex = 19;
			this.txtShohinKbn4.Text = "0";
			this.txtShohinKbn4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinKbn4.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbn4_Validating);
			// 
			// lblShohinKbn4
			// 
			this.lblShohinKbn4.BackColor = System.Drawing.Color.Silver;
			this.lblShohinKbn4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinKbn4.Location = new System.Drawing.Point(526, 3);
			this.lblShohinKbn4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinKbn4.Name = "lblShohinKbn4";
			this.lblShohinKbn4.Size = new System.Drawing.Size(116, 24);
			this.lblShohinKbn4.TabIndex = 45;
			this.lblShohinKbn4.Tag = "CHANGE";
			this.lblShohinKbn4.Text = "商 品 区 分 4";
			this.lblShohinKbn4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBarcodeCase
			// 
			this.txtBarcodeCase.AllowDrop = true;
			this.txtBarcodeCase.AutoSizeFromLength = false;
			this.txtBarcodeCase.DisplayLength = null;
			this.txtBarcodeCase.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtBarcodeCase.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtBarcodeCase.Location = new System.Drawing.Point(655, 4);
			this.txtBarcodeCase.Margin = new System.Windows.Forms.Padding(4);
			this.txtBarcodeCase.MaxLength = 13;
			this.txtBarcodeCase.Name = "txtBarcodeCase";
			this.txtBarcodeCase.Size = new System.Drawing.Size(172, 23);
			this.txtBarcodeCase.TabIndex = 20;
			this.txtBarcodeCase.Validating += new System.ComponentModel.CancelEventHandler(this.txtBarcodeCase_Validating);
			// 
			// lblBarcodeCase
			// 
			this.lblBarcodeCase.BackColor = System.Drawing.Color.Silver;
			this.lblBarcodeCase.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBarcodeCase.Location = new System.Drawing.Point(526, 3);
			this.lblBarcodeCase.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBarcodeCase.Name = "lblBarcodeCase";
			this.lblBarcodeCase.Size = new System.Drawing.Size(116, 24);
			this.lblBarcodeCase.TabIndex = 48;
			this.lblBarcodeCase.Tag = "CHANGE";
			this.lblBarcodeCase.Text = "ﾊﾞｰｺｰﾄﾞ (ｹｰｽ)";
			this.lblBarcodeCase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBarcodeBara
			// 
			this.txtBarcodeBara.AllowDrop = true;
			this.txtBarcodeBara.AutoSizeFromLength = false;
			this.txtBarcodeBara.DisplayLength = null;
			this.txtBarcodeBara.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtBarcodeBara.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtBarcodeBara.Location = new System.Drawing.Point(655, 4);
			this.txtBarcodeBara.Margin = new System.Windows.Forms.Padding(4);
			this.txtBarcodeBara.MaxLength = 13;
			this.txtBarcodeBara.Name = "txtBarcodeBara";
			this.txtBarcodeBara.Size = new System.Drawing.Size(172, 23);
			this.txtBarcodeBara.TabIndex = 21;
			this.txtBarcodeBara.Validating += new System.ComponentModel.CancelEventHandler(this.txtBarcodeBara_Validating);
			// 
			// lblBarcodeBara
			// 
			this.lblBarcodeBara.BackColor = System.Drawing.Color.Silver;
			this.lblBarcodeBara.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBarcodeBara.Location = new System.Drawing.Point(528, 3);
			this.lblBarcodeBara.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBarcodeBara.Name = "lblBarcodeBara";
			this.lblBarcodeBara.Size = new System.Drawing.Size(114, 24);
			this.lblBarcodeBara.TabIndex = 50;
			this.lblBarcodeBara.Tag = "CHANGE";
			this.lblBarcodeBara.Text = "ﾊﾞｰｺｰﾄﾞ (ﾊﾞﾗ)";
			this.lblBarcodeBara.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTekiseisu
			// 
			this.txtTekiseisu.AllowDrop = true;
			this.txtTekiseisu.AutoSizeFromLength = false;
			this.txtTekiseisu.DisplayLength = null;
			this.txtTekiseisu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTekiseisu.Location = new System.Drawing.Point(655, 4);
			this.txtTekiseisu.Margin = new System.Windows.Forms.Padding(4);
			this.txtTekiseisu.MaxLength = 12;
			this.txtTekiseisu.Name = "txtTekiseisu";
			this.txtTekiseisu.Size = new System.Drawing.Size(172, 23);
			this.txtTekiseisu.TabIndex = 22;
			this.txtTekiseisu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTekiseisu.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiseisu_Validating);
			// 
			// lblTekiseisu
			// 
			this.lblTekiseisu.BackColor = System.Drawing.Color.Silver;
			this.lblTekiseisu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTekiseisu.Location = new System.Drawing.Point(526, 4);
			this.lblTekiseisu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTekiseisu.Name = "lblTekiseisu";
			this.lblTekiseisu.Size = new System.Drawing.Size(131, 24);
			this.lblTekiseisu.TabIndex = 52;
			this.lblTekiseisu.Tag = "CHANGE";
			this.lblTekiseisu.Text = "適    正    数";
			this.lblTekiseisu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShishoCd
			// 
			this.txtShishoCd.AutoSizeFromLength = true;
			this.txtShishoCd.DisplayLength = null;
			this.txtShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShishoCd.Location = new System.Drawing.Point(138, 3);
			this.txtShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtShishoCd.MaxLength = 4;
			this.txtShishoCd.Name = "txtShishoCd";
			this.txtShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtShishoCd.TabIndex = 1;
			this.txtShishoCd.TabStop = false;
			this.txtShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCd_Validating);
			// 
			// lblShishoNm
			// 
			this.lblShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShishoNm.Location = new System.Drawing.Point(185, 2);
			this.lblShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShishoNm.Name = "lblShishoNm";
			this.lblShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblShishoNm.TabIndex = 3;
			this.lblShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShisho
			// 
			this.lblShisho.BackColor = System.Drawing.Color.Silver;
			this.lblShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShisho.Location = new System.Drawing.Point(0, 0);
			this.lblShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShisho.Name = "lblShisho";
			this.lblShisho.Size = new System.Drawing.Size(1063, 31);
			this.lblShisho.TabIndex = 1;
			this.lblShisho.Tag = "CHANGE";
			this.lblShisho.Text = "支　　所";
			this.lblShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.LightCyan;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(188, 3);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(312, 24);
			this.label1.TabIndex = 25;
			this.label1.Tag = "CHANGE";
			this.label1.Text = " 0:外税　1:内税";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiKbn
			// 
			this.txtShohizeiKbn.AllowDrop = true;
			this.txtShohizeiKbn.AutoSizeFromLength = false;
			this.txtShohizeiKbn.DisplayLength = null;
			this.txtShohizeiKbn.Enabled = false;
			this.txtShohizeiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiKbn.Location = new System.Drawing.Point(131, 3);
			this.txtShohizeiKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiKbn.MaxLength = 1;
			this.txtShohizeiKbn.Name = "txtShohizeiKbn";
			this.txtShohizeiKbn.Size = new System.Drawing.Size(51, 23);
			this.txtShohizeiKbn.TabIndex = 10;
			this.txtShohizeiKbn.Text = "0";
			this.txtShohizeiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiKbn_Validating);
			// 
			// lblShohiZeiKbn
			// 
			this.lblShohiZeiKbn.BackColor = System.Drawing.Color.Silver;
			this.lblShohiZeiKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohiZeiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohiZeiKbn.Location = new System.Drawing.Point(0, 0);
			this.lblShohiZeiKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohiZeiKbn.Name = "lblShohiZeiKbn";
			this.lblShohiZeiKbn.Size = new System.Drawing.Size(1063, 31);
			this.lblShohiZeiKbn.TabIndex = 23;
			this.lblShohiZeiKbn.Tag = "CHANGE";
			this.lblShohiZeiKbn.Text = "消 費 税 区 分";
			this.lblShohiZeiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblChushiKbnNm
			// 
			this.lblChushiKbnNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblChushiKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblChushiKbnNm.Location = new System.Drawing.Point(712, 3);
			this.lblChushiKbnNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblChushiKbnNm.Name = "lblChushiKbnNm";
			this.lblChushiKbnNm.Size = new System.Drawing.Size(312, 24);
			this.lblChushiKbnNm.TabIndex = 56;
			this.lblChushiKbnNm.Tag = "CHANGE";
			this.lblChushiKbnNm.Text = " 1: 取引有　2:取引中止";
			this.lblChushiKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtChushiKbn
			// 
			this.txtChushiKbn.AllowDrop = true;
			this.txtChushiKbn.AutoSizeFromLength = false;
			this.txtChushiKbn.DisplayLength = null;
			this.txtChushiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtChushiKbn.Location = new System.Drawing.Point(655, 3);
			this.txtChushiKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtChushiKbn.MaxLength = 1;
			this.txtChushiKbn.Name = "txtChushiKbn";
			this.txtChushiKbn.Size = new System.Drawing.Size(51, 23);
			this.txtChushiKbn.TabIndex = 23;
			this.txtChushiKbn.Text = "0";
			this.txtChushiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtChushiKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChushiKbn_KeyDown);
			this.txtChushiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtChushiKbn_Validating);
			// 
			// lblChuchiKbn
			// 
			this.lblChuchiKbn.BackColor = System.Drawing.Color.Silver;
			this.lblChuchiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblChuchiKbn.Location = new System.Drawing.Point(526, 4);
			this.lblChuchiKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblChuchiKbn.Name = "lblChuchiKbn";
			this.lblChuchiKbn.Size = new System.Drawing.Size(131, 24);
			this.lblChuchiKbn.TabIndex = 54;
			this.lblChuchiKbn.Tag = "CHANGE";
			this.lblChuchiKbn.Text = "中　止　区　分";
			this.lblChuchiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel12, 0, 11);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel11, 0, 10);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel10, 0, 9);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel9, 0, 8);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 7);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 12);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 12;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1071, 460);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel12
			// 
			this.fsiPanel12.Controls.Add(this.txtTani);
			this.fsiPanel12.Controls.Add(this.lblTani);
			this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel12.Location = new System.Drawing.Point(4, 422);
			this.fsiPanel12.Name = "fsiPanel12";
			this.fsiPanel12.Size = new System.Drawing.Size(1063, 34);
			this.fsiPanel12.TabIndex = 11;
			// 
			// fsiPanel11
			// 
			this.fsiPanel11.Controls.Add(this.txtIrisu);
			this.fsiPanel11.Controls.Add(this.lblChushiKbnNm);
			this.fsiPanel11.Controls.Add(this.lblChuchiKbn);
			this.fsiPanel11.Controls.Add(this.txtChushiKbn);
			this.fsiPanel11.Controls.Add(this.lblIrisu);
			this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel11.Location = new System.Drawing.Point(4, 384);
			this.fsiPanel11.Name = "fsiPanel11";
			this.fsiPanel11.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel11.TabIndex = 10;
			// 
			// fsiPanel10
			// 
			this.fsiPanel10.Controls.Add(this.txtShohizeiKbn);
			this.fsiPanel10.Controls.Add(this.label1);
			this.fsiPanel10.Controls.Add(this.lblTekiseisu);
			this.fsiPanel10.Controls.Add(this.txtTekiseisu);
			this.fsiPanel10.Controls.Add(this.lblShohiZeiKbn);
			this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel10.Location = new System.Drawing.Point(4, 346);
			this.fsiPanel10.Name = "fsiPanel10";
			this.fsiPanel10.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel10.TabIndex = 9;
			// 
			// fsiPanel9
			// 
			this.fsiPanel9.Controls.Add(this.txtShiireShiwakeCd);
			this.fsiPanel9.Controls.Add(this.lblShiireShiwakeCdResults);
			this.fsiPanel9.Controls.Add(this.lblBarcodeBara);
			this.fsiPanel9.Controls.Add(this.txtBarcodeBara);
			this.fsiPanel9.Controls.Add(this.lblShiireShiwakeCd);
			this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel9.Location = new System.Drawing.Point(4, 308);
			this.fsiPanel9.Name = "fsiPanel9";
			this.fsiPanel9.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel9.TabIndex = 8;
			// 
			// fsiPanel8
			// 
			this.fsiPanel8.Controls.Add(this.txtUriageShiwakeCd);
			this.fsiPanel8.Controls.Add(this.lblUriageShiwakeCdResults);
			this.fsiPanel8.Controls.Add(this.lblBarcodeCase);
			this.fsiPanel8.Controls.Add(this.txtBarcodeCase);
			this.fsiPanel8.Controls.Add(this.lblUriageShiwakeCd);
			this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel8.Location = new System.Drawing.Point(4, 270);
			this.fsiPanel8.Name = "fsiPanel8";
			this.fsiPanel8.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel8.TabIndex = 7;
			// 
			// fsiPanel7
			// 
			this.fsiPanel7.Controls.Add(this.txtZaikoKanriKbn);
			this.fsiPanel7.Controls.Add(this.lblZaikoKanriKbnResults);
			this.fsiPanel7.Controls.Add(this.lblShohinKbn4);
			this.fsiPanel7.Controls.Add(this.txtShohinKbn4);
			this.fsiPanel7.Controls.Add(this.lblShohinKbn4Results);
			this.fsiPanel7.Controls.Add(this.lblZaikoKanriKbn);
			this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel7.Location = new System.Drawing.Point(4, 232);
			this.fsiPanel7.Name = "fsiPanel7";
			this.fsiPanel7.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel7.TabIndex = 6;
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.txtTanaban);
			this.fsiPanel6.Controls.Add(this.lblShohinKbn3);
			this.fsiPanel6.Controls.Add(this.txtShohinKbn3);
			this.fsiPanel6.Controls.Add(this.lblShohinKbn3Results);
			this.fsiPanel6.Controls.Add(this.lblTanaban);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel6.Location = new System.Drawing.Point(4, 194);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel6.TabIndex = 5;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.txtShohinKikaku);
			this.fsiPanel5.Controls.Add(this.lblShohinKbn2);
			this.fsiPanel5.Controls.Add(this.txtShohinKbn2);
			this.fsiPanel5.Controls.Add(this.lblShohinKbn2Results);
			this.fsiPanel5.Controls.Add(this.lblShohinKikaku);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 156);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel5.TabIndex = 4;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtShohinKanaNm);
			this.fsiPanel4.Controls.Add(this.lblShohinKbn1);
			this.fsiPanel4.Controls.Add(this.txtShohinKbn1);
			this.fsiPanel4.Controls.Add(this.lblShohinKbn1Results);
			this.fsiPanel4.Controls.Add(this.lblShohinKanaNm);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 118);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel4.TabIndex = 3;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtShohinNm);
			this.fsiPanel3.Controls.Add(this.lblKouriTanka);
			this.fsiPanel3.Controls.Add(this.txtKouriTanka);
			this.fsiPanel3.Controls.Add(this.lblShohinNm);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 80);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtShohinCd);
			this.fsiPanel2.Controls.Add(this.lblOroshiTanka);
			this.fsiPanel2.Controls.Add(this.txtOroshiTanka);
			this.fsiPanel2.Controls.Add(this.lblShohinCd);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 42);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtShishoCd);
			this.fsiPanel1.Controls.Add(this.lblShishoNm);
			this.fsiPanel1.Controls.Add(this.lblShiireTanka);
			this.fsiPanel1.Controls.Add(this.txtShiireTanka);
			this.fsiPanel1.Controls.Add(this.lblShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(1063, 31);
			this.fsiPanel1.TabIndex = 0;
			// 
			// KBCM1022
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1084, 614);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "KBCM1022";
			this.Par1 = "";
			this.Par2 = "";
			this.ShowFButton = true;
			this.Text = "";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel12.ResumeLayout(false);
			this.fsiPanel12.PerformLayout();
			this.fsiPanel11.ResumeLayout(false);
			this.fsiPanel11.PerformLayout();
			this.fsiPanel10.ResumeLayout(false);
			this.fsiPanel10.PerformLayout();
			this.fsiPanel9.ResumeLayout(false);
			this.fsiPanel9.PerformLayout();
			this.fsiPanel8.ResumeLayout(false);
			this.fsiPanel8.PerformLayout();
			this.fsiPanel7.ResumeLayout(false);
			this.fsiPanel7.PerformLayout();
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblShohinCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCd;
        private System.Windows.Forms.Label lblShohinNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinNm;
        private System.Windows.Forms.Label lblShohinKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKanaNm;
        private System.Windows.Forms.Label lblShohinKikaku;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKikaku;
        private System.Windows.Forms.Label lblTanaban;
        private jp.co.fsi.common.controls.FsiTextBox txtTanaban;
        private System.Windows.Forms.Label lblZaikoKanriKbn;
        private jp.co.fsi.common.controls.FsiTextBox txtZaikoKanriKbn;
        private System.Windows.Forms.Label lblUriageShiwakeCd;
        private jp.co.fsi.common.controls.FsiTextBox txtUriageShiwakeCd;
        private System.Windows.Forms.Label lblShiireShiwakeCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireShiwakeCd;
        private System.Windows.Forms.Label lblIrisu;
        private jp.co.fsi.common.controls.FsiTextBox txtIrisu;
        private System.Windows.Forms.Label lblTani;
        private jp.co.fsi.common.controls.FsiTextBox txtTani;
        private System.Windows.Forms.Label lblZaikoKanriKbnResults;
        private System.Windows.Forms.Label lblUriageShiwakeCdResults;
        private System.Windows.Forms.Label lblShiireShiwakeCdResults;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireTanka;
        private System.Windows.Forms.Label lblShiireTanka;
        private jp.co.fsi.common.controls.FsiTextBox txtOroshiTanka;
        private System.Windows.Forms.Label lblOroshiTanka;
        private jp.co.fsi.common.controls.FsiTextBox txtKouriTanka;
        private System.Windows.Forms.Label lblKouriTanka;
        private System.Windows.Forms.Label lblShohinKbn1Results;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbn1;
        private System.Windows.Forms.Label lblShohinKbn1;
        private System.Windows.Forms.Label lblShohinKbn2Results;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbn2;
        private System.Windows.Forms.Label lblShohinKbn2;
        private System.Windows.Forms.Label lblShohinKbn3Results;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbn3;
        private System.Windows.Forms.Label lblShohinKbn3;
        private System.Windows.Forms.Label lblShohinKbn4Results;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbn4;
        private System.Windows.Forms.Label lblShohinKbn4;
        private jp.co.fsi.common.controls.FsiTextBox txtBarcodeCase;
        private System.Windows.Forms.Label lblBarcodeCase;
        private jp.co.fsi.common.controls.FsiTextBox txtBarcodeBara;
        private System.Windows.Forms.Label lblBarcodeBara;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiseisu;
        private System.Windows.Forms.Label lblTekiseisu;
        private common.controls.FsiTextBox txtShishoCd;
        private System.Windows.Forms.Label lblShishoNm;
        private System.Windows.Forms.Label lblShisho;
        private System.Windows.Forms.Label label1;
        private common.controls.FsiTextBox txtShohizeiKbn;
        private System.Windows.Forms.Label lblShohiZeiKbn;
        private System.Windows.Forms.Label lblChushiKbnNm;
        private common.controls.FsiTextBox txtChushiKbn;
        private System.Windows.Forms.Label lblChuchiKbn;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel12;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel10;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}