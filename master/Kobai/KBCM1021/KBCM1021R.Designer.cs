﻿namespace jp.co.fsi.kb.kbcm1021
{
    /// <summary>
    /// KBCM1021R の概要の説明です。
    /// </summary>
    partial class KBCM1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBCM1021R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtValue20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtValue22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtValue26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox2,
            this.txtTitle19,
            this.textBox1,
            this.txtTitle16,
            this.txtTitle18,
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtCompanyName,
            this.lblTitle,
            this.line1,
            this.txtTitle01,
            this.txtTitle02,
            this.txtTitle03,
            this.txtTitle04,
            this.txtTitle05,
            this.txtTitle06,
            this.txtTitle07,
            this.txtTitle08,
            this.txtTitle09,
            this.txtTitle10,
            this.txtTitle11,
            this.txtTitle12,
            this.txtTitle13,
            this.txtTitle14,
            this.txtTitle15,
            this.txtTitle17,
            this.line2,
            this.line3,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line4,
            this.line5,
            this.line6});
            this.pageHeader.Height = 1.265458F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // textBox2
            // 
            this.textBox2.Height = 0.4833703F;
            this.textBox2.Left = 0.01181102F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: bold; " +
    "text-align: center; vertical-align: middle";
            this.textBox2.Text = null;
            this.textBox2.Top = 0.8060001F;
            this.textBox2.Width = 9.112598F;
            // 
            // txtTitle19
            // 
            this.txtTitle19.Height = 0.1574803F;
            this.txtTitle19.Left = 10.21496F;
            this.txtTitle19.MultiLine = false;
            this.txtTitle19.Name = "txtTitle19";
            this.txtTitle19.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle";
            this.txtTitle19.Text = "商品区分5";
            this.txtTitle19.Top = 0.9633859F;
            this.txtTitle19.Width = 1.072441F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.1574803F;
            this.textBox1.Left = 10.10118F;
            this.textBox1.LineSpacing = 1F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: Cyan; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-" +
    "weight: normal; text-align: left; vertical-align: middle";
            this.textBox1.Text = null;
            this.textBox1.Top = 1.120866F;
            this.textBox1.Width = 1.18582F;
            // 
            // txtTitle16
            // 
            this.txtTitle16.Height = 0.1574803F;
            this.txtTitle16.Left = 9.134646F;
            this.txtTitle16.MultiLine = false;
            this.txtTitle16.Name = "txtTitle16";
            this.txtTitle16.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle";
            this.txtTitle16.Text = "商品区分2";
            this.txtTitle16.Top = 0.9633859F;
            this.txtTitle16.Width = 1.080315F;
            // 
            // txtTitle18
            // 
            this.txtTitle18.Height = 0.1574803F;
            this.txtTitle18.Left = 10.21496F;
            this.txtTitle18.MultiLine = false;
            this.txtTitle18.Name = "txtTitle18";
            this.txtTitle18.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle";
            this.txtTitle18.Text = "商品区分4";
            this.txtTitle18.Top = 0.8059056F;
            this.txtTitle18.Width = 1.072441F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.2070866F;
            this.txtToday.Left = 9.659841F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtToday.Text = "ggyy年M月d日";
            this.txtToday.Top = 0.4465468F;
            this.txtToday.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2070866F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 11.13623F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.4465468F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.2070866F;
            this.txtPageCount.Left = 10.84095F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.4465468F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0.1531496F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 1";
            this.txtCompanyName.Text = "ddd";
            this.txtCompanyName.Top = 0.1027559F;
            this.txtCompanyName.Width = 3.667323F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.2464567F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 4.72441F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center";
            this.lblTitle.Text = "商品一覧表";
            this.lblTitle.Top = 0.102846F;
            this.lblTitle.Width = 1.846457F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 4.72441F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.3493027F;
            this.line1.Width = 1.846457F;
            this.line1.X1 = 4.72441F;
            this.line1.X2 = 6.570867F;
            this.line1.Y1 = 0.3493027F;
            this.line1.Y2 = 0.3493027F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.3059056F;
            this.txtTitle01.Left = 0.01181102F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle01.Text = "コード";
            this.txtTitle01.Top = 0.8933071F;
            this.txtTitle01.Width = 0.4566929F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.2362205F;
            this.txtTitle02.Left = 0.468504F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle02.Text = "商 品 名";
            this.txtTitle02.Top = 0.8059056F;
            this.txtTitle02.Width = 1.994095F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.2362205F;
            this.txtTitle03.Left = 0.468504F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle03.Text = "規  格";
            this.txtTitle03.Top = 1.04252F;
            this.txtTitle03.Width = 1.994095F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.3059056F;
            this.txtTitle04.Left = 2.462599F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle04.Text = "カ ナ 名";
            this.txtTitle04.Top = 0.8933071F;
            this.txtTitle04.Width = 1.295276F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.Height = 0.3059056F;
            this.txtTitle05.Left = 3.757874F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle05.Text = "JANコード";
            this.txtTitle05.Top = 0.8933071F;
            this.txtTitle05.Width = 0.8224404F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.Height = 0.3059056F;
            this.txtTitle06.Left = 4.580315F;
            this.txtTitle06.MultiLine = false;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle06.Text = "入 数";
            this.txtTitle06.Top = 0.8933071F;
            this.txtTitle06.Width = 0.4925199F;
            // 
            // txtTitle07
            // 
            this.txtTitle07.Height = 0.3059056F;
            this.txtTitle07.Left = 5.072835F;
            this.txtTitle07.MultiLine = false;
            this.txtTitle07.Name = "txtTitle07";
            this.txtTitle07.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle07.Text = "単 位";
            this.txtTitle07.Top = 0.893307F;
            this.txtTitle07.Width = 0.4405513F;
            // 
            // txtTitle08
            // 
            this.txtTitle08.Height = 0.3059056F;
            this.txtTitle08.Left = 5.513386F;
            this.txtTitle08.MultiLine = false;
            this.txtTitle08.Name = "txtTitle08";
            this.txtTitle08.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle08.Text = "仕入単価";
            this.txtTitle08.Top = 0.893307F;
            this.txtTitle08.Width = 0.655118F;
            // 
            // txtTitle09
            // 
            this.txtTitle09.Height = 0.3059057F;
            this.txtTitle09.Left = 6.168504F;
            this.txtTitle09.MultiLine = false;
            this.txtTitle09.Name = "txtTitle09";
            this.txtTitle09.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle09.Text = "卸単価";
            this.txtTitle09.Top = 0.8933071F;
            this.txtTitle09.Width = 0.655118F;
            // 
            // txtTitle10
            // 
            this.txtTitle10.Height = 0.3059057F;
            this.txtTitle10.Left = 6.823623F;
            this.txtTitle10.MultiLine = false;
            this.txtTitle10.Name = "txtTitle10";
            this.txtTitle10.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle10.Text = "小売単価";
            this.txtTitle10.Top = 0.8933071F;
            this.txtTitle10.Width = 0.7523623F;
            // 
            // txtTitle11
            // 
            this.txtTitle11.Height = 0.2362205F;
            this.txtTitle11.Left = 7.575985F;
            this.txtTitle11.MultiLine = false;
            this.txtTitle11.Name = "txtTitle11";
            this.txtTitle11.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle";
            this.txtTitle11.Text = "棚  番";
            this.txtTitle11.Top = 0.8070866F;
            this.txtTitle11.Width = 0.5637797F;
            // 
            // txtTitle12
            // 
            this.txtTitle12.Height = 0.2362205F;
            this.txtTitle12.Left = 7.575985F;
            this.txtTitle12.MultiLine = false;
            this.txtTitle12.Name = "txtTitle12";
            this.txtTitle12.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle";
            this.txtTitle12.Text = "在庫管理";
            this.txtTitle12.Top = 1.043307F;
            this.txtTitle12.Width = 0.5637797F;
            // 
            // txtTitle13
            // 
            this.txtTitle13.Height = 0.2362205F;
            this.txtTitle13.Left = 8.139765F;
            this.txtTitle13.MultiLine = false;
            this.txtTitle13.Name = "txtTitle13";
            this.txtTitle13.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle13.Text = "売上仕訳";
            this.txtTitle13.Top = 0.8059056F;
            this.txtTitle13.Width = 0.9948816F;
            // 
            // txtTitle14
            // 
            this.txtTitle14.Height = 0.2362205F;
            this.txtTitle14.Left = 8.139765F;
            this.txtTitle14.MultiLine = false;
            this.txtTitle14.Name = "txtTitle14";
            this.txtTitle14.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle14.Text = "仕入仕訳";
            this.txtTitle14.Top = 1.042126F;
            this.txtTitle14.Width = 0.9948816F;
            // 
            // txtTitle15
            // 
            this.txtTitle15.Height = 0.1574803F;
            this.txtTitle15.Left = 9.134646F;
            this.txtTitle15.MultiLine = false;
            this.txtTitle15.Name = "txtTitle15";
            this.txtTitle15.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle";
            this.txtTitle15.Text = "商品区分1";
            this.txtTitle15.Top = 0.8059056F;
            this.txtTitle15.Width = 1.080315F;
            // 
            // txtTitle17
            // 
            this.txtTitle17.Height = 0.1574803F;
            this.txtTitle17.Left = 9.134646F;
            this.txtTitle17.MultiLine = false;
            this.txtTitle17.Name = "txtTitle17";
            this.txtTitle17.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; " +
    "text-align: center; vertical-align: middle";
            this.txtTitle17.Text = "商品分類";
            this.txtTitle17.Top = 1.120866F;
            this.txtTitle17.Width = 1.080315F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.01181103F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.8059056F;
            this.line2.Width = 11.28504F;
            this.line2.X1 = 0.01181103F;
            this.line2.X2 = 11.29685F;
            this.line2.Y1 = 0.8059056F;
            this.line2.Y2 = 0.8059056F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0.01181102F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 1.278347F;
            this.line3.Width = 11.28504F;
            this.line3.X1 = 0.01181102F;
            this.line3.X2 = 11.29685F;
            this.line3.Y1 = 1.278347F;
            this.line3.Y2 = 1.278347F;
            // 
            // line16
            // 
            this.line16.Height = 0.4728341F;
            this.line16.Left = 0.4685039F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.805906F;
            this.line16.Width = 1.192093E-07F;
            this.line16.X1 = 0.468504F;
            this.line16.X2 = 0.4685039F;
            this.line16.Y1 = 1.27874F;
            this.line16.Y2 = 0.805906F;
            // 
            // line17
            // 
            this.line17.Height = 0.4834641F;
            this.line17.Left = 2.462598F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0.8062999F;
            this.line17.Width = 9.536743E-07F;
            this.line17.X1 = 2.462599F;
            this.line17.X2 = 2.462598F;
            this.line17.Y1 = 1.289764F;
            this.line17.Y2 = 0.8062999F;
            // 
            // line18
            // 
            this.line18.Height = 0.4834641F;
            this.line18.Left = 3.747244F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0.8062999F;
            this.line18.Width = 1.192093E-06F;
            this.line18.X1 = 3.747244F;
            this.line18.X2 = 3.747245F;
            this.line18.Y1 = 1.289764F;
            this.line18.Y2 = 0.8062999F;
            // 
            // line19
            // 
            this.line19.Height = 0.4834644F;
            this.line19.Left = 4.580315F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.8059055F;
            this.line19.Width = 0F;
            this.line19.X1 = 4.580315F;
            this.line19.X2 = 4.580315F;
            this.line19.Y1 = 1.28937F;
            this.line19.Y2 = 0.8059055F;
            // 
            // line20
            // 
            this.line20.Height = 0.4830709F;
            this.line20.Left = 5.072835F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.8062991F;
            this.line20.Width = 0F;
            this.line20.X1 = 5.072835F;
            this.line20.X2 = 5.072835F;
            this.line20.Y1 = 1.28937F;
            this.line20.Y2 = 0.8062991F;
            // 
            // line21
            // 
            this.line21.Height = 0.4830709F;
            this.line21.Left = 5.513386F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.8062991F;
            this.line21.Width = 0F;
            this.line21.X1 = 5.513386F;
            this.line21.X2 = 5.513386F;
            this.line21.Y1 = 1.28937F;
            this.line21.Y2 = 0.8062991F;
            // 
            // line22
            // 
            this.line22.Height = 0.4834651F;
            this.line22.Left = 6.168504F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.8066929F;
            this.line22.Width = 0F;
            this.line22.X1 = 6.168504F;
            this.line22.X2 = 6.168504F;
            this.line22.Y1 = 1.290158F;
            this.line22.Y2 = 0.8066929F;
            // 
            // line23
            // 
            this.line23.Height = 0.4830715F;
            this.line23.Left = 6.823623F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.8070865F;
            this.line23.Width = 0F;
            this.line23.X1 = 6.823623F;
            this.line23.X2 = 6.823623F;
            this.line23.Y1 = 1.290158F;
            this.line23.Y2 = 0.8070865F;
            // 
            // line24
            // 
            this.line24.Height = 0.4830709F;
            this.line24.Left = 7.575984F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.8070871F;
            this.line24.Width = 9.536743E-07F;
            this.line24.X1 = 7.575985F;
            this.line24.X2 = 7.575984F;
            this.line24.Y1 = 1.290158F;
            this.line24.Y2 = 0.8070871F;
            // 
            // line25
            // 
            this.line25.Height = 0.4723514F;
            this.line25.Left = 0.01181102F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.8059956F;
            this.line25.Width = 0F;
            this.line25.X1 = 0.01181102F;
            this.line25.X2 = 0.01181102F;
            this.line25.Y1 = 1.278347F;
            this.line25.Y2 = 0.8059956F;
            // 
            // line4
            // 
            this.line4.Height = 0.4830709F;
            this.line4.Left = 9.134646F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.8070872F;
            this.line4.Width = 0F;
            this.line4.X1 = 9.134646F;
            this.line4.X2 = 9.134646F;
            this.line4.Y1 = 1.290158F;
            this.line4.Y2 = 0.8070872F;
            // 
            // line5
            // 
            this.line5.Height = 0.4724414F;
            this.line5.Left = 11.29685F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.8059056F;
            this.line5.Width = 0F;
            this.line5.X1 = 11.29685F;
            this.line5.X2 = 11.29685F;
            this.line5.Y1 = 1.278347F;
            this.line5.Y2 = 0.8059056F;
            // 
            // line6
            // 
            this.line6.Height = 0.47174F;
            this.line6.Left = 8.139765F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.807F;
            this.line6.Width = 0.0002355576F;
            this.line6.X1 = 8.139765F;
            this.line6.X2 = 8.14F;
            this.line6.Y1 = 1.27874F;
            this.line6.Y2 = 0.807F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue20,
            this.txtValue21,
            this.txtValue01,
            this.txtValue02,
            this.txtValue03,
            this.txtValue10,
            this.txtValue11,
            this.txtValue06,
            this.txtValue12,
            this.txtValue05,
            this.txtValue04,
            this.txtValue07,
            this.txtValue08,
            this.txtValue09,
            this.txtValue13,
            this.txtValue14,
            this.txtValue15,
            this.txtValue16,
            this.txtValue18,
            this.txtValue19,
            this.line27,
            this.line30,
            this.txtValue22,
            this.txtValue23,
            this.txtValue24,
            this.txtValue25,
            this.line15,
            this.line28,
            this.line29,
            this.line14,
            this.line13,
            this.line12,
            this.line11,
            this.line10,
            this.line7,
            this.line26,
            this.line8,
            this.line9,
            this.txtValue26,
            this.txtValue27});
            this.detail.Height = 0.3338637F;
            this.detail.Name = "detail";
            // 
            // txtValue20
            // 
            this.txtValue20.DataField = "ITEM20";
            this.txtValue20.Height = 0.07952756F;
            this.txtValue20.Left = 9.207088F;
            this.txtValue20.LineSpacing = 1F;
            this.txtValue20.MultiLine = false;
            this.txtValue20.Name = "txtValue20";
            this.txtValue20.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue20.Text = null;
            this.txtValue20.Top = 0.1220473F;
            this.txtValue20.Width = 0.101181F;
            // 
            // txtValue21
            // 
            this.txtValue21.DataField = "ITEM21";
            this.txtValue21.Height = 0.07952756F;
            this.txtValue21.Left = 9.329528F;
            this.txtValue21.LineSpacing = 1F;
            this.txtValue21.MultiLine = false;
            this.txtValue21.Name = "txtValue21";
            this.txtValue21.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue21.Text = null;
            this.txtValue21.Top = 0.1220473F;
            this.txtValue21.Width = 0.8854336F;
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM02";
            this.txtValue01.Height = 0.2980315F;
            this.txtValue01.Left = 0.01181102F;
            this.txtValue01.LineSpacing = 1F;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0.01417323F;
            this.txtValue01.Width = 0.4374016F;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM03";
            this.txtValue02.Height = 0.1354331F;
            this.txtValue02.Left = 0.5413386F;
            this.txtValue02.LineSpacing = 1F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0.01417323F;
            this.txtValue02.Width = 1.92126F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM04";
            this.txtValue03.Height = 0.1456693F;
            this.txtValue03.Left = 0.5413386F;
            this.txtValue03.LineSpacing = 1F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue03.Text = "\r\n";
            this.txtValue03.Top = 0.1665355F;
            this.txtValue03.Width = 1.92126F;
            // 
            // txtValue10
            // 
            this.txtValue10.DataField = "ITEM11";
            this.txtValue10.Height = 0.2980315F;
            this.txtValue10.Left = 6.823623F;
            this.txtValue10.LineSpacing = 1F;
            this.txtValue10.Name = "txtValue10";
            this.txtValue10.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtValue10.Text = null;
            this.txtValue10.Top = 0.01417323F;
            this.txtValue10.Width = 0.6377952F;
            // 
            // txtValue11
            // 
            this.txtValue11.DataField = "ITEM12";
            this.txtValue11.Height = 0.1314961F;
            this.txtValue11.Left = 7.655512F;
            this.txtValue11.LineSpacing = 1F;
            this.txtValue11.Name = "txtValue11";
            this.txtValue11.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue11.Text = null;
            this.txtValue11.Top = 0.01417323F;
            this.txtValue11.Width = 0.484252F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM07";
            this.txtValue06.Height = 0.2980315F;
            this.txtValue06.Left = 4.580315F;
            this.txtValue06.LineSpacing = 1F;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtValue06.Text = null;
            this.txtValue06.Top = 0.007086615F;
            this.txtValue06.Width = 0.4299216F;
            // 
            // txtValue12
            // 
            this.txtValue12.DataField = "ITEM13";
            this.txtValue12.Height = 0.1488189F;
            this.txtValue12.Left = 7.65945F;
            this.txtValue12.LineSpacing = 1F;
            this.txtValue12.Name = "txtValue12";
            this.txtValue12.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue12.Text = "\r\n";
            this.txtValue12.Top = 0.1633858F;
            this.txtValue12.Width = 0.4803152F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM06";
            this.txtValue05.Height = 0.2980315F;
            this.txtValue05.Left = 3.820473F;
            this.txtValue05.LineSpacing = 1F;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0.007086615F;
            this.txtValue05.Width = 0.7598424F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM05";
            this.txtValue04.Height = 0.2980315F;
            this.txtValue04.Left = 2.524803F;
            this.txtValue04.LineSpacing = 1F;
            this.txtValue04.MultiLine = false;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0.007086615F;
            this.txtValue04.Width = 1.222441F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM08";
            this.txtValue07.Height = 0.2980315F;
            this.txtValue07.Left = 5.138189F;
            this.txtValue07.LineSpacing = 1F;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0.01417323F;
            this.txtValue07.Width = 0.3751969F;
            // 
            // txtValue08
            // 
            this.txtValue08.DataField = "ITEM09";
            this.txtValue08.Height = 0.2980315F;
            this.txtValue08.Left = 5.513386F;
            this.txtValue08.LineSpacing = 1F;
            this.txtValue08.Name = "txtValue08";
            this.txtValue08.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtValue08.Text = null;
            this.txtValue08.Top = 0.007086615F;
            this.txtValue08.Width = 0.5688977F;
            // 
            // txtValue09
            // 
            this.txtValue09.DataField = "ITEM10";
            this.txtValue09.Height = 0.3051181F;
            this.txtValue09.Left = 6.168504F;
            this.txtValue09.LineSpacing = 1F;
            this.txtValue09.Name = "txtValue09";
            this.txtValue09.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtValue09.Text = null;
            this.txtValue09.Top = 0F;
            this.txtValue09.Width = 0.6027559F;
            // 
            // txtValue13
            // 
            this.txtValue13.DataField = "ITEM14";
            this.txtValue13.Height = 0.1354331F;
            this.txtValue13.Left = 8.181497F;
            this.txtValue13.LineSpacing = 1F;
            this.txtValue13.MultiLine = false;
            this.txtValue13.Name = "txtValue13";
            this.txtValue13.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue13.Text = null;
            this.txtValue13.Top = 0.01417323F;
            this.txtValue13.Width = 0.1354332F;
            // 
            // txtValue14
            // 
            this.txtValue14.DataField = "ITEM15";
            this.txtValue14.Height = 0.1314961F;
            this.txtValue14.Left = 8.353544F;
            this.txtValue14.LineSpacing = 1F;
            this.txtValue14.MultiLine = false;
            this.txtValue14.Name = "txtValue14";
            this.txtValue14.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue14.Text = null;
            this.txtValue14.Top = 0.01417323F;
            this.txtValue14.Width = 0.7759848F;
            // 
            // txtValue15
            // 
            this.txtValue15.DataField = "ITEM16";
            this.txtValue15.Height = 0.1314961F;
            this.txtValue15.Left = 8.181497F;
            this.txtValue15.LineSpacing = 1F;
            this.txtValue15.MultiLine = false;
            this.txtValue15.Name = "txtValue15";
            this.txtValue15.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue15.Text = null;
            this.txtValue15.Top = 0.1736221F;
            this.txtValue15.Width = 0.1354332F;
            // 
            // txtValue16
            // 
            this.txtValue16.DataField = "ITEM17";
            this.txtValue16.Height = 0.1314961F;
            this.txtValue16.Left = 8.353544F;
            this.txtValue16.LineSpacing = 1F;
            this.txtValue16.MultiLine = false;
            this.txtValue16.Name = "txtValue16";
            this.txtValue16.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue16.Text = null;
            this.txtValue16.Top = 0.1807087F;
            this.txtValue16.Width = 0.7708664F;
            // 
            // txtValue18
            // 
            this.txtValue18.DataField = "ITEM18";
            this.txtValue18.Height = 0.08267716F;
            this.txtValue18.Left = 9.207088F;
            this.txtValue18.LineSpacing = 1F;
            this.txtValue18.MultiLine = false;
            this.txtValue18.Name = "txtValue18";
            this.txtValue18.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue18.Text = null;
            this.txtValue18.Top = 0.01850394F;
            this.txtValue18.Width = 0.101181F;
            // 
            // txtValue19
            // 
            this.txtValue19.DataField = "ITEM19";
            this.txtValue19.Height = 0.08267718F;
            this.txtValue19.Left = 9.329528F;
            this.txtValue19.LineSpacing = 1F;
            this.txtValue19.MultiLine = false;
            this.txtValue19.Name = "txtValue19";
            this.txtValue19.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue19.Text = null;
            this.txtValue19.Top = 0.01850394F;
            this.txtValue19.Width = 0.8854332F;
            // 
            // line27
            // 
            this.line27.Height = 0.3299213F;
            this.line27.Left = 7.575985F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 9.313226E-10F;
            this.line27.Width = 0F;
            this.line27.X1 = 7.575985F;
            this.line27.X2 = 7.575985F;
            this.line27.Y1 = 0.3299213F;
            this.line27.Y2 = 9.313226E-10F;
            // 
            // line30
            // 
            this.line30.Height = 0F;
            this.line30.Left = 0.01181102F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 0.3299213F;
            this.line30.Width = 11.28504F;
            this.line30.X1 = 0.01181102F;
            this.line30.X2 = 11.29685F;
            this.line30.Y1 = 0.3299213F;
            this.line30.Y2 = 0.3299213F;
            // 
            // txtValue22
            // 
            this.txtValue22.DataField = "ITEM22";
            this.txtValue22.Height = 0.09015749F;
            this.txtValue22.Left = 9.207088F;
            this.txtValue22.LineSpacing = 1F;
            this.txtValue22.MultiLine = false;
            this.txtValue22.Name = "txtValue22";
            this.txtValue22.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue22.Text = null;
            this.txtValue22.Top = 0.2220473F;
            this.txtValue22.Width = 0.1015749F;
            // 
            // txtValue23
            // 
            this.txtValue23.DataField = "ITEM23";
            this.txtValue23.Height = 0.09724411F;
            this.txtValue23.Left = 9.329528F;
            this.txtValue23.LineSpacing = 1F;
            this.txtValue23.MultiLine = false;
            this.txtValue23.Name = "txtValue23";
            this.txtValue23.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue23.Text = null;
            this.txtValue23.Top = 0.2149606F;
            this.txtValue23.Width = 0.8854332F;
            // 
            // txtValue24
            // 
            this.txtValue24.DataField = "ITEM24";
            this.txtValue24.Height = 0.08267716F;
            this.txtValue24.Left = 10.24882F;
            this.txtValue24.LineSpacing = 1F;
            this.txtValue24.MultiLine = false;
            this.txtValue24.Name = "txtValue24";
            this.txtValue24.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue24.Text = null;
            this.txtValue24.Top = 0.01850394F;
            this.txtValue24.Width = 0.1015749F;
            // 
            // txtValue25
            // 
            this.txtValue25.DataField = "ITEM25";
            this.txtValue25.Height = 0.08267716F;
            this.txtValue25.Left = 10.37126F;
            this.txtValue25.LineSpacing = 1F;
            this.txtValue25.MultiLine = false;
            this.txtValue25.Name = "txtValue25";
            this.txtValue25.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue25.Text = null;
            this.txtValue25.Top = 0.01850394F;
            this.txtValue25.Width = 0.8854332F;
            // 
            // line15
            // 
            this.line15.Height = 0.3299213F;
            this.line15.Left = 11.29685F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 9.313226E-10F;
            this.line15.Width = 0F;
            this.line15.X1 = 11.29685F;
            this.line15.X2 = 11.29685F;
            this.line15.Y1 = 0.3299213F;
            this.line15.Y2 = 9.313226E-10F;
            // 
            // line28
            // 
            this.line28.Height = 0.3299213F;
            this.line28.Left = 8.139765F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 9.313226E-10F;
            this.line28.Width = 0F;
            this.line28.X1 = 8.139765F;
            this.line28.X2 = 8.139765F;
            this.line28.Y1 = 0.3299213F;
            this.line28.Y2 = 9.313226E-10F;
            // 
            // line29
            // 
            this.line29.Height = 0.3299213F;
            this.line29.Left = 9.134646F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 9.313226E-10F;
            this.line29.Width = 0F;
            this.line29.X1 = 9.134646F;
            this.line29.X2 = 9.134646F;
            this.line29.Y1 = 0.3299213F;
            this.line29.Y2 = 9.313226E-10F;
            // 
            // line14
            // 
            this.line14.Height = 0.3299213F;
            this.line14.Left = 6.823623F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 9.313226E-10F;
            this.line14.Width = 0F;
            this.line14.X1 = 6.823623F;
            this.line14.X2 = 6.823623F;
            this.line14.Y1 = 0.3299213F;
            this.line14.Y2 = 9.313226E-10F;
            // 
            // line13
            // 
            this.line13.Height = 0.3299213F;
            this.line13.Left = 6.168504F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0.007086636F;
            this.line13.Width = 0F;
            this.line13.X1 = 6.168504F;
            this.line13.X2 = 6.168504F;
            this.line13.Y1 = 0.3370079F;
            this.line13.Y2 = 0.007086636F;
            // 
            // line12
            // 
            this.line12.Height = 0.3299213F;
            this.line12.Left = 5.513386F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 9.313226E-10F;
            this.line12.Width = 0F;
            this.line12.X1 = 5.513386F;
            this.line12.X2 = 5.513386F;
            this.line12.Y1 = 0.3299213F;
            this.line12.Y2 = 9.313226E-10F;
            // 
            // line11
            // 
            this.line11.Height = 0.3299213F;
            this.line11.Left = 5.072835F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 9.313226E-10F;
            this.line11.Width = 0F;
            this.line11.X1 = 5.072835F;
            this.line11.X2 = 5.072835F;
            this.line11.Y1 = 0.3299213F;
            this.line11.Y2 = 9.313226E-10F;
            // 
            // line10
            // 
            this.line10.Height = 0.3299213F;
            this.line10.Left = 4.580315F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 9.313226E-10F;
            this.line10.Width = 0F;
            this.line10.X1 = 4.580315F;
            this.line10.X2 = 4.580315F;
            this.line10.Y1 = 0.3299213F;
            this.line10.Y2 = 9.313226E-10F;
            // 
            // line7
            // 
            this.line7.Height = 0.3299213F;
            this.line7.Left = 0.468504F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 0.468504F;
            this.line7.X2 = 0.468504F;
            this.line7.Y1 = 0.3299213F;
            this.line7.Y2 = 0F;
            // 
            // line26
            // 
            this.line26.Height = 0.3299213F;
            this.line26.Left = 0.01181102F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 9.313226E-10F;
            this.line26.Width = 0F;
            this.line26.X1 = 0.01181102F;
            this.line26.X2 = 0.01181102F;
            this.line26.Y1 = 0.3299213F;
            this.line26.Y2 = 9.313226E-10F;
            // 
            // line8
            // 
            this.line8.Height = 0.3299213F;
            this.line8.Left = 2.462599F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 9.313226E-10F;
            this.line8.Width = 0F;
            this.line8.X1 = 2.462599F;
            this.line8.X2 = 2.462599F;
            this.line8.Y1 = 0.3299213F;
            this.line8.Y2 = 9.313226E-10F;
            // 
            // line9
            // 
            this.line9.Height = 0.3299213F;
            this.line9.Left = 3.747244F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 9.313226E-10F;
            this.line9.Width = 0F;
            this.line9.X1 = 3.747244F;
            this.line9.X2 = 3.747244F;
            this.line9.Y1 = 0.3299213F;
            this.line9.Y2 = 9.313226E-10F;
            // 
            // txtValue26
            // 
            this.txtValue26.DataField = "ITEM26";
            this.txtValue26.Height = 0.08267716F;
            this.txtValue26.Left = 10.25079F;
            this.txtValue26.LineSpacing = 1F;
            this.txtValue26.MultiLine = false;
            this.txtValue26.Name = "txtValue26";
            this.txtValue26.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue26.Text = null;
            this.txtValue26.Top = 0.1322835F;
            this.txtValue26.Width = 0.09960669F;
            // 
            // txtValue27
            // 
            this.txtValue27.DataField = "ITEM27";
            this.txtValue27.Height = 0.08267716F;
            this.txtValue27.Left = 10.37126F;
            this.txtValue27.LineSpacing = 1F;
            this.txtValue27.MultiLine = false;
            this.txtValue27.Name = "txtValue27";
            this.txtValue27.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue27.Text = null;
            this.txtValue27.Top = 0.1393701F;
            this.txtValue27.Width = 0.8854332F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KBCM1021R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.1968504F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 11.29685F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
    }
}
