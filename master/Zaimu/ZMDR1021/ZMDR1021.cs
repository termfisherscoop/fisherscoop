﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmdr1021
{
    /// <summary>
    /// 仕訳帳(ZAMR1051)
    /// </summary>
    public partial class ZMDR1021 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDR1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblGengoFr.Text = jpDate[0];
            txtYearFr.Text = jpDate[2];
            txtMonthFr.Text = jpDate[3];
            txtDayFr.Text = jpDate[4];

            lblGengoTo.Text = jpDate[0];
            txtYearTo.Text = jpDate[2];
            txtMonthTo.Text = jpDate[3];
            txtDayTo.Text = jpDate[4];

            //rdoZenShiwake.Checked = true;
            //rdoTujoShiwake.Checked = true;
            rdoZeikomi.Checked = true;
            rdoDateJun.Checked = true;

            // 初期フォーカス
            //rdoZenShiwake.Select();
            //rdoZenShiwake.Focus();
            //rdoTujoShiwake.Select();
            //rdoTujoShiwake.Focus();
            string val = "0";
            try
            {
                val = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1021", "Setting", "ShiwakeKbn"));
                switch (val)
                {
                    case "0":
                        rdoTujoShiwake.Checked = true;
                        rdoTujoShiwake.Select();
                        rdoTujoShiwake.Focus();
                        break;
                    case "1":
                        rdoKessanShiwake.Checked = true;
                        rdoKessanShiwake.Select();
                        rdoKessanShiwake.Focus(); break;
                    case "2":
                        rdoZenShiwake.Checked = true;
                        rdoZenShiwake.Select();
                        rdoZenShiwake.Focus();
                        break;
                    default:
                        rdoTujoShiwake.Checked = true;
                        rdoTujoShiwake.Select();
                        rdoTujoShiwake.Focus(); break;
                }
            }
            catch (Exception)
            {
                rdoTujoShiwake.Checked = true;
                rdoTujoShiwake.Select();
                rdoTujoShiwake.Focus();
            }
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付,船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYearFr":
                case "txtYearTo":
                case "txtTantoshaFr":
                case "txtTantoshaTo":
                case "txtKanjoKamokuFr":
                case "txtKanjoKamokuTo":
                case "txtBumonFr":
                case "txtBumonTo":
                case "txtKojiFr":
                case "txtKojiTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" +"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYearFr":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpFr();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYearTo":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpTo();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTantoshaFr":
                    #region 担当者検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9041.KOBC9041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoshaFr.Text = outData[0];
                                this.lblTantoshaFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTantoshaTo":
                    #region 担当者検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9041.KOBC9041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoshaTo.Text = outData[0];
                                this.lblTantoshaTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKanjoKamokuFr":
                    #region 勘定科目検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9011");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoKamokuFr.Text = outData[0];
                                this.lblKanjoKamokuFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKanjoKamokuTo":
                    #region 勘定科目検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9011");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoKamokuTo.Text = outData[0];
                                this.lblKanjoKamokuTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtBumonFr":
                case "txtBumonTo":
                    #region 部門検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    //asm = Assembly.LoadFrom("CMCM2041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "TB_CM_BUMON";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                if (this.ActiveCtlNm.Equals("txtBumonFr"))
                                {
                                    this.txtBumonFr.Text = result[0];
                                    this.lblBumonFr.Text = result[1];
                                }
                                else
                                {
                                    this.txtBumonTo.Text = result[0];
                                    this.lblBumonTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;

                /*
                // TODO:工事項目を現場で使用していないため、一旦コメントアウト
                case "txtKojiFr":
                case "txtKojiTo":
                    Msg.Notice("TODO:工事検索画面を起動しないといけない");
                    break;
                 */
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            using (PrintSettingForm psForm = new PrintSettingForm(new string[1] { "ZMDR10211R" }))
            {
                psForm.ShowDialog();
            }
        }
        #endregion

        #region イベント

        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtYearFr.SelectAll();
            }
            else
            {
                this.txtYearFr.Text = Util.ToString(IsValid.SetYear(this.txtYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthFr.SelectAll();
            }
            else
            {
                this.txtMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayFr.SelectAll();
            }
            else
            {
                this.txtDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDayFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtYearTo.SelectAll();
            }
            else
            {
                this.txtYearTo.Text = Util.ToString(IsValid.SetYear(this.txtYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthTo.SelectAll();
            }
            else
            {
                this.txtMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayTo.SelectAll();
            }
            else
            {
                this.txtDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDayTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 担当者コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblTantoshaFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", Util.ToString(txtMizuageShishoCd.Text), this.txtTantoshaFr.Text);
            if (ValChk.IsEmpty(this.lblTantoshaFr.Text) || this.lblTantoshaFr.Text.Equals(""))
            {
                this.lblTantoshaFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 担当者コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblTantoshaTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", Util.ToString(txtMizuageShishoCd.Text), this.txtTantoshaTo.Text);
            if (ValChk.IsEmpty(this.lblTantoshaTo.Text) || this.lblTantoshaTo.Text.Equals(""))
            {
                this.lblTantoshaTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 勘定科目コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKanjoKamokuFr.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", Util.ToString(txtMizuageShishoCd.Text), this.txtKanjoKamokuFr.Text);
            if (ValChk.IsEmpty(this.lblKanjoKamokuFr.Text) || this.lblKanjoKamokuFr.Text.Equals(""))
            {
                this.lblKanjoKamokuFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 勘定科目コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKanjoKamokuTo.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", Util.ToString(txtMizuageShishoCd.Text), this.txtKanjoKamokuTo.Text);
            if (ValChk.IsEmpty(this.lblKanjoKamokuTo.Text) || this.lblKanjoKamokuTo.Text.Equals(""))
            {
                this.lblKanjoKamokuTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblBumonFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", Util.ToString(txtMizuageShishoCd.Text), this.txtBumonFr.Text);
            if (ValChk.IsEmpty(this.lblBumonFr.Text) || this.lblBumonFr.Text.Equals(""))
            {
                this.lblBumonFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblBumonTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", Util.ToString(txtMizuageShishoCd.Text), this.txtBumonTo.Text);
            if (ValChk.IsEmpty(this.lblBumonTo.Text) || this.lblBumonTo.Text.Equals(""))
            {
                this.lblBumonTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtKojiTo.Focus();
                }
            }
        }

        /// <summary>
        /// 工事コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojiFr_Validating(object sender, CancelEventArgs e)
        {
            //if (!IsValidKojiFr())
            //{
            //    e.Cancel = true;
            //    this.txtKojiFr.SelectAll();
            //}
        }

        /// <summary>
        /// 工事コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojiTo_Validating(object sender, CancelEventArgs e)
        {
            //if (!IsValidKojiTo())
            //{
            //    e.Cancel = true;
            //    this.txtKojiTo.SelectAll();

            //    // Enter処理を無効化
            //    this._dtFlg = false;
            //}
            //else
            //{
            //    // Enter処理を有効化
            //    this._dtFlg = true;
            //}
        }

        /// <summary>
        /// 工事コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojiTo_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter && this.Flg)
            //{
            //    // Enter処理を無効化
            //    this._dtFlg = false;

            //    // 全項目を再度入力値チェック
            //    if (!ValidateAll())
            //    {
            //        // エラーありの場合ここで処理終了
            //        return;
            //    }

            //    if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            //    {
            //        // ﾌﾟﾚﾋﾞｭｰ処理
            //        DoPrint(true);
            //    }
            //    else
            //    {
            //        this.txtKojiTo.Focus();
            //    }
            //}
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayFr.Text) > lastDayInMonth)
            {
                this.txtDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoFr.Text,
                                  this.txtYearFr.Text,
                                  this.txtMonthFr.Text,
                                  this.txtDayFr.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayTo.Text) > lastDayInMonth)
            {
                this.txtDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoTo.Text,
                                  this.txtYearTo.Text,
                                  this.txtMonthTo.Text,
                                  this.txtDayTo.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 伝票番号(自)の入力チェック
        /// </summary>
        private bool IsValidDenpyoBangoFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDenpyoBangoFr.Text))
            {
                Msg.Error("伝票番号は数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 伝票番号(至)の入力チェック
        /// </summary>
        private bool IsValidDenpyoBangoTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDenpyoBangoTo.Text))
            {
                Msg.Error("伝票番号は数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 担当者コード(自)の入力チェック
        /// </summary>
        private bool IsValidTantoshaFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoshaFr.Text))
            {
                Msg.Error("担当者コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 担当者コード(至)の入力チェック
        /// </summary>
        private bool IsValidTantoshaTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoshaTo.Text))
            {
                Msg.Error("担当者コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 勘定科目コード(自)の入力チェック
        /// </summary>
        private bool IsValidKanjoKamokuFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamokuFr.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 勘定科目コード(至)の入力チェック
        /// </summary>
        private bool IsValidKanjoKamokuTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamokuTo.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 部門コード(自)の入力チェック
        /// </summary>
        private bool IsValidBumonFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonFr.Text))
            {
                Msg.Error("部門コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 部門コード(至)の入力チェック
        /// </summary>
        private bool IsValidBumonTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonTo.Text))
            {
                Msg.Error("部門コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                this.txtYearFr.Focus();
                this.txtYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                this.txtMonthFr.Focus();
                this.txtMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                this.txtDayFr.Focus();
                this.txtDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                this.txtYearTo.Focus();
                this.txtYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                this.txtMonthTo.Focus();
                this.txtMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                this.txtDayTo.Focus();
                this.txtDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            // 伝票番号(自)の入力チェック
            if (!IsValidDenpyoBangoFr())
            {
                this.txtDenpyoBangoFr.Focus();
                this.txtDenpyoBangoFr.SelectAll();
                return false;
            }
            // 伝票番号(至)の入力チェック
            if (!IsValidDenpyoBangoTo())
            {
                this.txtDenpyoBangoTo.Focus();
                this.txtDenpyoBangoTo.SelectAll();
                return false;
            }

            // 担当者コード(自)の入力チェック
            if (!IsValidTantoshaFr())
            {
                this.txtTantoshaFr.Focus();
                this.txtTantoshaFr.SelectAll();
                return false;
            }
            // 担当者コード(至)の入力チェック
            if (!IsValidTantoshaTo())
            {
                this.txtTantoshaTo.Focus();
                this.txtTantoshaTo.SelectAll();
                return false;
            }

            // 勘定科目コード(自)の入力チェック
            if (!IsValidKanjoKamokuFr())
            {
                this.txtKanjoKamokuFr.Focus();
                this.txtKanjoKamokuFr.SelectAll();
                return false;
            }
            // 勘定科目コード(至)の入力チェック
            if (!IsValidKanjoKamokuTo())
            {
                this.txtKanjoKamokuTo.Focus();
                this.txtKanjoKamokuTo.SelectAll();
                return false;
            }

            // 部門コード(自)の入力チェック
            if (!IsValidBumonFr())
            {
                this.txtBumonFr.Focus();
                this.txtBumonFr.SelectAll();
                return false;
            }
            // 部門コード(至)の入力チェック
            if (!IsValidBumonTo())
            {
                this.txtBumonTo.Focus();
                this.txtBumonTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する。
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.AppendLine("  ITEM01");
                    cols.AppendLine(" ,ITEM02");
                    cols.AppendLine(" ,ITEM03");
                    cols.AppendLine(" ,ITEM04");
                    cols.AppendLine(" ,ITEM05");
                    cols.AppendLine(" ,ITEM06");
                    cols.AppendLine(" ,ITEM07");
                    cols.AppendLine(" ,ITEM08");
                    cols.AppendLine(" ,ITEM09");
                    cols.AppendLine(" ,ITEM10");
                    cols.AppendLine(" ,ITEM11");
                    cols.AppendLine(" ,ITEM12");
                    cols.AppendLine(" ,ITEM13");
                    cols.AppendLine(" ,ITEM14");
                    cols.AppendLine(" ,ITEM15");
                    cols.AppendLine(" ,ITEM16");
                    cols.AppendLine(" ,ITEM17");
                    cols.AppendLine(" ,ITEM18");
                    cols.AppendLine(" ,ITEM19");
                    cols.AppendLine(" ,ITEM20");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMDR10211R rpt = new ZMDR10211R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    // 捺印欄名
                    List<string> nmList = new List<string>() { "組合長", "課長", "係長", "照査", "起票" };
                    try
                    {
                        string[] nml;
                        nml = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1021", "Setting", "nameList")).Split(',');
                        nmList = new List<string>(nml);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message); ;
                    }
                    rpt.nmList = nmList;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        //// 帳票オブジェクトをインスタンス化
                        //ZMDR10211R rpt = new ZMDR10211R(dtOutput);
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        // プレビュー画面表示
                        pFrm.Show();
                    }
                    else
                    {
                        //// 帳票オブジェクトをインスタンス化
                        //ZMDR10211R rpt = new ZMDR10211R(dtOutput);
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            #region 準備
            // 日付を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                    this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                    this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);
            DateTime nowDate = System.DateTime.Now;

            // 日付を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpnowDate = Util.ConvJpDate(nowDate, this.Dba);

            // 税区分設定
            string zeiKubun;
            if (rdoZeikomi.Checked == true)
            {
                //zeiKubun = "【税込み】";
                zeiKubun = "【" + this.rdoZeikomi.Text + "】";
            }
            else
            {
                //zeiKubun = "【税抜き】";
                zeiKubun = "【" + this.rdoZeinuki.Text + "】";
            }

            // 支所コード
            int shishoCd = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));

            // 伝票番号設定
            string denpyoFr;
            string denpyoTo;
            if (Util.ToString(txtDenpyoBangoFr.Text) != "")
            {
                denpyoFr = txtDenpyoBangoFr.Text;
            }
            else
            {
                denpyoFr = "0";
            }
            if (Util.ToString(txtDenpyoBangoTo.Text) != "")
            {
                denpyoTo = txtDenpyoBangoTo.Text;
            }
            else
            {
                denpyoTo = "999999";
            }

            // 担当者コード設定
            string tantoshaFr;
            string tantoshaTo;
            if (Util.ToString(txtTantoshaFr.Text) != "")
            {
                tantoshaFr = txtTantoshaFr.Text;
            }
            else
            {
                tantoshaFr = "0";
            }
            if (Util.ToString(txtTantoshaTo.Text) != "")
            {
                tantoshaTo = txtTantoshaTo.Text;
            }
            else
            {
                tantoshaTo = "9999";
            }

            // 勘定科目コード設定
            string kanjoKamokuFr;
            string kanjoKamokuTo;
            if (Util.ToString(txtKanjoKamokuFr.Text) != "")
            {
                kanjoKamokuFr = txtKanjoKamokuFr.Text;
            }
            else
            {
                kanjoKamokuFr = "0";
            }
            if (Util.ToString(txtKanjoKamokuTo.Text) != "")
            {
                kanjoKamokuTo = txtKanjoKamokuTo.Text;
            }
            else
            {
                kanjoKamokuTo = "999999";
            }

            // 部門設定
            string bumonFr;
            string bumonTo;
            if (Util.ToString(txtBumonFr.Text) != "")
            {
                bumonFr = txtBumonFr.Text;
            }
            else
            {
                bumonFr = "0";
            }
            if (Util.ToString(txtBumonTo.Text) != "")
            {
                bumonTo = txtBumonTo.Text;
            }
            else
            {
                bumonTo = "9999";
            }

            //// 工事設定
            //string kojiFr;
            //string kojiTo;
            //if (Util.ToString(txtKojiFr.Text) != "")
            //{
            //    kojiFr = txtKojiFr.Text;
            //}
            //else
            //{
            //    kojiFr = "0";
            //}
            //if (Util.ToString(txtKojiTo.Text) != "")
            //{
            //    kojiTo = txtKojiTo.Text;
            //}
            //else
            //{
            //    kojiTo = "9999";
            //}
            #endregion

            #region IN句に使用するための伝票番号を取得
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            Sql.AppendLine(" SELECT");
            Sql.AppendLine("    DISTINCT   A.DENPYO_BANGO   AS DENPYO_BANGO");
            Sql.AppendLine(" FROM");
            Sql.AppendLine("    TB_ZM_SHIWAKE_MEISAI AS A ");
            Sql.AppendLine(" LEFT OUTER JOIN TB_ZM_SHIWAKE_DENPYO AS B ");

            //Sql.AppendLine("    ON (A.DENPYO_BANGO = B.DENPYO_BANGO) AND (A.KAISHA_CD = B.KAISHA_CD) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");
            Sql.AppendLine("    ON (A.KAISHA_CD = B.KAISHA_CD) AND (A.SHISHO_CD = B.SHISHO_CD) AND (A.DENPYO_BANGO = B.DENPYO_BANGO) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");

            Sql.AppendLine(" LEFT OUTER JOIN VI_ZM_KANJO_KAMOKU AS C");
            Sql.AppendLine("    ON (A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD) AND (A.KAISHA_CD = C.KAISHA_CD) AND (A.KAIKEI_NENDO = C.KAIKEI_NENDO)");
            Sql.AppendLine(" LEFT OUTER JOIN VI_ZM_HOJO_KAMOKU AS D");

            //Sql.AppendLine("    ON (A.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD) AND (A.HOJO_KAMOKU_CD = D.HOJO_KAMOKU_CD) AND (C.KAISHA_CD = D.KAISHA_CD) AND (A.KAIKEI_NENDO = D.KAIKEI_NENDO)");
            Sql.AppendLine("    ON (C.KAISHA_CD = D.KAISHA_CD) AND ((A.SHISHO_CD = D.SHISHO_CD or D.SHISHO_CD is null)) AND (A.KAIKEI_NENDO = D.KAIKEI_NENDO) AND (A.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD) AND (A.HOJO_KAMOKU_CD = D.HOJO_KAMOKU_CD)");

            Sql.AppendLine(" LEFT OUTER JOIN TB_CM_BUMON AS E");
            Sql.AppendLine("    ON (A.BUMON_CD = E.BUMON_CD) AND (A.KAISHA_CD = E.KAISHA_CD)");
            Sql.AppendLine(" LEFT OUTER JOIN VI_ZM_ZEI_KUBUN AS F");
            Sql.AppendLine("    ON (A.ZEI_KUBUN = F.ZEI_KUBUN)");
            Sql.AppendLine(" WHERE ");
            Sql.AppendLine(" 	(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.KANJO_KAMOKU_CD ELSE  -1 END BETWEEN @KANJO_KAMOKU_FR AND @KANJO_KAMOKU_TO OR");
            Sql.AppendLine(" 	CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.KANJO_KAMOKU_CD ELSE  -1 END BETWEEN @KANJO_KAMOKU_FR AND @KANJO_KAMOKU_TO) AND");
            Sql.AppendLine("    A.KAISHA_CD = @KAISHA_CD AND");

            if (shishoCd != 0)
                Sql.AppendLine("    A.SHISHO_CD = @SHISHO_CD AND");

            Sql.AppendLine("    A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.AppendLine("    A.DENPYO_BANGO BETWEEN @DENPYO_BANGO_FR AND @DENPYO_BANGO_TO AND");
            Sql.AppendLine("    B.TANTOSHA_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO AND");
            Sql.AppendLine("    A.BUMON_CD BETWEEN @BUMON_FR AND @BUMON_TO AND");
            //Sql.AppendLine("    A.KOJI_CD BETWEEN @KOJI_FR AND @KOJI_TO AND");
            Sql.AppendLine("    A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO");
            Sql.AppendLine(" GROUP BY");
            Sql.AppendLine("    A.KAISHA_CD,");

            if (shishoCd != 0)
                Sql.AppendLine("    A.SHISHO_CD,");

            Sql.AppendLine("    A.DENPYO_DATE,");
            Sql.AppendLine("    A.DENPYO_BANGO,");
            Sql.AppendLine("    A.GYO_BANGO,");
            Sql.AppendLine("    A.MEISAI_KUBUN");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            // 検索する日付
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            // 検索する伝票番号
            dpc.SetParam("@DENPYO_BANGO_FR", SqlDbType.Decimal, 6, denpyoFr);
            dpc.SetParam("@DENPYO_BANGO_TO", SqlDbType.Decimal, 6, denpyoTo);
            // 検索する担当者コード
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, tantoshaFr);
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, tantoshaTo);
            // 検索する勘定科目
            dpc.SetParam("@KANJO_KAMOKU_FR", SqlDbType.Decimal, 6, kanjoKamokuFr);
            dpc.SetParam("@KANJO_KAMOKU_TO", SqlDbType.Decimal, 6, kanjoKamokuTo);
            // 検索する部門
            dpc.SetParam("@BUMON_FR", SqlDbType.Decimal, 4, bumonFr);
            dpc.SetParam("@BUMON_TO", SqlDbType.Decimal, 4, bumonTo);
            //// 検索する工事
            //dpc.SetParam("@KOJI_FR", SqlDbType.Decimal, 4, kojiFr);
            //dpc.SetParam("@KOJI_TO", SqlDbType.Decimal, 4, kojiTo);

            DataTable dtDenpyoBango = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            int l = 1;
            int count = dtDenpyoBango.Rows.Count;
            if (count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            string denpyoBango = ""; // Where用変数
            foreach (DataRow dr in dtDenpyoBango.Rows)
            {
                denpyoBango += Util.ToString(dr["DENPYO_BANGO"]);
                if (count != l)
                {
                    denpyoBango += ",";
                }
                l++;
            }

            int i = 1; // ループ用カウント変数
            Decimal kariKingaku = 0;
            Decimal kariShohizei = 0;
            Decimal kashiKingaku = 0;
            Decimal kashiShohizei = 0;

            #region 検索日付に発生しているデータを取得
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            Sql.AppendLine(" SELECT");
            Sql.AppendLine("     A.KAISHA_CD         AS KAISHA_CD,");
            Sql.AppendLine("     A.SHISHO_CD         AS SHISHO_CD,");
            Sql.AppendLine("     A.DENPYO_DATE       AS DENPYO_DATE,");
            Sql.AppendLine("     A.DENPYO_BANGO      AS DENPYO_BANGO,");
            Sql.AppendLine("     A.GYO_BANGO         AS GYO_BANGO,");
            Sql.AppendLine("     A.MEISAI_KUBUN      AS MEISAI_KUBUN,");
            Sql.AppendLine("     MAX(B.SHOHYO_BANGO) AS SHOHYO_BANGO,");
            Sql.AppendLine("     MAX(B.DENPYO_KUBUN) AS SHIWAKE_DENPYO_DENPYO_KUBUN,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.KANJO_KAMOKU_CD ELSE  0 END)   AS KARIKATA_KANJO_KAMOKU_CD,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN C.KANJO_KAMOKU_NM ELSE ' ' END)  AS KARIKATA_KANJO_KAMOKU_NM,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN D.HOJO_KAMOKU_CD ELSE  0 END)    AS KARIKATA_HOJO_KAMOKU_CD,");

            // 補助科目の取得は使用区分で判断する事に
            //Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN D.HOJO_KAMOKU_NM ELSE ' ' END)   AS KARIKATA_HOJO_KAMOKU_NM,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN ");
            Sql.AppendLine("              (CASE WHEN C.HOJO_SHIYO_KUBUN=1 THEN D.HOJO_KAMOKU_NM ");
            Sql.AppendLine("                    WHEN C.HOJO_SHIYO_KUBUN=2 THEN G.TORIHIKISAKI_NM ELSE '' END) ");
            Sql.AppendLine("              ELSE ' ' END) AS KARIKATA_HOJO_KAMOKU_NM,");

            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.BUMON_CD ELSE  0 END)          AS KARIKATA_BUMON_CD,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN E.BUMON_NM ELSE ' ' END)         AS KARIKATA_BUMON_NM,");
            Sql.AppendLine("     SUM(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.ZEIKOMI_KINGAKU ELSE  0 END)   AS KARIKATA_KINGAKU,");
            Sql.AppendLine("     SUM(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.SHOHIZEI_KINGAKU ELSE  0 END)  AS KARIKATA_SHOHIZEI_KINGAKU,");

            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.ZEI_KUBUN ELSE  0 END)         AS KARIKATA_ZEI_KUBUN,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN F.ZEI_KUBUN_NM ELSE ' ' END)     AS KARIKATA_ZEI_KUBUN_NM,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.KAZEI_KUBUN ELSE  0 END)       AS KARIKATA_KAZEI_KUBUN,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.TORIHIKI_KUBUN ELSE  0 END)    AS KARIKATA_TORIHIKI_KUBUN,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.ZEI_RITSU ELSE 0 END)          AS KARIKATA_ZEIRITSU,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.JIGYO_KUBUN ELSE  0 END)       AS KARIKATA_JIGYO_KUBUN,");

            //Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN B.URIAGE_KINGAKU ELSE  0 END)    AS KARIKATA_SHIWAKE_DENPYO_URIAGE_KINGAKU,");
            //Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN B.SHOHIZEIGAKU ELSE  0 END)      AS KARIKATA_SHIWAKE_DENPYO_SHOHIZEIGAKU,");

            Sql.AppendLine("     MAX(A.TEKIYO) AS TEKIYO,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.KANJO_KAMOKU_CD ELSE  0 END)  AS KASHIKATA_KANJO_KAMOKU_CD,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN C.KANJO_KAMOKU_NM ELSE ' ' END) AS KASHIKATA_KANJO_KAMOKU_NM,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN D.HOJO_KAMOKU_CD ELSE 0 END)    AS KASHIKATA_HOJO_KAMOKU_CD,");

            // 補助科目の取得は使用区分で判断する事に
            //Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN D.HOJO_KAMOKU_NM ELSE ' ' END)  AS KASHIKATA_HOJO_KAMOKU_NM,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN ");
            Sql.AppendLine("              (CASE WHEN C.HOJO_SHIYO_KUBUN=1 THEN D.HOJO_KAMOKU_NM ");
            Sql.AppendLine("                    WHEN C.HOJO_SHIYO_KUBUN=2 THEN G.TORIHIKISAKI_NM ELSE '' END) ");
            Sql.AppendLine("              ELSE ' ' END) AS KASHIKATA_HOJO_KAMOKU_NM,");

            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.BUMON_CD ELSE 0 END)          AS KASHIKATA_BUMON_CD,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN E.BUMON_NM ELSE ' ' END)        AS KASHIKATA_BUMON_NM,");

            //Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN B.URIAGE_KINGAKU ELSE  0 END)   AS KASHIKATA_SHIWAKE_DENPYO_URIAGE_KINGAKU,");
            //Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN B.SHOHIZEIGAKU ELSE  0 END)     AS KASHIKATA_SHIWAKE_DENPYO_SHOHIZEIGAKU,");
            //Sql.AppendLine("     MAX(A.KOJI_CD) AS KOJI_CD,");

            Sql.AppendLine("     SUM(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.ZEIKOMI_KINGAKU ELSE 0 END)   AS KASHIKATA_KINGAKU,");
            Sql.AppendLine("     SUM(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.SHOHIZEI_KINGAKU ELSE 0 END)  AS KASHIKATA_SHOHIZEI_KINGAKU,");

            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.ZEI_KUBUN ELSE 0 END)         AS KASHIKATA_ZEI_KUBUN,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN F.ZEI_KUBUN_NM ELSE ' ' END)    AS KASHIKATA_ZEI_KUBUN_NM,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.KAZEI_KUBUN ELSE 0 END)       AS KASHIKATA_KAZEI_KUBUN,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.TORIHIKI_KUBUN ELSE 0 END)    AS KASHIKATA_TORIHIKI_KUBUN,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.ZEI_RITSU ELSE 0 END)         AS KASHIKATA_ZEIRITSU,");
            Sql.AppendLine("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.JIGYO_KUBUN ELSE 0 END)       AS KASHIKATA_JIGYO_KUBUN");
            Sql.AppendLine(" FROM");
            Sql.AppendLine("     TB_ZM_SHIWAKE_MEISAI AS A ");
            Sql.AppendLine(" LEFT OUTER JOIN TB_ZM_SHIWAKE_DENPYO AS B ");
            //Sql.AppendLine("     ON (A.DENPYO_BANGO = B.DENPYO_BANGO) AND (A.KAISHA_CD = B.KAISHA_CD) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");
            Sql.AppendLine("     ON (A.KAISHA_CD = B.KAISHA_CD) AND (A.SHISHO_CD = B.SHISHO_CD) AND (A.DENPYO_BANGO = B.DENPYO_BANGO) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");

            Sql.AppendLine(" LEFT OUTER JOIN VI_ZM_KANJO_KAMOKU AS C");
            Sql.AppendLine("     ON (A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD) AND (A.KAISHA_CD = C.KAISHA_CD) AND (A.KAIKEI_NENDO = C.KAIKEI_NENDO)");
            Sql.AppendLine(" LEFT OUTER JOIN VI_ZM_HOJO_KAMOKU AS D");
            //Sql.AppendLine("     ON (A.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD) AND (A.HOJO_KAMOKU_CD = D.HOJO_KAMOKU_CD) AND (C.KAISHA_CD = D.KAISHA_CD) AND (A.KAIKEI_NENDO = D.KAIKEI_NENDO)");
            Sql.AppendLine("     ON (C.KAISHA_CD = D.KAISHA_CD) AND ((A.SHISHO_CD = D.SHISHO_CD or D.SHISHO_CD is null)) AND (A.KAIKEI_NENDO = D.KAIKEI_NENDO) AND (A.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD) AND (A.HOJO_KAMOKU_CD = D.HOJO_KAMOKU_CD)");

            Sql.AppendLine(" LEFT OUTER JOIN TB_CM_BUMON AS E");
            Sql.AppendLine("     ON (A.BUMON_CD = E.BUMON_CD) AND (A.KAISHA_CD = E.KAISHA_CD)");
            Sql.AppendLine(" LEFT OUTER JOIN VI_ZM_ZEI_KUBUN AS F");
            Sql.AppendLine("     ON (A.ZEI_KUBUN = F.ZEI_KUBUN)");
            Sql.AppendLine(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS G");
            Sql.AppendLine("     ON (A.HOJO_KAMOKU_CD = G.TORIHIKISAKI_CD) AND (A.KAISHA_CD = G.KAISHA_CD)");
            Sql.AppendLine(" WHERE ");
            Sql.AppendLine(" 	A.KAISHA_CD = @KAISHA_CD AND");

            if (shishoCd != 0)
                Sql.AppendLine("    A.SHISHO_CD = @SHISHO_CD AND");

            Sql.AppendLine(" 	A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.AppendLine(" 	A.DENPYO_BANGO BETWEEN @DENPYO_BANGO_FR AND @DENPYO_BANGO_TO AND");
            Sql.AppendLine(" 	B.TANTOSHA_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO AND");
            Sql.AppendLine(" 	A.BUMON_CD BETWEEN @BUMON_FR AND @BUMON_TO AND");
            //Sql.AppendLine(" 	A.KOJI_CD BETWEEN @KOJI_FR AND @KOJI_TO AND");
            if (this.rdoTujoShiwake.Checked == true)
            {
                Sql.AppendLine("     A.KESSAN_KUBUN = 0 AND");
            }
            else
                if (this.rdoKessanShiwake.Checked == true)
                {
                    Sql.AppendLine("     A.KESSAN_KUBUN = 1 AND");
                }
            Sql.AppendLine(" 	A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND ");
            Sql.AppendLine("    A.DENPYO_BANGO IN(" + denpyoBango + ")");
            Sql.AppendLine(" GROUP BY");
            Sql.AppendLine(" 	A.KAISHA_CD,");
            Sql.AppendLine("    A.SHISHO_CD,");
            Sql.AppendLine(" 	A.DENPYO_DATE,");
            Sql.AppendLine(" 	A.DENPYO_BANGO,");
            Sql.AppendLine(" 	A.GYO_BANGO,");
            Sql.AppendLine(" 	A.MEISAI_KUBUN");
            if (rdoDateJun.Checked == true)
            {
                Sql.AppendLine(" ORDER BY A.DENPYO_DATE");
            }
            else
                if (rdoBangoJun.Checked == true)
                {
                    Sql.AppendLine(" ORDER BY A.DENPYO_BANGO");
                }

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            // 検索する日付
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            // 検索する伝票番号
            dpc.SetParam("@DENPYO_BANGO_FR", SqlDbType.Decimal, 6, denpyoFr);
            dpc.SetParam("@DENPYO_BANGO_TO", SqlDbType.Decimal, 6, denpyoTo);
            // 検索する担当者コード
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, tantoshaFr);
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, tantoshaTo);
            // 検索する部門
            dpc.SetParam("@BUMON_FR", SqlDbType.Decimal, 4, bumonFr);
            dpc.SetParam("@BUMON_TO", SqlDbType.Decimal, 4, bumonTo);
            //// 検索する工事
            //dpc.SetParam("@KOJI_FR", SqlDbType.Decimal, 4, kojiFr);
            //dpc.SetParam("@KOJI_TO", SqlDbType.Decimal, 4, kojiTo);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            //// コントロールブレイク用伝票番号
            //decimal oldDenpyoBango = -1;

            //// 伝票区分
            //int denpyoKubun = 0;

            if (dtMainLoop.Rows.Count > 0)
            {
                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    //// 伝票区分
                    //denpyoKubun = Util.ToInt(dr["DENPYO_BANGO"]);

                    //// 食堂の場合は、１伝票につき１行だけ書き込む
                    //if (denpyoKubun == 8 && oldDenpyoBango == Util.ToDecimal(dr["DENPYO_BANGO"]))
                    //{
                    //    continue;
                    //}
                    //oldDenpyoBango = Util.ToDecimal(dr["DENPYO_BANGO"]);

                    if (rdoZeikomi.Checked == true)
                    {
                        // 税込みの時の処理
                        if (Util.ToInt(dr["MEISAI_KUBUN"]) == 0)
                        {
                            //// 食堂（伝票区分が8）なら、仕訳伝票の売上金額を書き込む
                            //if (denpyoKubun == 8)
                            //{
                            //    kariShohizei = Util.ToDecimal(dr["KARIKATA_SHIWAKE_DENPYO_SHOHIZEIGAKU"]);
                            //    kariKingaku = kariShohizei + Util.ToDecimal(dr["KARIKATA_SHIWAKE_DENPYO_URIAGE_KINGAKU"]);
                            //    kashiShohizei = Util.ToDecimal(dr["KASHIKATA_SHIWAKE_DENPYO_SHOHIZEIGAKU"]);
                            //    kashiKingaku = kashiShohizei + Util.ToDecimal(dr["KASHIKATA_SHIWAKE_DENPYO_URIAGE_KINGAKU"]);
                            //}
                            //else
                            //{
                            //    kariKingaku = Util.ToDecimal(dr["KARIKATA_KINGAKU"]);
                            //    kariShohizei = Util.ToDecimal(dr["KARIKATA_SHOHIZEI_KINGAKU"]);
                            //    kashiKingaku = Util.ToDecimal(dr["KASHIKATA_KINGAKU"]);
                            //    kashiShohizei = Util.ToDecimal(dr["KASHIKATA_SHOHIZEI_KINGAKU"]);
                            //}
                            kariKingaku = Util.ToDecimal(dr["KARIKATA_KINGAKU"]);
                            kariShohizei = Util.ToDecimal(dr["KARIKATA_SHOHIZEI_KINGAKU"]);
                            kashiKingaku = Util.ToDecimal(dr["KASHIKATA_KINGAKU"]);
                            kashiShohizei = Util.ToDecimal(dr["KASHIKATA_SHOHIZEI_KINGAKU"]);

                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.AppendLine("INSERT INTO PR_ZM_TBL(");
                            Sql.AppendLine("  GUID");
                            Sql.AppendLine(" ,SORT");
                            Sql.AppendLine(" ,ITEM01");
                            Sql.AppendLine(" ,ITEM02");
                            Sql.AppendLine(" ,ITEM03");
                            Sql.AppendLine(" ,ITEM04");
                            Sql.AppendLine(" ,ITEM05");
                            Sql.AppendLine(" ,ITEM06");
                            Sql.AppendLine(" ,ITEM07");
                            Sql.AppendLine(" ,ITEM08");
                            Sql.AppendLine(" ,ITEM09");
                            Sql.AppendLine(" ,ITEM10");
                            Sql.AppendLine(" ,ITEM11");
                            Sql.AppendLine(" ,ITEM12");
                            Sql.AppendLine(" ,ITEM13");
                            Sql.AppendLine(" ,ITEM14");
                            Sql.AppendLine(" ,ITEM15");
                            Sql.AppendLine(" ,ITEM16");
                            Sql.AppendLine(" ,ITEM17");
                            Sql.AppendLine(" ,ITEM18");
                            Sql.AppendLine(" ,ITEM19");
                            Sql.AppendLine(" ,ITEM20");
                            Sql.AppendLine(") ");
                            Sql.AppendLine("VALUES(");
                            Sql.AppendLine("  @GUID");
                            Sql.AppendLine(" ,@SORT");
                            Sql.AppendLine(" ,@ITEM01");
                            Sql.AppendLine(" ,@ITEM02");
                            Sql.AppendLine(" ,@ITEM03");
                            Sql.AppendLine(" ,@ITEM04");
                            Sql.AppendLine(" ,@ITEM05");
                            Sql.AppendLine(" ,@ITEM06");
                            Sql.AppendLine(" ,@ITEM07");
                            Sql.AppendLine(" ,@ITEM08");
                            Sql.AppendLine(" ,@ITEM09");
                            Sql.AppendLine(" ,@ITEM10");
                            Sql.AppendLine(" ,@ITEM11");
                            Sql.AppendLine(" ,@ITEM12");
                            Sql.AppendLine(" ,@ITEM13");
                            Sql.AppendLine(" ,@ITEM14");
                            Sql.AppendLine(" ,@ITEM15");
                            Sql.AppendLine(" ,@ITEM16");
                            Sql.AppendLine(" ,@ITEM17");
                            Sql.AppendLine(" ,@ITEM18");
                            Sql.AppendLine(" ,@ITEM19");
                            Sql.AppendLine(" ,@ITEM20");
                            Sql.AppendLine(") ");

                            // 日付の設定
                            string[] date = Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba);

                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5] + " ～ " + tmpjpDateTo[5]);
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, zeiKubun);
                            // データを設定
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                                string.Format("{0}/{1}/{2}", date[2], date[3], date[4]));
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["DENPYO_BANGO"]);
                            if (0.Equals(Util.ToInt(dr["KARIKATA_KANJO_KAMOKU_CD"])))
                            {
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                            }
                            else
                            {
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["KARIKATA_KANJO_KAMOKU_CD"]);
                            }
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["KARIKATA_KANJO_KAMOKU_NM"]);
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["KARIKATA_HOJO_KAMOKU_NM"]);
                            if (0.Equals(Util.ToInt(dr["KARIKATA_BUMON_CD"])))
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "");
                            }
                            else
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["KARIKATA_BUMON_CD"]);
                            }
                            if (0.Equals(Util.ToInt(dr["KARIKATA_ZEI_KUBUN"])))
                            {
                                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "");
                            }
                            else
                            {
                                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["KARIKATA_ZEI_KUBUN"]);
                            }

                            if (kariKingaku == 0)
                            {
                                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, null);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(kariKingaku));
                            }

                            if (0.Equals(Util.ToInt(dr["KASHIKATA_KANJO_KAMOKU_CD"])))
                            {
                                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "");
                            }
                            else
                            {
                                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["KASHIKATA_KANJO_KAMOKU_CD"]);
                            }
                            dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dr["KASHIKATA_KANJO_KAMOKU_NM"]);
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dr["KASHIKATA_HOJO_KAMOKU_NM"]);
                            if (0.Equals(Util.ToInt(dr["KASHIKATA_BUMON_CD"])))
                            {
                                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, "");
                            }
                            else
                            {
                                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dr["KASHIKATA_BUMON_CD"]);
                            }
                            if (0.Equals(Util.ToInt(dr["KASHIKATA_ZEI_KUBUN"])))
                            {
                                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "");
                            }
                            else
                            {
                                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dr["KASHIKATA_ZEI_KUBUN"]);
                            }

                            if (kashiKingaku == 0)
                            {
                                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, null);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(kashiKingaku));
                            }
                            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, dr["TEKIYO"]);

                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, tmpnowDate[5]);
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToString(dr["SHISHO_CD"]));

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            i++;
                        }
                    }
                    else
                    {
                        // 税抜きの時の処理
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.AppendLine("INSERT INTO PR_ZM_TBL(");
                        Sql.AppendLine("  GUID");
                        Sql.AppendLine(" ,SORT");
                        Sql.AppendLine(" ,ITEM01");
                        Sql.AppendLine(" ,ITEM02");
                        Sql.AppendLine(" ,ITEM03");
                        Sql.AppendLine(" ,ITEM04");
                        Sql.AppendLine(" ,ITEM05");
                        Sql.AppendLine(" ,ITEM06");
                        Sql.AppendLine(" ,ITEM07");
                        Sql.AppendLine(" ,ITEM08");
                        Sql.AppendLine(" ,ITEM09");
                        Sql.AppendLine(" ,ITEM10");
                        Sql.AppendLine(" ,ITEM11");
                        Sql.AppendLine(" ,ITEM12");
                        Sql.AppendLine(" ,ITEM13");
                        Sql.AppendLine(" ,ITEM14");
                        Sql.AppendLine(" ,ITEM15");
                        Sql.AppendLine(" ,ITEM16");
                        Sql.AppendLine(" ,ITEM17");
                        Sql.AppendLine(" ,ITEM18");
                        Sql.AppendLine(" ,ITEM19");
                        Sql.AppendLine(" ,ITEM20");
                        Sql.AppendLine(") ");
                        Sql.AppendLine("VALUES(");
                        Sql.AppendLine("  @GUID");
                        Sql.AppendLine(" ,@SORT");
                        Sql.AppendLine(" ,@ITEM01");
                        Sql.AppendLine(" ,@ITEM02");
                        Sql.AppendLine(" ,@ITEM03");
                        Sql.AppendLine(" ,@ITEM04");
                        Sql.AppendLine(" ,@ITEM05");
                        Sql.AppendLine(" ,@ITEM06");
                        Sql.AppendLine(" ,@ITEM07");
                        Sql.AppendLine(" ,@ITEM08");
                        Sql.AppendLine(" ,@ITEM09");
                        Sql.AppendLine(" ,@ITEM10");
                        Sql.AppendLine(" ,@ITEM11");
                        Sql.AppendLine(" ,@ITEM12");
                        Sql.AppendLine(" ,@ITEM13");
                        Sql.AppendLine(" ,@ITEM14");
                        Sql.AppendLine(" ,@ITEM15");
                        Sql.AppendLine(" ,@ITEM16");
                        Sql.AppendLine(" ,@ITEM17");
                        Sql.AppendLine(" ,@ITEM18");
                        Sql.AppendLine(" ,@ITEM19");
                        Sql.AppendLine(" ,@ITEM20");
                        Sql.AppendLine(") ");

                        if (Util.ToInt(dr["MEISAI_KUBUN"]) == 0)
                        {
                            //// 食堂（伝票区分が8）なら、仕訳伝票の売上金額を書き込む
                            //if (denpyoKubun == 8)
                            //{
                            //    kariKingaku = Util.ToDecimal(dr["KARIKATA_SHIWAKE_DENPYO_URIAGE_KINGAKU"]);
                            //    kariShohizei = Util.ToDecimal(dr["KARIKATA_SHIWAKE_DENPYO_SHOHIZEIGAKU"]);
                            //    kashiKingaku = Util.ToDecimal(dr["KASHIKATA_SHIWAKE_DENPYO_URIAGE_KINGAKU"]);
                            //    kashiShohizei = Util.ToDecimal(dr["KASHIKATA_SHIWAKE_DENPYO_SHOHIZEIGAKU"]);
                            //}
                            //else
                            //{
                            //    kariKingaku = Util.ToDecimal(dr["KARIKATA_KINGAKU"]) - Util.ToDecimal(dr["KARIKATA_SHOHIZEI_KINGAKU"]);
                            //    kariShohizei = Util.ToDecimal(dr["KARIKATA_SHOHIZEI_KINGAKU"]);
                            //    kashiKingaku = Util.ToDecimal(dr["KASHIKATA_KINGAKU"]) - Util.ToDecimal(dr["KASHIKATA_SHOHIZEI_KINGAKU"]);
                            //    kashiShohizei = Util.ToDecimal(dr["KASHIKATA_SHOHIZEI_KINGAKU"]);
                            //}
                            kariKingaku = Util.ToDecimal(dr["KARIKATA_KINGAKU"]) - Util.ToDecimal(dr["KARIKATA_SHOHIZEI_KINGAKU"]);
                            kariShohizei = Util.ToDecimal(dr["KARIKATA_SHOHIZEI_KINGAKU"]);
                            kashiKingaku = Util.ToDecimal(dr["KASHIKATA_KINGAKU"]) - Util.ToDecimal(dr["KASHIKATA_SHOHIZEI_KINGAKU"]);
                            kashiShohizei = Util.ToDecimal(dr["KASHIKATA_SHOHIZEI_KINGAKU"]);
                        }

                        // 日付の設定
                        string[] date = Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba);

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5] + " ～ " + tmpjpDateTo[5]);
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, zeiKubun);
                        // データを設定
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                            string.Format("{0}/{1}/{2}", date[2], date[3], date[4]));
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["DENPYO_BANGO"]);
                        if (0.Equals(Util.ToInt(dr["KARIKATA_KANJO_KAMOKU_CD"])))
                        {
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["KARIKATA_KANJO_KAMOKU_CD"]);
                        }
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["KARIKATA_KANJO_KAMOKU_NM"]);
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["KARIKATA_HOJO_KAMOKU_NM"]);
                        if (0.Equals(Util.ToInt(dr["KARIKATA_BUMON_CD"])))
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["KARIKATA_BUMON_CD"]);
                        }
                        if (0.Equals(Util.ToInt(dr["KARIKATA_ZEI_KUBUN"])))
                        {
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["KARIKATA_ZEI_KUBUN"]);
                        }
                        if (Util.ToInt(dr["MEISAI_KUBUN"]) == 0)
                        {
                            if (kariKingaku == 0)
                            {
                                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, null);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(kariKingaku));
                            }
                        }
                        else
                        {
                            if (kariShohizei == 0)
                            {
                                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, null);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(kariShohizei));
                            }
                        }
                        if (0.Equals(Util.ToInt(dr["KASHIKATA_KANJO_KAMOKU_CD"])))
                        {
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["KASHIKATA_KANJO_KAMOKU_CD"]);
                        }
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dr["KASHIKATA_KANJO_KAMOKU_NM"]);
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dr["KASHIKATA_HOJO_KAMOKU_NM"]);
                        if (0.Equals(Util.ToInt(dr["KASHIKATA_BUMON_CD"])))
                        {
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dr["KASHIKATA_BUMON_CD"]);
                        }
                        if (0.Equals(Util.ToInt(dr["KASHIKATA_ZEI_KUBUN"])))
                        {
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dr["KASHIKATA_ZEI_KUBUN"]);
                        }
                        if (Util.ToInt(dr["MEISAI_KUBUN"]) == 0)
                        {
                            if (kashiKingaku == 0)
                            {
                                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, null);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(kashiKingaku));
                            }
                        }
                        else
                        {
                            if (kashiShohizei == 0)
                            {
                                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, null);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(kashiShohizei));
                            }
                        }
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, dr["TEKIYO"]);

                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, tmpnowDate[5]);
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.ToString(dr["SHISHO_CD"]));

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        i++;
                    }
                }
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_ZM_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_ZM_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }

        /// <summary>
        /// 会計年度内日付に変換
        /// </summary>
        private DateTime FixNendoDate(DateTime date)
        {
            DateTime dateFr = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            DateTime dateTo = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }
        #endregion
    }
}
