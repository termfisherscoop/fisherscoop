﻿namespace jp.co.fsi.zm.zmmr1031
{
    partial class ZMMR1033
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.F1 = new System.Windows.Forms.TabPage();
            this.mtbListF1 = new System.Windows.Forms.DataGridView();
            this.F1VisibleHandan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1kamokuCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1gyoBango = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1Taishaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1Moji = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1deleteAndAddNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1hyojiJuni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2 = new System.Windows.Forms.TabPage();
            this.mtbListF2 = new System.Windows.Forms.DataGridView();
            this.F2VisibleHandan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2kamokuCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2gyoBango = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2Taishaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2Moji = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2deleteAndAddNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2hyojiJuni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3 = new System.Windows.Forms.TabPage();
            this.mtbListF3 = new System.Windows.Forms.DataGridView();
            this.F3VisibleHandan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3kamokuCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3gyoBango = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3Taishaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3Moji = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3deleteAndAddNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3hyojiJuni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblMemo = new System.Windows.Forms.Label();
            this.txtMoji = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMoji = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.lbxTaishoKanjoKamoku = new System.Windows.Forms.ListBox();
            this.txtKamokuNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.lbxKanjoKamokuIchiran = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.rdoKashi = new System.Windows.Forms.RadioButton();
            this.rdoKari = new System.Windows.Forms.RadioButton();
            this.Label3 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.F1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF1)).BeginInit();
            this.F2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF2)).BeginInit();
            this.F3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF3)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(4, 65);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF1
            // 
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(89, 65);
            this.btnF6.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 649);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Text = "総勘定元帳 [印字設定]";
            this.lblTitle.Visible = false;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.F1);
            this.tabControl.Controls.Add(this.F2);
            this.tabControl.Controls.Add(this.F3);
            this.tabControl.Location = new System.Drawing.Point(8, 16);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(517, 609);
            this.tabControl.TabIndex = 1;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // F1
            // 
            this.F1.Controls.Add(this.mtbListF1);
            this.F1.Location = new System.Drawing.Point(4, 26);
            this.F1.Margin = new System.Windows.Forms.Padding(4);
            this.F1.Name = "F1";
            this.F1.Padding = new System.Windows.Forms.Padding(4);
            this.F1.Size = new System.Drawing.Size(509, 579);
            this.F1.TabIndex = 0;
            this.F1.Text = "F1：貸借対照表";
            this.F1.UseVisualStyleBackColor = true;
            // 
            // mtbListF1
            // 
            this.mtbListF1.AllowUserToAddRows = false;
            this.mtbListF1.AllowUserToDeleteRows = false;
            this.mtbListF1.AllowUserToResizeRows = false;
            this.mtbListF1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbListF1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.mtbListF1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbListF1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F1VisibleHandan,
            this.F1kamokuCd,
            this.F1KamokuNm,
            this.F1gyoBango,
            this.F1Taishaku,
            this.F1Moji,
            this.F1deleteAndAddNo,
            this.F1hyojiJuni});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mtbListF1.DefaultCellStyle = dataGridViewCellStyle6;
            this.mtbListF1.EnableHeadersVisualStyles = false;
            this.mtbListF1.Location = new System.Drawing.Point(8, 8);
            this.mtbListF1.Margin = new System.Windows.Forms.Padding(4);
            this.mtbListF1.MultiSelect = false;
            this.mtbListF1.Name = "mtbListF1";
            this.mtbListF1.ReadOnly = true;
            this.mtbListF1.RowHeadersVisible = false;
            this.mtbListF1.RowTemplate.Height = 21;
            this.mtbListF1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbListF1.Size = new System.Drawing.Size(497, 567);
            this.mtbListF1.TabIndex = 3;
            this.mtbListF1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellClick);
            // 
            // F1VisibleHandan
            // 
            this.F1VisibleHandan.HeaderText = "Visible判断";
            this.F1VisibleHandan.Name = "F1VisibleHandan";
            this.F1VisibleHandan.ReadOnly = true;
            this.F1VisibleHandan.Visible = false;
            // 
            // F1kamokuCd
            // 
            this.F1kamokuCd.HeaderText = "科目コード";
            this.F1kamokuCd.Name = "F1kamokuCd";
            this.F1kamokuCd.ReadOnly = true;
            this.F1kamokuCd.Visible = false;
            // 
            // F1KamokuNm
            // 
            this.F1KamokuNm.HeaderText = "科　目　名";
            this.F1KamokuNm.Name = "F1KamokuNm";
            this.F1KamokuNm.ReadOnly = true;
            this.F1KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F1KamokuNm.Width = 250;
            // 
            // F1gyoBango
            // 
            this.F1gyoBango.HeaderText = "行番号";
            this.F1gyoBango.Name = "F1gyoBango";
            this.F1gyoBango.ReadOnly = true;
            this.F1gyoBango.Visible = false;
            // 
            // F1Taishaku
            // 
            this.F1Taishaku.HeaderText = "貸借";
            this.F1Taishaku.Name = "F1Taishaku";
            this.F1Taishaku.ReadOnly = true;
            this.F1Taishaku.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F1Taishaku.Width = 50;
            // 
            // F1Moji
            // 
            this.F1Moji.HeaderText = "文字";
            this.F1Moji.Name = "F1Moji";
            this.F1Moji.ReadOnly = true;
            this.F1Moji.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F1Moji.Width = 50;
            // 
            // F1deleteAndAddNo
            // 
            this.F1deleteAndAddNo.HeaderText = "削除と追加用行番号";
            this.F1deleteAndAddNo.Name = "F1deleteAndAddNo";
            this.F1deleteAndAddNo.ReadOnly = true;
            this.F1deleteAndAddNo.Visible = false;
            // 
            // F1hyojiJuni
            // 
            this.F1hyojiJuni.HeaderText = "表示順位";
            this.F1hyojiJuni.Name = "F1hyojiJuni";
            this.F1hyojiJuni.ReadOnly = true;
            this.F1hyojiJuni.Visible = false;
            // 
            // F2
            // 
            this.F2.Controls.Add(this.mtbListF2);
            this.F2.Location = new System.Drawing.Point(4, 26);
            this.F2.Margin = new System.Windows.Forms.Padding(4);
            this.F2.Name = "F2";
            this.F2.Padding = new System.Windows.Forms.Padding(4);
            this.F2.Size = new System.Drawing.Size(509, 579);
            this.F2.TabIndex = 1;
            this.F2.Text = "F2：損益計算書";
            this.F2.UseVisualStyleBackColor = true;
            // 
            // mtbListF2
            // 
            this.mtbListF2.AllowUserToAddRows = false;
            this.mtbListF2.AllowUserToDeleteRows = false;
            this.mtbListF2.AllowUserToResizeRows = false;
            this.mtbListF2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbListF2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.mtbListF2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbListF2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F2VisibleHandan,
            this.F2kamokuCd,
            this.F2KamokuNm,
            this.F2gyoBango,
            this.F2Taishaku,
            this.F2Moji,
            this.F2deleteAndAddNo,
            this.F2hyojiJuni});
            this.mtbListF2.EnableHeadersVisualStyles = false;
            this.mtbListF2.Location = new System.Drawing.Point(8, 8);
            this.mtbListF2.Margin = new System.Windows.Forms.Padding(4);
            this.mtbListF2.MultiSelect = false;
            this.mtbListF2.Name = "mtbListF2";
            this.mtbListF2.ReadOnly = true;
            this.mtbListF2.RowHeadersVisible = false;
            this.mtbListF2.RowTemplate.Height = 21;
            this.mtbListF2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbListF2.Size = new System.Drawing.Size(485, 570);
            this.mtbListF2.TabIndex = 4;
            this.mtbListF2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellClick);
            // 
            // F2VisibleHandan
            // 
            this.F2VisibleHandan.HeaderText = "Visible判断";
            this.F2VisibleHandan.Name = "F2VisibleHandan";
            this.F2VisibleHandan.ReadOnly = true;
            this.F2VisibleHandan.Visible = false;
            // 
            // F2kamokuCd
            // 
            this.F2kamokuCd.HeaderText = "科目コード";
            this.F2kamokuCd.Name = "F2kamokuCd";
            this.F2kamokuCd.ReadOnly = true;
            this.F2kamokuCd.Visible = false;
            // 
            // F2KamokuNm
            // 
            this.F2KamokuNm.HeaderText = "科　目　名";
            this.F2KamokuNm.Name = "F2KamokuNm";
            this.F2KamokuNm.ReadOnly = true;
            this.F2KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F2KamokuNm.Width = 250;
            // 
            // F2gyoBango
            // 
            this.F2gyoBango.HeaderText = "行番号";
            this.F2gyoBango.Name = "F2gyoBango";
            this.F2gyoBango.ReadOnly = true;
            this.F2gyoBango.Visible = false;
            // 
            // F2Taishaku
            // 
            this.F2Taishaku.HeaderText = "貸借";
            this.F2Taishaku.Name = "F2Taishaku";
            this.F2Taishaku.ReadOnly = true;
            this.F2Taishaku.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F2Taishaku.Width = 50;
            // 
            // F2Moji
            // 
            this.F2Moji.HeaderText = "文字";
            this.F2Moji.Name = "F2Moji";
            this.F2Moji.ReadOnly = true;
            this.F2Moji.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F2Moji.Width = 50;
            // 
            // F2deleteAndAddNo
            // 
            this.F2deleteAndAddNo.HeaderText = "削除と追加用行番号";
            this.F2deleteAndAddNo.Name = "F2deleteAndAddNo";
            this.F2deleteAndAddNo.ReadOnly = true;
            this.F2deleteAndAddNo.Visible = false;
            // 
            // F2hyojiJuni
            // 
            this.F2hyojiJuni.HeaderText = "表示順位";
            this.F2hyojiJuni.Name = "F2hyojiJuni";
            this.F2hyojiJuni.ReadOnly = true;
            this.F2hyojiJuni.Visible = false;
            // 
            // F3
            // 
            this.F3.Controls.Add(this.mtbListF3);
            this.F3.Location = new System.Drawing.Point(4, 26);
            this.F3.Margin = new System.Windows.Forms.Padding(4);
            this.F3.Name = "F3";
            this.F3.Size = new System.Drawing.Size(509, 579);
            this.F3.TabIndex = 2;
            this.F3.Text = "F3：製造原価";
            this.F3.UseVisualStyleBackColor = true;
            // 
            // mtbListF3
            // 
            this.mtbListF3.AllowUserToAddRows = false;
            this.mtbListF3.AllowUserToDeleteRows = false;
            this.mtbListF3.AllowUserToResizeRows = false;
            this.mtbListF3.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbListF3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.mtbListF3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbListF3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F3VisibleHandan,
            this.F3kamokuCd,
            this.F3KamokuNm,
            this.F3gyoBango,
            this.F3Taishaku,
            this.F3Moji,
            this.F3deleteAndAddNo,
            this.F3hyojiJuni});
            this.mtbListF3.EnableHeadersVisualStyles = false;
            this.mtbListF3.Location = new System.Drawing.Point(8, 8);
            this.mtbListF3.Margin = new System.Windows.Forms.Padding(4);
            this.mtbListF3.MultiSelect = false;
            this.mtbListF3.Name = "mtbListF3";
            this.mtbListF3.ReadOnly = true;
            this.mtbListF3.RowHeadersVisible = false;
            this.mtbListF3.RowTemplate.Height = 21;
            this.mtbListF3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbListF3.Size = new System.Drawing.Size(488, 567);
            this.mtbListF3.TabIndex = 5;
            this.mtbListF3.Visible = false;
            this.mtbListF3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellClick);
            // 
            // F3VisibleHandan
            // 
            this.F3VisibleHandan.HeaderText = "Visible判断";
            this.F3VisibleHandan.Name = "F3VisibleHandan";
            this.F3VisibleHandan.ReadOnly = true;
            this.F3VisibleHandan.Visible = false;
            // 
            // F3kamokuCd
            // 
            this.F3kamokuCd.HeaderText = "科目コード";
            this.F3kamokuCd.Name = "F3kamokuCd";
            this.F3kamokuCd.ReadOnly = true;
            this.F3kamokuCd.Visible = false;
            // 
            // F3KamokuNm
            // 
            this.F3KamokuNm.HeaderText = "科　目　名";
            this.F3KamokuNm.Name = "F3KamokuNm";
            this.F3KamokuNm.ReadOnly = true;
            this.F3KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F3KamokuNm.Width = 250;
            // 
            // F3gyoBango
            // 
            this.F3gyoBango.HeaderText = "行番号";
            this.F3gyoBango.Name = "F3gyoBango";
            this.F3gyoBango.ReadOnly = true;
            this.F3gyoBango.Visible = false;
            // 
            // F3Taishaku
            // 
            this.F3Taishaku.HeaderText = "貸借";
            this.F3Taishaku.Name = "F3Taishaku";
            this.F3Taishaku.ReadOnly = true;
            this.F3Taishaku.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F3Taishaku.Width = 50;
            // 
            // F3Moji
            // 
            this.F3Moji.HeaderText = "文字";
            this.F3Moji.Name = "F3Moji";
            this.F3Moji.ReadOnly = true;
            this.F3Moji.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F3Moji.Width = 50;
            // 
            // F3deleteAndAddNo
            // 
            this.F3deleteAndAddNo.HeaderText = "削除と追加用行番号";
            this.F3deleteAndAddNo.Name = "F3deleteAndAddNo";
            this.F3deleteAndAddNo.ReadOnly = true;
            this.F3deleteAndAddNo.Visible = false;
            // 
            // F3hyojiJuni
            // 
            this.F3hyojiJuni.HeaderText = "表示順位";
            this.F3hyojiJuni.Name = "F3hyojiJuni";
            this.F3hyojiJuni.ReadOnly = true;
            this.F3hyojiJuni.Visible = false;
            // 
            // lblMemo
            // 
            this.lblMemo.BackColor = System.Drawing.Color.LightCyan;
            this.lblMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMemo.ForeColor = System.Drawing.Color.Black;
            this.lblMemo.Location = new System.Drawing.Point(171, 20);
            this.lblMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMemo.Name = "lblMemo";
            this.lblMemo.Size = new System.Drawing.Size(181, 24);
            this.lblMemo.TabIndex = 2;
            this.lblMemo.Tag = "DISPNAME";
            this.lblMemo.Text = "0：標準　1：太字";
            this.lblMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMoji
            // 
            this.txtMoji.AutoSizeFromLength = false;
            this.txtMoji.DisplayLength = null;
            this.txtMoji.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMoji.ForeColor = System.Drawing.Color.Black;
            this.txtMoji.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMoji.Location = new System.Drawing.Point(123, 20);
            this.txtMoji.Margin = new System.Windows.Forms.Padding(4);
            this.txtMoji.MaxLength = 2;
            this.txtMoji.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMoji.Name = "txtMoji";
            this.txtMoji.Size = new System.Drawing.Size(39, 23);
            this.txtMoji.TabIndex = 1;
            this.txtMoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMoji.Enter += new System.EventHandler(this.moji_Enter);
            this.txtMoji.KeyUp += new System.Windows.Forms.KeyEventHandler(this.moji_change);
            // 
            // lblMoji
            // 
            this.lblMoji.BackColor = System.Drawing.Color.Silver;
            this.lblMoji.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMoji.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMoji.ForeColor = System.Drawing.Color.Black;
            this.lblMoji.Location = new System.Drawing.Point(0, 0);
            this.lblMoji.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMoji.Name = "lblMoji";
            this.lblMoji.Size = new System.Drawing.Size(565, 63);
            this.lblMoji.TabIndex = 0;
            this.lblMoji.Tag = "CHANGE";
            this.lblMoji.Text = "文字スタイル";
            this.lblMoji.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAdd.Location = new System.Drawing.Point(260, 24);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(53, 53);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "▲";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnUMove_Click);
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnDel.Location = new System.Drawing.Point(187, 24);
            this.btnDel.Margin = new System.Windows.Forms.Padding(4);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(53, 53);
            this.btnDel.TabIndex = 2;
            this.btnDel.Text = "▼";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDMove_Click);
            // 
            // lbxTaishoKanjoKamoku
            // 
            this.lbxTaishoKanjoKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxTaishoKanjoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lbxTaishoKanjoKamoku.FormattingEnabled = true;
            this.lbxTaishoKanjoKamoku.ItemHeight = 16;
            this.lbxTaishoKanjoKamoku.Location = new System.Drawing.Point(0, 46);
            this.lbxTaishoKanjoKamoku.Margin = new System.Windows.Forms.Padding(4);
            this.lbxTaishoKanjoKamoku.Name = "lbxTaishoKanjoKamoku";
            this.lbxTaishoKanjoKamoku.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxTaishoKanjoKamoku.Size = new System.Drawing.Size(565, 122);
            this.lbxTaishoKanjoKamoku.TabIndex = 0;
            // 
            // txtKamokuNm
            // 
            this.txtKamokuNm.AutoSizeFromLength = false;
            this.txtKamokuNm.DisplayLength = null;
            this.txtKamokuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKamokuNm.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKamokuNm.Location = new System.Drawing.Point(87, 10);
            this.txtKamokuNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtKamokuNm.MaxLength = 30;
            this.txtKamokuNm.Name = "txtKamokuNm";
            this.txtKamokuNm.Size = new System.Drawing.Size(295, 23);
            this.txtKamokuNm.TabIndex = 0;
            this.txtKamokuNm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.kamokuNm_change);
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(528, 35);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(575, 509);
            this.fsiTableLayoutPanel1.TabIndex = 4;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.fsiPanel6);
            this.fsiPanel2.Controls.Add(this.fsiPanel4);
            this.fsiPanel2.Controls.Add(this.fsiPanel3);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 56);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(565, 448);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.btnAdd);
            this.fsiPanel6.Controls.Add(this.btnDel);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(0, 168);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(565, 128);
            this.fsiPanel6.TabIndex = 4;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lbxKanjoKamokuIchiran);
            this.fsiPanel4.Controls.Add(this.label2);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.fsiPanel4.Location = new System.Drawing.Point(0, 296);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(565, 152);
            this.fsiPanel4.TabIndex = 2;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // lbxKanjoKamokuIchiran
            // 
            this.lbxKanjoKamokuIchiran.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxKanjoKamokuIchiran.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lbxKanjoKamokuIchiran.FormattingEnabled = true;
            this.lbxKanjoKamokuIchiran.ItemHeight = 16;
            this.lbxKanjoKamokuIchiran.Location = new System.Drawing.Point(0, 45);
            this.lbxKanjoKamokuIchiran.Margin = new System.Windows.Forms.Padding(4);
            this.lbxKanjoKamokuIchiran.Name = "lbxKanjoKamokuIchiran";
            this.lbxKanjoKamokuIchiran.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxKanjoKamokuIchiran.Size = new System.Drawing.Size(565, 107);
            this.lbxKanjoKamokuIchiran.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(565, 45);
            this.label2.TabIndex = 5;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "勘定科目一覧";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.lbxTaishoKanjoKamoku);
            this.fsiPanel3.Controls.Add(this.label1);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.fsiPanel3.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(565, 168);
            this.fsiPanel3.TabIndex = 1;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(565, 46);
            this.label1.TabIndex = 5;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "対象勘定科目";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.rdoKashi);
            this.fsiPanel1.Controls.Add(this.txtKamokuNm);
            this.fsiPanel1.Controls.Add(this.rdoKari);
            this.fsiPanel1.Controls.Add(this.Label3);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(565, 42);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // rdoKashi
            // 
            this.rdoKashi.AutoSize = true;
            this.rdoKashi.BackColor = System.Drawing.Color.Silver;
            this.rdoKashi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoKashi.ForeColor = System.Drawing.Color.Black;
            this.rdoKashi.Location = new System.Drawing.Point(456, 9);
            this.rdoKashi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoKashi.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoKashi.Name = "rdoKashi";
            this.rdoKashi.Size = new System.Drawing.Size(58, 24);
            this.rdoKashi.TabIndex = 1;
            this.rdoKashi.TabStop = true;
            this.rdoKashi.Tag = "CHANGE";
            this.rdoKashi.Text = "貸方";
            this.rdoKashi.UseVisualStyleBackColor = false;
            this.rdoKashi.Click += new System.EventHandler(this.rdoKashi_Click);
            // 
            // rdoKari
            // 
            this.rdoKari.AutoSize = true;
            this.rdoKari.BackColor = System.Drawing.Color.Silver;
            this.rdoKari.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoKari.ForeColor = System.Drawing.Color.Black;
            this.rdoKari.Location = new System.Drawing.Point(390, 9);
            this.rdoKari.Margin = new System.Windows.Forms.Padding(4);
            this.rdoKari.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoKari.Name = "rdoKari";
            this.rdoKari.Size = new System.Drawing.Size(58, 24);
            this.rdoKari.TabIndex = 0;
            this.rdoKari.TabStop = true;
            this.rdoKari.Tag = "CHANGE";
            this.rdoKari.Text = "借方";
            this.rdoKari.UseVisualStyleBackColor = false;
            this.rdoKari.Click += new System.EventHandler(this.rdoKari_Click);
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.Color.Silver;
            this.Label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.Label3.ForeColor = System.Drawing.Color.Black;
            this.Label3.Location = new System.Drawing.Point(0, 0);
            this.Label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(565, 42);
            this.Label3.TabIndex = 4;
            this.Label3.Tag = "CHANGE";
            this.Label3.Text = "要約設定";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel5, 0, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(528, 552);
            this.fsiTableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 1;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(575, 73);
            this.fsiTableLayoutPanel2.TabIndex = 902;
            this.fsiTableLayoutPanel2.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblMemo);
            this.fsiPanel5.Controls.Add(this.txtMoji);
            this.fsiPanel5.Controls.Add(this.lblMoji);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(565, 63);
            this.fsiPanel5.TabIndex = 0;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // ZMMR1033
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 787);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.tabControl);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMMR1033";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.tabControl, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.pnlDebug.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.F1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF1)).EndInit();
            this.F2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF2)).EndInit();
            this.F3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF3)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage F1;
        private System.Windows.Forms.TabPage F2;
        private System.Windows.Forms.TabPage F3;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuNm;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Label lblMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtMoji;
        private System.Windows.Forms.Label lblMoji;
        private System.Windows.Forms.ListBox lbxTaishoKanjoKamoku;
        private System.Windows.Forms.DataGridView mtbListF1;
        private System.Windows.Forms.DataGridView mtbListF2;
        private System.Windows.Forms.DataGridView mtbListF3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label Label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private common.FsiPanel fsiPanel4;
        private System.Windows.Forms.ListBox lbxKanjoKamokuIchiran;
        private System.Windows.Forms.RadioButton rdoKashi;
        private System.Windows.Forms.RadioButton rdoKari;
        private common.FsiPanel fsiPanel6;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiPanel fsiPanel5;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1VisibleHandan;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1kamokuCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1gyoBango;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1Taishaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1Moji;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1deleteAndAddNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1hyojiJuni;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2VisibleHandan;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2kamokuCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2gyoBango;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2Taishaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2Moji;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2deleteAndAddNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2hyojiJuni;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3VisibleHandan;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3kamokuCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3gyoBango;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3Taishaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3Moji;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3deleteAndAddNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3hyojiJuni;
    }
}