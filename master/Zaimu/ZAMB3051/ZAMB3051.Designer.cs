﻿namespace jp.co.fsi.zam.zamb3051
{
    partial class ZAMB3051
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpRight = new System.Windows.Forms.GroupBox();
            this.chkOpt3 = new System.Windows.Forms.CheckBox();
            this.chkOpt2 = new System.Windows.Forms.CheckBox();
            this.chkOpt1 = new System.Windows.Forms.CheckBox();
            this.grpLeft = new System.Windows.Forms.GroupBox();
            this.grpKessankiNext = new System.Windows.Forms.GroupBox();
            this.lblKessankiNextFoot = new System.Windows.Forms.Label();
            this.lblKessankiNext = new System.Windows.Forms.Label();
            this.lblKessankiNextHead = new System.Windows.Forms.Label();
            this.grpKessanki = new System.Windows.Forms.GroupBox();
            this.lblKessankiFoot = new System.Windows.Forms.Label();
            this.lblKessanki = new System.Windows.Forms.Label();
            this.lblKessankiHead = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.grpRight.SuspendLayout();
            this.grpLeft.SuspendLayout();
            this.grpKessankiNext.SuspendLayout();
            this.grpKessanki.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "年次繰越";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpRight
            // 
            this.grpRight.BackColor = System.Drawing.Color.Transparent;
            this.grpRight.Controls.Add(this.chkOpt3);
            this.grpRight.Controls.Add(this.chkOpt2);
            this.grpRight.Controls.Add(this.chkOpt1);
            this.grpRight.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpRight.Location = new System.Drawing.Point(286, 92);
            this.grpRight.Name = "grpRight";
            this.grpRight.Size = new System.Drawing.Size(269, 111);
            this.grpRight.TabIndex = 0;
            this.grpRight.TabStop = false;
            // 
            // chkOpt3
            // 
            this.chkOpt3.AutoSize = true;
            this.chkOpt3.Checked = true;
            this.chkOpt3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOpt3.Location = new System.Drawing.Point(25, 75);
            this.chkOpt3.Name = "chkOpt3";
            this.chkOpt3.Size = new System.Drawing.Size(187, 20);
            this.chkOpt3.TabIndex = 2;
            this.chkOpt3.Text = "期末残高の繰越を行う";
            this.chkOpt3.UseVisualStyleBackColor = true;
            // 
            // chkOpt2
            // 
            this.chkOpt2.AutoSize = true;
            this.chkOpt2.Checked = true;
            this.chkOpt2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOpt2.Location = new System.Drawing.Point(25, 49);
            this.chkOpt2.Name = "chkOpt2";
            this.chkOpt2.Size = new System.Drawing.Size(155, 20);
            this.chkOpt2.TabIndex = 1;
            this.chkOpt2.Text = "実績の繰越を行う";
            this.chkOpt2.UseVisualStyleBackColor = true;
            // 
            // chkOpt1
            // 
            this.chkOpt1.AutoSize = true;
            this.chkOpt1.Checked = true;
            this.chkOpt1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOpt1.Location = new System.Drawing.Point(25, 23);
            this.chkOpt1.Name = "chkOpt1";
            this.chkOpt1.Size = new System.Drawing.Size(171, 20);
            this.chkOpt1.TabIndex = 0;
            this.chkOpt1.Text = "テーブルを移行する";
            this.chkOpt1.UseVisualStyleBackColor = true;
            // 
            // grpLeft
            // 
            this.grpLeft.BackColor = System.Drawing.Color.Transparent;
            this.grpLeft.Controls.Add(this.grpKessankiNext);
            this.grpLeft.Controls.Add(this.grpKessanki);
            this.grpLeft.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpLeft.Location = new System.Drawing.Point(12, 92);
            this.grpLeft.Name = "grpLeft";
            this.grpLeft.Size = new System.Drawing.Size(269, 111);
            this.grpLeft.TabIndex = 1;
            this.grpLeft.TabStop = false;
            // 
            // grpKessankiNext
            // 
            this.grpKessankiNext.BackColor = System.Drawing.Color.Transparent;
            this.grpKessankiNext.Controls.Add(this.lblKessankiNextFoot);
            this.grpKessankiNext.Controls.Add(this.lblKessankiNext);
            this.grpKessankiNext.Controls.Add(this.lblKessankiNextHead);
            this.grpKessankiNext.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpKessankiNext.Location = new System.Drawing.Point(26, 58);
            this.grpKessankiNext.Name = "grpKessankiNext";
            this.grpKessankiNext.Size = new System.Drawing.Size(212, 36);
            this.grpKessankiNext.TabIndex = 906;
            this.grpKessankiNext.TabStop = false;
            // 
            // lblKessankiNextFoot
            // 
            this.lblKessankiNextFoot.AutoSize = true;
            this.lblKessankiNextFoot.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessankiNextFoot.Location = new System.Drawing.Point(142, 13);
            this.lblKessankiNextFoot.Name = "lblKessankiNextFoot";
            this.lblKessankiNextFoot.Size = new System.Drawing.Size(24, 16);
            this.lblKessankiNextFoot.TabIndex = 8;
            this.lblKessankiNextFoot.Text = "期";
            // 
            // lblKessankiNext
            // 
            this.lblKessankiNext.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKessankiNext.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessankiNext.Location = new System.Drawing.Point(96, 10);
            this.lblKessankiNext.Name = "lblKessankiNext";
            this.lblKessankiNext.Size = new System.Drawing.Size(46, 23);
            this.lblKessankiNext.TabIndex = 7;
            this.lblKessankiNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKessankiNextHead
            // 
            this.lblKessankiNextHead.AutoSize = true;
            this.lblKessankiNextHead.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessankiNextHead.Location = new System.Drawing.Point(8, 13);
            this.lblKessankiNextHead.Name = "lblKessankiNextHead";
            this.lblKessankiNextHead.Size = new System.Drawing.Size(88, 16);
            this.lblKessankiNextHead.TabIndex = 6;
            this.lblKessankiNextHead.Text = "新年度　第";
            // 
            // grpKessanki
            // 
            this.grpKessanki.BackColor = System.Drawing.Color.Transparent;
            this.grpKessanki.Controls.Add(this.lblKessankiFoot);
            this.grpKessanki.Controls.Add(this.lblKessanki);
            this.grpKessanki.Controls.Add(this.lblKessankiHead);
            this.grpKessanki.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpKessanki.Location = new System.Drawing.Point(26, 16);
            this.grpKessanki.Name = "grpKessanki";
            this.grpKessanki.Size = new System.Drawing.Size(212, 36);
            this.grpKessanki.TabIndex = 905;
            this.grpKessanki.TabStop = false;
            // 
            // lblKessankiFoot
            // 
            this.lblKessankiFoot.AutoSize = true;
            this.lblKessankiFoot.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessankiFoot.Location = new System.Drawing.Point(142, 13);
            this.lblKessankiFoot.Name = "lblKessankiFoot";
            this.lblKessankiFoot.Size = new System.Drawing.Size(24, 16);
            this.lblKessankiFoot.TabIndex = 8;
            this.lblKessankiFoot.Text = "期";
            // 
            // lblKessanki
            // 
            this.lblKessanki.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKessanki.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessanki.Location = new System.Drawing.Point(96, 10);
            this.lblKessanki.Name = "lblKessanki";
            this.lblKessanki.Size = new System.Drawing.Size(46, 23);
            this.lblKessanki.TabIndex = 7;
            this.lblKessanki.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKessankiHead
            // 
            this.lblKessankiHead.AutoSize = true;
            this.lblKessankiHead.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessankiHead.Location = new System.Drawing.Point(8, 13);
            this.lblKessankiHead.Name = "lblKessankiHead";
            this.lblKessankiHead.Size = new System.Drawing.Size(88, 16);
            this.lblKessankiHead.TabIndex = 6;
            this.lblKessankiHead.Text = "今年度　第";
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblInfo.Location = new System.Drawing.Point(35, 220);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(72, 16);
            this.lblInfo.TabIndex = 902;
            this.lblInfo.Text = "繰越済み";
            // 
            // ZAMB3051
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.grpLeft);
            this.Controls.Add(this.grpRight);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZAMB3051";
            this.ShowFButton = true;
            this.Text = "年次繰越";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.grpRight, 0);
            this.Controls.SetChildIndex(this.grpLeft, 0);
            this.Controls.SetChildIndex(this.lblInfo, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpRight.ResumeLayout(false);
            this.grpRight.PerformLayout();
            this.grpLeft.ResumeLayout(false);
            this.grpKessankiNext.ResumeLayout(false);
            this.grpKessankiNext.PerformLayout();
            this.grpKessanki.ResumeLayout(false);
            this.grpKessanki.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpRight;
        private System.Windows.Forms.CheckBox chkOpt3;
        private System.Windows.Forms.CheckBox chkOpt2;
        private System.Windows.Forms.CheckBox chkOpt1;
        private System.Windows.Forms.GroupBox grpLeft;
        private System.Windows.Forms.GroupBox grpKessankiNext;
        private System.Windows.Forms.Label lblKessankiNextFoot;
        private System.Windows.Forms.Label lblKessankiNext;
        private System.Windows.Forms.Label lblKessankiNextHead;
        private System.Windows.Forms.GroupBox grpKessanki;
        private System.Windows.Forms.Label lblKessankiFoot;
        private System.Windows.Forms.Label lblKessanki;
        private System.Windows.Forms.Label lblKessankiHead;
        private System.Windows.Forms.Label lblInfo;



    }
}