﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Reflection;
using System;
using System.Collections;
using System.Data.SqlClient;

namespace jp.co.fsi.zam.zamb3051
{
    /// <summary>
    /// 年次繰越(ZAMB3051)
    /// </summary>
    public partial class ZAMB3051 : BasePgForm
    {
        #region 変数

        private bool _overWriteFlag;
        private DataTable _dtTableCopySetting;          
        
        #endregion

        #region プロパティ

        /// <summary>
        /// 上書きフラグ
        /// </summary>
        public bool OverWriteFlag
        {
            get
            {
                return _overWriteFlag;
            }
            set
            {
                _overWriteFlag = value;
            }
        }

        /// <summary>
        /// テーブル設定情報
        /// </summary>
        public DataTable DtTableCopySetting
        {
            get
            {
                return _dtTableCopySetting;
            }
            set
            {
                _dtTableCopySetting = value;
            }
        }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZAMB3051()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // ESC F6 F12のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Location = this.btnF3.Location;

            // プロパティ設定
            this.OverWriteFlag = !GetProccessed();
            this.DtTableCopySetting = InitDtTableCopySetting(this.OverWriteFlag);

            // 現在のステータス
            this.lblKessanki.Text = Util.ToString(this.UInfo.KessanKi);
            this.lblKessankiNext.Text = Util.ToString(this.UInfo.KessanKi + 1);
            this.lblInfo.Visible = !this.OverWriteFlag;
            this.chkOpt1.Checked = this.OverWriteFlag;

            // 繰越済メッセージの表示
            if (!this.OverWriteFlag)
            {
                StringBuilder info = new StringBuilder();
                info.Append("指定された年度のデータは既に繰越済みです。\r\n");
                info.Append("【今年度実績の繰越を行う】【期末残高の繰越を行う】を\r\n");
                info.Append("選択した場合、新年度の繰越残高情報は全て\r\n");
                info.Append("書き換えられますので注意して下さい。");
                Msg.Info(Util.ToString(info));
            }

            // フォーカス初期値
            this.chkOpt1.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            base.PressEsc();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 実行確認
            string msg = "実行しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();
                
                // テーブル移行
                if (this.chkOpt1.Checked)
                {
                    DbParamCollection dpc;
                    StringBuilder sql;
                    DataTable dt;
                    DataTable dtPrev;

                    // 本処理未実行の場合のみ
                    if (!GetProccessed())
                    {
                        #region TB会社情報
                        // 現年度会社情報レコード取得
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        sql = new StringBuilder();
                        sql.Append("SELECT * ");
                        sql.Append(" FROM TB_ZM_KAISHA_JOHO");
                        sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                        sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
                        dtPrev = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        if (dtPrev.Rows.Count == 1)
                        {
                            // 新年度レコードパラメータ取得
                            ArrayList alParams = SetZmKaishaJohoParams(dtPrev.Rows[0]);
                            // レコード追加
                            this.Dba.Insert("TB_ZM_KAISHA_JOHO", (DbParamCollection)alParams[0]);
                        }
                        else
                        {
                            throw new Exception();      // システムエラー
                        }
                        #endregion

                        #region TB消費税情報
                        // 現年度消費税情報レコード取得
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 3, this.UInfo.KessanKi);
                        sql = new StringBuilder();
                        sql.Append("SELECT * ");
                        sql.Append(" FROM TB_ZM_SHOHIZEI_JOHO");
                        sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                        sql.Append(" AND KESSANKI = @KESSANKI");
                        sql.Append(" ORDER BY TEKIYO_KAISHIBI DESC");            // 年度内最終レコードを複写
                        dtPrev = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        if (dtPrev.Rows.Count > 0)
                        {
                            // 新年度レコードパラメータ取得
                            ArrayList alParams = SetZmShohizeiJohoParams(dtPrev.Rows[0]);
                            // レコード追加
                            this.Dba.Insert("TB_ZM_SHOHIZEI_JOHO", (DbParamCollection)alParams[0]);
                        }
                        else
                        {
                            throw new Exception();      // システムエラー
                        }
                        #endregion

                        #region TB伝票番号情報
                        // 現年度伝票番号管理レコード取得
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        sql = new StringBuilder();
                        sql.Append("SELECT * ");
                        sql.Append(" FROM TB_ZM_DENPYO_BANGO");
                        sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                        sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
                        dtPrev = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        if (dtPrev.Rows.Count == 1)
                        {
                            // 新年度レコードパラメータ取得
                            ArrayList alParams = SetZmDenpyoBangoParams(dtPrev.Rows[0]);
                            // レコード追加
                            this.Dba.Insert("TB_ZM_DENPYO_BANGO", (DbParamCollection)alParams[0]);
                        }
                        else
                        {
                            throw new Exception();      // システムエラー
                        }
                        #endregion

                        #region TB_Ｆ伝票番号レコードの複写、伝票番号の更新設定
                        CopyPreviousYearData(false, Util.ToString("TB_HN_F_DENPYO_BANGO"));

                        DbParamCollection updParam = new DbParamCollection();
                        DbParamCollection whereParam = new DbParamCollection();
                        string where = "";

                        updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, 0);

                        whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);

                        where += "KAISHA_CD = @KAISHA_CD AND ";
                        where += "KAIKEI_NENDO = @KAIKEI_NENDO";

                        this.Dba.Update("TB_HN_F_DENPYO_BANGO", updParam, where, whereParam);
                        #endregion

                        #region SCRIPTS-DMLレコード実行
                        /*
                        // 3500番台のSCRIPTを実行
                        sql = new StringBuilder();
                        sql.Append("SELECT ");
                        sql.Append(" SQL");
                        sql.Append(" FROM TB_ZM_SCRIPTS");
                        sql.Append(" WHERE");
                        sql.Append(" ID >= 3500");
                        sql.Append(" AND ID < 3600");
                        dt = this.Dba.GetDataTableFromSql(Util.ToString(sql));
                        foreach (DataRow dr in dt.Rows)
                        {
                            // SQL発行
                            if (!ValChk.IsEmpty(dr["SQL"]))
                            {
                                // 
                                dpc = new DbParamCollection();
                                dpc.SetParam("@N", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);
                                this.Dba.ModifyBySql(Util.ToString(dr["SQL"]), dpc);
                            }
                        }
                        */
                        #endregion
                    }

                    #region 各マスタレコードの複写
                    foreach (DataRow dr in this.DtTableCopySetting.Rows)
                    {
                        if ((Boolean)dr["CHK"])
                        {
                            CopyPreviousYearData(this.OverWriteFlag, Util.ToString(dr["TABLE_NM"]));
                        }
                    }
                    #endregion
                }

                // 実績繰越
                if (this.chkOpt2.Checked)
                {
                    // ***<NOTE>***
                    // 前年実績レコードのKAIKEI_NENDOフィールド値は新年度値がセットされている
                    // レコードの実績年月の属する会計年度はレコードの会計年度　ではなく
                    // レコードの実績年月の属する会計年度はレコードの会計年度の前年度　となる

                    // データ更新用パラメータ
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);    // 新年度値をセット

                    // 登録済データの削除
                    this.Dba.Delete("TB_ZM_ZENNEN_JISSEKI", "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO", dpc);

                    // 実績データの追加
                    this.Dba.ModifyBySql(GetJissekiKurikoshiSql(), dpc);
                }

                // 残高繰越
                if (this.chkOpt3.Checked)
                {
                    DbParamCollection dpc;
                    StringBuilder sql;
                    ArrayList alParams;
                    int gyoBango = 0;

                    // データ更新用パラメータ
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);    // 新年度値をセット
                    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, 0);                             // 期首仕訳伝番＝０

                    // 登録済データの削除(仕訳伝票データ)
                    this.Dba.Delete("TB_ZM_SHIWAKE_DENPYO",
                                    "KAISHA_CD = @KAISHA_CD"
                                    + " AND KAIKEI_NENDO = @KAIKEI_NENDO"
                                    + " AND DENPYO_BANGO = @DENPYO_BANGO", dpc);

                    // 登録済データの削除(仕訳明細データ)
                    this.Dba.Delete("TB_ZM_SHIWAKE_MEISAI",
                                    "KAISHA_CD = @KAISHA_CD"
                                    + " AND KAIKEI_NENDO = @KAIKEI_NENDO"
                                    + " AND DENPYO_BANGO = @DENPYO_BANGO", dpc);
                    
                    // 仕訳明細データ登録
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);        // 現年度値をセット
                    sql = new StringBuilder();
                    sql.Append("SELECT * ");
                    sql.Append(" FROM VI_ZM_KIMATSU_ZANDAKA");
                    sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                    sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
                    sql.Append(" ORDER BY KANJO_KAMOKU_CD ASC, HOJO_KAMOKU_CD ASC");
                    DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]) != 0
                            || Util.ToDecimal(dr["ZEINUKI_KINGAKU"]) != 0)
                        {
                            // 行数カウントアップ
                            gyoBango += 1;

                            // 登録パラメータ取得
                            alParams = SetZmShiwakeMeisaiParams(gyoBango, dr);
                            // レコード追加
                            this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParams[0]);
                        }
                    }

                    // 仕訳伝票データ登録
                    alParams = SetZmShiwakeDenpyoParams(gyoBango);
                    // レコード追加
                    this.Dba.Insert("TB_ZM_SHIWAKE_DENPYO", (DbParamCollection)alParams[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("更新処理を実行しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            ShowTableCopySetting();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 本処理実行状態取得
        /// </summary>
        /// <returns></returns>
        private bool GetProccessed()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 3, this.UInfo.KessanKi + 1);
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" COUNT(*) AS CNT");
            sql.Append(" FROM");
            sql.Append(" VI_ZM_KAISHA_JOHO");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND KESSANKI = @KESSANKI");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            //return false;

            return (Util.ToDecimal(dt.Rows[0]["CNT"]) > 0);
        }

        /// <summary>
        /// テーブル操作設定情報初期化
        /// </summary>
        /// <param name="dt"></param>
        private DataTable InitDtTableCopySetting(bool overWriteFlag)
        {
            DataTable dt = new DataTable("TABLE_SETTING");
            dt.Columns.Add("NO", Type.GetType("System.Int32"));
            dt.Columns.Add("CHK", Type.GetType("System.Boolean"));
            dt.Columns.Add("TABLE_NM", Type.GetType("System.String"));

            DataRow dr;
            dr = dt.NewRow();
            // 勘定科目データ
            dr["NO"] = 1;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_KANJO_KAMOKU";
            dt.Rows.Add(dr);
            // 補助科目データ
            dr = dt.NewRow();
            dr["NO"] = 2;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_HOJO_KAMOKU";
            dt.Rows.Add(dr);
            // 摘要データ
            dr = dt.NewRow();
            dr["NO"] = 3;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_TEKIYO";
            dt.Rows.Add(dr);
            // 仕訳事例データ
            dr = dt.NewRow();
            dr["NO"] = 4;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_SHIWAKE_JIREI";
            dt.Rows.Add(dr);
            // 工事データ
            dr = dt.NewRow();
            dr["NO"] = 5;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_KOJI";
            dt.Rows.Add(dr);
            // 工種データ
            dr = dt.NewRow();
            dr["NO"] = 6;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_KOSHU";
            dt.Rows.Add(dr);
            // 決算書設定データ
            dr = dt.NewRow();
            dr["NO"] = 7;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_KESSANSHO_SETTEI";
            dt.Rows.Add(dr);
            // 決算書注記データ
            dr = dt.NewRow();
            dr["NO"] = 8;
            dr["CHK"] = false;
            dr["TABLE_NM"] = "TB_ZM_KESSANSHO_CHUKI";
            dt.Rows.Add(dr);
            // 決算書科目設定データ
            dr = dt.NewRow();
            dr["NO"] = 9;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_KESSANSHO_KAMOKU_SETTEI";
            dt.Rows.Add(dr);
            // 試算表設定データ
            dr = dt.NewRow();
            dr["NO"] = 10;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_SHISANHYO_SETTEI";
            dt.Rows.Add(dr);
            // 試算表科目設定データ
            dr = dt.NewRow();
            dr["NO"] = 11;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_SHISANHYO_KAMOKU_SETTEI";
            dt.Rows.Add(dr);
            // 明細表科目分類データ
            dr = dt.NewRow();
            dr["NO"] = 12;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_MEISAIHYO_KAMOKU_BUNRUI";
            dt.Rows.Add(dr);
            // 工事予算データ
            dr = dt.NewRow();
            dr["NO"] = 13;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_KOJI_YOSAN";
            dt.Rows.Add(dr);

            // 決算書科目分類データ
            dr = dt.NewRow();
            dr["NO"] = 14;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_KESSANSHO_KAMOKU_BUNRUI";
            dt.Rows.Add(dr);
            
            // 試算表科目分類データ
            dr = dt.NewRow();
            dr["NO"] = 15;
            dr["CHK"] = overWriteFlag;
            dr["TABLE_NM"] = "TB_ZM_SHISANHYO_KAMOKU_BUNRUI";
            dt.Rows.Add(dr);

            return dt;
        }

        /// <summary>
        /// テーブル操作設定画面を表示する
        /// </summary>
        private void ShowTableCopySetting()
        {
            ZAMB3052 frm = new ZAMB3052();
            frm.OverWriteFlag = this.OverWriteFlag;
            frm.DtTableCopySetting = this.DtTableCopySetting;
            DialogResult result = frm.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                this.OverWriteFlag = frm.OverWriteFlag;
                this.DtTableCopySetting = frm.DtTableCopySetting;
            }
        }

        /// <summary>
        /// 前年度データのコピー
        /// </summary>
        /// <param name="overWrite">上書き判定</param>
        /// <param name="tableNm">テーブル名</param>
        private void CopyPreviousYearData(bool overWrite, string tableNm)
        {
            DbParamCollection dpc;

            // テーブル定義情報取得
            DataTable dtDef = GetSpColumns(tableNm);

            // フィールドKAISHA_CDの存在判定
            bool includeKaishaCd = IncludeKaishaCd(dtDef);

            // 上書指定の場合は登録済データを削除
            if (overWrite)
            {
                dpc = new DbParamCollection();
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);
                if (includeKaishaCd) dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                this.Dba.Delete(tableNm, "KAIKEI_NENDO = @KAIKEI_NENDO", dpc);
            }

            // 前年度データ取得
            dpc = new DbParamCollection();
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            if (includeKaishaCd) dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT * FROM " + tableNm);
            sql.Append(" WHERE KAIKEI_NENDO = @KAIKEI_NENDO");
            if (includeKaishaCd) sql.Append(" AND KAISHA_CD = @KAISHA_CD");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            foreach (DataRow dr in dt.Rows)
            {
                // 新年度レコードパラメータ取得
                ArrayList alParams = SetCopyParams(dtDef, dr);

                try
                {
                    // レコード追加
                    this.Dba.Insert(tableNm, (DbParamCollection)alParams[0]);
                }
                catch (SqlException ex)
                {
                    if (ex.Errors[0].Number == 2627)        /// 主キー違反：上書しない時のINSERT失敗を無視
                    {
                        continue;
                    }
                }
            }
        }

        /// <summary>
        /// フィールドKAISHA_CDを含むかを判定
        /// </summary>
        /// <param name="dtSpColumns"></param>
        /// <returns></returns>
        private bool IncludeKaishaCd(DataTable dtSpColumns)
        {
            foreach (DataRow dr in dtSpColumns.Rows)
            {
                if (Util.ToString(dr["COLUMN_NAME"]) == "KAISHA_CD")
                    return true;
            }
            return false;
        }

        /// <summary>
        /// テーブルのフィールド定義情報取得
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private DataTable GetSpColumns(string tableName)
        {
            string sql = "sp_columns @table_name = '" + tableName + "'";
            return this.Dba.GetDataTableFromSql(sql);
        }

        /// <summary>
        /// 前年度データコピー用のパラメータ設定をします。
        /// </summary>
        /// <param name="dtDef">sp_columns取得情報</param>
        /// <param name="dr">挿入用前年度データ</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetCopyParams(DataTable dtDef, DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            foreach (DataRow drDef in dtDef.Rows)
            {
                switch (Util.ToString(drDef["COLUMN_NAME"]))
                {
                    case "KAIKEI_NENDO":
                        updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);
                        break;
                    case "REGIST_DATE":
                        updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
                        break;
                    case "UPDATE_DATE":
                        updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
                        break;
                    default:
                        switch (Util.ToInt(drDef["DATA_TYPE"]))
                        {
                            case 3:     // Decimal
                                updParam.SetParam(
                                    "@" + Util.ToString(drDef["COLUMN_NAME"]),
                                    SqlDbType.Decimal,
                                    Util.ToInt(Util.ToString(drDef["LENGTH"])),
                                    Util.ToInt(Util.ToString(drDef["SCALE"])),
                                    dr[Util.ToString(drDef["COLUMN_NAME"])]);
                                break;
                            case 11:    // DateTime
                                updParam.SetParam(
                                    "@" + Util.ToString(drDef["COLUMN_NAME"]),
                                    SqlDbType.DateTime,
                                    dr[Util.ToString(drDef["COLUMN_NAME"])]);
                                break;
                            case 12:    // varchar
                                updParam.SetParam(
                                    "@" + Util.ToString(drDef["COLUMN_NAME"]),
                                    SqlDbType.VarChar,
                                    Util.ToInt(Util.ToString(drDef["LENGTH"])),
                                    dr[Util.ToString(drDef["COLUMN_NAME"])]);
                                break;
                        }
                        break;
                }
            }
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_KAISHA_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="dr">前年度レコード</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmKaishaJohoParams(DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 新年度用フィールド値
            updParam.SetParam("@KESSANKI", SqlDbType.Decimal, 3, this.UInfo.KessanKi + 1);
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);
            updParam.SetParam("@FIX_FLG", SqlDbType.Decimal, 2, 0);
            updParam.SetParam("@KAIKEI_KIKAN_KAISHIBI", SqlDbType.DateTime, 
                Util.ToDate(dr["KAIKEI_KIKAN_KAISHIBI"]).AddYears(1));
            updParam.SetParam("@KAIKEI_KIKAN_SHURYOBI", SqlDbType.DateTime,
                Util.ToDate(dr["KAIKEI_KIKAN_SHURYOBI"]).AddYears(1));
            // 前年度コピーフィールド値
            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            updParam.SetParam("@KOJI_TITLE1", SqlDbType.VarChar, 20, dr["KOJI_TITLE1"]);
            updParam.SetParam("@KOJI_TITLE2", SqlDbType.VarChar, 20, dr["KOJI_TITLE2"]);
            updParam.SetParam("@KOJI_TITLE3", SqlDbType.VarChar, 20, dr["KOJI_TITLE3"]);
            updParam.SetParam("@KOJI_KANRI_KUBUN", SqlDbType.Decimal, 1, dr["KOJI_KANRI_KUBUN"]);
            updParam.SetParam("@KOSHU_KANRI_KUBUN", SqlDbType.Decimal, 1, dr["KOSHU_KANRI_KUBUN"]);
            updParam.SetParam("@KANSEI_URIAGEDAKA_KAMOKU_CD", SqlDbType.Decimal, 4, dr["KANSEI_URIAGEDAKA_KAMOKU_CD"]);
            updParam.SetParam("@KANSEI_MISHUNYUKIN_KAMOKU_CD", SqlDbType.Decimal, 4, dr["KANSEI_MISHUNYUKIN_KAMOKU_CD"]);
            updParam.SetParam("@MISEI_UKEIREKIN_KAMOKU_CD", SqlDbType.Decimal, 4, dr["MISEI_UKEIREKIN_KAMOKU_CD"]);
            updParam.SetParam("@KOJI_MIBARAIKIN_KAMOKU_CD", SqlDbType.Decimal, 4, dr["KOJI_MIBARAIKIN_KAMOKU_CD"]);
            updParam.SetParam("@TEKIYO_KAISHIBI", SqlDbType.DateTime, dr["TEKIYO_KAISHIBI"]);
            // 更新情報
            updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_SHOHIZEI_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="dr">前年度レコード</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmShohizeiJohoParams(DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 新年度用フィールド値
            updParam.SetParam("@KESSANKI", SqlDbType.Decimal, 3, this.UInfo.KessanKi + 1);
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);
            updParam.SetParam("@TEKIYO_KAISHIBI", SqlDbType.DateTime,
                this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            // 前年度コピーフィールド値
            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            updParam.SetParam("@KAZEI_HOHO", SqlDbType.Decimal, 1, dr["KAZEI_HOHO"]);
            updParam.SetParam("@KOJO_HOHO", SqlDbType.Decimal, 1, dr["KOJO_HOHO"]);
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, dr["JIGYO_KUBUN"]);
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, dr["SHOHIZEI_NYURYOKU_HOHO"]);
            updParam.SetParam("@SHOHIZEI_HASU_SHORI", SqlDbType.Decimal, 1, dr["SHOHIZEI_HASU_SHORI"]);
            updParam.SetParam("@KYU_SHOHIZEI_RITSU", SqlDbType.Decimal, 2, 1, dr["KYU_SHOHIZEI_RITSU"]);
            updParam.SetParam("@SHIN_SHOHIZEI_RITSU", SqlDbType.Decimal, 2, 1, dr["SHIN_SHOHIZEI_RITSU"]);
            updParam.SetParam("@KARIUKE_SHOHIZEI_KAMOKU_CD", SqlDbType.Decimal, 4, dr["KARIUKE_SHOHIZEI_KAMOKU_CD"]);
            updParam.SetParam("@KARIBARAI_SHOHIZEI_KAMOKU_CD", SqlDbType.Decimal, 4, dr["KARIBARAI_SHOHIZEI_KAMOKU_CD"]);
            updParam.SetParam("@CHIHO_SHOHIZEI_RITSU", SqlDbType.Decimal, 2, 1, dr["CHIHO_SHOHIZEI_RITSU"]);
            updParam.SetParam("@CHUKAN_NOFUZEIGAKU", SqlDbType.Decimal, 12, 0, dr["CHUKAN_NOFUZEIGAKU"]);
            updParam.SetParam("@CHUKAN_NOFU_JOTOWARIGAKU", SqlDbType.Decimal, 12, 0, dr["CHUKAN_NOFU_JOTOWARIGAKU"]);
            updParam.SetParam("@KYU_ZEIRITSUBUN_CHOSEIGAKU1", SqlDbType.Decimal, 12, 0, dr["KYU_ZEIRITSUBUN_CHOSEIGAKU1"]);
            updParam.SetParam("@KYU_ZEIRITSUBUN_CHOSEIGAKU2", SqlDbType.Decimal, 12, 0, dr["KYU_ZEIRITSUBUN_CHOSEIGAKU2"]);
            updParam.SetParam("@KYU_ZEIRITSUBUN_CHOSEIGAKU3", SqlDbType.Decimal, 12, 0, dr["KYU_ZEIRITSUBUN_CHOSEIGAKU3"]);
            updParam.SetParam("@SHIN_ZEIRITSUBUN_CHOSEIGAKU1", SqlDbType.Decimal, 12, 0, dr["SHIN_ZEIRITSUBUN_CHOSEIGAKU1"]);
            updParam.SetParam("@SHIN_ZEIRITSUBUN_CHOSEIGAKU2", SqlDbType.Decimal, 12, 0, dr["SHIN_ZEIRITSUBUN_CHOSEIGAKU2"]);
            updParam.SetParam("@SHIN_ZEIRITSUBUN_CHOSEIGAKU3", SqlDbType.Decimal, 12, 0, dr["SHIN_ZEIRITSUBUN_CHOSEIGAKU3"]);
            updParam.SetParam("@KIJUN_KIKAN_KAZEI_URIAGEDAKA", SqlDbType.Decimal, 12, 0, dr["KIJUN_KIKAN_KAZEI_URIAGEDAKA"]);
            // 更新情報
            updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_DENPYOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="gyosu">仕訳行数</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmShiwakeDenpyoParams(int gyosu)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 仕訳伝票情報
            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, 0);
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);
            // 伝票日付：現在情報の期末日をセット
            updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            updParam.SetParam("@SHOHYO_BANGO", SqlDbType.VarChar, 10, "期首残高");
            updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, 0);
            updParam.SetParam("@SHIWAKE_GYOSU", SqlDbType.Decimal, 6, gyosu);
            // 更新情報
            updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_MEISAIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="gyoBango">仕訳行数</param>
        /// <param name="dr">(前期)期末残高レコード</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmShiwakeMeisaiParams(int gyoBango, DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // レコード識別情報
            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, 0);
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);

            updParam.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, gyoBango);
            // 貸借区分判定
            int taishakuKubun = Util.ToInt(dr["TAISHAKU_KUBUN"]);
            if (Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]) < 0)
            {
                taishakuKubun = (taishakuKubun == 1) ? 2 : 1;
            }
            updParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, taishakuKubun);
            updParam.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, 0);
            updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, 0);
            updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, dr["DENPYO_DATE"]);
            updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4, dr["KANJO_KAMOKU_CD"]);
            updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 4, dr["HOJO_KAMOKU_CD"]);
            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, dr["BUMON_CD"]);
            updParam.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, dr["KOJI_CD"]);
            updParam.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, dr["KOSHU_CD"]);
            updParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, dr["TEKIYO_CD"]);
            updParam.SetParam("@TEKIYO", SqlDbType.VarChar, 40, "期首残高");
            updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, Math.Abs(Util.ToDecimal(dr["ZEIKOMI_KINGAKU"])));
            updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, Math.Abs(Util.ToDecimal(dr["ZEINUKI_KINGAKU"])));
            updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);
            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, dr["ZEI_KUBUN"]);
            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, dr["KAZEI_KUBUN"]);
            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, dr["TORIHIKI_KUBUN"]);
            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, dr["ZEI_RITSU"]);
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, dr["JIGYO_KUBUN"]);
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, dr["SHOHIZEI_NYURYOKU_HOHO"]);
            updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, dr["SHOHIZEI_HENKO"]);
            updParam.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, dr["KESSAN_KUBUN"]);
            updParam.SetParam("@SHIWAKE_SAKUSEI_KUBUN", SqlDbType.Decimal, 1, dr["SHIWAKE_SAKUSEI_KUBUN"]);
            // updParam.SetParam("@BIKO", SqlDbType.VarChar, 40, );
            // 更新情報
            updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_DENPYO_BANGOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="dr">前年度レコード</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmDenpyoBangoParams(DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 新年度用フィールド値
            updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, 0);
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1);
            // 前年度コピーフィールド値
            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, dr["DENPYO_KUBUN"]);
            updParam.SetParam("@BANGO_ZOBUN", SqlDbType.Decimal, 4, dr["BANGO_ZOBUN"]);
            updParam.SetParam("@BANGO_SAISHOCHI", SqlDbType.Decimal, 9, dr["BANGO_SAISHOCHI"]);
            updParam.SetParam("@BANGO_SAIDAICHI", SqlDbType.Decimal, 9, dr["BANGO_SAIDAICHI"]);
            // 更新情報
            updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 実績繰越SQLステートメント取得
        /// </summary>
        /// <returns>SQL文字列</returns>
        private string GetJissekiKurikoshiSql()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO TB_ZM_ZENNEN_JISSEKI");
            sql.Append(" (KAISHA_CD");
            sql.Append(", JISSEKI_NENGETSU");
            sql.Append(", KANJO_KAMOKU_CD");
            sql.Append(", HOJO_KAMOKU_CD");
            sql.Append(", BUMON_CD");
            sql.Append(", KAIKEI_NENDO");
            sql.Append(", ZEIKOMI_KINGAKU");
            sql.Append(", ZEINUKI_KINGAKU");
            sql.Append(", REGIST_DATE");
            sql.Append(", UPDATE_DATE");
            sql.Append(" )");
            sql.Append(" SELECT");
            sql.Append("  A.KAISHA_CD AS KAISHA_CD");
            sql.Append(", LEFT(CONVERT(VARCHAR, A.DENPYO_DATE, 112), 6) +'01' AS JISSEKI_NENGETSU");
            sql.Append(", A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD");
            sql.Append(", A.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD");
            sql.Append(", A.BUMON_CD AS BUMON_CD");
            sql.Append(", @KAIKEI_NENDO");              // @KAIKEI_NENDOは新年度を指定
            sql.Append(", SUM(");
            sql.Append("   CASE");
            sql.Append("    WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.Append("     THEN A.ZEIKOMI_KINGAKU");
            sql.Append("     ELSE (A.ZEIKOMI_KINGAKU * -1)");
            sql.Append("    END");
            sql.Append("  ) AS ZEIKOMI_KINGAKU");
            sql.Append(", SUM(");
            sql.Append("   CASE");
            sql.Append("    WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.Append("     THEN A.ZEIKOMI_KINGAKU");
            sql.Append("     ELSE (A.ZEIKOMI_KINGAKU * -1)");
            sql.Append("    END");
            sql.Append("  ) AS ZEINUKI_KINGAKU");
            sql.Append(", CONVERT(varchar(10), GETDATE(), 111)");
            sql.Append(", CONVERT(varchar(10), GETDATE(), 111)");
            sql.Append(" FROM");
            sql.Append("  TB_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" LEFT OUTER JOIN");
            sql.Append("  TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON");
            sql.Append("  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append("  AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append("  AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            sql.Append(" WHERE");
            sql.Append("  A.KAISHA_CD = @KAISHA_CD");
            sql.Append("  AND B.KAISHA_CD = @KAISHA_CD");
            sql.Append("  AND A.KAIKEI_NENDO = @KAIKEI_NENDO - 1");         // 前年度
            sql.Append("  AND B.KAIKEI_NENDO = @KAIKEI_NENDO - 1");         // 前年度
            sql.Append(" GROUP BY");
            sql.Append("  A.KAISHA_CD");
            sql.Append(", LEFT(CONVERT(VARCHAR, A.DENPYO_DATE, 112), 6) +'01'");
            sql.Append(", A.KANJO_KAMOKU_CD");
            sql.Append(", A.HOJO_KAMOKU_CD");
            sql.Append(", A.BUMON_CD");
            return Util.ToString(sql);
        }
        #endregion
    }
}
