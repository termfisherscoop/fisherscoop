﻿namespace jp.co.fsi.zm.zmde1011
{
    partial class ZMDE1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new jp.co.fsi.common.FsiPanel();
            this.lblShukkin = new System.Windows.Forms.Label();
            this.lblGokeiZandaka = new System.Windows.Forms.Label();
            this.lblNyukin = new System.Windows.Forms.Label();
            this.lblGokei = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblBumonNm = new System.Windows.Forms.Label();
            this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtZandaka = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuNm = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoKamokuNm = new System.Windows.Forms.Label();
            this.txtHojoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBakKanjoKamoku = new System.Windows.Forms.Label();
            this.lblBakHojoKamokuCd = new System.Windows.Forms.Label();
            this.lblBakBumonCd = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.pnlDebug.SuspendLayout();
            this.panel4.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlDebug.Location = new System.Drawing.Point(4, 654);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "出納帳入力";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Silver;
            this.panel4.Controls.Add(this.lblShukkin);
            this.panel4.Controls.Add(this.lblGokeiZandaka);
            this.panel4.Controls.Add(this.lblNyukin);
            this.panel4.Controls.Add(this.lblGokei);
            this.panel4.Location = new System.Drawing.Point(16, 613);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1052, 32);
            this.panel4.TabIndex = 910;
            this.panel4.Tag = "CHANGE";
            // 
            // lblShukkin
            // 
            this.lblShukkin.BackColor = System.Drawing.Color.Transparent;
            this.lblShukkin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblShukkin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukkin.Location = new System.Drawing.Point(704, 5);
            this.lblShukkin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShukkin.Name = "lblShukkin";
            this.lblShukkin.Size = new System.Drawing.Size(106, 21);
            this.lblShukkin.TabIndex = 914;
            this.lblShukkin.Tag = "CHANGE";
            this.lblShukkin.Text = "　";
            this.lblShukkin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGokeiZandaka
            // 
            this.lblGokeiZandaka.BackColor = System.Drawing.Color.Transparent;
            this.lblGokeiZandaka.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGokeiZandaka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokeiZandaka.Location = new System.Drawing.Point(811, 5);
            this.lblGokeiZandaka.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGokeiZandaka.Name = "lblGokeiZandaka";
            this.lblGokeiZandaka.Size = new System.Drawing.Size(106, 21);
            this.lblGokeiZandaka.TabIndex = 913;
            this.lblGokeiZandaka.Tag = "CHANGE";
            this.lblGokeiZandaka.Text = "　";
            this.lblGokeiZandaka.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNyukin
            // 
            this.lblNyukin.BackColor = System.Drawing.Color.Transparent;
            this.lblNyukin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNyukin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNyukin.Location = new System.Drawing.Point(597, 5);
            this.lblNyukin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNyukin.Name = "lblNyukin";
            this.lblNyukin.Size = new System.Drawing.Size(106, 21);
            this.lblNyukin.TabIndex = 912;
            this.lblNyukin.Tag = "CHANGE";
            this.lblNyukin.Text = "　";
            this.lblNyukin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGokei
            // 
            this.lblGokei.BackColor = System.Drawing.Color.Silver;
            this.lblGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei.Location = new System.Drawing.Point(320, 7);
            this.lblGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGokei.Name = "lblGokei";
            this.lblGokei.Size = new System.Drawing.Size(133, 19);
            this.lblGokei.TabIndex = 910;
            this.lblGokei.Tag = "CHANGE";
            this.lblGokei.Text = "合　　計";
            this.lblGokei.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblTantoshaNm.ForeColor = System.Drawing.Color.Black;
            this.lblTantoshaNm.Location = new System.Drawing.Point(154, 1);
            this.lblTantoshaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(253, 24);
            this.lblTantoshaNm.TabIndex = 0;
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = false;
            this.txtTantoshaCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Enabled = false;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTantoshaCd.Location = new System.Drawing.Point(82, 2);
            this.txtTantoshaCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.ReadOnly = true;
            this.txtTantoshaCd.Size = new System.Drawing.Size(69, 23);
            this.txtTantoshaCd.TabIndex = 1;
            this.txtTantoshaCd.TabStop = false;
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.LightCyan;
            this.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(259, 2);
            this.lblDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(347, 24);
            this.lblDate.TabIndex = 5;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonNm
            // 
            this.lblBumonNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblBumonNm.ForeColor = System.Drawing.Color.Black;
            this.lblBumonNm.Location = new System.Drawing.Point(154, 2);
            this.lblBumonNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonNm.Name = "lblBumonNm";
            this.lblBumonNm.Size = new System.Drawing.Size(253, 24);
            this.lblBumonNm.TabIndex = 7;
            this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AutoSizeFromLength = false;
            this.txtBumonCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtBumonCd.DisplayLength = null;
            this.txtBumonCd.Enabled = false;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBumonCd.Location = new System.Drawing.Point(82, 2);
            this.txtBumonCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtBumonCd.MaxLength = 4;
            this.txtBumonCd.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.ReadOnly = true;
            this.txtBumonCd.Size = new System.Drawing.Size(69, 23);
            this.txtBumonCd.TabIndex = 1;
            this.txtBumonCd.TabStop = false;
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtZandaka
            // 
            this.txtZandaka.AutoSizeFromLength = false;
            this.txtZandaka.BackColor = System.Drawing.SystemColors.Window;
            this.txtZandaka.DisplayLength = null;
            this.txtZandaka.Enabled = false;
            this.txtZandaka.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtZandaka.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZandaka.Location = new System.Drawing.Point(82, 3);
            this.txtZandaka.Margin = new System.Windows.Forms.Padding(4);
            this.txtZandaka.MaxLength = 4;
            this.txtZandaka.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtZandaka.Name = "txtZandaka";
            this.txtZandaka.ReadOnly = true;
            this.txtZandaka.Size = new System.Drawing.Size(173, 23);
            this.txtZandaka.TabIndex = 1;
            this.txtZandaka.TabStop = false;
            this.txtZandaka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblKanjoKamokuNm
            // 
            this.lblKanjoKamokuNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoKamokuNm.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamokuNm.Location = new System.Drawing.Point(154, 0);
            this.lblKanjoKamokuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuNm.Name = "lblKanjoKamokuNm";
            this.lblKanjoKamokuNm.Size = new System.Drawing.Size(253, 24);
            this.lblKanjoKamokuNm.TabIndex = 7;
            this.lblKanjoKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCd
            // 
            this.txtKanjoKamokuCd.AutoSizeFromLength = false;
            this.txtKanjoKamokuCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtKanjoKamokuCd.DisplayLength = null;
            this.txtKanjoKamokuCd.Enabled = false;
            this.txtKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKanjoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKanjoKamokuCd.Location = new System.Drawing.Point(82, 1);
            this.txtKanjoKamokuCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamokuCd.MaxLength = 4;
            this.txtKanjoKamokuCd.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtKanjoKamokuCd.Name = "txtKanjoKamokuCd";
            this.txtKanjoKamokuCd.ReadOnly = true;
            this.txtKanjoKamokuCd.Size = new System.Drawing.Size(69, 23);
            this.txtKanjoKamokuCd.TabIndex = 1;
            this.txtKanjoKamokuCd.TabStop = false;
            this.txtKanjoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblHojoKamokuNm
            // 
            this.lblHojoKamokuNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblHojoKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblHojoKamokuNm.ForeColor = System.Drawing.Color.Black;
            this.lblHojoKamokuNm.Location = new System.Drawing.Point(154, 1);
            this.lblHojoKamokuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoKamokuNm.Name = "lblHojoKamokuNm";
            this.lblHojoKamokuNm.Size = new System.Drawing.Size(253, 24);
            this.lblHojoKamokuNm.TabIndex = 7;
            this.lblHojoKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoKamokuCd
            // 
            this.txtHojoKamokuCd.AutoSizeFromLength = false;
            this.txtHojoKamokuCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtHojoKamokuCd.DisplayLength = null;
            this.txtHojoKamokuCd.Enabled = false;
            this.txtHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtHojoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHojoKamokuCd.Location = new System.Drawing.Point(82, 2);
            this.txtHojoKamokuCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtHojoKamokuCd.MaxLength = 4;
            this.txtHojoKamokuCd.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtHojoKamokuCd.Name = "txtHojoKamokuCd";
            this.txtHojoKamokuCd.ReadOnly = true;
            this.txtHojoKamokuCd.Size = new System.Drawing.Size(69, 23);
            this.txtHojoKamokuCd.TabIndex = 1;
            this.txtHojoKamokuCd.TabStop = false;
            this.txtHojoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBakKanjoKamoku
            // 
            this.lblBakKanjoKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblBakKanjoKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakKanjoKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBakKanjoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBakKanjoKamoku.Location = new System.Drawing.Point(0, 0);
            this.lblBakKanjoKamoku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBakKanjoKamoku.Name = "lblBakKanjoKamoku";
            this.lblBakKanjoKamoku.Size = new System.Drawing.Size(1106, 28);
            this.lblBakKanjoKamoku.TabIndex = 917;
            this.lblBakKanjoKamoku.Tag = "CHANGE";
            this.lblBakKanjoKamoku.Text = "勘定科目";
            this.lblBakKanjoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBakHojoKamokuCd
            // 
            this.lblBakHojoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblBakHojoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakHojoKamokuCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBakHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBakHojoKamokuCd.Location = new System.Drawing.Point(0, 0);
            this.lblBakHojoKamokuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBakHojoKamokuCd.Name = "lblBakHojoKamokuCd";
            this.lblBakHojoKamokuCd.Size = new System.Drawing.Size(1106, 28);
            this.lblBakHojoKamokuCd.TabIndex = 918;
            this.lblBakHojoKamokuCd.Tag = "CHANGE";
            this.lblBakHojoKamokuCd.Text = "補助科目";
            this.lblBakHojoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBakBumonCd
            // 
            this.lblBakBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBakBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakBumonCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBakBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBakBumonCd.Location = new System.Drawing.Point(0, 0);
            this.lblBakBumonCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBakBumonCd.Name = "lblBakBumonCd";
            this.lblBakBumonCd.Size = new System.Drawing.Size(1106, 28);
            this.lblBakBumonCd.TabIndex = 919;
            this.lblBakBumonCd.Tag = "CHANGE";
            this.lblBakBumonCd.Text = "部門";
            this.lblBakBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1106, 30);
            this.label2.TabIndex = 920;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "繰越残高";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1106, 28);
            this.label1.TabIndex = 921;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "担当者";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.txtZandaka);
            this.fsiPanel7.Controls.Add(this.lblDate);
            this.fsiPanel7.Controls.Add(this.label2);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(4, 144);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(1106, 30);
            this.fsiPanel7.TabIndex = 3;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtHojoKamokuCd);
            this.fsiPanel6.Controls.Add(this.lblHojoKamokuNm);
            this.fsiPanel6.Controls.Add(this.lblBakHojoKamokuCd);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 109);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(1106, 28);
            this.fsiPanel6.TabIndex = 2;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtBumonCd);
            this.fsiPanel4.Controls.Add(this.lblBumonNm);
            this.fsiPanel4.Controls.Add(this.lblBakBumonCd);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 39);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(1106, 28);
            this.fsiPanel4.TabIndex = 1;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtTantoshaCd);
            this.fsiPanel5.Controls.Add(this.lblTantoshaNm);
            this.fsiPanel5.Controls.Add(this.label1);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 74);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(1106, 28);
            this.fsiPanel5.TabIndex = 1;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtKanjoKamokuCd);
            this.fsiPanel3.Controls.Add(this.lblKanjoKamokuNm);
            this.fsiPanel3.Controls.Add(this.lblBakKanjoKamoku);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(1106, 28);
            this.fsiPanel3.TabIndex = 1;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // mtbList
            // 
            this.mtbList.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mtbList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtbList.FixedCols = 0;
            this.mtbList.FocusField = null;
            this.mtbList.ForeColor = System.Drawing.Color.Navy;
            this.mtbList.Location = new System.Drawing.Point(5, 5);
            this.mtbList.Margin = new System.Windows.Forms.Padding(4);
            this.mtbList.Name = "mtbList";
            this.mtbList.NotSelectableCols = 0;
            this.mtbList.SelectRange = null;
            this.mtbList.Size = new System.Drawing.Size(1105, 372);
            this.mtbList.TabIndex = 0;
            this.mtbList.Text = "SjMultiTable1";
            this.mtbList.UndoBufferEnabled = false;
            this.mtbList.RecordEnter += new systembase.table.UTable.RecordEnterEventHandler(this.mtbList_RecordEnter);
            this.mtbList.FieldValidating += new systembase.table.UTable.FieldValidatingEventHandler(this.mtbList_FieldValidating);
            this.mtbList.FieldEnter += new systembase.table.UTable.FieldEnterEventHandler(this.mtbList_FieldEnter);
            this.mtbList.EditStart += new systembase.table.UTable.EditStartEventHandler(this.mtbList_EditStart);
            this.mtbList.InitializeEditor += new systembase.table.UTable.InitializeEditorEventHandler(this.mtbList_InitializeEditor);
            this.mtbList.Leave += new System.EventHandler(this.mtbList_Leave);
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 3);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 44);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 5;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1114, 178);
            this.fsiTableLayoutPanel1.TabIndex = 923;
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Controls.Add(this.mtbList, 0, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(4, 224);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 1;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(1115, 382);
            this.fsiTableLayoutPanel2.TabIndex = 924;
            // 
            // ZMDE1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 918);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.panel4);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMDE1011";
            this.Text = "出納帳入力";
            this.Shown += new System.EventHandler(this.ZMDE1011_Shown);
            this.Controls.SetChildIndex(this.panel4, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.pnlDebug.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.FsiPanel panel4;
        private System.Windows.Forms.Label lblShukkin;
        private System.Windows.Forms.Label lblGokeiZandaka;
        private System.Windows.Forms.Label lblNyukin;
        private System.Windows.Forms.Label lblGokei;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblBumonNm;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private jp.co.fsi.common.controls.FsiTextBox txtZandaka;
        private System.Windows.Forms.Label lblKanjoKamokuNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCd;
        private System.Windows.Forms.Label lblHojoKamokuNm;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuCd;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblBakKanjoKamoku;
        private System.Windows.Forms.Label lblBakHojoKamokuCd;
        private System.Windows.Forms.Label lblBakBumonCd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel3;
        private common.controls.SjMultiTable mtbList;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
    }
}