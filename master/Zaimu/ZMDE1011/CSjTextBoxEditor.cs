﻿using jp.co.fsi.common.controls;
using jp.co.fsi.common.util;

using systembase.table;
using System.Windows.Forms;

namespace jp.co.fsi.zm.zmde1011
{
    class CFsiTextBoxEditor : FsiTextBox, IEditor
    {
        public new event IEditor.LeaveEventHandler Leave;
        public delegate void _LeaveEventHandler(object sender, string direction);

        public event IEditor.ValueChangedEventHandler ValueChanged;
        public delegate void _ValueChangedEventHandler(object sender);

        public Control Control()
        {
            return this;
        }

        public object Value
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = Util.ToString(value);
            }
        }

        public void EditEnter(char key)
        {
            this.SelectionStart = 0;
        }

        public void EditLeave()
        {
        }

        public void Initialize(UTable.CField field)
        {
            UTable.CDynamicSetting _with1 = field.DynamicSetting();
            this.Font = _with1.Font();
            switch (_with1.HorizontalAlignment())
            {
                case UTable.EHAlign.LEFT:
                    this.TextAlign = HorizontalAlignment.Left;
                    break;
                case UTable.EHAlign.MIDDLE:
                    this.TextAlign = HorizontalAlignment.Center;
                    break;
                case UTable.EHAlign.RIGHT:
                    this.TextAlign = HorizontalAlignment.Right;
                    break;
            }
        }

        private void CFsiTextBoxEditor_TextChanged(object sender, System.EventArgs e)
        {
            ValueChanged(this);
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                    if (!this.Multiline || this.SelectionStart <= (this.Text + "\r").IndexOf("\r"))
                    {
                        return false;
                    }
                    break;
                case Keys.Down:
                    if (!this.Multiline || this.SelectionStart >= ("\r" + this.Text).LastIndexOf("\r"))
                    {
                        return false;
                    }
                    break;
                case Keys.Left:
                    if (this.SelectionLength == 0 & this.SelectionStart == 0)
                    {
                        return false;
                    }
                    break;
                case Keys.Right:
                    if (this.SelectionLength == 0 & this.SelectionStart == this.Text.Length)
                    {
                        return false;
                    }
                    break;
            }
            return base.IsInputKey(keyData);
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
        switch (keyData)
            {
                case Keys.Escape:
                    if (Leave != null)
                    {
                        Leave(this, "ESC");
                    }

                    return true;
                case Keys.Up:
                    if (Leave != null)
                    {
                        Leave(this, "UP");
                    }

                    return true;
                case Keys.Down:
                    if (Leave != null)
                    {
                        Leave(this, "DOWN");
                    }

                    return true;
                case Keys.Left:
                    if (Leave != null)
                    {
                        Leave(this, "LEFT");
                    }

                    return true;
                case Keys.Right:
                    if (Leave != null)
                    {
                        Leave(this, "RIGHT");
                    }

                    return true;
                case Keys.Enter:
                    if (!this.Multiline)
                    {
                        if (Leave != null)
                        {
                            Leave(this, "ENTER");
                        }
                        return true;
                    }
                    break;
                case (Keys.Enter | Keys.Shift):
                    if (!this.Multiline)
                    {
                        if (Leave != null)
                        {
                            Leave(this, "ENTER_PREV");
                        }
                        return true;
                    }
                    break;
                case Keys.Tab:
                    if (Leave != null)
                    {
                        Leave(this, "TAB");
                    }

                    return true;
                case (Keys.Tab | Keys.Shift):
                    if (Leave != null)
                    {
                        Leave(this, "TAB_PREV");
                    }

                    return true;
            }
            return base.ProcessDialogKey(keyData);
        }

    }
}