﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmde1031
{
    /// <summary>
    /// 元帳照会(ZMDE1034)
    /// </summary>
    public partial class ZMDE1034 : BasePgForm
    {
        #region 変数
        private string _kanjoKamokuCd;       // 勘定科目コード
        private string _hojoKamokuCd;        // 補助科目コード(＝取引先コード)
        
        private int ShishoCode;              // 支所コード
        private bool gridEnterFlag;          // 
        #endregion

        #region プロパティ

        /// <summary>
        /// 勘定科目コード
        /// </summary>
        public string KanjoKamokuCd
        {
            get
            {
                return _kanjoKamokuCd;
            }
            set
            {
                _kanjoKamokuCd = Util.ToString(value);
            }
        }

        /// <summary>
        /// 補助科目コード
        /// </summary>
        public string HojoKamokuCd
        {
            get
            {
                return _hojoKamokuCd;
            }
            set
            {
                _hojoKamokuCd = Util.ToString(value);
            }
        }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDE1034()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            //// サイズを縮める
            //this.Size = new Size(600, 500);
            // ESC F1 F6のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 科目情報
            this.lblKamokuNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.ShishoCode.ToString(), this.KanjoKamokuCd)
                + "(" + this.GetHojoKmkNm(this.ShishoCode.ToString(), this.KanjoKamokuCd, this.HojoKamokuCd) + ")";

            // 日付範囲初期値(期首日からシステム日付まで)
            DateTime kishu = new DateTime(this.UInfo.KaikeiNendo, 4, 1);
            SetJpFr(Util.ConvJpDate(kishu, this.Dba));
            SetJpTo(Util.ConvJpDate(DateTime.Now, this.Dba));

            // 初期表示
            SearchData(false);
            dgvList.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (ActiveCtlNm)
            {
                case "txtGengoYearDenpyoDateFr":
                case "txtMonthDenpyoDateFr":
                case "txtDayDenpyoDateFr":
                case "txtGengoYearDenpyoDateTo":
                case "txtMonthDenpyoDateTo":
                case "txtDayDenpyoDateTo":
                    btnF1.Text = "F1" + "\n\r" + "\n\r" + "検索";
                    btnF6.Text = "F6" + "\n\r" + "\n\r" + "検索開始";
                    //SearchData(true);
                    break;
                default:
                    btnF1.Text = "F1" + "\n\r" + "\n\r" + "日付入力";
                    btnF6.Text = "F6" + "\n\r" + "\n\r" + "備考更新";
                    //SearchData(false);
                    break;
            }

            switch (ActiveCtlNm)
            {
                case "txtGengoYearDenpyoDateFr":
                case "txtGengoYearDenpyoDateTo":
                    btnF1.Enabled = true;
                    break;
                default:
                    btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // ダイアログとしての処理結果を返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            int flg = 0;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			//アクティブコントロールごとの処理
			switch (this.ActiveCtlNm)
            {
                case "txtGengoYearDenpyoDateFr":
                case "txtGengoYearDenpyoDateTo":
                    #region 元号検索
                    // アセンブリのロード
                    //System.Reflection.Assembly asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //Type t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    Type t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearDenpyoDateFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoDenpyoDateFr.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoDenpyoDateFr.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    //SetJpFr(Util.FixJpDate(this.lblGengoDenpyoDateFr.Text, 
                                    //                    this.txtGengoYearDenpyoDateFr.Text,
                                    //                    this.txtMonthDenpyoDateFr.Text, 
                                    //                    this.txtDayDenpyoDateFr.Text, 
                                    //                    this.Dba));
                                    SetJpFr();

                                }
                            }
                            if (this.ActiveCtlNm == "txtGengoYearDenpyoDateTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoDenpyoDateFr.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoDenpyoDateTo.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    //SetJpTo(Util.FixJpDate(this.lblGengoDenpyoDateTo.Text,
                                    //                    this.txtGengoYearDenpyoDateTo.Text,
                                    //                    this.txtMonthDenpyoDateTo.Text,
                                    //                    this.txtDayDenpyoDateTo.Text,
                                    //                    this.Dba));
                                    SetJpTo();
                                }
                            }
                            flg = 1;
                        }

                    }
                    #endregion
                    break;
                case "txtMonthDenpyoDateFr":
                case "txtDayDenpyoDateFr":
                case "txtMonthDenpyoDateTo":
                case "txtDayDenpyoDateTo":
                    // NONE
                    break;
                default:
                    // 初期状態に戻す
                    SearchData(true);
                    // 伝票日付(自)にフォーカスを戻す
                    this.txtGengoYearDenpyoDateFr.Focus();
                    this.txtGengoYearDenpyoDateFr.SelectAll();
                    flg = 1;
                    break;
            }

            // グリッド中からのF1対応
            if (gridEnterFlag && flg == 0)
            {
                // 初期状態に戻す
                SearchData(true);
                // 伝票日付(自)にフォーカスを戻す
                this.txtGengoYearDenpyoDateFr.Focus();
                this.txtGengoYearDenpyoDateFr.SelectAll();
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            //アクティブコントロールごとの処理
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearDenpyoDateFr":
                case "txtMonthDenpyoDateFr":
                case "txtDayDenpyoDateFr":
                case "txtGengoYearDenpyoDateTo":
                case "txtMonthDenpyoDateTo":
                case "txtDayDenpyoDateTo":
                    // 検索開始
                    //SearchData(false);
                    dgvList.Focus();
                    break;
                default:
                    // 備考保存
                    // 全項目を再度入力値チェック
                    if (!ValidateDataGridViewAll())
                    {
                        // エラーありの場合ここで処理終了
                        return;
                    }

                    // 確認メッセージを表示
                    if (Msg.ConfYesNo("更新しますか？") == DialogResult.No)
                    {
                        // 「いいえ」を押されたら処理終了
                        return;
                    }

                    try
                    {
                        decimal denpyoBango;
                        decimal denpyoKubun;
                        string biko;
                        DateTime upd = System.DateTime.Now;

                        // 伝票抽出用WHERE句
                        string where = "KAISHA_CD = @KAISHA_CD";
                        where += " AND SHISHO_CD = @SHISHO_CD";
                        where += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                        where += " AND DENPYO_BANGO = @DENPYO_BANGO";
                        where += " AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD";
                        where += " AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD";
                        where += " AND DENPYO_KUBUN = @DENPYO_KUBUN";

                        // トランザクション開始
                        this.Dba.BeginTransaction();

                        foreach (DataRow dr in ((DataTable)dgvList.DataSource).Rows)
                        {
                            // 変更フラグがオンの行を更新
                            if (Util.ToInt(dr["HENKO"]) == 1)
                            {
                                denpyoBango = Util.ToDecimal(dr["DENPYO_BANGO"]);
                                denpyoKubun = Util.ToDecimal(dr["DENPYO_KUBUN"]);
                                biko = Util.ToString(dr["BIKO"]);
                                ArrayList alParamsZmBiko = SetZmShiwakeMeisaiBikoParams(
                                    denpyoBango, denpyoKubun, biko, upd);
                                this.Dba.Update("TB_ZM_SHIWAKE_MEISAI"
                                                , (DbParamCollection)alParamsZmBiko[1]
                                                , where
                                                , (DbParamCollection)alParamsZmBiko[0]);
                            }
                        }

                        // トランザクションをコミット
                        this.Dba.Commit();
                    }
                    finally
                    {
                        // ロールバック
                        this.Dba.Rollback();
                    }

                    // 前画面に戻る
                    PressEsc();
                    break;
            }
        }
        #endregion

        #region イベント

        /// <summary>
        /// グリッドLeave時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Enter(object sender, System.EventArgs e)
        {
            // グリッドフォーカス時にOnMoveFocusで処理されない？ため
            btnF1.Text = "F1" + "\n\r" + "\n\r" + "日付入力";
            btnF6.Text = "F6" + "\n\r" + "\n\r" + "備考更新";
            btnF1.Enabled = true;
            gridEnterFlag = true;
            //SearchData(false);
        }

        /// <summary>
        /// グリッドEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Leave(object sender, EventArgs e)
        {
            gridEnterFlag = false;
        }

        /// <summary>
        /// グリッドセル値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            switch (dgvList.Columns[e.ColumnIndex].Name)
            {
                case "BIKO":                    // 備考列の時
                    if (!IsValidBiko(e.FormattedValue))
                    {
                        dgvList.CancelEdit();
                        e.Cancel = true;
                        return;
                    }
                    // 変更フラグをオン
                    dgvList.Rows[e.RowIndex].Cells["HENKO"].Value = 1;      
                    break;
                default:
                    // NONE
                    break;
            }
        }

        /// <summary>
        /// 伝票日付(自)・年値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearDenpyoDateFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtGengoYearDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYearDenpyoDateFr.SelectAll();
            }
            else
            {
                this.txtGengoYearDenpyoDateFr.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearDenpyoDateFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 伝票日付(自)・月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthDenpyoDateFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthDenpyoDateFr.Text, this.txtMonthDenpyoDateFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthDenpyoDateFr.SelectAll();
            }
            else
            {
                this.txtMonthDenpyoDateFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthDenpyoDateFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 伝票日付(自)・日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayDenpyoDateFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayDenpyoDateFr.Text, this.txtDayDenpyoDateFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayDenpyoDateFr.SelectAll();
            }
            else
            {
                this.txtDayDenpyoDateFr.Text = Util.ToString(IsValid.SetDay(this.txtDayDenpyoDateFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 伝票日付(至)・年値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearDenpyoDateTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtGengoYearDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYearDenpyoDateTo.SelectAll();
            }
            else
            {
                this.txtGengoYearDenpyoDateTo.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearDenpyoDateTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 伝票日付(至)・月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthDenpyoDateTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthDenpyoDateTo.Text, this.txtMonthDenpyoDateTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthDenpyoDateTo.SelectAll();
            }
            else
            {
                this.txtMonthDenpyoDateTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthDenpyoDateTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 伝票日付(至)・日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayDenpyoDateTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayDenpyoDateTo.Text, this.txtDayDenpyoDateTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayDenpyoDateTo.SelectAll();
            }
            else
            {
                this.txtDayDenpyoDateTo.Text = Util.ToString(IsValid.SetDay(this.txtDayDenpyoDateTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">グリッド初期化のみの場合true</param>
        private void SearchData(bool isInitial)
        {
            // 勘定科目貸借区分取得
            int taishakuKubun = GetTaishakuKubun(this.KanjoKamokuCd);

            // 残高初期化
            decimal zandaka = 0;

            // 日付値
            DateTime dateFr = Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text,
                this.txtGengoYearDenpyoDateFr.Text,
                this.txtMonthDenpyoDateFr.Text,
                this.txtDayDenpyoDateFr.Text,
                this.Dba);
            DateTime dateTo = Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text,
                this.txtGengoYearDenpyoDateTo.Text,
                this.txtMonthDenpyoDateTo.Text,
                this.txtDayDenpyoDateTo.Text,
                this.Dba);

            // 期首日・期中日開始タイプ
            DateTime kishu = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            int type = (dateFr.Month == kishu.Month && dateFr.Day == kishu.Day) ? 1 : 2;

            // データアクセスパラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.KanjoKamokuCd);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10,
                isInitial ? "-1" : this.HojoKamokuCd);                   // 初期処理の場合はあり得ない検索条件
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, 10, dateFr);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, 10, dateTo);

            // グリッドデータソース定義
            DataTable gdt = new DataTable();
            gdt.Columns.Add("KAISHA_CD", Type.GetType("System.Decimal"));               // 会社コード
            gdt.Columns.Add("KAIKEI_NENDO", Type.GetType("System.Decimal"));            // 会計年度
            gdt.Columns.Add("DENPYO_BANGO", Type.GetType("System.Decimal"));            // 伝票番号
            gdt.Columns.Add("KANJO_KAMOKU_CD", Type.GetType("System.Decimal"));         // 勘定科目コード
            gdt.Columns.Add("HOJO_KAMOKU_CD", Type.GetType("System.Decimal"));          // 補助科目コード
            gdt.Columns.Add("DENPYO_KUBUN", Type.GetType("System.Decimal"));            // 伝票区分
            gdt.Columns.Add("DENPYO_DATE", Type.GetType("System.DateTime"));            // 伝票日付
            gdt.Columns.Add("DISP_DATE", Type.GetType("System.String"));                // 表示用伝票日付
            gdt.Columns.Add("DISP_BANGO", Type.GetType("System.String"));               // 表示用伝票番号
            gdt.Columns.Add("TEKIYO", Type.GetType("System.String"));                   // 摘要
            gdt.Columns.Add("DrKINGAKU", Type.GetType("System.Decimal"));               // 借方金額
            gdt.Columns.Add("CrKINGAKU", Type.GetType("System.Decimal"));               // 貸方金額
            gdt.Columns.Add("ZANDAKA", Type.GetType("System.Decimal"));                 // 残高
            gdt.Columns.Add("BIKO", Type.GetType("System.String"));                     // 備考
            gdt.Columns.Add("HENKO", Type.GetType("System.Int32"));                     // 変更フラグ

            // 補助元帳データ取得
            DataTable dt;
            if (type == 1)
            {
                // 元帳データ
                dt = Dba.GetDataTableFromSqlWithParams(GetHojoMotochoSql(type), dpc);
            }
            else
            {
                // 残高データ
                DataTable dtZan = Dba.GetDataTableFromSqlWithParams(GetHojoZandakaSql(), dpc);
                // グリッドデータへ追加
                if (!isInitial && dtZan.Rows.Count > 0)
                {
                    DataRow gdr = gdt.NewRow();
                    gdr["KAISHA_CD"] = this.UInfo.KaishaCd;
                    gdr["KAIKEI_NENDO"] = this.UInfo.KaikeiNendo;
                    gdr["DENPYO_BANGO"] = DBNull.Value;
                    gdr["KANJO_KAMOKU_CD"] = this.KanjoKamokuCd;
                    gdr["HOJO_KAMOKU_CD"] = this.HojoKamokuCd;
                    gdr["DENPYO_KUBUN"] = DBNull.Value;
                    gdr["DISP_DATE"] = DBNull.Value;
                    gdr["DISP_BANGO"] = DBNull.Value;
                    gdr["TEKIYO"] = "繰越残高";
                    gdr["DrKINGAKU"] = Util.ToDecimal(dtZan.Rows[0]["DrKINGAKU"]);
                    gdr["CrKINGAKU"] = Util.ToDecimal(dtZan.Rows[0]["CrKINGAKU"]);
                    if (taishakuKubun == 1)
                    {
                        zandaka = Util.ToDecimal(dtZan.Rows[0]["DrKINGAKU"])
                                            - Util.ToDecimal(dtZan.Rows[0]["CrKINGAKU"]);
                    }
                    else if (taishakuKubun == 2)
                    {
                         zandaka = Util.ToDecimal(dtZan.Rows[0]["CrKINGAKU"])
                                            - Util.ToDecimal(dtZan.Rows[0]["DrKINGAKU"]);
                    }
                    gdr["ZANDAKA"] = zandaka;
                    gdt.Rows.Add(gdr);
                }

                // 元帳データ
                dt = Dba.GetDataTableFromSqlWithParams(GetHojoMotochoSql(type), dpc);
            }
            // グリッドデータ追加
            foreach (DataRow dr in dt.Rows)
            {
                DataRow gdr = gdt.NewRow();
                gdr["KAISHA_CD"] = this.UInfo.KaishaCd;
                gdr["KAIKEI_NENDO"] = this.UInfo.KaikeiNendo;
                gdr["DENPYO_BANGO"] = Util.ToDecimal(dr["DENPYO_BANGO"]);
                gdr["KANJO_KAMOKU_CD"] = this.KanjoKamokuCd;
                gdr["HOJO_KAMOKU_CD"] = this.HojoKamokuCd;
                gdr["DENPYO_KUBUN"] = Util.ToDecimal(dr["DENPYO_KUBUN"]);
                string[] jpDate = Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba);
                gdr["DISP_DATE"] = Util.ToDecimal(dr["DENPYO_BANGO"]) == 0 ? 
                                    "" : String.Format("{0, 2}", jpDate[2])
                                            + "/" + String.Format("{0, 2}", jpDate[3])
                                            + "/" + String.Format("{0, 2}", jpDate[4]);
                gdr["DISP_BANGO"] = Util.ToDecimal(dr["DENPYO_BANGO"]) == 0 ?
                                    "" : String.Format("{0, 6}", dr["DENPYO_BANGO"]);
                gdr["TEKIYO"] = Util.ToString(dr["TEKIYO"]).Trim();
                gdr["DrKINGAKU"] = Util.ToDecimal(dr["DrKINGAKU"]);
                gdr["CrKINGAKU"] = Util.ToDecimal(dr["CrKINGAKU"]);
                if (taishakuKubun == 1)
                {
                    zandaka += Util.ToDecimal(dr["DrKINGAKU"])
                                        - Util.ToDecimal(dr["CrKINGAKU"]);
                }
                else if (taishakuKubun == 2)
                {
                    zandaka += Util.ToDecimal(dr["CrKINGAKU"])
                                        - Util.ToDecimal(dr["DrKINGAKU"]);
                }
                gdr["ZANDAKA"] = zandaka;
                gdr["BIKO"] = Util.ToString(dr["BIKO"]);
                gdr["HENKO"] = 0;
                gdt.Rows.Add(gdr);
            }

            // グリッドへデータソース設定
            this.dgvList.DataSource = gdt;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvList.AlternatingRowsDefaultCellStyle.BackColor = Color.Aquamarine;

            // 編集設定
            this.dgvList.SelectionMode = DataGridViewSelectionMode.CellSelect;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Hiragana;       // 日本語入力可能
            this.dgvList.ReadOnly = false;
            this.dgvList.EditMode = DataGridViewEditMode.EditOnEnter;
            this.dgvList.Columns[7].ReadOnly = true;
            this.dgvList.Columns[8].ReadOnly = true;
            this.dgvList.Columns[9].ReadOnly = true;
            this.dgvList.Columns[10].ReadOnly = true;
            this.dgvList.Columns[11].ReadOnly = true;
            this.dgvList.Columns[12].ReadOnly = true;

            // 列設定
            DataGridViewCellStyle curStyle = new DataGridViewCellStyle();       // 金額列スタイル
            curStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            curStyle.Format = "#,###";
            this.dgvList.Columns[0].Visible = false;
            this.dgvList.Columns[1].Visible = false;
            this.dgvList.Columns[2].Visible = false;
            this.dgvList.Columns[3].Visible = false;
            this.dgvList.Columns[4].Visible = false;
            this.dgvList.Columns[5].Visible = false;
            this.dgvList.Columns[6].Visible = false;
            this.dgvList.Columns[7].HeaderText = "日付";
            this.dgvList.Columns[7].Width = 70;
            this.dgvList.Columns[8].HeaderText = "伝票NO";
            this.dgvList.Columns[8].Width = 60;
            this.dgvList.Columns[9].HeaderText = "摘要";
            this.dgvList.Columns[9].Width = 200;
            this.dgvList.Columns[10].HeaderText = "借方金額";
            this.dgvList.Columns[10].DefaultCellStyle = curStyle;
            this.dgvList.Columns[10].Width = 100;
            this.dgvList.Columns[11].HeaderText = "貸方金額";
            this.dgvList.Columns[11].DefaultCellStyle = curStyle;
            this.dgvList.Columns[11].Width = 100;
            this.dgvList.Columns[12].HeaderText = "残高";
            this.dgvList.Columns[12].DefaultCellStyle = curStyle;
            this.dgvList.Columns[12].Width = 100;
            this.dgvList.Columns[13].HeaderText = "備考";
            this.dgvList.Columns[13].Width = 148;
            this.dgvList.Columns[13].ReadOnly = false;
            this.dgvList.Columns[14].Visible = false;

            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;

            // フォーカス移動
            if (!isInitial)
            {
                if (dgvList.RowCount > 0)
                {
                    dgvList.Rows[0].ReadOnly = (type == 2);   // 期中日開始の場合は先頭行は編集不可

                    this.dgvList.Focus();
                    this.dgvList.CurrentCell = this.dgvList.Rows[0].Cells[13];
                }
            }

            // 合計
            decimal DrTotal = 0;
            decimal CrTotal = 0;
            foreach (DataGridViewRow ro in this.dgvList.Rows)
            {
                DrTotal += Util.ToDecimal(ro.Cells["DrKINGAKU"].Value);
                CrTotal += Util.ToDecimal(ro.Cells["CrKINGAKU"].Value);
            }
            this.lblDrTotal.Text = Util.FormatNum(DrTotal);
            this.lblCrTotal.Text = Util.FormatNum(CrTotal);
            if (taishakuKubun == 1)
            {
                this.lblZandaka.Text = Util.FormatNum(DrTotal - CrTotal);
            }
            else if (taishakuKubun == 2)
            {
                this.lblZandaka.Text = Util.FormatNum(CrTotal - DrTotal);
            }
        }

        /// <summary>
        /// 補助元帳照会SQL
        /// </summary>
        /// <param name="type">1期首日開始 2期中日開始</param>
        private string GetHojoMotochoSql(int type)
        {
            string ret;

            DateTime dateFr = Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text,
                    this.txtGengoYearDenpyoDateFr.Text,
                    this.txtMonthDenpyoDateFr.Text,
                    this.txtDayDenpyoDateFr.Text,
                    this.Dba);

            StringBuilder sql = new StringBuilder();

            // SELECT句
            StringBuilder select = new StringBuilder();
            select.Append("SELECT A.KAISHA_CD AS KAISHA_CD");                           // 会社コード
            select.Append(", A.SHISHO_CD AS SHISHO_CD");                                // 支所コード
            select.Append(", A.KAIKEI_NENDO AS KAIKEI_NENDO");                          // 会計年度
            select.Append(", A.DENPYO_BANGO AS DENPYO_BANGO");                          // 伝票番号
            select.Append(", A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD");                    // 勘定科目コード
            select.Append(", A.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD");                      // 補助科目コード
            select.Append(", A.DENPYO_KUBUN AS DENPYO_KUBUN");                          // 伝票区分
            select.Append(", A.DENPYO_DATE AS DENPYO_DATE");                            // 伝票日付
            select.Append(", (SELECT MAX(B.TEKIYO)");
            select.Append("    FROM TB_ZM_SHIWAKE_MEISAI AS B");
            select.Append("    WHERE B.KAISHA_CD = A.KAISHA_CD");
            select.Append("    AND B.SHISHO_CD = A.SHISHO_CD");
            select.Append("    AND B.KAIKEI_NENDO = A.KAIKEI_NENDO");
            select.Append("    AND B.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD");
            select.Append("    AND B.HOJO_KAMOKU_CD = A.HOJO_KAMOKU_CD");
            select.Append("    AND B.DENPYO_DATE = A.DENPYO_DATE");
            select.Append("    AND B.DENPYO_BANGO = A.DENPYO_BANGO");
            select.Append("    AND B.GYO_BANGO = ");
            select.Append("    (SELECT MIN(C.GYO_BANGO)");
            select.Append("      FROM TB_ZM_SHIWAKE_MEISAI AS C");
            select.Append("      WHERE C.KAISHA_CD = B.KAISHA_CD"); 
            select.Append("      AND C.SHISHO_CD = B.SHISHO_CD");
            select.Append("      AND C.KAIKEI_NENDO = B.KAIKEI_NENDO");
            select.Append("      AND C.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            select.Append("      AND C.HOJO_KAMOKU_CD = B.HOJO_KAMOKU_CD");
            select.Append("      AND C.DENPYO_DATE = B.DENPYO_DATE ");
            select.Append("      AND C.DENPYO_BANGO = B.DENPYO_BANGO");
            select.Append("    )");
            select.Append("  ) AS TEKIYO");                                             // 摘要
            select.Append(", MIN(A.BIKO) AS BIKO");                                     // 備考
            select.Append(", SUM(");
            select.Append("   CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEINUKI_KINGAKU");
            select.Append("   ELSE 0 END");
            select.Append("  ) AS DrKINGAKU");                                          // 借方金額
            select.Append(", SUM(");
            select.Append("   CASE WHEN A.TAISHAKU_KUBUN <> 1 THEN A.ZEINUKI_KINGAKU");
            select.Append("   ELSE 0 END");
            select.Append("  ) AS CrKINGAKU");                                          // 貸方金額
            select.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS A");
            // GROUP ORDER句 
            StringBuilder groupOrder = new StringBuilder();
            groupOrder.Append(" GROUP BY A.KAISHA_CD");
            groupOrder.Append(", A.SHISHO_CD");
            groupOrder.Append(", A.KAIKEI_NENDO");
            groupOrder.Append(", A.DENPYO_DATE");
            groupOrder.Append(", A.DENPYO_BANGO");
            groupOrder.Append(", A.KANJO_KAMOKU_CD");
            groupOrder.Append(", A.HOJO_KAMOKU_CD");
            groupOrder.Append(", A.DENPYO_KUBUN");
            groupOrder.Append(" ORDER BY A.KAISHA_CD");
            groupOrder.Append(", A.SHISHO_CD");
            groupOrder.Append(", A.KAIKEI_NENDO");
            groupOrder.Append(", A.DENPYO_DATE");
            groupOrder.Append(", A.DENPYO_BANGO");
            groupOrder.Append(", A.KANJO_KAMOKU_CD");
            groupOrder.Append(", A.HOJO_KAMOKU_CD");
            groupOrder.Append(", A.DENPYO_KUBUN");

            // SQLステートメント
            StringBuilder where = new StringBuilder();
            if (type == 1)          // 期首日開始の場合
            {
                // WHERE句
                where.Append(" WHERE A.KAISHA_CD = @KAISHA_CD");
                where.Append(" AND A.SHISHO_CD = @SHISHO_CD");
                where.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
                where.Append(" AND A.MEISAI_KUBUN = 0");
                where.Append(" AND A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
                where.Append(" AND A.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
                where.Append(" AND A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME)");
            }
            else if (type == 2)     // 期中日開始の場合
            {
                // WHERE句
                where.Append(" WHERE A.KAISHA_CD = @KAISHA_CD");
                where.Append(" AND A.SHISHO_CD = @SHISHO_CD");
                where.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
                where.Append(" AND A.MEISAI_KUBUN = 0");
                where.Append(" AND A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
                where.Append(" AND A.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
                where.Append(" AND A.DENPYO_DATE BETWEEN CAST(@DENPYO_DATE_FR AS DATETIME)");
                where.Append(" AND CAST(@DENPYO_DATE_TO AS DATETIME)");
            }
            ret = Util.ToString(select)
                + Util.ToString(where)
                + Util.ToString(groupOrder);

            return ret;
        }

        /// <summary>
        /// 補助残高照会SQL
        /// </summary>
        private string GetHojoZandakaSql()
        {
            // 残高レコードSELECTステートメント
            StringBuilder zandakaSql = new StringBuilder();
            zandakaSql.Append("SELECT ");
            zandakaSql.Append("  SUM(");
            zandakaSql.Append("   CASE WHEN TAISHAKU_KUBUN = 1 THEN ZEINUKI_KINGAKU");
            zandakaSql.Append("   ELSE 0 END");
            zandakaSql.Append("  ) AS DrKINGAKU");
            zandakaSql.Append(", SUM(");
            zandakaSql.Append("   CASE WHEN TAISHAKU_KUBUN <> 1 THEN ZEINUKI_KINGAKU");
            zandakaSql.Append("   ELSE 0 END");
            zandakaSql.Append("  ) AS CrKINGAKU");
            zandakaSql.Append(" FROM TB_ZM_SHIWAKE_MEISAI");
            zandakaSql.Append(" WHERE KAISHA_CD = @KAISHA_CD"); 
            zandakaSql.Append(" AND SHISHO_CD = @SHISHO_CD");
            zandakaSql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            zandakaSql.Append(" AND MEISAI_KUBUN = 0");
            zandakaSql.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            zandakaSql.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
            zandakaSql.Append(" AND DENPYO_DATE < CAST(@DENPYO_DATE_FR AS DATETIME)");

            return Util.ToString(zandakaSql);
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_MEISAIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="denpyoKubun">伝票区分</param>
        /// <param name="biko">備考</param>
        /// <param name="upd">更新日時</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmShiwakeMeisaiBikoParams(decimal denpyoBango, decimal denpyoKubun,
                                                                            string biko, DateTime upd)
        {
            ArrayList alParams = new ArrayList();

            // WHERE句のパラメータに設定
            DbParamCollection whereParam = new DbParamCollection();
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
            whereParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.KanjoKamokuCd);
            whereParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.HojoKamokuCd);
            whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, denpyoKubun);
            alParams.Add(whereParam);

            // SET句のパラメータに設定
            DbParamCollection updParam = new DbParamCollection();
            updParam.SetParam("@BIKO", SqlDbType.VarChar, 40, biko);
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, upd);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.Text,
                this.txtMonthDenpyoDateFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayDenpyoDateFr.Text) > lastDayInMonth)
            {
                this.txtDayDenpyoDateFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            //SetJpFr(Util.FixJpDate(this.lblGengoDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.Text,
            //    this.txtMonthDenpyoDateFr.Text, this.txtDayDenpyoDateFr.Text, this.Dba));
            SetJpFr(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text,
                                  this.txtGengoYearDenpyoDateFr.Text,
                                  this.txtMonthDenpyoDateFr.Text,
                                  this.txtDayDenpyoDateFr.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 配列に格納された伝票日付(自)和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoDenpyoDateFr.Text = arrJpDate[0];
            this.txtGengoYearDenpyoDateFr.Text = arrJpDate[2];
            this.txtMonthDenpyoDateFr.Text = arrJpDate[3];
            this.txtDayDenpyoDateFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.Text,
                this.txtMonthDenpyoDateTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayDenpyoDateTo.Text) > lastDayInMonth)
            {
                this.txtDayDenpyoDateTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            //SetJpTo(Util.FixJpDate(this.lblGengoDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.Text,
            //    this.txtMonthDenpyoDateTo.Text, this.txtDayDenpyoDateTo.Text, this.Dba));
            SetJpTo(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text,
                                  this.txtGengoYearDenpyoDateTo.Text,
                                  this.txtMonthDenpyoDateTo.Text,
                                  this.txtDayDenpyoDateTo.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 配列に格納された伝票日付(至)和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoDenpyoDateTo.Text = arrJpDate[0];
            this.txtGengoYearDenpyoDateTo.Text = arrJpDate[2];
            this.txtMonthDenpyoDateTo.Text = arrJpDate[3];
            this.txtDayDenpyoDateTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// グリッド全編集行を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateDataGridViewAll()
        {
            foreach (DataRow dr in ((DataTable)dgvList.DataSource).Rows)
            {
                // 変更フラグがオンの行をチェック
                if (Util.ToInt(Util.ToString(dr["HENKO"])) == 1)
                {
                    if (!IsValidBiko(dr["BIKO"]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 備考の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBiko(object val)
        {
            // 40文字を超えていたらエラー
            if (!ValChk.IsWithinLength(Util.ToString(val), 40))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目の貸借区分取得
        /// </summary>
        private int GetTaishakuKubun(string kanjoKamokuCd)
        {
            int ret = 0;

            if (Util.ToInt(kanjoKamokuCd) > 0)
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKamokuCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                StringBuilder cols = new StringBuilder();
                cols.Append("TAISHAKU_KUBUN");
                StringBuilder where = new StringBuilder();
                where.Append("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
                where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
                DataTable dt = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "TB_ZM_KANJO_KAMOKU"
                                , Util.ToString(where), dpc);
                if (dt.Rows.Count == 1)
                {
                    ret = Util.ToInt(dt.Rows[0]["TAISHAKU_KUBUN"]);
                }
            }
            return ret;
        }

        private string GetHojoKmkNm(string shishoCd, string kanjoKmkCd, string code)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKmkCd);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, code);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND (SHISHO_CD = @SHISHO_CD or SHISHO_CD is null)");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");

            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_ZM_HOJO_KAMOKU",
                where.ToString(),
                dpc);
            if (dt.Rows.Count != 0)
            {
                name = Util.ToString(dt.Rows[0]["HOJO_KAMOKU_NM"]);
            }
            return name;
        }

        /// <summary>
        /// 会計年度内日付に変換
        /// </summary>
        private DateTime FixNendoDate(DateTime date)
        {
            DateTime dateFr = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            DateTime dateTo = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }
        #endregion
    }
}
