﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblDayDenpyoDate = new System.Windows.Forms.Label();
			this.txtDayDenpyoDate = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMonthDenpyoDate = new System.Windows.Forms.Label();
			this.lblYearDenpyoDate = new System.Windows.Forms.Label();
			this.txtMonthDenpyoDate = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoDenpyoDate = new System.Windows.Forms.Label();
			this.txtGengoYearDenpyoDate = new jp.co.fsi.common.controls.FsiTextBox();
			this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
			this.panel2 = new jp.co.fsi.common.FsiPanel();
			this.label8 = new System.Windows.Forms.Label();
			this.panel3 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.lblCrShohizeiKingaku = new System.Windows.Forms.Label();
			this.lblCrDenpyoKingaku = new System.Windows.Forms.Label();
			this.lblDrShohizeiKingaku = new System.Windows.Forms.Label();
			this.lblDrDenpyoKingaku = new System.Windows.Forms.Label();
			this.lblInfo = new System.Windows.Forms.Label();
			this.lblGokei = new System.Windows.Forms.Label();
			this.lblTantoshaNm = new System.Windows.Forms.Label();
			this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDenpyoBango = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShohyoBango = new jp.co.fsi.common.controls.FsiTextBox();
			this.rdoKessanKubun1 = new System.Windows.Forms.RadioButton();
			this.rdoKessanKubun0 = new System.Windows.Forms.RadioButton();
			this.lblMode = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.btnShiftF4 = new System.Windows.Forms.Button();
			this.lblBiko = new System.Windows.Forms.Label();
			this.lblBakDenpyoDate = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel13 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
			this.lblMessage = new System.Windows.Forms.Label();
			this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel14 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel3 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel20 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel19 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel18 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel17 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel16 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel15 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel10.SuspendLayout();
			this.fsiPanel11.SuspendLayout();
			this.fsiPanel9.SuspendLayout();
			this.fsiPanel8.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel13.SuspendLayout();
			this.fsiPanel12.SuspendLayout();
			this.fsiPanel7.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiTableLayoutPanel2.SuspendLayout();
			this.fsiPanel14.SuspendLayout();
			this.fsiTableLayoutPanel3.SuspendLayout();
			this.fsiPanel20.SuspendLayout();
			this.fsiPanel19.SuspendLayout();
			this.fsiPanel18.SuspendLayout();
			this.fsiPanel17.SuspendLayout();
			this.fsiPanel16.SuspendLayout();
			this.fsiPanel15.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 624);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.Text = "振替伝票入力";
			// 
			// lblDayDenpyoDate
			// 
			this.lblDayDenpyoDate.AutoSize = true;
			this.lblDayDenpyoDate.BackColor = System.Drawing.Color.Silver;
			this.lblDayDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDayDenpyoDate.Location = new System.Drawing.Point(303, 10);
			this.lblDayDenpyoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDayDenpyoDate.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblDayDenpyoDate.Name = "lblDayDenpyoDate";
			this.lblDayDenpyoDate.Size = new System.Drawing.Size(24, 24);
			this.lblDayDenpyoDate.TabIndex = 16;
			this.lblDayDenpyoDate.Tag = "CHANGE";
			this.lblDayDenpyoDate.Text = "日";
			this.lblDayDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDayDenpyoDate
			// 
			this.txtDayDenpyoDate.AutoSizeFromLength = false;
			this.txtDayDenpyoDate.DisplayLength = null;
			this.txtDayDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDayDenpyoDate.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtDayDenpyoDate.Location = new System.Drawing.Point(269, 11);
			this.txtDayDenpyoDate.Margin = new System.Windows.Forms.Padding(4);
			this.txtDayDenpyoDate.MaxLength = 2;
			this.txtDayDenpyoDate.Name = "txtDayDenpyoDate";
			this.txtDayDenpyoDate.Size = new System.Drawing.Size(28, 23);
			this.txtDayDenpyoDate.TabIndex = 15;
			this.txtDayDenpyoDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayDenpyoDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDate_Validating);
			// 
			// lblMonthDenpyoDate
			// 
			this.lblMonthDenpyoDate.AutoSize = true;
			this.lblMonthDenpyoDate.BackColor = System.Drawing.Color.Silver;
			this.lblMonthDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMonthDenpyoDate.Location = new System.Drawing.Point(238, 10);
			this.lblMonthDenpyoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonthDenpyoDate.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblMonthDenpyoDate.Name = "lblMonthDenpyoDate";
			this.lblMonthDenpyoDate.Size = new System.Drawing.Size(24, 24);
			this.lblMonthDenpyoDate.TabIndex = 14;
			this.lblMonthDenpyoDate.Tag = "CHANGE";
			this.lblMonthDenpyoDate.Text = "月";
			this.lblMonthDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearDenpyoDate
			// 
			this.lblYearDenpyoDate.AutoSize = true;
			this.lblYearDenpyoDate.BackColor = System.Drawing.Color.Silver;
			this.lblYearDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblYearDenpyoDate.Location = new System.Drawing.Point(174, 10);
			this.lblYearDenpyoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYearDenpyoDate.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblYearDenpyoDate.Name = "lblYearDenpyoDate";
			this.lblYearDenpyoDate.Size = new System.Drawing.Size(24, 24);
			this.lblYearDenpyoDate.TabIndex = 12;
			this.lblYearDenpyoDate.Tag = "CHANGE";
			this.lblYearDenpyoDate.Text = "年";
			this.lblYearDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMonthDenpyoDate
			// 
			this.txtMonthDenpyoDate.AutoSizeFromLength = false;
			this.txtMonthDenpyoDate.DisplayLength = null;
			this.txtMonthDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMonthDenpyoDate.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtMonthDenpyoDate.Location = new System.Drawing.Point(205, 11);
			this.txtMonthDenpyoDate.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonthDenpyoDate.MaxLength = 2;
			this.txtMonthDenpyoDate.Name = "txtMonthDenpyoDate";
			this.txtMonthDenpyoDate.Size = new System.Drawing.Size(28, 23);
			this.txtMonthDenpyoDate.TabIndex = 13;
			this.txtMonthDenpyoDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthDenpyoDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDate_Validating);
			// 
			// lblGengoDenpyoDate
			// 
			this.lblGengoDenpyoDate.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblGengoDenpyoDate.ForeColor = System.Drawing.Color.Black;
			this.lblGengoDenpyoDate.Location = new System.Drawing.Point(89, 10);
			this.lblGengoDenpyoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengoDenpyoDate.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblGengoDenpyoDate.Name = "lblGengoDenpyoDate";
			this.lblGengoDenpyoDate.Size = new System.Drawing.Size(53, 24);
			this.lblGengoDenpyoDate.TabIndex = 10;
			this.lblGengoDenpyoDate.Text = "平成";
			this.lblGengoDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtGengoYearDenpyoDate
			// 
			this.txtGengoYearDenpyoDate.AutoSizeFromLength = false;
			this.txtGengoYearDenpyoDate.DisplayLength = null;
			this.txtGengoYearDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtGengoYearDenpyoDate.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtGengoYearDenpyoDate.Location = new System.Drawing.Point(145, 11);
			this.txtGengoYearDenpyoDate.Margin = new System.Windows.Forms.Padding(4);
			this.txtGengoYearDenpyoDate.MaxLength = 2;
			this.txtGengoYearDenpyoDate.Name = "txtGengoYearDenpyoDate";
			this.txtGengoYearDenpyoDate.Size = new System.Drawing.Size(28, 23);
			this.txtGengoYearDenpyoDate.TabIndex = 11;
			this.txtGengoYearDenpyoDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYearDenpyoDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDate_Validating);
			// 
			// mtbList
			// 
			this.mtbList.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.mtbList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mtbList.FixedCols = 0;
			this.mtbList.FocusField = null;
			this.mtbList.ForeColor = System.Drawing.Color.Navy;
			this.mtbList.Location = new System.Drawing.Point(0, 0);
			this.mtbList.Margin = new System.Windows.Forms.Padding(4);
			this.mtbList.Name = "mtbList";
			this.mtbList.NotSelectableCols = 0;
			this.mtbList.SelectRange = null;
			this.mtbList.Size = new System.Drawing.Size(1092, 366);
			this.mtbList.TabIndex = 23;
			this.mtbList.Text = "sjMultiTable1";
			this.mtbList.UndoBufferEnabled = false;
			this.mtbList.RecordEnter += new systembase.table.UTable.RecordEnterEventHandler(this.mtbList_RecordEnter);
			this.mtbList.FieldValidating += new systembase.table.UTable.FieldValidatingEventHandler(this.mtbList_FieldValidating);
			this.mtbList.FieldValueChanged += new systembase.table.UTable.FieldValueChangedEventHandler(this.mtbList_FieldValueChanged);
			this.mtbList.FieldEnter += new systembase.table.UTable.FieldEnterEventHandler(this.mtbList_FieldEnter);
			this.mtbList.EditStart += new systembase.table.UTable.EditStartEventHandler(this.mtbList_EditStart);
			this.mtbList.InitializeEditor += new systembase.table.UTable.InitializeEditorEventHandler(this.mtbList_InitializeEditor);
			this.mtbList.Leave += new System.EventHandler(this.mtbList_Leave);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.Silver;
			this.panel2.Controls.Add(this.label8);
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Margin = new System.Windows.Forms.Padding(4);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(397, 27);
			this.panel2.TabIndex = 908;
			// 
			// label8
			// 
			this.label8.BackColor = System.Drawing.Color.Silver;
			this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label8.Location = new System.Drawing.Point(0, 0);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(397, 27);
			this.label8.TabIndex = 9;
			this.label8.Tag = "CHANGE";
			this.label8.Text = "借　　　方";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.Silver;
			this.panel3.Controls.Add(this.label1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel3.Location = new System.Drawing.Point(724, 0);
			this.panel3.Margin = new System.Windows.Forms.Padding(4);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(368, 27);
			this.panel3.TabIndex = 909;
			this.panel3.Tag = "CHANGE";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(368, 27);
			this.label1.TabIndex = 10;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "貸　　　方";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblCrShohizeiKingaku
			// 
			this.lblCrShohizeiKingaku.BackColor = System.Drawing.Color.Transparent;
			this.lblCrShohizeiKingaku.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCrShohizeiKingaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCrShohizeiKingaku.Location = new System.Drawing.Point(0, 0);
			this.lblCrShohizeiKingaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCrShohizeiKingaku.Name = "lblCrShohizeiKingaku";
			this.lblCrShohizeiKingaku.Size = new System.Drawing.Size(159, 20);
			this.lblCrShohizeiKingaku.TabIndex = 915;
			this.lblCrShohizeiKingaku.Text = "1,234";
			this.lblCrShohizeiKingaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCrDenpyoKingaku
			// 
			this.lblCrDenpyoKingaku.BackColor = System.Drawing.Color.Transparent;
			this.lblCrDenpyoKingaku.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCrDenpyoKingaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCrDenpyoKingaku.Location = new System.Drawing.Point(0, 0);
			this.lblCrDenpyoKingaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCrDenpyoKingaku.Name = "lblCrDenpyoKingaku";
			this.lblCrDenpyoKingaku.Size = new System.Drawing.Size(159, 16);
			this.lblCrDenpyoKingaku.TabIndex = 914;
			this.lblCrDenpyoKingaku.Text = "1,234,567,890";
			this.lblCrDenpyoKingaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblDrShohizeiKingaku
			// 
			this.lblDrShohizeiKingaku.BackColor = System.Drawing.Color.Transparent;
			this.lblDrShohizeiKingaku.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDrShohizeiKingaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDrShohizeiKingaku.Location = new System.Drawing.Point(0, 0);
			this.lblDrShohizeiKingaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDrShohizeiKingaku.Name = "lblDrShohizeiKingaku";
			this.lblDrShohizeiKingaku.Size = new System.Drawing.Size(158, 20);
			this.lblDrShohizeiKingaku.TabIndex = 913;
			this.lblDrShohizeiKingaku.Text = "1,234";
			this.lblDrShohizeiKingaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblDrDenpyoKingaku
			// 
			this.lblDrDenpyoKingaku.BackColor = System.Drawing.Color.Transparent;
			this.lblDrDenpyoKingaku.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDrDenpyoKingaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDrDenpyoKingaku.Location = new System.Drawing.Point(0, 0);
			this.lblDrDenpyoKingaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDrDenpyoKingaku.Name = "lblDrDenpyoKingaku";
			this.lblDrDenpyoKingaku.Size = new System.Drawing.Size(158, 16);
			this.lblDrDenpyoKingaku.TabIndex = 912;
			this.lblDrDenpyoKingaku.Text = "1,234,567.890";
			this.lblDrDenpyoKingaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblInfo
			// 
			this.lblInfo.BackColor = System.Drawing.Color.Transparent;
			this.lblInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblInfo.ForeColor = System.Drawing.Color.Red;
			this.lblInfo.Location = new System.Drawing.Point(63, 53);
			this.lblInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblInfo.Name = "lblInfo";
			this.lblInfo.Size = new System.Drawing.Size(209, 20);
			this.lblInfo.TabIndex = 911;
			this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblGokei
			// 
			this.lblGokei.BackColor = System.Drawing.Color.Silver;
			this.lblGokei.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGokei.Location = new System.Drawing.Point(0, 0);
			this.lblGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGokei.Name = "lblGokei";
			this.lblGokei.Size = new System.Drawing.Size(323, 16);
			this.lblGokei.TabIndex = 910;
			this.lblGokei.Tag = "CHANGE";
			this.lblGokei.Text = "合　　計";
			this.lblGokei.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblTantoshaNm
			// 
			this.lblTantoshaNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTantoshaNm.ForeColor = System.Drawing.Color.Black;
			this.lblTantoshaNm.Location = new System.Drawing.Point(164, 7);
			this.lblTantoshaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaNm.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblTantoshaNm.Name = "lblTantoshaNm";
			this.lblTantoshaNm.Size = new System.Drawing.Size(112, 24);
			this.lblTantoshaNm.TabIndex = 4;
			this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTantoshaCd
			// 
			this.txtTantoshaCd.AutoSizeFromLength = false;
			this.txtTantoshaCd.DisplayLength = null;
			this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtTantoshaCd.Location = new System.Drawing.Point(85, 8);
			this.txtTantoshaCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtTantoshaCd.MaxLength = 4;
			this.txtTantoshaCd.Name = "txtTantoshaCd";
			this.txtTantoshaCd.Size = new System.Drawing.Size(71, 23);
			this.txtTantoshaCd.TabIndex = 3;
			this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCd_Validating);
			// 
			// txtDenpyoBango
			// 
			this.txtDenpyoBango.AutoSizeFromLength = false;
			this.txtDenpyoBango.DisplayLength = null;
			this.txtDenpyoBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDenpyoBango.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtDenpyoBango.Location = new System.Drawing.Point(85, 11);
			this.txtDenpyoBango.Margin = new System.Windows.Forms.Padding(4);
			this.txtDenpyoBango.MaxLength = 6;
			this.txtDenpyoBango.Name = "txtDenpyoBango";
			this.txtDenpyoBango.Size = new System.Drawing.Size(71, 23);
			this.txtDenpyoBango.TabIndex = 7;
			this.txtDenpyoBango.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDenpyoBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenpyoBango_Validating);
			// 
			// txtShohyoBango
			// 
			this.txtShohyoBango.AutoSizeFromLength = false;
			this.txtShohyoBango.DisplayLength = null;
			this.txtShohyoBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShohyoBango.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtShohyoBango.Location = new System.Drawing.Point(74, 11);
			this.txtShohyoBango.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohyoBango.MaxLength = 10;
			this.txtShohyoBango.Name = "txtShohyoBango";
			this.txtShohyoBango.Size = new System.Drawing.Size(120, 23);
			this.txtShohyoBango.TabIndex = 19;
			// 
			// rdoKessanKubun1
			// 
			this.rdoKessanKubun1.AutoSize = true;
			this.rdoKessanKubun1.BackColor = System.Drawing.Color.Silver;
			this.rdoKessanKubun1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.rdoKessanKubun1.Location = new System.Drawing.Point(108, 11);
			this.rdoKessanKubun1.Margin = new System.Windows.Forms.Padding(4);
			this.rdoKessanKubun1.MinimumSize = new System.Drawing.Size(0, 24);
			this.rdoKessanKubun1.Name = "rdoKessanKubun1";
			this.rdoKessanKubun1.Size = new System.Drawing.Size(90, 24);
			this.rdoKessanKubun1.TabIndex = 22;
			this.rdoKessanKubun1.Tag = "CHANGE";
			this.rdoKessanKubun1.Text = "決算仕訳";
			this.rdoKessanKubun1.UseVisualStyleBackColor = false;
			this.rdoKessanKubun1.CheckedChanged += new System.EventHandler(this.rdo_CheckedChanged);
			// 
			// rdoKessanKubun0
			// 
			this.rdoKessanKubun0.AutoSize = true;
			this.rdoKessanKubun0.BackColor = System.Drawing.Color.Silver;
			this.rdoKessanKubun0.Checked = true;
			this.rdoKessanKubun0.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.rdoKessanKubun0.Location = new System.Drawing.Point(8, 11);
			this.rdoKessanKubun0.Margin = new System.Windows.Forms.Padding(4);
			this.rdoKessanKubun0.MinimumSize = new System.Drawing.Size(0, 24);
			this.rdoKessanKubun0.Name = "rdoKessanKubun0";
			this.rdoKessanKubun0.Size = new System.Drawing.Size(90, 24);
			this.rdoKessanKubun0.TabIndex = 21;
			this.rdoKessanKubun0.TabStop = true;
			this.rdoKessanKubun0.Tag = "CHANGE";
			this.rdoKessanKubun0.Text = "通常仕訳";
			this.rdoKessanKubun0.UseVisualStyleBackColor = false;
			this.rdoKessanKubun0.CheckedChanged += new System.EventHandler(this.rdo_CheckedChanged);
			// 
			// lblMode
			// 
			this.lblMode.AutoSize = true;
			this.lblMode.BackColor = System.Drawing.Color.White;
			this.lblMode.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMode.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMode.Location = new System.Drawing.Point(0, 0);
			this.lblMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMode.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblMode.Name = "lblMode";
			this.lblMode.Size = new System.Drawing.Size(72, 24);
			this.lblMode.TabIndex = 915;
			this.lblMode.Text = "《修正》";
			this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(60, 8);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 5;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(52, 23);
			this.txtMizuageShishoCd.TabIndex = 917;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMizuageShishoNm.ForeColor = System.Drawing.Color.Black;
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(114, 7);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(304, 24);
			this.lblMizuageShishoNm.TabIndex = 919;
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(425, 46);
			this.lblMizuageShisho.TabIndex = 918;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = " 支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnShiftF4
			// 
			this.btnShiftF4.BackColor = System.Drawing.Color.SkyBlue;
			this.btnShiftF4.Enabled = false;
			this.btnShiftF4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnShiftF4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnShiftF4.ForeColor = System.Drawing.Color.Navy;
			this.btnShiftF4.Location = new System.Drawing.Point(1001, 555);
			this.btnShiftF4.Margin = new System.Windows.Forms.Padding(4);
			this.btnShiftF4.Name = "btnShiftF4";
			this.btnShiftF4.Size = new System.Drawing.Size(87, 60);
			this.btnShiftF4.TabIndex = 920;
			this.btnShiftF4.TabStop = false;
			this.btnShiftF4.Text = "Shift+F4\r\n伝票複写";
			this.btnShiftF4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnShiftF4.UseVisualStyleBackColor = false;
			this.btnShiftF4.Click += new System.EventHandler(this.btnShiftF4_Click);
			// 
			// lblBiko
			// 
			this.lblBiko.AutoSize = true;
			this.lblBiko.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblBiko.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBiko.Location = new System.Drawing.Point(0, 0);
			this.lblBiko.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBiko.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblBiko.Name = "lblBiko";
			this.lblBiko.Size = new System.Drawing.Size(328, 24);
			this.lblBiko.TabIndex = 921;
			this.lblBiko.Text = "０１２３４５６７８９０１２３４５６７８９";
			this.lblBiko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblBakDenpyoDate
			// 
			this.lblBakDenpyoDate.BackColor = System.Drawing.Color.Silver;
			this.lblBakDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBakDenpyoDate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblBakDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblBakDenpyoDate.Location = new System.Drawing.Point(0, 0);
			this.lblBakDenpyoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBakDenpyoDate.Name = "lblBakDenpyoDate";
			this.lblBakDenpyoDate.Size = new System.Drawing.Size(293, 46);
			this.lblBakDenpyoDate.TabIndex = 1;
			this.lblBakDenpyoDate.Tag = "CHANGE";
			this.lblBakDenpyoDate.Text = "担当者";
			this.lblBakDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(174, 44);
			this.label3.TabIndex = 5;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "伝票番号";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(363, 44);
			this.label4.TabIndex = 8;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "伝票日付";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Silver;
			this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label5.Location = new System.Drawing.Point(0, 0);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(201, 44);
			this.label5.TabIndex = 17;
			this.label5.Tag = "CHANGE";
			this.label5.Text = "信憑番号";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.Silver;
			this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label6.Location = new System.Drawing.Point(0, 0);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(354, 44);
			this.label6.TabIndex = 20;
			this.label6.Tag = "CHANGE";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 44);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.08549F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.759443F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 74.19355F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1100, 504);
			this.fsiTableLayoutPanel1.TabIndex = 927;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.mtbList);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 134);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(1092, 366);
			this.fsiPanel5.TabIndex = 2;
			this.fsiPanel5.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.panel2);
			this.fsiPanel2.Controls.Add(this.panel3);
			this.fsiPanel2.Location = new System.Drawing.Point(4, 100);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(1092, 27);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.fsiPanel4);
			this.fsiPanel1.Controls.Add(this.fsiPanel3);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(1092, 89);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.fsiPanel10);
			this.fsiPanel4.Controls.Add(this.fsiPanel11);
			this.fsiPanel4.Controls.Add(this.fsiPanel9);
			this.fsiPanel4.Controls.Add(this.fsiPanel8);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.fsiPanel4.Location = new System.Drawing.Point(0, 45);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(1092, 44);
			this.fsiPanel4.TabIndex = 1;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel10
			// 
			this.fsiPanel10.Controls.Add(this.rdoKessanKubun0);
			this.fsiPanel10.Controls.Add(this.rdoKessanKubun1);
			this.fsiPanel10.Controls.Add(this.label6);
			this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel10.Location = new System.Drawing.Point(738, 0);
			this.fsiPanel10.Name = "fsiPanel10";
			this.fsiPanel10.Size = new System.Drawing.Size(354, 44);
			this.fsiPanel10.TabIndex = 5;
			this.fsiPanel10.Tag = "CHANGE";
			// 
			// fsiPanel11
			// 
			this.fsiPanel11.Controls.Add(this.txtShohyoBango);
			this.fsiPanel11.Controls.Add(this.label5);
			this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel11.Location = new System.Drawing.Point(537, 0);
			this.fsiPanel11.Name = "fsiPanel11";
			this.fsiPanel11.Size = new System.Drawing.Size(201, 44);
			this.fsiPanel11.TabIndex = 4;
			this.fsiPanel11.Tag = "CHANGE";
			// 
			// fsiPanel9
			// 
			this.fsiPanel9.Controls.Add(this.lblGengoDenpyoDate);
			this.fsiPanel9.Controls.Add(this.txtGengoYearDenpyoDate);
			this.fsiPanel9.Controls.Add(this.txtMonthDenpyoDate);
			this.fsiPanel9.Controls.Add(this.lblYearDenpyoDate);
			this.fsiPanel9.Controls.Add(this.lblMonthDenpyoDate);
			this.fsiPanel9.Controls.Add(this.txtDayDenpyoDate);
			this.fsiPanel9.Controls.Add(this.lblDayDenpyoDate);
			this.fsiPanel9.Controls.Add(this.label4);
			this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel9.Location = new System.Drawing.Point(174, 0);
			this.fsiPanel9.Name = "fsiPanel9";
			this.fsiPanel9.Size = new System.Drawing.Size(363, 44);
			this.fsiPanel9.TabIndex = 2;
			this.fsiPanel9.Tag = "CHANGE";
			// 
			// fsiPanel8
			// 
			this.fsiPanel8.Controls.Add(this.txtDenpyoBango);
			this.fsiPanel8.Controls.Add(this.label3);
			this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel8.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel8.Name = "fsiPanel8";
			this.fsiPanel8.Size = new System.Drawing.Size(174, 44);
			this.fsiPanel8.TabIndex = 1;
			this.fsiPanel8.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.fsiPanel13);
			this.fsiPanel3.Controls.Add(this.fsiPanel12);
			this.fsiPanel3.Controls.Add(this.fsiPanel7);
			this.fsiPanel3.Controls.Add(this.fsiPanel6);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.fsiPanel3.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(1092, 46);
			this.fsiPanel3.TabIndex = 1;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel13
			// 
			this.fsiPanel13.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel13.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel13.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel13.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel13.Location = new System.Drawing.Point(602, 0);
			this.fsiPanel13.Name = "fsiPanel13";
			this.fsiPanel13.Size = new System.Drawing.Size(425, 46);
			this.fsiPanel13.TabIndex = 4;
			this.fsiPanel13.Tag = "CHANGE";
			// 
			// fsiPanel12
			// 
			this.fsiPanel12.Controls.Add(this.lblMessage);
			this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel12.Location = new System.Drawing.Point(293, 0);
			this.fsiPanel12.Name = "fsiPanel12";
			this.fsiPanel12.Size = new System.Drawing.Size(306, 46);
			this.fsiPanel12.TabIndex = 3;
			this.fsiPanel12.Tag = "CHANGE";
			// 
			// lblMessage
			// 
			this.lblMessage.BackColor = System.Drawing.Color.LightCyan;
			this.lblMessage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMessage.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMessage.ForeColor = System.Drawing.Color.Black;
			this.lblMessage.Location = new System.Drawing.Point(0, 0);
			this.lblMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(306, 46);
			this.lblMessage.TabIndex = 910;
			this.lblMessage.Tag = "";
			this.lblMessage.Text = "前回登録伝票番号：000000";
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel7
			// 
			this.fsiPanel7.Controls.Add(this.lblMode);
			this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel7.Location = new System.Drawing.Point(1027, 0);
			this.fsiPanel7.Name = "fsiPanel7";
			this.fsiPanel7.Size = new System.Drawing.Size(65, 46);
			this.fsiPanel7.TabIndex = 2;
			this.fsiPanel7.Tag = "CHANGE";
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.txtTantoshaCd);
			this.fsiPanel6.Controls.Add(this.lblTantoshaNm);
			this.fsiPanel6.Controls.Add(this.lblBakDenpyoDate);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel6.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(293, 46);
			this.fsiPanel6.TabIndex = 1;
			this.fsiPanel6.Tag = "CHANGE";
			// 
			// fsiTableLayoutPanel2
			// 
			this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel2.ColumnCount = 1;
			this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel14, 0, 0);
			this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(430, 554);
			this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
			this.fsiTableLayoutPanel2.RowCount = 1;
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(400, 38);
			this.fsiTableLayoutPanel2.TabIndex = 928;
			// 
			// fsiPanel14
			// 
			this.fsiPanel14.Controls.Add(this.lblBiko);
			this.fsiPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel14.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel14.Name = "fsiPanel14";
			this.fsiPanel14.Size = new System.Drawing.Size(392, 30);
			this.fsiPanel14.TabIndex = 0;
			this.fsiPanel14.Tag = "CHANGE";
			// 
			// fsiTableLayoutPanel3
			// 
			this.fsiTableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel3.ColumnCount = 3;
			this.fsiTableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel20, 2, 1);
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel19, 1, 1);
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel18, 0, 1);
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel17, 2, 0);
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel16, 1, 0);
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel15, 0, 0);
			this.fsiTableLayoutPanel3.Location = new System.Drawing.Point(240, 553);
			this.fsiTableLayoutPanel3.Name = "fsiTableLayoutPanel3";
			this.fsiTableLayoutPanel3.RowCount = 2;
			this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.875F));
			this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.125F));
			this.fsiTableLayoutPanel3.Size = new System.Drawing.Size(662, 51);
			this.fsiTableLayoutPanel3.TabIndex = 929;
			this.fsiTableLayoutPanel3.Tag = "CHANGE";
			// 
			// fsiPanel20
			// 
			this.fsiPanel20.Controls.Add(this.lblCrShohizeiKingaku);
			this.fsiPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel20.Location = new System.Drawing.Point(499, 27);
			this.fsiPanel20.Name = "fsiPanel20";
			this.fsiPanel20.Size = new System.Drawing.Size(159, 20);
			this.fsiPanel20.TabIndex = 29;
			this.fsiPanel20.Tag = "CHANGE";
			// 
			// fsiPanel19
			// 
			this.fsiPanel19.Controls.Add(this.lblInfo);
			this.fsiPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel19.Location = new System.Drawing.Point(169, 27);
			this.fsiPanel19.Name = "fsiPanel19";
			this.fsiPanel19.Size = new System.Drawing.Size(323, 20);
			this.fsiPanel19.TabIndex = 28;
			this.fsiPanel19.Tag = "CHANGE";
			// 
			// fsiPanel18
			// 
			this.fsiPanel18.Controls.Add(this.lblDrShohizeiKingaku);
			this.fsiPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel18.Location = new System.Drawing.Point(4, 27);
			this.fsiPanel18.Name = "fsiPanel18";
			this.fsiPanel18.Size = new System.Drawing.Size(158, 20);
			this.fsiPanel18.TabIndex = 27;
			this.fsiPanel18.Tag = "CHANGE";
			// 
			// fsiPanel17
			// 
			this.fsiPanel17.Controls.Add(this.lblCrDenpyoKingaku);
			this.fsiPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel17.Location = new System.Drawing.Point(499, 4);
			this.fsiPanel17.Name = "fsiPanel17";
			this.fsiPanel17.Size = new System.Drawing.Size(159, 16);
			this.fsiPanel17.TabIndex = 26;
			this.fsiPanel17.Tag = "CHANGE";
			// 
			// fsiPanel16
			// 
			this.fsiPanel16.Controls.Add(this.lblGokei);
			this.fsiPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel16.Location = new System.Drawing.Point(169, 4);
			this.fsiPanel16.Name = "fsiPanel16";
			this.fsiPanel16.Size = new System.Drawing.Size(323, 16);
			this.fsiPanel16.TabIndex = 25;
			this.fsiPanel16.Tag = "CHANGE";
			// 
			// fsiPanel15
			// 
			this.fsiPanel15.Controls.Add(this.lblDrDenpyoKingaku);
			this.fsiPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel15.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel15.Name = "fsiPanel15";
			this.fsiPanel15.Size = new System.Drawing.Size(158, 16);
			this.fsiPanel15.TabIndex = 24;
			this.fsiPanel15.Tag = "CHANGE";
			// 
			// ZMDE1031
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 760);
			this.Controls.Add(this.fsiTableLayoutPanel3);
			this.Controls.Add(this.fsiTableLayoutPanel2);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.btnShiftF4);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.MinimumSize = new System.Drawing.Size(20, 43);
			this.Name = "ZMDE1031";
			this.Text = "振替伝票入力";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ZMDE1031_FormClosing);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_KeyDown);
			this.Controls.SetChildIndex(this.btnShiftF4, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel3, 0);
			this.pnlDebug.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel10.ResumeLayout(false);
			this.fsiPanel10.PerformLayout();
			this.fsiPanel11.ResumeLayout(false);
			this.fsiPanel11.PerformLayout();
			this.fsiPanel9.ResumeLayout(false);
			this.fsiPanel9.PerformLayout();
			this.fsiPanel8.ResumeLayout(false);
			this.fsiPanel8.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel13.ResumeLayout(false);
			this.fsiPanel13.PerformLayout();
			this.fsiPanel12.ResumeLayout(false);
			this.fsiPanel7.ResumeLayout(false);
			this.fsiPanel7.PerformLayout();
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiTableLayoutPanel2.ResumeLayout(false);
			this.fsiPanel14.ResumeLayout(false);
			this.fsiPanel14.PerformLayout();
			this.fsiTableLayoutPanel3.ResumeLayout(false);
			this.fsiPanel20.ResumeLayout(false);
			this.fsiPanel19.ResumeLayout(false);
			this.fsiPanel18.ResumeLayout(false);
			this.fsiPanel17.ResumeLayout(false);
			this.fsiPanel16.ResumeLayout(false);
			this.fsiPanel15.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblDayDenpyoDate;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDate;
        private System.Windows.Forms.Label lblMonthDenpyoDate;
        private System.Windows.Forms.Label lblYearDenpyoDate;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDate;
        private System.Windows.Forms.Label lblGengoDenpyoDate;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDate;
        private jp.co.fsi.common.controls.SjMultiTable mtbList;
        private jp.co.fsi.common.FsiPanel panel2;
        private jp.co.fsi.common.FsiPanel panel3;
        private System.Windows.Forms.Label lblCrShohizeiKingaku;
        private System.Windows.Forms.Label lblCrDenpyoKingaku;
        private System.Windows.Forms.Label lblDrShohizeiKingaku;
        private System.Windows.Forms.Label lblDrDenpyoKingaku;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label lblGokei;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtDenpyoBango;
        private jp.co.fsi.common.controls.FsiTextBox txtShohyoBango;
        private System.Windows.Forms.RadioButton rdoKessanKubun1;
        private System.Windows.Forms.RadioButton rdoKessanKubun0;
        private System.Windows.Forms.Label lblMode;
        private jp.co.fsi.common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        protected System.Windows.Forms.Button btnShiftF4;
        private System.Windows.Forms.Label lblBiko;
        private System.Windows.Forms.Label lblBakDenpyoDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel13;
        private common.FsiPanel fsiPanel12;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel10;
        private System.Windows.Forms.Label lblMessage;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiPanel fsiPanel14;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel3;
        private common.FsiPanel fsiPanel20;
        private common.FsiPanel fsiPanel19;
        private common.FsiPanel fsiPanel18;
        private common.FsiPanel fsiPanel17;
        private common.FsiPanel fsiPanel16;
        private common.FsiPanel fsiPanel15;
    }
}