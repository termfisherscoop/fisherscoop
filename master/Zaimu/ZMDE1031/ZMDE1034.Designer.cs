﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1034
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblDrTotal = new System.Windows.Forms.Label();
            this.lblCrTotal = new System.Windows.Forms.Label();
            this.lblDenpyoDateTo = new System.Windows.Forms.Label();
            this.lblDayDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtDayDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthDenpyoDateTo = new System.Windows.Forms.Label();
            this.lblYearDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtMonthDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtGengoYearDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDayDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtDayDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoDateFr = new System.Windows.Forms.Label();
            this.lblMonthDenpyoDateFr = new System.Windows.Forms.Label();
            this.lblYearDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtMonthDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtGengoYearDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKamokuNm = new System.Windows.Forms.Label();
            this.lblZandaka = new System.Windows.Forms.Label();
            this.txtBiko = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBakKamoku = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 609);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "取引先元帳";
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvList.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvList.EnableHeadersVisualStyles = false;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(16, 156);
            this.dgvList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(1065, 505);
            this.dgvList.TabIndex = 0;
            this.dgvList.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvList_CellValidating);
            this.dgvList.Enter += new System.EventHandler(this.dgvList_Enter);
            this.dgvList.Leave += new System.EventHandler(this.dgvList_Leave);
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTotal.Location = new System.Drawing.Point(16, 665);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(440, 27);
            this.lblTotal.TabIndex = 903;
            this.lblTotal.Text = "合計";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDrTotal
            // 
            this.lblDrTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblDrTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDrTotal.Location = new System.Drawing.Point(456, 665);
            this.lblDrTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDrTotal.Name = "lblDrTotal";
            this.lblDrTotal.Size = new System.Drawing.Size(133, 27);
            this.lblDrTotal.TabIndex = 904;
            this.lblDrTotal.Text = "1,234,567,890,123";
            this.lblDrTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCrTotal
            // 
            this.lblCrTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblCrTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCrTotal.Location = new System.Drawing.Point(589, 665);
            this.lblCrTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCrTotal.Name = "lblCrTotal";
            this.lblCrTotal.Size = new System.Drawing.Size(133, 27);
            this.lblCrTotal.TabIndex = 905;
            this.lblCrTotal.Text = "1,234,567,890,123";
            this.lblCrTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDenpyoDateTo
            // 
            this.lblDenpyoDateTo.AutoSize = true;
            this.lblDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDenpyoDateTo.Location = new System.Drawing.Point(369, 10);
            this.lblDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDenpyoDateTo.Name = "lblDenpyoDateTo";
            this.lblDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
            this.lblDenpyoDateTo.TabIndex = 15;
            this.lblDenpyoDateTo.Tag = "CHANGE";
            this.lblDenpyoDateTo.Text = "～";
            this.lblDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayDenpyoDateTo
            // 
            this.lblDayDenpyoDateTo.AutoSize = true;
            this.lblDayDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDayDenpyoDateTo.Location = new System.Drawing.Point(662, 10);
            this.lblDayDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDayDenpyoDateTo.Name = "lblDayDenpyoDateTo";
            this.lblDayDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
            this.lblDayDenpyoDateTo.TabIndex = 14;
            this.lblDayDenpyoDateTo.Tag = "CHANGE";
            this.lblDayDenpyoDateTo.Text = "日";
            this.lblDayDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayDenpyoDateTo
            // 
            this.txtDayDenpyoDateTo.AutoSizeFromLength = false;
            this.txtDayDenpyoDateTo.DisplayLength = null;
            this.txtDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDayDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayDenpyoDateTo.Location = new System.Drawing.Point(620, 11);
            this.txtDayDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayDenpyoDateTo.MaxLength = 2;
            this.txtDayDenpyoDateTo.Name = "txtDayDenpyoDateTo";
            this.txtDayDenpyoDateTo.Size = new System.Drawing.Size(32, 23);
            this.txtDayDenpyoDateTo.TabIndex = 13;
            this.txtDayDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateTo_Validating);
            // 
            // lblMonthDenpyoDateTo
            // 
            this.lblMonthDenpyoDateTo.AutoSize = true;
            this.lblMonthDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMonthDenpyoDateTo.Location = new System.Drawing.Point(589, 10);
            this.lblMonthDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblMonthDenpyoDateTo.Name = "lblMonthDenpyoDateTo";
            this.lblMonthDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
            this.lblMonthDenpyoDateTo.TabIndex = 12;
            this.lblMonthDenpyoDateTo.Tag = "CHANGE";
            this.lblMonthDenpyoDateTo.Text = "月";
            this.lblMonthDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearDenpyoDateTo
            // 
            this.lblYearDenpyoDateTo.AutoSize = true;
            this.lblYearDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblYearDenpyoDateTo.Location = new System.Drawing.Point(517, 10);
            this.lblYearDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblYearDenpyoDateTo.Name = "lblYearDenpyoDateTo";
            this.lblYearDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
            this.lblYearDenpyoDateTo.TabIndex = 10;
            this.lblYearDenpyoDateTo.Tag = "CHANGE";
            this.lblYearDenpyoDateTo.Text = "年";
            this.lblYearDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMonthDenpyoDateTo
            // 
            this.txtMonthDenpyoDateTo.AutoSizeFromLength = false;
            this.txtMonthDenpyoDateTo.DisplayLength = null;
            this.txtMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMonthDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthDenpyoDateTo.Location = new System.Drawing.Point(549, 11);
            this.txtMonthDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthDenpyoDateTo.MaxLength = 2;
            this.txtMonthDenpyoDateTo.Name = "txtMonthDenpyoDateTo";
            this.txtMonthDenpyoDateTo.Size = new System.Drawing.Size(32, 23);
            this.txtMonthDenpyoDateTo.TabIndex = 11;
            this.txtMonthDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateTo_Validating);
            // 
            // lblGengoDenpyoDateTo
            // 
            this.lblGengoDenpyoDateTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoDenpyoDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblGengoDenpyoDateTo.ForeColor = System.Drawing.Color.Black;
            this.lblGengoDenpyoDateTo.Location = new System.Drawing.Point(409, 10);
            this.lblGengoDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoDenpyoDateTo.Name = "lblGengoDenpyoDateTo";
            this.lblGengoDenpyoDateTo.Size = new System.Drawing.Size(65, 24);
            this.lblGengoDenpyoDateTo.TabIndex = 8;
            this.lblGengoDenpyoDateTo.Text = "平成";
            this.lblGengoDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearDenpyoDateTo
            // 
            this.txtGengoYearDenpyoDateTo.AutoSizeFromLength = false;
            this.txtGengoYearDenpyoDateTo.DisplayLength = null;
            this.txtGengoYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtGengoYearDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearDenpyoDateTo.Location = new System.Drawing.Point(478, 11);
            this.txtGengoYearDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtGengoYearDenpyoDateTo.MaxLength = 2;
            this.txtGengoYearDenpyoDateTo.Name = "txtGengoYearDenpyoDateTo";
            this.txtGengoYearDenpyoDateTo.Size = new System.Drawing.Size(32, 23);
            this.txtGengoYearDenpyoDateTo.TabIndex = 9;
            this.txtGengoYearDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateTo_Validating);
            // 
            // lblDayDenpyoDateFr
            // 
            this.lblDayDenpyoDateFr.AutoSize = true;
            this.lblDayDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDayDenpyoDateFr.Location = new System.Drawing.Point(333, 10);
            this.lblDayDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDayDenpyoDateFr.Name = "lblDayDenpyoDateFr";
            this.lblDayDenpyoDateFr.Size = new System.Drawing.Size(24, 24);
            this.lblDayDenpyoDateFr.TabIndex = 7;
            this.lblDayDenpyoDateFr.Tag = "CHANGE";
            this.lblDayDenpyoDateFr.Text = "日";
            this.lblDayDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayDenpyoDateFr
            // 
            this.txtDayDenpyoDateFr.AutoSizeFromLength = false;
            this.txtDayDenpyoDateFr.DisplayLength = null;
            this.txtDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDayDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayDenpyoDateFr.Location = new System.Drawing.Point(290, 11);
            this.txtDayDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayDenpyoDateFr.MaxLength = 2;
            this.txtDayDenpyoDateFr.Name = "txtDayDenpyoDateFr";
            this.txtDayDenpyoDateFr.Size = new System.Drawing.Size(32, 23);
            this.txtDayDenpyoDateFr.TabIndex = 5;
            this.txtDayDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateFr_Validating);
            // 
            // lblDenpyoDateFr
            // 
            this.lblDenpyoDateFr.AutoSize = true;
            this.lblDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDateFr.Location = new System.Drawing.Point(31, 113);
            this.lblDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDenpyoDateFr.Name = "lblDenpyoDateFr";
            this.lblDenpyoDateFr.Size = new System.Drawing.Size(0, 16);
            this.lblDenpyoDateFr.TabIndex = 5;
            // 
            // lblMonthDenpyoDateFr
            // 
            this.lblMonthDenpyoDateFr.AutoSize = true;
            this.lblMonthDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMonthDenpyoDateFr.Location = new System.Drawing.Point(260, 10);
            this.lblMonthDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblMonthDenpyoDateFr.Name = "lblMonthDenpyoDateFr";
            this.lblMonthDenpyoDateFr.Size = new System.Drawing.Size(24, 24);
            this.lblMonthDenpyoDateFr.TabIndex = 4;
            this.lblMonthDenpyoDateFr.Tag = "CHANGE";
            this.lblMonthDenpyoDateFr.Text = "月";
            this.lblMonthDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearDenpyoDateFr
            // 
            this.lblYearDenpyoDateFr.AutoSize = true;
            this.lblYearDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblYearDenpyoDateFr.Location = new System.Drawing.Point(188, 10);
            this.lblYearDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblYearDenpyoDateFr.Name = "lblYearDenpyoDateFr";
            this.lblYearDenpyoDateFr.Size = new System.Drawing.Size(24, 24);
            this.lblYearDenpyoDateFr.TabIndex = 2;
            this.lblYearDenpyoDateFr.Tag = "CHANGE";
            this.lblYearDenpyoDateFr.Text = "年";
            this.lblYearDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMonthDenpyoDateFr
            // 
            this.txtMonthDenpyoDateFr.AutoSizeFromLength = false;
            this.txtMonthDenpyoDateFr.DisplayLength = null;
            this.txtMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMonthDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthDenpyoDateFr.Location = new System.Drawing.Point(220, 11);
            this.txtMonthDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthDenpyoDateFr.MaxLength = 2;
            this.txtMonthDenpyoDateFr.Name = "txtMonthDenpyoDateFr";
            this.txtMonthDenpyoDateFr.Size = new System.Drawing.Size(32, 23);
            this.txtMonthDenpyoDateFr.TabIndex = 3;
            this.txtMonthDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateFr_Validating);
            // 
            // lblGengoDenpyoDateFr
            // 
            this.lblGengoDenpyoDateFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoDenpyoDateFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblGengoDenpyoDateFr.ForeColor = System.Drawing.Color.Black;
            this.lblGengoDenpyoDateFr.Location = new System.Drawing.Point(80, 10);
            this.lblGengoDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoDenpyoDateFr.Name = "lblGengoDenpyoDateFr";
            this.lblGengoDenpyoDateFr.Size = new System.Drawing.Size(65, 24);
            this.lblGengoDenpyoDateFr.TabIndex = 0;
            this.lblGengoDenpyoDateFr.Text = "平成";
            this.lblGengoDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearDenpyoDateFr
            // 
            this.txtGengoYearDenpyoDateFr.AutoSizeFromLength = false;
            this.txtGengoYearDenpyoDateFr.DisplayLength = null;
            this.txtGengoYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtGengoYearDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearDenpyoDateFr.Location = new System.Drawing.Point(149, 11);
            this.txtGengoYearDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtGengoYearDenpyoDateFr.MaxLength = 2;
            this.txtGengoYearDenpyoDateFr.Name = "txtGengoYearDenpyoDateFr";
            this.txtGengoYearDenpyoDateFr.Size = new System.Drawing.Size(32, 23);
            this.txtGengoYearDenpyoDateFr.TabIndex = 1;
            this.txtGengoYearDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateFr_Validating);
            // 
            // lblKamokuNm
            // 
            this.lblKamokuNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKamokuNm.ForeColor = System.Drawing.Color.Black;
            this.lblKamokuNm.Location = new System.Drawing.Point(80, 5);
            this.lblKamokuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKamokuNm.Name = "lblKamokuNm";
            this.lblKamokuNm.Size = new System.Drawing.Size(460, 32);
            this.lblKamokuNm.TabIndex = 7;
            this.lblKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZandaka
            // 
            this.lblZandaka.BackColor = System.Drawing.Color.Transparent;
            this.lblZandaka.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZandaka.Location = new System.Drawing.Point(723, 665);
            this.lblZandaka.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZandaka.Name = "lblZandaka";
            this.lblZandaka.Size = new System.Drawing.Size(133, 27);
            this.lblZandaka.TabIndex = 906;
            this.lblZandaka.Text = "1,234,567,890,123";
            this.lblZandaka.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBiko
            // 
            this.txtBiko.AutoSizeFromLength = false;
            this.txtBiko.DisplayLength = null;
            this.txtBiko.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBiko.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBiko.Location = new System.Drawing.Point(549, 10);
            this.txtBiko.Margin = new System.Windows.Forms.Padding(4);
            this.txtBiko.MaxLength = 40;
            this.txtBiko.Name = "txtBiko";
            this.txtBiko.Size = new System.Drawing.Size(112, 23);
            this.txtBiko.TabIndex = 907;
            this.txtBiko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBiko.Visible = false;
            // 
            // lblBakKamoku
            // 
            this.lblBakKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblBakKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBakKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBakKamoku.Location = new System.Drawing.Point(0, 0);
            this.lblBakKamoku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBakKamoku.Name = "lblBakKamoku";
            this.lblBakKamoku.Size = new System.Drawing.Size(836, 43);
            this.lblBakKamoku.TabIndex = 908;
            this.lblBakKamoku.Tag = "CHANGE";
            this.lblBakKamoku.Text = "勘定科目";
            this.lblBakKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(836, 44);
            this.label1.TabIndex = 909;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "伝票日付";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(12, 47);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(844, 102);
            this.fsiTableLayoutPanel1.TabIndex = 910;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblGengoDenpyoDateFr);
            this.fsiPanel2.Controls.Add(this.txtGengoYearDenpyoDateFr);
            this.fsiPanel2.Controls.Add(this.lblDenpyoDateTo);
            this.fsiPanel2.Controls.Add(this.txtMonthDenpyoDateFr);
            this.fsiPanel2.Controls.Add(this.lblDayDenpyoDateTo);
            this.fsiPanel2.Controls.Add(this.lblYearDenpyoDateFr);
            this.fsiPanel2.Controls.Add(this.txtDayDenpyoDateTo);
            this.fsiPanel2.Controls.Add(this.lblMonthDenpyoDateFr);
            this.fsiPanel2.Controls.Add(this.lblMonthDenpyoDateTo);
            this.fsiPanel2.Controls.Add(this.txtDayDenpyoDateFr);
            this.fsiPanel2.Controls.Add(this.lblYearDenpyoDateTo);
            this.fsiPanel2.Controls.Add(this.lblDayDenpyoDateFr);
            this.fsiPanel2.Controls.Add(this.txtMonthDenpyoDateTo);
            this.fsiPanel2.Controls.Add(this.txtGengoYearDenpyoDateTo);
            this.fsiPanel2.Controls.Add(this.lblGengoDenpyoDateTo);
            this.fsiPanel2.Controls.Add(this.label1);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 54);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(836, 44);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.lblKamokuNm);
            this.fsiPanel1.Controls.Add(this.txtBiko);
            this.fsiPanel1.Controls.Add(this.lblBakKamoku);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(836, 43);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // ZMDE1034
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 745);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.lblDenpyoDateFr);
            this.Controls.Add(this.lblZandaka);
            this.Controls.Add(this.lblCrTotal);
            this.Controls.Add(this.lblDrTotal);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMDE1034";
            this.ShowFButton = true;
            this.Text = "元帳照会";
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblTotal, 0);
            this.Controls.SetChildIndex(this.lblDrTotal, 0);
            this.Controls.SetChildIndex(this.lblCrTotal, 0);
            this.Controls.SetChildIndex(this.lblZandaka, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblDrTotal;
        private System.Windows.Forms.Label lblCrTotal;
        private System.Windows.Forms.Label lblDayDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateFr;
        private System.Windows.Forms.Label lblDenpyoDateFr;
        private System.Windows.Forms.Label lblMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblYearDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblGengoDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateFr;
        private System.Windows.Forms.Label lblDenpyoDateTo;
        private System.Windows.Forms.Label lblDayDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateTo;
        private System.Windows.Forms.Label lblMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblYearDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblGengoDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateTo;
        private System.Windows.Forms.Label lblKamokuNm;
        private System.Windows.Forms.Label lblZandaka;
        private jp.co.fsi.common.controls.FsiTextBox txtBiko;
        private System.Windows.Forms.Label lblBakKamoku;
        private System.Windows.Forms.Label label1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}