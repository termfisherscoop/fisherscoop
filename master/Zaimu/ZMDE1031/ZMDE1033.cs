﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmde1031
{
    /// <summary>
    /// 元帳照会(ZMDE1033)
    /// </summary>
    public partial class ZMDE1033 : BasePgForm
    {
        #region 変数
        // 支所コード
        private int ShishoCode;

        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDE1033()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            // サイズを縮める
            this.Size = new Size(930, 650);
            // ESC F1 F6のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // 取引先コードにフォーカス
            this.txtTorihikisakiCd.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (ActiveCtlNm)
            {
                case "txtTorihikisakiCd":
                    btnF1.Text = "F1" + "\n\r" + "\n\r" + "検索";
                    break;
                default:
                    btnF1.Text = "F1" + "\n\r" + "\n\r" + "取引先";
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // ダイアログとしての処理結果を返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			switch (this.ActiveCtlNm)
            {
                case "txtTorihikisakiCd":
                    #region 取引先検索
                    // アセンブリのロード
                    //Assembly asm = System.Reflection.Assembly.LoadFrom("COMC2011.exe");
                    Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    //Type t = asm.GetType("jp.co.fsi.com.comc2011.COMC2011");
                    Type t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "9"; // 取引先全て
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtTorihikisakiCd.Text = result[0];
                                this.lblTorihikisakiNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                default:
                    // 初期状態に戻す
                    SearchData(true);
                    // 取引先コードにフォーカスを戻す
                    this.txtTorihikisakiCd.Focus();
                    this.txtTorihikisakiCd.SelectAll();
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 元帳照会画面の起動
            if (this.dgvList.SelectedRows.Count > 0)
            {
               ShowMotocho(Util.ToString(this.dgvList.SelectedRows[0].Cells["KANJO_KAMOKU_CD"].Value)
                    , this.txtTorihikisakiCd.Text);
            }
        }
        #endregion

        #region イベント

        // グリッドフォーカス時にOnMoveFocusで処理されない？ため
        private void dgvList_Enter(object sender, System.EventArgs e)
        {
            btnF1.Text = "F1" + "\n\r" + "\n\r" + "取引先";
        }

        /// <summary>
        /// 取引先コード検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTorihikisakiCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力された情報を元に検索する

            if (!IsValidTorihikisakiCd())
            {
                e.Cancel = true;
                this.txtTorihikisakiCd.SelectAll();
            }
            else
            {
                // if (this.txtTorihikisakiCd.Modified)
                {
                    if (ValChk.IsEmpty(this.txtTorihikisakiCd.Text))
                    {
                        lblTorihikisakiNm.Text = "";
                        SearchData(true);
                    }
                    else
                    {
                        lblTorihikisakiNm.Text = 
                            this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", this.ShishoCode.ToString(), this.txtTorihikisakiCd.Text);
                        SearchData(false);
                    }
                    this.txtTorihikisakiCd.Modified = false;
                }
            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dgvList.Rows.Count > 0)
                {
                    ShowMotocho(Util.ToString(this.dgvList.SelectedRows[0].Cells["KANJO_KAMOKU_CD"].Value)
                        , this.txtTorihikisakiCd.Text);
                    e.Handled = true;
                }
            }
            if (e.KeyCode == Keys.F1)
            {
                this.txtTorihikisakiCd.Focus();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvList.Rows.Count > 0)
            {
                if ((this.dgvList.SelectedRows[0].Cells["SAIKEN_ZANDAKA"].Value != DBNull.Value && int.Parse(this.dgvList.SelectedRows[0].Cells["SAIKEN_ZANDAKA"].Value.ToString()) != 0) ||
                    (this.dgvList.SelectedRows[0].Cells["SAIMU_ZANDAKA"].Value != DBNull.Value && int.Parse(this.dgvList.SelectedRows[0].Cells["SAIMU_ZANDAKA"].Value.ToString()) != 0))
                {
                    ShowMotocho(Util.ToString(this.dgvList.SelectedRows[0].Cells["KANJO_KAMOKU_CD"].Value)
                        , this.txtTorihikisakiCd.Text);
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // データアクセスパラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10,
                isInitial ? "-1" : this.txtTorihikisakiCd.Text);       // 初期処理の場合はあり得ない検索条件
            // SQL文字列
            StringBuilder sql = new StringBuilder();
            #region sql = 取引先別残高データ照会SQL
            sql.Append("SELECT A.KAISHA_CD AS KAISHA_CD");
            sql.Append(", A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD");
            sql.Append(", B.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD");
            sql.Append(", MIN(A.KANJO_KAMOKU_NM) AS KANJO_KAMOKU_NM");
            sql.Append(", MIN(A.TAISHAKU_KUBUN) AS TAISHAKU_KUBUN");
            sql.Append(", MIN(B.HOJO_KAMOKU_NM) AS HOJO_KAMOKU_NM");
            sql.Append(", SUM(");
            sql.Append("   CASE WHEN A.TAISHAKU_KUBUN = 1 THEN");
            sql.Append("    CASE WHEN C.TAISHAKU_KUBUN = 1 THEN C.ZEINUKI_KINGAKU");
            sql.Append("    ELSE (C.ZEINUKI_KINGAKU * -1) END");
            sql.Append("   ELSE 0 END");
            sql.Append("  ) AS SAIKEN_ZANDAKA");
            sql.Append(", SUM(");
            sql.Append("   CASE WHEN A.TAISHAKU_KUBUN = 2 THEN");
            sql.Append("    CASE WHEN C.TAISHAKU_KUBUN = 2 THEN C.ZEINUKI_KINGAKU");
            sql.Append("    ELSE (C.ZEINUKI_KINGAKU * -1) END ");
            sql.Append("   ELSE 0 END");
            sql.Append("  ) AS SAIMU_ZANDAKA");
            sql.Append(" FROM TB_ZM_KANJO_KAMOKU AS A");
            sql.Append(" LEFT OUTER JOIN VI_ZM_HOJO_KAMOKU AS B");
            sql.Append(" ON (A.KAISHA_CD = B.KAISHA_CD)");
            sql.Append(" AND (A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD)");
            sql.Append(" AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");
            //sql.Append(" AND (B.SHISHO_CD = @SHISHO_CD or B.SHISHO_CD is null)"); // 補助使用区分：２の為不要
            sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS C");
            sql.Append(" ON (B.KAISHA_CD = C.KAISHA_CD)");
            sql.Append(" AND C.SHISHO_CD = @SHISHO_CD");
            sql.Append(" AND (B.KAIKEI_NENDO = C.KAIKEI_NENDO)");
            sql.Append(" AND (B.HOJO_KAMOKU_CD = C.HOJO_KAMOKU_CD)");
            sql.Append(" AND (B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD)");
            sql.Append(" WHERE A.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" AND A.HOJO_KAMOKU_UMU = 1");
            sql.Append(" AND A.HOJO_SHIYO_KUBUN = 2");
            sql.Append(" AND B.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
            sql.Append(" GROUP BY A.KAISHA_CD");
            sql.Append(", A.KANJO_KAMOKU_CD");
            sql.Append(", B.HOJO_KAMOKU_CD");
            sql.Append(" ORDER BY A.KAISHA_CD ASC");
            sql.Append(", A.KANJO_KAMOKU_CD ASC");
            sql.Append(", B.HOJO_KAMOKU_CD ASC");
            #endregion
            DataTable dt = Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            // グリッドへデータソース設定
            this.dgvList.DataSource = dt;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // 列設定
            DataGridViewCellStyle curStyle = new DataGridViewCellStyle();       // 金額列スタイル
            curStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            curStyle.Format = "#,###";
            this.dgvList.Columns[0].Visible = false;
            this.dgvList.Columns[1].Visible = false;
            this.dgvList.Columns[2].Visible = false;
            this.dgvList.Columns[3].HeaderText = "勘定科目名";
            this.dgvList.Columns[3].Width = 200;
            this.dgvList.Columns[4].Visible = false;
            this.dgvList.Columns[5].Visible = false;
            this.dgvList.Columns[6].HeaderText = "債権残高";
            this.dgvList.Columns[6].Width = 150;
            this.dgvList.Columns[6].DefaultCellStyle = curStyle;
            this.dgvList.Columns[7].HeaderText = "債務残高";
            this.dgvList.Columns[7].Width = 150;
            this.dgvList.Columns[7].DefaultCellStyle = curStyle;

            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;

            // 合計
            decimal saikenTotal = 0;
            decimal saimuTotal = 0;
            foreach (DataGridViewRow ro in this.dgvList.Rows)
            {
                saikenTotal += Util.ToDecimal(ro.Cells["SAIKEN_ZANDAKA"].Value);
                saimuTotal += Util.ToDecimal(ro.Cells["SAIMU_ZANDAKA"].Value);
            }
            this.lblSaikenTotal.Text = Util.FormatNum(saikenTotal);
            this.lblSaimuTotal.Text = Util.FormatNum(saimuTotal);
        }

        /// <summary>
        /// 取引先コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTorihikisakiCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTorihikisakiCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はOK
            if (ValChk.IsEmpty(this.txtTorihikisakiCd.Text))
            {
                return true;
            }

            // 存在しないコードを入力した場合はエラーとする
            string name = this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", this.ShishoCode.ToString(), this.txtTorihikisakiCd.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 元帳画面を表示する
        /// </summary>
        /// <param name="kanjoKamokuCd">勘定科目コード</param>
        /// <param name="torihikisakiCd">取引先コード</param>
        private void ShowMotocho(string kanjoKamokuCd, string torihikisakiCd)
        {
            //KYUC1072 frmKYUC1072;

            //if (ValChk.IsEmpty(code))
            //{
            //    // 新規登録モードで登録画面を起動
            //    frmKYUC1072 = new KYUC1072("1");
            //}
            //else
            //{
            //    // 編集モードで登録画面を起動
            //    frmKYUC1072 = new KYUC1072("2");
            //    frmKYUC1072.InData = code;
            //}

            //DialogResult result = frmKYUC1072.ShowDialog(this);

            //if (result == DialogResult.OK)
            //{
            //    // データを再検索する
            //    SearchData(false);
            //    // 元々選択していたデータを選択
            //    for (int i = 0; i < this.dgvList.Rows.Count; i++)
            //    {
            //        if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["市町村コード"].Value)))
            //        {
            //            this.dgvList.Rows[i].Selected = true;
            //            break;
            //        }
            //    }
            //    // Gridに再度フォーカスをセット
            //    this.ActiveControl = this.dgvList;
            //    this.dgvList.Focus();
            //}
            // グリッド行が選択されている場合に実行
            if (this.dgvList.SelectedRows.Count > 0)
            {
                ZMDE1034 frmZMDE1034 = new ZMDE1034();
                frmZMDE1034.KanjoKamokuCd = Util.ToString(this.dgvList.SelectedRows[0].Cells["KANJO_KAMOKU_CD"].Value);
                frmZMDE1034.HojoKamokuCd = this.txtTorihikisakiCd.Text;
                DialogResult result = frmZMDE1034.ShowDialog(this);
            }
            }

        #endregion

    }
}
