﻿namespace jp.co.fsi.zm.zmcm1021
{
    partial class ZMCM1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKariukeShohizeiKamoku = new System.Windows.Forms.Label();
            this.txtKariukeShohizeiKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateYearFr = new System.Windows.Forms.Label();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.lblKariukeShohizeiKamokuNm = new System.Windows.Forms.Label();
            this.lblKaribaraiShohizeiKamokuNm = new System.Windows.Forms.Label();
            this.lblShohizeiHasuShoriNm = new System.Windows.Forms.Label();
            this.lblShohizeiNyuryokuHohoNm = new System.Windows.Forms.Label();
            this.lblJigyoKubunNm = new System.Windows.Forms.Label();
            this.lblKojoHohoNm = new System.Windows.Forms.Label();
            this.lblKazeiHohoNm = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtChihoShohizeiRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblChihoShohizeiRitsu = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKaribaraiShohizeiKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShinShohizeiRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKyuShohizeiRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJigyoKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojoHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKazeiHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKaribaraiShohizeiKamoku = new System.Windows.Forms.Label();
            this.lblShinShohizeiRitsu = new System.Windows.Forms.Label();
            this.lblKyuShohizeiRitsu = new System.Windows.Forms.Label();
            this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
            this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
            this.lblJigyoKubun = new System.Windows.Forms.Label();
            this.lblKojoHoho = new System.Windows.Forms.Label();
            this.lblKazeiHoho = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel10.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel11.SuspendLayout();
            this.fsiPanel12.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\nさくじょ";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\nとうろく";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(7, 717);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1135, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1124, 31);
            this.lblTitle.Text = "";
            // 
            // lblKariukeShohizeiKamoku
            // 
            this.lblKariukeShohizeiKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblKariukeShohizeiKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKariukeShohizeiKamoku.Enabled = false;
            this.lblKariukeShohizeiKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKariukeShohizeiKamoku.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblKariukeShohizeiKamoku.Location = new System.Drawing.Point(0, 0);
            this.lblKariukeShohizeiKamoku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKariukeShohizeiKamoku.Name = "lblKariukeShohizeiKamoku";
            this.lblKariukeShohizeiKamoku.Size = new System.Drawing.Size(739, 44);
            this.lblKariukeShohizeiKamoku.TabIndex = 23;
            this.lblKariukeShohizeiKamoku.Tag = "CHANGE";
            this.lblKariukeShohizeiKamoku.Text = "仮受消費税科目";
            this.lblKariukeShohizeiKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKariukeShohizeiKamokuCd
            // 
            this.txtKariukeShohizeiKamokuCd.AllowDrop = true;
            this.txtKariukeShohizeiKamokuCd.AutoSizeFromLength = false;
            this.txtKariukeShohizeiKamokuCd.DisplayLength = null;
            this.txtKariukeShohizeiKamokuCd.Enabled = false;
            this.txtKariukeShohizeiKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKariukeShohizeiKamokuCd.Location = new System.Drawing.Point(163, 12);
            this.txtKariukeShohizeiKamokuCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtKariukeShohizeiKamokuCd.MaxLength = 6;
            this.txtKariukeShohizeiKamokuCd.Name = "txtKariukeShohizeiKamokuCd";
            this.txtKariukeShohizeiKamokuCd.ReadOnly = true;
            this.txtKariukeShohizeiKamokuCd.Size = new System.Drawing.Size(61, 23);
            this.txtKariukeShohizeiKamokuCd.TabIndex = 24;
            this.txtKariukeShohizeiKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateDayFr.Location = new System.Drawing.Point(409, 7);
            this.lblDateDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(20, 24);
            this.lblDateDayFr.TabIndex = 959;
            this.lblDateDayFr.Tag = "CHANGE";
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthFr.Location = new System.Drawing.Point(336, 7);
            this.lblDateMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(20, 24);
            this.lblDateMonthFr.TabIndex = 957;
            this.lblDateMonthFr.Tag = "CHANGE";
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearFr
            // 
            this.lblDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearFr.Location = new System.Drawing.Point(265, 7);
            this.lblDateYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateYearFr.Name = "lblDateYearFr";
            this.lblDateYearFr.Size = new System.Drawing.Size(20, 24);
            this.lblDateYearFr.TabIndex = 955;
            this.lblDateYearFr.Tag = "CHANGE";
            this.lblDateYearFr.Text = "年";
            this.lblDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateDayFr.Location = new System.Drawing.Point(365, 8);
            this.txtDateDayFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(39, 23);
            this.txtDateDayFr.TabIndex = 3;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearFr.Location = new System.Drawing.Point(223, 8);
            this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(39, 23);
            this.txtDateYearFr.TabIndex = 1;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthFr.Location = new System.Drawing.Point(293, 8);
            this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(39, 23);
            this.txtDateMonthFr.TabIndex = 2;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Enabled = false;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoFr.Location = new System.Drawing.Point(163, 7);
            this.lblDateGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(55, 24);
            this.lblDateGengoFr.TabIndex = 952;
            this.lblDateGengoFr.Tag = "DISPNAME";
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKariukeShohizeiKamokuNm
            // 
            this.lblKariukeShohizeiKamokuNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblKariukeShohizeiKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKariukeShohizeiKamokuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKariukeShohizeiKamokuNm.Location = new System.Drawing.Point(231, 11);
            this.lblKariukeShohizeiKamokuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKariukeShohizeiKamokuNm.Name = "lblKariukeShohizeiKamokuNm";
            this.lblKariukeShohizeiKamokuNm.Size = new System.Drawing.Size(297, 24);
            this.lblKariukeShohizeiKamokuNm.TabIndex = 950;
            this.lblKariukeShohizeiKamokuNm.Tag = "DISPNAME";
            this.lblKariukeShohizeiKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKaribaraiShohizeiKamokuNm
            // 
            this.lblKaribaraiShohizeiKamokuNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblKaribaraiShohizeiKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKaribaraiShohizeiKamokuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKaribaraiShohizeiKamokuNm.Location = new System.Drawing.Point(231, 6);
            this.lblKaribaraiShohizeiKamokuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKaribaraiShohizeiKamokuNm.Name = "lblKaribaraiShohizeiKamokuNm";
            this.lblKaribaraiShohizeiKamokuNm.Size = new System.Drawing.Size(297, 24);
            this.lblKaribaraiShohizeiKamokuNm.TabIndex = 949;
            this.lblKaribaraiShohizeiKamokuNm.Tag = "DISPNAME";
            this.lblKaribaraiShohizeiKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiHasuShoriNm
            // 
            this.lblShohizeiHasuShoriNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohizeiHasuShoriNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiHasuShoriNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShohizeiHasuShoriNm.Location = new System.Drawing.Point(199, 7);
            this.lblShohizeiHasuShoriNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohizeiHasuShoriNm.Name = "lblShohizeiHasuShoriNm";
            this.lblShohizeiHasuShoriNm.Size = new System.Drawing.Size(316, 24);
            this.lblShohizeiHasuShoriNm.TabIndex = 948;
            this.lblShohizeiHasuShoriNm.Tag = "DISPNAME";
            this.lblShohizeiHasuShoriNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiNyuryokuHohoNm
            // 
            this.lblShohizeiNyuryokuHohoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohizeiNyuryokuHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHohoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShohizeiNyuryokuHohoNm.Location = new System.Drawing.Point(199, 7);
            this.lblShohizeiNyuryokuHohoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohizeiNyuryokuHohoNm.Name = "lblShohizeiNyuryokuHohoNm";
            this.lblShohizeiNyuryokuHohoNm.Size = new System.Drawing.Size(316, 24);
            this.lblShohizeiNyuryokuHohoNm.TabIndex = 947;
            this.lblShohizeiNyuryokuHohoNm.Tag = "DISPNAME";
            this.lblShohizeiNyuryokuHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJigyoKubunNm
            // 
            this.lblJigyoKubunNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblJigyoKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKubunNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJigyoKubunNm.Location = new System.Drawing.Point(199, 6);
            this.lblJigyoKubunNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJigyoKubunNm.Name = "lblJigyoKubunNm";
            this.lblJigyoKubunNm.Size = new System.Drawing.Size(316, 24);
            this.lblJigyoKubunNm.TabIndex = 946;
            this.lblJigyoKubunNm.Tag = "DISPNAME";
            this.lblJigyoKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKojoHohoNm
            // 
            this.lblKojoHohoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblKojoHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoHohoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKojoHohoNm.Location = new System.Drawing.Point(199, 7);
            this.lblKojoHohoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKojoHohoNm.Name = "lblKojoHohoNm";
            this.lblKojoHohoNm.Size = new System.Drawing.Size(316, 24);
            this.lblKojoHohoNm.TabIndex = 945;
            this.lblKojoHohoNm.Tag = "DISPNAME";
            this.lblKojoHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKazeiHohoNm
            // 
            this.lblKazeiHohoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblKazeiHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKazeiHohoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKazeiHohoNm.Location = new System.Drawing.Point(199, 6);
            this.lblKazeiHohoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKazeiHohoNm.Name = "lblKazeiHohoNm";
            this.lblKazeiHohoNm.Size = new System.Drawing.Size(316, 24);
            this.lblKazeiHohoNm.TabIndex = 944;
            this.lblKazeiHohoNm.Tag = "DISPNAME";
            this.lblKazeiHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.Enabled = false;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(245, 5);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 24);
            this.label4.TabIndex = 943;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "％";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChihoShohizeiRitsu
            // 
            this.txtChihoShohizeiRitsu.AllowDrop = true;
            this.txtChihoShohizeiRitsu.AutoSizeFromLength = false;
            this.txtChihoShohizeiRitsu.BackColor = System.Drawing.Color.White;
            this.txtChihoShohizeiRitsu.DisplayLength = null;
            this.txtChihoShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChihoShohizeiRitsu.Location = new System.Drawing.Point(192, 7);
            this.txtChihoShohizeiRitsu.Margin = new System.Windows.Forms.Padding(4);
            this.txtChihoShohizeiRitsu.MaxLength = 4;
            this.txtChihoShohizeiRitsu.Name = "txtChihoShohizeiRitsu";
            this.txtChihoShohizeiRitsu.Size = new System.Drawing.Size(47, 23);
            this.txtChihoShohizeiRitsu.TabIndex = 19;
            this.txtChihoShohizeiRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChihoShohizeiRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtChihoShohizeiritsu);
            // 
            // lblChihoShohizeiRitsu
            // 
            this.lblChihoShohizeiRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblChihoShohizeiRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChihoShohizeiRitsu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblChihoShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChihoShohizeiRitsu.Location = new System.Drawing.Point(0, 0);
            this.lblChihoShohizeiRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblChihoShohizeiRitsu.Name = "lblChihoShohizeiRitsu";
            this.lblChihoShohizeiRitsu.Size = new System.Drawing.Size(424, 38);
            this.lblChihoShohizeiRitsu.TabIndex = 18;
            this.lblChihoShohizeiRitsu.Tag = "CHANGE";
            this.lblChihoShohizeiRitsu.Text = "地 方 消 費 税 率";
            this.lblChihoShohizeiRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.Enabled = false;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(216, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 24);
            this.label3.TabIndex = 940;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "％";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(216, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 24);
            this.label1.TabIndex = 939;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "％";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKaribaraiShohizeiKamokuCd
            // 
            this.txtKaribaraiShohizeiKamokuCd.AllowDrop = true;
            this.txtKaribaraiShohizeiKamokuCd.AutoSizeFromLength = false;
            this.txtKaribaraiShohizeiKamokuCd.BackColor = System.Drawing.SystemColors.Control;
            this.txtKaribaraiShohizeiKamokuCd.DisplayLength = null;
            this.txtKaribaraiShohizeiKamokuCd.Enabled = false;
            this.txtKaribaraiShohizeiKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKaribaraiShohizeiKamokuCd.Location = new System.Drawing.Point(163, 7);
            this.txtKaribaraiShohizeiKamokuCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtKaribaraiShohizeiKamokuCd.MaxLength = 6;
            this.txtKaribaraiShohizeiKamokuCd.Name = "txtKaribaraiShohizeiKamokuCd";
            this.txtKaribaraiShohizeiKamokuCd.ReadOnly = true;
            this.txtKaribaraiShohizeiKamokuCd.Size = new System.Drawing.Size(61, 23);
            this.txtKaribaraiShohizeiKamokuCd.TabIndex = 21;
            this.txtKaribaraiShohizeiKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtShinShohizeiRitsu
            // 
            this.txtShinShohizeiRitsu.AllowDrop = true;
            this.txtShinShohizeiRitsu.AutoSizeFromLength = false;
            this.txtShinShohizeiRitsu.BackColor = System.Drawing.Color.White;
            this.txtShinShohizeiRitsu.DisplayLength = null;
            this.txtShinShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShinShohizeiRitsu.Location = new System.Drawing.Point(163, 7);
            this.txtShinShohizeiRitsu.Margin = new System.Windows.Forms.Padding(4);
            this.txtShinShohizeiRitsu.MaxLength = 4;
            this.txtShinShohizeiRitsu.Name = "txtShinShohizeiRitsu";
            this.txtShinShohizeiRitsu.Size = new System.Drawing.Size(47, 23);
            this.txtShinShohizeiRitsu.TabIndex = 17;
            this.txtShinShohizeiRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShinShohizeiRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtShinShohizeiRitsu_Validating);
            // 
            // txtKyuShohizeiRitsu
            // 
            this.txtKyuShohizeiRitsu.AllowDrop = true;
            this.txtKyuShohizeiRitsu.AutoSizeFromLength = false;
            this.txtKyuShohizeiRitsu.BackColor = System.Drawing.Color.White;
            this.txtKyuShohizeiRitsu.DisplayLength = null;
            this.txtKyuShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyuShohizeiRitsu.Location = new System.Drawing.Point(163, 8);
            this.txtKyuShohizeiRitsu.Margin = new System.Windows.Forms.Padding(4);
            this.txtKyuShohizeiRitsu.MaxLength = 4;
            this.txtKyuShohizeiRitsu.Name = "txtKyuShohizeiRitsu";
            this.txtKyuShohizeiRitsu.Size = new System.Drawing.Size(47, 23);
            this.txtKyuShohizeiRitsu.TabIndex = 15;
            this.txtKyuShohizeiRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuShohizeiRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuShohizeiRitsu_Validating);
            // 
            // txtShohizeiHasuShori
            // 
            this.txtShohizeiHasuShori.AllowDrop = true;
            this.txtShohizeiHasuShori.AutoSizeFromLength = false;
            this.txtShohizeiHasuShori.BackColor = System.Drawing.Color.White;
            this.txtShohizeiHasuShori.DisplayLength = null;
            this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShohizeiHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohizeiHasuShori.Location = new System.Drawing.Point(163, 8);
            this.txtShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohizeiHasuShori.MaxLength = 1;
            this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
            this.txtShohizeiHasuShori.Size = new System.Drawing.Size(28, 23);
            this.txtShohizeiHasuShori.TabIndex = 13;
            this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
            // 
            // txtShohizeiNyuryokuHoho
            // 
            this.txtShohizeiNyuryokuHoho.AllowDrop = true;
            this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
            this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.SystemColors.Control;
            this.txtShohizeiNyuryokuHoho.DisplayLength = null;
            this.txtShohizeiNyuryokuHoho.Enabled = false;
            this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShohizeiNyuryokuHoho.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(163, 8);
            this.txtShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohizeiNyuryokuHoho.MaxLength = 1;
            this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
            this.txtShohizeiNyuryokuHoho.ReadOnly = true;
            this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(28, 23);
            this.txtShohizeiNyuryokuHoho.TabIndex = 11;
            this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtJigyoKubun
            // 
            this.txtJigyoKubun.AllowDrop = true;
            this.txtJigyoKubun.AutoSizeFromLength = false;
            this.txtJigyoKubun.BackColor = System.Drawing.Color.White;
            this.txtJigyoKubun.DisplayLength = null;
            this.txtJigyoKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJigyoKubun.Location = new System.Drawing.Point(163, 7);
            this.txtJigyoKubun.Margin = new System.Windows.Forms.Padding(4);
            this.txtJigyoKubun.MaxLength = 1;
            this.txtJigyoKubun.Name = "txtJigyoKubun";
            this.txtJigyoKubun.Size = new System.Drawing.Size(28, 23);
            this.txtJigyoKubun.TabIndex = 9;
            this.txtJigyoKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJigyoKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtJigyoKubun_Validating);
            // 
            // txtKojoHoho
            // 
            this.txtKojoHoho.AllowDrop = true;
            this.txtKojoHoho.AutoSizeFromLength = false;
            this.txtKojoHoho.BackColor = System.Drawing.Color.White;
            this.txtKojoHoho.DisplayLength = null;
            this.txtKojoHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKojoHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKojoHoho.Location = new System.Drawing.Point(163, 8);
            this.txtKojoHoho.Margin = new System.Windows.Forms.Padding(4);
            this.txtKojoHoho.MaxLength = 1;
            this.txtKojoHoho.Name = "txtKojoHoho";
            this.txtKojoHoho.Size = new System.Drawing.Size(28, 23);
            this.txtKojoHoho.TabIndex = 7;
            this.txtKojoHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojoHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoHoho_Validating);
            // 
            // txtKazeiHoho
            // 
            this.txtKazeiHoho.AllowDrop = true;
            this.txtKazeiHoho.AutoSizeFromLength = false;
            this.txtKazeiHoho.BackColor = System.Drawing.Color.White;
            this.txtKazeiHoho.DisplayLength = null;
            this.txtKazeiHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKazeiHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKazeiHoho.Location = new System.Drawing.Point(163, 7);
            this.txtKazeiHoho.Margin = new System.Windows.Forms.Padding(4);
            this.txtKazeiHoho.MaxLength = 1;
            this.txtKazeiHoho.Name = "txtKazeiHoho";
            this.txtKazeiHoho.Size = new System.Drawing.Size(28, 23);
            this.txtKazeiHoho.TabIndex = 5;
            this.txtKazeiHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKazeiHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtKazeiHoho_Validating);
            // 
            // lblKaribaraiShohizeiKamoku
            // 
            this.lblKaribaraiShohizeiKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblKaribaraiShohizeiKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKaribaraiShohizeiKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKaribaraiShohizeiKamoku.Enabled = false;
            this.lblKaribaraiShohizeiKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKaribaraiShohizeiKamoku.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblKaribaraiShohizeiKamoku.Location = new System.Drawing.Point(0, 0);
            this.lblKaribaraiShohizeiKamoku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKaribaraiShohizeiKamoku.Name = "lblKaribaraiShohizeiKamoku";
            this.lblKaribaraiShohizeiKamoku.Size = new System.Drawing.Size(739, 38);
            this.lblKaribaraiShohizeiKamoku.TabIndex = 20;
            this.lblKaribaraiShohizeiKamoku.Tag = "CHANGE";
            this.lblKaribaraiShohizeiKamoku.Text = "仮払消費税科目";
            this.lblKaribaraiShohizeiKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShinShohizeiRitsu
            // 
            this.lblShinShohizeiRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblShinShohizeiRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShinShohizeiRitsu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShinShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShinShohizeiRitsu.Location = new System.Drawing.Point(0, 0);
            this.lblShinShohizeiRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShinShohizeiRitsu.Name = "lblShinShohizeiRitsu";
            this.lblShinShohizeiRitsu.Size = new System.Drawing.Size(739, 38);
            this.lblShinShohizeiRitsu.TabIndex = 16;
            this.lblShinShohizeiRitsu.Tag = "CHANGE";
            this.lblShinShohizeiRitsu.Text = "新 消 費 税 率";
            this.lblShinShohizeiRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuShohizeiRitsu
            // 
            this.lblKyuShohizeiRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblKyuShohizeiRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuShohizeiRitsu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKyuShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKyuShohizeiRitsu.Location = new System.Drawing.Point(0, 0);
            this.lblKyuShohizeiRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKyuShohizeiRitsu.Name = "lblKyuShohizeiRitsu";
            this.lblKyuShohizeiRitsu.Size = new System.Drawing.Size(739, 38);
            this.lblKyuShohizeiRitsu.TabIndex = 14;
            this.lblKyuShohizeiRitsu.Tag = "CHANGE";
            this.lblKyuShohizeiRitsu.Text = "旧 消 費 税 率";
            this.lblKyuShohizeiRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiHasuShori
            // 
            this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiHasuShori.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShohizeiHasuShori.Location = new System.Drawing.Point(0, 0);
            this.lblShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
            this.lblShohizeiHasuShori.Size = new System.Drawing.Size(739, 38);
            this.lblShohizeiHasuShori.TabIndex = 12;
            this.lblShohizeiHasuShori.Tag = "CHANGE";
            this.lblShohizeiHasuShori.Text = "消費税端数処理";
            this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiNyuryokuHoho
            // 
            this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHoho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShohizeiNyuryokuHoho.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(0, 0);
            this.lblShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
            this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(739, 38);
            this.lblShohizeiNyuryokuHoho.TabIndex = 10;
            this.lblShohizeiNyuryokuHoho.Tag = "CHANGE";
            this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
            this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJigyoKubun
            // 
            this.lblJigyoKubun.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKubun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJigyoKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJigyoKubun.Location = new System.Drawing.Point(0, 0);
            this.lblJigyoKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJigyoKubun.Name = "lblJigyoKubun";
            this.lblJigyoKubun.Size = new System.Drawing.Size(739, 38);
            this.lblJigyoKubun.TabIndex = 8;
            this.lblJigyoKubun.Tag = "CHANGE";
            this.lblJigyoKubun.Text = "主たる事業区分";
            this.lblJigyoKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKojoHoho
            // 
            this.lblKojoHoho.BackColor = System.Drawing.Color.Silver;
            this.lblKojoHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoHoho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKojoHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKojoHoho.Location = new System.Drawing.Point(0, 0);
            this.lblKojoHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKojoHoho.Name = "lblKojoHoho";
            this.lblKojoHoho.Size = new System.Drawing.Size(739, 38);
            this.lblKojoHoho.TabIndex = 6;
            this.lblKojoHoho.Tag = "CHANGE";
            this.lblKojoHoho.Text = "控  除  方  法";
            this.lblKojoHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKazeiHoho
            // 
            this.lblKazeiHoho.BackColor = System.Drawing.Color.Silver;
            this.lblKazeiHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKazeiHoho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKazeiHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKazeiHoho.Location = new System.Drawing.Point(0, 0);
            this.lblKazeiHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKazeiHoho.Name = "lblKazeiHoho";
            this.lblKazeiHoho.Size = new System.Drawing.Size(739, 38);
            this.lblKazeiHoho.TabIndex = 4;
            this.lblKazeiHoho.Tag = "CHANGE";
            this.lblKazeiHoho.Text = "課  税  方  法";
            this.lblKazeiHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(739, 38);
            this.label5.TabIndex = 960;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "適 用 開 始 日";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel10, 0, 9);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel9, 0, 8);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 7);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 35);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 10;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(749, 477);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel10
            // 
            this.fsiPanel10.Controls.Add(this.lblKariukeShohizeiKamokuNm);
            this.fsiPanel10.Controls.Add(this.txtKariukeShohizeiKamokuCd);
            this.fsiPanel10.Controls.Add(this.lblKariukeShohizeiKamoku);
            this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel10.Location = new System.Drawing.Point(5, 428);
            this.fsiPanel10.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel10.Name = "fsiPanel10";
            this.fsiPanel10.Size = new System.Drawing.Size(739, 44);
            this.fsiPanel10.TabIndex = 9;
            this.fsiPanel10.Tag = "CHANGE";
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.lblKaribaraiShohizeiKamokuNm);
            this.fsiPanel9.Controls.Add(this.txtKaribaraiShohizeiKamokuCd);
            this.fsiPanel9.Controls.Add(this.lblKaribaraiShohizeiKamoku);
            this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel9.Location = new System.Drawing.Point(5, 381);
            this.fsiPanel9.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel9.TabIndex = 8;
            this.fsiPanel9.Tag = "CHANGE";
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.fsiPanel11);
            this.fsiPanel8.Controls.Add(this.fsiPanel12);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(5, 334);
            this.fsiPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel8.TabIndex = 7;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // fsiPanel11
            // 
            this.fsiPanel11.Controls.Add(this.txtChihoShohizeiRitsu);
            this.fsiPanel11.Controls.Add(this.label4);
            this.fsiPanel11.Controls.Add(this.lblChihoShohizeiRitsu);
            this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel11.Location = new System.Drawing.Point(315, 0);
            this.fsiPanel11.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel11.Name = "fsiPanel11";
            this.fsiPanel11.Size = new System.Drawing.Size(424, 38);
            this.fsiPanel11.TabIndex = 12;
            this.fsiPanel11.Tag = "CHANGE";
            // 
            // fsiPanel12
            // 
            this.fsiPanel12.Controls.Add(this.txtShinShohizeiRitsu);
            this.fsiPanel12.Controls.Add(this.label3);
            this.fsiPanel12.Controls.Add(this.lblShinShohizeiRitsu);
            this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel12.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel12.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel12.Name = "fsiPanel12";
            this.fsiPanel12.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel12.TabIndex = 13;
            this.fsiPanel12.Tag = "CHANGE";
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.txtKyuShohizeiRitsu);
            this.fsiPanel7.Controls.Add(this.label1);
            this.fsiPanel7.Controls.Add(this.lblKyuShohizeiRitsu);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(5, 287);
            this.fsiPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel7.TabIndex = 6;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtShohizeiHasuShori);
            this.fsiPanel6.Controls.Add(this.lblShohizeiHasuShoriNm);
            this.fsiPanel6.Controls.Add(this.lblShohizeiHasuShori);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(5, 240);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel6.TabIndex = 5;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtShohizeiNyuryokuHoho);
            this.fsiPanel5.Controls.Add(this.lblShohizeiNyuryokuHohoNm);
            this.fsiPanel5.Controls.Add(this.lblShohizeiNyuryokuHoho);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(5, 193);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtJigyoKubun);
            this.fsiPanel4.Controls.Add(this.lblJigyoKubunNm);
            this.fsiPanel4.Controls.Add(this.lblJigyoKubun);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 146);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtKojoHoho);
            this.fsiPanel3.Controls.Add(this.lblKojoHohoNm);
            this.fsiPanel3.Controls.Add(this.lblKojoHoho);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 99);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtKazeiHoho);
            this.fsiPanel2.Controls.Add(this.lblKazeiHohoNm);
            this.fsiPanel2.Controls.Add(this.lblKazeiHoho);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 52);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.lblDateDayFr);
            this.fsiPanel1.Controls.Add(this.lblDateMonthFr);
            this.fsiPanel1.Controls.Add(this.lblDateGengoFr);
            this.fsiPanel1.Controls.Add(this.lblDateYearFr);
            this.fsiPanel1.Controls.Add(this.txtDateMonthFr);
            this.fsiPanel1.Controls.Add(this.txtDateDayFr);
            this.fsiPanel1.Controls.Add(this.txtDateYearFr);
            this.fsiPanel1.Controls.Add(this.label5);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(739, 38);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // ZMCM1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1124, 856);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1021";
            this.ShowFButton = true;
            this.Text = "ReportSample";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel10.ResumeLayout(false);
            this.fsiPanel10.PerformLayout();
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel9.PerformLayout();
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel11.ResumeLayout(false);
            this.fsiPanel11.PerformLayout();
            this.fsiPanel12.ResumeLayout(false);
            this.fsiPanel12.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKariukeShohizeiKamoku;
        private jp.co.fsi.common.controls.FsiTextBox txtKariukeShohizeiKamokuCd;
        private System.Windows.Forms.Label lblShinShohizeiRitsu;
        private System.Windows.Forms.Label lblKyuShohizeiRitsu;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblJigyoKubun;
        private System.Windows.Forms.Label lblKojoHoho;
        private System.Windows.Forms.Label lblKazeiHoho;
        private System.Windows.Forms.Label lblKaribaraiShohizeiKamoku;
        private jp.co.fsi.common.controls.FsiTextBox txtKazeiHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtKaribaraiShohizeiKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShinShohizeiRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtKyuShohizeiRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtJigyoKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtKojoHoho;
        private System.Windows.Forms.Label label4;
        private common.controls.FsiTextBox txtChihoShohizeiRitsu;
        private System.Windows.Forms.Label lblChihoShohizeiRitsu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblKariukeShohizeiKamokuNm;
        private System.Windows.Forms.Label lblKaribaraiShohizeiKamokuNm;
        private System.Windows.Forms.Label lblShohizeiHasuShoriNm;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHohoNm;
        private System.Windows.Forms.Label lblJigyoKubunNm;
        private System.Windows.Forms.Label lblKojoHohoNm;
        private System.Windows.Forms.Label lblKazeiHohoNm;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateYearFr;
        private common.controls.FsiTextBox txtDateDayFr;
        private common.controls.FsiTextBox txtDateYearFr;
        private common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label label5;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel10;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel12;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}