﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1051
{
    /// <summary>
    /// 摘要の印刷(ZMCM1053)
    /// </summary>
    public partial class ZMCM1053 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// データ取得用
        /// </summary>
        private const int MAX_LINE_NO = 82;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1053()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // ボタンを設定
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;
            this.btnEsc.Visible = true;
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = true;
            this.btnF5.Visible = true;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 初期フォーカスを設定
            this.txtTekiyoCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                // 摘要コード(自)・(至)の場合のみ有効にする
                case "txtTekiyoCdFr":
                case "txtTekiyoCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            switch (this.ActiveCtlNm)
            {
                // 摘要コード(自)・(至)の場合、摘要の検索画面を立ち上げる
                case "txtTekiyoCdFr":
                case "txtTekiyoCdTo":
                    #region 摘要コード
                    // 摘要選択画面
                    using (ZMCM1051 frm = new ZMCM1051())
                    {
                        frm.Par1 = "1";
                        frm.ShowDialog(this);

                        if (frm.DialogResult == DialogResult.OK)
                        {
                            string[] result = (string[])frm.OutData;

                            if (this.ActiveCtlNm == "txtTekiyoCdFr")
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTekiyoCdFr.Text = outData[0];
                                this.lblTekiyoCdFr.Text = outData[1];
                            }
                            if (this.ActiveCtlNm == "txtTekiyoCdTo")
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTekiyoCdTo.Text = outData[0];
                                this.lblTekiyoCdTo.Text = outData[1];

                            }

                        }
                    }
                    #endregion
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー","実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 摘要コード(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoCdFr_Validating(object sender, CancelEventArgs e)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCdFr.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND TEKIYO_CD = @TEKIYO_CD");
            DataTable dtTekiyoDate =
                this.Dba.GetDataTableByConditionWithParams("TEKIYO_NM",
                    "VI_ZM_TEKIYO", Util.ToString(where), dpc);
            if (dtTekiyoDate.Rows.Count > 0)
            {
                this.lblTekiyoCdFr.Text = dtTekiyoDate.Rows[0]["TEKIYO_NM"].ToString();
            }
            else
            {
                this.lblTekiyoCdFr.Text = "";
            }
            // 未入力の場合、「先　頭」を表示
            if (ValChk.IsEmpty(this.txtTekiyoCdFr.Text))
            {
                this.lblTekiyoCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 摘要コード(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoCdTo_Validating(object sender, CancelEventArgs e)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCdTo.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND TEKIYO_CD = @TEKIYO_CD");
            DataTable dtTekiyoDate =
                this.Dba.GetDataTableByConditionWithParams("TEKIYO_NM",
                    "VI_ZM_TEKIYO", Util.ToString(where), dpc);
            if (dtTekiyoDate.Rows.Count > 0)
            {
                this.lblTekiyoCdTo.Text = dtTekiyoDate.Rows[0]["TEKIYO_NM"].ToString();
            }
            else
            {
                this.lblTekiyoCdTo.Text = "";
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 摘要コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoCdFr()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTekiyoCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 摘要コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoCdTo()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTekiyoCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 摘要コード(自)のチェック
            if (!IsValidTekiyoCdFr())
            {
                this.txtTekiyoCdFr.Focus();
                return false;
            }

            // 摘要コード(至)のチェック
            if (!IsValidTekiyoCdTo())
            {
                this.txtTekiyoCdTo.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            // 現状仮の状態
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMCM10511R rpt = new ZMCM10511R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region 設定値をセットする
            // 摘要コードを取得
            string TekiyoCdFr;
            string TekiyoCdTo;
            if (Util.ToString(this.txtTekiyoCdFr.Text) != "")
            {
                TekiyoCdFr = this.txtTekiyoCdFr.Text;
            }
            else
            {
                TekiyoCdFr = "0";
            }
            if (Util.ToString(this.txtTekiyoCdTo.Text) != "")
            {
                TekiyoCdTo = this.txtTekiyoCdTo.Text;
            }
            else
            {
                TekiyoCdTo = "9999";
            }
            #endregion

            #region データを取得する
            // 現状仮のSQL。
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" VI_ZM_TEKIYO ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" TEKIYO_CD BETWEEN @TEKIYO_CD_FR AND @TEKIYO_CD_TO");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd); // 会社コード
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd); // 支所コード
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TEKIYO_CD_FR", SqlDbType.Decimal, 4, TekiyoCdFr); // 摘要コードFr
            dpc.SetParam("@TEKIYO_CD_TO", SqlDbType.Decimal, 4, TekiyoCdTo); // 摘要コードTo

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            int i = 0; // ソート番号用変数
            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_ZM_TBL(");
                    Sql.Append(" GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(") ");
                    // データを設定
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dr["TEKIYO_CD"].ToString()); // ｺｰﾄﾞ
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["TEKIYO_NM"].ToString()); // 摘要名
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["TEKIYO_KANA_NM"].ToString()); // カナ名
                    // データを登録
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                #endregion

                #region 印刷ワークテーブルに空行登録
                // 現在のページの行の余りを計算する
                int Amari = i % MAX_LINE_NO;
                for (int j = Amari; j < MAX_LINE_NO; j++)
                {
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_ZM_TBL(");
                    Sql.Append(" GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(") ");
                    // データを設定
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    // データを登録
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_ZM_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_ZM_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}



