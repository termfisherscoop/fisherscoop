﻿namespace jp.co.fsi.zm.zmcm1031
{
    partial class ZMCM1035
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKamokuBunruiCdTo = new System.Windows.Forms.Label();
            this.lblKamokuBunruiCdFr = new System.Windows.Forms.Label();
            this.txtKamokuBunruiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKamokuBunruiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCdTo = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCdFr = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet2 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 141);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(863, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(853, 31);
            this.lblTitle.Text = "勘定科目の登録";
            // 
            // lblKamokuBunruiCdTo
            // 
            this.lblKamokuBunruiCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblKamokuBunruiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokuBunruiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKamokuBunruiCdTo.Location = new System.Drawing.Point(566, 7);
            this.lblKamokuBunruiCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKamokuBunruiCdTo.Name = "lblKamokuBunruiCdTo";
            this.lblKamokuBunruiCdTo.Size = new System.Drawing.Size(240, 24);
            this.lblKamokuBunruiCdTo.TabIndex = 4;
            this.lblKamokuBunruiCdTo.Tag = "DISPNAME";
            this.lblKamokuBunruiCdTo.Text = "最　後";
            this.lblKamokuBunruiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKamokuBunruiCdFr
            // 
            this.lblKamokuBunruiCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblKamokuBunruiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokuBunruiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKamokuBunruiCdFr.Location = new System.Drawing.Point(223, 7);
            this.lblKamokuBunruiCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKamokuBunruiCdFr.Name = "lblKamokuBunruiCdFr";
            this.lblKamokuBunruiCdFr.Size = new System.Drawing.Size(240, 24);
            this.lblKamokuBunruiCdFr.TabIndex = 1;
            this.lblKamokuBunruiCdFr.Tag = "DISPNAME";
            this.lblKamokuBunruiCdFr.Text = "先　頭";
            this.lblKamokuBunruiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKamokuBunruiCdTo
            // 
            this.txtKamokuBunruiCdTo.AutoSizeFromLength = true;
            this.txtKamokuBunruiCdTo.DisplayLength = null;
            this.txtKamokuBunruiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKamokuBunruiCdTo.Location = new System.Drawing.Point(498, 8);
            this.txtKamokuBunruiCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtKamokuBunruiCdTo.MaxLength = 5;
            this.txtKamokuBunruiCdTo.Name = "txtKamokuBunruiCdTo";
            this.txtKamokuBunruiCdTo.Size = new System.Drawing.Size(63, 23);
            this.txtKamokuBunruiCdTo.TabIndex = 3;
            this.txtKamokuBunruiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKamokuBunruiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKamokuBunruiCdTo_Validating);
            // 
            // txtKamokuBunruiCdFr
            // 
            this.txtKamokuBunruiCdFr.AutoSizeFromLength = true;
            this.txtKamokuBunruiCdFr.DisplayLength = null;
            this.txtKamokuBunruiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKamokuBunruiCdFr.Location = new System.Drawing.Point(155, 8);
            this.txtKamokuBunruiCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtKamokuBunruiCdFr.MaxLength = 5;
            this.txtKamokuBunruiCdFr.Name = "txtKamokuBunruiCdFr";
            this.txtKamokuBunruiCdFr.Size = new System.Drawing.Size(63, 23);
            this.txtKamokuBunruiCdFr.TabIndex = 0;
            this.txtKamokuBunruiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKamokuBunruiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKamokuBunruiCdFr_Validating);
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblCodeBet1.Location = new System.Drawing.Point(466, 7);
            this.lblCodeBet1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(24, 24);
            this.lblCodeBet1.TabIndex = 2;
            this.lblCodeBet1.Tag = "CHANGE";
            this.lblCodeBet1.Text = "～";
            this.lblCodeBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCdTo
            // 
            this.lblKanjoKamokuCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoKamokuCdTo.Location = new System.Drawing.Point(566, 6);
            this.lblKanjoKamokuCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuCdTo.Name = "lblKanjoKamokuCdTo";
            this.lblKanjoKamokuCdTo.Size = new System.Drawing.Size(240, 24);
            this.lblKanjoKamokuCdTo.TabIndex = 4;
            this.lblKanjoKamokuCdTo.Tag = "DISPNAME";
            this.lblKanjoKamokuCdTo.Text = "最　後";
            this.lblKanjoKamokuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCdFr
            // 
            this.lblKanjoKamokuCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoKamokuCdFr.Location = new System.Drawing.Point(223, 6);
            this.lblKanjoKamokuCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuCdFr.Name = "lblKanjoKamokuCdFr";
            this.lblKanjoKamokuCdFr.Size = new System.Drawing.Size(240, 24);
            this.lblKanjoKamokuCdFr.TabIndex = 1;
            this.lblKanjoKamokuCdFr.Tag = "DISPNAME";
            this.lblKanjoKamokuCdFr.Text = "先　頭";
            this.lblKanjoKamokuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCdTo
            // 
            this.txtKanjoKamokuCdTo.AutoSizeFromLength = true;
            this.txtKanjoKamokuCdTo.DisplayLength = null;
            this.txtKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKanjoKamokuCdTo.Location = new System.Drawing.Point(498, 7);
            this.txtKanjoKamokuCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamokuCdTo.MaxLength = 6;
            this.txtKanjoKamokuCdTo.Name = "txtKanjoKamokuCdTo";
            this.txtKanjoKamokuCdTo.Size = new System.Drawing.Size(63, 23);
            this.txtKanjoKamokuCdTo.TabIndex = 3;
            this.txtKanjoKamokuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanjoKamokuCdTo_KeyDown);
            this.txtKanjoKamokuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdTo_Validating);
            // 
            // txtKanjoKamokuCdFr
            // 
            this.txtKanjoKamokuCdFr.AutoSizeFromLength = true;
            this.txtKanjoKamokuCdFr.DisplayLength = null;
            this.txtKanjoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKanjoKamokuCdFr.Location = new System.Drawing.Point(155, 7);
            this.txtKanjoKamokuCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamokuCdFr.MaxLength = 6;
            this.txtKanjoKamokuCdFr.Name = "txtKanjoKamokuCdFr";
            this.txtKanjoKamokuCdFr.Size = new System.Drawing.Size(63, 23);
            this.txtKanjoKamokuCdFr.TabIndex = 0;
            this.txtKanjoKamokuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdFr_Validating);
            // 
            // lblCodeBet2
            // 
            this.lblCodeBet2.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblCodeBet2.Location = new System.Drawing.Point(466, 6);
            this.lblCodeBet2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet2.Name = "lblCodeBet2";
            this.lblCodeBet2.Size = new System.Drawing.Size(24, 24);
            this.lblCodeBet2.TabIndex = 2;
            this.lblCodeBet2.Tag = "CHANGE";
            this.lblCodeBet2.Text = "～";
            this.lblCodeBet2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(836, 90);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblKanjoKamokuCdTo);
            this.fsiPanel2.Controls.Add(this.lblKanjoKamokuCdFr);
            this.fsiPanel2.Controls.Add(this.txtKanjoKamokuCdFr);
            this.fsiPanel2.Controls.Add(this.txtKanjoKamokuCdTo);
            this.fsiPanel2.Controls.Add(this.lblCodeBet2);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 48);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(828, 38);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(828, 38);
            this.label2.TabIndex = 2;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "勘定科目コード範囲";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.lblKamokuBunruiCdTo);
            this.fsiPanel1.Controls.Add(this.txtKamokuBunruiCdFr);
            this.fsiPanel1.Controls.Add(this.lblKamokuBunruiCdFr);
            this.fsiPanel1.Controls.Add(this.lblCodeBet1);
            this.fsiPanel1.Controls.Add(this.txtKamokuBunruiCdTo);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(828, 37);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(828, 37);
            this.label1.TabIndex = 2;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "科目分類コード範囲";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMCM1035
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 278);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1035";
            this.ShowFButton = true;
            this.Text = "勘定科目の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblKamokuBunruiCdTo;
        private System.Windows.Forms.Label lblKamokuBunruiCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiCdFr;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.Label lblKanjoKamokuCdTo;
        private System.Windows.Forms.Label lblKanjoKamokuCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCdFr;
        private System.Windows.Forms.Label lblCodeBet2;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel2;
		private System.Windows.Forms.Label label2;
		private common.FsiPanel fsiPanel1;
		private System.Windows.Forms.Label label1;
	}
}