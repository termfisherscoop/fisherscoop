﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmdr1041
{
    /// <summary>
    /// 現金出納帳(ZMDR1041)
    /// </summary>
    public partial class ZMDR1041 : BasePgForm
    {
        #region 構造体
    　　/// <summary>
    　　/// 印刷ワークテーブルのデータ構造体
    　　/// </summary>
    　　struct NumVals
    　　{
        　　public decimal SHUNYU;
        　　public decimal SHIHARAI;

        　　public void Clear()
        　　{
            　　SHUNYU = 0;
            　　SHIHARAI = 0;
        　　}
    　　}
    　　#endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDR1041()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }

            // 日付の表示
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblGengo.Text = jpDate[0];
            txtYear.Text = jpDate[2];
            txtMonth.Text = jpDate[3];
            txtDay.Text = jpDate[4];

            //rdoTujoShiwake.Checked = true;
            try
            {
                string val = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1041", "Setting", "ShiwakeKbn"));
                switch (val)
                {
                    case "0":
                        rdoTujoShiwake.Checked = true;
                        break;
                    case "1":
                        rdoKessanShiwake.Checked = true;
                        break;
                    case "2":
                        rdoZenShiwake.Checked = true;
                        break;
                    default:
                        rdoTujoShiwake.Checked = true;
                        break;
                }
            }
            catch (Exception)
            {
                rdoTujoShiwake.Checked = true;
            }

            // 初期フォーカス
            txtYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYear":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJp();
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            // 科目設定画面を起動
            using (ZMDR1042 frm = new ZMDR1042())
            {
                DialogResult result = frm.ShowDialog(this);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            using (PrintSettingForm psForm = new PrintSettingForm(new string[1] { "ZMDR10411R" }))
            {
                psForm.ShowDialog();
            }
        }
        #endregion

        #region イベント

        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
            }
        }
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYear.Text, this.txtYear.MaxLength))
            {
                e.Cancel = true;
                this.txtYear.SelectAll();
            }
            else
            {
                this.txtYear.Text = Util.ToString(IsValid.SetYear(this.txtYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtMonth.SelectAll();
            }
            else
            {
                this.txtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtMonth.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDay.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                this.txtDay.Text = Util.ToString(IsValid.SetDay(this.txtDay.Text));
                CheckJp();
                SetJp();

                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(日)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter && this.Flg)
            //{
            //    // Enter処理を無効化
            //    this._dtFlg = false;

            //    // 全項目を再度入力値チェック
            //    if (!ValidateAll())
            //    {
            //        // エラーありの場合ここで処理終了
            //        return;
            //    }

            //    if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            //    {
            //        // ﾌﾟﾚﾋﾞｭｰ処理
            //        DoPrint(true);
            //    }
            //    else
            //    {
            //        this.txtDay.Focus();
            //    }
            //}
        }

        /// <summary>
        /// 仕訳種類Enter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoTujoShiwake_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //// Enter処理を無効化
                //this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.rdoTujoShiwake.Focus();
                }
            }
        }

        /// <summary>
        /// 仕訳種類Enter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoKessanShiwake_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //// Enter処理を無効化
                //this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.rdoKessanShiwake.Focus();
                }
            }
        }

        /// <summary>
        /// 仕訳種類Enter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoZenShiwake_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //// Enter処理を無効化
                //this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.rdoZenShiwake.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDay.Text) > lastDayInMonth)
            {
                this.txtDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengo.Text,
                                  this.txtYear.Text,
                                  this.txtMonth.Text,
                                  this.txtDay.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtYear.Text, this.txtYear.MaxLength))
            {
                this.txtYear.Focus();
                this.txtYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                this.txtMonth.Focus();
                this.txtMonth.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                this.txtDay.Focus();
                this.txtDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJp();
            // 年月日(自)の正しい和暦への変換処理
            SetJp();

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMDR10411R rpt = new ZMDR10411R(dtOutput);

                    // 捺印欄名
                    List<string> nmList = new List<string>() { "組合長", "課長", "係長", "照査", "起票" };
                    try
                    {
                        string[] nml;
                        nml = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1041", "Setting", "nameList")).Split(',');
                        nmList = new List<string>(nml);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message); ;
                    }
                    rpt.nmList = nmList;

                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            // 支所コード
            int shishoCd = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));
            if (shishoCd != 0)
                Sql.Append(" AND SHISHO_CD = @SHISHO_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_SHISHO",
                "KAISHA_CD = @KAISHA_CD " + Sql.ToString(),
                "SHISHO_CD",
                dpc);

            foreach (DataRow dr in dt.Rows)
            {
                MakeWkDataMain(Util.ToInt(Util.ToString(dr["SHISHO_CD"])));
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_ZM_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_ZM_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private void MakeWkDataMain(int shishoCd)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            // 日付を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);

            // 日付を和暦で保持
            string[] tmpjpDate = Util.ConvJpDate(tmpDate, this.Dba);

            NumVals GOKEI = new NumVals();

            // 決算区分
            int kessanKbn = 2;
            if (this.rdoTujoShiwake.Checked == true)
                kessanKbn = 0;
            else if (this.rdoKessanShiwake.Checked == true)
                kessanKbn = 1;

            #region ループ準備処理
            //int dbSORT = 1;
            int dbSORT = (shishoCd * 1000000) + 1;
            int i = 0; // ループ用カウント変数
            int k = 1;// 改ページ用カウント変数
            int taishakuKubun = 1; // 貸借区分
            int lastNum = i; // 最終残高表示用変数
            int flag = 0;  // 空行表示判断用変数

            #endregion
            // 繰越残高
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            Sql.Append(" SELECT");
            Sql.Append("     A.KANJO_KAMOKU_CD  AS KAMOKU_CD,");
            Sql.Append("     MAX(B.KANJO_KAMOKU_NM) AS KAMOKU_NM,");
            Sql.Append("     MAX(B.TAISHAKU_KUBUN)   AS TAISHAKU_KUBUN,");
            Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE < CAST(@DENPYO_DATE AS DATETIME) THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN A.ZEIKOMI_KINGAKU ELSE A.ZEIKOMI_KINGAKU * -1 END ELSE 0 END) AS KURIKOSHI_ZANDAKA");
            Sql.Append(" FROM");
            Sql.Append("     TB_ZM_SHIWAKE_MEISAI AS A ");
            Sql.Append(" LEFT OUTER JOIN ");
            Sql.Append("     TB_ZM_KANJO_KAMOKU AS B ");
            Sql.Append("     ON (A.KAISHA_CD = B.KAISHA_CD) AND (A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append("     A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            //Sql.Append("     A.BUMON_CD BETWEEN 0 AND 0 AND");
            //Sql.Append("     A.KANJO_KAMOKU_CD = 100 AND");
            Sql.Append("     A.KANJO_KAMOKU_CD = @SETTEI_CD AND");
            //Sql.Append("     A.KOJI_CD BETWEEN 0 AND 0 AND");
            Sql.Append("     A.DENPYO_DATE <= CAST(@DENPYO_DATE AS DATETIME)  AND");
            if (kessanKbn < 2)
                Sql.Append("     A.KESSAN_KUBUN = @KESSAN_KUBUN AND");
            //Sql.Append("     A.MEISAI_KUBUN <= 0 AND");
            Sql.Append("     A.MEISAI_KUBUN <= 0 ");
            //Sql.Append("     A.KESSAN_KUBUN = 0");
            Sql.Append(" GROUP BY");
            Sql.Append("     A.KANJO_KAMOKU_CD");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.KANJO_KAMOKU_CD");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            // 検索する日付をセット
            dpc.SetParam("@DENPYO_DATE", SqlDbType.VarChar, 10, tmpDate.Date.ToString("yyyy/MM/dd"));
            // 設定された科目コード
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1041", "Setting", "KamokuCd"));
            // 決算区分
            if (kessanKbn < 2)
                dpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 4, kessanKbn);

            DataTable dtZandaka = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            // 伝票番号、金額
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            Sql.Append(" SELECT");
            Sql.Append("     A.SHISHO_CD,");
            Sql.Append("     A.DENPYO_BANGO       AS DENPYO_BANGO,");
            Sql.Append("     A.GYO_BANGO         AS GYO_BANGO,");
            Sql.Append("     A.TAISHAKU_KUBUN       AS TAISHAKU_KUBUN,");
            Sql.Append("     A.DENPYO_DATE       AS DENPYO_DATE,");
            Sql.Append("     A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD,");
            Sql.Append("     A.KANJO_KAMOKU_NM     AS KANJO_KAMOKU_NM,");
            Sql.Append("     A.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD,");
            Sql.Append("     A.HOJO_KAMOKU_NM     AS HOJO_KAMOKU_NM,");
            Sql.Append("     A.BUMON_CD     AS BUMON_CD,");
            Sql.Append("     A.BUMON_NM         AS BUMON_NM,");
            //Sql.Append("     A.KOJI_CD     AS KOJI_CD,");
            //Sql.Append("     A.KOJI_NM       AS KOJI_NM,");
            Sql.Append("     A.TEKIYO_CD     AS TEKIYO_CD,");
            Sql.Append("     A.TEKIYO           AS TEKIYO,");
            Sql.Append("     A.SHOHYO_BANGO       AS SHOHYO_BANGO,");
            Sql.Append("     A.ZEIKOMI_KINGAKU     AS ZEIKOMI_KINGAKU,");
            Sql.Append("     A.ZEINUKI_KINGAKU     AS ZEINUKI_KINGAKU,");
            Sql.Append("     A.SHOHIZEI_KINGAKU     AS SHOHIZEI_KINGAKU");
            Sql.Append(" FROM");
            Sql.Append("     VI_ZM_SHIWAKE_MEISAI AS A");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append("     A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            //Sql.Append("     A.BUMON_CD BETWEEN 0 AND 0 AND");
            //Sql.Append("     A.KANJO_KAMOKU_CD = 100 AND");
            Sql.Append("     A.KANJO_KAMOKU_CD = @SETTEI_CD AND");
            //Sql.Append("     A.KOJI_CD BETWEEN 0 AND 0 AND");
            Sql.Append("     A.DENPYO_DATE  = CAST(@DENPYO_DATE AS DATETIME) AND");
            Sql.Append("     A.MEISAI_KUBUN <= 0 AND");
            if (kessanKbn < 2)
                Sql.Append("     A.KESSAN_KUBUN = @KESSAN_KUBUN AND");
            //Sql.Append("     A.DENPYO_KUBUN = 1  AND");
            Sql.Append("     A.DENPYO_KUBUN = 1 ");
            //Sql.Append("     A.KESSAN_KUBUN = 0");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.DENPYO_DATE,");
            Sql.Append("     A.DENPYO_BANGO,");
            Sql.Append("     A.GYO_BANGO");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            // 検索する日付をセット
            dpc.SetParam("@DENPYO_DATE", SqlDbType.VarChar, 10, tmpDate.Date.ToString("yyyy/MM/dd"));
            // 設定された科目コード
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 6, this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1041", "Setting", "KamokuCd"));
            // 決算区分
            if (kessanKbn < 2)
                dpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 4, kessanKbn);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count > i)
            {
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    // 次頁繰越の行を追加
                    if (k % 22 == 0)
                    {
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_ZM_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dpc.SetParam("@SORT", SqlDbType.Decimal, 9, dbSORT);
                        dbSORT++;
                        #endregion

                        #region 頁合計
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpjpDate[5]);
                        // データを設定
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "　次頁へ繰越　");
                        // 繰越残高が0の場合　空白を表示
                        if (Util.ToDecimal(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]) == 0)
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, 0);
                        }
                        else
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]));
                        }
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHUNYU));
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHIHARAI));
                        dpc.SetParam("@ITEM12", SqlDbType.Decimal, 4, shishoCd);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 前頁より繰越
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_ZM_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(") ");
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dpc.SetParam("@SORT", SqlDbType.Decimal, 9, dbSORT);
                        dbSORT++;
                        #endregion

                        #region 小計データ登録
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpjpDate[5]);
                        // データを設定
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "　前頁より繰越　");
                        // 繰越残高が0の場合　空白を表示
                        if (Util.ToDecimal(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]) == 0)
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, 0);
                        }
                        else
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]));
                        }
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHUNYU));
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHIHARAI));
                        dpc.SetParam("@ITEM12", SqlDbType.Decimal, 4, shishoCd);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        k = 2;
                    }
                    else if ((k % 22 == 21 && dtMainLoop.Rows.Count == i) || (k % 22 == 20 && dtMainLoop.Rows.Count == i))
                    {
                        while (k <= 21)
                        {
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_ZM_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM09");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM09");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(") ");
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dpc.SetParam("@SORT", SqlDbType.Decimal, 9, dbSORT);
                            dbSORT++;
                            #endregion

                            #region 空行データ登録
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpjpDate[5]);
                            // 繰越残高が0の場合　空白を表示
                            if (Util.ToDecimal(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]) == 0)
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, 0);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]));
                            }
                            dpc.SetParam("@ITEM12", SqlDbType.Decimal, 4, shishoCd);
                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            k++;
                            i++;
                            #endregion
                        }

                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_ZM_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dpc.SetParam("@SORT", SqlDbType.Decimal, 9, dbSORT);
                        dbSORT++;
                        #endregion

                        #region 頁合計
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpjpDate[5]);
                        // データを設定
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "　次頁へ繰越　");
                        // 繰越残高が0の場合　空白を表示
                        if (Util.ToDecimal(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]) == 0)
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, 0);
                        }
                        else
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]));
                        }
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHUNYU));
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHIHARAI));
                        dpc.SetParam("@ITEM12", SqlDbType.Decimal, 4, shishoCd);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 前頁より繰越
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_ZM_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(") ");
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dpc.SetParam("@SORT", SqlDbType.Decimal, 9, dbSORT);
                        dbSORT++;
                        #endregion

                        #region 頁合計
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpjpDate[5]);
                        // データを設定
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "　前頁より繰越　");
                        // 繰越残高が0の場合　空白を表示
                        if (Util.ToDecimal(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]) == 0)
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, 0);
                        }
                        else
                        {
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]));
                        }
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHUNYU));
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHIHARAI));
                        dpc.SetParam("@ITEM12", SqlDbType.Decimal, 4, shishoCd);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion
                        k = 1;
                    }

                    // 相手勘定科目データ
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append(" SELECT");
                    Sql.Append("     A.SHISHO_CD,");
                    Sql.Append("     A.DENPYO_BANGO       AS DENPYO_BANGO,");
                    Sql.Append("     A.GYO_BANGO         AS GYO_BANGO,");
                    Sql.Append("     A.TAISHAKU_KUBUN       AS TAISHAKU_KUBUN,");
                    Sql.Append("     A.DENPYO_DATE       AS DENPYO_DATE,");
                    Sql.Append("     A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD,");
                    Sql.Append("     A.KANJO_KAMOKU_NM     AS KANJO_KAMOKU_NM,");
                    Sql.Append("     A.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD,");
                    Sql.Append("     A.HOJO_KAMOKU_NM     AS HOJO_KAMOKU_NM,");
                    //Sql.Append("     A.KOJI_CD     AS KOJI_CD,");
                    //Sql.Append("     A.KOJI_NM       AS KOJI_NM,");
                    Sql.Append("     A.ZEIKOMI_KINGAKU       AS ZEIKOMI_KINGAKU");
                    Sql.Append(" FROM");
                    Sql.Append("     VI_ZM_SHIWAKE_MEISAI AS A");
                    Sql.Append(" WHERE");
                    Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
                    Sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");
                    Sql.Append("     A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                    Sql.Append("     A.DENPYO_BANGO = @DENPYO_BANGO AND");
                    Sql.Append("     A.GYO_BANGO = @GYO_BANGO AND");
                    Sql.Append("     A.TAISHAKU_KUBUN = @TAISHAKU_KUBUN AND");
                    Sql.Append("     A.DENPYO_DATE  = CAST(@DENPYO_DATE AS DATETIME)  AND");
                    Sql.Append("     A.MEISAI_KUBUN <= 0 AND");
                    Sql.Append("     A.DENPYO_KUBUN = 1  AND");
                    Sql.Append("     A.KESSAN_KUBUN = 0");
                    Sql.Append(" ORDER BY");
                    Sql.Append("     A.DENPYO_DATE,");
                    Sql.Append("     A.GYO_BANGO");

                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                    // 検索する日付をセット
                    dpc.SetParam("@DENPYO_DATE", SqlDbType.VarChar, 10, tmpDate.Date.ToString("yyyy/MM/dd"));
                    // 検索する伝票番号をセット
                    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, dr["DENPYO_BANGO"]);
                    // 検索する行番号をセット
                    dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 6, dr["GYO_BANGO"]);
                    // 検索する貸借区分をセット
                    if (Util.ToInt(dr["TAISHAKU_KUBUN"]) == 1)
                    {
                        taishakuKubun = 2;
                    }
                    else
                    {
                        taishakuKubun = 1;
                    }
                    dpc.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, taishakuKubun);

                    DataTable dtDate = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                    // 印刷ワークテーブルに登録
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_ZM_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dpc.SetParam("@SORT", SqlDbType.Decimal, 9, dbSORT);
                    dbSORT++;
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpjpDate[5]);
                    // データを設定
                    if (dtDate.Rows.Count == 0 || Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]) != Util.ToDecimal(dtDate.Rows[0]["ZEIKOMI_KINGAKU"]))
                    {
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "  諸    口");
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtDate.Rows[0]["KANJO_KAMOKU_CD"]);
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtDate.Rows[0]["KANJO_KAMOKU_NM"]);
                        // 補助科目コードが０の場合空白を表示
                        if (Util.ToDecimal(dr["HOJO_KAMOKU_CD"]) == 0)
                        {
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtDate.Rows[0]["HOJO_KAMOKU_CD"]);
                        }

                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtDate.Rows[0]["HOJO_KAMOKU_NM"]);
                    }
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["TEKIYO"]);
                    if (Util.ToString(dr["TAISHAKU_KUBUN"]) == "1")
                    {
                        // 税込金額が0の場合　空白を表示
                        if (Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]) == 0)
                        {
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEIKOMI_KINGAKU"]));
                            GOKEI.SHUNYU += Util.ToDecimal(Util.FormatNum(dr["ZEIKOMI_KINGAKU"]));
                        }
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                        // 税込金額が0の場合　空白を表示
                        if (Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]) == 0)
                        {
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEIKOMI_KINGAKU"]));
                            GOKEI.SHIHARAI += Util.ToDecimal(Util.FormatNum(dr["ZEIKOMI_KINGAKU"]));
                        }
                    }

                    // 繰越残高が0の場合　空白を表示
                    if (Util.ToDecimal(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]) == 0)
                    {
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, 0);
                    }
                    else
                    {
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]));
                    }
                    dpc.SetParam("@ITEM12", SqlDbType.Decimal, 4, shishoCd);

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                    k++;
                    i++;

                    // 空行表示準備
                    if (Util.ToInt(dtMainLoop.Rows.Count) == i)
                    {
                        flag = 1;
                    }

                    if (flag == 1)
                    {
                        // 次頁繰越の行を追加
                        if (k % 22 == 0)
                        {
                            #region 次頁へ繰越
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_ZM_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM09");
                            Sql.Append(" ,ITEM10");
                            Sql.Append(" ,ITEM11");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM09");
                            Sql.Append(" ,@ITEM10");
                            Sql.Append(" ,@ITEM11");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(") ");

                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dpc.SetParam("@SORT", SqlDbType.Decimal, 9, dbSORT);
                            dbSORT++;
                            #endregion

                            #region 頁合計
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpjpDate[5]);
                            // データを設定
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "　次頁へ繰越　");
                            // 繰越残高が0の場合　空白を表示
                            if (Util.ToDecimal(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]) == 0)
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, 0);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]));
                            }
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHUNYU));
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHIHARAI));
                            dpc.SetParam("@ITEM12", SqlDbType.Decimal, 4, shishoCd);

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            #endregion

                            #region 前頁より繰越
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_ZM_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM09");
                            Sql.Append(" ,ITEM10");
                            Sql.Append(" ,ITEM11");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM09");
                            Sql.Append(" ,@ITEM10");
                            Sql.Append(" ,@ITEM11");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(") ");
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dpc.SetParam("@SORT", SqlDbType.Decimal, 9, dbSORT);
                            dbSORT++;
                            #endregion

                            #region 頁合計
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpjpDate[5]);
                            // データを設定
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "　前頁より繰越　");
                            // 繰越残高が0の場合　空白を表示
                            if (Util.ToDecimal(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]) == 0)
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, 0);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]));
                            }
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHUNYU));
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SHIHARAI));
                            dpc.SetParam("@ITEM12", SqlDbType.Decimal, 4, shishoCd);

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            #endregion

                            k = 2;
                        }

                        while (k <= 18)
                        {
                            #region 空行インサート
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_ZM_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM09");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM09");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(") ");
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dpc.SetParam("@SORT", SqlDbType.Decimal, 9, dbSORT);
                            dbSORT++;
                            #endregion

                            #region 空行データ登録
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpjpDate[5]);
                            // 繰越残高が0の場合　空白を表示
                            if (Util.ToDecimal(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]) == 0)
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, 0);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtZandaka.Rows[0]["KURIKOSHI_ZANDAKA"]));
                            }
                            dpc.SetParam("@ITEM12", SqlDbType.Decimal, 4, shishoCd);
                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            k++;
                            i++;
                            #endregion

                        }
                    }
                }
            }
        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }

        /// <summary>
        /// 会計年度内日付に変換
        /// </summary>
        private DateTime FixNendoDate(DateTime date)
        {
            DateTime dateFr = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            DateTime dateTo = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }
        #endregion
    }
}

