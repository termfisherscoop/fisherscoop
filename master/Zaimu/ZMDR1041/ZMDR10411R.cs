﻿using System;
using System.Data;
using System.Collections.Generic;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmdr1041
{
    /// <summary>
    /// ZAMR1071R の概要の説明です。
    /// </summary>
    public partial class ZMDR10411R : BaseReport
    {
        public List<string> nmList;

        public ZMDR10411R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void reportFooter1_Format(object sender, EventArgs e)
        {
            if (this.テキスト76.Text == "0")
            {
                this.テキスト76.Text = "";
            }

            if (this.テキスト48.Text == "0")
            {
                this.テキスト48.Text = "";
            }

            if (this.テキスト74.Text == "0")
            {
                this.テキスト74.Text = "";
            }

            // 本日現金
            this.テキスト80.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.テキスト48.Text) + Util.ToDecimal(this.テキスト78.Text) - Util.ToDecimal(this.テキスト76.Text)));

            // 合計収入
            this.テキスト77.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.テキスト48.Text) + Util.ToDecimal(this.テキスト74.Text)));

            // 合計支払
            this.テキスト79.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.テキスト48.Text) + Util.ToDecimal(this.テキスト74.Text)));
        }

        private void gfShishoCd_Format(object sender, EventArgs e)
        {
            if (this.textBox5.Text == "0")
            {
                this.textBox5.Text = "";
            }

            if (this.textBox3.Text == "0")
            {
                this.textBox3.Text = "";
            }

            if (this.textBox4.Text == "0")
            {
                this.textBox4.Text = "";
            }

            // 本日現金
            this.textBox9.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.textBox3.Text) + Util.ToDecimal(this.textBox7.Text) - Util.ToDecimal(this.textBox5.Text)));

            // 合計収入
            this.textBox6.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.textBox3.Text) + Util.ToDecimal(this.textBox4.Text)));

            // 合計支払
            this.textBox8.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.textBox3.Text) + Util.ToDecimal(this.textBox4.Text)));
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            //this.label1.Text = "組合長";
            //this.label2.Text = "課  長\r\n";
            //this.ラベル57.Text = "係  長";
            //this.ラベル58.Text = "照  査";
            //this.ラベル59.Text = "起  票";
            this.label1.Text = nmList[0];
            this.label2.Text = nmList[1];
            this.ラベル57.Text = nmList[2];
            this.ラベル58.Text = nmList[3];
            this.ラベル59.Text = nmList[4];
        }
    }
}
