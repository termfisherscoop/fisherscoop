﻿namespace jp.co.fsi.zm.zmdr1041
{
    /// <summary>
    /// ZAMR1071R の概要の説明です。
    /// </summary>
    partial class ZMDR10411R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMDR10411R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ラベル59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ボックス80 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.直線14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線81 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト004 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト002 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト005 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト003 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト006 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト007 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト008 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線84 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.テキスト48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル59,
            this.ラベル58,
            this.ラベル57,
            this.label1,
            this.label2,
            this.ラベル0,
            this.直線1,
            this.テキスト001,
            this.直線53,
            this.直線55,
            this.直線56,
            this.直線62,
            this.直線64,
            this.ボックス80,
            this.直線14,
            this.直線16,
            this.直線17,
            this.ラベル19,
            this.ラベル21,
            this.ラベル22,
            this.ラベル23,
            this.直線81,
            this.line1,
            this.line2});
            this.pageHeader.Height = 0.875F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // ラベル59
            // 
            this.ラベル59.CharacterSpacing = 10F;
            this.ラベル59.Height = 0.196063F;
            this.ラベル59.HyperLink = null;
            this.ラベル59.Left = 5.533071F;
            this.ラベル59.Name = "ラベル59";
            this.ラベル59.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: bottom; ddo-char-set: 128";
            this.ラベル59.Tag = "";
            this.ラベル59.Text = "起票";
            this.ラベル59.Top = 0.04370079F;
            this.ラベル59.Width = 0.7661419F;
            // 
            // ラベル58
            // 
            this.ラベル58.CharacterSpacing = 10F;
            this.ラベル58.Height = 0.196063F;
            this.ラベル58.HyperLink = null;
            this.ラベル58.Left = 4.72441F;
            this.ラベル58.Name = "ラベル58";
            this.ラベル58.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: bottom; ddo-char-set: 128";
            this.ラベル58.Tag = "";
            this.ラベル58.Text = "照査";
            this.ラベル58.Top = 0.04370079F;
            this.ラベル58.Width = 0.7874017F;
            // 
            // ラベル57
            // 
            this.ラベル57.CharacterSpacing = 10F;
            this.ラベル57.Height = 0.196063F;
            this.ラベル57.HyperLink = null;
            this.ラベル57.Left = 3.937008F;
            this.ラベル57.Name = "ラベル57";
            this.ラベル57.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: bottom; ddo-char-set: 128";
            this.ラベル57.Tag = "";
            this.ラベル57.Text = "係長";
            this.ラベル57.Top = 0.04370079F;
            this.ラベル57.Width = 0.7874014F;
            // 
            // label1
            // 
            this.label1.Height = 0.196063F;
            this.label1.HyperLink = null;
            this.label1.Left = 2.362205F;
            this.label1.Name = "label1";
            this.label1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: bottom; ddo-char-set: 128";
            this.label1.Tag = "";
            this.label1.Text = "組合長";
            this.label1.Top = 0.05393701F;
            this.label1.Width = 0.7874017F;
            // 
            // label2
            // 
            this.label2.CharacterSpacing = 10F;
            this.label2.Height = 0.196063F;
            this.label2.HyperLink = null;
            this.label2.Left = 3.155118F;
            this.label2.Name = "label2";
            this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: bottom; ddo-char-set: 128";
            this.label2.Tag = "";
            this.label2.Text = "課長\r\n";
            this.label2.Top = 0.05393701F;
            this.label2.Width = 0.7874017F;
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.25F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 0.08784755F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "金　銭　出　納　帳";
            this.ラベル0.Top = 0F;
            this.ラベル0.Width = 2.364583F;
            // 
            // 直線1
            // 
            this.直線1.Height = 0.0001804978F;
            this.直線1.Left = 0F;
            this.直線1.LineWeight = 1F;
            this.直線1.Name = "直線1";
            this.直線1.Tag = "";
            this.直線1.Top = 0.2395833F;
            this.直線1.Width = 2.165354F;
            this.直線1.X1 = 0F;
            this.直線1.X2 = 2.165354F;
            this.直線1.Y1 = 0.2397638F;
            this.直線1.Y2 = 0.2395833F;
            // 
            // テキスト001
            // 
            this.テキスト001.DataField = "ITEM01";
            this.テキスト001.Height = 0.15625F;
            this.テキスト001.Left = 0.1607642F;
            this.テキスト001.Name = "テキスト001";
            this.テキスト001.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト001.Tag = "";
            this.テキスト001.Text = "ITEM01";
            this.テキスト001.Top = 0.4479167F;
            this.テキスト001.Width = 1.379167F;
            // 
            // 直線53
            // 
            this.直線53.Height = 0F;
            this.直線53.Left = 2.362205F;
            this.直線53.LineWeight = 1F;
            this.直線53.Name = "直線53";
            this.直線53.Tag = "";
            this.直線53.Top = 0.2395833F;
            this.直線53.Width = 3.942309F;
            this.直線53.X1 = 2.362205F;
            this.直線53.X2 = 6.304514F;
            this.直線53.Y1 = 0.2395833F;
            this.直線53.Y2 = 0.2395833F;
            // 
            // 直線55
            // 
            this.直線55.Height = 0.6326498F;
            this.直線55.Left = 4.72441F;
            this.直線55.LineWeight = 1F;
            this.直線55.Name = "直線55";
            this.直線55.Tag = "";
            this.直線55.Top = 0.04097222F;
            this.直線55.Width = 0F;
            this.直線55.X1 = 4.72441F;
            this.直線55.X2 = 4.72441F;
            this.直線55.Y1 = 0.04097222F;
            this.直線55.Y2 = 0.6736221F;
            // 
            // 直線56
            // 
            this.直線56.Height = 0.6326498F;
            this.直線56.Left = 5.511811F;
            this.直線56.LineWeight = 1F;
            this.直線56.Name = "直線56";
            this.直線56.Tag = "";
            this.直線56.Top = 0.04097222F;
            this.直線56.Width = 0F;
            this.直線56.X1 = 5.511811F;
            this.直線56.X2 = 5.511811F;
            this.直線56.Y1 = 0.04097222F;
            this.直線56.Y2 = 0.6736221F;
            // 
            // 直線62
            // 
            this.直線62.Height = 0.634252F;
            this.直線62.Left = 6.299213F;
            this.直線62.LineWeight = 0F;
            this.直線62.Name = "直線62";
            this.直線62.Tag = "";
            this.直線62.Top = 0.03937008F;
            this.直線62.Width = 0F;
            this.直線62.X1 = 6.299213F;
            this.直線62.X2 = 6.299213F;
            this.直線62.Y1 = 0.03937008F;
            this.直線62.Y2 = 0.6736221F;
            // 
            // 直線64
            // 
            this.直線64.Height = 0F;
            this.直線64.Left = 2.362205F;
            this.直線64.LineWeight = 1F;
            this.直線64.Name = "直線64";
            this.直線64.Tag = "";
            this.直線64.Top = 0.04097222F;
            this.直線64.Width = 3.942309F;
            this.直線64.X1 = 2.362205F;
            this.直線64.X2 = 6.304514F;
            this.直線64.Y1 = 0.04097222F;
            this.直線64.Y2 = 0.04097222F;
            // 
            // ボックス80
            // 
            this.ボックス80.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス80.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス80.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス80.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス80.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス80.Height = 0.2048611F;
            this.ボックス80.Left = 0.004514217F;
            this.ボックス80.Name = "ボックス80";
            this.ボックス80.RoundingRadius = 9.999999F;
            this.ボックス80.Tag = "";
            this.ボックス80.Top = 0.6701389F;
            this.ボックス80.Width = 6.301389F;
            // 
            // 直線14
            // 
            this.直線14.Height = 0.1972222F;
            this.直線14.Left = 1.563542F;
            this.直線14.LineWeight = 1F;
            this.直線14.Name = "直線14";
            this.直線14.Tag = "";
            this.直線14.Top = 0.6701389F;
            this.直線14.Width = 0F;
            this.直線14.X1 = 1.563542F;
            this.直線14.X2 = 1.563542F;
            this.直線14.Y1 = 0.6701389F;
            this.直線14.Y2 = 0.8673611F;
            // 
            // 直線16
            // 
            this.直線16.Height = 0.1972222F;
            this.直線16.Left = 3.942014F;
            this.直線16.LineWeight = 1F;
            this.直線16.Name = "直線16";
            this.直線16.Tag = "";
            this.直線16.Top = 0.6701389F;
            this.直線16.Width = 0F;
            this.直線16.X1 = 3.942014F;
            this.直線16.X2 = 3.942014F;
            this.直線16.Y1 = 0.6701389F;
            this.直線16.Y2 = 0.8673611F;
            // 
            // 直線17
            // 
            this.直線17.Height = 0.1972222F;
            this.直線17.Left = 5.118403F;
            this.直線17.LineWeight = 1F;
            this.直線17.Name = "直線17";
            this.直線17.Tag = "";
            this.直線17.Top = 0.6701389F;
            this.直線17.Width = 0F;
            this.直線17.X1 = 5.118403F;
            this.直線17.X2 = 5.118403F;
            this.直線17.Y1 = 0.6701389F;
            this.直線17.Y2 = 0.8673611F;
            // 
            // ラベル19
            // 
            this.ラベル19.Height = 0.2047244F;
            this.ラベル19.HyperLink = null;
            this.ラベル19.Left = 0F;
            this.ラベル19.Name = "ラベル19";
            this.ラベル19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 128";
            this.ラベル19.Tag = "";
            this.ラベル19.Text = "科　　　目";
            this.ラベル19.Top = 0.6700788F;
            this.ラベル19.Width = 1.56378F;
            // 
            // ラベル21
            // 
            this.ラベル21.Height = 0.2047244F;
            this.ラベル21.HyperLink = null;
            this.ラベル21.Left = 1.569291F;
            this.ラベル21.Name = "ラベル21";
            this.ラベル21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 128";
            this.ラベル21.Tag = "";
            this.ラベル21.Text = "摘　　　　　要";
            this.ラベル21.Top = 0.6700788F;
            this.ラベル21.Width = 2.367717F;
            // 
            // ラベル22
            // 
            this.ラベル22.Height = 0.2047244F;
            this.ラベル22.HyperLink = null;
            this.ラベル22.Left = 3.937008F;
            this.ラベル22.Name = "ラベル22";
            this.ラベル22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 128";
            this.ラベル22.Tag = "";
            this.ラベル22.Text = "収入金額";
            this.ラベル22.Top = 0.6700788F;
            this.ラベル22.Width = 1.16063F;
            // 
            // ラベル23
            // 
            this.ラベル23.Height = 0.2047244F;
            this.ラベル23.HyperLink = null;
            this.ラベル23.Left = 5.113398F;
            this.ラベル23.Name = "ラベル23";
            this.ラベル23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 128";
            this.ラベル23.Tag = "";
            this.ラベル23.Text = "支払金額";
            this.ラベル23.Top = 0.6700788F;
            this.ラベル23.Width = 1.191721F;
            // 
            // 直線81
            // 
            this.直線81.Height = 0.6319554F;
            this.直線81.Left = 3.937008F;
            this.直線81.LineWeight = 1F;
            this.直線81.Name = "直線81";
            this.直線81.Tag = "";
            this.直線81.Top = 0.04166667F;
            this.直線81.Width = 0F;
            this.直線81.X1 = 3.937008F;
            this.直線81.X2 = 3.937008F;
            this.直線81.Y1 = 0.04166667F;
            this.直線81.Y2 = 0.6736221F;
            // 
            // line1
            // 
            this.line1.Height = 0.6318898F;
            this.line1.Left = 3.149606F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 0.04173229F;
            this.line1.Width = 0F;
            this.line1.X1 = 3.149606F;
            this.line1.X2 = 3.149606F;
            this.line1.Y1 = 0.04173229F;
            this.line1.Y2 = 0.6736221F;
            // 
            // line2
            // 
            this.line2.Height = 0.6318898F;
            this.line2.Left = 2.362205F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Tag = "";
            this.line2.Top = 0.04173229F;
            this.line2.Width = 0F;
            this.line2.X1 = 2.362205F;
            this.line2.X2 = 2.362205F;
            this.line2.Y1 = 0.04173229F;
            this.line2.Y2 = 0.6736221F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM12";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト004,
            this.テキスト002,
            this.テキスト005,
            this.テキスト003,
            this.直線24,
            this.直線25,
            this.直線26,
            this.直線29,
            this.直線30,
            this.テキスト006,
            this.テキスト007,
            this.テキスト008,
            this.直線84,
            this.textBox1,
            this.textBox2});
            this.detail.Height = 0.4006452F;
            this.detail.Name = "detail";
            // 
            // テキスト004
            // 
            this.テキスト004.DataField = "ITEM04";
            this.テキスト004.Height = 0.15625F;
            this.テキスト004.Left = 0.01574803F;
            this.テキスト004.Name = "テキスト004";
            this.テキスト004.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト004.Tag = "";
            this.テキスト004.Text = "ITEM04";
            this.テキスト004.Top = 0.2223042F;
            this.テキスト004.Width = 0.4031496F;
            // 
            // テキスト002
            // 
            this.テキスト002.DataField = "ITEM02";
            this.テキスト002.Height = 0.15625F;
            this.テキスト002.Left = 0.01574803F;
            this.テキスト002.Name = "テキスト002";
            this.テキスト002.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト002.Tag = "";
            this.テキスト002.Text = "ITEM02";
            this.テキスト002.Top = 0.003937008F;
            this.テキスト002.Width = 0.4031496F;
            // 
            // テキスト005
            // 
            this.テキスト005.CanGrow = false;
            this.テキスト005.DataField = "ITEM05";
            this.テキスト005.Height = 0.15625F;
            this.テキスト005.Left = 0.4094488F;
            this.テキスト005.Name = "テキスト005";
            this.テキスト005.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト005.Tag = "";
            this.テキスト005.Text = "ITEM05";
            this.テキスト005.Top = 0.2223042F;
            this.テキスト005.Width = 1.159843F;
            // 
            // テキスト003
            // 
            this.テキスト003.DataField = "ITEM03";
            this.テキスト003.Height = 0.15625F;
            this.テキスト003.Left = 0.4094489F;
            this.テキスト003.Name = "テキスト003";
            this.テキスト003.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト003.Tag = "";
            this.テキスト003.Text = "ITEM03";
            this.テキスト003.Top = 0.02440945F;
            this.テキスト003.Width = 1.159843F;
            // 
            // 直線24
            // 
            this.直線24.Height = 0.3937008F;
            this.直線24.Left = 0F;
            this.直線24.LineWeight = 1F;
            this.直線24.Name = "直線24";
            this.直線24.Tag = "";
            this.直線24.Top = 0F;
            this.直線24.Width = 0F;
            this.直線24.X1 = 0F;
            this.直線24.X2 = 0F;
            this.直線24.Y1 = 0F;
            this.直線24.Y2 = 0.3937008F;
            // 
            // 直線25
            // 
            this.直線25.Height = 0F;
            this.直線25.Left = 0.005208492F;
            this.直線25.LineWeight = 0F;
            this.直線25.Name = "直線25";
            this.直線25.Tag = "";
            this.直線25.Top = 0.3973042F;
            this.直線25.Width = 6.300001F;
            this.直線25.X1 = 0.005208492F;
            this.直線25.X2 = 6.305209F;
            this.直線25.Y1 = 0.3973042F;
            this.直線25.Y2 = 0.3973042F;
            // 
            // 直線26
            // 
            this.直線26.Height = 0.39375F;
            this.直線26.Left = 1.563386F;
            this.直線26.LineWeight = 1F;
            this.直線26.Name = "直線26";
            this.直線26.Tag = "";
            this.直線26.Top = 0.00355424F;
            this.直線26.Width = 0F;
            this.直線26.X1 = 1.563386F;
            this.直線26.X2 = 1.563386F;
            this.直線26.Y1 = 0.00355424F;
            this.直線26.Y2 = 0.3973042F;
            // 
            // 直線29
            // 
            this.直線29.Height = 0.3973042F;
            this.直線29.Left = 5.118504F;
            this.直線29.LineWeight = 1F;
            this.直線29.Name = "直線29";
            this.直線29.Tag = "";
            this.直線29.Top = 0F;
            this.直線29.Width = 0F;
            this.直線29.X1 = 5.118504F;
            this.直線29.X2 = 5.118504F;
            this.直線29.Y1 = 0F;
            this.直線29.Y2 = 0.3973042F;
            // 
            // 直線30
            // 
            this.直線30.Height = 0.3937008F;
            this.直線30.Left = 6.299212F;
            this.直線30.LineWeight = 1F;
            this.直線30.Name = "直線30";
            this.直線30.Tag = "";
            this.直線30.Top = 0F;
            this.直線30.Width = 0F;
            this.直線30.X1 = 6.299212F;
            this.直線30.X2 = 6.299212F;
            this.直線30.Y1 = 0F;
            this.直線30.Y2 = 0.3937008F;
            // 
            // テキスト006
            // 
            this.テキスト006.CanGrow = false;
            this.テキスト006.DataField = "ITEM06";
            this.テキスト006.Height = 0.15625F;
            this.テキスト006.Left = 1.619792F;
            this.テキスト006.Name = "テキスト006";
            this.テキスト006.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト006.Tag = "";
            this.テキスト006.Text = "ITEM06";
            this.テキスト006.Top = 0.2327209F;
            this.テキスト006.Width = 2.285417F;
            // 
            // テキスト007
            // 
            this.テキスト007.DataField = "ITEM07";
            this.テキスト007.Height = 0.1713966F;
            this.テキスト007.Left = 3.984375F;
            this.テキスト007.Name = "テキスト007";
            this.テキスト007.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.テキスト007.Tag = "";
            this.テキスト007.Text = "ITEM07";
            this.テキスト007.Top = 0.2223042F;
            this.テキスト007.Width = 1.077083F;
            // 
            // テキスト008
            // 
            this.テキスト008.DataField = "ITEM08";
            this.テキスト008.Height = 0.1700077F;
            this.テキスト008.Left = 5.161458F;
            this.テキスト008.Name = "テキスト008";
            this.テキスト008.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.テキスト008.Tag = "";
            this.テキスト008.Text = "ITEM08";
            this.テキスト008.Top = 0.2236931F;
            this.テキスト008.Width = 1.077083F;
            // 
            // 直線84
            // 
            this.直線84.Height = 0.39375F;
            this.直線84.Left = 3.942126F;
            this.直線84.LineWeight = 1F;
            this.直線84.Name = "直線84";
            this.直線84.Tag = "";
            this.直線84.Top = 0.00355424F;
            this.直線84.Width = 0F;
            this.直線84.X1 = 3.942126F;
            this.直線84.X2 = 3.942126F;
            this.直線84.Y1 = 0.00355424F;
            this.直線84.Y2 = 0.3973042F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM10";
            this.textBox1.Height = 0.1712599F;
            this.textBox1.Left = 3.984252F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM10";
            this.textBox1.Top = 0.2224409F;
            this.textBox1.Width = 1.077165F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM11";
            this.textBox2.Height = 0.1700077F;
            this.textBox2.Left = 5.161417F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM11";
            this.textBox2.Top = 0.2236221F;
            this.textBox2.Width = 1.077083F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox3,
            this.textBox4,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.label3,
            this.line8,
            this.line9,
            this.label4,
            this.label5,
            this.label6,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.line10,
            this.line11});
            this.gfShishoCd.Height = 1.595913F;
            this.gfShishoCd.Name = "gfShishoCd";
            this.gfShishoCd.Format += new System.EventHandler(this.gfShishoCd_Format);
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM07";
            this.textBox3.Height = 0.15625F;
            this.textBox3.Left = 3.979921F;
            this.textBox3.Name = "textBox3";
            this.textBox3.OutputFormat = resources.GetString("textBox3.OutputFormat");
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; white-space: inherit; ddo-char-set: 1; ddo-wra" +
    "p-mode: inherit";
            this.textBox3.SummaryGroup = "ghShishoCd";
            this.textBox3.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox3.Tag = "";
            this.textBox3.Text = null;
            this.textBox3.Top = 0.2314961F;
            this.textBox3.Width = 1.077083F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM09";
            this.textBox4.Height = 0.1770833F;
            this.textBox4.Left = 3.979921F;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM09";
            this.textBox4.Top = 0.6066929F;
            this.textBox4.Width = 1.077083F;
            // 
            // line3
            // 
            this.line3.Height = 1.575197F;
            this.line3.Left = 1.563386F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Tag = "";
            this.line3.Top = 0F;
            this.line3.Width = 0F;
            this.line3.X1 = 1.563386F;
            this.line3.X2 = 1.563386F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 1.575197F;
            // 
            // line4
            // 
            this.line4.Height = 1.575197F;
            this.line4.Left = 3.942126F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Tag = "";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 3.942126F;
            this.line4.X2 = 3.942126F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 1.575197F;
            // 
            // line5
            // 
            this.line5.Height = 1.575197F;
            this.line5.Left = 5.118504F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Tag = "";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 5.118504F;
            this.line5.X2 = 5.118504F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 1.575197F;
            // 
            // line6
            // 
            this.line6.Height = 1.575197F;
            this.line6.Left = 6.299212F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Tag = "";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 6.299212F;
            this.line6.X2 = 6.299212F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 1.575197F;
            // 
            // line7
            // 
            this.line7.Height = 0F;
            this.line7.Left = 0.01023622F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Tag = "";
            this.line7.Top = 0.3956693F;
            this.line7.Width = 6.3F;
            this.line7.X1 = 0.01023622F;
            this.line7.X2 = 6.310236F;
            this.line7.Y1 = 0.3956693F;
            this.line7.Y2 = 0.3956693F;
            // 
            // label3
            // 
            this.label3.Height = 0.15625F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.3543307F;
            this.label3.Name = "label3";
            this.label3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "小　　　　　計";
            this.label3.Top = 0.2275591F;
            this.label3.Width = 0.9166667F;
            // 
            // line8
            // 
            this.line8.Height = 0F;
            this.line8.Left = 0.01023622F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Tag = "";
            this.line8.Top = 0.7874016F;
            this.line8.Width = 6.3F;
            this.line8.X1 = 0.01023622F;
            this.line8.X2 = 6.310236F;
            this.line8.Y1 = 0.7874016F;
            this.line8.Y2 = 0.7874016F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 0.01023622F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Tag = "";
            this.line9.Top = 1.181102F;
            this.line9.Width = 6.3F;
            this.line9.X1 = 0.01023622F;
            this.line9.X2 = 6.310236F;
            this.line9.Y1 = 1.181102F;
            this.line9.Y2 = 1.181102F;
            // 
            // label4
            // 
            this.label4.Height = 0.15625F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.3543307F;
            this.label4.Name = "label4";
            this.label4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label4.Tag = "";
            this.label4.Text = "前　日　現　金";
            this.label4.Top = 0.6145669F;
            this.label4.Width = 0.9166667F;
            // 
            // label5
            // 
            this.label5.Height = 0.15625F;
            this.label5.HyperLink = null;
            this.label5.Left = 0.3543307F;
            this.label5.Name = "label5";
            this.label5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label5.Tag = "";
            this.label5.Text = "本　日　現　金";
            this.label5.Top = 1.010236F;
            this.label5.Width = 0.9166667F;
            // 
            // label6
            // 
            this.label6.Height = 0.15625F;
            this.label6.HyperLink = null;
            this.label6.Left = 0.3543307F;
            this.label6.Name = "label6";
            this.label6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label6.Tag = "";
            this.label6.Text = "合　　　　　計";
            this.label6.Top = 1.406299F;
            this.label6.Width = 0.9166667F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM08";
            this.textBox5.Height = 0.15625F;
            this.textBox5.Left = 5.19685F;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox5.SummaryGroup = "ghShishoCd";
            this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM08";
            this.textBox5.Top = 0.2314961F;
            this.textBox5.Width = 1.077083F;
            // 
            // textBox6
            // 
            this.textBox6.Height = 0.15625F;
            this.textBox6.Left = 3.979921F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox6.Tag = "";
            this.textBox6.Text = null;
            this.textBox6.Top = 1.416535F;
            this.textBox6.Width = 1.077083F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM09";
            this.textBox7.Height = 0.15625F;
            this.textBox7.Left = 5.19685F;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; text-justify: auto; ddo-char-set: 1";
            this.textBox7.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Min;
            this.textBox7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox7.Tag = "";
            this.textBox7.Text = null;
            this.textBox7.Top = 0.6220472F;
            this.textBox7.Visible = false;
            this.textBox7.Width = 1.077083F;
            // 
            // textBox8
            // 
            this.textBox8.Height = 0.15625F;
            this.textBox8.Left = 5.156299F;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox8.Tag = "";
            this.textBox8.Text = null;
            this.textBox8.Top = 1.417323F;
            this.textBox8.Width = 1.077083F;
            // 
            // textBox9
            // 
            this.textBox9.Height = 0.15625F;
            this.textBox9.Left = 5.19685F;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; text-justify: auto; ddo-char-set: 1";
            this.textBox9.Tag = "";
            this.textBox9.Text = null;
            this.textBox9.Top = 1.011811F;
            this.textBox9.Width = 1.077083F;
            // 
            // line10
            // 
            this.line10.Height = 0F;
            this.line10.Left = 0.01023622F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Tag = "";
            this.line10.Top = 1.575197F;
            this.line10.Width = 6.3F;
            this.line10.X1 = 0.01023622F;
            this.line10.X2 = 6.310236F;
            this.line10.Y1 = 1.575197F;
            this.line10.Y2 = 1.575197F;
            // 
            // line11
            // 
            this.line11.Height = 1.575197F;
            this.line11.Left = 0F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Tag = "";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 0F;
            this.line11.X2 = 0F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 1.575197F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト48,
            this.テキスト74,
            this.直線39,
            this.直線40,
            this.直線42,
            this.直線43,
            this.直線44,
            this.直線45,
            this.ラベル46,
            this.直線68,
            this.直線69,
            this.ラベル71,
            this.ラベル72,
            this.ラベル73,
            this.テキスト76,
            this.テキスト77,
            this.テキスト78,
            this.テキスト79,
            this.テキスト80,
            this.直線70});
            this.reportFooter1.Height = 1.592361F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Visible = false;
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // テキスト48
            // 
            this.テキスト48.DataField = "ITEM07";
            this.テキスト48.Height = 0.15625F;
            this.テキスト48.Left = 3.979922F;
            this.テキスト48.Name = "テキスト48";
            this.テキスト48.OutputFormat = resources.GetString("テキスト48.OutputFormat");
            this.テキスト48.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; white-space: inherit; ddo-char-set: 1; ddo-wra" +
    "p-mode: inherit";
            this.テキスト48.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト48.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト48.Tag = "";
            this.テキスト48.Text = null;
            this.テキスト48.Top = 0.2314961F;
            this.テキスト48.Width = 1.077083F;
            // 
            // テキスト74
            // 
            this.テキスト74.DataField = "ITEM09";
            this.テキスト74.Height = 0.1770833F;
            this.テキスト74.Left = 3.979922F;
            this.テキスト74.Name = "テキスト74";
            this.テキスト74.OutputFormat = resources.GetString("テキスト74.OutputFormat");
            this.テキスト74.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト74.Tag = "";
            this.テキスト74.Text = "ITEM09";
            this.テキスト74.Top = 0.606693F;
            this.テキスト74.Width = 1.077083F;
            // 
            // 直線39
            // 
            this.直線39.Height = 1.585417F;
            this.直線39.Left = 2.384186E-07F;
            this.直線39.LineWeight = 1F;
            this.直線39.Name = "直線39";
            this.直線39.Tag = "";
            this.直線39.Top = 0F;
            this.直線39.Width = 0F;
            this.直線39.X1 = 2.384186E-07F;
            this.直線39.X2 = 2.384186E-07F;
            this.直線39.Y1 = 0F;
            this.直線39.Y2 = 1.585417F;
            // 
            // 直線40
            // 
            this.直線40.Height = 1.575F;
            this.直線40.Left = 1.563386F;
            this.直線40.LineWeight = 1F;
            this.直線40.Name = "直線40";
            this.直線40.Tag = "";
            this.直線40.Top = 0F;
            this.直線40.Width = 0F;
            this.直線40.X1 = 1.563386F;
            this.直線40.X2 = 1.563386F;
            this.直線40.Y1 = 0F;
            this.直線40.Y2 = 1.575F;
            // 
            // 直線42
            // 
            this.直線42.Height = 1.575F;
            this.直線42.Left = 3.942126F;
            this.直線42.LineWeight = 1F;
            this.直線42.Name = "直線42";
            this.直線42.Tag = "";
            this.直線42.Top = 0F;
            this.直線42.Width = 0F;
            this.直線42.X1 = 3.942126F;
            this.直線42.X2 = 3.942126F;
            this.直線42.Y1 = 0F;
            this.直線42.Y2 = 1.575F;
            // 
            // 直線43
            // 
            this.直線43.Height = 1.575F;
            this.直線43.Left = 5.118504F;
            this.直線43.LineWeight = 1F;
            this.直線43.Name = "直線43";
            this.直線43.Tag = "";
            this.直線43.Top = 0F;
            this.直線43.Width = 0F;
            this.直線43.X1 = 5.118504F;
            this.直線43.X2 = 5.118504F;
            this.直線43.Y1 = 0F;
            this.直線43.Y2 = 1.575F;
            // 
            // 直線44
            // 
            this.直線44.Height = 1.574803F;
            this.直線44.Left = 6.299212F;
            this.直線44.LineWeight = 1F;
            this.直線44.Name = "直線44";
            this.直線44.Tag = "";
            this.直線44.Top = 0F;
            this.直線44.Width = 0F;
            this.直線44.X1 = 6.299212F;
            this.直線44.X2 = 6.299212F;
            this.直線44.Y1 = 0F;
            this.直線44.Y2 = 1.574803F;
            // 
            // 直線45
            // 
            this.直線45.Height = 0F;
            this.直線45.Left = 0.01041691F;
            this.直線45.LineWeight = 1F;
            this.直線45.Name = "直線45";
            this.直線45.Tag = "";
            this.直線45.Top = 0.3958333F;
            this.直線45.Width = 6.3F;
            this.直線45.X1 = 0.01041691F;
            this.直線45.X2 = 6.310417F;
            this.直線45.Y1 = 0.3958333F;
            this.直線45.Y2 = 0.3958333F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.15625F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 0.3543307F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "小　　　　　計";
            this.ラベル46.Top = 0.2275591F;
            this.ラベル46.Width = 0.9166667F;
            // 
            // 直線68
            // 
            this.直線68.Height = 0F;
            this.直線68.Left = 0.01041691F;
            this.直線68.LineWeight = 1F;
            this.直線68.Name = "直線68";
            this.直線68.Tag = "";
            this.直線68.Top = 0.7875F;
            this.直線68.Width = 6.3F;
            this.直線68.X1 = 0.01041691F;
            this.直線68.X2 = 6.310417F;
            this.直線68.Y1 = 0.7875F;
            this.直線68.Y2 = 0.7875F;
            // 
            // 直線69
            // 
            this.直線69.Height = 0F;
            this.直線69.Left = 0.01041691F;
            this.直線69.LineWeight = 1F;
            this.直線69.Name = "直線69";
            this.直線69.Tag = "";
            this.直線69.Top = 1.18125F;
            this.直線69.Width = 6.3F;
            this.直線69.X1 = 0.01041691F;
            this.直線69.X2 = 6.310417F;
            this.直線69.Y1 = 1.18125F;
            this.直線69.Y2 = 1.18125F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.15625F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 0.3541669F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "前　日　現　金";
            this.ラベル71.Top = 0.6145833F;
            this.ラベル71.Width = 0.9166667F;
            // 
            // ラベル72
            // 
            this.ラベル72.Height = 0.15625F;
            this.ラベル72.HyperLink = null;
            this.ラベル72.Left = 0.3541669F;
            this.ラベル72.Name = "ラベル72";
            this.ラベル72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル72.Tag = "";
            this.ラベル72.Text = "本　日　現　金";
            this.ラベル72.Top = 1.010417F;
            this.ラベル72.Width = 0.9166667F;
            // 
            // ラベル73
            // 
            this.ラベル73.Height = 0.15625F;
            this.ラベル73.HyperLink = null;
            this.ラベル73.Left = 0.3541669F;
            this.ラベル73.Name = "ラベル73";
            this.ラベル73.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル73.Tag = "";
            this.ラベル73.Text = "合　　　　　計";
            this.ラベル73.Top = 1.40625F;
            this.ラベル73.Width = 0.9166667F;
            // 
            // テキスト76
            // 
            this.テキスト76.DataField = "ITEM08";
            this.テキスト76.Height = 0.15625F;
            this.テキスト76.Left = 5.196851F;
            this.テキスト76.Name = "テキスト76";
            this.テキスト76.OutputFormat = resources.GetString("テキスト76.OutputFormat");
            this.テキスト76.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト76.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト76.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト76.Tag = "";
            this.テキスト76.Text = "ITEM08";
            this.テキスト76.Top = 0.2314961F;
            this.テキスト76.Width = 1.077083F;
            // 
            // テキスト77
            // 
            this.テキスト77.Height = 0.15625F;
            this.テキスト77.Left = 3.979861F;
            this.テキスト77.Name = "テキスト77";
            this.テキスト77.OutputFormat = resources.GetString("テキスト77.OutputFormat");
            this.テキスト77.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト77.Tag = "";
            this.テキスト77.Text = null;
            this.テキスト77.Top = 1.416667F;
            this.テキスト77.Width = 1.077083F;
            // 
            // テキスト78
            // 
            this.テキスト78.DataField = "ITEM09";
            this.テキスト78.Height = 0.15625F;
            this.テキスト78.Left = 5.196851F;
            this.テキスト78.Name = "テキスト78";
            this.テキスト78.OutputFormat = resources.GetString("テキスト78.OutputFormat");
            this.テキスト78.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; text-justify: auto; ddo-char-set: 1";
            this.テキスト78.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Min;
            this.テキスト78.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト78.Tag = "";
            this.テキスト78.Text = null;
            this.テキスト78.Top = 0.6220473F;
            this.テキスト78.Visible = false;
            this.テキスト78.Width = 1.077083F;
            // 
            // テキスト79
            // 
            this.テキスト79.Height = 0.15625F;
            this.テキスト79.Left = 5.1563F;
            this.テキスト79.Name = "テキスト79";
            this.テキスト79.OutputFormat = resources.GetString("テキスト79.OutputFormat");
            this.テキスト79.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト79.Tag = "";
            this.テキスト79.Text = null;
            this.テキスト79.Top = 1.417323F;
            this.テキスト79.Width = 1.077083F;
            // 
            // テキスト80
            // 
            this.テキスト80.Height = 0.15625F;
            this.テキスト80.Left = 5.196851F;
            this.テキスト80.Name = "テキスト80";
            this.テキスト80.OutputFormat = resources.GetString("テキスト80.OutputFormat");
            this.テキスト80.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; text-justify: auto; ddo-char-set: 1";
            this.テキスト80.Tag = "";
            this.テキスト80.Text = null;
            this.テキスト80.Top = 1.011811F;
            this.テキスト80.Width = 1.077083F;
            // 
            // 直線70
            // 
            this.直線70.Height = 0F;
            this.直線70.Left = 0.01041691F;
            this.直線70.LineWeight = 1F;
            this.直線70.Name = "直線70";
            this.直線70.Tag = "";
            this.直線70.Top = 1.575F;
            this.直線70.Width = 6.3F;
            this.直線70.X1 = 0.01041691F;
            this.直線70.X2 = 6.310417F;
            this.直線70.Y1 = 1.575F;
            this.直線70.Y2 = 1.575F;
            // 
            // ZMDR10411R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Left = 0.9645669F;
            this.PageSettings.Margins.Right = 0.9657481F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6.329397F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト001;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線53;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線55;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線56;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル57;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル58;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル59;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線62;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線64;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス80;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線14;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線16;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線17;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル19;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル21;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル22;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル23;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線81;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線25;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線26;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線29;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト002;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト003;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト006;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト007;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト008;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト004;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト005;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線84;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線39;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線40;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線42;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線43;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線44;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト48;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線68;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線69;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線70;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル72;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト78;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト79;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト80;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
    }
}
