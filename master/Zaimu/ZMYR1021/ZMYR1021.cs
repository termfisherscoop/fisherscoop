﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Reflection;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.zam.zame1031;

namespace jp.co.fsi.zm.zmyr1021
{
    /// <summary>
    /// 貸借対照表付属明細書(ZMYR1021)
    /// </summary>
    public partial class ZMYR1021 : BasePgForm
    {
        #region 定数
        //定数
        public const int prtCols = 111;
        #endregion

        #region 変数

        int _sort;              // ワークテーブルレコードカウンタ
        DataTable _dtMei;       // 明細表科目分類データ
        DataTable _dtKmk1;       // 科目情報参照データ
        DataTable _dtKmk2;       // 科目情報参照データ
        DataTable _dtJis;       // 科目実績参照データ

        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMYR1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }

            // 会計期間を取得
            string[] jpDateFr = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
            string[] jpDateTo = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);

            // 期間（自）
            lblDateGengoFr.Text = jpDateFr[0];
            txtDateYearFr.Text = jpDateFr[2];
            txtDateMonthFr.Text = jpDateFr[3];
            txtDateDayFr.Text = jpDateFr[4];
            // 期間（至）
            lblDateGengoTo.Text = jpDateTo[0];
            txtDateYearTo.Text = jpDateTo[2];
            txtDateMonthTo.Text = jpDateTo[3];
            txtDateDayTo.Text = jpDateTo[4];

            rdoOpt2.Checked = true;

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付にフォーカス時のみF1を有効にする
            switch (this.ActiveControl.Name)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveControl.Name)
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearFr":
                case "txtDateYearTo":
                    #region 元号検索
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveControl.Name == "txtDateYearFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblDateGengoFr.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblDateGengoFr.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        ZaUtil.FixJpDate(this.lblDateGengoFr.Text,
                                            this.txtDateYearFr.Text,
                                            this.txtDateMonthFr.Text,
                                            this.txtDateDayFr.Text,
                                            this.Dba);
                                    SetJpDateFr(arrJpDate);
                                }
                            }
                            else if (this.ActiveControl.Name == "txtDateYearTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblDateGengoTo.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblDateGengoTo.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        ZaUtil.FixJpDate(this.lblDateGengoTo.Text,
                                            this.txtDateYearTo.Text,
                                            this.txtDateMonthTo.Text,
                                            this.txtDateDayTo.Text,
                                            this.Dba);
                                    SetJpDateTo(arrJpDate);
                                }
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷","実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "ZMYR1021R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
            }
        }

        /// <summary>
        /// 期間自(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateYearFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateYearFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(ZaUtil.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 期間自(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateMonthFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(ZaUtil.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 期間自(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateDayFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateDayFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(ZaUtil.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 期間至(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateYearTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateYearTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(ZaUtil.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 期間至(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateMonthTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateMonthTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(ZaUtil.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 期間至(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateDayTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateDayTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(ZaUtil.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// ラジオボタンでEnterキーが押された場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Enter)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 印刷処理
                this.PressF4();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            string[] fixJpDate =
                Util.ConvJpDate(
                ZaUtil.FixNendoDate(
                Util.ConvAdDate(arrJpDate[0], arrJpDate[2], arrJpDate[3], arrJpDate[4], this.Dba), this.UInfo)
                , this.Dba);

            this.lblDateGengoFr.Text = fixJpDate[0];
            this.txtDateYearFr.Text = fixJpDate[2];
            this.txtDateMonthFr.Text = fixJpDate[3];
            this.txtDateDayFr.Text = fixJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            string[] fixJpDate =
                Util.ConvJpDate(
                ZaUtil.FixNendoDate(
                Util.ConvAdDate(arrJpDate[0], arrJpDate[2], arrJpDate[3], arrJpDate[4], this.Dba), this.UInfo)
                , this.Dba);

            this.lblDateGengoTo.Text = fixJpDate[0];
            this.txtDateYearTo.Text = fixJpDate[2];
            this.txtDateMonthTo.Text = fixJpDate[3];
            this.txtDateDayTo.Text = fixJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する。
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                //// 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                //this.Dba.Commit();
                //DataTable dtOutput = MakeWkData();
                //dataFlag = InsertWkData(dtOutput);

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMYR1021R rpt = new ZMYR1021R(dtOutput);

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 日付を西暦にして保持
            DateTime denpyoDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime denpyoDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);

            // 参照データの取得
            DbParamCollection dpc;
            StringBuilder sql;

            #region 明細表科目分類データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.AppendLine("SELECT *");
            sql.AppendLine(" FROM TB_ZM_MEISAIHYO_KAMOKU_BUNRUI");
            //sql.AppendLine(" WHERE CHOHYO_BUNRUI = 1");                         // 帳票分類＝1(固定)
            sql.AppendLine(" WHERE ");                         // 帳票分類＝1(固定)
            sql.AppendLine(" FUZOKU_MEISAI_KUBUN = 1");                     // 明細区分＝1(固定)
            sql.AppendLine(" AND SHIYO_KUBUN = 1");                             // 使用区分＝1(固定)
            sql.AppendLine(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            _dtMei = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (_dtMei.Rows.Count == 0)
            {
                Msg.Info("設定データにエラーがあります。");
                return false;
            }
            #endregion

            #region 科目情報データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.AppendLine("SELECT A.CHOHYO_BUNRUI AS CHOHYO_BUNRUI");
            sql.AppendLine(",A.KAMOKU_BUNRUI AS KAMOKU_BUNRUI");
            sql.AppendLine(",A.GYO_BANGO AS GYO_BANGO");
            sql.AppendLine(",A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD");
            sql.AppendLine(",A.KANJO_KAMOKU_NM AS KANJO_KAMOKU_NM");
            sql.AppendLine(",A.TAISHAKU_KUBUN AS TAISHAKU_KUBUN");
            sql.AppendLine(",B.KANJO_KAMOKU_NM AS M_KANJO_KAMOKU_NM");
            sql.AppendLine(",B.TAISHAKU_KUBUN AS M_TAISHAKU_KUBUN");
            sql.AppendLine(" FROM TB_ZM_SHISANHYO_KAMOKU_SETTEI AS A");
            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.AppendLine(" ON B.KAISHA_CD = @KAISHA_CD");
            sql.AppendLine(" AND B.KAIKEI_NENDO = A.KAIKEI_NENDO");
            sql.AppendLine(" AND B.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD");
            sql.AppendLine(" WHERE CHOHYO_BUNRUI = 1 AND ");                         // 帳票分類＝1(固定)
            sql.AppendLine(" A.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.AppendLine(" ORDER BY A.GYO_BANGO ASC,A.KANJO_KAMOKU_CD ASC");
            _dtKmk1 = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (_dtKmk1.Rows.Count == 0)
            {
                Msg.Info("設定データにエラーがあります。");
                return false;
            }
            #endregion

            #region 科目情報データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.AppendLine("SELECT A.CHOHYO_BUNRUI AS CHOHYO_BUNRUI");
            sql.AppendLine(",A.KAMOKU_BUNRUI AS KAMOKU_BUNRUI");
            sql.AppendLine(",A.GYO_BANGO AS GYO_BANGO");
            sql.AppendLine(",A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD");
            sql.AppendLine(",A.KANJO_KAMOKU_NM AS KANJO_KAMOKU_NM");
            sql.AppendLine(",A.TAISHAKU_KUBUN AS TAISHAKU_KUBUN");
            sql.AppendLine(",B.KANJO_KAMOKU_NM AS M_KANJO_KAMOKU_NM");
            sql.AppendLine(",B.TAISHAKU_KUBUN AS M_TAISHAKU_KUBUN");
            sql.AppendLine(" FROM TB_ZM_SHISANHYO_KAMOKU_SETTEI AS A");
            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.AppendLine(" ON B.KAISHA_CD = @KAISHA_CD");
            sql.AppendLine(" AND B.KAIKEI_NENDO = A.KAIKEI_NENDO");
            sql.AppendLine(" AND B.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD");
            sql.AppendLine(" WHERE CHOHYO_BUNRUI = 2 AND ");                         // 帳票分類＝2(固定)
            sql.AppendLine(" A.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.AppendLine(" ORDER BY A.GYO_BANGO ASC,A.KANJO_KAMOKU_CD ASC");
            _dtKmk2 = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (_dtKmk2.Rows.Count == 0)
            {
                Msg.Info("設定データにエラーがあります。");
                return false;
            }
            #endregion

            #region 科目実績データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, denpyoDateFr);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, denpyoDateTo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            sql = new StringBuilder();
            sql.AppendLine("SELECT A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD");
            sql.AppendLine(",MIN(A.TAISHAKU_KUBUN) AS A_TAISHAKU_KUBUN");
            sql.AppendLine(",MIN(B.TAISHAKU_KUBUN) AS TAISHAKU_KUBUN");
            sql.AppendLine(",SUM(");
            sql.AppendLine("  CASE WHEN A.DENPYO_DATE < @DENPYO_DATE_FR");
            sql.AppendLine("  THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.AppendLine("       THEN A.ZEIKOMI_KINGAKU");
            sql.AppendLine("       ELSE (A.ZEIKOMI_KINGAKU * -1)");
            sql.AppendLine("       END");
            sql.AppendLine("  ELSE 0");
            sql.AppendLine("  END");
            sql.AppendLine(" ) AS ZENZAN");
            sql.AppendLine(",SUM(");
            sql.AppendLine("  CASE WHEN A.DENPYO_DATE >= @DENPYO_DATE_FR");
            sql.AppendLine("  THEN CASE WHEN A.TAISHAKU_KUBUN = 1");
            sql.AppendLine("       THEN A.ZEIKOMI_KINGAKU");
            sql.AppendLine("       ELSE 0");
            sql.AppendLine("       END");
            sql.AppendLine("  ELSE 0");
            sql.AppendLine("  END");
            sql.AppendLine(" ) AS KARIKATA");
            sql.AppendLine(",SUM(");
            sql.AppendLine("  CASE WHEN A.DENPYO_DATE >= @DENPYO_DATE_FR");
            sql.AppendLine("  THEN CASE WHEN A.TAISHAKU_KUBUN = 2");
            sql.AppendLine("       THEN A.ZEIKOMI_KINGAKU");
            sql.AppendLine("       ELSE 0");
            sql.AppendLine("       END");
            sql.AppendLine("  ELSE 0");
            sql.AppendLine("  END) AS KASHIKATA");
            sql.AppendLine(",SUM(");
            sql.AppendLine("  CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.AppendLine("  THEN A.ZEIKOMI_KINGAKU");
            sql.AppendLine("  ELSE (A.ZEIKOMI_KINGAKU * -1)");
            sql.AppendLine("  END");
            sql.AppendLine(" ) AS ZANDAKA");
            sql.AppendLine(",COUNT(*) AS KENSU");
            sql.AppendLine(" FROM TB_ZM_SHIWAKE_MEISAI AS A");
            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.AppendLine(" ON A.KAISHA_CD = B.KAISHA_CD");
            sql.AppendLine(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.AppendLine(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            sql.AppendLine(" WHERE A.KAISHA_CD = @KAISHA_CD");
            sql.AppendLine(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" AND A.DENPYO_DATE <= @DENPYO_DATE_TO");
            sql.AppendLine(" AND A.MEISAI_KUBUN <= 0");
            sql.AppendLine(" AND A.KANJO_KAMOKU_CD IN");
            sql.AppendLine("  (SELECT S.KANJO_KAMOKU_CD");
            sql.AppendLine("   FROM TB_ZM_SHISANHYO_KAMOKU_SETTEI AS S");
            sql.AppendLine("   WHERE S.KAIKEI_NENDO = @KAIKEI_NENDO )");
            //sql.AppendLine("   AND S.KAMOKU_BUNRUI = B.KAMOKU_BUNRUI_CD )");
            if (this.txtMizuageShishoCd.Text != "" && this.txtMizuageShishoCd.Text != "0")
            {
                sql.AppendLine(" AND A.SHISHO_CD = @SHISHO_CD ");
            }
            sql.AppendLine(" GROUP BY A.KANJO_KAMOKU_CD");
            _dtJis = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (_dtJis.Rows.Count == 0)
            {
                Msg.Info("設定データにエラーがあります。");
                return false;
            }
            #endregion

            _sort = 0;                      // ソートフィールドカウンタリセット

            // セクション別に出力ワーク作成
            int firstNo = 0;
            string[] bItem = new string[150];
            ArrayList returnList = new ArrayList();
            MakeSectionData01(1, 0, 30, 11132);
            returnList = MakeSectionData02(2, 1, 40, 11133, firstNo, 54, bItem, false);
            MakeSectionData02(2, 2, 50, 11134, (int)returnList[0], 110, (string[])returnList[1], true);
            MakeSectionData03(3, 0, 60, 11120);
            MakeSectionData03(4, 0, 70, 11130);
            MakeSectionData03(5, 0, 170, 11240);
            bItem = new string[150];
            returnList = MakeSectionData02(6, 1, 220, 12110, firstNo, 54, bItem, false);
            MakeSectionData02(6, 2, 230, 12130, (int)returnList[0], 110, (string[])returnList[1], true);
            MakeSectionData03(7, 0, 250, 12100);
            bItem = new string[150];
            returnList = MakeSectionData02(8, 0, 300, 13100, firstNo, 4, bItem, false); // 出資金 0
            //returnList = MakeSectionData02(8, 0, 301, 13320, (int)returnList[0], 4, (string[])returnList[1], false); // sqlに無し 資本準備金
            returnList = MakeSectionData02(8, 0, 310, 13300, (int)returnList[0], 54, (string[])returnList[1], false); // 剰余金 10
            //returnList = MakeSectionData02(8, 0, 311, 13350, (int)returnList[0], 54, (string[])returnList[1], false); // sqlに無し 教育情報資金
            //returnList = MakeSectionData02(8, 0, 312, 13370, (int)returnList[0], 54, (string[])returnList[1], false); // sqlに無し  特別配当金
            returnList = MakeSectionData021(8, 0, 320, 99910, (int)returnList[0], 59, (string[])returnList[1], false); // 当期未処分剰余金 0
            returnList = MakeSectionData021(8, 0, 330, 0, (int)returnList[0], 64, (string[])returnList[1], false); // (うち剰余金)0
            MakeSectionData02(8, 0, 340, 0, (int)returnList[0], 110, (string[])returnList[1], true);   
            ////MakeSectionDataEx(8, 0, "（８）資本", 340, 0);        // 例外集計を実行

            // 印刷ワークテーブルのデータ件数有無で戻り値セット
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable dtPr = this.Dba.GetDataTableByConditionWithParams("SORT", "PR_ZM_TBL", "GUID = @GUID", dpc);
            return (dtPr.Rows.Count > 0);
        }

        /// <summary>
        /// セクション別印刷ワークデータ作成
        /// </summary>
        /// <param name="groupNo">グループ番号</param>
        /// <param name="edaNo">セクション枝番</param>
        /// <param name="secNm">セクション名称</param>
        /// <param name="hyojiJuni">TB明細表設定テーブル参照キー(表示順位)</param>
        /// <param name="kamokuBunrui">TB明細表設定テーブル参照キー(科目分類)</param>
        private void MakeSectionData01(int groupNo, int edaNo, int hyojiJuni, int kamokuBunrui)
        {
            // 明細表科目分類データ参照
            DataRow[] fRowsMei = _dtMei.Select("HYOJI_JUNI = " + Util.ToString(hyojiJuni)
                                       + " AND KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui));
            if (fRowsMei.Length == 0)
            {
                //Msg.Info("設定データにエラーがあります。");
                return;
            }
            DataRow drMei = fRowsMei[0];

            string[] bItem;     // 明細情報格納用

            ArrayList alParamsPrZmTbl;          // ワークテーブルINSERT用パラメータ

            // 明細情報レコードを出力
            int gyoMax = Util.ToInt(drMei["MEISAI_KOMOKUSU"]);      // 明細表設定テーブルの明細項目数だけ行出力
            int row_item_su = 6;
            int item01 = 0;
            int item02 = 1;
            int item03 = 2;
            int item04 = 3;
            int item05 = 4;
            int item06 = 5;
            //bItem = new string[54];
            bItem = new string[150];
            if (gyoMax > 0)
            {
                int gyoCnt = 0;

                // 科目情報データをGYO_BANGO順に取得
                DataRow[] fRowsKmk = _dtKmk1.Select("KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui), "GYO_BANGO");

                int curGyoBango = 0;
                // 明細データの作成
                foreach (DataRow drKmk in fRowsKmk)
                {
                    // 行番号ブレイクの時だけワークレコード書出し
                    if (curGyoBango != Util.ToInt(drKmk["GYO_BANGO"]))
                    {
                        curGyoBango = Util.ToInt(drKmk["GYO_BANGO"]);       // カレント行番号

                        bItem[item01] = Util.ToString(drKmk["KANJO_KAMOKU_NM"]); // TB_試算表科目設定.勘定科目名

                        DataRow dr = GetDrJisByKamokuBunruiGyoBango(Util.ToString(drKmk["KAMOKU_BUNRUI"]), curGyoBango);
                        // 実績なし科目の出力判定
                        if (rdoOpt2.Checked
                            || Util.ToDecimal(dr["ZENZAN"]) > 0
                            || Util.ToDecimal(dr["KARIKATA"]) > 0
                            || Util.ToDecimal(dr["KASHIKATA"]) > 0
                            || Util.ToDecimal(dr["ZANDAKA"]) > 0)
                        {
                            bItem[item02] = Util.FormatNum(dr["ZENZAN"]);
                            if (Util.ToInt(drMei["TAISHAKU_KUBUN"]) == 1)
                            {
                                bItem[item03] = Util.FormatNum(dr["KARIKATA"]);
                                bItem[item04] = Util.FormatNum(dr["KASHIKATA"]);
                            }
                            else
                            {
                                bItem[item03] = Util.FormatNum(dr["KASHIKATA"]);
                                bItem[item04] = Util.FormatNum(dr["KARIKATA"]);
                            }
                            bItem[item05] = Util.FormatNum(dr["ZANDAKA"]);
                            bItem[item06] = Util.FormatNum(0);

                            // ワークレコード書出し
                            gyoCnt++;
                        }
                    }

                    item01 += row_item_su;
                    item02 += row_item_su;
                    item03 += row_item_su;
                    item04 += row_item_su;
                    item05 += row_item_su;
                    item06 += row_item_su;

                    // 明細出力終了判定
                    if (gyoCnt >= gyoMax) break;
                }

                // ブランクレコード作成(空行)
                while (gyoCnt < gyoMax)
                {
                    bItem[item01] = "";
                    bItem[item02] = "";
                    bItem[item03] = "";
                    bItem[item04] = "";
                    bItem[item05] = "";
                    bItem[item06] = "";

                    item01 += row_item_su;
                    item02 += row_item_su;
                    item03 += row_item_su;
                    item04 += row_item_su;
                    item05 += row_item_su;
                    item06 += row_item_su;

                    gyoCnt++;
                }
            }

            // 明細表設定レコード行(合計行)の出力
            if (Util.ToInt(drMei["INJI_KUBUN"]) == 1)
            {
                //bItem = new string[6];

                bItem[item01] = Util.ToString(drMei["KAMOKU_BUNRUI_NM"]); // TB_明細表科目設定.科目分類名

                // 集計計算式より実績集計
                DataRow drSecJis = GetDrJisKeisanshikiValue(Util.ToString(drMei["SHUKEI_KEISANSHIKI"]));
                bItem[item02] = Util.FormatNum(Util.ToDecimal(drSecJis["ZENZAN"]));
                if (Util.ToInt(drMei["TAISHAKU_KUBUN"]) == 1)
                {
                    bItem[item03] = Util.FormatNum(Util.ToDecimal(drSecJis["KARIKATA"]));
                    bItem[item04] = Util.FormatNum(Util.ToDecimal(drSecJis["KASHIKATA"]));
                }
                else
                {
                    bItem[item03] = Util.FormatNum(Util.ToDecimal(drSecJis["KASHIKATA"]));
                    bItem[item04] = Util.FormatNum(Util.ToDecimal(drSecJis["KARIKATA"]));
                }
                bItem[item05] = Util.FormatNum(Util.ToDecimal(drSecJis["ZANDAKA"]));

                // 計算区分＝3の時の当期増減額の逆算
                if (Util.ToInt(drMei["SHUKEI_KUBUN"]) == 3)
                {
                    decimal zogen = Util.ToDecimal(bItem[item05]) - Util.ToDecimal(bItem[item02]);
                    if (zogen >= 0)
                    {
                        bItem[item03] = Util.FormatNum(zogen);
                        bItem[item04] = "0";
                    }
                    else
                    {
                        bItem[item03] = "0";
                        bItem[item04] = Util.FormatNum(Math.Abs(zogen));
                    }
                }

                item01 += row_item_su;
                item02 += row_item_su;
                item03 += row_item_su;
                item04 += row_item_su;
                item05 += row_item_su;
                item06 += row_item_su;
            }

            // ブランクレコード作成(空行)
            for(int i = item06 + 1; i < 111; i++)
            {
                bItem[i] = "";
            }

            alParamsPrZmTbl = SetPrZmTblParams(groupNo, bItem);
            this.Dba.Insert("PR_ZM_TBL", (DbParamCollection)alParamsPrZmTbl[0]);
        }

        /// <summary>
        /// セクション別印刷ワークデータ作成
        /// </summary>
        /// <param name="groupNo">グループ番号</param>
        /// <param name="edaNo">セクション枝番</param>
        /// <param name="secNm">セクション名称</param>
        /// <param name="hyojiJuni">TB明細表設定テーブル参照キー(表示順位)</param>
        /// <param name="kamokuBunrui">TB明細表設定テーブル参照キー(科目分類)</param>
        private ArrayList MakeSectionData02(int groupNo, int edaNo, int hyojiJuni, int kamokuBunrui, int firstNo, int totalNo, string[] bItem, bool flg)
        {
            // 明細表科目分類データ参照
            DataRow[] fRowsMei = _dtMei.Select("HYOJI_JUNI = " + Util.ToString(hyojiJuni)
                                       + " AND KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui));

            ArrayList alParamsPrZmTbl; // ワークテーブルINSERT用パラメータ
            ArrayList returnList = new ArrayList(); // 戻り値格納用変数
            int item_last = firstNo-1;  // 戻り値格納用変数

            // 該当データが1件でない場合
            if (fRowsMei.Length == 0)
            //if (1 == 1)
            {
                //Msg.Info("設定データにエラーがあります。");

                // ブランクレコード作成
                for (int i = firstNo; i <= totalNo; i++)
                {
                    bItem[i] = "";
                    item_last = i;

                }

                // 最後の場合のみ実行
                if (flg)
                {
                    // インサート
                    alParamsPrZmTbl = SetPrZmTblParams(groupNo, bItem);
                    this.Dba.Insert("PR_ZM_TBL", (DbParamCollection)alParamsPrZmTbl[0]);
                }

                returnList.Add(++item_last);
                returnList.Add(bItem);

                return returnList;
            }
           
            DataRow drMei = fRowsMei[0];

            // 明細情報レコードを出力
            int gyoMax = Util.ToInt(drMei["MEISAI_KOMOKUSU"]);      // 明細表設定テーブルの明細項目数だけ行出力
            int row_item_su = 5;
            item_last = firstNo - 1;
            int item01 = firstNo;
            int item02 = item01 + 1;
            int item03 = item02 + 1;
            int item04 = item03 + 1;
            int item05 = item04 + 1;
            if (gyoMax > 0) //gyoMaxが0より大きいか
            {
                int gyoCnt = 0;

                // 科目情報データをGYO_BANGO順に取得
                DataRow[] fRowsKmk = _dtKmk1.Select("KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui), "GYO_BANGO");

                int curGyoBango = 0;
                // 明細データの作成
                foreach (DataRow drKmk in fRowsKmk)
                {
                    // 行番号ブレイクの時だけワークレコード書出し
                    if (curGyoBango != Util.ToInt(drKmk["GYO_BANGO"]))
                    {
                        curGyoBango = Util.ToInt(drKmk["GYO_BANGO"]);       // カレント行番号

                        //bItem = new string[6];
                        bItem[item01] = Util.ToString(drKmk["KANJO_KAMOKU_NM"]); // TB_試算表科目設定.勘定科目名

                        DataRow dr = GetDrJisByKamokuBunruiGyoBango(Util.ToString(drKmk["KAMOKU_BUNRUI"]), curGyoBango);
                        // 実績なし科目の出力判定
                        if (rdoOpt2.Checked
                            || Util.ToDecimal(dr["ZENZAN"]) > 0
                            || Util.ToDecimal(dr["KARIKATA"]) > 0
                            || Util.ToDecimal(dr["KASHIKATA"]) > 0
                            || Util.ToDecimal(dr["ZANDAKA"]) > 0)
                        {
                            bItem[item02] = Util.FormatNum(dr["ZENZAN"]);
                            if (Util.ToInt(drMei["TAISHAKU_KUBUN"]) == 1)
                            {
                                bItem[item03] = Util.FormatNum(dr["KARIKATA"]);
                                bItem[item04] = Util.FormatNum(dr["KASHIKATA"]);
                            }
                            else
                            {
                                bItem[item03] = Util.FormatNum(dr["KASHIKATA"]);
                                bItem[item04] = Util.FormatNum(dr["KARIKATA"]);
                            }
                            bItem[item05] = Util.FormatNum(dr["ZANDAKA"]);

                            // ワークレコード書出し
                            gyoCnt++;
                        }
                    }

                    item_last = item05;
                    item01 += row_item_su;
                    item02 += row_item_su;
                    item03 += row_item_su;
                    item04 += row_item_su;
                    item05 += row_item_su;

                    // 明細出力終了判定
                    if (gyoCnt >= gyoMax) break;
                }

                // ブランクレコード作成
                while (gyoCnt < gyoMax)
                {
                    bItem[item01] = "";
                    bItem[item02] = "";
                    bItem[item03] = "";
                    bItem[item04] = "";
                    bItem[item05] = "";

                    item_last = item05;
                    item01 += row_item_su;
                    item02 += row_item_su;
                    item03 += row_item_su;
                    item04 += row_item_su;
                    item05 += row_item_su;

                    gyoCnt++;

                }
            }

            // 明細表設定レコード行(合計行)の出力
            if (Util.ToInt(drMei["INJI_KUBUN"]) == 1)
            {
                //bItem = new string[6];

                bItem[item01] = Util.ToString(drMei["KAMOKU_BUNRUI_NM"]); // TB_明細表科目設定.科目分類名
                string karikata = "";
                string kashikata = "";

                // 集計計算式より実績集計
                DataRow drSecJis = GetDrJisKeisanshikiValue(Util.ToString(drMei["SHUKEI_KEISANSHIKI"]));
                bItem[item02] = Util.FormatNum(Util.ToDecimal(drSecJis["ZENZAN"]));
                if (Util.ToInt(drMei["TAISHAKU_KUBUN"]) == 1)
                {
                    bItem[item03] = Util.FormatNum(Util.ToDecimal(drSecJis["KARIKATA"]));
                    bItem[item04] = Util.FormatNum(Util.ToDecimal(drSecJis["KASHIKATA"]));
                }
                else
                {
                    bItem[item03] = Util.FormatNum(Util.ToDecimal(drSecJis["KASHIKATA"]));
                    bItem[item04] = Util.FormatNum(Util.ToDecimal(drSecJis["KARIKATA"]));
                }
                bItem[item05] = Util.FormatNum(Util.ToDecimal(drSecJis["ZANDAKA"]));

                item_last = item05;

                // 計算区分＝3の時の当期増減額の逆算
                if (Util.ToInt(drMei["SHUKEI_KUBUN"]) == 3)
                {
                    decimal zogen = Util.ToDecimal(bItem[item05]) - Util.ToDecimal(bItem[item02]);
                    if (zogen >= 0)
                    {
                        bItem[item03] = Util.FormatNum(zogen);
                        bItem[item04] = "0";
                        karikata = "0";
                        kashikata = Util.FormatNum(zogen);
                    }
                    else
                    {
                        bItem[item03] = "0";
                        bItem[item04] = Util.FormatNum(Math.Abs(zogen));
                        karikata = Util.FormatNum(Math.Abs(zogen));
                        kashikata = "0";
                    }
                    item_last = item05;
                }
            }

            // ブランクレコード作成
            for (int i = item05 + 1; i <= totalNo; i++)
            {
                bItem[i] = "";
                item_last = i;
            }

            if (flg)
            {
                alParamsPrZmTbl = SetPrZmTblParams(groupNo, bItem);
                this.Dba.Insert("PR_ZM_TBL", (DbParamCollection)alParamsPrZmTbl[0]);
            }

            returnList.Add(++item_last);
            returnList.Add(bItem);

            return returnList;
        }

        /// <summary>
        /// セクション別印刷ワークデータ作成 (当期未処分剰余金/(うち剰余金))
        /// </summary>
        /// <param name="groupNo">グループ番号</param>
        /// <param name="edaNo">セクション枝番</param>
        /// <param name="secNm">セクション名称</param>
        /// <param name="hyojiJuni">TB明細表設定テーブル参照キー(表示順位)</param>
        /// <param name="kamokuBunrui">TB明細表設定テーブル参照キー(科目分類)</param>
        private ArrayList MakeSectionData021(int groupNo, int edaNo, int hyojiJuni, int kamokuBunrui, int firstNo, int totalNo, string[] bItem, bool flg)
        {
            // 明細表科目分類データ参照
            DataRow[] fRowsMei = _dtMei.Select("HYOJI_JUNI = " + Util.ToString(hyojiJuni)
                                       + " AND KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui));

            ArrayList alParamsPrZmTbl; // ワークテーブルINSERT用パラメータ
            ArrayList returnList = new ArrayList(); // 戻り値格納用変数
            int item_last = firstNo - 1;  // 戻り値格納用変数

            // 該当データが1件でない場合
            if (fRowsMei.Length == 0)
            //if (1 == 1)
            {
                //Msg.Info("設定データにエラーがあります。");

                // ブランクレコード作成
                for (int i = firstNo; i <= totalNo; i++)
                {
                    bItem[i] = "";
                    item_last = i;

                }

                // 最後の場合のみ実行
                if (flg)
                {
                    // インサート
                    alParamsPrZmTbl = SetPrZmTblParams(groupNo, bItem);
                    this.Dba.Insert("PR_ZM_TBL", (DbParamCollection)alParamsPrZmTbl[0]);
                }

                returnList.Add(++item_last);
                returnList.Add(bItem);

                return returnList;
            }

            DataRow drMei = fRowsMei[0];

            // 明細情報レコードを出力
            int gyoMax = Util.ToInt(drMei["MEISAI_KOMOKUSU"]);      // 明細表設定テーブルの明細項目数だけ行出力
            int row_item_su = 5;
            item_last = firstNo - 1;
            int item01 = firstNo;
            int item02 = item01 + 1;
            int item03 = item02 + 1;
            int item04 = item03 + 1;
            int item05 = item04 + 1;
            if (gyoMax > 0) //gyoMaxが0より大きいか
            {
                int gyoCnt = 0;

                // 科目情報データをGYO_BANGO順に取得
                DataRow[] fRowsKmk = _dtKmk2.Select("KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui), "GYO_BANGO");

                int curGyoBango = 0;
                // 明細データの作成
                foreach (DataRow drKmk in fRowsKmk)
                {
                    // 行番号ブレイクの時だけワークレコード書出し
                    if (curGyoBango != Util.ToInt(drKmk["GYO_BANGO"]))
                    {
                        curGyoBango = Util.ToInt(drKmk["GYO_BANGO"]);       // カレント行番号

                        //bItem = new string[6];
                        bItem[item01] = Util.ToString(drKmk["KANJO_KAMOKU_NM"]); // TB_試算表科目設定.勘定科目名

                        DataRow dr = GetDrJisByKamokuBunruiGyoBango(Util.ToString(drKmk["KAMOKU_BUNRUI"]), curGyoBango);
                        // 実績なし科目の出力判定
                        if (rdoOpt2.Checked
                            || Util.ToDecimal(dr["ZENZAN"]) > 0
                            || Util.ToDecimal(dr["KARIKATA"]) > 0
                            || Util.ToDecimal(dr["KASHIKATA"]) > 0
                            || Util.ToDecimal(dr["ZANDAKA"]) > 0)
                        {
                            bItem[item02] = Util.FormatNum(dr["ZENZAN"]);
                            if (Util.ToInt(drMei["TAISHAKU_KUBUN"]) == 1)
                            {
                                bItem[item03] = Util.FormatNum(dr["KARIKATA"]);
                                bItem[item04] = Util.FormatNum(dr["KASHIKATA"]);
                            }
                            else
                            {
                                bItem[item03] = Util.FormatNum(dr["KASHIKATA"]);
                                bItem[item04] = Util.FormatNum(dr["KARIKATA"]);
                            }
                            bItem[item05] = Util.FormatNum(dr["ZANDAKA"]);

                            // ワークレコード書出し
                            gyoCnt++;
                        }
                    }

                    item_last = item05;
                    item01 += row_item_su;
                    item02 += row_item_su;
                    item03 += row_item_su;
                    item04 += row_item_su;
                    item05 += row_item_su;

                    // 明細出力終了判定
                    if (gyoCnt >= gyoMax) break;
                }

                // ブランクレコード作成
                while (gyoCnt < gyoMax)
                {
                    bItem[item01] = "";
                    bItem[item02] = "";
                    bItem[item03] = "";
                    bItem[item04] = "";
                    bItem[item05] = "";

                    item_last = item05;
                    item01 += row_item_su;
                    item02 += row_item_su;
                    item03 += row_item_su;
                    item04 += row_item_su;
                    item05 += row_item_su;

                    gyoCnt++;

                }
            }

            // 明細表設定レコード行(合計行)の出力
            if (Util.ToInt(drMei["INJI_KUBUN"]) == 1)
            {
                //bItem = new string[6];

                bItem[item01] = Util.ToString(drMei["KAMOKU_BUNRUI_NM"]); // TB_明細表科目設定.科目分類名
                string karikata = "";
                string kashikata = "";

                // 集計計算式より実績集計
                DataRow drSecJis = GetDrJisKeisanshikiValueToki(Util.ToString(drMei["SHUKEI_KEISANSHIKI"]));
                bItem[item02] = Util.FormatNum(Util.ToDecimal(drSecJis["ZENZAN"]));
                if (Util.ToInt(drMei["TAISHAKU_KUBUN"]) == 1)
                {
                    bItem[item03] = Util.FormatNum(Util.ToDecimal(drSecJis["KARIKATA"]));
                    bItem[item04] = Util.FormatNum(Util.ToDecimal(drSecJis["KASHIKATA"]));
                }
                else
                {
                    bItem[item03] = Util.FormatNum(Util.ToDecimal(drSecJis["KASHIKATA"]));
                    bItem[item04] = Util.FormatNum(Util.ToDecimal(drSecJis["KARIKATA"]));
                }
                bItem[item05] = Util.FormatNum(Util.ToDecimal(drSecJis["ZANDAKA"]));

                item_last = item05;

                // 計算区分＝3の時の当期増減額の逆算
                if (Util.ToInt(drMei["SHUKEI_KUBUN"]) == 3)
                {
                    decimal zogen = Util.ToDecimal(bItem[item05]) - Util.ToDecimal(bItem[item02]);
                    if (zogen >= 0)
                    {
                        bItem[item03] = Util.FormatNum(zogen);
                        bItem[item04] = "0";
                        karikata = "0";
                        kashikata = Util.FormatNum(zogen);
                    }
                    else
                    {
                        bItem[item03] = "0";
                        bItem[item04] = Util.FormatNum(Math.Abs(zogen));
                        karikata = Util.FormatNum(Math.Abs(zogen));
                        kashikata = "0";
                    }
                    item_last = item05;
                }
                if (kamokuBunrui == 99910)
                {
                    DataRow misyobunJoyo = _dtJis.NewRow();
                    misyobunJoyo["KANJO_KAMOKU_CD"] = "99910";
                    misyobunJoyo["A_TAISHAKU_KUBUN"] = Util.ToInt(drMei["TAISHAKU_KUBUN"]);
                    misyobunJoyo["TAISHAKU_KUBUN"] = Util.ToInt(drMei["TAISHAKU_KUBUN"]);
                    misyobunJoyo["ZENZAN"] = Util.ToDecimal(drSecJis["ZENZAN"]);
                    misyobunJoyo["KARIKATA"] = karikata;
                    misyobunJoyo["KASHIKATA"] = kashikata;
                    misyobunJoyo["ZANDAKA"] = Util.ToDecimal(drSecJis["ZANDAKA"]);

                    _dtJis.Rows.Add(misyobunJoyo);

                    DataRow misyobunJoyonm = _dtKmk1.NewRow();
                    misyobunJoyonm["CHOHYO_BUNRUI"] = "1";
                    misyobunJoyonm["KAMOKU_BUNRUI"] = "99910";
                    misyobunJoyonm["GYO_BANGO"] = "0";
                    misyobunJoyonm["KANJO_KAMOKU_CD"] = "99910";
                    misyobunJoyonm["KANJO_KAMOKU_NM"] = "当期未処分剰余金";

                    _dtKmk1.Rows.Add(misyobunJoyonm);
                }
            }

            // ブランクレコード作成
            for (int i = item05 + 1; i <= totalNo; i++)
            {
                bItem[i] = "";
                item_last = i;
            }

            if (flg)
            {
                alParamsPrZmTbl = SetPrZmTblParams(groupNo, bItem);
                this.Dba.Insert("PR_ZM_TBL", (DbParamCollection)alParamsPrZmTbl[0]);
            }

            returnList.Add(++item_last);
            returnList.Add(bItem);

            return returnList;
        }


        /// <summary>
        /// セクション別印刷ワークデータ作成
        /// </summary>
        /// <param name="groupNo">グループ番号</param>
        /// <param name="edaNo">セクション枝番</param>
        /// <param name="secNm">セクション名称</param>
        /// <param name="hyojiJuni">TB明細表設定テーブル参照キー(表示順位)</param>
        /// <param name="kamokuBunrui">TB明細表設定テーブル参照キー(科目分類)</param>
        private void MakeSectionData03(int groupNo, int edaNo, int hyojiJuni, int kamokuBunrui)
        {
            // 明細表科目分類データ参照
             DataRow[] fRowsMei = _dtMei.Select("HYOJI_JUNI = " + Util.ToString(hyojiJuni)
                                        + " AND KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui));
            if (fRowsMei.Length != 1)
            {
                //Msg.Info("設定データにエラーがあります。");
                return;
            }
            DataRow drMei = fRowsMei[0];

            string[] bItem;     // 明細情報格納用

            ArrayList alParamsPrZmTbl;          // ワークテーブルINSERT用パラメータ
            
            // 明細情報レコードを出力
            int gyoMax = Util.ToInt(drMei["MEISAI_KOMOKUSU"]);      // 明細表設定テーブルの明細項目数だけ行出力
            int row_item_su = 5;
            int item01 = 0;
            int item02 = 1;
            int item03 = 2;
            int item04 = 3;
            int item05 = 4;
            //bItem = new string[54];
            bItem = new string[111];
            if (gyoMax > 0)
            {
                int gyoCnt = 0;

                // 科目情報データをGYO_BANGO順に取得
                DataRow[] fRowsKmk = _dtKmk1.Select("KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui), "GYO_BANGO");

                int curGyoBango = 0;
                // 明細データの作成
                foreach (DataRow drKmk in fRowsKmk)
                {
                    // 行番号ブレイクの時だけワークレコード書出し
                    if (curGyoBango != Util.ToInt(drKmk["GYO_BANGO"]))
                    {
                        curGyoBango = Util.ToInt(drKmk["GYO_BANGO"]);       // カレント行番号

                        //bItem = new string[6];
                        bItem[item01] = Util.ToString(drKmk["KANJO_KAMOKU_NM"]); // TB_試算表科目設定.勘定科目名

                        DataRow dr = GetDrJisByKamokuBunruiGyoBango(Util.ToString(drKmk["KAMOKU_BUNRUI"]), curGyoBango);
                        // 実績なし科目の出力判定
                        if (rdoOpt2.Checked
                            || Util.ToDecimal(dr["ZENZAN"]) > 0
                            || Util.ToDecimal(dr["KARIKATA"]) > 0
                            || Util.ToDecimal(dr["KASHIKATA"]) > 0
                            || Util.ToDecimal(dr["ZANDAKA"]) > 0)
                        {
                            bItem[item02] = Util.FormatNum(dr["ZENZAN"]);
                            if (Util.ToInt(drMei["TAISHAKU_KUBUN"]) == 1)
                            {
                                bItem[item03] = Util.FormatNum(dr["KARIKATA"]);
                                bItem[item04] = Util.FormatNum(dr["KASHIKATA"]);
                            }
                            else
                            {
                                bItem[item03] = Util.FormatNum(dr["KASHIKATA"]);
                                bItem[item04] = Util.FormatNum(dr["KARIKATA"]);
                            }
                            bItem[item05] = Util.FormatNum(dr["ZANDAKA"]);

                            // ワークレコード書出し
                            gyoCnt++;
                        }
                    }

                    item01 += row_item_su;
                    item02 += row_item_su;
                    item03 += row_item_su;
                    item04 += row_item_su;
                    item05 += row_item_su;

                    // 明細出力終了判定
                    if (gyoCnt >= gyoMax) break;
                }

                // ブランクレコード作成
                while (gyoCnt < gyoMax)
                {
                    //bItem = new string[6];

                    bItem[item01] = "";
                    bItem[item02] = "";
                    bItem[item03] = "";
                    bItem[item04] = "";
                    bItem[item05] = "";

                    item01 += row_item_su;
                    item02 += row_item_su;
                    item03 += row_item_su;
                    item04 += row_item_su;
                    item05 += row_item_su;

                    gyoCnt++;

                }
            }

            // 明細表設定レコード行(合計行)の出力
            if (Util.ToInt(drMei["INJI_KUBUN"]) == 1)
            {
                //bItem = new string[6];

                bItem[item01] = Util.ToString(drMei["KAMOKU_BUNRUI_NM"]); // TB_明細表科目設定.科目分類名

                // 集計計算式より実績集計
                DataRow drSecJis = GetDrJisKeisanshikiValue(Util.ToString(drMei["SHUKEI_KEISANSHIKI"]));
                bItem[item02] = Util.FormatNum(Util.ToDecimal(drSecJis["ZENZAN"]));
                if (Util.ToInt(drMei["TAISHAKU_KUBUN"]) == 1)
                {
                    bItem[item03] = Util.FormatNum(Util.ToDecimal(drSecJis["KARIKATA"]));
                    bItem[item04] = Util.FormatNum(Util.ToDecimal(drSecJis["KASHIKATA"]));
                }
                else
                {
                    bItem[item03] = Util.FormatNum(Util.ToDecimal(drSecJis["KASHIKATA"]));
                    bItem[item04] = Util.FormatNum(Util.ToDecimal(drSecJis["KARIKATA"]));
                }
                bItem[item05] = Util.FormatNum(Util.ToDecimal(drSecJis["ZANDAKA"]));

                // 計算区分＝3の時の当期増減額の逆算
                if (Util.ToInt(drMei["SHUKEI_KUBUN"]) == 3)
                {
                    decimal zogen = Util.ToDecimal(bItem[item05]) - Util.ToDecimal(bItem[item02]);
                    if (zogen >= 0)
                    {
                        bItem[item03] = Util.FormatNum(zogen);
                        bItem[item04] = "0";
                    }
                    else
                    {
                        bItem[item03] = "0";
                        bItem[item04] = Util.FormatNum(Math.Abs(zogen));
                    }
                    item01 += row_item_su;
                    item02 += row_item_su;
                    item03 += row_item_su;
                    item04 += row_item_su;
                    item05 += row_item_su;
                }
            }

            // ブランクレコード作成
            for (int i = item05 + 1; i < 111; i++)
            {
                bItem[i] = "";
            }

            alParamsPrZmTbl = SetPrZmTblParams(groupNo, bItem);
            this.Dba.Insert("PR_ZM_TBL", (DbParamCollection)alParamsPrZmTbl[0]);
        }

        /// <summary>
        /// セクション別印刷ワークデータ作成(例外処理)
        /// </summary>
        /// <param name="secNo">セクション番号</param>
        /// <param name="edaNo">セクション枝番</param>
        /// <param name="secNm">セクション名称</param>
        /// <param name="hyojiJuni">TB明細表設定テーブル参照キー(表示順位)</param>
        /// <param name="kamokuBunrui">TB明細表設定テーブル参照キー(科目分類)</param>
        private void MakeSectionDataEx(int secNo, int edaNo, string secNm, int hyojiJuni, int kamokuBunrui)
        {
            // 明細表科目分類データ参照
            DataRow[] fRowsMei = _dtMei.Select("HYOJI_JUNI = " + Util.ToString(hyojiJuni)
                                       + " AND KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui));
            if (fRowsMei.Length != 1)
            {
                //Msg.Info("設定データにエラーがあります。");
                return;
            }
            DataRow drMei = fRowsMei[0];

            // 制御情報をセット
            string[] cItem = new string[3];
            cItem[0] = Util.ToString(drMei["CHOHYO_KUBUN"]);                    // 帳票区分：ページレベルとして利用
            cItem[1] = Util.ToString(secNo);                                    // セクション番号
            cItem[2] = Util.ToString(edaNo);                                    // セクション枝番

            // ヘッダー情報をセット
            string[] hItem = new string[6];
            hItem[0] = secNm;                                                   // セクション見出し
            hItem[1] = "科　　　　目";                                           // 列見出し
            hItem[2] = "前年度末残高";
            hItem[3] = "本年度増加額";
            hItem[4] = "本年度減少額";
            hItem[5] = "本年度末残高";

            // グループレベル情報をセット
            string[] gItem = new string[1];

            // 明細情報
            string[] bItem = new string[6];     // 明細情報格納用
            
            // セクション８設定レコード行(合計行)をかなり強引に出力
            if (secNo == 8)
            {
                bItem[0] = Util.ToString(drMei["KAMOKU_BUNRUI_NM"]); // TB_明細表科目設定.科目分類名

                // 作成済出力ワークより集計実行
                StringBuilder sql = new StringBuilder();
                sql.AppendLine("SELECT * FROM PR_ZM_TBL");
                sql.AppendLine(" WHERE GUID = '" + this.UnqId + "'");
                sql.AppendLine(" AND ITEM02 = '8'");                    // セクション８
                sql.AppendLine(" AND SORT < " + Util.ToString(_sort));  // 最後の追加行は剰余金内訳行なので含まない(無理やり)
                DataTable dt = this.Dba.GetDataTableFromSql(Util.ToString(sql));
                decimal[] gokei = new decimal[4];
                foreach (DataRow dr in dt.Rows)
                {
                    gokei[0] += Util.ToDecimal(Util.ToString(dr["ITEM32"]));
                    gokei[1] += Util.ToDecimal(Util.ToString(dr["ITEM33"]));
                    gokei[2] += Util.ToDecimal(Util.ToString(dr["ITEM34"]));
                    gokei[3] += Util.ToDecimal(Util.ToString(dr["ITEM35"]));
                }

                // 明細情報をセット
                bItem[1] = Util.FormatNum(gokei[0]);
                bItem[2] = Util.FormatNum(gokei[1]);
                bItem[3] = Util.FormatNum(gokei[2]);
                bItem[4] = Util.FormatNum(gokei[3]);

                // 定義合計レコード書出し
                //alParamsPrZmTbl = SetPrZmTblParams(cItem, hItem, gItem, bItem);
                //alParamsPrZmTbl = SetPrZmTblParams(cItem, gItem, bItem);
                //this.Dba.Insert("PR_ZM_TBL", (DbParamCollection)alParamsPrZmTbl[0]);
            }
        }

        /// <summary>
        /// 試算表科目設定の科目分類・行番号別の実績集計DataRowを取得
        /// </summary>
        /// <param name="kamokuBunrui">科目分類コード</param>
        /// <param name="gyoBango">試算表科目設定の行番号</param>
        /// <returns>DataRowフィールド名：ZENZAN KARIKATA KASHIKATA ZANDAKA</returns>
        private DataRow GetDrJisByKamokuBunruiGyoBango(string kamokuBunrui, int gyoBango)
        {
            // 集計結果返却用DataRow定義
            DataTable dt = new DataTable();
            dt.Columns.Add("ZENZAN", Type.GetType("System.Decimal"));
            dt.Columns.Add("KARIKATA", Type.GetType("System.Decimal"));
            dt.Columns.Add("KASHIKATA", Type.GetType("System.Decimal"));
            dt.Columns.Add("ZANDAKA", Type.GetType("System.Decimal"));
            DataRow dr = dt.NewRow();
            // 初期化
            dr["ZENZAN"] = 0;
            dr["KARIKATA"] = 0;
            dr["KASHIKATA"] = 0;
            dr["ZANDAKA"] = 0;

            // 科目情報データを取得
            DataRow[] fRowsKmk = _dtKmk1.Select("KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui)
                                                + "AND GYO_BANGO = " + Util.ToString(gyoBango));
            // 科目実績を戻り値DataRowへ加算
            foreach (DataRow drKmk in fRowsKmk)
            {
                DataRow[] fRowsJis = _dtJis.Select("KANJO_KAMOKU_CD = " + Util.ToString(drKmk["KANJO_KAMOKU_CD"]));
                foreach (DataRow drJis in fRowsJis)
                {
                    dr["ZENZAN"] = Util.ToDecimal(dr["ZENZAN"]) + Util.ToDecimal(drJis["ZENZAN"]);
                    dr["KARIKATA"] = Util.ToDecimal(dr["KARIKATA"]) + Util.ToDecimal(drJis["KARIKATA"]);
                    dr["KASHIKATA"] = Util.ToDecimal(dr["KASHIKATA"]) + Util.ToDecimal(drJis["KASHIKATA"]);
                    dr["ZANDAKA"] = Util.ToDecimal(dr["ZANDAKA"]) + Util.ToDecimal(drJis["ZANDAKA"]);
                }
            }

            return dr;
        }

        /// <summary>
        /// 集計計算式の実績集計DataRowを取得
        /// </summary>
        /// <param name="shukeiKeisanshiki">集計計算式</param>
        /// <returns>DataRowフィールド名：ZENZAN KARIKATA KASHIKATA ZANDAKA</returns>
        private DataRow GetDrJisKeisanshikiValue(string shukeiKeisanshiki)
        {
            // 集計結果返却用DataRow定義
            DataTable dt = new DataTable();
            dt.Columns.Add("ZENZAN", Type.GetType("System.Decimal"));
            dt.Columns.Add("KARIKATA", Type.GetType("System.Decimal"));
            dt.Columns.Add("KASHIKATA", Type.GetType("System.Decimal"));
            dt.Columns.Add("ZANDAKA", Type.GetType("System.Decimal"));
            DataRow dr = dt.NewRow();
            // 初期化
            dr["ZENZAN"] = 0;
            dr["KARIKATA"] = 0;
            dr["KASHIKATA"] = 0;
            dr["ZANDAKA"] = 0;

            // 引数の計算式から科目分類コードと加減情報を取得
            string shiki = shukeiKeisanshiki;
            if (shiki.Substring(0, 1) != "+" && shiki.Substring(0, 1) != "-")
            {
                shiki = "+" + shiki;                                    // 先頭科目分類コードへ加減文字を付加
            }

            // 科目分類値の要素数
            int itemCount = shiki.Length - Regex.Replace(shiki, "[+-]", "").Length;
            // 計算式を科目分類値と加減情報に分割
            int[] sign = new int[itemCount];
            int[] bunruiCd = new int[itemCount];
            for (int i = 0; i < itemCount; i++)
            {
                // 加減サインを配列へセット
                sign[i] = (shiki.Substring(0, 1) == "+") ? 1 : -1;
                shiki = shiki.Substring(1);                             // 計算式を加減サイン抽出後文字列へ編集

                // 科目分類コードを配列へセット
                Match m = Regex.Match(shiki, "[+-]");
                if (m.Index > 0)
                {
                    bunruiCd[i] = Util.ToInt(shiki.Substring(0, m.Index));
                }
                else
                {
                    bunruiCd[i] = Util.ToInt(shiki);
                }
                shiki = shiki.Substring(m.Index);                       // 計算式を科目分類コード抽出後文字列へ編集
            }


            // 集計演算を実行
            decimal zenzan;
            decimal karikata;
            decimal kashikata;
            decimal zandaka;

            // 集計演算を実行
            for (int i = 0; i < itemCount; i++)
            {
                DataRow[] fRowsKmk;
                DataRow[] fRowsMei;

                // 明細表科目分類データに名称があれば集計
                fRowsMei = _dtMei.Select("KAMOKU_BUNRUI = " + Util.ToString(bunruiCd[i]));
                if (fRowsMei.Length > 0)
                {
                    // 科目情報データから該当する科目分類の勘定科目レコードを抽出
                    fRowsKmk = _dtKmk1.Select("KAMOKU_BUNRUI = " + Util.ToString(bunruiCd[i]));

                    // 科目実績を戻り値DataRowへ加算
                    foreach (DataRow drKmk in fRowsKmk)
                    {
                        // 科目実績データを加減

                        DataRow[] fRowsJis = _dtJis.Select("KANJO_KAMOKU_CD = " + Util.ToString(drKmk["KANJO_KAMOKU_CD"]));
                        foreach (DataRow drJis in fRowsJis)
                        {
                            if (Util.ToDecimal(drJis["A_TAISHAKU_KUBUN"]) == 2 && Util.ToDecimal(drJis["TAISHAKU_KUBUN"]) == 2 && sign[i] == -1)
                            {
                                zenzan = Util.ToDecimal(drJis["ZENZAN"]) * -1;
                                karikata = Util.ToDecimal(drJis["KARIKATA"]) * -1;
                                kashikata = Util.ToDecimal(drJis["KASHIKATA"]) * -1;
                                zandaka = Util.ToDecimal(drJis["ZANDAKA"]) * -1;
                            }
                            else
                            {
                                zenzan = Util.ToDecimal(drJis["ZENZAN"]);
                                karikata = Util.ToDecimal(drJis["KARIKATA"]);
                                kashikata = Util.ToDecimal(drJis["KASHIKATA"]);
                                zandaka = Util.ToDecimal(drJis["ZANDAKA"]);
                            }
                            dr["ZENZAN"] = Util.ToDecimal(dr["ZENZAN"])
                                + zenzan * Util.ToInt(sign[i]);
                            dr["KARIKATA"] = Util.ToDecimal(dr["KARIKATA"])
                                + karikata * Util.ToInt(sign[i]);
                            dr["KASHIKATA"] = Util.ToDecimal(dr["KASHIKATA"])
                                + kashikata * Util.ToInt(sign[i]);
                            dr["ZANDAKA"] = Util.ToDecimal(dr["ZANDAKA"])
                                + zandaka * Util.ToInt(sign[i]);
                        }
                    }
                }
            }
            return dr;
        }


        /// <summary>
        /// 集計計算式の実績集計DataRowを取得(当期未処分剰余金用)
        /// </summary>
        /// <param name="shukeiKeisanshiki">集計計算式</param>
        /// <returns>DataRowフィールド名：ZENZAN KARIKATA KASHIKATA ZANDAKA</returns>
        private DataRow GetDrJisKeisanshikiValueToki(string shukeiKeisanshiki)
        {
            // 集計結果返却用DataRow定義
            DataTable dt = new DataTable();
            dt.Columns.Add("ZENZAN", Type.GetType("System.Decimal"));
            dt.Columns.Add("KARIKATA", Type.GetType("System.Decimal"));
            dt.Columns.Add("KASHIKATA", Type.GetType("System.Decimal"));
            dt.Columns.Add("ZANDAKA", Type.GetType("System.Decimal"));
            DataRow dr = dt.NewRow();
            // 初期化
            dr["ZENZAN"] = 0;
            dr["KARIKATA"] = 0;
            dr["KASHIKATA"] = 0;
            dr["ZANDAKA"] = 0;

            // 引数の計算式から科目分類コードと加減情報を取得
            string shiki = shukeiKeisanshiki;
            if (shiki.Substring(0, 1) != "+" && shiki.Substring(0, 1) != "-")
            {
                shiki = "+" + shiki;                                    // 先頭科目分類コードへ加減文字を付加
            }

            // 科目分類値の要素数
            int itemCount = shiki.Length - Regex.Replace(shiki, "[+-]", "").Length;
            // 計算式を科目分類値と加減情報に分割
            int[] sign = new int[itemCount];
            int[] bunruiCd = new int[itemCount];
            for (int i = 0; i < itemCount; i++)
            {
                // 加減サインを配列へセット
                sign[i] = (shiki.Substring(0, 1) == "+") ? 1 : -1;
                shiki = shiki.Substring(1);                             // 計算式を加減サイン抽出後文字列へ編集

                // 科目分類コードを配列へセット
                Match m = Regex.Match(shiki, "[+-]");
                if (m.Index > 0)
                {
                    bunruiCd[i] = Util.ToInt(shiki.Substring(0, m.Index));
                }
                else
                {
                    bunruiCd[i] = Util.ToInt(shiki);
                }
                shiki = shiki.Substring(m.Index);                       // 計算式を科目分類コード抽出後文字列へ編集
            }


            // 集計演算を実行
            decimal zenzan;
            decimal karikata;
            decimal kashikata;
            decimal zandaka;

            // 集計演算を実行
            for (int i = 0; i < itemCount; i++)
            {
                DataRow[] fRowsKmk;

                DataRow[] fRowsMei;

                // 明細表科目分類データに名称があれば集計
                fRowsMei = _dtMei.Select("KAMOKU_BUNRUI = " + Util.ToString(bunruiCd[i]));
                if (fRowsMei.Length > 0)
                {
                    // 科目情報データから該当する科目分類の勘定科目レコードを抽出
                    fRowsKmk = _dtKmk2.Select("KAMOKU_BUNRUI = " + Util.ToString(bunruiCd[i]));

                    // 科目実績を戻り値DataRowへ加算
                    foreach (DataRow drKmk in fRowsKmk)
                    {
                        // 科目実績データを加減

                        DataRow[] fRowsJis = _dtJis.Select("KANJO_KAMOKU_CD = " + Util.ToString(drKmk["KANJO_KAMOKU_CD"]));
                        foreach (DataRow drJis in fRowsJis)
                        {
                            if (Util.ToDecimal(drJis["A_TAISHAKU_KUBUN"]) == 2 && Util.ToDecimal(drJis["TAISHAKU_KUBUN"]) == 2 && sign[i] == -1)
                            {
                                zenzan = Util.ToDecimal(drJis["ZENZAN"]) * -1;
                                karikata = Util.ToDecimal(drJis["KARIKATA"]) * -1;
                                kashikata = Util.ToDecimal(drJis["KASHIKATA"]) * -1;
                                zandaka = Util.ToDecimal(drJis["ZANDAKA"]) * -1;
                            }
                            else
                            {
                                zenzan = Util.ToDecimal(drJis["ZENZAN"]);
                                karikata = Util.ToDecimal(drJis["KARIKATA"]);
                                kashikata = Util.ToDecimal(drJis["KASHIKATA"]);
                                zandaka = Util.ToDecimal(drJis["ZANDAKA"]);
                            }
                            dr["ZENZAN"] = Util.ToDecimal(dr["ZENZAN"])
                                + zenzan * Util.ToInt(sign[i]);
                            dr["KARIKATA"] = Util.ToDecimal(dr["KARIKATA"])
                                + karikata * Util.ToInt(sign[i]);
                            dr["KASHIKATA"] = Util.ToDecimal(dr["KASHIKATA"])
                                + kashikata * Util.ToInt(sign[i]);
                            dr["ZANDAKA"] = Util.ToDecimal(dr["ZANDAKA"])
                                + zandaka * Util.ToInt(sign[i]);
                        }
                    }
                }
            }
            return dr;
        }

        /// <summary>
        /// PR_ZM_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="cItem">制御情報フィールド値</param>
        /// <param name="hItem">ヘッダー情報フィールド値</param>
        /// <param name="bItem">明細情報フィールド値</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        //private ArrayList SetPrZmTblParams(string[] cItem, string[] hItem, string[] gItem, string[] bItem)
        private ArrayList SetPrZmTblParams(int cItem, string[] bItem)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            _sort++;

            // 制御情報フィールド
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, _sort);
            updParam.SetParam("@ITEM01", SqlDbType.VarChar, 200, cItem); // グループ用
            // 明細情報フィールド
            updParam.SetParam("@ITEM02", SqlDbType.VarChar, 200, bItem[0]);
            updParam.SetParam("@ITEM03", SqlDbType.VarChar, 200, bItem[1]);
            updParam.SetParam("@ITEM04", SqlDbType.VarChar, 200, bItem[2]);
            updParam.SetParam("@ITEM05", SqlDbType.VarChar, 200, bItem[3]);
            updParam.SetParam("@ITEM06", SqlDbType.VarChar, 200, bItem[4]);
            updParam.SetParam("@ITEM07", SqlDbType.VarChar, 200, bItem[5]);
            updParam.SetParam("@ITEM08", SqlDbType.VarChar, 200, bItem[6]);
            updParam.SetParam("@ITEM09", SqlDbType.VarChar, 200, bItem[7]);
            updParam.SetParam("@ITEM10", SqlDbType.VarChar, 200, bItem[8]);
            updParam.SetParam("@ITEM11", SqlDbType.VarChar, 200, bItem[9]);
            updParam.SetParam("@ITEM12", SqlDbType.VarChar, 200, bItem[10]);
            updParam.SetParam("@ITEM13", SqlDbType.VarChar, 200, bItem[11]);
            updParam.SetParam("@ITEM14", SqlDbType.VarChar, 200, bItem[12]);
            updParam.SetParam("@ITEM15", SqlDbType.VarChar, 200, bItem[13]);
            updParam.SetParam("@ITEM16", SqlDbType.VarChar, 200, bItem[14]);
            updParam.SetParam("@ITEM17", SqlDbType.VarChar, 200, bItem[15]);
            updParam.SetParam("@ITEM18", SqlDbType.VarChar, 200, bItem[16]);
            updParam.SetParam("@ITEM19", SqlDbType.VarChar, 200, bItem[17]);
            updParam.SetParam("@ITEM20", SqlDbType.VarChar, 200, bItem[18]);
            updParam.SetParam("@ITEM21", SqlDbType.VarChar, 200, bItem[19]);
            updParam.SetParam("@ITEM22", SqlDbType.VarChar, 200, bItem[20]);
            updParam.SetParam("@ITEM23", SqlDbType.VarChar, 200, bItem[21]);
            updParam.SetParam("@ITEM24", SqlDbType.VarChar, 200, bItem[22]);
            updParam.SetParam("@ITEM25", SqlDbType.VarChar, 200, bItem[23]);
            updParam.SetParam("@ITEM26", SqlDbType.VarChar, 200, bItem[24]);
            updParam.SetParam("@ITEM27", SqlDbType.VarChar, 200, bItem[25]);
            updParam.SetParam("@ITEM28", SqlDbType.VarChar, 200, bItem[26]);
            updParam.SetParam("@ITEM29", SqlDbType.VarChar, 200, bItem[27]);
            updParam.SetParam("@ITEM30", SqlDbType.VarChar, 200, bItem[28]);
            updParam.SetParam("@ITEM31", SqlDbType.VarChar, 200, bItem[29]);
            updParam.SetParam("@ITEM32", SqlDbType.VarChar, 200, bItem[30]);
            updParam.SetParam("@ITEM33", SqlDbType.VarChar, 200, bItem[31]);
            updParam.SetParam("@ITEM34", SqlDbType.VarChar, 200, bItem[32]);
            updParam.SetParam("@ITEM35", SqlDbType.VarChar, 200, bItem[33]);
            updParam.SetParam("@ITEM36", SqlDbType.VarChar, 200, bItem[34]);
            updParam.SetParam("@ITEM37", SqlDbType.VarChar, 200, bItem[35]);
            updParam.SetParam("@ITEM38", SqlDbType.VarChar, 200, bItem[36]);
            updParam.SetParam("@ITEM39", SqlDbType.VarChar, 200, bItem[37]);
            updParam.SetParam("@ITEM40", SqlDbType.VarChar, 200, bItem[38]);
            updParam.SetParam("@ITEM41", SqlDbType.VarChar, 200, bItem[39]);
            updParam.SetParam("@ITEM42", SqlDbType.VarChar, 200, bItem[40]);
            updParam.SetParam("@ITEM43", SqlDbType.VarChar, 200, bItem[41]);
            updParam.SetParam("@ITEM44", SqlDbType.VarChar, 200, bItem[42]);
            updParam.SetParam("@ITEM45", SqlDbType.VarChar, 200, bItem[43]);
            updParam.SetParam("@ITEM46", SqlDbType.VarChar, 200, bItem[44]);
            updParam.SetParam("@ITEM47", SqlDbType.VarChar, 200, bItem[45]);
            updParam.SetParam("@ITEM48", SqlDbType.VarChar, 200, bItem[46]);
            updParam.SetParam("@ITEM49", SqlDbType.VarChar, 200, bItem[47]);
            updParam.SetParam("@ITEM50", SqlDbType.VarChar, 200, bItem[48]);
            updParam.SetParam("@ITEM51", SqlDbType.VarChar, 200, bItem[49]);
            updParam.SetParam("@ITEM52", SqlDbType.VarChar, 200, bItem[50]);
            updParam.SetParam("@ITEM53", SqlDbType.VarChar, 200, bItem[51]);
            updParam.SetParam("@ITEM54", SqlDbType.VarChar, 200, bItem[52]);
            updParam.SetParam("@ITEM55", SqlDbType.VarChar, 200, bItem[53]);
            updParam.SetParam("@ITEM56", SqlDbType.VarChar, 200, bItem[54]);
            updParam.SetParam("@ITEM57", SqlDbType.VarChar, 200, bItem[55]);
            updParam.SetParam("@ITEM58", SqlDbType.VarChar, 200, bItem[56]);
            updParam.SetParam("@ITEM59", SqlDbType.VarChar, 200, bItem[57]);
            updParam.SetParam("@ITEM60", SqlDbType.VarChar, 200, bItem[58]);
            updParam.SetParam("@ITEM61", SqlDbType.VarChar, 200, bItem[59]);
            updParam.SetParam("@ITEM62", SqlDbType.VarChar, 200, bItem[60]);
            updParam.SetParam("@ITEM63", SqlDbType.VarChar, 200, bItem[61]);
            updParam.SetParam("@ITEM64", SqlDbType.VarChar, 200, bItem[62]);
            updParam.SetParam("@ITEM65", SqlDbType.VarChar, 200, bItem[63]);
            updParam.SetParam("@ITEM66", SqlDbType.VarChar, 200, bItem[64]);
            updParam.SetParam("@ITEM67", SqlDbType.VarChar, 200, bItem[65]);
            updParam.SetParam("@ITEM68", SqlDbType.VarChar, 200, bItem[66]);
            updParam.SetParam("@ITEM69", SqlDbType.VarChar, 200, bItem[67]);
            updParam.SetParam("@ITEM70", SqlDbType.VarChar, 200, bItem[68]);
            updParam.SetParam("@ITEM71", SqlDbType.VarChar, 200, bItem[69]);
            updParam.SetParam("@ITEM72", SqlDbType.VarChar, 200, bItem[70]);
            updParam.SetParam("@ITEM73", SqlDbType.VarChar, 200, bItem[71]);
            updParam.SetParam("@ITEM74", SqlDbType.VarChar, 200, bItem[72]);
            updParam.SetParam("@ITEM75", SqlDbType.VarChar, 200, bItem[73]);
            updParam.SetParam("@ITEM76", SqlDbType.VarChar, 200, bItem[74]);
            updParam.SetParam("@ITEM77", SqlDbType.VarChar, 200, bItem[75]);
            updParam.SetParam("@ITEM78", SqlDbType.VarChar, 200, bItem[76]);
            updParam.SetParam("@ITEM79", SqlDbType.VarChar, 200, bItem[77]);
            updParam.SetParam("@ITEM80", SqlDbType.VarChar, 200, bItem[78]);
            updParam.SetParam("@ITEM81", SqlDbType.VarChar, 200, bItem[79]);
            updParam.SetParam("@ITEM82", SqlDbType.VarChar, 200, bItem[80]);
            updParam.SetParam("@ITEM83", SqlDbType.VarChar, 200, bItem[81]);
            updParam.SetParam("@ITEM84", SqlDbType.VarChar, 200, bItem[82]);
            updParam.SetParam("@ITEM85", SqlDbType.VarChar, 200, bItem[83]);
            updParam.SetParam("@ITEM86", SqlDbType.VarChar, 200, bItem[84]);
            updParam.SetParam("@ITEM87", SqlDbType.VarChar, 200, bItem[85]);
            updParam.SetParam("@ITEM88", SqlDbType.VarChar, 200, bItem[86]);
            updParam.SetParam("@ITEM89", SqlDbType.VarChar, 200, bItem[87]);
            updParam.SetParam("@ITEM90", SqlDbType.VarChar, 200, bItem[88]);
            updParam.SetParam("@ITEM91", SqlDbType.VarChar, 200, bItem[89]);
            updParam.SetParam("@ITEM92", SqlDbType.VarChar, 200, bItem[90]);
            updParam.SetParam("@ITEM93", SqlDbType.VarChar, 200, bItem[91]);
            updParam.SetParam("@ITEM94", SqlDbType.VarChar, 200, bItem[92]);
            updParam.SetParam("@ITEM95", SqlDbType.VarChar, 200, bItem[93]);
            updParam.SetParam("@ITEM96", SqlDbType.VarChar, 200, bItem[94]);
            updParam.SetParam("@ITEM97", SqlDbType.VarChar, 200, bItem[95]);
            updParam.SetParam("@ITEM98", SqlDbType.VarChar, 200, bItem[96]);
            updParam.SetParam("@ITEM99", SqlDbType.VarChar, 200, bItem[97]);
            updParam.SetParam("@ITEM100", SqlDbType.VarChar, 200, bItem[98]);
            updParam.SetParam("@ITEM101", SqlDbType.VarChar, 200, bItem[99]);
            updParam.SetParam("@ITEM102", SqlDbType.VarChar, 200, bItem[100]);
            updParam.SetParam("@ITEM103", SqlDbType.VarChar, 200, bItem[101]);
            updParam.SetParam("@ITEM104", SqlDbType.VarChar, 200, bItem[102]);
            updParam.SetParam("@ITEM105", SqlDbType.VarChar, 200, bItem[103]);
            updParam.SetParam("@ITEM106", SqlDbType.VarChar, 200, bItem[104]);
            updParam.SetParam("@ITEM107", SqlDbType.VarChar, 200, bItem[105]);
            updParam.SetParam("@ITEM108", SqlDbType.VarChar, 200, bItem[106]);
            updParam.SetParam("@ITEM109", SqlDbType.VarChar, 200, bItem[107]);
            updParam.SetParam("@ITEM110", SqlDbType.VarChar, 200, bItem[108]);
            updParam.SetParam("@ITEM111", SqlDbType.VarChar, 200, bItem[109]);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }
        #endregion
    }
}
