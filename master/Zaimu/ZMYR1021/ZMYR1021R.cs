﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.zm.zmyr1021
{
    /// <summary>
    /// ZMYR1021R の帳票
    /// </summary>
    public partial class ZMYR1021R : BaseReport
    {

        public ZMYR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void group01_Format(object sender, EventArgs e)
        {
            if (this.groupId.Text != "1")
            {
                this.group01_H.Visible = false;
                this.group01_F.Visible = false;
            }
            else
            {
                this.group01_H.Visible = true;
                this.group01_F.Visible = true;
            }
        }

        private void group02_Format(object sender, EventArgs e)
        {
            if (this.groupId.Text != "2")
            {
                this.group02_H.Visible = false;
                this.group02_F.Visible = false;
            }
            else
            {
                this.group02_H.Visible = true;
                this.group02_F.Visible = true;
            }
        }

        private void group03_Format(object sender, EventArgs e)
        {
            if (this.groupId.Text != "3")
            {
                this.group03_H.Visible = false;
                this.group03_F.Visible = false;
            }
            else
            {
                this.group03_H.Visible = true;
                this.group03_F.Visible = true;
            }
        }

        private void group04_Format(object sender, EventArgs e)
        {
            if (this.groupId.Text != "4")
            {
                this.group04_H.Visible = false;
                this.group04_F.Visible = false;
            }
            else
            {
                this.group04_H.Visible = true;
                this.group04_F.Visible = true;
            }
        }

        private void group05_Format(object sender, EventArgs e)
        {
            if (this.groupId.Text != "5")
            {
                this.group05_H.Visible = false;
                this.group05_F.Visible = false;
            }
            else
            {
                this.group05_H.Visible = true;
                this.group05_F.Visible = true;
            }
        }

        private void group06_Format(object sender, EventArgs e)
        {
            if (this.groupId.Text != "6")
            {
                this.group06_H.Visible = false;
                this.group06_F.Visible = false;
            }
            else
            {
                this.group06_H.Visible = true;
                this.group06_F.Visible = true;
            }
        }

        private void group07_Format(object sender, EventArgs e)
        {
            if (this.groupId.Text != "7")
            {
                this.group07_H.Visible = false;
                this.group07_F.Visible = false;
            }
            else
            {
                this.group07_H.Visible = true;
                this.group07_F.Visible = true;
            }
        }

        private void group08_Format(object sender, EventArgs e)
        {
            if (this.groupId.Text != "8")
            {
                this.group08_H.Visible = false;
                this.group08_F.Visible = false;
            }
            else
            {
                this.group08_H.Visible = true;
                this.group08_F.Visible = true;
            }
        }
    }
}
