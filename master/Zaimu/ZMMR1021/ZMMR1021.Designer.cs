﻿namespace jp.co.fsi.zm.zmmr1021
{
    partial class ZMMR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKikanCodeBet = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.txtKanjoKamoku = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamoku = new System.Windows.Forms.Label();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.lblBumonTo = new System.Windows.Forms.Label();
            this.lblBumonCodeBet = new System.Windows.Forms.Label();
            this.txtBumonFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonFr = new System.Windows.Forms.Label();
            this.txtBumonTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojiTo = new System.Windows.Forms.Label();
            this.lblKojiCodeBet = new System.Windows.Forms.Label();
            this.txtKojiFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojiFr = new System.Windows.Forms.Label();
            this.txtKojiTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.rdoYes = new System.Windows.Forms.RadioButton();
            this.rdoNo = new System.Windows.Forms.RadioButton();
            this.lblDay = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.rdoZenbu = new System.Windows.Forms.RadioButton();
            this.rdoKessan = new System.Windows.Forms.RadioButton();
            this.rdoTsujo = new System.Windows.Forms.RadioButton();
            this.lblHojoKamokuTo = new System.Windows.Forms.Label();
            this.lblHojoKamokuCodeBet = new System.Windows.Forms.Label();
            this.txtHojoKamokuFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoKamokuFr = new System.Windows.Forms.Label();
            this.txtHojoKamokuTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel11.SuspendLayout();
            this.fsiPanel10.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "";
            // 
            // lblKikanCodeBet
            // 
            this.lblKikanCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblKikanCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKikanCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblKikanCodeBet.Location = new System.Drawing.Point(458, 5);
            this.lblKikanCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKikanCodeBet.Name = "lblKikanCodeBet";
            this.lblKikanCodeBet.Size = new System.Drawing.Size(20, 24);
            this.lblKikanCodeBet.TabIndex = 8;
            this.lblKikanCodeBet.Tag = "CHANGE";
            this.lblKikanCodeBet.Text = "～";
            this.lblKikanCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblDayTo.Location = new System.Drawing.Point(733, 5);
            this.lblDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(20, 24);
            this.lblDayTo.TabIndex = 16;
            this.lblDayTo.Tag = "CHANGE";
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblMonthTo.Location = new System.Drawing.Point(659, 5);
            this.lblMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthTo.MinimumSize = new System.Drawing.Size(27, 0);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(27, 24);
            this.lblMonthTo.TabIndex = 14;
            this.lblMonthTo.Tag = "CHANGE";
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearTo
            // 
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblYearTo.Location = new System.Drawing.Point(589, 5);
            this.lblYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(20, 24);
            this.lblYearTo.TabIndex = 12;
            this.lblYearTo.Tag = "CHANGE";
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtDayTo.Location = new System.Drawing.Point(689, 6);
            this.txtDayTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(39, 23);
            this.txtDayTo.TabIndex = 15;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtYearTo.Location = new System.Drawing.Point(546, 6);
            this.txtYearTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(39, 23);
            this.txtYearTo.TabIndex = 11;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtMonthTo.Location = new System.Drawing.Point(617, 6);
            this.txtMonthTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(39, 23);
            this.txtMonthTo.TabIndex = 13;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblGengoTo.Location = new System.Drawing.Point(486, 5);
            this.lblGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(55, 24);
            this.lblGengoTo.TabIndex = 10;
            this.lblGengoTo.Tag = "DISPNAME";
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDayFr
            // 
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblDayFr.Location = new System.Drawing.Point(393, 5);
            this.lblDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(20, 24);
            this.lblDayFr.TabIndex = 7;
            this.lblDayFr.Tag = "CHANGE";
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblMonthFr.Location = new System.Drawing.Point(320, 5);
            this.lblMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(20, 24);
            this.lblMonthFr.TabIndex = 5;
            this.lblMonthFr.Tag = "CHANGE";
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearFr
            // 
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblYearFr.Location = new System.Drawing.Point(249, 5);
            this.lblYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(20, 24);
            this.lblYearFr.TabIndex = 3;
            this.lblYearFr.Tag = "CHANGE";
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDayFr.Location = new System.Drawing.Point(349, 6);
            this.txtDayFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(39, 23);
            this.txtDayFr.TabIndex = 6;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtYearFr.Location = new System.Drawing.Point(207, 6);
            this.txtYearFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(39, 23);
            this.txtYearFr.TabIndex = 2;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtMonthFr.Location = new System.Drawing.Point(277, 6);
            this.txtMonthFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(39, 23);
            this.txtMonthFr.TabIndex = 4;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblGengoFr.Location = new System.Drawing.Point(147, 5);
            this.lblGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(55, 24);
            this.lblGengoFr.TabIndex = 1;
            this.lblGengoFr.Tag = "DISPNAME";
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKanjoKamoku
            // 
            this.txtKanjoKamoku.AutoSizeFromLength = false;
            this.txtKanjoKamoku.DisplayLength = null;
            this.txtKanjoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKanjoKamoku.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamoku.Location = new System.Drawing.Point(147, 6);
            this.txtKanjoKamoku.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamoku.MaxLength = 6;
            this.txtKanjoKamoku.Name = "txtKanjoKamoku";
            this.txtKanjoKamoku.Size = new System.Drawing.Size(65, 23);
            this.txtKanjoKamoku.TabIndex = 0;
            this.txtKanjoKamoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamoku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamoku_Validating);
            // 
            // lblKanjoKamoku
            // 
            this.lblKanjoKamoku.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoKamoku.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamoku.Location = new System.Drawing.Point(215, 5);
            this.lblKanjoKamoku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamoku.Name = "lblKanjoKamoku";
            this.lblKanjoKamoku.Size = new System.Drawing.Size(227, 24);
            this.lblKanjoKamoku.TabIndex = 1;
            this.lblKanjoKamoku.Tag = "DISPNAME";
            this.lblKanjoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.BackColor = System.Drawing.Color.Silver;
            this.rdoZeinuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoZeinuki.ForeColor = System.Drawing.Color.Black;
            this.rdoZeinuki.Location = new System.Drawing.Point(245, 7);
            this.rdoZeinuki.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeinuki.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(58, 24);
            this.rdoZeinuki.TabIndex = 1;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Tag = "CHANGE";
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = false;
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.BackColor = System.Drawing.Color.Silver;
            this.rdoZeikomi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoZeikomi.ForeColor = System.Drawing.Color.Black;
            this.rdoZeikomi.Location = new System.Drawing.Point(147, 7);
            this.rdoZeikomi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeikomi.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(58, 24);
            this.rdoZeikomi.TabIndex = 0;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Tag = "CHANGE";
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = false;
            // 
            // lblBumonTo
            // 
            this.lblBumonTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblBumonTo.ForeColor = System.Drawing.Color.Black;
            this.lblBumonTo.Location = new System.Drawing.Point(554, 4);
            this.lblBumonTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonTo.Name = "lblBumonTo";
            this.lblBumonTo.Size = new System.Drawing.Size(227, 24);
            this.lblBumonTo.TabIndex = 3;
            this.lblBumonTo.Tag = "DISPNAME";
            this.lblBumonTo.Text = "最　後";
            this.lblBumonTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCodeBet
            // 
            this.lblBumonCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblBumonCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblBumonCodeBet.Location = new System.Drawing.Point(458, 5);
            this.lblBumonCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonCodeBet.Name = "lblBumonCodeBet";
            this.lblBumonCodeBet.Size = new System.Drawing.Size(20, 24);
            this.lblBumonCodeBet.TabIndex = 2;
            this.lblBumonCodeBet.Tag = "CHANGE";
            this.lblBumonCodeBet.Text = "～";
            this.lblBumonCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonFr
            // 
            this.txtBumonFr.AutoSizeFromLength = false;
            this.txtBumonFr.DisplayLength = null;
            this.txtBumonFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtBumonFr.ForeColor = System.Drawing.Color.Black;
            this.txtBumonFr.Location = new System.Drawing.Point(147, 6);
            this.txtBumonFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtBumonFr.MaxLength = 4;
            this.txtBumonFr.Name = "txtBumonFr";
            this.txtBumonFr.Size = new System.Drawing.Size(65, 23);
            this.txtBumonFr.TabIndex = 0;
            this.txtBumonFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonFr_Validating);
            // 
            // lblBumonFr
            // 
            this.lblBumonFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblBumonFr.ForeColor = System.Drawing.Color.Black;
            this.lblBumonFr.Location = new System.Drawing.Point(215, 5);
            this.lblBumonFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonFr.Name = "lblBumonFr";
            this.lblBumonFr.Size = new System.Drawing.Size(227, 24);
            this.lblBumonFr.TabIndex = 1;
            this.lblBumonFr.Tag = "DISPNAME";
            this.lblBumonFr.Text = "先　頭";
            this.lblBumonFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonTo
            // 
            this.txtBumonTo.AutoSizeFromLength = false;
            this.txtBumonTo.DisplayLength = null;
            this.txtBumonTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtBumonTo.ForeColor = System.Drawing.Color.Black;
            this.txtBumonTo.Location = new System.Drawing.Point(486, 5);
            this.txtBumonTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtBumonTo.MaxLength = 4;
            this.txtBumonTo.Name = "txtBumonTo";
            this.txtBumonTo.Size = new System.Drawing.Size(65, 23);
            this.txtBumonTo.TabIndex = 3;
            this.txtBumonTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonTo_Validating);
            // 
            // lblKojiTo
            // 
            this.lblKojiTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblKojiTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojiTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKojiTo.ForeColor = System.Drawing.Color.Black;
            this.lblKojiTo.Location = new System.Drawing.Point(554, 6);
            this.lblKojiTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKojiTo.Name = "lblKojiTo";
            this.lblKojiTo.Size = new System.Drawing.Size(227, 24);
            this.lblKojiTo.TabIndex = 4;
            this.lblKojiTo.Tag = "DISPNAME";
            this.lblKojiTo.Text = "最　後";
            this.lblKojiTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblKojiTo.Visible = false;
            // 
            // lblKojiCodeBet
            // 
            this.lblKojiCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblKojiCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKojiCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblKojiCodeBet.Location = new System.Drawing.Point(458, 7);
            this.lblKojiCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKojiCodeBet.Name = "lblKojiCodeBet";
            this.lblKojiCodeBet.Size = new System.Drawing.Size(20, 24);
            this.lblKojiCodeBet.TabIndex = 2;
            this.lblKojiCodeBet.Tag = "CHANGE";
            this.lblKojiCodeBet.Text = "～";
            this.lblKojiCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblKojiCodeBet.Visible = false;
            // 
            // txtKojiFr
            // 
            this.txtKojiFr.AutoSizeFromLength = false;
            this.txtKojiFr.DisplayLength = null;
            this.txtKojiFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKojiFr.ForeColor = System.Drawing.Color.Black;
            this.txtKojiFr.Location = new System.Drawing.Point(147, 7);
            this.txtKojiFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtKojiFr.MaxLength = 4;
            this.txtKojiFr.Name = "txtKojiFr";
            this.txtKojiFr.Size = new System.Drawing.Size(58, 23);
            this.txtKojiFr.TabIndex = 0;
            this.txtKojiFr.Visible = false;
            this.txtKojiFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojiFr_Validating);
            // 
            // lblKojiFr
            // 
            this.lblKojiFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblKojiFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojiFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKojiFr.ForeColor = System.Drawing.Color.Black;
            this.lblKojiFr.Location = new System.Drawing.Point(209, 6);
            this.lblKojiFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKojiFr.Name = "lblKojiFr";
            this.lblKojiFr.Size = new System.Drawing.Size(227, 24);
            this.lblKojiFr.TabIndex = 1;
            this.lblKojiFr.Tag = "DISPNAME";
            this.lblKojiFr.Text = "先　頭";
            this.lblKojiFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblKojiFr.Visible = false;
            // 
            // txtKojiTo
            // 
            this.txtKojiTo.AutoSizeFromLength = false;
            this.txtKojiTo.DisplayLength = null;
            this.txtKojiTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKojiTo.ForeColor = System.Drawing.Color.Black;
            this.txtKojiTo.Location = new System.Drawing.Point(486, 6);
            this.txtKojiTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtKojiTo.MaxLength = 4;
            this.txtKojiTo.Name = "txtKojiTo";
            this.txtKojiTo.Size = new System.Drawing.Size(65, 23);
            this.txtKojiTo.TabIndex = 3;
            this.txtKojiTo.Visible = false;
            this.txtKojiTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojiTo_Validating);
            // 
            // rdoYes
            // 
            this.rdoYes.AutoSize = true;
            this.rdoYes.BackColor = System.Drawing.Color.Silver;
            this.rdoYes.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoYes.ForeColor = System.Drawing.Color.Black;
            this.rdoYes.Location = new System.Drawing.Point(265, 7);
            this.rdoYes.Margin = new System.Windows.Forms.Padding(4);
            this.rdoYes.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoYes.Name = "rdoYes";
            this.rdoYes.Size = new System.Drawing.Size(58, 24);
            this.rdoYes.TabIndex = 1;
            this.rdoYes.TabStop = true;
            this.rdoYes.Tag = "CHANGE";
            this.rdoYes.Text = "する";
            this.rdoYes.UseVisualStyleBackColor = false;
            // 
            // rdoNo
            // 
            this.rdoNo.AutoSize = true;
            this.rdoNo.BackColor = System.Drawing.Color.Silver;
            this.rdoNo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoNo.ForeColor = System.Drawing.Color.Black;
            this.rdoNo.Location = new System.Drawing.Point(183, 7);
            this.rdoNo.Margin = new System.Windows.Forms.Padding(4);
            this.rdoNo.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoNo.Name = "rdoNo";
            this.rdoNo.Size = new System.Drawing.Size(74, 24);
            this.rdoNo.TabIndex = 0;
            this.rdoNo.TabStop = true;
            this.rdoNo.Tag = "CHANGE";
            this.rdoNo.Text = "しない";
            this.rdoNo.UseVisualStyleBackColor = false;
            // 
            // lblDay
            // 
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDay.ForeColor = System.Drawing.Color.Black;
            this.lblDay.Location = new System.Drawing.Point(393, 9);
            this.lblDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(20, 24);
            this.lblDay.TabIndex = 6;
            this.lblDay.Tag = "CHANGE";
            this.lblDay.Text = "日";
            this.lblDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonth
            // 
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMonth.ForeColor = System.Drawing.Color.Black;
            this.lblMonth.Location = new System.Drawing.Point(320, 9);
            this.lblMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(20, 24);
            this.lblMonth.TabIndex = 4;
            this.lblMonth.Tag = "CHANGE";
            this.lblMonth.Text = "月";
            this.lblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYear
            // 
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblYear.ForeColor = System.Drawing.Color.Black;
            this.lblYear.Location = new System.Drawing.Point(249, 9);
            this.lblYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(20, 24);
            this.lblYear.TabIndex = 2;
            this.lblYear.Tag = "CHANGE";
            this.lblYear.Text = "年";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = false;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDay.ForeColor = System.Drawing.Color.Black;
            this.txtDay.Location = new System.Drawing.Point(349, 10);
            this.txtDay.Margin = new System.Windows.Forms.Padding(4);
            this.txtDay.MaxLength = 2;
            this.txtDay.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(39, 23);
            this.txtDay.TabIndex = 5;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDay_KeyDown);
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // txtYear
            // 
            this.txtYear.AutoSizeFromLength = false;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtYear.ForeColor = System.Drawing.Color.Black;
            this.txtYear.Location = new System.Drawing.Point(207, 10);
            this.txtYear.Margin = new System.Windows.Forms.Padding(4);
            this.txtYear.MaxLength = 2;
            this.txtYear.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(39, 23);
            this.txtYear.TabIndex = 1;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMonth.ForeColor = System.Drawing.Color.Black;
            this.txtMonth.Location = new System.Drawing.Point(277, 10);
            this.txtMonth.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(39, 23);
            this.txtMonth.TabIndex = 3;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblGengo.ForeColor = System.Drawing.Color.Black;
            this.lblGengo.Location = new System.Drawing.Point(147, 9);
            this.lblGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(55, 24);
            this.lblGengo.TabIndex = 0;
            this.lblGengo.Tag = "DISPNAME";
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoZenbu
            // 
            this.rdoZenbu.AutoSize = true;
            this.rdoZenbu.BackColor = System.Drawing.Color.Silver;
            this.rdoZenbu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoZenbu.ForeColor = System.Drawing.Color.Black;
            this.rdoZenbu.Location = new System.Drawing.Point(362, 8);
            this.rdoZenbu.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZenbu.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZenbu.Name = "rdoZenbu";
            this.rdoZenbu.Size = new System.Drawing.Size(74, 24);
            this.rdoZenbu.TabIndex = 2;
            this.rdoZenbu.TabStop = true;
            this.rdoZenbu.Tag = "CHANGE";
            this.rdoZenbu.Text = "全仕訳";
            this.rdoZenbu.UseVisualStyleBackColor = false;
            // 
            // rdoKessan
            // 
            this.rdoKessan.AutoSize = true;
            this.rdoKessan.BackColor = System.Drawing.Color.Silver;
            this.rdoKessan.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoKessan.ForeColor = System.Drawing.Color.Black;
            this.rdoKessan.Location = new System.Drawing.Point(245, 8);
            this.rdoKessan.Margin = new System.Windows.Forms.Padding(4);
            this.rdoKessan.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoKessan.Name = "rdoKessan";
            this.rdoKessan.Size = new System.Drawing.Size(90, 24);
            this.rdoKessan.TabIndex = 1;
            this.rdoKessan.TabStop = true;
            this.rdoKessan.Tag = "CHANGE";
            this.rdoKessan.Text = "決算仕訳";
            this.rdoKessan.UseVisualStyleBackColor = false;
            // 
            // rdoTsujo
            // 
            this.rdoTsujo.AutoSize = true;
            this.rdoTsujo.BackColor = System.Drawing.Color.Silver;
            this.rdoTsujo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoTsujo.ForeColor = System.Drawing.Color.Black;
            this.rdoTsujo.Location = new System.Drawing.Point(147, 8);
            this.rdoTsujo.Margin = new System.Windows.Forms.Padding(4);
            this.rdoTsujo.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoTsujo.Name = "rdoTsujo";
            this.rdoTsujo.Size = new System.Drawing.Size(90, 24);
            this.rdoTsujo.TabIndex = 0;
            this.rdoTsujo.TabStop = true;
            this.rdoTsujo.Tag = "CHANGE";
            this.rdoTsujo.Text = "通常仕訳";
            this.rdoTsujo.UseVisualStyleBackColor = false;
            // 
            // lblHojoKamokuTo
            // 
            this.lblHojoKamokuTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblHojoKamokuTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokuTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblHojoKamokuTo.ForeColor = System.Drawing.Color.Black;
            this.lblHojoKamokuTo.Location = new System.Drawing.Point(554, 6);
            this.lblHojoKamokuTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoKamokuTo.Name = "lblHojoKamokuTo";
            this.lblHojoKamokuTo.Size = new System.Drawing.Size(227, 24);
            this.lblHojoKamokuTo.TabIndex = 4;
            this.lblHojoKamokuTo.Tag = "DISPNAME";
            this.lblHojoKamokuTo.Text = "最　後";
            this.lblHojoKamokuTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHojoKamokuCodeBet
            // 
            this.lblHojoKamokuCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblHojoKamokuCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblHojoKamokuCodeBet.Location = new System.Drawing.Point(459, 5);
            this.lblHojoKamokuCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoKamokuCodeBet.Name = "lblHojoKamokuCodeBet";
            this.lblHojoKamokuCodeBet.Size = new System.Drawing.Size(20, 24);
            this.lblHojoKamokuCodeBet.TabIndex = 2;
            this.lblHojoKamokuCodeBet.Tag = "CHANGE";
            this.lblHojoKamokuCodeBet.Text = "～";
            this.lblHojoKamokuCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoKamokuFr
            // 
            this.txtHojoKamokuFr.AutoSizeFromLength = false;
            this.txtHojoKamokuFr.DisplayLength = null;
            this.txtHojoKamokuFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtHojoKamokuFr.ForeColor = System.Drawing.Color.Black;
            this.txtHojoKamokuFr.Location = new System.Drawing.Point(147, 6);
            this.txtHojoKamokuFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtHojoKamokuFr.MaxLength = 4;
            this.txtHojoKamokuFr.Name = "txtHojoKamokuFr";
            this.txtHojoKamokuFr.Size = new System.Drawing.Size(65, 23);
            this.txtHojoKamokuFr.TabIndex = 0;
            this.txtHojoKamokuFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoKamokuFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoKamokuFr_Validating);
            // 
            // lblHojoKamokuFr
            // 
            this.lblHojoKamokuFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblHojoKamokuFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokuFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblHojoKamokuFr.ForeColor = System.Drawing.Color.Black;
            this.lblHojoKamokuFr.Location = new System.Drawing.Point(215, 5);
            this.lblHojoKamokuFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoKamokuFr.Name = "lblHojoKamokuFr";
            this.lblHojoKamokuFr.Size = new System.Drawing.Size(227, 24);
            this.lblHojoKamokuFr.TabIndex = 1;
            this.lblHojoKamokuFr.Tag = "DISPNAME";
            this.lblHojoKamokuFr.Text = "先　頭";
            this.lblHojoKamokuFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoKamokuTo
            // 
            this.txtHojoKamokuTo.AutoSizeFromLength = false;
            this.txtHojoKamokuTo.DisplayLength = null;
            this.txtHojoKamokuTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtHojoKamokuTo.ForeColor = System.Drawing.Color.Black;
            this.txtHojoKamokuTo.Location = new System.Drawing.Point(486, 7);
            this.txtHojoKamokuTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtHojoKamokuTo.MaxLength = 4;
            this.txtHojoKamokuTo.Name = "txtHojoKamokuTo";
            this.txtHojoKamokuTo.Size = new System.Drawing.Size(65, 23);
            this.txtHojoKamokuTo.TabIndex = 3;
            this.txtHojoKamokuTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoKamokuTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoKamokuTo_Validating);
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(147, 5);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(195, 4);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(360, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
            this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(795, 38);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(795, 38);
            this.label1.TabIndex = 0;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "仕訳種類";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(795, 38);
            this.label2.TabIndex = 0;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "消費税処理";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(795, 38);
            this.label3.TabIndex = 0;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "期間";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(795, 34);
            this.label4.TabIndex = 0;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "勘定科目";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(795, 38);
            this.label5.TabIndex = 0;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "部門範囲";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(795, 38);
            this.label6.TabIndex = 0;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "金額がｾﾞﾛの科目を印字";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(795, 43);
            this.label7.TabIndex = 0;
            this.label7.Tag = "CHANGE";
            this.label7.Text = "出力日付";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(795, 38);
            this.label8.TabIndex = 0;
            this.label8.Tag = "CHANGE";
            this.label8.Text = "補助科目範囲";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel11, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel9, 0, 9);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 8);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 7);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 4);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(9, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 10;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999999F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(805, 476);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.rdoZeikomi);
            this.fsiPanel3.Controls.Add(this.rdoZeinuki);
            this.fsiPanel3.Controls.Add(this.label2);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 99);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(795, 38);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.rdoTsujo);
            this.fsiPanel2.Controls.Add(this.rdoKessan);
            this.fsiPanel2.Controls.Add(this.rdoZenbu);
            this.fsiPanel2.Controls.Add(this.label1);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 52);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(795, 38);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(795, 38);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel11
            // 
            this.fsiPanel11.Controls.Add(this.label10);
            this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel11.Location = new System.Drawing.Point(4, 145);
            this.fsiPanel11.Name = "fsiPanel11";
            this.fsiPanel11.Size = new System.Drawing.Size(797, 40);
            this.fsiPanel11.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Silver;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(797, 40);
            this.label10.TabIndex = 1;
            this.label10.Tag = "CHANGE";
            this.label10.Text = "範囲";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fsiPanel10
            // 
            this.fsiPanel10.Controls.Add(this.txtKojiFr);
            this.fsiPanel10.Controls.Add(this.txtKojiTo);
            this.fsiPanel10.Controls.Add(this.lblKojiCodeBet);
            this.fsiPanel10.Controls.Add(this.lblKojiTo);
            this.fsiPanel10.Controls.Add(this.lblKojiFr);
            this.fsiPanel10.Controls.Add(this.label9);
            this.fsiPanel10.Location = new System.Drawing.Point(9, 529);
            this.fsiPanel10.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel10.Name = "fsiPanel10";
            this.fsiPanel10.Size = new System.Drawing.Size(800, 36);
            this.fsiPanel10.TabIndex = 9;
            this.fsiPanel10.Tag = "CHANGE";
            this.fsiPanel10.Visible = false;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Silver;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(800, 36);
            this.label9.TabIndex = 0;
            this.label9.Tag = "CHANGE";
            this.label9.Text = "工事範囲";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label9.Visible = false;
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.lblGengo);
            this.fsiPanel9.Controls.Add(this.txtMonth);
            this.fsiPanel9.Controls.Add(this.lblMonth);
            this.fsiPanel9.Controls.Add(this.txtYear);
            this.fsiPanel9.Controls.Add(this.lblYear);
            this.fsiPanel9.Controls.Add(this.lblDay);
            this.fsiPanel9.Controls.Add(this.txtDay);
            this.fsiPanel9.Controls.Add(this.label7);
            this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel9.Location = new System.Drawing.Point(5, 428);
            this.fsiPanel9.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(795, 43);
            this.fsiPanel9.TabIndex = 8;
            this.fsiPanel9.Tag = "CHANGE";
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.rdoNo);
            this.fsiPanel8.Controls.Add(this.rdoYes);
            this.fsiPanel8.Controls.Add(this.label6);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(5, 381);
            this.fsiPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(795, 38);
            this.fsiPanel8.TabIndex = 7;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.txtBumonFr);
            this.fsiPanel7.Controls.Add(this.txtBumonTo);
            this.fsiPanel7.Controls.Add(this.lblBumonTo);
            this.fsiPanel7.Controls.Add(this.lblBumonFr);
            this.fsiPanel7.Controls.Add(this.lblBumonCodeBet);
            this.fsiPanel7.Controls.Add(this.label5);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(5, 334);
            this.fsiPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(795, 38);
            this.fsiPanel7.TabIndex = 6;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.lblHojoKamokuCodeBet);
            this.fsiPanel6.Controls.Add(this.txtHojoKamokuFr);
            this.fsiPanel6.Controls.Add(this.lblHojoKamokuTo);
            this.fsiPanel6.Controls.Add(this.lblHojoKamokuFr);
            this.fsiPanel6.Controls.Add(this.txtHojoKamokuTo);
            this.fsiPanel6.Controls.Add(this.label8);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(5, 287);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(795, 38);
            this.fsiPanel6.TabIndex = 5;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtKanjoKamoku);
            this.fsiPanel5.Controls.Add(this.lblKanjoKamoku);
            this.fsiPanel5.Controls.Add(this.label4);
            this.fsiPanel5.Location = new System.Drawing.Point(5, 240);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(795, 34);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblGengoFr);
            this.fsiPanel4.Controls.Add(this.txtMonthFr);
            this.fsiPanel4.Controls.Add(this.txtYearFr);
            this.fsiPanel4.Controls.Add(this.txtDayFr);
            this.fsiPanel4.Controls.Add(this.lblYearFr);
            this.fsiPanel4.Controls.Add(this.lblMonthFr);
            this.fsiPanel4.Controls.Add(this.lblDayFr);
            this.fsiPanel4.Controls.Add(this.lblGengoTo);
            this.fsiPanel4.Controls.Add(this.txtMonthTo);
            this.fsiPanel4.Controls.Add(this.txtYearTo);
            this.fsiPanel4.Controls.Add(this.txtDayTo);
            this.fsiPanel4.Controls.Add(this.lblYearTo);
            this.fsiPanel4.Controls.Add(this.lblMonthTo);
            this.fsiPanel4.Controls.Add(this.lblDayTo);
            this.fsiPanel4.Controls.Add(this.lblKikanCodeBet);
            this.fsiPanel4.Controls.Add(this.label3);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 193);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(795, 38);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // ZMMR1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.fsiPanel10);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMMR1021";
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiPanel10, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel11.ResumeLayout(false);
            this.fsiPanel10.ResumeLayout(false);
            this.fsiPanel10.PerformLayout();
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel9.PerformLayout();
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamoku;
        private System.Windows.Forms.Label lblKanjoKamoku;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.Label lblKikanCodeBet;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblBumonTo;
        private System.Windows.Forms.Label lblBumonCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonFr;
        private System.Windows.Forms.Label lblBumonFr;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonTo;
        private System.Windows.Forms.Label lblKojiTo;
        private System.Windows.Forms.Label lblKojiCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtKojiFr;
        private System.Windows.Forms.Label lblKojiFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKojiTo;
        private System.Windows.Forms.RadioButton rdoYes;
        private System.Windows.Forms.RadioButton rdoNo;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Label lblYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDay;
        private jp.co.fsi.common.controls.FsiTextBox txtYear;
        private jp.co.fsi.common.controls.FsiTextBox txtMonth;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.RadioButton rdoKessan;
        private System.Windows.Forms.RadioButton rdoTsujo;
        private System.Windows.Forms.RadioButton rdoZenbu;
        private System.Windows.Forms.Label lblHojoKamokuTo;
        private System.Windows.Forms.Label lblHojoKamokuCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuFr;
        private System.Windows.Forms.Label lblHojoKamokuFr;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel10;
        private System.Windows.Forms.Label label9;
        private common.FsiPanel fsiPanel11;
        private System.Windows.Forms.Label label10;
    }
}