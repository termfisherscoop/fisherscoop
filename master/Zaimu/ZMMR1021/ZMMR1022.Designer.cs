﻿namespace jp.co.fsi.zm.zmmr1021
{
    partial class ZMMR1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanjoBango = new System.Windows.Forms.Label();
            this.lblKanjoKamoku = new System.Windows.Forms.Label();
            this.lblJp = new System.Windows.Forms.Label();
            this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
            this.lblZei = new System.Windows.Forms.Label();
            this.lblGokei = new System.Windows.Forms.Label();
            this.lblKariGokei = new System.Windows.Forms.Label();
            this.lblKariZeiGokei = new System.Windows.Forms.Label();
            this.lblKashiZeiGokei = new System.Windows.Forms.Label();
            this.lblKashiGokei = new System.Windows.Forms.Label();
            this.lblZan = new System.Windows.Forms.Label();
            this.lblWaku = new System.Windows.Forms.Label();
            this.lblHojoBango = new System.Windows.Forms.Label();
            this.lblHojoKamoku = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel17 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel14 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel16 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel13 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel18 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel15 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.fsiPanel11.SuspendLayout();
            this.fsiPanel17.SuspendLayout();
            this.fsiPanel14.SuspendLayout();
            this.fsiPanel10.SuspendLayout();
            this.fsiPanel16.SuspendLayout();
            this.fsiPanel13.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel12.SuspendLayout();
            this.fsiPanel18.SuspendLayout();
            this.fsiPanel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 752);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1264, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1253, 31);
            this.lblTitle.Text = "";
            // 
            // lblKanjoBango
            // 
            this.lblKanjoBango.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoBango.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoBango.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoBango.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoBango.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblKanjoBango.Name = "lblKanjoBango";
            this.lblKanjoBango.Size = new System.Drawing.Size(65, 33);
            this.lblKanjoBango.TabIndex = 0;
            this.lblKanjoBango.Tag = "CHANGE";
            this.lblKanjoBango.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKanjoKamoku
            // 
            this.lblKanjoKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamoku.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamoku.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoKamoku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamoku.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblKanjoKamoku.Name = "lblKanjoKamoku";
            this.lblKanjoKamoku.Size = new System.Drawing.Size(311, 33);
            this.lblKanjoKamoku.TabIndex = 1;
            this.lblKanjoKamoku.Tag = "CHANGE";
            this.lblKanjoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJp
            // 
            this.lblJp.BackColor = System.Drawing.Color.Silver;
            this.lblJp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJp.ForeColor = System.Drawing.Color.Black;
            this.lblJp.Location = new System.Drawing.Point(0, 0);
            this.lblJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJp.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblJp.Name = "lblJp";
            this.lblJp.Size = new System.Drawing.Size(404, 33);
            this.lblJp.TabIndex = 4;
            this.lblJp.Tag = "CHANGE";
            this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mtbList
            // 
            this.mtbList.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mtbList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtbList.FixedCols = 0;
            this.mtbList.FocusField = null;
            this.mtbList.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.mtbList.Location = new System.Drawing.Point(0, 0);
            this.mtbList.Margin = new System.Windows.Forms.Padding(4);
            this.mtbList.Name = "mtbList";
            this.mtbList.NotSelectableCols = 0;
            this.mtbList.SelectRange = null;
            this.mtbList.Size = new System.Drawing.Size(1137, 540);
            this.mtbList.TabIndex = 6;
            this.mtbList.Text = "SjMultiTable1";
            this.mtbList.UndoBufferEnabled = false;
            // 
            // lblZei
            // 
            this.lblZei.BackColor = System.Drawing.Color.Silver;
            this.lblZei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZei.ForeColor = System.Drawing.Color.Black;
            this.lblZei.Location = new System.Drawing.Point(0, 0);
            this.lblZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZei.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblZei.Name = "lblZei";
            this.lblZei.Size = new System.Drawing.Size(104, 33);
            this.lblZei.TabIndex = 5;
            this.lblZei.Tag = "CHANGE";
            this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGokei
            // 
            this.lblGokei.BackColor = System.Drawing.Color.Silver;
            this.lblGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblGokei.ForeColor = System.Drawing.Color.Black;
            this.lblGokei.Location = new System.Drawing.Point(0, 0);
            this.lblGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGokei.Name = "lblGokei";
            this.lblGokei.Size = new System.Drawing.Size(448, 97);
            this.lblGokei.TabIndex = 7;
            this.lblGokei.Tag = "CHANGE";
            this.lblGokei.Text = "合　　　計";
            this.lblGokei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKariGokei
            // 
            this.lblKariGokei.BackColor = System.Drawing.Color.LightCyan;
            this.lblKariGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKariGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKariGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKariGokei.ForeColor = System.Drawing.Color.Black;
            this.lblKariGokei.Location = new System.Drawing.Point(0, 0);
            this.lblKariGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKariGokei.MinimumSize = new System.Drawing.Size(2, 31);
            this.lblKariGokei.Name = "lblKariGokei";
            this.lblKariGokei.Size = new System.Drawing.Size(251, 49);
            this.lblKariGokei.TabIndex = 9;
            this.lblKariGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKariZeiGokei
            // 
            this.lblKariZeiGokei.BackColor = System.Drawing.Color.LightCyan;
            this.lblKariZeiGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKariZeiGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKariZeiGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKariZeiGokei.ForeColor = System.Drawing.Color.Black;
            this.lblKariZeiGokei.Location = new System.Drawing.Point(0, 0);
            this.lblKariZeiGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKariZeiGokei.MinimumSize = new System.Drawing.Size(2, 31);
            this.lblKariZeiGokei.Name = "lblKariZeiGokei";
            this.lblKariZeiGokei.Size = new System.Drawing.Size(251, 48);
            this.lblKariZeiGokei.TabIndex = 8;
            this.lblKariZeiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashiZeiGokei
            // 
            this.lblKashiZeiGokei.BackColor = System.Drawing.Color.LightCyan;
            this.lblKashiZeiGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashiZeiGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKashiZeiGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKashiZeiGokei.ForeColor = System.Drawing.Color.Black;
            this.lblKashiZeiGokei.Location = new System.Drawing.Point(0, 0);
            this.lblKashiZeiGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKashiZeiGokei.MinimumSize = new System.Drawing.Size(2, 31);
            this.lblKashiZeiGokei.Name = "lblKashiZeiGokei";
            this.lblKashiZeiGokei.Size = new System.Drawing.Size(202, 48);
            this.lblKashiZeiGokei.TabIndex = 10;
            this.lblKashiZeiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashiGokei
            // 
            this.lblKashiGokei.BackColor = System.Drawing.Color.LightCyan;
            this.lblKashiGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashiGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKashiGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKashiGokei.ForeColor = System.Drawing.Color.Black;
            this.lblKashiGokei.Location = new System.Drawing.Point(0, 0);
            this.lblKashiGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKashiGokei.MinimumSize = new System.Drawing.Size(2, 31);
            this.lblKashiGokei.Name = "lblKashiGokei";
            this.lblKashiGokei.Size = new System.Drawing.Size(202, 49);
            this.lblKashiGokei.TabIndex = 11;
            this.lblKashiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblZan
            // 
            this.lblZan.BackColor = System.Drawing.Color.LightCyan;
            this.lblZan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblZan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZan.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblZan.ForeColor = System.Drawing.Color.Black;
            this.lblZan.Location = new System.Drawing.Point(0, 0);
            this.lblZan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZan.MinimumSize = new System.Drawing.Size(2, 31);
            this.lblZan.Name = "lblZan";
            this.lblZan.Size = new System.Drawing.Size(203, 49);
            this.lblZan.TabIndex = 13;
            this.lblZan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWaku
            // 
            this.lblWaku.BackColor = System.Drawing.Color.LightCyan;
            this.lblWaku.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblWaku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblWaku.ForeColor = System.Drawing.Color.Black;
            this.lblWaku.Location = new System.Drawing.Point(0, 0);
            this.lblWaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWaku.MinimumSize = new System.Drawing.Size(2, 31);
            this.lblWaku.Name = "lblWaku";
            this.lblWaku.Size = new System.Drawing.Size(203, 48);
            this.lblWaku.TabIndex = 12;
            this.lblWaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblHojoBango
            // 
            this.lblHojoBango.BackColor = System.Drawing.Color.Silver;
            this.lblHojoBango.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHojoBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoBango.ForeColor = System.Drawing.Color.Black;
            this.lblHojoBango.Location = new System.Drawing.Point(0, 0);
            this.lblHojoBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoBango.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblHojoBango.Name = "lblHojoBango";
            this.lblHojoBango.Size = new System.Drawing.Size(59, 33);
            this.lblHojoBango.TabIndex = 2;
            this.lblHojoBango.Tag = "CHANGE";
            this.lblHojoBango.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblHojoKamoku
            // 
            this.lblHojoKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHojoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamoku.ForeColor = System.Drawing.Color.Black;
            this.lblHojoKamoku.Location = new System.Drawing.Point(0, 0);
            this.lblHojoKamoku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoKamoku.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblHojoKamoku.Name = "lblHojoKamoku";
            this.lblHojoKamoku.Size = new System.Drawing.Size(193, 33);
            this.lblHojoKamoku.TabIndex = 3;
            this.lblHojoKamoku.Tag = "CHANGE";
            this.lblHojoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 36);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1147, 592);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.mtbList);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 47);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(1137, 540);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel8);
            this.fsiPanel1.Controls.Add(this.fsiPanel7);
            this.fsiPanel1.Controls.Add(this.fsiPanel6);
            this.fsiPanel1.Controls.Add(this.fsiPanel5);
            this.fsiPanel1.Controls.Add(this.fsiPanel4);
            this.fsiPanel1.Controls.Add(this.fsiPanel3);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(1137, 33);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.lblHojoBango);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel8.Location = new System.Drawing.Point(377, 0);
            this.fsiPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(59, 33);
            this.fsiPanel8.TabIndex = 6;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.lblHojoKamoku);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel7.Location = new System.Drawing.Point(436, 0);
            this.fsiPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(193, 33);
            this.fsiPanel7.TabIndex = 5;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.lblJp);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel6.Location = new System.Drawing.Point(629, 0);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(404, 33);
            this.fsiPanel6.TabIndex = 4;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblZei);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel5.Location = new System.Drawing.Point(1033, 0);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(104, 33);
            this.fsiPanel5.TabIndex = 3;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblKanjoKamoku);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel4.Location = new System.Drawing.Point(65, 0);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(311, 33);
            this.fsiPanel4.TabIndex = 2;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.lblKanjoBango);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel3.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(65, 33);
            this.fsiPanel3.TabIndex = 1;
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 4;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.35088F));
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.92398F));
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.59649F));
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.47953F));
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel11, 2, 0);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel10, 1, 0);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel9, 0, 0);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel12, 3, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(5, 636);
            this.fsiTableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 1;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(1141, 107);
            this.fsiTableLayoutPanel2.TabIndex = 903;
            // 
            // fsiPanel11
            // 
            this.fsiPanel11.Controls.Add(this.fsiPanel17);
            this.fsiPanel11.Controls.Add(this.fsiPanel14);
            this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel11.Location = new System.Drawing.Point(722, 5);
            this.fsiPanel11.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel11.Name = "fsiPanel11";
            this.fsiPanel11.Size = new System.Drawing.Size(202, 97);
            this.fsiPanel11.TabIndex = 2;
            this.fsiPanel11.Tag = "CHANGE";
            // 
            // fsiPanel17
            // 
            this.fsiPanel17.Controls.Add(this.lblKashiGokei);
            this.fsiPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel17.Location = new System.Drawing.Point(0, 48);
            this.fsiPanel17.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel17.Name = "fsiPanel17";
            this.fsiPanel17.Size = new System.Drawing.Size(202, 49);
            this.fsiPanel17.TabIndex = 3;
            // 
            // fsiPanel14
            // 
            this.fsiPanel14.Controls.Add(this.lblKashiZeiGokei);
            this.fsiPanel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.fsiPanel14.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel14.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel14.Name = "fsiPanel14";
            this.fsiPanel14.Size = new System.Drawing.Size(202, 48);
            this.fsiPanel14.TabIndex = 2;
            this.fsiPanel14.Tag = "CHANGE";
            // 
            // fsiPanel10
            // 
            this.fsiPanel10.Controls.Add(this.fsiPanel16);
            this.fsiPanel10.Controls.Add(this.fsiPanel13);
            this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel10.Location = new System.Drawing.Point(462, 5);
            this.fsiPanel10.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel10.Name = "fsiPanel10";
            this.fsiPanel10.Size = new System.Drawing.Size(251, 97);
            this.fsiPanel10.TabIndex = 1;
            this.fsiPanel10.Tag = "CHANGE";
            // 
            // fsiPanel16
            // 
            this.fsiPanel16.Controls.Add(this.lblKariGokei);
            this.fsiPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel16.Location = new System.Drawing.Point(0, 48);
            this.fsiPanel16.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel16.Name = "fsiPanel16";
            this.fsiPanel16.Size = new System.Drawing.Size(251, 49);
            this.fsiPanel16.TabIndex = 2;
            // 
            // fsiPanel13
            // 
            this.fsiPanel13.Controls.Add(this.lblKariZeiGokei);
            this.fsiPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.fsiPanel13.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel13.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel13.Name = "fsiPanel13";
            this.fsiPanel13.Size = new System.Drawing.Size(251, 48);
            this.fsiPanel13.TabIndex = 1;
            this.fsiPanel13.Tag = "CHANGE";
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.lblGokei);
            this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel9.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel9.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(448, 97);
            this.fsiPanel9.TabIndex = 0;
            this.fsiPanel9.Tag = "CHANGE";
            // 
            // fsiPanel12
            // 
            this.fsiPanel12.Controls.Add(this.fsiPanel18);
            this.fsiPanel12.Controls.Add(this.fsiPanel15);
            this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel12.Location = new System.Drawing.Point(933, 5);
            this.fsiPanel12.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel12.Name = "fsiPanel12";
            this.fsiPanel12.Size = new System.Drawing.Size(203, 97);
            this.fsiPanel12.TabIndex = 2;
            this.fsiPanel12.Tag = "CHANGE";
            // 
            // fsiPanel18
            // 
            this.fsiPanel18.BackColor = System.Drawing.Color.White;
            this.fsiPanel18.Controls.Add(this.lblZan);
            this.fsiPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel18.Location = new System.Drawing.Point(0, 48);
            this.fsiPanel18.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel18.Name = "fsiPanel18";
            this.fsiPanel18.Size = new System.Drawing.Size(203, 49);
            this.fsiPanel18.TabIndex = 3;
            // 
            // fsiPanel15
            // 
            this.fsiPanel15.Controls.Add(this.lblWaku);
            this.fsiPanel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.fsiPanel15.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel15.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel15.Name = "fsiPanel15";
            this.fsiPanel15.Size = new System.Drawing.Size(203, 48);
            this.fsiPanel15.TabIndex = 2;
            this.fsiPanel15.Tag = "CHANGE";
            // 
            // ZMMR1022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1253, 889);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMMR1022";
            this.ShowFButton = true;
            this.Text = "";
            this.Shown += new System.EventHandler(this.ZMMR1022_Shown);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.fsiPanel11.ResumeLayout(false);
            this.fsiPanel17.ResumeLayout(false);
            this.fsiPanel14.ResumeLayout(false);
            this.fsiPanel10.ResumeLayout(false);
            this.fsiPanel16.ResumeLayout(false);
            this.fsiPanel13.ResumeLayout(false);
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel12.ResumeLayout(false);
            this.fsiPanel18.ResumeLayout(false);
            this.fsiPanel15.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKanjoBango;
        private System.Windows.Forms.Label lblKanjoKamoku;
        private System.Windows.Forms.Label lblJp;
        private jp.co.fsi.common.controls.SjMultiTable mtbList;
        private System.Windows.Forms.Label lblZei;
        private System.Windows.Forms.Label lblGokei;
        private System.Windows.Forms.Label lblKariGokei;
        private System.Windows.Forms.Label lblKariZeiGokei;
        private System.Windows.Forms.Label lblKashiZeiGokei;
        private System.Windows.Forms.Label lblKashiGokei;
        private System.Windows.Forms.Label lblZan;
        private System.Windows.Forms.Label lblWaku;
        private System.Windows.Forms.Label lblHojoBango;
        private System.Windows.Forms.Label lblHojoKamoku;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel17;
        private common.FsiPanel fsiPanel14;
        private common.FsiPanel fsiPanel10;
        private common.FsiPanel fsiPanel16;
        private common.FsiPanel fsiPanel13;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel12;
        private common.FsiPanel fsiPanel18;
        private common.FsiPanel fsiPanel15;
    }
}