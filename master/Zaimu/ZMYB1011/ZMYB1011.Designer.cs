﻿namespace jp.co.fsi.zm.zmyb1011
{
    partial class ZMYB1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkOpt3 = new System.Windows.Forms.CheckBox();
            this.chkOpt2 = new System.Windows.Forms.CheckBox();
            this.chkOpt1 = new System.Windows.Forms.CheckBox();
            this.lblKessankiNext = new System.Windows.Forms.Label();
            this.lblKessanki = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel3 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.lblKessankiNextHead = new System.Windows.Forms.Label();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.lblKessankiHead = new System.Windows.Forms.Label();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.lblInfo = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiTableLayoutPanel3.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 609);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "年次繰越";
            // 
            // chkOpt3
            // 
            this.chkOpt3.AutoSize = true;
            this.chkOpt3.BackColor = System.Drawing.Color.Silver;
            this.chkOpt3.Checked = true;
            this.chkOpt3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOpt3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkOpt3.Location = new System.Drawing.Point(20, 7);
            this.chkOpt3.Margin = new System.Windows.Forms.Padding(4);
            this.chkOpt3.MinimumSize = new System.Drawing.Size(0, 24);
            this.chkOpt3.Name = "chkOpt3";
            this.chkOpt3.Size = new System.Drawing.Size(187, 24);
            this.chkOpt3.TabIndex = 2;
            this.chkOpt3.Tag = "CHANGE";
            this.chkOpt3.Text = "期末残高の繰越を行う";
            this.chkOpt3.UseVisualStyleBackColor = false;
            this.chkOpt3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chk_KeyPress);
            // 
            // chkOpt2
            // 
            this.chkOpt2.AutoSize = true;
            this.chkOpt2.BackColor = System.Drawing.Color.Silver;
            this.chkOpt2.Checked = true;
            this.chkOpt2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOpt2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkOpt2.Location = new System.Drawing.Point(21, 7);
            this.chkOpt2.Margin = new System.Windows.Forms.Padding(4);
            this.chkOpt2.MinimumSize = new System.Drawing.Size(0, 24);
            this.chkOpt2.Name = "chkOpt2";
            this.chkOpt2.Size = new System.Drawing.Size(155, 24);
            this.chkOpt2.TabIndex = 1;
            this.chkOpt2.Tag = "CHANGE";
            this.chkOpt2.Text = "実績の繰越を行う";
            this.chkOpt2.UseVisualStyleBackColor = false;
            this.chkOpt2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chk_KeyPress);
            // 
            // chkOpt1
            // 
            this.chkOpt1.AutoSize = true;
            this.chkOpt1.BackColor = System.Drawing.Color.Silver;
            this.chkOpt1.Checked = true;
            this.chkOpt1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOpt1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkOpt1.Location = new System.Drawing.Point(21, 9);
            this.chkOpt1.Margin = new System.Windows.Forms.Padding(4);
            this.chkOpt1.MinimumSize = new System.Drawing.Size(0, 24);
            this.chkOpt1.Name = "chkOpt1";
            this.chkOpt1.Size = new System.Drawing.Size(171, 24);
            this.chkOpt1.TabIndex = 0;
            this.chkOpt1.Tag = "CHANGE";
            this.chkOpt1.Text = "テーブルを移行する";
            this.chkOpt1.UseVisualStyleBackColor = false;
            this.chkOpt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chk_KeyPress);
            // 
            // lblKessankiNext
            // 
            this.lblKessankiNext.BackColor = System.Drawing.Color.LightCyan;
            this.lblKessankiNext.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKessankiNext.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessankiNext.ForeColor = System.Drawing.Color.Black;
            this.lblKessankiNext.Location = new System.Drawing.Point(128, 15);
            this.lblKessankiNext.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKessankiNext.Name = "lblKessankiNext";
            this.lblKessankiNext.Size = new System.Drawing.Size(61, 24);
            this.lblKessankiNext.TabIndex = 7;
            this.lblKessankiNext.Tag = "DISPNAME";
            this.lblKessankiNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKessanki
            // 
            this.lblKessanki.BackColor = System.Drawing.Color.LightCyan;
            this.lblKessanki.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKessanki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessanki.ForeColor = System.Drawing.Color.Black;
            this.lblKessanki.Location = new System.Drawing.Point(128, 15);
            this.lblKessanki.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKessanki.Name = "lblKessanki";
            this.lblKessanki.Size = new System.Drawing.Size(61, 24);
            this.lblKessanki.TabIndex = 7;
            this.lblKessanki.Tag = "DISPNAME";
            this.lblKessanki.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(132, 11);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(59, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(193, 10);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
            this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(584, 44);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 44);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.02876F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.94069F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.03056F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(592, 303);
            this.fsiTableLayoutPanel1.TabIndex = 903;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(584, 44);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.fsiTableLayoutPanel3);
            this.fsiPanel2.Controls.Add(this.fsiTableLayoutPanel2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 55);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(584, 131);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiTableLayoutPanel3
            // 
            this.fsiTableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel3.ColumnCount = 1;
            this.fsiTableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel6, 0, 0);
            this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel7, 0, 1);
            this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel8, 0, 2);
            this.fsiTableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiTableLayoutPanel3.Location = new System.Drawing.Point(316, 0);
            this.fsiTableLayoutPanel3.Name = "fsiTableLayoutPanel3";
            this.fsiTableLayoutPanel3.RowCount = 3;
            this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel3.Size = new System.Drawing.Size(268, 131);
            this.fsiTableLayoutPanel3.TabIndex = 905;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.BackColor = System.Drawing.Color.White;
            this.fsiPanel6.Controls.Add(this.chkOpt1);
            this.fsiPanel6.Controls.Add(this.label1);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(260, 36);
            this.fsiPanel6.TabIndex = 4;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 36);
            this.label1.TabIndex = 906;
            this.label1.Tag = "CHANGE";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.BackColor = System.Drawing.Color.White;
            this.fsiPanel7.Controls.Add(this.chkOpt2);
            this.fsiPanel7.Controls.Add(this.label2);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(4, 47);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(260, 36);
            this.fsiPanel7.TabIndex = 3;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(260, 36);
            this.label2.TabIndex = 907;
            this.label2.Tag = "CHANGE";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.BackColor = System.Drawing.Color.White;
            this.fsiPanel8.Controls.Add(this.chkOpt3);
            this.fsiPanel8.Controls.Add(this.label3);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(4, 90);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(260, 37);
            this.fsiPanel8.TabIndex = 3;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(260, 37);
            this.label3.TabIndex = 907;
            this.label3.Tag = "CHANGE";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel5, 0, 1);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel4, 0, 0);
            this.fsiTableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 2;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(315, 131);
            this.fsiTableLayoutPanel2.TabIndex = 904;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.BackColor = System.Drawing.Color.White;
            this.fsiPanel5.Controls.Add(this.lblKessankiNext);
            this.fsiPanel5.Controls.Add(this.lblKessankiNextHead);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 69);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(307, 58);
            this.fsiPanel5.TabIndex = 3;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // lblKessankiNextHead
            // 
            this.lblKessankiNextHead.BackColor = System.Drawing.Color.Silver;
            this.lblKessankiNextHead.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKessankiNextHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKessankiNextHead.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessankiNextHead.Location = new System.Drawing.Point(0, 0);
            this.lblKessankiNextHead.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKessankiNextHead.Name = "lblKessankiNextHead";
            this.lblKessankiNextHead.Size = new System.Drawing.Size(307, 58);
            this.lblKessankiNextHead.TabIndex = 904;
            this.lblKessankiNextHead.Tag = "CHANGE";
            this.lblKessankiNextHead.Text = "新年度　第　　　　　　　　期";
            this.lblKessankiNextHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.BackColor = System.Drawing.Color.White;
            this.fsiPanel4.Controls.Add(this.lblKessanki);
            this.fsiPanel4.Controls.Add(this.lblKessankiHead);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(307, 58);
            this.fsiPanel4.TabIndex = 905;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // lblKessankiHead
            // 
            this.lblKessankiHead.BackColor = System.Drawing.Color.Silver;
            this.lblKessankiHead.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKessankiHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKessankiHead.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessankiHead.Location = new System.Drawing.Point(0, 0);
            this.lblKessankiHead.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKessankiHead.Name = "lblKessankiHead";
            this.lblKessankiHead.Size = new System.Drawing.Size(307, 58);
            this.lblKessankiHead.TabIndex = 904;
            this.lblKessankiHead.Tag = "CHANGE";
            this.lblKessankiHead.Text = "今年度　第　　　　　　　　期";
            this.lblKessankiHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.BackColor = System.Drawing.Color.White;
            this.fsiPanel3.Controls.Add(this.lblInfo);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 193);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(584, 106);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // lblInfo
            // 
            this.lblInfo.BackColor = System.Drawing.Color.Silver;
            this.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblInfo.Location = new System.Drawing.Point(0, 0);
            this.lblInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(584, 106);
            this.lblInfo.TabIndex = 907;
            this.lblInfo.Tag = "CHANGE";
            this.lblInfo.Text = "繰越済み";
            // 
            // ZMYB1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 745);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMYB1011";
            this.ShowFButton = true;
            this.Text = "年次繰越";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiTableLayoutPanel3.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.CheckBox chkOpt3;
        private System.Windows.Forms.CheckBox chkOpt2;
        private System.Windows.Forms.CheckBox chkOpt1;
        private System.Windows.Forms.Label lblKessankiNext;
        private System.Windows.Forms.Label lblKessanki;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel5;
        private System.Windows.Forms.Label lblKessankiNextHead;
        private System.Windows.Forms.Label lblKessankiHead;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel3;
        private common.FsiPanel fsiPanel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblInfo;
    }
}