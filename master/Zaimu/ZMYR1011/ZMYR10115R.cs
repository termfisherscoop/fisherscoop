﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10115R の帳票
    /// </summary>
    public partial class ZMYR10115R : BaseReport
    {

        public ZMYR10115R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
