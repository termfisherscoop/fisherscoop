﻿namespace jp.co.fsi.zm.zmyr1011
{
    partial class ZMYR1017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRiekiShobunanTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtHanbaihiMeisaihyoTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rdoMinusKingaku1 = new System.Windows.Forms.RadioButton();
            this.rdoMinusKingaku0 = new System.Windows.Forms.RadioButton();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel10.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 377);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(547, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(536, 31);
            this.lblTitle.Text = "";
            // 
            // txtRiekiShobunanTitle
            // 
            this.txtRiekiShobunanTitle.AutoSizeFromLength = false;
            this.txtRiekiShobunanTitle.DisplayLength = null;
            this.txtRiekiShobunanTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRiekiShobunanTitle.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanTitle.Location = new System.Drawing.Point(167, 5);
            this.txtRiekiShobunanTitle.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanTitle.MaxLength = 64;
            this.txtRiekiShobunanTitle.Name = "txtRiekiShobunanTitle";
            this.txtRiekiShobunanTitle.Size = new System.Drawing.Size(281, 23);
            this.txtRiekiShobunanTitle.TabIndex = 5;
            this.txtRiekiShobunanTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // txtHanbaihiMeisaihyoTitle
            // 
            this.txtHanbaihiMeisaihyoTitle.AutoSizeFromLength = false;
            this.txtHanbaihiMeisaihyoTitle.DisplayLength = null;
            this.txtHanbaihiMeisaihyoTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHanbaihiMeisaihyoTitle.ForeColor = System.Drawing.Color.Black;
            this.txtHanbaihiMeisaihyoTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtHanbaihiMeisaihyoTitle.Location = new System.Drawing.Point(167, 4);
            this.txtHanbaihiMeisaihyoTitle.Margin = new System.Windows.Forms.Padding(4);
            this.txtHanbaihiMeisaihyoTitle.MaxLength = 64;
            this.txtHanbaihiMeisaihyoTitle.Name = "txtHanbaihiMeisaihyoTitle";
            this.txtHanbaihiMeisaihyoTitle.Size = new System.Drawing.Size(281, 23);
            this.txtHanbaihiMeisaihyoTitle.TabIndex = 4;
            this.txtHanbaihiMeisaihyoTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // txtSeizoGenkaTitle
            // 
            this.txtSeizoGenkaTitle.AutoSizeFromLength = false;
            this.txtSeizoGenkaTitle.DisplayLength = null;
            this.txtSeizoGenkaTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeizoGenkaTitle.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaTitle.Location = new System.Drawing.Point(167, 5);
            this.txtSeizoGenkaTitle.Margin = new System.Windows.Forms.Padding(4);
            this.txtSeizoGenkaTitle.MaxLength = 64;
            this.txtSeizoGenkaTitle.Name = "txtSeizoGenkaTitle";
            this.txtSeizoGenkaTitle.Size = new System.Drawing.Size(281, 23);
            this.txtSeizoGenkaTitle.TabIndex = 3;
            this.txtSeizoGenkaTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // txtSonekiKeisanshoTitle
            // 
            this.txtSonekiKeisanshoTitle.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoTitle.DisplayLength = null;
            this.txtSonekiKeisanshoTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSonekiKeisanshoTitle.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoTitle.Location = new System.Drawing.Point(167, 4);
            this.txtSonekiKeisanshoTitle.Margin = new System.Windows.Forms.Padding(4);
            this.txtSonekiKeisanshoTitle.MaxLength = 64;
            this.txtSonekiKeisanshoTitle.Name = "txtSonekiKeisanshoTitle";
            this.txtSonekiKeisanshoTitle.Size = new System.Drawing.Size(281, 23);
            this.txtSonekiKeisanshoTitle.TabIndex = 2;
            this.txtSonekiKeisanshoTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // txtTaishakuTaishohyoTitle
            // 
            this.txtTaishakuTaishohyoTitle.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoTitle.DisplayLength = null;
            this.txtTaishakuTaishohyoTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaishakuTaishohyoTitle.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoTitle.Location = new System.Drawing.Point(167, 5);
            this.txtTaishakuTaishohyoTitle.Margin = new System.Windows.Forms.Padding(4);
            this.txtTaishakuTaishohyoTitle.MaxLength = 64;
            this.txtTaishakuTaishohyoTitle.Name = "txtTaishakuTaishohyoTitle";
            this.txtTaishakuTaishohyoTitle.Size = new System.Drawing.Size(281, 23);
            this.txtTaishakuTaishohyoTitle.TabIndex = 1;
            this.txtTaishakuTaishohyoTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.MinimumSize = new System.Drawing.Size(0, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(465, 33);
            this.label6.TabIndex = 11;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "利益処分案";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.MinimumSize = new System.Drawing.Size(0, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(465, 32);
            this.label5.TabIndex = 10;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "販売費及び一般管理費";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(465, 32);
            this.label4.TabIndex = 9;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "製造原価報告書";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(465, 32);
            this.label3.TabIndex = 8;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "損益計算書";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(465, 32);
            this.label2.TabIndex = 7;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "貸借対照表";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoMinusKingaku1
            // 
            this.rdoMinusKingaku1.AutoSize = true;
            this.rdoMinusKingaku1.BackColor = System.Drawing.Color.Silver;
            this.rdoMinusKingaku1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdoMinusKingaku1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoMinusKingaku1.Location = new System.Drawing.Point(0, 0);
            this.rdoMinusKingaku1.Margin = new System.Windows.Forms.Padding(4);
            this.rdoMinusKingaku1.MinimumSize = new System.Drawing.Size(0, 32);
            this.rdoMinusKingaku1.Name = "rdoMinusKingaku1";
            this.rdoMinusKingaku1.Size = new System.Drawing.Size(231, 41);
            this.rdoMinusKingaku1.TabIndex = 8;
            this.rdoMinusKingaku1.TabStop = true;
            this.rdoMinusKingaku1.Tag = "CHANGE";
            this.rdoMinusKingaku1.Text = "△999,999,999";
            this.rdoMinusKingaku1.UseVisualStyleBackColor = false;
            // 
            // rdoMinusKingaku0
            // 
            this.rdoMinusKingaku0.AutoSize = true;
            this.rdoMinusKingaku0.BackColor = System.Drawing.Color.Silver;
            this.rdoMinusKingaku0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdoMinusKingaku0.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoMinusKingaku0.Location = new System.Drawing.Point(0, 0);
            this.rdoMinusKingaku0.Margin = new System.Windows.Forms.Padding(4);
            this.rdoMinusKingaku0.MinimumSize = new System.Drawing.Size(0, 32);
            this.rdoMinusKingaku0.Name = "rdoMinusKingaku0";
            this.rdoMinusKingaku0.Size = new System.Drawing.Size(237, 41);
            this.rdoMinusKingaku0.TabIndex = 7;
            this.rdoMinusKingaku0.TabStop = true;
            this.rdoMinusKingaku0.Tag = "CHANGE";
            this.rdoMinusKingaku0.Text = "-999,999,999";
            this.rdoMinusKingaku0.UseVisualStyleBackColor = false;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(11, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 6;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.15152F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.9697F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.9697F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.9697F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.9697F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.9697F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(473, 232);
            this.fsiTableLayoutPanel1.TabIndex = 905;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtRiekiShobunanTitle);
            this.fsiPanel6.Controls.Add(this.label6);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 195);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(465, 33);
            this.fsiPanel6.TabIndex = 908;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.label7);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(465, 28);
            this.fsiPanel1.TabIndex = 907;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.MinimumSize = new System.Drawing.Size(0, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(465, 32);
            this.label7.TabIndex = 7;
            this.label7.Tag = "CHANGE";
            this.label7.Text = "帳票タイトル";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtHanbaihiMeisaihyoTitle);
            this.fsiPanel5.Controls.Add(this.label5);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 156);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(465, 32);
            this.fsiPanel5.TabIndex = 908;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtTaishakuTaishohyoTitle);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 39);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(465, 32);
            this.fsiPanel2.TabIndex = 908;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtSeizoGenkaTitle);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 117);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(465, 32);
            this.fsiPanel4.TabIndex = 908;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtSonekiKeisanshoTitle);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 78);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(465, 32);
            this.fsiPanel3.TabIndex = 908;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel8, 0, 1);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel7, 0, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(11, 272);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 2;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(476, 96);
            this.fsiTableLayoutPanel2.TabIndex = 906;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.fsiPanel10);
            this.fsiPanel8.Controls.Add(this.fsiPanel9);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(4, 51);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(468, 41);
            this.fsiPanel8.TabIndex = 909;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // fsiPanel10
            // 
            this.fsiPanel10.Controls.Add(this.rdoMinusKingaku1);
            this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel10.Location = new System.Drawing.Point(237, 0);
            this.fsiPanel10.Name = "fsiPanel10";
            this.fsiPanel10.Size = new System.Drawing.Size(231, 41);
            this.fsiPanel10.TabIndex = 910;
            this.fsiPanel10.Tag = "CHANGE";
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.rdoMinusKingaku0);
            this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel9.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(237, 41);
            this.fsiPanel9.TabIndex = 910;
            this.fsiPanel9.Tag = "CHANGE";
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.label8);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(468, 40);
            this.fsiPanel7.TabIndex = 908;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.MinimumSize = new System.Drawing.Size(0, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(468, 40);
            this.label8.TabIndex = 7;
            this.label8.Tag = "CHANGE";
            this.label8.Text = "■マイナス金額表示";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ZMYR1017
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 514);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMYR1017";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel10.ResumeLayout(false);
            this.fsiPanel10.PerformLayout();
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel9.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanTitle;
        private jp.co.fsi.common.controls.FsiTextBox txtHanbaihiMeisaihyoTitle;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaTitle;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoTitle;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoTitle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdoMinusKingaku1;
        private System.Windows.Forms.RadioButton rdoMinusKingaku0;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label7;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private System.Windows.Forms.Label label8;
        private common.FsiPanel fsiPanel10;
        private common.FsiPanel fsiPanel9;
    }
}