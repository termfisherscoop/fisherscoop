﻿namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10112R の概要の説明です。
    /// </summary>
    partial class ZMYR10112R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMYR10112R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtSouko = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTanaban = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtItem15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtItem14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSaiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZaikoKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHyojiDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSouko,
            this.txtTanaban,
            this.textBox1,
            this.label3,
            this.txtTitle02,
            this.txtTitle03,
            this.txtTitle01,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.line3,
            this.line4,
            this.line5,
            this.line2,
            this.line1,
            this.line6,
            this.line7,
            this.line8,
            this.line14});
            this.pageHeader.Height = 0.8173283F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtSouko
            // 
            this.txtSouko.DataField = "ITEM10";
            this.txtSouko.Height = 0.2283465F;
            this.txtSouko.Left = 0.4062992F;
            this.txtSouko.Name = "txtSouko";
            this.txtSouko.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtSouko.Text = "10 貸借対照表";
            this.txtSouko.Top = 0F;
            this.txtSouko.Width = 6.299212F;
            // 
            // txtTanaban
            // 
            this.txtTanaban.DataField = "ITEM05";
            this.txtTanaban.Height = 0.2F;
            this.txtTanaban.Left = 0F;
            this.txtTanaban.MultiLine = false;
            this.txtTanaban.Name = "txtTanaban";
            this.txtTanaban.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTanaban.Text = "5";
            this.txtTanaban.Top = 0.2283465F;
            this.txtTanaban.Width = 2.461417F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM04";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 2.592126F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.textBox1.Text = "04";
            this.textBox1.Top = 0.2283465F;
            this.textBox1.Width = 1.935039F;
            // 
            // label3
            // 
            this.label3.Height = 0.2F;
            this.label3.HyperLink = null;
            this.label3.Left = 6.08819F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.label3.Text = "(単位：円)";
            this.label3.Top = 0.2283465F;
            this.label3.Width = 1.002362F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.1795277F;
            this.txtTitle02.Left = 0F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: bottom; ddo-char-set: 1";
            this.txtTitle02.Text = "科         目";
            this.txtTitle02.Top = 0.6338583F;
            this.txtTitle02.Width = 1.812599F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.1795277F;
            this.txtTitle03.Left = 1.812599F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: bottom; ddo-char-set: 1";
            this.txtTitle03.Text = "金         額";
            this.txtTitle03.Top = 0.6338583F;
            this.txtTitle03.Width = 1.714173F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.144882F;
            this.txtTitle01.Left = 0F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: top; ddo-char-set: 1";
            this.txtTitle01.Text = "資                     産";
            this.txtTitle01.Top = 0.4889764F;
            this.txtTitle01.Width = 3.526772F;
            // 
            // textBox3
            // 
            this.textBox3.Height = 0.1795276F;
            this.textBox3.Left = 3.526772F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: bottom; ddo-char-set: 1";
            this.textBox3.Text = "科         目";
            this.textBox3.Top = 0.6338583F;
            this.textBox3.Width = 1.838977F;
            // 
            // textBox4
            // 
            this.textBox4.Height = 0.1795276F;
            this.textBox4.Left = 5.365748F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: bottom; ddo-char-set: 1";
            this.textBox4.Text = "金          額";
            this.textBox4.Top = 0.6338583F;
            this.textBox4.Width = 1.724804F;
            // 
            // textBox5
            // 
            this.textBox5.Height = 0.1448819F;
            this.textBox5.Left = 3.526772F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: top; ddo-char-set: 1";
            this.textBox5.Text = "負    債    及    び    資    本";
            this.textBox5.Top = 0.4889764F;
            this.textBox5.Width = 3.56378F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.4889764F;
            this.line3.Width = 7.090551F;
            this.line3.X1 = 0F;
            this.line3.X2 = 7.090551F;
            this.line3.Y1 = 0.4889764F;
            this.line3.Y2 = 0.4889764F;
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 0F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.6338583F;
            this.line4.Width = 7.090549F;
            this.line4.X1 = 0F;
            this.line4.X2 = 7.090549F;
            this.line4.Y1 = 0.6338583F;
            this.line4.Y2 = 0.6338583F;
            // 
            // line5
            // 
            this.line5.Height = 0F;
            this.line5.Left = 0F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.8133859F;
            this.line5.Width = 7.090549F;
            this.line5.X1 = 0F;
            this.line5.X2 = 7.090549F;
            this.line5.Y1 = 0.8133859F;
            this.line5.Y2 = 0.8133859F;
            // 
            // line2
            // 
            this.line2.Height = 0.3245737F;
            this.line2.Left = 3.526756F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.4889764F;
            this.line2.Width = 1.597404E-05F;
            this.line2.X1 = 3.526772F;
            this.line2.X2 = 3.526756F;
            this.line2.Y1 = 0.4889764F;
            this.line2.Y2 = 0.8135501F;
            // 
            // line1
            // 
            this.line1.Height = 0.3245737F;
            this.line1.Left = 3.558252F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.4889764F;
            this.line1.Width = 1.597404E-05F;
            this.line1.X1 = 3.558268F;
            this.line1.X2 = 3.558252F;
            this.line1.Y1 = 0.4889764F;
            this.line1.Y2 = 0.8135501F;
            // 
            // line6
            // 
            this.line6.Height = 0.1867782F;
            this.line6.Left = 5.365732F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.6338583F;
            this.line6.Width = 1.573563E-05F;
            this.line6.X1 = 5.365748F;
            this.line6.X2 = 5.365732F;
            this.line6.Y1 = 0.6338583F;
            this.line6.Y2 = 0.8206366F;
            // 
            // line7
            // 
            this.line7.Height = 0.3245737F;
            this.line7.Left = 7.090536F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.4889764F;
            this.line7.Width = 1.573563E-05F;
            this.line7.X1 = 7.090552F;
            this.line7.X2 = 7.090536F;
            this.line7.Y1 = 0.4889764F;
            this.line7.Y2 = 0.8135501F;
            // 
            // line8
            // 
            this.line8.Height = 0.3245737F;
            this.line8.Left = 0F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.4889764F;
            this.line8.Width = 1.597404E-05F;
            this.line8.X1 = 1.597404E-05F;
            this.line8.X2 = 0F;
            this.line8.Y1 = 0.4889764F;
            this.line8.Y2 = 0.8135501F;
            // 
            // line14
            // 
            this.line14.Height = 0.1795277F;
            this.line14.Left = 1.812599F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0.6338583F;
            this.line14.Width = 0F;
            this.line14.X1 = 1.812599F;
            this.line14.X2 = 1.812599F;
            this.line14.Y1 = 0.6338583F;
            this.line14.Y2 = 0.813386F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtItem15,
            this.txtItem14,
            this.txtKesusu01,
            this.txtSaiKingaku,
            this.txtKesusu03,
            this.txtZaikoKingaku,
            this.textBox2,
            this.textBox6,
            this.textBox8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line15,
            this.line16,
            this.line21,
            this.line18,
            this.line19,
            this.textBox7,
            this.line20,
            this.line17});
            this.detail.Height = 0.1653544F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // txtItem15
            // 
            this.txtItem15.DataField = "ITEM15";
            this.txtItem15.Height = 0.1688976F;
            this.txtItem15.Left = 4.413386F;
            this.txtItem15.Name = "txtItem15";
            this.txtItem15.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtItem15.Text = "ITEM15";
            this.txtItem15.Top = 0F;
            this.txtItem15.Visible = false;
            this.txtItem15.Width = 0.6409445F;
            // 
            // txtItem14
            // 
            this.txtItem14.DataField = "ITEM14";
            this.txtItem14.Height = 0.1653543F;
            this.txtItem14.Left = 0.934252F;
            this.txtItem14.Name = "txtItem14";
            this.txtItem14.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtItem14.Text = "ITEM14";
            this.txtItem14.Top = 0F;
            this.txtItem14.Visible = false;
            this.txtItem14.Width = 0.4535434F;
            // 
            // txtKesusu01
            // 
            this.txtKesusu01.DataField = "ITEM24";
            this.txtKesusu01.Height = 0.1653543F;
            this.txtKesusu01.Left = 2.677559F;
            this.txtKesusu01.MultiLine = false;
            this.txtKesusu01.Name = "txtKesusu01";
            this.txtKesusu01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtKesusu01.Text = "24";
            this.txtKesusu01.Top = 0F;
            this.txtKesusu01.Width = 0.8492129F;
            // 
            // txtSaiKingaku
            // 
            this.txtSaiKingaku.DataField = "ITEM22";
            this.txtSaiKingaku.Height = 0.1653543F;
            this.txtSaiKingaku.Left = 1.387795F;
            this.txtSaiKingaku.Name = "txtSaiKingaku";
            this.txtSaiKingaku.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtSaiKingaku.Text = "22";
            this.txtSaiKingaku.Top = 0F;
            this.txtSaiKingaku.Visible = false;
            this.txtSaiKingaku.Width = 0.3830709F;
            // 
            // txtKesusu03
            // 
            this.txtKesusu03.DataField = "ITEM21";
            this.txtKesusu03.Height = 0.1653543F;
            this.txtKesusu03.Left = 0.03110236F;
            this.txtKesusu03.MultiLine = false;
            this.txtKesusu03.Name = "txtKesusu03";
            this.txtKesusu03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: left; vertical-align: middle; ddo" +
    "-char-set: 1";
            this.txtKesusu03.Text = "21";
            this.txtKesusu03.Top = 0F;
            this.txtKesusu03.Width = 1.781496F;
            // 
            // txtZaikoKingaku
            // 
            this.txtZaikoKingaku.DataField = "ITEM23";
            this.txtZaikoKingaku.Height = 0.1653543F;
            this.txtZaikoKingaku.Left = 1.812599F;
            this.txtZaikoKingaku.Name = "txtZaikoKingaku";
            this.txtZaikoKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtZaikoKingaku.Text = "23";
            this.txtZaikoKingaku.Top = 0F;
            this.txtZaikoKingaku.Width = 0.8661417F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM34";
            this.textBox2.Height = 0.1653543F;
            this.textBox2.Left = 6.277166F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.textBox2.Text = "34";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 0.781889F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM32";
            this.textBox6.Height = 0.1653543F;
            this.textBox6.Left = 5.152362F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.textBox6.Text = "32";
            this.textBox6.Top = 0F;
            this.textBox6.Visible = false;
            this.textBox6.Width = 0.1822839F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM33";
            this.textBox8.Height = 0.1653543F;
            this.textBox8.Left = 5.365748F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.textBox8.Text = "33";
            this.textBox8.Top = 0F;
            this.textBox8.Width = 0.880315F;
            // 
            // line9
            // 
            this.line9.Height = 0.1653543F;
            this.line9.Left = 0F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 9.313226E-08F;
            this.line9.X1 = 9.313226E-08F;
            this.line9.X2 = 0F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.1653543F;
            // 
            // line10
            // 
            this.line10.Height = 0.1653543F;
            this.line10.Left = 3.526772F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 3.526772F;
            this.line10.X2 = 3.526772F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.1653543F;
            // 
            // line11
            // 
            this.line11.Height = 0.1653543F;
            this.line11.Left = 3.558268F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 3.558268F;
            this.line11.X2 = 3.558268F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.1653543F;
            // 
            // line12
            // 
            this.line12.Height = 0.1653543F;
            this.line12.Left = 5.365748F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 5.365748F;
            this.line12.X2 = 5.365748F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.1653543F;
            // 
            // line13
            // 
            this.line13.Height = 0.1653543F;
            this.line13.Left = 1.812599F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 1.812599F;
            this.line13.X2 = 1.812599F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.1653543F;
            // 
            // line15
            // 
            this.line15.Height = 0.1653543F;
            this.line15.Left = 2.677559F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 2.677559F;
            this.line15.X2 = 2.677559F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.1653543F;
            // 
            // line16
            // 
            this.line16.Height = 0.1653543F;
            this.line16.Left = 6.277166F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 6.277166F;
            this.line16.X2 = 6.277166F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.1653543F;
            // 
            // line21
            // 
            this.line21.Height = 0F;
            this.line21.Left = 0F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.1688976F;
            this.line21.Visible = false;
            this.line21.Width = 3.558268F;
            this.line21.X1 = 0F;
            this.line21.X2 = 3.558268F;
            this.line21.Y1 = 0.1688976F;
            this.line21.Y2 = 0.1688976F;
            // 
            // line18
            // 
            this.line18.Height = 0.1653543F;
            this.line18.Left = 7.090552F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 7.090552F;
            this.line18.X2 = 7.090552F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.1653543F;
            // 
            // line19
            // 
            this.line19.Height = 0.1688976F;
            this.line19.Left = 7.090551F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0F;
            this.line19.Width = 0F;
            this.line19.X1 = 7.090551F;
            this.line19.X2 = 7.090551F;
            this.line19.Y1 = 0F;
            this.line19.Y2 = 0.1688976F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM31";
            this.textBox7.Height = 0.1653543F;
            this.textBox7.Left = 3.58937F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: left; vertical-align: middle; ddo" +
    "-char-set: 1";
            this.textBox7.Text = "31";
            this.textBox7.Top = 0F;
            this.textBox7.Width = 1.776379F;
            // 
            // line20
            // 
            this.line20.Height = 0F;
            this.line20.Left = 3.558268F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.1688976F;
            this.line20.Visible = false;
            this.line20.Width = 3.526772F;
            this.line20.X1 = 3.558268F;
            this.line20.X2 = 7.08504F;
            this.line20.Y1 = 0.1688976F;
            this.line20.Y2 = 0.1688976F;
            // 
            // line17
            // 
            this.line17.Height = 0F;
            this.line17.Left = 3.558268F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0F;
            this.line17.Visible = false;
            this.line17.Width = 3.532281F;
            this.line17.X1 = 3.558268F;
            this.line17.X2 = 7.090549F;
            this.line17.Y1 = 0F;
            this.line17.Y2 = 0F;
            // 
            // pageFooter
            // 
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18});
            this.pageFooter.Height = 1.302083F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM71";
            this.textBox9.Height = 0.1299213F;
            this.textBox9.Left = 0F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox9.Text = "71";
            this.textBox9.Top = 0F;
            this.textBox9.Width = 7.090551F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM72";
            this.textBox10.Height = 0.1299213F;
            this.textBox10.Left = 0F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox10.Text = "72";
            this.textBox10.Top = 0.1299213F;
            this.textBox10.Width = 7.090551F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM73";
            this.textBox11.Height = 0.1299212F;
            this.textBox11.Left = 0F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox11.Text = "73";
            this.textBox11.Top = 0.2598425F;
            this.textBox11.Width = 7.090549F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM74";
            this.textBox12.Height = 0.1299212F;
            this.textBox12.Left = 0F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox12.Text = "74";
            this.textBox12.Top = 0.3897638F;
            this.textBox12.Width = 7.090549F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM75";
            this.textBox13.Height = 0.1299212F;
            this.textBox13.Left = 0F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox13.Text = "75";
            this.textBox13.Top = 0.5196851F;
            this.textBox13.Width = 7.090549F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM76";
            this.textBox14.Height = 0.1299212F;
            this.textBox14.Left = 0F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox14.Text = "76";
            this.textBox14.Top = 0.6496063F;
            this.textBox14.Width = 7.090549F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM77";
            this.textBox15.Height = 0.1299212F;
            this.textBox15.Left = 0F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox15.Text = "77";
            this.textBox15.Top = 0.7795276F;
            this.textBox15.Width = 7.090549F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM78";
            this.textBox16.Height = 0.1299212F;
            this.textBox16.Left = 0F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox16.Text = "78";
            this.textBox16.Top = 0.9094489F;
            this.textBox16.Width = 7.090549F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM79";
            this.textBox17.Height = 0.1299212F;
            this.textBox17.Left = 0F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox17.Text = "79";
            this.textBox17.Top = 1.03937F;
            this.textBox17.Width = 7.090549F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM80";
            this.textBox18.Height = 0.1299212F;
            this.textBox18.Left = 0F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
            this.textBox18.Text = "80";
            this.textBox18.Top = 1.169291F;
            this.textBox18.Width = 7.090549F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.textBox19,
            this.txtShohinNm,
            this.txtShohinCd,
            this.txtCompanyName,
            this.txtHyojiDate,
            this.textBox20,
            this.line22,
            this.shape1,
            this.shape2});
            this.reportHeader1.Height = 10.38394F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // label1
            // 
            this.label1.Height = 0.3937007F;
            this.label1.HyperLink = null;
            this.label1.Left = 1.577165F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 20.25pt; font-style: normal; font-weight: bold; te" +
    "xt-align: center; text-decoration: none; vertical-align: middle; ddo-char-set: 1" +
    "";
            this.label1.Text = "決 算 報 告 書";
            this.label1.Top = 2.734252F;
            this.label1.Width = 3.937008F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM05";
            this.textBox19.Height = 0.1968504F;
            this.textBox19.Left = 1.858662F;
            this.textBox19.MultiLine = false;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: center; ddo-c" +
    "har-set: 1";
            this.textBox19.Text = "5";
            this.textBox19.Top = 7.208268F;
            this.textBox19.Width = 3.346457F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.DataField = "ITEM07";
            this.txtShohinNm.Height = 0.1968504F;
            this.txtShohinNm.Left = 1.701575F;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: left; ddo-char-set: 1";
            this.txtShohinNm.Text = "7";
            this.txtShohinNm.Top = 7.665355F;
            this.txtShohinNm.Width = 3.514567F;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.DataField = "ITEM06";
            this.txtShohinCd.Height = 0.1968504F;
            this.txtShohinCd.Left = 1.701575F;
            this.txtShohinCd.MultiLine = false;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: left; ddo-char-set: 1";
            this.txtShohinCd.Text = "6";
            this.txtShohinCd.Top = 7.468504F;
            this.txtShohinCd.Width = 3.503544F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.2700787F;
            this.txtCompanyName.Left = 2.442913F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 18pt; text-align: center; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtCompanyName.Text = "第  26  期";
            this.txtCompanyName.Top = 3.775984F;
            this.txtCompanyName.Width = 2.399606F;
            // 
            // txtHyojiDate
            // 
            this.txtHyojiDate.DataField = "ITEM03";
            this.txtHyojiDate.Height = 0.1811024F;
            this.txtHyojiDate.Left = 3.652756F;
            this.txtHyojiDate.Name = "txtHyojiDate";
            this.txtHyojiDate.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 1";
            this.txtHyojiDate.Text = "3";
            this.txtHyojiDate.Top = 4.385433F;
            this.txtHyojiDate.Width = 2.050787F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM02";
            this.textBox20.Height = 0.1811024F;
            this.textBox20.Left = 1.701575F;
            this.textBox20.MultiLine = false;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.textBox20.Text = "02";
            this.textBox20.Top = 4.385433F;
            this.textBox20.Width = 1.951181F;
            // 
            // line22
            // 
            this.line22.Height = 0F;
            this.line22.Left = 1.869685F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 7.405118F;
            this.line22.Width = 3.353938F;
            this.line22.X1 = 1.869685F;
            this.line22.X2 = 5.223623F;
            this.line22.Y1 = 7.405118F;
            this.line22.Y2 = 7.405118F;
            // 
            // shape1
            // 
            this.shape1.Height = 0.708268F;
            this.shape1.Left = 1.472441F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F);
            this.shape1.Top = 2.55748F;
            this.shape1.Width = 4.161811F;
            // 
            // shape2
            // 
            this.shape2.Height = 0.7685041F;
            this.shape2.Left = 1.441339F;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F);
            this.shape2.Top = 2.528347F;
            this.shape2.Width = 4.233465F;
            // 
            // reportFooter1
            // 
            this.reportFooter1.Height = 0F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // ZMYR10112R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.5905512F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.090551F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSouko;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanaban;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtItem15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtItem14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZaikoKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        public GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
    }
}
