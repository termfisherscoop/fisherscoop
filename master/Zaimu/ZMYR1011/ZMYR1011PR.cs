﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Linq;
using System.ComponentModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;
using jp.co.fsi.common.forms;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMYR1011PR
    {
        #region private変数
        /// <summary>
        /// ZAMR3011(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMYR1011 _pForm;

        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// ユニークID
        /// </summary>
        string _unqId;

        // 参照データテーブル
        DataTable _dtKessanshoKamokuBunrui;         // 決算書科目分類
        DataTable _dtKessanshoKamokuSettei;         // 決算書科目設定
        DataTable _dtKessanshoSettei;               // 決算書設定
        DataTable _dtKessanshoChuki;                // 決算書注記
        DataTable _dtKamokuJis;                     // 科目実績
        DataTable _dtKessanshoKamoku;               // 決算書科目データ(帳票分類・科目分類・行番号で識別)
        int _sort;

        #endregion

        #region プロパティ
        /// <summary>
        /// 勘定科目毎金額格納するGridView用データテーブル
        /// </summary>
        private DataTable _dtKanjoKamokuKingaku = new DataTable();
        public DataTable KanjoKamokuKingaku
        {
            get
            {
                return this._dtKanjoKamokuKingaku;
            }
        }

        /// <summary>
        /// 貸借対照表用データテーブル
        /// </summary>
        private DataTable _dtTaishakuTaishohyo = new DataTable();
        public DataTable TaishakuTaishohyo
        {
            get
            {
                return this._dtTaishakuTaishohyo;
            }
        }

        /// <summary>
        /// 損益計算書用データテーブル
        /// </summary>
        private DataTable _dtSonekiKeisansho = new DataTable();
        public DataTable SonekiKeisansho
        {
            get
            {
                return this._dtSonekiKeisansho;
            }
        }

        /// <summary>
        /// 製造原価用データテーブル
        /// </summary>
        private DataTable _dtSeizoGenka = new DataTable();
        public DataTable SeizoGenka
        {
            get
            {
                return this._dtSeizoGenka;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMYR1011PR(UserInfo uInfo, DbAccess dba, ConfigLoader config, string unuqId, ZMYR1011 frm)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
            this._unqId = unuqId;
            this._pForm = frm;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        public void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            // 参照データ読込
            DataLoad();

            ZMYR1018 msgFrm = new ZMYR1018();
            try
            {
                bool Taishaku = System.Convert.ToBoolean(this._pForm._checkList["Taishaku"]);
                bool Soneki = System.Convert.ToBoolean(this._pForm._checkList["Soneki"]);
                bool Hanbaihi = System.Convert.ToBoolean(this._pForm._checkList["Hanbaihi"]);
                bool Rieki = System.Convert.ToBoolean(this._pForm._checkList["Rieki"]);

                bool dataFlag;

                // 処理中メッセージ表示
                msgFrm.Show();
                msgFrm.Refresh();

                this._dba.BeginTransaction();

                //// 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                //this._dba.Commit();
                //DataTable dtOutput = MakeWkData();
                //dataFlag = InsertWkData(dtOutput);

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.AppendLine("  ITEM01");
                    cols.AppendLine(" ,ITEM02");
                    cols.AppendLine(" ,ITEM03");
                    cols.AppendLine(" ,ITEM04");
                    cols.AppendLine(" ,ITEM05");
                    cols.AppendLine(" ,ITEM06");
                    cols.AppendLine(" ,ITEM07");
                    cols.AppendLine(" ,ITEM08");
                    cols.AppendLine(" ,ITEM09");
                    cols.AppendLine(" ,ITEM10");
                    cols.AppendLine(" ,ITEM11");
                    cols.AppendLine(" ,ITEM12");
                    cols.AppendLine(" ,ITEM13");
                    cols.AppendLine(" ,ITEM14");
                    cols.AppendLine(" ,ITEM15");
                    cols.AppendLine(" ,ITEM16");
                    cols.AppendLine(" ,ITEM17");
                    cols.AppendLine(" ,ITEM18");
                    cols.AppendLine(" ,ITEM19");
                    cols.AppendLine(" ,ITEM20");
                    cols.AppendLine(" ,ITEM21");
                    cols.AppendLine(" ,ITEM22");
                    cols.AppendLine(" ,ITEM23");
                    cols.AppendLine(" ,ITEM24");
                    cols.AppendLine(" ,ITEM25");
                    cols.AppendLine(" ,ITEM26");
                    cols.AppendLine(" ,ITEM27");
                    cols.AppendLine(" ,ITEM28");
                    cols.AppendLine(" ,ITEM29");
                    cols.AppendLine(" ,ITEM30");
                    cols.AppendLine(" ,ITEM31");
                    cols.AppendLine(" ,ITEM32");
                    cols.AppendLine(" ,ITEM33");
                    cols.AppendLine(" ,ITEM34");
                    cols.AppendLine(" ,ITEM35");
                    cols.AppendLine(" ,ITEM36");
                    cols.AppendLine(" ,ITEM37");
                    cols.AppendLine(" ,ITEM38");
                    cols.AppendLine(" ,ITEM39");
                    cols.AppendLine(" ,ITEM40");
                    cols.AppendLine(" ,ITEM41");
                    cols.AppendLine(" ,ITEM42");
                    cols.AppendLine(" ,ITEM43");
                    cols.AppendLine(" ,ITEM44");
                    cols.AppendLine(" ,ITEM45");
                    cols.AppendLine(" ,ITEM46");
                    cols.AppendLine(" ,ITEM47");
                    cols.AppendLine(" ,ITEM48");
                    cols.AppendLine(" ,ITEM49");
                    cols.AppendLine(" ,ITEM50");
                    cols.AppendLine(" ,ITEM51");
                    cols.AppendLine(" ,ITEM52");
                    cols.AppendLine(" ,ITEM53");
                    cols.AppendLine(" ,ITEM54");
                    cols.AppendLine(" ,ITEM55");
                    cols.AppendLine(" ,ITEM56");
                    cols.AppendLine(" ,ITEM57");
                    cols.AppendLine(" ,ITEM58");
                    cols.AppendLine(" ,ITEM59");
                    cols.AppendLine(" ,ITEM60");
                    cols.AppendLine(" ,ITEM61");
                    cols.AppendLine(" ,ITEM62");
                    cols.AppendLine(" ,ITEM63");
                    cols.AppendLine(" ,ITEM64");
                    cols.AppendLine(" ,ITEM65");
                    cols.AppendLine(" ,ITEM66");
                    cols.AppendLine(" ,ITEM67");
                    cols.AppendLine(" ,ITEM68");
                    cols.AppendLine(" ,ITEM69");
                    cols.AppendLine(" ,ITEM70");
                    cols.AppendLine(" ,ITEM71");
                    cols.AppendLine(" ,ITEM72");
                    cols.AppendLine(" ,ITEM73");
                    cols.AppendLine(" ,ITEM74");
                    cols.AppendLine(" ,ITEM75");
                    cols.AppendLine(" ,ITEM76");
                    cols.AppendLine(" ,ITEM77");
                    cols.AppendLine(" ,ITEM78");
                    cols.AppendLine(" ,ITEM79");
                    cols.AppendLine(" ,ITEM80");
                    cols.AppendLine(" ,ITEM81");
                    cols.AppendLine(" ,ITEM82");
                    cols.AppendLine(" ,ITEM83");
                    cols.AppendLine(" ,ITEM84");
                    cols.AppendLine(" ,ITEM85");
                    cols.AppendLine(" ,ITEM86");
                    cols.AppendLine(" ,ITEM87");
                    cols.AppendLine(" ,ITEM88");
                    cols.AppendLine(" ,ITEM89");
                    cols.AppendLine(" ,ITEM90");//90まで作る

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);

                    GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport;// = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                    GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport pdfExport;// = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();

                    #region コメントアウト
                    //// 貸借対照表にチェックがついている場合
                    //if (Taishaku)
                    //{
                    //    dpc.SetParam("@ITEM11_T", SqlDbType.VarChar, 200, 1);
                    //    // データの取得
                    //    DataTable dtOutput = this._dba.GetDataTableByConditionWithParams(
                    //        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID AND ITEM11 = @ITEM11_T", "SORT ASC", dpc);
                    //    // 帳票オブジェクトをインスタンス化
                    //    ZMYR10112R rpt = new ZMYR10112R(dtOutput);

                    //    if (isExcel)
                    //    {
                    //        //GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                    //        xlsExport = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                    //        //SetExcelSetting(xlsExport1);
                    //        xlsExport.MinRowHeight = 0.4f;
                    //        xlsExport.MinColumnWidth = 0.3f;
                    //        rpt.Document.CacheToDisk = true;
                    //        rpt.Run(false);
                    //        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                    //        if (!ValChk.IsEmpty(saveFileName))
                    //        {
                    //            xlsExport.Export(rpt.Document, saveFileName);
                    //            Msg.InfoNm("EXCEL出力", "保存しました。");
                    //            Util.OpenFolder(saveFileName);
                    //        }
                    //        xlsExport.Dispose();
                    //    }
                    //    else if (isPdf)
                    //    {
                    //        //GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    //        pdfExport = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    //        rpt.Run(false);
                    //        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                    //        if (!ValChk.IsEmpty(saveFileName))
                    //        {
                    //            pdfExport.Export(rpt.Document, saveFileName);
                    //            Msg.InfoNm("PDF出力", "保存しました。");
                    //            Util.OpenFolder(saveFileName);
                    //        }
                    //        pdfExport.Dispose();
                    //    }
                    //    else if (isPreview)
                    //    {
                    //        // プレビュー画面表示
                    //        PreviewForm pFrm = new PreviewForm(rpt, this._unqId);
                    //        pFrm.WindowState = FormWindowState.Maximized;
                    //        pFrm.Show();
                    //    }
                    //    else
                    //    {
                    //        // 直接印刷
                    //        rpt.Run(false);
                    //        rpt.Document.Print(true, true, false);
                    //    }
                    //}
                    //// 損益計算書にチェックがついている場合
                    //if (Soneki)
                    //{
                    //    dpc.SetParam("@ITEM11_S", SqlDbType.VarChar, 200, 2);
                    //    // データの取得
                    //    DataTable dtOutput = this._dba.GetDataTableByConditionWithParams(
                    //        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID AND ITEM11 = @ITEM11_S", "SORT ASC", dpc);
                    //    // 帳票オブジェクトをインスタンス化
                    //    ZMYR10113R rpt = new ZMYR10113R(dtOutput);

                    //    if (isExcel)
                    //    {
                    //        //GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                    //        xlsExport = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                    //        //SetExcelSetting(xlsExport1);
                    //        rpt.Run(false);
                    //        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                    //        if (!ValChk.IsEmpty(saveFileName))
                    //        {
                    //            //xlsExport1.Export(rpt.Document, saveFileName);
                    //            xlsExport.Export(rpt.Document, saveFileName);
                    //            Msg.InfoNm("EXCEL出力", "保存しました。");
                    //            Util.OpenFolder(saveFileName);
                    //        }
                    //        xlsExport.Dispose();
                    //    }
                    //    else if (isPdf)
                    //    {
                    //        //GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    //        pdfExport = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    //        rpt.Run(false);
                    //        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                    //        if (!ValChk.IsEmpty(saveFileName))
                    //        {
                    //            //p.Export(rpt.Document, saveFileName);
                    //            pdfExport.Export(rpt.Document, saveFileName);
                    //            Msg.InfoNm("PDF出力", "保存しました。");
                    //            Util.OpenFolder(saveFileName);
                    //        }
                    //        pdfExport.Dispose();
                    //    }
                    //    else if (isPreview)
                    //    {
                    //        // プレビュー画面表示
                    //        PreviewForm pFrm = new PreviewForm(rpt, this._unqId);
                    //        pFrm.WindowState = FormWindowState.Maximized;
                    //        pFrm.Show();
                    //    }
                    //    else
                    //    {
                    //        // 直接印刷
                    //        rpt.Run(false);
                    //        rpt.Document.Print(true, true, false);
                    //    }
                    //}
                    //// 販売費及び一般管理費明細にチェックがついている場合
                    //if (Hanbaihi)
                    //{
                    //    dpc.SetParam("@ITEM11_H", SqlDbType.VarChar, 200, 3);
                    //    // データの取得
                    //    DataTable dtOutput = this._dba.GetDataTableByConditionWithParams(
                    //        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID AND ITEM11 = @ITEM11_H", "SORT ASC", dpc);
                    //    // 帳票オブジェクトをインスタンス化
                    //    ZMYR10114R rpt = new ZMYR10114R(dtOutput);

                    //    if (isExcel)
                    //    {
                    //        //GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                    //        xlsExport = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                    //        //SetExcelSetting(xlsExport1);
                    //        rpt.Run(false);
                    //        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                    //        if (!ValChk.IsEmpty(saveFileName))
                    //        {
                    //            //xlsExport1.Export(rpt.Document, saveFileName);
                    //            xlsExport.Export(rpt.Document, saveFileName);
                    //            Msg.InfoNm("EXCEL出力", "保存しました。");
                    //            Util.OpenFolder(saveFileName);
                    //        }
                    //        xlsExport.Dispose();
                    //    }
                    //    else if (isPdf)
                    //    {
                    //        //GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    //        pdfExport = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    //        rpt.Run(false);
                    //        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                    //        if (!ValChk.IsEmpty(saveFileName))
                    //        {
                    //            //p.Export(rpt.Document, saveFileName);
                    //            pdfExport.Export(rpt.Document, saveFileName);
                    //            Msg.InfoNm("PDF出力", "保存しました。");
                    //            Util.OpenFolder(saveFileName);
                    //        }
                    //        pdfExport.Dispose();
                    //    }
                    //    else if (isPreview)
                    //    {
                    //        // プレビュー画面表示
                    //        PreviewForm pFrm = new PreviewForm(rpt, this._unqId);
                    //        pFrm.WindowState = FormWindowState.Maximized;
                    //        pFrm.Show();
                    //    }
                    //    else
                    //    {
                    //        // 直接印刷
                    //        rpt.Run(false);
                    //        rpt.Document.Print(true, true, false);
                    //    }
                    //}
                    //// 利益処分案にチェックがついている場合
                    //if (Rieki)
                    //{
                    //    dpc.SetParam("@ITEM11_R", SqlDbType.VarChar, 200, 4);
                    //    // データの取得
                    //    DataTable dtOutput = this._dba.GetDataTableByConditionWithParams(
                    //        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID AND ITEM11 = @ITEM11_R", "SORT ASC", dpc);
                    //    // 帳票オブジェクトをインスタンス化
                    //    ZMYR10115R rpt = new ZMYR10115R(dtOutput);

                    //    if (isExcel)
                    //    {
                    //        //GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                    //        xlsExport = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                    //        //SetExcelSetting(xlsExport1);
                    //        rpt.Run(false);
                    //        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                    //        if (!ValChk.IsEmpty(saveFileName))
                    //        {
                    //            //xlsExport1.Export(rpt.Document, saveFileName);
                    //            xlsExport.Export(rpt.Document, saveFileName);
                    //            Msg.InfoNm("EXCEL出力", "保存しました。");
                    //            Util.OpenFolder(saveFileName);
                    //        }
                    //        xlsExport.Dispose();
                    //    }
                    //    else if (isPdf)
                    //    {
                    //        //GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    //        pdfExport = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    //        rpt.Run(false);
                    //        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                    //        if (!ValChk.IsEmpty(saveFileName))
                    //        {
                    //            //p.Export(rpt.Document, saveFileName);
                    //            pdfExport.Export(rpt.Document, saveFileName);
                    //            Msg.InfoNm("PDF出力", "保存しました。");
                    //            Util.OpenFolder(saveFileName);
                    //        }
                    //        pdfExport.Dispose();
                    //    }
                    //    else if (isPreview)
                    //    {
                    //        // プレビュー画面表示
                    //        PreviewForm pFrm = new PreviewForm(rpt, this._unqId);
                    //        pFrm.WindowState = FormWindowState.Maximized;
                    //        pFrm.Show();
                    //    }
                    //    else
                    //    {
                    //        // 直接印刷
                    //        rpt.Run(false);
                    //        rpt.Document.Print(true, true, false);
                    //    }
                    //}
                    #endregion

                    #region レポート合成パターン

                    DataTable dtOutput = null;
                    // 貸借対照表
                    ZMYR10112R rpt1 = null;
                    // 損益計算書
                    ZMYR10113R rpt2 = null;
                    // 販売費及び一般管理費明細
                    ZMYR10114R rpt3 = null;
                    // 利益処分案
                    ZMYR10115R rpt4 = null;
                    // 
                    bool putFlag = false;
                    int putId = 0;
                    // 貸借対照表にチェックがついている場合
                    if (Taishaku)
                    {
                        dpc.SetParam("@ITEM11_T", SqlDbType.VarChar, 200, 1);
                        // データの取得
                        dtOutput = this._dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID AND ITEM11 = @ITEM11_T", "SORT ASC", dpc);
                        rpt1 = new ZMYR10112R(dtOutput);

                        rpt1.Document.Printer.DocumentName = Util.ToString(this._pForm.Condition["ReportName"]);
                        rpt1.Document.Name = Util.ToString(this._pForm.Condition["ReportName"]);

                        rpt1.Run(false);
                        putFlag = true;
                        putId = 1;
                    }
                    // 損益計算書にチェックがついている場合
                    if (Soneki)
                    {
                        dpc.SetParam("@ITEM11_S", SqlDbType.VarChar, 200, 2);
                        // データの取得
                        dtOutput = this._dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID AND ITEM11 = @ITEM11_S", "SORT ASC", dpc);
                        // 帳票オブジェクトをインスタンス化
                        rpt2 = new ZMYR10113R(dtOutput);

                        rpt2.Document.Printer.DocumentName = Util.ToString(this._pForm.Condition["ReportName"]);
                        rpt2.Document.Name = Util.ToString(this._pForm.Condition["ReportName"]);

                        if (putFlag)
                        {
                            rpt2.reportHeader1.Visible = false;
                            rpt2.Run(false);
                            // ページの追加
                            rpt1.Document.Pages.AddRange((GrapeCity.ActiveReports.Document.Section.PagesCollection)rpt2.Document.Pages.Clone());
                        }
                        else
                        {
                            putFlag = true;
                            putId = 2;
                            rpt2.Run(false);
                        }
                    }
                    // 販売費及び一般管理費明細にチェックがついている場合
                    if (Hanbaihi)
                    {
                        dpc.SetParam("@ITEM11_H", SqlDbType.VarChar, 200, 3);
                        // データの取得
                        dtOutput = this._dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID AND ITEM11 = @ITEM11_H", "SORT ASC", dpc);
                        // 帳票オブジェクトをインスタンス化
                        rpt3 = new ZMYR10114R(dtOutput);

                        rpt3.Document.Printer.DocumentName = Util.ToString(this._pForm.Condition["ReportName"]);
                        rpt3.Document.Name = Util.ToString(this._pForm.Condition["ReportName"]);

                        if (putFlag)
                        {
                            rpt3.reportHeader1.Visible = false;
                            rpt3.Run(false);
                            // ページの追加
                            switch (putId)
                            {
                                case 1:
                                    rpt1.Document.Pages.AddRange((GrapeCity.ActiveReports.Document.Section.PagesCollection)rpt3.Document.Pages.Clone());
                                    break;
                                case 2:
                                    rpt2.Document.Pages.AddRange((GrapeCity.ActiveReports.Document.Section.PagesCollection)rpt3.Document.Pages.Clone());
                                    break;
                            }
                        }
                        else
                        {
                            putFlag = true;
                            putId = 3;
                            rpt3.Run(false);
                        }
                    }
                    // 利益処分案にチェックがついている場合
                    if (Rieki)
                    {
                        dpc.SetParam("@ITEM11_R", SqlDbType.VarChar, 200, 4);
                        // データの取得
                        dtOutput = this._dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID AND ITEM11 = @ITEM11_R", "SORT ASC", dpc);
                        // 帳票オブジェクトをインスタンス化
                        rpt4 = new ZMYR10115R(dtOutput);

                        rpt4.Document.Printer.DocumentName = Util.ToString(this._pForm.Condition["ReportName"]);
                        rpt4.Document.Name = Util.ToString(this._pForm.Condition["ReportName"]);

                        if (putFlag)
                        {
                            rpt4.reportHeader1.Visible = false;
                            rpt4.Run(false);
                            // ページの追加
                            switch (putId)
                            {
                                case 1:
                                    rpt1.Document.Pages.AddRange((GrapeCity.ActiveReports.Document.Section.PagesCollection)rpt4.Document.Pages.Clone());
                                    break;
                                case 2:
                                    rpt2.Document.Pages.AddRange((GrapeCity.ActiveReports.Document.Section.PagesCollection)rpt4.Document.Pages.Clone());
                                    break;
                                case 3:
                                    rpt3.Document.Pages.AddRange((GrapeCity.ActiveReports.Document.Section.PagesCollection)rpt4.Document.Pages.Clone());
                                    break;
                            }
                        }
                        else
                        {
                            putFlag = true;
                            putId = 4;
                            rpt4.Run(false);
                        }
                    }
                    // 
                    string docName = "";
                    switch (putId)
                    {
                        case 1:
                            docName = rpt1.Document.Name;
                            break;
                        case 2:
                            docName = rpt2.Document.Name;
                            break;
                        case 3:
                            docName = rpt3.Document.Name;
                            break;
                        case 4:
                            docName = rpt4.Document.Name;
                            break;
                    }
                    if (isExcel)
                    {
                        using (xlsExport = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport())
                        {
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, docName, 2);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                switch (putId)
                                {
                                    case 1:
                                        xlsExport.Export(rpt1.Document, saveFileName);
                                        break;
                                    case 2:
                                        xlsExport.Export(rpt2.Document, saveFileName);
                                        break;
                                    case 3:
                                        xlsExport.Export(rpt3.Document, saveFileName);
                                        break;
                                    case 4:
                                        xlsExport.Export(rpt4.Document, saveFileName);
                                        break;
                                }
                                Msg.InfoNm("EXCEL出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                    }
                    else if (isPdf)
                    {
                        using (pdfExport = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport())
                        {
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, docName, 1);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                switch (putId)
                                {
                                    case 1:
                                        pdfExport.Export(rpt1.Document, saveFileName);
                                        break;
                                    case 2:
                                        pdfExport.Export(rpt2.Document, saveFileName);
                                        break;
                                    case 3:
                                        pdfExport.Export(rpt3.Document, saveFileName);
                                        break;
                                    case 4:
                                        pdfExport.Export(rpt4.Document, saveFileName);
                                        break;
                                }
                                Msg.InfoNm("PDF出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = null;
                        switch (putId)
                        {
                            case 1:
                                pFrm = new PreviewForm(rpt1, this._unqId);
                                pFrm.setDocument(rpt1.Document);
                                break;
                            case 2:
                                pFrm = new PreviewForm(rpt2, this._unqId);
                                pFrm.setDocument(rpt2.Document);
                                break;
                            case 3:
                                pFrm = new PreviewForm(rpt3, this._unqId);
                                pFrm.setDocument(rpt3.Document);
                                break;
                            case 4:
                                pFrm = new PreviewForm(rpt4, this._unqId);
                                pFrm.setDocument(rpt4.Document);
                                break;
                        }
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        switch (putId)
                        {
                            case 1:
                                rpt1.Document.Print(true, true, false);
                                break;
                            case 2:
                                rpt2.Document.Print(true, true, false);
                                break;
                            case 3:
                                rpt3.Document.Print(true, true, false);
                                break;
                            case 4:
                                rpt4.Document.Print(true, true, false);
                                break;
                        }
                    }

                    #endregion

                }
            }

            /*bool dataFlag;
            try
            {
                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this._dba.Commit();
            }
            finally
            {
                this._dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                Report rpt = new Report();
                rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "ZAMR3011.mdb"), "R_ZAMR3011", this._unqId, isPreview);
            }

            // ワークテーブルに作成したデータを削除
            try
            {
                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
                this._dba.Delete("PR_ZM_TBL", "GUID = @GUID", dpc);
                this._dba.Commit();
            }*/
            finally
            {
                this._dba.Rollback();

                msgFrm.Close();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 参照データの読込
        /// </summary>
        private void DataLoad()
        {
            DbParamCollection dpc;
            StringBuilder sql;

            // 出力条件
            decimal bumonCdFr = Util.ToDecimal(this._pForm.Condition["BumonFr"]);               // 部門コード自
            decimal bumonCdTo = Util.ToDecimal(this._pForm.Condition["BumonTo"]);               // 部門コード至
            DateTime denpyoDateFr = Util.ToDate(this._pForm.Condition["DtFr"]);                 // 期間自
            DateTime denpyoDateTo = Util.ToDate(this._pForm.Condition["DtTo"]);                 // 期間至
            bool taxIncluded = (Util.ToInt(this._pForm.Condition["ShohizeiShoriHandan"]) == 1); // 税込フラグ
            // 支所コード
            int shishoCd = Util.ToInt(this._pForm.Condition["ShishoCode"]);

            #region TB_ZM_KESSANSHO_KAMOKU_BUNRUI
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT CHOHYO_BUNRUI");
            sql.Append("      ,HYOJI_JUNI");
            sql.Append("      ,KAMOKU_BUNRUI");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,KAMOKU_BUNRUI_NM");
            sql.Append("      ,TAISHAKU_KUBUN");
            sql.Append("      ,MEISAI_KOMOKUSU");
            sql.Append("      ,MEISAI_KUBUN");
            sql.Append("      ,SHUKEI_KUBUN");
            sql.Append("      ,SHUKEI_KEISANSHIKI");
            sql.Append("      ,BUNRUI_KUBUN");
            sql.Append("      ,KAKKO_KUBUN");
            sql.Append("      ,KAKKO_HYOJI");
            sql.Append("      ,MOJI_SHUBETSU");
            sql.Append("      ,SHIYO_KUBUN");
            sql.Append("  FROM TB_ZM_KESSANSHO_KAMOKU_BUNRUI");
            sql.Append("  WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append("    AND SHISHO_CD = 0");
            sql.Append("    AND KAIKEI_NENDO = @KAIKEI_NENDO");
            _dtKessanshoKamokuBunrui = _dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            #region TB_ZM_KESSANSHO_KAMOKU_SETTEI
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT CHOHYO_BUNRUI");
            sql.Append("      ,KAMOKU_BUNRUI");
            sql.Append("      ,GYO_BANGO");
            sql.Append("      ,KANJO_KAMOKU_CD");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,KANJO_KAMOKU_NM");
            sql.Append("      ,TAISHAKU_KUBUN");
            sql.Append("      ,REGIST_DATE");
            sql.Append("      ,UPDATE_DATE");
            sql.Append("  FROM TB_ZM_KESSANSHO_KAMOKU_SETTEI");
            sql.Append("  WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append("    AND SHISHO_CD = 0");
            sql.Append("    AND KAIKEI_NENDO = @KAIKEI_NENDO");
            _dtKessanshoKamokuSettei = _dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            #region TB_ZM_KESSANSHO_SETTEI
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT KAISHA_CD");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,TAISHAKU_TAISHOHYO_TITLE");
            sql.Append("      ,SONEKI_KEISANSHO_TITLE");
            sql.Append("      ,SEIZO_GENKA_TITLE");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_TITLE");
            sql.Append("      ,HANBAIHI_MEISAIHYO_TITLE");
            sql.Append("      ,RIEKI_SHOBUNAN_TITLE");
            sql.Append("      ,MINUS_KINGAKU");
            sql.Append("      ,GAICHU_KAKOHI_KUBUN");
            sql.Append("  FROM TB_ZM_KESSANSHO_SETTEI");
            sql.Append("  WHERE KAIKEI_NENDO <= @KAIKEI_NENDO");
            sql.Append("    AND KAISHA_CD = @KAISHA_CD");
            sql.Append("    AND SHISHO_CD = 0");
            sql.Append(" ORDER BY KAIKEI_NENDO DESC");

            _dtKessanshoSettei = _dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if (_dtKessanshoSettei.Rows.Count == 0)
            {
                DataRow drKessanshoSetteiDef = _dtKessanshoSettei.NewRow();
                drKessanshoSetteiDef["KAISHA_CD"] = this._uInfo.KaishaCd;
                drKessanshoSetteiDef["KAIKEI_NENDO"] = this._uInfo.KaikeiNendo;
                _dtKessanshoSettei.Rows.Add(drKessanshoSetteiDef);
            }
            #endregion

            #region TB_ZM_KESSANSHO_CHUKI
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT KAISHA_CD");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI1");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI2");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI3");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI4");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI5");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI6");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI7");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI8");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI9");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI10");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI1");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI2");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI3");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI4");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI5");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI6");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI7");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI8");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI9");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI10");
            sql.Append("      ,SEIZO_GENKA_CHUKI1");
            sql.Append("      ,SEIZO_GENKA_CHUKI2");
            sql.Append("      ,SEIZO_GENKA_CHUKI3");
            sql.Append("      ,SEIZO_GENKA_CHUKI4");
            sql.Append("      ,SEIZO_GENKA_CHUKI5");
            sql.Append("      ,SEIZO_GENKA_CHUKI6");
            sql.Append("      ,SEIZO_GENKA_CHUKI7");
            sql.Append("      ,SEIZO_GENKA_CHUKI8");
            sql.Append("      ,SEIZO_GENKA_CHUKI9");
            sql.Append("      ,SEIZO_GENKA_CHUKI10");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI1");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI2");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI3");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI4");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI5");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI6");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI7");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI8");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI9");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CK10");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI1");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI2");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI3");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI4");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI5");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI6");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI7");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI8");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI9");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI10");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI11");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI12");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI13");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI14");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI15");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI16");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI17");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI18");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI19");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI20");
            sql.Append("      ,TOKI_MISHOBUN_RIEKIKIN");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU1");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU2");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU3");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU4");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU5");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU6");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU7");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU8");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU9");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU10");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU1");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU2");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU3");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU4");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU5");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU6");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU7");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU8");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU9");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU10");
            sql.Append("  FROM TB_ZM_KESSANSHO_CHUKI");
            sql.Append("  WHERE KAIKEI_NENDO <= @KAIKEI_NENDO");
            sql.Append("    AND KAISHA_CD = @KAISHA_CD");
            sql.Append("    AND SHISHO_CD = 0");
            sql.Append(" ORDER BY KAIKEI_NENDO DESC");

            try
            {
                _dtKessanshoChuki = _dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (_dtKessanshoChuki.Rows.Count == 0)
            {
                DataRow drKessanshoChukiDef = _dtKessanshoChuki.NewRow();
                drKessanshoChukiDef["KAISHA_CD"] = this._uInfo.KaishaCd;
                drKessanshoChukiDef["KAIKEI_NENDO"] = this._uInfo.KaikeiNendo;
                _dtKessanshoChuki.Rows.Add(drKessanshoChukiDef);
            }
            #endregion

            #region 決算書科目データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT DISTINCT");
            sql.Append("      CHOHYO_BUNRUI");
            sql.Append("      ,KAMOKU_BUNRUI");
            sql.Append("      ,GYO_BANGO");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,KANJO_KAMOKU_NM");
            sql.Append("      ,TAISHAKU_KUBUN");
            sql.Append(" FROM TB_ZM_KESSANSHO_KAMOKU_SETTEI");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append("   AND SHISHO_CD = 0");
            sql.Append("   AND KAIKEI_NENDO = @KAIKEI_NENDO");
            _dtKessanshoKamoku = _dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            #region 科目別実績データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, denpyoDateTo);
            sql = new StringBuilder();
            sql.Append("SELECT A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD");
            sql.Append(",MIN(B.TAISHAKU_KUBUN) AS TAISHAKU_KUBUN");
            sql.Append(",SUM(");
            sql.Append("  CASE");
            sql.Append("   WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.Append("   THEN A." + (taxIncluded ? "ZEIKOMI" : "ZEINUKI") + "_KINGAKU");
            sql.Append("   ELSE (A." + (taxIncluded ? "ZEIKOMI" : "ZEINUKI") + "_KINGAKU * -1) ");
            sql.Append("  END");
            sql.Append(" ) AS KINGAKU");
            sql.Append(",COUNT(*) AS KENSU");

            //sql.Append(",MIN(B.KAMOKU_BUNRUI_CD) AS KAMOKU_BUNRUI_CD");

            sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            sql.Append(" WHERE A.KAISHA_CD = @KAISHA_CD");

            if (shishoCd != 0)
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD");

            sql.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" AND A.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND A.DENPYO_DATE  <= @DENPYO_DATE");
            sql.Append(" AND A.MEISAI_KUBUN  <= " + (taxIncluded ? "0" : "1"));
            sql.Append(" AND A.KANJO_KAMOKU_CD IN ");
            sql.Append(" (SELECT X.KANJO_KAMOKU_CD FROM TB_ZM_KESSANSHO_KAMOKU_SETTEI AS X");
            sql.Append("  WHERE X.KAISHA_CD = @KAISHA_CD AND SHISHO_CD = 0)");
            sql.Append(" GROUP BY A.KANJO_KAMOKU_CD");
            _dtKamokuJis = _dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            #endregion
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 出力条件
            bool injiYes = (Util.ToString(this._pForm.Condition["Inji"]) == "yes");             // 金額０印字オプション 

            int chohyoNo;
            int chohyoBunrui;
            int page;
            int gyo;
            int maxGyo;
            string selectFilter;
            string selectSort;
            DataRow[] drsBunrui;                                                                // 科目分類レコード保持用
            DataRow[] drsKamoku;                                                                // 科目設定レコード保持用
            DataRow drPrZmTbl;                                              

            #region PR_ZM_TBL登録用DataTable定義
            DataTable dtPrZmTbl = new DataTable();
            dtPrZmTbl.Columns.Add("GUID", Type.GetType("System.String"));
            dtPrZmTbl.Columns.Add("SORT", Type.GetType("System.Int32"));
            for (int i = 1; i < 100; i++)           // ITEM01～ITEM99まで
            {
                dtPrZmTbl.Columns.Add("ITEM" + string.Format("{0:D2}", i), Type.GetType("System.String"));
            }
            #endregion

            _sort = 0;                                                                          // PR_ZM_TBL.SORT値初期化

            #region 貸借対照表
            chohyoNo = 1;
            chohyoBunrui = 1;
            page = 1;                                                                           
            
            // 勘定式借方出力内容のセット
            gyo = 0;
            /// 科目分類レコードの取得
            selectFilter ="CHOHYO_BUNRUI = "+ Util.ToString(chohyoBunrui) +  " AND HYOJI_JUNI <= 160";
            selectSort = "HYOJI_JUNI";
            drsBunrui = _dtKessanshoKamokuBunrui.Select(selectFilter,selectSort);
            foreach (DataRow drBunrui in drsBunrui)
            {
                #region 分類レコードの追加
                // 編集内容
                // ITEM14 借方罫線行フラグ
                // ITEM21 借方科目名
                // ITEM22 借方科目太字フラグ
                // ITEM24 借方合計
                drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);                 // 行追加・編集行取得

                switch (Util.ToInt(drBunrui["HYOJI_JUNI"]))                                     // 借方罫線行フラグ
                {
                    case 160:
                        drPrZmTbl["ITEM14"] = "1";
                        break;
                    default:
                        break;
                }

                drPrZmTbl["ITEM21"] = Util.ToString(drBunrui["KAMOKU_BUNRUI_NM"]);              // 借方科目名
                drPrZmTbl["ITEM22"] = Util.ToString(drBunrui["MOJI_SHUBETSU"]);                 // 借方科目太字フラグ

                // 集計計算式の実行
                if (Util.ToInt(drBunrui["SHUKEI_KUBUN"]) > 0 && !ValChk.IsEmpty(drBunrui["SHUKEI_KEISANSHIKI"]))
                {
                    drPrZmTbl["ITEM24"] = FormatKingaku(GetJisKingaku_Keisanshiki(              // 借方合計
                        chohyoBunrui, Util.ToInt(drBunrui["TAISHAKU_KUBUN"]), Util.ToString(drBunrui["SHUKEI_KEISANSHIKI"])));
                }
                #endregion

                #region 明細レコードの追加
                if (Util.ToInt(drBunrui["MEISAI_KUBUN"]) == 1)
                {
                    // 編集内容
                    // ITEM21 借方科目名
                    // ITEM23 借方金額

                    int meisaiKomokusu = Util.ToInt(drBunrui["MEISAI_KOMOKUSU"]);
                    int countKomokusu = 0;

                    // ***<決算書科目分類データのむりやり調整>***         
                    if (Util.ToInt(drBunrui["HYOJI_JUNI"]) == 150) meisaiKomokusu = 12;     
                    // ***<決算書科目分類データのむりやり調整>*** 

                    if (Util.ToInt(drBunrui["KAMOKU_BUNRUI"]) > 0)
                    {
                        // 決算書科目データの取得
                        selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
                            + " AND KAMOKU_BUNRUI = " + Util.ToString(drBunrui["KAMOKU_BUNRUI"]);
                        selectSort = "GYO_BANGO";
                        drsKamoku = _dtKessanshoKamoku.Select(selectFilter, selectSort);
                        foreach (DataRow drKamoku in drsKamoku)
                        {
                            if (countKomokusu < meisaiKomokusu)
                            {
                                decimal kingaku = GetJisKingaku_Meisai(
                                    chohyoBunrui, Util.ToInt(drKamoku["KAMOKU_BUNRUI"]), Util.ToInt(drKamoku["GYO_BANGO"]));

                                // 金額０科目は表示しないオプション判定
                                if (injiYes || kingaku != 0)
                                {
                                    drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);     // 行追加・編集行取得
                                    countKomokusu++;

                                    drPrZmTbl["ITEM21"] = Util.ToString(drKamoku["KANJO_KAMOKU_NM"]);   // 借方科目名
                                    drPrZmTbl["ITEM23"] = FormatKingaku(kingaku);
                                }
                            }
                        }
                    }

                    // ブランク行の追加
                    while (countKomokusu < meisaiKomokusu)
                    {
                        drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);                 // 行追加
                        countKomokusu++;
                    }
                }
                #endregion
            }

            // 勘定式貸方出力内容のセット
            gyo = 0;
            /// 科目分類レコードの取得
            selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui) + " AND HYOJI_JUNI > 160";
            selectSort = "HYOJI_JUNI";
            drsBunrui = _dtKessanshoKamokuBunrui.Select(selectFilter, selectSort);
            foreach (DataRow drBunrui in drsBunrui)
            {
                #region 分類レコードの追加
                // 編集内容
                // ITEM15 貸方罫線行フラグ
                // ITEM31 貸方科目名
                // ITEM32 貸方科目太字フラグ
                // ITEM34 貸方合計

                drPrZmTbl = GetPrZmTblEditRow(dtPrZmTbl, chohyoNo, page, ++gyo);                // 編集行取得
                if (drPrZmTbl == null)
                {
                    drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, gyo);               // 行追加・編集行取得
                }

                switch (Util.ToInt(drBunrui["HYOJI_JUNI"]))                                     // 貸方罫線行フラグ
                {
                    case 250:
                    case 340:
                    case 350:
                        drPrZmTbl["ITEM15"] = "1";
                        break;
                    default:
                        break;
                }

                drPrZmTbl["ITEM31"] = Util.ToString(drBunrui["KAMOKU_BUNRUI_NM"]);              // 貸方科目名
                drPrZmTbl["ITEM32"] = Util.ToString(drBunrui["MOJI_SHUBETSU"]);                 // 貸方科目太字フラグ

                // 集計計算式の実行
                if (!ValChk.IsEmpty(drBunrui["SHUKEI_KEISANSHIKI"]))
                {

                    //if (Util.ToInt(drBunrui["SHUKEI_KUBUN"]) == 2)
                    //{
                    //    drPrZmTbl["ITEM34"] = FormatKingaku(GetJisKingaku_Keisanshiki(              // 貸方合計
                    //        chohyoBunrui, Util.ToInt(drBunrui["TAISHAKU_KUBUN"]), Util.ToString(drBunrui["SHUKEI_KEISANSHIKI"])));
                    //}
                    //else
                    //{
                    //    drPrZmTbl["ITEM34"] = FormatKingaku(GetJisKingaku_Keisanshiki(              // 貸方合計
                    //        2, Util.ToInt(drBunrui["TAISHAKU_KUBUN"]), Util.ToString(drBunrui["SHUKEI_KEISANSHIKI"])));
                    //}

                    decimal val = 0m;
                    decimal calcValue = 0m;
                    // 式の合算科目分類が存在時
                    string calc = IsCalcValue(Util.ToString(drBunrui["SHUKEI_KEISANSHIKI"]));
                    if (calc != "")
                    {
                        // 合成式の場合対象の科目分類から式を取得し算出
                        DataRow[] drsBunruiWork = _dtKessanshoKamokuBunrui.Select("CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui) + " AND KAMOKU_BUNRUI = " + Util.ToString(Math.Abs(Util.ToInt(calc))));
                        if (drsBunruiWork.Length != 0)
                        {
                            calcValue = GetJisKingaku_Keisanshiki(              // 貸方合計
                                (Util.ToInt(drsBunruiWork[0]["SHUKEI_KUBUN"]) == 2 ? chohyoBunrui : 2), Util.ToInt(drsBunruiWork[0]["TAISHAKU_KUBUN"]), Util.ToString(drsBunruiWork[0]["SHUKEI_KEISANSHIKI"]));
                        }
                    }

                    if (Util.ToInt(drBunrui["SHUKEI_KUBUN"]) == 2)
                    {
                        val = GetJisKingaku_Keisanshiki(              // 貸方合計
                            chohyoBunrui, Util.ToInt(drBunrui["TAISHAKU_KUBUN"]), Util.ToString(drBunrui["SHUKEI_KEISANSHIKI"]));
                    }
                    else
                    {
                        drPrZmTbl["ITEM34"] = FormatKingaku(GetJisKingaku_Keisanshiki(              // 貸方合計
                            2, Util.ToInt(drBunrui["TAISHAKU_KUBUN"]), Util.ToString(drBunrui["SHUKEI_KEISANSHIKI"])));
                    }
                    // 追加分がある場合は付け足し
                    if (calc != "")
                    {
                        int pm = Util.ToInt(calc) < 0 ? -1 : 1;
                        if (pm == 1)
                            val = val + calcValue;
                        else
                            val = val - calcValue;
                    }

                    drPrZmTbl["ITEM34"] = FormatKingaku(val);
                }
                #endregion

                #region 明細レコードの追加
                if (Util.ToInt(drBunrui["MEISAI_KUBUN"]) == 1)
                {
                    // 編集内容
                    // ITEM31 貸方科目名
                    // ITEM33 貸方金額

                    int meisaiKomokusu = Util.ToInt(drBunrui["MEISAI_KOMOKUSU"]);
                    int countKomokusu = 0;

                    if (Util.ToInt(drBunrui["KAMOKU_BUNRUI"]) > 0)
                    {
                        // 決算書科目データの取得
                        selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
                            + " AND KAMOKU_BUNRUI = " + Util.ToString(drBunrui["KAMOKU_BUNRUI"]);
                        selectSort = "GYO_BANGO";
                        drsKamoku = _dtKessanshoKamoku.Select(selectFilter, selectSort);
                        foreach (DataRow drKamoku in drsKamoku)
                        {
                            if (countKomokusu < meisaiKomokusu)
                            {
                                decimal kingaku = GetJisKingaku_Meisai(
                                    chohyoBunrui, Util.ToInt(drKamoku["KAMOKU_BUNRUI"]), Util.ToInt(drKamoku["GYO_BANGO"]));

                                // 金額０科目は表示しないオプション判定
                                if (injiYes || kingaku != 0)
                                {

                                    drPrZmTbl = GetPrZmTblEditRow(dtPrZmTbl, chohyoNo, page, ++gyo);        // 編集行取得
                                    if (drPrZmTbl == null)
                                    {
                                        drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, gyo);       // 行追加・編集行取得
                                    }
                                    countKomokusu++;

                                    drPrZmTbl["ITEM31"] = Util.ToString(drKamoku["KANJO_KAMOKU_NM"]);       // 貸方科目名
                                    drPrZmTbl["ITEM33"] = FormatKingaku(kingaku);                           // 貸方金額
                                }
                            }
                        }
                    }

                    // ブランク行の追加
                    while (countKomokusu < meisaiKomokusu)
                    {
                        drPrZmTbl = GetPrZmTblEditRow(dtPrZmTbl, chohyoNo, page, ++gyo);                    // 編集行取得
                        if (drPrZmTbl == null)
                        {
                            drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, gyo);                   // 行追加・編集行取得
                        }
                        countKomokusu++;
                    }
                }
                #endregion
            }
            #endregion

            #region 損益計算書
            chohyoNo = 2;
            chohyoBunrui = 2;
            page = 0;                                                                           
            maxGyo = 56;                                                                        // 行数／1頁

            // 報告式出力内容のセット
            gyo = 0;
            /// 科目分類レコードの取得
            selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui);
            selectSort = "HYOJI_JUNI";
            drsBunrui = _dtKessanshoKamokuBunrui.Select(selectFilter, selectSort);
            foreach (DataRow drBunrui in drsBunrui)
            {
                #region 分類レコードの追加
                // 編集内容
                // ITEM21 科目名
                // ITEM22 科目太字フラグ
                // ITEM23 金額(明細行)
                // ITEM24 合計(分類行)
                // ITEM25 損益計算

                if ((gyo % maxGyo) == 0)                                                        // 改頁判定
                {
                    page++;
                    gyo = 0;
                }
                drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);                 // 行追加・編集行取得

                drPrZmTbl["ITEM21"] = Util.ToString(drBunrui["KAMOKU_BUNRUI_NM"]);              // 科目名
                drPrZmTbl["ITEM22"] = Util.ToString(drBunrui["MOJI_SHUBETSU"]);                 // 科目太字フラグ

                // ***<決算書科目分類データのむりやり調整>***
                if ((Util.ToInt(drBunrui["SHUKEI_KUBUN"]) == 3
                    && Util.ToInt(drBunrui["HYOJI_JUNI"]) != 6
                    && Util.ToInt(drBunrui["HYOJI_JUNI"]) != 320)
                    || Util.ToInt(drBunrui["HYOJI_JUNI"]) == 560)
                {
                    drPrZmTbl["ITEM21"] = "　　　　　" + Util.ToString(drPrZmTbl["ITEM21"]); 
                }
                // ***<決算書科目分類データのむりやり調整>***

                // 集計計算式の実行(集計区分＝3の時は分類行の金額第3列へ表示)
                if (Util.ToInt(drBunrui["SHUKEI_KUBUN"]) == 3 && !ValChk.IsEmpty(drBunrui["SHUKEI_KEISANSHIKI"]))
                {
                    drPrZmTbl["ITEM25"] = FormatKingaku(GetJisKingaku_Keisanshiki(              // 損益計算
                        chohyoBunrui, Util.ToInt(drBunrui["TAISHAKU_KUBUN"]), Util.ToString(drBunrui["SHUKEI_KEISANSHIKI"])));
                }
                #endregion

                #region 明細レコードの追加
                if (Util.ToInt(drBunrui["MEISAI_KUBUN"]) == 1)
                {
                    // 編集内容
                    // ITEM21 科目名
                    // ITEM23 金額
                    int meisaiKomokusu = Util.ToInt(drBunrui["MEISAI_KOMOKUSU"]);
                    int countKomokusu = 0;

                    if (Util.ToInt(drBunrui["KAMOKU_BUNRUI"]) > 0)
                    {
                        // 決算書科目データの取得
                        selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
                            + " AND KAMOKU_BUNRUI = " + Util.ToString(drBunrui["KAMOKU_BUNRUI"]);
                        selectSort = "GYO_BANGO";
                        drsKamoku = _dtKessanshoKamoku.Select(selectFilter, selectSort);
                        foreach (DataRow drKamoku in drsKamoku)
                        {
                            if (countKomokusu < meisaiKomokusu)
                            {
                                decimal kingaku = GetJisKingaku_Meisai(
                                    chohyoBunrui, Util.ToInt(drKamoku["KAMOKU_BUNRUI"]), Util.ToInt(drKamoku["GYO_BANGO"]));

                                // 金額０科目は表示しないオプション判定
                                if (injiYes || kingaku != 0)
                                {
                                    if ((gyo % maxGyo) == 0)                                                // 改頁判定
                                    {
                                        page++;
                                        gyo = 0;
                                    }
                                    drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);         // 行追加・編集行取得
                                    countKomokusu++;

                                    drPrZmTbl["ITEM21"] = "　" + Util.ToString(drKamoku["KANJO_KAMOKU_NM"]);       // 科目名
                                    drPrZmTbl["ITEM23"] = FormatKingaku(kingaku);
                                }
                            }
                        }
                    }

                    // ブランク行の追加
                    while (countKomokusu < meisaiKomokusu)
                    {
                        if ((gyo % maxGyo) == 0)                                                            // 改頁判定
                        {
                            page++;
                            gyo = 0;
                        }
                        drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);                     // 行追加
                        countKomokusu++;
                    }
                }
                #endregion

                // 集計計算式の実行(集計区分＝2の時は科目分類最終行の金額第2列へ表示)
                if (Util.ToInt(drBunrui["SHUKEI_KUBUN"]) == 2 && !ValChk.IsEmpty(drBunrui["SHUKEI_KEISANSHIKI"]))
                {
                    drPrZmTbl["ITEM24"] = FormatKingaku(GetJisKingaku_Keisanshiki(      // 小計
                        chohyoBunrui, Util.ToInt(drBunrui["TAISHAKU_KUBUN"]), Util.ToString(drBunrui["SHUKEI_KEISANSHIKI"])));
                }
            }

            #endregion

            #region 販売費及び一般管理費明細
            chohyoNo = 3;
            chohyoBunrui = 2;
            page = 1;

            // 出力内容をセット
            gyo = 0;
            /// 科目分類レコードの取得
            selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
                            + " AND MEISAI_KUBUN = 0 AND KAMOKU_BUNRUI <> 0"
                            + " AND BUNRUI_KUBUN = 10";// 追加
            selectSort = "HYOJI_JUNI";
            drsBunrui = _dtKessanshoKamokuBunrui.Select(selectFilter, selectSort);
            foreach (DataRow drBunrui in drsBunrui)
            {
                #region 分類レコードの追加
                // 編集内容
                // ITEM14 費目分類番号(表示順位をセット)
                // ITEM21 科目名
                // ITEM23 集計用金額値

                drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);                     // 行追加・編集行取得
                drPrZmTbl["ITEM14"] = string.Format("{0:D4}", Util.ToInt(drBunrui["HYOJI_JUNI"]));  // 表示順位
                drPrZmTbl["ITEM21"] = Util.ToString(drBunrui["KAMOKU_BUNRUI_NM"]);                  // 科目名
                drPrZmTbl["ITEM23"] = "0";                                                          // 集計用金額値

                #endregion

                #region 明細レコードの追加
                // 編集内容
                // ITEM14 費目分類番号(表示順位をセット)
                // ITEM21 科目名
                // ITEM22 金額
                // ITEM23 集計用金額
                int meisaiKomokusu = Util.ToInt(drBunrui["MEISAI_KOMOKUSU"]);
                int countKomokusu = 0;

                // 決算書科目データの取得
                selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
                    + " AND KAMOKU_BUNRUI = " + Util.ToString(drBunrui["KAMOKU_BUNRUI"]);
                selectSort = "GYO_BANGO";
                drsKamoku = _dtKessanshoKamoku.Select(selectFilter, selectSort);
                foreach (DataRow drKamoku in drsKamoku)
                {
                    if (countKomokusu < meisaiKomokusu)
                    {
                        decimal kingaku = GetJisKingaku_Meisai(
                            chohyoBunrui, Util.ToInt(drKamoku["KAMOKU_BUNRUI"]), Util.ToInt(drKamoku["GYO_BANGO"]));

                        // 金額０科目は表示しないオプション判定
                        if (injiYes || kingaku != 0)
                        {
                            drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);             // 行追加・編集行取得
                            countKomokusu++;

                            string spc = "　　　　";
                            drPrZmTbl["ITEM14"] = string.Format("{0:D4}", Util.ToInt(drBunrui["HYOJI_JUNI"]));  // 表示順位
                            drPrZmTbl["ITEM21"] = spc + Util.ToString(drKamoku["KANJO_KAMOKU_NM"]);             // 科目名
                            drPrZmTbl["ITEM22"] = FormatKingaku(kingaku);                                       // 金額
                            drPrZmTbl["ITEM23"] = Util.ToString(kingaku);                                       // 集計用金額値
                        }
                    }
                }

                // ブランク行の追加
                while (countKomokusu < meisaiKomokusu)
                {
                    drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);                             // 行追加
                    
                    // ITEM14でグループレベルを設定しているためブランク行へも値をセット
                    drPrZmTbl["ITEM14"] = string.Format("{0:D4}", Util.ToInt(drBunrui["HYOJI_JUNI"]));          // 表示順位
                    drPrZmTbl["ITEM23"] = "0";                                                                  // 集計用金額値
                    countKomokusu++;
                }
                #endregion
            }

            #endregion

            #region 利益処分案
            chohyoNo = 4;
            chohyoBunrui = 4;
            page = 1;

            // 出力内容をセット
            gyo = 0;
            drPrZmTbl = GetPrZmTblNewRow(dtPrZmTbl, chohyoNo, page, ++gyo);                     // 行追加・編集行取得

            // 編集内容
            // ITEM21-25 分類名1-5
            // ITEM26-30 合計金額1-5
            // ITEM31-50 科目名1-20
            // ITEM51-70 金額1-20
            // ITEM71-90 備考1-20

            #region 決算書科目分類より
            selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui);
            selectSort = "HYOJI_JUNI";
            drsBunrui = _dtKessanshoKamokuBunrui.Select(selectFilter, selectSort);
            foreach (DataRow drBunrui in drsBunrui)
            {
                // 分類名をセット
                /// ITEM21 ～ ITEM25
                switch (Util.ToInt(drBunrui["HYOJI_JUNI"]))
                {
                    case 10:
                        drPrZmTbl["ITEM21"] = Util.ToString(drBunrui["KAMOKU_BUNRUI_NM"]);
                        break;
                    case 20:
                        drPrZmTbl["ITEM22"] = Util.ToString(drBunrui["KAMOKU_BUNRUI_NM"]);
                        break;
                    case 30:
                        drPrZmTbl["ITEM23"] = Util.ToString(drBunrui["KAMOKU_BUNRUI_NM"]);
                        break;
                    case 40:
                        drPrZmTbl["ITEM24"] = Util.ToString(drBunrui["KAMOKU_BUNRUI_NM"]);
                        break;
                    case 50:
                        drPrZmTbl["ITEM25"] = Util.ToString(drBunrui["KAMOKU_BUNRUI_NM"]);
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region 決算書科目設定より
            DataRow[] drs;
            for (int i = 1; i <= 10; i++)
            {
                // 利益処分科目名１～１０
                selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
                                + " AND KAMOKU_BUNRUI = 90020"
                                + " AND GYO_BANGO = " + Util.ToString(i);
                drs = _dtKessanshoKamokuSettei.Select(selectFilter);
                if (drs.Length == 1)
                {
                    drPrZmTbl["ITEM" + Util.ToString(30 + i)] = Util.ToString(drs[0]["KANJO_KAMOKU_NM"]);
                }
                // 任意積立金取崩額科目名１～１０
                selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
                                + " AND KAMOKU_BUNRUI = 90040"
                                + " AND GYO_BANGO = " + Util.ToString(i);
                drs = _dtKessanshoKamokuSettei.Select(selectFilter);
                if (drs.Length == 1)
                {
                    drPrZmTbl["ITEM" + Util.ToString(40 + i)] = Util.ToString(drs[0]["KANJO_KAMOKU_NM"]);
                }
            }
            #endregion

            #region 決算書注記より
            decimal tokiMishobunRiekikin = 0;
            decimal niniTmttknTorikuzushigaku = 0;
            decimal riekikinShobungaku = 0;
            for (int i = 1; i <= 10; i++)
            {
                // 任意積立金取崩額１～１０
                if (!ValChk.IsEmpty(_dtKessanshoChuki.Rows[0]["NINI_TMTTKN_TORIKUZUSHIGAKU" + Util.ToString(i)]))
                {
                    drPrZmTbl["ITEM" + Util.ToString(50 + i)] =
                        FormatKingaku(_dtKessanshoChuki.Rows[0]["NINI_TMTTKN_TORIKUZUSHIGAKU" + Util.ToString(i)]);
                    niniTmttknTorikuzushigaku +=
                        Util.ToDecimal(_dtKessanshoChuki.Rows[0]["NINI_TMTTKN_TORIKUZUSHIGAKU" + Util.ToString(i)]);
                }
                // 利益金処分額１～１０
                if (!ValChk.IsEmpty(_dtKessanshoChuki.Rows[0]["RIEKIKIN_SHOBUNGAKU" + Util.ToString(i)]))
                {
                    drPrZmTbl["ITEM" + Util.ToString(60 + i)] =
                        FormatKingaku(_dtKessanshoChuki.Rows[0]["RIEKIKIN_SHOBUNGAKU" + Util.ToString(i)]);
                    riekikinShobungaku +=
                        Util.ToDecimal(_dtKessanshoChuki.Rows[0]["RIEKIKIN_SHOBUNGAKU" + Util.ToString(i)]);
                }
            }
            tokiMishobunRiekikin = Util.ToDecimal(_dtKessanshoChuki.Rows[0]["TOKI_MISHOBUN_RIEKIKIN"]);
            // 合計１～５
            drPrZmTbl["ITEM26"] = FormatKingaku(tokiMishobunRiekikin);
            drPrZmTbl["ITEM27"] = FormatKingaku(niniTmttknTorikuzushigaku);
            drPrZmTbl["ITEM28"] = FormatKingaku(tokiMishobunRiekikin + niniTmttknTorikuzushigaku);
            drPrZmTbl["ITEM29"] = FormatKingaku(riekikinShobungaku);
            drPrZmTbl["ITEM30"] = FormatKingaku(tokiMishobunRiekikin + niniTmttknTorikuzushigaku - riekikinShobungaku);
            // 備考１～２０
            for (int i = 1; i <= 20; i++)
            {
                drPrZmTbl["ITEM" + Util.ToString(70 + i)] =
                    Util.ToString(_dtKessanshoChuki.Rows[0]["RIEKI_SHOBUNAN_CHUKI" + Util.ToString(i)]);
            }
            #endregion

            #endregion

            // ワークテーブルへ出力
            foreach (DataRow dr in dtPrZmTbl.Rows)
            {
                ArrayList alParams = SetPrZmTblParams(dr);
                this._dba.Insert("PR_ZM_TBL", (DbParamCollection)alParams[0]);
            }

            return true;
        }

        /// <summary>
        /// 実績金額を集計(集計計算式)
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類</param>
        /// <param name="taishakuKubun">貸借区分</param>
        /// <param name="shukeiKeisanshiki">集計計算式</param>
        /// <returns></returns>
        private decimal GetJisKingaku_Keisanshiki(int chohyoBunrui, int taishakuKubun, string shukeiKeisanshiki)
        {
            decimal ret = 0;

            // 引数の計算式から科目分類コードと加減情報を取得
            string shiki = shukeiKeisanshiki;
            if (shiki.Substring(0, 1) != "+" && shiki.Substring(0, 1) != "-")
            {
                shiki = "+" + shiki;                                    // 先頭科目分類コードへ加減文字を付加
            }

            // 科目分類値の要素数
            int itemCount = shiki.Length - Regex.Replace(shiki, "[+-]", "").Length;
            // 計算式を科目分類値と加減情報に分割
            int[] sign = new int[itemCount];
            int[] bunruiCd = new int[itemCount];
            for (int i = 0; i < itemCount; i++)
            {
                // 加減サインを配列へセット
                sign[i] = (shiki.Substring(0, 1) == "+") ? 1 : -1;
                shiki = shiki.Substring(1);                             // 計算式を加減サイン抽出後文字列へ編集

                // 科目分類コードを配列へセット
                Match m = Regex.Match(shiki, "[+-]");
                if (m.Index > 0)
                {
                    bunruiCd[i] = Util.ToInt(shiki.Substring(0, m.Index));
                }
                else
                {
                    bunruiCd[i] = Util.ToInt(shiki);
                }
                shiki = shiki.Substring(m.Index);                       // 計算式を科目分類コード抽出後文字列へ編集
            }

            // 重複科目の管理（設定で勘定科目が重複する設定時に金額が二重計上の為）
            List<string> KamokuList = new List<string>();

            // 集計演算を実行
            for (int i = 0; i < itemCount; i++)
            {
                // 決算書科目設定データから該当する科目分類の勘定科目コードを取得し、科目実績データを集計する。
                // (∵決算書科目分類データの計算式は勘定科目マスタの実科目分類ではなく、
                //  　決算書科目設定データにセットされている科目分類値を元に集計されている)

                // 決算書科目設定データから該当する科目分類の勘定科目を抽出
                ////string filter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
                ////                + " AND KAMOKU_BUNRUI = " + Util.ToString(bunruiCd[i]);
                string filter = "KAMOKU_BUNRUI = " + Util.ToString(bunruiCd[i]);
                DataRow[] fRowsSti = _dtKessanshoKamokuSettei.Select(filter);
                foreach (DataRow dr in fRowsSti)
                {
                    if (!KamokuList.Contains(Util.ToString(dr["KANJO_KAMOKU_CD"])))
                    {
                        // 科目実績データから該当する勘定科目レコードを抽出
                        DataRow[] fRowsJis = _dtKamokuJis.Select("KANJO_KAMOKU_CD = " + Util.ToString(dr["KANJO_KAMOKU_CD"]));
                        if (fRowsJis.Length == 1)
                        {
                            if (chohyoBunrui == 1)      // BS集計
                            {
                                // 決算書科目分類データと勘定科目マスタ(科目実績データ)の貸借区分が異なる場合は符号反転する
                                if (taishakuKubun == Util.ToInt(fRowsJis[0]["TAISHAKU_KUBUN"]))
                                {
                                    ret = ret + Util.ToDecimal(fRowsJis[0]["KINGAKU"]) * Util.ToInt(sign[i]);
                                }
                                else
                                {
                                    ret = ret + Util.ToDecimal(fRowsJis[0]["KINGAKU"]) * Util.ToInt(sign[i]) * (-1);
                                }
                            }
                            else                        // PL集計
                            {
                                // 計算式のとおりに算出
                                ret = ret + Util.ToDecimal(fRowsJis[0]["KINGAKU"]) * Util.ToInt(sign[i]);
                            }
                        }
                        KamokuList.Add(Util.ToString(dr["KANJO_KAMOKU_CD"]));
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// 明細行実績金額を集計
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類</param>
        /// <param name="kamokuBunrui">科目分類</param>
        /// <param name="gyoBango">行番号</param>
        /// <returns></returns>
        private decimal GetJisKingaku_Meisai(int chohyoBunrui, int kamokuBunrui, int gyoBango)
        {
            decimal ret = 0;

            // 決算書科目設定データから該当する科目分類の勘定科目を抽出
            ////string filter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
            ////                + " AND KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui)
            ////                + " AND GYO_BANGO = " + Util.ToString(gyoBango);
            string filter = "KAMOKU_BUNRUI = " + Util.ToString(kamokuBunrui)
                            + " AND GYO_BANGO = " + Util.ToString(gyoBango);
            DataRow[] fRowsSti = _dtKessanshoKamokuSettei.Select(filter);
            foreach (DataRow dr in fRowsSti)
            {
                // 科目実績データから該当する勘定科目レコードを抽出
                DataRow[] fRowsJis = _dtKamokuJis.Select("KANJO_KAMOKU_CD = " + Util.ToString(dr["KANJO_KAMOKU_CD"]));
                if (fRowsJis.Length == 1)
                {
                    // 決算書科目設定データと勘定科目マスタ(科目実績データ)の貸借区分が異なる場合は符号反転する
                    if (Util.ToInt(dr["TAISHAKU_KUBUN"]) == Util.ToInt(fRowsJis[0]["TAISHAKU_KUBUN"]))
                    {
                        ret = ret + Util.ToDecimal(fRowsJis[0]["KINGAKU"]);
                    }
                    else
                    {
                        ret = ret + Util.ToDecimal(fRowsJis[0]["KINGAKU"]) * (-1);
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// 出力ワークDataTable保存済DataRowの取得
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="chohyoBunrui">帳票分類</param>
        /// <param name="pageNo">頁番号</param>
        /// <param name="gyoNo">行番号</param>
        /// <returns>帳票・頁・行番号で指定される行</returns>
        private DataRow GetPrZmTblEditRow(DataTable dt, int chohyoBunrui, int pageNo, int gyoNo)
        {
            string filter = "ITEM11 = '" + Util.ToString(chohyoBunrui) + "'"
                            + " AND ITEM12 = '" + string.Format("{0:D2}", pageNo) + "'"
                            + " AND ITEM13 = '" + string.Format("{0:D3}", gyoNo) + "'";
            DataRow[] foundRows = dt.Select(filter);
            if (foundRows.Length == 1)
            {
                return foundRows[0];
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// 出力ワークDataTable追加DataRowの取得
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="chohyoBunrui">帳票分類</param>
        /// <param name="pageNo">頁番号</param>
        /// <param name="gyoNo">行番号</param>
        /// <returns>帳票・頁・行番号で指定される行</returns>
        private DataRow GetPrZmTblNewRow(DataTable dt, int chohyoBunrui, int pageNo, int gyoNo)
        {
            string filter = "ITEM11 = '" + Util.ToString(chohyoBunrui) + "'"
                            + " AND ITEM12 = '" + string.Format("{0:D2}", pageNo) + "'"
                            + " AND ITEM13 = '" + string.Format("{0:D3}", gyoNo) + "'";
            DataRow[] foundRows = dt.Select(filter);
            if (foundRows.Length == 0)
            {
                DataRow newRow = dt.NewRow();
                newRow["GUID"] = this._unqId;
                newRow["SORT"] = ++_sort;
                // 共通情報
                newRow["ITEM01"] = "第 " + Util.ToString(this._uInfo.KessanKi) + " 期";
                newRow["ITEM02"] = string.Format("自 {0} {2}年 {3}月 {4}日",
                    Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtFr"]), this._dba));
                newRow["ITEM03"] = string.Format("至 {0} {2}年 {3}月 {4}日",
                    Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtTo"]), this._dba));
                newRow["ITEM04"] = string.Format("{0} {2}年 {3}月 {4}日 現在",
                    Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtTo"]), this._dba));
                newRow["ITEM05"] = this._uInfo.KaishaNm;
                newRow["ITEM06"] = Util.ToString(this._uInfo.KaikeiSettings["JUSHO1"]);
                newRow["ITEM07"] = Util.ToString(this._uInfo.KaikeiSettings["JUSHO2"]);
                // 帳票タイトル・注記
                switch (chohyoBunrui)
                {
                    case 1:
                        newRow["ITEM10"] = Util.ToString(_dtKessanshoSettei.Rows[0]["TAISHAKU_TAISHOHYO_TITLE"]);
                        for (int i = 1; i <= 10; i++)
                        {
                            newRow["ITEM" + Util.ToString(i + 70)] =
                                Util.ToString(_dtKessanshoChuki.Rows[0]["TAISHAKU_TAISHOHYO_CHUKI" + Util.ToString(i)]);
                        }
                        break;
                    case 2:
                        newRow["ITEM10"] = Util.ToString(_dtKessanshoSettei.Rows[0]["SONEKI_KEISANSHO_TITLE"]);
                        for (int i = 1; i <= 10; i++)
                        {
                            newRow["ITEM" + Util.ToString(i + 70)] =
                                Util.ToString(_dtKessanshoChuki.Rows[0]["SONEKI_KEISANSHO_CHUKI" + Util.ToString(i)]);
                        }
                        break;
                    case 3:
                        newRow["ITEM10"] = Util.ToString(_dtKessanshoSettei.Rows[0]["HANBAIHI_MEISAIHYO_TITLE"]);
                        break;
                    case 4:
                        newRow["ITEM10"] = Util.ToString(_dtKessanshoSettei.Rows[0]["RIEKI_SHOBUNAN_TITLE"]);
                        for (int i = 1; i <= 20; i++)
                        {
                            newRow["ITEM" + Util.ToString(i + 70)] =
                                Util.ToString(_dtKessanshoChuki.Rows[0]["RIEKI_SHOBUNAN_CHUKI" + Util.ToString(i)]);
                        }
                        break;
                    default:
                        break;
                }
                // レコード情報
                newRow["ITEM11"] = Util.ToString(chohyoBunrui);
                newRow["ITEM12"] = string.Format("{0:D2}", pageNo);
                newRow["ITEM13"] = string.Format("{0:D3}", gyoNo);
                // 行追加
                dt.Rows.Add(newRow);

                return newRow;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        /// <summary>
        /// 金額値フォーマット
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string FormatKingaku(object value)
        {
            string ret;

            if (ValChk.IsEmpty(value))
            {
                ret = "";
            }
            else
            {
                if (Util.ToInt(_dtKessanshoSettei.Rows[0]["MINUS_KINGAKU"]) == 0)
                {
                    ret = Util.ToDecimal(Util.ToString(value)).ToString("#,##0;-#,##0");
                }
                else
                {
                    
                    ret = Util.ToDecimal(Util.ToString(value)).ToString("#,##0;△#,##0");
                }
            }

            return ret;
        }

        /// <summary>
        /// PR_ZM_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrZmTblParams(DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection insParam = new DbParamCollection();

            // SET句パラメータ
            insParam.SetParam("@GUID", SqlDbType.VarChar, 36, Util.ToString(dr["GUID"]));
            insParam.SetParam("@SORT", SqlDbType.Int, 4, Util.ToInt(dr["SORT"]));

            for (int i = 1; i < 100; i++)       // ITEM01～ITEM99までループ
            {
                {
                    insParam.SetParam("@ITEM" + string.Format("{0:D2}", i), SqlDbType.VarChar, 200,
                        Util.ToString(dr["ITEM" + string.Format("{0:D2}", i)]));
                }
            }
            insParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(insParam);

            return alParams;
        }

        /// <summary>
        /// 式に合成科目分類が含まれているかの判定
        /// </summary>
        /// <param name="calc">検証式</param>
        /// <returns>該当が存在時に科目分類を戻す</returns>
        private string IsCalcValue(string calc)
        {
            string siki = calc.Replace("+", " +");
            siki = siki.Replace("-", " -");
            string[] items = siki.Split(' ');
            foreach (string item in items)
            {
                int cd = Util.ToInt(item);
                if (Math.Abs(cd) > 90000)
                    return item;
            }
            return "";
        }

        #endregion
    }
}
