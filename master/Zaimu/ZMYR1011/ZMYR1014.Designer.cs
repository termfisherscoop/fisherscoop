﻿namespace jp.co.fsi.zm.zmyr1011
{
    partial class ZMYR1014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPages = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pnlPage1 = new jp.co.fsi.common.FsiPanel();
            this.txtTaishakuTaishohyoChuki10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoChuki9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoChuki8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoChuki7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoChuki6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoChuki5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoChuki4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoChuki3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoChuki2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoChuki1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pnlPage2 = new jp.co.fsi.common.FsiPanel();
            this.txtSonekiKeisanshoChuki10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoChuki9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoChuki8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoChuki7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoChuki6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoChuki5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoChuki4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoChuki3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoChuki2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoChuki1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pnlPage3 = new jp.co.fsi.common.FsiPanel();
            this.txtSeizoGenkaChuki10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaChuki9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaChuki8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaChuki7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaChuki6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaChuki5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaChuki4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaChuki3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaChuki2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaChuki1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.tabPages.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pnlPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.pnlPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.pnlPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(4, 9);
            this.lblTitle.Size = new System.Drawing.Size(681, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 255);
            this.pnlDebug.Size = new System.Drawing.Size(698, 100);
            // 
            // tabPages
            // 
            this.tabPages.Controls.Add(this.tabPage1);
            this.tabPages.Controls.Add(this.tabPage2);
            this.tabPages.Controls.Add(this.tabPage3);
            this.tabPages.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tabPages.ItemSize = new System.Drawing.Size(68, 25);
            this.tabPages.Location = new System.Drawing.Point(21, 21);
            this.tabPages.Name = "tabPages";
            this.tabPages.SelectedIndex = 0;
            this.tabPages.Size = new System.Drawing.Size(659, 269);
            this.tabPages.TabIndex = 902;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pnlPage1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(651, 236);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "F1 貸借対照表";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pnlPage1
            // 
            this.pnlPage1.BackColor = System.Drawing.Color.Transparent;
            this.pnlPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki10);
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki9);
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki8);
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki7);
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki6);
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki5);
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki4);
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki3);
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki2);
            this.pnlPage1.Controls.Add(this.txtTaishakuTaishohyoChuki1);
            this.pnlPage1.Location = new System.Drawing.Point(13, 13);
            this.pnlPage1.Name = "pnlPage1";
            this.pnlPage1.Size = new System.Drawing.Size(625, 210);
            this.pnlPage1.TabIndex = 8;
            // 
            // txtTaishakuTaishohyoChuki10
            // 
            this.txtTaishakuTaishohyoChuki10.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki10.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki10.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki10.Location = new System.Drawing.Point(3, 183);
            this.txtTaishakuTaishohyoChuki10.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki10.Name = "txtTaishakuTaishohyoChuki10";
            this.txtTaishakuTaishohyoChuki10.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki10.TabIndex = 10;
            this.txtTaishakuTaishohyoChuki10.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtTaishakuTaishohyoChuki9
            // 
            this.txtTaishakuTaishohyoChuki9.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki9.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki9.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki9.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki9.Location = new System.Drawing.Point(3, 163);
            this.txtTaishakuTaishohyoChuki9.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki9.Name = "txtTaishakuTaishohyoChuki9";
            this.txtTaishakuTaishohyoChuki9.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki9.TabIndex = 9;
            this.txtTaishakuTaishohyoChuki9.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtTaishakuTaishohyoChuki8
            // 
            this.txtTaishakuTaishohyoChuki8.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki8.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki8.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki8.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki8.Location = new System.Drawing.Point(3, 143);
            this.txtTaishakuTaishohyoChuki8.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki8.Name = "txtTaishakuTaishohyoChuki8";
            this.txtTaishakuTaishohyoChuki8.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki8.TabIndex = 8;
            this.txtTaishakuTaishohyoChuki8.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtTaishakuTaishohyoChuki7
            // 
            this.txtTaishakuTaishohyoChuki7.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki7.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki7.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki7.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki7.Location = new System.Drawing.Point(3, 123);
            this.txtTaishakuTaishohyoChuki7.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki7.Name = "txtTaishakuTaishohyoChuki7";
            this.txtTaishakuTaishohyoChuki7.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki7.TabIndex = 7;
            this.txtTaishakuTaishohyoChuki7.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtTaishakuTaishohyoChuki6
            // 
            this.txtTaishakuTaishohyoChuki6.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki6.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki6.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki6.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki6.Location = new System.Drawing.Point(3, 103);
            this.txtTaishakuTaishohyoChuki6.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki6.Name = "txtTaishakuTaishohyoChuki6";
            this.txtTaishakuTaishohyoChuki6.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki6.TabIndex = 6;
            this.txtTaishakuTaishohyoChuki6.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtTaishakuTaishohyoChuki5
            // 
            this.txtTaishakuTaishohyoChuki5.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki5.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki5.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki5.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki5.Location = new System.Drawing.Point(3, 83);
            this.txtTaishakuTaishohyoChuki5.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki5.Name = "txtTaishakuTaishohyoChuki5";
            this.txtTaishakuTaishohyoChuki5.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki5.TabIndex = 5;
            this.txtTaishakuTaishohyoChuki5.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtTaishakuTaishohyoChuki4
            // 
            this.txtTaishakuTaishohyoChuki4.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki4.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki4.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki4.Location = new System.Drawing.Point(3, 63);
            this.txtTaishakuTaishohyoChuki4.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki4.Name = "txtTaishakuTaishohyoChuki4";
            this.txtTaishakuTaishohyoChuki4.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki4.TabIndex = 4;
            this.txtTaishakuTaishohyoChuki4.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtTaishakuTaishohyoChuki3
            // 
            this.txtTaishakuTaishohyoChuki3.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki3.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki3.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki3.Location = new System.Drawing.Point(3, 43);
            this.txtTaishakuTaishohyoChuki3.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki3.Name = "txtTaishakuTaishohyoChuki3";
            this.txtTaishakuTaishohyoChuki3.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki3.TabIndex = 3;
            this.txtTaishakuTaishohyoChuki3.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtTaishakuTaishohyoChuki2
            // 
            this.txtTaishakuTaishohyoChuki2.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki2.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki2.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki2.Location = new System.Drawing.Point(3, 23);
            this.txtTaishakuTaishohyoChuki2.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki2.Name = "txtTaishakuTaishohyoChuki2";
            this.txtTaishakuTaishohyoChuki2.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki2.TabIndex = 2;
            this.txtTaishakuTaishohyoChuki2.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtTaishakuTaishohyoChuki1
            // 
            this.txtTaishakuTaishohyoChuki1.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoChuki1.DisplayLength = null;
            this.txtTaishakuTaishohyoChuki1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoChuki1.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoChuki1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoChuki1.Location = new System.Drawing.Point(3, 3);
            this.txtTaishakuTaishohyoChuki1.MaxLength = 128;
            this.txtTaishakuTaishohyoChuki1.Name = "txtTaishakuTaishohyoChuki1";
            this.txtTaishakuTaishohyoChuki1.Size = new System.Drawing.Size(615, 20);
            this.txtTaishakuTaishohyoChuki1.TabIndex = 1;
            this.txtTaishakuTaishohyoChuki1.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pnlPage2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(651, 236);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "F2 損益計算書";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pnlPage2
            // 
            this.pnlPage2.BackColor = System.Drawing.Color.Transparent;
            this.pnlPage2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki10);
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki9);
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki8);
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki7);
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki6);
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki5);
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki4);
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki3);
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki2);
            this.pnlPage2.Controls.Add(this.txtSonekiKeisanshoChuki1);
            this.pnlPage2.Location = new System.Drawing.Point(13, 13);
            this.pnlPage2.Name = "pnlPage2";
            this.pnlPage2.Size = new System.Drawing.Size(625, 210);
            this.pnlPage2.TabIndex = 9;
            // 
            // txtSonekiKeisanshoChuki10
            // 
            this.txtSonekiKeisanshoChuki10.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki10.DisplayLength = null;
            this.txtSonekiKeisanshoChuki10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki10.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki10.Location = new System.Drawing.Point(3, 183);
            this.txtSonekiKeisanshoChuki10.MaxLength = 128;
            this.txtSonekiKeisanshoChuki10.Name = "txtSonekiKeisanshoChuki10";
            this.txtSonekiKeisanshoChuki10.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki10.TabIndex = 10;
            this.txtSonekiKeisanshoChuki10.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSonekiKeisanshoChuki9
            // 
            this.txtSonekiKeisanshoChuki9.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki9.DisplayLength = null;
            this.txtSonekiKeisanshoChuki9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki9.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki9.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki9.Location = new System.Drawing.Point(3, 163);
            this.txtSonekiKeisanshoChuki9.MaxLength = 128;
            this.txtSonekiKeisanshoChuki9.Name = "txtSonekiKeisanshoChuki9";
            this.txtSonekiKeisanshoChuki9.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki9.TabIndex = 9;
            this.txtSonekiKeisanshoChuki9.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSonekiKeisanshoChuki8
            // 
            this.txtSonekiKeisanshoChuki8.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki8.DisplayLength = null;
            this.txtSonekiKeisanshoChuki8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki8.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki8.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki8.Location = new System.Drawing.Point(3, 143);
            this.txtSonekiKeisanshoChuki8.MaxLength = 128;
            this.txtSonekiKeisanshoChuki8.Name = "txtSonekiKeisanshoChuki8";
            this.txtSonekiKeisanshoChuki8.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki8.TabIndex = 8;
            this.txtSonekiKeisanshoChuki8.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSonekiKeisanshoChuki7
            // 
            this.txtSonekiKeisanshoChuki7.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki7.DisplayLength = null;
            this.txtSonekiKeisanshoChuki7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki7.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki7.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki7.Location = new System.Drawing.Point(3, 123);
            this.txtSonekiKeisanshoChuki7.MaxLength = 128;
            this.txtSonekiKeisanshoChuki7.Name = "txtSonekiKeisanshoChuki7";
            this.txtSonekiKeisanshoChuki7.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki7.TabIndex = 7;
            this.txtSonekiKeisanshoChuki7.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSonekiKeisanshoChuki6
            // 
            this.txtSonekiKeisanshoChuki6.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki6.DisplayLength = null;
            this.txtSonekiKeisanshoChuki6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki6.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki6.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki6.Location = new System.Drawing.Point(3, 103);
            this.txtSonekiKeisanshoChuki6.MaxLength = 128;
            this.txtSonekiKeisanshoChuki6.Name = "txtSonekiKeisanshoChuki6";
            this.txtSonekiKeisanshoChuki6.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki6.TabIndex = 6;
            this.txtSonekiKeisanshoChuki6.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSonekiKeisanshoChuki5
            // 
            this.txtSonekiKeisanshoChuki5.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki5.DisplayLength = null;
            this.txtSonekiKeisanshoChuki5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki5.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki5.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki5.Location = new System.Drawing.Point(3, 83);
            this.txtSonekiKeisanshoChuki5.MaxLength = 128;
            this.txtSonekiKeisanshoChuki5.Name = "txtSonekiKeisanshoChuki5";
            this.txtSonekiKeisanshoChuki5.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki5.TabIndex = 5;
            this.txtSonekiKeisanshoChuki5.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSonekiKeisanshoChuki4
            // 
            this.txtSonekiKeisanshoChuki4.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki4.DisplayLength = null;
            this.txtSonekiKeisanshoChuki4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki4.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki4.Location = new System.Drawing.Point(3, 63);
            this.txtSonekiKeisanshoChuki4.MaxLength = 128;
            this.txtSonekiKeisanshoChuki4.Name = "txtSonekiKeisanshoChuki4";
            this.txtSonekiKeisanshoChuki4.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki4.TabIndex = 4;
            this.txtSonekiKeisanshoChuki4.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSonekiKeisanshoChuki3
            // 
            this.txtSonekiKeisanshoChuki3.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki3.DisplayLength = null;
            this.txtSonekiKeisanshoChuki3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki3.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki3.Location = new System.Drawing.Point(3, 43);
            this.txtSonekiKeisanshoChuki3.MaxLength = 128;
            this.txtSonekiKeisanshoChuki3.Name = "txtSonekiKeisanshoChuki3";
            this.txtSonekiKeisanshoChuki3.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki3.TabIndex = 3;
            this.txtSonekiKeisanshoChuki3.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSonekiKeisanshoChuki2
            // 
            this.txtSonekiKeisanshoChuki2.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki2.DisplayLength = null;
            this.txtSonekiKeisanshoChuki2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki2.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki2.Location = new System.Drawing.Point(3, 23);
            this.txtSonekiKeisanshoChuki2.MaxLength = 128;
            this.txtSonekiKeisanshoChuki2.Name = "txtSonekiKeisanshoChuki2";
            this.txtSonekiKeisanshoChuki2.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki2.TabIndex = 2;
            this.txtSonekiKeisanshoChuki2.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSonekiKeisanshoChuki1
            // 
            this.txtSonekiKeisanshoChuki1.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoChuki1.DisplayLength = null;
            this.txtSonekiKeisanshoChuki1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoChuki1.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoChuki1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoChuki1.Location = new System.Drawing.Point(3, 3);
            this.txtSonekiKeisanshoChuki1.MaxLength = 128;
            this.txtSonekiKeisanshoChuki1.Name = "txtSonekiKeisanshoChuki1";
            this.txtSonekiKeisanshoChuki1.Size = new System.Drawing.Size(615, 20);
            this.txtSonekiKeisanshoChuki1.TabIndex = 1;
            this.txtSonekiKeisanshoChuki1.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.pnlPage3);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(651, 236);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "F3 製造原価報告書";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // pnlPage3
            // 
            this.pnlPage3.BackColor = System.Drawing.Color.Transparent;
            this.pnlPage3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki10);
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki9);
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki8);
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki7);
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki6);
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki5);
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki4);
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki3);
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki2);
            this.pnlPage3.Controls.Add(this.txtSeizoGenkaChuki1);
            this.pnlPage3.Location = new System.Drawing.Point(13, 13);
            this.pnlPage3.Name = "pnlPage3";
            this.pnlPage3.Size = new System.Drawing.Size(625, 210);
            this.pnlPage3.TabIndex = 8;
            // 
            // txtSeizoGenkaChuki10
            // 
            this.txtSeizoGenkaChuki10.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki10.DisplayLength = null;
            this.txtSeizoGenkaChuki10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki10.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki10.Location = new System.Drawing.Point(3, 183);
            this.txtSeizoGenkaChuki10.MaxLength = 128;
            this.txtSeizoGenkaChuki10.Name = "txtSeizoGenkaChuki10";
            this.txtSeizoGenkaChuki10.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki10.TabIndex = 10;
            this.txtSeizoGenkaChuki10.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSeizoGenkaChuki9
            // 
            this.txtSeizoGenkaChuki9.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki9.DisplayLength = null;
            this.txtSeizoGenkaChuki9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki9.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki9.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki9.Location = new System.Drawing.Point(3, 163);
            this.txtSeizoGenkaChuki9.MaxLength = 128;
            this.txtSeizoGenkaChuki9.Name = "txtSeizoGenkaChuki9";
            this.txtSeizoGenkaChuki9.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki9.TabIndex = 9;
            this.txtSeizoGenkaChuki9.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSeizoGenkaChuki8
            // 
            this.txtSeizoGenkaChuki8.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki8.DisplayLength = null;
            this.txtSeizoGenkaChuki8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki8.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki8.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki8.Location = new System.Drawing.Point(3, 143);
            this.txtSeizoGenkaChuki8.MaxLength = 128;
            this.txtSeizoGenkaChuki8.Name = "txtSeizoGenkaChuki8";
            this.txtSeizoGenkaChuki8.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki8.TabIndex = 8;
            this.txtSeizoGenkaChuki8.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSeizoGenkaChuki7
            // 
            this.txtSeizoGenkaChuki7.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki7.DisplayLength = null;
            this.txtSeizoGenkaChuki7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki7.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki7.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki7.Location = new System.Drawing.Point(3, 123);
            this.txtSeizoGenkaChuki7.MaxLength = 128;
            this.txtSeizoGenkaChuki7.Name = "txtSeizoGenkaChuki7";
            this.txtSeizoGenkaChuki7.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki7.TabIndex = 7;
            this.txtSeizoGenkaChuki7.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSeizoGenkaChuki6
            // 
            this.txtSeizoGenkaChuki6.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki6.DisplayLength = null;
            this.txtSeizoGenkaChuki6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki6.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki6.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki6.Location = new System.Drawing.Point(3, 103);
            this.txtSeizoGenkaChuki6.MaxLength = 128;
            this.txtSeizoGenkaChuki6.Name = "txtSeizoGenkaChuki6";
            this.txtSeizoGenkaChuki6.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki6.TabIndex = 6;
            this.txtSeizoGenkaChuki6.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSeizoGenkaChuki5
            // 
            this.txtSeizoGenkaChuki5.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki5.DisplayLength = null;
            this.txtSeizoGenkaChuki5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki5.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki5.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki5.Location = new System.Drawing.Point(3, 83);
            this.txtSeizoGenkaChuki5.MaxLength = 128;
            this.txtSeizoGenkaChuki5.Name = "txtSeizoGenkaChuki5";
            this.txtSeizoGenkaChuki5.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki5.TabIndex = 5;
            this.txtSeizoGenkaChuki5.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSeizoGenkaChuki4
            // 
            this.txtSeizoGenkaChuki4.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki4.DisplayLength = null;
            this.txtSeizoGenkaChuki4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki4.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki4.Location = new System.Drawing.Point(3, 63);
            this.txtSeizoGenkaChuki4.MaxLength = 128;
            this.txtSeizoGenkaChuki4.Name = "txtSeizoGenkaChuki4";
            this.txtSeizoGenkaChuki4.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki4.TabIndex = 4;
            this.txtSeizoGenkaChuki4.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSeizoGenkaChuki3
            // 
            this.txtSeizoGenkaChuki3.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki3.DisplayLength = null;
            this.txtSeizoGenkaChuki3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki3.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki3.Location = new System.Drawing.Point(3, 43);
            this.txtSeizoGenkaChuki3.MaxLength = 128;
            this.txtSeizoGenkaChuki3.Name = "txtSeizoGenkaChuki3";
            this.txtSeizoGenkaChuki3.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki3.TabIndex = 3;
            this.txtSeizoGenkaChuki3.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSeizoGenkaChuki2
            // 
            this.txtSeizoGenkaChuki2.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki2.DisplayLength = null;
            this.txtSeizoGenkaChuki2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki2.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki2.Location = new System.Drawing.Point(3, 23);
            this.txtSeizoGenkaChuki2.MaxLength = 128;
            this.txtSeizoGenkaChuki2.Name = "txtSeizoGenkaChuki2";
            this.txtSeizoGenkaChuki2.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki2.TabIndex = 2;
            this.txtSeizoGenkaChuki2.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtSeizoGenkaChuki1
            // 
            this.txtSeizoGenkaChuki1.AutoSizeFromLength = false;
            this.txtSeizoGenkaChuki1.DisplayLength = null;
            this.txtSeizoGenkaChuki1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaChuki1.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaChuki1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaChuki1.Location = new System.Drawing.Point(3, 3);
            this.txtSeizoGenkaChuki1.MaxLength = 128;
            this.txtSeizoGenkaChuki1.Name = "txtSeizoGenkaChuki1";
            this.txtSeizoGenkaChuki1.Size = new System.Drawing.Size(615, 20);
            this.txtSeizoGenkaChuki1.TabIndex = 1;
            this.txtSeizoGenkaChuki1.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // ZMYR1014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 358);
            this.Controls.Add(this.tabPages);
            this.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMYR1014";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.tabPages, 0);
            this.pnlDebug.ResumeLayout(false);
            this.tabPages.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.pnlPage1.ResumeLayout(false);
            this.pnlPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.pnlPage2.ResumeLayout(false);
            this.pnlPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.pnlPage3.ResumeLayout(false);
            this.pnlPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabPages;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private jp.co.fsi.common.FsiPanel pnlPage1;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki10;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki9;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki8;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki7;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki6;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki5;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki4;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki3;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki2;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoChuki1;
        private jp.co.fsi.common.FsiPanel pnlPage2;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki10;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki9;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki8;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki7;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki6;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki5;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki4;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki3;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki2;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoChuki1;
        private jp.co.fsi.common.FsiPanel pnlPage3;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki10;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki9;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki8;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki7;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki6;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki5;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki4;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki3;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki2;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaChuki1;




    }
}