﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10114R の帳票
    /// </summary>
    public partial class ZMYR10114R : BaseReport
    {

        public ZMYR10114R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// 値のフォーマット
        /// </summary>
        private void groupFooter2_Format(object sender, EventArgs e)
        {

            this.txtKingaku02.Text = Util.FormatNum(Util.ToDecimal(this.txtKingaku02.Text));
        }

        /// <summary>
        /// 値のフォーマット
        /// </summary>
        private void groupFooter1_Format(object sender, EventArgs e)
        {

            this.txtKingaku03.Text = Util.FormatNum(Util.ToDecimal(this.txtKingaku03.Text));
        }
    }
}
