﻿namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10113R の概要の説明です。
    /// </summary>
    partial class ZMYR10113R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMYR10113R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtSouko = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTanaban = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.crossSectionLine1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine2 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine3 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine4 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine6 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtKesusu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZaikoKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHyojiDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSouko,
            this.txtTanaban,
            this.textBox1,
            this.label3,
            this.textBox2,
            this.txtTitle02,
            this.txtTitle03,
            this.line4,
            this.line6,
            this.crossSectionLine1,
            this.crossSectionLine2,
            this.crossSectionLine3,
            this.crossSectionLine4,
            this.crossSectionLine6});
            this.pageHeader.Height = 0.7033847F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtSouko
            // 
            this.txtSouko.DataField = "ITEM10";
            this.txtSouko.Height = 0.2283465F;
            this.txtSouko.Left = 0.3956695F;
            this.txtSouko.Name = "txtSouko";
            this.txtSouko.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtSouko.Text = "10損益計算書";
            this.txtSouko.Top = 0F;
            this.txtSouko.Width = 6.299212F;
            // 
            // txtTanaban
            // 
            this.txtTanaban.DataField = "ITEM05";
            this.txtTanaban.Height = 0.1555118F;
            this.txtTanaban.Left = 0.06259843F;
            this.txtTanaban.MultiLine = false;
            this.txtTanaban.Name = "txtTanaban";
            this.txtTanaban.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTanaban.Text = "5";
            this.txtTanaban.Top = 0.2283465F;
            this.txtTanaban.Width = 6.694881F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM02";
            this.textBox1.Height = 0.1555118F;
            this.textBox1.Left = 2.010236F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.textBox1.Text = "02";
            this.textBox1.Top = 0.3716536F;
            this.textBox1.Width = 1.487402F;
            // 
            // label3
            // 
            this.label3.Height = 0.1488189F;
            this.label3.HyperLink = null;
            this.label3.Left = 6.08819F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 128";
            this.label3.Text = "(単位：円)";
            this.label3.Top = 0.3783465F;
            this.label3.Width = 1.002362F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM03";
            this.textBox2.Height = 0.1555118F;
            this.textBox2.Left = 3.585827F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.textBox2.Text = "03";
            this.textBox2.Top = 0.3716536F;
            this.textBox2.Width = 1.487402F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.1795276F;
            this.txtTitle02.Left = 2.384186E-07F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: bottom; ddo-char-set: 1";
            this.txtTitle02.Text = "科     目";
            this.txtTitle02.Top = 0.5271654F;
            this.txtTitle02.Width = 2.836221F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.1795277F;
            this.txtTitle03.Left = 2.836221F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: bottom; ddo-char-set: 1";
            this.txtTitle03.Text = "金                     額";
            this.txtTitle03.Top = 0.5271654F;
            this.txtTitle03.Width = 4.254332F;
            // 
            // line4
            // 
            this.line4.Height = 0.0001641512F;
            this.line4.Left = 0F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.7066929F;
            this.line4.Width = 7.090549F;
            this.line4.X1 = 7.090549F;
            this.line4.X2 = 0F;
            this.line4.Y1 = 0.7066929F;
            this.line4.Y2 = 0.706857F;
            // 
            // line6
            // 
            this.line6.Height = 0.0001639128F;
            this.line6.Left = 0F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.5271654F;
            this.line6.Width = 7.090549F;
            this.line6.X1 = 7.090549F;
            this.line6.X2 = 0F;
            this.line6.Y1 = 0.5271654F;
            this.line6.Y2 = 0.5273293F;
            // 
            // crossSectionLine1
            // 
            this.crossSectionLine1.Bottom = 0F;
            this.crossSectionLine1.Left = 2.836221F;
            this.crossSectionLine1.LineWeight = 1F;
            this.crossSectionLine1.Name = "crossSectionLine1";
            this.crossSectionLine1.Top = 0.5271654F;
            // 
            // crossSectionLine2
            // 
            this.crossSectionLine2.Bottom = 0F;
            this.crossSectionLine2.Left = 7.090552F;
            this.crossSectionLine2.LineWeight = 1F;
            this.crossSectionLine2.Name = "crossSectionLine2";
            this.crossSectionLine2.Top = 0.5271654F;
            // 
            // crossSectionLine3
            // 
            this.crossSectionLine3.Bottom = 0F;
            this.crossSectionLine3.Left = 0F;
            this.crossSectionLine3.LineWeight = 1F;
            this.crossSectionLine3.Name = "crossSectionLine3";
            this.crossSectionLine3.Top = 0.5271654F;
            // 
            // crossSectionLine4
            // 
            this.crossSectionLine4.Bottom = 0F;
            this.crossSectionLine4.Left = 4.23504F;
            this.crossSectionLine4.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.crossSectionLine4.LineWeight = 1F;
            this.crossSectionLine4.Name = "crossSectionLine4";
            this.crossSectionLine4.Top = 0.7066929F;
            // 
            // crossSectionLine6
            // 
            this.crossSectionLine6.Bottom = 0F;
            this.crossSectionLine6.Left = 5.698426F;
            this.crossSectionLine6.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.crossSectionLine6.LineWeight = 1F;
            this.crossSectionLine6.Name = "crossSectionLine6";
            this.crossSectionLine6.Top = 0.7066929F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtKesusu01,
            this.txtKesusu03,
            this.txtZaikoKingaku,
            this.textBox3,
            this.textBox4});
            this.detail.Height = 0.1584099F;
            this.detail.Name = "detail";
            // 
            // txtKesusu01
            // 
            this.txtKesusu01.DataField = "ITEM24";
            this.txtKesusu01.Height = 0.1653543F;
            this.txtKesusu01.Left = 4.23504F;
            this.txtKesusu01.MultiLine = false;
            this.txtKesusu01.Name = "txtKesusu01";
            this.txtKesusu01.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtKesusu01.Text = "24";
            this.txtKesusu01.Top = 0F;
            this.txtKesusu01.Width = 1.432283F;
            // 
            // txtKesusu03
            // 
            this.txtKesusu03.DataField = "ITEM21";
            this.txtKesusu03.Height = 0.1653543F;
            this.txtKesusu03.Left = 0F;
            this.txtKesusu03.Name = "txtKesusu03";
            this.txtKesusu03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: left; vertical-align: middle; ddo" +
    "-char-set: 1";
            this.txtKesusu03.Text = "21";
            this.txtKesusu03.Top = 0F;
            this.txtKesusu03.Width = 2.83622F;
            // 
            // txtZaikoKingaku
            // 
            this.txtZaikoKingaku.DataField = "ITEM23";
            this.txtZaikoKingaku.Height = 0.1653543F;
            this.txtZaikoKingaku.Left = 2.836221F;
            this.txtZaikoKingaku.Name = "txtZaikoKingaku";
            this.txtZaikoKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtZaikoKingaku.Text = "23";
            this.txtZaikoKingaku.Top = 0F;
            this.txtZaikoKingaku.Width = 1.357481F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM25";
            this.textBox3.Height = 0.1653543F;
            this.textBox3.Left = 5.698426F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.textBox3.Text = "25";
            this.textBox3.Top = 0F;
            this.textBox3.Width = 1.37126F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM22";
            this.textBox4.Height = 0.1653543F;
            this.textBox4.Left = 1.001969F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox4.Text = "22";
            this.textBox4.Top = 0F;
            this.textBox4.Visible = false;
            this.textBox4.Width = 1.834252F;
            // 
            // pageFooter
            // 
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line3,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18});
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            this.pageFooter.Format += new System.EventHandler(this.pageFooter_Format);
            // 
            // line3
            // 
            this.line3.Height = 0.0001640394F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0F;
            this.line3.Width = 7.090552F;
            this.line3.X1 = 7.090552F;
            this.line3.X2 = 0F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0.0001640394F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM71";
            this.textBox9.Height = 0.1299213F;
            this.textBox9.Left = 0F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox9.Text = "71";
            this.textBox9.Top = 0.2602362F;
            this.textBox9.Visible = false;
            this.textBox9.Width = 7.090549F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM72";
            this.textBox10.Height = 0.1299213F;
            this.textBox10.Left = 0F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox10.Text = "72";
            this.textBox10.Top = 0.3901576F;
            this.textBox10.Visible = false;
            this.textBox10.Width = 7.090549F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM73";
            this.textBox11.Height = 0.1299212F;
            this.textBox11.Left = 0F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox11.Text = "73";
            this.textBox11.Top = 0.5200788F;
            this.textBox11.Visible = false;
            this.textBox11.Width = 7.090549F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM74";
            this.textBox12.Height = 0.1299212F;
            this.textBox12.Left = 0F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox12.Text = "74";
            this.textBox12.Top = 0.6500001F;
            this.textBox12.Visible = false;
            this.textBox12.Width = 7.090549F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM75";
            this.textBox13.Height = 0.1299212F;
            this.textBox13.Left = 0F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox13.Text = "75";
            this.textBox13.Top = 0.7799213F;
            this.textBox13.Visible = false;
            this.textBox13.Width = 7.090549F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM76";
            this.textBox14.Height = 0.1299212F;
            this.textBox14.Left = 0F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox14.Text = "76";
            this.textBox14.Top = 0.9098425F;
            this.textBox14.Visible = false;
            this.textBox14.Width = 7.090549F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM77";
            this.textBox15.Height = 0.1299212F;
            this.textBox15.Left = 0F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox15.Text = "77";
            this.textBox15.Top = 1.039764F;
            this.textBox15.Visible = false;
            this.textBox15.Width = 7.090549F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM78";
            this.textBox16.Height = 0.1299212F;
            this.textBox16.Left = 0F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox16.Text = "78";
            this.textBox16.Top = 1.169685F;
            this.textBox16.Visible = false;
            this.textBox16.Width = 7.090549F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM79";
            this.textBox17.Height = 0.1299212F;
            this.textBox17.Left = 0F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox17.Text = "79";
            this.textBox17.Top = 1.299606F;
            this.textBox17.Visible = false;
            this.textBox17.Width = 7.090549F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM80";
            this.textBox18.Height = 0.1299212F;
            this.textBox18.Left = 0F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox18.Text = "80";
            this.textBox18.Top = 1.429527F;
            this.textBox18.Visible = false;
            this.textBox18.Width = 7.090549F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line5});
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Visible = false;
            // 
            // line5
            // 
            this.line5.Height = 0.0001639128F;
            this.line5.Left = 0F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 7.090549F;
            this.line5.X1 = 7.090549F;
            this.line5.X2 = 0F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0.0001639128F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.textBox5,
            this.txtShohinNm,
            this.txtShohinCd,
            this.txtCompanyName,
            this.txtHyojiDate,
            this.textBox6,
            this.line1,
            this.shape1,
            this.shape2,
            this.textBox7});
            this.reportHeader1.Height = 10.31102F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // label1
            // 
            this.label1.Height = 0.3937007F;
            this.label1.HyperLink = null;
            this.label1.Left = 1.577165F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 20.25pt; font-style: normal; font-weight: bold; te" +
    "xt-align: center; text-decoration: none; vertical-align: middle; ddo-char-set: 1" +
    "";
            this.label1.Text = "決 算 報 告 書";
            this.label1.Top = 2.734252F;
            this.label1.Width = 3.937008F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM05";
            this.textBox5.Height = 0.1968504F;
            this.textBox5.Left = 1.858662F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: center; ddo-c" +
    "har-set: 1";
            this.textBox5.Text = "5";
            this.textBox5.Top = 7.208268F;
            this.textBox5.Width = 3.346457F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.DataField = "ITEM07";
            this.txtShohinNm.Height = 0.1968504F;
            this.txtShohinNm.Left = 1.754331F;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: left; ddo-char-set: 1";
            this.txtShohinNm.Text = "7";
            this.txtShohinNm.Top = 7.665355F;
            this.txtShohinNm.Width = 3.450788F;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.DataField = "ITEM06";
            this.txtShohinCd.Height = 0.1968504F;
            this.txtShohinCd.Left = 1.754331F;
            this.txtShohinCd.MultiLine = false;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: left; ddo-char-set: 1";
            this.txtShohinCd.Text = "6";
            this.txtShohinCd.Top = 7.468504F;
            this.txtShohinCd.Width = 3.450788F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.2700787F;
            this.txtCompanyName.Left = 2.442913F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 18pt; text-align: center; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtCompanyName.Text = "第  26  期";
            this.txtCompanyName.Top = 3.775984F;
            this.txtCompanyName.Width = 2.399606F;
            // 
            // txtHyojiDate
            // 
            this.txtHyojiDate.DataField = "ITEM03";
            this.txtHyojiDate.Height = 0.1811024F;
            this.txtHyojiDate.Left = 3.652756F;
            this.txtHyojiDate.Name = "txtHyojiDate";
            this.txtHyojiDate.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 1";
            this.txtHyojiDate.Text = "3";
            this.txtHyojiDate.Top = 4.385433F;
            this.txtHyojiDate.Width = 2.050787F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM02";
            this.textBox6.Height = 0.1811024F;
            this.textBox6.Left = 1.701575F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.textBox6.Text = "02";
            this.textBox6.Top = 4.385433F;
            this.textBox6.Width = 1.951181F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 1.851181F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 7.405118F;
            this.line1.Width = 3.353939F;
            this.line1.X1 = 1.851181F;
            this.line1.X2 = 5.20512F;
            this.line1.Y1 = 7.405118F;
            this.line1.Y2 = 7.405118F;
            // 
            // shape1
            // 
            this.shape1.Height = 0.708268F;
            this.shape1.Left = 1.472441F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 2.55748F;
            this.shape1.Width = 4.161811F;
            // 
            // shape2
            // 
            this.shape2.Height = 0.768504F;
            this.shape2.Left = 1.441339F;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = 9.999999F;
            this.shape2.Top = 2.528347F;
            this.shape2.Width = 4.233465F;
            // 
            // textBox7
            // 
            this.textBox7.Height = 0.4066929F;
            this.textBox7.Left = 6.08819F;
            this.textBox7.Name = "textBox7";
            this.textBox7.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox7.Text = null;
            this.textBox7.Top = 0.4578741F;
            this.textBox7.Visible = false;
            this.textBox7.Width = 0.3946037F;
            // 
            // reportFooter1
            // 
            this.reportFooter1.Height = 0F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // ZMYR10113R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.5905512F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.090552F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSouko;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanaban;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZaikoKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine1;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine2;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine3;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine4;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        public GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
    }
}
