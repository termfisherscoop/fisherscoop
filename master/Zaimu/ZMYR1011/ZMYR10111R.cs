﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10111R の帳票
    /// </summary>
    public partial class ZMYR10111R : BaseReport
    {

        public ZMYR10111R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

    }
}
