﻿namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10111R の概要の説明です。
    /// </summary>
    partial class ZMYR10111R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMYR10111R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTanaban = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHyojiDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.txtTanaban,
            this.txtShohinNm,
            this.txtShohinCd,
            this.txtCompanyName,
            this.txtHyojiDate,
            this.textBox2,
            this.line1,
            this.shape1,
            this.shape2});
            this.detail.Height = 10.90158F;
            this.detail.Name = "detail";
            // 
            // label1
            // 
            this.label1.Height = 0.3937008F;
            this.label1.HyperLink = null;
            this.label1.Left = 1.577165F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 20.25pt; font-style: normal; font-weight: bold; te" +
    "xt-align: center; text-decoration: none; vertical-align: middle; ddo-char-set: 1" +
    "";
            this.label1.Text = "決 算 報 告 書";
            this.label1.Top = 2.734252F;
            this.label1.Width = 3.937008F;
            // 
            // txtTanaban
            // 
            this.txtTanaban.DataField = "ITEM05";
            this.txtTanaban.Height = 0.1968504F;
            this.txtTanaban.Left = 1.858662F;
            this.txtTanaban.MultiLine = false;
            this.txtTanaban.Name = "txtTanaban";
            this.txtTanaban.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: center; ddo-c" +
    "har-set: 1";
            this.txtTanaban.Text = "5";
            this.txtTanaban.Top = 7.208268F;
            this.txtTanaban.Width = 3.346457F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.DataField = "ITEM07";
            this.txtShohinNm.Height = 0.1968504F;
            this.txtShohinNm.Left = 1.701575F;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: left; ddo-char-set: 1";
            this.txtShohinNm.Text = "7";
            this.txtShohinNm.Top = 7.665355F;
            this.txtShohinNm.Width = 3.503544F;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.DataField = "ITEM06";
            this.txtShohinCd.Height = 0.1968504F;
            this.txtShohinCd.Left = 1.701575F;
            this.txtShohinCd.MultiLine = false;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: left; ddo-char-set: 1";
            this.txtShohinCd.Text = "6";
            this.txtShohinCd.Top = 7.468504F;
            this.txtShohinCd.Width = 3.503544F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.2700787F;
            this.txtCompanyName.Left = 2.442913F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 18pt; text-align: center; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtCompanyName.Text = "第  26  期";
            this.txtCompanyName.Top = 3.775984F;
            this.txtCompanyName.Width = 2.399606F;
            // 
            // txtHyojiDate
            // 
            this.txtHyojiDate.DataField = "ITEM03";
            this.txtHyojiDate.Height = 0.1811024F;
            this.txtHyojiDate.Left = 3.652756F;
            this.txtHyojiDate.Name = "txtHyojiDate";
            this.txtHyojiDate.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 1";
            this.txtHyojiDate.Text = "3";
            this.txtHyojiDate.Top = 4.385433F;
            this.txtHyojiDate.Width = 2.050787F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM02";
            this.textBox2.Height = 0.1811024F;
            this.textBox2.Left = 1.701575F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.textBox2.Text = "02";
            this.textBox2.Top = 4.385433F;
            this.textBox2.Width = 1.951181F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 1.851181F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 7.405118F;
            this.line1.Width = 3.353938F;
            this.line1.X1 = 1.851181F;
            this.line1.X2 = 5.205119F;
            this.line1.Y1 = 7.405118F;
            this.line1.Y2 = 7.405118F;
            // 
            // shape1
            // 
            this.shape1.Height = 0.7082678F;
            this.shape1.Left = 1.472441F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F);
            this.shape1.Top = 2.55748F;
            this.shape1.Width = 4.161811F;
            // 
            // shape2
            // 
            this.shape2.Height = 0.768504F;
            this.shape2.Left = 1.441339F;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F);
            this.shape2.Top = 2.528347F;
            this.shape2.Width = 4.233465F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // ZAMR3011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.5905512F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.090551F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanaban;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;

    }
}
