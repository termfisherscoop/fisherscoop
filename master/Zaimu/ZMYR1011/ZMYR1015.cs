﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// 利益処分(ZMYR1015)
    /// </summary>
    public partial class ZMYR1015 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// ZAMR3011(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMYR1011 _pForm;
        // 
        int hyoji_juni = 0;
        // 参照データテーブル
        DataTable _dtKessanshoKamokuBunrui;         // 決算書科目分類
        DataTable _dtKessanshoKamokuSettei;         // 決算書科目設定
        DataTable _dtKessanshoSettei;               // 決算書設定
        DataTable _dtKessanshoChuki;                // 決算書注記
        DataTable _dtKamokuJis;                     // 科目実績
        DataTable _dtKessanshoKamoku;               // 決算書科目データ(帳票分類・科目分類・行番号で識別)
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMYR1015(ZMYR1011 frm)
        {
            InitializeComponent();
            BindGotFocusEvent();

            this._pForm = frm;
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // サイズ変更
            //this.Size = new Size(650, 660);
            // Escape F6 F8のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF8.Location = this.btnF3.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            //this.ShowFButton = true;

            try
            {
                // 設定ファイル情報
                this.hyoji_juni = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMYR1011", "Setting", "hyoji_juni"));
            }
            catch (Exception)
            {
                this.hyoji_juni = 570;
            }

            // 画面の初期表示
            DispData();

            //// フォーカス設定
            //this.txtKingaku1.Focus();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = ("保存しますか？");
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            try
            {
                string tableNm;
                string where;
                ArrayList alParams;

                // トランザクション開始
                this.Dba.BeginTransaction();

                #region 決算書科目分類テーブル更新
                tableNm = "TB_ZM_KESSANSHO_KAMOKU_BUNRUI";
                where = "KAISHA_CD = @KAISHA_CD"
                        + " AND SHISHO_CD = 0"
                        + " AND CHOHYO_BUNRUI = @CHOHYO_BUNRUI"
                        + " AND HYOJI_JUNI = @HYOJI_JUNI"
                        + " AND KAMOKU_BUNRUI = @KAMOKU_BUNRUI"
                        + " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                /// 表示順位10レコード
                alParams = SetZmKessanshoKamokuBunrui(4, 10, 90010, Util.ToString(this.txtKamokuBunruiNm10.Text));
                this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);
                /// 表示順位20レコード
                alParams = SetZmKessanshoKamokuBunrui(4, 20, 90020, Util.ToString(this.txtKamokuBunruiNm20.Text));
                this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);
                /// 表示順位30レコード
                alParams = SetZmKessanshoKamokuBunrui(4, 30, 0, Util.ToString(this.txtKamokuBunruiNm30.Text));
                this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);
                /// 表示順位40レコード
                alParams = SetZmKessanshoKamokuBunrui(4, 40, 90040, Util.ToString(this.txtKamokuBunruiNm40.Text));
                this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);
                /// 表示順位50レコード
                alParams = SetZmKessanshoKamokuBunrui(4, 50, 0, Util.ToString(this.txtKamokuBunruiNm50.Text));
                this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);
                #endregion

                #region 決算書科目設定テーブル更新
                // 保存済データをクリア
                tableNm = "TB_ZM_KESSANSHO_KAMOKU_SETTEI";
                where = "KAISHA_CD = @KAISHA_CD"
                        + " AND SHISHO_CD = 0"
                        + " AND CHOHYO_BUNRUI = @CHOHYO_BUNRUI"
                        + " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
                dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, 4);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                this.Dba.Delete(tableNm, where, dpc);
                // 編集データを保存
                tableNm = "TB_ZM_KESSANSHO_KAMOKU_SETTEI";
                where = "KAISHA_CD = @KAISHA_CD"
                        + " AND SHISHO_CD = 0"
                        + " AND CHOHYO_BUNRUI = @CHOHYO_BUNRUI"
                        + " AND KAMOKU_BUNRUI = @KAMOKU_BUNRUI"
                        + " AND GYO_BANGO = @GYO_BANGO"
                        + " AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD"
                        + " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                for (int i = 1; i <= 10; i++)
                {
                    Control[] ctrl;
                    FsiTextBox txtBox;

                    // 科目分類90020科目名をセット
                    ctrl = this.Controls.Find("txtKanjoKamokuNm90020_" + Util.ToString(i), true);
                    if (ctrl.Length > 0)
                    {
                        txtBox = (FsiTextBox)ctrl[0];
                        alParams = SetZmKessanshoKamokuSettei(4, 90020, i, txtBox.Text);
                        this.Dba.Insert(tableNm, (DbParamCollection)alParams[0]);
                    }
                    // 科目分類90040科目名をセット
                    ctrl = this.Controls.Find("txtKanjoKamokuNm90040_" + Util.ToString(i), true);
                    if (ctrl.Length > 0)
                    {
                        txtBox = (FsiTextBox)ctrl[0];
                        alParams = SetZmKessanshoKamokuSettei(4, 90040, i, txtBox.Text);
                        this.Dba.Insert(tableNm, (DbParamCollection)alParams[0]);
                    }
                }
                #endregion

                #region 決算書注記テーブル更新
                tableNm = "TB_ZM_KESSANSHO_CHUKI";
                where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = 0 AND KAIKEI_NENDO = @KAIKEI_NENDO";
                alParams = SetZmKessanshoChukiParams();
                this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);
                #endregion

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            // 利益処分注記入力処理
            ZMYR1016 frm3016 = new ZMYR1016();
            if (frm3016.ShowDialog(this) == DialogResult.OK)
            {
                this.Config.ReloadConfig();
            }
            frm3016.Dispose();
        }

        /// <summary>
        /// ESCキー押下時処理
        /// </summary>
        public override void PressEsc()
        {
            base.PressEsc();
        }
        #endregion

        #region イベント

        /// <summary>
        /// 金額テキストボックスEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKingaku_Enter(object sender, System.EventArgs e)
        {
            FsiTextBox txtBox = (FsiTextBox)sender;

            txtBox.Text = Util.ToString(Util.ToDecimal(txtBox.Text));
            txtBox.SelectAll();
        }

        /// <summary>
        /// 金額項目Validating時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKingaku_Validating(object sender, CancelEventArgs e)
        {
            FsiTextBox txtBox = (FsiTextBox)sender;

            if (!IsValidKingaku(txtBox))
            {
                e.Cancel = true;
                txtBox.SelectAll();
            }
            else
            {
                txtBox.Text = Util.FormatNum(txtBox.Text);
            }
        }

        /// <summary>
        /// テキスト項目Validating時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtText_Validating(object sender, CancelEventArgs e)
        {
            FsiTextBox txtBox = (FsiTextBox)sender;

            if (!IsValidText(txtBox))
            {
                e.Cancel = true;
                txtBox.SelectAll();
            }
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// データを表示
        /// </summary>
        private void DispData()
        {
            DbParamCollection dpc;
            StringBuilder sql;
            DataTable dt;

            #region 利益処分科目分類名を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, 4);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            #region sql.Append(SQL文)
            sql.AppendLine("SELECT HYOJI_JUNI");
            sql.AppendLine(",KAMOKU_BUNRUI_NM");
            sql.AppendLine(" FROM TB_ZM_KESSANSHO_KAMOKU_BUNRUI");
            sql.AppendLine(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.AppendLine(" AND SHISHO_CD = @SHISHO_CD");
            sql.AppendLine(" AND CHOHYO_BUNRUI = @CHOHYO_BUNRUI");
            sql.AppendLine(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" AND SHIYO_KUBUN = 1");
            sql.AppendLine(" ORDER BY HYOJI_JUNI");
            #endregion
            dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            foreach (DataRow dr in dt.Rows)
            {
                Control[] ctrl;
                FsiTextBox txtBox;

                // 科目分類名をセット
                ctrl = this.Controls.Find("txtKamokuBunruiNm" + Util.ToString(dr["HYOJI_JUNI"]), true);
                if (ctrl.Length > 0)
                {
                    txtBox = (FsiTextBox)ctrl[0];
                    txtBox.Text = Util.ToString(dr["KAMOKU_BUNRUI_NM"]);
                }
            }
            #endregion

            #region 利益処分科目名を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, 4);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            #region sql.Append(SQL文)
            sql.AppendLine("SELECT KAMOKU_BUNRUI");
            sql.AppendLine(",GYO_BANGO");
            sql.AppendLine(",KANJO_KAMOKU_NM");
            sql.AppendLine(" FROM TB_ZM_KESSANSHO_KAMOKU_SETTEI");
            sql.AppendLine(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.AppendLine(" AND SHISHO_CD = @SHISHO_CD");
            sql.AppendLine(" AND CHOHYO_BUNRUI = @CHOHYO_BUNRUI");
            sql.AppendLine(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" ORDER BY GYO_BANGO");
            #endregion
            dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            foreach (DataRow dr in dt.Rows)
            {
                Control[] ctrl;
                FsiTextBox txtBox;

                // 科目名をセット
                ctrl = this.Controls.Find("txtKanjoKamokuNm" 
                    + Util.ToString(dr["KAMOKU_BUNRUI"]) 
                    + "_"
                    + Util.ToString(dr["GYO_BANGO"]), true);
                if (ctrl.Length > 0)
                {
                    txtBox = (FsiTextBox)ctrl[0];
                    txtBox.Text = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                }
            }

            #endregion

            #region 決算書注記情報より当期未処分剰余金、任意積立金取崩額内訳、利益金処分額内訳を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            #region sql文
            sql.AppendLine("SELECT KAISHA_CD");
            sql.AppendLine(",KAIKEI_NENDO");
            sql.AppendLine(",TOKI_MISHOBUN_RIEKIKIN");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU1");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU2");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU3");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU4");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU5");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU6");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU7");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU8");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU9");
            sql.AppendLine(",NINI_TMTTKN_TORIKUZUSHIGAKU10");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU1");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU2");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU3");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU4");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU5");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU6");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU7");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU8");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU9");
            sql.AppendLine(",RIEKIKIN_SHOBUNGAKU10");
            sql.AppendLine(" FROM TB_ZM_KESSANSHO_CHUKI");
            sql.AppendLine(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.AppendLine(" AND SHISHO_CD = @SHISHO_CD");
            sql.AppendLine(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            #endregion
            dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                this.txtKingaku1.Text = Util.FormatNum(dr["TOKI_MISHOBUN_RIEKIKIN"]);
                this.txtNiniTmttknTorikuzushigaku1.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU1"]);
                this.txtNiniTmttknTorikuzushigaku2.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU2"]);
                this.txtNiniTmttknTorikuzushigaku3.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU3"]);
                this.txtNiniTmttknTorikuzushigaku4.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU4"]);
                this.txtNiniTmttknTorikuzushigaku5.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU5"]);
                this.txtNiniTmttknTorikuzushigaku6.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU6"]);
                this.txtNiniTmttknTorikuzushigaku7.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU7"]);
                this.txtNiniTmttknTorikuzushigaku8.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU8"]);
                this.txtNiniTmttknTorikuzushigaku9.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU9"]);
                this.txtNiniTmttknTorikuzushigaku10.Text = Util.FormatNum(dr["NINI_TMTTKN_TORIKUZUSHIGAKU10"]);
                this.txtRiekikinShobungaku1.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU1"]);
                this.txtRiekikinShobungaku2.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU2"]);
                this.txtRiekikinShobungaku3.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU3"]);
                this.txtRiekikinShobungaku4.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU4"]);
                this.txtRiekikinShobungaku5.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU5"]);
                this.txtRiekikinShobungaku6.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU6"]);
                this.txtRiekikinShobungaku7.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU7"]);
                this.txtRiekikinShobungaku8.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU8"]);
                this.txtRiekikinShobungaku9.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU9"]);
                this.txtRiekikinShobungaku10.Text = Util.FormatNum(dr["RIEKIKIN_SHOBUNGAKU10"]);
            }

            decimal rieki = Util.ToDecimal(Util.ToString(this.txtKingaku1.Text));
            decimal soneki = GetSonekiTokiMishobunriekikin();
            if (rieki != soneki)
            {
                this.txtKingaku1.Text = Util.FormatNum(Util.ToString(soneki));
                this.txtKanjoKamokuNm90020_1.Focus();
            }
            else
            {
                // フォーカス設定
                this.txtKingaku1.Focus();
            }

            #endregion

            // 画面上の計算実行
            CalculationRun();
        }

        /// <summary>
        /// 画面上の計算実行
        /// </summary>
        private void CalculationRun()
        {
            // 任意積立金取崩額の小計
            this.txtKingaku2.Text = Util.FormatNum(Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku1.Text)
                                    + Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku2.Text)
                                    + Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku3.Text)
                                    + Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku4.Text)
                                    + Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku5.Text)
                                    + Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku6.Text)
                                    + Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku7.Text)
                                    + Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku8.Text)
                                    + Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku9.Text)
                                    + Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku10.Text));
            // 累計
            this.txtKingaku3.Text = Util.FormatNum(Util.ToDecimal(this.txtKingaku1.Text)
                                    + Util.ToDecimal(this.txtKingaku2.Text));
            // 利益処分額の小計
            this.txtKingaku4.Text = Util.FormatNum(Util.ToDecimal(this.txtRiekikinShobungaku1.Text)
                                    + Util.ToDecimal(this.txtRiekikinShobungaku2.Text)
                                    + Util.ToDecimal(this.txtRiekikinShobungaku3.Text)
                                    + Util.ToDecimal(this.txtRiekikinShobungaku4.Text)
                                    + Util.ToDecimal(this.txtRiekikinShobungaku5.Text)
                                    + Util.ToDecimal(this.txtRiekikinShobungaku6.Text)
                                    + Util.ToDecimal(this.txtRiekikinShobungaku7.Text)
                                    + Util.ToDecimal(this.txtRiekikinShobungaku8.Text)
                                    + Util.ToDecimal(this.txtRiekikinShobungaku9.Text)
                                    + Util.ToDecimal(this.txtRiekikinShobungaku10.Text));

            // 累計
            this.txtKingaku5.Text = Util.FormatNum(Util.ToDecimal(this.txtKingaku3.Text)
                                    - Util.ToDecimal(this.txtKingaku4.Text));
        }

        /// <summary>
        /// テキスト項目の入力チェック
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool IsValidText(FsiTextBox txt)
        {
            // 入力サイズが指定バイト数以上はNG
            if (!ValChk.IsWithinLength(txt.Text, txt.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                txt.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 金額項目の入力チェック
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool IsValidKingaku(FsiTextBox txt)
        {
            // 数値のみ入力可能
            if (!ValChk.IsNumber(txt.Text))
            {
                Msg.Error("入力に誤りがあります。");
                txt.Focus();
                return false;
            }

            // 入力サイズが指定バイト数以上はNG
            if (!ValChk.IsWithinLength(txt.Text, txt.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                txt.Focus();
                return false;
            }

            // 画面上の計算実行
            CalculationRun();

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            Control[] ctrl;
            FsiTextBox txtBox;

            for (int i = 1; i <= 10; i++)
            {
                // 貸借対照表注記コントロール
                ctrl = this.Controls.Find("txtTaishakuTaishohyoChuki" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    txtBox = (FsiTextBox)ctrl[0];
                    return IsValidText(txtBox);
                }

                // 損益計算書注記コントロール
                ctrl = this.Controls.Find("txtSonekiKeisanshoChuki" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    txtBox = (FsiTextBox)ctrl[0];
                    return IsValidText(txtBox);
                }

                // 製造原価計算書注記コントロール
                ctrl = this.Controls.Find("txtSeizoGenkaChuki" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    txtBox = (FsiTextBox)ctrl[0];
                    return IsValidText(txtBox);
                }
            }

            return true;
        }

        /// <summary>
        /// TB_ZM_KESSANSHO_KAMOKU_BUNRUIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 更新処理：DbParamCollection*2(WHERE句・SET句)
        /// </returns>
        private ArrayList SetZmKessanshoKamokuBunrui(int chohyoBunrui, int hyojiJuni, int kamokuBunrui, string kamokuBunruiNm)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection whereParam = new DbParamCollection();
            DbParamCollection setParam = new DbParamCollection();

            // WHERE句パラメータ
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            whereParam.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, chohyoBunrui);
            whereParam.SetParam("@HYOJI_JUNI", SqlDbType.Decimal, 4, hyojiJuni);
            whereParam.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 5, kamokuBunrui);
            whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            alParams.Add(whereParam);
            // SET句パラメータ
            setParam.SetParam("@KAMOKU_BUNRUI_NM", SqlDbType.VarChar, 64, kamokuBunruiNm);
            setParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(setParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_KESSANSHO_KAMOKU_SETTEIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmKessanshoKamokuSettei(int chohyoBunrui, int kamokuBunrui, int gyoBango, string kamokuBunruiNm)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection insParam = new DbParamCollection();

            // WHERE句パラメータ
            insParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            insParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            insParam.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, chohyoBunrui);
            insParam.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 5, kamokuBunrui);
            insParam.SetParam("@GYO_BANGO", SqlDbType.Decimal, 2, gyoBango);
            insParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, 0);
            insParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            insParam.SetParam("@KANJO_KAMOKU_NM", SqlDbType.VarChar, 64, kamokuBunruiNm);
            insParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, 0);
            insParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            insParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(insParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_KESSANSHO_CHUKIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 更新処理：DbParamCollection*2(WHERE句・SET句)
        /// </returns>
        private ArrayList SetZmKessanshoChukiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection whereParam = new DbParamCollection();
            DbParamCollection setParam = new DbParamCollection();

            // WHERE句パラメータ
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            alParams.Add(whereParam);
            // SET句パラメータ
            setParam.SetParam("@TOKI_MISHOBUN_RIEKIKIN", SqlDbType.Decimal, 15, 0,  Util.ToDecimal(this.txtKingaku1.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU1", SqlDbType.Decimal, 15, 0, 
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku1.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU2", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku2.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU3", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku3.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU4", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku4.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU5", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku5.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU6", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku6.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU7", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku7.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU8", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku8.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU9", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku9.Text));
            setParam.SetParam("@NINI_TMTTKN_TORIKUZUSHIGAKU10", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtNiniTmttknTorikuzushigaku10.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU1", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku1.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU2", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku2.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU3", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku3.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU4", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku4.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU5", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku5.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU6", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku6.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU7", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku7.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU8", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku8.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU9", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku9.Text));
            setParam.SetParam("@RIEKIKIN_SHOBUNGAKU10", SqlDbType.Decimal, 15, 0,
                Util.ToDecimal(this.txtRiekikinShobungaku10.Text));
            setParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(setParam);

            return alParams;
        }

        /// <summary>
        /// 当期未処分利益金の取得
        /// </summary>
        /// <returns></returns>
        private decimal GetSonekiTokiMishobunriekikin()
        {
            decimal returnValue = 0m;

            DataLoad();

            int chohyoBunrui = 2;
            DataRow[] drsBunrui;                                                                // 科目分類レコード保持用

            /// 科目分類レコードの取得
            string selectFilter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui) + " AND HYOJI_JUNI = " + this.hyoji_juni.ToString();
            string selectSort = "HYOJI_JUNI";
            drsBunrui = _dtKessanshoKamokuBunrui.Select(selectFilter, selectSort);
            foreach (DataRow drBunrui in drsBunrui)
            {
                // 集計計算式の実行(集計区分＝2の時は科目分類最終行の金額第2列へ表示)
                if (Util.ToInt(drBunrui["SHUKEI_KUBUN"]) == 3 && !ValChk.IsEmpty(drBunrui["SHUKEI_KEISANSHIKI"]))
                {
                    returnValue = GetJisKingaku_Keisanshiki(      // 小計
                        chohyoBunrui, Util.ToInt(drBunrui["TAISHAKU_KUBUN"]), Util.ToString(drBunrui["SHUKEI_KEISANSHIKI"]));
                    break;
                }
            }

            return returnValue;
        }

        #region ZMYR1011PRメソッド
        /// <summary>
        /// 参照データの読込
        /// </summary>
        private void DataLoad()
        {
            DbParamCollection dpc;
            StringBuilder sql;

            // 出力条件
            decimal bumonCdFr = Util.ToDecimal(this._pForm.Condition["BumonFr"]);               // 部門コード自
            decimal bumonCdTo = Util.ToDecimal(this._pForm.Condition["BumonTo"]);               // 部門コード至
            DateTime denpyoDateFr = Util.ToDate(this._pForm.Condition["DtFr"]);                 // 期間自
            DateTime denpyoDateTo = Util.ToDate(this._pForm.Condition["DtTo"]);                 // 期間至
            bool taxIncluded = (Util.ToInt(this._pForm.Condition["ShohizeiShoriHandan"]) == 1); // 税込フラグ
            // 支所コード
            int shishoCd = Util.ToInt(this._pForm.Condition["ShishoCode"]);

            #region TB_ZM_KESSANSHO_KAMOKU_BUNRUI
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT CHOHYO_BUNRUI");
            sql.Append("      ,HYOJI_JUNI");
            sql.Append("      ,KAMOKU_BUNRUI");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,KAMOKU_BUNRUI_NM");
            sql.Append("      ,TAISHAKU_KUBUN");
            sql.Append("      ,MEISAI_KOMOKUSU");
            sql.Append("      ,MEISAI_KUBUN");
            sql.Append("      ,SHUKEI_KUBUN");
            sql.Append("      ,SHUKEI_KEISANSHIKI");
            sql.Append("      ,BUNRUI_KUBUN");
            sql.Append("      ,KAKKO_KUBUN");
            sql.Append("      ,KAKKO_HYOJI");
            sql.Append("      ,MOJI_SHUBETSU");
            sql.Append("      ,SHIYO_KUBUN");
            sql.Append("  FROM TB_ZM_KESSANSHO_KAMOKU_BUNRUI");
            sql.Append("  WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append("    AND SHISHO_CD = 0");
            sql.Append("    AND KAIKEI_NENDO = @KAIKEI_NENDO");
            _dtKessanshoKamokuBunrui = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            #region TB_ZM_KESSANSHO_KAMOKU_SETTEI
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT CHOHYO_BUNRUI");
            sql.Append("      ,KAMOKU_BUNRUI");
            sql.Append("      ,GYO_BANGO");
            sql.Append("      ,KANJO_KAMOKU_CD");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,KANJO_KAMOKU_NM");
            sql.Append("      ,TAISHAKU_KUBUN");
            sql.Append("      ,REGIST_DATE");
            sql.Append("      ,UPDATE_DATE");
            sql.Append("  FROM TB_ZM_KESSANSHO_KAMOKU_SETTEI");
            sql.Append("  WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append("    AND SHISHO_CD = 0");
            sql.Append("    AND KAIKEI_NENDO = @KAIKEI_NENDO");
            _dtKessanshoKamokuSettei = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            #region TB_ZM_KESSANSHO_SETTEI
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT KAISHA_CD");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,TAISHAKU_TAISHOHYO_TITLE");
            sql.Append("      ,SONEKI_KEISANSHO_TITLE");
            sql.Append("      ,SEIZO_GENKA_TITLE");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_TITLE");
            sql.Append("      ,HANBAIHI_MEISAIHYO_TITLE");
            sql.Append("      ,RIEKI_SHOBUNAN_TITLE");
            sql.Append("      ,MINUS_KINGAKU");
            sql.Append("      ,GAICHU_KAKOHI_KUBUN");
            sql.Append("  FROM TB_ZM_KESSANSHO_SETTEI");
            sql.Append("  WHERE KAIKEI_NENDO <= @KAIKEI_NENDO");
            sql.Append("    AND KAISHA_CD = @KAISHA_CD");
            sql.Append("    AND SHISHO_CD = 0");
            sql.Append(" ORDER BY KAIKEI_NENDO DESC");

            _dtKessanshoSettei = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if (_dtKessanshoSettei.Rows.Count == 0)
            {
                DataRow drKessanshoSetteiDef = _dtKessanshoSettei.NewRow();
                drKessanshoSetteiDef["KAISHA_CD"] = this.UInfo.KaishaCd;
                drKessanshoSetteiDef["KAIKEI_NENDO"] = this.UInfo.KaikeiNendo;
                _dtKessanshoSettei.Rows.Add(drKessanshoSetteiDef);
            }
            #endregion

            #region TB_ZM_KESSANSHO_CHUKI
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT KAISHA_CD");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI1");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI2");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI3");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI4");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI5");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI6");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI7");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI8");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI9");
            sql.Append("      ,TAISHAKU_TAISHOHYO_CHUKI10");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI1");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI2");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI3");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI4");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI5");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI6");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI7");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI8");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI9");
            sql.Append("      ,SONEKI_KEISANSHO_CHUKI10");
            sql.Append("      ,SEIZO_GENKA_CHUKI1");
            sql.Append("      ,SEIZO_GENKA_CHUKI2");
            sql.Append("      ,SEIZO_GENKA_CHUKI3");
            sql.Append("      ,SEIZO_GENKA_CHUKI4");
            sql.Append("      ,SEIZO_GENKA_CHUKI5");
            sql.Append("      ,SEIZO_GENKA_CHUKI6");
            sql.Append("      ,SEIZO_GENKA_CHUKI7");
            sql.Append("      ,SEIZO_GENKA_CHUKI8");
            sql.Append("      ,SEIZO_GENKA_CHUKI9");
            sql.Append("      ,SEIZO_GENKA_CHUKI10");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI1");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI2");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI3");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI4");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI5");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI6");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI7");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI8");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CHUKI9");
            //sql.Append("      ,MISEI_KOJI_SHISHUTSUKIN_CK10");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI1");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI2");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI3");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI4");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI5");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI6");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI7");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI8");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI9");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI10");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI11");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI12");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI13");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI14");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI15");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI16");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI17");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI18");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI19");
            sql.Append("      ,RIEKI_SHOBUNAN_CHUKI20");
            sql.Append("      ,TOKI_MISHOBUN_RIEKIKIN");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU1");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU2");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU3");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU4");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU5");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU6");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU7");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU8");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU9");
            sql.Append("      ,NINI_TMTTKN_TORIKUZUSHIGAKU10");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU1");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU2");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU3");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU4");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU5");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU6");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU7");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU8");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU9");
            sql.Append("      ,RIEKIKIN_SHOBUNGAKU10");
            sql.Append("  FROM TB_ZM_KESSANSHO_CHUKI");
            sql.Append("  WHERE KAIKEI_NENDO <= @KAIKEI_NENDO");
            sql.Append("    AND KAISHA_CD = @KAISHA_CD");
            sql.Append("    AND SHISHO_CD = 0");
            sql.Append(" ORDER BY KAIKEI_NENDO DESC");

            try
            {
                _dtKessanshoChuki = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (_dtKessanshoChuki.Rows.Count == 0)
            {
                DataRow drKessanshoChukiDef = _dtKessanshoChuki.NewRow();
                drKessanshoChukiDef["KAISHA_CD"] = this.UInfo.KaishaCd;
                drKessanshoChukiDef["KAIKEI_NENDO"] = this.UInfo.KaikeiNendo;
                _dtKessanshoChuki.Rows.Add(drKessanshoChukiDef);
            }
            #endregion

            #region 決算書科目データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            sql = new StringBuilder();
            sql.Append("SELECT DISTINCT");
            sql.Append("      CHOHYO_BUNRUI");
            sql.Append("      ,KAMOKU_BUNRUI");
            sql.Append("      ,GYO_BANGO");
            sql.Append("      ,KAIKEI_NENDO");
            sql.Append("      ,KANJO_KAMOKU_NM");
            sql.Append("      ,TAISHAKU_KUBUN");
            sql.Append(" FROM TB_ZM_KESSANSHO_KAMOKU_SETTEI");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append("   AND SHISHO_CD = 0");
            sql.Append("   AND KAIKEI_NENDO = @KAIKEI_NENDO");
            _dtKessanshoKamoku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            #region 科目別実績データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, denpyoDateTo);
            sql = new StringBuilder();
            sql.Append("SELECT A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD");
            sql.Append(",MIN(B.TAISHAKU_KUBUN) AS TAISHAKU_KUBUN");
            sql.Append(",SUM(");
            sql.Append("  CASE");
            sql.Append("   WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.Append("   THEN A." + (taxIncluded ? "ZEIKOMI" : "ZEINUKI") + "_KINGAKU");
            sql.Append("   ELSE (A." + (taxIncluded ? "ZEIKOMI" : "ZEINUKI") + "_KINGAKU * -1) ");
            sql.Append("  END");
            sql.Append(" ) AS KINGAKU");
            sql.Append(",COUNT(*) AS KENSU");

            //sql.Append(",MIN(B.KAMOKU_BUNRUI_CD) AS KAMOKU_BUNRUI_CD");

            sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            sql.Append(" WHERE A.KAISHA_CD = @KAISHA_CD");

            if (shishoCd != 0)
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD");

            sql.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" AND A.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND A.DENPYO_DATE  <= @DENPYO_DATE");
            sql.Append(" AND A.MEISAI_KUBUN  <= " + (taxIncluded ? "0" : "1"));
            sql.Append(" AND A.KANJO_KAMOKU_CD IN ");
            sql.Append(" (SELECT X.KANJO_KAMOKU_CD FROM TB_ZM_KESSANSHO_KAMOKU_SETTEI AS X");
            sql.Append("  WHERE X.KAISHA_CD = @KAISHA_CD AND SHISHO_CD = 0)");
            sql.Append(" GROUP BY A.KANJO_KAMOKU_CD");
            _dtKamokuJis = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            #endregion
        }

        /// <summary>
        /// 実績金額を集計(集計計算式)
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類</param>
        /// <param name="taishakuKubun">貸借区分</param>
        /// <param name="shukeiKeisanshiki">集計計算式</param>
        /// <returns></returns>
        private decimal GetJisKingaku_Keisanshiki(int chohyoBunrui, int taishakuKubun, string shukeiKeisanshiki)
        {
            decimal ret = 0;

            // 引数の計算式から科目分類コードと加減情報を取得
            string shiki = shukeiKeisanshiki;
            if (shiki.Substring(0, 1) != "+" && shiki.Substring(0, 1) != "-")
            {
                shiki = "+" + shiki;                                    // 先頭科目分類コードへ加減文字を付加
            }

            // 科目分類値の要素数
            int itemCount = shiki.Length - Regex.Replace(shiki, "[+-]", "").Length;
            // 計算式を科目分類値と加減情報に分割
            int[] sign = new int[itemCount];
            int[] bunruiCd = new int[itemCount];
            for (int i = 0; i < itemCount; i++)
            {
                // 加減サインを配列へセット
                sign[i] = (shiki.Substring(0, 1) == "+") ? 1 : -1;
                shiki = shiki.Substring(1);                             // 計算式を加減サイン抽出後文字列へ編集

                // 科目分類コードを配列へセット
                Match m = Regex.Match(shiki, "[+-]");
                if (m.Index > 0)
                {
                    bunruiCd[i] = Util.ToInt(shiki.Substring(0, m.Index));
                }
                else
                {
                    bunruiCd[i] = Util.ToInt(shiki);
                }
                shiki = shiki.Substring(m.Index);                       // 計算式を科目分類コード抽出後文字列へ編集
            }

            // 重複科目の管理（設定で勘定科目が重複する設定時に金額が二重計上の為）
            List<string> KamokuList = new List<string>();

            // 集計演算を実行
            for (int i = 0; i < itemCount; i++)
            {
                // 決算書科目設定データから該当する科目分類の勘定科目コードを取得し、科目実績データを集計する。
                // (∵決算書科目分類データの計算式は勘定科目マスタの実科目分類ではなく、
                //  　決算書科目設定データにセットされている科目分類値を元に集計されている)

                // 決算書科目設定データから該当する科目分類の勘定科目を抽出
                ////string filter = "CHOHYO_BUNRUI = " + Util.ToString(chohyoBunrui)
                ////                + " AND KAMOKU_BUNRUI = " + Util.ToString(bunruiCd[i]);
                string filter = "KAMOKU_BUNRUI = " + Util.ToString(bunruiCd[i]);
                DataRow[] fRowsSti = _dtKessanshoKamokuSettei.Select(filter);
                foreach (DataRow dr in fRowsSti)
                {
                    if (!KamokuList.Contains(Util.ToString(dr["KANJO_KAMOKU_CD"])))
                    {
                        // 科目実績データから該当する勘定科目レコードを抽出
                        DataRow[] fRowsJis = _dtKamokuJis.Select("KANJO_KAMOKU_CD = " + Util.ToString(dr["KANJO_KAMOKU_CD"]));
                        if (fRowsJis.Length == 1)
                        {
                            if (chohyoBunrui == 1)      // BS集計
                            {
                                // 決算書科目分類データと勘定科目マスタ(科目実績データ)の貸借区分が異なる場合は符号反転する
                                if (taishakuKubun == Util.ToInt(fRowsJis[0]["TAISHAKU_KUBUN"]))
                                {
                                    ret = ret + Util.ToDecimal(fRowsJis[0]["KINGAKU"]) * Util.ToInt(sign[i]);
                                }
                                else
                                {
                                    ret = ret + Util.ToDecimal(fRowsJis[0]["KINGAKU"]) * Util.ToInt(sign[i]) * (-1);
                                }
                            }
                            else                        // PL集計
                            {
                                // 計算式のとおりに算出
                                ret = ret + Util.ToDecimal(fRowsJis[0]["KINGAKU"]) * Util.ToInt(sign[i]);
                            }
                        }
                        KamokuList.Add(Util.ToString(dr["KANJO_KAMOKU_CD"]));
                    }
                }
            }

            return ret;
        }
        #endregion

        #endregion

    }
}
