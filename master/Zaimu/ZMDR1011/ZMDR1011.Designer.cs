﻿namespace jp.co.fsi.zm.zmdr1011
{
    partial class ZMDR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.rdoZenShiwake = new System.Windows.Forms.RadioButton();
            this.rdoTujoShiwake = new System.Windows.Forms.RadioButton();
            this.rdoKessanShiwake = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "";
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.Location = new System.Drawing.Point(241, 9);
            this.txtMonthFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(39, 23);
            this.txtMonthFr.TabIndex = 3;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearFr.Location = new System.Drawing.Point(165, 9);
            this.txtYearFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(39, 23);
            this.txtYearFr.TabIndex = 1;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.Location = new System.Drawing.Point(317, 9);
            this.txtDayFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(39, 23);
            this.txtDayFr.TabIndex = 5;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblYearFr
            // 
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.Location = new System.Drawing.Point(207, 8);
            this.lblYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(27, 24);
            this.lblYearFr.TabIndex = 2;
            this.lblYearFr.Tag = "CHANGE";
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.Location = new System.Drawing.Point(283, 8);
            this.lblMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(27, 24);
            this.lblMonthFr.TabIndex = 4;
            this.lblMonthFr.Tag = "CHANGE";
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.Location = new System.Drawing.Point(359, 8);
            this.lblDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(27, 24);
            this.lblDayFr.TabIndex = 6;
            this.lblDayFr.Tag = "CHANGE";
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblGengoTo.Location = new System.Drawing.Point(433, 8);
            this.lblGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(55, 24);
            this.lblGengoTo.TabIndex = 8;
            this.lblGengoTo.Tag = "DISPNAME";
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(394, 8);
            this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(23, 24);
            this.lblCodeBetDate.TabIndex = 7;
            this.lblCodeBetDate.Tag = "CHANGE";
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblGengoFr.Location = new System.Drawing.Point(109, 8);
            this.lblGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(55, 24);
            this.lblGengoFr.TabIndex = 0;
            this.lblGengoFr.Tag = "DISPNAME";
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDayTo
            // 
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTo.Location = new System.Drawing.Point(679, 8);
            this.lblDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(27, 24);
            this.lblDayTo.TabIndex = 14;
            this.lblDayTo.Tag = "CHANGE";
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.Location = new System.Drawing.Point(603, 8);
            this.lblMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(27, 24);
            this.lblMonthTo.TabIndex = 12;
            this.lblMonthTo.Tag = "CHANGE";
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearTo
            // 
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.Location = new System.Drawing.Point(531, 8);
            this.lblYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(27, 24);
            this.lblYearTo.TabIndex = 10;
            this.lblYearTo.Tag = "CHANGE";
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTo.Location = new System.Drawing.Point(637, 9);
            this.txtDayTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(39, 23);
            this.txtDayTo.TabIndex = 13;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearTo.Location = new System.Drawing.Point(489, 9);
            this.txtYearTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(39, 23);
            this.txtYearTo.TabIndex = 9;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.Location = new System.Drawing.Point(561, 9);
            this.txtMonthTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(39, 23);
            this.txtMonthTo.TabIndex = 11;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.BackColor = System.Drawing.Color.Silver;
            this.rdoZeikomi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeikomi.Location = new System.Drawing.Point(111, 10);
            this.rdoZeikomi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(58, 20);
            this.rdoZeikomi.TabIndex = 0;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Tag = "CHANGE";
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = false;
            this.rdoZeikomi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoZeikomi_KeyDown);
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.BackColor = System.Drawing.Color.Silver;
            this.rdoZeinuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeinuki.Location = new System.Drawing.Point(177, 10);
            this.rdoZeinuki.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(58, 20);
            this.rdoZeinuki.TabIndex = 1;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Tag = "CHANGE";
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = false;
            this.rdoZeinuki.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoZeinuki_KeyDown);
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(109, 9);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(157, 8);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(360, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoZenShiwake
            // 
            this.rdoZenShiwake.AutoSize = true;
            this.rdoZenShiwake.BackColor = System.Drawing.Color.Silver;
            this.rdoZenShiwake.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZenShiwake.Location = new System.Drawing.Point(308, 11);
            this.rdoZenShiwake.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZenShiwake.Name = "rdoZenShiwake";
            this.rdoZenShiwake.Size = new System.Drawing.Size(74, 20);
            this.rdoZenShiwake.TabIndex = 2;
            this.rdoZenShiwake.TabStop = true;
            this.rdoZenShiwake.Tag = "CHANGE";
            this.rdoZenShiwake.Text = "全仕訳";
            this.rdoZenShiwake.UseVisualStyleBackColor = false;
            // 
            // rdoTujoShiwake
            // 
            this.rdoTujoShiwake.AutoSize = true;
            this.rdoTujoShiwake.BackColor = System.Drawing.Color.Silver;
            this.rdoTujoShiwake.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoTujoShiwake.Location = new System.Drawing.Point(111, 11);
            this.rdoTujoShiwake.Margin = new System.Windows.Forms.Padding(4);
            this.rdoTujoShiwake.Name = "rdoTujoShiwake";
            this.rdoTujoShiwake.Size = new System.Drawing.Size(90, 20);
            this.rdoTujoShiwake.TabIndex = 0;
            this.rdoTujoShiwake.TabStop = true;
            this.rdoTujoShiwake.Tag = "CHANGE";
            this.rdoTujoShiwake.Text = "通常仕訳";
            this.rdoTujoShiwake.UseVisualStyleBackColor = false;
            // 
            // rdoKessanShiwake
            // 
            this.rdoKessanShiwake.AutoSize = true;
            this.rdoKessanShiwake.BackColor = System.Drawing.Color.Silver;
            this.rdoKessanShiwake.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessanShiwake.Location = new System.Drawing.Point(210, 11);
            this.rdoKessanShiwake.Margin = new System.Windows.Forms.Padding(4);
            this.rdoKessanShiwake.Name = "rdoKessanShiwake";
            this.rdoKessanShiwake.Size = new System.Drawing.Size(90, 20);
            this.rdoKessanShiwake.TabIndex = 1;
            this.rdoKessanShiwake.TabStop = true;
            this.rdoKessanShiwake.Tag = "CHANGE";
            this.rdoKessanShiwake.Text = "決算仕訳";
            this.rdoKessanShiwake.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(709, 40);
            this.label1.TabIndex = 902;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "支所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(709, 40);
            this.label2.TabIndex = 902;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "日付";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(709, 40);
            this.label3.TabIndex = 902;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "仕訳種類";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(709, 41);
            this.label4.TabIndex = 902;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "消費税処理";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 4;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(719, 198);
            this.fsiTableLayoutPanel1.TabIndex = 903;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.rdoZeikomi);
            this.fsiPanel4.Controls.Add(this.rdoZeinuki);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 152);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(709, 41);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.rdoTujoShiwake);
            this.fsiPanel3.Controls.Add(this.rdoKessanShiwake);
            this.fsiPanel3.Controls.Add(this.rdoZenShiwake);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 103);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(709, 40);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblGengoFr);
            this.fsiPanel2.Controls.Add(this.txtYearFr);
            this.fsiPanel2.Controls.Add(this.txtDayFr);
            this.fsiPanel2.Controls.Add(this.lblGengoTo);
            this.fsiPanel2.Controls.Add(this.txtMonthFr);
            this.fsiPanel2.Controls.Add(this.lblYearFr);
            this.fsiPanel2.Controls.Add(this.lblMonthFr);
            this.fsiPanel2.Controls.Add(this.lblCodeBetDate);
            this.fsiPanel2.Controls.Add(this.lblDayFr);
            this.fsiPanel2.Controls.Add(this.txtMonthTo);
            this.fsiPanel2.Controls.Add(this.lblDayTo);
            this.fsiPanel2.Controls.Add(this.txtYearTo);
            this.fsiPanel2.Controls.Add(this.lblMonthTo);
            this.fsiPanel2.Controls.Add(this.txtDayTo);
            this.fsiPanel2.Controls.Add(this.lblYearTo);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 54);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(709, 40);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(709, 40);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // ZMDR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMDR1011";
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private System.Windows.Forms.Label lblYearFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblGengoFr;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.RadioButton rdoZenShiwake;
        private System.Windows.Forms.RadioButton rdoTujoShiwake;
        private System.Windows.Forms.RadioButton rdoKessanShiwake;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}