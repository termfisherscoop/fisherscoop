﻿using System.Data;
using System.Collections.Generic;

using jp.co.fsi.common.report;

namespace jp.co.fsi.zm.zmdr1011
{
    /// <summary>
    /// ZAMR1041R の概要の説明です。
    /// </summary>
    public partial class ZMDR10111R : BaseReport
    {
        public List<string> nmList;

        public ZMDR10111R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void pageHeader_Format(object sender, System.EventArgs e)
        {
            //this.label1.Text = "組合長";
            //this.label2.Text = "課長";
            //this.label3.Text = "係長";
            //this.label4.Text = "照査";
            //this.label5.Text = "起票";
            this.label1.Text = nmList[0];
            this.label2.Text = nmList[1];
            this.label3.Text = nmList[2];
            this.label4.Text = nmList[3];
            this.label5.Text = nmList[4];
        }
    }
}
