﻿namespace jp.co.fsi.zm.zmdr1011
{
    /// <summary>
    /// ZMDR10111R の概要の説明です。
    /// </summary>
    partial class ZMDR10111R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMDR10111R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ボックス49 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.直線24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.直線39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ボックス49,
            this.ラベル0,
            this.直線1,
            this.テキスト2,
            this.ラベル6,
            this.テキスト01,
            this.直線51,
            this.直線52,
            this.直線53,
            this.直線54,
            this.ラベル55,
            this.ラベル56,
            this.ラベル57,
            this.ラベル58,
            this.テキスト6,
            this.textBox1,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.shape1,
            this.ラベル50,
            this.textBox4});
            this.pageHeader.Height = 1.73067F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // ボックス49
            // 
            this.ボックス49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス49.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス49.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス49.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス49.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス49.Height = 0.26875F;
            this.ボックス49.Left = 0F;
            this.ボックス49.Name = "ボックス49";
            this.ボックス49.RoundingRadius = 9.999999F;
            this.ボックス49.Tag = "";
            this.ボックス49.Top = 1.447999F;
            this.ボックス49.Width = 7.043006F;
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.2291667F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 3.105906F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "日　計　表";
            this.ラベル0.Top = 0.9358268F;
            this.ラベル0.Width = 1.270833F;
            // 
            // 直線1
            // 
            this.直線1.Height = 0F;
            this.直線1.Left = 2.663878F;
            this.直線1.LineWeight = 1F;
            this.直線1.Name = "直線1";
            this.直線1.Tag = "";
            this.直線1.Top = 1.191158F;
            this.直線1.Width = 1.968749F;
            this.直線1.X1 = 2.663878F;
            this.直線1.X2 = 4.632627F;
            this.直線1.Y1 = 1.191158F;
            this.直線1.Y2 = 1.191158F;
            // 
            // テキスト2
            // 
            this.テキスト2.DataField = "ITEM11";
            this.テキスト2.Height = 0.15625F;
            this.テキスト2.Left = 5.441734F;
            this.テキスト2.Name = "テキスト2";
            this.テキスト2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト2.Tag = "";
            this.テキスト2.Text = "date";
            this.テキスト2.Top = 0.1716536F;
            this.テキスト2.Width = 1.097917F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.15625F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 6.879234F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "頁";
            this.ラベル6.Top = 0.1716536F;
            this.ラベル6.Width = 0.1770833F;
            // 
            // テキスト01
            // 
            this.テキスト01.DataField = "ITEM01";
            this.テキスト01.Height = 0.15625F;
            this.テキスト01.Left = 0F;
            this.テキスト01.Name = "テキスト01";
            this.テキスト01.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト01.Tag = "";
            this.テキスト01.Text = "ITEM01";
            this.テキスト01.Top = 1.239665F;
            this.テキスト01.Width = 1.96811F;
            // 
            // 直線51
            // 
            this.直線51.Height = 0.275694F;
            this.直線51.Left = 2.356299F;
            this.直線51.LineWeight = 1F;
            this.直線51.Name = "直線51";
            this.直線51.Tag = "";
            this.直線51.Top = 1.448032F;
            this.直線51.Width = 0F;
            this.直線51.X1 = 2.356299F;
            this.直線51.X2 = 2.356299F;
            this.直線51.Y1 = 1.448032F;
            this.直線51.Y2 = 1.723726F;
            // 
            // 直線52
            // 
            this.直線52.Height = 0.275694F;
            this.直線52.Left = 3.5437F;
            this.直線52.LineWeight = 0F;
            this.直線52.Name = "直線52";
            this.直線52.Tag = "";
            this.直線52.Top = 1.447999F;
            this.直線52.Width = 0F;
            this.直線52.X1 = 3.5437F;
            this.直線52.X2 = 3.5437F;
            this.直線52.Y1 = 1.447999F;
            this.直線52.Y2 = 1.723693F;
            // 
            // 直線53
            // 
            this.直線53.Height = 0.275694F;
            this.直線53.Left = 4.710367F;
            this.直線53.LineWeight = 0F;
            this.直線53.Name = "直線53";
            this.直線53.Tag = "";
            this.直線53.Top = 1.447999F;
            this.直線53.Width = 0F;
            this.直線53.X1 = 4.710367F;
            this.直線53.X2 = 4.710367F;
            this.直線53.Y1 = 1.447999F;
            this.直線53.Y2 = 1.723693F;
            // 
            // 直線54
            // 
            this.直線54.Height = 0.275694F;
            this.直線54.Left = 5.897867F;
            this.直線54.LineWeight = 0F;
            this.直線54.Name = "直線54";
            this.直線54.Tag = "";
            this.直線54.Top = 1.447999F;
            this.直線54.Width = 0F;
            this.直線54.X1 = 5.897867F;
            this.直線54.X2 = 5.897867F;
            this.直線54.Y1 = 1.447999F;
            this.直線54.Y2 = 1.723693F;
            // 
            // ラベル55
            // 
            this.ラベル55.Height = 0.2685039F;
            this.ラベル55.HyperLink = null;
            this.ラベル55.Left = 2.377165F;
            this.ラベル55.Name = "ラベル55";
            this.ラベル55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル55.Tag = "";
            this.ラベル55.Text = "前日残高";
            this.ラベル55.Top = 1.448032F;
            this.ラベル55.Width = 1.13937F;
            // 
            // ラベル56
            // 
            this.ラベル56.Height = 0.2685039F;
            this.ラベル56.HyperLink = null;
            this.ラベル56.Left = 3.549212F;
            this.ラベル56.Name = "ラベル56";
            this.ラベル56.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル56.Tag = "";
            this.ラベル56.Text = "借　　方";
            this.ラベル56.Top = 1.448032F;
            this.ラベル56.Width = 1.139764F;
            // 
            // ラベル57
            // 
            this.ラベル57.Height = 0.2685039F;
            this.ラベル57.HyperLink = null;
            this.ラベル57.Left = 4.731496F;
            this.ラベル57.Name = "ラベル57";
            this.ラベル57.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル57.Tag = "";
            this.ラベル57.Text = "貸　　方";
            this.ラベル57.Top = 1.448032F;
            this.ラベル57.Width = 1.13937F;
            // 
            // ラベル58
            // 
            this.ラベル58.Height = 0.2685039F;
            this.ラベル58.HyperLink = null;
            this.ラベル58.Left = 5.911811F;
            this.ラベル58.Name = "ラベル58";
            this.ラベル58.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル58.Tag = "";
            this.ラベル58.Text = "繰越残高";
            this.ラベル58.Top = 1.448032F;
            this.ラベル58.Width = 1.12126F;
            // 
            // テキスト6
            // 
            this.テキスト6.DataField = "ITEM04";
            this.テキスト6.Height = 0.1666667F;
            this.テキスト6.Left = 2.444434F;
            this.テキスト6.Name = "テキスト6";
            this.テキスト6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト6.Tag = "";
            this.テキスト6.Text = "ITEM04";
            this.テキスト6.Top = 1.229352F;
            this.テキスト6.Width = 2.570833F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.1562992F;
            this.textBox1.Left = 6.639569F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox1.Text = "page";
            this.textBox1.Top = 0.1715551F;
            this.textBox1.Width = 0.2396169F;
            // 
            // label1
            // 
            this.label1.Height = 0.472441F;
            this.label1.HyperLink = null;
            this.label1.Left = 3.119292F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; t" +
    "ext-justify: auto; vertical-align: middle; ddo-char-set: 128; ddo-font-vertical:" +
    " true";
            this.label1.Text = "組合長";
            this.label1.Top = 0.3437008F;
            this.label1.Width = 0.2232287F;
            // 
            // label2
            // 
            this.label2.Height = 0.472441F;
            this.label2.HyperLink = null;
            this.label2.Left = 3.895277F;
            this.label2.LineSpacing = 10F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 128; ddo-font-vertical: true";
            this.label2.Text = "課長";
            this.label2.Top = 0.3437008F;
            this.label2.Width = 0.2129921F;
            // 
            // label3
            // 
            this.label3.Height = 0.472441F;
            this.label3.HyperLink = null;
            this.label3.Left = 4.712206F;
            this.label3.LineSpacing = 10F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 128; ddo-font-vertical: true";
            this.label3.Text = "係長";
            this.label3.Top = 0.3437008F;
            this.label3.Width = 0.2129921F;
            // 
            // label4
            // 
            this.label4.Height = 0.472441F;
            this.label4.HyperLink = null;
            this.label4.Left = 5.509844F;
            this.label4.LineSpacing = 10F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 128; ddo-font-vertical: true";
            this.label4.Text = "照査";
            this.label4.Top = 0.3437008F;
            this.label4.Width = 0.2129921F;
            // 
            // label5
            // 
            this.label5.Height = 0.472441F;
            this.label5.HyperLink = null;
            this.label5.Left = 6.270474F;
            this.label5.LineSpacing = 10F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 128; ddo-font-vertical: true";
            this.label5.Text = "起票";
            this.label5.Top = 0.3437008F;
            this.label5.Width = 0.2129921F;
            // 
            // line1
            // 
            this.line1.Height = 0.4673228F;
            this.line1.Left = 3.337403F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.3484252F;
            this.line1.Width = 0F;
            this.line1.X1 = 3.337403F;
            this.line1.X2 = 3.337403F;
            this.line1.Y1 = 0.3484252F;
            this.line1.Y2 = 0.815748F;
            // 
            // line2
            // 
            this.line2.Height = 0.4673228F;
            this.line2.Left = 4.103151F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.3484252F;
            this.line2.Width = 0F;
            this.line2.X1 = 4.103151F;
            this.line2.X2 = 4.103151F;
            this.line2.Y1 = 0.3484252F;
            this.line2.Y2 = 0.815748F;
            // 
            // line3
            // 
            this.line3.Height = 0.4673228F;
            this.line3.Left = 4.92008F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.3484252F;
            this.line3.Width = 0F;
            this.line3.X1 = 4.92008F;
            this.line3.X2 = 4.92008F;
            this.line3.Y1 = 0.3484252F;
            this.line3.Y2 = 0.815748F;
            // 
            // line4
            // 
            this.line4.Height = 0.4677165F;
            this.line4.Left = 5.717718F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.3484252F;
            this.line4.Width = 0F;
            this.line4.X1 = 5.717718F;
            this.line4.X2 = 5.717718F;
            this.line4.Y1 = 0.3484252F;
            this.line4.Y2 = 0.8161417F;
            // 
            // line5
            // 
            this.line5.Height = 0.4673228F;
            this.line5.Left = 6.478347F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.3484252F;
            this.line5.Width = 0F;
            this.line5.X1 = 6.478347F;
            this.line5.X2 = 6.478347F;
            this.line5.Y1 = 0.3484252F;
            this.line5.Y2 = 0.815748F;
            // 
            // line6
            // 
            this.line6.Height = 0.4673228F;
            this.line6.Left = 3.895277F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.3484252F;
            this.line6.Width = 0F;
            this.line6.X1 = 3.895277F;
            this.line6.X2 = 3.895277F;
            this.line6.Y1 = 0.3484252F;
            this.line6.Y2 = 0.815748F;
            // 
            // line7
            // 
            this.line7.Height = 0.4673228F;
            this.line7.Left = 4.712206F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.3484252F;
            this.line7.Width = 0F;
            this.line7.X1 = 4.712206F;
            this.line7.X2 = 4.712206F;
            this.line7.Y1 = 0.3484252F;
            this.line7.Y2 = 0.815748F;
            // 
            // line8
            // 
            this.line8.Height = 0.4673228F;
            this.line8.Left = 5.509844F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.3484252F;
            this.line8.Width = 0F;
            this.line8.X1 = 5.509844F;
            this.line8.X2 = 5.509844F;
            this.line8.Y1 = 0.3484252F;
            this.line8.Y2 = 0.815748F;
            // 
            // line9
            // 
            this.line9.Height = 0.4673228F;
            this.line9.Left = 6.270474F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0.3484252F;
            this.line9.Width = 0F;
            this.line9.X1 = 6.270474F;
            this.line9.X2 = 6.270474F;
            this.line9.Y1 = 0.3484252F;
            this.line9.Y2 = 0.815748F;
            // 
            // shape1
            // 
            this.shape1.Height = 0.472441F;
            this.shape1.Left = 3.105906F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0.3484252F;
            this.shape1.Width = 3.927166F;
            // 
            // ラベル50
            // 
            this.ラベル50.Height = 0.2686955F;
            this.ラベル50.HyperLink = null;
            this.ラベル50.Left = 0F;
            this.ラベル50.Name = "ラベル50";
            this.ラベル50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル50.Tag = "";
            this.ラベル50.Text = "勘定科目名";
            this.ラベル50.Top = 1.448032F;
            this.ラベル50.Width = 2.335432F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM13";
            this.textBox4.Height = 0.1666667F;
            this.textBox4.Left = 5.229134F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM13";
            this.textBox4.Top = 1.239764F;
            this.textBox4.Width = 1.779101F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM12";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線24,
            this.直線25,
            this.直線26,
            this.直線27,
            this.直線28,
            this.直線29,
            this.直線30,
            this.テキスト05,
            this.テキスト06,
            this.テキスト07,
            this.テキスト08,
            this.テキスト09,
            this.テキスト10});
            this.detail.Height = 0.2756944F;
            this.detail.Name = "detail";
            // 
            // 直線24
            // 
            this.直線24.Height = 0.2756944F;
            this.直線24.Left = 0F;
            this.直線24.LineWeight = 1F;
            this.直線24.Name = "直線24";
            this.直線24.Tag = "";
            this.直線24.Top = 0F;
            this.直線24.Width = 0F;
            this.直線24.X1 = 0F;
            this.直線24.X2 = 0F;
            this.直線24.Y1 = 0F;
            this.直線24.Y2 = 0.2756944F;
            // 
            // 直線25
            // 
            this.直線25.Height = 0F;
            this.直線25.Left = 0F;
            this.直線25.LineWeight = 0F;
            this.直線25.Name = "直線25";
            this.直線25.Tag = "";
            this.直線25.Top = 0.2756944F;
            this.直線25.Width = 7.042913F;
            this.直線25.X1 = 0F;
            this.直線25.X2 = 7.042913F;
            this.直線25.Y1 = 0.2756944F;
            this.直線25.Y2 = 0.2756944F;
            // 
            // 直線26
            // 
            this.直線26.Height = 0.2756944F;
            this.直線26.Left = 2.354779F;
            this.直線26.LineWeight = 1F;
            this.直線26.Name = "直線26";
            this.直線26.Tag = "";
            this.直線26.Top = 0F;
            this.直線26.Width = 0F;
            this.直線26.X1 = 2.354779F;
            this.直線26.X2 = 2.354779F;
            this.直線26.Y1 = 0F;
            this.直線26.Y2 = 0.2756944F;
            // 
            // 直線27
            // 
            this.直線27.Height = 0.2756944F;
            this.直線27.Left = 3.54252F;
            this.直線27.LineWeight = 0F;
            this.直線27.Name = "直線27";
            this.直線27.Tag = "";
            this.直線27.Top = 0F;
            this.直線27.Width = 0F;
            this.直線27.X1 = 3.54252F;
            this.直線27.X2 = 3.54252F;
            this.直線27.Y1 = 0F;
            this.直線27.Y2 = 0.2756944F;
            // 
            // 直線28
            // 
            this.直線28.Height = 0.2756944F;
            this.直線28.Left = 4.708716F;
            this.直線28.LineWeight = 0F;
            this.直線28.Name = "直線28";
            this.直線28.Tag = "";
            this.直線28.Top = 0F;
            this.直線28.Width = 0F;
            this.直線28.X1 = 4.708716F;
            this.直線28.X2 = 4.708716F;
            this.直線28.Y1 = 0F;
            this.直線28.Y2 = 0.2756944F;
            // 
            // 直線29
            // 
            this.直線29.Height = 0.2756944F;
            this.直線29.Left = 5.89982F;
            this.直線29.LineWeight = 0F;
            this.直線29.Name = "直線29";
            this.直線29.Tag = "";
            this.直線29.Top = 0F;
            this.直線29.Width = 0F;
            this.直線29.X1 = 5.89982F;
            this.直線29.X2 = 5.89982F;
            this.直線29.Y1 = 0F;
            this.直線29.Y2 = 0.2756944F;
            // 
            // 直線30
            // 
            this.直線30.Height = 0.2756944F;
            this.直線30.Left = 7.042876F;
            this.直線30.LineWeight = 1F;
            this.直線30.Name = "直線30";
            this.直線30.Tag = "";
            this.直線30.Top = 0F;
            this.直線30.Width = 0F;
            this.直線30.X1 = 7.042876F;
            this.直線30.X2 = 7.042876F;
            this.直線30.Y1 = 0F;
            this.直線30.Y2 = 0.2756944F;
            // 
            // テキスト05
            // 
            this.テキスト05.DataField = "ITEM05";
            this.テキスト05.Height = 0.15625F;
            this.テキスト05.Left = 0.007874016F;
            this.テキスト05.Name = "テキスト05";
            this.テキスト05.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト05.Tag = "";
            this.テキスト05.Text = "ITEM05";
            this.テキスト05.Top = 0.0519685F;
            this.テキスト05.Width = 0.472441F;
            // 
            // テキスト06
            // 
            this.テキスト06.DataField = "ITEM06";
            this.テキスト06.Height = 0.15625F;
            this.テキスト06.Left = 0.3858268F;
            this.テキスト06.MultiLine = false;
            this.テキスト06.Name = "テキスト06";
            this.テキスト06.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト06.Tag = "";
            this.テキスト06.Text = "ITEM06";
            this.テキスト06.Top = 0.0519685F;
            this.テキスト06.Width = 1.811024F;
            // 
            // テキスト07
            // 
            this.テキスト07.DataField = "ITEM07";
            this.テキスト07.Height = 0.15625F;
            this.テキスト07.Left = 2.438013F;
            this.テキスト07.Name = "テキスト07";
            this.テキスト07.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト07.Tag = "";
            this.テキスト07.Text = "ITEM07";
            this.テキスト07.Top = 0.05208333F;
            this.テキスト07.Width = 1.077083F;
            // 
            // テキスト08
            // 
            this.テキスト08.DataField = "ITEM08";
            this.テキスト08.Height = 0.15625F;
            this.テキスト08.Left = 3.547736F;
            this.テキスト08.Name = "テキスト08";
            this.テキスト08.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト08.Tag = "";
            this.テキスト08.Text = "ITEM08";
            this.テキスト08.Top = 0.05208333F;
            this.テキスト08.Width = 1.13972F;
            // 
            // テキスト09
            // 
            this.テキスト09.DataField = "ITEM09";
            this.テキスト09.Height = 0.15625F;
            this.テキスト09.Left = 4.729976F;
            this.テキスト09.Name = "テキスト09";
            this.テキスト09.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト09.Tag = "";
            this.テキスト09.Text = "ITEM09";
            this.テキスト09.Top = 0.05208333F;
            this.テキスト09.Width = 1.14901F;
            // 
            // テキスト10
            // 
            this.テキスト10.DataField = "ITEM10";
            this.テキスト10.Height = 0.15625F;
            this.テキスト10.Left = 5.910237F;
            this.テキスト10.Name = "テキスト10";
            this.テキスト10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト10.Tag = "";
            this.テキスト10.Text = "ITEM10";
            this.テキスト10.Top = 0.05208333F;
            this.テキスト10.Width = 1.097917F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.label6,
            this.textBox2,
            this.textBox3,
            this.line15,
            this.line16});
            this.gfShishoCd.Height = 0.2828794F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // line10
            // 
            this.line10.Height = 0.2754921F;
            this.line10.Left = 2.354725F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Tag = "";
            this.line10.Top = 0.007185049F;
            this.line10.Width = 0F;
            this.line10.X1 = 2.354725F;
            this.line10.X2 = 2.354725F;
            this.line10.Y1 = 0.007185049F;
            this.line10.Y2 = 0.2826771F;
            // 
            // line11
            // 
            this.line11.Height = 0.2756944F;
            this.line11.Left = 3.542615F;
            this.line11.LineWeight = 0F;
            this.line11.Name = "line11";
            this.line11.Tag = "";
            this.line11.Top = 0.007185049F;
            this.line11.Width = 0F;
            this.line11.X1 = 3.542615F;
            this.line11.X2 = 3.542615F;
            this.line11.Y1 = 0.007185049F;
            this.line11.Y2 = 0.2828794F;
            // 
            // line12
            // 
            this.line12.Height = 0.2756944F;
            this.line12.Left = 4.708662F;
            this.line12.LineWeight = 0F;
            this.line12.Name = "line12";
            this.line12.Tag = "";
            this.line12.Top = 0.007185049F;
            this.line12.Width = 0F;
            this.line12.X1 = 4.708662F;
            this.line12.X2 = 4.708662F;
            this.line12.Y1 = 0.007185049F;
            this.line12.Y2 = 0.2828794F;
            // 
            // line13
            // 
            this.line13.Height = 0.2756944F;
            this.line13.Left = 5.900001F;
            this.line13.LineWeight = 0F;
            this.line13.Name = "line13";
            this.line13.Tag = "";
            this.line13.Top = 0.007185049F;
            this.line13.Width = 0F;
            this.line13.X1 = 5.900001F;
            this.line13.X2 = 5.900001F;
            this.line13.Y1 = 0.007185049F;
            this.line13.Y2 = 0.2828794F;
            // 
            // line14
            // 
            this.line14.Height = 0.2755904F;
            this.line14.Left = 7.042127F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Tag = "";
            this.line14.Top = 0.007185049F;
            this.line14.Width = 0F;
            this.line14.X1 = 7.042127F;
            this.line14.X2 = 7.042127F;
            this.line14.Y1 = 0.007185049F;
            this.line14.Y2 = 0.2827755F;
            // 
            // label6
            // 
            this.label6.Height = 0.15625F;
            this.label6.HyperLink = null;
            this.label6.Left = 0.396063F;
            this.label6.Name = "label6";
            this.label6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label6.Tag = "";
            this.label6.Text = "合　　      　　　　計";
            this.label6.Top = 0.06259842F;
            this.label6.Width = 1.751575F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM08";
            this.textBox2.Height = 0.15625F;
            this.textBox2.Left = 3.547639F;
            this.textBox2.Name = "textBox2";
            this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox2.SummaryGroup = "ghShishoCd";
            this.textBox2.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM08";
            this.textBox2.Top = 0.06968504F;
            this.textBox2.Width = 1.139583F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM09";
            this.textBox3.Height = 0.15625F;
            this.textBox3.Left = 4.729922F;
            this.textBox3.Name = "textBox3";
            this.textBox3.OutputFormat = resources.GetString("textBox3.OutputFormat");
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox3.SummaryGroup = "ghShishoCd";
            this.textBox3.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM09";
            this.textBox3.Top = 0.09055119F;
            this.textBox3.Width = 1.139424F;
            // 
            // line15
            // 
            this.line15.Height = 0.2756944F;
            this.line15.Left = 0F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Tag = "";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 0F;
            this.line15.X2 = 0F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.2756944F;
            // 
            // line16
            // 
            this.line16.Height = 0F;
            this.line16.Left = 0F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Tag = "";
            this.line16.Top = 0.2826771F;
            this.line16.Width = 7.042913F;
            this.line16.X1 = 0F;
            this.line16.X2 = 7.042913F;
            this.line16.Y1 = 0.2826771F;
            this.line16.Y2 = 0.2826771F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線39,
            this.直線40,
            this.直線41,
            this.直線42,
            this.直線43,
            this.直線44,
            this.直線45,
            this.ラベル46,
            this.テキスト47,
            this.テキスト48});
            this.reportFooter1.Height = 0.2756944F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Visible = false;
            // 
            // 直線39
            // 
            this.直線39.Height = 0.2756944F;
            this.直線39.Left = 0.0001368523F;
            this.直線39.LineWeight = 1F;
            this.直線39.Name = "直線39";
            this.直線39.Tag = "";
            this.直線39.Top = 0F;
            this.直線39.Width = 0F;
            this.直線39.X1 = 0.0001368523F;
            this.直線39.X2 = 0.0001368523F;
            this.直線39.Y1 = 0F;
            this.直線39.Y2 = 0.2756944F;
            // 
            // 直線40
            // 
            this.直線40.Height = 0.2756944F;
            this.直線40.Left = 2.36496F;
            this.直線40.LineWeight = 1F;
            this.直線40.Name = "直線40";
            this.直線40.Tag = "";
            this.直線40.Top = 0F;
            this.直線40.Width = 0F;
            this.直線40.X1 = 2.36496F;
            this.直線40.X2 = 2.36496F;
            this.直線40.Y1 = 0F;
            this.直線40.Y2 = 0.2756944F;
            // 
            // 直線41
            // 
            this.直線41.Height = 0.2756944F;
            this.直線41.Left = 3.547637F;
            this.直線41.LineWeight = 0F;
            this.直線41.Name = "直線41";
            this.直線41.Tag = "";
            this.直線41.Top = 0F;
            this.直線41.Width = 0F;
            this.直線41.X1 = 3.547637F;
            this.直線41.X2 = 3.547637F;
            this.直線41.Y1 = 0F;
            this.直線41.Y2 = 0.2756944F;
            // 
            // 直線42
            // 
            this.直線42.Height = 0.2756944F;
            this.直線42.Left = 4.718898F;
            this.直線42.LineWeight = 0F;
            this.直線42.Name = "直線42";
            this.直線42.Tag = "";
            this.直線42.Top = 0F;
            this.直線42.Width = 0F;
            this.直線42.X1 = 4.718898F;
            this.直線42.X2 = 4.718898F;
            this.直線42.Y1 = 0F;
            this.直線42.Y2 = 0.2756944F;
            // 
            // 直線43
            // 
            this.直線43.Height = 0.2756944F;
            this.直線43.Left = 5.910138F;
            this.直線43.LineWeight = 0F;
            this.直線43.Name = "直線43";
            this.直線43.Tag = "";
            this.直線43.Top = 0F;
            this.直線43.Width = 0F;
            this.直線43.X1 = 5.910138F;
            this.直線43.X2 = 5.910138F;
            this.直線43.Y1 = 0F;
            this.直線43.Y2 = 0.2756944F;
            // 
            // 直線44
            // 
            this.直線44.Height = 0.2755905F;
            this.直線44.Left = 7.052499F;
            this.直線44.LineWeight = 1F;
            this.直線44.Name = "直線44";
            this.直線44.Tag = "";
            this.直線44.Top = 0F;
            this.直線44.Width = 0F;
            this.直線44.X1 = 7.052499F;
            this.直線44.X2 = 7.052499F;
            this.直線44.Y1 = 0F;
            this.直線44.Y2 = 0.2755905F;
            // 
            // 直線45
            // 
            this.直線45.Height = 0F;
            this.直線45.Left = 0.0001368523F;
            this.直線45.LineWeight = 1F;
            this.直線45.Name = "直線45";
            this.直線45.Tag = "";
            this.直線45.Top = 0.2756944F;
            this.直線45.Width = 6.249863F;
            this.直線45.X1 = 0.0001368523F;
            this.直線45.X2 = 6.25F;
            this.直線45.Y1 = 0.2756944F;
            this.直線45.Y2 = 0.2756944F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.15625F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 0.3959702F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "合　　      　　　　計";
            this.ラベル46.Top = 0.0625F;
            this.ラベル46.Width = 1.57214F;
            // 
            // テキスト47
            // 
            this.テキスト47.DataField = "ITEM08";
            this.テキスト47.Height = 0.15625F;
            this.テキスト47.Left = 3.558053F;
            this.テキスト47.Name = "テキスト47";
            this.テキスト47.OutputFormat = resources.GetString("テキスト47.OutputFormat");
            this.テキスト47.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト47.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト47.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト47.Tag = "";
            this.テキスト47.Text = "ITEM08";
            this.テキスト47.Top = 0.0625F;
            this.テキスト47.Width = 1.139583F;
            // 
            // テキスト48
            // 
            this.テキスト48.DataField = "ITEM09";
            this.テキスト48.Height = 0.15625F;
            this.テキスト48.Left = 4.740158F;
            this.テキスト48.Name = "テキスト48";
            this.テキスト48.OutputFormat = resources.GetString("テキスト48.OutputFormat");
            this.テキスト48.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト48.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト48.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト48.Tag = "";
            this.テキスト48.Text = "ITEM09";
            this.テキスト48.Top = 0.08333334F;
            this.テキスト48.Width = 1.139424F;
            // 
            // ZMDR10111R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.7011811F;
            this.PageSettings.Margins.Right = 0.4011811F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.170079F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト01;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス49;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル50;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線51;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線52;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線53;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線54;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル55;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル56;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル57;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト6;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線24;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線25;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線26;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線27;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線28;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線29;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト10;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線39;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線40;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線41;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線42;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線43;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線44;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
    }
}
