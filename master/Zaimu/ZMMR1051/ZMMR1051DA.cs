﻿using System;
using System.Collections;
using System.Data;
using System.Text;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1051
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1051DA
    {
        #region private変数
        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// ワークテーブル名
        /// </summary>
        private string WK_TABLE;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMMR1051DA(UserInfo uInfo, DbAccess dba, ConfigLoader config)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
        }
        #endregion

        #region publicメソッド

        public DataTable CreateData(Hashtable condition, string unuqId)
        {
            DataTable dt = null;
            // 支所コード
            int shishoCd = Util.ToInt(condition["ShishoCode"]);

            WK_TABLE = "TB_" + unuqId.Replace("-", "");

            // 仕訳データより集計データを中間ワークへ作成
            SetKamokuKingakuWorkTable(shishoCd, condition);
            // 印刷用データの作成
            dt = GetDataList(shishoCd, condition);

            return dt;
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// 指定した科目分類の金額をワークテーブルへ集計抽出
        /// </summary>
        /// <param name="shishoCd">支所コード</param>
        /// <param name="condition">条件テーブル</param>
        /// <returns>結果</returns>
        private bool SetKamokuKingakuWorkTable(int shishoCd, Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shiwakeSyori = Util.ToInt(condition["ShiwakeShurui"]);
            int bumonCdFr = Util.ToInt(condition["BumonFr"]);
            int bumonCdTo = Util.ToInt(condition["BumonTo"]);
            int zeiHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
            DateTime dtFr = (DateTime)condition["DtFr"];
            DateTime dtTo = (DateTime)condition["DtTo"];

            #region ワークテーブル抽出SQL
            try
            {
                // 無条件に削除
                this._dba.ModifyBySql("DROP TABLE " + WK_TABLE, dpc);
            }
            catch (Exception) { }


            string colName = "ZEIKOMI_KINGAKU";
            if (zeiHandan == 1)
            {
                colName = "ZEIKOMI_KINGAKU";
            }
            else
            {
                colName = "ZEINUKI_KINGAKU";
            }

            sql = new StringBuilder();
            sql.AppendLine(" SELECT");

            if (shishoCd != 0)
                sql.AppendLine(" A.SHISHO_CD,");
            else
                sql.AppendLine(" 0 AS SHISHO_CD,");

            sql.AppendLine(" A.KANJO_KAMOKU_CD,");
            sql.AppendLine(" CASE WHEN A.BUMON_CD <> 0 THEN A.BUMON_CD ");
            sql.AppendLine("      ELSE ISNULL(C.BUMON_CD, 0) END BUMON_CD,");
            sql.AppendLine(" MAX(B.TAISHAKU_KUBUN) AS TAISHAKU_KUBUN,");

            sql.AppendLine(" SUM(CASE WHEN A.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME) THEN");
            sql.AppendLine("               CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN A." + colName + " ELSE A." + colName + " * -1 END ");
            sql.AppendLine("                    ELSE 0 ");
            sql.AppendLine("     END) AS HASSEI_KINGAKU,");

            sql.AppendLine(" SUM(CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN A." + colName + "");
            sql.AppendLine("          ELSE A." + colName + " * -1");
            sql.AppendLine("     END) AS RUIKEI_KINGAKU ");

            sql.AppendLine(" INTO " + WK_TABLE + " ");

            sql.AppendLine(" FROM");
            sql.AppendLine(" TB_ZM_SHIWAKE_MEISAI AS A");
            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.AppendLine(" ON A.KAISHA_CD = B.KAISHA_CD AND");
            sql.AppendLine(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            sql.AppendLine(" A.KAIKEI_NENDO = B.KAIKEI_NENDO");

            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_BMN_SNK_KMK_STI AS C");
            sql.AppendLine(" ON A.KAISHA_CD = C.KAISHA_CD AND");
            sql.AppendLine(" A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD AND");
            sql.AppendLine(" A.KAIKEI_NENDO = C.KAIKEI_NENDO");

            sql.AppendLine(" WHERE");
            sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" A.DENPYO_DATE <= CAST(@KIKAN_TO AS DATETIME) AND");
            if (zeiHandan == 1)
            {
                sql.AppendLine(" A.MEISAI_KUBUN <= 0 AND");
            }
            else
            {
                sql.AppendLine(" A.MEISAI_KUBUN >= 0 AND ");
            }

            if (shishoCd != 0)
                sql.AppendLine(" A.SHISHO_CD = @SHISHO_CD AND");

            sql.AppendLine(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            if (shiwakeSyori != 9)
            {
                sql.AppendLine(" AND A.KESSAN_KUBUN = @KESSAN_KUBUN");
            }

            sql.AppendLine(" AND A.KANJO_KAMOKU_CD IN(SELECT S.KANJO_KAMOKU_CD FROM TB_ZM_BMN_SNK_YUYK_STI AS S ");
            sql.AppendLine("                          WHERE S.KAISHA_CD = @KAISHA_CD AND S.KAIKEI_NENDO = @KAIKEI_NENDO)");

            sql.AppendLine(" AND ( ");
            sql.AppendLine(" (A.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO) OR ");
            sql.AppendLine(" (C.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO) ");
            sql.AppendLine(" ) ");

            sql.AppendLine(" GROUP BY");

            if (shishoCd != 0)
                sql.AppendLine(" A.SHISHO_CD,");

            sql.AppendLine(" A.KANJO_KAMOKU_CD,");
            sql.AppendLine(" CASE WHEN A.BUMON_CD <> 0 THEN A.BUMON_CD ");
            sql.AppendLine("      ELSE ISNULL(C.BUMON_CD, 0) END");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, shiwakeSyori);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
            dpc.SetParam("@KIKAN_FR", SqlDbType.VarChar, dtFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KIKAN_TO", SqlDbType.VarChar, dtTo.Date.ToString("yyyy/MM/dd"));
            #endregion

            try
            {
                int cnt = this._dba.ModifyBySql(sql.ToString(), dpc);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 印字対象用のDataTable作成処理
        /// </summary>
        /// <param name="shishoCd">支所コード</param>
        /// <param name="condition">条件テーブル</param>
        /// <returns>印字用科目、部門毎の集計DataTable</returns>
        private DataTable GetDataList(int shishoCd, Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int bumonCdFr = Util.ToInt(condition["BumonFr"]);
            int bumonCdTo = Util.ToInt(condition["BumonTo"]);

            // 総合計用の部門コード
            const int BUMON_TOTAL_CD = 99999;

            #region 部門毎科目分類毎の印字ベース情報の抽出
            sql.AppendLine(" SELECT");
            sql.AppendLine(" @SHISHO_CD AS SHISHO_CD,");
            sql.AppendLine(" A.BUMON_CD,");
            sql.AppendLine(" A.BUMON_NM,");
            sql.AppendLine(" B.HYOJI_JUNI,");
            sql.AppendLine(" B.KAMOKU_BUNRUI,");
            sql.AppendLine(" B.KAMOKU_BUNRUI_NM,");
            sql.AppendLine(" B.TAISHAKU_KUBUN,");
            sql.AppendLine(" B.MEISAI_UMU,");
            sql.AppendLine(" B.KAMOKU_KUBUN,");
            sql.AppendLine(" B.KEISAN_SHIKI,");
            sql.AppendLine(" C.YOUYAKU_KAMOKU,");
            sql.AppendLine(" C.YOUYAKU_NM,");
            sql.AppendLine(" 0 AS HASSEI_KINGAKU,");
            sql.AppendLine(" 0 AS RUIKEI_KINGAKU,");
            sql.AppendLine(" '' AS NAME,");
            sql.AppendLine(" 0 AS RUIKEI_KINGAKU_KEI,");
            sql.AppendLine(" 0 AS BUMON_KINGAKU_KEI,");
            sql.AppendLine(" 0.0 AS KOUSEI_HI,");
            sql.AppendLine(" 0.0 AS BUMON_HI");

            sql.AppendLine(" FROM");
            sql.AppendLine(" TB_CM_BUMON AS A");

            sql.AppendLine(" LEFT OUTER JOIN");
            sql.AppendLine("    TB_ZM_BMN_SNK_KMK_BNRI AS B");
            sql.AppendLine("  ON A.KAISHA_CD = B.KAISHA_CD ");

            sql.AppendLine(" LEFT OUTER JOIN");
            sql.AppendLine("    (SELECT DISTINCT ");
            sql.AppendLine("      KAISHA_CD,");
            sql.AppendLine("      KAIKEI_NENDO,");
            sql.AppendLine("      KAMOKU_BUNRUI,");
            sql.AppendLine("      YOUYAKU_KAMOKU,");
            sql.AppendLine("      YOUYAKU_NM,");
            sql.AppendLine("      TAISHAKU_KUBUN");
            sql.AppendLine("     FROM TB_ZM_BMN_SNK_YUYK_STI");
            sql.AppendLine("     WHERE KAISHA_CD = @KAISHA_CD ");
            sql.AppendLine("       AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.AppendLine("    ) AS C");
            sql.AppendLine("  ON B.KAISHA_CD = C.KAISHA_CD ");
            sql.AppendLine(" AND B.KAIKEI_NENDO = C.KAIKEI_NENDO");
            sql.AppendLine(" AND B.KAMOKU_BUNRUI = C.KAMOKU_BUNRUI");

            sql.AppendLine(" WHERE");
            sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" A.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO AND ");
            sql.AppendLine(" B.KAIKEI_NENDO = @KAIKEI_NENDO");

            sql.AppendLine(" ORDER BY ");
            sql.AppendLine(" A.BUMON_CD,");
            sql.AppendLine(" B.HYOJI_JUNI,");
            sql.AppendLine(" B.KAMOKU_BUNRUI,");
            sql.AppendLine(" C.YOUYAKU_KAMOKU");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);

            DataTable dt = null;
            dt = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            //　返却用のDataTable
            DataTable dtBmnData = new DataTable();
            dtBmnData.Columns.Add("SHISHO_CD", typeof(decimal));
            dtBmnData.Columns.Add("BUMON_CD", typeof(decimal));
            dtBmnData.Columns.Add("BUMON_NM", typeof(string));
            dtBmnData.Columns.Add("HYOJI_JUNI", typeof(decimal)); //
            dtBmnData.Columns.Add("KAMOKU_BUNRUI", typeof(decimal));
            dtBmnData.Columns.Add("KAMOKU_BUNRUI_NM", typeof(string));
            dtBmnData.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
            dtBmnData.Columns.Add("MEISAI_UMU", typeof(decimal));
            dtBmnData.Columns.Add("KAMOKU_KUBUN", typeof(decimal));
            dtBmnData.Columns.Add("KEISAN_SHIKI", typeof(string));
            dtBmnData.Columns.Add("YOUYAKU_KAMOKU", typeof(decimal)); //
            dtBmnData.Columns.Add("YOUYAKU_NM", typeof(string));
            dtBmnData.Columns.Add("HASSEI_KINGAKU", typeof(decimal)); //
            dtBmnData.Columns.Add("RUIKEI_KINGAKU", typeof(decimal));
            // 作成の中で設定用の項目
            dtBmnData.Columns.Add("NAME", typeof(string));
            dtBmnData.Columns.Add("RUIKEI_KINGAKU_KEI", typeof(decimal));
            dtBmnData.Columns.Add("BUMON_KINGAKU_KEI", typeof(decimal));
            dtBmnData.Columns.Add("KOUSEI_HI", typeof(decimal));
            dtBmnData.Columns.Add("BUMON_HI", typeof(decimal));
            #endregion

            decimal kamoku_bunrui = 0;
            int kamoku_bunrui_cnt = 0;
            decimal kamoku_kubun = 0;
            string shukeiKeisanShiki;

            DataRow dr;
            DataRow drBak = null;

            #region 1.部門毎の一覧テーブルの作成

            int i = 0;
            while (dt.Rows.Count > i)
            {
                if (i == 0)
                {
                    kamoku_bunrui = Util.ToDecimal(Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI"]));
                    drBak = dtBmnData.NewRow();
                }

                if (kamoku_bunrui != Util.ToDecimal(Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI"])))
                {
                    // 変わった場合に合計を作成（分類小計行）
                    if (kamoku_bunrui_cnt > 0)
                    {
                        dr = dtBmnData.NewRow();
                        dr["SHISHO_CD"] = Util.ToDecimal(Util.ToString(drBak["SHISHO_CD"]));
                        dr["BUMON_CD"] = Util.ToDecimal(Util.ToString(drBak["BUMON_CD"]));
                        dr["BUMON_NM"] = Util.ToString(drBak["BUMON_NM"]);
                        dr["HYOJI_JUNI"] = Util.ToDecimal(Util.ToString(drBak["HYOJI_JUNI"]));
                        dr["KAMOKU_BUNRUI"] = Util.ToDecimal(Util.ToString(drBak["KAMOKU_BUNRUI"]));
                        dr["KAMOKU_BUNRUI_NM"] = Util.ToString(drBak["KAMOKU_BUNRUI_NM"]);
                        dr["TAISHAKU_KUBUN"] = Util.ToDecimal(Util.ToString(drBak["TAISHAKU_KUBUN"]));
                        dr["MEISAI_UMU"] = Util.ToDecimal(Util.ToString(drBak["MEISAI_UMU"]));
                        dr["KAMOKU_KUBUN"] = 0;
                        dr["KEISAN_SHIKI"] = Util.ToString(drBak["KEISAN_SHIKI"]);
                        dr["YOUYAKU_KAMOKU"] = 0;
                        dr["YOUYAKU_NM"] = Util.ToString(drBak["YOUYAKU_NM"]);
                        dr["HASSEI_KINGAKU"] = 0;
                        dr["RUIKEI_KINGAKU"] = 0;
                        dr["NAME"] = Util.ToString(drBak["KAMOKU_BUNRUI_NM"]);
                        dtBmnData.Rows.Add(dr);
                    }

                    kamoku_bunrui = Util.ToDecimal(Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI"]));
                    kamoku_bunrui_cnt = 0;
                }

                kamoku_kubun = Util.ToDecimal(Util.ToString(dt.Rows[i]["KAMOKU_KUBUN"]));

                // 要約明細行
                dr = dtBmnData.NewRow();
                dr["SHISHO_CD"] = Util.ToDecimal(Util.ToString(dt.Rows[i]["SHISHO_CD"]));
                dr["BUMON_CD"] = Util.ToDecimal(Util.ToString(dt.Rows[i]["BUMON_CD"]));
                dr["BUMON_NM"] = Util.ToString(dt.Rows[i]["BUMON_NM"]);
                dr["HYOJI_JUNI"] = Util.ToDecimal(Util.ToString(dt.Rows[i]["HYOJI_JUNI"]));
                dr["KAMOKU_BUNRUI"] = Util.ToDecimal(Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI"]));
                dr["KAMOKU_BUNRUI_NM"] = Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI_NM"]);
                dr["TAISHAKU_KUBUN"] = Util.ToDecimal(Util.ToString(dt.Rows[i]["TAISHAKU_KUBUN"]));
                dr["MEISAI_UMU"] = Util.ToDecimal(Util.ToString(dt.Rows[i]["MEISAI_UMU"]));
                dr["KAMOKU_KUBUN"] = Util.ToDecimal(Util.ToString(dt.Rows[i]["KAMOKU_KUBUN"]));
                dr["KEISAN_SHIKI"] = Util.ToString(dt.Rows[i]["KEISAN_SHIKI"]);
                dr["YOUYAKU_KAMOKU"] = Util.ToDecimal(Util.ToString(dt.Rows[i]["YOUYAKU_KAMOKU"]));
                dr["YOUYAKU_NM"] = Util.ToString(dt.Rows[i]["YOUYAKU_NM"]);

                // 金額の取得（先に抽出しているワークテーブルより取得）
                if (kamoku_kubun == 1)
                {
                    decimal[] Kingk = GetKingk(Util.ToInt(Util.ToString(dt.Rows[i]["BUMON_CD"])), Util.ToDecimal(Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI"])), Util.ToDecimal(Util.ToString(dt.Rows[i]["YOUYAKU_KAMOKU"])));
                    dr["HASSEI_KINGAKU"] = Kingk[0];
                    dr["RUIKEI_KINGAKU"] = Kingk[1];
                    if (Util.ToString(dt.Rows[i]["YOUYAKU_NM"]).Trim().Length != 0)
                    {
                        dr["NAME"] = Util.ToString(dt.Rows[i]["YOUYAKU_NM"]);
                        kamoku_bunrui_cnt++;
                    }
                    else
                    {
                        dr["NAME"] = Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI_NM"]);
                    }
                }
                else
                {
                    dr["HASSEI_KINGAKU"] = 0;
                    dr["RUIKEI_KINGAKU"] = 0;
                    dr["NAME"] = Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI_NM"]);
                    kamoku_bunrui_cnt = 0;
                }

                // 行の追加
                dtBmnData.Rows.Add(dr);

                // 直近行の退避
                drBak = dtBmnData.NewRow();
                drBak.ItemArray = dr.ItemArray;

                i++;
            }

            // 最終小計行
            if (kamoku_bunrui_cnt > 0)
            {
                dr = dtBmnData.NewRow();
                dr["SHISHO_CD"] = Util.ToDecimal(Util.ToString(drBak["SHISHO_CD"]));
                dr["BUMON_CD"] = Util.ToDecimal(Util.ToString(drBak["BUMON_CD"]));
                dr["BUMON_NM"] = Util.ToString(drBak["BUMON_NM"]);
                dr["HYOJI_JUNI"] = Util.ToDecimal(Util.ToString(drBak["HYOJI_JUNI"]));
                dr["KAMOKU_BUNRUI"] = Util.ToDecimal(Util.ToString(drBak["KAMOKU_BUNRUI"]));
                dr["KAMOKU_BUNRUI_NM"] = Util.ToString(drBak["KAMOKU_BUNRUI_NM"]);
                dr["TAISHAKU_KUBUN"] = Util.ToDecimal(Util.ToString(drBak["TAISHAKU_KUBUN"]));
                dr["MEISAI_UMU"] = Util.ToDecimal(Util.ToString(drBak["MEISAI_UMU"]));
                dr["KAMOKU_KUBUN"] = 0;
                dr["KEISAN_SHIKI"] = Util.ToString(drBak["KEISAN_SHIKI"]);
                dr["YOUYAKU_KAMOKU"] = 0;
                dr["YOUYAKU_NM"] = Util.ToString(drBak["YOUYAKU_NM"]);
                dr["HASSEI_KINGAKU"] = 0;
                dr["RUIKEI_KINGAKU"] = 0;
                dr["NAME"] = Util.ToString(drBak["KAMOKU_BUNRUI_NM"]);
                dtBmnData.Rows.Add(dr);
            }
            #endregion

            #region 2.分類小計の設定及び式の計算結果設定

            // 1で作成した結果をループし、金額類の算出結果を次のテーブルへ
            DataTable dtBumonData = dtBmnData.Clone();
            i = 0;
            // 構成比用の科目分類
            decimal kamoku_bunrui_uriagedaka = Util.ToDecimal(condition["KAMOKU_BUNRUI_URIAGEDAKA"]);

            while (dtBmnData.Rows.Count > i)
            {
                kamoku_bunrui = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["KAMOKU_BUNRUI"]));
                kamoku_kubun = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["KAMOKU_KUBUN"]));

                dr = dtBumonData.NewRow();
                dr["SHISHO_CD"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["SHISHO_CD"]));
                dr["BUMON_CD"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["BUMON_CD"]));
                dr["BUMON_NM"] = Util.ToString(dtBmnData.Rows[i]["BUMON_NM"]);
                dr["HYOJI_JUNI"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["HYOJI_JUNI"]));
                dr["KAMOKU_BUNRUI"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["KAMOKU_BUNRUI"]));
                dr["KAMOKU_BUNRUI_NM"] = Util.ToString(dtBmnData.Rows[i]["KAMOKU_BUNRUI_NM"]);
                dr["TAISHAKU_KUBUN"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["TAISHAKU_KUBUN"]));
                dr["MEISAI_UMU"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["MEISAI_UMU"]));
                dr["KAMOKU_KUBUN"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["KAMOKU_KUBUN"]));
                dr["KEISAN_SHIKI"] = Util.ToString(dtBmnData.Rows[i]["KEISAN_SHIKI"]);
                dr["YOUYAKU_KAMOKU"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["YOUYAKU_KAMOKU"]));
                dr["YOUYAKU_NM"] = Util.ToString(dtBmnData.Rows[i]["YOUYAKU_NM"]);

                decimal[] Kingk = new decimal[2] { 0, 0 };
                int bumon_code = Util.ToInt(Util.ToString(dtBmnData.Rows[i]["BUMON_CD"]));
                if (kamoku_kubun == 0)
                {
                    // 小計行の金額設定
                    Kingk = GetBunruiKingk(bumon_code, kamoku_bunrui, dtBmnData, 1);
                    dr["HASSEI_KINGAKU"] = Kingk[0];
                    dr["RUIKEI_KINGAKU"] = Kingk[1];
                    dr["RUIKEI_KINGAKU_KEI"] = Kingk[1];

                    // 売上高の場合に構成比を設定
                    if (kamoku_bunrui == kamoku_bunrui_uriagedaka)
                        dr["KOUSEI_HI"] = GetKouseihi(Kingk[1], Kingk[1]);
                    else
                        dr["KOUSEI_HI"] = 0;
                }
                else if (kamoku_kubun == 1)
                {
                    // 要約明細の分類合計の設定
                    dr["HASSEI_KINGAKU"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["HASSEI_KINGAKU"]));
                    dr["RUIKEI_KINGAKU"] = Util.ToDecimal(Util.ToString(dtBmnData.Rows[i]["RUIKEI_KINGAKU"]));
                    Kingk = GetBunruiKingk(bumon_code, kamoku_bunrui, dtBmnData, 1);
                    dr["RUIKEI_KINGAKU_KEI"] = Kingk[1];

                    // 売上高の場合に構成比を設定
                    if (kamoku_bunrui == kamoku_bunrui_uriagedaka)
                        dr["KOUSEI_HI"] = GetKouseihi(Util.ToDecimal(dr["RUIKEI_KINGAKU"]), Kingk[1]);
                    else
                        dr["KOUSEI_HI"] = 0;
                }
                else if (kamoku_kubun == 2)
                {
                    // 計算式の金額設定
                    shukeiKeisanShiki = Util.ToString(dtBmnData.Rows[i]["KEISAN_SHIKI"]);
                    // 合算の科目分類時は現在の算出した物を渡す
                    if (shukeiKeisanShiki.StartsWith("9"))
                    {
                        Kingk = SetKeisansiki(bumon_code, shukeiKeisanShiki, dtBmnData, dtBumonData);
                    }
                    else
                    {
                        Kingk = SetKeisansiki(bumon_code, shukeiKeisanShiki, dtBmnData);
                    }
                    dr["HASSEI_KINGAKU"] = Kingk[0];
                    dr["RUIKEI_KINGAKU"] = Kingk[1];
                    dr["RUIKEI_KINGAKU_KEI"] = Kingk[1];
                }
                dr["NAME"] = Util.ToString(dtBmnData.Rows[i]["NAME"]);

                dtBumonData.Rows.Add(dr);

                i++;
            }
            #endregion

            #region 3.最終データ作成
            try
            {
                // １部門分をループし科目分類毎の総合計データの作成
                DataTable dtBumonTotalData = dtBmnData.Clone();
                DataRow[] bmnRow = dtBumonData.Select("BUMON_CD = " + Util.ToString(dtBumonData.Rows[0]["BUMON_CD"]));
                for (i = 0; i < bmnRow.Length; i++)
                {
                    kamoku_bunrui = Util.ToDecimal(Util.ToString(bmnRow[i]["KAMOKU_BUNRUI"]));
                    decimal[] Kingk = new decimal[2] { 0, 0 };
                    Kingk = GetBunruiKingk(kamoku_bunrui, Util.ToString(bmnRow[i]["YOUYAKU_KAMOKU"]), dtBumonData, 1);

                    dr = dtBumonTotalData.NewRow();
                    dr["SHISHO_CD"] = Util.ToDecimal(Util.ToString(bmnRow[i]["SHISHO_CD"]));

                    dr["BUMON_CD"] = BUMON_TOTAL_CD;
                    
                    dr["BUMON_NM"] = "総　合　計";
                    dr["HYOJI_JUNI"] = Util.ToDecimal(Util.ToString(bmnRow[i]["HYOJI_JUNI"]));
                    dr["KAMOKU_BUNRUI"] = Util.ToDecimal(Util.ToString(bmnRow[i]["KAMOKU_BUNRUI"]));
                    dr["KAMOKU_BUNRUI_NM"] = Util.ToString(bmnRow[i]["KAMOKU_BUNRUI_NM"]);
                    dr["TAISHAKU_KUBUN"] = Util.ToDecimal(Util.ToString(bmnRow[i]["TAISHAKU_KUBUN"]));
                    dr["MEISAI_UMU"] = Util.ToDecimal(Util.ToString(bmnRow[i]["MEISAI_UMU"]));
                    dr["KAMOKU_KUBUN"] = Util.ToDecimal(Util.ToString(bmnRow[i]["KAMOKU_KUBUN"]));
                    dr["KEISAN_SHIKI"] = Util.ToString(bmnRow[i]["KEISAN_SHIKI"]);
                    dr["YOUYAKU_KAMOKU"] = Util.ToDecimal(Util.ToString(bmnRow[i]["YOUYAKU_KAMOKU"]));
                    dr["YOUYAKU_NM"] = Util.ToString(bmnRow[i]["YOUYAKU_NM"]);

                    dr["HASSEI_KINGAKU"] = Kingk[0];
                    dr["RUIKEI_KINGAKU"] = Kingk[1];
                    decimal[] Kingk2 = new decimal[2] { 0, 0 };
                    Kingk2 = GetBunruiKingk(kamoku_bunrui, dtBumonData, 1);
                    dr["RUIKEI_KINGAKU_KEI"] = Kingk2[1];
                    dr["BUMON_KINGAKU_KEI"] = Kingk[1];
                    //dr["KOUSEI_HI"] = GetKouseihi(Kingk[1], Kingk2[1]);
                    dr["KOUSEI_HI"] = 0;
                    dr["BUMON_HI"] = Kingk[1] == 0 ? 0 : 100.0;
                    dr["NAME"] = Util.ToString(bmnRow[i]["NAME"]);

                    dtBumonTotalData.Rows.Add(dr);
                }

                // 部門合計を元に各部門毎の部門比を設定し、部門合計を追加
                DataTable dtBumonDataTable = dtBmnData.Clone();
                i = 0;
                while (dtBumonData.Rows.Count > i)
                {
                    if (Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["BUMON_CD"])) != BUMON_TOTAL_CD)
                    {
                        dr = dtBumonDataTable.NewRow();
                        dr["SHISHO_CD"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["SHISHO_CD"]));
                        dr["BUMON_CD"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["BUMON_CD"]));
                        dr["BUMON_NM"] = Util.ToString(dtBumonData.Rows[i]["BUMON_NM"]);
                        dr["HYOJI_JUNI"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["HYOJI_JUNI"]));
                        dr["KAMOKU_BUNRUI"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["KAMOKU_BUNRUI"]));
                        dr["KAMOKU_BUNRUI_NM"] = Util.ToString(dtBumonData.Rows[i]["KAMOKU_BUNRUI_NM"]);
                        dr["TAISHAKU_KUBUN"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["TAISHAKU_KUBUN"]));
                        dr["MEISAI_UMU"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["MEISAI_UMU"]));
                        dr["KAMOKU_KUBUN"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["KAMOKU_KUBUN"]));
                        dr["KEISAN_SHIKI"] = Util.ToString(dtBumonData.Rows[i]["KEISAN_SHIKI"]);
                        dr["YOUYAKU_KAMOKU"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["YOUYAKU_KAMOKU"]));
                        dr["YOUYAKU_NM"] = Util.ToString(dtBumonData.Rows[i]["YOUYAKU_NM"]);

                        dr["HASSEI_KINGAKU"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["HASSEI_KINGAKU"]));
                        dr["RUIKEI_KINGAKU"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["RUIKEI_KINGAKU"]));
                        dr["RUIKEI_KINGAKU_KEI"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["RUIKEI_KINGAKU_KEI"]));
                        dr["KOUSEI_HI"] = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["KOUSEI_HI"]));
                        dr["NAME"] = Util.ToString(dtBumonData.Rows[i]["NAME"]);
                        decimal[] Kingk = new decimal[2] { 0, 0 };
                        kamoku_bunrui = Util.ToDecimal(Util.ToString(dtBumonData.Rows[i]["KAMOKU_BUNRUI"]));
                        Kingk = GetBumonKingk(kamoku_bunrui, Util.ToString(dtBumonData.Rows[i]["YOUYAKU_KAMOKU"]), dtBumonTotalData);
                        dr["BUMON_KINGAKU_KEI"] = Kingk[1];
                        dr["BUMON_HI"] = GetKouseihi(Util.ToDecimal(dr["RUIKEI_KINGAKU"]), Kingk[1]);

                        dtBumonDataTable.Rows.Add(dr);
                        i++;
                    }
                }

                // 最後に合計を追加
                foreach (DataRow row in dtBumonTotalData.Rows)
                {
                    dtBumonDataTable.ImportRow(row);
                }

                return dtBumonDataTable;
            }
            catch (Exception ex)
            {
                dt = null;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                try
                {
                    // 無条件に削除
                    this._dba.ModifyBySql("DROP TABLE " + WK_TABLE, dpc);
                }
                catch (Exception) { }

            }
            #endregion

            return dt;
        }

        /// <summary>
        /// 構成比取得
        /// </summary>
        /// <param name="si">分子</param>
        /// <param name="bo">分母</param>
        /// <returns>結果</returns>
        private decimal GetKouseihi(decimal si, decimal bo)
        {
            decimal ret = 0;
            if (si == 0 || bo == 0)
                return 0;
            else
            {
                ret = TaxUtil.CalcFraction((si / bo * 100), -1, 2);
            }
            return ret;
        }

        /// <summary>
        /// 分類明細の金額取得
        /// </summary>
        /// <param name="bumonCd">部門コード</param>
        /// <param name="kamoku_bunrui">科目分類</param>
        /// <param name="youyaku_kamoku">要約科目</param>
        /// <returns>金額情報</returns>
        private decimal[] GetKingk(int bumonCd, decimal kamoku_bunrui, decimal youyaku_kamoku)
        {
            // 明細の金額の取得

            decimal[] Kingk = new decimal[2] { 0, 0 };

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" SELECT");

            sql.AppendLine(" A.KAMOKU_BUNRUI,");
            sql.AppendLine(" A.TAISHAKU_KUBUN,");
            sql.AppendLine(" B.YOUYAKU_KAMOKU,");
            sql.AppendLine(" B.KANJO_KAMOKU_CD,");
            sql.AppendLine(" B.YOUYAKU_NM,");

            sql.AppendLine(" C.TAISHAKU_KUBUN AS MEISAI_TAISHAKU_KUBUN,");
            sql.AppendLine(" C.HASSEI_KINGAKU,");
            sql.AppendLine(" C.RUIKEI_KINGAKU");

            sql.AppendLine(" FROM");
            sql.AppendLine(" TB_ZM_BMN_SNK_KMK_BNRI AS A");

            sql.AppendLine(" LEFT OUTER JOIN");
            sql.AppendLine("    TB_ZM_BMN_SNK_YUYK_STI AS B");
            sql.AppendLine("  ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.AppendLine(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.AppendLine(" AND A.KAMOKU_BUNRUI = B.KAMOKU_BUNRUI");
            
            sql.AppendLine(" LEFT OUTER JOIN");
            sql.AppendLine("    " + WK_TABLE + " AS C");
            sql.AppendLine("  ON C.BUMON_CD = @BUMON_CD ");
            sql.AppendLine(" AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");

            sql.AppendLine(" WHERE");
            sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" A.KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND");
            sql.AppendLine(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.AppendLine(" B.YOUYAKU_KAMOKU = @YOUYAKU_KAMOKU AND");
            sql.AppendLine(" C.BUMON_CD = @BUMON_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, bumonCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 5, kamoku_bunrui);
            dpc.SetParam("@YOUYAKU_KAMOKU", SqlDbType.Decimal, 4, youyaku_kamoku);

            DataTable dt = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            foreach (DataRow r in dt.Rows)
            {
                // 要約設定の貸借区分と勘定科目の貸借区分判定
                int sign = 1;
                if (Util.ToDecimal(Util.ToString(r["TAISHAKU_KUBUN"])) !=
                    Util.ToDecimal(Util.ToString(r["MEISAI_TAISHAKU_KUBUN"])))
                {
                    sign = -1;
                }

                Kingk[0] += ((Util.ToDecimal(Util.ToString(r["HASSEI_KINGAKU"])) * sign));
                Kingk[1] += ((Util.ToDecimal(Util.ToString(r["RUIKEI_KINGAKU"])) * sign));
            }

            return Kingk;
        }

        // 計算式での金額取得
        private decimal[] SetKeisansiki(int bumonCd, string calc, DataTable dt)
        {
            decimal[] Kingk = new decimal[2] { 0, 0 };

            string siki = calc.Replace("+", " +");
            siki = siki.Replace("-", " -");
            string[] items = siki.Split(' ');
            foreach (string item in items)
            {
                int cd = Util.ToInt(item);
                int pm = cd < 0 ? -1 : 1;
                decimal[] w = GetBunruiKingk(bumonCd, Math.Abs(cd), dt, 1);
                Kingk[0] += (w[0] * pm);
                Kingk[1] += (w[1] * pm);
            }
            return Kingk;
        }
        private decimal[] SetKeisansiki(int bumonCd, string calc, DataTable dt, DataTable dtLog)
        {
            decimal[] Kingk = new decimal[2] { 0, 0 };

            string siki = calc.Replace("+", " +");
            siki = siki.Replace("-", " -");
            string[] items = siki.Split(' ');
            foreach (string item in items)
            {
                int cd = Util.ToInt(item);
                int pm = cd < 0 ? -1 : 1;
                decimal[] w = GetBunruiKingk(bumonCd, Math.Abs(cd), (Math.Abs(cd) > 90000 ? dtLog : dt), 1);
                Kingk[0] += (w[0] * pm);
                Kingk[1] += (w[1] * pm);
            }
            return Kingk;
        }
        // 部門、科目分類毎の合計金額の取得
        private decimal[] GetBunruiKingk(int bumonCd, decimal kamoku_bunrui, DataTable dt, int sign)
        {
            decimal[] Kingk = new decimal[2] { 0, 0};
            DataRow[] kingkRow = dt.Select("BUMON_CD = " + bumonCd.ToString() + " AND KAMOKU_BUNRUI = " + kamoku_bunrui.ToString());
            for (int i = 0; i < kingkRow.Length; i++)
            {
                Kingk[0] += ((Util.ToDecimal(Util.ToString(kingkRow[i]["HASSEI_KINGAKU"])) * sign));
                Kingk[1] += ((Util.ToDecimal(Util.ToString(kingkRow[i]["RUIKEI_KINGAKU"])) * sign));
            }
            return Kingk;
        }
        // 科目分類、要約科目毎の合計金額の取得
        private decimal[] GetBunruiKingk(decimal kamoku_bunrui, string youyaku_kamoku, DataTable dt, int sign)
        {
            decimal[] Kingk = new decimal[2] { 0, 0 };
            DataRow[] kingkRow = dt.Select("KAMOKU_BUNRUI = " + kamoku_bunrui.ToString() + " AND YOUYAKU_KAMOKU = " + youyaku_kamoku);
            for (int i = 0; i < kingkRow.Length; i++)
            {
                Kingk[0] += ((Util.ToDecimal(Util.ToString(kingkRow[i]["HASSEI_KINGAKU"])) * sign));
                Kingk[1] += ((Util.ToDecimal(Util.ToString(kingkRow[i]["RUIKEI_KINGAKU"])) * sign));
            }
            return Kingk;
        }
        // 科目分類の合計金額の取得
        private decimal[] GetBunruiKingk(decimal kamoku_bunrui, DataTable dt, int sign)
        {
            decimal[] Kingk = new decimal[2] { 0, 0 };
            DataRow[] kingkRow = dt.Select("KAMOKU_BUNRUI = " + kamoku_bunrui.ToString() + " AND YOUYAKU_KAMOKU <> 0 ");
            for (int i = 0; i < kingkRow.Length; i++)
            {
                Kingk[0] += ((Util.ToDecimal(Util.ToString(kingkRow[i]["HASSEI_KINGAKU"])) * sign));
                Kingk[1] += ((Util.ToDecimal(Util.ToString(kingkRow[i]["RUIKEI_KINGAKU"])) * sign));
            }
            return Kingk;
        }

        /// <summary>
        /// 科目分類、要約科目毎の部門合計金額の取得
        /// </summary>
        /// <param name="kamoku_bunrui">科目分類</param>
        /// <param name="youyaku_kamoku">要約科目</param>
        /// <param name="dt">取得対象DataTable（部門合計）</param>
        /// <returns>条件で集計した発生、累計金額</returns>
        private decimal[] GetBumonKingk(decimal kamoku_bunrui, string youyaku_kamoku, DataTable dt)
        {
            decimal[] Kingk = new decimal[2] { 0, 0 };
            DataRow[] kingkRow = dt.Select("KAMOKU_BUNRUI = " + kamoku_bunrui.ToString() + " AND YOUYAKU_KAMOKU = " + youyaku_kamoku);
            for (int i = 0; i < kingkRow.Length; i++)
            {
                Kingk[0] += ((Util.ToDecimal(Util.ToString(kingkRow[i]["HASSEI_KINGAKU"]))));
                Kingk[1] += ((Util.ToDecimal(Util.ToString(kingkRow[i]["RUIKEI_KINGAKU"]))));
            }
            return Kingk;
        }
        #endregion
    }
}
