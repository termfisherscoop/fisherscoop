﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1051
{
    /// <summary>
    /// 部門別損益管理表[要約設定](ZMMR1053)
    /// </summary>
    public partial class ZMMR1053 : BasePgForm
    {
        #region 変数

        // 要約設定の対象となる勘定科目情報の保持
        [Serializable()]
        class Kamoku
        {
            public int Code { get; set; }
            public string Name { get; set; }
            public int BSKb { get; set; }
            public int Sel { get; set; }
        }

        [Serializable()]
        class ItemsTable
        {
            public int BSKb { get; set; }
            public List<Kamoku> KanjyoKamoku { get; set; }
            public ItemsTable()
            {
                KanjyoKamoku = new List<Kamoku>();
            }
        }
        // 要約設定の保持
        [Serializable()]
        class YoyakuSettei
        {
            public long KamokuBunruiCd { get; set; }
            public string KamokuBunruiNm { get; set; }
            public int BSKb { get; set; }
            public int MeUm { get; set; }
            public int KMKb { get; set; }
            public string CalcFormula { get; set; }
            public int YykKMKCd { get; set; }
            public string YykKMKNm { get; set; }
            public List<Kamoku> KanjyoKamoku { get; set; }
            public YoyakuSettei()
            {
                KanjyoKamoku = new List<Kamoku>();
            }
        }

        // 表示行情報
        class RowInfo
        {
            public List<ItemsTable> No { get; set; }
        }

        // 要約設定リスト
        List<YoyakuSettei> Yoyaku;

        // 勘定科目桁数
        const int CodeLength = 4;

        // 勘定科目情報リスト
        List<Kamoku> KanjyoKamoku;

        // 表示行情報
        RowInfo SelKmk;
        // 表示行位置
        int SelRow;

        #endregion

        #region 定数
        /// <summary>
        /// タブコントロールindex
        /// </summary>
        private string mojiStyle;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMMR1053(ZMMR1051 frm)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            InitializeComponent();

            // タイトル非表示
            this.lblTitle.Visible = false;

            // データ表示
            yoyakuSetteiArea();

            // Escape F1 F6のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF7.Location = this.btnF3.Location;
            this.btnF8.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            if (Msg.ConfYesNo("保存しますか？") == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            // データ保存処理
            UpdateData();

            // 画面を閉じる
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        public override void PressF7()
        {
            RowAddItem();
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            RowRemoveItem();
        }
        #endregion

        #region イベント

        /// <summary>
        /// 選択した項目の移動処理(上→下)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDMove_Click(object sender, EventArgs e)
        {
            if (lbxTaishoKanjoKamoku.SelectedItem != null)
            {
                var cn =(CodeNameListItem)lbxTaishoKanjoKamoku.SelectedItem;

                RemoveRow(Util.ToInt(cn.Code));

                ShowSelect(SelKmk.No[SelRow].KanjyoKamoku);
                ShowList();
            }
        }

        /// <summary>
        /// 選択した項目の移動処理(下→上)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUMove_Click(object sender, EventArgs e)
        {
            if (lbxKanjoKamokuIchiran.SelectedItem != null)
            {
                var cn = (CodeNameListItem)lbxKanjoKamokuIchiran.SelectedItem;

                AddRow(Util.ToInt(cn.Code));

                ShowSelect(SelKmk.No[SelRow].KanjyoKamoku);
                ShowList();
            }
        }
        
        /// <summary>
        /// 科目名変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void kamokuNm_change(object sender, KeyEventArgs e)
        {
            string kamokuNm = Util.ToString(this.txtKamokuNm.Text);

            KamokuDisp(kamokuNm);
        }

        /// <summary>
        /// ラジオボタン(借方)押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoKari_Click(object sender, EventArgs e)
        {
            // 状態保持の為、特に
        }

        /// <summary>
        /// ラジオボタン(貸方)押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoKashi_Click(object sender, EventArgs e)
        {
            // 状態保持の為、特に
        }

        /// <summary>
        /// 文字スタイル変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void moji_Enter(object sender, EventArgs e)
        {
            mojiStyle = this.txtMoji.Text;
        }

        /// <summary>
        /// 文字スタイル変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void moji_change(object sender, KeyEventArgs e)
        {
            //数字以外が入力された時はキャンセルする
            if (!ValChk.IsNumber(this.txtMoji.Text))
            {
                this.txtMoji.Text = mojiStyle;
                this.txtMoji.SelectAll();
                return;
            }

            if (mojiStyle == "0" && this.txtMoji.Text == "00")
            {
                this.txtMoji.Text = "0";
            }
            else if (mojiStyle == "0" && this.txtMoji.Text == "01")
            {
                this.txtMoji.Text = "1";
            }
            else if (mojiStyle == "0" && this.txtMoji.Text == "10")
            {
                this.txtMoji.Text = "1";
            }

            else if (mojiStyle == "1" && this.txtMoji.Text == "01")
            {
                this.txtMoji.Text = "0";
            }
            else if (mojiStyle == "1" && this.txtMoji.Text == "10")
            {
                this.txtMoji.Text = "0";
            }
            else if (mojiStyle == "1" && this.txtMoji.Text == "11")
            {
                this.txtMoji.Text = "1";
            }
            
            int mojiShubetsu = Util.ToInt(this.txtMoji.Text);

            if (mojiShubetsu <= 0)
            {
                this.txtMoji.Text = "0";
                mojiStyle = this.txtMoji.Text;
                mojiShubetsu = 0;
            }
            else if (mojiShubetsu >= 1)
            {
                this.txtMoji.Text = "1";
                mojiStyle = this.txtMoji.Text;
                mojiShubetsu = 1;
            }
        }

        /// <summary>
        /// データグリッド表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView g = (DataGridView)sender;

            if (g.Columns[e.ColumnIndex].Index == 4)
            {
                if (g["colKMKb", e.RowIndex].Value != null)
                {
                    var v = g["colKMKb", e.RowIndex].Value.ToString();
                    if (v == "1")
                    {
                        g.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.SkyBlue;
                        e.FormattingApplied = true;
                    }
                }
            }
        }

        /// <summary>
        /// データグリッドのクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // 項目名称にフォーカス
            this.txtKamokuNm.Focus();
        }

        /// <summary>
        /// データグリッドのエンター
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {

            SelRow = e.RowIndex;
            ShowFrame(SelRow);
        }

        /// <summary>
        /// 貸借区分ラジオボタンチェンジ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdo_CheckedChanged(object sender, EventArgs e)
        {

            int taisyakuKbn = 0;
            if (rdoKari.Checked)
                taisyakuKbn = 1;
            if (rdoKashi.Checked)
                taisyakuKbn = 2;
            TaisyakuDisp(taisyakuKbn);
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 画面の表示処理
        /// </summary>
        /// <returns></returns>
        bool yoyakuSetteiArea()
        {
            // 要約情報の設定
            if (!SetYoyaku())
            {
                return false;
            }

            // 要約設定の対象となる勘定科目情報の保持
            SetKamoku();

            // 要約情報から画面制御のDataTable作成及び表示
            DataTable dt = CreateTable(Yoyaku);
            if (dt != null)
            {
                SelRow = 0;
                this.dataGridView1.DataSource = dt;

                //this.dataGridView1.Columns["colName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            return true;
        }

        #region 表示情報の作成

        /// <summary>
        /// 要約設定データの保持
        /// </summary>
        /// <returns></returns>
        private bool SetYoyaku()
        {
            // TB_ZM_BMN_SNK_KMK_BNRIより明細科目の取得
            DataTable dt = GetKamokuBunrui(1);
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                Yoyaku = new List<YoyakuSettei>();
                int i = 0;
                while (dt.Rows.Count > i)
                {
                    YoyakuSettei y = new YoyakuSettei();
                    // 科目分類情報を設定
                    y.KamokuBunruiCd = Util.ToLong(dt.Rows[i]["KAMOKU_BUNRUI"]);
                    y.KamokuBunruiNm = Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI_NM"]);
                    y.BSKb = Util.ToInt(dt.Rows[i]["TAISHAKU_KUBUN"]);
                    y.MeUm = Util.ToInt(dt.Rows[i]["MEISAI_UMU"]);
                    y.KMKb = Util.ToInt(dt.Rows[i]["KAMOKU_KUBUN"]);
                    y.CalcFormula = Util.ToString(dt.Rows[i]["KEISAN_SHIKI"]);
                    y.YykKMKCd = 0;
                    y.YykKMKNm = "";
                    Yoyaku.Add(y);

                    if (y.MeUm == 1)
                    {
                        long YykCode = -1;
                        // 要約設定（明細）情報の設定
                        DataTable dty = GetYoyakSettei(y.KamokuBunruiCd);
                        int j = 0;
                        YoyakuSettei z = new YoyakuSettei();
                        while (dty.Rows.Count > j)
                        {
                            // 要約科目を設定
                            if (Util.ToLong(dty.Rows[j]["YOYAK_KAMOKU_CD"]) != YykCode)
                            {
                                z = new YoyakuSettei();
                                z.KamokuBunruiCd = Util.ToLong(dt.Rows[i]["KAMOKU_BUNRUI"]);
                                z.KamokuBunruiNm = Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI_NM"]);
                                z.BSKb = Util.ToInt(dty.Rows[j]["TAISHAKU_KUBUN"]);
                                z.MeUm = Util.ToInt(dt.Rows[i]["MEISAI_UMU"]);
                                z.KMKb = 0;
                                z.CalcFormula = "";
                                z.YykKMKCd = Util.ToInt(dty.Rows[j]["YOYAK_KAMOKU_CD"]);
                                z.YykKMKNm = Util.ToString(dty.Rows[j]["YOYAK_KAMOKU_NM"]);
                                Yoyaku.Add(z);
                                YykCode = Util.ToLong(dty.Rows[j]["YOYAK_KAMOKU_CD"]);
                            }
                            // 勘定科目を設定
                            if (Util.ToLong(dty.Rows[j]["KANJO_KAMOKU_CD"]) != 0)
                            {
                                Kamoku k = new Kamoku();
                                k.Code = Util.ToInt(dty.Rows[j]["KANJO_KAMOKU_CD"]);
                                k.Name = Util.ToString(dty.Rows[j]["KANJO_KAMOKU_NM"]);
                                k.BSKb = Util.ToInt(dty.Rows[j]["KMK_TAISHAKU_KUBUN"]);
                                z.KanjyoKamoku.Add(k);
                            }
                            j++;
                        }
                    }
                    i++;
                }
            }
            return true;
        }

        /// <summary>
        /// 科目分類情報の取得
        /// </summary>
        /// <returns></returns>
        private DataTable GetKamokuBunrui()
        {
            return GetKamokuBunrui(-1);
        }
        private DataTable GetKamokuBunrui(int kamokukubun)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" A.*");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_BMN_SNK_KMK_BNRI AS A");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = 0 AND");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            if (kamokukubun != -1)
            {
                sql.Append(" AND A.KAMOKU_KUBUN = @KAMOKU_KUBUN");
            }
            sql.Append(" ORDER BY");
            sql.Append(" A.HYOJI_JUNI ASC,");
            sql.Append(" A.KAMOKU_BUNRUI ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAMOKU_KUBUN", SqlDbType.Decimal, 1, kamokukubun);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dt;
        }

        /// <summary>
        /// 要約設定の取得
        /// </summary>
        /// <returns></returns>
        private DataTable GetYoyakSettei()
        {
            return GetYoyakSettei(-1);
        }
        private DataTable GetYoyakSettei(long kamokubunrui)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" A.KAMOKU_BUNRUI,");
            sql.Append(" A.YOUYAKU_KAMOKU AS YOYAK_KAMOKU_CD,");
            sql.Append(" A.KANJO_KAMOKU_CD,");
            sql.Append(" A.YOUYAKU_NM AS YOYAK_KAMOKU_NM,");
            sql.Append(" A.TAISHAKU_KUBUN,");
            sql.Append(" B.TAISHAKU_KUBUN AS KMK_TAISHAKU_KUBUN,");
            sql.Append(" B.KANJO_KAMOKU_NM");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_BMN_SNK_YUYK_STI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.Append("  ON A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            sql.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = 0 AND");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            if (kamokubunrui != -1)
            {
                sql.Append(" AND A.KAMOKU_BUNRUI = @KAMOKU_BUNRUI");
            }
            sql.Append(" AND A.KANJO_KAMOKU_CD IS NOT NULL");
            sql.Append(" ORDER BY");
            sql.Append(" A.KAMOKU_BUNRUI ASC,");
            sql.Append(" A.YOUYAKU_KAMOKU ASC,");
            sql.Append(" A.KANJO_KAMOKU_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, UInfo.KaikeiNendo);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 5, kamokubunrui);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dt;
        }

        /// <summary>
        /// 要約設定の対象となる勘定科目情報の保持
        /// </summary>
        /// <returns></returns>
        private bool SetKamoku()
        {
            KanjyoKamoku = new List<Kamoku>();

            // 要約設定の対象となる勘定科目情報の取得
            DataTable dt = GetKamoku();
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                int i = 0;
                while (dt.Rows.Count > i)
                {
                    Kamoku k = new Kamoku();
                    k.Code = Util.ToInt(dt.Rows[i]["KANJO_KAMOKU_CD"]);
                    k.Name = Util.ToString(dt.Rows[i]["KANJO_KAMOKU_NM"]);
                    k.BSKb = Util.ToInt(dt.Rows[i]["TAISHAKU_KUBUN"]);
                    k.Sel = Util.ToInt(dt.Rows[i]["FLG"]);
                    KanjyoKamoku.Add(k);

                    i++;
                }
            }
            return true;
        }

        /// <summary>
        /// 要約設定の対象となる勘定科目情報の取得
        /// </summary>
        /// <returns></returns>
        private DataTable GetKamoku()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append(" A.KANJO_KAMOKU_CD");
            sql.Append(",A.KANJO_KAMOKU_NM");
            sql.Append(",A.KAMOKU_BUNRUI_CD");
            sql.Append(",A.TAISHAKU_KUBUN");
            //
            sql.Append(",CASE WHEN ISNULL(C.KANJO_KAMOKU_CD, 0) <> 0 THEN 1 ELSE 0 END AS FLG");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_KANJO_KAMOKU AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_F_KAMOKU_BUNRUI AS B");
            sql.Append("  ON A.KAMOKU_BUNRUI_CD = B.KAMOKU_BUNRUI_CD");
            sql.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" AND A.KAISHA_CD = @KAISHA_CD");

            sql.Append(" LEFT OUTER JOIN TB_ZM_BMN_SNK_YUYK_STI AS C");
            sql.Append("  ON C.KAISHA_CD = A.KAISHA_CD");
            sql.Append(" AND C.SHISHO_CD = 0");
            sql.Append(" AND C.KAIKEI_NENDO = A.KAIKEI_NENDO");
            sql.Append(" AND C.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD");

            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append(" B.BUNRUI_CD1 = 2");
            sql.Append(" ORDER BY");
            sql.Append(" A.KAMOKU_BUNRUI_CD ASC,");
            sql.Append(" A.KANJO_KAMOKU_CD ASC");
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dt;
        }

        /// <summary>
        /// 画面制御のDataTable作成及
        /// </summary>
        /// <param name="yykList"></param>
        /// <returns></returns>
        DataTable CreateTable(List<YoyakuSettei> yykList)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Code", Type.GetType("System.Int32"));
            dt.Columns.Add("Name", Type.GetType("System.String"));
            dt.Columns.Add("BS", Type.GetType("System.String"));
            dt.Columns.Add("BSKbn", Type.GetType("System.Int32"));
            dt.Columns.Add("KMKb", Type.GetType("System.Int32"));

            SelKmk = new RowInfo();
            SelKmk.No = new List<ItemsTable>();
            ItemsTable item;

            foreach (YoyakuSettei y in yykList)
            {
                var r = dt.NewRow();
                r["Code"] = y.KamokuBunruiCd;
                if (y.KMKb == 0)
                {
                    r["Name"] = y.YykKMKNm;
                }
                else
                {
                    r["Name"] = y.KamokuBunruiNm;
                }
                r["BS"] = (y.BSKb == 1 ? "借方" : "貸方");
                r["BSKbn"] = y.BSKb;
                r["KMKb"] = y.KMKb;

                item = new ItemsTable();
                if (y.KanjyoKamoku.Count != 0)
                {
                    // 内訳勘定科目の設定
                    SetKamokuCode(y.KanjyoKamoku, ref item);
                }

                // 画面制御用行情報
                SelKmk.No.Add(item);

                // 画面表示用行情報
                dt.Rows.Add(r);
            }

            return dt;
        }

        /// <summary>
        /// 内訳勘定科目の設定
        /// </summary>
        /// <param name="t"></param>
        /// <param name="s"></param>
        void SetKamokuCode(List<Kamoku> t, ref ItemsTable s)
        {
            try
            {
                int i = 0;
                s.BSKb = 0;
                foreach (Kamoku k in t)
                {
                    s.BSKb = i + 1;
                    Kamoku km = new Kamoku();
                    km.Code = k.Code;
                    km.Name = k.Name;
                    km.BSKb = k.BSKb;
                    s.KanjyoKamoku.Add(km);
                    KamokuCodeSetting(k.Code, 1);
                    i++;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// 保持勘定科目情報の検索及び設定
        /// </summary>
        /// <param name="kmkcode"></param>
        /// <param name="sel"></param>
        void KamokuCodeSetting(int kmkcode, int sel)
        {
            try
            {
                var q =
                    from a in KanjyoKamoku
                    where a.Code == kmkcode
                    select a;

                if (q.Count<Kamoku>() != 0)
                {
                    q.ElementAtOrDefault(0).Sel = sel;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        #endregion

        #region 画面制御サポート

        /// <summary>
        /// 選択行の科目情報の表示
        /// </summary>
        /// <param name="row">選択行</param>
        void ShowFrame(int row)
        {

            DataGridView g = dataGridView1;
            if (g.Rows.Count >= 0 && g.Rows[row] != null)
            {
                this.txtKamokuNm.Text = Util.ToString(g["colName", row].Value);
                if (Util.ToInt(g["colBSKbn", row].Value) == 1)
                {
                    rdoKari.Checked = true;
                }
                else if (Util.ToInt(g["colBSKbn", row].Value) == 2)
                {
                    rdoKashi.Checked = true;
                }

                if (Util.ToInt(g["colKMKb", row].Value) == 0)
                {
                    fsiTableLayoutPanel2.Visible = true;
                    ShowSelect(SelKmk.No[row].KanjyoKamoku);
                    ShowList();
                }
                else
                {
                    fsiTableLayoutPanel2.Visible = false;
                }
                g.Focus();
            }
        }

        /// <summary>
        /// 選択勘定科目の内容及びリスト設定
        /// </summary>
        /// <param name="tbl"></param>
        void ShowSelect(List<Kamoku> tbl)
        {
            lbxTaishoKanjoKamoku.Items.Clear();
            lbxTaishoKanjoKamoku.Items.AddRange(ToIListItems(tbl).ToArray());
        }

        /// <summary>
        /// 選択勘定科目のリスト表示
        /// </summary>
        void ShowList()
        {
            var q =
                from p in KanjyoKamoku
                where p.Sel == 0
                select p;

            List<Kamoku> tbl = new List<Kamoku>();
            tbl.AddRange(q);

            lbxKanjoKamokuIchiran.Items.Clear();
            lbxKanjoKamokuIchiran.Items.AddRange(ToIListItems(tbl).ToArray());
        }

        /// <summary>
        /// 科目名称の表示
        /// </summary>
        /// <param name="kmkName"></param>
        void KamokuDisp(string kmkName)
        {
            DataGridView g = dataGridView1;
            try
            {
                g.Rows[SelRow].Cells["colName"].Value = kmkName;
                g.Refresh();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// 貸借区分の表示
        /// </summary>
        /// <param name="taisyakuKbn"></param>
        void TaisyakuDisp(int taisyakuKbn)
        {
            DataGridView g = dataGridView1;
            try
            {
                if (taisyakuKbn == 1)
                {
                    g.Rows[SelRow].Cells["colBSKbn"].Value = 1;
                    g.Rows[SelRow].Cells["colBS"].Value = "借方";
                }
                else if (taisyakuKbn == 2)
                {
                    g.Rows[SelRow].Cells["colBSKbn"].Value = 2;
                    g.Rows[SelRow].Cells["colBS"].Value = "貸方";
                }
                g.Refresh();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// グリッド行追加
        /// </summary>
        void RowAddItem()
        {
            DataGridView g = dataGridView1;

            if (g.SelectedRows != null)
            {
                int Row = SelRow + 1;

                try
                {
                    {
                        ItemsTable item = new ItemsTable();
                        SelKmk.No.Insert(Row, item);
                    }
                    {
                        YoyakuSettei item = new YoyakuSettei();
                        Yoyaku.Insert(Row, item);
                    }
                    {
                        DataTable dt = (DataTable)g.DataSource;
                        DataRow dr = dt.NewRow();
                        dt.Rows.InsertAt(dr, Row);
                    }

                    g.CurrentCell = g[5, Row];

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// グリッド行削除
        /// </summary>
        void RowRemoveItem()
        {
            DataGridView g = dataGridView1;

            if (g.SelectedRows != null)
            {
                int Row = SelRow;

                try
                {
                    foreach (Kamoku k in SelKmk.No[SelRow].KanjyoKamoku)
                    {
                        var kmkCode = k.Code;

                        // 科目リストから解除
                        KamokuCodeSetting(k.Code, 0);
                    }
                    SelKmk.No.RemoveAt(SelRow);
                    Yoyaku.RemoveAt(SelRow);

                    g.Rows.RemoveAt(Row);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
        }

        /// <summary>
        /// 勘定科目リストの追加
        /// </summary>
        /// <param name="KmkCode"></param>
        void AddRow(int KmkCode)
        {
            KamokuCodeSetting(KmkCode, 1);

            var q =
                from a in KanjyoKamoku
                where a.Code == KmkCode
                select a;

            if (q.Count<Kamoku>() != 0)
            {
                var k = new Kamoku();
                k.Code = q.ElementAtOrDefault(0).Code;
                k.Name = q.ElementAtOrDefault(0).Name;
                k.BSKb = q.ElementAtOrDefault(0).BSKb;

                if (lbxTaishoKanjoKamoku.Items.Count == 0)
                {
                    if (k.BSKb == 1)
                        this.rdoKari.Checked = true;
                    if (k.BSKb == 2)
                        this.rdoKashi.Checked = true;
                    TaisyakuDisp(k.BSKb);

                    txtKamokuNm.Text = k.Name;
                    KamokuDisp(k.Name);

                }

                SelKmk.No[SelRow].KanjyoKamoku.Add(k.DeepClone());

                Yoyaku[SelRow].KanjyoKamoku.Add(k.DeepClone());
            }
        }

        /// <summary>
        /// 勘定科目リストの削除
        /// </summary>
        /// <param name="KmkCode"></param>
        void RemoveRow(int KmkCode)
        {
            KamokuCodeSetting(KmkCode, 0);
            SelKmk.No[SelRow].KanjyoKamoku.RemoveAll(k => k.Code == KmkCode);

            Yoyaku[SelRow].KanjyoKamoku.RemoveAll(k => k.Code == KmkCode);
        }

        /// <summary>
        /// 勘定科目リストコピー
        /// </summary>
        /// <param name="tbl"></param>
        /// <returns></returns>
        List<IListItem> ToIListItems(List<Kamoku> tbl)
        {
            if (tbl == null)
            {
                return new List<IListItem>();
            }

            List<IListItem> l = new List<IListItem>();
            for (int i = 0; i < tbl.Count; i++)
            {
                l.Add(new CodeNameListItem(CodeLength, ((tbl[i].Code > 0) ? tbl[i].Code.ToString() : ""), tbl[i].Name));
            }

            return l;
        }
        #endregion

        /// <summary>
        /// 要約設定データ更新処理
        /// </summary>
        void UpdateData()
        {
            try
            {
                this.Dba.BeginTransaction();

                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                // データDelete
                this.Dba.Delete("TB_ZM_BMN_SNK_YUYK_STI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                    dpc);

                DbParamCollection dpcInsert;
                int kamokuBunrui = -1;
                int kamokuCode;
                string kamokuNm;
                int taishakuNo;
                int gyoBango = 0;
                int allDataCount = dataGridView1.RowCount;
                for (int i = 0; allDataCount > i; i++)
                {
                    if (kamokuBunrui != Util.ToInt(dataGridView1["colCode", i].Value))
                    {
                        if (kamokuBunrui != 0 && Util.ToInt(dataGridView1["colCode", i].Value) == 0)
                        {
                            // 行挿入時
                        }
                        else
                        {
                            kamokuBunrui = Util.ToInt(dataGridView1["colCode", i].Value);
                            gyoBango = 0;
                        }
                    }
                    if (0 == Util.ToInt(dataGridView1["colKMKb", i].Value))
                    {
                        kamokuNm = Util.ToString(dataGridView1["colName", i].Value);
                        taishakuNo = Util.ToInt(dataGridView1["colBSKbn", i].Value);
                        gyoBango++;
                        foreach (Kamoku k in SelKmk.No[i].KanjyoKamoku)
                        {
                            kamokuCode = k.Code;

                            dpcInsert = new DbParamCollection();
                            dpcInsert.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                            dpcInsert.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
                            dpcInsert.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

                            dpcInsert.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 5, kamokuBunrui);
                            dpcInsert.SetParam("@YOUYAKU_KAMOKU", SqlDbType.Decimal, 4, gyoBango);
                            dpcInsert.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kamokuCode);
                            dpcInsert.SetParam("@YOUYAKU_NM", SqlDbType.VarChar, 64, kamokuNm);
                            dpcInsert.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, taishakuNo);
                            dpcInsert.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
                            // データInsert
                            this.Dba.Insert("TB_ZM_BMN_SNK_YUYK_STI", dpcInsert);
                        }
                    }
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }
        #endregion

    }

    #region IListItemインタフェース実装クラス

    public class CodeNameListItem : IListItem, ICloneable
    {
        #region コンストラクタ
        int codelen = 10;
        private CodeNameListItem()
        {
        }
        public CodeNameListItem(int codeLen, string code, string name)
        {
            this.codelen = codeLen;
            this.code = code;
            this.name = name;
        }
        public CodeNameListItem(string code, string name)
        {
            this.code = code;
            this.name = name;
        }
        public CodeNameListItem(CodeNameListItem other)
            : this(other.code, other.name)
        {
        }
        #endregion

        #region プロパティ
        private readonly string code;
        public string Code
        {
            get { return this.code; }
        }
        private readonly string name = null;
        public string Name
        {
            get { return this.name; }
        }
        #endregion

        #region IListItem メンバ

        public virtual string DisplayValue
        {
            get
            {
                if (this.codelen < 0)
                {
                    return name;
                }
                else
                {
                    int chkOut;

                    bool res = int.TryParse(code, out chkOut);
                    if (res && chkOut < 0)
                    {
                        return string.Format("{0, " + this.codelen.ToString() + "} {1}", " ", name);
                    }
                }

                return string.Format("{0, " + this.codelen.ToString() + "} {1}", code, name);
                //return string.Format("{0, -" + this.codelen.ToString() + "} {1}", code, name); 
            }
        }

        public virtual object Value
        {
            get { return this.code; }
        }
        #endregion

        #region IComparable メンバ

        public virtual int CompareTo(object obj)
        {
            CodeNameListItem other = (CodeNameListItem)obj;
            return this.code.CompareTo(other.code);
        }
        #endregion

        #region ICloneable メンバ

        public virtual object Clone()
        {
            return new CodeNameListItem(this);
        }
        #endregion

        #region オーバーライド

        public override int GetHashCode()
        {
            return (code + name).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == (object)this)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (!(obj is CodeNameListItem))
            {
                return false;
            }
            CodeNameListItem other = (CodeNameListItem)obj;
            return ((this.code == other.code) && (this.name == other.name));
        }

        public override string ToString()
        {
            return DisplayValue;
        }
        #endregion
    }
    #endregion

    #region リスト(ListBox、ComboBox)用の項目(Item)として扱う為のインタフェース

    public interface IListItem : IComparable
    {
        /// <summary>
        /// リストの表示用に使用される値です
        /// </summary>
        string DisplayValue
        {
            get;
        }

        /// <summary>
        /// リストの表示項目に対応する、実値です
        /// </summary>
        object Value
        {
            get;
        }
    }
    #endregion

    #region 拡張メソッド

    public static class ObjectExtension
    {
        // ディープコピーの複製を作る拡張メソッド
        public static T DeepClone<T>(this T src)
        {
            using (var memoryStream = new System.IO.MemoryStream())
            {
                var binaryFormatter
                  = new System.Runtime.Serialization
                        .Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, src); // シリアライズ
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                //return (T)binaryFormatter.Deserialize(memoryStream); // デシリアライズ
                return src;
            }
        }
    }

    #endregion

}
