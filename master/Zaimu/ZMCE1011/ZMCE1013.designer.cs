﻿namespace jp.co.fsi.zm.zmce1011
{
    partial class ZMCE1013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdoUchiwake = new System.Windows.Forms.RadioButton();
            this.rdoGokei = new System.Windows.Forms.RadioButton();
            this.rdoNashi = new System.Windows.Forms.RadioButton();
            this.rdoAri = new System.Windows.Forms.RadioButton();
            this.lblKanjoKamokuNmTo = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuNmFr = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBt = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 194);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(784, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(773, 31);
            this.lblTitle.Text = "補助科目残高";
            // 
            // rdoUchiwake
            // 
            this.rdoUchiwake.AutoSize = true;
            this.rdoUchiwake.BackColor = System.Drawing.Color.Silver;
            this.rdoUchiwake.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoUchiwake.Location = new System.Drawing.Point(249, 9);
            this.rdoUchiwake.Margin = new System.Windows.Forms.Padding(4);
            this.rdoUchiwake.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoUchiwake.Name = "rdoUchiwake";
            this.rdoUchiwake.Size = new System.Drawing.Size(90, 24);
            this.rdoUchiwake.TabIndex = 1;
            this.rdoUchiwake.Tag = "CHANGE";
            this.rdoUchiwake.Text = "科目内訳";
            this.rdoUchiwake.UseVisualStyleBackColor = false;
            // 
            // rdoGokei
            // 
            this.rdoGokei.AutoSize = true;
            this.rdoGokei.BackColor = System.Drawing.Color.Silver;
            this.rdoGokei.Checked = true;
            this.rdoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoGokei.Location = new System.Drawing.Point(151, 9);
            this.rdoGokei.Margin = new System.Windows.Forms.Padding(4);
            this.rdoGokei.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoGokei.Name = "rdoGokei";
            this.rdoGokei.Size = new System.Drawing.Size(90, 24);
            this.rdoGokei.TabIndex = 0;
            this.rdoGokei.TabStop = true;
            this.rdoGokei.Tag = "CHANGE";
            this.rdoGokei.Text = "科目合計";
            this.rdoGokei.UseVisualStyleBackColor = false;
            // 
            // rdoNashi
            // 
            this.rdoNashi.AutoSize = true;
            this.rdoNashi.BackColor = System.Drawing.Color.Silver;
            this.rdoNashi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoNashi.Location = new System.Drawing.Point(249, 9);
            this.rdoNashi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoNashi.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoNashi.Name = "rdoNashi";
            this.rdoNashi.Size = new System.Drawing.Size(58, 24);
            this.rdoNashi.TabIndex = 1;
            this.rdoNashi.Tag = "CHANGE";
            this.rdoNashi.Text = "無し";
            this.rdoNashi.UseVisualStyleBackColor = false;
            // 
            // rdoAri
            // 
            this.rdoAri.AutoSize = true;
            this.rdoAri.BackColor = System.Drawing.Color.Silver;
            this.rdoAri.Checked = true;
            this.rdoAri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoAri.Location = new System.Drawing.Point(151, 9);
            this.rdoAri.Margin = new System.Windows.Forms.Padding(4);
            this.rdoAri.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoAri.Name = "rdoAri";
            this.rdoAri.Size = new System.Drawing.Size(58, 24);
            this.rdoAri.TabIndex = 0;
            this.rdoAri.TabStop = true;
            this.rdoAri.Tag = "CHANGE";
            this.rdoAri.Text = "有り";
            this.rdoAri.UseVisualStyleBackColor = false;
            // 
            // lblKanjoKamokuNmTo
            // 
            this.lblKanjoKamokuNmTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuNmTo.Location = new System.Drawing.Point(510, 10);
            this.lblKanjoKamokuNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuNmTo.Name = "lblKanjoKamokuNmTo";
            this.lblKanjoKamokuNmTo.Size = new System.Drawing.Size(239, 24);
            this.lblKanjoKamokuNmTo.TabIndex = 4;
            this.lblKanjoKamokuNmTo.Tag = "DISPNAME";
            this.lblKanjoKamokuNmTo.Text = "最　後";
            this.lblKanjoKamokuNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCdTo
            // 
            this.txtKanjoKamokuCdTo.AutoSizeFromLength = true;
            this.txtKanjoKamokuCdTo.DisplayLength = null;
            this.txtKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokuCdTo.Location = new System.Drawing.Point(462, 11);
            this.txtKanjoKamokuCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamokuCdTo.MaxLength = 4;
            this.txtKanjoKamokuCdTo.Name = "txtKanjoKamokuCdTo";
            this.txtKanjoKamokuCdTo.Size = new System.Drawing.Size(44, 23);
            this.txtKanjoKamokuCdTo.TabIndex = 3;
            this.txtKanjoKamokuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanjoKamokuCdTo_KeyDown);
            this.txtKanjoKamokuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdTo_Validating);
            // 
            // lblKanjoKamokuNmFr
            // 
            this.lblKanjoKamokuNmFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuNmFr.Location = new System.Drawing.Point(199, 10);
            this.lblKanjoKamokuNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuNmFr.Name = "lblKanjoKamokuNmFr";
            this.lblKanjoKamokuNmFr.Size = new System.Drawing.Size(236, 24);
            this.lblKanjoKamokuNmFr.TabIndex = 1;
            this.lblKanjoKamokuNmFr.Tag = "DISPNAME";
            this.lblKanjoKamokuNmFr.Text = "先　頭";
            this.lblKanjoKamokuNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCdFr
            // 
            this.txtKanjoKamokuCdFr.AutoSizeFromLength = true;
            this.txtKanjoKamokuCdFr.DisplayLength = null;
            this.txtKanjoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokuCdFr.Location = new System.Drawing.Point(151, 11);
            this.txtKanjoKamokuCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamokuCdFr.MaxLength = 4;
            this.txtKanjoKamokuCdFr.Name = "txtKanjoKamokuCdFr";
            this.txtKanjoKamokuCdFr.Size = new System.Drawing.Size(44, 23);
            this.txtKanjoKamokuCdFr.TabIndex = 0;
            this.txtKanjoKamokuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdFr_Validating);
            // 
            // lblBt
            // 
            this.lblBt.BackColor = System.Drawing.Color.Silver;
            this.lblBt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBt.Location = new System.Drawing.Point(437, 10);
            this.lblBt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBt.Name = "lblBt";
            this.lblBt.Size = new System.Drawing.Size(20, 24);
            this.lblBt.TabIndex = 2;
            this.lblBt.Tag = "CHANGE";
            this.lblBt.Text = "～";
            this.lblBt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(762, 153);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.lblKanjoKamokuNmTo);
            this.fsiPanel3.Controls.Add(this.txtKanjoKamokuCdTo);
            this.fsiPanel3.Controls.Add(this.txtKanjoKamokuCdFr);
            this.fsiPanel3.Controls.Add(this.lblBt);
            this.fsiPanel3.Controls.Add(this.lblKanjoKamokuNmFr);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 104);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(754, 45);
            this.fsiPanel3.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(754, 45);
            this.label3.TabIndex = 3;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "勘定科目コード範囲";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.rdoAri);
            this.fsiPanel2.Controls.Add(this.rdoNashi);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 54);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(754, 43);
            this.fsiPanel2.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(754, 43);
            this.label2.TabIndex = 2;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "発生額ゼロの印字";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.rdoGokei);
            this.fsiPanel1.Controls.Add(this.rdoUchiwake);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(754, 43);
            this.fsiPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(754, 43);
            this.label1.TabIndex = 1;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "明細出力方法";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMCE1013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 331);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCE1013";
            this.Text = "期首残高の印刷";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton rdoUchiwake;
        private System.Windows.Forms.RadioButton rdoGokei;
        private System.Windows.Forms.RadioButton rdoNashi;
        private System.Windows.Forms.RadioButton rdoAri;
        private System.Windows.Forms.Label lblBt;
        private System.Windows.Forms.Label lblKanjoKamokuNmTo;
        private common.controls.FsiTextBox txtKanjoKamokuCdTo;
        private System.Windows.Forms.Label lblKanjoKamokuNmFr;
        private common.controls.FsiTextBox txtKanjoKamokuCdFr;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label3;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label1;
    }
}