﻿namespace jp.co.fsi.zm.zmce1011
{
    static class Const
    {
        /// <summary>
        /// 補助科目有無
        /// </summary>
        public const int HOJO_UMU_NASHI = 0;
        public const int HOJO_UMU_ARI = 1;

        /// <summary>
        /// 補助使用区分
        /// </summary>
        public const int HOJO_SHIYO_KUBUN_ZAM = 1;　// 財務補助科目
        public const int HOJO_SHIYO_KUBUN_TRI = 2;  // 取引先

        /// <summary>
        /// 部門有無
        /// </summary>
        public const int BUMON_UMU_NASHI = 0;
        public const int BUMON_UMU_ARI = 1;

    }
}
