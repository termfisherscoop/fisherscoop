﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.zm.zmce1011
{
    /// <summary>
    /// ZMCE1011R の帳票
    /// </summary>
    public partial class ZMCE1011R : BaseReport
    {
        public ZMCE1011R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
