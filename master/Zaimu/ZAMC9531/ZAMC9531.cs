﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zam.zamc9531
{
    /// <summary>
    /// 仕訳事例検索(ZAMC9531)
    /// </summary>
    public partial class ZAMC9531 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        /// <summary>
        /// モード(コード検索：自動検索)　～該当データなしは直ちに終了、１件のみは自動選択
        /// </summary>
        private const string MODE_CD_AUTO = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZAMC9531()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // コード検索画面としての挙動をする
            // サイズを縮める
            this.Size = new Size(400, 500);
            // Escapeのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.ShowFButton = true;

            // データ表示
            SearchData(true);

        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // ダイアログとしての処理結果を返却する
            this.DialogResult = DialogResult.Cancel;

            base.PressEsc();
        }

        #endregion

        #region イベント

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 呼出元からのパラメータ：摘要コード、出納勘定科目コード
            string[] args = new string[2];

            if (this.InData == null)
            {
                args[0] = "-1";
            }
            else
            {
                switch (((string[])this.InData).Length)
                {
                    case 1:
                        args[0] = ((string[])this.InData)[0];
                        break;
                    case 2:
                        args[0] = ((string[])this.InData)[0];
                        args[1] = ((string[])this.InData)[1];
                        break;
                }
            }

            // 仕訳マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(args[0]));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            string sql;
            if (ValChk.IsEmpty(args[1]))
            {
                sql = "SELECT DISTINCT SHIWAKE_CD AS 仕訳コード, SHIWAKE_NM AS 仕訳名"
                            + " FROM TB_ZM_SHIWAKE_JIREI"
                            + " WHERE KAISHA_CD = @KAISHA_CD"
                            + " AND TEKIYO_CD = @TEKIYO_CD"
                            + " AND KAIKEI_NENDO = @KAIKEI_NENDO"
                            + " ORDER BY SHIWAKE_CD ASC";
            }
            else
            {
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4, args[1]);

                sql = "SELECT DISTINCT SHIWAKE_CD AS 仕訳コード, SHIWAKE_NM AS 仕訳名"
                            + " FROM TB_ZM_SHIWAKE_JIREI"
                            + " WHERE KAISHA_CD = @KAISHA_CD"
                            + " AND TEKIYO_CD = @TEKIYO_CD"
                            + " AND KAIKEI_NENDO = @KAIKEI_NENDO"
                            + " AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD"
                            + " AND TAISHAKU_KUBUN = 1"
                            + " GROUP BY SHIWAKE_CD, SHIWAKE_NM"
                            + " HAVING MAX(GYO_BANGO) = 1"
                            + " ORDER BY SHIWAKE_CD ASC";
            }
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql, dpc);

            // 該当データが存在しない時はCANCELで終了
            if (dt.Rows.Count == 0)
            {
                // 勘定科目コードが指定されていない場合は確認メッセージ表示
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    Msg.Info("該当データがありません。");
                }

                // 終了
                this.PressEsc();
            }
            // 該当データが１件の場合、自動検索の場合は１件を選択終了
            else if (dt.Rows.Count == 1 && MODE_CD_AUTO.Equals(this.Par1))
            {
                DataRow dr = dt.Rows[0];
                this.OutData = new string[2] { 
                Util.ToString(dr["仕訳コード"]),
                Util.ToString(dr["仕訳名"])
                };
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                this.dgvList.DataSource = dt;

                // ユーザーによるソートを禁止させる
                foreach (DataGridViewColumn c in this.dgvList.Columns)
                    c.SortMode = DataGridViewColumnSortMode.NotSortable;

                // フォントを設定する
                this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
                this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

                // 列幅を設定する
                this.dgvList.Columns[0].Width = 110;
                this.dgvList.Columns[1].Width = 200;
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[2] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仕訳コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仕訳名"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
