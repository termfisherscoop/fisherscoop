﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;
using jp.co.fsi.common.forms;

using ClosedXML.Excel;

namespace jp.co.fsi.zm.zmmr1071
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1071PR
    {
        #region private変数
        /// <summary>
        /// ZAMR2031(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMMR1071 _pForm;

        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// ユニークID
        /// </summary>
        string _unqId;


        private ZMMR1071DA _da;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMMR1071PR(UserInfo uInfo, DbAccess dba, ConfigLoader config, string unuqId, ZMMR1071 frm)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
            this._unqId = unuqId;
            this._pForm = frm;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        public bool DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            if (!DispSetKikan())
            {
                return false;
            }

            bool dataFlag;
            ZMMR1074 msgFrm = new ZMMR1074();
            try
            {
                // 処理中メッセージ表示
                msgFrm.Show();
                msgFrm.Refresh();

                this._dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    #region 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.AppendLine("  ITEM01");
                    cols.AppendLine(" ,ITEM02");
                    cols.AppendLine(" ,ITEM03");
                    cols.AppendLine(" ,ITEM04");
                    cols.AppendLine(" ,ITEM05");
                    cols.AppendLine(" ,ITEM06");
                    cols.AppendLine(" ,ITEM07");
                    cols.AppendLine(" ,ITEM08");
                    cols.AppendLine(" ,ITEM09");
                    cols.AppendLine(" ,ITEM10");
                    cols.AppendLine(" ,ITEM11");
                    cols.AppendLine(" ,ITEM12");
                    cols.AppendLine(" ,ITEM13");
                    cols.AppendLine(" ,ITEM14");
                    cols.AppendLine(" ,ITEM15");
                    cols.AppendLine(" ,ITEM16");
                    cols.AppendLine(" ,ITEM17");
                    cols.AppendLine(" ,ITEM18");
                    cols.AppendLine(" ,ITEM19");
                    cols.AppendLine(" ,ITEM20");
                    cols.AppendLine(" ,ITEM21");
                    cols.AppendLine(" ,ITEM22");
                    cols.AppendLine(" ,ITEM23");
                    cols.AppendLine(" ,ITEM24");
                    cols.AppendLine(" ,ITEM25");
                    cols.AppendLine(" ,ITEM26");
                    cols.AppendLine(" ,ITEM27");
                    cols.AppendLine(" ,ITEM28");
                    cols.AppendLine(" ,ITEM29");
                    cols.AppendLine(" ,ITEM30");
                    cols.AppendLine(" ,ITEM31");
                    cols.AppendLine(" ,ITEM32");
                    cols.AppendLine(" ,ITEM33");
                    cols.AppendLine(" ,ITEM34");
                    cols.AppendLine(" ,ITEM35");
                    cols.AppendLine(" ,ITEM36");
                    cols.AppendLine(" ,ITEM37");
                    cols.AppendLine(" ,ITEM38");
                    cols.AppendLine(" ,ITEM39");
                    cols.AppendLine(" ,ITEM40");
                    cols.AppendLine(" ,ITEM41");
                    cols.AppendLine(" ,ITEM42");
                    cols.AppendLine(" ,ITEM43");
                    cols.AppendLine(" ,ITEM44");
                    cols.AppendLine(" ,ITEM45");
                    cols.AppendLine(" ,ITEM46");
                    cols.AppendLine(" ,ITEM47");
                    cols.AppendLine(" ,ITEM48");
                    cols.AppendLine(" ,ITEM49");
                    cols.AppendLine(" ,ITEM50");
                    cols.AppendLine(" ,ITEM51");
                    cols.AppendLine(" ,ITEM52");
                    cols.AppendLine(" ,ITEM53");
                    cols.AppendLine(" ,ITEM54");
                    cols.AppendLine(" ,ITEM55");
                    cols.AppendLine(" ,ITEM56");
                    cols.AppendLine(" ,ITEM57");
                    cols.AppendLine(" ,ITEM58");
                    cols.AppendLine(" ,ITEM59");
                    cols.AppendLine(" ,ITEM60");
                    cols.AppendLine(" ,ITEM61");
                    cols.AppendLine(" ,ITEM62");
                    cols.AppendLine(" ,ITEM63");
                    cols.AppendLine(" ,ITEM64");
                    cols.AppendLine(" ,ITEM65");
                    cols.AppendLine(" ,ITEM66");
                    cols.AppendLine(" ,ITEM67");
                    cols.AppendLine(" ,ITEM68");
                    cols.AppendLine(" ,ITEM69");
                    cols.AppendLine(" ,ITEM70");
                    cols.AppendLine(" ,ITEM71");
                    cols.AppendLine(" ,ITEM72");
                    cols.AppendLine(" ,ITEM73");
                    cols.AppendLine(" ,ITEM74");
                    cols.AppendLine(" ,ITEM75");
                    cols.AppendLine(" ,ITEM76");
                    cols.AppendLine(" ,ITEM77");
                    cols.AppendLine(" ,ITEM78");
                    cols.AppendLine(" ,ITEM79");
                    cols.AppendLine(" ,ITEM80");
                    cols.AppendLine(" ,ITEM81");
                    cols.AppendLine(" ,ITEM82");
                    cols.AppendLine(" ,ITEM83");
                    cols.AppendLine(" ,ITEM84");
                    cols.AppendLine(" ,ITEM85");
                    cols.AppendLine(" ,ITEM86");
                    cols.AppendLine(" ,ITEM87");
                    cols.AppendLine(" ,ITEM88");
                    cols.AppendLine(" ,ITEM89");
                    cols.AppendLine(" ,ITEM90");
                    cols.AppendLine(" ,ITEM91");
                    cols.AppendLine(" ,ITEM92");
                    cols.AppendLine(" ,ITEM93");
                    cols.AppendLine(" ,ITEM94");
                    cols.AppendLine(" ,ITEM95");
                    cols.AppendLine(" ,ITEM96");
                    cols.AppendLine(" ,ITEM97");
                    cols.AppendLine(" ,ITEM98");
                    cols.AppendLine(" ,ITEM99");
                    cols.AppendLine(" ,ITEM100");
                    cols.AppendLine(" ,ITEM101");
                    cols.AppendLine(" ,ITEM102");
                    cols.AppendLine(" ,ITEM103");
                    cols.AppendLine(" ,ITEM104");
                    cols.AppendLine(" ,ITEM105");
                    cols.AppendLine(" ,ITEM106");
                    cols.AppendLine(" ,ITEM107");
                    cols.AppendLine(" ,ITEM108");
                    cols.AppendLine(" ,ITEM109");
                    cols.AppendLine(" ,ITEM110");
                    cols.AppendLine(" ,ITEM111");
                    cols.AppendLine(" ,ITEM112");
                    cols.AppendLine(" ,ITEM113");
                    cols.AppendLine(" ,ITEM114");
                    cols.AppendLine(" ,ITEM115");
                    cols.AppendLine(" ,ITEM116");
                    cols.AppendLine(" ,ITEM117");
                    cols.AppendLine(" ,ITEM118");
                    cols.AppendLine(" ,ITEM119");
                    cols.AppendLine(" ,ITEM120");
                    cols.AppendLine(" ,ITEM121");
                    cols.AppendLine(" ,ITEM122");
                    cols.AppendLine(" ,ITEM123");
                    cols.AppendLine(" ,ITEM124");
                    cols.AppendLine(" ,ITEM125");
                    cols.AppendLine(" ,ITEM126");
                    cols.AppendLine(" ,ITEM127");
                    cols.AppendLine(" ,ITEM128");
                    cols.AppendLine(" ,ITEM129");
                    cols.AppendLine(" ,ITEM130");
                    cols.AppendLine(" ,ITEM131");
                    cols.AppendLine(" ,ITEM132");
                    cols.AppendLine(" ,ITEM133");
                    cols.AppendLine(" ,ITEM134");
                    cols.AppendLine(" ,ITEM135");
                    cols.AppendLine(" ,ITEM136");
                    cols.AppendLine(" ,ITEM137");
                    cols.AppendLine(" ,ITEM138");
                    cols.AppendLine(" ,ITEM139");
                    cols.AppendLine(" ,ITEM140");
                    cols.AppendLine(" ,ITEM141");
                    cols.AppendLine(" ,ITEM142");
                    cols.AppendLine(" ,ITEM143");
                    cols.AppendLine(" ,ITEM144");
                    cols.AppendLine(" ,ITEM145");
                    cols.AppendLine(" ,ITEM146");
                    cols.AppendLine(" ,ITEM147");
                    cols.AppendLine(" ,ITEM148");
                    cols.AppendLine(" ,ITEM149");
                    cols.AppendLine(" ,ITEM150");
                    cols.AppendLine(" ,ITEM151");
                    cols.AppendLine(" ,ITEM152");
                    cols.AppendLine(" ,ITEM153");
                    cols.AppendLine(" ,ITEM154");
                    cols.AppendLine(" ,ITEM155");
                    cols.AppendLine(" ,ITEM156");
                    cols.AppendLine(" ,ITEM157");
                    cols.AppendLine(" ,ITEM158");
                    cols.AppendLine(" ,ITEM159");
                    cols.AppendLine(" ,ITEM160");
                    cols.AppendLine(" ,ITEM161");
                    cols.AppendLine(" ,ITEM162");
                    cols.AppendLine(" ,ITEM163");
                    cols.AppendLine(" ,ITEM164");
                    cols.AppendLine(" ,ITEM165");
                    cols.AppendLine(" ,ITEM166");
                    cols.AppendLine(" ,ITEM167");
                    cols.AppendLine(" ,ITEM168");
                    cols.AppendLine(" ,ITEM169");
                    cols.AppendLine(" ,ITEM170");
                    cols.AppendLine(" ,ITEM171");
                    cols.AppendLine(" ,ITEM172");
                    cols.AppendLine(" ,ITEM173");
                    cols.AppendLine(" ,ITEM174");
                    cols.AppendLine(" ,ITEM175");
                    cols.AppendLine(" ,ITEM176");
                    cols.AppendLine(" ,ITEM177");
                    cols.AppendLine(" ,ITEM178");
                    cols.AppendLine(" ,ITEM179");
                    cols.AppendLine(" ,ITEM180");
                    cols.AppendLine(" ,ITEM181");
                    cols.AppendLine(" ,ITEM182");
                    cols.AppendLine(" ,ITEM183");
                    cols.AppendLine(" ,ITEM184");
                    cols.AppendLine(" ,ITEM185");
                    cols.AppendLine(" ,ITEM186");
                    cols.AppendLine(" ,ITEM187");
                    cols.AppendLine(" ,ITEM188");
                    cols.AppendLine(" ,ITEM189");
                    cols.AppendLine(" ,ITEM190");
                    cols.AppendLine(" ,ITEM191");
                    cols.AppendLine(" ,ITEM192");
                    cols.AppendLine(" ,ITEM193");
                    cols.AppendLine(" ,ITEM194");
                    cols.AppendLine(" ,ITEM195");
                    cols.AppendLine(" ,ITEM196");
                    cols.AppendLine(" ,ITEM197");
                    cols.AppendLine(" ,ITEM198");
                    cols.AppendLine(" ,ITEM199");
                    cols.AppendLine(" ,ITEM200");
                    cols.AppendLine(" ,ITEM201");
                    cols.AppendLine(" ,ITEM202");
                    cols.AppendLine(" ,ITEM203");
                    cols.AppendLine(" ,ITEM204");
                    cols.AppendLine(" ,ITEM205");
                    cols.AppendLine(" ,ITEM206");
                    cols.AppendLine(" ,ITEM207");
                    cols.AppendLine(" ,ITEM208");
                    cols.AppendLine(" ,ITEM209");
                    cols.AppendLine(" ,ITEM210");
                    cols.AppendLine(" ,ITEM211");
                    cols.AppendLine(" ,ITEM212");
                    cols.AppendLine(" ,ITEM213");
                    cols.AppendLine(" ,ITEM214");
                    cols.AppendLine(" ,ITEM215");
                    cols.AppendLine(" ,ITEM216");
                    cols.AppendLine(" ,ITEM217");
                    cols.AppendLine(" ,ITEM218");
                    cols.AppendLine(" ,ITEM219");
                    cols.AppendLine(" ,ITEM220");
                    cols.AppendLine(" ,ITEM221");
                    cols.AppendLine(" ,ITEM222");
                    cols.AppendLine(" ,ITEM223");
                    cols.AppendLine(" ,ITEM224");
                    cols.AppendLine(" ,ITEM225");
                    cols.AppendLine(" ,ITEM226");
                    cols.AppendLine(" ,ITEM227");
                    cols.AppendLine(" ,ITEM228");
                    cols.AppendLine(" ,ITEM229");
                    cols.AppendLine(" ,ITEM230");
                    cols.AppendLine(" ,ITEM231");
                    cols.AppendLine(" ,ITEM232");
                    cols.AppendLine(" ,ITEM233");
                    cols.AppendLine(" ,ITEM234");
                    cols.AppendLine(" ,ITEM235");
                    cols.AppendLine(" ,ITEM236");
                    cols.AppendLine(" ,ITEM237");
                    cols.AppendLine(" ,ITEM238");
                    cols.AppendLine(" ,ITEM239");
                    cols.AppendLine(" ,ITEM240");
                    cols.AppendLine(" ,ITEM241");
                    cols.AppendLine(" ,ITEM242");
                    cols.AppendLine(" ,ITEM243");
                    cols.AppendLine(" ,ITEM244");
                    cols.AppendLine(" ,ITEM245");
                    cols.AppendLine(" ,ITEM246");
                    cols.AppendLine(" ,ITEM247");
                    cols.AppendLine(" ,ITEM248");
                    cols.AppendLine(" ,ITEM249");
                    cols.AppendLine(" ,ITEM250");
                    cols.AppendLine(" ,ITEM251");
                    cols.AppendLine(" ,ITEM252");
                    cols.AppendLine(" ,ITEM253");
                    cols.AppendLine(" ,ITEM254");
                    cols.AppendLine(" ,ITEM255");
                    cols.AppendLine(" ,ITEM256");
                    cols.AppendLine(" ,ITEM257");
                    cols.AppendLine(" ,ITEM258");
                    cols.AppendLine(" ,ITEM259");
                    cols.AppendLine(" ,ITEM260");
                    cols.AppendLine(" ,ITEM261");
                    cols.AppendLine(" ,ITEM262");
                    cols.AppendLine(" ,ITEM263");
                    cols.AppendLine(" ,ITEM264");
                    cols.AppendLine(" ,ITEM265");
                    cols.AppendLine(" ,ITEM266");
                    cols.AppendLine(" ,ITEM267");
                    cols.AppendLine(" ,ITEM268");
                    cols.AppendLine(" ,ITEM269");
                    cols.AppendLine(" ,ITEM270");
                    cols.AppendLine(" ,ITEM271");
                    cols.AppendLine(" ,ITEM272");
                    cols.AppendLine(" ,ITEM273");
                    cols.AppendLine(" ,ITEM274");
                    cols.AppendLine(" ,ITEM275");
                    cols.AppendLine(" ,ITEM276");
                    cols.AppendLine(" ,ITEM277");
                    cols.AppendLine(" ,ITEM278");
                    cols.AppendLine(" ,ITEM279");
                    cols.AppendLine(" ,ITEM280");
                    cols.AppendLine(" ,ITEM281");
                    cols.AppendLine(" ,ITEM282");
                    cols.AppendLine(" ,ITEM283");
                    cols.AppendLine(" ,ITEM284");
                    cols.AppendLine(" ,ITEM285");
                    cols.AppendLine(" ,ITEM286");
                    cols.AppendLine(" ,ITEM287");
                    cols.AppendLine(" ,ITEM288");
                    cols.AppendLine(" ,ITEM289");
                    cols.AppendLine(" ,ITEM290");
                    cols.AppendLine(" ,ITEM291");
                    cols.AppendLine(" ,ITEM292");
                    cols.AppendLine(" ,ITEM293");
                    cols.AppendLine(" ,ITEM294");
                    cols.AppendLine(" ,ITEM295");
                    cols.AppendLine(" ,ITEM296");
                    cols.AppendLine(" ,ITEM297");
                    cols.AppendLine(" ,ITEM298");
                    cols.AppendLine(" ,ITEM299");
                    cols.AppendLine(" ,ITEM300");
                    cols.AppendLine(" ,ITEM301");
                    cols.AppendLine(" ,ITEM302");
                    cols.AppendLine(" ,ITEM303");
                    cols.AppendLine(" ,ITEM304");
                    cols.AppendLine(" ,ITEM305");
                    cols.AppendLine(" ,ITEM306");
                    cols.AppendLine(" ,ITEM307");
                    cols.AppendLine(" ,ITEM308");
                    cols.AppendLine(" ,ITEM309");
                    cols.AppendLine(" ,ITEM310");
                    cols.AppendLine(" ,ITEM311");
                    cols.AppendLine(" ,ITEM312");
                    cols.AppendLine(" ,ITEM313");
                    cols.AppendLine(" ,ITEM314");
                    cols.AppendLine(" ,ITEM315");
                    cols.AppendLine(" ,ITEM316");
                    cols.AppendLine(" ,ITEM317");
                    cols.AppendLine(" ,ITEM318");
                    cols.AppendLine(" ,ITEM319");
                    cols.AppendLine(" ,ITEM320");
                    cols.AppendLine(" ,ITEM321");
                    cols.AppendLine(" ,ITEM322");
                    cols.AppendLine(" ,ITEM323");
                    cols.AppendLine(" ,ITEM324");
                    cols.AppendLine(" ,ITEM325");
                    cols.AppendLine(" ,ITEM326");
                    cols.AppendLine(" ,ITEM327");
                    cols.AppendLine(" ,ITEM328");
                    cols.AppendLine(" ,ITEM329");
                    cols.AppendLine(" ,ITEM330");
                    cols.AppendLine(" ,ITEM331");
                    cols.AppendLine(" ,ITEM332");
                    cols.AppendLine(" ,ITEM333");
                    cols.AppendLine(" ,ITEM334");
                    cols.AppendLine(" ,ITEM335");
                    cols.AppendLine(" ,ITEM336");
                    cols.AppendLine(" ,ITEM337");
                    cols.AppendLine(" ,ITEM338");
                    cols.AppendLine(" ,ITEM339");
                    cols.AppendLine(" ,ITEM340");
                    cols.AppendLine(" ,ITEM341");
                    cols.AppendLine(" ,ITEM342");
                    cols.AppendLine(" ,ITEM343");
                    cols.AppendLine(" ,ITEM344");
                    cols.AppendLine(" ,ITEM345");
                    cols.AppendLine(" ,ITEM346");
                    cols.AppendLine(" ,ITEM347");
                    cols.AppendLine(" ,ITEM348");
                    cols.AppendLine(" ,ITEM349");
                    cols.AppendLine(" ,ITEM350");
                    cols.AppendLine(" ,ITEM351");
                    cols.AppendLine(" ,ITEM352");
                    cols.AppendLine(" ,ITEM353");
                    cols.AppendLine(" ,ITEM354");
                    cols.AppendLine(" ,ITEM355");
                    cols.AppendLine(" ,ITEM356");
                    cols.AppendLine(" ,ITEM357");
                    cols.AppendLine(" ,ITEM358");
                    cols.AppendLine(" ,ITEM359");
                    cols.AppendLine(" ,ITEM360");
                    cols.AppendLine(" ,ITEM361");
                    cols.AppendLine(" ,ITEM362");
                    cols.AppendLine(" ,ITEM363");
                    cols.AppendLine(" ,ITEM364");
                    cols.AppendLine(" ,ITEM365");
                    cols.AppendLine(" ,ITEM366");
                    cols.AppendLine(" ,ITEM367");
                    cols.AppendLine(" ,ITEM368");
                    cols.AppendLine(" ,ITEM369");
                    cols.AppendLine(" ,ITEM370");
                    cols.AppendLine(" ,ITEM371");
                    cols.AppendLine(" ,ITEM372");
                    cols.AppendLine(" ,ITEM373");
                    cols.AppendLine(" ,ITEM374");
                    cols.AppendLine(" ,ITEM375");
                    cols.AppendLine(" ,ITEM376");
                    cols.AppendLine(" ,ITEM377");
                    // 名護へ合わせ
                    cols.AppendLine(" ,ITEM378");
                    cols.AppendLine(" ,ITEM379");
                    cols.AppendLine(" ,ITEM380");
                    cols.AppendLine(" ,ITEM381");
                    cols.AppendLine(" ,ITEM382");
                    #endregion

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);

                    // データの取得
                    DataTable dtOutput = this._dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    ZMMR10711R rpt = null;
                    // 帳票オブジェクトをインスタンス化
                    if (!isExcel)
                    {
                        rpt = new ZMMR10711R(dtOutput);

                        rpt.Document.Printer.DocumentName = Util.ToString(this._pForm.Condition["ReportName"]);
                        rpt.Document.Name = Util.ToString(this._pForm.Condition["ReportName"]);
                    }

                    if (isExcel)
                    {
                        // エクセル出力はレポート機能を使わない方式

                        //GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        ////SetExcelSetting(xlsExport1);
                        //rpt.Run();
                        //string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                        //if (!ValChk.IsEmpty(saveFileName))
                        //{
                        //    xlsExport1.Export(rpt.Document, saveFileName);
                        //    Msg.InfoNm("EXCEL出力", "保存しました。");
                        //    Util.OpenFolder(saveFileName);
                        //}
                        if (dtOutput.Rows.Count > 0)
                        {
                            List<string> title = new List<string>();
                            title.Add("科　目　名"); // タイトル1
                            title.Add(Util.ToString(dtOutput.Rows[0]["ITEM06"])); // タイトル2
                            title.Add(Util.ToString(dtOutput.Rows[0]["ITEM07"])); // タイトル3
                            title.Add(Util.ToString(dtOutput.Rows[0]["ITEM08"])); // タイトル4
                            title.Add(Util.ToString(dtOutput.Rows[0]["ITEM09"])); // タイトル5
                            title.Add("当　期"); // タイトル6

                            string sheetName = "";
                            int docNo = 1;
                            var workbook = new XLWorkbook();
                            IXLWorksheet ws = null;
                            int lin = 1;
                            foreach (DataRow dr in dtOutput.Rows)
                            {
                                // 帳票タイトルでシート分け
                                if (sheetName != Util.ToString(dr["ITEM01"]))
                                {
                                    // 見出しの設定
                                    lin = 1;
                                    sheetName = "五期比較諸表" + docNo.ToString();
                                    ws = workbook.Worksheets.Add(sheetName);
                                    sheetName = Util.ToString(dr["ITEM01"]);
                                    docNo++;
                                    // タイトル
                                    ws.Cell(lin, 1).Value = sheetName;
                                    // 出力日
                                    ws.Cell(lin, 5).Value = Util.ToString(dr["ITEM17"]);
                                    lin++;
                                    // 部門条件
                                    ws.Cell(lin, 1).Value = Util.ToString(dr["ITEM02"]);
                                    // 日付最終
                                    ws.Cell(lin, 5).Value = Util.ToString(dr["ITEM03"]);
                                    lin++;
                                    // 会社名
                                    ws.Cell(lin, 1).Value = Util.ToString(dr["ITEM04"]);
                                    // 消費税処理
                                    ws.Cell(lin, 5).Value = Util.ToString(dr["ITEM05"]);
                                    lin++;
                                    for (int i = 0; i < title.Count; i++)
                                    {
                                        ws.Cell(lin, (i + 1)).Value = title[i];
                                    }
                                    lin++;
                                }

                                // 明細部の設定

                                // 科目名
                                if (Util.ToString(dr["ITEM18"]) == "1")
                                    ws.Cell(lin, 1).Style.Font.Bold = true;
                                ws.Cell(lin, 1).Value = Util.ToString(dr["ITEM10"]);
                                // 金額１
                                ws.Cell(lin, 2).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 2).Value = Util.ToString(dr["ITEM11"]);
                                // 金額２
                                ws.Cell(lin, 3).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 3).Value = Util.ToString(dr["ITEM12"]);
                                // 金額３
                                ws.Cell(lin, 4).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 4).Value = Util.ToString(dr["ITEM13"]);
                                // 金額４
                                ws.Cell(lin, 5).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 5).Value = Util.ToString(dr["ITEM14"]);
                                // 金額５
                                ws.Cell(lin, 6).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 6).Value = Util.ToString(dr["ITEM15"]);

                                lin++;
                            }

                            // 保存先の指定
                            string saveFileName = GetSavePath("五期比較諸表", 2);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                workbook.SaveAs(saveFileName);

                                Msg.InfoNm("EXCEL出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this._unqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this._dba.Rollback();

                msgFrm.Close();
            }
            if (dataFlag)
                return true;
            else
                return false;
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 保持用データをセット
        /// </summary>
        private Boolean DispSetKikan()
        {
            // 該当会計年度の会計期間開始日を取得
            DataTable dt = Util.GetKaikeiKikan(this._uInfo.KaikeiNendo, this._dba);
   
            if (dt.Rows.Count == 0)
            {
                Msg.Info("該当会計年度の情報が取得できませんでした。");
                return false;
            }

            //this._dtKikanKaishibi = Util.ToDate(dt.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]);

            return true;
        }

        #region コメント
        ///// <summary>
        ///// データを格納
        ///// </summary>
        //private void DataSet()
        //{
        //    // 対象データテーブルにカラムを5列ずつ定義
        //    KanjoKamokuKingaku.Columns.Add("shukeiKeisanShiki", Type.GetType("System.String"));
        //    KanjoKamokuKingaku.Columns.Add("zandakaKingaku", Type.GetType("System.Int64"));
        //    KanjoKamokuKingaku.Columns.Add("karikataKingaku", Type.GetType("System.Int64"));
        //    KanjoKamokuKingaku.Columns.Add("kashikataKingaku", Type.GetType("System.Int64"));
        //    KanjoKamokuKingaku.Columns.Add("touzanKingaku", Type.GetType("System.Int64"));

        //    ZMMR1031DA da = new ZMMR1031DA(this._uInfo, this._dba, this._config);

        //    // F6:貸借対照表、F7:損益計算書、F8:製造原価の大項目名を取得
        //    this._dtTaishakuTaishohyo = da.GetKamokuDaiKomoku(LIST_F1);
        //    this._dtSonekiKeisansho = da.GetKamokuDaiKomoku(LIST_F2);
        //    this._dtSeizoGenka = da.GetKamokuDaiKomoku(LIST_F3);

        //    DataTable kanjoKamokuIchiran;
        //    DataTable kingakuData;
        //    DataRow row;
        //    int shukeiKubun;
        //    string kamokuNm;
        //    int kamokuBunrui;
        //    int meisaiKubun;
        //    int mojiShubetsu;
        //    int daiKomokuTaishakuKubun;
        //    int shoKomokuTaishakuKubun;
        //    string shukeiKeisanShiki;
        //    decimal zenZanKingaku;
        //    decimal karikataKingaku;
        //    decimal kashikataKingaku;
        //    decimal zandakaKingaku;
        //    int taishoNo;
        //    string taishoNm;
        //    int gyoBango;
        //    string jokenPlus = "";
        //    string jokenMinus = "";
        //    int i = 0;
        //    int j = 0;

        //    // 法定準備金の金額を保持
        //    // 金額データを取得
        //    DataTable dt = da.GetHoteJunbiki(kikanKaishibi, this._pForm.Condition);
        //    //if (dt.Rows.Count > 0)
        //    if (dt.Rows.Count > 0 && Util.ToInt(dt.Rows[0]["JUNBIKIN"]) > 0)
        //    {
        //        this._dtHoteJunbikin = Util.ToDecimal(dt.Rows[0]["JUNBIKIN"]);
        //    }
        //    // 当期未処分剰余金の金額を保持
        //    dt = da.GetTokiMishobunJoyokin(this._pForm.Condition);
        //    //if (dt.Rows.Count > 0)
        //    if (dt.Rows.Count > 0 && Util.ToDecimal(dt.Rows[0]["KINGAKU"]) != 0)
        //    {
        //        this._dtTokiMishobunJoyokin = Util.ToDecimal(dt.Rows[0]["KINGAKU"]);
        //    }

        //    // 貸借対照表、損益計算書、製造原価の大項目名毎の小項目データを取得し、格納
        //    #region 製造原価
        //    while (SeizoGenka.Rows.Count > i)
        //    {
        //        meisaiKubun = Util.ToInt(SeizoGenka.Rows[i]["MEISAI_KUBUN"]);
        //        // 明細区分が0以外のデータのみ処理
        //        if (meisaiKubun != 0)
        //        {
        //            kamokuNm = Util.ToString(SeizoGenka.Rows[i]["KAMOKU_BUNRUI_NM"]);
        //            shukeiKeisanShiki = Util.ToString(SeizoGenka.Rows[i]["SHUKEI_KEISANSHIKI"]);
        //            mojiShubetsu = Util.ToInt(SeizoGenka.Rows[i]["MOJI_SHUBETSU"]);
        //            daiKomokuTaishakuKubun = Util.ToInt(SeizoGenka.Rows[i]["TAISHAKU_KUBUN"]);
        //            kamokuBunrui = Util.ToInt(SeizoGenka.Rows[i]["KAMOKU_BUNRUI"]);
        //            kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F3, kamokuBunrui);

        //            // 小項目データを取得
        //            while (kanjoKamokuIchiran.Rows.Count > j)
        //            {
        //                zenZanKingaku = 0;
        //                karikataKingaku = 0;
        //                kashikataKingaku = 0;
        //                zandakaKingaku = 0;
        //                taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
        //                taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
        //                gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
        //                shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

        //                // 金額データを取得
        //                kingakuData = da.GetKamokuKingaku(taishoNo, this._pForm.Condition);
        //                if (kingakuData.Rows.Count != 0)
        //                {
        //                    zenZanKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZEN_ZAN"]);
        //                    karikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KARIKATA"]);
        //                    kashikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KASHIKATA"]);
        //                    zandakaKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZANDAKA"]);
        //                }

        //                // 金額をデータテーブルに格納
        //                row = KanjoKamokuKingaku.NewRow();
        //                row["shukeiKeisanShiki"] = shukeiKeisanShiki;
        //                // 小項目の貸借区分が貸方の場合、-1を掛ける
        //                if (shoKomokuTaishakuKubun == 1)
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku;
        //                    row["karikataKingaku"] = karikataKingaku;
        //                    row["kashikataKingaku"] = kashikataKingaku;
        //                    row["touzanKingaku"] = zandakaKingaku;
        //                }
        //                else
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku * -1;
        //                    row["karikataKingaku"] = karikataKingaku * -1;
        //                    row["kashikataKingaku"] = kashikataKingaku * -1;
        //                    row["touzanKingaku"] = zandakaKingaku * -1;
        //                }
        //                KanjoKamokuKingaku.Rows.Add(row);
        //                j++;
        //            }
        //        }
        //        j = 0;
        //        i++;
        //    }
        //    #endregion
        //    i = 0;
        //    #region 損益計算書
        //    while (SonekiKeisansho.Rows.Count > i)
        //    {
        //        shukeiKubun = Util.ToInt(SonekiKeisansho.Rows[i]["SHUKEI_KUBUN"]);
        //        //集計区分が2のデータのみ処理
        //        if (shukeiKubun == 2)
        //        {
        //            kamokuNm = Util.ToString(SonekiKeisansho.Rows[i]["KAMOKU_BUNRUI_NM"]);
        //            meisaiKubun = Util.ToInt(SonekiKeisansho.Rows[i]["MEISAI_KUBUN"]);
        //            shukeiKeisanShiki = Util.ToString(SonekiKeisansho.Rows[i]["SHUKEI_KEISANSHIKI"]);
        //            mojiShubetsu = Util.ToInt(SonekiKeisansho.Rows[i]["MOJI_SHUBETSU"]);
        //            daiKomokuTaishakuKubun = Util.ToInt(SonekiKeisansho.Rows[i]["TAISHAKU_KUBUN"]);
        //            kamokuBunrui = Util.ToInt(SonekiKeisansho.Rows[i]["KAMOKU_BUNRUI"]);
        //            kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F2, kamokuBunrui);

        //            // 小項目データを取得
        //            while (kanjoKamokuIchiran.Rows.Count > j)
        //            {
        //                zenZanKingaku = 0;
        //                karikataKingaku = 0;
        //                kashikataKingaku = 0;
        //                zandakaKingaku = 0;
        //                taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
        //                taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
        //                gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
        //                shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

        //                // 金額データを取得
        //                kingakuData = da.GetKamokuKingaku(taishoNo, this._pForm.Condition);
        //                if (kingakuData.Rows.Count != 0)
        //                {
        //                    zenZanKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZEN_ZAN"]);
        //                    karikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KARIKATA"]);
        //                    kashikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KASHIKATA"]);
        //                    zandakaKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZANDAKA"]);
        //                }

        //                // 金額をデータテーブルに格納
        //                row = KanjoKamokuKingaku.NewRow();
        //                row["shukeiKeisanShiki"] = shukeiKeisanShiki;
        //                // 小項目の貸借区分が貸方の場合、-1を掛ける
        //                if (shoKomokuTaishakuKubun == 1)
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku;
        //                    row["karikataKingaku"] = karikataKingaku;
        //                    row["kashikataKingaku"] = kashikataKingaku;
        //                    row["touzanKingaku"] = zandakaKingaku;
        //                }
        //                else
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku * -1;
        //                    row["karikataKingaku"] = karikataKingaku * -1;
        //                    row["kashikataKingaku"] = kashikataKingaku * -1;
        //                    row["touzanKingaku"] = zandakaKingaku * -1;
        //                }
        //                KanjoKamokuKingaku.Rows.Add(row);
        //                j++;
        //            }
        //        }
        //        j = 0;
        //        i++;
        //    }
        //    #endregion
        //    i = 0;
        //    #region 貸借対照表
        //    while (TaishakuTaishohyo.Rows.Count > i)
        //    {
        //        meisaiKubun = Util.ToInt(TaishakuTaishohyo.Rows[i]["MEISAI_KUBUN"]);
        //        kamokuBunrui = Util.ToInt(TaishakuTaishohyo.Rows[i]["KAMOKU_BUNRUI"]);
        //        kamokuNm = Util.ToString(TaishakuTaishohyo.Rows[i]["KAMOKU_BUNRUI_NM"]);
        //        shukeiKeisanShiki = Util.ToString(TaishakuTaishohyo.Rows[i]["SHUKEI_KEISANSHIKI"]);
        //        mojiShubetsu = Util.ToInt(TaishakuTaishohyo.Rows[i]["MOJI_SHUBETSU"]);
        //        daiKomokuTaishakuKubun = Util.ToInt(TaishakuTaishohyo.Rows[i]["TAISHAKU_KUBUN"]);
        //        // 明細区分が0以外のデータのみ処理
        //        if (meisaiKubun != 0)
        //        {
        //            kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F1, kamokuBunrui);

        //            // 小項目データを取得、格納
        //            while (kanjoKamokuIchiran.Rows.Count > j)
        //            {
        //                zenZanKingaku = 0;
        //                karikataKingaku = 0;
        //                kashikataKingaku = 0;
        //                zandakaKingaku = 0;
        //                taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
        //                taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
        //                gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
        //                shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

        //                // 金額データを取得
        //                kingakuData = da.GetKamokuKingaku(taishoNo, this._pForm.Condition);
        //                if (kingakuData.Rows.Count != 0)
        //                {
        //                    zenZanKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZEN_ZAN"]);
        //                    karikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KARIKATA"]);
        //                    kashikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KASHIKATA"]);
        //                    zandakaKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZANDAKA"]);
        //                }

        //                // 金額をデータテーブルに格納
        //                row = KanjoKamokuKingaku.NewRow();
        //                row["shukeiKeisanShiki"] = kamokuBunrui;
        //                // 小項目の貸借区分が貸方の場合、-1を掛ける
        //                if (shoKomokuTaishakuKubun == 1)
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku;
        //                    row["karikataKingaku"] = karikataKingaku;
        //                    row["kashikataKingaku"] = kashikataKingaku;
        //                    row["touzanKingaku"] = zandakaKingaku;
        //                }
        //                else
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku * -1;
        //                    row["karikataKingaku"] = karikataKingaku * -1;
        //                    row["kashikataKingaku"] = kashikataKingaku * -1;
        //                    row["touzanKingaku"] = zandakaKingaku * -1;
        //                }
        //                KanjoKamokuKingaku.Rows.Add(row);
        //                j++;
        //            }
        //        }
        //        // 明細区分が0で、科目分類が0以上のデータのみ処理
        //        if (meisaiKubun == 0 && kamokuBunrui > 0)
        //        {
        //            // 集計計算式にて計算
        //            ArrayList stTargetPlus = new ArrayList();
        //            ArrayList stTargetMinus = new ArrayList();
        //            object sumZandakaKingakuPlus;
        //            object sumKarikataKingakuPlus;
        //            object sumKashikataKingakuPlus;
        //            object sumTouzanKingakuPlus;
        //            object sumZandakaKingakuMinus;
        //            object sumKarikataKingakuMinus;
        //            object sumKashikataKingakuMinus;
        //            object sumTouzanKingakuMinus;
        //            int findPlus;
        //            int findMinus;
        //            int flag = 0;
        //            int first = 0;
        //            int plusMinusCheckFlag = 0;
        //            int deleteCount;

        //            #region 集計計算式の"+"と"-"を分別
        //            while (flag == 0)
        //            {
        //                // "+"と"-"の文字を検索
        //                findPlus = shukeiKeisanShiki.IndexOf("+");
        //                findMinus = shukeiKeisanShiki.IndexOf("-");
        //                // "+"も"-"も無ければ、処理終了
        //                if (findPlus <= 0 && findMinus <= 0)
        //                {
        //                    if (first == 0)
        //                    {
        //                        stTargetPlus.Add(shukeiKeisanShiki);
        //                    }
        //                    else
        //                    {
        //                        if (plusMinusCheckFlag == 0)
        //                        {
        //                            stTargetPlus.Add(shukeiKeisanShiki);
        //                        }
        //                        else if (plusMinusCheckFlag == 1)
        //                        {
        //                            stTargetMinus.Add(shukeiKeisanShiki);
        //                        }
        //                    }
        //                    flag = 1;
        //                }
        //                // "+"又は"-"があれば分けて格納
        //                else
        //                {
        //                    // 最初の処理 1つ目の項目を格納し、文字列から削除
        //                    if (first == 0)
        //                    {
        //                        // "+"の時の処理
        //                        if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
        //                        {
        //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
        //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
        //                        }
        //                        // "-"の時の処理
        //                        else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                        {
        //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
        //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
        //                        }
        //                    }
        //                    // 2つ目以降の処理 項目を順番に格納し、文字列から削除
        //                    else
        //                    {
        //                        // 削除する文字数を取得
        //                        if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                        {
        //                            deleteCount = findMinus;
        //                        }
        //                        else
        //                        {
        //                            deleteCount = findPlus;
        //                        }
        //                        // 項目を格納
        //                        if (plusMinusCheckFlag == 0)
        //                        {
        //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                        }
        //                        else if (plusMinusCheckFlag == 1)
        //                        {
        //                            stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                        }
        //                    }
        //                    // "+"と"-"の文字を検索
        //                    findPlus = shukeiKeisanShiki.IndexOf("+");
        //                    findMinus = shukeiKeisanShiki.IndexOf("-");
        //                    // 次の項目がが"+"か"-"なのかの判断フラグを設定
        //                    if (findPlus == 0)
        //                    {
        //                        plusMinusCheckFlag = 0;
        //                    }
        //                    else if (findMinus == 0)
        //                    {
        //                        plusMinusCheckFlag = 1;
        //                    }
        //                    // "+"又は"-"の文字を削除
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
        //                }
        //                first = 1;
        //            }
        //            #endregion

        //            #region 条件式の作成
        //            // "+"の項目の条件式を作成
        //            j = 0;
        //            while (stTargetPlus.Count > j)
        //            {
        //                if (j == 0)
        //                {
        //                    jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[j];
        //                }
        //                else
        //                {
        //                    jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[j];
        //                }

        //                j++;
        //            }
        //            // "-"の項目の条件式を作成
        //            j = 0;
        //            while (stTargetMinus.Count > j)
        //            {
        //                if (j == 0)
        //                {
        //                    jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[j];
        //                }
        //                else
        //                {
        //                    jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[j];
        //                }

        //                j++;
        //            }
        //            #endregion

        //            #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //            if (jokenPlus != "")
        //            {
        //                sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
        //                sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
        //                sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
        //                sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
        //            }
        //            else
        //            {
        //                sumZandakaKingakuPlus = 0;
        //                sumKarikataKingakuPlus = 0;
        //                sumKashikataKingakuPlus = 0;
        //                sumTouzanKingakuPlus = 0;
        //            }
        //            if (jokenMinus != "")
        //            {
        //                sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
        //                sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
        //                sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
        //                sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
        //            }
        //            else
        //            {
        //                sumZandakaKingakuMinus = 0;
        //                sumKarikataKingakuMinus = 0;
        //                sumKashikataKingakuMinus = 0;
        //                sumTouzanKingakuMinus = 0;
        //            }
        //            zenZanKingaku = Util.ToDecimal(sumZandakaKingakuPlus) + Util.ToDecimal(sumZandakaKingakuMinus);
        //            zandakaKingaku = Util.ToDecimal(sumTouzanKingakuPlus) + Util.ToDecimal(sumTouzanKingakuMinus);

        //            // 貸借区分が借方の場合
        //            if (daiKomokuTaishakuKubun == 1)
        //            {
        //                karikataKingaku = 0;
        //                kashikataKingaku = (zandakaKingaku + zenZanKingaku) * -1;
        //            }
        //            else
        //            {
        //                karikataKingaku = (zandakaKingaku - zenZanKingaku) * -1;
        //                kashikataKingaku = 0;
        //            }
        //            #endregion

        //            row = KanjoKamokuKingaku.NewRow();
        //            row["shukeiKeisanShiki"] = kamokuBunrui;
        //            row["zandakaKingaku"] = zenZanKingaku;
        //            row["karikataKingaku"] = karikataKingaku;
        //            row["kashikataKingaku"] = kashikataKingaku;
        //            row["touzanKingaku"] = zandakaKingaku;
        //            KanjoKamokuKingaku.Rows.Add(row);
        //        }
        //        j = 0;
        //        i++;
        //    }
        //    #endregion
        //}
        #endregion

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 期間
            string kikan = "自 ";
            string[] aryJpDateFr = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtFr"]), this._dba);
            kikan += aryJpDateFr[5];
            string[] aryJpDateTo = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtTo"]), this._dba);
            //kikan += " 至 " + aryJpDateTo[5];
            kikan = "至 " + aryJpDateTo[5];

            // 出力日付
            string[] aryJpDateOt = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["ShurutyokuDt"]), this._dba);
            string outPutDate = "出力日付 " + aryJpDateOt[5]; // Util.ToString(this._pForm.Condition["ShurutyokuDt"]);

            // 部門範囲を表示 ※開始と終了が未設定の場合は、【全社】
            string bumonRange;
            string bumonNmFr = Util.ToString(this._pForm.Condition["BumonNmFr"]);
            string bumonNmTo = Util.ToString(this._pForm.Condition["BumonNmTo"]);
            if (bumonNmFr == "先　頭" && bumonNmTo == "最　後")
            {
                bumonRange = "【全社】";
            }
            else
            {
                bumonRange = bumonNmFr + "　～　" + bumonNmTo;
            }

            // 残高タイトル
            // 現在設定されている会計年度を取得
            DateTime kaikeiNendoKaishiBi = Util.ToDate(this._uInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            // 取得したデータを和暦に変換
            string[] warekiKaikeiNendoKaishiBiDate = Util.ConvJpDate(kaikeiNendoKaishiBi, this._dba);
            // 期間の開始日付が設定会計年度開始日付と一致する場合、期首残高を設定
            string zandakaNm;
            if (aryJpDateFr[6] == warekiKaikeiNendoKaishiBiDate[6])
            {
                zandakaNm = "期首残高";
            }
            // 期間の開始日が1日の場合、前月残高を設定
            else if (aryJpDateFr[4] == "1")
            {
                zandakaNm = "前月残高";
            }
            // 期間の開始日付が上記以外だった場合、前日残高を設定
            else
            {
                zandakaNm = "前日残高";
            }

            // 集計処理
            this._da = new ZMMR1071DA(this._uInfo, this._dba, this._config);
            this._da.Summary(this._pForm.Condition, this._pForm.UnqId);

            // データ登録
            decimal dbSORT = 1;
            DataView dv = null;
            // 貸借対照
            dv = new DataView(this._da.TaishakuTaishohyo.Copy());
            dv.RowFilter = "KAMOKU_BUNRUI_NM <> ''";
            this.MakeWkData_insertTable(ref dbSORT, "五期比較諸表［貸借対照表］", kikan, bumonRange, outPutDate, zandakaNm, dv.ToTable().Copy());
            // 損益計算
            dv = new DataView(this._da.SonekiKeisansho.Copy());
            dv.RowFilter = "KAMOKU_BUNRUI_NM <> ''";
            this.MakeWkData_insertTable(ref dbSORT, "五期比較諸表［損益計算書］", kikan, bumonRange, outPutDate, zandakaNm, dv.ToTable().Copy());

            return true;
        }

        /// <summary>
        /// 印刷データの登録処理。
        /// </summary>
        private void MakeWkData_insertTable(ref decimal dbSORT, string title, string kikan, string bumonRange, string outPutDate, string zandakaNm, DataTable dt)
        {
            #region 前準備
            int kamokuBunrui;

            decimal zenZanKingaku1;
            decimal zenZanKingaku2;
            decimal zenZanKingaku3;

            decimal zenZanKingaku;
            decimal karikataKingaku;
            decimal kashikataKingaku;
            decimal zandakaKingaku;

            String tmpKanjoKamokuNm;
            String tmpZenZanKingaku1;
            String tmpZenZanKingaku2;
            String tmpZenZanKingaku3;
            String tmpZenZanKingaku;
            String tmpKarikataKingaku;
            String tmpKashikataKingaku;
            String tmpZandakaKingaku;
            String moji_shubetsu;
            List<string> nendo;
            #endregion

            if (dt.Rows.Count > 0)
            {
                // 年度タイトル設定
                nendo = new List<string>();
                nendo.Add(this._da.KAIKEI_NENDO1);
                nendo.Add(this._da.KAIKEI_NENDO2);
                nendo.Add(this._da.KAIKEI_NENDO3);
                nendo.Add(this._da.KAIKEI_NENDO4);
                string[] aryJpDateNd;
                for (int n = 0; n < 4; n++)
                {
                    if (nendo[n] != "")
                    {
                        aryJpDateNd = Util.ConvJpDate(Util.ToDate(nendo[n] + "/01/01"), this._dba);
                        nendo[n] = aryJpDateNd[0] + aryJpDateNd[2] + " 年度";
                    }
                }

                DbParamCollection dpc = new DbParamCollection();
                StringBuilder Sql = new StringBuilder();
                foreach (DataRow r in dt.Rows)
                {
                    // 一旦金額を初期化
                    zenZanKingaku1 = 0;
                    zenZanKingaku2 = 0;
                    zenZanKingaku3 = 0;
                    zenZanKingaku = 0;
                    karikataKingaku = 0;
                    kashikataKingaku = 0;
                    zandakaKingaku = 0;

                    zenZanKingaku1 = Util.ToDecimal(r["ZANDAKA_KINGAKUZEN1"]);
                    zenZanKingaku2 = Util.ToDecimal(r["ZANDAKA_KINGAKUZEN2"]);
                    zenZanKingaku3 = Util.ToDecimal(r["ZANDAKA_KINGAKUZEN3"]);

                    zenZanKingaku = Util.ToDecimal(r["ZANDAKA_KINGAKU"]);
                    karikataKingaku = Util.ToDecimal(r["KARIKATA_KINGAKU"]);
                    kashikataKingaku = Util.ToDecimal(r["KASHIKATA_KINGAKU"]);
                    zandakaKingaku = Util.ToDecimal(r["TOUZAN_KINGAKU"]);

                    kamokuBunrui = Util.ToInt(r["KAMOKU_BUNRUI"]);

                    moji_shubetsu = Util.ToString(r["MOJI_SHUBETSU"]);

                    #region 全て同じ扱いに
                    if (zenZanKingaku1 != 0)
                    {
                        tmpZenZanKingaku1 = Util.FormatNum(Util.ToString(zenZanKingaku1));
                    }
                    else
                    {
                        tmpZenZanKingaku1 = "";
                    }
                    if (zenZanKingaku2 != 0)
                    {
                        tmpZenZanKingaku2 = Util.FormatNum(Util.ToString(zenZanKingaku2));
                    }
                    else
                    {
                        tmpZenZanKingaku2 = "";
                    }
                    if (zenZanKingaku3 != 0)
                    {
                        tmpZenZanKingaku3 = Util.FormatNum(Util.ToString(zenZanKingaku3));
                    }
                    else
                    {
                        tmpZenZanKingaku3 = "";
                    }

                    if (zenZanKingaku != 0)//  < 0
                    {
                        tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
                    }
                    else
                    {
                        tmpZenZanKingaku = "";
                    }

                    if (karikataKingaku != 0)
                    {
                        tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
                    }
                    else
                    {
                        tmpKarikataKingaku = "";
                    }

                    if (kashikataKingaku != 0)
                    {
                        tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
                    }
                    else
                    {
                        tmpKashikataKingaku = "";
                    }

                    if (zandakaKingaku != 0)
                    {
                        tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
                    }
                    else
                    {
                        tmpZandakaKingaku = "";
                    }
                    #endregion

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.AppendLine("INSERT INTO PR_ZM_TBL(");
                    Sql.AppendLine("  GUID");
                    Sql.AppendLine(" ,SORT");
                    Sql.AppendLine(" ,ITEM01");
                    Sql.AppendLine(" ,ITEM02");
                    Sql.AppendLine(" ,ITEM03");
                    Sql.AppendLine(" ,ITEM04");
                    Sql.AppendLine(" ,ITEM05");
                    Sql.AppendLine(" ,ITEM06");
                    Sql.AppendLine(" ,ITEM07");
                    Sql.AppendLine(" ,ITEM08");
                    Sql.AppendLine(" ,ITEM09");
                    Sql.AppendLine(" ,ITEM10");
                    Sql.AppendLine(" ,ITEM11");
                    Sql.AppendLine(" ,ITEM12");
                    Sql.AppendLine(" ,ITEM13");
                    Sql.AppendLine(" ,ITEM14");
                    Sql.AppendLine(" ,ITEM15");
                    Sql.AppendLine(" ,ITEM16");
                    Sql.AppendLine(" ,ITEM17");
                    Sql.AppendLine(" ,ITEM18");
                    Sql.AppendLine(") ");
                    Sql.AppendLine("VALUES(");
                    Sql.AppendLine("  @GUID");
                    Sql.AppendLine(" ,@SORT");
                    Sql.AppendLine(" ,@ITEM01");
                    Sql.AppendLine(" ,@ITEM02");
                    Sql.AppendLine(" ,@ITEM03");
                    Sql.AppendLine(" ,@ITEM04");
                    Sql.AppendLine(" ,@ITEM05");
                    Sql.AppendLine(" ,@ITEM06");
                    Sql.AppendLine(" ,@ITEM07");
                    Sql.AppendLine(" ,@ITEM08");
                    Sql.AppendLine(" ,@ITEM09");
                    Sql.AppendLine(" ,@ITEM10");
                    Sql.AppendLine(" ,@ITEM11");
                    Sql.AppendLine(" ,@ITEM12");
                    Sql.AppendLine(" ,@ITEM13");
                    Sql.AppendLine(" ,@ITEM14");
                    Sql.AppendLine(" ,@ITEM15");
                    Sql.AppendLine(" ,@ITEM16");
                    Sql.AppendLine(" ,@ITEM17");
                    Sql.AppendLine(" ,@ITEM18");
                    Sql.AppendLine(") ");

                    // 金額がｾﾞﾛの科目を印字する場合、あるいは金額がｾﾞﾛの科目を印字しない場合かつ金額が0でない場合
                    if ((Util.ToString(this._pForm.Condition["Inji"]) == "yes")
                        || (tmpZenZanKingaku1 != "" || tmpZenZanKingaku2 != "" || tmpZenZanKingaku3 != "" || tmpZenZanKingaku != "" || tmpKarikataKingaku != "" || tmpKashikataKingaku != "" || tmpZandakaKingaku != ""))
                    {

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, title); // タイトル
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, bumonRange); // 部門名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, kikan); // 期間
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, this._uInfo.KaishaNm); // 会社名
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.ToString(this._pForm.Condition["ShohizeiShori"])); // 税抜きor税込み

                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, nendo[0]); // タイトル1
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, nendo[1]); // タイトル2
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, nendo[2]); // タイトル3
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, nendo[3]); // タイトル4

                        tmpKanjoKamokuNm = Util.ToString(r["KAMOKU_BUNRUI_NM"]);

                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, tmpKanjoKamokuNm); // 科目名

                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, tmpZenZanKingaku1); // 前前前前期残高
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, tmpZenZanKingaku2); // 前前前期残高
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, tmpZenZanKingaku3); // 前前期残高

                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前期残高
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当期

                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, title); // 改ページ

                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, outPutDate); // 出力日付
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, moji_shubetsu); // 文字種別

                        this._dba.ModifyBySql(Util.ToString(Sql), dpc);

                        dbSORT++;
                    }
                }
            }
        }

        /// <summary>
        /// ファイル保存先の表示
        /// </summary>
        /// <param name="fName"></param>
        /// <param name="filetype"></param>
        /// <returns></returns>
        private string GetSavePath(string fName, decimal filetype)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.FileName = fName;
            string fType = "";
            if (filetype == 1)
            {
                fType = "PDF";
            }
            else
            {
                fType = "EXCEL";
            }
            string folderName = @"C:\";
            try
            {
                folderName = this._config.LoadPgConfig(Constants.SubSys.Han, "OUTPUT", fType, "PATH");
            }
            catch (Exception)
            {
                folderName = @"C:\";
            }
            if (ValChk.IsEmpty(folderName))
            {
                folderName = @"C:\";
            }
            sfd.InitialDirectory = folderName;

            sfd.AddExtension = true;

            if (filetype == 1)
            {
                sfd.Title = "PDFファイルの保存";
                sfd.Filter = "全てのファイル(*.*)|*.*|" +
                             "ＰＤＦファイル(*.pdf)|*.pdf";
            }
            else
            {
                sfd.Title = "EXCELファイルの保存";
                sfd.Filter = "全てのファイル(*.*)|*.*|" +
                             "Excelファイル(*.xlsx)|*.xlsx";
            }
            sfd.FilterIndex = 2;
            DialogResult ret = sfd.ShowDialog();
            if (ret == DialogResult.OK)
            {
                folderName = System.IO.Path.GetDirectoryName(sfd.FileName);
                try
                {
                    this._config.SetPgConfig(Constants.SubSys.Han, "OUTPUT", fType, "PATH", folderName);
                    this._config.SaveConfig();
                }
                catch (Exception) { }
                return sfd.FileName;
            }
            else
            {
                return "";
            }
        }

        #region コメント
        ///// <summary>
        ///// インサートテーブルを作成します。
        ///// </summary>
        //private StringBuilder MakeWkData_insertTable(StringBuilder Sql, int maxNo)
        //{
        //    Sql.AppendLine("INSERT INTO PR_ZM_TBL(");
        //    Sql.AppendLine("  GUID");
        //    Sql.AppendLine(" ,SORT");
        //    Sql.AppendLine(" ,ITEM01");
        //    Sql.AppendLine(" ,ITEM02");
        //    Sql.AppendLine(" ,ITEM03");
        //    Sql.AppendLine(" ,ITEM04");
        //    Sql.AppendLine(" ,ITEM05");
        //    Sql.AppendLine(" ,ITEM06");
        //    Sql.AppendLine(" ,ITEM07");
        //    Sql.AppendLine(" ,ITEM08");
        //    Sql.AppendLine(" ,ITEM09");
        //    for (int itemNo = 10; itemNo <= maxNo; itemNo++)
        //    {
        //        Sql.AppendLine(" ,ITEM" + itemNo);
        //    }
        //    Sql.AppendLine(") ");
        //    Sql.AppendLine("VALUES(");
        //    Sql.AppendLine("  @GUID");
        //    Sql.AppendLine(" ,@SORT");
        //    Sql.AppendLine(" ,@ITEM01");
        //    Sql.AppendLine(" ,@ITEM02");
        //    Sql.AppendLine(" ,@ITEM03");
        //    Sql.AppendLine(" ,@ITEM04");
        //    Sql.AppendLine(" ,@ITEM05");
        //    Sql.AppendLine(" ,@ITEM06");
        //    Sql.AppendLine(" ,@ITEM07");
        //    Sql.AppendLine(" ,@ITEM08");
        //    Sql.AppendLine(" ,@ITEM09");
        //    for (int itemNo = 10; itemNo <= maxNo; itemNo++)
        //    {
        //        Sql.AppendLine(" ,@ITEM" + itemNo);
        //    }
        //    Sql.AppendLine(") ");

        //    return Sql;
        //}

        ///// <summary>
        ///// 貸借対照表のデータを作成します。
        ///// </summary>
        //private void MakeWkData_taishaku(string kikan, string bumonRange, string outPutDate, string zandakaNm)
        //{
        //    #region 前準備
        //    int dbSORT = 0;
        //    StringBuilder Sql = new StringBuilder();
        //    DbParamCollection dpc = new DbParamCollection();
        //    ZMMR1031DA da = new ZMMR1031DA(this._uInfo, this._dba, this._config);

        //    int addRowCount = 5;

        //    String itemNo01 = "08";
        //    String itemNo02 = "09";
        //    String itemNo03 = "10";
        //    String itemNo04 = "11";
        //    String itemNo05 = "12";

        //    String itemNm = "@ITEM";

        //    DataTable kanjoKamokuIchiran;
        //    DataRow[] dataRows;
        //    Decimal[] kingakuDate;
        //    String joken;
        //    int kamokuBunrui;
        //    int meisaiKomokuSu;
        //    int cnt;
        //    int meisaiCnt;
        //    int dataCnt = 1;
        //    //int handHouteiJunbi = 13320;
        //    int handHouteiJunbi = this._pForm.KAMOKU_BUNRUI_HOTE_JUNBIKIN;

        //    decimal zenZanKingaku;
        //    decimal karikataKingaku;
        //    decimal kashikataKingaku;
        //    decimal zandakaKingaku;
        //    decimal houteiJunbiKin = 0;

        //    decimal toukiZandaka = 0;
        //    decimal toukiKarikata = 0;
        //    decimal toukiKashikata = 0;
        //    decimal toukiTouzan = 0;
        //    String tmpKanjoKamokuNm;
        //    String tmpZenZanKingaku;
        //    String tmpKarikataKingaku;
        //    String tmpKashikataKingaku;
        //    String tmpZandakaKingaku;

        //    // ページ最終表示順位番号
        //    String hyojiNo_taishaku01 = "220";
        //    String hyojiNo_taishaku02 = "420";
        //    #endregion

        //    /* 貸借対照表(P1)を準備 */
        //    Sql = MakeWkData_insertTable(new StringBuilder(), taishaku01);
        //    dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //    foreach (DataRow date in TaishakuTaishohyo.Rows)
        //    {
        //        // 一旦金額を初期化
        //        zenZanKingaku = 0;
        //        karikataKingaku = 0;
        //        kashikataKingaku = 0;
        //        zandakaKingaku = 0;

        //        // 明細項目数を取得
        //        meisaiKomokuSu = Util.ToInt(date["MEISAI_KOMOKUSU"]);
        //        kamokuBunrui = Util.ToInt(date["KAMOKU_BUNRUI"]);
        //        kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F1, kamokuBunrui);
        //        joken = "shukeiKeisanShiki = " + kamokuBunrui;
        //        dataRows = KanjoKamokuKingaku.Select(joken);

        //        #region 小項目データをワークテーブルに登録
        //        cnt = 0;
        //        meisaiCnt = 0;
        //        foreach (DataRow subDate in kanjoKamokuIchiran.Rows)
        //        {
        //            int kanjoKamokuCd = Util.ToInt(kanjoKamokuIchiran.Rows[cnt]["KANJO_KAMOKU_CD"]);
        //            if (meisaiKomokuSu <= meisaiCnt)
        //            {
        //                break;
        //            }

        //            #region 金額データを取得
        //            // 現在の行番号と次の行番号が同じ場合
        //            try
        //            {
        //                if (Util.ToInt(subDate["GYO_BANGO"]) == Util.ToInt(kanjoKamokuIchiran.Rows[cnt + 1]["GYO_BANGO"]))
        //                {
        //                    zenZanKingaku += Util.ToDecimal(dataRows[cnt]["zandakaKingaku"]);
        //                    karikataKingaku += Util.ToDecimal(dataRows[cnt]["karikataKingaku"]);
        //                    kashikataKingaku += Util.ToDecimal(dataRows[cnt]["kashikataKingaku"]);
        //                    zandakaKingaku += Util.ToDecimal(dataRows[cnt]["touzanKingaku"]);
        //                    cnt++;
        //                    continue;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }

        //            zenZanKingaku += Util.ToDecimal(dataRows[cnt]["zandakaKingaku"]);
        //            karikataKingaku += Util.ToDecimal(dataRows[cnt]["karikataKingaku"]);
        //            kashikataKingaku += Util.ToDecimal(dataRows[cnt]["kashikataKingaku"]);
        //            zandakaKingaku += Util.ToDecimal(dataRows[cnt]["touzanKingaku"]);

        //            //勘定科目CD141以上147以下
        //            //if (141 <= kanjoKamokuCd && kanjoKamokuCd <= 147)
        //            if (this._pForm.KAMOKU_BUNRUI_KEIZAI_JIGYO_MISHUKIN == kamokuBunrui)
        //            {
        //                #region 科目分類11133のみ
        //                if (zenZanKingaku != 0)//  < 0
        //                {
        //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
        //                }
        //                else
        //                {
        //                    tmpZenZanKingaku = "";
        //                }

        //                if (karikataKingaku != 0)
        //                {
        //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
        //                }
        //                else
        //                {
        //                    tmpKarikataKingaku = "";
        //                }

        //                if (kashikataKingaku != 0)
        //                {
        //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
        //                }
        //                else
        //                {
        //                    tmpKashikataKingaku = "";
        //                }

        //                if (zandakaKingaku != 0)
        //                {
        //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
        //                }
        //                else
        //                {
        //                    tmpZandakaKingaku = "";
        //                }
        //                #endregion
        //            }
        //            else
        //            {
        //                #region それ以外
        //                if (zenZanKingaku > 0)
        //                {
        //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
        //                }
        //                else if (zenZanKingaku < 0)
        //                {
        //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku * -1));
        //                }
        //                else
        //                {
        //                    tmpZenZanKingaku = "";
        //                }
        //                if (karikataKingaku > 0)
        //                {
        //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
        //                }
        //                else if (karikataKingaku < 0)
        //                {
        //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku * -1));
        //                }
        //                else
        //                {
        //                    tmpKarikataKingaku = "";
        //                }
        //                if (kashikataKingaku > 0)
        //                {
        //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
        //                }
        //                else if (kashikataKingaku < 0)
        //                {
        //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku * -1));
        //                }
        //                else
        //                {
        //                    tmpKashikataKingaku = "";
        //                }
        //                if (zandakaKingaku > 0)
        //                {
        //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
        //                }
        //                else if (zandakaKingaku < 0)
        //                {
        //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku * -1));
        //                }
        //                else
        //                {
        //                    tmpZandakaKingaku = "";
        //                }
        //                #endregion
        //            }
        //            #endregion
        //            // 金額がｾﾞﾛの科目を印字する場合、あるいは金額がｾﾞﾛの科目を印字しない場合かつ金額が0でない場合
        //            if ((Util.ToString(this._pForm.Condition["Inji"]) == "yes")
        //                || (tmpZenZanKingaku != "" || tmpKarikataKingaku != "" || tmpKashikataKingaku != "" || tmpZandakaKingaku != ""))
        //            {
        //                tmpKanjoKamokuNm = Util.ToString(subDate["KANJO_KAMOKU_NM"]);

        //                dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, tmpKanjoKamokuNm); // 科目名
        //                dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前日残高
        //                dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, tmpKarikataKingaku); // 貸方
        //                dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, tmpKashikataKingaku); // 借方
        //                dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当残

        //                // ITEM名称をCOUNTUP
        //                itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //                itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //                itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //                itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //                itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);

        //                meisaiCnt++;
        //            }

        //            // 現在の行番号と次の行番号が一致しない場合
        //            try
        //            {
        //                if (Util.ToInt(subDate["GYO_BANGO"]) != Util.ToInt(kanjoKamokuIchiran.Rows[cnt + 1]["GYO_BANGO"]))
        //                {
        //                    zenZanKingaku = 0;
        //                    karikataKingaku = 0;
        //                    kashikataKingaku = 0;
        //                    zandakaKingaku = 0;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }

        //            cnt++;
        //        }

        //        // 指定明細数に満たない場合、空行をセット
        //        while (meisaiKomokuSu > meisaiCnt)
        //        {
        //            dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, ""); // 科目名
        //            dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, ""); // 前日残高
        //            dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, ""); // 貸方
        //            dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, ""); // 借方
        //            dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, ""); // 当残

        //            // ITEM名称をCOUNTUP
        //            itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //            itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //            itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //            itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //            itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);

        //            meisaiCnt++;
        //        }
        //        #endregion

        //        #region 大項目データをワークテーブルに登録
        //        // 明細区分が0以外の場合
        //        if (Util.ToInt(date["MEISAI_KUBUN"]) != 0)
        //        {
        //            kingakuDate = getKingakuDate01(date);
        //        }
        //        // 明細区分が0の場合
        //        else
        //        {
        //            kingakuDate = getKingakuDate02(date);
        //        }
        //        // 勘定科目が法定準備金の場合金額を保持
        //        if (Util.ToInt(date["KAMOKU_BUNRUI"]) == handHouteiJunbi)
        //        {
        //            houteiJunbiKin = kingakuDate[4];
        //            TaishakuHoteiJunbikin[0] = houteiJunbiKin;
        //        }

        //        #region 勘定科目が"当期未処分剰余金"、"(うち当期剰余金)"の場合
        //        // 名護へ合わせ
        //        //if (TaishakuTaishohyo.Rows.Count - 3 == dataCnt || TaishakuTaishohyo.Rows.Count - 2 == dataCnt)
        //        if (TaishakuTaishohyo.Rows.Count - 4 == dataCnt || TaishakuTaishohyo.Rows.Count - 3 == dataCnt)
        //        {
        //            // 対象区分が借方の場合
        //            if (Util.ToInt(date["TAISHAKU_KUBUN"]) == 1)
        //            {
        //                kingakuDate[1] = kingakuDate[3] - kingakuDate[0];
        //                kingakuDate[2] = kingakuDate[3] + kingakuDate[0];
        //                // 当残が0未満の場合
        //                if (kingakuDate[3] < 0)
        //                {
        //                    // 借方発生
        //                    kingakuDate[1] = 0;
        //                }
        //            }
        //            // 対象区分が貸方の場合
        //            else
        //            {
        //                //kingakuDate[0] = kingakuDate[0] * -1;
        //                kingakuDate[1] = kingakuDate[3] + kingakuDate[0];
        //                //kingakuDate[3] = kingakuDate[3] * -1;
        //                kingakuDate[2] = kingakuDate[3] - kingakuDate[0];
        //                // 勘定科目が"(うち当期剰余金)"の場合
        //                // 名護へ合わせ
        //                //if (TaishakuTaishohyo.Rows.Count - 3 != dataCnt)
        //                if (TaishakuTaishohyo.Rows.Count - 4 != dataCnt)
        //                {
        //                    // 当残が0以上の場合
        //                    if (kingakuDate[3] >= 0)
        //                    {
        //                        // 借方発生
        //                        kingakuDate[1] = 0;
        //                    }
        //                    // 0未満の場合
        //                    else
        //                    {
        //                        // 貸方発生
        //                        kingakuDate[2] = 0;
        //                    }
        //                }
        //                else
        //                {
        //                    // 当残が0以上の場合
        //                    if (kingakuDate[3] >= 0)
        //                    {
        //                        // 借方発生
        //                        kingakuDate[1] = houteiJunbiKin;
        //                        kingakuDate[3] = kingakuDate[0] + kingakuDate[2] - kingakuDate[1];
        //                    }
        //                    // 0以下の場合
        //                    else
        //                    {
        //                        // 貸方発生
        //                        kingakuDate[2] = 0;
        //                    }
        //                }
        //            }

        //            // 名護へ合わせ
        //            //if (TaishakuTaishohyo.Rows.Count - 3 == dataCnt)// 当期未処分剰余金を保持
        //            if (TaishakuTaishohyo.Rows.Count - 4 == dataCnt)// 当期未処分剰余金を保持
        //            {
        //                toukiZandaka = kingakuDate[0];
        //                toukiKarikata = kingakuDate[1];
        //                toukiKashikata = kingakuDate[2];
        //                toukiTouzan = kingakuDate[3];
        //            }
        //        }
        //        else if (TaishakuTaishohyo.Rows.Count - 1 == dataCnt || TaishakuTaishohyo.Rows.Count - 0 == dataCnt)// "資本の部合計"、"負債・資本の部合計"の場合
        //        {
        //            kingakuDate[0] = kingakuDate[0] + toukiZandaka;
        //            kingakuDate[1] = kingakuDate[1] + toukiKarikata;
        //            kingakuDate[2] = kingakuDate[2] + toukiKashikata;
        //            kingakuDate[3] = kingakuDate[3] + toukiTouzan;
        //        }

        //        // 2016-05-11 追記 kisemori
        //        // 法定準備金と当期未処分剰余金を計算
        //        // 名護へ合わせ
        //        //if (TaishakuTaishohyo.Rows.Count - 3 == dataCnt || TaishakuTaishohyo.Rows.Count - 1 == dataCnt || TaishakuTaishohyo.Rows.Count == dataCnt)
        //        if (TaishakuTaishohyo.Rows.Count - 4 == dataCnt || TaishakuTaishohyo.Rows.Count - 1 == dataCnt || TaishakuTaishohyo.Rows.Count == dataCnt)
        //        {
        //            kingakuDate[0] = kingakuDate[0] - hoteJunbikin + tokiMishobunJoyokin;
        //            kingakuDate[3] = kingakuDate[3] - hoteJunbikin + tokiMishobunJoyokin;
        //        }
        //        #endregion

        //        // 金額がｾﾞﾛの科目を印字しない場合
        //        if (Util.ToString(this._pForm.Condition["Inji"]) == "no" &&
        //            kingakuDate[0] == 0 && kingakuDate[1] == 0 && kingakuDate[2] == 0 && kingakuDate[3] == 0)
        //        {
        //            tmpKanjoKamokuNm = "";
        //            tmpZenZanKingaku = "";
        //            tmpKarikataKingaku = "";
        //            tmpKashikataKingaku = "";
        //            tmpZandakaKingaku = "";
        //        }
        //        else
        //        {

        //            tmpKanjoKamokuNm = Util.ToString(date["KAMOKU_BUNRUI_NM"]);
        //            tmpZenZanKingaku = Util.FormatNum(kingakuDate[0]);
        //            tmpKarikataKingaku = Util.FormatNum(kingakuDate[1]);
        //            tmpKashikataKingaku = Util.FormatNum(kingakuDate[2]);
        //            tmpZandakaKingaku = Util.FormatNum(kingakuDate[3]);
        //        }

        //        dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, tmpKanjoKamokuNm); // 科目名
        //        dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前日残高
        //        dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, tmpKarikataKingaku); // 貸方
        //        dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, tmpKashikataKingaku); // 借方
        //        dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当残

        //        // ITEM名称をCOUNTUP
        //        itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //        itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //        itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //        itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //        itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);
        //        #endregion

        //        // 1P目の最後の項目でインサート.
        //        if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_taishaku01)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);

        //            /* 貸借対照表(P2)を準備*/
        //            dbSORT++;
        //            Sql = MakeWkData_insertTable(new StringBuilder(), taishaku02);
        //            dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //            itemNo01 = "08";
        //            itemNo02 = "09";
        //            itemNo03 = "10";
        //            itemNo04 = "11";
        //            itemNo05 = "12";
        //        }
        //        // 2P目の最後の項目でインサート.
        //        else if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_taishaku02)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);
        //        }

        //        dataCnt++;
        //    }
        //}

        ///// <summary>
        ///// 損益計算書のデータを作成します。
        ///// </summary>
        //private void MakeWkData_soneki(string kikan, string bumonRange, string outPutDate, string zandakaNm)
        //{
        //    #region 前準備
        //    int dbSORT = 2;
        //    StringBuilder Sql = new StringBuilder();
        //    DbParamCollection dpc = new DbParamCollection();
        //    ZMMR1031DA da = new ZMMR1031DA(this._uInfo, this._dba, this._config);

        //    int addRowCount = 5;

        //    String itemNo01 = "08";
        //    String itemNo02 = "09";
        //    String itemNo03 = "10";
        //    String itemNo04 = "11";
        //    String itemNo05 = "12";

        //    String itemNm = "@ITEM";

        //    DataTable kanjoKamokuIchiran;
        //    DataRow[] dataRows;
        //    Decimal[] kingakuDate;
        //    String joken;
        //    int kamokuBunrui;
        //    int meisaiKomokuSu;
        //    int cnt;
        //    int meisaicnt;
        //    int dataCnt = 1;

        //    decimal zenZanKingakuData;
        //    decimal karikataKingakuData;
        //    decimal kashikataKingakuData;
        //    decimal zandakaKingakuData;

        //    String tmpZenZanKingaku;
        //    String tmpKarikataKingaku;
        //    String tmpKashikataKingaku;
        //    String tmpZandakaKingaku;

        //    // ページ最終表示順位番号
        //    String hyojiNo_soneki01 = "150";
        //    String hyojiNo_soneki02 = "350";
        //    String hyojiNo_soneki03 = "440";
        //    String hyojiNo_soneki04 = "510";
        //    #endregion

        //    /* 損益計算書(P1) */
        //    Sql = MakeWkData_insertTable(new StringBuilder(), soneki01);
        //    dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //    foreach (DataRow date in SonekiKeisansho.Rows)
        //    {
        //        // 一旦金額を初期化
        //        zenZanKingakuData = 0;
        //        karikataKingakuData = 0;
        //        kashikataKingakuData = 0;
        //        zandakaKingakuData = 0;

        //        // 明細項目数を取得
        //        meisaiKomokuSu = Util.ToInt(date["MEISAI_KOMOKUSU"]);
        //        kamokuBunrui = Util.ToInt(date["KAMOKU_BUNRUI"]);
        //        kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F2, kamokuBunrui);
        //        joken = "shukeiKeisanShiki = " + kamokuBunrui;
        //        dataRows = KanjoKamokuKingaku.Select(joken);

        //        #region 小項目データをワークテーブルに登録
        //        cnt = 0;
        //        meisaicnt = 0;
        //        foreach (DataRow subDate in kanjoKamokuIchiran.Rows)
        //        {
        //            if (meisaiKomokuSu <= meisaicnt)
        //            {
        //                break;
        //            }

        //            #region 金額データを取得
        //            // 現在の行番号と次の行番号が同じ場合
        //            try
        //            {
        //                if (Util.ToInt(subDate["GYO_BANGO"]) == Util.ToInt(kanjoKamokuIchiran.Rows[cnt + 1]["GYO_BANGO"]))
        //                {
        //                    zenZanKingakuData += Util.ToDecimal(dataRows[cnt]["zandakaKingaku"]);
        //                    karikataKingakuData += Util.ToDecimal(dataRows[cnt]["karikataKingaku"]);
        //                    kashikataKingakuData += Util.ToDecimal(dataRows[cnt]["kashikataKingaku"]);
        //                    zandakaKingakuData += Util.ToDecimal(dataRows[cnt]["touzanKingaku"]);
        //                    cnt++;
        //                    continue;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }

        //            zenZanKingakuData += Util.ToDecimal(dataRows[cnt]["zandakaKingaku"]);
        //            karikataKingakuData += Util.ToDecimal(dataRows[cnt]["karikataKingaku"]);
        //            kashikataKingakuData += Util.ToDecimal(dataRows[cnt]["kashikataKingaku"]);
        //            zandakaKingakuData += Util.ToDecimal(dataRows[cnt]["touzanKingaku"]);

        //            if (zenZanKingakuData < 0)
        //            {
        //                zenZanKingakuData = zenZanKingakuData * -1;
        //            }
        //            if (karikataKingakuData < 0)
        //            {
        //                karikataKingakuData = karikataKingakuData * -1;
        //            }
        //            if (kashikataKingakuData < 0)
        //            {
        //                kashikataKingakuData = kashikataKingakuData * -1;
        //            }
        //            if (zandakaKingakuData < 0)
        //            {
        //                zandakaKingakuData = zandakaKingakuData * -1;
        //            }
        //            #endregion

        //            // 金額がｾﾞﾛの科目を印字する場合、あるいは金額がｾﾞﾛの科目を印字しない場合かつ金額が0でない場合
        //            if (Util.ToString(this._pForm.Condition["Inji"]) == "yes" ||
        //                    zenZanKingakuData != 0 || karikataKingakuData != 0 || kashikataKingakuData != 0 || zandakaKingakuData != 0)
        //            {
        //                tmpZenZanKingaku = Util.FormatNum(zenZanKingakuData);
        //                tmpKarikataKingaku = Util.FormatNum(karikataKingakuData);
        //                tmpKashikataKingaku = Util.FormatNum(kashikataKingakuData);
        //                tmpZandakaKingaku = Util.FormatNum(zandakaKingakuData);

        //                dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, Util.ToString(subDate["KANJO_KAMOKU_NM"])); // 科目名
        //                dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前日残高
        //                dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, tmpKarikataKingaku); // 貸方
        //                dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, tmpKashikataKingaku); // 借方
        //                dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当残

        //                // ITEM名称をCOUNTUP
        //                itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //                itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //                itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //                itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //                itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);

        //                meisaicnt++;
        //            }

        //            // 現在の行番号と次の行番号が一致しない場合
        //            try
        //            {
        //                if (Util.ToInt(subDate["GYO_BANGO"]) != Util.ToInt(kanjoKamokuIchiran.Rows[cnt + 1]["GYO_BANGO"]))
        //                {
        //                    zenZanKingakuData = 0;
        //                    karikataKingakuData = 0;
        //                    kashikataKingakuData = 0;
        //                    zandakaKingakuData = 0;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }

        //            cnt++;
        //        }

        //        // 指定明細数に満たない場合、空行をセット
        //        while (meisaiKomokuSu > meisaicnt)
        //        {
        //            dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, ""); // 科目名
        //            dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, ""); // 前日残高
        //            dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, ""); // 貸方
        //            dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, ""); // 借方
        //            dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, ""); // 当残

        //            // ITEM名称をCOUNTUP
        //            itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //            itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //            itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //            itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //            itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);

        //            meisaicnt++;
        //        }
        //        #endregion

        //        #region 大項目データをワークテーブルに登録
        //        // 集計区分が2の場合
        //        if (Util.ToInt(date["SHUKEI_KUBUN"]) == 2)
        //        {
        //            kingakuDate = getKingakuDate03(date);
        //            //if(Util.ToInt(date["KAMOKU_BUNRUI"]) == 22400)
        //            if (Util.ToInt(date["KAMOKU_BUNRUI"]) == this._pForm.KAMOKU_BUNRUI_TOKI_MISYOBUN_JOYOKIN)
        //            {
        //                kingakuDate[0] = kingakuDate[0] - hoteJunbikin + tokiMishobunJoyokin;
        //                kingakuDate[1] = TaishakuHoteiJunbikin[0];
        //                kingakuDate[3] = kingakuDate[0] - kingakuDate[1] + kingakuDate[2];
        //                kingakuDate[3] = kingakuDate[3] - hoteJunbikin;// - tokiMishobunJoyokin;
        //                tokiMishobunzenZanKingakuSoneki[0] = kingakuDate[3];
        //            }
        //        }
        //        // 集計区分が3の場合
        //        else if (Util.ToInt(date["SHUKEI_KUBUN"]) == 3)
        //        {
        //            kingakuDate = getKingakuDate04(date);
        //            // 勘定科目が"【経常利益】"の場合
        //            if (SonekiKeisansho.Rows.Count - 7 == dataCnt)
        //            {
        //                //kingakuDate[0] = kingakuDate[0] * -1;
        //                //kingakuDate[3] = kingakuDate[3] * -1;
        //                kingakuDate[0] = kingakuDate[0];
        //                kingakuDate[3] = kingakuDate[3];
        //            }
        //            // 勘定科目が"【未処分剰余金】"の場合
        //            else if (SonekiKeisansho.Rows.Count == dataCnt)
        //            {
        //                // 対象区分が借方の場合
        //                if (Util.ToInt(date["TAISHAKU_KUBUN"]) == 1)
        //                {
        //                    kingakuDate[0] = tokiMishobunzenZanKingakuSoneki[0];// + hoteJunbikin - tokiMishobunJoyokin;
        //                    kingakuDate[3] = kingakuDate[0] + kingakuDate[1];// + hoteJunbikin - tokiMishobunJoyokin;
        //                }
        //                // 対象区分が貸方の場合
        //                else
        //                {
        //                    kingakuDate[0] = tokiMishobunzenZanKingakuSoneki[0];// + hoteJunbikin - tokiMishobunJoyokin;
        //                    kingakuDate[3] = kingakuDate[0] + kingakuDate[2];// + hoteJunbikin - tokiMishobunJoyokin;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            continue;
        //        }

        //        tmpZenZanKingaku = Util.FormatNum(kingakuDate[0]);
        //        tmpKarikataKingaku = Util.FormatNum(kingakuDate[1]);
        //        tmpKashikataKingaku = Util.FormatNum(kingakuDate[2]);
        //        tmpZandakaKingaku = Util.FormatNum(kingakuDate[3]);

        //        // 金額がｾﾞﾛの科目を印字しない場合
        //        if (Util.ToString(this._pForm.Condition["Inji"]) == "no")
        //        {
        //            if (kingakuDate[0] == 0 && kingakuDate[1] == 0 && kingakuDate[2] == 0 && kingakuDate[3] == 0)
        //            {
        //                tmpZenZanKingaku = "";
        //                tmpKarikataKingaku = "";
        //                tmpKashikataKingaku = "";
        //                tmpZandakaKingaku = "";
        //            }
        //        }

        //        dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, date["KAMOKU_BUNRUI_NM"]); // 科目名
        //        dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前日残高
        //        dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, tmpKarikataKingaku); // 貸方
        //        dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, tmpKashikataKingaku); // 借方
        //        dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当残

        //        // ITEM名称をCOUNTUP
        //        itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //        itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //        itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //        itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //        itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);
        //        #endregion

        //        // 1P目の最後の項目でインサート.
        //        if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_soneki01)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);

        //            /* 損益計算書(P2)を準備*/
        //            dbSORT++;
        //            Sql = MakeWkData_insertTable(new StringBuilder(), soneki02);
        //            dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //            itemNo01 = "08";
        //            itemNo02 = "09";
        //            itemNo03 = "10";
        //            itemNo04 = "11";
        //            itemNo05 = "12";
        //        }
        //        // 2P目の最後の項目でインサート.
        //        else if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_soneki02)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);

        //            /* 損益計算書(P3)を準備*/
        //            dbSORT++;
        //            Sql = MakeWkData_insertTable(new StringBuilder(), soneki03);
        //            dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //            itemNo01 = "08";
        //            itemNo02 = "09";
        //            itemNo03 = "10";
        //            itemNo04 = "11";
        //            itemNo05 = "12";
        //        }
        //        // 3P目の最後の項目でインサート.
        //        else if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_soneki03)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);

        //            /* 損益計算書(P4)を準備*/
        //            dbSORT++;
        //            Sql = MakeWkData_insertTable(new StringBuilder(), soneki04);
        //            dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //            itemNo01 = "08";
        //            itemNo02 = "09";
        //            itemNo03 = "10";
        //            itemNo04 = "11";
        //            itemNo05 = "12";
        //        }
        //        // 4P目の最後の項目でインサート.
        //        else if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_soneki04)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);
        //        }

        //        dataCnt++;
        //    }
        //}

        ///// <summary>
        ///// 共通パラム
        ///// </summary>
        //private DbParamCollection MakeWkData_setDpc(int dbSORT, String kikan, string bumonRange, string outPutDate, string zandakaNm)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    /* ▼▼▼ 共通 ▼▼▼ */
        //    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
        //    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
        //    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, bumonRange); // 部門名
        //    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, this._uInfo.KaishaNm); // 会社名
        //    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, kikan); // 期間
        //    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, Util.ToString(this._pForm.Condition["ShohizeiShori"])); // 税抜きor税込み
        //    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, outPutDate); // 出力日付
        //    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dbSORT); // ページ切り替え用
        //    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, zandakaNm); // 項目タイトル
        //    /* ▲▲▲ 共通 ▲▲▲ */

        //    return dpc;
        //}
        #endregion

        #region コメント
        ///// <summary>
        ///// 大項目の金額を取得
        ///// 明細区分が0以外の場合
        ///// </summary>
        //private Decimal[] getKingakuDate01(DataRow date)
        //{
        //    #region 金額データを取得
        //    String joken = "shukeiKeisanShiki = " + Util.ToString(date["SHUKEI_KEISANSHIKI"]);
        //    decimal zenZanKingaku;
        //    decimal karikataKingaku;
        //    decimal kashikataKingaku;
        //    decimal zandakaKingaku;
        //    decimal houteiJunbiKin = 0;

        //    // 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //    // 貸借区分が借方の場合
        //    if (Util.ToInt(date["TAISHAKU_KUBUN"]) == 1)
        //    {
        //        zenZanKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken));
        //        karikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken));
        //        kashikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken));
        //        zandakaKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken));
        //    }
        //    // 貸借区分が貸方の場合
        //    else
        //    {
        //        zenZanKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken)) * -1;
        //        karikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken)) * -1;
        //        kashikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken)) * -1;
        //        zandakaKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken)) * -1;

        //        // 勘定科目が法定準備金の場合金額を保持
        //        //if (Util.ToInt(date["KAMOKU_BUNRUI"]) == 13320)
        //        if (Util.ToInt(date["KAMOKU_BUNRUI"]) == this._pForm.KAMOKU_BUNRUI_HOTE_JUNBIKIN)
        //        {
        //            houteiJunbiKin = kashikataKingaku;
        //        }
        //    }
        //    #endregion

        //    Decimal[] kingakuDate = new Decimal[5] { zenZanKingaku, karikataKingaku, kashikataKingaku, zandakaKingaku, houteiJunbiKin };

        //    return kingakuDate;
        //}

        ///// <summary>
        ///// 大項目の金額を取得
        ///// 明細区分が0の場合
        ///// </summary>
        //private Decimal[] getKingakuDate02(DataRow date)
        //{
        //    int first = 0;
        //    bool flag = false;
        //    ArrayList stTargetPlus = new ArrayList();
        //    ArrayList stTargetMinus = new ArrayList();
        //    bool plusMinusCheckFlag = false;
        //    int deleteCount;
        //    String shukeiKeisanShiki = Util.ToString(date["SHUKEI_KEISANSHIKI"]);
        //    int daiKomokuTaishakuKubun = Util.ToInt(date["TAISHAKU_KUBUN"]);

        //    #region 集計計算式の"+"と"-"を分別
        //    while (!flag)
        //    {
        //        // "+"と"-"の文字を検索
        //        int findPlus = shukeiKeisanShiki.IndexOf("+");
        //        int findMinus = shukeiKeisanShiki.IndexOf("-");
        //        // "+"も"-"も無ければ、処理終了
        //        if (findPlus <= 0 && findMinus <= 0)
        //        {
        //            if (first == 0)
        //            {
        //                stTargetPlus.Add(shukeiKeisanShiki);
        //            }
        //            else
        //            {
        //                if (!plusMinusCheckFlag)
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki);
        //                }
        //                else if (plusMinusCheckFlag)
        //                {
        //                    stTargetMinus.Add(shukeiKeisanShiki);
        //                }
        //            }
        //            flag = true;
        //        }
        //        // "+"又は"-"があれば分けて格納
        //        else
        //        {
        //            // 最初の処理 1つ目の項目を格納し、文字列から削除
        //            if (first == 0)
        //            {
        //                // "+"の時の処理
        //                if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
        //                }
        //                // "-"の時の処理
        //                else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
        //                }
        //            }
        //            // 2つ目以降の処理 項目を順番に格納し、文字列から削除
        //            else
        //            {
        //                // 削除する文字数を取得
        //                if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                {
        //                    deleteCount = findMinus;
        //                }
        //                else
        //                {
        //                    deleteCount = findPlus;
        //                }
        //                // 項目を格納
        //                if (!plusMinusCheckFlag)
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                }
        //                else if (plusMinusCheckFlag)
        //                {
        //                    stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                }
        //            }
        //            // "+"と"-"の文字を検索
        //            findPlus = shukeiKeisanShiki.IndexOf("+");
        //            findMinus = shukeiKeisanShiki.IndexOf("-");
        //            // 次の項目がが"+"か"-"なのかの判断フラグを設定
        //            if (findPlus == 0)
        //            {
        //                plusMinusCheckFlag = false;
        //            }
        //            else if (findMinus == 0)
        //            {
        //                plusMinusCheckFlag = true;
        //            }
        //            // "+"又は"-"の文字を削除
        //            shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
        //        }
        //        first = 1;
        //    }
        //    #endregion

        //    #region 条件式の作成
        //    // "+"の項目の条件式を作成
        //    int cnt = 0;
        //    string jokenPlus = "";
        //    while (stTargetPlus.Count > cnt)
        //    {
        //        if (cnt == 0)
        //        {
        //            jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[cnt];
        //        }
        //        else
        //        {
        //            jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[cnt];
        //        }

        //        cnt++;
        //    }
        //    // "-"の項目の条件式を作成
        //    cnt = 0;
        //    string jokenMinus = "";
        //    while (stTargetMinus.Count > cnt)
        //    {
        //        if (cnt == 0)
        //        {
        //            jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[cnt];
        //        }
        //        else
        //        {
        //            jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[cnt];
        //        }

        //        cnt++;
        //    }
        //    #endregion

        //    #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //    object sumZandakaKingakuPlus;
        //    object sumKarikataKingakuPlus;
        //    object sumKashikataKingakuPlus;
        //    object sumTouzanKingakuPlus;
        //    object sumZandakaKingakuMinus;
        //    object sumKarikataKingakuMinus;
        //    object sumKashikataKingakuMinus;
        //    object sumTouzanKingakuMinus;
        //    decimal sumZandakaKingaku;
        //    decimal sumKarikataKingaku;
        //    decimal sumKashikataKingaku;
        //    decimal sumTouzanKingaku;
        //    if (jokenPlus != "")
        //    {
        //        sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
        //        sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
        //        sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
        //        sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
        //    }
        //    else
        //    {
        //        sumZandakaKingakuPlus = 0;
        //        sumKarikataKingakuPlus = 0;
        //        sumKashikataKingakuPlus = 0;
        //        sumTouzanKingakuPlus = 0;
        //    }
        //    if (jokenMinus != "")
        //    {
        //        sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
        //        sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
        //        sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
        //        sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
        //    }
        //    else
        //    {
        //        sumZandakaKingakuMinus = 0;
        //        sumKarikataKingakuMinus = 0;
        //        sumKashikataKingakuMinus = 0;
        //        sumTouzanKingakuMinus = 0;
        //    }
        //    sumZandakaKingaku = Util.ToDecimal(sumZandakaKingakuPlus) + Util.ToDecimal(sumZandakaKingakuMinus);
        //    sumKarikataKingaku = Util.ToDecimal(sumKarikataKingakuPlus) + Util.ToDecimal(sumKarikataKingakuMinus);
        //    sumKashikataKingaku = Util.ToDecimal(sumKashikataKingakuPlus) + Util.ToDecimal(sumKashikataKingakuMinus);
        //    sumTouzanKingaku = Util.ToDecimal(sumTouzanKingakuPlus) + Util.ToDecimal(sumTouzanKingakuMinus);

        //    // 貸借区分が貸方の場合
        //    if (daiKomokuTaishakuKubun == 2)
        //    {
        //        sumZandakaKingaku = sumZandakaKingaku * -1;
        //        sumKarikataKingaku = sumKarikataKingaku * -1;
        //        sumKashikataKingaku = sumKashikataKingaku * -1;
        //        sumTouzanKingaku = sumTouzanKingaku * -1;
        //    }
        //    #endregion

        //    Decimal[] kingakuDate = new Decimal[4] { sumZandakaKingaku, sumKarikataKingaku, sumKashikataKingaku, sumTouzanKingaku };

        //    return kingakuDate;
        //}

        ///// <summary>
        ///// 大項目の金額を取得
        ///// 明細区分が0以外の場合　集計区分が2の場合
        ///// </summary>
        //private Decimal[] getKingakuDate03(DataRow date)
        //{
        //    #region 金額データを取得
        //    String joken = "shukeiKeisanShiki = " + Util.ToString(date["SHUKEI_KEISANSHIKI"]);
        //    decimal zenZanKingakuData;
        //    decimal karikataKingakuData;
        //    decimal kashikataKingakuData;
        //    decimal zandakaKingakuData;

        //    // 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //    // 貸借区分が借方の場合
        //    if (Util.ToInt(date["TAISHAKU_KUBUN"]) == 1)
        //    {
        //        zenZanKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken));
        //        karikataKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken));
        //        kashikataKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken));
        //        zandakaKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken));
        //    }
        //    // 貸借区分が貸方の場合
        //    else
        //    {
        //        zenZanKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken)) * -1;
        //        karikataKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken)) * -1;
        //        kashikataKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken)) * -1;
        //        zandakaKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken)) * -1;
        //    }
        //    #endregion

        //    Decimal[] kingakuDate = new Decimal[4] { zenZanKingakuData, karikataKingakuData, kashikataKingakuData, zandakaKingakuData };

        //    return kingakuDate;
        //}

        ///// <summary>
        ///// 大項目の金額を取得
        ///// 明細区分が0の場合 集計区分が3の場合
        ///// </summary>
        //private Decimal[] getKingakuDate04(DataRow date)
        //{
        //    int first = 0;
        //    bool flag = false;
        //    ArrayList stTargetPlus = new ArrayList();
        //    ArrayList stTargetMinus = new ArrayList();
        //    bool plusMinusCheckFlag = false;
        //    int deleteCount;
        //    String shukeiKeisanShiki = Util.ToString(date["SHUKEI_KEISANSHIKI"]);
        //    int daiKomokuTaishakuKubun = Util.ToInt(date["TAISHAKU_KUBUN"]);

        //    #region 集計計算式の"+"と"-"を分別
        //    while (!flag)
        //    {
        //        // "+"と"-"の文字を検索
        //        int findPlus = shukeiKeisanShiki.IndexOf("+");
        //        int findMinus = shukeiKeisanShiki.IndexOf("-");
        //        // "+"も"-"も無ければ、処理終了
        //        if (findPlus <= 0 && findMinus <= 0)
        //        {
        //            if (first == 0)
        //            {
        //                stTargetPlus.Add(shukeiKeisanShiki);
        //            }
        //            else
        //            {
        //                if (!plusMinusCheckFlag)
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki);
        //                }
        //                else if (plusMinusCheckFlag)
        //                {
        //                    stTargetMinus.Add(shukeiKeisanShiki);
        //                }
        //            }
        //            flag = true;
        //        }
        //        // "+"又は"-"があれば分けて格納
        //        else
        //        {
        //            // 最初の処理 1つ目の項目を格納し、文字列から削除
        //            if (first == 0)
        //            {
        //                // "+"の時の処理
        //                if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
        //                }
        //                // "-"の時の処理
        //                else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
        //                }
        //            }
        //            // 2つ目以降の処理 項目を順番に格納し、文字列から削除
        //            else
        //            {
        //                // 削除する文字数を取得
        //                if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                {
        //                    deleteCount = findMinus;
        //                }
        //                else
        //                {
        //                    deleteCount = findPlus;
        //                }
        //                // 項目を格納
        //                if (!plusMinusCheckFlag)
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                }
        //                else if (plusMinusCheckFlag)
        //                {
        //                    stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                }
        //            }
        //            // "+"と"-"の文字を検索
        //            findPlus = shukeiKeisanShiki.IndexOf("+");
        //            findMinus = shukeiKeisanShiki.IndexOf("-");
        //            // 次の項目がが"+"か"-"なのかの判断フラグを設定
        //            if (findPlus == 0)
        //            {
        //                plusMinusCheckFlag = false;
        //            }
        //            else if (findMinus == 0)
        //            {
        //                plusMinusCheckFlag = true;
        //            }
        //            // "+"又は"-"の文字を削除
        //            shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
        //        }
        //        first = 1;
        //    }
        //    #endregion

        //    #region 条件式の作成
        //    // "+"の項目の条件式を作成
        //    int cnt = 0;
        //    string jokenPlus = "";
        //    while (stTargetPlus.Count > cnt)
        //    {
        //        if (cnt == 0)
        //        {
        //            jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[cnt];
        //        }
        //        else
        //        {
        //            jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[cnt];
        //        }

        //        cnt++;
        //    }
        //    // "-"の項目の条件式を作成
        //    cnt = 0;
        //    string jokenMinus = "";
        //    while (stTargetMinus.Count > cnt)
        //    {
        //        if (cnt == 0)
        //        {
        //            jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[cnt];
        //        }
        //        else
        //        {
        //            jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[cnt];
        //        }

        //        cnt++;
        //    }
        //    #endregion

        //    #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //    object sumZandakaKingakuPlus;
        //    object sumKarikataKingakuPlus;
        //    object sumKashikataKingakuPlus;
        //    object sumTouzanKingakuPlus;
        //    object sumZandakaKingakuMinus;
        //    object sumKarikataKingakuMinus;
        //    object sumKashikataKingakuMinus;
        //    object sumTouzanKingakuMinus;
        //    decimal sumZandakaKingaku;
        //    decimal sumKarikataKingaku;
        //    decimal sumKashikataKingaku;
        //    decimal sumTouzanKingaku;
        //    if (jokenPlus != "")
        //    {
        //        sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
        //        sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
        //        sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
        //        sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
        //    }
        //    else
        //    {
        //        sumZandakaKingakuPlus = 0;
        //        sumKarikataKingakuPlus = 0;
        //        sumKashikataKingakuPlus = 0;
        //        sumTouzanKingakuPlus = 0;
        //    }
        //    if (jokenMinus != "")
        //    {
        //        sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
        //        sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
        //        sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
        //        sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
        //    }
        //    else
        //    {
        //        sumZandakaKingakuMinus = 0;
        //        sumKarikataKingakuMinus = 0;
        //        sumKashikataKingakuMinus = 0;
        //        sumTouzanKingakuMinus = 0;
        //    }
        //    sumZandakaKingaku = Util.ToDecimal(sumZandakaKingakuPlus) + Util.ToDecimal(sumZandakaKingakuMinus);
        //    sumKarikataKingaku = Util.ToDecimal(sumKarikataKingakuMinus) - Util.ToDecimal(sumKarikataKingakuPlus);
        //    sumKashikataKingaku = Util.ToDecimal(sumKashikataKingakuMinus) - Util.ToDecimal(sumKashikataKingakuPlus);
        //    sumTouzanKingaku = Util.ToDecimal(sumTouzanKingakuPlus) + Util.ToDecimal(sumTouzanKingakuMinus);

        //    // 貸借区分が借方の場合
        //    if (daiKomokuTaishakuKubun == 1)
        //    {
        //        // 借方>貸方の場合
        //        if (sumKarikataKingaku > sumKashikataKingaku)
        //        {
        //            // 貸方発生
        //            sumKashikataKingaku = (sumKashikataKingaku - sumKarikataKingaku) * -1;
        //            // 借方発生
        //            sumKarikataKingaku = 0;
        //        }
        //        // 借方<貸方の場合
        //        else if (sumKarikataKingaku < sumKashikataKingaku)
        //        {
        //            // 借方発生
        //            sumKarikataKingaku = (sumKarikataKingaku - sumKashikataKingaku) * -1;
        //            // 貸方発生
        //            sumKashikataKingaku = 0;
        //        }
        //        // 借方==貸方の場合
        //        else
        //        {
        //            // 借方発生
        //            sumKarikataKingaku = 0;
        //            // 貸方発生
        //            sumKashikataKingaku = 0;
        //        }
        //    }
        //    // 貸借区分が貸方の場合
        //    else
        //    {
        //        sumZandakaKingaku = sumZandakaKingaku * -1;
        //        sumTouzanKingaku = sumTouzanKingaku * -1;
        //        // 借方>貸方の場合
        //        if (sumKarikataKingaku > sumKashikataKingaku)
        //        {
        //            // 借方発生
        //            sumKarikataKingaku = sumKarikataKingaku - sumKashikataKingaku;
        //            // 貸方発生
        //            sumKashikataKingaku = 0;
        //        }
        //        // 借方<貸方の場合
        //        else if (sumKarikataKingaku < sumKashikataKingaku)
        //        {
        //            // 貸方発生
        //            sumKashikataKingaku = sumKashikataKingaku - sumKarikataKingaku;
        //            // 借方発生
        //            sumKarikataKingaku = 0;
        //        }
        //        // 借方==貸方の場合
        //        else
        //        {
        //            // 借方発生
        //            sumKarikataKingaku = 0;
        //            // 貸方発生
        //            sumKashikataKingaku = 0;
        //        }
        //    }
        //    #endregion

        //    Decimal[] kingakuDate = new Decimal[4] { sumZandakaKingaku, sumKarikataKingaku, sumKashikataKingaku, sumTouzanKingaku };

        //    return kingakuDate;
        //}
        #endregion

        #endregion
    }
}
