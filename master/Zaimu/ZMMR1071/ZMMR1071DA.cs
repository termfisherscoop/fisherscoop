﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1071
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1071DA
    {
        #region private変数
        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// ワークテーブル名
        /// </summary>
        private string WK_TABLE;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMMR1071DA(UserInfo uInfo, DbAccess dba, ConfigLoader config)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
        }
        #endregion

        #region コメント
        #region publicメソッド
        /// <summary>
        /// 貸借対照表設定データ 又は 損益計算書設定データ 又は 製造原価設定データ を取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <returns>貸借対照表設定データ 又は 損益計算書設定データ 又は 製造原価設定データ</returns>
        public DataTable GetKamokuDaiKomoku(int chohyoBunrui)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" HYOJI_JUNI,");
            sql.Append(" KAMOKU_BUNRUI,");
            sql.Append(" KAMOKU_BUNRUI_NM,");
            sql.Append(" TAISHAKU_KUBUN,");
            sql.Append(" MEISAI_KOMOKUSU,");
            sql.Append(" MEISAI_KUBUN,");
            sql.Append(" MOJI_SHUBETSU,");
            sql.Append(" SHUKEI_KUBUN,");
            sql.Append(" SHUKEI_KEISANSHIKI");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHISANHYO_KAMOKU_BUNRUI");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.Append(" SHIYO_KUBUN = 1 AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" ORDER BY");
            sql.Append(" HYOJI_JUNI ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 1, chohyoBunrui);

            DataTable dtYoyakuSettei = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtYoyakuSettei;
        }

        /// <summary>
        /// 貸借勘定科目データを取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <param name="kamokuBunrui">科目分類番号</param>
        /// <param name="gyoBango">行番号</param>
        /// <returns>貸借勘定科目データ</returns>
        public DataTable GetKanjoKamokuIchiran(int kamokuBunrui)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" KANJO_KAMOKU_CD,");
            sql.Append(" KANJO_KAMOKU_NM,");
            sql.Append(" TAISHAKU_KUBUN");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_KANJO_KAMOKU");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" KAMOKU_BUNRUI_CD = @KAMOKU_BUNRUI AND");
            sql.Append(" SHIYO_MISHIYO = 1 AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" ORDER BY");
            sql.Append(" KANJO_KAMOKU_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 6, kamokuBunrui);

            DataTable dtKanjoKamokuIchiran = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtKanjoKamokuIchiran;
        }

        /// <summary>
        /// 貸借対照表データ 又は 損益計算書データ 又は 製造原価データ を取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <param name="kamokuBunrui">科目分類番号</param>
        /// <returns>貸借対照表データ 又は 損益計算書データ 又は 製造原価データ</returns>
        public DataTable GetKamokuShoKomoku(int chohyoBunrui, int kamokuBunrui)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" A.KAMOKU_BUNRUI,");
            sql.Append(" A.GYO_BANGO,");
            sql.Append(" A.KANJO_KAMOKU_CD,");
            sql.Append(" A.KANJO_KAMOKU_NM,");
            sql.Append(" A.TAISHAKU_KUBUN,");
            sql.Append(" B.KANJO_KAMOKU_NM AS M_KANJO_KAMOKU_NM,");
            sql.Append(" B.TAISHAKU_KUBUN AS M_TAISHAKU_KUBUN");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHISANHYO_KAMOKU_SETTEI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" B.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD AND");
            sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.Append(" A.KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" ORDER BY");
            sql.Append(" A.GYO_BANGO ASC,");
            sql.Append(" A.KANJO_KAMOKU_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 1, chohyoBunrui);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 6, kamokuBunrui);

            DataTable dtYoyakuSetteiData = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtYoyakuSetteiData;
        }

        /// <summary>
        /// 貸借勘定科目データを取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <param name="kamokuBunrui">科目分類番号</param>
        /// <param name="gyoBango">行番号</param>
        /// <returns>貸借勘定科目</returns>
        public DataTable GetYoyakuSetteiData(int chohyoBunrui, int kamokuBunrui, int gyoBango)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" A.KAMOKU_BUNRUI,");
            sql.Append(" A.GYO_BANGO,");
            sql.Append(" A.KANJO_KAMOKU_CD,");
            sql.Append(" A.KANJO_KAMOKU_NM,");
            sql.Append(" A.TAISHAKU_KUBUN,");
            sql.Append(" B.KANJO_KAMOKU_NM AS M_KANJO_KAMOKU_NM,");
            sql.Append(" B.TAISHAKU_KUBUN AS M_TAISHAKU_KUBUN");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHISANHYO_KAMOKU_SETTEI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" B.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD AND");
            sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.Append(" A.KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND");
            sql.Append(" A.GYO_BANGO = @GYO_BANGO AND");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" ORDER BY");
            sql.Append(" A.GYO_BANGO ASC,");
            sql.Append(" A.KANJO_KAMOKU_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, chohyoBunrui);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 6, kamokuBunrui);
            dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 2, gyoBango);

            DataTable dtYoyakuSetteiData = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtYoyakuSetteiData;
        }

        /// <summary>
        /// 指定した科目分類のデータを取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <param name="kamokuBunrui">科目分類番号</param>
        /// <param name="hyojiJuni">表示順位番号</param>
        /// <returns>指定した科目分類のデータ</returns>
        public DataTable GetUpDateMotoJoho(int chohyoBunrui, int kamokuBunrui, int hyojiJuni)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" *");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHISANHYO_KAMOKU_BUNRUI");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.Append(" KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND");
            sql.Append(" HYOJI_JUNI = @HYOJI_JUNI AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 1, chohyoBunrui);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 6, kamokuBunrui);
            dpc.SetParam("@HYOJI_JUNI", SqlDbType.Decimal, 6, hyojiJuni);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);

            DataTable dtUpDateMotoJoho = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtUpDateMotoJoho;
        }

        /// <summary>
        /// 法定準備金の金額を取得
        /// </summary>
        /// <param name="dtFr">該当会計期間の開始日</param>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>指定した科目分類の金額データ</returns>
        public DataTable GetHoteJunbiki(DateTime dtFr, Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shiwakeSyori = Util.ToInt(condition["ShiwakeShurui"]);
            int bumonCdFr = Util.ToInt(condition["BumonFr"]);
            int bumonCdTo = Util.ToInt(condition["BumonTo"]);
            int zeiHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
            DateTime dtTo = (DateTime)condition["DtFr"];
            // 支所コード
            int shishoCd = Util.ToInt(condition["ShishoCode"]);

            sql.Append("SELECT");
            // 税込みの場合
            if (zeiHandan == 1)
            {
                sql.Append(" SUM(CASE WHEN A.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEIKOMI_KINGAKU ELSE 0 END ELSE 0 END) AS JUNBIKIN ");
            }
            // 税抜きの場合
            else
            {
                sql.Append(" SUM(CASE WHEN A.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEINUKI_KINGAKU ELSE 0 END ELSE 0 END) AS JUNBIKIN ");
            }
            sql.Append("FROM");
            sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_ZM_KANJO_KAMOKU AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");

            if (shishoCd != 0)
                sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");

            sql.Append(" A.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO AND");
            sql.Append(" A.DENPYO_DATE < CAST(@KIKAN_TO AS DATETIME) AND");

            //sql.Append(" A.MEISAI_KUBUN <= 0 AND");
            if (zeiHandan == 1)
            {
                sql.Append(" A.MEISAI_KUBUN <= 0 AND");
            }
            else
            {
                sql.Append(" A.MEISAI_KUBUN >= 0 AND ");
            }

            sql.Append(" A.KANJO_KAMOKU_CD = @KAMOKU_CD AND");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            if (shiwakeSyori != 9)
            {
                sql.Append(" AND A.KESSAN_KUBUN = @KESSAN_KUBUN");
            }
            sql.Append(" GROUP BY");
            sql.Append(" A.KANJO_KAMOKU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);

            //dpc.SetParam("@KAMOKU_CD", SqlDbType.Decimal, 6, 404); // 法定準備金の勘定科目CD※現状固定
            dpc.SetParam("@KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToInt(condition["KMK_CD_HOTE_JUNBIKIN"]));

            dpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, shiwakeSyori);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
            dpc.SetParam("@KIKAN_FR", SqlDbType.VarChar, dtFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KIKAN_TO", SqlDbType.VarChar, dtTo.Date.ToString("yyyy/MM/dd"));

            DataTable dt = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dt;
        }

        /// <summary>
        /// 当期未処分剰余金の金額を取得
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>指定した科目分類の金額データ</returns>
        public DataTable GetTokiMishobunJoyokin(Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int zeiHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
            // 支所コード
            int shishoCd = Util.ToInt(condition["ShishoCode"]);

            sql.Append("SELECT");
            // 税込みの場合
            if (zeiHandan == 1)
            {
                sql.Append(" ZEIKOMI_KINGAKU AS KINGAKU ");
            }
            // 税抜きの場合
            else
            {
                sql.Append(" ZEINUKI_KINGAKU AS KINGAKU ");
            }
            sql.Append("FROM");
            sql.Append(" VI_ZM_KIMATSU_ZANDAKA ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");

            if (shishoCd != 0)
                sql.Append(" SHISHO_CD = @SHISHO_CD AND");

            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append(" DENPYO_BANGO = 0 AND");
            sql.Append(" KANJO_KAMOKU_CD = @KAMOKU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo - 1); // 前年度を指定

            //dpc.SetParam("@KAMOKU_CD", SqlDbType.Decimal, 6, TOKI_MISYOBUN_JOYOKIN); // 当期未処分剰余金の勘定科目CD※現状固定
            dpc.SetParam("@KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToInt(condition["KMK_CD_TOKI_MISYOBUN_JOYOKIN"]));

            DataTable dt = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dt;
        }

        /// <summary>
        /// 指定した科目分類の金額を取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <param name="kamokuBunrui">科目分類番号</param>
        /// <returns>指定した科目分類の金額データ</returns>
        public DataTable GetKamokuKingaku(int kamokuCd, Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shiwakeSyori = Util.ToInt(condition["ShiwakeShurui"]);
            int bumonCdFr = Util.ToInt(condition["BumonFr"]);
            int bumonCdTo = Util.ToInt(condition["BumonTo"]);
            int zeiHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
            DateTime dtFr = (DateTime)condition["DtFr"];
            DateTime dtTo = (DateTime)condition["DtTo"];
            // 支所コード
            int shishoCd = Util.ToInt(condition["ShishoCode"]);

            sql.Append(" SELECT");
            sql.Append(" A.KANJO_KAMOKU_CD,");
            // 税込みの場合
            if (zeiHandan == 1)
            {
                sql.Append(" SUM(CASE WHEN A.DENPYO_DATE < CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
                sql.Append(" THEN A.ZEIKOMI_KINGAKU ELSE (A.ZEIKOMI_KINGAKU * -1) END ELSE 0 END) AS ZEN_ZAN,");
                sql.Append(" SUM(CASE WHEN A.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEIKOMI_KINGAKU ELSE 0 END ELSE 0 END) AS KARIKATA,");
                sql.Append(" SUM(CASE WHEN A.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEIKOMI_KINGAKU ELSE 0 END ELSE 0 END) AS KASHIKATA,");
                sql.Append(" SUM(CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
                sql.Append(" THEN A.ZEIKOMI_KINGAKU ELSE (A.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA");
            }
            // 税抜きの場合
            else
            {
                sql.Append(" SUM(CASE WHEN A.DENPYO_DATE < CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
                sql.Append(" THEN A.ZEINUKI_KINGAKU ELSE (A.ZEINUKI_KINGAKU * -1) END ELSE 0 END) AS ZEN_ZAN,");
                sql.Append(" SUM(CASE WHEN A.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEINUKI_KINGAKU ELSE 0 END ELSE 0 END) AS KARIKATA,");
                sql.Append(" SUM(CASE WHEN A.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEINUKI_KINGAKU ELSE 0 END ELSE 0 END) AS KASHIKATA,");
                sql.Append(" SUM(CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
                sql.Append(" THEN A.ZEINUKI_KINGAKU ELSE (A.ZEINUKI_KINGAKU * -1) END) AS ZANDAKA");
            }
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            //sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");

            if (shishoCd != 0)
                sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");

            sql.Append(" A.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO AND");
            sql.Append(" A.DENPYO_DATE <= CAST(@KIKAN_TO AS DATETIME) AND");

            //sql.Append(" A.MEISAI_KUBUN <= 0 AND");
            if (zeiHandan == 1)
            {
                sql.Append(" A.MEISAI_KUBUN <= 0 AND");
            }
            else
            {
                sql.Append(" A.MEISAI_KUBUN >= 0 AND ");
            }

            sql.Append(" A.KANJO_KAMOKU_CD = @KAMOKU_CD AND");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            if (shiwakeSyori != 9)
            {
                sql.Append(" AND A.KESSAN_KUBUN = @KESSAN_KUBUN");
            }
            sql.Append(" GROUP BY");
            sql.Append(" A.KANJO_KAMOKU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KAMOKU_CD", SqlDbType.Decimal, 6, kamokuCd);
            dpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, shiwakeSyori);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
            dpc.SetParam("@KIKAN_FR", SqlDbType.VarChar, dtFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KIKAN_TO", SqlDbType.VarChar, dtTo.Date.ToString("yyyy/MM/dd"));

            DataTable dtUpDateMotoJoho = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtUpDateMotoJoho;
        }

        #region 未使用->コメント
        ///// <summary>
        ///// 補助科目名を取得
        ///// </summary>
        ///// <param name="hojoKamokuCd">補助科目コード</param>
        ///// <returns>補助科目名</returns>
        //public string GetHojoKamokuNm(int kanjoKamoku, int hojoKamoku)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder sql = new StringBuilder();

        //    sql.Append(" SELECT");
        //    sql.Append(" HOJO_KAMOKU_NM");
        //    sql.Append(" FROM");
        //    sql.Append(" TB_ZM_HOJO_KAMOKU");
        //    sql.Append(" WHERE");
        //    sql.Append(" KAISHA_CD = @KAISHA_CD AND");
        //    sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND");
        //    sql.Append(" HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
        //    dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4, kanjoKamoku);
        //    dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 4, hojoKamoku);

        //    DataTable dtHojoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
        //    string hojoKamokuNm = Util.ToString(dtHojoKamoku.Rows[0]["HOJO_KAMOKU_NM"]);

        //    return hojoKamokuNm;
        //}

        ///// <summary>
        ///// 貸借区分、繰越残高を取得
        ///// </summary>
        ///// <param name="condition">条件画面の入力値</param>
        ///// <returns>貸借区分、繰越残高</returns>
        //public DataTable GetKamokuJoho(Hashtable condition, int hojoKamokuCd)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder sql = new StringBuilder();
        //    int kanjoKamoku = Util.ToInt(condition["KanjoKamokuCd"]);

        //    sql.Append(" SELECT");
        //    sql.Append(" A.HOJO_KAMOKU_CD,");
        //    sql.Append(" B.TAISHAKU_KUBUN,");
        //    sql.Append(" SUM(");
        //    sql.Append(" CASE WHEN A.DENPYO_DATE < CAST(@DATE_FR AS DATETIME) THEN");
        //    sql.Append(" CASE WHEN B.TAISHAKU_KUBUN = 1 THEN");
        //    sql.Append(" (CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEIKOMI_KINGAKU ELSE 0 END) -");
        //    sql.Append(" (CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEIKOMI_KINGAKU ELSE 0 END)");
        //    sql.Append(" ELSE");
        //    sql.Append(" (CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEIKOMI_KINGAKU ELSE 0 END) -");
        //    sql.Append(" (CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEIKOMI_KINGAKU ELSE 0 END)");
        //    sql.Append(" END");
        //    sql.Append(" ELSE 0");
        //    sql.Append(" END) AS KURIKOSHI_ZANDAKA");
        //    sql.Append(" FROM");
        //    sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A");
        //    sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ON");
        //    sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
        //    sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
        //    sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO");
        //    sql.Append(" LEFT OUTER JOIN TB_ZM_HOJO_KAMOKU AS C ON");
        //    sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
        //    sql.Append(" A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD AND");
        //    sql.Append(" A.HOJO_KAMOKU_CD = C.HOJO_KAMOKU_CD AND");
        //    sql.Append(" C.KAIKEI_NENDO = @KAIKEI_NENDO");
        //    sql.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS D ON");
        //    sql.Append(" A.KAISHA_CD = D.KAISHA_CD AND");
        //    sql.Append(" A.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD");
        //    sql.Append(" WHERE");
        //    sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
        //    sql.Append(" A.BUMON_CD BETWEEN @BUMON_FR AND @BUMON_TO AND");
        //    sql.Append(" A.KANJO_KAMOKU_CD = @KANJO_KAMOKU AND");
        //    sql.Append(" A.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND");
        //    sql.Append(" A.KOJI_CD BETWEEN @KOJI_FR AND @KOJI_TO AND");
        //    sql.Append(" A.DENPYO_DATE <= CAST(@DATE_TO AS DATETIME) AND");
        //    sql.Append(" A.MEISAI_KUBUN <= 0 AND");
        //    sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
        //    sql.Append(" GROUP BY");
        //    sql.Append(" A.KANJO_KAMOKU_CD,");
        //    sql.Append(" B.KANJO_KAMOKU_NM,");
        //    sql.Append(" A.HOJO_KAMOKU_CD,");
        //    sql.Append(" B.TAISHAKU_KUBUN");
        //    sql.Append(" ORDER BY");
        //    sql.Append(" A.HOJO_KAMOKU_CD");
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
        //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
        //    DateTime dtFr = (DateTime)condition["DtFr"];
        //    dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, dtFr.Date.ToString("yyyy/MM/dd"));
        //    DateTime dtTo = (DateTime)condition["DtTo"];
        //    dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, dtTo.Date.ToString("yyyy/MM/dd"));
        //    dpc.SetParam("@KANJO_KAMOKU", SqlDbType.VarChar, 4, kanjoKamoku);
        //    dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.VarChar, 4, hojoKamokuCd);
        //    dpc.SetParam("@BUMON_FR", SqlDbType.VarChar, 4, Util.ToString(condition["BumonFr"]));
        //    dpc.SetParam("@BUMON_TO", SqlDbType.VarChar, 4, Util.ToString(condition["BumonTo"]));
        //    dpc.SetParam("@KOJI_FR", SqlDbType.VarChar, 4, Util.ToString(condition["KojiFr"]));
        //    dpc.SetParam("@KOJI_TO", SqlDbType.VarChar, 4, Util.ToString(condition["KojiTo"]));
        //    DataTable dtKurikoshiZandaka = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

        //    return dtKurikoshiZandaka;
        //}

        ///// <summary>
        ///// 相手科目コードを取得
        ///// </summary>
        ///// <param name="denpyoBango">伝票番号</param>
        ///// <param name="denpyoData">伝票日付</param>
        ///// <param name="gyoBango">行番号</param>
        ///// <param name="taishakuKubun">貸借区分</param>
        ///// <returns>相手科目名コード</returns>
        //public int GetKanjoKamokuNm(int denpyoBango, string denpyoData, int gyoBango, int taishakuKubun)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder sql = new StringBuilder();

        //    int kamokuCd = 0;

        //    if (taishakuKubun == 1)
        //    {
        //        taishakuKubun = 2;
        //    }
        //    else
        //    {
        //        taishakuKubun = 1;
        //    }

        //    sql.Append(" SELECT");
        //    sql.Append(" KANJO_KAMOKU_CD");
        //    sql.Append(" FROM");
        //    sql.Append(" TB_ZM_SHIWAKE_MEISAI");
        //    sql.Append(" WHERE");
        //    sql.Append(" KAISHA_CD = @KAISHA_CD AND");
        //    sql.Append(" DENPYO_BANGO = @DENPYO_BANGO AND");
        //    sql.Append(" DENPYO_DATE = CAST(@DENPYO_DATE AS DATETIME) AND");
        //    sql.Append(" GYO_BANGO = @GYO_BANGO AND");
        //    sql.Append(" TAISHAKU_KUBUN = @TAISHAKU_KUBUN AND");
        //    sql.Append(" MEISAI_KUBUN <= 0 AND");
        //    sql.Append(" DENPYO_KUBUN = 1");
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
        //    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 4, denpyoBango);
        //    dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, 4, Util.ToDate(denpyoData));
        //    dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 4, gyoBango);
        //    dpc.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 4, taishakuKubun);

        //    DataTable dtKanjoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

        //    if (dtKanjoKamoku.Rows.Count != 0)
        //    {
        //        kamokuCd = Util.ToInt(dtKanjoKamoku.Rows[0]["KANJO_KAMOKU_CD"]);
        //    }

        //    return kamokuCd;
        //}

        ///// <summary>
        ///// 相手科目名を取得
        ///// </summary>
        ///// <param name="kanjoKamoku">勘定科目コード</param>
        ///// <returns>相手科目名</returns>
        //public string GetKanjoKamokuNm(int kanjoKamokuCd)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder sql = new StringBuilder();

        //    sql.Append(" SELECT");
        //    sql.Append(" MAX(B.KANJO_KAMOKU_NM) AS KAMOKU_NM");
        //    sql.Append(" FROM");
        //    sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A");
        //    sql.Append(" LEFT OUTER JOIN");
        //    sql.Append(" TB_ZM_KANJO_KAMOKU AS B");
        //    sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD AND");
        //    sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
        //    sql.Append(" WHERE");
        //    sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
        //    sql.Append(" A.KANJO_KAMOKU_CD = @KANJO_KAMOKU");
        //    sql.Append(" GROUP BY");
        //    sql.Append(" A.KANJO_KAMOKU_CD");
        //    sql.Append(" ORDER BY");
        //    sql.Append(" A.KANJO_KAMOKU_CD");
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
        //    dpc.SetParam("@KANJO_KAMOKU", SqlDbType.Decimal, 4, kanjoKamokuCd);

        //    DataTable dtKanjoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

        //    string kanjoKamoku = " " + kanjoKamokuCd + " " + Util.ToString(dtKanjoKamoku.Rows[0]["KAMOKU_NM"]);

        //    return kanjoKamoku;
        //}

        ///// <summary>
        ///// 相手補助科目名を取得
        ///// </summary>
        ///// <param name="kanjoKamoku">勘定科目コード</param>
        ///// <param name="hojoKamoku">相手補助科目コード</param>
        ///// <returns>相手補助科目名</returns>
        //public string GetAiteHojoKamokuNm(int kanjoKamoku, int hojoKamoku)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder sql = new StringBuilder();
        //    string hojoKamokuNm = "";

        //    if (hojoKamoku > 0)
        //    {
        //        sql.Append(" SELECT");
        //        sql.Append(" HOJO_KAMOKU_NM");
        //        sql.Append(" FROM");
        //        sql.Append(" VI_ZM_HOJO_KAMOKU");
        //        sql.Append(" WHERE");
        //        sql.Append(" KANJO_KAMOKU_CD = " + kanjoKamoku);
        //        sql.Append(" AND HOJO_KAMOKU_CD = " + hojoKamoku);

        //        DataTable dtHojoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

        //        hojoKamokuNm = Util.ToString(dtHojoKamoku.Rows[0]["HOJO_KAMOKU_NM"]);
        //    }

        //    return hojoKamokuNm;
        //}

        ///// <summary>
        ///// 工事名を取得
        ///// </summary>
        ///// <param name="kojiCd">工事コード</param>
        ///// <returns>工事名</returns>
        //public string GetKojiNm(int kojiCd)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder sql = new StringBuilder();
        //    string kojiNm;
        //    sql.Append(" SELECT");
        //    sql.Append(" KOJI_NM");
        //    sql.Append(" FROM");
        //    sql.Append(" TB_ZM_KOJI");
        //    sql.Append(" WHERE");
        //    sql.Append(" KAISHA_CD = @KAISHA_CD AND");
        //    sql.Append(" KOJI_CD = " + kojiCd);
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);

        //    DataTable dtKoji = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

        //    kojiNm = Util.ToString(dtKoji.Rows[0]["KOJI_NM"]);

        //    return kojiNm;
        //}

        ///// <summary>
        ///// 勘定科目を指定した表示データを取得
        ///// </summary>
        ///// <param name="condition">条件画面の入力値</param>
        ///// <param name="kanjoKamoku">勘定科目データ</param>
        ///// <returns>勘定科目を指定した表示データ</returns>
        //public DataTable GetTaishakuData(Hashtable condition, int hojoKamoku)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder sql = new StringBuilder();

        //    int shiwakeShori = Util.ToInt(condition["ShiwakeShurui"]);
        //    int shohizeiShoriHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
        //    int kanjoKamoku = Util.ToInt(condition["KanjoKamokuCd"]);

        //    sql.Append(" SELECT");
        //    sql.Append(" DENPYO_BANGO,");
        //    sql.Append(" GYO_BANGO,");
        //    sql.Append(" TAISHAKU_KUBUN,");
        //    sql.Append(" DENPYO_DATE,");
        //    sql.Append(" KANJO_KAMOKU_CD,");
        //    sql.Append(" KANJO_KAMOKU_NM,");
        //    sql.Append(" HOJO_KAMOKU_CD,");
        //    sql.Append(" HOJO_KAMOKU_NM,");
        //    sql.Append(" AITE_KANJO_KAMOKU_CD,");
        //    sql.Append(" AITE_HOJO_KAMOKU_CD,");
        //    sql.Append(" HOJO_SHIYO_KUBUN,");
        //    sql.Append(" AITE_KOJI_CD,");
        //    sql.Append(" BUMON_CD,");
        //    sql.Append(" BUMON_NM,");
        //    sql.Append(" KOJI_CD,");
        //    sql.Append(" KOJI_NM,");
        //    sql.Append(" TEKIYO_CD,");
        //    sql.Append(" TEKIYO,");
        //    sql.Append(" SHOHYO_BANGO,");
        //    sql.Append(" ZEIKOMI_KINGAKU,");
        //    sql.Append(" ZEINUKI_KINGAKU,");
        //    sql.Append(" SHOHIZEI_KINGAKU");
        //    sql.Append(" FROM");
        //    sql.Append(" VI_ZM_SHIWAKE_MEISAI_AITE");
        //    sql.Append(" WHERE");
        //    sql.Append(" KAISHA_CD = @KAISHA_CD AND");
        //    sql.Append(" BUMON_CD BETWEEN @BUMON_FR AND @BUMON_TO AND");
        //    sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND");
        //    sql.Append(" HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND");
        //    sql.Append(" KOJI_CD BETWEEN @KOJI_FR AND @KOJI_TO AND");
        //    sql.Append(" DENPYO_DATE BETWEEN CAST(@DATE_FR AS DATETIME) AND");
        //    sql.Append(" CAST(@DATE_TO AS DATETIME)");
        //    if (shohizeiShoriHandan == 1)
        //    {
        //        sql.Append(" AND MEISAI_KUBUN <= 0 AND");
        //        sql.Append(" DENPYO_KUBUN = 1");
        //    }
        //    if (shiwakeShori != 9)
        //    {
        //        sql.Append(" AND KESSAN_KUBUN = " + shiwakeShori);
        //    }
        //    sql.Append(" ORDER BY");
        //    sql.Append(" HOJO_KAMOKU_CD, DENPYO_DATE, DENPYO_BANGO, GYO_BANGO");
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
        //    DateTime dtFr = (DateTime)condition["DtFr"];
        //    dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, dtFr.Date.ToString("yyyy/MM/dd"));
        //    DateTime dtTo = (DateTime)condition["DtTo"];
        //    dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, dtTo.Date.ToString("yyyy/MM/dd"));
        //    dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.VarChar, 4, kanjoKamoku);
        //    dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.VarChar, 4, hojoKamoku);
        //    dpc.SetParam("@BUMON_FR", SqlDbType.VarChar, 4, Util.ToString(condition["BumonFr"]));
        //    dpc.SetParam("@BUMON_TO", SqlDbType.VarChar, 4, Util.ToString(condition["BumonTo"]));
        //    dpc.SetParam("@KOJI_FR", SqlDbType.VarChar, 4, Util.ToString(condition["KojiFr"]));
        //    dpc.SetParam("@KOJI_TO", SqlDbType.VarChar, 4, Util.ToString(condition["KojiTo"]));

        //    DataTable dsTaishakuData = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

        //    return dsTaishakuData;
        //}
        #endregion

        #endregion
        #endregion

        #region 五期比較諸表（ベースは合計残高試算表データ作成（既存に合わせた形））

        #region プロパティ
        /// <summary>
        /// 貸借対照表用データテーブル
        /// </summary>
        public DataTable TaishakuTaishohyo
        {
            get
            {
                return this.CreateTableTaisyaku(Taisyaku, "Taisyaku");
            }
        }

        /// <summary>
        /// 損益計算書用データテーブル
        /// </summary>
        public DataTable SonekiKeisansho
        {
            get
            {
                return this.CreateTableSoneki(Soneki, "Soneki");
            }
        }

        /// <summary>
        /// 製造原価報告書用データテーブル
        /// </summary>
        public DataTable SeizouGenkaHoukokusho
        {
            get
            {
                return this.CreateTableSeizou(Seizou, "Seizou");
            }
        }

        private string _KAIKEI_NENDO1 = "";
        public string KAIKEI_NENDO1
        {
            get { return ((_KAIKEI_NENDO1 != "" && _KAIKEI_NENDO1 != "0")  ? Util.ToString(Util.ToInt(_KAIKEI_NENDO1) - 1) : ""); }
        }
        private string _KAIKEI_NENDO2 = "";
        public string KAIKEI_NENDO2
        {
            get { return ((_KAIKEI_NENDO2 != "" && _KAIKEI_NENDO2 != "0") ? Util.ToString(Util.ToInt(_KAIKEI_NENDO2) - 1) : ""); }
        }
        private string _KAIKEI_NENDO3 = "";
        public string KAIKEI_NENDO3
        {
            get { return ((_KAIKEI_NENDO3 != "" && _KAIKEI_NENDO3 !="0") ? Util.ToString(Util.ToInt(_KAIKEI_NENDO3) - 1) : ""); }
        }
        public string KAIKEI_NENDO4
        {
            get { return _KAIKEI_NENDO3; }
        }
        #endregion

        #region 定義

        // 各帳票保持List
        private List<Youyaku> Taisyaku;
        private List<Youyaku> Soneki;
        private List<Youyaku> Seizou;

        // 抽出条件の保持
        private Hashtable _condition;

        private const int cSeizouGenkaCd = -20;
        private const string cSeizouGenkaNm = "当期工事原価";
        private const int cSeizouGenkaKbn = 1;                //貸借区分[1:借方]
        private const int cMiseiSisituCd = -30;
        private const string cMiseiSisituNm = "未成工事支出金";
        private const int cMiseiSisituKbn = 1;                //貸借区分[1:借方]

        // 勘定科目、金額情報List
        private List<Kamoku> 勘定科目;

        // 金額操作用
        [Serializable()]
        class Kingaku
        {
            public decimal 前残1;
            public decimal 前残2;
            public decimal 前残3;
            public decimal 前残;
            public decimal 借方;
            public decimal 貸方;
            public decimal 残高;

            public Kingaku()
            {
                前残1 = 0;
                前残2 = 0;
                前残3 = 0;
                前残 = 0;
                借方 = 0;
                貸方 = 0;
                残高 = 0;
            }
        }

        // 勘定科目操作用
        [Serializable()]
        class Kamoku
        {
            public int コード;
            public string 名称;
            public int 貸借区分;
            public int マスタコード;
            public Kingaku 金額;

            public Kamoku()
            {
                金額 = new Kingaku();
            }
        }

        // 科目設定操作用
        class ItemsTable
        {
            public int 行番号;
            public int Code;
            public string 名称;
            public int 貸借区分;
            public Kingaku 金額;
            public decimal 構成比;
            public int 出力区分;
            public List<Kamoku> 勘定科目;

            public ItemsTable()
            {
                金額 = new Kingaku();
                勘定科目 = new List<Kamoku>();
            }
        }

        // 科目分類操作用
        class Youyaku
        {
            public int 表示順位;
            public int 科目分類;
            public string 科目分類名;
            public int 貸借区分;
            public int 明細項目数;
            public int 明細区分;
            public int 集計区分;
            public string 集計計算式;
            public int 分類区分;
            public int 文字種別;
            public int 帳票区分;
            public int 構成比区分;
            public int 印字区分;
            public Kingaku 金額;
            public decimal 構成比;
            public int 出力区分;
            public List<ItemsTable> 科目明細;

            public Youyaku()
            {
                金額 = new Kingaku();
                科目明細 = new List<ItemsTable>();
            }
        }
        #endregion

        // 集計処理メイン
        public bool Summary(Hashtable condition, string unuqId)
        {
            _condition = condition;

            WK_TABLE = "TB_" + unuqId.Replace("-", "");

            bool[] flg = new bool[3];

            if (!SetShisanhyoRecord())
                return false;

            // 要約設定が無い場合
            if (Seizou == null && Soneki == null && Taisyaku == null)
            {
                return false;
            }

            if (!SetKmkKingk())
                return false;

            // 製造原価報告書
            if (!SetKamokuSyukei(ref Seizou, ref flg[2]))
            {
                return false;
            }
            else
            {
                if (flg[2])
                    if (!SetBunruiSyukei(ref Seizou)) return false;

                SetKouseihi(ref Seizou, 30);
                CheckPrintRecords(ref Seizou, 30);
            }


            // 損益計算書
            if (!SetKamokuSyukei(ref Soneki, ref flg[1]))
            {
                return false;
            }
            else
            {
                if (flg[1])
                    if (!SetBunruiSyukei(ref Soneki)) return false;

                SetKouseihi(ref Soneki, 20);
                CheckPrintRecords(ref Soneki, 20);
            }

            // 貸借対照表
            if (!SetKamokuSyukei(ref Taisyaku, ref flg[0]))
            {
                return false;
            }
            else
            {
                if (flg[1])
                    if (!SetBunruiSyukei(ref Taisyaku)) return false;

                // 貸借対照表(資産の部)の構成比を設定する。
                SetKouseihi(ref Taisyaku, 10);
                CheckPrintRecords(ref Taisyaku, 10);

                // 貸借対照表(負債･資本の部)の構成比を設定する。
                SetKouseihi(ref Taisyaku, 11);
                CheckPrintRecords(ref Taisyaku, 11);
            }

            // 確認用
            try
            {
                int outputxml = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1071", "Setting", "XML_OUTPUT"));
                if (outputxml == 1)
                {
                    CreateTableTaisyaku(Taisyaku, "Taisyaku").WriteXml("Taisyaku.xml");
                    CreateTableSoneki(Soneki, "Soneki").WriteXml("Soneki.xml");
                    CreateTableSeizou(Seizou, "Seizou").WriteXml("Seizou.xml");
                }
            }
            catch (Exception)
            { }

            if (!flg[0] && !flg[1])
                return false;

            return true;
        }

        #region DataTable作成

        // 画面、印刷用DataTable定義
        DataTable CreateTable(string tableName)
        {
            DataTable dt = new DataTable(tableName);

            dt.Columns.Add("KAMOKU_BUNRUI", typeof(decimal));
            dt.Columns.Add("KAMOKU_BUNRUI_NM", typeof(string));
            dt.Columns.Add("SHUKEI_KEISANSHIKI", typeof(string));
            dt.Columns.Add("KAMOKU_BUNRUI_SUB_NM", typeof(string));
            dt.Columns.Add("MOJI_SHUBETSU", typeof(decimal));

            dt.Columns.Add("ZANDAKA_KINGAKUZEN1", typeof(decimal));
            dt.Columns.Add("ZANDAKA_KINGAKUZEN2", typeof(decimal));
            dt.Columns.Add("ZANDAKA_KINGAKUZEN3", typeof(decimal));

            dt.Columns.Add("ZANDAKA_KINGAKU", typeof(decimal));
            dt.Columns.Add("KARIKATA_KINGAKU", typeof(decimal));
            dt.Columns.Add("KASHIKATA_KINGAKU", typeof(decimal));
            dt.Columns.Add("TOUZAN_KINGAKU", typeof(decimal));
            dt.Columns.Add("KANJO_KAMOKU_CD_START", typeof(string));
            dt.Columns.Add("KANJO_KAMOKU_CD_END", typeof(string));
            dt.Columns.Add("OUTPUT_KUBUN", typeof(decimal));

            return dt;
        }

        // 貸借対照表
        DataTable CreateTableTaisyaku(List<Youyaku> yykList, string tableName)
        {
            DataTable dt = null;
            try
            {
                dt = CreateTable(tableName);

                string stkmk = "";
                string edkmk = "";

                // 資産（1P）
                {
                    foreach (Youyaku y in yykList)
                    {
                        if (y.帳票区分 == 10)
                        {
                            if (y.明細項目数 != 0)
                            {
                                foreach (ItemsTable item in y.科目明細)
                                {
                                    var r = dt.NewRow();

                                    r["KAMOKU_BUNRUI"] = y.科目分類;
                                    r["KAMOKU_BUNRUI_NM"] = item.名称; //y.科目分類名;
                                    r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                    r["KAMOKU_BUNRUI_SUB_NM"] = item.名称;
                                    r["MOJI_SHUBETSU"] = 0;

                                    r["ZANDAKA_KINGAKUZEN1"] = item.金額.前残1;
                                    r["ZANDAKA_KINGAKUZEN2"] = item.金額.前残2;
                                    r["ZANDAKA_KINGAKUZEN3"] = item.金額.前残3;

                                    r["ZANDAKA_KINGAKU"] = item.金額.前残;
                                    r["KARIKATA_KINGAKU"] = item.金額.借方;
                                    r["KASHIKATA_KINGAKU"] = item.金額.貸方;
                                    r["TOUZAN_KINGAKU"] = item.金額.残高;
                                    foreach (Kamoku kmk in item.勘定科目)
                                    {
                                        if (stkmk == "")
                                            stkmk = kmk.マスタコード.ToString();
                                        edkmk = kmk.マスタコード.ToString();
                                    }
                                    r["KANJO_KAMOKU_CD_START"] = stkmk;
                                    r["KANJO_KAMOKU_CD_END"] = edkmk;
                                    r["OUTPUT_KUBUN"] = item.出力区分;
                                    dt.Rows.Add(r);
                                    stkmk = "";
                                    edkmk = "";
                                }
                            }
                            if (y.印字区分 == 1)
                            {
                                var r = dt.NewRow();

                                r["KAMOKU_BUNRUI"] = y.科目分類;
                                r["KAMOKU_BUNRUI_NM"] = y.科目分類名;
                                r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                r["MOJI_SHUBETSU"] = y.文字種別;

                                r["ZANDAKA_KINGAKUZEN1"] = y.金額.前残1;
                                r["ZANDAKA_KINGAKUZEN2"] = y.金額.前残2;
                                r["ZANDAKA_KINGAKUZEN3"] = y.金額.前残3;

                                r["ZANDAKA_KINGAKU"] = y.金額.前残;
                                r["KARIKATA_KINGAKU"] = y.金額.借方;
                                r["KASHIKATA_KINGAKU"] = y.金額.貸方;
                                r["TOUZAN_KINGAKU"] = y.金額.残高;
                                r["OUTPUT_KUBUN"] = y.出力区分;
                                dt.Rows.Add(r);
                            }
                        }
                    }
                }

                // 負債・資本（2P）
                {
                    foreach (Youyaku y in yykList)
                    {
                        if (y.帳票区分 == 11)
                        {
                            if (y.明細項目数 != 0)
                            {
                                foreach (ItemsTable item in y.科目明細)
                                {
                                    var r = dt.NewRow();

                                    r["KAMOKU_BUNRUI"] = y.科目分類;
                                    r["KAMOKU_BUNRUI_NM"] = item.名称; //y.科目分類名;
                                    r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                    r["KAMOKU_BUNRUI_SUB_NM"] = item.名称;
                                    r["MOJI_SHUBETSU"] = 0;

                                    r["ZANDAKA_KINGAKUZEN1"] = item.金額.前残1;
                                    r["ZANDAKA_KINGAKUZEN2"] = item.金額.前残2;
                                    r["ZANDAKA_KINGAKUZEN3"] = item.金額.前残3;

                                    r["ZANDAKA_KINGAKU"] = item.金額.前残;
                                    r["KARIKATA_KINGAKU"] = item.金額.借方;
                                    r["KASHIKATA_KINGAKU"] = item.金額.貸方;
                                    r["TOUZAN_KINGAKU"] = item.金額.残高;
                                    foreach (Kamoku kmk in item.勘定科目)
                                    {
                                        if (stkmk == "")
                                            stkmk = kmk.マスタコード.ToString();
                                        edkmk = kmk.マスタコード.ToString();
                                    }
                                    r["KANJO_KAMOKU_CD_START"] = stkmk;
                                    r["KANJO_KAMOKU_CD_END"] = edkmk;
                                    r["OUTPUT_KUBUN"] = item.出力区分;
                                    dt.Rows.Add(r);
                                    stkmk = "";
                                    edkmk = "";
                                }
                            }
                            if (y.印字区分 == 1)
                            {
                                var r = dt.NewRow();

                                r["KAMOKU_BUNRUI"] = y.科目分類;
                                r["KAMOKU_BUNRUI_NM"] = y.科目分類名;
                                r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                r["MOJI_SHUBETSU"] = y.文字種別;

                                r["ZANDAKA_KINGAKUZEN1"] = y.金額.前残1;
                                r["ZANDAKA_KINGAKUZEN2"] = y.金額.前残2;
                                r["ZANDAKA_KINGAKUZEN3"] = y.金額.前残3;

                                r["ZANDAKA_KINGAKU"] = y.金額.前残;
                                r["KARIKATA_KINGAKU"] = y.金額.借方;
                                r["KASHIKATA_KINGAKU"] = y.金額.貸方;
                                r["TOUZAN_KINGAKU"] = y.金額.残高;
                                r["OUTPUT_KUBUN"] = y.出力区分;
                                dt.Rows.Add(r);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return dt;
        }

        // 損益計算書
        DataTable CreateTableSoneki(List<Youyaku> yykList, string tableName)
        {
            DataTable dt = null;
            try
            {
                dt = CreateTable(tableName);

                string stkmk = "";
                string edkmk = "";

                // 1P
                {
                    foreach (Youyaku y in yykList)
                    {
                        if (y.帳票区分 == 20)
                        {
                            if (y.明細項目数 != 0)
                            {
                                foreach (ItemsTable item in y.科目明細)
                                {
                                    var r = dt.NewRow();

                                    r["KAMOKU_BUNRUI"] = y.科目分類;
                                    r["KAMOKU_BUNRUI_NM"] = item.名称; //y.科目分類名;
                                    r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                    r["KAMOKU_BUNRUI_SUB_NM"] = item.名称;
                                    r["MOJI_SHUBETSU"] = 0;

                                    r["ZANDAKA_KINGAKUZEN1"] = item.金額.前残1;
                                    r["ZANDAKA_KINGAKUZEN2"] = item.金額.前残2;
                                    r["ZANDAKA_KINGAKUZEN3"] = item.金額.前残3;

                                    r["ZANDAKA_KINGAKU"] = item.金額.前残;
                                    r["KARIKATA_KINGAKU"] = item.金額.借方;
                                    r["KASHIKATA_KINGAKU"] = item.金額.貸方;
                                    r["TOUZAN_KINGAKU"] = item.金額.残高;
                                    foreach (Kamoku kmk in item.勘定科目)
                                    {
                                        if (stkmk == "")
                                            stkmk = kmk.マスタコード.ToString();
                                        edkmk = kmk.マスタコード.ToString();
                                    }
                                    r["KANJO_KAMOKU_CD_START"] = stkmk;
                                    r["KANJO_KAMOKU_CD_END"] = edkmk;
                                    r["OUTPUT_KUBUN"] = item.出力区分;
                                    dt.Rows.Add(r);
                                    stkmk = "";
                                    edkmk = "";
                                }
                            }
                            if (y.印字区分 == 1)
                            {
                                var r = dt.NewRow();

                                r["KAMOKU_BUNRUI"] = y.科目分類;
                                r["KAMOKU_BUNRUI_NM"] = y.科目分類名;
                                r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                r["MOJI_SHUBETSU"] = y.文字種別;

                                r["ZANDAKA_KINGAKUZEN1"] = y.金額.前残1;
                                r["ZANDAKA_KINGAKUZEN2"] = y.金額.前残2;
                                r["ZANDAKA_KINGAKUZEN3"] = y.金額.前残3;

                                r["ZANDAKA_KINGAKU"] = y.金額.前残;
                                r["KARIKATA_KINGAKU"] = y.金額.借方;
                                r["KASHIKATA_KINGAKU"] = y.金額.貸方;
                                r["TOUZAN_KINGAKU"] = y.金額.残高;
                                r["OUTPUT_KUBUN"] = y.出力区分;
                                dt.Rows.Add(r);
                            }
                        }
                    }
                }

                // 2P
                {
                    foreach (Youyaku y in yykList)
                    {
                        if (y.帳票区分 == 21)
                        {
                            if (y.明細項目数 != 0)
                            {
                                foreach (ItemsTable item in y.科目明細)
                                {
                                    var r = dt.NewRow();

                                    r["KAMOKU_BUNRUI"] = y.科目分類;
                                    r["KAMOKU_BUNRUI_NM"] = item.名称; //y.科目分類名;
                                    r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                    r["KAMOKU_BUNRUI_SUB_NM"] = item.名称;
                                    r["MOJI_SHUBETSU"] = 0;

                                    r["ZANDAKA_KINGAKUZEN1"] = item.金額.前残1;
                                    r["ZANDAKA_KINGAKUZEN2"] = item.金額.前残2;
                                    r["ZANDAKA_KINGAKUZEN3"] = item.金額.前残3;

                                    r["ZANDAKA_KINGAKU"] = item.金額.前残;
                                    r["KARIKATA_KINGAKU"] = item.金額.借方;
                                    r["KASHIKATA_KINGAKU"] = item.金額.貸方;
                                    r["TOUZAN_KINGAKU"] = item.金額.残高;
                                    foreach (Kamoku kmk in item.勘定科目)
                                    {
                                        if (stkmk == "")
                                            stkmk = kmk.マスタコード.ToString();
                                        edkmk = kmk.マスタコード.ToString();
                                    }
                                    r["KANJO_KAMOKU_CD_START"] = stkmk;
                                    r["KANJO_KAMOKU_CD_END"] = edkmk;
                                    r["OUTPUT_KUBUN"] = item.出力区分;
                                    dt.Rows.Add(r);
                                    stkmk = "";
                                    edkmk = "";
                                }
                            }
                            if (y.印字区分 == 1)
                            {
                                var r = dt.NewRow();

                                r["KAMOKU_BUNRUI"] = y.科目分類;
                                r["KAMOKU_BUNRUI_NM"] = y.科目分類名;
                                r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                r["MOJI_SHUBETSU"] = y.文字種別;

                                r["ZANDAKA_KINGAKUZEN1"] = y.金額.前残1;
                                r["ZANDAKA_KINGAKUZEN2"] = y.金額.前残2;
                                r["ZANDAKA_KINGAKUZEN3"] = y.金額.前残3;

                                r["ZANDAKA_KINGAKU"] = y.金額.前残;
                                r["KARIKATA_KINGAKU"] = y.金額.借方;
                                r["KASHIKATA_KINGAKU"] = y.金額.貸方;
                                r["TOUZAN_KINGAKU"] = y.金額.残高;
                                r["OUTPUT_KUBUN"] = y.出力区分;
                                dt.Rows.Add(r);
                            }
                        }
                    }
                }

                // 3P
                {
                    foreach (Youyaku y in yykList)
                    {
                        if (y.帳票区分 == 22)
                        {
                            if (y.明細項目数 != 0)
                            {
                                foreach (ItemsTable item in y.科目明細)
                                {
                                    var r = dt.NewRow();

                                    r["KAMOKU_BUNRUI"] = y.科目分類;
                                    r["KAMOKU_BUNRUI_NM"] = item.名称; //y.科目分類名;
                                    r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                    r["KAMOKU_BUNRUI_SUB_NM"] = item.名称;
                                    r["MOJI_SHUBETSU"] = 0;

                                    r["ZANDAKA_KINGAKUZEN1"] = item.金額.前残1;
                                    r["ZANDAKA_KINGAKUZEN2"] = item.金額.前残2;
                                    r["ZANDAKA_KINGAKUZEN3"] = item.金額.前残3;

                                    r["ZANDAKA_KINGAKU"] = item.金額.前残;
                                    r["KARIKATA_KINGAKU"] = item.金額.借方;
                                    r["KASHIKATA_KINGAKU"] = item.金額.貸方;
                                    r["TOUZAN_KINGAKU"] = item.金額.残高;
                                    foreach (Kamoku kmk in item.勘定科目)
                                    {
                                        if (stkmk == "")
                                            stkmk = kmk.マスタコード.ToString();
                                        edkmk = kmk.マスタコード.ToString();
                                    }
                                    r["KANJO_KAMOKU_CD_START"] = stkmk;
                                    r["KANJO_KAMOKU_CD_END"] = edkmk;
                                    r["OUTPUT_KUBUN"] = item.出力区分;
                                    dt.Rows.Add(r);
                                    stkmk = "";
                                    edkmk = "";
                                }
                            }
                            if (y.印字区分 == 1)
                            {
                                var r = dt.NewRow();

                                r["KAMOKU_BUNRUI"] = y.科目分類;
                                r["KAMOKU_BUNRUI_NM"] = y.科目分類名;
                                r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                r["MOJI_SHUBETSU"] = y.文字種別;

                                r["ZANDAKA_KINGAKUZEN1"] = y.金額.前残1;
                                r["ZANDAKA_KINGAKUZEN2"] = y.金額.前残2;
                                r["ZANDAKA_KINGAKUZEN3"] = y.金額.前残3;

                                r["ZANDAKA_KINGAKU"] = y.金額.前残;
                                r["KARIKATA_KINGAKU"] = y.金額.借方;
                                r["KASHIKATA_KINGAKU"] = y.金額.貸方;
                                r["TOUZAN_KINGAKU"] = y.金額.残高;
                                r["OUTPUT_KUBUN"] = y.出力区分;
                                dt.Rows.Add(r);
                            }
                        }
                    }
                }

                // 4P
                {
                    foreach (Youyaku y in yykList)
                    {
                        if (y.帳票区分 == 23)
                        {
                            if (y.明細項目数 != 0)
                            {
                                foreach (ItemsTable item in y.科目明細)
                                {
                                    var r = dt.NewRow();

                                    r["KAMOKU_BUNRUI"] = y.科目分類;
                                    r["KAMOKU_BUNRUI_NM"] = item.名称; //y.科目分類名;
                                    r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                    r["KAMOKU_BUNRUI_SUB_NM"] = item.名称;
                                    r["MOJI_SHUBETSU"] = 0;

                                    r["ZANDAKA_KINGAKUZEN1"] = item.金額.前残1;
                                    r["ZANDAKA_KINGAKUZEN2"] = item.金額.前残2;
                                    r["ZANDAKA_KINGAKUZEN3"] = item.金額.前残3;

                                    r["ZANDAKA_KINGAKU"] = item.金額.前残;
                                    r["KARIKATA_KINGAKU"] = item.金額.借方;
                                    r["KASHIKATA_KINGAKU"] = item.金額.貸方;
                                    r["TOUZAN_KINGAKU"] = item.金額.残高;
                                    foreach (Kamoku kmk in item.勘定科目)
                                    {
                                        if (stkmk == "")
                                            stkmk = kmk.マスタコード.ToString();
                                        edkmk = kmk.マスタコード.ToString();
                                    }
                                    r["KANJO_KAMOKU_CD_START"] = stkmk;
                                    r["KANJO_KAMOKU_CD_END"] = edkmk;
                                    r["OUTPUT_KUBUN"] = item.出力区分;
                                    dt.Rows.Add(r);
                                    stkmk = "";
                                    edkmk = "";
                                }
                            }
                            if (y.印字区分 == 1)
                            {
                                var r = dt.NewRow();

                                r["KAMOKU_BUNRUI"] = y.科目分類;
                                r["KAMOKU_BUNRUI_NM"] = y.科目分類名;
                                r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                r["MOJI_SHUBETSU"] = y.文字種別;

                                r["ZANDAKA_KINGAKUZEN1"] = y.金額.前残1;
                                r["ZANDAKA_KINGAKUZEN2"] = y.金額.前残2;
                                r["ZANDAKA_KINGAKUZEN3"] = y.金額.前残3;

                                r["ZANDAKA_KINGAKU"] = y.金額.前残;
                                r["KARIKATA_KINGAKU"] = y.金額.借方;
                                r["KASHIKATA_KINGAKU"] = y.金額.貸方;
                                r["TOUZAN_KINGAKU"] = y.金額.残高;
                                r["OUTPUT_KUBUN"] = y.出力区分;
                                dt.Rows.Add(r);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return dt;
        }

        // 製造原価報告書
        DataTable CreateTableSeizou(List<Youyaku> yykList, string tableName)
        {
            DataTable dt = null;
            try
            {
                dt = CreateTable(tableName);

                string stkmk = "";
                string edkmk = "";

                // 1P
                {
                    foreach (Youyaku y in yykList)
                    {
                        if (y.帳票区分 == 30)
                        {
                            if (y.明細項目数 != 0)
                            {
                                foreach (ItemsTable item in y.科目明細)
                                {
                                    var r = dt.NewRow();

                                    r["KAMOKU_BUNRUI"] = y.科目分類;
                                    r["KAMOKU_BUNRUI_NM"] = Util.ToString(item.名称); //y.科目分類名;
                                    r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                    r["KAMOKU_BUNRUI_SUB_NM"] = Util.ToString(item.名称);
                                    r["MOJI_SHUBETSU"] = 0;
                                    r["ZANDAKA_KINGAKU"] = item.金額.前残;
                                    r["KARIKATA_KINGAKU"] = item.金額.借方;
                                    r["KASHIKATA_KINGAKU"] = item.金額.貸方;
                                    r["TOUZAN_KINGAKU"] = item.金額.残高;
                                    foreach (Kamoku kmk in item.勘定科目)
                                    {
                                        if (stkmk == "")
                                            stkmk = kmk.マスタコード.ToString();
                                        edkmk = kmk.マスタコード.ToString();
                                    }
                                    r["KANJO_KAMOKU_CD_START"] = stkmk;
                                    r["KANJO_KAMOKU_CD_END"] = edkmk;
                                    r["OUTPUT_KUBUN"] = item.出力区分;
                                    dt.Rows.Add(r);
                                    stkmk = "";
                                    edkmk = "";
                                }
                            }
                            if (y.印字区分 == 1)
                            {
                                var r = dt.NewRow();

                                r["KAMOKU_BUNRUI"] = y.科目分類;
                                r["KAMOKU_BUNRUI_NM"] = y.科目分類名;
                                r["SHUKEI_KEISANSHIKI"] = y.集計計算式;
                                r["MOJI_SHUBETSU"] = y.文字種別;
                                r["ZANDAKA_KINGAKU"] = y.金額.前残;
                                r["KARIKATA_KINGAKU"] = y.金額.借方;
                                r["KASHIKATA_KINGAKU"] = y.金額.貸方;
                                r["TOUZAN_KINGAKU"] = y.金額.残高;
                                r["OUTPUT_KUBUN"] = y.出力区分;
                                dt.Rows.Add(r);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return dt;
        }
        #endregion

        #region privateメソッド

        // 要約設定情報の保持メイン
        private bool SetShisanhyoRecord()
        {
            // 貸借対照表設定
            if (!SetTable(1, ref Taisyaku))
                return false;

            // 損益計算書設定
            if (!SetTable(2, ref Soneki))
                return false;

            // 製造原価報告書設定
            if (!SetTable(3, ref Seizou))
                return false;

            return true;
        }

        // 要約設定情報の保持
        private bool SetTable(int kubun, ref List<Youyaku> s)
        {
            DataTable Tbl = null;
            DataTable t = null;

            // ﾃｰﾌﾞﾙ決算書科目分類を読込む。
            if (SelectShisanhyo(kubun, ref Tbl))
            {
                s = new List<Youyaku>();
                foreach (DataRow item in Tbl.Rows)
                {
                    Youyaku y = new Youyaku();
                    y.表示順位 = Util.ToInt(item["HYOJI_JUNI"]);
                    y.科目分類 = Util.ToInt(item["KAMOKU_BUNRUI"]);
                    y.科目分類名 = Util.ToString(item["KAMOKU_BUNRUI_NM"]);
                    y.貸借区分 = Util.ToInt(item["TAISHAKU_KUBUN"]);
                    y.明細項目数 = Util.ToInt(item["MEISAI_KOMOKUSU"]);
                    y.明細区分 = Util.ToInt(item["MEISAI_KUBUN"]);
                    y.集計区分 = Util.ToInt(item["SHUKEI_KUBUN"]);
                    y.集計計算式 = Util.ToString(item["SHUKEI_KEISANSHIKI"]);
                    y.分類区分 = Util.ToInt(item["BUNRUI_KUBUN"]);
                    y.文字種別 = Util.ToInt(item["MOJI_SHUBETSU"]);
                    y.帳票区分 = Util.ToInt(item["CHOHYO_KUBUN"]);

                    y.構成比区分 = Util.ToInt(item["KOSEIHI_KUBUN"]);
                    y.印字区分 = Util.ToInt(item["INJI_KUBUN"]);

                    if (y.明細区分 != 0 || y.明細項目数 != 0)
                    {
                        // ﾃｰﾌﾞﾙ決算書科目設定を読込む。 帳票分類, 科目分類, 行番号, 勘定科目コード
                        if (SelectKamoku(kubun, y.科目分類, ref t))
                        {
                        }

                        int len = y.明細項目数 > 0 ? (t.Rows.Count > y.明細項目数 ? y.明細項目数 : y.明細項目数 - 1) : 0;
                        len += 1;

                        int row = 0;
                        bool yFlags = false;
                        int yFlagsCount = 0;

                        for (int j = 0; j < len; j++)
                        {
                            ItemsTable it = new ItemsTable();
                            Kamoku ik = new Kamoku();

                            if (row <= j && row < t.Rows.Count)
                            {
                                yFlags = false;

                                it = y.科目明細.Find(itm => itm.行番号 == Util.ToInt(t.Rows[row]["GYO_BANGO"]));
                                if (it == null)
                                {
                                    it = new ItemsTable();
                                    it.行番号 = Util.ToInt(t.Rows[row]["GYO_BANGO"]);
                                    it.Code = Util.ToInt(t.Rows[row]["KANJO_KAMOKU_CD"]);
                                    it.名称 = Util.ToString(t.Rows[row]["KANJO_KAMOKU_NM"]);
                                    it.貸借区分 = Util.ToInt(t.Rows[row]["TAISHAKU_KUBUN"]);
                                }
                                else
                                {
                                    yFlags = true;
                                    yFlagsCount += 1;
                                }

                                ik.コード = Util.ToInt(t.Rows[row]["KANJO_KAMOKU_CD"]);
                                ik.名称 = Util.ToString(t.Rows[row]["M_KANJO_KAMOKU_NM"]);
                                ik.貸借区分 = Util.ToInt(t.Rows[row]["M_TAISHAKU_KUBUN"]);
                                ik.マスタコード = Util.ToInt(t.Rows[row]["M_KANJO_KAMOKU_CD"]);

                                if (Util.ToInt(t.Rows[row]["KANJO_KAMOKU_CD"]) == cSeizouGenkaCd)
                                {
                                    ik.名称 = cSeizouGenkaNm;
                                    ik.貸借区分 = cSeizouGenkaKbn;
                                }
                                if (Util.ToInt(t.Rows[row]["KANJO_KAMOKU_CD"]) == cMiseiSisituCd)
                                {
                                    ik.名称 = cMiseiSisituNm;
                                    ik.貸借区分 = cMiseiSisituKbn;
                                }
                                row += 1;
                            }

                            it.勘定科目.Add(ik);

                            if (!yFlags)
                            {
                                if (y.科目明細.Count == (it.行番号 - 1))
                                {
                                    y.科目明細.Add(it);
                                }
                                else
                                {
                                    int stat = y.科目明細.Count;
                                    int line = it.行番号 - 1;

                                    if (stat < y.明細項目数)
                                    {
                                        for (int yIndex = stat; yIndex < line; yIndex++)
                                        {
                                            y.科目明細.Add(new ItemsTable());
                                        }
                                        y.科目明細.Add(it);
                                    }
                                    else if (stat == 0 && y.明細項目数 == 0)
                                    {
                                        y.科目明細.Add(it);
                                    }
                                }
                            }
                        }

                        if (yFlagsCount != 0)
                        {
                            yFlagsCount = y.明細項目数 - y.科目明細.Count;
                            for (int yIndex = 0; yIndex < yFlagsCount; yIndex++)
                            {
                                y.科目明細.Add(new ItemsTable());
                            }
                        }
                    }

                    s.Add(y);
                }
            }

            return true;
        }

        // 科目分類の抽出
        private bool SelectShisanhyo(int kubun, ref DataTable t)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" *");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHISANHYO_KAMOKU_BUNRUI");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.Append(" SHIYO_KUBUN = 1 AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 1, kubun);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);

            t = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            if (t.Rows.Count > 0)
                return true;

            return false;
        }

        // 科目設定及び関連勘定科目情報の抽出
        private bool SelectKamoku(int kubun, int kamokuBunrui, ref DataTable t)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" A.KAMOKU_BUNRUI,");
            sql.Append(" A.GYO_BANGO,");
            sql.Append(" A.KANJO_KAMOKU_CD,");
            sql.Append(" A.KANJO_KAMOKU_NM,");
            sql.Append(" A.TAISHAKU_KUBUN,");
            sql.Append(" B.KANJO_KAMOKU_CD AS M_KANJO_KAMOKU_CD,");
            sql.Append(" B.KANJO_KAMOKU_NM AS M_KANJO_KAMOKU_NM,");
            sql.Append(" B.TAISHAKU_KUBUN AS M_TAISHAKU_KUBUN");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHISANHYO_KAMOKU_SETTEI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON B.KAISHA_CD = A.KAISHA_CD AND");
            sql.Append(" B.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD AND");
            sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.Append(" A.KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" ORDER BY");
            sql.Append(" A.GYO_BANGO ASC,");
            sql.Append(" A.KANJO_KAMOKU_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, kubun);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 6, kamokuBunrui);

            t = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            if (t.Rows.Count > 0)
                return true;

            return false;
        }

        // 科目、金額情報の保持
        private bool SetKmkKingk()
        {
            // 勘定科目単位の金額を集計する。
            DataTable t = null;
            if (SelectData(ref t))
            {
                勘定科目 = new List<Kamoku>();
                foreach (DataRow r in t.Rows)
                {
                    Kamoku k = new Kamoku();
                    k.コード = Util.ToInt(r["KANJO_KAMOKU_CD"]);
                    k.貸借区分 = Util.ToInt(r["TAISHAKU_KUBUN"]);

                    k.金額.前残1 = Util.ToDecimal(r["ZEN_ZAN1"]);
                    k.金額.前残2 = Util.ToDecimal(r["ZEN_ZAN2"]);
                    k.金額.前残3 = Util.ToDecimal(r["ZEN_ZAN3"]);

                    k.金額.前残 = Util.ToDecimal(r["ZEN_ZAN"]);
                    k.金額.借方 = Util.ToDecimal(r["KARIKATA"]);
                    k.金額.貸方 = Util.ToDecimal(r["KASHIKATA"]);
                    k.金額.残高 = Util.ToDecimal(r["ZANDAKA"]);
                    勘定科目.Add(k);
                }
            }

            return true;
        }

        // 仕訳明細より勘定科目毎の金額抽出
        private bool SelectData(ref DataTable t)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shiwakeSyori = Util.ToInt(_condition["ShiwakeShurui"]);
            int bumonCdFr = Util.ToInt(_condition["BumonFr"]);
            int bumonCdTo = Util.ToInt(_condition["BumonTo"]);
            int zeiHandan = Util.ToInt(_condition["ShohizeiShoriHandan"]);
            DateTime dtFr = (DateTime)_condition["DtFr"];
            DateTime dtTo = (DateTime)_condition["DtTo"];
            // 支所コード
            int shishoCd = Util.ToInt(_condition["ShishoCode"]);

            #region ワークテーブル抽出SQL
            List<int> KAIKEI_NENDO = new List<int>();
            KAIKEI_NENDO.Add(0);
            KAIKEI_NENDO.Add(0);
            KAIKEI_NENDO.Add(0);
            try
            {
                // 無条件に削除
                this._dba.ModifyBySql("DROP TABLE " + WK_TABLE, dpc);
            }
            catch (Exception) { }
            DbParamCollection insdpc = new DbParamCollection();
            insdpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this._uInfo.KaishaCd);
            insdpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            DataTable dtCheck = this._dba.GetDataTableByConditionWithParams(
                "*",
                "TB_ZM_KAISHA_JOHO",
                "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO < @KAIKEI_NENDO ",
                "KAIKEI_NENDO DESC",
                insdpc);
            if (dtCheck.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                int iCnt = 2;
                foreach (DataRow r in dtCheck.Rows)
                {
                    KAIKEI_NENDO[iCnt] = Util.ToInt(r["KAIKEI_NENDO"]);
                    iCnt--;
                    if (iCnt < 0) break;
                }

                this._KAIKEI_NENDO1 = KAIKEI_NENDO[0].ToString();
                this._KAIKEI_NENDO2 = KAIKEI_NENDO[1].ToString();
                this._KAIKEI_NENDO3 = KAIKEI_NENDO[2].ToString();
            }

            insdpc = new DbParamCollection();
            string colName = "ZEIKOMI_KINGAKU";
            if (zeiHandan == 1)
            {
                colName = "A.ZEIKOMI_KINGAKU";
            }
            else
            {
                colName = "A.ZEINUKI_KINGAKU";
            }
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append(" A.KANJO_KAMOKU_CD,");
            sql.Append(" MIN(B.TAISHAKU_KUBUN) AS TAISHAKU_KUBUN,");
            sql.Append(" SUM(CASE WHEN A.KAIKEI_NENDO = " + KAIKEI_NENDO[0].ToString() + " ");
            sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.Append(" THEN " + colName + " ELSE (" + colName + " * -1) END ELSE 0 END) AS ZEN_ZAN1,");
            sql.Append(" SUM(CASE WHEN A.KAIKEI_NENDO = " + KAIKEI_NENDO[1].ToString() + " ");
            sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.Append(" THEN " + colName + " ELSE (" + colName + " * -1) END ELSE 0 END) AS ZEN_ZAN2,");
            sql.Append(" SUM(CASE WHEN A.KAIKEI_NENDO = " + KAIKEI_NENDO[2].ToString() + " ");
            sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.Append(" THEN " + colName + " ELSE (" + colName + " * -1) END ELSE 0 END) AS ZEN_ZAN3 ");

            sql.Append(" INTO " + WK_TABLE + " ");

            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            sql.Append(" A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.DENPYO_BANGO = 0 AND "); // 期首のみ

            if (shishoCd != 0)
                sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");

            sql.Append(" A.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO AND");

            if (zeiHandan == 1)
            {
                sql.Append(" A.MEISAI_KUBUN <= 0 AND");
            }
            else
            {
                sql.Append(" A.MEISAI_KUBUN >= 0 AND ");
            }

            sql.Append(" A.KAIKEI_NENDO IN(@KAIKEI_NENDO1, @KAIKEI_NENDO2, @KAIKEI_NENDO3)");
            if (shiwakeSyori != 9)
            {
                sql.Append(" AND A.KESSAN_KUBUN = @KESSAN_KUBUN");
            }

            sql.Append(" AND A.KANJO_KAMOKU_CD IN(SELECT S.KANJO_KAMOKU_CD FROM TB_ZM_SHISANHYO_KAMOKU_SETTEI AS S ");
            sql.Append("                          WHERE S.KAISHA_CD = @KAISHA_CD AND S.KAIKEI_NENDO IN(@KAIKEI_NENDO1, @KAIKEI_NENDO2, @KAIKEI_NENDO3))");

            sql.Append(" GROUP BY");
            sql.Append(" A.KANJO_KAMOKU_CD");
            insdpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            insdpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            insdpc.SetParam("@KAIKEI_NENDO1", SqlDbType.Decimal, 4, KAIKEI_NENDO[0]);
            insdpc.SetParam("@KAIKEI_NENDO2", SqlDbType.Decimal, 4, KAIKEI_NENDO[1]);
            insdpc.SetParam("@KAIKEI_NENDO3", SqlDbType.Decimal, 4, KAIKEI_NENDO[2]);
            insdpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, shiwakeSyori);
            insdpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
            insdpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
            insdpc.SetParam("@KIKAN_FR", SqlDbType.VarChar, dtFr.Date.ToString("yyyy/MM/dd"));
            insdpc.SetParam("@KIKAN_TO", SqlDbType.VarChar, dtTo.Date.ToString("yyyy/MM/dd"));
            try
            {
                int cnt = this._dba.ModifyBySql(sql.ToString(), insdpc);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            #endregion

            #region 金額情報の抽出
            sql = new StringBuilder();

            // 試算表との差異は試算表は１期のみだが五期分の為、科目が使われない歳を考慮して科目マスタをベースに抽出へ
            sql.Append(" SELECT");
            sql.Append(" A.KANJO_KAMOKU_CD,");
            sql.Append(" MIN(A.TAISHAKU_KUBUN) AS TAISHAKU_KUBUN,");
            // 税込みの場合
            if (zeiHandan == 1)
            {
                sql.Append(" SUM(CASE WHEN B.DENPYO_DATE < CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
                sql.Append(" THEN B.ZEIKOMI_KINGAKU ELSE (B.ZEIKOMI_KINGAKU * -1) END ELSE 0 END) AS ZEN_ZAN,");
                sql.Append(" SUM(CASE WHEN B.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN B.TAISHAKU_KUBUN = 1 THEN B.ZEIKOMI_KINGAKU ELSE 0 END ELSE 0 END) AS KARIKATA,");
                sql.Append(" SUM(CASE WHEN B.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN B.TAISHAKU_KUBUN = 2 THEN B.ZEIKOMI_KINGAKU ELSE 0 END ELSE 0 END) AS KASHIKATA,");
                sql.Append(" SUM(CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
                sql.Append(" THEN B.ZEIKOMI_KINGAKU ELSE (B.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA");
            }
            // 税抜きの場合
            else
            {
                sql.Append(" SUM(CASE WHEN B.DENPYO_DATE < CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
                sql.Append(" THEN B.ZEINUKI_KINGAKU ELSE (B.ZEINUKI_KINGAKU * -1) END ELSE 0 END) AS ZEN_ZAN,");
                sql.Append(" SUM(CASE WHEN B.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN B.TAISHAKU_KUBUN = 1 THEN B.ZEINUKI_KINGAKU ELSE 0 END ELSE 0 END) AS KARIKATA,");
                sql.Append(" SUM(CASE WHEN B.DENPYO_DATE >= CAST(@KIKAN_FR AS DATETIME)");
                sql.Append(" THEN CASE WHEN B.TAISHAKU_KUBUN = 2 THEN B.ZEINUKI_KINGAKU ELSE 0 END ELSE 0 END) AS KASHIKATA,");
                sql.Append(" SUM(CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
                sql.Append(" THEN B.ZEINUKI_KINGAKU ELSE (B.ZEINUKI_KINGAKU * -1) END) AS ZANDAKA");
            }
            sql.Append(" ,");
            sql.Append(" MIN(ISNULL(Z.ZEN_ZAN1, 0)) AS ZEN_ZAN1,");
            sql.Append(" MIN(ISNULL(Z.ZEN_ZAN2, 0)) AS ZEN_ZAN2,");
            sql.Append(" MIN(ISNULL(Z.ZEN_ZAN3, 0)) AS ZEN_ZAN3 ");

            sql.Append(" FROM");
            sql.Append(" TB_ZM_KANJO_KAMOKU AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            sql.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");

            if (shishoCd != 0)
                sql.Append(" AND B.SHISHO_CD = @SHISHO_CD");

            sql.Append(" AND B.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND B.DENPYO_DATE <= CAST(@KIKAN_TO AS DATETIME)");
            if (zeiHandan == 1)
                sql.Append(" AND B.MEISAI_KUBUN <= 0");
            else
                sql.Append(" AND B.MEISAI_KUBUN >= 0");
            sql.Append(" AND B.KAIKEI_NENDO = @KAIKEI_NENDO");
            if (shiwakeSyori != 9)
                sql.Append(" AND B.KESSAN_KUBUN = @KESSAN_KUBUN");

            sql.Append(" AND B.KANJO_KAMOKU_CD IN(SELECT S.KANJO_KAMOKU_CD FROM TB_ZM_SHISANHYO_KAMOKU_SETTEI AS S ");
            sql.Append("                          WHERE S.KAISHA_CD = @KAISHA_CD AND S.KAIKEI_NENDO = @KAIKEI_NENDO)");

            sql.Append(" LEFT OUTER JOIN " + WK_TABLE + " AS Z ");
            sql.Append(" ON A.KANJO_KAMOKU_CD = Z.KANJO_KAMOKU_CD ");

            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD ");
            sql.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");

            sql.Append(" AND A.KANJO_KAMOKU_CD IN(SELECT S.KANJO_KAMOKU_CD FROM TB_ZM_SHISANHYO_KAMOKU_SETTEI AS S ");
            sql.Append("                          WHERE S.KAISHA_CD = @KAISHA_CD AND S.KAIKEI_NENDO = @KAIKEI_NENDO)");

            sql.Append(" GROUP BY");
            sql.Append(" A.KANJO_KAMOKU_CD");
            #endregion

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, shiwakeSyori);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
            dpc.SetParam("@KIKAN_FR", SqlDbType.VarChar, dtFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KIKAN_TO", SqlDbType.VarChar, dtTo.Date.ToString("yyyy/MM/dd"));

            t = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            try
            {
                // 無条件に削除
                this._dba.ModifyBySql("DROP TABLE " + WK_TABLE, dpc);
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            if (t.Rows.Count > 0)
                return true;

            return false;
        }

        private bool SetKamokuSyukei(ref List<Youyaku> s, ref bool flg)
        {
            try
            {
                Kingaku kin;
                flg = false;
                foreach (Youyaku i in s)
                {
                    if (i.明細区分 != 0 || i.明細項目数 != 0)
                    {
                        foreach (ItemsTable j in i.科目明細)
                        {
                            foreach (Kamoku k in j.勘定科目)
                            {
                                kin = new Kingaku();
                                if (k.コード > 0)
                                {
                                    int kmk = k.コード;
                                    var q = from a in 勘定科目
                                            where a.コード == kmk
                                            select a;
                                    if (q.Count<Kamoku>() != 0)
                                    {
                                        k.貸借区分 = q.ElementAtOrDefault(0).貸借区分;

                                        kin.前残1 = q.ElementAtOrDefault(0).金額.前残1;
                                        kin.前残2 = q.ElementAtOrDefault(0).金額.前残2;
                                        kin.前残3 = q.ElementAtOrDefault(0).金額.前残3;

                                        kin.前残 = q.ElementAtOrDefault(0).金額.前残;
                                        kin.借方 = q.ElementAtOrDefault(0).金額.借方;
                                        kin.貸方 = q.ElementAtOrDefault(0).金額.貸方;
                                        kin.残高 = q.ElementAtOrDefault(0).金額.残高;
                                    }
                                }
                                else
                                {
                                    // 合計科目の金額を取得する。(未成工事支出金､当期工事原価)
                                    //Console.WriteLine("ゼロ");
                                }

                                if (k.貸借区分 > 0)
                                {
                                    // 要約科目の金額を集計する。

                                    j.金額.前残1 += (j.貸借区分 == k.貸借区分 ? kin.前残1 : kin.前残1 * -1);
                                    j.金額.前残2 += (j.貸借区分 == k.貸借区分 ? kin.前残2 : kin.前残2 * -1);
                                    j.金額.前残3 += (j.貸借区分 == k.貸借区分 ? kin.前残3 : kin.前残3 * -1);

                                    j.金額.前残 += (j.貸借区分 == k.貸借区分 ? kin.前残 : kin.前残 * -1);
                                    j.金額.借方 += (j.貸借区分 == k.貸借区分 ? kin.借方 : kin.借方 * -1);
                                    j.金額.貸方 += (j.貸借区分 == k.貸借区分 ? kin.貸方 : kin.貸方 * -1);
                                    j.金額.残高 += (j.貸借区分 == k.貸借区分 ? kin.残高 : kin.残高 * -1);
                                    // 科目分類の金額を集計する。

                                    i.金額.前残1 += (i.貸借区分 == k.貸借区分 ? kin.前残1 : kin.前残1 * -1);
                                    i.金額.前残2 += (i.貸借区分 == k.貸借区分 ? kin.前残2 : kin.前残2 * -1);
                                    i.金額.前残3 += (i.貸借区分 == k.貸借区分 ? kin.前残3 : kin.前残3 * -1);

                                    i.金額.前残 += (i.貸借区分 == k.貸借区分 ? kin.前残 : kin.前残 * -1);
                                    i.金額.借方 += (i.貸借区分 == k.貸借区分 ? kin.借方 : kin.借方 * -1);
                                    i.金額.貸方 += (i.貸借区分 == k.貸借区分 ? kin.貸方 : kin.貸方 * -1);
                                    i.金額.残高 += (i.貸借区分 == k.貸借区分 ? kin.残高 : kin.残高 * -1);
                                    flg = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return true;
        }

        private bool SetBunruiSyukei(ref List<Youyaku> s)
        {
            Kingaku kin;
            foreach (Youyaku i in s)
            {
                if (i.集計区分 != 0)
                {
                    kin = new Kingaku();

                    SetKeisansiki(i.集計計算式, ref kin);
                    if (i.集計区分 == 3)
                    {
                        kin.借方 = 0; kin.貸方 = 0;
                        var Kingk = kin.残高 - kin.前残;
                        if (Kingk < 0)
                        {
                            Kingk = Math.Abs(Kingk);
                            if (i.貸借区分 == 1)
                            {
                                kin.貸方 = Kingk;
                            }
                            else
                            {
                                kin.借方 = Kingk;
                            }
                        }
                        else
                        {
                            if (i.貸借区分 == 1)
                            {
                                kin.借方 = Kingk;
                            }
                            else
                            {
                                kin.貸方 = Kingk;
                            }
                        }
                    }
                    i.金額 = kin;
                }
            }

            return true;
        }

        private void SetKeisansiki(string calc, ref Kingaku kin)
        {
            string siki = calc.Replace("+", " +");
            siki = siki.Replace("-", " -");
            string[] items = siki.Split(' ');
            foreach (string item in items)
            {
                int cd = Util.ToInt(item);
                int pm = cd < 0 ? -1 : 1;
                var w = GetBunruiKingk(Math.Abs(cd));

                kin.前残1 += (w.前残1 * pm);
                kin.前残2 += (w.前残2 * pm);
                kin.前残3 += (w.前残3 * pm);

                kin.前残 += (w.前残 * pm);
                kin.借方 += (w.借方 * pm);
                kin.貸方 += (w.貸方 * pm);
                kin.残高 += (w.残高 * pm);
            }
        }

        private Kingaku GetBunruiKingk(int kmkcode)
        {
            Kingaku kin = new Kingaku();

            // 貸借対照表
            {
                var q = from a in Taisyaku
                        where a.科目分類 == kmkcode
                        select a;
                if (q.Count<Youyaku>() != 0)
                {
                    kin = q.ElementAtOrDefault(0).金額;
                    return kin;
                }
            }

            // 損益計算書
            {
                var q = from a in Soneki
                        where a.科目分類 == kmkcode
                        select a;
                if (q.Count<Youyaku>() != 0)
                {
                    kin = q.ElementAtOrDefault(0).金額;
                    return kin;
                }
            }

            return kin;
        }

        private void SetKouseihi(ref List<Youyaku> s, int repKbn)
        {
            decimal Kingk = 0;
            foreach (Youyaku i in s)
            {
                if ((i.帳票区分 == repKbn && i.構成比区分 == 1) ||
                    (repKbn == 20 && i.帳票区分 == repKbn + 1 && i.構成比区分 == 1))
                {
                    Kingk += i.金額.残高;
                }
            }
            if (Kingk == 0) return;

            foreach (Youyaku i in s)
            {
                if (i.帳票区分 == repKbn || (repKbn == 20 && i.帳票区分 == (repKbn + 1)))
                {
                    // 合計科目の構成比を設定する
                    i.構成比 = (i.金額.残高 / Kingk) * 100;
                    if (i.明細項目数 != 0)
                    {
                        foreach (ItemsTable j in i.科目明細)
                        {
                            j.構成比 = (j.金額.残高 / Kingk) * 100;
                        }
                    }
                }
            }
        }

        private bool CheckPrintRecords(ref List<Youyaku> s, int repKbn)
        {
            bool retValue = false;

            var flg = false;
            foreach (Youyaku i in s)
            {
                if (i.帳票区分 == repKbn)
                {
                    i.出力区分 = 0;
                    if (i.明細区分 == 1)
                    {
                        foreach (ItemsTable j in i.科目明細)
                        {
                            flg = false;
                            j.出力区分 = 0;
                            if (Util.ToString(_condition["Inji"]) == "no")
                            {
                                // 発生額ゼロの印字無し
                                // 勘定科目が設定されている
                                if (j.勘定科目.Count >= 1 && j.勘定科目[0].コード != 0)
                                {
                                    if (KingakuZero(j.金額))
                                    {
                                        flg = true;
                                        retValue = true;
                                    }
                                }
                            }
                            else
                            {
                                // 発生額ゼロの印字有り
                                // 勘定科目が設定されている
                                if (j.勘定科目.Count >= 1 && j.勘定科目[0].コード != 0)
                                {
                                    flg = true;
                                    retValue = true;
                                }
                            }
                            if (flg)
                            {
                                j.出力区分 = 1;
                                i.出力区分 = 1;
                            }
                        }
                    }
                    else
                    {
                        i.出力区分 = 1;
                    }
                }
            }

            return retValue;
        }

        private bool KingakuZero(Kingaku kin)
        {
            if (kin.前残 != 0) return true;
            if (kin.借方 != 0) return true;
            if (kin.貸方 != 0) return true;
            if (kin.残高 != 0) return true;

            return false;
        }
        #endregion

        #endregion
    }
}
