﻿namespace jp.co.fsi.zm.zmdr1031
{
    /// <summary>
    /// ZAMR1061R の概要の説明です。
    /// </summary>
    partial class ZMDR10311R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMDR10311R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ボックス27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス29 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス35 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.直線36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ボックス48 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.直線49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ボックス69 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ボックス71 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ボックス73 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ボックス75 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線77 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線78 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線79 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ITEM05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト81 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト83 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト84 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト94 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト95 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト96 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト97 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト98 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト99 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト101 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト103 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト104 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト105 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト106 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト107 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト108 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト109 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト113 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト115 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト116 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト120 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト122 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト123 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト125 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト126 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト127 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト128 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト129 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト130 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト131 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト132 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト133 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト134 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト135 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト136 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト137 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト138 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト139 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト140 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト141 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト142 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト143 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト144 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト145 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト146 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト147 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト148 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト149 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト150 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト151 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト152 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト153 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト154 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト155 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト156 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト157 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト158 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト159 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト160 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト161 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト162 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト163 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト164 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト165 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト166 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト167 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト168 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト169 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト170 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト171 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト172 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト173 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト174 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト175 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線176 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト164)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト165)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト166)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト167)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト169)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト170)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト172)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト173)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト174)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト175)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape1,
            this.label1,
            this.ボックス27,
            this.ラベル24,
            this.ITEM02,
            this.ITEM01,
            this.ボックス29,
            this.ラベル30,
            this.テキスト31,
            this.ITEM03,
            this.ボックス35,
            this.直線36,
            this.直線37,
            this.ラベル38,
            this.ラベル39,
            this.ラベル40,
            this.ラベル41,
            this.ラベル43,
            this.ラベル44,
            this.ラベル45,
            this.ラベル46,
            this.ラベル47,
            this.ボックス48,
            this.直線49,
            this.直線50,
            this.直線51,
            this.直線52,
            this.直線54,
            this.直線55,
            this.直線56,
            this.直線58,
            this.直線60,
            this.直線61,
            this.直線62,
            this.直線64,
            this.直線65,
            this.直線66,
            this.ラベル67,
            this.ラベル68,
            this.ボックス69,
            this.ラベル70,
            this.ボックス71,
            this.ラベル72,
            this.ボックス73,
            this.ラベル74,
            this.ボックス75,
            this.ラベル76,
            this.直線77,
            this.直線78,
            this.直線79,
            this.ITEM05,
            this.テキスト81,
            this.テキスト82,
            this.テキスト83,
            this.テキスト84,
            this.ITEM06,
            this.ITEM07,
            this.ITEM08,
            this.ITEM09,
            this.ITEM10,
            this.ITEM11,
            this.ITEM12,
            this.ITEM13,
            this.ITEM14,
            this.テキスト94,
            this.テキスト95,
            this.テキスト96,
            this.テキスト97,
            this.テキスト98,
            this.テキスト99,
            this.テキスト100,
            this.テキスト101,
            this.テキスト102,
            this.テキスト103,
            this.テキスト104,
            this.テキスト105,
            this.テキスト106,
            this.テキスト107,
            this.テキスト108,
            this.テキスト109,
            this.テキスト110,
            this.テキスト111,
            this.テキスト112,
            this.テキスト113,
            this.テキスト114,
            this.テキスト115,
            this.テキスト116,
            this.テキスト117,
            this.テキスト118,
            this.テキスト119,
            this.テキスト120,
            this.テキスト121,
            this.テキスト122,
            this.テキスト123,
            this.テキスト124,
            this.テキスト125,
            this.テキスト126,
            this.テキスト127,
            this.テキスト128,
            this.テキスト129,
            this.テキスト130,
            this.テキスト131,
            this.テキスト132,
            this.テキスト133,
            this.テキスト134,
            this.テキスト135,
            this.テキスト136,
            this.テキスト137,
            this.テキスト138,
            this.テキスト139,
            this.テキスト140,
            this.テキスト141,
            this.テキスト142,
            this.テキスト143,
            this.テキスト144,
            this.テキスト145,
            this.テキスト146,
            this.テキスト147,
            this.テキスト148,
            this.テキスト149,
            this.テキスト150,
            this.テキスト151,
            this.テキスト152,
            this.テキスト153,
            this.テキスト154,
            this.テキスト155,
            this.テキスト156,
            this.テキスト157,
            this.テキスト158,
            this.テキスト159,
            this.テキスト160,
            this.テキスト161,
            this.テキスト162,
            this.テキスト163,
            this.テキスト164,
            this.テキスト165,
            this.テキスト166,
            this.テキスト167,
            this.テキスト168,
            this.テキスト169,
            this.テキスト170,
            this.テキスト171,
            this.テキスト172,
            this.テキスト173,
            this.テキスト174,
            this.テキスト175,
            this.直線176,
            this.line1,
            this.label2});
            this.detail.Height = 5.708219F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // shape1
            // 
            this.shape1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape1.Height = 0.4326389F;
            this.shape1.Left = 2.726296F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape1.Tag = "";
            this.shape1.Top = 5.176592F;
            this.shape1.Width = 0.2548611F;
            // 
            // label1
            // 
            this.label1.Height = 0.4346458F;
            this.label1.HyperLink = null;
            this.label1.Left = 2.726378F;
            this.label1.Name = "label1";
            this.label1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.label1.Tag = "";
            this.label1.Text = "組合長";
            this.label1.Top = 5.179528F;
            this.label1.Width = 0.2547243F;
            // 
            // ボックス27
            // 
            this.ボックス27.BackColor = System.Drawing.Color.White;
            this.ボックス27.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス27.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス27.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス27.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス27.Height = 0.4397638F;
            this.ボックス27.Left = 0.5905512F;
            this.ボックス27.Name = "ボックス27";
            this.ボックス27.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス27.Tag = "";
            this.ボックス27.Top = 1.023622F;
            this.ボックス27.Width = 1.972441F;
            // 
            // ラベル24
            // 
            this.ラベル24.Height = 0.1756945F;
            this.ラベル24.HyperLink = null;
            this.ラベル24.Left = 0.6614174F;
            this.ラベル24.Name = "ラベル24";
            this.ラベル24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル24.Tag = "";
            this.ラベル24.Text = "組　　合　　名";
            this.ラベル24.Top = 1.056693F;
            this.ラベル24.Width = 1.846457F;
            // 
            // ITEM02
            // 
            this.ITEM02.DataField = "ITEM02";
            this.ITEM02.Height = 0.3125F;
            this.ITEM02.Left = 2.961417F;
            this.ITEM02.Name = "ITEM02";
            this.ITEM02.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18.75pt; fo" +
    "nt-weight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM02.Tag = "";
            this.ITEM02.Text = "ITEM02";
            this.ITEM02.Top = 0.9686677F;
            this.ITEM02.Width = 2.731496F;
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.15625F;
            this.ITEM01.Left = 0.6062993F;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM01";
            this.ITEM01.Top = 1.259843F;
            this.ITEM01.Width = 1.937008F;
            // 
            // ボックス29
            // 
            this.ボックス29.BackColor = System.Drawing.Color.White;
            this.ボックス29.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス29.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス29.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス29.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス29.Height = 0.45F;
            this.ボックス29.Left = 6.231103F;
            this.ボックス29.Name = "ボックス29";
            this.ボックス29.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス29.Tag = "";
            this.ボックス29.Top = 1.012205F;
            this.ボックス29.Width = 1.141667F;
            // 
            // ラベル30
            // 
            this.ラベル30.Height = 0.15625F;
            this.ラベル30.HyperLink = null;
            this.ラベル30.Left = 6.258206F;
            this.ラベル30.Name = "ラベル30";
            this.ラベル30.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル30.Tag = "";
            this.ラベル30.Text = "伝　票　番　号";
            this.ラベル30.Top = 1.044893F;
            this.ラベル30.Width = 1.072917F;
            // 
            // テキスト31
            // 
            this.テキスト31.DataField = "ITEM04";
            this.テキスト31.Height = 0.15625F;
            this.テキスト31.Left = 6.259596F;
            this.テキスト31.Name = "テキスト31";
            this.テキスト31.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト31.Tag = "";
            this.テキスト31.Text = "ITEM04";
            this.テキスト31.Top = 1.260171F;
            this.テキスト31.Width = 1.066667F;
            // 
            // ITEM03
            // 
            this.ITEM03.DataField = "ITEM03";
            this.ITEM03.Height = 0.15625F;
            this.ITEM03.Left = 2.961417F;
            this.ITEM03.Name = "ITEM03";
            this.ITEM03.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM03.Tag = "";
            this.ITEM03.Text = "ITEM03";
            this.ITEM03.Top = 1.322835F;
            this.ITEM03.Width = 2.731496F;
            // 
            // ボックス35
            // 
            this.ボックス35.BackColor = System.Drawing.Color.White;
            this.ボックス35.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス35.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス35.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス35.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス35.Height = 0.4722222F;
            this.ボックス35.Left = 0.5070866F;
            this.ボックス35.Name = "ボックス35";
            this.ボックス35.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス35.Tag = "";
            this.ボックス35.Top = 1.603937F;
            this.ボックス35.Width = 7.367192F;
            // 
            // 直線36
            // 
            this.直線36.Height = 9.536743E-07F;
            this.直線36.Left = 6.231103F;
            this.直線36.LineWeight = 0F;
            this.直線36.Name = "直線36";
            this.直線36.Tag = "";
            this.直線36.Top = 1.232283F;
            this.直線36.Width = 1.141732F;
            this.直線36.X1 = 6.231103F;
            this.直線36.X2 = 7.372835F;
            this.直線36.Y1 = 1.232284F;
            this.直線36.Y2 = 1.232283F;
            // 
            // 直線37
            // 
            this.直線37.Height = 0F;
            this.直線37.Left = 0.5905512F;
            this.直線37.LineWeight = 0F;
            this.直線37.Name = "直線37";
            this.直線37.Tag = "";
            this.直線37.Top = 1.23388F;
            this.直線37.Width = 1.968504F;
            this.直線37.X1 = 0.5905512F;
            this.直線37.X2 = 2.559055F;
            this.直線37.Y1 = 1.23388F;
            this.直線37.Y2 = 1.23388F;
            // 
            // ラベル38
            // 
            this.ラベル38.Height = 0.2020833F;
            this.ラベル38.HyperLink = null;
            this.ラベル38.Left = 3.361418F;
            this.ラベル38.Name = "ラベル38";
            this.ラベル38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル38.Tag = "";
            this.ラベル38.Text = "摘　　　　　　要";
            this.ラベル38.Top = 1.749606F;
            this.ラベル38.Width = 1.660237F;
            // 
            // ラベル39
            // 
            this.ラベル39.Height = 0.15625F;
            this.ラベル39.HyperLink = null;
            this.ラベル39.Left = 0.9976379F;
            this.ラベル39.Name = "ラベル39";
            this.ラベル39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル39.Tag = "";
            this.ラベル39.Text = "借　　　　　　　方";
            this.ラベル39.Top = 1.635039F;
            this.ラベル39.Width = 2.104167F;
            // 
            // ラベル40
            // 
            this.ラベル40.Height = 0.2604167F;
            this.ラベル40.HyperLink = null;
            this.ラベル40.Left = 0.7363848F;
            this.ラベル40.Name = "ラベル40";
            this.ラベル40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 128";
            this.ラベル40.Tag = "";
            this.ラベル40.Text = "科　　　目\r\n内　　　訳";
            this.ラベル40.Top = 1.791421F;
            this.ラベル40.Width = 1.177083F;
            // 
            // ラベル41
            // 
            this.ラベル41.Height = 0.15625F;
            this.ラベル41.HyperLink = null;
            this.ラベル41.Left = 2.46811F;
            this.ラベル41.Name = "ラベル41";
            this.ラベル41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ラベル41.Tag = "";
            this.ラベル41.Text = "金　　額";
            this.ラベル41.Top = 1.864173F;
            this.ラベル41.Width = 0.8657482F;
            // 
            // ラベル43
            // 
            this.ラベル43.Height = 0.15625F;
            this.ラベル43.HyperLink = null;
            this.ラベル43.Left = 5.444095F;
            this.ラベル43.Name = "ラベル43";
            this.ラベル43.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル43.Tag = "";
            this.ラベル43.Text = "貸　　　　　　　方";
            this.ラベル43.Top = 1.624803F;
            this.ラベル43.Width = 2.104167F;
            // 
            // ラベル44
            // 
            this.ラベル44.Height = 0.2604167F;
            this.ラベル44.HyperLink = null;
            this.ラベル44.Left = 5.214961F;
            this.ラベル44.Name = "ラベル44";
            this.ラベル44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ラベル44.Tag = "";
            this.ラベル44.Text = "科　　　目\r\n内　　　訳";
            this.ラベル44.Top = 1.791339F;
            this.ラベル44.Width = 1.135417F;
            // 
            // ラベル45
            // 
            this.ラベル45.Height = 0.15625F;
            this.ラベル45.HyperLink = null;
            this.ラベル45.Left = 6.926378F;
            this.ラベル45.Name = "ラベル45";
            this.ラベル45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ラベル45.Tag = "";
            this.ラベル45.Text = "金　　額";
            this.ラベル45.Top = 1.864173F;
            this.ラベル45.Width = 0.8894362F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.2818899F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 6.672835F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "税区";
            this.ラベル46.Top = 1.794095F;
            this.ラベル46.Width = 0.1527778F;
            // 
            // ラベル47
            // 
            this.ラベル47.Height = 0.15625F;
            this.ラベル47.HyperLink = null;
            this.ラベル47.Left = 0.5384681F;
            this.ラベル47.Name = "ラベル47";
            this.ラベル47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル47.Tag = "";
            this.ラベル47.Text = "行";
            this.ラベル47.Top = 1.791421F;
            this.ラベル47.Width = 0.1770833F;
            // 
            // ボックス48
            // 
            this.ボックス48.BackColor = System.Drawing.Color.White;
            this.ボックス48.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス48.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス48.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス48.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス48.Height = 2.322917F;
            this.ボックス48.Left = 0.5072181F;
            this.ボックス48.Name = "ボックス48";
            this.ボックス48.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス48.Tag = "";
            this.ボックス48.Top = 2.062254F;
            this.ボックス48.Width = 7.367192F;
            // 
            // 直線49
            // 
            this.直線49.Height = 0.0001480579F;
            this.直線49.Left = 0.5200788F;
            this.直線49.LineWeight = 0F;
            this.直線49.Name = "直線49";
            this.直線49.Tag = "";
            this.直線49.Top = 2.499606F;
            this.直線49.Width = 7.354296F;
            this.直線49.X1 = 0.5200788F;
            this.直線49.X2 = 7.874375F;
            this.直線49.Y1 = 2.499606F;
            this.直線49.Y2 = 2.499754F;
            // 
            // 直線50
            // 
            this.直線50.Height = 0F;
            this.直線50.Left = 0.5196851F;
            this.直線50.LineWeight = 0F;
            this.直線50.Name = "直線50";
            this.直線50.Tag = "";
            this.直線50.Top = 2.97874F;
            this.直線50.Width = 7.354559F;
            this.直線50.X1 = 0.5196851F;
            this.直線50.X2 = 7.874244F;
            this.直線50.Y1 = 2.97874F;
            this.直線50.Y2 = 2.97874F;
            // 
            // 直線51
            // 
            this.直線51.Height = 0F;
            this.直線51.Left = 0.5196851F;
            this.直線51.LineWeight = 0F;
            this.直線51.Name = "直線51";
            this.直線51.Tag = "";
            this.直線51.Top = 3.44749F;
            this.直線51.Width = 7.354559F;
            this.直線51.X1 = 0.5196851F;
            this.直線51.X2 = 7.874244F;
            this.直線51.Y1 = 3.44749F;
            this.直線51.Y2 = 3.44749F;
            // 
            // 直線52
            // 
            this.直線52.Height = 0F;
            this.直線52.Left = 0.5196851F;
            this.直線52.LineWeight = 0F;
            this.直線52.Name = "直線52";
            this.直線52.Tag = "";
            this.直線52.Top = 3.91624F;
            this.直線52.Width = 7.354559F;
            this.直線52.X1 = 0.5196851F;
            this.直線52.X2 = 7.874244F;
            this.直線52.Y1 = 3.91624F;
            this.直線52.Y2 = 3.91624F;
            // 
            // 直線54
            // 
            this.直線54.Height = 3.440972F;
            this.直線54.Left = 3.361418F;
            this.直線54.LineWeight = 0F;
            this.直線54.Name = "直線54";
            this.直線54.Tag = "";
            this.直線54.Top = 1.603937F;
            this.直線54.Width = 0F;
            this.直線54.X1 = 3.361418F;
            this.直線54.X2 = 3.361418F;
            this.直線54.Y1 = 1.603937F;
            this.直線54.Y2 = 5.044909F;
            // 
            // 直線55
            // 
            this.直線55.Height = 3.440972F;
            this.直線55.Left = 5.077953F;
            this.直線55.LineWeight = 0F;
            this.直線55.Name = "直線55";
            this.直線55.Tag = "";
            this.直線55.Top = 1.603937F;
            this.直線55.Width = 0F;
            this.直線55.X1 = 5.077953F;
            this.直線55.X2 = 5.077953F;
            this.直線55.Y1 = 1.603937F;
            this.直線55.Y2 = 5.044909F;
            // 
            // 直線56
            // 
            this.直線56.Height = 9.906292E-05F;
            this.直線56.Left = 5.083465F;
            this.直線56.LineWeight = 0F;
            this.直線56.Name = "直線56";
            this.直線56.Tag = "";
            this.直線56.Top = 1.781004F;
            this.直線56.Width = 2.791009F;
            this.直線56.X1 = 5.083465F;
            this.直線56.X2 = 7.874474F;
            this.直線56.Y1 = 1.781103F;
            this.直線56.Y2 = 1.781004F;
            // 
            // 直線58
            // 
            this.直線58.Height = 2.598611F;
            this.直線58.Left = 2.196457F;
            this.直線58.LineWeight = 0F;
            this.直線58.Name = "直線58";
            this.直線58.Tag = "";
            this.直線58.Top = 1.791339F;
            this.直線58.Width = 0F;
            this.直線58.X1 = 2.196457F;
            this.直線58.X2 = 2.196457F;
            this.直線58.Y1 = 1.791339F;
            this.直線58.Y2 = 4.38995F;
            // 
            // 直線60
            // 
            this.直線60.Height = 2.598611F;
            this.直線60.Left = 2.412599F;
            this.直線60.LineWeight = 0F;
            this.直線60.Name = "直線60";
            this.直線60.Tag = "";
            this.直線60.Top = 1.791339F;
            this.直線60.Width = 0F;
            this.直線60.X1 = 2.412599F;
            this.直線60.X2 = 2.412599F;
            this.直線60.Y1 = 1.791339F;
            this.直線60.Y2 = 4.38995F;
            // 
            // 直線61
            // 
            this.直線61.Height = 2.608848F;
            this.直線61.Left = 6.633465F;
            this.直線61.LineWeight = 0F;
            this.直線61.Name = "直線61";
            this.直線61.Tag = "";
            this.直線61.Top = 1.781102F;
            this.直線61.Width = 0F;
            this.直線61.X1 = 6.633465F;
            this.直線61.X2 = 6.633465F;
            this.直線61.Y1 = 1.781102F;
            this.直線61.Y2 = 4.38995F;
            // 
            // 直線62
            // 
            this.直線62.Height = 2.608848F;
            this.直線62.Left = 6.878347F;
            this.直線62.LineWeight = 0F;
            this.直線62.Name = "直線62";
            this.直線62.Tag = "";
            this.直線62.Top = 1.781102F;
            this.直線62.Width = 0F;
            this.直線62.X1 = 6.878347F;
            this.直線62.X2 = 6.878347F;
            this.直線62.Y1 = 1.781102F;
            this.直線62.Y2 = 4.38995F;
            // 
            // 直線64
            // 
            this.直線64.Height = 0.6687489F;
            this.直線64.Left = 7.87441F;
            this.直線64.LineWeight = 0F;
            this.直線64.Name = "直線64";
            this.直線64.Tag = "";
            this.直線64.Top = 4.375985F;
            this.直線64.Width = 0F;
            this.直線64.X1 = 7.87441F;
            this.直線64.X2 = 7.87441F;
            this.直線64.Y1 = 4.375985F;
            this.直線64.Y2 = 5.044734F;
            // 
            // 直線65
            // 
            this.直線65.Height = 0F;
            this.直線65.Left = 0.7051348F;
            this.直線65.LineWeight = 0F;
            this.直線65.Name = "直線65";
            this.直線65.Tag = "";
            this.直線65.Top = 5.044882F;
            this.直線65.Width = 7.169275F;
            this.直線65.X1 = 0.7051348F;
            this.直線65.X2 = 7.87441F;
            this.直線65.Y1 = 5.044882F;
            this.直線65.Y2 = 5.044882F;
            // 
            // 直線66
            // 
            this.直線66.Height = 3.430556F;
            this.直線66.Left = 0.7051348F;
            this.直線66.LineWeight = 0F;
            this.直線66.Name = "直線66";
            this.直線66.Tag = "";
            this.直線66.Top = 1.614337F;
            this.直線66.Width = 0F;
            this.直線66.X1 = 0.7051348F;
            this.直線66.X2 = 0.7051348F;
            this.直線66.Y1 = 1.614337F;
            this.直線66.Y2 = 5.044893F;
            // 
            // ラベル67
            // 
            this.ラベル67.Height = 0.3488191F;
            this.ラベル67.HyperLink = null;
            this.ラベル67.Left = 3.361418F;
            this.ラベル67.Name = "ラベル67";
            this.ラベル67.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
            this.ラベル67.Tag = "";
            this.ラベル67.Text = "合　　　　　計";
            this.ラベル67.Top = 4.659843F;
            this.ラベル67.Width = 1.716536F;
            // 
            // ラベル68
            // 
            this.ラベル68.Height = 0.240108F;
            this.ラベル68.HyperLink = null;
            this.ラベル68.Left = 3.361418F;
            this.ラベル68.Name = "ラベル68";
            this.ラベル68.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
            this.ラベル68.Tag = "";
            this.ラベル68.Text = "小　　　　　計";
            this.ラベル68.Top = 4.389764F;
            this.ラベル68.Width = 1.716536F;
            // 
            // ボックス69
            // 
            this.ボックス69.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス69.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス69.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス69.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス69.Height = 0.4326389F;
            this.ボックス69.Left = 3.61063F;
            this.ボックス69.Name = "ボックス69";
            this.ボックス69.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス69.Tag = "";
            this.ボックス69.Top = 5.178741F;
            this.ボックス69.Width = 0.2548611F;
            // 
            // ラベル70
            // 
            this.ラベル70.Height = 0.4374015F;
            this.ラベル70.HyperLink = null;
            this.ラベル70.Left = 3.61063F;
            this.ラベル70.LineSpacing = 9F;
            this.ラベル70.Name = "ラベル70";
            this.ラベル70.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: bottom; ddo-char-set: 128";
            this.ラベル70.Tag = "";
            this.ラベル70.Text = "課長";
            this.ラベル70.Top = 5.176772F;
            this.ラベル70.Width = 0.2547243F;
            // 
            // ボックス71
            // 
            this.ボックス71.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス71.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス71.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス71.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス71.Height = 0.4326389F;
            this.ボックス71.Left = 4.517624F;
            this.ボックス71.Name = "ボックス71";
            this.ボックス71.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス71.Tag = "";
            this.ボックス71.Top = 5.178561F;
            this.ボックス71.Width = 0.2548611F;
            // 
            // ラベル72
            // 
            this.ラベル72.Height = 0.4374015F;
            this.ラベル72.HyperLink = null;
            this.ラベル72.Left = 4.517717F;
            this.ラベル72.LineSpacing = 9F;
            this.ラベル72.Name = "ラベル72";
            this.ラベル72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: bottom; ddo-char-set: 128";
            this.ラベル72.Tag = "";
            this.ラベル72.Text = "係長";
            this.ラベル72.Top = 5.176772F;
            this.ラベル72.Width = 0.2547245F;
            // 
            // ボックス73
            // 
            this.ボックス73.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス73.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス73.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス73.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス73.Height = 0.4326389F;
            this.ボックス73.Left = 5.413294F;
            this.ボックス73.Name = "ボックス73";
            this.ボックス73.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス73.Tag = "";
            this.ボックス73.Top = 5.176592F;
            this.ボックス73.Width = 0.2548611F;
            // 
            // ラベル74
            // 
            this.ラベル74.Height = 0.4374015F;
            this.ラベル74.HyperLink = null;
            this.ラベル74.Left = 5.413386F;
            this.ラベル74.LineSpacing = 9F;
            this.ラベル74.Name = "ラベル74";
            this.ラベル74.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: bottom; ddo-char-set: 128";
            this.ラベル74.Tag = "";
            this.ラベル74.Text = "照査";
            this.ラベル74.Top = 5.176772F;
            this.ラベル74.Width = 0.2547245F;
            // 
            // ボックス75
            // 
            this.ボックス75.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス75.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス75.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス75.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス75.Height = 0.4326389F;
            this.ボックス75.Left = 6.319592F;
            this.ボックス75.Name = "ボックス75";
            this.ボックス75.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.ボックス75.Tag = "";
            this.ボックス75.Top = 5.176592F;
            this.ボックス75.Width = 0.2548611F;
            // 
            // ラベル76
            // 
            this.ラベル76.Height = 0.4354331F;
            this.ラベル76.HyperLink = null;
            this.ラベル76.Left = 6.319685F;
            this.ラベル76.LineSpacing = 9F;
            this.ラベル76.Name = "ラベル76";
            this.ラベル76.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; text-justify: distribute-all-lines; vertical-align: bottom; ddo-cha" +
    "r-set: 128; ddo-font-vertical: true";
            this.ラベル76.Tag = "";
            this.ラベル76.Text = "起票";
            this.ラベル76.Top = 5.178741F;
            this.ラベル76.Width = 0.2547245F;
            // 
            // 直線77
            // 
            this.直線77.Height = 6.532669E-05F;
            this.直線77.Left = 2.981021F;
            this.直線77.LineWeight = 0F;
            this.直線77.Name = "直線77";
            this.直線77.Tag = "";
            this.直線77.Top = 5.176592F;
            this.直線77.Width = 4.214254F;
            this.直線77.X1 = 2.981021F;
            this.直線77.X2 = 7.195275F;
            this.直線77.Y1 = 5.176592F;
            this.直線77.Y2 = 5.176657F;
            // 
            // 直線78
            // 
            this.直線78.Height = 0.002918243F;
            this.直線78.Left = 2.949213F;
            this.直線78.LineWeight = 0F;
            this.直線78.Name = "直線78";
            this.直線78.Tag = "";
            this.直線78.Top = 5.611418F;
            this.直線78.Width = 4.245985F;
            this.直線78.X1 = 2.949213F;
            this.直線78.X2 = 7.195198F;
            this.直線78.Y1 = 5.611418F;
            this.直線78.Y2 = 5.614336F;
            // 
            // 直線79
            // 
            this.直線79.Height = 0.4326391F;
            this.直線79.Left = 7.19534F;
            this.直線79.LineWeight = 0F;
            this.直線79.Name = "直線79";
            this.直線79.Tag = "";
            this.直線79.Top = 5.178741F;
            this.直線79.Width = 0F;
            this.直線79.X1 = 7.19534F;
            this.直線79.X2 = 7.19534F;
            this.直線79.Y1 = 5.178741F;
            this.直線79.Y2 = 5.61138F;
            // 
            // ITEM05
            // 
            this.ITEM05.DataField = "ITEM05";
            this.ITEM05.Height = 0.15625F;
            this.ITEM05.Left = 0.5419403F;
            this.ITEM05.Name = "ITEM05";
            this.ITEM05.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM05.Tag = "";
            this.ITEM05.Text = "ITEM05";
            this.ITEM05.Top = 2.226143F;
            this.ITEM05.Width = 0.1564849F;
            // 
            // テキスト81
            // 
            this.テキスト81.DataField = "ITEM23";
            this.テキスト81.Height = 0.15625F;
            this.テキスト81.Left = 0.5419403F;
            this.テキスト81.Name = "テキスト81";
            this.テキスト81.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト81.Tag = "";
            this.テキスト81.Text = "ITEM23";
            this.テキスト81.Top = 2.698365F;
            this.テキスト81.Width = 0.1564849F;
            // 
            // テキスト82
            // 
            this.テキスト82.DataField = "ITEM41";
            this.テキスト82.Height = 0.15625F;
            this.テキスト82.Left = 0.5419403F;
            this.テキスト82.Name = "テキスト82";
            this.テキスト82.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト82.Tag = "";
            this.テキスト82.Text = "ITEM41";
            this.テキスト82.Top = 3.131698F;
            this.テキスト82.Width = 0.1564849F;
            // 
            // テキスト83
            // 
            this.テキスト83.DataField = "ITEM59";
            this.テキスト83.Height = 0.15625F;
            this.テキスト83.Left = 0.5419403F;
            this.テキスト83.Name = "テキスト83";
            this.テキスト83.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト83.Tag = "";
            this.テキスト83.Text = "ITEM59";
            this.テキスト83.Top = 3.603921F;
            this.テキスト83.Width = 0.1564849F;
            // 
            // テキスト84
            // 
            this.テキスト84.DataField = "ITEM77";
            this.テキスト84.Height = 0.15625F;
            this.テキスト84.Left = 0.5419403F;
            this.テキスト84.Name = "テキスト84";
            this.テキスト84.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト84.Tag = "";
            this.テキスト84.Text = "ITEM77";
            this.テキスト84.Top = 4.076143F;
            this.テキスト84.Width = 0.1564849F;
            // 
            // ITEM06
            // 
            this.ITEM06.DataField = "ITEM06";
            this.ITEM06.Height = 0.15625F;
            this.ITEM06.Left = 0.7384681F;
            this.ITEM06.Name = "ITEM06";
            this.ITEM06.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM06.Tag = "";
            this.ITEM06.Text = "ITEM06";
            this.ITEM06.Top = 2.108087F;
            this.ITEM06.Width = 0.4044454F;
            // 
            // ITEM07
            // 
            this.ITEM07.DataField = "ITEM07";
            this.ITEM07.Height = 0.1562992F;
            this.ITEM07.Left = 1.142913F;
            this.ITEM07.MultiLine = false;
            this.ITEM07.Name = "ITEM07";
            this.ITEM07.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM07.Tag = "";
            this.ITEM07.Text = "ITEM07";
            this.ITEM07.Top = 2.108268F;
            this.ITEM07.Width = 1.01811F;
            // 
            // ITEM08
            // 
            this.ITEM08.DataField = "ITEM08";
            this.ITEM08.Height = 0.15625F;
            this.ITEM08.Left = 2.204282F;
            this.ITEM08.Name = "ITEM08";
            this.ITEM08.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM08.Tag = "";
            this.ITEM08.Text = "ITEM08";
            this.ITEM08.Top = 2.103921F;
            this.ITEM08.Width = 0.1916667F;
            // 
            // ITEM09
            // 
            this.ITEM09.DataField = "ITEM09";
            this.ITEM09.Height = 0.1562992F;
            this.ITEM09.Left = 2.430709F;
            this.ITEM09.Name = "ITEM09";
            this.ITEM09.OutputFormat = resources.GetString("ITEM09.OutputFormat");
            this.ITEM09.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM09.Tag = "";
            this.ITEM09.Text = "ITEM09";
            this.ITEM09.Top = 2.108268F;
            this.ITEM09.Width = 0.892126F;
            // 
            // ITEM10
            // 
            this.ITEM10.DataField = "ITEM10";
            this.ITEM10.Height = 0.15625F;
            this.ITEM10.Left = 0.7384681F;
            this.ITEM10.Name = "ITEM10";
            this.ITEM10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM10.Tag = "";
            this.ITEM10.Text = "ITEM10";
            this.ITEM10.Top = 2.304615F;
            this.ITEM10.Width = 0.4043307F;
            // 
            // ITEM11
            // 
            this.ITEM11.DataField = "ITEM11";
            this.ITEM11.Height = 0.1562992F;
            this.ITEM11.Left = 1.142913F;
            this.ITEM11.MultiLine = false;
            this.ITEM11.Name = "ITEM11";
            this.ITEM11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM11.Tag = "";
            this.ITEM11.Text = "ITEM11";
            this.ITEM11.Top = 2.304796F;
            this.ITEM11.Width = 1.01811F;
            // 
            // ITEM12
            // 
            this.ITEM12.DataField = "ITEM12";
            this.ITEM12.Height = 0.15625F;
            this.ITEM12.Left = 2.204282F;
            this.ITEM12.Name = "ITEM12";
            this.ITEM12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM12.Tag = "";
            this.ITEM12.Text = "ITEM12";
            this.ITEM12.Top = 2.300448F;
            this.ITEM12.Width = 0.1916667F;
            // 
            // ITEM13
            // 
            this.ITEM13.DataField = "ITEM13";
            this.ITEM13.Height = 0.1562992F;
            this.ITEM13.Left = 2.430709F;
            this.ITEM13.Name = "ITEM13";
            this.ITEM13.OutputFormat = resources.GetString("ITEM13.OutputFormat");
            this.ITEM13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM13.Tag = "";
            this.ITEM13.Text = "ITEM13";
            this.ITEM13.Top = 2.304724F;
            this.ITEM13.Width = 0.892126F;
            // 
            // ITEM14
            // 
            this.ITEM14.DataField = "ITEM14";
            this.ITEM14.Height = 0.2708333F;
            this.ITEM14.Left = 3.413386F;
            this.ITEM14.Name = "ITEM14";
            this.ITEM14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM14.Tag = "";
            this.ITEM14.Text = "ITEM14";
            this.ITEM14.Top = 2.145669F;
            this.ITEM14.Width = 1.608268F;
            // 
            // テキスト94
            // 
            this.テキスト94.DataField = "ITEM15";
            this.テキスト94.Height = 0.15625F;
            this.テキスト94.Left = 5.101575F;
            this.テキスト94.Name = "テキスト94";
            this.テキスト94.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト94.Tag = "";
            this.テキスト94.Text = "ITEM15";
            this.テキスト94.Top = 2.103937F;
            this.テキスト94.Width = 0.4043307F;
            // 
            // テキスト95
            // 
            this.テキスト95.DataField = "ITEM16";
            this.テキスト95.Height = 0.15625F;
            this.テキスト95.Left = 5.505906F;
            this.テキスト95.MultiLine = false;
            this.テキスト95.Name = "テキスト95";
            this.テキスト95.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト95.Tag = "";
            this.テキスト95.Text = "ITEM16";
            this.テキスト95.Top = 2.103937F;
            this.テキスト95.Width = 1.115748F;
            // 
            // テキスト96
            // 
            this.テキスト96.DataField = "ITEM17";
            this.テキスト96.Height = 0.15625F;
            this.テキスト96.Left = 6.664961F;
            this.テキスト96.Name = "テキスト96";
            this.テキスト96.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト96.Tag = "";
            this.テキスト96.Text = "ITEM17";
            this.テキスト96.Top = 2.103855F;
            this.テキスト96.Width = 0.1916667F;
            // 
            // テキスト97
            // 
            this.テキスト97.DataField = "ITEM18";
            this.テキスト97.Height = 0.1562992F;
            this.テキスト97.Left = 6.926378F;
            this.テキスト97.Name = "テキスト97";
            this.テキスト97.OutputFormat = resources.GetString("テキスト97.OutputFormat");
            this.テキスト97.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト97.Tag = "";
            this.テキスト97.Text = "ITEM18";
            this.テキスト97.Top = 2.103937F;
            this.テキスト97.Width = 0.892126F;
            // 
            // テキスト98
            // 
            this.テキスト98.DataField = "ITEM19";
            this.テキスト98.Height = 0.15625F;
            this.テキスト98.Left = 5.101575F;
            this.テキスト98.Name = "テキスト98";
            this.テキスト98.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト98.Tag = "";
            this.テキスト98.Text = "ITEM19";
            this.テキスト98.Top = 2.300465F;
            this.テキスト98.Width = 0.4043307F;
            // 
            // テキスト99
            // 
            this.テキスト99.DataField = "ITEM20";
            this.テキスト99.Height = 0.1562992F;
            this.テキスト99.Left = 5.505906F;
            this.テキスト99.MultiLine = false;
            this.テキスト99.Name = "テキスト99";
            this.テキスト99.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト99.Tag = "";
            this.テキスト99.Text = "ITEM20";
            this.テキスト99.Top = 2.300465F;
            this.テキスト99.Width = 1.115748F;
            // 
            // テキスト100
            // 
            this.テキスト100.DataField = "ITEM21";
            this.テキスト100.Height = 0.15625F;
            this.テキスト100.Left = 6.664961F;
            this.テキスト100.Name = "テキスト100";
            this.テキスト100.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト100.Tag = "";
            this.テキスト100.Text = "ITEM21";
            this.テキスト100.Top = 2.300383F;
            this.テキスト100.Width = 0.1916667F;
            // 
            // テキスト101
            // 
            this.テキスト101.DataField = "ITEM22";
            this.テキスト101.Height = 0.1562992F;
            this.テキスト101.Left = 6.926378F;
            this.テキスト101.Name = "テキスト101";
            this.テキスト101.OutputFormat = resources.GetString("テキスト101.OutputFormat");
            this.テキスト101.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト101.Tag = "";
            this.テキスト101.Text = "ITEM22";
            this.テキスト101.Top = 2.301854F;
            this.テキスト101.Width = 0.892126F;
            // 
            // テキスト102
            // 
            this.テキスト102.DataField = "ITEM24";
            this.テキスト102.Height = 0.15625F;
            this.テキスト102.Left = 0.7384681F;
            this.テキスト102.Name = "テキスト102";
            this.テキスト102.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト102.Tag = "";
            this.テキスト102.Text = "ITEM24";
            this.テキスト102.Top = 2.584476F;
            this.テキスト102.Width = 0.4043307F;
            // 
            // テキスト103
            // 
            this.テキスト103.DataField = "ITEM25";
            this.テキスト103.Height = 0.1562992F;
            this.テキスト103.Left = 1.142913F;
            this.テキスト103.MultiLine = false;
            this.テキスト103.Name = "テキスト103";
            this.テキスト103.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト103.Tag = "";
            this.テキスト103.Text = "ITEM25";
            this.テキスト103.Top = 2.584657F;
            this.テキスト103.Width = 1.01811F;
            // 
            // テキスト104
            // 
            this.テキスト104.DataField = "ITEM26";
            this.テキスト104.Height = 0.15625F;
            this.テキスト104.Left = 2.204282F;
            this.テキスト104.Name = "テキスト104";
            this.テキスト104.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト104.Tag = "";
            this.テキスト104.Text = "ITEM26";
            this.テキスト104.Top = 2.58031F;
            this.テキスト104.Width = 0.1916667F;
            // 
            // テキスト105
            // 
            this.テキスト105.DataField = "ITEM27";
            this.テキスト105.Height = 0.1562992F;
            this.テキスト105.Left = 2.430709F;
            this.テキスト105.Name = "テキスト105";
            this.テキスト105.OutputFormat = resources.GetString("テキスト105.OutputFormat");
            this.テキスト105.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト105.Tag = "";
            this.テキスト105.Text = "ITEM27";
            this.テキスト105.Top = 2.562363F;
            this.テキスト105.Width = 0.892126F;
            // 
            // テキスト106
            // 
            this.テキスト106.DataField = "ITEM28";
            this.テキスト106.Height = 0.15625F;
            this.テキスト106.Left = 0.7384681F;
            this.テキスト106.Name = "テキスト106";
            this.テキスト106.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト106.Tag = "";
            this.テキスト106.Text = "ITEM28";
            this.テキスト106.Top = 2.781004F;
            this.テキスト106.Width = 0.4043307F;
            // 
            // テキスト107
            // 
            this.テキスト107.DataField = "ITEM29";
            this.テキスト107.Height = 0.1562992F;
            this.テキスト107.Left = 1.142913F;
            this.テキスト107.MultiLine = false;
            this.テキスト107.Name = "テキスト107";
            this.テキスト107.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト107.Tag = "";
            this.テキスト107.Text = "ITEM29";
            this.テキスト107.Top = 2.781184F;
            this.テキスト107.Width = 1.01811F;
            // 
            // テキスト108
            // 
            this.テキスト108.DataField = "ITEM30";
            this.テキスト108.Height = 0.15625F;
            this.テキスト108.Left = 2.204282F;
            this.テキスト108.Name = "テキスト108";
            this.テキスト108.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト108.Tag = "";
            this.テキスト108.Text = "ITEM30";
            this.テキスト108.Top = 2.776837F;
            this.テキスト108.Width = 0.1916667F;
            // 
            // テキスト109
            // 
            this.テキスト109.DataField = "ITEM31";
            this.テキスト109.Height = 0.1562992F;
            this.テキスト109.Left = 2.430709F;
            this.テキスト109.Name = "テキスト109";
            this.テキスト109.OutputFormat = resources.GetString("テキスト109.OutputFormat");
            this.テキスト109.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト109.Tag = "";
            this.テキスト109.Text = "ITEM31";
            this.テキスト109.Top = 2.758891F;
            this.テキスト109.Width = 0.892126F;
            // 
            // テキスト110
            // 
            this.テキスト110.DataField = "ITEM33";
            this.テキスト110.Height = 0.15625F;
            this.テキスト110.Left = 5.101575F;
            this.テキスト110.Name = "テキスト110";
            this.テキスト110.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト110.Tag = "";
            this.テキスト110.Text = "ITEM33";
            this.テキスト110.Top = 2.580326F;
            this.テキスト110.Width = 0.4043307F;
            // 
            // テキスト111
            // 
            this.テキスト111.DataField = "ITEM34";
            this.テキスト111.Height = 0.1562992F;
            this.テキスト111.Left = 5.505906F;
            this.テキスト111.MultiLine = false;
            this.テキスト111.Name = "テキスト111";
            this.テキスト111.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト111.Tag = "";
            this.テキスト111.Text = "ITEM34";
            this.テキスト111.Top = 2.580326F;
            this.テキスト111.Width = 1.115748F;
            // 
            // テキスト112
            // 
            this.テキスト112.DataField = "ITEM35";
            this.テキスト112.Height = 0.15625F;
            this.テキスト112.Left = 6.664961F;
            this.テキスト112.Name = "テキスト112";
            this.テキスト112.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト112.Tag = "";
            this.テキスト112.Text = "ITEM35";
            this.テキスト112.Top = 2.580244F;
            this.テキスト112.Width = 0.1916667F;
            // 
            // テキスト113
            // 
            this.テキスト113.DataField = "ITEM36";
            this.テキスト113.Height = 0.1562992F;
            this.テキスト113.Left = 6.926378F;
            this.テキスト113.Name = "テキスト113";
            this.テキスト113.OutputFormat = resources.GetString("テキスト113.OutputFormat");
            this.テキスト113.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト113.Tag = "";
            this.テキスト113.Text = "ITEM36";
            this.テキスト113.Top = 2.56227F;
            this.テキスト113.Width = 0.892126F;
            // 
            // テキスト114
            // 
            this.テキスト114.DataField = "ITEM37";
            this.テキスト114.Height = 0.15625F;
            this.テキスト114.Left = 5.101575F;
            this.テキスト114.Name = "テキスト114";
            this.テキスト114.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト114.Tag = "";
            this.テキスト114.Text = "ITEM37";
            this.テキスト114.Top = 2.776854F;
            this.テキスト114.Width = 0.4043307F;
            // 
            // テキスト115
            // 
            this.テキスト115.DataField = "ITEM38";
            this.テキスト115.Height = 0.1562992F;
            this.テキスト115.Left = 5.505906F;
            this.テキスト115.MultiLine = false;
            this.テキスト115.Name = "テキスト115";
            this.テキスト115.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト115.Tag = "";
            this.テキスト115.Text = "ITEM38";
            this.テキスト115.Top = 2.776854F;
            this.テキスト115.Width = 1.115748F;
            // 
            // テキスト116
            // 
            this.テキスト116.DataField = "ITEM39";
            this.テキスト116.Height = 0.15625F;
            this.テキスト116.Left = 6.664961F;
            this.テキスト116.Name = "テキスト116";
            this.テキスト116.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト116.Tag = "";
            this.テキスト116.Text = "ITEM39";
            this.テキスト116.Top = 2.776772F;
            this.テキスト116.Width = 0.1916667F;
            // 
            // テキスト117
            // 
            this.テキスト117.DataField = "ITEM40";
            this.テキスト117.Height = 0.1562992F;
            this.テキスト117.Left = 6.926378F;
            this.テキスト117.Name = "テキスト117";
            this.テキスト117.OutputFormat = resources.GetString("テキスト117.OutputFormat");
            this.テキスト117.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト117.Tag = "";
            this.テキスト117.Text = "ITEM40";
            this.テキスト117.Top = 2.760187F;
            this.テキスト117.Width = 0.892126F;
            // 
            // テキスト118
            // 
            this.テキスト118.DataField = "ITEM42";
            this.テキスト118.Height = 0.15625F;
            this.テキスト118.Left = 0.7384681F;
            this.テキスト118.Name = "テキスト118";
            this.テキスト118.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト118.Tag = "";
            this.テキスト118.Text = "ITEM42";
            this.テキスト118.Top = 3.056699F;
            this.テキスト118.Width = 0.4043307F;
            // 
            // テキスト119
            // 
            this.テキスト119.DataField = "ITEM43";
            this.テキスト119.Height = 0.1562992F;
            this.テキスト119.Left = 1.142913F;
            this.テキスト119.MultiLine = false;
            this.テキスト119.Name = "テキスト119";
            this.テキスト119.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト119.Tag = "";
            this.テキスト119.Text = "ITEM43";
            this.テキスト119.Top = 3.056879F;
            this.テキスト119.Width = 1.01811F;
            // 
            // テキスト120
            // 
            this.テキスト120.DataField = "ITEM44";
            this.テキスト120.Height = 0.15625F;
            this.テキスト120.Left = 2.204282F;
            this.テキスト120.Name = "テキスト120";
            this.テキスト120.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト120.Tag = "";
            this.テキスト120.Text = "ITEM44";
            this.テキスト120.Top = 3.052532F;
            this.テキスト120.Width = 0.1916667F;
            // 
            // テキスト121
            // 
            this.テキスト121.DataField = "ITEM45";
            this.テキスト121.Height = 0.1562992F;
            this.テキスト121.Left = 2.430709F;
            this.テキスト121.Name = "テキスト121";
            this.テキスト121.OutputFormat = resources.GetString("テキスト121.OutputFormat");
            this.テキスト121.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト121.Tag = "";
            this.テキスト121.Text = "ITEM45";
            this.テキスト121.Top = 3.03528F;
            this.テキスト121.Width = 0.892126F;
            // 
            // テキスト122
            // 
            this.テキスト122.DataField = "ITEM46";
            this.テキスト122.Height = 0.15625F;
            this.テキスト122.Left = 0.7384681F;
            this.テキスト122.Name = "テキスト122";
            this.テキスト122.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト122.Tag = "";
            this.テキスト122.Text = "ITEM46";
            this.テキスト122.Top = 3.253226F;
            this.テキスト122.Width = 0.4043307F;
            // 
            // テキスト123
            // 
            this.テキスト123.DataField = "ITEM47";
            this.テキスト123.Height = 0.1562992F;
            this.テキスト123.Left = 1.142913F;
            this.テキスト123.MultiLine = false;
            this.テキスト123.Name = "テキスト123";
            this.テキスト123.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト123.Tag = "";
            this.テキスト123.Text = "ITEM47";
            this.テキスト123.Top = 3.253407F;
            this.テキスト123.Width = 1.01811F;
            // 
            // テキスト124
            // 
            this.テキスト124.DataField = "ITEM48";
            this.テキスト124.Height = 0.15625F;
            this.テキスト124.Left = 2.204282F;
            this.テキスト124.Name = "テキスト124";
            this.テキスト124.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト124.Tag = "";
            this.テキスト124.Text = "ITEM48";
            this.テキスト124.Top = 3.24906F;
            this.テキスト124.Width = 0.1916667F;
            // 
            // テキスト125
            // 
            this.テキスト125.DataField = "ITEM49";
            this.テキスト125.Height = 0.1562992F;
            this.テキスト125.Left = 2.430709F;
            this.テキスト125.Name = "テキスト125";
            this.テキスト125.OutputFormat = resources.GetString("テキスト125.OutputFormat");
            this.テキスト125.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト125.Tag = "";
            this.テキスト125.Text = "ITEM49";
            this.テキスト125.Top = 3.231808F;
            this.テキスト125.Width = 0.892126F;
            // 
            // テキスト126
            // 
            this.テキスト126.DataField = "ITEM51";
            this.テキスト126.Height = 0.15625F;
            this.テキスト126.Left = 5.101575F;
            this.テキスト126.Name = "テキスト126";
            this.テキスト126.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト126.Tag = "";
            this.テキスト126.Text = "ITEM51";
            this.テキスト126.Top = 3.052548F;
            this.テキスト126.Width = 0.4043307F;
            // 
            // テキスト127
            // 
            this.テキスト127.DataField = "ITEM52";
            this.テキスト127.Height = 0.1562992F;
            this.テキスト127.Left = 5.505906F;
            this.テキスト127.MultiLine = false;
            this.テキスト127.Name = "テキスト127";
            this.テキスト127.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト127.Tag = "";
            this.テキスト127.Text = "ITEM52";
            this.テキスト127.Top = 3.052548F;
            this.テキスト127.Width = 1.115748F;
            // 
            // テキスト128
            // 
            this.テキスト128.DataField = "ITEM53";
            this.テキスト128.Height = 0.15625F;
            this.テキスト128.Left = 6.664961F;
            this.テキスト128.Name = "テキスト128";
            this.テキスト128.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト128.Tag = "";
            this.テキスト128.Text = "ITEM53";
            this.テキスト128.Top = 3.052466F;
            this.テキスト128.Width = 0.1916667F;
            // 
            // テキスト129
            // 
            this.テキスト129.DataField = "ITEM54";
            this.テキスト129.Height = 0.1562992F;
            this.テキスト129.Left = 6.926378F;
            this.テキスト129.Name = "テキスト129";
            this.テキスト129.OutputFormat = resources.GetString("テキスト129.OutputFormat");
            this.テキスト129.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト129.Tag = "";
            this.テキスト129.Text = "ITEM54";
            this.テキスト129.Top = 3.03102F;
            this.テキスト129.Width = 0.892126F;
            // 
            // テキスト130
            // 
            this.テキスト130.DataField = "ITEM55";
            this.テキスト130.Height = 0.15625F;
            this.テキスト130.Left = 5.101575F;
            this.テキスト130.Name = "テキスト130";
            this.テキスト130.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト130.Tag = "";
            this.テキスト130.Text = "ITEM55";
            this.テキスト130.Top = 3.249076F;
            this.テキスト130.Width = 0.4043307F;
            // 
            // テキスト131
            // 
            this.テキスト131.DataField = "ITEM56";
            this.テキスト131.Height = 0.1562992F;
            this.テキスト131.Left = 5.505906F;
            this.テキスト131.MultiLine = false;
            this.テキスト131.Name = "テキスト131";
            this.テキスト131.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト131.Tag = "";
            this.テキスト131.Text = "ITEM56";
            this.テキスト131.Top = 3.249076F;
            this.テキスト131.Width = 1.115748F;
            // 
            // テキスト132
            // 
            this.テキスト132.DataField = "ITEM57";
            this.テキスト132.Height = 0.15625F;
            this.テキスト132.Left = 6.664961F;
            this.テキスト132.Name = "テキスト132";
            this.テキスト132.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト132.Tag = "";
            this.テキスト132.Text = "ITEM57";
            this.テキスト132.Top = 3.248994F;
            this.テキスト132.Width = 0.1916667F;
            // 
            // テキスト133
            // 
            this.テキスト133.DataField = "ITEM58";
            this.テキスト133.Height = 0.1562992F;
            this.テキスト133.Left = 6.926378F;
            this.テキスト133.Name = "テキスト133";
            this.テキスト133.OutputFormat = resources.GetString("テキスト133.OutputFormat");
            this.テキスト133.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト133.Tag = "";
            this.テキスト133.Text = "ITEM58";
            this.テキスト133.Top = 3.228937F;
            this.テキスト133.Width = 0.892126F;
            // 
            // テキスト134
            // 
            this.テキスト134.DataField = "ITEM60";
            this.テキスト134.Height = 0.15625F;
            this.テキスト134.Left = 0.7384681F;
            this.テキスト134.Name = "テキスト134";
            this.テキスト134.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト134.Tag = "";
            this.テキスト134.Text = "ITEM60";
            this.テキスト134.Top = 3.490032F;
            this.テキスト134.Width = 0.4043307F;
            // 
            // テキスト135
            // 
            this.テキスト135.DataField = "ITEM61";
            this.テキスト135.Height = 0.1562992F;
            this.テキスト135.Left = 1.142913F;
            this.テキスト135.MultiLine = false;
            this.テキスト135.Name = "テキスト135";
            this.テキスト135.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト135.Tag = "";
            this.テキスト135.Text = "ITEM61";
            this.テキスト135.Top = 3.490212F;
            this.テキスト135.Width = 1.01811F;
            // 
            // テキスト136
            // 
            this.テキスト136.DataField = "ITEM62";
            this.テキスト136.Height = 0.15625F;
            this.テキスト136.Left = 2.204282F;
            this.テキスト136.Name = "テキスト136";
            this.テキスト136.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト136.Tag = "";
            this.テキスト136.Text = "ITEM62";
            this.テキスト136.Top = 3.485865F;
            this.テキスト136.Width = 0.1916667F;
            // 
            // テキスト137
            // 
            this.テキスト137.DataField = "ITEM63";
            this.テキスト137.Height = 0.1562992F;
            this.テキスト137.Left = 2.430709F;
            this.テキスト137.Name = "テキスト137";
            this.テキスト137.OutputFormat = resources.GetString("テキスト137.OutputFormat");
            this.テキスト137.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト137.Tag = "";
            this.テキスト137.Text = "ITEM63";
            this.テキスト137.Top = 3.490158F;
            this.テキスト137.Width = 0.892126F;
            // 
            // テキスト138
            // 
            this.テキスト138.DataField = "ITEM64";
            this.テキスト138.Height = 0.15625F;
            this.テキスト138.Left = 0.7384681F;
            this.テキスト138.Name = "テキスト138";
            this.テキスト138.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト138.Tag = "";
            this.テキスト138.Text = "ITEM64";
            this.テキスト138.Top = 3.68656F;
            this.テキスト138.Width = 0.4043307F;
            // 
            // テキスト139
            // 
            this.テキスト139.DataField = "ITEM65";
            this.テキスト139.Height = 0.1562992F;
            this.テキスト139.Left = 1.142913F;
            this.テキスト139.MultiLine = false;
            this.テキスト139.Name = "テキスト139";
            this.テキスト139.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト139.Tag = "";
            this.テキスト139.Text = "ITEM65";
            this.テキスト139.Top = 3.68674F;
            this.テキスト139.Width = 1.01811F;
            // 
            // テキスト140
            // 
            this.テキスト140.DataField = "ITEM66";
            this.テキスト140.Height = 0.15625F;
            this.テキスト140.Left = 2.204282F;
            this.テキスト140.Name = "テキスト140";
            this.テキスト140.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト140.Tag = "";
            this.テキスト140.Text = "ITEM66";
            this.テキスト140.Top = 3.682393F;
            this.テキスト140.Width = 0.1916667F;
            // 
            // テキスト141
            // 
            this.テキスト141.DataField = "ITEM67";
            this.テキスト141.Height = 0.1562992F;
            this.テキスト141.Left = 2.430709F;
            this.テキスト141.Name = "テキスト141";
            this.テキスト141.OutputFormat = resources.GetString("テキスト141.OutputFormat");
            this.テキスト141.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト141.Tag = "";
            this.テキスト141.Text = "ITEM67";
            this.テキスト141.Top = 3.686686F;
            this.テキスト141.Width = 0.892126F;
            // 
            // テキスト142
            // 
            this.テキスト142.DataField = "ITEM69";
            this.テキスト142.Height = 0.15625F;
            this.テキスト142.Left = 5.101575F;
            this.テキスト142.Name = "テキスト142";
            this.テキスト142.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト142.Tag = "";
            this.テキスト142.Text = "ITEM69";
            this.テキスト142.Top = 3.485882F;
            this.テキスト142.Width = 0.4043307F;
            // 
            // テキスト143
            // 
            this.テキスト143.DataField = "ITEM70";
            this.テキスト143.Height = 0.1562992F;
            this.テキスト143.Left = 5.505906F;
            this.テキスト143.MultiLine = false;
            this.テキスト143.Name = "テキスト143";
            this.テキスト143.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト143.Tag = "";
            this.テキスト143.Text = "ITEM70";
            this.テキスト143.Top = 3.485882F;
            this.テキスト143.Width = 1.115748F;
            // 
            // テキスト144
            // 
            this.テキスト144.DataField = "ITEM71";
            this.テキスト144.Height = 0.15625F;
            this.テキスト144.Left = 6.664961F;
            this.テキスト144.Name = "テキスト144";
            this.テキスト144.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト144.Tag = "";
            this.テキスト144.Text = "ITEM71";
            this.テキスト144.Top = 3.4858F;
            this.テキスト144.Width = 0.1916667F;
            // 
            // テキスト145
            // 
            this.テキスト145.DataField = "ITEM72";
            this.テキスト145.Height = 0.1562992F;
            this.テキスト145.Left = 6.926378F;
            this.テキスト145.Name = "テキスト145";
            this.テキスト145.OutputFormat = resources.GetString("テキスト145.OutputFormat");
            this.テキスト145.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト145.Tag = "";
            this.テキスト145.Text = "ITEM72";
            this.テキスト145.Top = 3.485882F;
            this.テキスト145.Width = 0.892126F;
            // 
            // テキスト146
            // 
            this.テキスト146.DataField = "ITEM73";
            this.テキスト146.Height = 0.15625F;
            this.テキスト146.Left = 5.101575F;
            this.テキスト146.Name = "テキスト146";
            this.テキスト146.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト146.Tag = "";
            this.テキスト146.Text = "ITEM73";
            this.テキスト146.Top = 3.682409F;
            this.テキスト146.Width = 0.4043307F;
            // 
            // テキスト147
            // 
            this.テキスト147.DataField = "ITEM74";
            this.テキスト147.Height = 0.1562992F;
            this.テキスト147.Left = 5.505906F;
            this.テキスト147.MultiLine = false;
            this.テキスト147.Name = "テキスト147";
            this.テキスト147.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト147.Tag = "";
            this.テキスト147.Text = "ITEM74";
            this.テキスト147.Top = 3.682409F;
            this.テキスト147.Width = 1.115748F;
            // 
            // テキスト148
            // 
            this.テキスト148.DataField = "ITEM75";
            this.テキスト148.Height = 0.15625F;
            this.テキスト148.Left = 6.664961F;
            this.テキスト148.Name = "テキスト148";
            this.テキスト148.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト148.Tag = "";
            this.テキスト148.Text = "ITEM75";
            this.テキスト148.Top = 3.682327F;
            this.テキスト148.Width = 0.1916667F;
            // 
            // テキスト149
            // 
            this.テキスト149.DataField = "ITEM76";
            this.テキスト149.Height = 0.1562992F;
            this.テキスト149.Left = 6.926378F;
            this.テキスト149.Name = "テキスト149";
            this.テキスト149.OutputFormat = resources.GetString("テキスト149.OutputFormat");
            this.テキスト149.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト149.Tag = "";
            this.テキスト149.Text = "ITEM76";
            this.テキスト149.Top = 3.683798F;
            this.テキスト149.Width = 0.892126F;
            // 
            // テキスト150
            // 
            this.テキスト150.DataField = "ITEM78";
            this.テキスト150.Height = 0.15625F;
            this.テキスト150.Left = 0.7384681F;
            this.テキスト150.Name = "テキスト150";
            this.テキスト150.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト150.Tag = "";
            this.テキスト150.Text = "ITEM78";
            this.テキスト150.Top = 3.962254F;
            this.テキスト150.Width = 0.4043307F;
            // 
            // テキスト151
            // 
            this.テキスト151.DataField = "ITEM79";
            this.テキスト151.Height = 0.1562992F;
            this.テキスト151.Left = 1.142913F;
            this.テキスト151.MultiLine = false;
            this.テキスト151.Name = "テキスト151";
            this.テキスト151.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト151.Tag = "";
            this.テキスト151.Text = "ITEM79";
            this.テキスト151.Top = 3.962435F;
            this.テキスト151.Width = 1.01811F;
            // 
            // テキスト152
            // 
            this.テキスト152.DataField = "ITEM80";
            this.テキスト152.Height = 0.15625F;
            this.テキスト152.Left = 2.204282F;
            this.テキスト152.Name = "テキスト152";
            this.テキスト152.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト152.Tag = "";
            this.テキスト152.Text = "ITEM80";
            this.テキスト152.Top = 3.958087F;
            this.テキスト152.Width = 0.1916667F;
            // 
            // テキスト153
            // 
            this.テキスト153.DataField = "ITEM81";
            this.テキスト153.Height = 0.1562992F;
            this.テキスト153.Left = 2.430709F;
            this.テキスト153.Name = "テキスト153";
            this.テキスト153.OutputFormat = resources.GetString("テキスト153.OutputFormat");
            this.テキスト153.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト153.Tag = "";
            this.テキスト153.Text = "ITEM81";
            this.テキスト153.Top = 3.96238F;
            this.テキスト153.Width = 0.892126F;
            // 
            // テキスト154
            // 
            this.テキスト154.DataField = "ITEM82";
            this.テキスト154.Height = 0.15625F;
            this.テキスト154.Left = 0.7384681F;
            this.テキスト154.Name = "テキスト154";
            this.テキスト154.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト154.Tag = "";
            this.テキスト154.Text = "ITEM82";
            this.テキスト154.Top = 4.158782F;
            this.テキスト154.Width = 0.4043307F;
            // 
            // テキスト155
            // 
            this.テキスト155.DataField = "ITEM83";
            this.テキスト155.Height = 0.1562992F;
            this.テキスト155.Left = 1.142913F;
            this.テキスト155.MultiLine = false;
            this.テキスト155.Name = "テキスト155";
            this.テキスト155.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト155.Tag = "";
            this.テキスト155.Text = "ITEM83";
            this.テキスト155.Top = 4.158962F;
            this.テキスト155.Width = 1.01811F;
            // 
            // テキスト156
            // 
            this.テキスト156.DataField = "ITEM84";
            this.テキスト156.Height = 0.15625F;
            this.テキスト156.Left = 2.204282F;
            this.テキスト156.Name = "テキスト156";
            this.テキスト156.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト156.Tag = "";
            this.テキスト156.Text = "ITEM84";
            this.テキスト156.Top = 4.154615F;
            this.テキスト156.Width = 0.1916667F;
            // 
            // テキスト157
            // 
            this.テキスト157.DataField = "ITEM85";
            this.テキスト157.Height = 0.1562992F;
            this.テキスト157.Left = 2.430709F;
            this.テキスト157.Name = "テキスト157";
            this.テキスト157.OutputFormat = resources.GetString("テキスト157.OutputFormat");
            this.テキスト157.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト157.Tag = "";
            this.テキスト157.Text = "ITEM85";
            this.テキスト157.Top = 4.158908F;
            this.テキスト157.Width = 0.892126F;
            // 
            // テキスト158
            // 
            this.テキスト158.DataField = "ITEM87";
            this.テキスト158.Height = 0.15625F;
            this.テキスト158.Left = 5.101575F;
            this.テキスト158.Name = "テキスト158";
            this.テキスト158.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト158.Tag = "";
            this.テキスト158.Text = "ITEM87";
            this.テキスト158.Top = 3.958104F;
            this.テキスト158.Width = 0.4043307F;
            // 
            // テキスト159
            // 
            this.テキスト159.DataField = "ITEM88";
            this.テキスト159.Height = 0.1562992F;
            this.テキスト159.Left = 5.505906F;
            this.テキスト159.MultiLine = false;
            this.テキスト159.Name = "テキスト159";
            this.テキスト159.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト159.Tag = "";
            this.テキスト159.Text = "ITEM88";
            this.テキスト159.Top = 3.958104F;
            this.テキスト159.Width = 1.115748F;
            // 
            // テキスト160
            // 
            this.テキスト160.DataField = "ITEM89";
            this.テキスト160.Height = 0.15625F;
            this.テキスト160.Left = 6.664961F;
            this.テキスト160.Name = "テキスト160";
            this.テキスト160.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト160.Tag = "";
            this.テキスト160.Text = "ITEM89";
            this.テキスト160.Top = 3.958022F;
            this.テキスト160.Width = 0.1916667F;
            // 
            // テキスト161
            // 
            this.テキスト161.DataField = "ITEM90";
            this.テキスト161.Height = 0.1562992F;
            this.テキスト161.Left = 6.926378F;
            this.テキスト161.Name = "テキスト161";
            this.テキスト161.OutputFormat = resources.GetString("テキスト161.OutputFormat");
            this.テキスト161.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト161.Tag = "";
            this.テキスト161.Text = "ITEM90";
            this.テキスト161.Top = 3.958104F;
            this.テキスト161.Width = 0.892126F;
            // 
            // テキスト162
            // 
            this.テキスト162.DataField = "ITEM91";
            this.テキスト162.Height = 0.15625F;
            this.テキスト162.Left = 5.101575F;
            this.テキスト162.Name = "テキスト162";
            this.テキスト162.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト162.Tag = "";
            this.テキスト162.Text = "ITEM91";
            this.テキスト162.Top = 4.154632F;
            this.テキスト162.Width = 0.4043307F;
            // 
            // テキスト163
            // 
            this.テキスト163.DataField = "ITEM92";
            this.テキスト163.Height = 0.1562992F;
            this.テキスト163.Left = 5.505906F;
            this.テキスト163.MultiLine = false;
            this.テキスト163.Name = "テキスト163";
            this.テキスト163.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト163.Tag = "";
            this.テキスト163.Text = "ITEM92";
            this.テキスト163.Top = 4.154632F;
            this.テキスト163.Width = 1.115748F;
            // 
            // テキスト164
            // 
            this.テキスト164.DataField = "ITEM93";
            this.テキスト164.Height = 0.15625F;
            this.テキスト164.Left = 6.664961F;
            this.テキスト164.Name = "テキスト164";
            this.テキスト164.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト164.Tag = "";
            this.テキスト164.Text = "ITEM93";
            this.テキスト164.Top = 4.15455F;
            this.テキスト164.Width = 0.1916667F;
            // 
            // テキスト165
            // 
            this.テキスト165.DataField = "ITEM94";
            this.テキスト165.Height = 0.1562992F;
            this.テキスト165.Left = 6.926378F;
            this.テキスト165.Name = "テキスト165";
            this.テキスト165.OutputFormat = resources.GetString("テキスト165.OutputFormat");
            this.テキスト165.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト165.Tag = "";
            this.テキスト165.Text = "ITEM94";
            this.テキスト165.Top = 4.15602F;
            this.テキスト165.Width = 0.892126F;
            // 
            // テキスト166
            // 
            this.テキスト166.DataField = "ITEM95";
            this.テキスト166.Height = 0.15625F;
            this.テキスト166.Left = 1.482284F;
            this.テキスト166.Name = "テキスト166";
            this.テキスト166.OutputFormat = resources.GetString("テキスト166.OutputFormat");
            this.テキスト166.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト166.Tag = "";
            this.テキスト166.Text = "ITEM95";
            this.テキスト166.Top = 4.430315F;
            this.テキスト166.Width = 1.847917F;
            // 
            // テキスト167
            // 
            this.テキスト167.DataField = "ITEM96";
            this.テキスト167.Height = 0.15625F;
            this.テキスト167.Left = 5.95887F;
            this.テキスト167.Name = "テキスト167";
            this.テキスト167.OutputFormat = resources.GetString("テキスト167.OutputFormat");
            this.テキスト167.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト167.Tag = "";
            this.テキスト167.Text = "ITEM96";
            this.テキスト167.Top = 4.430146F;
            this.テキスト167.Width = 1.847917F;
            // 
            // テキスト168
            // 
            this.テキスト168.DataField = "ITEM97";
            this.テキスト168.Height = 0.15625F;
            this.テキスト168.Left = 1.482284F;
            this.テキスト168.Name = "テキスト168";
            this.テキスト168.OutputFormat = resources.GetString("テキスト168.OutputFormat");
            this.テキスト168.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト168.Tag = "";
            this.テキスト168.Text = "ITEM97";
            this.テキスト168.Top = 4.65601F;
            this.テキスト168.Width = 1.847917F;
            // 
            // テキスト169
            // 
            this.テキスト169.DataField = "ITEM98";
            this.テキスト169.Height = 0.15625F;
            this.テキスト169.Left = 1.482284F;
            this.テキスト169.Name = "テキスト169";
            this.テキスト169.OutputFormat = resources.GetString("テキスト169.OutputFormat");
            this.テキスト169.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト169.Tag = "";
            this.テキスト169.Text = "ITEM98";
            this.テキスト169.Top = 4.852537F;
            this.テキスト169.Width = 1.847917F;
            // 
            // テキスト170
            // 
            this.テキスト170.DataField = "ITEM99";
            this.テキスト170.Height = 0.15625F;
            this.テキスト170.Left = 5.95887F;
            this.テキスト170.Name = "テキスト170";
            this.テキスト170.OutputFormat = resources.GetString("テキスト170.OutputFormat");
            this.テキスト170.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト170.Tag = "";
            this.テキスト170.Text = "ITEM99";
            this.テキスト170.Top = 4.655841F;
            this.テキスト170.Width = 1.847917F;
            // 
            // テキスト171
            // 
            this.テキスト171.DataField = "ITEM100";
            this.テキスト171.Height = 0.15625F;
            this.テキスト171.Left = 5.95887F;
            this.テキスト171.Name = "テキスト171";
            this.テキスト171.OutputFormat = resources.GetString("テキスト171.OutputFormat");
            this.テキスト171.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト171.Tag = "";
            this.テキスト171.Text = "ITEM100";
            this.テキスト171.Top = 4.852367F;
            this.テキスト171.Width = 1.847917F;
            // 
            // テキスト172
            // 
            this.テキスト172.DataField = "ITEM32";
            this.テキスト172.Height = 0.2708333F;
            this.テキスト172.Left = 3.413386F;
            this.テキスト172.Name = "テキスト172";
            this.テキスト172.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト172.Tag = "";
            this.テキスト172.Text = "ITEM32";
            this.テキスト172.Top = 2.624836F;
            this.テキスト172.Width = 1.608268F;
            // 
            // テキスト173
            // 
            this.テキスト173.DataField = "ITEM50";
            this.テキスト173.Height = 0.2708333F;
            this.テキスト173.Left = 3.413386F;
            this.テキスト173.Name = "テキスト173";
            this.テキスト173.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト173.Tag = "";
            this.テキスト173.Text = "ITEM50";
            this.テキスト173.Top = 3.090808F;
            this.テキスト173.Width = 1.608268F;
            // 
            // テキスト174
            // 
            this.テキスト174.DataField = "ITEM68";
            this.テキスト174.Height = 0.2708333F;
            this.テキスト174.Left = 3.413386F;
            this.テキスト174.Name = "テキスト174";
            this.テキスト174.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト174.Tag = "";
            this.テキスト174.Text = "ITEM68";
            this.テキスト174.Top = 3.563031F;
            this.テキスト174.Width = 1.608268F;
            // 
            // テキスト175
            // 
            this.テキスト175.DataField = "ITEM86";
            this.テキスト175.Height = 0.2708333F;
            this.テキスト175.Left = 3.413386F;
            this.テキスト175.Name = "テキスト175";
            this.テキスト175.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト175.Tag = "";
            this.テキスト175.Text = "ITEM86";
            this.テキスト175.Top = 4.035254F;
            this.テキスト175.Width = 1.608268F;
            // 
            // 直線176
            // 
            this.直線176.Height = 8.201599E-05F;
            this.直線176.Left = 0.7157481F;
            this.直線176.LineWeight = 0F;
            this.直線176.Name = "直線176";
            this.直線176.Tag = "";
            this.直線176.Top = 1.791339F;
            this.直線176.Width = 2.645555F;
            this.直線176.X1 = 0.7157481F;
            this.直線176.X2 = 3.361303F;
            this.直線176.Y1 = 1.791339F;
            this.直線176.Y2 = 1.791421F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.7090552F;
            this.line1.LineWeight = 0F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 4.629921F;
            this.line1.Width = 7.165355F;
            this.line1.X1 = 0.7090552F;
            this.line1.X2 = 7.87441F;
            this.line1.Y1 = 4.629921F;
            this.line1.Y2 = 4.629921F;
            // 
            // label2
            // 
            this.label2.Height = 0.2531497F;
            this.label2.HyperLink = null;
            this.label2.Left = 2.227559F;
            this.label2.Name = "label2";
            this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label2.Tag = "";
            this.label2.Text = "税区";
            this.label2.Top = 1.798032F;
            this.label2.Width = 0.1527777F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // ZMDR10311R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 8.229167F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト164)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト165)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト166)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト167)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト169)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト170)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト172)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト173)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト174)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト175)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM02;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス27;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス29;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM03;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線36;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線37;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル38;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル39;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル40;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル41;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル43;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル44;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル47;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス48;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線49;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線50;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線51;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線52;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線54;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線55;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線56;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線58;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線60;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線61;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線62;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線64;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線65;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線66;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル67;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル68;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス69;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル70;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス71;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル72;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス73;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル74;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス75;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル76;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線77;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線78;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト81;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト82;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト83;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト84;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト94;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト95;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト96;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト97;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト98;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト99;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト100;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト101;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト103;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト104;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト105;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト107;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト108;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト109;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト110;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト111;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト112;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト113;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト115;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト116;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト118;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト119;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト120;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト122;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト123;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト125;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト126;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト127;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト128;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト129;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト130;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト131;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト132;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト133;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト134;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト135;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト136;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト137;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト138;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト139;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト140;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト141;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト142;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト143;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト144;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト145;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト146;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト147;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト148;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト149;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト150;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト151;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト152;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト153;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト154;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト155;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト156;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト157;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト158;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト159;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト160;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト161;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト162;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト163;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト164;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト165;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト166;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト167;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト168;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト169;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト170;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト171;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト172;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト173;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト174;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト175;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線176;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス35;
    }
}
