﻿namespace jp.co.fsi.zm.zmcm1041
{
    partial class ZMCM1042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtHojoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoCd = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtHojoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.lblkanjoKanaNm = new System.Windows.Forms.Label();
            this.lblKanjoK = new System.Windows.Forms.Label();
            this.txtHojoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoNm = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 158);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(812, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(823, 31);
            this.lblTitle.Text = "補助科目の登録";
            // 
            // txtHojoCd
            // 
            this.txtHojoCd.AutoSizeFromLength = true;
            this.txtHojoCd.DisplayLength = null;
            this.txtHojoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtHojoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtHojoCd.Location = new System.Drawing.Point(142, 4);
            this.txtHojoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtHojoCd.MaxLength = 6;
            this.txtHojoCd.Name = "txtHojoCd";
            this.txtHojoCd.Size = new System.Drawing.Size(63, 23);
            this.txtHojoCd.TabIndex = 2;
            this.txtHojoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtkanjoCd_Validating);
            // 
            // lblKanjoCd
            // 
            this.lblKanjoCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKanjoCd.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoCd.Name = "lblKanjoCd";
            this.lblKanjoCd.Size = new System.Drawing.Size(755, 32);
            this.lblKanjoCd.TabIndex = 1;
            this.lblKanjoCd.Tag = "CHANGE";
            this.lblKanjoCd.Text = "補助科目コード";
            this.lblKanjoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCd
            // 
            this.txtKanjoKamokuCd.AutoSizeFromLength = true;
            this.txtKanjoKamokuCd.DisplayLength = null;
            this.txtKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKanjoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokuCd.Location = new System.Drawing.Point(426, 4);
            this.txtKanjoKamokuCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamokuCd.MaxLength = 6;
            this.txtKanjoKamokuCd.Name = "txtKanjoKamokuCd";
            this.txtKanjoKamokuCd.Size = new System.Drawing.Size(105, 23);
            this.txtKanjoKamokuCd.TabIndex = 905;
            this.txtKanjoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtHojoKanaNm
            // 
            this.txtHojoKanaNm.AutoSizeFromLength = false;
            this.txtHojoKanaNm.DisplayLength = null;
            this.txtHojoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHojoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtHojoKanaNm.Location = new System.Drawing.Point(142, 6);
            this.txtHojoKanaNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtHojoKanaNm.MaxLength = 30;
            this.txtHojoKanaNm.Name = "txtHojoKanaNm";
            this.txtHojoKanaNm.Size = new System.Drawing.Size(357, 23);
            this.txtHojoKanaNm.TabIndex = 3;
            this.txtHojoKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHojoKanaNm_KeyDown);
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(539, 3);
            this.lblKanjoKamokuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(207, 24);
            this.lblKanjoKamokuCd.TabIndex = 904;
            this.lblKanjoKamokuCd.Tag = "DISPNAME";
            this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblkanjoKanaNm
            // 
            this.lblkanjoKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblkanjoKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblkanjoKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblkanjoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblkanjoKanaNm.Location = new System.Drawing.Point(0, 0);
            this.lblkanjoKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblkanjoKanaNm.Name = "lblkanjoKanaNm";
            this.lblkanjoKanaNm.Size = new System.Drawing.Size(755, 32);
            this.lblkanjoKanaNm.TabIndex = 2;
            this.lblkanjoKanaNm.Tag = "CHANGE";
            this.lblkanjoKanaNm.Text = "補助科目カナ名";
            this.lblkanjoKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoK
            // 
            this.lblKanjoK.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoK.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoK.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKanjoK.Location = new System.Drawing.Point(207, 3);
            this.lblKanjoK.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoK.Name = "lblKanjoK";
            this.lblKanjoK.Size = new System.Drawing.Size(211, 24);
            this.lblKanjoK.TabIndex = 902;
            this.lblKanjoK.Tag = "DISPNAME";
            this.lblKanjoK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoNm
            // 
            this.txtHojoNm.AutoSizeFromLength = false;
            this.txtHojoNm.DisplayLength = null;
            this.txtHojoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHojoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtHojoNm.Location = new System.Drawing.Point(142, 4);
            this.txtHojoNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtHojoNm.MaxLength = 40;
            this.txtHojoNm.Name = "txtHojoNm";
            this.txtHojoNm.Size = new System.Drawing.Size(357, 23);
            this.txtHojoNm.TabIndex = 1;
            // 
            // lblKanjoNm
            // 
            this.lblKanjoNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKanjoNm.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoNm.Name = "lblKanjoNm";
            this.lblKanjoNm.Size = new System.Drawing.Size(755, 32);
            this.lblKanjoNm.TabIndex = 0;
            this.lblKanjoNm.Tag = "CHANGE";
            this.lblKanjoNm.Text = "補助科目名";
            this.lblKanjoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(763, 118);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtHojoKanaNm);
            this.fsiPanel3.Controls.Add(this.lblkanjoKanaNm);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 82);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(755, 32);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtHojoNm);
            this.fsiPanel2.Controls.Add(this.lblKanjoNm);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 43);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(755, 32);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.lblKanjoKamokuCd);
            this.fsiPanel1.Controls.Add(this.txtKanjoKamokuCd);
            this.fsiPanel1.Controls.Add(this.txtHojoCd);
            this.fsiPanel1.Controls.Add(this.lblKanjoK);
            this.fsiPanel1.Controls.Add(this.lblKanjoCd);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(755, 32);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // ZMCM1042
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 295);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1042";
            this.ShowFButton = true;
            this.Text = "補助科目の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtHojoCd;
        private System.Windows.Forms.Label lblKanjoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoNm;
        private System.Windows.Forms.Label lblKanjoNm;
        private System.Windows.Forms.Label lblkanjoKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKanaNm;
        private System.Windows.Forms.Label lblKanjoK;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCd;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
	}
}