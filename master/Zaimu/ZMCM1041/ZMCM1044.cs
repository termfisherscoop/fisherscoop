﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.zm.zmcm1041
{
    /// <summary>
    /// 補助科目の検索画面(ZMCM1044)
    /// </summary>
    public partial class ZMCM1044 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1044()
        {
            InitializeComponent();
        }
                /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public ZMCM1044(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            
            // 勘定科目コードは入力不可
            this.txtKnjoKamkCd.Enabled = false;
            // カナ名にフォーカス
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Focus();
            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }
        #endregion

        #region イベント
        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            //カナ名で検索されたら検索
            if (!ValChk.IsEmpty(this.txtKnjoKamkCd.Text))
            {
                SearchData(false);
            }
            //カナ入力が無かった場合データ無
            else
            {
                SearchData(true);

                Msg.Info("該当データがありません。");
                this.txtKnjoKamkCd.Focus();

            }
        }
        /// <summary>
        /// 勘定科目検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKnjoKamkCd_Validating(object sender, CancelEventArgs e)
        {
            //カナ名で検索されたら検索
            if (!ValChk.IsEmpty(this.txtKnjoKamkCd.Text))
            {
                SearchData(false);
            }
            //カナ入力が無かった場合データ無
            else
            {
                SearchData(true);
                Msg.Info("入力に誤りがあります。");
            }
        }
        
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 補助科目ビューからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            // 外部から支所コード渡しの時
            if (!ValChk.IsEmpty(this.Par2))
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            }
            else
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            }
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            
            StringBuilder where = new StringBuilder("ZM.KAISHA_CD = @KAISHA_CD");
            where.Append(" AND ZM.KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND ZM.HOJO_KAMOKU_CD IS NOT NULL");

            this.txtKnjoKamkCd.Text = Util.ToString(this.InData);
            this.lblKnjoKamok.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKnjoKamkCd.Text);

            //if (isInitial)
            //{
            //    where.Append(" AND ZM.HOJO_KAMOKU_CD = -1");
            //}
            //else
            //{
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaName.Text))
                {
                    where.Append(" AND ZM.HOJO_KAMOKU_KANA_NM LIKE @HOJO_KAMOKU_KANA_NM");

                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@HOJO_KAMOKU_KANA_NM", SqlDbType.VarChar, 32, "%" + this.txtKanaName.Text + "%");
                }
                if (txtKnjoKamkCd.Text != "0" && !ValChk.IsEmpty(txtKnjoKamkCd.Text))
                {
                    dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKnjoKamkCd.Text);
                    where.Append(" AND ZM.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
                }
                where.Append(" AND (ZM.SHISHO_CD = @SHISHO_CD or ZM.SHISHO_CD is null)");
            //}
            string cols = "ZM.HOJO_KAMOKU_CD AS コード";
            cols += ", ZM.HOJO_KAMOKU_NM AS 名称";
            cols += ", ZM.HOJO_KAMOKU_KANA_NM AS カナ名";

            string from = "VI_ZM_HOJO_KAMOKU AS ZM";

            DataTable dthojo =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "ZM.HOJO_KAMOKU_CD", dpc);

            
                
            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dthojo.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dthojo.Rows.Add(dthojo.NewRow());
            }

            this.dgvList.DataSource = dthojo;

            
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 70;
            this.dgvList.Columns[1].Width = 210;
            this.dgvList.Columns[2].Width = 210;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["名称"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["カナ名"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion






    }
}
