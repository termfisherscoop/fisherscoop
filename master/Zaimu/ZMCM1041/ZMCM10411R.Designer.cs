﻿namespace jp.co.fsi.zm.zmcm1041
{
    /// <summary>
    /// ZAMC9021R の概要の説明です。
    /// </summary>
    partial class ZMCM10411R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMCM10411R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape1,
            this.textBox1,
            this.label1,
            this.line1,
            this.txtToday,
            this.txtPage,
            this.label2,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line7,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8});
            this.pageHeader.Height = 0.9389764F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.shape1.Height = 0.4271654F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F);
            this.shape1.Top = 0.511811F;
            this.shape1.Width = 7.283465F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM01";
            this.textBox1.Height = 0.1979167F;
            this.textBox1.Left = 0F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center";
            this.textBox1.Text = "ITEM01";
            this.textBox1.Top = 0.2362205F;
            this.textBox1.Width = 2.218504F;
            // 
            // label1
            // 
            this.label1.Height = 0.2362205F;
            this.label1.HyperLink = null;
            this.label1.Left = 2.834646F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center";
            this.label1.Text = "補助科目リスト";
            this.label1.Top = 0F;
            this.label1.Width = 2.204725F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.755906F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2559054F;
            this.line1.Width = 2.362204F;
            this.line1.X1 = 2.755906F;
            this.line1.X2 = 5.11811F;
            this.line1.Y1 = 0.2559054F;
            this.line1.Y2 = 0.2559054F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.1968504F;
            this.txtToday.Left = 5.658268F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.txtToday.Top = 0.07401575F;
            this.txtToday.Width = 1.041339F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1968504F;
            this.txtPage.Left = 6.699607F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = null;
            this.txtPage.Top = 0.07401575F;
            this.txtPage.Width = 0.2992125F;
            // 
            // label2
            // 
            this.label2.Height = 0.1968504F;
            this.label2.HyperLink = null;
            this.label2.Left = 6.998819F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.label2.Text = "頁\r\n";
            this.label2.Top = 0.07401577F;
            this.label2.Width = 0.1968504F;
            // 
            // line2
            // 
            this.line2.Height = 0.4133855F;
            this.line2.Left = 0.5905511F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.511811F;
            this.line2.Width = 0F;
            this.line2.X1 = 0.5905511F;
            this.line2.X2 = 0.5905511F;
            this.line2.Y1 = 0.511811F;
            this.line2.Y2 = 0.9251965F;
            // 
            // line3
            // 
            this.line3.Height = 0.4133855F;
            this.line3.Left = 2.165354F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.511811F;
            this.line3.Width = 0F;
            this.line3.X1 = 2.165354F;
            this.line3.X2 = 2.165354F;
            this.line3.Y1 = 0.511811F;
            this.line3.Y2 = 0.9251965F;
            // 
            // line4
            // 
            this.line4.Height = 0.4133855F;
            this.line4.Left = 3.641732F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.511811F;
            this.line4.Width = 0F;
            this.line4.X1 = 3.641732F;
            this.line4.X2 = 3.641732F;
            this.line4.Y1 = 0.511811F;
            this.line4.Y2 = 0.9251965F;
            // 
            // line5
            // 
            this.line5.Height = 0.4133855F;
            this.line5.Left = 4.232284F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.511811F;
            this.line5.Width = 0F;
            this.line5.X1 = 4.232284F;
            this.line5.X2 = 4.232284F;
            this.line5.Y1 = 0.511811F;
            this.line5.Y2 = 0.9251965F;
            // 
            // line7
            // 
            this.line7.Height = 0.4133855F;
            this.line7.Left = 5.807086F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.511811F;
            this.line7.Width = 0F;
            this.line7.X1 = 5.807086F;
            this.line7.X2 = 5.807086F;
            this.line7.Y1 = 0.511811F;
            this.line7.Y2 = 0.9251965F;
            // 
            // label3
            // 
            this.label3.Height = 0.2452761F;
            this.label3.HyperLink = null;
            this.label3.Left = 0F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.label3.Text = "コード";
            this.label3.Top = 0.6299213F;
            this.label3.Width = 0.5748032F;
            // 
            // label4
            // 
            this.label4.Height = 0.2452761F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.6299213F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.label4.Text = "補助科目名\r\n";
            this.label4.Top = 0.6299213F;
            this.label4.Width = 1.515748F;
            // 
            // label5
            // 
            this.label5.Height = 0.2452761F;
            this.label5.HyperLink = null;
            this.label5.Left = 2.204725F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.label5.Text = "カ　ナ　名";
            this.label5.Top = 0.6299213F;
            this.label5.Width = 1.411417F;
            // 
            // label6
            // 
            this.label6.Height = 0.2452756F;
            this.label6.HyperLink = null;
            this.label6.Left = 3.681103F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.label6.Text = "コード";
            this.label6.Top = 0.6299213F;
            this.label6.Width = 0.5157483F;
            // 
            // label7
            // 
            this.label7.Height = 0.2452761F;
            this.label7.HyperLink = null;
            this.label7.Left = 4.271654F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.label7.Text = "補助科目名";
            this.label7.Top = 0.6299213F;
            this.label7.Width = 1.496063F;
            // 
            // label8
            // 
            this.label8.Height = 0.2452761F;
            this.label8.HyperLink = null;
            this.label8.Left = 5.84252F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.label8.Text = "カ　ナ　名";
            this.label8.Top = 0.6299213F;
            this.label8.Width = 1.39567F;
            // 
            // detail
            // 
            this.detail.ColumnCount = 2;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox2,
            this.line6,
            this.line8,
            this.line10,
            this.line11,
            this.line12,
            this.textBox3,
            this.textBox4});
            this.detail.Height = 0.2440945F;
            this.detail.Name = "detail";
            // 
            // textBox2
            // 
            this.textBox2.CanGrow = false;
            this.textBox2.DataField = "ITEM02";
            this.textBox2.Height = 0.1968504F;
            this.textBox2.Left = 0F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: bottom;" +
    " ddo-char-set: 128";
            this.textBox2.Text = "ITEM02\r\n";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 0.5039371F;
            // 
            // line6
            // 
            this.line6.Height = 0F;
            this.line6.Left = 0F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.2440945F;
            this.line6.Width = 7.283465F;
            this.line6.X1 = 0F;
            this.line6.X2 = 7.283465F;
            this.line6.Y1 = 0.2440945F;
            this.line6.Y2 = 0.2440945F;
            // 
            // line8
            // 
            this.line8.Height = 0.2440945F;
            this.line8.Left = 0F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 0F;
            this.line8.X2 = 0F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.2440945F;
            // 
            // line10
            // 
            this.line10.Height = 0.2440945F;
            this.line10.Left = 0.5905512F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 0.5905512F;
            this.line10.X2 = 0.5905512F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.2440945F;
            // 
            // line11
            // 
            this.line11.Height = 0.2440945F;
            this.line11.Left = 2.165354F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 2.165354F;
            this.line11.X2 = 2.165354F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.2440945F;
            // 
            // line12
            // 
            this.line12.Height = 0.2440945F;
            this.line12.Left = 3.641732F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 3.641732F;
            this.line12.X2 = 3.641732F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.2440945F;
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = false;
            this.textBox3.DataField = "ITEM03";
            this.textBox3.Height = 0.1968504F;
            this.textBox3.Left = 0.6377953F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: bottom; ddo-char-set: 128";
            this.textBox3.Text = "ITEM03\r\n";
            this.textBox3.Top = 0F;
            this.textBox3.Width = 1.452756F;
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.DataField = "ITEM04";
            this.textBox4.Height = 0.1968504F;
            this.textBox4.Left = 2.236221F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: bottom; ddo-char-set: 128";
            this.textBox4.Text = "ITEM04\r\n";
            this.textBox4.Top = 0F;
            this.textBox4.Width = 1.346457F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // ZAMC9021R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.1968504F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.283465F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
    }
}
