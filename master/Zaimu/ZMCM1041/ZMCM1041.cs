﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.zm.zmcm1041
{
    /// <summary>
    /// 補助科目の登録(ZMCM1041)
    /// </summary>
    public partial class ZMCM1041 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1041()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // サイズを縮める
                this.Size = new Size(600, 500);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // 勘定科目にフォーカス
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKnjoKamkCd.Focus();

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtKnjoKamkCd":    // 勘定科目コード
                    this.btnF1.Enabled = true;     // 検索
                    // 表示済みがある場合は初期表示へ
                    if (this.dgvList.RowCount != 0)
                    {
                        SearchData(true);
                    }
                    break;
                case "txtKanaName":      // カナ名
                    this.btnF1.Enabled = false;    // 検索
                    // 表示済みがある場合は初期表示へ
                    if (this.dgvList.RowCount != 0)
                    {
                        SearchData(true);
                    }
                    break;
                default:
                    this.btnF1.Enabled = false;    // 検索
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			switch (this.ActiveCtlNm)
            {
                case "txtKnjoKamkCd":
                    #region 勘定科目
                    //アセンブリのロード
                    //System.Reflection.Assembly asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                    Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //Type t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9013"); ;
                    Type t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtKnjoKamkCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKnjoKamkCd.Text = result[0];
                            }
                        }
                    }
                    #endregion
                    break;

                default:
                    break;
            }
        
            // カナ名にフォーカスを戻す
            this.txtKnjoKamkCd.Focus();
            this.txtKanaName.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ補助科目登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1) && btnF4.Enabled == true)
            {
                // 補助科目登録画面の起動
                EditHojo(string.Empty);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // メンテ機能で立ち上げている場合のみ得意先マスタ一覧を立ち上げる
            if (ValChk.IsEmpty(this.Par1) && btnF5.Enabled == true)
            {
                // 補助科目リストの起動
                ItiranHojoKamoku();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            //カナ名で検索されたら検索
            if (!ValChk.IsEmpty(this.txtKnjoKamkCd.Text))
            {
                SearchData(false);
            }
            //カナ入力が無かった場合データ無
            else
            {
                SearchData(true);

                Msg.Info("該当データがありません。");
                this.txtKnjoKamkCd.Focus();

            }
        }

        /// <summary>
        /// 勘定科目検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKnjoKamkCd_Validating(object sender, CancelEventArgs e)
        {
            if (!ValChk.IsEmpty(this.txtKnjoKamkCd.Text) && ValChk.IsNumber(this.txtKnjoKamkCd.Text))
            {
                SearchData(false);
            }
            //勘定科目に入力が無かった場合　数字でない場合
            //else
            //{
            //    SearchData(true);
            //    Msg.Info("入力に誤りがあります。");
            //    this.txtKnjoKamkCd.Focus();
            //}
            if (!IsValidKanjoKamoku())
            {
                e.Cancel = true;
                this.txtKnjoKamkCd.SelectAll();
            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    try
                    {
                        EditHojo(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
                        e.Handled = true;
                    }
                    catch (Exception) { }
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditHojo(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 補助科目ビューからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            // 外部から支所コード渡しの時
            if (!ValChk.IsEmpty(this.Par2))
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            }
            else
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            }
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            
            StringBuilder where = new StringBuilder("ZM.KAISHA_CD = @KAISHA_CD");
            where.AppendLine(" AND ZM.KAIKEI_NENDO = @KAIKEI_NENDO");
            where.AppendLine(" AND ZM.HOJO_KAMOKU_CD IS NOT NULL");

            if (isInitial)
            {
                where.AppendLine(" AND ZM.HOJO_KAMOKU_CD = -1");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaName.Text))
                {
                    where.AppendLine(" AND ZM.HOJO_KAMOKU_KANA_NM LIKE @HOJO_KAMOKU_KANA_NM");

                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@HOJO_KAMOKU_KANA_NM", SqlDbType.VarChar, 32, "%" + this.txtKanaName.Text + "%");
                }
                if (txtKnjoKamkCd.Text != "0" && !ValChk.IsEmpty(txtKnjoKamkCd.Text))
                {
                    where.AppendLine(" AND ZM.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
                    dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 7, Util.ToDecimal(this.txtKnjoKamkCd.Text));
                    this.lblKnjoKamok.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKnjoKamkCd.Text);
                }
                where.AppendLine(" AND (ZM.SHISHO_CD = @SHISHO_CD or ZM.SHISHO_CD is null)");
            }
            string cols = "ZM.HOJO_KAMOKU_CD AS コード";
            cols += ", ZM.HOJO_KAMOKU_NM AS 補助科目名";
            cols += ", ZM.HOJO_KAMOKU_KANA_NM AS カナ名";

            string from = "VI_ZM_HOJO_KAMOKU AS ZM";

            DataTable dthojo =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "ZM.HOJO_KAMOKU_CD", dpc);
                
            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dthojo.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                    //TODO 03/10 CHIBANA 
                    btnF4.Enabled = false;
                    btnF5.Enabled = false;
                }

                //dthojo.Rows.Add(dthojo.NewRow());
            }

            this.dgvList.DataSource = dthojo;
            
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 90;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 300;
            this.dgvList.Columns[2].Width = 270;

            this.dgvList.CurrentCell = null;
        }

        /// <summary>
        /// 補助科目を追加編集する
        /// </summary>
        /// <param name="code">補助科目コード(空：新規登録、以外：編集)</param>
        private void EditHojo(string code)
        {
            //　SQLを組んで勘定科目を参照して
            //　VI_ZM_KANJO_KAMOKUから補助使用区分からデータ取得

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 7, Util.ToDecimal(this.txtKnjoKamkCd.Text));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            StringBuilder sql = new StringBuilder();
            sql.AppendLine(" SELECT ");
            sql.AppendLine(" HOJO_SHIYO_KUBUN ");
            sql.AppendLine(" FROM ");
            sql.AppendLine(" VI_ZM_KANJO_KAMOKU ");
            sql.AppendLine(" WHERE ");
            sql.AppendLine(" KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD ");
            sql.AppendLine(" AND ");
            sql.AppendLine(" KAIKEI_NENDO = @KAIKEI_NENDO ");

            DataTable dtKanjo = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if (dtKanjo.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return;
            }
            if (Util.ToDecimal(dtKanjo.Rows[0]["HOJO_SHIYO_KUBUN"]).Equals(1))
            {
                ZMCM1042 frm;

                string[] KamokuCd = new string[2];

                KamokuCd[0] = this.txtKnjoKamkCd.Text;
                KamokuCd[1] = code;

                if (ValChk.IsEmpty(code))
                {
                    // 新規登録モードで登録画面を起動
                    frm = new ZMCM1042("1");
                    frm.InData = KamokuCd;
                }
                else
                {
                    // 編集モードで登録画面を起動
                    frm = new ZMCM1042("2");
                    frm.InData = KamokuCd;
                }

                // 外部から支所コード渡しの時
                if (!ValChk.IsEmpty(this.Par2))
                {
                    frm.Par2 = this.Par2;
                }

                DialogResult result = frm.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    // データを再検索する
                    SearchData(false);
                    // 元々選択していたデータを選択
                    for (int i = 0; i < this.dgvList.Rows.Count; i++)
                    {
                        if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["コード"].Value)))
                        {
                            this.dgvList.FirstDisplayedScrollingRowIndex = i;
                            this.dgvList.Rows[i].Selected = true;
                            break;
                        }
                    }
                    // Gridに再度フォーカスをセット
                    this.ActiveControl = this.dgvList;
                    this.dgvList.Focus();
                }
            }
            else
            {
                ZMCM1043 frm;

                // 新規作成時
                if (ValChk.IsEmpty(code))
                {
                    // 新規登録モードで登録画面を起動
                    frm = new ZMCM1043("1");
                    frm.InData = this.txtKnjoKamkCd.Text;
                }
                // 編集時
                else
                {
                    string[] KamokuCd = new string[2];

                    KamokuCd[0] = this.txtKnjoKamkCd.Text;
                    KamokuCd[1] = code;

                    // 編集モードで登録画面を起動
                    frm = new ZMCM1043("2");
                    frm.InData = KamokuCd;

                }

                DialogResult result = frm.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    // データを再検索する
                    SearchData(false);
                    // 元々選択していたデータを選択
                    for (int i = 0; i < this.dgvList.Rows.Count; i++)
                    {
                        if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["コード"].Value)))
                        {
                            this.dgvList.FirstDisplayedScrollingRowIndex = i;
                            this.dgvList.Rows[i].Selected = true;
                            break;
                        }
                    }
                    // Gridに再度フォーカスをセット
                    this.ActiveControl = this.dgvList;
                    this.dgvList.Focus();
                }
            }
        }

        /// <summary>
        /// 得意先マスタ一覧画面を起動する
        /// </summary>
        /// <param name="code"></param>
        private void ItiranHojoKamoku()
        {
            ZMCM1045 frm;
            // 得意先マスタ一覧画面を起動
            frm = new ZMCM1045();

            DialogResult result = frm.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["補助科目名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["カナ名"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 勘定科目コードの入力チェック
        /// </summary>
        /// <returns></returns>
        private bool IsValidKanjoKamoku()
        {
            if (ValChk.IsEmpty(this.txtKnjoKamkCd.Text))
            {
                Msg.Notice("勘定科目コードを入力してください。");
                this.txtKnjoKamkCd.SelectAll();
                return false;
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKnjoKamkCd.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する

                this.lblKnjoKamok.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKnjoKamkCd.Text);
                if (this.lblKnjoKamok.Text == "")
                {
                    Msg.Notice("勘定科目コードを入力してください。");
                    this.txtKnjoKamkCd.SelectAll();
                    return false;
                }
            }

            return true;
        }
        #endregion
    }
}
