﻿namespace jp.co.fsi.zm.zmcm1041
{
    partial class ZMCM1041
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblKanaName = new System.Windows.Forms.Label();
            this.txtKanaName = new System.Windows.Forms.TextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.txtKnjoKamkCd = new System.Windows.Forms.TextBox();
            this.lblKnjoKamk = new System.Windows.Forms.Label();
            this.lblKnjoKamok = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 585);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(948, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(938, 41);
            this.lblTitle.Text = "補助科目の登録";
            // 
            // lblKanaName
            // 
            this.lblKanaName.BackColor = System.Drawing.Color.Silver;
            this.lblKanaName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanaName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanaName.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanaName.Location = new System.Drawing.Point(0, 0);
            this.lblKanaName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanaName.Name = "lblKanaName";
            this.lblKanaName.Size = new System.Drawing.Size(395, 32);
            this.lblKanaName.TabIndex = 1;
            this.lblKanaName.Tag = "CHANGE";
            this.lblKanaName.Text = "カ　ナ　名";
            this.lblKanaName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaName
            // 
            this.txtKanaName.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Location = new System.Drawing.Point(93, 5);
            this.txtKanaName.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanaName.MaxLength = 30;
            this.txtKanaName.Name = "txtKanaName";
            this.txtKanaName.Size = new System.Drawing.Size(239, 23);
            this.txtKanaName.TabIndex = 2;
            this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.EnableHeadersVisualStyles = false;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(17, 91);
            this.dgvList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(899, 471);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // txtKnjoKamkCd
            // 
            this.txtKnjoKamkCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKnjoKamkCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKnjoKamkCd.Location = new System.Drawing.Point(99, 5);
            this.txtKnjoKamkCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtKnjoKamkCd.MaxLength = 6;
            this.txtKnjoKamkCd.Name = "txtKnjoKamkCd";
            this.txtKnjoKamkCd.Size = new System.Drawing.Size(103, 23);
            this.txtKnjoKamkCd.TabIndex = 1;
            this.txtKnjoKamkCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKnjoKamkCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKnjoKamkCd_Validating);
            // 
            // lblKnjoKamk
            // 
            this.lblKnjoKamk.BackColor = System.Drawing.Color.Silver;
            this.lblKnjoKamk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKnjoKamk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKnjoKamk.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKnjoKamk.Location = new System.Drawing.Point(0, 0);
            this.lblKnjoKamk.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKnjoKamk.Name = "lblKnjoKamk";
            this.lblKnjoKamk.Size = new System.Drawing.Size(504, 32);
            this.lblKnjoKamk.TabIndex = 902;
            this.lblKnjoKamk.Tag = "CHANGE";
            this.lblKnjoKamk.Text = "勘 定 科 目";
            this.lblKnjoKamk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKnjoKamok
            // 
            this.lblKnjoKamok.BackColor = System.Drawing.Color.LightCyan;
            this.lblKnjoKamok.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKnjoKamok.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKnjoKamok.Location = new System.Drawing.Point(210, 4);
            this.lblKnjoKamok.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKnjoKamok.Name = "lblKnjoKamok";
            this.lblKnjoKamok.Size = new System.Drawing.Size(284, 24);
            this.lblKnjoKamok.TabIndex = 904;
            this.lblKnjoKamok.Tag = "DISPNAME";
            this.lblKnjoKamok.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(17, 48);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 1;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(907, 40);
            this.fsiTableLayoutPanel1.TabIndex = 905;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel3);
            this.fsiPanel1.Controls.Add(this.fsiPanel2);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(899, 32);
            this.fsiPanel1.TabIndex = 906;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtKanaName);
            this.fsiPanel3.Controls.Add(this.lblKanaName);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(504, 0);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(395, 32);
            this.fsiPanel3.TabIndex = 906;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtKnjoKamkCd);
            this.fsiPanel2.Controls.Add(this.lblKnjoKamok);
            this.fsiPanel2.Controls.Add(this.lblKnjoKamk);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel2.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(504, 32);
            this.fsiPanel2.TabIndex = 906;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // ZMCM1041
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 721);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1041";
            this.Text = "補助科目の登録";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKanaName;
        private System.Windows.Forms.TextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.TextBox txtKnjoKamkCd;
        private System.Windows.Forms.Label lblKnjoKamk;
        private System.Windows.Forms.Label lblKnjoKamok;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel1;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
    }
}