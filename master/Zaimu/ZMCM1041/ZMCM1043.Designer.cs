﻿namespace jp.co.fsi.zm.zmcm1041
{
    partial class ZMCM1043
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtToriSkCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblToriSkCd = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.txtFax = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoK = new System.Windows.Forms.Label();
            this.txtDenwa = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtYubin2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYubin1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtToriSkKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblToriSkKanaNm = new System.Windows.Forms.Label();
            this.txtToriSkNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblToriSkNm = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 335);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(763, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(774, 31);
            this.lblTitle.Text = "取引先の登録";
            // 
            // txtToriSkCd
            // 
            this.txtToriSkCd.AutoSizeFromLength = true;
            this.txtToriSkCd.DisplayLength = null;
            this.txtToriSkCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtToriSkCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtToriSkCd.Location = new System.Drawing.Point(132, 3);
            this.txtToriSkCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtToriSkCd.MaxLength = 4;
            this.txtToriSkCd.Name = "txtToriSkCd";
            this.txtToriSkCd.Size = new System.Drawing.Size(63, 23);
            this.txtToriSkCd.TabIndex = 1;
            this.txtToriSkCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtToriSkCd.TextChanged += new System.EventHandler(this.txtToriSkCd_TextChanged);
            this.txtToriSkCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtToriSk_Validating);
            // 
            // lblToriSkCd
            // 
            this.lblToriSkCd.BackColor = System.Drawing.Color.Silver;
            this.lblToriSkCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblToriSkCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblToriSkCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblToriSkCd.Location = new System.Drawing.Point(0, 0);
            this.lblToriSkCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblToriSkCd.Name = "lblToriSkCd";
            this.lblToriSkCd.Size = new System.Drawing.Size(742, 28);
            this.lblToriSkCd.TabIndex = 1;
            this.lblToriSkCd.Tag = "CHANGE";
            this.lblToriSkCd.Text = "取引先コード";
            this.lblToriSkCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblToriSkCd.Click += new System.EventHandler(this.lblToriSkCd_Click);
            // 
            // txtKanjoKamokuCd
            // 
            this.txtKanjoKamokuCd.AutoSizeFromLength = true;
            this.txtKanjoKamokuCd.DisplayLength = null;
            this.txtKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokuCd.Location = new System.Drawing.Point(417, 3);
            this.txtKanjoKamokuCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamokuCd.MaxLength = 6;
            this.txtKanjoKamokuCd.Name = "txtKanjoKamokuCd";
            this.txtKanjoKamokuCd.Size = new System.Drawing.Size(105, 23);
            this.txtKanjoKamokuCd.TabIndex = 999;
            this.txtKanjoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCd.TextChanged += new System.EventHandler(this.txtKanjoKamokuCd_TextChanged);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(172, 2);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 24);
            this.label6.TabIndex = 1000;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "-";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(523, 2);
            this.lblKanjoKamokuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(207, 24);
            this.lblKanjoKamokuCd.TabIndex = 904;
            this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblKanjoKamokuCd.Click += new System.EventHandler(this.lblKanjoKamokuCd_Click);
            // 
            // txtFax
            // 
            this.txtFax.AutoSizeFromLength = false;
            this.txtFax.DisplayLength = null;
            this.txtFax.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFax.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFax.Location = new System.Drawing.Point(132, 3);
            this.txtFax.Margin = new System.Windows.Forms.Padding(4);
            this.txtFax.MaxLength = 15;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(168, 23);
            this.txtFax.TabIndex = 9;
            this.txtFax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFax_KeyDown);
            // 
            // lblKanjoK
            // 
            this.lblKanjoK.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoK.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoK.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoK.Location = new System.Drawing.Point(196, 2);
            this.lblKanjoK.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoK.Name = "lblKanjoK";
            this.lblKanjoK.Size = new System.Drawing.Size(209, 24);
            this.lblKanjoK.TabIndex = 902;
            this.lblKanjoK.Tag = "DISPNAME";
            this.lblKanjoK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDenwa
            // 
            this.txtDenwa.AutoSizeFromLength = false;
            this.txtDenwa.DisplayLength = null;
            this.txtDenwa.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenwa.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDenwa.Location = new System.Drawing.Point(132, 3);
            this.txtDenwa.Margin = new System.Windows.Forms.Padding(4);
            this.txtDenwa.MaxLength = 15;
            this.txtDenwa.Name = "txtDenwa";
            this.txtDenwa.Size = new System.Drawing.Size(167, 23);
            this.txtDenwa.TabIndex = 8;
            // 
            // txtJusho2
            // 
            this.txtJusho2.AutoSizeFromLength = false;
            this.txtJusho2.DisplayLength = null;
            this.txtJusho2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho2.Location = new System.Drawing.Point(132, 1);
            this.txtJusho2.Margin = new System.Windows.Forms.Padding(4);
            this.txtJusho2.MaxLength = 30;
            this.txtJusho2.Name = "txtJusho2";
            this.txtJusho2.Size = new System.Drawing.Size(357, 23);
            this.txtJusho2.TabIndex = 7;
            // 
            // txtJusho1
            // 
            this.txtJusho1.AutoSizeFromLength = false;
            this.txtJusho1.DisplayLength = null;
            this.txtJusho1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho1.Location = new System.Drawing.Point(132, 3);
            this.txtJusho1.Margin = new System.Windows.Forms.Padding(4);
            this.txtJusho1.MaxLength = 30;
            this.txtJusho1.Name = "txtJusho1";
            this.txtJusho1.Size = new System.Drawing.Size(357, 23);
            this.txtJusho1.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(742, 28);
            this.label5.TabIndex = 10;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "FAX番号";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(742, 28);
            this.label4.TabIndex = 9;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "電話番号";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(742, 28);
            this.label3.TabIndex = 8;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "住所2";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(742, 28);
            this.label2.TabIndex = 7;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "住所1";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYubin2
            // 
            this.txtYubin2.AutoSizeFromLength = false;
            this.txtYubin2.DisplayLength = null;
            this.txtYubin2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubin2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYubin2.Location = new System.Drawing.Point(200, 3);
            this.txtYubin2.Margin = new System.Windows.Forms.Padding(4);
            this.txtYubin2.MaxLength = 4;
            this.txtYubin2.Name = "txtYubin2";
            this.txtYubin2.Size = new System.Drawing.Size(51, 23);
            this.txtYubin2.TabIndex = 5;
            // 
            // txtYubin1
            // 
            this.txtYubin1.AutoSizeFromLength = false;
            this.txtYubin1.DisplayLength = null;
            this.txtYubin1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubin1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYubin1.Location = new System.Drawing.Point(132, 3);
            this.txtYubin1.Margin = new System.Windows.Forms.Padding(4);
            this.txtYubin1.MaxLength = 3;
            this.txtYubin1.Name = "txtYubin1";
            this.txtYubin1.Size = new System.Drawing.Size(36, 23);
            this.txtYubin1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(742, 28);
            this.label1.TabIndex = 4;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "郵便";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtToriSkKanaNm
            // 
            this.txtToriSkKanaNm.AutoSizeFromLength = false;
            this.txtToriSkKanaNm.DisplayLength = null;
            this.txtToriSkKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtToriSkKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtToriSkKanaNm.Location = new System.Drawing.Point(132, 3);
            this.txtToriSkKanaNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtToriSkKanaNm.MaxLength = 30;
            this.txtToriSkKanaNm.Name = "txtToriSkKanaNm";
            this.txtToriSkKanaNm.Size = new System.Drawing.Size(357, 23);
            this.txtToriSkKanaNm.TabIndex = 3;
            // 
            // lblToriSkKanaNm
            // 
            this.lblToriSkKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblToriSkKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblToriSkKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblToriSkKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblToriSkKanaNm.Location = new System.Drawing.Point(0, 0);
            this.lblToriSkKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblToriSkKanaNm.Name = "lblToriSkKanaNm";
            this.lblToriSkKanaNm.Size = new System.Drawing.Size(742, 28);
            this.lblToriSkKanaNm.TabIndex = 2;
            this.lblToriSkKanaNm.Tag = "CHANGE";
            this.lblToriSkKanaNm.Text = "取引先カナ名";
            this.lblToriSkKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtToriSkNm
            // 
            this.txtToriSkNm.AutoSizeFromLength = false;
            this.txtToriSkNm.DisplayLength = null;
            this.txtToriSkNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtToriSkNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtToriSkNm.Location = new System.Drawing.Point(132, 3);
            this.txtToriSkNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtToriSkNm.MaxLength = 40;
            this.txtToriSkNm.Name = "txtToriSkNm";
            this.txtToriSkNm.Size = new System.Drawing.Size(357, 23);
            this.txtToriSkNm.TabIndex = 2;
            // 
            // lblToriSkNm
            // 
            this.lblToriSkNm.BackColor = System.Drawing.Color.Silver;
            this.lblToriSkNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblToriSkNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblToriSkNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblToriSkNm.Location = new System.Drawing.Point(0, 0);
            this.lblToriSkNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblToriSkNm.Name = "lblToriSkNm";
            this.lblToriSkNm.Size = new System.Drawing.Size(742, 28);
            this.lblToriSkNm.TabIndex = 0;
            this.lblToriSkNm.Tag = "CHANGE";
            this.lblToriSkNm.Text = "取引先名";
            this.lblToriSkNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 7);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 8;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(750, 281);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.txtFax);
            this.fsiPanel8.Controls.Add(this.label5);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(4, 249);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(742, 28);
            this.fsiPanel8.TabIndex = 7;
            this.fsiPanel8.Tag = "CHAGE";
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.txtDenwa);
            this.fsiPanel7.Controls.Add(this.label4);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(4, 214);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(742, 28);
            this.fsiPanel7.TabIndex = 6;
            this.fsiPanel7.Tag = "CHAGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtJusho2);
            this.fsiPanel6.Controls.Add(this.label3);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 179);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(742, 28);
            this.fsiPanel6.TabIndex = 5;
            this.fsiPanel6.Tag = "CHAGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtJusho1);
            this.fsiPanel5.Controls.Add(this.label2);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 144);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(742, 28);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHAGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.label6);
            this.fsiPanel4.Controls.Add(this.txtYubin1);
            this.fsiPanel4.Controls.Add(this.txtYubin2);
            this.fsiPanel4.Controls.Add(this.label1);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 109);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(742, 28);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHAGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtToriSkKanaNm);
            this.fsiPanel3.Controls.Add(this.lblToriSkKanaNm);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 74);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(742, 28);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHAGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtToriSkNm);
            this.fsiPanel2.Controls.Add(this.lblToriSkNm);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 39);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(742, 28);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHAGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtKanjoKamokuCd);
            this.fsiPanel1.Controls.Add(this.txtToriSkCd);
            this.fsiPanel1.Controls.Add(this.lblKanjoK);
            this.fsiPanel1.Controls.Add(this.lblKanjoKamokuCd);
            this.fsiPanel1.Controls.Add(this.lblToriSkCd);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(742, 28);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHAGE";
            // 
            // ZMCM1043
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 472);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1043";
            this.ShowFButton = true;
            this.Text = "取引先の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtToriSkCd;
        private System.Windows.Forms.Label lblToriSkCd;
        private jp.co.fsi.common.controls.FsiTextBox txtToriSkNm;
        private System.Windows.Forms.Label lblToriSkNm;
        private System.Windows.Forms.Label lblToriSkKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtToriSkKanaNm;
        private System.Windows.Forms.Label lblKanjoK;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private jp.co.fsi.common.controls.FsiTextBox txtYubin2;
        private jp.co.fsi.common.controls.FsiTextBox txtYubin1;
        private System.Windows.Forms.Label label1;
        private jp.co.fsi.common.controls.FsiTextBox txtFax;
        private jp.co.fsi.common.controls.FsiTextBox txtDenwa;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho2;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho1;
        private System.Windows.Forms.Label label6;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}