﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.zm.zmcm1041
{
    /// <summary>
    /// 補助科目一覧(ZMCM1045)
    /// </summary>
    public partial class ZMCM1045 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 末日を表す固定値
        /// </summary>
        private const string MATSUJITSU = "99";
        #endregion

        #region 変数
        private bool _dtFlg = new bool();
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1045()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // ボタンの位置調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;
            // フォーカス設定
            this.txtKanjoCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            //勘定科目コードに、
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtKanjoCdFr":
                case "txtKanjoCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtKanjoCdFr":
                    #region 勘定科目
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("SKDC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.skd.skdc9011.SKDC9011");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoCdFr.Text = outData[0];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKanjoCdTo":
                    #region 勘定科目
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("SKDC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.skd.skdc9011.SKDC9011");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoCdTo.Text = outData[0];
                            }
                        }
                    }
                    #endregion
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if(Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 勘定科目コードの検証(先頭)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKanjoCdFr.Text);
            this.lblKanjoCdFr.Text = name;
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.lblKanjoCdFr.Text))
            {
                this.lblKanjoCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 勘定科目コードの検証(最後)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKanjoCdTo.Text);
            this.lblKanjoCdTo.Text = name;
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.lblKanjoCdTo.Text))
            {
                this.lblKanjoCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 勘定科目のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this._dtFlg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // ﾌﾟﾚﾋﾞｭｰ処理の呼び出し
                this.btnF4.PerformClick();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 勘定科目コードの入力チェック(先頭）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTokuisakiCdFr()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKanjoCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 勘定科目コードの入力チェック(最後）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTokuisakiCdTo()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKanjoCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 得意先コードのチェック
            if (!IsValidTokuisakiCdFr())
            {
                this.txtKanjoCdFr.Focus();
                this.txtKanjoCdFr.SelectAll();
                return false;
            }

            // 得意先コードのチェック
            if (!IsValidTokuisakiCdTo())
            {
                this.txtKanjoCdTo.Focus();
                this.txtKanjoCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMCM10411R rpt = new ZMCM10411R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }


        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 得意先コード設定
            string kanjoKamokuCdFr;
            string kanjoKamokuCdTo;
            if (Util.ToDecimal(txtKanjoCdFr.Text) > 0)
            {
                kanjoKamokuCdFr = txtKanjoCdFr.Text;
            }
            else
            {
                kanjoKamokuCdFr = "0";
            }
            if (Util.ToDecimal(txtKanjoCdTo.Text) > 0)
            {
                kanjoKamokuCdTo = txtKanjoCdTo.Text;
            }
            else
            {
                kanjoKamokuCdTo = "999999";
            }
            int i = 0; // ループ用カウント変数
            int j = 0; // データカウント用変数
            int dbSORT = 1;
            int maxRows = 80;
            decimal kanjoKamokuCd = -1; // 勘定科目比較用変数
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("    * ");
            sql.Append(" FROM ");
            sql.Append("    VI_ZM_HOJO_KAMOKU AS A");
            sql.Append(" WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD AND");
            sql.Append("	KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            sql.Append("    KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND");
            sql.Append("    @KANJO_KAMOKU_CD_TO AND");
            sql.Append("    HOJO_KAMOKU_CD IS NOT NULL");
            sql.Append(" AND (A.SHISHO_CD = @SHISHO_CD or A.SHISHO_CD is null)");
            sql.Append(" ORDER BY ");
            sql.Append("    A.KAISHA_CD ASC,");
            sql.Append("    A.KANJO_KAMOKU_CD ASC,");
            sql.Append("    A.HOJO_KAMOKU_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD_FR", SqlDbType.Decimal, 6, kanjoKamokuCdFr);
            dpc.SetParam("@KANJO_KAMOKU_CD_TO", SqlDbType.Decimal, 6, kanjoKamokuCdTo);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    if (kanjoKamokuCd == -1 || kanjoKamokuCd != Util.ToDecimal(dtMainLoop.Rows[i]["KANJO_KAMOKU_CD"]) || j % 80 == 0)
                    {
                        #region 勘定科目詳細インサート
                        sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        sql.Append("INSERT INTO PR_ZM_TBL(");
                        sql.Append("  GUID");
                        sql.Append(" ,SORT");
                        sql.Append(" ,ITEM01");
                        sql.Append(" ,ITEM02");
                        sql.Append(" ,ITEM03");
                        sql.Append(") ");
                        sql.Append("VALUES(");
                        sql.Append("  @GUID");
                        sql.Append(" ,@SORT");
                        sql.Append(" ,@ITEM01");
                        sql.Append(" ,@ITEM02");
                        sql.Append(" ,@ITEM03");
                        sql.Append(") ");
                        #endregion

                        #region データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 6, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KANJO_KAMOKU_CD"]); // 勘定科目コード名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "【" + dtMainLoop.Rows[i]["KANJO_KAMOKU_NM"] + "】"); // 勘定科目名
                        this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                        #endregion
                        j++;
                    }
                    #region 補助科目詳細インサート
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("INSERT INTO PR_ZM_TBL(");
                    sql.Append("  GUID");
                    sql.Append(" ,SORT");
                    sql.Append(" ,ITEM01");
                    sql.Append(" ,ITEM02");
                    sql.Append(" ,ITEM03");
                    sql.Append(" ,ITEM04");
                    sql.Append(") ");
                    sql.Append("VALUES(");
                    sql.Append("  @GUID");
                    sql.Append(" ,@SORT");
                    sql.Append(" ,@ITEM01");
                    sql.Append(" ,@ITEM02");
                    sql.Append(" ,@ITEM03");
                    sql.Append(" ,@ITEM04");
                    sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 6, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["HOJO_KAMOKU_CD"]); // 補助科目コード名
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["HOJO_KAMOKU_NM"]); // 補助科目名
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["HOJO_KAMOKU_KANA_NM"]); // 補助科目カナ名
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion

                    kanjoKamokuCd = Util.ToDecimal(dtMainLoop.Rows[i]["KANJO_KAMOKU_CD"]);
                    j++;
                    i++;
                }
                decimal karaGyoInsert = j - Math.Floor(Util.ToDecimal(j / maxRows)) * maxRows;
                while (karaGyoInsert < maxRows)
                {
                    #region 空行インサート
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("INSERT INTO PR_ZM_TBL(");
                    sql.Append("  GUID");
                    sql.Append(" ,SORT");
                    sql.Append(" ,ITEM01");
                    sql.Append(") ");
                    sql.Append("VALUES(");
                    sql.Append("  @GUID");
                    sql.Append(" ,@SORT");
                    sql.Append(" ,@ITEM01");
                    sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 6, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion
                    karaGyoInsert++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}

