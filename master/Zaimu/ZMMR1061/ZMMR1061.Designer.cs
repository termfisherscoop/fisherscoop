﻿namespace jp.co.fsi.zm.zmmr1061
{
    partial class ZMMR1061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKikanCodeBet = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblKanjoKamokuTo = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCodeBet = new System.Windows.Forms.Label();
            this.txtKanjoKamokuFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuFr = new System.Windows.Forms.Label();
            this.txtKanjoKamokuTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.lblBumonTo = new System.Windows.Forms.Label();
            this.lblBumonBet = new System.Windows.Forms.Label();
            this.txtBumonFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonFr = new System.Windows.Forms.Label();
            this.txtBumonTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.rdoYes = new System.Windows.Forms.RadioButton();
            this.rdoNo = new System.Windows.Forms.RadioButton();
            this.rdoZenbu = new System.Windows.Forms.RadioButton();
            this.rdoKessan = new System.Windows.Forms.RadioButton();
            this.rdoTsujo = new System.Windows.Forms.RadioButton();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "";
            // 
            // lblKikanCodeBet
            // 
            this.lblKikanCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblKikanCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKikanCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblKikanCodeBet.Location = new System.Drawing.Point(526, 3);
            this.lblKikanCodeBet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblKikanCodeBet.Name = "lblKikanCodeBet";
            this.lblKikanCodeBet.Size = new System.Drawing.Size(20, 24);
            this.lblKikanCodeBet.TabIndex = 8;
            this.lblKikanCodeBet.Tag = "CHANGE";
            this.lblKikanCodeBet.Text = "～";
            this.lblKikanCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblDayTo.Location = new System.Drawing.Point(808, 5);
            this.lblDayTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(20, 24);
            this.lblDayTo.TabIndex = 16;
            this.lblDayTo.Tag = "CHANGE";
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblMonthTo.Location = new System.Drawing.Point(735, 5);
            this.lblMonthTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(20, 24);
            this.lblMonthTo.TabIndex = 14;
            this.lblMonthTo.Tag = "CHANGE";
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearTo
            // 
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblYearTo.Location = new System.Drawing.Point(664, 5);
            this.lblYearTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(20, 24);
            this.lblYearTo.TabIndex = 12;
            this.lblYearTo.Tag = "CHANGE";
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtDayTo.Location = new System.Drawing.Point(764, 3);
            this.txtDayTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(39, 23);
            this.txtDayTo.TabIndex = 15;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtYearTo.Location = new System.Drawing.Point(621, 3);
            this.txtYearTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(39, 23);
            this.txtYearTo.TabIndex = 11;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtMonthTo.Location = new System.Drawing.Point(692, 3);
            this.txtMonthTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(39, 23);
            this.txtMonthTo.TabIndex = 13;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblGengoTo.Location = new System.Drawing.Point(561, 2);
            this.lblGengoTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(55, 24);
            this.lblGengoTo.TabIndex = 10;
            this.lblGengoTo.Tag = "DISPNAME";
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblDayFr.Location = new System.Drawing.Point(449, 5);
            this.lblDayFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(20, 24);
            this.lblDayFr.TabIndex = 7;
            this.lblDayFr.Tag = "CHANGE";
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblMonthFr.Location = new System.Drawing.Point(375, 5);
            this.lblMonthFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(20, 24);
            this.lblMonthFr.TabIndex = 5;
            this.lblMonthFr.Tag = "CHANGE";
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearFr
            // 
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblYearFr.Location = new System.Drawing.Point(305, 5);
            this.lblYearFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(20, 24);
            this.lblYearFr.TabIndex = 3;
            this.lblYearFr.Tag = "CHANGE";
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDayFr.Location = new System.Drawing.Point(405, 3);
            this.txtDayFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(39, 23);
            this.txtDayFr.TabIndex = 6;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtYearFr.Location = new System.Drawing.Point(262, 3);
            this.txtYearFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(39, 23);
            this.txtYearFr.TabIndex = 2;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtMonthFr.Location = new System.Drawing.Point(333, 3);
            this.txtMonthFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(39, 23);
            this.txtMonthFr.TabIndex = 4;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblGengoFr.Location = new System.Drawing.Point(202, 2);
            this.lblGengoFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(55, 24);
            this.lblGengoFr.TabIndex = 1;
            this.lblGengoFr.Tag = "DISPNAME";
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKanjoKamokuTo
            // 
            this.lblKanjoKamokuTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoKamokuTo.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamokuTo.Location = new System.Drawing.Point(631, 5);
            this.lblKanjoKamokuTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblKanjoKamokuTo.Name = "lblKanjoKamokuTo";
            this.lblKanjoKamokuTo.Size = new System.Drawing.Size(227, 24);
            this.lblKanjoKamokuTo.TabIndex = 4;
            this.lblKanjoKamokuTo.Tag = "DISPNAME";
            this.lblKanjoKamokuTo.Text = "最　後";
            this.lblKanjoKamokuTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCodeBet
            // 
            this.lblKanjoKamokuCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoKamokuCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamokuCodeBet.Location = new System.Drawing.Point(526, 5);
            this.lblKanjoKamokuCodeBet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblKanjoKamokuCodeBet.Name = "lblKanjoKamokuCodeBet";
            this.lblKanjoKamokuCodeBet.Size = new System.Drawing.Size(20, 24);
            this.lblKanjoKamokuCodeBet.TabIndex = 2;
            this.lblKanjoKamokuCodeBet.Tag = "CHANGE";
            this.lblKanjoKamokuCodeBet.Text = "～";
            this.lblKanjoKamokuCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuFr
            // 
            this.txtKanjoKamokuFr.AutoSizeFromLength = false;
            this.txtKanjoKamokuFr.DisplayLength = null;
            this.txtKanjoKamokuFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKanjoKamokuFr.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuFr.Location = new System.Drawing.Point(202, 5);
            this.txtKanjoKamokuFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtKanjoKamokuFr.MaxLength = 6;
            this.txtKanjoKamokuFr.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtKanjoKamokuFr.Name = "txtKanjoKamokuFr";
            this.txtKanjoKamokuFr.Size = new System.Drawing.Size(65, 23);
            this.txtKanjoKamokuFr.TabIndex = 0;
            this.txtKanjoKamokuFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKnjoKamokuFr_Validating);
            // 
            // lblKanjoKamokuFr
            // 
            this.lblKanjoKamokuFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoKamokuFr.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamokuFr.Location = new System.Drawing.Point(270, 4);
            this.lblKanjoKamokuFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblKanjoKamokuFr.Name = "lblKanjoKamokuFr";
            this.lblKanjoKamokuFr.Size = new System.Drawing.Size(227, 24);
            this.lblKanjoKamokuFr.TabIndex = 1;
            this.lblKanjoKamokuFr.Tag = "DISPNAME";
            this.lblKanjoKamokuFr.Text = "先　頭";
            this.lblKanjoKamokuFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuTo
            // 
            this.txtKanjoKamokuTo.AutoSizeFromLength = false;
            this.txtKanjoKamokuTo.DisplayLength = null;
            this.txtKanjoKamokuTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKanjoKamokuTo.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuTo.Location = new System.Drawing.Point(561, 6);
            this.txtKanjoKamokuTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtKanjoKamokuTo.MaxLength = 6;
            this.txtKanjoKamokuTo.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtKanjoKamokuTo.Name = "txtKanjoKamokuTo";
            this.txtKanjoKamokuTo.Size = new System.Drawing.Size(65, 23);
            this.txtKanjoKamokuTo.TabIndex = 3;
            this.txtKanjoKamokuTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKnjoKamokuTo_Validating);
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.BackColor = System.Drawing.Color.Silver;
            this.rdoZeinuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoZeinuki.ForeColor = System.Drawing.Color.Black;
            this.rdoZeinuki.Location = new System.Drawing.Point(314, 2);
            this.rdoZeinuki.Margin = new System.Windows.Forms.Padding(5);
            this.rdoZeinuki.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(58, 24);
            this.rdoZeinuki.TabIndex = 1;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Tag = "CHANGE";
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = false;
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.BackColor = System.Drawing.Color.Silver;
            this.rdoZeikomi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoZeikomi.ForeColor = System.Drawing.Color.Black;
            this.rdoZeikomi.Location = new System.Drawing.Point(202, 2);
            this.rdoZeikomi.Margin = new System.Windows.Forms.Padding(5);
            this.rdoZeikomi.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(58, 24);
            this.rdoZeikomi.TabIndex = 0;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Tag = "CHANGE";
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = false;
            // 
            // lblBumonTo
            // 
            this.lblBumonTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblBumonTo.ForeColor = System.Drawing.Color.Black;
            this.lblBumonTo.Location = new System.Drawing.Point(631, 5);
            this.lblBumonTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblBumonTo.Name = "lblBumonTo";
            this.lblBumonTo.Size = new System.Drawing.Size(227, 24);
            this.lblBumonTo.TabIndex = 4;
            this.lblBumonTo.Tag = "DISPNAME";
            this.lblBumonTo.Text = "最　後";
            this.lblBumonTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonBet
            // 
            this.lblBumonBet.BackColor = System.Drawing.Color.Silver;
            this.lblBumonBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblBumonBet.ForeColor = System.Drawing.Color.Black;
            this.lblBumonBet.Location = new System.Drawing.Point(526, 5);
            this.lblBumonBet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblBumonBet.Name = "lblBumonBet";
            this.lblBumonBet.Size = new System.Drawing.Size(20, 24);
            this.lblBumonBet.TabIndex = 2;
            this.lblBumonBet.Tag = "CHANGE";
            this.lblBumonBet.Text = "～";
            this.lblBumonBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonFr
            // 
            this.txtBumonFr.AutoSizeFromLength = false;
            this.txtBumonFr.DisplayLength = null;
            this.txtBumonFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtBumonFr.ForeColor = System.Drawing.Color.Black;
            this.txtBumonFr.Location = new System.Drawing.Point(202, 5);
            this.txtBumonFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtBumonFr.MaxLength = 4;
            this.txtBumonFr.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtBumonFr.Name = "txtBumonFr";
            this.txtBumonFr.Size = new System.Drawing.Size(65, 23);
            this.txtBumonFr.TabIndex = 0;
            this.txtBumonFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonFr_Validating);
            // 
            // lblBumonFr
            // 
            this.lblBumonFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblBumonFr.ForeColor = System.Drawing.Color.Black;
            this.lblBumonFr.Location = new System.Drawing.Point(270, 4);
            this.lblBumonFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblBumonFr.Name = "lblBumonFr";
            this.lblBumonFr.Size = new System.Drawing.Size(227, 24);
            this.lblBumonFr.TabIndex = 1;
            this.lblBumonFr.Tag = "DISPNAME";
            this.lblBumonFr.Text = "先　頭";
            this.lblBumonFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonTo
            // 
            this.txtBumonTo.AutoSizeFromLength = false;
            this.txtBumonTo.DisplayLength = null;
            this.txtBumonTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtBumonTo.ForeColor = System.Drawing.Color.Black;
            this.txtBumonTo.Location = new System.Drawing.Point(561, 6);
            this.txtBumonTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtBumonTo.MaxLength = 4;
            this.txtBumonTo.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtBumonTo.Name = "txtBumonTo";
            this.txtBumonTo.Size = new System.Drawing.Size(65, 23);
            this.txtBumonTo.TabIndex = 3;
            this.txtBumonTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonTo_Validating);
            // 
            // rdoYes
            // 
            this.rdoYes.AutoSize = true;
            this.rdoYes.BackColor = System.Drawing.Color.Silver;
            this.rdoYes.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoYes.ForeColor = System.Drawing.Color.Black;
            this.rdoYes.Location = new System.Drawing.Point(314, 8);
            this.rdoYes.Margin = new System.Windows.Forms.Padding(5);
            this.rdoYes.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoYes.Name = "rdoYes";
            this.rdoYes.Size = new System.Drawing.Size(58, 24);
            this.rdoYes.TabIndex = 1;
            this.rdoYes.TabStop = true;
            this.rdoYes.Tag = "CHANGE";
            this.rdoYes.Text = "する";
            this.rdoYes.UseVisualStyleBackColor = false;
            this.rdoYes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdo_KeyPress);
            // 
            // rdoNo
            // 
            this.rdoNo.AutoSize = true;
            this.rdoNo.BackColor = System.Drawing.Color.Silver;
            this.rdoNo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoNo.ForeColor = System.Drawing.Color.Black;
            this.rdoNo.Location = new System.Drawing.Point(202, 8);
            this.rdoNo.Margin = new System.Windows.Forms.Padding(5);
            this.rdoNo.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoNo.Name = "rdoNo";
            this.rdoNo.Size = new System.Drawing.Size(74, 24);
            this.rdoNo.TabIndex = 0;
            this.rdoNo.TabStop = true;
            this.rdoNo.Tag = "CHANGE";
            this.rdoNo.Text = "しない";
            this.rdoNo.UseVisualStyleBackColor = false;
            this.rdoNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdo_KeyPress);
            // 
            // rdoZenbu
            // 
            this.rdoZenbu.AutoSize = true;
            this.rdoZenbu.BackColor = System.Drawing.Color.Silver;
            this.rdoZenbu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoZenbu.ForeColor = System.Drawing.Color.Black;
            this.rdoZenbu.Location = new System.Drawing.Point(415, 2);
            this.rdoZenbu.Margin = new System.Windows.Forms.Padding(5);
            this.rdoZenbu.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZenbu.Name = "rdoZenbu";
            this.rdoZenbu.Size = new System.Drawing.Size(74, 24);
            this.rdoZenbu.TabIndex = 2;
            this.rdoZenbu.TabStop = true;
            this.rdoZenbu.Tag = "CHANGE";
            this.rdoZenbu.Text = "全仕訳";
            this.rdoZenbu.UseVisualStyleBackColor = false;
            // 
            // rdoKessan
            // 
            this.rdoKessan.AutoSize = true;
            this.rdoKessan.BackColor = System.Drawing.Color.Silver;
            this.rdoKessan.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoKessan.ForeColor = System.Drawing.Color.Black;
            this.rdoKessan.Location = new System.Drawing.Point(314, 2);
            this.rdoKessan.Margin = new System.Windows.Forms.Padding(5);
            this.rdoKessan.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoKessan.Name = "rdoKessan";
            this.rdoKessan.Size = new System.Drawing.Size(90, 24);
            this.rdoKessan.TabIndex = 1;
            this.rdoKessan.TabStop = true;
            this.rdoKessan.Tag = "CHANGE";
            this.rdoKessan.Text = "決算仕訳";
            this.rdoKessan.UseVisualStyleBackColor = false;
            // 
            // rdoTsujo
            // 
            this.rdoTsujo.AutoSize = true;
            this.rdoTsujo.BackColor = System.Drawing.Color.Silver;
            this.rdoTsujo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoTsujo.ForeColor = System.Drawing.Color.Black;
            this.rdoTsujo.Location = new System.Drawing.Point(202, 2);
            this.rdoTsujo.Margin = new System.Windows.Forms.Padding(5);
            this.rdoTsujo.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoTsujo.Name = "rdoTsujo";
            this.rdoTsujo.Size = new System.Drawing.Size(90, 24);
            this.rdoTsujo.TabIndex = 0;
            this.rdoTsujo.TabStop = true;
            this.rdoTsujo.Tag = "CHANGE";
            this.rdoTsujo.Text = "通常仕訳";
            this.rdoTsujo.UseVisualStyleBackColor = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(202, 5);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(250, 4);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(360, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
            this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(890, 34);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(890, 30);
            this.label1.TabIndex = 0;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "仕訳種類";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(890, 30);
            this.label2.TabIndex = 0;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "消費税処理";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(890, 32);
            this.label3.TabIndex = 0;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "期間";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(890, 34);
            this.label4.TabIndex = 0;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "勘定科目";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(890, 34);
            this.label5.TabIndex = 0;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "部門範囲";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(890, 40);
            this.label6.TabIndex = 0;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "金額がｾﾞﾛの科目を印字";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 7);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 5);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 8;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.05567F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.94433F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.94433F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.94433F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.94433F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.05567F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.05567F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.05567F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(900, 335);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.label10);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(5, 165);
            this.fsiPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(890, 30);
            this.fsiPanel8.TabIndex = 903;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Silver;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(890, 30);
            this.label10.TabIndex = 0;
            this.label10.Tag = "CHANGE";
            this.label10.Text = "範囲";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblKikanCodeBet);
            this.fsiPanel4.Controls.Add(this.lblGengoFr);
            this.fsiPanel4.Controls.Add(this.txtMonthFr);
            this.fsiPanel4.Controls.Add(this.lblDayTo);
            this.fsiPanel4.Controls.Add(this.txtYearFr);
            this.fsiPanel4.Controls.Add(this.txtDayFr);
            this.fsiPanel4.Controls.Add(this.lblMonthTo);
            this.fsiPanel4.Controls.Add(this.lblYearFr);
            this.fsiPanel4.Controls.Add(this.lblMonthFr);
            this.fsiPanel4.Controls.Add(this.lblYearTo);
            this.fsiPanel4.Controls.Add(this.lblDayFr);
            this.fsiPanel4.Controls.Add(this.lblGengoTo);
            this.fsiPanel4.Controls.Add(this.txtDayTo);
            this.fsiPanel4.Controls.Add(this.txtMonthTo);
            this.fsiPanel4.Controls.Add(this.txtYearTo);
            this.fsiPanel4.Controls.Add(this.label3);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 126);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(890, 30);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.rdoZeinuki);
            this.fsiPanel3.Controls.Add(this.rdoZeikomi);
            this.fsiPanel3.Controls.Add(this.label2);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 87);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(890, 30);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.rdoZenbu);
            this.fsiPanel2.Controls.Add(this.rdoTsujo);
            this.fsiPanel2.Controls.Add(this.rdoKessan);
            this.fsiPanel2.Controls.Add(this.label1);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 48);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(890, 30);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(890, 34);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.rdoYes);
            this.fsiPanel7.Controls.Add(this.rdoNo);
            this.fsiPanel7.Controls.Add(this.label6);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(5, 290);
            this.fsiPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(890, 40);
            this.fsiPanel7.TabIndex = 6;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtBumonFr);
            this.fsiPanel6.Controls.Add(this.lblBumonTo);
            this.fsiPanel6.Controls.Add(this.txtBumonTo);
            this.fsiPanel6.Controls.Add(this.lblBumonFr);
            this.fsiPanel6.Controls.Add(this.lblBumonBet);
            this.fsiPanel6.Controls.Add(this.label5);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(5, 247);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(890, 34);
            this.fsiPanel6.TabIndex = 5;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtKanjoKamokuFr);
            this.fsiPanel5.Controls.Add(this.txtKanjoKamokuTo);
            this.fsiPanel5.Controls.Add(this.lblKanjoKamokuFr);
            this.fsiPanel5.Controls.Add(this.lblKanjoKamokuTo);
            this.fsiPanel5.Controls.Add(this.lblKanjoKamokuCodeBet);
            this.fsiPanel5.Controls.Add(this.label4);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(5, 204);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(890, 34);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // ZMMR1061
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMMR1061";
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private System.Windows.Forms.Label lblKanjoKamokuTo;
        private System.Windows.Forms.Label lblKanjoKamokuCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuFr;
        private System.Windows.Forms.Label lblKanjoKamokuFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuTo;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.Label lblKikanCodeBet;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblBumonTo;
        private System.Windows.Forms.Label lblBumonBet;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonFr;
        private System.Windows.Forms.Label lblBumonFr;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonTo;
        private System.Windows.Forms.RadioButton rdoYes;
        private System.Windows.Forms.RadioButton rdoNo;
        private System.Windows.Forms.RadioButton rdoKessan;
        private System.Windows.Forms.RadioButton rdoTsujo;
        private System.Windows.Forms.RadioButton rdoZenbu;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel8;
        private System.Windows.Forms.Label label10;
    }
}