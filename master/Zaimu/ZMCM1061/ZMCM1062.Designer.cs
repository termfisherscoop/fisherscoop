﻿namespace jp.co.fsi.zm.zmcm1061
{
    partial class ZMCM1062
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtZeiKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKubunCd = new System.Windows.Forms.Label();
            this.txtGridEdit = new jp.co.fsi.common.controls.FsiTextBox();
            this.dgvInputList = new System.Windows.Forms.DataGridView();
            this.gyo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tekiyouKaishiBi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zeiRitsu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtZeiRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblPercent = new System.Windows.Forms.Label();
            this.lblZeiRitsu = new System.Windows.Forms.Label();
            this.lblTaishakuKubunSelect = new System.Windows.Forms.Label();
            this.txtTaishakuKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTaishakuKubunCd = new System.Windows.Forms.Label();
            this.lblTorihikiKubunNm = new System.Windows.Forms.Label();
            this.txtTorihikiKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTorihikiKubunCd = new System.Windows.Forms.Label();
            this.lblKazeiKubunCdSelect = new System.Windows.Forms.Label();
            this.txtKazeiKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKazeiKubunCd = new System.Windows.Forms.Label();
            this.txtZeiKubunNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKubunNm = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4";
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n保存";
            // 
            // btnF8
            // 
            this.btnF8.Text = "F8\r\n\r\n行削除";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 257);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(885, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(895, 31);
            this.lblTitle.Text = "税区分の登録";
            // 
            // txtZeiKubunCd
            // 
            this.txtZeiKubunCd.AutoSizeFromLength = true;
            this.txtZeiKubunCd.DisplayLength = null;
            this.txtZeiKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtZeiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtZeiKubunCd.Location = new System.Drawing.Point(103, 2);
            this.txtZeiKubunCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtZeiKubunCd.MaxLength = 2;
            this.txtZeiKubunCd.Name = "txtZeiKubunCd";
            this.txtZeiKubunCd.Size = new System.Drawing.Size(25, 23);
            this.txtZeiKubunCd.TabIndex = 0;
            this.txtZeiKubunCd.Text = "0";
            this.txtZeiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblZeiKubunCd
            // 
            this.lblZeiKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKubunCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZeiKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZeiKubunCd.Location = new System.Drawing.Point(0, 0);
            this.lblZeiKubunCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZeiKubunCd.Name = "lblZeiKubunCd";
            this.lblZeiKubunCd.Size = new System.Drawing.Size(598, 27);
            this.lblZeiKubunCd.TabIndex = 1;
            this.lblZeiKubunCd.Tag = "CHANGE";
            this.lblZeiKubunCd.Text = "税区分";
            this.lblZeiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGridEdit
            // 
            this.txtGridEdit.AutoSizeFromLength = true;
            this.txtGridEdit.BackColor = System.Drawing.Color.White;
            this.txtGridEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGridEdit.DisplayLength = null;
            this.txtGridEdit.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGridEdit.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGridEdit.Location = new System.Drawing.Point(761, 175);
            this.txtGridEdit.Margin = new System.Windows.Forms.Padding(4);
            this.txtGridEdit.MaxLength = 10;
            this.txtGridEdit.Name = "txtGridEdit";
            this.txtGridEdit.Size = new System.Drawing.Size(101, 23);
            this.txtGridEdit.TabIndex = 902;
            this.txtGridEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGridEdit.Visible = false;
            this.txtGridEdit.Validating += new System.ComponentModel.CancelEventHandler(this.txtGridEdit_Validating);
            // 
            // dgvInputList
            // 
            this.dgvInputList.AllowUserToAddRows = false;
            this.dgvInputList.AllowUserToDeleteRows = false;
            this.dgvInputList.AllowUserToResizeColumns = false;
            this.dgvInputList.AllowUserToResizeRows = false;
            this.dgvInputList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInputList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gyo,
            this.tekiyouKaishiBi,
            this.zeiRitsu});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvInputList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvInputList.EnableHeadersVisualStyles = false;
            this.dgvInputList.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.dgvInputList.Location = new System.Drawing.Point(617, 124);
            this.dgvInputList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvInputList.MultiSelect = false;
            this.dgvInputList.Name = "dgvInputList";
            this.dgvInputList.RowHeadersVisible = false;
            this.dgvInputList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInputList.RowTemplate.Height = 21;
            this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInputList.Size = new System.Drawing.Size(272, 119);
            this.dgvInputList.TabIndex = 14;
            this.dgvInputList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellEnter);
            this.dgvInputList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvInputList_Scroll);
            this.dgvInputList.Enter += new System.EventHandler(this.dgvInputList_Enter);
            this.dgvInputList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgvInputList_MouseUp);
            // 
            // gyo
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gyo.DefaultCellStyle = dataGridViewCellStyle7;
            this.gyo.HeaderText = "行";
            this.gyo.Name = "gyo";
            this.gyo.ReadOnly = true;
            this.gyo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gyo.Width = 30;
            // 
            // tekiyouKaishiBi
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.tekiyouKaishiBi.DefaultCellStyle = dataGridViewCellStyle8;
            this.tekiyouKaishiBi.HeaderText = "適用開始日";
            this.tekiyouKaishiBi.Name = "tekiyouKaishiBi";
            this.tekiyouKaishiBi.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.tekiyouKaishiBi.Width = 120;
            // 
            // zeiRitsu
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.zeiRitsu.DefaultCellStyle = dataGridViewCellStyle9;
            this.zeiRitsu.HeaderText = "税率";
            this.zeiRitsu.Name = "zeiRitsu";
            this.zeiRitsu.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.zeiRitsu.Width = 70;
            // 
            // txtZeiRitsu
            // 
            this.txtZeiRitsu.AutoSizeFromLength = false;
            this.txtZeiRitsu.BackColor = System.Drawing.SystemColors.Window;
            this.txtZeiRitsu.DisplayLength = null;
            this.txtZeiRitsu.Enabled = false;
            this.txtZeiRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtZeiRitsu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtZeiRitsu.Location = new System.Drawing.Point(103, 2);
            this.txtZeiRitsu.Margin = new System.Windows.Forms.Padding(4);
            this.txtZeiRitsu.MaxLength = 4;
            this.txtZeiRitsu.Name = "txtZeiRitsu";
            this.txtZeiRitsu.ReadOnly = true;
            this.txtZeiRitsu.Size = new System.Drawing.Size(57, 23);
            this.txtZeiRitsu.TabIndex = 12;
            this.txtZeiRitsu.Text = "0.0";
            this.txtZeiRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiRitsu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtZeiRitsu_KeyDown);
            this.txtZeiRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiRitsu_Validating);
            // 
            // lblPercent
            // 
            this.lblPercent.BackColor = System.Drawing.Color.Silver;
            this.lblPercent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPercent.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPercent.Location = new System.Drawing.Point(164, 1);
            this.lblPercent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPercent.Name = "lblPercent";
            this.lblPercent.Size = new System.Drawing.Size(20, 24);
            this.lblPercent.TabIndex = 13;
            this.lblPercent.Tag = "CHANGE";
            this.lblPercent.Text = "％";
            this.lblPercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblZeiRitsu
            // 
            this.lblZeiRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblZeiRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiRitsu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZeiRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZeiRitsu.Location = new System.Drawing.Point(0, 0);
            this.lblZeiRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZeiRitsu.Name = "lblZeiRitsu";
            this.lblZeiRitsu.Size = new System.Drawing.Size(598, 31);
            this.lblZeiRitsu.TabIndex = 11;
            this.lblZeiRitsu.Tag = "CHANGE";
            this.lblZeiRitsu.Text = "税率";
            this.lblZeiRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaishakuKubunSelect
            // 
            this.lblTaishakuKubunSelect.BackColor = System.Drawing.Color.LightCyan;
            this.lblTaishakuKubunSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishakuKubunSelect.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaishakuKubunSelect.ForeColor = System.Drawing.Color.Black;
            this.lblTaishakuKubunSelect.Location = new System.Drawing.Point(130, 0);
            this.lblTaishakuKubunSelect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTaishakuKubunSelect.Name = "lblTaishakuKubunSelect";
            this.lblTaishakuKubunSelect.Size = new System.Drawing.Size(284, 24);
            this.lblTaishakuKubunSelect.TabIndex = 10;
            this.lblTaishakuKubunSelect.Tag = "DISPNAME";
            this.lblTaishakuKubunSelect.Text = "1：借方　2：貸方";
            this.lblTaishakuKubunSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTaishakuKubunCd
            // 
            this.txtTaishakuKubunCd.AutoSizeFromLength = false;
            this.txtTaishakuKubunCd.DisplayLength = null;
            this.txtTaishakuKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaishakuKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTaishakuKubunCd.Location = new System.Drawing.Point(103, 1);
            this.txtTaishakuKubunCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTaishakuKubunCd.MaxLength = 1;
            this.txtTaishakuKubunCd.Name = "txtTaishakuKubunCd";
            this.txtTaishakuKubunCd.Size = new System.Drawing.Size(25, 23);
            this.txtTaishakuKubunCd.TabIndex = 9;
            this.txtTaishakuKubunCd.Text = "0";
            this.txtTaishakuKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaishakuKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaishakuKubunCd_Validating);
            // 
            // lblTaishakuKubunCd
            // 
            this.lblTaishakuKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakuKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishakuKubunCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTaishakuKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaishakuKubunCd.Location = new System.Drawing.Point(0, 0);
            this.lblTaishakuKubunCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTaishakuKubunCd.Name = "lblTaishakuKubunCd";
            this.lblTaishakuKubunCd.Size = new System.Drawing.Size(598, 27);
            this.lblTaishakuKubunCd.TabIndex = 8;
            this.lblTaishakuKubunCd.Tag = "CHANGE";
            this.lblTaishakuKubunCd.Text = "貸借区分";
            this.lblTaishakuKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTorihikiKubunNm
            // 
            this.lblTorihikiKubunNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblTorihikiKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKubunNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTorihikiKubunNm.ForeColor = System.Drawing.Color.Black;
            this.lblTorihikiKubunNm.Location = new System.Drawing.Point(130, 0);
            this.lblTorihikiKubunNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTorihikiKubunNm.Name = "lblTorihikiKubunNm";
            this.lblTorihikiKubunNm.Size = new System.Drawing.Size(284, 24);
            this.lblTorihikiKubunNm.TabIndex = 7;
            this.lblTorihikiKubunNm.Tag = "DISPNAME";
            this.lblTorihikiKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTorihikiKubunCd
            // 
            this.txtTorihikiKubunCd.AutoSizeFromLength = false;
            this.txtTorihikiKubunCd.DisplayLength = null;
            this.txtTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTorihikiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTorihikiKubunCd.Location = new System.Drawing.Point(103, 1);
            this.txtTorihikiKubunCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTorihikiKubunCd.MaxLength = 2;
            this.txtTorihikiKubunCd.Name = "txtTorihikiKubunCd";
            this.txtTorihikiKubunCd.Size = new System.Drawing.Size(25, 23);
            this.txtTorihikiKubunCd.TabIndex = 6;
            this.txtTorihikiKubunCd.Text = "0";
            this.txtTorihikiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikiKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikiKubunCd_Validating);
            // 
            // lblTorihikiKubunCd
            // 
            this.lblTorihikiKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikiKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKubunCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTorihikiKubunCd.Location = new System.Drawing.Point(0, 0);
            this.lblTorihikiKubunCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTorihikiKubunCd.Name = "lblTorihikiKubunCd";
            this.lblTorihikiKubunCd.Size = new System.Drawing.Size(598, 27);
            this.lblTorihikiKubunCd.TabIndex = 5;
            this.lblTorihikiKubunCd.Tag = "CHANGE";
            this.lblTorihikiKubunCd.Text = "取引区分";
            this.lblTorihikiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKazeiKubunCdSelect
            // 
            this.lblKazeiKubunCdSelect.BackColor = System.Drawing.Color.LightCyan;
            this.lblKazeiKubunCdSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKazeiKubunCdSelect.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKazeiKubunCdSelect.ForeColor = System.Drawing.Color.Black;
            this.lblKazeiKubunCdSelect.Location = new System.Drawing.Point(130, 0);
            this.lblKazeiKubunCdSelect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKazeiKubunCdSelect.Name = "lblKazeiKubunCdSelect";
            this.lblKazeiKubunCdSelect.Size = new System.Drawing.Size(284, 24);
            this.lblKazeiKubunCdSelect.TabIndex = 4;
            this.lblKazeiKubunCdSelect.Tag = "DISPNAME";
            this.lblKazeiKubunCdSelect.Text = "0：非課税 1：課税 ２：不課税";
            this.lblKazeiKubunCdSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKazeiKubunCd
            // 
            this.txtKazeiKubunCd.AutoSizeFromLength = false;
            this.txtKazeiKubunCd.DisplayLength = null;
            this.txtKazeiKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKazeiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKazeiKubunCd.Location = new System.Drawing.Point(103, 1);
            this.txtKazeiKubunCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtKazeiKubunCd.MaxLength = 1;
            this.txtKazeiKubunCd.Name = "txtKazeiKubunCd";
            this.txtKazeiKubunCd.Size = new System.Drawing.Size(25, 23);
            this.txtKazeiKubunCd.TabIndex = 3;
            this.txtKazeiKubunCd.Text = "0";
            this.txtKazeiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKazeiKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKazeiKubunCd_Validating);
            // 
            // lblKazeiKubunCd
            // 
            this.lblKazeiKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblKazeiKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKazeiKubunCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKazeiKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKazeiKubunCd.Location = new System.Drawing.Point(0, 0);
            this.lblKazeiKubunCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKazeiKubunCd.Name = "lblKazeiKubunCd";
            this.lblKazeiKubunCd.Size = new System.Drawing.Size(598, 27);
            this.lblKazeiKubunCd.TabIndex = 2;
            this.lblKazeiKubunCd.Tag = "CHANGE";
            this.lblKazeiKubunCd.Text = "課税区分";
            this.lblKazeiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZeiKubunNm
            // 
            this.txtZeiKubunNm.AutoSizeFromLength = false;
            this.txtZeiKubunNm.DisplayLength = null;
            this.txtZeiKubunNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtZeiKubunNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtZeiKubunNm.Location = new System.Drawing.Point(103, 1);
            this.txtZeiKubunNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtZeiKubunNm.MaxLength = 30;
            this.txtZeiKubunNm.Name = "txtZeiKubunNm";
            this.txtZeiKubunNm.Size = new System.Drawing.Size(482, 23);
            this.txtZeiKubunNm.TabIndex = 1;
            // 
            // lblZeiKubunNm
            // 
            this.lblZeiKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKubunNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZeiKubunNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZeiKubunNm.Location = new System.Drawing.Point(0, 0);
            this.lblZeiKubunNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZeiKubunNm.Name = "lblZeiKubunNm";
            this.lblZeiKubunNm.Size = new System.Drawing.Size(598, 27);
            this.lblZeiKubunNm.TabIndex = 0;
            this.lblZeiKubunNm.Tag = "CHANGE";
            this.lblZeiKubunNm.Text = "税区分名称";
            this.lblZeiKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 6;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(606, 209);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtZeiKubunCd);
            this.fsiPanel1.Controls.Add(this.lblZeiKubunCd);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(598, 27);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtZeiKubunNm);
            this.fsiPanel2.Controls.Add(this.lblZeiKubunNm);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 38);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(598, 27);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtKazeiKubunCd);
            this.fsiPanel3.Controls.Add(this.lblKazeiKubunCdSelect);
            this.fsiPanel3.Controls.Add(this.lblKazeiKubunCd);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 72);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(598, 27);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtTorihikiKubunCd);
            this.fsiPanel4.Controls.Add(this.lblTorihikiKubunNm);
            this.fsiPanel4.Controls.Add(this.lblTorihikiKubunCd);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 106);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(598, 27);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtTaishakuKubunCd);
            this.fsiPanel5.Controls.Add(this.lblTaishakuKubunSelect);
            this.fsiPanel5.Controls.Add(this.lblTaishakuKubunCd);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 140);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(598, 27);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtZeiRitsu);
            this.fsiPanel6.Controls.Add(this.lblPercent);
            this.fsiPanel6.Controls.Add(this.lblZeiRitsu);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 174);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(598, 31);
            this.fsiPanel6.TabIndex = 5;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // ZMCM1062
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 394);
            this.Controls.Add(this.txtGridEdit);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.dgvInputList);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1062";
            this.ShowFButton = true;
            this.Text = "税区分の登録";
            this.Controls.SetChildIndex(this.dgvInputList, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.txtGridEdit, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunCd;
        private System.Windows.Forms.Label lblZeiKubunCd;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunNm;
        private System.Windows.Forms.Label lblZeiKubunNm;
        private System.Windows.Forms.Label lblKazeiKubunCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKazeiKubunCd;
        private System.Windows.Forms.Label lblKazeiKubunCdSelect;
        private System.Windows.Forms.Label lblTorihikiKubunCd;
        private System.Windows.Forms.Label lblTorihikiKubunNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTorihikiKubunCd;
        private System.Windows.Forms.Label lblTaishakuKubunSelect;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuKubunCd;
        private System.Windows.Forms.Label lblTaishakuKubunCd;
        private System.Windows.Forms.Label lblZeiRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiRitsu;
        private System.Windows.Forms.Label lblPercent;
        private System.Windows.Forms.DataGridView dgvInputList;
        private jp.co.fsi.common.controls.FsiTextBox txtGridEdit;
        private System.Windows.Forms.DataGridViewTextBoxColumn gyo;
        private System.Windows.Forms.DataGridViewTextBoxColumn tekiyouKaishiBi;
        private System.Windows.Forms.DataGridViewTextBoxColumn zeiRitsu;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}