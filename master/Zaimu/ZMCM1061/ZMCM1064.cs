﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.zm.zmcm1061
{
    /// <summary>
    /// 税区分の検索(ZMCM1064)
    /// </summary>
    public partial class ZMCM1064 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1064()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.btnEsc.Location = this.btnF1.Location;
            //this.btnF1.Location = this.btnF2.Location;

            // データ取得してグリッドに設定
            this.SearchData();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを取得する
        /// </summary>
        private void SearchData()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            // 税区分ビューからデータを取得して表示
            Sql.Append("SELECT");
            Sql.Append(" A.ZEI_KUBUN,");
            Sql.Append(" A.ZEI_KUBUN_NM ");
            Sql.Append("FROM");
            Sql.Append(" VI_ZM_ZEI_KUBUN AS A ");
            Sql.Append("ORDER BY");
            Sql.Append(" A.ZEI_KUBUN");
            DataTable dtMailLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 該当データがなければエラーメッセージを表示
            if (dtMailLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
            }

            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 列の定義を作成
            dtResult.Columns.Add("ｺｰﾄﾞ", typeof(int));
            dtResult.Columns.Add("名　　　称", typeof(string));

            for (int i = 0; i < dtMailLoop.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();
                drResult["ｺｰﾄﾞ"] = dtMailLoop.Rows[i]["ZEI_KUBUN"];
                drResult["名　　　称"] = dtMailLoop.Rows[i]["ZEI_KUBUN_NM"];
                dtResult.Rows.Add(drResult);
            }

            this.dgvList.DataSource = dtResult;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            //this.dgvList.Columns[0].Width = 30;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //this.dgvList.Columns[1].Width = 320;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[2] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["ｺｰﾄﾞ"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["名　　　称"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
