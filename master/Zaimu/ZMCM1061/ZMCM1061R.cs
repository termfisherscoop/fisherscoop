﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1061
{
    /// <summary>
    /// ZAMC9081R の概要の説明です。
    /// </summary>
    public partial class ZMCM1061R : BaseReport
    {

        public ZMCM1061R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            this.txtToday_tate.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }
    }
}
