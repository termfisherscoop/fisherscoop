﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.zm.zmcm1061
{
    /// <summary>
    /// 税区分の登録(ZMCM1061)
    /// </summary>
    public partial class ZMCM1061 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1061()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // タイトルは非表示
                this.Text = "税区分検索";
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(829, 768);
                // フォームの配置を上へ移動する
                //this.lblZeiKubunNm.Visible = false;
                //this.txtZeiKubunNm.Visible = false;
                //this.dgvList.Location = new System.Drawing.Point(12, 13);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                //this.btnF1.Location = this.btnF2.Location;
                this.btnEnter.Location = this.btnF2.Location;
                this.btnEnter.Visible = true;
                this.btnF1.Visible = false;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // カナにフォーカス
            this.txtZeiKubunNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtZeiKubunNm.Focus();

            // グリッドビューを表示
            SearchData();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }

            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                return;
            }
            this.dgvList.Columns.Clear();
            this.SetViewData();
        
            // カナ名にフォーカスを戻す
            this.txtZeiKubunNm.Focus();
            this.txtZeiKubunNm.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                return;
            }
            //ZMCM1062 frm1062 = new ZMCM1062("1", "0");
            //frm1062.ShowDialog();
            //frm1062.Dispose();

            //// グリッドビューを表示
            //SearchData();

            EditZeiKbn("");
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF5();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF5()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                return;
            }
            ZMCM1063 frm1063 = new ZMCM1063();
            frm1063.ShowDialog();
            frm1063.Dispose();
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                return;
            }
                
            // 設定画面の起動
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "ZMCM1061R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 税区分名称クリック時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiKubunNm_Click(object sender, EventArgs e)
        {
            this.dgvList.Columns.Clear();
            this.SetViewData();
        }

        /// <summary>
        /// 税区分名称検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiKubunNm_Validating(object sender, CancelEventArgs e)
        {
            this.dgvList.Columns.Clear();
            SearchData();
        }
        
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    //ZMCM1062 frm1062 = new ZMCM1062("2", dgvList.CurrentRow.Cells[0].Value.ToString());
                    //frm1062.ShowDialog();
                    //frm1062.Dispose();

                    //// グリッドビューを表示
                    //SearchData();

                    EditZeiKbn(dgvList.CurrentRow.Cells[0].Value.ToString());
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                //ZMCM1062 frm1062 = new ZMCM1062("2", dgvList.CurrentRow.Cells[0].Value.ToString());
                //frm1062.ShowDialog();
                //frm1062.Dispose();

                //// グリッドビューを表示
                //SearchData();

                EditZeiKbn(dgvList.CurrentRow.Cells[0].Value.ToString());
            }
        }

        /// <summary>
        /// フォーム表示後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Shown(object sender, EventArgs e)
        {
            if (this.dgvList.RowCount == 0)
            {
                if (Msg.ConfYesNo("該当データがありません、登録しますか？") == DialogResult.Yes)
                {
                    this.PressF4();
                }
            }
            else
            {
                ActiveControl = this.dgvList;
                this.dgvList.Rows[0].Selected = true;
                this.dgvList.CurrentCell = this.dgvList[0, 0];
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 空データをセットする
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SetViewData()
        {
            DataTable dtResult = new DataTable();
            DataRow drResult = dtResult.NewRow();

            // 列の定義を作成
            dtResult.Columns.Add("区分", typeof(string));
            dtResult.Columns.Add("税 区 分 名 称", typeof(string));
            dtResult.Columns.Add("課税区分名称", typeof(string));
            dtResult.Columns.Add("貸借区分", typeof(string));
            dtResult.Columns.Add("税率", typeof(string));

            drResult["区分"] = "";
            drResult["税 区 分 名 称"] = "";
            drResult["課税区分名称"] = "";
            drResult["貸借区分"] = "";
            drResult["税率"] = "";

            dtResult.Rows.Add(drResult);

            this.dgvList.DataSource = dtResult;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);1
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            this.dgvList.Columns[0].Width = 60;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 323;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[2].Width = 110;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[3].Width = 110;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[4].Width = 60;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            if (MODE_CD_SRC.Equals(this.Par1))
            {
                this.dgvList.Size = new Size(280, 403);
                this.dgvList.Columns[0].Width = 50;
                this.dgvList.Columns[1].Width = 227;
            }
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            // 税区分ビューからデータを取得して表示
            Sql.AppendLine("SELECT");
            Sql.AppendLine(" A.ZEI_KUBUN,");
            Sql.AppendLine(" A.ZEI_KUBUN_NM,");
            Sql.AppendLine(" A.KAZEI_KUBUN_NM,");
            Sql.AppendLine(" B.TAISHAKU_KUBUN_NM,");
            Sql.AppendLine(" A.ZEI_RITSU ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" VI_ZM_ZEI_KUBUN AS A ");
            Sql.AppendLine("LEFT JOIN");
            Sql.AppendLine(" TB_ZM_F_TAISHAKU_KUBUN AS B ");
            Sql.AppendLine("ON");
            Sql.AppendLine(" A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN ");
            // 初期処理でない場合、入力されたカナ名から検索する
            if (!ValChk.IsEmpty(this.txtZeiKubunNm.Text))
            {
                Sql.AppendLine("WHERE");
                Sql.AppendLine(" A.ZEI_KUBUN_NM LIKE @ZEI_KUBUN_NM ");

                // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                dpc.SetParam("@ZEI_KUBUN_NM", SqlDbType.VarChar, 62, "%" + this.txtZeiKubunNm.Text + "%");
            }
            Sql.AppendLine("ORDER BY");
            Sql.AppendLine(" A.ZEI_KUBUN");
            DataTable dtMailLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 該当データがなければエラーメッセージを表示
            if (dtMailLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
            }

            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 列の定義を作成
            dtResult.Columns.Add("区分", typeof(int));
            dtResult.Columns.Add("税 区 分 名 称", typeof(string));
            dtResult.Columns.Add("課税区分名称", typeof(string));
            dtResult.Columns.Add("貸借区分", typeof(string));
            dtResult.Columns.Add("税率", typeof(decimal));

            for (int i = 0; i < dtMailLoop.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();

                drResult["区分"] = dtMailLoop.Rows[i]["ZEI_KUBUN"];
                drResult["税 区 分 名 称"] = dtMailLoop.Rows[i]["ZEI_KUBUN_NM"];
                drResult["課税区分名称"] = dtMailLoop.Rows[i]["KAZEI_KUBUN_NM"];
                drResult["貸借区分"] = dtMailLoop.Rows[i]["TAISHAKU_KUBUN_NM"];
                drResult["税率"] = dtMailLoop.Rows[i]["ZEI_RITSU"];

                dtResult.Rows.Add(drResult);
            }

            this.dgvList.DataSource = dtResult;
            
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            this.dgvList.Columns[0].Width = 60;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 323;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[2].Width = 110;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[3].Width = 110;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[4].Width = 60;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            if (MODE_CD_SRC.Equals(this.Par1))
            {
                //this.dgvList.Size = new Size(280, 403);
                //this.dgvList.Columns[0].Width = 50;
                //this.dgvList.Columns[1].Width = 227;
            }
        }

        /// <summary>
        /// 税区分を追加編集する
        /// </summary>
        /// <param name="code"></param>
        private void EditZeiKbn(string code)
        {
            ZMCM1062 frm1062;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frm1062 = new ZMCM1062("1", "0");
            }
            else
            {
                // 編集モードで登録画面を起動
                frm1062 = new ZMCM1062("2", code);
            }

            DialogResult result = frm1062.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData();
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["区分"].Value)))
                    {
                        this.dgvList.FirstDisplayedScrollingRowIndex = i;
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }

                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }

        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[2] {
                Util.ToString(this.dgvList.SelectedRows[0].Cells[0].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells[1].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
