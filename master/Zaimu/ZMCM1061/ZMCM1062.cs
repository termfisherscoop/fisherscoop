﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1061
{
    /// <summary>
    /// 税区分の登録(ZMCM1062)
    /// </summary>
    public partial class ZMCM1062 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public ZMCM1062(string par1, string par2)
            : base(par1, par2)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();

            //MessageForwarder(マウスホール用) のインスタンス生成．対象はtxtGridEditとdgvInputList
            new MessageForwarder(this.txtGridEdit, 0x20A);
            new MessageForwarder(this.dgvInputList, 0x20A);
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                // ボタンの活性非活性を設定
                this.btnF3.Enabled = false;
                this.btnF6.Text = "F6" + "\n\r" + "\n\r" + "登録";
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
                // ボタンの活性非活性を設定
                this.btnF3.Enabled = true;
                this.btnF6.Text = "F6" + "\n\r" + "\n\r" + "更新";
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF1.Enabled = false; // 現状常に非活性
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtZeiKubunCd":
                case "txtZeiKubunNm":
                case "txtKazeiKubunCd":
                case "txtTorihikiKubunCd":
                case "txtTaishakuKubunCd":
                case "txtZeiRitsu":
                    this.btnF8.Enabled = false;
                    this.txtGridEdit.Visible = false;
                    break;
                case "txtGridEdit":
                    this.btnF8.Enabled = true;
                    break;
                default:
                    this.btnF8.Enabled = true;
                    this.txtGridEdit.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        
        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }
            DeleteZEI_KUBUN();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            //入力チェック
            if(!ValidateAll())
            {
                return;
            }

            // F6ボタンにフォーカスを当てる
            this.btnF6.Select();

            // フォーカスがF6ボタンにある場合のみ、次の処理へ進む
            // ※エラーがある場合、フォーカスはエラーのある箇所へ移動するため
            if (!this.btnF6.Focused)
            {
                return;
            }

            // 登録前チェック
            if (!IsValidTekiyouKaishi())
            {
                Msg.Error("課税の場合、適用開始日はを件以上登録してください。");
                return;
            }

            // 引数：Par1／モード(1:新規、2:変更)
            if (MODE_NEW.Equals(this.Par1))
            {
                if (Msg.ConfYesNo("登録しますか？") == DialogResult.No)
                {
                    // 「いいえ」を押されたら処理終了
                    return;
                }
                InsertZEI_KUBUN();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                if (Msg.ConfYesNo("更新しますか？") == DialogResult.No)
                {
                    // 「いいえ」を押されたら処理終了
                    return;
                }
                UpdateZEI_KUBUN();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF8();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF8()
        {
            // 行削除処理
            if (this.dgvInputList.RowCount > 1 && this.dgvInputList.RowCount > this.dgvInputList.CurrentCell.RowIndex)
            {
                this.dgvInputList.Rows.RemoveAt(this.dgvInputList.CurrentCell.RowIndex);
                int i = 0;
                while (this.dgvInputList.RowCount > i)
                {
                    this.dgvInputList[0, i].Value = i + 1;
                    i++;
                }
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 課税区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKazeiKubunCd_Validating(object sender, CancelEventArgs e)
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtKazeiKubunCd.Text))
            {
                this.txtKazeiKubunCd.Text = "0";
            }

            // 0と２の場合、税率とグリッドビューを非活性にし、税率は初期標示「0.0」にする
            if (this.txtKazeiKubunCd.Text == "0" || this.txtKazeiKubunCd.Text == "2")
            {
                this.txtZeiRitsu.Enabled = false;
                this.dgvInputList.Enabled = false;
                this.txtZeiRitsu.ReadOnly = true;
                this.dgvInputList.ReadOnly = true;
                this.txtZeiRitsu.Text = "0.0";
            }
            // 1の場合、税率とグリッドビューを活性する
            else
            {
                this.txtZeiRitsu.Enabled = true;
                this.dgvInputList.Enabled = true;
                this.txtZeiRitsu.ReadOnly = false;
                this.dgvInputList.ReadOnly = false;
                this.dgvInputList.Columns[0].ReadOnly = true;
            }

            // 取引区分にフォーカスを移動する
            this.txtTorihikiKubunCd.Focus();
        }

        /// <summary>
        /// 取引区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTorihikiKubunCd_Validating(object sender, CancelEventArgs e)
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtTorihikiKubunCd.Text.Trim()))
            {
                this.txtTorihikiKubunCd.Text = "0";
            }
        }

        /// <summary>
        /// 貸借区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTaishakuKubunCd_Validating(object sender, CancelEventArgs e)
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtTaishakuKubunCd.Text))
            {
                this.txtTaishakuKubunCd.Text = "1";
            }
            else if (this.txtTaishakuKubunCd.Text == "0")
            {
                this.txtTaishakuKubunCd.Text = "1";
            }
        }

        /// <summary>
        /// 税率の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiRitsu_Validating(object sender, CancelEventArgs e)
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtZeiRitsu.Text))
            {
                this.txtZeiRitsu.Text = "0.0";
            }
        }

        /// <summary>
        /// データグリッドのセルにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 1:
                case 2:
                    this.txtGridEdit.BackColor = Color.White;
                    this.txtGridEdit.Visible = true;
                    DataGridView dgv = (DataGridView)sender;
                    Rectangle rctCell = dgv.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);

                    this.txtGridEdit.Size = rctCell.Size;
                    this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
                    this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;
                    this.txtGridEdit.Text = Util.ToString(dgvInputList[e.ColumnIndex, e.RowIndex].Value).Replace(",", "");

                    this.txtGridEdit.Focus();
                    break;
                default:
                    this.txtGridEdit.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// データグリッドがスクロールした時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            Rectangle rctCell = dgv.GetCellDisplayRectangle(this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex, false);

            this.txtGridEdit.Size = rctCell.Size;
            this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
            this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;
        }

        /// <summary>
        /// データグリッド用のテキストでのフォーカス移動時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGridEdit_Validating(object sender, CancelEventArgs e)
        {
            this.IsValidGridEdit();
        }

        /// <summary>
        /// グリッドでのマウスアップ時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_MouseUp(object sender, MouseEventArgs e)
        {
            // F8ボタンを活性化する
            this.btnF8.Enabled = true;
        }

        /// <summary>
        /// グリッドアクティブ時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Enter(object sender, EventArgs e)
        {
            // F8ボタンを活性化する
            this.btnF8.Enabled = true;
        }

        /// <summary>
        /// 税率Enter押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiRitsu_KeyDown(object sender, KeyEventArgs e)
        {
            // Enterキーの場合、山Noにフォーカス
            if (e.KeyCode == Keys.Enter)
            {
                dgvInputList.Focus();
                dgvInputList.CurrentCell = dgvInputList[1, 0];
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 税区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidZeiKubun()
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtZeiKubunCd.Text))
            {
                this.txtZeiKubunCd.Text = "0";
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtZeiKubunCd.Text, this.txtZeiKubunCd.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtZeiKubunCd.Focus();
                this.txtZeiKubunCd.SelectAll();
                return false;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示
            else if (!ValChk.IsNumber(this.txtZeiKubunCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtZeiKubunCd.Focus();
                this.txtZeiKubunCd.SelectAll();
                return false;
            }

            // 税区分の存在チェック(既に存在する場合、エラーを表示)
            DataTable dtZeiKubun = GetZeiKubunData(this.txtZeiKubunCd.Text);
            if (dtZeiKubun.Rows.Count > 0)
            {
                Msg.Notice("登録済です。");
                this.txtZeiKubunCd.Focus();
                this.txtZeiKubunCd.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 課税区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKazeiKubun()
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtKazeiKubunCd.Text))
            {
                this.txtKazeiKubunCd.Text = "0";
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtKazeiKubunCd.Text, this.txtKazeiKubunCd.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtKazeiKubunCd.Focus();
                this.txtKazeiKubunCd.SelectAll();
                return false;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示
            else if (!ValChk.IsNumber(this.txtKazeiKubunCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtKazeiKubunCd.Focus();
                this.txtKazeiKubunCd.SelectAll();
                return false;
            }
            //// 1より大きい場合は、1として処理
            //int kazeiKubunCd = Util.ToInt(this.txtKazeiKubunCd.Text);
            //if (kazeiKubunCd > 1)
            //{
            //    this.txtKazeiKubunCd.Text = "1";
            //}
            // 2より大きい場合は、1として処理
            int kazeiKubunCd = Util.ToInt(this.txtKazeiKubunCd.Text);
            if (kazeiKubunCd > 2)
            {
                this.txtKazeiKubunCd.Text = "1";
            }

            return true;
        }

        /// <summary>
        /// 取引区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTorihikiKubun()
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtTorihikiKubunCd.Text.Trim()))
            {
                this.txtTorihikiKubunCd.Text = "0";
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtTorihikiKubunCd.Text, this.txtTorihikiKubunCd.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtTorihikiKubunCd.Focus();
                this.txtTorihikiKubunCd.SelectAll();
                return false;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示
            else if (!ValChk.IsNumber(this.txtTorihikiKubunCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtTorihikiKubunCd.Focus();
                this.txtTorihikiKubunCd.SelectAll();
                return false;
            }

            // 取引区分コードに紐づく取引区分名を取得し表示
            DataTable dtResult = this.GetTorihikiKubunData();
            if (dtResult.Rows.Count == 0)
            {
                this.lblTorihikiKubunNm.Text = "";
                Msg.Notice("入力に誤りがあります。");
                this.txtTorihikiKubunCd.Focus();
                this.txtTorihikiKubunCd.SelectAll();
                return false;
            }
            else
            {
                this.lblTorihikiKubunNm.Text = dtResult.Rows[0]["NM"].ToString();
            }

            return true;
        }

        /// <summary>
        /// 貸借区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTaishakuKubunCd()
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtTaishakuKubunCd.Text))
            {
                this.txtTaishakuKubunCd.Text = "1";
            }
            else if (this.txtTaishakuKubunCd.Text == "0")
            {
                this.txtTaishakuKubunCd.Text = "1";
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtTaishakuKubunCd.Text, this.txtTaishakuKubunCd.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtTaishakuKubunCd.Focus();
                this.txtTaishakuKubunCd.SelectAll();
                return false;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示
            else if (!ValChk.IsNumber(this.txtTaishakuKubunCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtTaishakuKubunCd.Focus();
                this.txtTaishakuKubunCd.SelectAll();
                return false;
            }
            // 2より大きい場合は、2として処理
            int taishakuKubunCd = Util.ToInt(this.txtTaishakuKubunCd.Text);
            if (taishakuKubunCd > 2)
            {
                this.txtTaishakuKubunCd.Text = "2";
            }

            return true;
        }

        /// <summary>
        /// 税率の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidZeiRitsu()
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtZeiRitsu.Text))
            {
                this.txtZeiRitsu.Text = "0.0";
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtZeiRitsu.Text, this.txtZeiRitsu.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtZeiRitsu.Focus();
                this.txtZeiRitsu.SelectAll();
                return false;
            }
            // 99.9%より大きい場合は、99.9として処理
            else if (!ValChk.IsDecNumWithinLength(this.txtZeiRitsu.Text, 2, 1, true))
            {
                Msg.Notice("99.9%以内の数値で入力してください。");
                this.txtZeiRitsu.Text = "99.9";
                this.txtZeiRitsu.Focus();
                this.txtZeiRitsu.SelectAll();
                return false;
            }

            // 入力値をフォーマット
            this.txtZeiRitsu.Text = Util.FormatNum(this.txtZeiRitsu.Text, 1);

            return true;
        }

        /// <summary>
        /// 適用開始日の入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool IsValidTekiyouKaishi()
        {
            // 課税区分が課税の場合のみチェックを行う
            if (this.txtKazeiKubunCd.Text == "1")
            {
                foreach (DataGridViewRow rows in this.dgvInputList.Rows)
                {
                    // 適用開始日が1件でも存在する場合、trueを返す
                    if (this.dgvInputList.Rows[rows.Index].Cells[1].Value != null)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// データグリッドの入力チェック
        /// </summary>
        private void IsValidGridEdit()
        {
            DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
            string value;
            switch (dgvCell.ColumnIndex)
            {
                // 適用開始日
                case 1:
                    value = this.txtGridEdit.Text;
                    this.dgvInputList.Rows[dgvCell.RowIndex].Cells[1].Value = value;
                    // 編集セルが最終行かつ空の場合
                    if (dgvCell.RowIndex == dgvInputList.RowCount - 1 && value == "")
                    {
                        break;
                    }

                    if (!this.IsValidDgvInputListTekiyouKaishi(value, dgvCell.RowIndex, dgvCell.ColumnIndex))
                    {
                        // TODO:直接その場にフォーカス設定できなかったため、一度税率に移動してから戻しています
                        this.dgvInputList.Rows[dgvCell.RowIndex].Cells[2].Selected = true;
                        this.dgvInputList.Rows[dgvCell.RowIndex].Cells[1].Selected = true;
                    }
                    else
                    {
                        // フォーカスを税率に移動させる
                        this.dgvInputList.Rows[dgvCell.RowIndex].Cells[2].Selected = true;
                        if (this.dgvInputList.RowCount == dgvCell.RowIndex + 1)
                        {
                            // 空行を追加
                            this.dgvInputList.Rows.Add();
                            this.dgvInputList.Rows[dgvCell.RowIndex + 1].Cells[0].Value = dgvInputList.RowCount;
                        }
                    }
                    break;
                // 税率
                case 2:
                    value = this.txtGridEdit.Text;
                    this.dgvInputList.Rows[dgvCell.RowIndex].Cells[2].Value = value;
                    // 編集セルが最終行かつ空の場合
                    if (dgvCell.RowIndex == dgvInputList.RowCount - 1 && value == "")
                    {
                        break;
                    }
                    if (!this.IsValidDgvInputListZeiRitsu(value, dgvCell.RowIndex, dgvCell.ColumnIndex))
                    {
                        // TODO:直接その場にフォーカス設定できなかったため、一度適用開始日に移動してから戻しています
                        this.dgvInputList.Rows[dgvCell.RowIndex].Cells[1].Selected = true;
                        this.dgvInputList.Rows[dgvCell.RowIndex].Cells[2].Selected = true;
                    }
                    else
                    {
                        // 適用開始日が入力されている場合、フォーカスを次の行の適用開始日に移動させる
                        if (this.dgvInputList.Rows[dgvCell.RowIndex].Cells[1].Value != null)
                        {
                            this.dgvInputList.Rows[dgvCell.RowIndex + 1].Cells[1].Selected = true;
                        }
                        // 適用開始日が未入力の場合、フォーカスをその場にとどめる
                        else
                        {
                            // TODO:直接その場にフォーカス設定できなかったため、一度適用開始日に移動してから戻しています
                            this.dgvInputList.Rows[dgvCell.RowIndex].Cells[1].Selected = true;
                            this.dgvInputList.Rows[dgvCell.RowIndex].Cells[2].Selected = true;
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if(!IsValidZeiKubun())
            {
                return false;
            }

            if (!IsValidKazeiKubun())
            {
                return false;
            }

            if (!IsValidTorihikiKubun())
            {
                return false;
            }

            if(!IsValidTaishakuKubunCd())
            {
                return false;
            }

            if(!IsValidZeiRitsu())
            {
                return false;
            }

            if(!IsValidTekiyouKaishi())
            {
                return false;
            }
            this.IsValidGridEdit();

            return true;
        }

        /// <summary>
        /// データグリッド(適用開始日)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDgvInputListTekiyouKaishi(string value, int rowIndexNo, int columnIndexNo)
        {
            DateTime dtmCheck;
            // 空の場合、エラーを表示する
            if (ValChk.IsEmpty(value))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 正しい日時でない場合、エラーを表示する
            else if(!DateTime.TryParseExact(value, "yyyy/M/d", null, System.Globalization.DateTimeStyles.None, out dtmCheck))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            DateTime imevaluet = DateTime.Parse(value);
            this.dgvInputList.Rows[rowIndexNo].Cells[columnIndexNo].Value = imevaluet.ToString("yyyy/MM/dd");

            return true;
        }

        /// <summary>
        /// データグリッド(税率)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDgvInputListZeiRitsu(string value, int rowIndexNo, int columnIndexNo)
        {
            // 空は許可する
            if (ValChk.IsEmpty(value))
            {
                return true;
            }
            // 99.99%より大きい場合は、99.99として処理
            else if (!ValChk.IsDecNumWithinLength(value, 2, 2, true))
            {
                Msg.Notice("99.9%以内の数値で入力してください。");
                return false;
            }

            // 入力値をフォーマット
            this.dgvInputList.Rows[rowIndexNo].Cells[2].Value = Util.FormatNum(value, 2);

            return true;
        }

        /// <summary>
        /// データグリッドビューの設定
        /// </summary>
        private void InitDispDataGridView()
        {
            DataTable dtResult = new DataTable();

            // 列の定義を作成
            dtResult.Columns.Add("行", typeof(int));
            dtResult.Columns.Add("適用開始日", typeof(string));
            dtResult.Columns.Add("税率", typeof(double));

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvInputList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvInputList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvInputList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvInputList.Columns[0].Width = 30;
            this.dgvInputList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInputList.Columns[1].Width = 100;
            this.dgvInputList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvInputList.Columns[2].Width = 50;
            this.dgvInputList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // ボタンの活性非活性を設定
            this.btnF3.Enabled = false;
            // 取引区分名を取得(現状のデフォルト0の場合にて)
            DataTable dtTorihikiKubun = this.GetTorihikiKubunData();
            if (dtTorihikiKubun.Rows.Count > 0)
            {
                this.lblTorihikiKubunNm.Text = dtTorihikiKubun.Rows[0]["NM"].ToString();
            }

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvInputList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // ユーザ操作による行追加を無効(禁止)
            this.dgvInputList.AllowUserToAddRows = false;
            // 入力モード設定
            //this.dgvInputList.EditMode = DataGridViewEditMode.EditProgrammatically;

            // 行追加(1行目)
            this.dgvInputList.Rows.Clear();
            this.dgvInputList.RowCount = 1;
            this.dgvInputList[0, 0].Value = "1";

            // 税率とデータグリッドビューを非活性にする
            this.txtZeiRitsu.Enabled = false;
            this.dgvInputList.Enabled = false;
            this.txtZeiRitsu.ReadOnly = true;
            this.dgvInputList.ReadOnly = true;

            // 税区分にフォーカスを当てる
            this.txtZeiKubunCd.Focus();
            this.txtZeiKubunCd.Select();
            DataTable dtMaxZeikbn =
                this.Dba.GetDataTableByCondition("MAX(ZEI_KUBUN + 1) AS MAX_ZEI_KUBUN",
                    "TB_ZM_F_ZEI_KUBUN");
            if (dtMaxZeikbn.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxZeikbn.Rows[0]["MAX_ZEI_KUBUN"]))
            {
                this.txtZeiKubunCd.Text = Util.ToString(Util.ToInt(dtMaxZeikbn.Rows[0]["MAX_ZEI_KUBUN"]));
            }
            else 
            {
                this.txtZeiKubunCd.Text = "0";
            }
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 親画面で選択された区分値を設定
            this.txtZeiKubunCd.Text = this.Par2;

            // 税区分値に紐づくデータを取得し、表示する
            DataTable dtZeiKubun = GetZeiKubunData(this.Par2);
            if (dtZeiKubun.Rows.Count > 0)
            {
                this.txtZeiKubunNm.Text = dtZeiKubun.Rows[0]["ZEI_KUBUN_NM"].ToString();
                this.txtKazeiKubunCd.Text = dtZeiKubun.Rows[0]["KAZEI_KUBUN"].ToString();
                this.txtTorihikiKubunCd.Text = dtZeiKubun.Rows[0]["TORIHIKI_KUBUN"].ToString();
                this.lblTorihikiKubunNm.Text = dtZeiKubun.Rows[0]["TORIHIKI_KUBUN_NM"].ToString();
                this.txtTaishakuKubunCd.Text = dtZeiKubun.Rows[0]["TAISHAKU_KUBUN"].ToString();
                this.txtZeiRitsu.Text = Util.FormatNum(dtZeiKubun.Rows[0]["ZEI_RITSU"].ToString(), 1);
            }

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvInputList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // ユーザ操作による行追加を無効(禁止)
            this.dgvInputList.AllowUserToAddRows = false;
            // 入力モード設定
            this.dgvInputList.EditMode = DataGridViewEditMode.EditProgrammatically;

            // グリッドをクリア
            this.dgvInputList.Rows.Clear();
            // 税区分値に紐づくデータを取得し、データグリッドビューに設定する
            DataTable dtShinZeiRitsu = GetZeiKubunShinZeiRitsu(this.Par2);
            // データグリッドビューを設定
            this.dgvInputList.RowCount = dtShinZeiRitsu.Rows.Count+1;
            int i = 0;
            foreach (DataRow dr in dtShinZeiRitsu.Rows)
            {
                this.dgvInputList[0, i].Value = i + 1;
                this.dgvInputList[1, i].Value = ((DateTime)dr["TEKIYOU_KAISHI"]).ToShortDateString();
                this.dgvInputList[2, i].Value = dr["SHIN_ZEI_RITSU"].ToString();
                i++;
            }
            // 行追加(最後の行目)
            this.dgvInputList[0, i].Value = i + 1;

            // 税区分を非活性にする
            this.txtZeiKubunCd.Enabled = false;
            this.txtZeiKubunCd.ReadOnly = true;

            // 税率とデータグリッドビューの活性非活性を判断する
            if (this.txtKazeiKubunCd.Text == "0")
            {
                this.txtZeiRitsu.Enabled = false;
                this.dgvInputList.Enabled = false;
                this.txtZeiRitsu.ReadOnly = true;
                this.dgvInputList.ReadOnly = true;
            }
            else
            {
                this.txtZeiRitsu.Enabled = true;
                this.dgvInputList.Enabled = true;
                this.txtZeiRitsu.ReadOnly = false;
                this.dgvInputList.ReadOnly = false;
                this.dgvInputList.Columns[0].ReadOnly = true;
            }

            // 税区分名称にフォーカスを当てる
            this.txtZeiKubunNm.Focus();
            this.txtZeiKubunNm.Select();
        }

        /// <summary>
        /// 取引区分名を取得
        /// </summary>
        private DataTable GetTorihikiKubunData()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT");
            sql.AppendLine(" TORIHIKI_KUBUN AS KUBUN,");
            sql.AppendLine(" TORIHIKI_KUBUN_NM AS NM ");
            sql.AppendLine("FROM");
            sql.AppendLine(" TB_ZM_F_TORIHIKI_KUBUN ");
            sql.AppendLine("WHERE");
            sql.AppendLine(" TORIHIKI_KUBUN = @TORIHIKI_KUBUN");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, Util.ToInt(this.txtTorihikiKubunCd.Text));

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// 税区分に紐づくデータを取得
        /// </summary>
        private DataTable GetZeiKubunData(string zeiKubun)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            // 税区分ビューからデータを取得して表示
            Sql.AppendLine("SELECT");
            Sql.AppendLine(" ZEI_KUBUN_NM,");
            Sql.AppendLine(" KAZEI_KUBUN,");
            Sql.AppendLine(" TORIHIKI_KUBUN,");
            Sql.AppendLine(" TORIHIKI_KUBUN_NM,");
            Sql.AppendLine(" TAISHAKU_KUBUN,");
            Sql.AppendLine(" ZEI_RITSU ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" VI_ZM_ZEI_KUBUN ");
            Sql.AppendLine("WHERE");
            Sql.AppendLine(" ZEI_KUBUN = @ZEI_KUBUN ");
            Sql.AppendLine("ORDER BY");
            Sql.AppendLine(" ZEI_KUBUN");

            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, zeiKubun);

            DataTable dtZeiKubun = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtZeiKubun;
        }

        /// <summary>
        /// 税区分に紐づくデータを取得
        /// </summary>
        private DataTable GetZeiKubunShinZeiRitsu(string zeiKubun)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            // 税区分ビューからデータを取得して表示
            Sql.AppendLine("SELECT");
            Sql.AppendLine(" TEKIYOU_KAISHI,");
            Sql.AppendLine(" SHIN_ZEI_RITSU ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" TB_ZM_F_ZEI_KBN_SHIN_ZEI_RT ");
            Sql.AppendLine("WHERE");
            Sql.AppendLine(" ZEI_KUBUN = @ZEI_KUBUN ");

            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, zeiKubun);

            DataTable dtShinZeiRitsu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtShinZeiRitsu;
        }

        // 削除
        private void DeleteZEI_KUBUN()
        {
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // TB_ZM_F_ZEI_KUBUNテーブルから削除
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, this.txtZeiKubunCd.Text);
                this.Dba.Delete("TB_ZM_F_ZEI_KUBUN", "ZEI_KUBUN = @ZEI_KUBUN", whereParam);

                // TB_ZM_F_ZEI_KBN_SHIN_ZEI_RTテーブルから削除
                this.Dba.Delete("TB_ZM_F_ZEI_KBN_SHIN_ZEI_RT", "ZEI_KUBUN = @ZEI_KUBUN", whereParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 税区分のデータを更新
        /// </summary>
        private void UpdateZEI_KUBUN()
        {
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // TB_ZM_F_ZEI_KUBUNテーブルを更新
                DbParamCollection updParam = new DbParamCollection();
                DbParamCollection whereParam = new DbParamCollection();
                updParam.SetParam("@ZEI_KUBUN_NM", SqlDbType.VarChar, 60, this.txtZeiKubunNm.Text);
                updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, this.txtKazeiKubunCd.Text);
                updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, this.txtTorihikiKubunCd.Text);
                updParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, this.txtTaishakuKubunCd.Text);
                updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 6, this.txtZeiRitsu.Text);
                whereParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, this.txtZeiKubunCd.Text);
                this.Dba.Update("TB_ZM_F_ZEI_KUBUN", updParam, "ZEI_KUBUN = @ZEI_KUBUN", whereParam);

                // TB_ZM_F_ZEI_KBN_SHIN_ZEI_RTテーブルから削除
                whereParam = new DbParamCollection();
                whereParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, this.txtZeiKubunCd.Text);
                this.Dba.Delete("TB_ZM_F_ZEI_KBN_SHIN_ZEI_RT", "ZEI_KUBUN = @ZEI_KUBUN", whereParam);

                // TB_ZM_F_ZEI_KBN_SHIN_ZEI_RTテーブルへ登録
                //INSERT INTO ZAM.TB_Ｆ税区分新税率 (税区分, 適用開始, 新税率 ) VALUES (36,'2015/02/15', 0)
                // 課税区分が課税の場合のみ登録を行う
                DbParamCollection insParam = new DbParamCollection();
                if (this.txtKazeiKubunCd.Text == "1")
                {
                    foreach (DataGridViewRow rows in this.dgvInputList.Rows)
                    {
                        // 適用開始日と税率が入力されている行の登録を行う
                        if (this.dgvInputList.Rows[rows.Index].Cells[1].Value != null &&
                            (this.dgvInputList.Rows[rows.Index].Cells[2].Value != null))
                        {
                            insParam = new DbParamCollection();
                            insParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, this.txtZeiKubunCd.Text);
                            insParam.SetParam("@TEKIYOU_KAISHI", SqlDbType.DateTime, DateTime.Parse(this.dgvInputList.Rows[rows.Index].Cells[1].Value.ToString()));
                            insParam.SetParam("@SHIN_ZEI_RITSU", SqlDbType.Decimal, 6, this.dgvInputList.Rows[rows.Index].Cells[2].Value.ToString());
                            this.Dba.Insert("TB_ZM_F_ZEI_KBN_SHIN_ZEI_RT", insParam);
                        }
                    }
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 税区分のデータを登録
        /// </summary>
        private void InsertZEI_KUBUN()
        {
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // TB_ZM_F_ZEI_KUBUNテーブルへの登録
                DbParamCollection insParam = new DbParamCollection();
                insParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, this.txtZeiKubunCd.Text);
                insParam.SetParam("@ZEI_KUBUN_NM", SqlDbType.VarChar, 60, this.txtZeiKubunNm.Text);
                insParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, this.txtKazeiKubunCd.Text);
                insParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, this.txtTorihikiKubunCd.Text);
                insParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 6, this.txtZeiRitsu.Text);
                insParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, this.txtTaishakuKubunCd.Text);
                this.Dba.Insert("TB_ZM_F_ZEI_KUBUN", insParam);

                // TB_ZM_F_ZEI_KBN_SHIN_ZEI_RTテーブルからの削除
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, this.txtZeiKubunCd.Text);
                this.Dba.Delete("TB_ZM_F_ZEI_KBN_SHIN_ZEI_RT", "ZEI_KUBUN = @ZEI_KUBUN", whereParam);

                // TB_ZM_F_ZEI_KBN_SHIN_ZEI_RTテーブルへの登録
                //INSERT INTO ZAM.TB_Ｆ税区分新税率 (税区分, 適用開始, 新税率 ) VALUES (36,'2015/02/15', 0)
                // 課税区分が課税の場合のみ登録を行う
                if (this.txtKazeiKubunCd.Text == "1")
                {
                    foreach (DataGridViewRow rows in this.dgvInputList.Rows)
                    {
                        // 適用開始日と税率が入力されている行の登録を行う
                        if (this.dgvInputList.Rows[rows.Index].Cells[1].Value.ToString() != "" &&
                           (this.dgvInputList.Rows[rows.Index].Cells[2].Value.ToString() != null))
                        {
                            insParam = new DbParamCollection();
                            insParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, this.txtZeiKubunCd.Text);
                            insParam.SetParam("@TEKIYOU_KAISHI", SqlDbType.DateTime, DateTime.Parse(this.dgvInputList.Rows[rows.Index].Cells[1].Value.ToString()));
                            insParam.SetParam("@SHIN_ZEI_RITSU", SqlDbType.Decimal, 6, this.dgvInputList.Rows[rows.Index].Cells[2].Value.ToString());
                            this.Dba.Insert("TB_ZM_F_ZEI_KBN_SHIN_ZEI_RT", insParam);
                        }
                    }
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }
		#endregion
	}
}
