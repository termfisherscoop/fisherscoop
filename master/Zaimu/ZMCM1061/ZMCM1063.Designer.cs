﻿namespace jp.co.fsi.zm.zmcm1061
{
    partial class ZMCM1063
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtZeiKubunFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKubunFr = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblZeiKubunTo = new System.Windows.Forms.Label();
            this.txtZeiKubunTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Text = "Esc\r\n\r\n戻る";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4\r\n\r\nプレビュー";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 74);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(833, 129);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(843, 31);
            this.lblTitle.Text = "税区分の印刷";
            // 
            // txtZeiKubunFr
            // 
            this.txtZeiKubunFr.AutoSizeFromLength = false;
            this.txtZeiKubunFr.DisplayLength = null;
            this.txtZeiKubunFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtZeiKubunFr.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtZeiKubunFr.Location = new System.Drawing.Point(3, 18);
            this.txtZeiKubunFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtZeiKubunFr.MaxLength = 2;
            this.txtZeiKubunFr.Name = "txtZeiKubunFr";
            this.txtZeiKubunFr.Size = new System.Drawing.Size(36, 23);
            this.txtZeiKubunFr.TabIndex = 2;
            this.txtZeiKubunFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiKubunFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKubunFr_Validating);
            // 
            // lblZeiKubunFr
            // 
            this.lblZeiKubunFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblZeiKubunFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKubunFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblZeiKubunFr.Location = new System.Drawing.Point(42, 17);
            this.lblZeiKubunFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZeiKubunFr.Name = "lblZeiKubunFr";
            this.lblZeiKubunFr.Size = new System.Drawing.Size(300, 24);
            this.lblZeiKubunFr.TabIndex = 2;
            this.lblZeiKubunFr.Tag = "DISPNAME";
            this.lblZeiKubunFr.Text = "先　頭";
            this.lblZeiKubunFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblCodeBet.Location = new System.Drawing.Point(350, 17);
            this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(24, 24);
            this.lblCodeBet.TabIndex = 16;
            this.lblCodeBet.Tag = "CHANGE";
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZeiKubunTo
            // 
            this.lblZeiKubunTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblZeiKubunTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKubunTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblZeiKubunTo.Location = new System.Drawing.Point(422, 17);
            this.lblZeiKubunTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZeiKubunTo.Name = "lblZeiKubunTo";
            this.lblZeiKubunTo.Size = new System.Drawing.Size(300, 24);
            this.lblZeiKubunTo.TabIndex = 3;
            this.lblZeiKubunTo.Tag = "DISPNAME";
            this.lblZeiKubunTo.Text = "最　後";
            this.lblZeiKubunTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZeiKubunTo
            // 
            this.txtZeiKubunTo.AutoSizeFromLength = false;
            this.txtZeiKubunTo.DisplayLength = null;
            this.txtZeiKubunTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtZeiKubunTo.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtZeiKubunTo.Location = new System.Drawing.Point(377, 18);
            this.txtZeiKubunTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtZeiKubunTo.MaxLength = 2;
            this.txtZeiKubunTo.Name = "txtZeiKubunTo";
            this.txtZeiKubunTo.Size = new System.Drawing.Size(36, 23);
            this.txtZeiKubunTo.TabIndex = 4;
            this.txtZeiKubunTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiKubunTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtZeiKubunTo_KeyDown);
            this.txtZeiKubunTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKubunTo_Validating);
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 31);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 1;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(827, 52);
            this.fsiTableLayoutPanel1.TabIndex = 13;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.lblCodeBet);
            this.fsiPanel1.Controls.Add(this.lblZeiKubunTo);
            this.fsiPanel1.Controls.Add(this.txtZeiKubunFr);
            this.fsiPanel1.Controls.Add(this.txtZeiKubunTo);
            this.fsiPanel1.Controls.Add(this.lblZeiKubunFr);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(819, 44);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(819, 44);
            this.label1.TabIndex = 2;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "税区分範囲";
            // 
            // ZMCM1063
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 204);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1063";
            this.ShowFButton = true;
            this.Text = "税区分の印刷";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunFr;
        private System.Windows.Forms.Label lblZeiKubunFr;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblZeiKubunTo;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunTo;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel1;
		private System.Windows.Forms.Label label1;
	}
}