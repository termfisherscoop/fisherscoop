﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace jp.co.fsi.FrmCommon
{
	[TypeConverter(typeof(CustomClassConverter))]
	public class BaseValidate:Component
	{

		public BaseValidate()
		{

		}

		public virtual void Formmatted() { }
		public virtual void Validating(object sender, CancelEventArgs e) { }
		public virtual bool Validating(object sender) { return true; }
		public virtual void Enter(object sender, EventArgs e) { }
		public virtual bool Enter(object sender) { return true; }

	}
}
