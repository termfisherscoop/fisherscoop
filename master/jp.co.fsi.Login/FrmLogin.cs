﻿using jp.co.fsi.ClientCommon;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jp.co.fsi.Login
{

	/// <summary>
	/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
	/// <para>■ 機能説明： ログイン画面</para>
	/// <para>■ 機能概要： システム利用者認証を行う。</para>
	/// <para>■</para>
	/// <para>■ 作成者：   FSI</para>
	/// <para>■ 作成日：   2019-11</para>
	/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
	/// </summary>
	public partial class FrmLogin : BaseForm
	{

		private Point mousePoint;

		private bool isInit = false;

		private DataTable loDt = null;
		private DataTable losDt = null;
		private bool isWide = false;

		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： コンストラクタ</para>
		/// <para>■ 機能概要： システム利用者認証を行う。</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		public FrmLogin()
		{
			InitializeComponent();
		}


		#region 画面初期化

		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： 画面初期化</para>
		/// <para>■ 機能概要： 画面インスタンス後に実行されるメソッド</para>
		/// <para>■ 　　　　　　ここで画面の初期化を行う。</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		protected override void InitForm()
		{

			this.SuspendLayout();
			this.Height = 282;
			this.ResumeLayout(false);

			string userid = this.Config.LoadCommonConfig("UserInfo", "userid");
			this.txtUserId.Text = userid;

			string loginj = this.Config.LoadCommonConfig("UserInfo", "dologin");

			chkLogin.Checked = true;

			// 次回からログインしない場合はチェックをつける
			if ("1" == loginj)
			{
				chkLogin.Checked = false;
			}

			fsiPanel3.Visible = true;

			isWide = true;

			if (string.Empty == this.UInfo.KaishaNm)
			{
				isInit = true;

				this.SuspendLayout();
				this.Height = 669;
				this.ResumeLayout(false);

				fsiPanel3.Visible = false;

				isWide = false;

				SetList();
				#region 削除
				/*
				loDt = GetDispList();
				losDt = GetDispSList();
				dgvList.DataSource = loDt;

				foreach (DataGridViewColumn ccc in dgvList.Columns)
				{
					if ((ccc.Name == "会社名") || ("決算期" == ccc.Name))
					{
						ccc.Visible = true;
					}
					else
					{
						ccc.Visible = false;
					}
				}

				dtShisho.DataSource = losDt;

				foreach (DataGridViewColumn ccc in dtShisho.Columns)
				{
					if (ccc.Name == "支所名")
					{
						ccc.Visible = true;
					}
					else
					{
						ccc.Visible = false;
					}
				}*/
				#endregion
			}

			this.lblLoginTitle.Text = this.UInfo.KaishaNm;

			ClientUtil.CircleForm(this, 10);

			this.txtPassWord.Font = new System.Drawing.Font("ＭＳ 明朝", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtUserId.Font = new System.Drawing.Font("ＭＳ 明朝", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));

			this.BackColor = Color.SkyBlue;

			//ＭＳ 明朝, 20.25pt
		}

		private void FsiPanel3_Click(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}
		#endregion

		#region ボタンクリックイベント
		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： ボタンクリックイベント</para>
		/// <para>■ 機能概要： ログインあるいはキャンセルボタンが押された場合に</para>
		/// <para>■ 　　　　　　実行するイベント</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		private void btn_Click(object sender, EventArgs e)
		{

			switch(((Button)sender).Name)
			{
				case "btnLogin":

					bLogin();

					break;
				case "btnCancel":

					bCancel();

					break;
			}

		}
		#endregion

		#region プライベートメソッド

		#region ログイン処理
		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： ログイン処理</para>
		/// <para>■ 機能概要： ユーザＩＤ、パスワードで認証チェックを行い</para>
		/// <para>■ 　　　　　　ＯＫの場合はメニュー画面を起動する。</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		private void bLogin()
		{
			string tantoshaCd;// 担当者ID
			string userId;    // ユーザID
			string passWord;  // パスワード
			string seriFlg;   // セリ販売の画面を1：表示する 0：表示しない
			string kobaiFlg;  // 購買の画面を1：表示する 0：表示しない
			string zaimuFlg;  // 財務の画面を1：表示する 0：表示しない
			string kyuyoFlg;  // 給与の画面を1：表示する 0：表示しない

			string useName;

			// TODO 改修したほうがいい部分 
			DbParamCollection dpc = new DbParamCollection();
			StringBuilder sql = new StringBuilder();
/*
			sql.AppendLine("SELECT");
			sql.AppendLine("KAISHA_CD ");
            sql.AppendLine(",SHISHO_CD ");
            sql.AppendLine(",TANTOSHA_CD ");
            sql.AppendLine(",USER_ID ");
            sql.AppendLine(",PASSWORD ");
            sql.AppendLine(",TANTOSHA_NM ");
            sql.AppendLine(",SERI_FLG ");
            sql.AppendLine(",KOBAI_FLG ");
            sql.AppendLine(",ZAIMU_FLG ");
            sql.AppendLine(",KYUYO_FLG ");
            sql.AppendLine("FROM TB_CM_TANTOSHA");
            sql.AppendLine("WHERE");
            sql.AppendLine(" KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" USER_ID = @USER_ID AND");
            sql.AppendLine(" PASSWORD = @PASSWORD");*/

			// 会社名をとってきてないのでコンフィグの中身がかわらん！！！！
			sql.AppendLine("SELECT");
			sql.AppendLine("     MAIN.KAISHA_CD ");
			sql.AppendLine(",    MAIN.SHISHO_CD ");
			sql.AppendLine(",    MAIN.TANTOSHA_CD ");
			sql.AppendLine(",    MAIN.USER_ID ");
			sql.AppendLine(",    MAIN.PASSWORD ");
			sql.AppendLine(",    MAIN.TANTOSHA_NM ");
			sql.AppendLine(",    MAIN.SERI_FLG ");
			sql.AppendLine(",    MAIN.KOBAI_FLG ");
			sql.AppendLine(",    MAIN.ZAIMU_FLG ");
			sql.AppendLine(",    MAIN.KYUYO_FLG ");
			sql.AppendLine(" FROM TB_CM_TANTOSHA MAIN ");
			sql.AppendLine(" WHERE ");
			sql.AppendLine(" KAISHA_CD = @KAISHA_CD AND");
			sql.AppendLine(" USER_ID = @USER_ID AND");
			sql.AppendLine(" PASSWORD = @PASSWORD");


			// ログイン有無設定
			if (chkLogin.Checked)
			{
				this.Config.SetCommonConfig("UserInfo", "dologin", "0");
			}
			else
			{
				this.Config.SetCommonConfig("UserInfo", "dologin", "1");
			}

			if (isInit)
			{
				dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, dgvList.SelectedRows[0].Cells["ｺｰﾄﾞ"].Value.ToString());
			}
			else
			{
				dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
			}

			dpc.SetParam("@USER_ID", SqlDbType.VarChar, 15, this.txtUserId.Text);
			dpc.SetParam("@PASSWORD", SqlDbType.VarChar, 15, this.txtPassWord.Text);
			DataTable dtUser = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

			if (dtUser.Rows.Count > 0)
			{
				tantoshaCd = Util.ToString(dtUser.Rows[0]["TANTOSHA_CD"]);
				userId = Util.ToString(dtUser.Rows[0]["USER_ID"]);
				passWord = Util.ToString(dtUser.Rows[0]["PASSWORD"]);
				seriFlg = Util.ToString(dtUser.Rows[0]["SERI_FLG"]);
				kobaiFlg = Util.ToString(dtUser.Rows[0]["KOBAI_FLG"]);
				zaimuFlg = Util.ToString(dtUser.Rows[0]["ZAIMU_FLG"]);
				kyuyoFlg = Util.ToString(dtUser.Rows[0]["KYUYO_FLG"]);

				useName = Util.ToString(dtUser.Rows[0]["TANTOSHA_NM"]);

				//configを書き換える
				this.Config.SetCommonConfig("UserInfo", "usercd", tantoshaCd);
				this.Config.SetCommonConfig("UserInfo", "userid", userId);
				this.Config.SetCommonConfig("UserInfo", "passwd", passWord);
				this.Config.SetCommonConfig("UserInfo", "usehn", seriFlg);
				this.Config.SetCommonConfig("UserInfo", "usekb", kobaiFlg);
				this.Config.SetCommonConfig("UserInfo", "usezm", zaimuFlg);
				this.Config.SetCommonConfig("UserInfo", "useky", kyuyoFlg);
				this.Config.SetCommonConfig("UserInfo", "usernm", useName);
				//shishocd

				if (isInit)
				{
					this.Config.SetCommonConfig("UserInfo", "kaishacd", dgvList.SelectedRows[0].Cells["ｺｰﾄﾞ"].Value.ToString());
					this.Config.SetCommonConfig("UserInfo", "kaishanm", dgvList.SelectedRows[0].Cells["会社名"].Value.ToString());
					this.Config.SetCommonConfig("UserInfo", "kaikeinendo", dgvList.SelectedRows[0].Cells["会計年度"].Value.ToString());
					this.Config.SetCommonConfig("UserInfo", "kessanki", dgvList.SelectedRows[0].Cells["決算期"].Value.ToString());
					//会計期間
					string ss = "第"+ dgvList.SelectedRows[0].Cells["決算期"].Value.ToString() + "期 (" + 
						dgvList.SelectedRows[0].Cells["会計期間"].Value.ToString() + ")";

					this.Config.SetCommonConfig("UserInfo", "kessankiDsp", ss);


					this.Config.SetCommonConfig("UserInfo", "shishocd", dtShisho.SelectedRows[0].Cells["支所"].Value.ToString());
					this.Config.SetCommonConfig("UserInfo", "shishonm", dtShisho.SelectedRows[0].Cells["支所名"].Value.ToString());


				}

				this.Config.SaveConfig();

				// ユーザ情報は再読み込みさせないといけない
				base.UInfo = new UserInfo(this.Dba);

				// メニュー画面を起動
				this.Visible = false;
                // アセンブリのロード
                Assembly asm = Assembly.LoadFrom("Menu.exe");
                // フォーム作成
                Type t = asm.GetType("jp.co.fsi.Menu.FrmMenu");
                if (t != null)
                {
                    Object obj = System.Activator.CreateInstance(t);
                    if (obj != null)
                    {
                        BaseForm frm = (BaseForm)obj;
                        frm.ShowDialog(this);

                        frm.Dispose();
                    }
                }
                this.Close();
			}
			else
			{
				Msg.Notice("ユーザーIDもしくはパスワードが不正です。" + Environment.NewLine
					+ "再度入力してください。");
				this.txtUserId.Focus();
			}
		}
		#endregion

		#region キャンセル処理

		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： キャンセル処理</para>
		/// <para>■ 機能概要： キャンセル処理を行う。</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		private void bCancel()
		{
			if (Msg.ConfOKCancel(
						"ログインを取りやめます。" + Environment.NewLine +
						"よろしいですか？")
					== DialogResult.OK)
			{
				this.Close();
			}
		}

		#endregion

		#endregion

		private void FrmLogin_MouseDown(object sender, MouseEventArgs e)
		{
			if ((e.Button & MouseButtons.Left)==MouseButtons.Left)
			{
				mousePoint = new Point(e.X,e.Y);
			}
		}

		private void FrmLogin_MouseMove(object sender, MouseEventArgs e)
		{
			if ((e.Button & MouseButtons.Left)== MouseButtons.Left)
			{
				this.Left += e.X - mousePoint.X;
				this.Top += e.Y - mousePoint.Y;
			}
		}

		private void fsiPanel1_MouseDown(object sender, MouseEventArgs e)
		{
			this.Close();
		}

		private void FrmLogin_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				this.SelectNextControl(this.ActiveControl, true, true, true, true);
			}
		}


		private DataTable GetDispSList()
		{
			DataTable dtDBData =
				this.Dba.GetDataTableByCondition("KAISHA_CD, SHISHO_CD, SHISHO_NM",
					"TB_CM_SHISHO", null, "KAISHA_CD ASC, SHISHO_CD ASC");

			DataTable dtResult = new DataTable();
			DataRow drResult;
			dtResult.Columns.Add("会社", typeof(int));
			dtResult.Columns.Add("支所", typeof(int));
			dtResult.Columns.Add("支所名", typeof(string));

			for (int i = 0; i < dtDBData.Rows.Count; i++)
			{
				drResult = dtResult.NewRow();
				drResult["会社"] = dtDBData.Rows[i]["KAISHA_CD"];
				drResult["支所"] = dtDBData.Rows[i]["SHISHO_CD"];
				drResult["支所名"] = dtDBData.Rows[i]["SHISHO_NM"];
				dtResult.Rows.Add(drResult);
			}
			return dtResult;
		}


		private DataTable GetDispList()
		{
			DataTable dtDBData =
				this.Dba.GetDataTableByCondition("KAISHA_CD, KAISHA_NM, KESSANKI, KAIKEI_NENDO, KAIKEI_KIKAN_KAISHIBI, KAIKEI_KIKAN_SHURYOBI, FIX_FLG",
					"VI_ZM_KAISHA_JOHO", null, "KAISHA_CD ASC, KESSANKI DESC");

			DataTable dtResult = new DataTable();
			DataRow drResult;
			dtResult.Columns.Add("ｺｰﾄﾞ", typeof(int));
			dtResult.Columns.Add("会社名", typeof(string));
			dtResult.Columns.Add("決算期", typeof(int));
			dtResult.Columns.Add("会計期間", typeof(string));
			dtResult.Columns.Add("会計年度", typeof(int));

			string kaikeiKikan;
			string[] arrJpDate;

			for (int i = 0; i < dtDBData.Rows.Count; i++)
			{
				drResult = dtResult.NewRow();
				drResult["ｺｰﾄﾞ"] = dtDBData.Rows[i]["KAISHA_CD"];
				drResult["会社名"] = dtDBData.Rows[i]["KAISHA_NM"];
				drResult["決算期"] = dtDBData.Rows[i]["KESSANKI"];
				arrJpDate = Util.ConvJpDate(Util.ToDate(dtDBData.Rows[i]["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
				kaikeiKikan = arrJpDate[6];
				arrJpDate = Util.ConvJpDate(Util.ToDate(dtDBData.Rows[i]["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);
				kaikeiKikan += " ～ " + arrJpDate[6];
				drResult["会計期間"] = kaikeiKikan;
				drResult["会計年度"] = dtDBData.Rows[i]["KAIKEI_NENDO"];
				dtResult.Rows.Add(drResult);
			}
			return dtResult;
		}

		private void fsiPanel3_Click(object sender, EventArgs e)
		{
			if (isWide)
			{
				this.Enabled = false;
				this.SuspendLayout();
				this.AutoSize = true;
				this.FormBorderStyle = FormBorderStyle.Sizable;
				this.Height = 708;
				ClientUtil.CircleForm(this, 10);
				this.FormBorderStyle = FormBorderStyle.None;
				this.AutoSize = false;
				this.ResumeLayout(true);
				isWide = false;
				this.Enabled = true;

				isInit = true;

				SetList();

			}
			else
			{
				this.Enabled = false;
				this.SuspendLayout();
				this.AutoSize = true;
				this.FormBorderStyle = FormBorderStyle.Sizable;
				this.Height = 321;
				ClientUtil.CircleForm(this, 10);
				this.FormBorderStyle = FormBorderStyle.None;
				this.AutoSize = false;
				dgvList.DataSource = null;
				dtShisho.DataSource = null;
				this.ResumeLayout(true);
				isWide = true;
				this.Enabled = true;
				isInit = false;

			}
		}

		private void SetList()
		{

			dgvList.DataSource = null;
			
			dtShisho.DataSource = null;

			loDt = GetDispList();
			losDt = GetDispSList();
			dgvList.DataSource = loDt;
			foreach (DataGridViewColumn ccc in dgvList.Columns)
			{
				if ((ccc.Name == "会社名") || ("決算期" == ccc.Name))
				{
					ccc.Visible = true;
				}
				else
				{
					ccc.Visible = false;
				}
			}
			dtShisho.DataSource = losDt;
			foreach (DataGridViewColumn ccc in dtShisho.Columns)
			{
				if (ccc.Name == "支所名")
				{
					ccc.Visible = true;
				}
				else
				{
					ccc.Visible = false;
				}
			}
		}

	}
}
