using Microsoft.VisualBasic.ApplicationServices;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace systembase.table.My
{
	[GeneratedCode("MyTemplate", "8.0.0.0")]
	[EditorBrowsable(EditorBrowsableState.Never)]
	internal class MyApplication : ApplicationBase
	{
		[DebuggerNonUserCode]
		public MyApplication()
		{
		}
	}
}
