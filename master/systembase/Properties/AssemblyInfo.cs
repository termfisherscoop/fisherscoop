using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTrademark("")]
[assembly: AssemblyProduct("table")]
[assembly: AssemblyCompany("")]
[assembly: ComVisible(false)]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTitle("table")]
[assembly: AssemblyFileVersion("2.11.112.0")]
[assembly: AssemblyCopyright("Copyright ©  2010")]
[assembly: Guid("7b5b0b94-52f6-4582-bd8e-e93bf4235b61")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyVersion("2.11.112.0")]
