using System.Windows.Forms;

namespace systembase.table
{
	public interface IEditor
	{
		public delegate void LeaveEventHandler(object sender, string direction);

		public delegate void ValueChangedEventHandler(object sender);

		object Value
		{
			get;
			set;
		}

		event LeaveEventHandler Leave;

		event ValueChangedEventHandler ValueChanged;

		void Initialize(UTable.CField field);

		void EditEnter(char key);

		void EditLeave();

		Control Control();
	}
}
