using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace systembase.table
{
	public class CButtonFieldProvider : CFieldProvider
	{
		public class CButtonField : UTable.CField
		{
			public bool Down;

			public CButtonField()
			{
				base.Leave += CButtonField_Leave;
				base.KeyDown += CButtonField_KeyDown;
				base.KeyUp += CButtonField_KeyUp;
				base.MouseDown += CButtonField_MouseDown;
				base.MouseUp += CButtonField_MouseUp;
				Down = false;
			}

			private void CButtonField_Leave(UTable.CField field)
			{
				Down = false;
			}

			private void CButtonField_KeyDown(UTable.CField field, KeyEventArgs e)
			{
				if (e.KeyCode == Keys.Space && DynamicSetting().Editable() == UTable.EAllow.ALLOW)
				{
					Down = true;
					Table().Render();
				}
			}

			private void CButtonField_KeyUp(UTable.CField field, KeyEventArgs e)
			{
				if (e.KeyCode == Keys.Space && Down)
				{
					Down = false;
					Table().Render();
					Table().raiseFieldButtonClick(this);
				}
			}

			private void CButtonField_MouseDown(UTable.CField field, Point location, MouseEventArgs e)
			{
				if (e.Button == MouseButtons.Left && DynamicSetting().Editable() == UTable.EAllow.ALLOW)
				{
					Down = true;
					Table().Render();
				}
			}

			private void CButtonField_MouseUp(UTable.CField field, Point location, MouseEventArgs e)
			{
				if (Down)
				{
					Down = false;
					Table().Render();
					if ((location.X >= 0) & (location.Y >= 0))
					{
						Table().raiseFieldButtonClick(this);
					}
				}
			}

			public override void Clear()
			{
			}
		}

		public object DefaultValue;

		private Image _Image;

		private Image _DisabledImage;

		public Image Image
		{
			get
			{
				return _Image;
			}
			set
			{
				_Image = value;
				if (_Image != null)
				{
					float[][] newColorMatrix = new float[5][]
					{
						new float[5]
						{
							0.299f,
							0.299f,
							0.299f,
							0f,
							0f
						},
						new float[5]
						{
							0.587f,
							0.587f,
							0.587f,
							0f,
							0f
						},
						new float[5]
						{
							0.114f,
							0.114f,
							0.114f,
							0f,
							0f
						},
						new float[5]
						{
							0f,
							0f,
							0f,
							1f,
							0f
						},
						new float[5]
						{
							0f,
							0f,
							0f,
							0f,
							1f
						}
					};
					ColorMatrix colorMatrix = new ColorMatrix(newColorMatrix);
					ImageAttributes imageAttributes = new ImageAttributes();
					imageAttributes.SetColorMatrix(colorMatrix);
					_DisabledImage = new Bitmap(_Image.Width, _Image.Height);
					Graphics graphics = Graphics.FromImage(_DisabledImage);
					Image image = _Image;
					Rectangle destRect = new Rectangle(0, 0, _Image.Width, _Image.Height);
					graphics.DrawImage(image, destRect, 0, 0, _Image.Width, _Image.Height, GraphicsUnit.Pixel, imageAttributes);
				}
				else
				{
					_DisabledImage = null;
				}
			}
		}

		public override string Clipboard
		{
			get
			{
				return "";
			}
			set
			{
			}
		}

		public CButtonFieldProvider()
			: this(null)
		{
		}

		public CButtonFieldProvider(object defaultValue)
			: this(null, RuntimeHelpers.GetObjectValue(defaultValue))
		{
		}

		public CButtonFieldProvider(string caption, object defaultValue)
			: this(caption, RuntimeHelpers.GetObjectValue(defaultValue), null)
		{
		}

		public CButtonFieldProvider(string caption, object defaultValue, Image image)
			: base(caption)
		{
			DefaultValue = RuntimeHelpers.GetObjectValue(defaultValue);
			Image = image;
		}

		public Image DisabledImage()
		{
			return _DisabledImage;
		}

		public override UTable.CField CreateField()
		{
			return new CButtonField();
		}

		public override void FieldInitialize(UTable.CField field)
		{
			field.Value = RuntimeHelpers.GetObjectValue(DefaultValue);
		}

		public override UTable.CSetting Setting()
		{
			UTable.CSetting cSetting = new UTable.CSetting();
			cSetting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
			return cSetting;
		}

		protected override void renderForeground(Graphics g, UTable.CField field, UTable.CDynamicSetting s, Rectangle rect, bool alter)
		{
			CFieldProvider.renderButton(g, ((CButtonField)field).Down, formatValue(RuntimeHelpers.GetObjectValue(field.Value)), Image, DisabledImage(), s, rect);
		}

		public override bool UndoEnabled()
		{
			return false;
		}

		public override Size GetAdjustSize(Graphics g, UTable.CField field)
		{
			Size adjustSize = base.GetAdjustSize(g, field);
			return new Size(checked(adjustSize.Width + 6), adjustSize.Height);
		}
	}
}
