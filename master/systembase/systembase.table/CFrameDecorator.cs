using System.Drawing;

namespace systembase.table
{
	public class CFrameDecorator : IFieldDecorator
	{
		public Color Color;

		public CFrameDecorator(Color color)
		{
			Color = color;
		}

		public void RenderBackground(Graphics g, UTable.CField field, Rectangle rect)
		{
			using (Pen pen = new Pen(Color, 2f))
			{
				Rectangle rect2 = checked(new Rectangle(rect.Left + 2, rect.Top + 2, rect.Width - 3, rect.Height - 3));
				g.DrawRectangle(pen, rect2);
			}
		}

		void IFieldDecorator.RenderBackground(Graphics g, UTable.CField field, Rectangle rect)
		{
			//ILSpy generated this explicit interface implementation from .override directive in RenderBackground
			this.RenderBackground(g, field, rect);
		}

		public void RenderForeground(Graphics g, UTable.CField field, Rectangle rect)
		{
		}

		void IFieldDecorator.RenderForeground(Graphics g, UTable.CField field, Rectangle rect)
		{
			//ILSpy generated this explicit interface implementation from .override directive in RenderForeground
			this.RenderForeground(g, field, rect);
		}
	}
}
