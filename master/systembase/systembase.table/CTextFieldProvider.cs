using System.Windows.Forms;

namespace systembase.table
{
	public class CTextFieldProvider : CFieldProvider
	{
		private ImeMode _imeMode;

		public CTextFieldProvider()
			: this(null)
		{
		}

		public CTextFieldProvider(string caption)
			: this(caption, System.Windows.Forms.ImeMode.NoControl)
		{
		}

		public CTextFieldProvider(string caption, ImeMode imeMode)
			: base(caption)
		{
			_imeMode = imeMode;
		}

		public override IEditor CreateEditor()
		{
			return new CTextBoxEditor();
		}

		public override ImeMode ImeMode()
		{
			return _imeMode;
		}
	}
}
