using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace systembase.table
{
	public class CConstFieldProvider : CFieldProvider
	{
		public class CConstField : UTable.CField
		{
			public override object Value
			{
				get
				{
					return ((CConstFieldProvider)Desc.Provider).Value;
				}
				set
				{
				}
			}

			[DebuggerNonUserCode]
			public CConstField()
			{
			}

			public override void Clear()
			{
			}
		}

		public object Value;

		public CConstFieldProvider(string value)
			: this(null, value)
		{
		}

		public CConstFieldProvider(string caption, object value)
			: base(caption)
		{
			Value = RuntimeHelpers.GetObjectValue(value);
		}

		public override bool UndoEnabled()
		{
			return false;
		}

		public override UTable.CField CreateField()
		{
			return new CConstField();
		}
	}
}
