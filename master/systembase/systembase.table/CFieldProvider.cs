using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace systembase.table
{
	public class CFieldProvider : IFieldProvider
	{
		private string _Caption;

		private int _TabOrder;

		public int BorderLine;

		private static Bitmap _checkedImage = null;

		private static Bitmap _uncheckedImage = null;

		public static int TOGGLERECT_SIZE = 10;

		public bool Toggle;

		public string Caption
		{
			get
			{
				return _Caption;
			}
			set
			{
				_Caption = value;
			}
		}

		public int TabOrder
		{
			get
			{
				return _TabOrder;
			}
			set
			{
				_TabOrder = value;
			}
		}

		public virtual string Clipboard
		{
			get
			{
				if (field.Value != null)
				{
					return field.Value.ToString();
				}
				return "";
			}
			set
			{
				field.SetValueIfValidated(value);
			}
		}

		public CFieldProvider()
			: this(null)
		{
		}

		public CFieldProvider(string caption)
		{
			_TabOrder = 0;
			BorderLine = 15;
			Toggle = false;
			Caption = caption;
		}

		public virtual UTable.CField CreateField()
		{
			return new UTable.CField();
		}

		UTable.CField IFieldProvider.CreateField()
		{
			//ILSpy generated this explicit interface implementation from .override directive in CreateField
			return this.CreateField();
		}

		public virtual void FieldInitialize(UTable.CField field)
		{
		}

		void IFieldProvider.FieldInitialize(UTable.CField field)
		{
			//ILSpy generated this explicit interface implementation from .override directive in FieldInitialize
			this.FieldInitialize(field);
		}

		public virtual IEditor CreateEditor()
		{
			return null;
		}

		IEditor IFieldProvider.CreateEditor()
		{
			//ILSpy generated this explicit interface implementation from .override directive in CreateEditor
			return this.CreateEditor();
		}

		public virtual void EditorInitialize(UTable.CField field, IEditor editor)
		{
		}

		void IFieldProvider.EditorInitialize(UTable.CField field, IEditor editor)
		{
			//ILSpy generated this explicit interface implementation from .override directive in EditorInitialize
			this.EditorInitialize(field, editor);
		}

		public virtual UTable.CSetting Setting()
		{
			return null;
		}

		UTable.CSetting IFieldProvider.Setting()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Setting
			return this.Setting();
		}

		public virtual bool Focusable()
		{
			return true;
		}

		bool IFieldProvider.Focusable()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Focusable
			return this.Focusable();
		}

		public virtual bool UndoEnabled()
		{
			return true;
		}

		bool IFieldProvider.UndoEnabled()
		{
			//ILSpy generated this explicit interface implementation from .override directive in UndoEnabled
			return this.UndoEnabled();
		}

		public virtual ImeMode ImeMode()
		{
			return System.Windows.Forms.ImeMode.Disable;
		}

		ImeMode IFieldProvider.ImeMode()
		{
			//ILSpy generated this explicit interface implementation from .override directive in ImeMode
			return this.ImeMode();
		}

		public virtual bool Draggable()
		{
			return false;
		}

		bool IFieldProvider.Draggable()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Draggable
			return this.Draggable();
		}

		public virtual object ValueRegularize(object value, IEditor editor)
		{
			return value;
		}

		object IFieldProvider.ValueRegularize(object value, IEditor editor)
		{
			//ILSpy generated this explicit interface implementation from .override directive in ValueRegularize
			return this.ValueRegularize(value, editor);
		}

		public virtual void Render(Graphics g, UTable.CField field, Rectangle rect, bool alter)
		{
			UTable.CDynamicSetting s = field.DynamicSetting();
			renderBackground(g, field, s, rect, alter);
			if (field.Decorator != null)
			{
				field.Decorator.RenderBackground(g, field, rect);
			}
			if (field.Table().Focused && field.Focused() && field.Table().FocusFieldDecorator != null)
			{
				field.Table().FocusFieldDecorator.RenderBackground(g, field, rect);
			}
			renderForeground(g, field, s, rect, alter);
			if (field.Decorator != null)
			{
				field.Decorator.RenderForeground(g, field, rect);
			}
			if (field.Table().Focused && field.Focused() && field.Table().FocusFieldDecorator != null)
			{
				field.Table().FocusFieldDecorator.RenderForeground(g, field, rect);
			}
		}

		void IFieldProvider.Render(Graphics g, UTable.CField field, Rectangle rect, bool alter)
		{
			//ILSpy generated this explicit interface implementation from .override directive in Render
			this.Render(g, field, rect, alter);
		}

		protected virtual void renderBackground(Graphics g, UTable.CField field, UTable.CDynamicSetting s, Rectangle rect, bool alter)
		{
			RenderBackgroudRect(g, rect, BackColor(field, s, alter));
		}

		protected virtual void renderForeground(Graphics g, UTable.CField field, UTable.CDynamicSetting s, Rectangle rect, bool alter)
		{
			if (field.Editor() == null)
			{
				RenderValue(g, CreateValueRect(rect, s, field.TopLevelContent().RenderingRect), formatValue(RuntimeHelpers.GetObjectValue(field.Value)), ForeColor(field, s, alter), s.Font(), GetBoundedStringFormat(g, field, s, rect));
			}
		}

		public virtual Size GetAdjustSize(Graphics g, UTable.CField field)
		{
			UTable.CDynamicSetting cDynamicSetting = field.DynamicSetting();
			return g.MeasureString(formatValue(RuntimeHelpers.GetObjectValue(field.Value)), cDynamicSetting.Font(), 100000, cDynamicSetting.GetStringFormat()).ToSize();
		}

		Size IFieldProvider.GetAdjustSize(Graphics g, UTable.CField field)
		{
			//ILSpy generated this explicit interface implementation from .override directive in GetAdjustSize
			return this.GetAdjustSize(g, field);
		}

		protected virtual string formatValue(object v)
		{
			return v?.ToString();
		}

		public StringFormat GetBoundedStringFormat(Graphics g, UTable.CField f, UTable.CDynamicSetting s, Rectangle rect)
		{
			StringFormat stringFormat = s.GetStringFormat();
			Size size = default(Size);
			if ((s.LeftBound() == UTable.EBound.BOUND) | (s.TopBound() == UTable.EBound.BOUND))
			{
				size = g.MeasureString(formatValue(RuntimeHelpers.GetObjectValue(f.Value)), s.Font(), rect.Width, s.GetStringFormat()).ToSize();
			}
			if (s.LeftBound() == UTable.EBound.BOUND && size.Width > rect.Width)
			{
				stringFormat.Alignment = StringAlignment.Near;
			}
			if (s.TopBound() == UTable.EBound.BOUND && size.Height > rect.Height)
			{
				stringFormat.LineAlignment = StringAlignment.Near;
			}
			return stringFormat;
		}

		public static Color BackColor(UTable.CField field, UTable.CDynamicSetting setting, bool alter)
		{
			if (field == field.Table().FocusField && !setting.FocusBackColor().Equals(Color.Transparent) && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.FocusBackColor();
			}
			if (field.Table().SelectRange != null && field.Table().SelectRange.IsInclude(field) && !setting.RangedBackColor().Equals(Color.Transparent) && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.RangedBackColor();
			}
			if (field.Record == field.Table().FocusRecord && !setting.FocusRecordBackColor().Equals(Color.Transparent) && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.FocusRecordBackColor();
			}
			if (alter && !setting.AlterBackColor().Equals(Color.Transparent))
			{
				return setting.AlterBackColor();
			}
			return setting.BackColor();
		}

		public static Color ForeColor(UTable.CField field, UTable.CDynamicSetting setting, bool alter)
		{
			if (field == field.Table().FocusField && !setting.FocusForeColor().Equals(Color.Transparent) && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.FocusForeColor();
			}
			if (field.Table().SelectRange != null && field.Table().SelectRange.IsInclude(field) && !setting.RangedForeColor().Equals(Color.Transparent) && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.RangedForeColor();
			}
			if (field.Record == field.Table().FocusRecord && !setting.FocusRecordForeColor().Equals(Color.Transparent) && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.FocusRecordForeColor();
			}
			return setting.ForeColor();
		}

		public static Color CaptionBackColor(UTable.CField field, UTable.CDynamicSetting setting, bool drag)
		{
			if (drag && !setting.DraggingBackColor().Equals(Color.Transparent))
			{
				return setting.DraggingBackColor();
			}
			if (field.Table().FocusField != null && field.Table().FocusField.Desc.Layout != null && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways) && field.Content().TopLevelContent() == field.Table().HeaderContent && !setting.FocusCaptionBackColor().Equals(Color.Transparent) && field.Content().Level().Equals(field.Table().FocusField.Content().Level()) && field == field.Record.FindField(field.Table().FocusField.Desc.Layout.Point()))
			{
				return setting.FocusCaptionBackColor();
			}
			if (!setting.FocusRecordCaptionBackColor().Equals(Color.Transparent) && field.Record == field.Table().FocusRecord && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.FocusRecordCaptionBackColor();
			}
			if (!setting.IncludeFocusRecordCaptionBackColor().Equals(Color.Transparent) && field.Table().FocusField != null && _GetIncludeFocusRecord(field.Table().FocusRecord).IsChild(field) && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.IncludeFocusRecordCaptionBackColor();
			}
			return setting.CaptionBackColor();
		}

		public static Color CaptionForeColor(UTable.CField field, UTable.CDynamicSetting setting, bool drag)
		{
			if (drag && !setting.DraggingForeColor().Equals(Color.Transparent))
			{
				return setting.DraggingForeColor();
			}
			if (field.Table().FocusField != null && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways) && field.Content().TopLevelContent() == field.Table().HeaderContent && !setting.FocusCaptionForeColor().Equals(Color.Transparent) && field.Content().Level().Equals(field.Table().FocusField.Content().Level()) && field == field.Record.FindField(field.Table().FocusField.Desc.Layout.Point()))
			{
				return setting.FocusCaptionForeColor();
			}
			if (!setting.FocusRecordCaptionForeColor().Equals(Color.Transparent) && field.Record == field.Table().FocusRecord && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.FocusRecordCaptionForeColor();
			}
			if (!setting.IncludeFocusRecordCaptionForeColor().Equals(Color.Transparent) && field.Table().FocusRecord != null && _GetIncludeFocusRecord(field.Table().FocusRecord).IsChild(field) && (field.Table().Focused | (field.Table().Editor() != null) | field.Table().Setting.FocusColorAlways))
			{
				return setting.IncludeFocusRecordCaptionForeColor();
			}
			return setting.CaptionForeColor();
		}

		private static UTable.CRecord _GetIncludeFocusRecord(UTable.CRecord record)
		{
			UTable.CRecord cRecord = record;
			while (true)
			{
				if (cRecord.Content.LimitIncludeFocusColor)
				{
					return cRecord;
				}
				if (cRecord.Content.ParentRecord == null)
				{
					break;
				}
				cRecord = cRecord.Content.ParentRecord;
			}
			return cRecord;
		}

		public static RectangleF CreateValueRect(Rectangle rect, UTable.CDynamicSetting s, Rectangle rrect)
		{
			float num = rect.Left;
			float num2 = rect.Right;
			float num3 = rect.Top;
			float num4 = rect.Bottom;
			if (s.StayHorizontal() == UTable.EStay.STAY)
			{
				num = Math.Max(num, rrect.Left);
				num2 = Math.Min(num2, rrect.Right);
			}
			if (s.StayVertical() == UTable.EStay.STAY)
			{
				num3 = Math.Max(num3, rrect.Top);
				num4 = Math.Min(num4, rrect.Bottom);
			}
			return new RectangleF(num, num3 + 2f, num2 - num, num4 - num3 - 2f);
		}

		public static void RenderBackgroudRect(Graphics g, Rectangle rect, Color color)
		{
			using (SolidBrush brush = new SolidBrush(color))
			{
				g.FillRectangle(brush, rect);
			}
		}

		public static void RenderValue(Graphics g, RectangleF rect, string formattedValue, Color color, Font font, StringFormat stringFormat)
		{
			if (!string.IsNullOrEmpty(formattedValue))
			{
				using (SolidBrush brush = new SolidBrush(color))
				{
					g.DrawString(ValueSanitize(formattedValue), font, brush, rect, stringFormat);
				}
			}
		}

		public static void RenderCaptionBackgroudRect(Graphics g, Rectangle rect, Color color, UTable.ECaptionStyle style)
		{
			//Discarded unreachable code: IL_00d2, IL_00f9
			checked
			{
				switch (style)
				{
				case UTable.ECaptionStyle.METALLIC:
				{
					using (LinearGradientBrush brush3 = new LinearGradientBrush(rect, color, GradientColor(color, 32), LinearGradientMode.Vertical))
					{
						g.FillRectangle(brush3, rect);
					}
					using (Pen pen = new Pen(GradientColor(color, 64)))
					{
						Point point = new Point(rect.X + 1, rect.Y + 1);
						Point pt = point;
						Point pt2 = new Point(rect.Right - 1, rect.Y + 1);
						g.DrawLine(pen, pt, pt2);
						pt2 = new Point(rect.X + 1, rect.Y + 1);
						Point pt3 = pt2;
						point = new Point(rect.X + 1, rect.Bottom - 1);
						g.DrawLine(pen, pt3, point);
					}
					break;
				}
				case UTable.ECaptionStyle.GRADIENT:
				{
					using (LinearGradientBrush brush2 = new LinearGradientBrush(rect, color, GradientColor(color, 32), LinearGradientMode.Vertical))
					{
						g.FillRectangle(brush2, rect);
					}
					break;
				}
				case UTable.ECaptionStyle.FLAT:
				{
					using (SolidBrush brush = new SolidBrush(color))
					{
						g.FillRectangle(brush, rect);
					}
					break;
				}
				}
			}
		}

		public static void RenderCaptionValue(Graphics g, UTable.CField field, UTable.CSortState sortState, RectangleF rect, string formattedValue, Color color, Font font, StringFormat stringFormat)
		{
			using (SolidBrush brush = new SolidBrush(color))
			{
				if ((rect.Width > 15f) & (field.Table().SortState.Field == field))
				{
					string s = ValueSanitize(formattedValue);
					RectangleF layoutRectangle = new RectangleF(rect.X, rect.Y + 2f, rect.Width - 15f, rect.Height - 2f);
					g.DrawString(s, font, brush, layoutRectangle, stringFormat);
					List<PointF> list = new List<PointF>();
					switch (field.Table().SortState.Order)
					{
					case UTable.CSortState.EOrder.ASCEND:
					{
						PointF item = new PointF(rect.Right - 14f, rect.Bottom - 7f);
						list.Add(item);
						item = new PointF(rect.Right - 2f, rect.Bottom - 7f);
						list.Add(item);
						item = new PointF(rect.Right - 8f, rect.Bottom - 14f);
						list.Add(item);
						break;
					}
					case UTable.CSortState.EOrder.DESCEND:
					{
						PointF item = new PointF(rect.Right - 14f, rect.Bottom - 12f);
						list.Add(item);
						item = new PointF(rect.Right - 2f, rect.Bottom - 12f);
						list.Add(item);
						item = new PointF(rect.Right - 8f, rect.Bottom - 6f);
						list.Add(item);
						break;
					}
					}
					g.FillPolygon(brush, list.ToArray());
				}
				else
				{
					g.DrawString(ValueSanitize(formattedValue), font, brush, rect, stringFormat);
				}
			}
		}

		public static void RenderCheckBox(Graphics g, Rectangle rect, bool value)
		{
			Point point = checked(new Point((int)Math.Round((double)rect.Left + (double)rect.Width / 2.0 - 6.0), (int)Math.Round((double)rect.Top + (double)rect.Height / 2.0 - 6.0)));
			g.SetClip(rect);
			if (_checkedImage == null)
			{
				_checkedImage = new Bitmap(13, 13);
				_uncheckedImage = new Bitmap(13, 13);
				Graphics g2 = Graphics.FromImage(_checkedImage);
				Point glyphLocation = new Point(0, 0);
				CheckBoxRenderer.DrawCheckBox(g2, glyphLocation, CheckBoxState.CheckedNormal);
				Graphics g3 = Graphics.FromImage(_uncheckedImage);
				glyphLocation = new Point(0, 0);
				CheckBoxRenderer.DrawCheckBox(g3, glyphLocation, CheckBoxState.UncheckedNormal);
			}
			if (value)
			{
				g.DrawImage(_checkedImage, point);
			}
			else
			{
				g.DrawImage(_uncheckedImage, point);
			}
			g.ResetClip();
		}

		public static void RenderNarrowChild(Graphics g, Rectangle rect, bool value, bool enabled)
		{
			checked
			{
				if (enabled)
				{
					g.FillRectangle(Brushes.White, rect);
					g.DrawRectangle(Pens.Gray, rect);
					Pen black = Pens.Black;
					Point point = new Point(rect.Left + 3, (int)Math.Round((double)rect.Top + (double)rect.Height / 2.0));
					Point pt = point;
					Point pt2 = new Point(rect.Right - 3, (int)Math.Round((double)rect.Top + (double)rect.Height / 2.0));
					g.DrawLine(black, pt, pt2);
					if (!value)
					{
						Pen black2 = Pens.Black;
						pt2 = new Point((int)Math.Round((double)rect.Left + (double)rect.Width / 2.0), rect.Top + 3);
						Point pt3 = pt2;
						point = new Point((int)Math.Round((double)rect.Left + (double)rect.Width / 2.0), rect.Top + rect.Height - 3);
						g.DrawLine(black2, pt3, point);
					}
				}
				else
				{
					g.FillRectangle(Brushes.LightGray, rect);
					g.DrawRectangle(Pens.Gray, rect);
				}
			}
		}

		protected virtual Rectangle getToggleRect(UTable.CField field, Rectangle rect, int w)
		{
			checked
			{
				int num = TOGGLERECT_SIZE + w;
				return new Rectangle((int)Math.Round((double)rect.Left + (double)rect.Width / 2.0 - (double)num / 2.0), (int)Math.Round((double)rect.Top + (double)rect.Height / 2.0 - (double)num / 2.0), num, num);
			}
		}

		protected static void renderButton(Graphics g, bool down, string formattedValue, Image image, Image disabledImage, UTable.CDynamicSetting s, Rectangle rect)
		{
			if (rect.Width <= 4)
			{
				return;
			}
			Font font = s.Font();
			checked
			{
				int num = rect.X + 2;
				int x = 0;
				int num2 = 0;
				int num3 = 0;
				int num4 = 0;
				StringFormat format;
				if (image == null)
				{
					format = s.GetStringFormat();
				}
				else
				{
					format = s.GetButtonStringFormat();
					int num5 = rect.Width - 4;
					int num6 = rect.Height - 4;
					int num7 = (int)Math.Round(g.MeasureString(formattedValue, font, 1000000, format).Width);
					if (image != null)
					{
						num3 = image.Width;
						num4 = image.Height;
						if ((num3 > num5) | (num4 > num6))
						{
							float num8 = (float)((double)num5 / (double)num3);
							float num9 = (float)((double)num6 / (double)num4);
							if (num8 < num9)
							{
								num3 = (int)Math.Round((float)num3 * num8);
								num4 = (int)Math.Round((float)num4 * num8);
							}
							else
							{
								num3 = (int)Math.Round((float)num3 * num9);
								num4 = (int)Math.Round((float)num4 * num9);
							}
						}
					}
					switch (s.HorizontalAlignment())
					{
					case UTable.EHAlign.LEFT:
						x = rect.X + 2;
						num = rect.X + 2 + num3;
						break;
					case UTable.EHAlign.MIDDLE:
						x = (int)Math.Round((double)rect.X + Math.Max((double)(rect.Width - num3 - num7) / 2.0, 2.0));
						num = (int)Math.Round((double)rect.X + Math.Max((double)(rect.Width - num3 - num7) / 2.0 + (double)num3, num3 + 2));
						break;
					case UTable.EHAlign.RIGHT:
						x = rect.X + Math.Max(rect.Width - num7 - num3 - 2, 2);
						num = rect.X + Math.Max(rect.Width - num7 - 2, num3 + 2);
						break;
					}
					switch (s.VerticalAlignment())
					{
					case UTable.EVAlign.TOP:
						num2 = rect.Y + 2;
						break;
					case UTable.EVAlign.MIDDLE:
						num2 = (int)Math.Round((double)rect.Y + Math.Max((double)(rect.Height - num4) / 2.0, 2.0));
						break;
					case UTable.EVAlign.BOTTOM:
						num2 = rect.Y + Math.Max(rect.Height - num4 - 2, 2);
						break;
					}
				}
				if (s.Editable() == UTable.EAllow.ALLOW)
				{
					int num10 = 0;
					if (down)
					{
						_renderButton_aux(g, rect, s, down: true);
						num10 = 2;
					}
					else
					{
						_renderButton_aux(g, rect, s, down: false);
					}
					if ((((image != null && num3 > 0) ? 1 : 0) & ((num4 > 0) ? 1 : 0)) != 0)
					{
						Rectangle rectangle = new Rectangle(x, num2 + num10, num3, num4);
						Rectangle destRect = rectangle;
						Rectangle srcRect = new Rectangle(0, 0, image.Width, image.Height);
						g.DrawImage(image, destRect, srcRect, GraphicsUnit.Pixel);
					}
					if (!string.IsNullOrEmpty(formattedValue))
					{
						string s2 = ValueSanitize(formattedValue);
						using (SolidBrush brush = new SolidBrush(s.ForeColor()))
						{
							int num11 = rect.Y + 2 + num10;
							RectangleF layoutRectangle = new RectangleF(num, num11, rect.Right + num10 - num, rect.Bottom + num10 - num11);
							g.DrawString(s2, font, brush, layoutRectangle, format);
						}
					}
				}
				else
				{
					_renderButton_aux(g, rect, s, down: false);
					if ((((disabledImage != null && num3 > 0) ? 1 : 0) & ((num4 > 0) ? 1 : 0)) != 0)
					{
						Rectangle srcRect = new Rectangle(x, num2, num3, num4);
						Rectangle destRect2 = srcRect;
						Rectangle rectangle = new Rectangle(0, 0, image.Width, image.Height);
						g.DrawImage(disabledImage, destRect2, rectangle, GraphicsUnit.Pixel);
					}
					if (!string.IsNullOrEmpty(formattedValue))
					{
						string s3 = ValueSanitize(formattedValue);
						int num12 = rect.Y + 2;
						Font font2 = s.Font();
						Color white = Color.White;
						RectangleF layoutRectangle = new RectangleF(num, num12, rect.Right - num, rect.Bottom - num12);
						ControlPaint.DrawStringDisabled(g, s3, font2, white, layoutRectangle, format);
					}
				}
			}
		}

		private static void _renderButton_aux(Graphics g, Rectangle rect, UTable.CDynamicSetting s, bool down)
		{
			//Discarded unreachable code: IL_01c9
			RectangleF rect2 = new RectangleF(rect.X, rect.Y, rect.Width, rect.Height);
			checked
			{
				using (LinearGradientBrush brush = new LinearGradientBrush(rect2, s.ButtonBackColor(), GradientColor(s.ButtonBackColor(), 32), LinearGradientMode.Vertical))
				{
					Rectangle rect3 = new Rectangle(rect.X + 2, rect.Y + 2, rect.Width - 3, rect.Height - 3);
					g.FillRectangle(brush, rect3);
				}
				using (Pen pen = new Pen(GradientColor(s.ButtonBackColor(), -128)))
				{
					Rectangle rect3 = new Rectangle(rect.X + 2, rect.Y + 2, rect.Width - 4, rect.Height - 4);
					g.DrawRectangle(pen, rect3);
				}
				if (down)
				{
					using (Pen pen2 = new Pen(GradientColor(s.ButtonBackColor(), 64)))
					{
						g.DrawLine(pen2, rect.Right - 3, rect.Y + 3, rect.Right - 3, rect.Bottom - 3);
						g.DrawLine(pen2, rect.X + 3, rect.Bottom - 3, rect.Right - 3, rect.Bottom - 3);
					}
					using (Pen pen3 = new Pen(GradientColor(s.ButtonBackColor(), -64)))
					{
						g.DrawLine(pen3, rect.X + 3, rect.Y + 3, rect.Right - 3, rect.Y + 3);
						g.DrawLine(pen3, rect.X + 3, rect.Y + 3, rect.X + 3, rect.Bottom - 3);
					}
				}
				else
				{
					using (Pen pen4 = new Pen(GradientColor(s.ButtonBackColor(), -64)))
					{
						g.DrawLine(pen4, rect.Right - 3, rect.Y + 3, rect.Right - 3, rect.Bottom - 3);
						g.DrawLine(pen4, rect.X + 3, rect.Bottom - 3, rect.Right - 3, rect.Bottom - 3);
					}
					using (Pen pen5 = new Pen(GradientColor(s.ButtonBackColor(), 64)))
					{
						g.DrawLine(pen5, rect.X + 3, rect.Y + 3, rect.Right - 3, rect.Y + 3);
						g.DrawLine(pen5, rect.X + 3, rect.Y + 3, rect.X + 3, rect.Bottom - 3);
					}
				}
			}
		}

		public static string ValueSanitize(object v)
		{
			if (v == null)
			{
				return "";
			}
			return Regex.Replace(v.ToString(), "‐", "-");
		}

		public static Color GradientColor(Color c, int d)
		{
			checked
			{
				int num = unchecked((int)c.R) + d;
				int num2 = unchecked((int)c.G) + d;
				int num3 = unchecked((int)c.B) + d;
				if (num > 255)
				{
					num = 255;
				}
				if (num < 0)
				{
					num = 0;
				}
				if (num2 > 255)
				{
					num2 = 255;
				}
				if (num2 < 0)
				{
					num2 = 0;
				}
				if (num3 > 255)
				{
					num3 = 255;
				}
				if (num3 < 0)
				{
					num3 = 0;
				}
				return Color.FromArgb(num, num2, num3);
			}
		}

		public virtual void SetBorder(UTable.CField field, UTable.CBorder border, bool merged)
		{
			SetFieldBorder(field, border, merged, BorderLine);
		}

		void IFieldProvider.SetBorder(UTable.CField field, UTable.CBorder border, bool merged)
		{
			//ILSpy generated this explicit interface implementation from .override directive in SetBorder
			this.SetBorder(field, border, merged);
		}

		public static void SetFieldBorder(UTable.CField field, UTable.CBorder border, bool merged, int borderLine)
		{
			if (field.Desc.Layout != null)
			{
				int num = borderLine;
				if (merged)
				{
					num &= 0xD;
				}
				border.Fill(field.Desc.Layout, UTable.CBorder.EBorderType.FIELD, num);
			}
		}

		public static void SetCaptionBorder(UTable.CField field, UTable.CBorder border, bool merged, int borderLine)
		{
			if (field.Desc.Layout != null)
			{
				int num = borderLine;
				if (merged)
				{
					num &= 0xD;
				}
				border.Fill(field.Desc.Layout, UTable.CBorder.EBorderType.CAPTION, num);
			}
		}

		public static void FlatBorder(Dictionary<object, UTable.CFieldDesc> descs, object key1, object key2)
		{
			UTable.CGrid.CRegion cRegion = descs[RuntimeHelpers.GetObjectValue(key1)].Layout.Union(descs[RuntimeHelpers.GetObjectValue(key2)].Layout);
			checked
			{
				foreach (UTable.CFieldDesc value in descs.Values)
				{
					if (value.Provider is CFieldProvider && value.Layout != null && ((value.Layout.Row < cRegion.Row + cRegion.Rows) & (value.Layout.Row + value.Layout.Rows > cRegion.Row) & (value.Layout.Col < cRegion.Col + cRegion.Cols) & (value.Layout.Col + value.Layout.Cols > cRegion.Col)))
					{
						CFieldProvider cFieldProvider = (CFieldProvider)value.Provider;
						cFieldProvider.BorderLine = 0;
						if (value.Layout.Row == cRegion.Row)
						{
							cFieldProvider.BorderLine |= 1;
						}
						if (value.Layout.Row + value.Layout.Rows == cRegion.Row + cRegion.Rows)
						{
							cFieldProvider.BorderLine |= 2;
						}
						if (value.Layout.Col == cRegion.Col)
						{
							cFieldProvider.BorderLine |= 4;
						}
						if (value.Layout.Col + value.Layout.Cols == cRegion.Col + cRegion.Cols)
						{
							cFieldProvider.BorderLine |= 8;
						}
						cFieldProvider = null;
					}
				}
			}
		}

		public UTable.CField SetToggle(UTable.CField field)
		{
			field.MouseDown += _toggle_mousedown;
			field.KeyPress += _toggle_keyPress;
			Toggle = true;
			return field;
		}

		private static void _toggle_mousedown(UTable.CField field, Point location, MouseEventArgs e)
		{
			if (!(field.Desc.Provider is CFieldProvider))
			{
				return;
			}
			CFieldProvider cFieldProvider = (CFieldProvider)field.Desc.Provider;
			if (e.Button == MouseButtons.Left && field.Table().FocusField == field)
			{
				UTable.CRenderCache.CField cField = field.Table().RenderCache.FindField(field);
				if (cField != null && cField.Field.Editable() == UTable.EAllow.ALLOW && (TOGGLERECT_SIZE == 0 || cFieldProvider.getToggleRect(cField.Field, cField.Rect, 2).Contains(e.Location)))
				{
					field.Desc.Provider.ToggleValue(field, !Conversions.ToBoolean(field.Value));
				}
			}
		}

		private static void _toggle_keyPress(UTable.CField field, KeyPressEventArgs e)
		{
			if (field.Desc.Provider is CFieldProvider)
			{
				using (field.Table().RenderBlock())
				{
					if (e.KeyChar.Equals(' ') && field.Editable() == UTable.EAllow.ALLOW)
					{
						field.Desc.Provider.ToggleValue(field, !Conversions.ToBoolean(field.Value));
						if (field.Table().SelectRange != null && field.Table().Setting.RangeToggleValue)
						{
							foreach (List<UTable.CField> item in field.Table().SelectRange.FieldMatrix())
							{
								foreach (UTable.CField item2 in item)
								{
									if (item2 != null && item2.Desc.Provider is CFieldProvider && ((CFieldProvider)item2.Desc.Provider).Toggle && item2 != field && item2.Editable() == UTable.EAllow.ALLOW)
									{
										item2.Desc.Provider.ToggleValue(item2, RuntimeHelpers.GetObjectValue(field.Value));
									}
								}
							}
						}
					}
				}
			}
		}

		public virtual void ToggleValue(UTable.CField field, object value)
		{
			field.SetValueIfValidated(RuntimeHelpers.GetObjectValue(value));
		}

		void IFieldProvider.ToggleValue(UTable.CField field, object value)
		{
			//ILSpy generated this explicit interface implementation from .override directive in ToggleValue
			this.ToggleValue(field, value);
		}

		public virtual UTable.CFieldModify CreateFieldModify(UTable.CField field)
		{
			return new UTable.CFieldModify(field);
		}

		UTable.CFieldModify IFieldProvider.CreateModifyField(UTable.CField field)
		{
			//ILSpy generated this explicit interface implementation from .override directive in CreateFieldModify
			return this.CreateFieldModify(field);
		}
	}
}
