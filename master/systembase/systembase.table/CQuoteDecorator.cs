using System.Collections.Generic;
using System.Drawing;

namespace systembase.table
{
	public class CQuoteDecorator : IFieldDecorator
	{
		public Color Color;

		public CQuoteDecorator(Color color)
		{
			Color = color;
		}

		public void RenderBackground(Graphics g, UTable.CField field, Rectangle rect)
		{
			checked
			{
				using (SolidBrush brush = new SolidBrush(Color))
				{
					List<PointF> list = new List<PointF>();
					PointF item = new PointF(rect.Left, rect.Top);
					list.Add(item);
					item = new PointF(rect.Left + 8, rect.Top);
					list.Add(item);
					item = new PointF(rect.Left, rect.Top + 8);
					list.Add(item);
					g.FillPolygon(brush, list.ToArray());
				}
			}
		}

		void IFieldDecorator.RenderBackground(Graphics g, UTable.CField field, Rectangle rect)
		{
			//ILSpy generated this explicit interface implementation from .override directive in RenderBackground
			this.RenderBackground(g, field, rect);
		}

		public void RenderForeground(Graphics g, UTable.CField field, Rectangle rect)
		{
		}

		void IFieldDecorator.RenderForeground(Graphics g, UTable.CField field, Rectangle rect)
		{
			//ILSpy generated this explicit interface implementation from .override directive in RenderForeground
			this.RenderForeground(g, field, rect);
		}
	}
}
