using Microsoft.VisualBasic.CompilerServices;
using System.Diagnostics;
using System.Drawing;

namespace systembase.table
{
	public class CNarrowChildFieldProvider : CFieldProvider
	{
		public class CNarrowChildField : UTable.CField
		{
			protected override object rawValue
			{
				get
				{
					if (Record.Child != null && Record.Child.Records.Count > 0)
					{
						return Record.Child.Visible;
					}
					return false;
				}
				set
				{
					if (Record.Child != null)
					{
						Record.Child.Visible = Conversions.ToBoolean(value);
					}
				}
			}

			[DebuggerNonUserCode]
			public CNarrowChildField()
			{
			}

			public override void Clear()
			{
			}
		}

		public override string Clipboard
		{
			get
			{
				return "";
			}
			set
			{
			}
		}

		public CNarrowChildFieldProvider()
			: this(null)
		{
		}

		public CNarrowChildFieldProvider(string caption)
			: base(caption)
		{
		}

		public override UTable.CField CreateField()
		{
			return new CNarrowChildField();
		}

		public override void FieldInitialize(UTable.CField field)
		{
			SetToggle(field);
		}

		protected override void renderForeground(Graphics g, UTable.CField field, UTable.CDynamicSetting s, Rectangle rect, bool alter)
		{
			CFieldProvider.RenderNarrowChild(g, getToggleRect(field, rect, 0), Conversions.ToBoolean(field.Value), (field.Record.Child != null && field.Record.Child.Records.Count > 0) ? true : false);
		}

		public override Size GetAdjustSize(Graphics g, UTable.CField field)
		{
			return checked(new Size(CFieldProvider.TOGGLERECT_SIZE + 8, CFieldProvider.TOGGLERECT_SIZE + 8));
		}

		public override bool UndoEnabled()
		{
			return false;
		}
	}
}
