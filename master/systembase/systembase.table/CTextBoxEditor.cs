using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace systembase.table
{
	public class CTextBoxEditor : TextBox, IEditor
	{
		private IEditor.LeaveEventHandler _LeaveEvent;

		private IEditor.ValueChangedEventHandler _ValueChangedEvent;

		public object Value
		{
			get
			{
				return Text;
			}
			set
			{
				Text = Conversions.ToString(value);
			}
		}

		public event IEditor.LeaveEventHandler _Leave
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				_LeaveEvent = (IEditor.LeaveEventHandler)Delegate.Combine(_LeaveEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				_LeaveEvent = (IEditor.LeaveEventHandler)Delegate.Remove(_LeaveEvent, value);
			}
		}

		public event IEditor.ValueChangedEventHandler _ValueChanged
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				_ValueChangedEvent = (IEditor.ValueChangedEventHandler)Delegate.Combine(_ValueChangedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				_ValueChangedEvent = (IEditor.ValueChangedEventHandler)Delegate.Remove(_ValueChangedEvent, value);
			}
		}

		[DebuggerNonUserCode]
		public CTextBoxEditor()
		{
			base.TextChanged += CTextBoxEditor_TextChanged;
		}

		public void Initialize(UTable.CField field)
		{
			UTable.CDynamicSetting cDynamicSetting = field.DynamicSetting();
			Font = cDynamicSetting.Font();
			switch (cDynamicSetting.HorizontalAlignment())
			{
			case UTable.EHAlign.LEFT:
				TextAlign = HorizontalAlignment.Left;
				break;
			case UTable.EHAlign.MIDDLE:
				TextAlign = HorizontalAlignment.Center;
				break;
			case UTable.EHAlign.RIGHT:
				TextAlign = HorizontalAlignment.Right;
				break;
			}
			cDynamicSetting = null;
		}

		void IEditor.Initialize(UTable.CField field)
		{
			//ILSpy generated this explicit interface implementation from .override directive in Initialize
			this.Initialize(field);
		}

		public void EditEnter(char key)
		{
			if (key != 0)
			{
				Text = Conversions.ToString(key);
				SelectionStart = 1;
			}
		}

		void IEditor.EditEnter(char key)
		{
			//ILSpy generated this explicit interface implementation from .override directive in EditEnter
			this.EditEnter(key);
		}

		public void EditLeave()
		{
		}

		void IEditor.EditLeave()
		{
			//ILSpy generated this explicit interface implementation from .override directive in EditLeave
			this.EditLeave();
		}

		protected override bool IsInputKey(Keys keyData)
		{
			switch (keyData)
			{
			case Keys.Up:
				if (!Multiline || SelectionStart <= (Text + "\r").IndexOf("\r"))
				{
					return false;
				}
				break;
			case Keys.Down:
				if (!Multiline || SelectionStart >= ("\r" + Text).LastIndexOf("\r"))
				{
					return false;
				}
				break;
			case Keys.Left:
				if ((SelectionLength == 0) & (SelectionStart == 0))
				{
					return false;
				}
				break;
			case Keys.Right:
				if ((SelectionLength == 0) & (SelectionStart == Text.Length))
				{
					return false;
				}
				break;
			}
			return base.IsInputKey(keyData);
		}

		protected override bool ProcessDialogKey(Keys keyData)
		{
			switch (keyData)
			{
			case Keys.Escape:
				_LeaveEvent?.Invoke(this, "ESC");
				return true;
			case Keys.Up:
				_LeaveEvent?.Invoke(this, "UP");
				return true;
			case Keys.Down:
				_LeaveEvent?.Invoke(this, "DOWN");
				return true;
			case Keys.Left:
				_LeaveEvent?.Invoke(this, "LEFT");
				return true;
			case Keys.Right:
				_LeaveEvent?.Invoke(this, "RIGHT");
				return true;
			case Keys.Return:
				if (!Multiline)
				{
					_LeaveEvent?.Invoke(this, "ENTER");
					return true;
				}
				break;
			case Keys.LButton | Keys.MButton | Keys.Back | Keys.Shift:
				if (!Multiline)
				{
					_LeaveEvent?.Invoke(this, "ENTER_PREV");
					return true;
				}
				break;
			case Keys.Tab:
				_LeaveEvent?.Invoke(this, "TAB");
				return true;
			case Keys.LButton | Keys.Back | Keys.Shift:
				_LeaveEvent?.Invoke(this, "TAB_PREV");
				return true;
			}
			return base.ProcessDialogKey(keyData);
		}

		private void CTextBoxEditor_TextChanged(object sender, EventArgs e)
		{
			_ValueChangedEvent?.Invoke(this);
		}

		public Control Control()
		{
			return this;
		}

		Control IEditor.Control()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Control
			return this.Control();
		}
	}
}
