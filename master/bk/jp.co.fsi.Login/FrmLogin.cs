﻿using jp.co.fsi.ClientCommon;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jp.co.fsi.Login
{

	/// <summary>
	/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
	/// <para>■ 機能説明： ログイン画面</para>
	/// <para>■ 機能概要： システム利用者認証を行う。</para>
	/// <para>■</para>
	/// <para>■ 作成者：   FSI</para>
	/// <para>■ 作成日：   2019-11</para>
	/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
	/// </summary>
	public partial class FrmLogin : BaseForm
	{

		private Point mousePoint;

		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： コンストラクタ</para>
		/// <para>■ 機能概要： システム利用者認証を行う。</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		public FrmLogin()
		{
			InitializeComponent();
		}


		#region 画面初期化

		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： 画面初期化</para>
		/// <para>■ 機能概要： 画面インスタンス後に実行されるメソッド</para>
		/// <para>■ 　　　　　　ここで画面の初期化を行う。</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		protected override void InitForm()
		{
			string userid = this.Config.LoadCommonConfig("UserInfo", "userid");
			this.txtUserId.Text = userid;

			this.lblLoginTitle.Text = this.UInfo.KaishaNm;

			ClientUtil.CircleForm(this, 10);


		}
		#endregion

		#region ボタンクリックイベント
		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： ボタンクリックイベント</para>
		/// <para>■ 機能概要： ログインあるいはキャンセルボタンが押された場合に</para>
		/// <para>■ 　　　　　　実行するイベント</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		private void btn_Click(object sender, EventArgs e)
		{

			switch(((Button)sender).Name)
			{
				case "btnLogin":

					bLogin();

					break;
				case "btnCancel":

					bCancel();

					break;
			}

		}
		#endregion

		#region プライベートメソッド

		#region ログイン処理
		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： ログイン処理</para>
		/// <para>■ 機能概要： ユーザＩＤ、パスワードで認証チェックを行い</para>
		/// <para>■ 　　　　　　ＯＫの場合はメニュー画面を起動する。</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		private void bLogin()
		{

			string tantoshaCd;// 担当者ID
			string userId;    // ユーザID
			string passWord;  // パスワード
			string seriFlg;   // セリ販売の画面を1：表示する 0：表示しない
			string kobaiFlg;  // 購買の画面を1：表示する 0：表示しない
			string zaimuFlg;  // 財務の画面を1：表示する 0：表示しない
			string kyuyoFlg;  // 給与の画面を1：表示する 0：表示しない


			// TODO 改修したほうがいい部分 
			DbParamCollection dpc = new DbParamCollection();
			StringBuilder sql = new StringBuilder();

			sql.AppendLine("SELECT");
			sql.AppendLine("KAISHA_CD ");
            sql.AppendLine(",SHISHO_CD ");
            sql.AppendLine(",TANTOSHA_CD ");
            sql.AppendLine(",USER_ID ");
            sql.AppendLine(",PASSWORD ");
            sql.AppendLine(",TANTOSHA_NM ");
            sql.AppendLine(",SERI_FLG ");
            sql.AppendLine(",KOBAI_FLG ");
            sql.AppendLine(",ZAIMU_FLG ");
            sql.AppendLine(",KYUYO_FLG ");
            sql.AppendLine("FROM TB_CM_TANTOSHA");
            sql.AppendLine("WHERE");
            sql.AppendLine(" KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" USER_ID = @USER_ID AND");
            sql.AppendLine(" PASSWORD = @PASSWORD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
			dpc.SetParam("@USER_ID", SqlDbType.VarChar, 15, this.txtUserId.Text);
			dpc.SetParam("@PASSWORD", SqlDbType.VarChar, 15, this.txtPassWord.Text);
			DataTable dtUser = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

			if (dtUser.Rows.Count > 0)
			{
				tantoshaCd = Util.ToString(dtUser.Rows[0]["TANTOSHA_CD"]);
				userId = Util.ToString(dtUser.Rows[0]["USER_ID"]);
				passWord = Util.ToString(dtUser.Rows[0]["PASSWORD"]);
				seriFlg = Util.ToString(dtUser.Rows[0]["SERI_FLG"]);
				kobaiFlg = Util.ToString(dtUser.Rows[0]["KOBAI_FLG"]);
				zaimuFlg = Util.ToString(dtUser.Rows[0]["ZAIMU_FLG"]);
				kyuyoFlg = Util.ToString(dtUser.Rows[0]["KYUYO_FLG"]);

				//configを書き換える
				this.Config.SetCommonConfig("UserInfo", "usercd", tantoshaCd);
				this.Config.SetCommonConfig("UserInfo", "userid", userId);
				this.Config.SetCommonConfig("UserInfo", "passwd", passWord);
				this.Config.SetCommonConfig("UserInfo", "usehn", seriFlg);
				this.Config.SetCommonConfig("UserInfo", "usekb", kobaiFlg);
				this.Config.SetCommonConfig("UserInfo", "usezm", zaimuFlg);
				this.Config.SetCommonConfig("UserInfo", "useky", kyuyoFlg);
				this.Config.SaveConfig();
                // メニュー画面を起動
                this.Visible = false;
                // アセンブリのロード
                Assembly asm = Assembly.LoadFrom("Menu.exe");
                // フォーム作成
                Type t = asm.GetType("jp.co.fsi.Menu.FrmMenu");
                if (t != null)
                {
                    Object obj = System.Activator.CreateInstance(t);
                    if (obj != null)
                    {
                        BaseForm frm = (BaseForm)obj;
                        frm.ShowDialog(this);

                        frm.Dispose();
                    }
                }
                this.Close();
			}
			else
			{
				Msg.Notice("ユーザーIDもしくはパスワードが不正です。" + Environment.NewLine
					+ "再度入力してください。");
				this.txtUserId.Focus();
			}

		}
		#endregion

		#region キャンセル処理

		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： キャンセル処理</para>
		/// <para>■ 機能概要： キャンセル処理を行う。</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		private void bCancel()
		{
			if (Msg.ConfOKCancel(
						"ログインを取りやめます。" + Environment.NewLine +
						"よろしいですか？")
					== DialogResult.OK)
			{
				this.Close();
			}
		}

		#endregion

		#endregion

		private void FrmLogin_MouseDown(object sender, MouseEventArgs e)
		{
			if ((e.Button & MouseButtons.Left)==MouseButtons.Left)
			{
				mousePoint = new Point(e.X,e.Y);
			}
		}

		private void FrmLogin_MouseMove(object sender, MouseEventArgs e)
		{
			if ((e.Button & MouseButtons.Left)== MouseButtons.Left)
			{
				this.Left += e.X - mousePoint.X;
				this.Top += e.Y - mousePoint.Y;
			}
		}
	}
}
